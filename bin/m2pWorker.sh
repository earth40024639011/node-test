#!/bin/bash

month=${month:-1}
start=${start:-1}
end=${end:-1}
dbHost=${dbHost:-}
dbName=${dbName:-}
queueName=${queueName:-}
read=${read:-p}

while [ $# -gt 0 ]; do
    
    if [[ $1 == *"--"* ]]; then
        param="${1/--/}"
        declare $param="$2"
        # echo $1 $2 // Optional to see the parameter:value result
   fi

    shift
done

if [ -z ${dbHost} ]; then 
    dbHost=""
else
    dbHost="--dbHost=${dbHost}"
fi

if [ -z ${dbName} ]; then 
    dbName=""
else
    dbName="--dbName=${dbName}"
fi

if [ -z ${queueName} ]; then 
    queueName=""
else
    queueName="--queueName=${queueName}"
fi

if [ -z ${read} ]; then 
    read=""
else
    read="--read=${read}"
fi

echo $month $start $end $dbHost $dbName $queueName $read

echo "start running month $month from $start to $end"
for i in `seq $start $end`
do
    if [ "$i" -lt 10 ];
    then
        date="0$i"
        if [ "$i" = 9 ]
        then
            nextDate="10"
        else
            nextDate="0$((i+1))"
        fi
    else
        date="$i" 
        nextDate="$((i+1))"
    fi
    echo "client $client -> $month $date $nextDate"
    echo "docker run --rm -d --name m2m_${dbName}_d${date}_00-d${nextDate}-00 pentor/m2m:latest --s=2020-03-${date}T00:00:00 --e=2020-04-${nextDate}T00:00:00 $dbHost $dbName $queueName $read"
    #docker run --rm -d --name "${client}-${date}-${nextDate}" pentor/m2p:latest --s=2020-"${month}"-"${date}"T00:00:00 --e=2020-"${month}"-"${nextDate}"T00:00:00 --c="${client}"
done

# Build
# docker build -t pentor/m2p:4-final -f m2p.Dockerfile .

# Delete all run
# docker rm -f $(docker ps -a -q --filter ancestor=pentor/m2p:latest --format="{{.ID}}")

# Delete log
# echo "" > $(docker inspect --format='{{.LogPath}}' <container_name_or_id>)

# Clean up docker
# docker system prune -a -f

# Auth to ECR
# aws ecr get-login-password --region ap-southeast-1 | docker login --username AWS --password-stdin 249210355722.dkr.ecr.ap-southeast-1.amazonaws.com
# aws ecr get-login-password --region ap-southeast-1 --profile pentor-m2p | docker login --username AWS --password-stdin 249210355722.dkr.ecr.ap-southeast-1.amazonaws.com

# pull
# docker pull 249210355722.dkr.ecr.ap-southeast-1.amazonaws.com/pentor/m2p:latest

# tag
# docker tag 249210355722.dkr.ecr.ap-southeast-1.amazonaws.com/pentor/m2p:latest pentor/m2p:latest