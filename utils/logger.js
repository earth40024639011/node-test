const winston = require('winston');
const moment = require('moment');
const DateUtils = require('../app/common/dateUtils');
const fs = require('fs');
winston.emitErrs = true;


const logDir = '../log';
// Create the log directory if it does not exist
if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
}

const logFormatter = function(options) {
    // Return string will be passed to logger.

    return options.timestamp() +` | `+ options.level.toUpperCase() +
        ` | `+(options.meta.requestId ? options.meta.requestId : ``)+
        ` `+ (options.message ? options.message : ``) +
        (options.meta && Object.keys(options.meta).length ?
            `\n\t`+ JSON.stringify(options.meta) : `` );
};

const timestamp = function() {
    return moment().format('DD/MM/YYYY HH:mm:ss:SSS');
};


let logger = new winston.Logger({
    // rewriters: [
    //     (level, msg, meta) => {
    //         meta.requestId = '123213213';
    //         return meta;
    //     }
    // ],
    transports: [
        // new (winston.transports.Console)(),
        new winston.transports.File({
            level: 'info',
            name: 'info-file',
            filename: `${logDir}/log-info.log`,
            handleExceptions: true,
            json: true,
            maxsize: 5242880, //5MB
            maxFiles: 5,
            colorize: false,
            formatter: logFormatter,
        }),
        new winston.transports.Console({
            level: 'debug',
            name: 'debug-file',
            handleExceptions: true,
            json: false,
            colorize: true,
            formatter:logFormatter,
            timestamp:timestamp,
        }),
        new winston.transports.Console({
            level: 'error',
            name: 'debug-error-console',
            handleExceptions: true,
            json: true,
            colorize: true,
            formatter:logFormatter,
            timestamp:timestamp,
        }),
        new winston.transports.File({
            level: 'error',
            name: 'error-file',
            filename: `${logDir}/log-error.log`,
            handleExceptions: true,
            json: true,
            maxsize: 5242880, //5MB
            maxFiles: 5,
            colorize: true,
            timestamp:function() {return DateUtils.getCurrentDate(); }
        }),
        new (require('winston-daily-rotate-file'))({
            filename: `${logDir}/-results.log`,
            timestamp: timestamp,
            formatter:logFormatter,
            datePattern: 'yyyy-MM-dd',
            prepend: true,
            level:  'info'
        })
    ],
    exitOnError: false
});

module.exports = logger;
module.exports.stream = {
    write: function(message, encoding){
        logger.info(message);
    }
};