/**
 * Created by boonsongrit on 9/18/17.
 */

const moment = require('moment');
const dateFormat = require('dateformat');
const querystring = require('querystring');
const crypto = require('crypto');
const request = require('request');
const parseString = require('xml2js').parseString;
const _ = require('underscore');

const TokenModel = require("../models/token.model");

const getClientKey = require('../controllers/getClientKey');
const {
    AG_LOGIN_URL = 'EB7_AGIN',
    AG_ENCRYPT_KEY = '7rPaAz8C',
    AG_MD5_KEY = 'HX4vtEcTaFHd',
    AG_PASS = 'agpass123',
    AG_URL = 'http://gi.aggdemo.com:81/',
    CREATE_SESSION_URL = 'http://uatapi.hgline88.com:3381/',
    FORWARD_GAME_URL = 'http://gci.aggdemo.com:81/'
} = getClientKey.getKey();

let CAGENT = AG_LOGIN_URL;
let ENCRYPT_KEY = AG_ENCRYPT_KEY;
let MD5_KEY = AG_MD5_KEY;
let API_URL = AG_URL;

const CLIENT_NAME         = process.env.CLIENT_NAME          || 'SPORTBOOK88';

function encodeDesECB(textToEncode) {
    var key = new Buffer(ENCRYPT_KEY, 'utf8');
    var cipher = crypto.createCipheriv('des-ecb', key, '');
    var encryptedString = cipher.update(textToEncode, 'utf8', 'base64');
    encryptedString += cipher.final('base64');
    return encryptedString;
}

function buildSignature(val) {
    return crypto.createHash('md5').update(val).digest("hex");
}

function callLoginRequest(username, prefix, gameType, currency, callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };


    let queryString = `cagent=${CAGENT}/\\\\/loginname=${prefix}${username}/\\\\/method=lg/\\\\/actype=1/\\\\/password=${AG_PASS}/\\\\/oddtype=${gameType}/\\\\/cur=${currency}`;
    // console.log('queryString===',queryString);
    let desEncrypt = encodeDesECB(queryString);
    let md5Encrypt = buildSignature(`${desEncrypt}${MD5_KEY}`);
    // console.log(desEncrypt,'-------callLoginRequest--------', md5Encrypt)

    if(CLIENT_NAME === 'SPORTBOOK88'){
        let options = {
            url: API_URL+'doBusiness.do?params=' + desEncrypt + "&key=" + md5Encrypt,
            method: 'GET',
            headers: headers
        };

        request(options, (error, response, body) => {
            if (error) {
                callback(error, null);
            } else {
                parseString(body, function (err, result) {
                    callback(null, result.result.$.info);
                });

            }
        });
    }else {
        let data = {
            "desEncrypt": desEncrypt,
            "md5Encrypt": md5Encrypt
        };

        let options = {
            url: `http://lb-sbo-master.api-hub.com/aggaming/login`,
            method: 'POST',
            headers: headers,
            form: data
        };

        console.log('--------------------', options);

        request(options, (error, response, body) => {
            if (error) {
                callback(error, null);
            } else {
                callback(null, body)
            }
        });
    }


}

function callCreateGameSession(username, balance, token, prefix, callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    if(CLIENT_NAME === 'SPORTBOOK88'){
        let options = {
            url: `${CREATE_SESSION_URL}resource/player-tickets.ucs?productid=EB7&username=${prefix}${username}&session_token=${token}&credit=${balance}`,
            method: 'GET',
            headers: headers
        };

        request(options, (error, response, body) => {
            if (error) {
                callback(error, null);
            } else {
                parseString(body, function (err, result) {
                    callback(null, result);
                });
            }
        });
    }else {
        let options = {
            url: `http://lb-sbo-master.api-hub.com/aggaming/createGameSession?prefix=${prefix}&username=${username}&token=${token}&balance=${balance}`,
            method: 'GET',
            headers: headers
        };

        console.log('option ======== ',options);

        request(options, (error, response, body) => {
            if (error) {
                callback(error, null);
            } else {
                callback(null, body)
            }
        });
    }


}


function callLaunchingLiveGameURL(username, sid, gameId, client, oddType, currency, web, prefix) {
    let gameType = 0;
    if(client != 'PC'){
        if(gameId === 1){
            gameType = 12 //12 = AGQ Mobile web version of baccarat
        }else if(gameId === 2){
            gameType = 13; //13 = AGIN Mobile web version of baccarat
        }else {
            gameType = gameId
        }
    }

    let queryString = `cagent=${CAGENT}/\\\\/loginname=${prefix}${username}/\\\\/actype=1/\\\\/lang=6/\\\\/password=${AG_PASS}/\\\\/dm=${web}/\\\\/sid=${sid}/\\\\/gameType=${gameType}/\\\\/oddtype=${oddType}/\\\\/cur=${currency}`;

    // console.log('-----------------',queryString);

    let desEncrypt = encodeDesECB(queryString);
    let md5Encrypt = buildSignature(`${desEncrypt}${MD5_KEY}`);

    let url = `${FORWARD_GAME_URL}forwardGame.do?params=${desEncrypt}&key=${md5Encrypt}`;
    return url;

}

function callCheckTicketStatus(ticketId, callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    const url = `${CREATE_SESSION_URL}resource/CheckTicketStatus.ucs?transactionID=`;

    let options = {
        url: url + ticketId,
        method: 'GET',
        headers: headers
    };

    console.log('callCheckTicketStatus ==  ',options);

    request(options, (error, response, body) => {

        if (error) {
            callback('', null);
        } else {

            parseString(body, function (err, result) {
                console.log('callCheckTicketStatus result ==  ',result);
                callback(null, result)
            });
        }
    });
}

function checkSessionToken(sessionToken, callback) {
    TokenModel.findOne({ uniqueId: sessionToken }, 'username', function (err, result) {
        if(err){
            callback(err, null);
        }else {
            callback(null, result);
        }
    });
}

function getGameType(gameType) {

    if (gameType) {
        switch (gameType) {
            case 'BAC' :
                return "Baccarat";
            case 'DT' :
                return "Dragon Tiger";
            case 'SHB' :
                return "Sicbo";
            case 'ROU' :
                return "Roulette";
            case 'CBAC' :
                return "VIP Baccarat";
            case 'LBAC' :
                return "Bid Baccarat";
            case 'NN' :
                return "Bull Bull";
            case 'ZJH' :
                return "Win Three Cards";
            case 'BF' :
                return "Bull Fight";
            default:
                return "";
        }
    } else {
        return "";
    }

}

function getRound(round) {

    if (round) {
        switch (round) {
            case 'DSP' :
                return "AGIN";
            case 'AGQ' :
                return "AG Deluxe";
            case 'VIP' :
                return "AG VIP";
            case 'LED' :
                return "AG BID";
            case 'EMA' :
                return "AG Euro";
            case 'AGNW' :
                return "AG New World";
            default:
                return "";
        }
    } else {
        return "";
    }

}

function getLimit(limit) {

    if (limit) {
        switch (limit) {
            case 1 :
                return "C";
            case 2 :
                return "A";
            case 3 :
                return "D";
            case 4 :
                return "E";
            case 5 :
                return "G";
            default:
                return "A";
        }
    } else {
        return "";
    }

}

function getPlayType(playType) {

    if (playType) {
        switch (Number.parseInt(playType)) {
            case 1 :
                return "banker";
            case 2 :
                return "player";
            case 3 :
                return "tie";
            case 4 :
                return "banker pair";
            case 5 :
                return "player pair";
            case 6 :
                return "big";
            case 7 :
                return "small";
            case 8 :
                return "banker insurance";
            case 9 :
                return "player insurance";
            case 11 :
                return "banker no commission";
            case 12 :
                return "dragon bonus banker";
            case 13 :
                return "dragon bonus player";
            case 14 :
                return "Super Six";
            case 15 :
                return "Any Pair";
            case 16 :
                return "Perfect Pair";
            case 21 :
                return "Dragon";
            case 22 :
                return "Tiger";
            case 23 :
                return "Tie";
            case 41 :
                return "big";
            case 42 :
                return "small";
            case 43 :
                return "single";
            case 44 :
                return "double";
            case 45 :
                return "any triple";
            case 46 :
                return "wei 1";
            case 47 :
                return "wei 2";
            case 48 :
                return "wei 3";
            case 49 :
                return "wei 4";
            case 50 :
                return "wei 5";
            case 51 :
                return "wei 6";
            case 52 :
                return "single 1";
            case 53 :
                return "single 2";
            case 54 :
                return "single 3";
            case 55 :
                return "single 4";
            case 56 :
                return "single 5";
            case 57 :
                return "single 6";
            case 58 :
                return "double 1";
            case 59 :
                return "double 2";
            case 60 :
                return "double 3";
            case 61 :
                return "double 4";
            case 62 :
                return "double 5";
            case 63 :
                return "double 6";
            case 64 :
                return "combine 12";
            case 65 :
                return "combine 13";
            case 66 :
                return "combine 14";
            case 67 :
                return "combine 15";
            case 68 :
                return "combine 16";
            case 69 :
                return "combine 23";
            case 70 :
                return "combine 24";
            case 71 :
                return "combine 25";
            case 72 :
                return "combine 26";
            case 73 :
                return "combine 34";
            case 74 :
                return "combine 35";
            case 75 :
                return "combine 36";
            case 76 :
                return "combine 45";
            case 77 :
                return "combine 46";
            case 78 :
                return "combine 56";
            case 79 :
                return "sum 4";
            case 80 :
                return "sum 5";
            case 81 :
                return "sum 6";
            case 82 :
                return "sum 7";
            case 83 :
                return "sum 8";
            case 84 :
                return "sum 9";
            case 85 :
                return "sum 10";
            case 86 :
                return "sum 11";
            case 87 :
                return "sum 12";
            case 88 :
                return "sum 13";
            case 89 :
                return "sum 14";
            case 90 :
                return "sum 15";
            case 91 :
                return "sum 16";
            case 92 :
                return "sum 17";
            case 101 :
                return "direct";
            case 102 :
                return "separate";
            case 103 :
                return "street";
            case 104 :
                return "triangle";
            case 105 :
                return "four_num";
            case 106 :
                return "corner";
            case 107 :
                return "column1";
            case 108 :
                return "column2";
            case 109 :
                return "column3";
            case 110 :
                return "line";
            case 111 :
                return "dozen1";
            case 112 :
                return "dozen2";
            case 113 :
                return "dozen3";
            case 114 :
                return "red";
            case 115 :
                return "black";
            case 116 :
                return "big";
            case 117 :
                return "small";
            case 118 :
                return "odd";
            case 119 :
                return "event";
            case 207 :
                return "Banker 1 Equal";
            case 208 :
                return "Banker 1 Double";
            case 209 :
                return "Banker 2 Equal";
            case 210 :
                return "Banker 2 Double";
            case 211 :
                return "Player 1 Equal";
            case 212 :
                return "Player 1 Double";
            case 213 :
                return "Player 2 Equal";
            case 214 :
                return "Player 2 Double";
            case 215 :
                return "player 3 Equal";
            case 216 :
                return "Player 3 Double";
            case 217 :
                return "Banker 3 Equal";
            case 218 :
                return "Banker 3 Double";
            case 260 :
                return "Dragon";
            case 261 :
                return "Phoenix";
            case 262 :
                return "Pair 8 Plus";
            case 263 :
                return "Flush";
            case 264 :
                return "Straight";
            case 265 :
                return "Three of a Kind";
            case 266 :
                return "straight Flush";
            case 270 :
                return "Black Bull";
            case 271 :
                return "Red Bull";
            case 272 :
                return "Tie";
            case 273 :
                return "Bull 1";
            case 274 :
                return "Bull 2";
            case 275 :
                return "Bull 3";
            case 276 :
                return "Bull 4";
            case 277 :
                return "Bull 5";
            case 278 :
                return "Bull 6";
            case 279 :
                return "Bull 7";
            case 280 :
                return "Bull 8";
            case 281 :
                return "Bull 9";
            case 282 :
                return "Bull Bull";
            case 283 :
                return "Double Bull";
            case 284 :
                return "SV/GL/Bomb/5calf";

            default:
                return "";
        }
    } else {
        return "";
    }

}



module.exports = {
    encodeDesECB,
    buildSignature,
    callLaunchingLiveGameURL,
    callCreateGameSession,
    callLoginRequest,
    callCheckTicketStatus,
    checkSessionToken,
    getGameType,
    getPlayType,
    getRound,
    getLimit
}
