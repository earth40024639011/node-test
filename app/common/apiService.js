const MemberModel = require('../models/member.model');
const request = require('request');
const _ = require('underscore');
const mongoose = require('mongoose');
const BetTransactionModel = require('../models/betTransaction.model.js');
const async = require("async");
const moment = require('moment');
const querystring = require('querystring');
const Crypto = require("crypto");

const Hashing = require('../common/hashing');

const roundTo = require('round-to');

const ApiBetTransactionModel = require('../models/apiBetTransaction.model.js');
const ApiSettleTransactionModel = require('../models/apiSettleTransaction.model.js');
const ApiCancelMatchModel = require('../models/apiCancelMatch.model.js');
const ApiCancelBetModel = require('../models/apiCancelBet.model.js');

const CERT = process.env.APP_ID || 'TF33';
const HOST_NAME = process.env.HOST_NAME || 'http://localhost:8002';
const SLOT_XO_SECRET_KEY = process.env.SLOT_XO_SECRET_KEY || 'qc8xtfra7ga56';

const DRAW = "DRAW";
const WIN = "WIN";
const HALF_WIN = "HALF_WIN";
const LOSE = "LOSE";
const HALF_LOSE = "HALF_LOSE";


const SECRET_KEY = "AMB";


function getBalance(endpoint, username, callback) {


    let bodyHashing = username;
    const signature = Hashing.makeSignature(SECRET_KEY, bodyHashing);

    const headers = {
        'Content-Type': 'application/json'
    };

    let form = {
        playerId: username,
        signature: signature
    };

    const option = {
        url: `${endpoint}/callback/getBalance`,
        method: 'POST',
        headers: headers,
        form: form
    };


    request(option, (err, response, body) => {
        if (err) {
            callback(err, null);
        } else {

            let responseBody = JSON.parse(response.body);


            try {
                if (responseBody.code == 200) {
                    callback(null, responseBody.balance);
                } else {
                    callback(responseBody, null);
                }
            } catch (e) {
                callback(e, null);
            }
        }
    });
}


function placeBet(endpoint, requestBody, callback) {

    console.log("============  placeBet : ", requestBody)

    function updateBet(body, result, updateBetCallback) {

        let sendingBody = {
            playerId: body.username,
            memberCredit: body.memberCredit,
            betId: body.betId,
            betAmount: body.amount,
            playerBalance: body.memberCredit,
            betTime: moment(body.createdDate).format('YYYY-MM-DD HH:mm:ss'),
            gameType: body.gameType,
            priceType: body.priceType,
            currency: body.currency,
            ipAddress: body.ipAddress,
            gameDate: body.gameDate,
            status: 'RUNNING',
            game: 'SPORTBOOK',
            result: JSON.stringify(result)
        };

        if (sendingBody.gameType === 'TODAY') {
            sendingBody.hdp = {
                matchId: body.hdp.matchId,
                matchDate: "2014-09-30 14:16:32",
                matchName: body.hdp.matchName,
                matchType: body.hdp.matchType,
                odd: body.hdp.odd,
                oddType: body.hdp.oddType,
                handicap: body.hdp.handicap,
                bet: body.hdp.bet,
                score: body.hdp.score
            };
        } else {
            sendingBody.parlay = body.parlay;
        }


        let model = ApiBetTransactionModel(sendingBody);

        model.save((err, response) => {

            if (err) {
                updateBetCallback(err, null);
            } else {
                updateBetCallback(null, response);
            }
        });
    }

    try {
        let bodyHashing = requestBody.username + requestBody.betId + moment(requestBody.createdDate).format('YYYY-MM-DD HH:mm:ss');
        const signature = Hashing.makeSignature(SECRET_KEY, bodyHashing);

        let body = {
            playerId: requestBody.username,
            betId: requestBody.betId,
            betAmount: requestBody.amount,
            playerBalance: requestBody.memberCredit,
            betTime: moment(requestBody.createdDate).format('YYYY-MM-DD HH:mm:ss'),
            gameType: requestBody.gameType,
            sportType : requestBody.sportType,
            status: requestBody.status,
            currency: requestBody.currency,
            ipAddress: requestBody.ipAddress,
            signature: signature
        };

        if (requestBody.gameType === 'TODAY') {
            body.hdp = {
                matchId: requestBody.hdp.matchId,
                matchDate: requestBody.hdp.matchDate,
                leagueName: requestBody.hdp.leagueName,
                matchName: requestBody.hdp.matchName.en.h + '|' + requestBody.hdp.matchName.en.a,
                matchType: requestBody.hdp.matchType,
                odd: requestBody.hdp.odd,
                oddType: requestBody.hdp.oddType,
                handicap: requestBody.hdp.handicap,
                bet: requestBody.hdp.bet,
                score: requestBody.hdp.score
            };
        } else {

            let matches = _.map(requestBody.parlay.matches, (item) => {
                let newItem = Object.assign({}, item)._doc;
                return {
                    matchId: newItem.matchId,
                    matchDate: moment(newItem.matchDate).format('YYYY-MM-DD HH:mm:ss'),
                    odd: newItem.odd,
                    oddType: newItem.oddType,
                    handicap: newItem.handicap,
                    bet: newItem.bet,
                    score: newItem.score,
                    matchType: 'NONE_LIVE',
                }

            });
            body.parlay = {};
            body.parlay.odd = requestBody.parlay.odds;
            body.parlay.matches = matches;
        }

        console.log('before : ', body)

        const headers = {
            'Content-Type': 'application/json'
        };

        const option = {
            url: `${endpoint}/callback/bet?cert=${CERT}`,
            method: 'POST',
            headers: headers,
            form: body
        };


        request(option, (err, response, body) => {
            if (err) {
                callback(err, null);
            } else {
                try {

                    let responseBody = JSON.parse(response.body);

                    updateBet(requestBody, responseBody, (err, response) => {

                        if (responseBody.code == 200) {
                            callback(null, responseBody);
                        } else {
                            callback(responseBody, null);
                        }
                    });
                } catch (e) {
                    updateBet(requestBody, e, (err, response) => {
                        callback(e, null);
                    });
                }
            }
        });
    } catch (err) {

        updateBet(requestBody, err, (err, response) => {
            callback(err, null);
        });

    }

}


function settle(endpoint, settleInfo, requestBody, callback) {


    console.log("============  settle : ", requestBody);


    function settleFunction(settleInfo,body, result, settleCallback) {
        try {
            let txns = _.map(body, (item) => {
                return {
                    "username": item.memberId.username_lower,
                    "betId": item.betId,
                    "betAmount": item.amount,
                    "betTime": moment(item.createdDate).format('YYYY-MM-DD HH:mm:ss'),
                    "winLose": item.commission.member.totalWinLoseCom,
                    "betResult": item.betResult,

                }
            });

            let sendingBody = {
                "action": settleInfo.action,
                "matchId": settleInfo.matchId,
                "scoreHT": settleInfo.scoreHT,
                "scoreFT": settleInfo.scoreFT,
                "txns": txns,
                "result": JSON.stringify(result),
                "resultCode":result.code
            };

            let model = ApiSettleTransactionModel(sendingBody);

            model.save((err, response) => {

                if (err) {
                    console.log(err)
                    settleCallback(err, null);
                } else {
                    console.log('eee : ',response)
                    settleCallback(null, response);
                }
            });
        }catch (e){
            console.log(e)
        }
    }


    let bodyHashing = settleInfo.action + settleInfo.matchId;
    const signature = Hashing.makeSignature(SECRET_KEY, bodyHashing);

        let txns = _.map(requestBody, (item) => {
            return {
                "playerId": item.memberId.username_lower,
                "betId": item.betId,
                "betAmount": item.amount,
                "betTime": moment(item.createdDate).format('YYYY-MM-DD HH:mm:ss'),
                "winLose": item.commission.member.winLose,
                "winLoseCom": item.commission.member.winLoseCom,
                "totalWinLoseCom": item.commission.member.totalWinLoseCom,
                "betResult": item.betResult,
                "scoreHT": settleInfo.scoreHT,
                "scoreFT": settleInfo.scoreFT,
            }
        });

        let body = {
            "action": settleInfo.action,
            "matchId": settleInfo.matchId,
            "txns": txns,
            "signature": signature
        };

        const headers = {
            'Content-Type': 'application/json'
        };

        const option = {
            url: `${endpoint}/callback/settle?cert=${CERT}`,
            method: 'POST',
            headers: headers,
            form: body
        };

        request(option, (err, response, body) => {
            if (err) {
                callback(err, null);
            } else {

                let responseBody = JSON.parse(response.body);

                try {

                    settleFunction(settleInfo,requestBody, responseBody, (err, response) => {
                        if (responseBody.code == 200) {
                            callback(null, responseBody.result);
                        } else {
                            callback(responseBody, null);
                        }
                    });
                } catch (e) {
                    settleFunction(settleInfo,requestBody, e, (err, response) => {
                        callback(e, null);
                    });
                }
            }
        });

}


function cancelMatch(endpoint, matchId, remark, betResponse, callback) {

    console.log("============  cancelMatch : ");
    async.series([callback => {

        let txns = _.map(betResponse, (item) => {
            return {
                "username": item.memberId.username_lower,
                "betId": item.betId,
                "betAmount": item.amount,
                "betTime": moment(item.createdDate).format('YYYY-MM-DD HH:mm:ss')
            }
        });

        let body = {
            "matchId": matchId,
            "txns": txns,
            "remark": remark
        };

        let model = ApiCancelMatchModel(body);

        model.save((err, response) => {

            if (err) {
                callback(err, null);
            } else {
                callback(null, response);
            }
        });
    }], (err, asyncResponse) => {

        if (err) {
            callback(err, null);
            return;
        }


        let bodyHashing = matchId;
        const signature = Hashing.makeSignature(SECRET_KEY, bodyHashing);

        let txns = _.map(betResponse, (item) => {
            return {
                "playerId": item.memberId.username_lower,
                "betId": item.betId,
                // "betAmount": item.amount,
                "betTime": moment(item.createdDate).format('YYYY-MM-DD HH:mm:ss')
            }
        });

        let body = {
            "matchId": matchId,
            "txns": txns,
            "remark": remark,
            "signature": signature
        };

        const headers = {
            'Content-Type': 'application/json'
        };

        const option = {
            url: `${endpoint}/callback/cancel-match`,
            method: 'POST',
            headers: headers,
            form: body
        };

        request(option, (err, response, body) => {
            if (err) {
                callback(err, null);
            } else {

                let responseBody = JSON.parse(response.body);

                try {
                    if (responseBody.code == 200) {
                        callback(null, responseBody.result);
                    } else {
                        callback(responseBody, null);
                    }
                } catch (e) {
                    callback(e, null);
                }
            }
        });
    });
}


function cancelBet(endpoint, remark, betResponse, callback) {


    console.log("============  cancelBet : ", betResponse);

    function cancelBetFunction(body,remark, result, cancelBetCallback) {
        let sendingBody = {
            username: body.memberId.username_lower,
            betId: body.betId,
            betTime: moment(body.createdDate).format('YYYY-MM-DD HH:mm:ss'),
            remark: remark,
            result: JSON.stringify(result)
        };

        let model = ApiCancelBetModel(sendingBody);

        model.save((err, response) => {

            if (err) {
                cancelBetCallback(err, null);
            } else {
                cancelBetCallback(null, response);
            }
        });
    }


    let bodyHashing = betResponse.memberId.username_lower + betResponse.betId;
    const signature = Hashing.makeSignature(SECRET_KEY, bodyHashing);

    let body = {
        playerId: betResponse.memberId.username_lower,
        betId: betResponse.betId,
        betTime: moment(betResponse.createdDate).format('YYYY-MM-DD HH:mm:ss'),
        remark: remark,
        signature: signature
    };


    const headers = {
        'Content-Type': 'application/json'
    };

    const option = {
        url: `${endpoint}/callback/cancel-bet?cert=${CERT}`,
        method: 'POST',
        headers: headers,
        form: body
    };

    console.log(option)
    request(option, (err, response, body) => {
        if (err) {
            callback(err, null);
        } else {

            let responseBody = JSON.parse(response.body);

            try {
                cancelBetFunction(betResponse, remark,responseBody, (err, response) => {

                    if (responseBody.code == 200) {
                        callback(null, responseBody.result);
                    } else {
                        callback(responseBody, null);
                    }
                });

            } catch (e) {
                cancelBetFunction(betResponse, remark,e, (err, response) => {
                    callback(e, null);
                });
            }
        }

    });
}

function generateSignature(parameters, callback) {
    console.log('parameters ==========> ', parameters);

    let key = new Buffer(SLOT_XO_SECRET_KEY, 'utf8');
    let hmac = Crypto.createHmac("sha1", key);
    let hash2 = hmac.update(parameters, 'utf8');
    let digest = hash2.digest("base64");

    callback(null, digest)
}


module.exports = {
    getBalance,
    placeBet,
    settle,
    cancelBet,
    cancelMatch
}