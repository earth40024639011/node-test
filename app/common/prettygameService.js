/**
 * Created by boonsongrit on 9/18/17.
 */

const moment = require('moment');
const dateFormat = require('dateformat');
const querystring = require('querystring');
const crypto = require('crypto');
const request = require('request');
const parseString = require('xml2js').parseString;
const _ = require('underscore');

const getClientKey = require('../controllers/getClientKey');
// const {
//   PRETTY_API_URL: API_URL = 'https://staging-pt-gaming-api.secure-restapi.com/apirouting.aspx',
//   PRETTY_SECRET_KEY: SECRET_KEY = 'CF0E08BEA3CD23210B4828D3CCFB405C',
//   PRETTY_AGENT: AGENT = 'testapi1'
// } = getClientKey.getKey();

const {
  PRETTY_API_URL: API_URL = 'https://api-dev.prettygaming.asia/apiRoute/member/loginRequest',
  PRETTY_SECRET_KEY: SECRET_KEY = '7c443b7e-b34b-b3e2-d054-da49ce47c193',
  PRETTY_AGENT: AGENT = 'sportbook88'
} = getClientKey.getKey();


function getBetDetail(gameType, betType) {

    console.log('gameType : ', gameType);
    console.log('betType : ', betType);
    if (gameType.toUpperCase() == 'BACCARAT') {
        switch (betType) {
            case "0" :
                return "Small";
            case "1" :
                return "Banker";
            case "2" :
                return "Player";
            case "3" :
                return "Tie";
            case "4" :
                return "BankerPair";
            case "5" :
                return "PlayerPair";
            case "6" :
                return "PlayerNatural";
            case "7" :
                return "BankerNatural";
            case "8" :
                return "Super6";
            case "9" :
                return "BankerCow";
            case "10" :
                return "PlayerCow";
            case "11" :
                return "TieCow";
            default :
                return betType
        }
    }
    else if (gameType.toUpperCase() == 'DRAGON') {
        switch (betType) {
            case "1" :
                return "Dragon";
            case "2" :
                return "Tiger";
            case "3" :
                return "Tie";
            default :
                return betType
        }
    }
    else if (gameType.toUpperCase() == 'SICBO') {
        switch (betType) {
            case "0" :
                return "Small";
            case "1" :
                return "Big";
            case "2" :
                return "Odd";
            case "3" :
                return "Even";
            case "4" :
                return "Points4";
            case "5" :
                return "Points5";
            case "6" :
                return "Points6";
            case "7" :
                return "Points7";
            case "8" :
                return "Points8";
            case "9" :
                return "Points9";
            case "10" :
                return "Points10";
            case "11" :
                return "Points11";
            case "12" :
                return "Points12";
            case "13" :
                return "Points13";
            case "14" :
                return "Points14";
            case "15" :
                return "Points15";
            case "16" :
                return "Points16";
            case "17" :
                return "Points17";
            case "18" :
                return "All same";
            case "19" :
                return "Three Odd";
            case "20" :
                return "Three Even";
            case "21" :
                return "Two Odd One Even";
            case "22" :
                return "Two Even One Odd";
            case "1|||" :
                return "Triple army 1";
            case "2|||" :
                return "Triple army 2";
            case "3|||" :
                return "Triple army 3";
            case "4|||" :
                return "Triple army 4";
            case "5|||" :
                return "Triple army 5";
            case "6|||" :
                return "Triple army 6";
            case "1|1||" :
                return "Short 1";
            case "2|2||" :
                return "Short 2";
            case "3|3||" :
                return "Short 3";
            case "4|4||" :
                return "Short 4";
            case "5|5||" :
                return "Short 5";
            case "6|6||" :
                return "Short 6";
            case "1|1|1|" :
                return "Triple 1";
            case "2|2|2|" :
                return "Triple 2";
            case "3|3|3|" :
                return "Triple 3";
            case "4|4|4|" :
                return "Triple 4";
            case "5|5|5|" :
                return "Triple 5";
            case "6|6|6|" :
                return "Triple 6";
            case "1|2||" :
                return "Long 12";
            case "1|3||" :
                return "Long 13";
            case "1|4||" :
                return "Long 14";
            case "1|5||" :
                return "Long 15";
            case "1|6||" :
                return "Long 16";
            case "2|3||" :
                return "Long 23";
            case "2|4||" :
                return "Long 24";
            case "2|5||" :
                return "Long 25";
            case "2|6||" :
                return "Long 26";
            case "3|4||" :
                return "Long 34";
            case "3|5||" :
                return "Long 35";
            case "3|6||" :
                return "Long 36";
            case "4|5||" :
                return "Long 45";
            case "4|6||" :
                return "Long 46";
            case "5|6||" :
                return "Long 56";
            case "1|1|2|" :
                return "1 2 3";
            case "1|1|3|" :
                return "1 2 3";
            case "5|5|6|" :
                return "5 5 6";
            case "5|6|6|" :
                return "5 6 6";
            case "1|2|3|4" :
                return "1 2 3 4";
            case "2|3|4|5" :
                return "2 3 4 5";
            case "2|3|5|6" :
                return "2 3 5 6";
            case "3|4|5|6" :
                return "3 4 5 6";
            default :
                return betType
        }
    }
    else if (gameType.toUpperCase() == 'FANTAN') {
        switch (betType) {
            case "1" :
                return "Odd";
            case "2" :
                return "Evenn";
            case "3" :
                return "1 Fan";
            case "4" :
                return "2 Fan";
            case "5" :
                return "3 Fan";
            case "6" :
                return "4 Fan";
            case "7" :
                return "Kwok 12";
            case "8" :
                return "Kwok 23";
            case "9" :
                return "Kwok 34";
            case "10" :
                return "Kwok 41";
            case "11" :
                return "1 Nim 2";
            case "12" :
                return "1 Nim 3";
            case "13" :
                return "1 Nim 4";
            case "14" :
                return "2 Nim 1";
            case "15" :
                return "2 Nim 3";
            case "16" :
                return "2 Nim 4";
            case "17" :
                return "3 Nim 1";
            case "18" :
                return "3 Nim 2";
            case "19" :
                return "3 Nim 4";
            case "20" :
                return "4 Nim 1";
            case "21" :
                return "4 Nim 2";
            case "22" :
                return "4 Nim 3";
            case "23" :
                return "1 Tong 23";
            case "24" :
                return "1 Tong 24";
            case "25" :
                return "1 Tong 34";
            case "26" :
                return "2 Tong 13";
            case "27" :
                return "2 Tong 14";
            case "28" :
                return "2 Tong 34";
            case "29" :
                return "3 Tong 12";
            case "30" :
                return "3 Tong 14";
            case "31" :
                return "3 Tong 24";
            case "32" :
                return "4 Tong 12";
            case "33" :
                return "4 Tong 13";
            case "34" :
                return "4 Tong 23";
            case "35" :
                return "Chun 234";
            case "36" :
                return "Chun 134";
            case "37" :
                return "Chun 124";
            case "38" :
                return "Chun 123";
            case "39" :
                return "1 Zheng";
            case "40" :
                return "2 Zheng";
            case "41" :
                return "3 Zheng";
            case "42" :
                return "4 Zheng";
            default :
                return betType
        }
    }
    else if (gameType.toUpperCase() == 'ROULETTE') {
        console.log(betType, '121112');
        switch (betType) {
            case "1" :
                return "1";
            case "2" :
                return "2";
            case "3" :
                return "3";
            case "4" :
                return "4";
            case "5" :
                return "5";
            case "6" :
                return "6";
            case "7" :
                return "7";
            case "8" :
                return "8";
            case "9" :
                return "9";
            case "10" :
                return "10";
            case "11" :
                return "11";
            case "12" :
                return "12";
            case "13" :
                return "13";
            case "14" :
                return "14";
            case "15" :
                return "15";
            case "16" :
                return "16";
            case "17" :
                return "17";
            case "18" :
                return "18";
            case "19" :
                return "19";
            case "20" :
                return "20";
            case "21" :
                return "21";
            case "22" :
                return "22";
            case "23" :
                return "23";
            case "24" :
                return "24";
            case "25" :
                return "25";
            case "26" :
                return "26";
            case "27" :
                return "27";
            case "28" :
                return "28";
            case "29" :
                return "29";
            case "30" :
                return "30";
            case "31" :
                return "31";
            case "32" :
                return "32";
            case "33" :
                return "33";
            case "34" :
                return "34";
            case "35" :
                return "35";
            case "36" :
                return "36";
            case "37" :
                return "Odd";
            case "38" :
                return "Even";
            case "39" :
                return "Red";
            case "40" :
                return "Black";
            case "41" :
                return "Small(1-18)";
            case "42" :
                return "Big(19-36)";
            case "43" :
                return "1st 12(13-24);";
            case "44" :
                return "2nd 12(13-24);";
            case "45" :
                return "3rd 12(25-36)";
            case "46" :
                return "1st Row";
            case "47" :
                return "2nd Row";
            case "48" :
                return "3rd Row";
            default :
                return betType
        }
    }
    else {
        return betType;
    }

}

function getKeyBetDetail(gameType, betType) {
    let list = [];
    if (gameType.toUpperCase() == 'BACCARAT') {
        getBetDetail(gameType, betType)
    }
    else if (gameType.toUpperCase() == 'DRAGON') {
        getBetDetail(gameType, betType)
    }
    else if (gameType.toUpperCase() == 'SICBO') {
        getBetDetail(gameType, betType)
    }
    else if (gameType.toUpperCase() == 'FANTAN') {
        getBetDetail(gameType, betType)
    }
    else if (gameType.toUpperCase() == 'ROULETTE') {
        return list = _.map(betType.split('|'), (k) => {
            return getBetDetail(gameType, k)
        }).join(',');
    }
    return list
}

function getGameCardResultSuit(type) {

    switch (type) {
        case "0" :
            return "H";
        case "1" :
            return "D";
        case "2" :
            return "C";
        case "3" :
            return "S";
        case "F" :
            return "";
        default :
            return type
    }

}
function getGameCardResultRank(type) {

    switch (type) {
        case "1" :
            return "A";
        case "2" :
            return "2";
        case "3" :
            return "3";
        case "4" :
            return "4";
        case "5" :
            return "5";
        case "6" :
            return "6";
        case "7" :
            return "7";
        case "8" :
            return "8";
        case "9" :
            return "9";
        case "A" :
            return "10";
        case "B" :
            return "J";
        case "C" :
            return "Q";
        case "D" :
            return "K";
        case "F" :
            return "";
        default :
            return type
    }

}
function getKeyBetResult(gameType, betType) {

    if (gameType.toUpperCase() == 'BACCARAT') {

        if (betType) {
            let playerCard1 = betType.substring(0, 2);
            let playerCard2 = betType.substring(2, 4);
            let playerCard3 = betType.substring(4, 6);
            let bankerCard1 = betType.substring(6, 8);
            let bankerCard2 = betType.substring(8, 10);
            let bankerCard3 = betType.substring(10, 12);

            let player1Sult = getGameCardResultSuit(playerCard1.substring(0, 1));
            let player1Rank = getGameCardResultRank(playerCard1.substring(1, 2));

            let player2Sult = getGameCardResultSuit(playerCard2.substring(0, 1));
            let player2Rank = getGameCardResultRank(playerCard2.substring(1, 2));

            let player3Sult = getGameCardResultSuit(playerCard3.substring(0, 1));
            let player3Rank = getGameCardResultRank(playerCard3.substring(1, 2));

            let banker1Sult = getGameCardResultSuit(bankerCard1.substring(0, 1));
            let banker1Rank = getGameCardResultRank(bankerCard1.substring(1, 2));

            let banker2Sult = getGameCardResultSuit(bankerCard2.substring(0, 1));
            let banker2Rank = getGameCardResultRank(bankerCard2.substring(1, 2));

            let banker3Sult = getGameCardResultSuit(bankerCard3.substring(0, 1));
            let banker3Rank = getGameCardResultRank(bankerCard3.substring(1, 2));

            // console.log(`player1Sult:${player1Sult},player1Rank:${player1Rank}/player2Sult:${player2Sult},player2Rank:${player2Rank}/player3Sult:${player3Sult},player3Rank:${player3Rank}/banker1Sult:${banker1Sult},banker1Rank:${banker1Rank}/banker2Sult:${banker2Sult},banker2Rank:${banker2Rank}/banker3Sult:${banker3Sult},banker3Rank:${banker3Rank}`


            // console.log(`${player1Sult},${player1Rank}/${player2Sult},${player2Rank}/${player3Sult},${player3Rank}/${banker1Sult},${banker1Rank}/${banker2Sult},${banker2Rank}/${banker3Sult},${banker3Rank}`,'1211')
            // return `player1Sult:${player1Sult},player1Rank:${player1Rank}/player2Sult:${player2Sult},player2Rank:${player2Rank}/player3Sult:${player3Sult},player3Rank:${player3Rank}/banker1Sult:${banker1Sult},banker1Rank:${banker1Rank}/banker2Sult:${banker2Sult},banker2Rank:${banker2Rank}/banker3Sult:${banker3Sult},banker3Rank:${banker3Rank}`
            return  `Player:(${player1Rank}${player1Sult}),(${player2Rank}${player2Sult}),(${player3Rank}${player3Sult}) Banker:(${banker1Rank}${banker1Sult}),(${banker2Rank}${banker2Sult}),(${banker3Rank}${banker3Sult})`


        }else{
            return  `Player:(-) Banker:(-)`


        }
    }
    else if(gameType.toUpperCase() == 'DRAGON') {
        if(betType) {
            let tigerCard  = betType.substring(2, 4);
            let dragonCard1 = betType.substring(6, 8);


            let dragonSult = getGameCardResultSuit(dragonCard1.substring(0, 1));
            let dragonRank = getGameCardResultRank(dragonCard1.substring(1, 2));

            let tigerSult = getGameCardResultSuit(tigerCard.substring(0, 1));
            let tigerRank = getGameCardResultRank(tigerCard.substring(1, 2));


            return `dragonCard:(${dragonRank},${dragonSult})  tigerCard:(${tigerRank},${tigerSult})`


        }else {

            return `dragonCard:(-)  tigerCard:(-)`
        }

    }
}

function getBetResult(username, betID, fromTime, callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    let method = "GetUserBetItemDV";
    let time = moment().utc(true).format('YYYYMMDDHHmmss');
    let formDate = moment(fromTime).add(-1, 'day').utc(true).format('YYYY-MM-DD HH:mm:ss');
    let toDate = moment().utc(true).format('YYYY-MM-DD HH:mm:ss');


    let queryString = querystring.stringify({

        method: method,
        Key: SECRET_KEY,
        Time: time,
        Username: username,
        FromTime: formDate,
        ToTime: toDate,
        Offset: '0'
    });
    // console.log(queryString,'<--------')   ;

    let desEncrypt = encodeDesCBC(queryString);
    let queryStringEncode = encodeURIComponent(desEncrypt);
    let signature = buildSignature(queryString, time);


    let options = {
        url: API_URL + '?q=' + queryStringEncode + "&s=" + signature,
        method: 'POST',
        headers: headers
    };

    console.log(options);

    request(options, (error, response, body) => {
        if (error) {
            callback('', null);
        } else {
            parseString(body, function (err, result) {
                console.log(result, 'yyy');

                if (result === null) {
                    console.log('errrrrrrr')

                } else if (result.GetUserBetItemResponse.ErrorMsgId[0] === '111') {

                    let list = {
                        massage: 'เกินเวลา 7วัน ในการดู',
                        code: 111,
                    }
                    callback(null, list)
                    co
                }

                else if (result.GetUserBetItemResponse.ErrorMsgId[0] === '0') {

                    let responseMsg = result.GetUserBetItemResponse;
                    console.log(responseMsg, 'popopopopopo');


                    let list = responseMsg.UserBetItemList[0].UserBetItem;

                    list = _.map(list, (item) => {

                        if (item.GameType[0] === 'bac') {

                            try {
                                item.BetType = getBetTypeDetailBA(item.BetType[0])
                                // getGameCardResultSuit
                                item.GameType = getGameTypeDetail(item.GameType[0])
                                item.GameResult[0].BaccaratResult[0].BankerCard1[0].Suit = getGameCardResultSuit(item.GameResult[0].BaccaratResult[0].BankerCard1[0].Suit[0]);
                                item.GameResult[0].BaccaratResult[0].BankerCard1[0].Rank = getGameCardResultRank(item.GameResult[0].BaccaratResult[0].BankerCard1[0].Rank[0]);
                                //
                                item.GameResult[0].BaccaratResult[0].BankerCard2[0].Suit = getGameCardResultSuit(item.GameResult[0].BaccaratResult[0].BankerCard2[0].Suit[0]);
                                item.GameResult[0].BaccaratResult[0].BankerCard2[0].Rank = getGameCardResultRank(item.GameResult[0].BaccaratResult[0].BankerCard2[0].Rank[0]);
                                //
                                item.GameResult[0].BaccaratResult[0].BankerCard3[0].Suit = getGameCardResultSuit(item.GameResult[0].BaccaratResult[0].BankerCard3[0].Suit[0])
                                item.GameResult[0].BaccaratResult[0].BankerCard3[0].Rank = getGameCardResultRank(item.GameResult[0].BaccaratResult[0].BankerCard3[0].Rank[0])


                                item.GameResult[0].BaccaratResult[0].PlayerCard1[0].Suit = getGameCardResultSuit(item.GameResult[0].BaccaratResult[0].PlayerCard1[0].Suit[0]);
                                item.GameResult[0].BaccaratResult[0].PlayerCard1[0].Rank = getGameCardResultRank(item.GameResult[0].BaccaratResult[0].PlayerCard1[0].Rank[0]);


                                item.GameResult[0].BaccaratResult[0].PlayerCard2[0].Suit = getGameCardResultSuit(item.GameResult[0].BaccaratResult[0].PlayerCard2[0].Suit[0])
                                item.GameResult[0].BaccaratResult[0].PlayerCard2[0].Rank = getGameCardResultRank(item.GameResult[0].BaccaratResult[0].PlayerCard2[0].Rank[0])


                                item.GameResult[0].BaccaratResult[0].PlayerCard3[0].Suit = getGameCardResultSuit(item.GameResult[0].BaccaratResult[0].PlayerCard3[0].Suit[0])
                                item.GameResult[0].BaccaratResult[0].PlayerCard3[0].Rank = getGameCardResultRank(item.GameResult[0].BaccaratResult[0].PlayerCard3[0].Rank[0])

                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRBankerNatural = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRBankerNatural[0], 'BRBankerNatural')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRBankerPair = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRBankerPair[0], 'BRBankerPair')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRBankerWin = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRBankerWin[0], 'BRBankerWin')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowBankerNatural = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowBankerNatural[0], 'BRCowBankerNatural')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowBankerPair = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowBankerPair[0], 'BRCowBankerPair')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowBankerWin = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowBankerWin[0], 'BRCowBankerWin')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowPlayerNatural = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowPlayerNatural[0], 'BRCowPlayerNatural')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowPlayerPair = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowPlayerPair[0], 'BRCowPlayerPair')

                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowPlayerWin = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowPlayerWin[0], 'BRCowPlayerWin')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowTie = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowTie[0], 'BRCowTie')

                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerNatural = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerNatural[0], 'BRPlayerNatural')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerPair = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerPair[0], 'BRPlayerPair')

                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerWin = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerWin[0], 'BRPlayerWin')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSBankerNatural = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSBankerNatural[0], 'BRSSBankerNatural')

                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSBankerPair = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSBankerPair[0], 'BRSSBankerPair')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSBankerWin = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSBankerWin[0], 'BRSSBankerWin')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSPlayerNatural = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSPlayerNatural[0], 'BRSSPlayerNatural')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSPlayerPair = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSPlayerPair[0], 'BRSSPlayerPair')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSPlayerWin = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSPlayerWin[0], 'BRSSPlayerWin')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSSuperSix = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSSuperSix[0], 'BRSSSuperSix')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSTie = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSTie[0], 'BRSSTie')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRTie = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRTie[0], 'BRTie')


                            }
                            catch (err) {
                                item.BetType = getBetTypeDetailBA(item.BetType[0])
                                // getGameCardResultSuit
                                item.GameType = getGameTypeDetail(item.GameType[0])
                                item.GameResult[0].BaccaratResult[0].BankerCard1[0].Suit = getGameCardResultSuit(item.GameResult[0].BaccaratResult[0].BankerCard1[0].Suit[0]);
                                item.GameResult[0].BaccaratResult[0].BankerCard1[0].Rank = getGameCardResultRank(item.GameResult[0].BaccaratResult[0].BankerCard1[0].Rank[0]);
                                //
                                item.GameResult[0].BaccaratResult[0].BankerCard2[0].Suit = getGameCardResultSuit(item.GameResult[0].BaccaratResult[0].BankerCard2[0].Suit[0]);
                                item.GameResult[0].BaccaratResult[0].BankerCard2[0].Rank = getGameCardResultRank(item.GameResult[0].BaccaratResult[0].BankerCard2[0].Rank[0]);
                                //
                                // item.GameResult[0].BaccaratResult[0].BankerCard3[0].Suit = getGameCardResultSuit(item.GameResult[0].BaccaratResult[0].BankerCard3[0].Suit[0])
                                // item.GameResult[0].BaccaratResult[0].BankerCard3[0].Rank = getGameCardResultRank(item.GameResult[0].BaccaratResult[0].BankerCard3[0].Rank[0])


                                item.GameResult[0].BaccaratResult[0].PlayerCard1[0].Suit = getGameCardResultSuit(item.GameResult[0].BaccaratResult[0].PlayerCard1[0].Suit[0]);
                                item.GameResult[0].BaccaratResult[0].PlayerCard1[0].Rank = getGameCardResultRank(item.GameResult[0].BaccaratResult[0].PlayerCard1[0].Rank[0]);


                                item.GameResult[0].BaccaratResult[0].PlayerCard2[0].Suit = getGameCardResultSuit(item.GameResult[0].BaccaratResult[0].PlayerCard2[0].Suit[0])
                                item.GameResult[0].BaccaratResult[0].PlayerCard2[0].Rank = getGameCardResultRank(item.GameResult[0].BaccaratResult[0].PlayerCard2[0].Rank[0])


                                // item.GameResult[0].BaccaratResult[0].PlayerCard3[0].Suit = getGameCardResultSuit(item.GameResult[0].BaccaratResult[0].PlayerCard3[0].Suit[0])
                                // item.GameResult[0].BaccaratResult[0].PlayerCard3[0].Rank = getGameCardResultRank(item.GameResult[0].BaccaratResult[0].PlayerCard3[0].Rank[0])

                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRBankerNatural = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRBankerNatural[0], 'BRBankerNatural')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRBankerPair = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRBankerPair[0], 'BRBankerPair')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRBankerWin = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRBankerWin[0], 'BRBankerWin')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowBankerNatural = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowBankerNatural[0], 'BRCowBankerNatural')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowBankerPair = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowBankerPair[0], 'BRCowBankerPair')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowBankerWin = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowBankerWin[0], 'BRCowBankerWin')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowPlayerNatural = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowPlayerNatural[0], 'BRCowPlayerNatural')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowPlayerPair = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowPlayerPair[0], 'BRCowPlayerPair')

                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowPlayerWin = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowPlayerWin[0], 'BRCowPlayerWin')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowTie = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowTie[0], 'BRCowTie')

                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerNatural = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerNatural[0], 'BRPlayerNatural')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerPair = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerPair[0], 'BRPlayerPair')

                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerWin = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerWin[0], 'BRPlayerWin')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSBankerNatural = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSBankerNatural[0], 'BRSSBankerNatural')

                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSBankerPair = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSBankerPair[0], 'BRSSBankerPair')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSBankerWin = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSBankerWin[0], 'BRSSBankerWin')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSPlayerNatural = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSPlayerNatural[0], 'BRSSPlayerNatural')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSPlayerPair = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSPlayerPair[0], 'BRSSPlayerPair')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSPlayerWin = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSPlayerWin[0], 'BRSSPlayerWin')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSSuperSix = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSSuperSix[0], 'BRSSSuperSix')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSTie = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSTie[0], 'BRSSTie')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRTie = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRTie[0], 'BRTie')


                            }
                            // BRPlayerWin:item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerWin[0],
                        } else if (item.GameType[0] === 'dtx') {
                            item.BetType = getBetTypeDetailDG(item.BetType[0])
                        }
                        else if (item.GameType[0] === 'sicbo') {
                            item.BetType = getBetTypeDetailSB(item.BetType[0])
                        }
                        return {
                            betType: item.BetType,
                            gameType: item.GameType,
                            betAmount: item.BetAmount[0],
                            gameID: item.GameID[0],
                            Balance: item.Balance[0],
                            Rolling: item.Rolling[0],
                            ResultAmount: item.ResultAmount[0],
                            gamedetail: item.GameResult[0],
                            GameResult: {
                                ResultDetail: {
                                    BRBankerNatural: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRBankerNatural,
                                    BRBankerPair: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRBankerPair,
                                    BRBankerWin: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRBankerWin,
                                    BRCowBankerNatural: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowBankerNatural,
                                    BRCowBankerPair: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowBankerPair,
                                    BRCowBankerWin: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowBankerWin,
                                    BRCowPlayerNatural: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowPlayerNatural,
                                    BRCowPlayerPair: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowPlayerPair,
                                    BRCowPlayerWin: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowPlayerWin,
                                    BRCowTie: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowTie,
                                    BRPlayerNatural: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerNatural,
                                    BRPlayerPair: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerPair,
                                    BRPlayerWin: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerWin,
                                    BRSSBankerNatural: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSBankerNatural,
                                    BRSSBankerPair: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSBankerPair,
                                    BRSSBankerWin: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSBankerWin,
                                    BRSSPlayerNatural: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSPlayerNatural,
                                    BRSSPlayerPair: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSPlayerPair,
                                    BRSSPlayerWin: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSPlayerWin,
                                    BRSSSuperSix: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSSuperSix,
                                    BRSSTie: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSTie,
                                    BRTie: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRTie
                                },
                                BankerCard1: {
                                    Rank: item.GameResult[0].BaccaratResult[0].BankerCard1[0].Rank,
                                    Suit: item.GameResult[0].BaccaratResult[0].BankerCard1[0].Suit
                                },
                                BankerCard2: {
                                    Rank: item.GameResult[0].BaccaratResult[0].BankerCard2[0].Rank,
                                    Suit: item.GameResult[0].BaccaratResult[0].BankerCard2[0].Suit
                                },
                                BankerCard3: {
                                    Rank: item.GameResult[0].BaccaratResult[0].BankerCard3[0].Rank,
                                    Suit: item.GameResult[0].BaccaratResult[0].BankerCard3[0].Suit
                                },
                                PlayerCard1: {
                                    Rank: item.GameResult[0].BaccaratResult[0].PlayerCard1[0].Rank,
                                    Suit: item.GameResult[0].BaccaratResult[0].PlayerCard1[0].Suit
                                },
                                PlayerCard2: {
                                    Rank: item.GameResult[0].BaccaratResult[0].PlayerCard2[0].Rank,
                                    Suit: item.GameResult[0].BaccaratResult[0].PlayerCard2[0].Suit
                                },
                                PlayerCard3: {
                                    Rank: item.GameResult[0].BaccaratResult[0].PlayerCard3[0].Rank,
                                    Suit: item.GameResult[0].BaccaratResult[0].PlayerCard3[0].Suit
                                }
                            }


                        };
                    });
                    console.log(list, '=======<');
                    filterBetID(list, betID, (_responseCallback) => {
                        let jsonResponse = {
                            betDetail: _responseCallback
                        };

                        callback(null, jsonResponse)
                        // callback(null, body)
                    })


                    // callback(null,list)
                    // console.log(list,'poippipip');


                }

            });
        }
    });
}

function filterBetID(obj, betID, callback) {
    let list = _.filter(obj, (l) => {
        return betID === l.gameID
    });

    callback(list)
}


function buildSignature(qs) {
    console.log('---- buildSignature ---- ');
    return crypto.createHash('md5').update(qs).digest("hex");
}

function callLoginRequest(username, limit, callback) {

    // username = AGENT + '_' + username;
    let headers = {
        'Content-Type': 'application/json'
    };

    // let queryString = querystring.stringify({
    //     agent: AGENT,
    //     key: SECRET_KEY,
    //     username: username.toLowerCase(),
    //     password: '123456'
    // });

    let body = {
        "agentUsername": AGENT,
        "agentApiKey": SECRET_KEY,
        "playerUsername": username,
        "playerIp": "127.0.0.1",
        "betLimit": [1001, 1002, 1003, 1004]
    }

    // let desEncrypt = encodeDesCBC(queryString);
    // let signature = buildSignature(queryString);

    // let options = {
    //     url: API_URL,
    //     method: 'POST',
    //     headers: headers,
    //     form: body
    // };

    request.post({
        url: API_URL,
        body: body,
        timeout:10000,
        json:true
    }, function(error, response, body){
        // let responseBody = JSON.parse(body);
        console.log('response ==== : ', body)
        callback(null, body);
    });

    // console.log('--------------------', options);
    //
    // request(options, (error, response, body) => {
    //     if (error) {
    //         callback(error, null);
    //     } else {
    //         console.log('body : ',body)
    //         let responseBody = JSON.parse(body);
    //         console.log('response ==== : ', responseBody)
    //         if (responseBody.Success || responseBody.ErrorCode == 10008) {
    //             callback(null, responseBody);
    //         } else {
    //             callback(responseBody, null);
    //         }
    //     }
    // });



    // let options = {
    //     url: API_URL + '?service=Agent.CreateAccount&' + queryString + "&sign=" + signature,
    //     method: 'POST',
    //     headers: headers
    // };
    //
    // callSetBetLimit(username.toLowerCase(), limit, (err, response) => {
    //     console.log(response)
    // });
    //
    // console.log(options)
    // request(options, (error, response, body) => {
    //     if (error) {
    //         console.log(error)
    //         callback('', null);
    //     } else {
    //         console.log('body : ',body)
    //         let responseBody = JSON.parse(body);
    //         console.log('limit : ', responseBody)
    //         if (responseBody.Success || responseBody.ErrorCode == 10008) {
    //             callback(null, responseBody);
    //         } else {
    //             callback(responseBody, null);
    //         }
    //
    //     }
    // });

}


function callEnterGameInterfaceRequest(username, client, ipAddress, callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };
    username = AGENT + '_' + username;

    let queryString = querystring.stringify({
        agent: AGENT,
        key: SECRET_KEY,
        username: username.toLowerCase(),
        password: '123456',
        gametype: '0',
        loginIP: ipAddress,
        busway: client == 'MB' ? '01' : '00'
    });

    // let desEncrypt = encodeDesCBC(queryString);
    let signature = buildSignature(queryString);


    let options = {
        url: API_URL + '?service=Agent.Forward&' + queryString + "&sign=" + signature,
        method: 'POST',
        headers: headers
    };

    console.log(options)
    request(options, (error, response, body) => {
        if (error) {
            callback('', null);
        } else {
            let responseBody = JSON.parse(body);
            console.log(responseBody)
            if (responseBody.Success) {
                callback(null, responseBody);
            } else {
                callback(responseBody, null);
            }


        }
    });

}


function callEnterGameUrlRequest(token, gameType, url, callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    let queryString = querystring.stringify({
        Token: token,
        GameType: '0',
        url: 'www.google.com'
    });

    // let desEncrypt = encodeDesCBC(queryString);
    let signature = buildSignature(queryString);


    let options = {
        url: url + '?' + queryString,
        method: 'POST',
        headers: headers
    };

    console.log(options)
    request(options, (error, response, body) => {
        if (error) {
            callback('', null);
        } else {
            console.log(body)
            // let responseBody = JSON.parse(body);
            // console.log('xxx ',responseBody)
            // if(responseBody.Success){
            //     callback(null, responseBody);
            // }else {
            //     callback(responseBody, null);
            // }


        }
    });

}


function callRegUserInfo(username, callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    let method = "RegUserInfo";
    let time = moment().utc(true).format('YYYYMMDDHHmmss');


    let queryString = querystring.stringify({
        method: method,
        Key: SECRET_KEY,
        Time: time,
        Username: username.toLowerCase(),
        CurrencyType: 'THB'
    });

    let desEncrypt = encodeDesCBC(queryString);
    let queryStringEncode = encodeURIComponent(desEncrypt);
    let signature = buildSignature(queryString, time);

    let options = {
        url: API_URL + '?q=' + queryStringEncode + "&s=" + signature,
        method: 'POST',
        headers: headers
    };

    request(options, (error, response, body) => {
        if (error) {
            callback('', null);
        } else {
            callback(null, body)
        }
    });
}

function callVerifyUsername(username, callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    let method = "VerifyUsername";
    let time = moment().utc(true).format('YYYYMMDDHHmmss');


    let queryString = querystring.stringify({
        method: method,
        Key: SECRET_KEY,
        Time: time,
        Username: username.toLowerCase()
    });

    let desEncrypt = encodeDesCBC(queryString);
    let queryStringEncode = encodeURIComponent(desEncrypt);
    let signature = buildSignature(queryString, time);


    let options = {
        url: API_URL + '?q=' + queryStringEncode + "&s=" + signature,
        method: 'POST',
        headers: headers
    };

    console.log(options);

    request(options, (error, response, body) => {
        // console.log('ressssssss')
        // console.log(response,body,error)
        if (error) {
            callback('', null);
        } else {
            console.log(body)
            // let json = JSON.parse(body);

            // console.log(body)
            callback(null, body)
        }
    });
}

function callGetUserStatusDV(username, callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    let method = "GetUserStatusDV";
    let time = moment().utc(true).format('YYYYMMDDHHmmss');


    let queryString = querystring.stringify({
        method: method,
        Key: SECRET_KEY,
        Time: time,
        Username: username.toLowerCase()
    });

    let desEncrypt = encodeDesCBC(queryString);
    let queryStringEncode = encodeURIComponent(desEncrypt);
    let signature = buildSignature(queryString, time);


    let options = {
        url: API_URL + '?q=' + queryStringEncode + "&s=" + signature,
        method: 'POST',
        headers: headers
    };

    console.log(options);

    request(options, (error, response, body) => {
        // console.log('ressssssss')
        // console.log(response,body,error)
        if (error) {
            callback('', null);
        } else {
            console.log(body)
            // let json = JSON.parse(body);

            // console.log(body)
            callback(null, body)
        }
    });
}

function callQueryBetLimit(callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    let method = "QueryBetLimit";
    let time = moment().utc(true).format('YYYYMMDDHHmmss');


    let queryString = querystring.stringify({
        method: method,
        Key: SECRET_KEY,
        Time: time,
        Currency: 'THB'
    });

    let desEncrypt = encodeDesCBC(queryString);
    let queryStringEncode = encodeURIComponent(desEncrypt);
    let signature = buildSignature(queryString, time);


    let options = {
        url: API_URL + '?q=' + queryStringEncode + "&s=" + signature,
        method: 'POST',
        headers: headers
    };

    console.log(options);

    request(options, (error, response, body) => {
        if (error) {
            callback('', null);
        } else {

            parseString(body, function (err, result) {

                console.log(result)
                let responseMsg = result.QueryBetLimitResponse;

                let jsonResponse = {
                    errMsgId: responseMsg.ErrorMsg[0],
                    errMsg: responseMsg.ErrorMsgId[0],
                    betLimitList: responseMsg.BetLimitList[0]
                }
                callback(null, jsonResponse)
            });
            // callback(null, body)
        }
    });
}

function callGetUserMaxBalance(username, callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    let method = "GetUserMaxBalance";
    let time = moment().utc(true).format('YYYYMMDDHHmmss');


    let queryString = querystring.stringify({
        method: method,
        Key: SECRET_KEY,
        Time: time,
        Username: username.toLowerCase()
    });

    let desEncrypt = encodeDesCBC(queryString);
    let queryStringEncode = encodeURIComponent(desEncrypt);
    let signature = buildSignature(queryString, time);


    let options = {
        url: API_URL + '?q=' + queryStringEncode + "&s=" + signature,
        method: 'POST',
        headers: headers
    };

    console.log(options);

    request(options, (error, response, body) => {
        if (error) {
            callback('', null);
        } else {

            parseString(body, function (err, result) {

                let responseMsg = result.GetUserMaxWinResponse;

                let jsonResponse = {
                    errMsgId: responseMsg.ErrorMsgId[0],
                    // errMsg : responseMsg.ErrorMsg[0],
                    maxBalance: responseMsg.MaxBalance[0]
                }
                callback(null, jsonResponse)
            });
            // callback(null, body)
        }
    });
}

function callSetUserMaxBalance(username, callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    let method = "SetUserMaxBalance";
    let time = moment().utc(true).format('YYYYMMDDHHmmss');


    let queryString = querystring.stringify({
        method: method,
        Key: SECRET_KEY,
        Time: time,
        Username: username.toLowerCase(),
        MaxBalance: 500000
    });

    let desEncrypt = encodeDesCBC(queryString);
    let queryStringEncode = encodeURIComponent(desEncrypt);
    let signature = buildSignature(queryString, time);


    let options = {
        url: API_URL + '?q=' + queryStringEncode + "&s=" + signature,
        method: 'POST',
        headers: headers
    };

    console.log(options);

    request(options, (error, response, body) => {
        if (error) {
            callback('', null);
        } else {

            parseString(body, function (err, result) {

                console.log(result)
                let responseMsg = result.SetUserMaxWinResponse;

                let jsonResponse = {
                    errMsgId: responseMsg.ErrorMsgId[0],
                }
                callback(null, jsonResponse)
            });
            // callback(null, body)
        }
    });
}

function callSetBetLimit(username, limit, callback) {

    //3           5-3000
    //15          100-10000
    //17    200-50000
    //22       1000-100000
    //26      5000-200000

    let limitIds;
    //limitIds 3, 8 , 13 , 21 , 29
    if (limit == 1) {
        limitIds = '3';
    } else if (limit == 2) {
        limitIds = '3,15';
    } else if (limit == 3) {
        limitIds = '3,15,17';
    } else if (limit == 4) {
        limitIds = '3,15,17,22';
    } else if (limit == 5) {
        limitIds = '3,15,17,22,26';
    } else {
        limitIds = '3';
    }


    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    let queryString = querystring.stringify({
        agent: AGENT,
        key: SECRET_KEY,
        username: username.toLowerCase(),
        password: '123456',
        LimitID: limitIds
    });

    // let desEncrypt = encodeDesCBC(queryString);
    let signature = buildSignature(queryString);


    let options = {
        url: API_URL + '?service=Agent.ChangeLimit&' + queryString + "&sign=" + signature,
        method: 'POST',
        headers: headers
    };

    console.log(options)
    request(options, (error, response, body) => {
        if (error) {
            console.log(error)
            callback('', null);
        } else {
            console.log(body)
            let responseBody = JSON.parse(body);
            console.log(responseBody)
            if (responseBody.Success) {
                callback(null, responseBody);
            } else {
                callback(responseBody, null);
            }


        }
    });
}


function callKickUser(username, callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    let method = "KickUser";
    let time = moment().utc(true).format('YYYYMMDDHHmmss');


    let queryString = querystring.stringify({
        method: method,
        Key: SECRET_KEY,
        Time: time,
        Username: username.toLowerCase()
    });

    let desEncrypt = encodeDesCBC(queryString);
    let queryStringEncode = encodeURIComponent(desEncrypt);
    let signature = buildSignature(queryString, time);

    let options = {
        url: API_URL + '?q=' + queryStringEncode + "&s=" + signature,
        method: 'POST',
        headers: headers
    };


    request(options, (error, response, body) => {

        if (error) {
            callback('', null);
        } else {

            parseString(body, function (err, result) {

                if (!result.KickUserResponse) {

                    let responseMsg = result.APIResponse;

                    let jsonResponse = {
                        errMsgId: responseMsg.ErrorMsgId[0],
                        errMsg: responseMsg.ErrorMsg[0],
                    }
                    callback(null, jsonResponse)
                } else {
                    let responseMsg = result.KickUserResponse;

                    let jsonResponse = {
                        errMsgId: responseMsg.ErrorMsgId[0],
                        errMsg: responseMsg.ErrorMsg[0]
                    }
                    callback(null, jsonResponse)
                }

            });

        }
    });
}


module.exports = {
    getBetDetail,
    callRegUserInfo,
    callLoginRequest,
    callEnterGameInterfaceRequest,
    callEnterGameUrlRequest,
    callQueryBetLimit,
    callGetUserMaxBalance,
    callSetUserMaxBalance,
    callKickUser,
    callSetBetLimit,
    getBetResult,
    getKeyBetResult
}
