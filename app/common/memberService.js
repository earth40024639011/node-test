const MemberModel = require('../models/member.model');
const OddAdjustmentModel = require('../models/oddAdjustment.model');
const FootballModel = require('../models/football.model');
const PaymentHistoryModel = require('../models/paymentHistory.model');
const _ = require('underscore');
const mongoose = require('mongoose');
const BetTransactionModel = require('../models/betTransaction.model.js');
const UpdateBalanceHistoryModel = require('../models/updateBalanceHistory.model.js');
const UpdateBalanceHistory2Model = require('../models/updateBalanceHistory2.model.js');
const TokenModel = require('../models/token.model.js');
const async = require("async");
const moment = require('moment');
const roundTo = require('round-to');
const DateUtils = require('../common/dateUtils');

const SexyService = require('../common/sexyService');
const SaGameService = require('../common/sagameService');
const DreamGameService = require('../common/dreamgameService');

const getClientKey = require('../controllers/getClientKey');
const {
  ODD_ADJUSTMENT = 1,
  MAX_BACK_LIST = 200000
} = getClientKey.getKey();


const AgentGroupModel = require('../models/agentGroup.model');
const waterfall = require("async/waterfall");

const clientWhiteList = ['AMB_SPORTBOOK', 'SPORTBOOK88'];
const CLIENT_NAME = process.env.CLIENT_NAME || 'SPORTBOOK88';
const ODDS_BLACK_LIST = process.env.ODDS_BLACK_LIST || '33sha:26km:49vz:44gz:57qt:96fe:49vz5:49vz55a:49vz51:49vz52:49vz53';
const ODDS_BLACK_LIST2 = process.env.ODDS_BLACK_LIST2 || '3sha';
const ODDS_WHITE_LIST = process.env.ODDS_WHITE_LIST || '50sha';

const ODDS_BLACK_LIST_BY_AGENT = process.env.ODDS_BLACK_LIST_BY_AGENT || '33sha:22td:42ot:44gz:49vz:57hn:79iw:88jl';
const AUTO_CHANGE_ODDS_BY_AGENT = process.env.AUTO_CHANGE_ODDS_BY_AGENT || '0show:71sha';

module.exports = {
    logoutAllPartnerGame: function (memberId, callback) {

        MemberModel.findById(mongoose.Types.ObjectId(memberId))
            .select('username_lower')
            .exec(function (err, response) {

                if (err) {
                    callback(err, null);
                } else {

                    async.parallel([callback => {
                        SexyService.callKickUsers([response.username_lower], (err, response) => {
                            if (err) {
                                callback(err, null);
                            } else {
                                callback(null, response);
                            }
                        });

                    }, callback => {

                        SaGameService.callKickUser(response.username_lower, (err, response) => {
                            if (err) {
                                callback(err, null);
                            } else {
                                callback(null, response);
                            }
                        });
                    }, callback => {

                        DreamGameService.callKickUser(response.username_lower, false, (err, response) => {
                            if (err) {
                                callback(err, null);
                            } else {
                                callback(null, response);
                            }
                        });

                    }], (err, results) => {

                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, results);
                        }

                    });
                }

            });

    },
    modifyMemberStateDreamGame: function (memberId, isEnabled, callback) {

        MemberModel.findById(mongoose.Types.ObjectId(memberId))
            .select('username_lower')
            .exec(function (err, response) {

                if (err) {
                    callback(err, null);
                } else {

                        DreamGameService.callKickUser(response.username_lower, isEnabled, (err, response) => {
                            if (err) {
                                callback(err, null);
                            } else {
                                callback(null, response);
                            }
                        });

                }

            });

    },
    clearMemberSession : function (username,callback) {

        TokenModel.remove({username: username}).exec((err, response) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, response);
            }
        });

    },
    findById: function (memberId, selected, callback) {

        MemberModel.findById(mongoose.Types.ObjectId(memberId))
            .populate({path: 'group', select: '_id type name endpoint secretKey'})
            .select(selected)
            .exec(function (err, response) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            });

    },
    findByUserName: function (username, callback) {

        MemberModel.findOne({username_lower: username.toLowerCase()})
            .populate({path: 'group', select: '_id type name endpoint secretKey'})
            .exec(function (err, response) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            });

    },
    findByUserNameForPartnerService: function (username, callback) {

        MemberModel.findOne({username_lower: username.toLowerCase()})
        // .select('_id creditLimit balance group currency shareSetting commissionSetting limitSetting')
            .lean()
            .exec(function (err, response) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            });
    },
    getCreditSecondaryModeByName: function (username, callback) {

        MemberModel.findOne({username_lower: username.toLowerCase()}).select('creditLimit balance').read('secondaryPreferred').exec((err, memberResponse) => {

            if (err) {
                callback(err, null);
            } else {

                if (memberResponse) {
                    let credit = roundTo(roundTo(memberResponse.creditLimit,2) + roundTo(memberResponse.balance,2), 2);
                    callback(null, credit);
                } else {
                    callback(888, null);
                }
            }
        });

    },
    getOutstanding: function (memberId, callback) {

        let condition = {game:{$in:['FOOTBALL','LOTTO','M2']}};

        condition['memberId'] = mongoose.Types.ObjectId(memberId);
        condition['status'] = 'RUNNING';


        BetTransactionModel.aggregate([
            {
                $match: condition
            },
            {
                "$group": {
                    "_id": {
                        member: '$memberId'
                        // m:'$memberParentGroup'
                    },
                    "amount": {$sum: {$abs: '$memberCredit'}},
                }
            },
            {
                $project: {
                    _id: '$_id.member',
                    amount: '$amount',
                    // company_stake: '$company_stake'
                }
            }
        ], (err, results) => {
            if (err) {
                callback(err, null);
            }
            if (results && results[0]) {
                callback(null, roundTo(results[0].amount, 2))
            } else {
                callback(null, 0);
            }
        });
    },
    getOutstandingMemberByAgent: function (agentId, callback) {

        let condition = {'game': {$in:['FOOTBALL','LOTTO','M2']}};

        condition['memberParentGroup'] = mongoose.Types.ObjectId(agentId);
        condition['status'] = 'RUNNING';


        BetTransactionModel.aggregate([
            {
                $match: condition
            },
            {
                "$group": {
                    "_id": {
                        member: '$memberId'
                        // m:'$memberParentGroup'
                    },
                    "amount": {$sum: {$abs: '$memberCredit'}}
                }
            },
            {
                $project: {
                    _id: '$_id.member',
                    amount: '$amount',
                    // company_stake: '$company_stake'
                }
            }
        ], (err, results) => {
            if (err) {
                callback(err, null);
            }
            if (results) {
                callback(null, results)
            } else {
                callback(null, []);
            }
        });
    },
    getOutstandingMemberByAgent2: function (agentId, callback) {

        let condition = {'game': {$in:['FOOTBALL','LOTTO','M2']}};

        condition['memberParentGroup'] = mongoose.Types.ObjectId(agentId);
        condition['status'] = 'RUNNING';


        BetTransactionModel.aggregate([
            {
                $match: condition
            },
            {
                "$group": {
                    "_id": {
                        member: '$memberId'
                        // m:'$memberParentGroup'
                    },
                    "amount": {$sum: {$abs: '$memberCredit'}}
                }
            },
            {
                $project: {
                    _id: '$_id.member',
                    amount: '$amount',
                    // company_stake: '$company_stake'
                }
            }
        ], (err, results) => {
            if (err) {
                callback(err, null);
            }
            if (results) {
                callback(null, results)
            } else {
                callback(null, []);
            }
        });
    },
    getCredit: function (memberId, callback) {
        MemberModel.aggregate([
            {
                $match: {_id: mongoose.Types.ObjectId(memberId)}
            },
            {
                $project: {
                    credit:  {$toDouble:{$add: [{$toDecimal:'$creditLimit'}, {$toDecimal:'$balance'}]}},
                }
            },
        ], (err, results) => {

            if (err) {
                callback(err, null);
            }else {

                let credit = roundTo(results[0].credit, 2);
                if (typeof credit !== 'number' || _.isUndefined(credit) || _.isNaN(credit)) {
                    callback(null, 0);
                }else {
                    callback(null, credit);
                }
            }
        });
    },
    getInfo: function (memberId, callback) {

        async.parallel([callback => {
            MemberModel.aggregate([
                {
                    $match: {_id: mongoose.Types.ObjectId(memberId)}
                },
                {
                    $project: {
                        username: '$username',
                        loginName: '$loginName',
                        lastLogin: '$lastLogin',
                        currency: '$currency',
                        balance: '$balance',
                        givenCredit: '$creditLimit',
                        credit: {$add: ['$creditLimit', '$balance']},
                        passwordExpiry: '$forceChangePasswordDate'
                    }
                },
            ], (err, results) => {

                if (err) {
                    callback(err, null);
                } else {
                    callback(null, results[0]);
                }
            });
        }, callback => {

            let condition = {};
            condition.memberId = mongoose.Types.ObjectId(memberId);
            condition.status = {$in:['RUNNING','WAITING']};
            BetTransactionModel.aggregate([
                {
                    $match: condition
                },
                {
                    "$group": {
                        "_id": {
                            memberId: '$memberId',
                        },
                        outStandingTxn: {$sum: {$abs: '$memberCredit'}},
                        lastTransaction: {$last: '$createdDate'}
                    }
                },
                {
                    $project: {
                        outStandingTxn: '$outStandingTxn',
                        lastTransaction: '$lastTransaction'
                    }
                },
            ], (err, results) => {
                if (err) {
                    callback(err, null);
                }
                if (results && results[0]) {
                    callback(null, results[0]);
                } else {
                    callback(null, {
                        outStandingTxn: 0,
                        lastTransaction: null,
                    });
                }
            });
        }, callback => {

            BetTransactionModel.findOne({}, 'createdDate').sort('-createdDate').exec(function (err, response) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response ? response.createdDate : DateUtils.getCurrentDate());
                }
            });

        }], function (err, asyncResponse) {

            let memberInfo = asyncResponse[0];
            let transInfo = asyncResponse[1];
            let lastTransaction = asyncResponse[2];

            memberInfo.balance = memberInfo.balance + transInfo.outStandingTxn;

            let result = _.extend(memberInfo, transInfo);
            result.lastTransaction = lastTransaction;
            callback(null, _.extend(memberInfo, transInfo));

        });


    },
    updateBalance: function (userId, balance, betId, description, ref1, callback) {

        let amount = Number.parseFloat(balance);

        console.log('updateBalance : ', amount);


        if (typeof amount !== 'number' || _.isUndefined(amount) || _.isNaN(amount)) {

            let body = {
                memberId: userId,
                amount: 0,
                betId: betId,
                description: 'balance is not number : ' + amount + ' ' + (typeof amount),
                status: 'FAIL',
                ref1: ref1,
                createdDate: DateUtils.getCurrentDate()
            };

            let model = new UpdateBalanceHistoryModel(body);
            model.save(function (err, betResponse) {
                callback('balance is not number', null);
            });

        } else {

            let body = {
                memberId: userId,
                amount: amount,
                betId: betId,
                description: description,
                status: 'SUCCESS',
                ref1: ref1,
                createdDate: DateUtils.getCurrentDate()
            };
            let model = new UpdateBalanceHistoryModel(body);
            model.save(function (err, updateResponse) {
                if (err) {
                    callback(888, null);
                } else {
                    MemberModel.findOneAndUpdate({_id: userId}, {$inc: {balance: amount},lastUpdateBalance:DateUtils.getCurrentDate()}, {new: true}).exec(function (err, updateMemberResponse) {
                        if (err) {
                            UpdateBalanceHistoryModel.update({_id: updateResponse._id}, {status: 'FAIL'}, function (err, updateResponse2) {
                            });
                        }
                        callback(null, {newBalance: roundTo((updateMemberResponse.creditLimit + updateMemberResponse.balance), 2)});
                    })
                }
            });
        }
    },
    updateBalance2: function (username, balance, betId, description, ref, callback) {

        let amount = Number.parseFloat(balance);

        console.log('updateBalance : ', amount);


        if (typeof amount !== 'number' || _.isUndefined(amount) || _.isNaN(amount)) {

            let body = {
                username: username,
                amount: 0,
                betId: betId,
                description: 'balance is not number : ' + amount + ' ' + (typeof amount),
                status: 'FAIL',
                ref: ref,
                createdDate: DateUtils.getCurrentDate()
            };

            let model = new UpdateBalanceHistory2Model(body);
            model.save(function (err, betResponse) {
                callback('balance is not number', null);
            });

        } else {

            let body = {
                username: username,
                amount: amount,
                betId: betId,
                description: description,
                status: 'SUCCESS',
                ref: ref,
                createdDate: DateUtils.getCurrentDate()
            };
            let model = new UpdateBalanceHistory2Model(body);
            model.save(function (err, updateResponse) {
                if (err) {
                    callback(888, null);
                } else {
                    MemberModel.findOneAndUpdate({username_lower: username.toLowerCase()}, {$inc: {balance: amount},lastUpdateBalance:DateUtils.getCurrentDate()}, {new: true}).exec(function (err, updateMemberResponse) {
                        if (err) {
                            UpdateBalanceHistory2Model.update({_id: updateResponse._id}, {status: 'FAIL'}, function (err, updateResponse2) {
                            });
                        }
                        callback(null, {newBalance: roundTo((updateMemberResponse.creditLimit + updateMemberResponse.balance), 2)});
                    })
                }
            });
        }
    },
    createOddAdjustment: (objectModel, callback) => {
        const {
            username,
            matchId,
            prefix,
            key,
            value,
            maxBetPrice
        } = objectModel;

        const previousAmount = objectModel.amount;//current

        console.log(JSON.stringify(objectModel));

        async.waterfall([
                (callback) => {
                    if (_.contains(clientWhiteList, CLIENT_NAME)) {
                        const obj = {
                            username: username,
                            blackList: ODDS_WHITE_LIST
                        };
                        isUserBlackList(obj, (error, result) => {
                            console.log('isUserWhiteList ', result);
                            callback(null, result);
                        })
                    } else {
                        callback(null, false);
                    }
                }
            ],
            (error, isWhiteList) => {
                if (error) {
                    callback(error, null);
                } else {
                    OddAdjustmentModel.find(
                        {
                            username: username,
                            matchId: matchId,
                            prefix: prefix,
                            key: key,
                            value: value,
                            maxBetPrice: maxBetPrice
                        },
                        (err, data) => {
                            if (err) {
                                callback(err, null);
                            } else if (_.size(data) === 0) {
                                console.log(`new record ${username} ${matchId} ${prefix} ${key} ${value} ${previousAmount} ${maxBetPrice} ${countMaxBetPrice(previousAmount, maxBetPrice) * ODD_ADJUSTMENT}`);
                                const model = new OddAdjustmentModel();
                                model.amount = previousAmount;
                                model.maxBetPrice = maxBetPrice;
                                // model.count = countMaxBetPrice(model.amount, model.maxBetPrice) * ODD_ADJUSTMENT;
                                model.count = countMaxBetPrice(model.amount, model.maxBetPrice); // 1 1 2

                                if (_.isEqual('MARDUO', CLIENT_NAME)) {
                                    model.count = countMaxBetPrice(model.amount, model.maxBetPrice) * ODD_ADJUSTMENT;
                                }

                                model.value = value;
                                model.key = key;
                                model.prefix = prefix;
                                model.matchId = matchId;
                                model.username = username;

                                if (isWhiteList) {
                                    model.amount = 0;
                                    model.count = 0;
                                }
                                model.save((err) => {
                                    if (err) {
                                        callback(err, null);
                                    } else {
                                        callback(null, objectModel);
                                    }
                                });
                            } else {
                                let {
                                    username,
                                    matchId,
                                    prefix,
                                    key,
                                    value,
                                    amount,
                                    maxBetPrice,
                                    _id
                                } = _.first(data);

                                OddAdjustmentModel.remove({
                                    _id: _id
                                }, (err) => {
                                    if (err) {
                                        callback(err, null);
                                    } else {
                                        const model = new OddAdjustmentModel();
                                        model.amount = amount + previousAmount;
                                        model.maxBetPrice = maxBetPrice;
                                        // model.count = countMaxBetPrice(model.amount, model.maxBetPrice) * ODD_ADJUSTMENT;

                                        let calCount = countMaxBetPrice(model.amount, model.maxBetPrice);
                                        if (_.isEqual('MARDUO', CLIENT_NAME)) {
                                            calCount = countMaxBetPrice(model.amount, model.maxBetPrice) * ODD_ADJUSTMENT;
                                        }

                                        console.log('calCount => ', calCount);
                                        let modelCount = 17;
                                        switch (calCount) {
                                            case 0:
                                                modelCount = 0;
                                                break;
                                            case 1:
                                                modelCount = 1;
                                                break;
                                            case 2:
                                                modelCount = 2;
                                                break;
                                            case 3:
                                                modelCount = 4;
                                                break;
                                            case 4:
                                                modelCount = 6;
                                                break;
                                            case 5:
                                                modelCount = 8;
                                                break;
                                            case 6:
                                                modelCount = 10;
                                                break;
                                            case 7:
                                                modelCount = 12;
                                                break;
                                            case 8:
                                                modelCount = 14;
                                                break;
                                            case 9:
                                                modelCount = 16;
                                                break;
                                            case 10:
                                                modelCount = 18;
                                                break;
                                        }

                                        model.count = modelCount;
                                        console.log('model.count => ', model.count);

                                        model.value = value;
                                        model.key = key;
                                        model.prefix = prefix;
                                        model.matchId = matchId;
                                        model.username = username;

                                        if (isWhiteList) {
                                            model.amount = 0;
                                            model.count = 0;
                                        }
                                        model.save((err) => {
                                            if (err) {
                                                callback(err, null);
                                            } else {
                                                console.log(`update record ${_id} ${username} ${matchId} ${prefix} ${key} ${value} ${model.amount} ${model.maxBetPrice} ${model.count}`);
                                                callback(null, objectModel);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                }
            });
    },
    getOddAdjustment: (username, callback) => {
        if (!_.isEqual('GOAL6969', CLIENT_NAME)) {
            async.waterfall([
                    (callback) => {
                        if (_.contains(clientWhiteList, CLIENT_NAME)) {
                            // isUserBlackList(username, (error, result) => {
                            //     // console.log('isUserBlackList ', result);
                            //     callback(null, result);
                            // })
                            const obj = {
                                username: username,
                                blackList: ODDS_BLACK_LIST
                            };
                            isUserBlackList(obj, (error, result) => {
                                console.log('isUserBlackList ', result);
                                callback(null, result);
                            })
                        } else {
                            callback(null, false);
                        }
                    }
                ],
                (error, isWatchList) => {
                    console.log('isWatchList', isWatchList);
                    OddAdjustmentModel.find(
                        {
                            username: username
                        },
                        (err, dataUser) => {
                            if (err) {
                                callback(err, null);
                            } else {
                                OddAdjustmentModel.find(
                                    {},
                                    (err, data) => {
                                        if (err) {
                                            callback(err, null);
                                        } else {
                                            // console.log('data => ',JSON.stringify(data));
                                            const uniqModel = _.uniq(data, model => {
                                                return model.value;
                                            });
                                            // console.log('uniqModel => ',JSON.stringify(uniqModel));
                                            _.each(uniqModel, uniqModel => {
                                                const amount = _.chain(data)
                                                    .filter(model => {
                                                        return _.isEqual(uniqModel.value, model.value)
                                                    })
                                                    .pluck('amount')
                                                    .reduce((amount, num) => {
                                                        return amount + num;
                                                    }, 0)
                                                    .value();

                                                const predicate = JSON.parse(`{"value":"${uniqModel.value}"}`);
                                                const member = _.findWhere(dataUser, predicate);
                                                let memberCount = 0;
                                                if (!_.isUndefined(member)) {
                                                    memberCount = member.count;
                                                }
                                                uniqModel.amount = amount;

                                                // if (isWatchList) {
                                                if (false) {
                                                    uniqModel.count = (countMaxBetPrice(amount, MAX_BACK_LIST) * ODD_ADJUSTMENT) + memberCount;
                                                    uniqModel.count = uniqModel.count * 2;
                                                } else {
                                                    if (_.isEqual('MARDUO', CLIENT_NAME)) {
                                                        uniqModel.count = (countMaxBetPrice(amount, 1000000) * ODD_ADJUSTMENT) + memberCount;
                                                    } else {
                                                        uniqModel.count = (countMaxBetPrice(amount, 2000000) * ODD_ADJUSTMENT) + memberCount;
                                                    }
                                                }

                                                // console.log('Total amount ' + amount);
                                                // console.log('Total count ' + uniqModel.count);
                                            });


                                            if (_.contains(clientWhiteList, CLIENT_NAME)) {
                                                const obj = {
                                                    username: username,
                                                    blackList: ODDS_WHITE_LIST
                                                };
                                                isUserBlackList(obj, (error, result) => {
                                                    if (error) {
                                                        callback(null, uniqModel);
                                                    } else if (result) {
                                                        console.log('isUserWhitList ', result);
                                                        callback(null, []);
                                                    } else {
                                                        callback(null, uniqModel);
                                                    }
                                                });
                                            } else {
                                                callback(null, uniqModel);
                                            }
                                        }
                                    });
                            }
                        })
                        .select('matchId prefix key value count');
                });
        } else {
            callback(null, []);
        }

    },
    getAdjustmentByMatch: (username, callback) => {
        async.waterfall([
                (callback) => {
                    if (_.contains(clientWhiteList, CLIENT_NAME)) {
                        const obj = {
                            username: username,
                            blackList: ODDS_BLACK_LIST_BY_AGENT
                        };
                        isUserBlackListForMatch(obj, (error, result) => {
                            callback(null, result);
                        })
                    } else {
                        callback(null, false);
                    }
                }
            ],
            (error, isWatchList) => {
                console.log(`getAdjustmentByMatch user[${username}] agent[${ODDS_BLACK_LIST_BY_AGENT}] result[${isWatchList}]`);
                if (isWatchList) {
                    OddAdjustmentModel.find(
                        {
                            username: username
                        },
                        (err, dataUser) => {
                            if (err) {
                                callback(err, null);
                            } else {
                                OddAdjustmentModel.find(
                                    {},
                                    (err, data) => {
                                        if (err) {
                                            callback(err, null);
                                        } else {
                                            // console.log('data => ',JSON.stringify(data));
                                            // const uniqModel = _.uniq(data, model => {
                                            //     return model.value;
                                            // });
                                            // console.log('uniqModel => ',JSON.stringify(uniqModel));

                                            //EDIT 3
                                            let array3 = [];
                                            _.each(data, obj => {
                                                const {
                                                    value,
                                                    count,
                                                    amount
                                                } = obj;
                                                if (count > 0) {
                                                    if (_.contains(_.chain(array3)
                                                        .pluck('value')
                                                        .value(), value)) {
                                                        let odd = _.chain(array3)
                                                            .findWhere(JSON.parse(`{"value":"${value}"}`))
                                                            .value();

                                                        odd.count = count + odd.count;
                                                        odd.amount = amount + odd.amount;
                                                        array3.push(obj);
                                                    } else {
                                                        array3.push(obj);
                                                    }
                                                }
                                            });
                                            const uniqModel  = _.uniq(array3, obj => {
                                                return obj.value;
                                            });
                                            //EDIT 3

                                            _.each(uniqModel, uniqModel => {
                                                const amount = _.chain(data)
                                                    .filter(model => {
                                                        return _.isEqual(uniqModel.value, model.value)
                                                    })
                                                    .pluck('amount')
                                                    .reduce((amount, num) => {
                                                        return amount + num;
                                                    }, 0)
                                                    .value();

                                                const predicate = JSON.parse(`{"value":"${uniqModel.value}"}`);
                                                console.log(predicate);
                                                const member = _.findWhere(dataUser, predicate);
                                                console.log(member);
                                                let memberCount = 0;
                                                if (!_.isUndefined(member)) {
                                                    memberCount = member.count;
                                                }
                                                uniqModel.amount = amount;
                                                uniqModel.count = Math.min((countMaxBetPrice(amount, 100000) + memberCount), 9);

                                                console.log('Total amount ' + amount);
                                                console.log('Total count ' + uniqModel.count);
                                            });
                                            // console.log(JSON.stringify(uniqModel));
                                            let array = [];
                                            _.each(uniqModel, obj => {
                                                const {
                                                    matchId,
                                                    prefix,
                                                    key,
                                                    count
                                                } = obj;
                                                if (count > 0) {
                                                    let odd = {
                                                        id : matchId,
                                                        ah : {
                                                            h : 0,
                                                            a : 0
                                                        },
                                                        ou : {
                                                            o : 0,
                                                            u : 0
                                                        },
                                                        ah1st : {
                                                            h : 0,
                                                            a : 0
                                                        },
                                                        ou1st : {
                                                            o : 0,
                                                            u : 0
                                                        },
                                                    };
                                                    if (_.contains(_.chain(array)
                                                        .pluck('id')
                                                        .value(), matchId)) {
                                                        console.log('old');
                                                        odd = _.chain(array)
                                                            .findWhere(JSON.parse(`{"id":"${matchId}"}`))
                                                            .value();
                                                    }
                                                    if (_.isEqual(prefix, 'ah')) {
                                                        console.log('AH');
                                                        if (_.isEqual(key, 'hpk')) {
                                                            console.log('   home');
                                                            console.log(`           ${matchId} ${prefix} ${key} ${count}`);
                                                            odd.ah.h = count * -1;
                                                        } else {
                                                            console.log('   away');
                                                            console.log(`           ${matchId} ${prefix} ${key} ${count}`);
                                                            odd.ah.a = count * -1;
                                                        }
                                                    } else if (_.isEqual(prefix, 'ah1st')) {
                                                        console.log('AH 1ST');
                                                        if (_.isEqual(key, 'hpk')) {
                                                            console.log('   home');
                                                            console.log(`           ${matchId} ${prefix} ${key} ${count}`);
                                                            odd.ah1st.h = count * -1;
                                                        } else {
                                                            console.log('   away');
                                                            console.log(`           ${matchId} ${prefix} ${key} ${count}`);
                                                            odd.ah1st.a = count * -1;
                                                        }
                                                    } else if (_.isEqual(prefix, 'ou')) {
                                                        console.log('OU');
                                                        if (_.isEqual(key, 'opk')) {
                                                            console.log('   over');
                                                            console.log(`           ${matchId} ${prefix} ${key} ${count}`);
                                                            odd.ou.o = count * -1;
                                                        } else {
                                                            console.log('   under');
                                                            console.log(`           ${matchId} ${prefix} ${key} ${count}`);
                                                            odd.ou.u = count * -1;
                                                        }
                                                    } else if (_.isEqual(prefix, 'ou1st')) {
                                                        console.log('OU 1ST');
                                                        if (_.isEqual(key, 'opk')) {
                                                            console.log('   over');
                                                            console.log(`           ${matchId} ${prefix} ${key} ${count}`);
                                                            odd.ou1st.o = count * -1;
                                                        } else {
                                                            console.log('   under');
                                                            console.log(`           ${matchId} ${prefix} ${key} ${count}`);
                                                            odd.ou1st.u = count * -1;
                                                        }
                                                    }
                                                    array.push(odd);
                                                }
                                            });

                                            array  = _.uniq(array, obj => {
                                                return obj.id;
                                            });
                                            callback(null, array);
                                        }
                                    });
                            }
                        })
                        .select('matchId prefix key value count');
                } else {
                    callback(null, []);
                }
            });
    },
    getOddAdjustmentMuayThaiLocal: (username, callback) => {
        OddAdjustmentModel.find(
            {
                username: username
            },
            (err, data) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, data);
                }
            })
            .select('matchId prefix key value item amount');
    },
    findMemberOrAgentPaymentHistory: (id, dateFrom, dateTo, callback) => {

        let condition = {};
        if (dateFrom && dateTo) {
            condition['createdDate'] = {
                "$gte": moment(dateFrom, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(11).add(0, 'days').utc(true).toDate(),
                "$lt": moment(dateTo, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(11).add(1, 'days').utc(true).toDate()
            }
        }

        condition['$or'] = [{member: mongoose.Types.ObjectId(id)}, {agent: mongoose.Types.ObjectId(id)}];

        console.log('findMemberOrAgentStatement : ', condition);


        PaymentHistoryModel.aggregate([
            {
                $match: condition
            },
            {
                $group: {
                    _id: {
                        date: {
                            $cond: [
                                // {$and:[
                                {$gte: [{$hour: "$createdDate"}, 11]},
                                {$dateToString: {format: "%d/%m/%Y", date: "$createdDate"}},
                                {
                                    $dateToString: {
                                        format: "%d/%m/%Y",
                                        date: {$subtract: ["$createdDate", 24 * 60 * 60 * 1000]}
                                    }
                                }
                            ]
                        },
                    },
                    balance: {$last: '$remainingBalance'},
                    commission: {$sum: 0},
                    wl: {$sum: '$amount'},
                }
            },
            {
                $project: {
                    _id: 0,
                    date: '$_id.date',
                    balance: '$balance',
                    wl: '$wl',
                    commission: '$commission',
                    remark: 'Transactions'
                }
            }
        ], (err, results) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, results);
            }
        });

    },
    getLimitSetting: function (memberId, callback) {
        MemberModel.findOne({
            _id: memberId
        }, (err, data) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, data);
            }
        }).select('limitSetting')
    },
    getAllLimitSetting: function (callback) {
        MemberModel.find({
            suspend: false,
            lock: false,
            active: true,
            balance: {
                $gte: 0.0
            },
            lastLogin: {
                $gte: moment().add(-2, 'h').utc(true).toDate()
            }
        }, (err, data) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, data);
            }
        }).select('limitSetting')
    },
    getIsUserBlackList: (username, callback) => {
        const obj = {
            username: username,
            blackList: ODDS_BLACK_LIST
        };
        isUserBlackList(obj, (error, result) => {
            console.log('isUserBlackList ', result);
            callback(null, result);
        })
    },
    getIsUserBlackList2: (username, callback) => {
        const obj = {
            username: username,
            blackList: ODDS_BLACK_LIST2
        };
        isUserBlackList(obj, (error, result) => {
            console.log('isUserBlackList ', result);
            callback(null, result);
        });
    },
    updateBank: (memberId, reqBody, callback) => {

        let body = {
            name1: reqBody.name1,
            number1: reqBody.number1,
            name2: reqBody.name2,
            number2: reqBody.number2,
            name3: reqBody.name3,
            number3: reqBody.number3,
        };

        console.log({_id: mongoose.Types.ObjectId(memberId)})

        MemberModel.update({_id: mongoose.Types.ObjectId(memberId)}, {$set: {'configuration.bank': (body)}}).exec(function (err, response) {
            if (err) {
                callback(999, null);
                console.log('err')
            } else if (response.nModified == 1 || response.n == 1) {
                console.log(response)
                console.log('success');
                callback(null, body);
            }
            else {
                console.log(response)
                console.log('fail');
                callback(null);
            }
        })
    },


    isAutoChangeOddAgent: (group, callback) => {
        try {
            AgentGroupModel.findById(group)
                .select('_id name parentId')
                .populate({
                    path: 'parentId',
                    select: '_id name parentId',
                    populate: {
                        path: 'parentId',
                        select: '_id name parentId',
                        populate: {
                            path: 'parentId',
                            select: '_id name parentId',
                            populate: {
                                path: 'parentId',
                                select: '_id name parentId',
                                populate: {
                                    path: 'parentId',
                                    select: '_id name parentId',
                                    populate: {
                                        path: 'parentId',
                                        select: '_id name parentId',
                                    }
                                }
                            }
                        }
                    }
                }).exec((err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    const result = isDisabled(response);
                    callback(null, result);
                }
            });

            const isDisabled = (data) => {
                const {
                    parentId,
                    name
                } = data;
                console.log('^^^^^^^^^^^^^^');
                // console.log(parentId);
                console.log(name);
                console.log('^^^^^^^^^^^^^^');

                const arraySha = AUTO_CHANGE_ODDS_BY_AGENT.split(':');
                let result = false;
                if (_.contains(arraySha, name)) {
                    result = true;
                    return result;
                } else if (!_.isUndefined(data.parentId) && !_.isNull(data.parentId) && !result) {
                    if (_.contains(arraySha, data.parentId.name)) {
                        console.log('^^^^^^^^^^^^^^');
                        console.log(data.parentId.name);
                        console.log('^^^^^^^^^^^^^^');
                        result = true;
                        return result;
                    }
                    if (!result) {
                        return isDisabled(data.parentId);
                    } else {
                        return result
                    }
                } else {
                    console.log('********************** ',data.name);
                    return result;
                }
            }
        } catch (e) {
            console.error(e);
            callback(null, false);
        }
    }
};

const isUserBlackListForMatch = (obj, callback) => {
    const {
        username,
        blackList
    } = obj;
    try {
        MemberModel
            .findOne({
                username_lower: username.toLowerCase()
            })
            .populate({
                path: 'group', select: '_id'
            })
            .select('group')
            .exec((err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    AgentGroupModel.findById(response.group._id)
                        .select('_id name parentId')
                        .populate({
                            path: 'parentId',
                            select: '_id name parentId',
                            populate: {
                                path: 'parentId',
                                select: '_id name parentId',
                                populate: {
                                    path: 'parentId',
                                    select: '_id name parentId',
                                    populate: {
                                        path: 'parentId',
                                        select: '_id name parentId',
                                        populate: {
                                            path: 'parentId',
                                            select: '_id name parentId',
                                            populate: {
                                                path: 'parentId',
                                                select: '_id name parentId',
                                            }
                                        }
                                    }
                                }
                            }
                        }).exec((err, response) => {
                        if (err) {
                            callback(err, null);
                        } else {
                            // console.log('getShaUpLine response => ', JSON.stringify(response));


                            const result = isDisabled(response);
                            // console.log('isDisabled : ', result);
                            callback(null, result);
                        }
                    });

                    const isDisabled = (data) => {
                        const {
                            parentId,
                            name
                        } = data;
                        console.log('^^^^^^^^^^^^^^');
                        // console.log(parentId);
                        console.log(name);
                        console.log('^^^^^^^^^^^^^^');

                        const arraySha = blackList.split(':');
                        let result = false;
                        if (_.contains(arraySha, name)) {
                            result = true;
                            return result;
                        } else if (!_.isUndefined(data.parentId) && !_.isNull(data.parentId) && !result) {
                            if (_.contains(arraySha, data.parentId.name)) {
                                console.log('^^^^^^^^^^^^^^');
                                console.log(data.parentId.name);
                                console.log('^^^^^^^^^^^^^^');
                                result = true;
                                return result;
                            }
                            if (!result) {
                                return isDisabled(data.parentId);
                            } else {
                                return result
                            }
                        } else {
                            console.log('********************** ',data.name);
                            return result;
                        }
                    }
                }
            });
    } catch (e) {
        console.error(e);
        callback(null, false);
    }
};

const isUserBlackList = (obj, callback) => {
    const {
        username,
        blackList
    } = obj;
    try {
        MemberModel
            .findOne({
                username_lower: username.toLowerCase()
            })
            .populate({
                path: 'group', select: '_id'
            })
            .select('group')
            .exec((err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    AgentGroupModel.findById(response.group._id)
                        .select('_id name parentId')
                        .populate({
                            path: 'parentId',
                            select: '_id name parentId',
                            populate: {
                                path: 'parentId',
                                select: '_id name parentId',
                                populate: {
                                    path: 'parentId',
                                    select: '_id name parentId',
                                    populate: {
                                        path: 'parentId',
                                        select: '_id name parentId',
                                        populate: {
                                            path: 'parentId',
                                            select: '_id name parentId',
                                            populate: {
                                                path: 'parentId',
                                                select: '_id name parentId',
                                            }
                                        }
                                    }
                                }
                            }
                        }).exec((err, response) => {
                        if (err) {
                            callback(err, null);
                        } else {
                            // console.log('getShaUpLine response => ', JSON.stringify(response));

                            const result = isDisabled(response);
                            // console.log('isDisabled : ', result);
                            callback(null, result);
                        }
                    });

                    const isDisabled = (data) => {
                        const arraySha = blackList.split(':');
                        let result = false;
                        if (!_.isUndefined(data.parentId) && !_.isNull(data.parentId) && !result) {
                            // console.log('======================');
                            // console.log('data.parentId => ', JSON.stringify(data.parentId));
                            // console.log('data.parentId.name => ', data.parentId.name);
                            if (_.contains(arraySha, data.parentId.name)) {
                                result = true;
                                return result;
                            }
                            if (!result) {
                                return isDisabled(data.parentId);
                            } else {
                                return result
                            }
                        } else {
                            return result;
                        }
                    }
                }
            });
    } catch (e) {
        callback(null, false);
    }
};

const countMaxBetPriceMatch = (amount, maxBetPrice) => {
    return Math.floor(amount / maxBetPrice);
};


const countMaxBetPrice = (amount, maxBetPrice) => {
    return Math.floor(amount / convertMaxBetPricePercent(maxBetPrice));
};

const convertMaxBetPricePercent = (maxBetPrice) => {
    const result = ((maxBetPrice / 100) * 80).toFixed(2);
    // console.log(`80% of maxBetPrice is ${result}`);
    return result;
};
