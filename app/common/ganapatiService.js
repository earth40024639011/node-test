
const _ = require('underscore');


const GAMATRON_IMAGE_URL = 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/gamatron-game';
// const FORWARD_GAME = process.env.GANAPATI_URL ||'https://tw-dev.ganapati.tech:8080/';
// const AGENT_NAME = process.env.GANAPATI_AGENT_NAME || 'askmebet';
// const AGENT_CODE = process.env.GANAPATI_AGENT_CODE || 'ambbet';
const CURRENCY = 'THB';

const getClientKey = require('../controllers/getClientKey');
const {
    GANAPATI_URL: FORWARD_GAME = 'https://tw-dev.ganapati.tech:8080/',
    GANAPATI_AGENT_NAME: AGENT_NAME = 'askmebet',
    GANAPATI_AGENT_CODE: AGENT_CODE = 'ambbet'
} = getClientKey.getKey();

const CLIENT_NAME         = process.env.CLIENT_NAME          || 'SPORTBOOK88';


function forwardToGame(token, gameCode, callback) {
    let url = `${FORWARD_GAME}${AGENT_NAME}/launchGame?game=${gameCode}&currency=${CURRENCY}&locale=th&launchToken=${token}&brand=${AGENT_CODE}`;
    callback(null, url);
}

function getGameName(id) {
    switch (id){
        case "powerofelements":
            return "Power of Elements";
        case "monsterquest":
            return "Monster Quest";
        case "crypcrusade":
            return "Crypcrusade";
        case "crypbattle":
            return "Crypbattle ";
        case "sheninja":
            return "She Ninja Suzu";
        case "sushicade":
            return "Sushicade";
        case "ppap":
            return "Pikotaro's Pineapple Pen";
        case "greatbeauties":
            return "Great Beauties of China";
        case "journey_to_the_gold":
            return "Journey to the Gold";
        case "ukiyo":
            return "Ukiyo-e";
        case "wildsumo":
            return "Wild Sumo";
        case "dragonhunter":
            return "Dragon Hunter";
        case "pachinkofever":
            return "Fireworks Fever";
        case "onmyoji":
            return "Onmyoji";
        case "samuraigirl":
            return "Samurai Girl";
        case "neotokyo":
            return "Neo Tokyo";
        case "kokeshi":
            return "Kokeshi";
        case "moemoecuisine":
            return "AMoe Moe Cuisine ver.China";
        case "shibainu":
            return "Shiba Inu";
        case "pingpongking":
            return "Ping Pong King";
        case "sumi":
            return "Sumi-e";
        case "japanesemask":
            return "Japanese Mask";
        case "caishenfour":
            return "Cai Shen Four";
        case "twinklestar":
            return "Twinkle Star";
        case "pacquiao":
            return "Pacquiao One Punch KO";
        case "changingface":
            return "Sichuan Opera Face Changing";
        case "piggybankmillionaire":
            return "Piggy Bank Millionaire";
        case "battleofemperors":
            return "Battle of Emperors";
        case "safecracker":
            return "Safe Cracker";
        case "beatbox":
            return "Beat Box";
        case "xingfawang":
            return "Pockets of Riches";
        case "league":
            return "League of Conquerors";
        default:
            return id;
    }
}


function callGetGameList() {

    let gameList = [
        {
            Type: 'Slot',
            GameId: 'powerofelements',
            GameName: getGameName('powerofelements'),
            ImageUrl: GAMATRON_IMAGE_URL+'/powerofelements.png'
        },
        {
            Type: 'Slot',
            GameId: 'monsterquest',
            GameName: getGameName('monsterquest'),
            ImageUrl: GAMATRON_IMAGE_URL+'/monsterquest.png'
        },
        {
            Type: 'Slot',
            GameId: 'crypcrusade',
            GameName: getGameName('crypcrusade'),
            ImageUrl: GAMATRON_IMAGE_URL+'/crypcrusade.png'
        },
        {
            Type: 'Slot',
            GameId: 'crypbattle',
            GameName: getGameName('crypbattle'),
            ImageUrl: GAMATRON_IMAGE_URL+'/crybattle.png'
        },
        {
            Type: 'Slot',
            GameId: 'sheninja',
            GameName: getGameName('sheninja'),
            ImageUrl: GAMATRON_IMAGE_URL+'/sheninjasuzu.png'
        },
        {
            Type: 'Slot',
            GameId: 'sushicade',
            GameName: getGameName('sushicade'),
            ImageUrl: GAMATRON_IMAGE_URL+'/sushicade.png'
        },
        {
            Type: 'Slot',
            GameId: 'ppap',
            GameName: getGameName('ppap'),
            ImageUrl: GAMATRON_IMAGE_URL+'/ppap.png'
        },
        {
            Type: 'Slot',
            GameId: 'greatbeauties',
            GameName: getGameName('greatbeauties'),
            ImageUrl: GAMATRON_IMAGE_URL+'/greatbeauties.png'
        },
        {
            Type: 'Slot',
            GameId: 'journey_to_the_gold',
            GameName: getGameName('journey_to_the_gold'),
            ImageUrl: GAMATRON_IMAGE_URL+'/gold.png'
        },
        {
            Type: 'Slot',
            GameId: 'ukiyo',
            GameName: getGameName('ukiyo'),
            ImageUrl: GAMATRON_IMAGE_URL+ '/ukiyo.png'
        },
        {
            Type: 'Slot',
            GameId: 'wildsumo',
            GameName: getGameName('wildsumo'),
            ImageUrl: GAMATRON_IMAGE_URL+'/wildsumo.png'
        },
        {
            Type: 'Slot',
            GameId: 'dragonhunter',
            GameName: getGameName('dragonhunter'),
            ImageUrl: GAMATRON_IMAGE_URL+'/dragonhunter.png'
        },
        {
            Type: 'Slot',
            GameId: 'pachinkofever',
            GameName: getGameName('pachinkofever'),
            ImageUrl: GAMATRON_IMAGE_URL+'/fireworks.png'
        },
        {
            Type: 'Slot',
            GameId: 'onmyoji',
            GameName: getGameName('onmyoji'),
            ImageUrl: GAMATRON_IMAGE_URL+'/onmyoji.png'
        },
        {
            Type: 'Slot',
            GameId: 'samuraigirl',
            GameName: getGameName('samuraigirl'),
            ImageUrl: GAMATRON_IMAGE_URL+'/samuraigirl.png'
        },
        {
            Type: 'Slot',
            GameId: 'neotokyo',
            GameName: getGameName('neotokyo'),
            ImageUrl: GAMATRON_IMAGE_URL+'/neotokyo.png'
        },
        {
            Type: 'Slot',
            GameId: 'kokeshi',
            GameName: getGameName('kokeshi'),
            ImageUrl: GAMATRON_IMAGE_URL+'/kokeshi.png'
        },
        {
            Type: 'Slot',
            GameId: 'moemoecuisine',
            GameName: getGameName('moemoecuisine'),
            ImageUrl: GAMATRON_IMAGE_URL+ '/moemoecuisin.png'
        },
        {
            Type: 'Slot',
            GameId: 'shibainu',
            GameName: getGameName('shibainu'),
            ImageUrl: GAMATRON_IMAGE_URL+'/shibaInu.png'
        },
        {
            Type: 'Slot',
            GameId: 'pingpongking',
            GameName: getGameName('pingpongking'),
            ImageUrl: GAMATRON_IMAGE_URL+'/pingpongking.png'
        },
        {
            Type: 'Slot',
            GameId: 'sumi',
            GameName: getGameName('sumi'),
            ImageUrl: GAMATRON_IMAGE_URL+'/sumi-e.png'
        },
        {
            Type: 'Slot',
            GameId: 'japanesemask',
            GameName: getGameName('japanesemask'),
            ImageUrl: GAMATRON_IMAGE_URL+'/japanesemask.png'
        },
        {
            Type: 'Slot',
            GameId: 'caishenfour',
            GameName: getGameName('caishenfour'),
            ImageUrl: GAMATRON_IMAGE_URL+'/caishen.png'
        },
        {
            Type: 'Slot',
            GameId: 'twinklestar',
            GameName: getGameName('twinklestar'),
            ImageUrl: GAMATRON_IMAGE_URL+'/twinklestar.png'
        },
        {
            Type: 'Slot',
            GameId: 'pacquiao-gm',
            GameName: getGameName('pacquiao'),
            ImageUrl: GAMATRON_IMAGE_URL+'/pacquiao.png'
        },
        {
            Type: 'Slot',
            GameId: 'changingface',
            GameName: getGameName('changingface'),
            ImageUrl: GAMATRON_IMAGE_URL+'/facechanging.png'
        },
        {
            Type: 'Slot',
            GameId: 'piggybankmillionaire',
            GameName: getGameName('piggybankmillionaire'),
            ImageUrl: GAMATRON_IMAGE_URL+'/piggybank.png'
        },
        {
            Type: 'Slot',
            GameId: 'battleofemperors',
            GameName: getGameName('battleofemperors'),
            ImageUrl: GAMATRON_IMAGE_URL+'/battleofemperors.png'
        },
        // {
        //     Type: 'Slot',
        //     GameId: 'safecracker',
        //     GameName:getGameName('safecracker'),
        //     ImageUrl: GAMATRON_IMAGE_URL+'/safecracker.png'
        // },
        {
            Type: 'Slot',
            GameId: 'beatbox',
            GameName: getGameName('beatbox'),
            ImageUrl: GAMATRON_IMAGE_URL+'/beatbox.png'
        },
        {
            Type: 'Slot',
            GameId: 'xingfawang',
            GameName: getGameName('xingfawang'),
            ImageUrl: GAMATRON_IMAGE_URL+'/pocketsofriches.png'
        },
        {
            Type: 'Slot',
            GameId: 'league',
            GameName: getGameName('league'),
            ImageUrl: GAMATRON_IMAGE_URL+'/leagueofconquerors.png'
        }
    ]

    return gameList;
}

module.exports = {callGetGameList, forwardToGame}