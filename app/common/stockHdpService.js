
const express = require('express');
const router = express.Router();


const AdjustmentByMatch = require('../models/oddAdjustmentByMatch.model');



function getHdpList(callback,err) {

    AdjustmentByMatch.find({})
        .lean()
        .exec((error, response) => {
            if (error) {
                callback (err,null);
            } else {
                callback (null,response);
            }
        });
}

module.exports = {
    getHdpList
};
