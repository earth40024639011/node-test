'use strict'

const getIndexName = (clientName) => {
    let _indexName = ''

    switch(clientName) {
        case 'AMB_SPORTBOOK':
            _indexName = 'sportbook-ambbet_latest'
            break;
        case 'CSR':
            _indexName = 'sportbook-csr_latest'
            break;
        default:
            _indexName = 'sportbook88'
    }

    return _indexName;
}

module.exports = {
    getIndexName
}