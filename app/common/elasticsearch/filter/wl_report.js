const Filter = {
    gameType: (game) => {

        let gameFilter = { "bool": { "should": [] } };
    
        let multiSourceGames = {
            'FOOTBALL': {
                'gameType': 'FOOTBALL',
                'gameSubType': 'TODAY'
            },
            'STEP': {
                'gameType': 'FOOTBALL',
                'gameSubType': 'MIX_STEP'
            },
            'PARLAY': {
                'gameType': 'FOOTBALL',
                'gameSubType': 'PARLAY'
            },
            'LOTTO': {
                'gameType': 'LOTTO',
                'gameSubType': 'AMB_LOTTO'
            },
            'LOTTO_LAOS': {
                'gameType': 'LOTTO',
                'gameSubType': 'AMB_LOTTO_LAOS'
            },
            'LOTTO_PP': {
                'gameType': 'LOTTO',
                'gameSubType': 'AMB_LOTTO_PP'
            }
        }
    
        let arrayGame = game.split(',');
        
        let multiSourceBuilder = {};
    
        // gameTermPattern      { "term": { "game.keyword": "" } }
        // sourceTermPattern    { "term": {"source.keyword": ""} }
    
        arrayGame.forEach((value, index) => {
    
            // check game is in multiSourceGame
            let gameDetail = multiSourceGames[value];
            if (gameDetail) {
    
                if (!multiSourceBuilder[gameDetail.gameType]) {
                    multiSourceBuilder[gameDetail.gameType] = [];
                }
                multiSourceBuilder[gameDetail.gameType].push({ "term": {"source": gameDetail.gameSubType} })
    
            } else {
                gameFilter.bool.should.push({ "term": { "game": value } })
            }
        })
    
        for (let key in multiSourceBuilder) {
            let boolMustPattern = { "bool": { "must": [] } };
    
            boolMustPattern.bool.must.push({ "term": { "game": key } });
            boolMustPattern.bool.must.push({ "bool": { "should": multiSourceBuilder[key] } });
    
            gameFilter.bool.should.push(boolMustPattern);
        }
    
        return gameFilter;
    }
}

module.exports = {
    Filter
}