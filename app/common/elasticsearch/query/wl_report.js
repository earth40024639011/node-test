const { Filter } = require('../filter/wl_report')

let lookingStatus = ['DONE', 'REJECTED', 'CANCELLED'];
let inStatus = {
    "terms": {
        "status": lookingStatus
    }
};

const wlESQueryBuilder = (_config) => {
    let config = {
        startDate: _config.startDate,
        endDate: _config.endDate,
        matchLevel: _config.matchLevel,
        groupByLevel: _config.groupByLevel,
        termLevel: _config.termLevel,
        grossComCol : _config.grossComCol,
        games: _config.games
    }


    let gameFilter = Filter.gameType(config.games);

    config.termLevel = config.termLevel + '';

    let query = {
        "size": 0,
        "query": {
            "bool": {
                "must": [
                    {
                        "range": {
                            "game_date": {
                                "gte" : `${config.startDate}`,
                                "lt" : `${config.endDate}`
                            }
                        }
                    },
                    {
                        "term": {
                            [config.termLevel]: `${config.matchLevel}`
                        }
                    }, gameFilter , inStatus
                ]
            }
        },
        "aggs": {
            "report": {
                "terms": {
                    "field":`${config.groupByLevel}`,
                    "size": 100000
                },
                "aggs": {
                    "gross_comm": {
                        "sum": {
                            // "field": "amount"
                            "script": {
                                "lang": "painless",
                                "source": "if (doc['valid_amount'].size() != 0 && doc['"+config.grossComCol+"'].size() != 0) {(doc['valid_amount'].value * (doc['"+config.grossComCol+"'].value / 100.000))}"
                            }
                        }
                    },
                    // "stack_count": {
                    //     "value_count": {
                    //         "field": "valid_amount"
                    //     }
                    // },
                    "amount": {
                        "sum": {
                            "field": "amount"
                        }
                    },
                    "valid_amount": {
                        "sum": {
                            "field": "valid_amount"
                        }
                    },
                    "superadmin_winlose": {
                        "sum": {
                            "field": "com_sa_wl"
                        }
                    },
                    "superadmin_winlose_com": {
                        "sum": {
                            "field": "com_sa_wl_com"
                        }
                    },
                    "company_winlose": {
                        "sum": {
                            "field": "com_co_wl"
                        }
                    },
                    "company_winlose_com": {
                        "sum": {
                            "field": "com_co_wl_com"
                        }
                    },
                    "shareholder_winlose": {
                        "sum": {
                            "field": "com_sh_wl"
                        }
                    },
                    "shareholder_winlose_com": {
                        "sum": {
                            "field": "com_sh_wl_com"
                        }
                    },
                    "senior_winlose": {
                        "sum": {
                            "field": "com_sr_wl"
                        }
                    },
                    "senior_winlose_com": {
                        "sum": {
                            "field": "com_sr_wl_com"
                        }
                    },
                    "masteragent_winlose": {
                        "sum": {
                            "field": "com_ma_wl"
                        }
                    },
                    "masteragent_winlose_com": {
                        "sum": {
                            "field": "com_ma_wl_com"
                        }
                    },
                    "agent_winlose": {
                        "sum": {
                            "field": "com_ag_wl"
                        }
                    },
                    "agent_winlose_com": {
                        "sum": {
                            "field": "com_ag_wl_com"
                        }
                    },
                    "member_winlose": {
                        "sum": {
                            "field": "com_mb_wl"
                        }
                    },
                    "member_winlose_com": {
                        "sum": {
                            "field": "com_mb_wl_com"
                        }
                    }
                }
            }
        }
    }

    return query;
}

const wlMatchESQueryBuilder = (_config) => {
    let config = {
        startDate: _config.startDate,
        endDate: _config.endDate,
        matchLevel: _config.matchLevel,
        groupByLevel: _config.groupByLevel,
        termLevel: _config.termLevel,
        grossComCol : _config.grossComCol
    }


    config.termLevel = config.termLevel + '';

    console.log(config)

    let query = {
        "size": 0,
        "query": {
            "bool": {
                "must": [
                    {
                        "range": {
                            "game_date": {
                                "gte" : `${config.startDate}`,
                                "lt" : `${config.endDate}`
                            }
                        }
                    },
                    {
                        "term": {
                            [config.termLevel]: `${config.matchLevel}`
                        }
                    },inStatus
                ]
            }
        },
        "aggs": {
            "report": {
                "terms": {
                    "field":'source',//`${config.groupByLevel}`,
                    "size": 100000
                },
                "aggs": {
                    // "source": {
                    //     "terms": {"field": "source"}
                    // },
                    "gross_comm": {
                        "sum": {
                            // "field": "amount"
                            "script": {
                                "lang": "painless",
                                "source": "if (doc['valid_amount'].size() != 0 && doc['"+config.grossComCol+"'].size() != 0) {(doc['valid_amount'].value * (doc['"+config.grossComCol+"'].value / 100.000))}"
                            }
                        }
                    },
                    "amount": {
                        "sum": {
                            "field": "amount"
                        }
                    },
                    "valid_amount": {
                        "sum": {
                            "field": "valid_amount"
                        }
                    },
                    "superadmin_winlose": {
                        "sum": {
                            "field": "com_sa_wl"
                        }
                    },
                    "superadmin_winlose_com": {
                        "sum": {
                            "field": "com_sa_wl_com"
                        }
                    },
                    "company_winlose": {
                        "sum": {
                            "field": "com_co_wl"
                        }
                    },
                    "company_winlose_com": {
                        "sum": {
                            "field": "com_co_wl_com"
                        }
                    },
                    "shareholder_winlose": {
                        "sum": {
                            "field": "com_sh_wl"
                        }
                    },
                    "shareholder_winlose_com": {
                        "sum": {
                            "field": "com_sh_wl_com"
                        }
                    },
                    "senior_winlose": {
                        "sum": {
                            "field": "com_sr_wl"
                        }
                    },
                    "senior_winlose_com": {
                        "sum": {
                            "field": "com_sr_wl_com"
                        }
                    },
                    "masteragent_winlose": {
                        "sum": {
                            "field": "com_ma_wl"
                        }
                    },
                    "masteragent_winlose_com": {
                        "sum": {
                            "field": "com_ma_wl_com"
                        }
                    },
                    "agent_winlose": {
                        "sum": {
                            "field": "com_ag_wl"
                        }
                    },
                    "agent_winlose_com": {
                        "sum": {
                            "field": "com_ag_wl_com"
                        }
                    },
                    "member_winlose": {
                        "sum": {
                            "field": "com_mb_wl"
                        }
                    },
                    "member_winlose_com": {
                        "sum": {
                            "field": "com_mb_wl_com"
                        }
                    }
                }
            }
        }
    }

    return query;
}


const agentPaymentESQueryBuilder = (_config) => {
    let config = {
        startDate: _config.startDate,
        endDate: _config.endDate,
        matchLevel: _config.matchLevel,
        groupByLevel: _config.groupByLevel,
        termLevel: _config.termLevel
    }


    config.termLevel = config.termLevel + '';

    console.log(config);

    let query = {
        "size": 0,
        "query": {
            "bool": {
                "must": [
                    {
                        "range": {
                            "game_date": {
                                "gte" : `${config.startDate}`,
                                "lt" : `${config.endDate}`
                            }
                        }
                    },
                    {
                        "term": {
                            [config.termLevel]: `${config.matchLevel}`
                        }
                    },
                    {
                        "term": {
                            'status': 'DONE'
                        }
                    }
                ]
            }
        },
        "aggs": {
            "report": {
                "terms": {
                    "field":`${config.groupByLevel}`,
                    "size": 100000
                },
                "aggs": {
                    "amount": {
                        "sum": {
                            "field": "amount"
                        }
                    },
                    "valid_amount": {
                        "sum": {
                            "field": "valid_amount"
                        }
                    },
                    "superadmin_winlose": {
                        "sum": {
                            "field": "com_sa_wl"
                        }
                    },
                    "superadmin_winlose_com": {
                        "sum": {
                            "field": "com_sa_wl_com"
                        }
                    },
                    "company_winlose": {
                        "sum": {
                            "field": "com_co_wl"
                        }
                    },
                    "company_winlose_com": {
                        "sum": {
                            "field": "com_co_wl_com"
                        }
                    },
                    "shareholder_winlose": {
                        "sum": {
                            "field": "com_sh_wl"
                        }
                    },
                    "shareholder_winlose_com": {
                        "sum": {
                            "field": "com_sh_wl_com"
                        }
                    },
                    "senior_winlose": {
                        "sum": {
                            "field": "com_sr_wl"
                        }
                    },
                    "senior_winlose_com": {
                        "sum": {
                            "field": "com_sr_wl_com"
                        }
                    },
                    "masteragent_winlose": {
                        "sum": {
                            "field": "com_ma_wl"
                        }
                    },
                    "masteragent_winlose_com": {
                        "sum": {
                            "field": "com_ma_wl_com"
                        }
                    },
                    "agent_winlose": {
                        "sum": {
                            "field": "com_ag_wl"
                        }
                    },
                    "agent_winlose_com": {
                        "sum": {
                            "field": "com_ag_wl_com"
                        }
                    },
                    "member_winlose": {
                        "sum": {
                            "field": "com_mb_wl"
                        }
                    },
                    "member_winlose_com": {
                        "sum": {
                            "field": "com_mb_wl_com"
                        }
                    }
                }
            }
        }
    }

    return query;
}

module.exports = {
    wlESQueryBuilder,
    wlMatchESQueryBuilder,
    agentPaymentESQueryBuilder
}