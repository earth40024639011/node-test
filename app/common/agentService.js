const AgentGroupModel = require('../models/agentGroup.model');
const MemberModel = require('../models/member.model');
const UserModel = require('../models/users.model');
const MemberService = require('../common/memberService');
const NumberUtils = require('../common/numberUtils1');

const SexyService = require('../common/sexyService');
const SaGameService = require('../common/sagameService');
const DreamGameService = require('../common/dreamgameService');

const roundTo = require('round-to');
const _ = require('underscore');
const mongoose = require('mongoose');
const async = require("async");

const DRAW = "DRAW";
const WIN = "WIN";
const HALF_WIN = "HALF_WIN";
const LOSE = "LOSE";
const HALF_LOSE = "HALF_LOSE";
const CLIENT_NAME      = process.env.CLIENT_NAME || 'SPORTBOOK88';

const getClientKey = require('../controllers/getClientKey');
const {
    LICENSE
} = getClientKey.getKey();


module.exports = {
    isAllowPermission: function (userAgentId,memberId ,callback) {
        console.log('userAgentId : '+userAgentId + ' , memberId : '+memberId);
        UserModel.findOne({_id:mongoose.Types.ObjectId(userAgentId)})
            .exec(function (err, userResponse) {
                if (err) {
                    callback(err, false);
                } else {

                    if(userResponse) {
                        console.log('userResponse : ',userResponse.username);
                        MemberModel.findById(mongoose.Types.ObjectId(memberId))
                            .select('username_lower group')
                            .exec(function (err, memberResponse) {


                            console.log('memberResponse : ',memberResponse.username_lower)
                            if(memberResponse) {
                                console.log(userResponse.group.toString() + ' : '+memberResponse.group.toString())
                                if (userResponse.group.toString() == memberResponse.group.toString()) {
                                    callback(null, true);
                                } else {
                                    callback(null, false);
                                }
                            }else {
                                callback(null, false);
                            }

                        });
                    }else {
                        callback(null, false);
                    }
                }
            });
    },
    getOfflineCashStatus: function (username, callback) {
        console.log('LICENSE => ', LICENSE);
        try {
            if (!_.isUndefined(LICENSE)) {
                MemberModel
                    .findOne({
                        username_lower: username.toLowerCase()
                    })
                    .populate({
                        path: 'group', select: '_id'
                    })
                    .exec((err, response) => {
                        if (err) {
                            callback(err, false);
                        } else {
                            AgentGroupModel.findById(response.group._id)
                                .select('_id name parentId')
                                .populate({
                                    path: 'parentId',
                                    select: '_id name parentId',
                                    populate: {
                                        path: 'parentId',
                                        select: '_id name parentId',
                                        populate: {
                                            path: 'parentId',
                                            select: '_id name parentId',
                                            populate: {
                                                path: 'parentId',
                                                select: '_id name parentId',
                                                populate: {
                                                    path: 'parentId',
                                                    select: '_id name parentId',
                                                    populate: {
                                                        path: 'parentId',
                                                        select: '_id name parentId',
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }).exec((err, response) => {
                                if (err) {
                                    callback(err, null);
                                } else {
                                    console.log('getOfflineCashStatus response => ', JSON.stringify(response));

                                    const result = isAllowed(response);
                                    console.log('isAllowed : ', result);
                                    if (result) {
                                        callback(err, true);
                                    } else {
                                        callback(err, false);
                                    }
                                }
                            });
                            let isFirst = true;
                            const isAllowed = (data) => {
                                let result = false;
                                if (isFirst) {
                                    isFirst = !isFirst;
                                    console.log('========== isFirst ============');
                                    // console.log('data.parentId => ', JSON.stringify(data.parentId));

                                    const arraySha = LICENSE.split(':');
                                    _.each(arraySha, obj => {
                                        console.log(obj + ' data.parentId._id => ', data._id, ' ', data.name);
                                        if (obj == data._id) {
                                            result = true;
                                            return result;
                                        } else {
                                            console.log('========== isND ============');
                                            console.log(' data.parentId._id => ', data.parentId._id, ' ', data.parentId.name);
                                            if (obj == data.parentId._id) {
                                                result = true;
                                                return result;
                                            }
                                        }
                                    });

                                    if (!result) {
                                        return isAllowed(data.parentId);
                                    } else {
                                        return result
                                    }
                                } else {
                                    if (!_.isUndefined(data.parentId) && !_.isNull(data.parentId) && !result) {
                                        console.log('======================');
                                        // console.log('data.parentId => ', JSON.stringify(data.parentId));
                                        const arraySha = LICENSE.split(':');
                                        // console.log(arraySha + ' data.parentId._id => ',data.parentId._id, ' ', data.parentId.name);
                                        // console.log(_.contains(arraySha, data.parentId._id));
                                        // if (LICENSE == data.parentId._id) {
                                        // if (_.contains(arraySha, data.parentId._id)) {
                                        //     result = true;
                                        //     return result;
                                        // }

                                        _.each(arraySha, obj => {
                                            console.log(obj + ' data.parentId._id => ', data.parentId._id, ' ', data.parentId.name);
                                            if (obj == data.parentId._id) {
                                                result = true;
                                                return result;
                                            }
                                        });
                                        if (!result) {
                                            return isAllowed(data.parentId);
                                        } else {
                                            return result
                                        }
                                    } else {
                                        return result;
                                    }
                                }
                            }
                        }
                    });
            } else {
                console.log('LICENSE is undefined');
                callback('LICENSE is undefined', false);
            }
        } catch (e) {
            callback(e, false);
        }
    },
    findById: function (agentId, callback) {

        AgentGroupModel.findById(mongoose.Types.ObjectId(agentId))
            .exec(function (err, response) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            });
    },
    getCreditLimitUsage: function (groupId, callback) {

        async.parallel([(callback) => {
            AgentGroupModel.aggregate([
                {
                    $match: {
                        parentId: mongoose.Types.ObjectId(groupId)
                    }
                },
                {
                    $project: {_id: 0}
                },
                {
                    "$group": {
                        "_id": {
                            id: '$_id'
                        },
                        "creditLimit": {$sum: '$creditLimit'}
                    }
                }
            ], function (err, results) {

                if (err) {
                    callback(err, '');
                }
                callback(null, results[0] ? results[0].creditLimit : 0);
            });
        }, (callback) => {
            MemberModel.aggregate([
                {
                    $match: {
                        group: mongoose.Types.ObjectId(groupId)
                    }
                },
                {
                    $project: {_id: 0}
                },
                {
                    "$group": {
                        "_id": {
                            id: '$_id'
                        },
                        "creditLimit": {$sum: '$creditLimit'}
                    }
                }
            ], function (err, results) {

                if (err) {
                    callback(err, '');
                }
                callback(null, results[0] ? results[0].creditLimit : 0);
            });
        }], function (err, results) {

            if (err) {
                callback(err, '');
            }
            callback(null, results[0] + results[1]);

        });

    },
    getMaxShareUsage: function (groupId, callback) {
        async.parallel([subCallback => {

            AgentGroupModel.aggregate([
                {
                    $match: {
                        parentId: mongoose.Types.ObjectId(groupId)
                    }
                },
                {
                    "$group": {
                        "_id": {
                            id: '$parentId'
                        },
                        "today": {$max: {$add: ['$shareSetting.sportsBook.today.parent', '$shareSetting.sportsBook.today.own']}},
                        "live": {$max: {$add: ['$shareSetting.sportsBook.live.parent', '$shareSetting.sportsBook.live.own']}},
                        "parlay": {$max: {$add: ['$shareSetting.step.parlay.parent', '$shareSetting.step.parlay.own']}},
                        "sexy": {$max: {$add: ['$shareSetting.casino.sexy.parent', '$shareSetting.casino.sexy.own']}},
                        // "lotus": {$max: {$add: ['$shareSetting.casino.lotus.parent', '$shareSetting.casino.lotus.own']}},
                        "sa": {$max: {$add: ['$shareSetting.casino.sa.parent', '$shareSetting.casino.sa.own']}},
                        "dg": {$max: {$add: ['$shareSetting.casino.dg.parent', '$shareSetting.casino.dg.own']}},
                        "ag": {$max: {$add: ['$shareSetting.casino.ag.parent', '$shareSetting.casino.ag.own']}},
                        "ambGame": {$max: {$add: ['$shareSetting.multi.amb.parent', '$shareSetting.multi.amb.own']}},
                        // "allbet": {$max: {$add: ['$shareSetting.casino.allbet.parent', '$shareSetting.casino.allbet.own']}},
                        "slotXO": {$max: {$add: ['$shareSetting.game.slotXO.parent', '$shareSetting.game.slotXO.own']}},
                        "ambLotto": {$max: {$add: ['$shareSetting.lotto.amb.parent', '$shareSetting.lotto.amb.own']}},
                        "m2": {$max: {$add: ['$shareSetting.other.m2.parent', '$shareSetting.other.m2.own']}},
                        "creditLimit": {$sum: '$creditLimit'}
                    }
                },
                {
                    $project: {
                        _id: 0,
                        // hdpOuOe: '$hdpOuOe',
                        // oneTwoDoubleChance: '$oneTwoDoubleChance',
                        today: '$today',
                        live: '$live',
                        parlay: '$parlay',
                        // others: '$others',
                        sexy: '$sexy',
                        // lotus: '$lotus',
                        sa: '$sa',
                        dg: '$dg',
                        ag: '$ag',
                        ambGame: '$ambGame',
                        // allbet: '$allbet',
                        slotXO: '$slotXO',
                        ambLotto: '$ambLotto',
                        m2: '$m2',
                        creditLimit: '$creditLimit'
                    }
                }
            ], function (err, results) {

                if (err) {
                    subCallback(err, '');
                }
                if (results[0]) {
                    subCallback(null, results[0]);
                } else {
                    subCallback(null, {
                        today: 0,
                        live: 0,
                        parlay: 0,
                        sexy: 0,
                        // lotus: 0,
                        sa: 0,
                        dg: 0,
                        ag: 0,
                        ambGame: 0,
                        // allbet: 0,
                        slotXO: 0,
                        m2: 0,
                        creditLimit: 0,
                        ambLotto: 0
                    })
                }
            });
        }, subCallback => {


            MemberModel.aggregate([
                {
                    $match: {
                        group: mongoose.Types.ObjectId(groupId)
                    }
                },
                {
                    "$group": {
                        "_id": {
                            id: '$group'
                        },
                        "today": {$max: '$shareSetting.sportsBook.today.parent'},
                        "live": {$max: '$shareSetting.sportsBook.live.parent'},
                        "parlay": {$max: '$shareSetting.step.parlay.parent'},
                        "sexy": {$max: '$shareSetting.casino.sexy.parent'},
                        // "lotus": {$max: '$shareSetting.casino.lotus.parent'},
                        "sa": {$max: '$shareSetting.casino.sa.parent'},
                        "dg": {$max: '$shareSetting.casino.dg.parent'},
                        "ag": {$max: '$shareSetting.casino.ag.parent'},
                        "ambGame": {$max: '$shareSetting.multi.amb.parent'},
                        // "allbet": {$max: '$shareSetting.casino.allbet.parent'},
                        "slotXO": {$max: '$shareSetting.game.slotXO.parent'},
                        "ambLotto": {$max: '$shareSetting.lotto.amb.parent'},
                        "m2": {$max: '$shareSetting.other.m2.parent'},
                        "creditLimit": {$sum: '$creditLimit'}
                    }
                },
                {
                    $project: {
                        _id: 0,
                        today: '$today',
                        live: '$live',
                        parlay: '$parlay',
                        sexy: '$sexy',
                        // lotus: '$lotus',
                        sa: '$sa',
                        dg: '$dg',
                        ag: '$ag',
                        ambGame: '$ambGame',
                        // allbet: '$allbet',
                        slotXO: '$slotXO',
                        ambLotto: '$ambLotto',
                        m2: '$m2',
                        creditLimit: '$creditLimit'
                    }
                }
            ], function (err, results) {

                if (err) {
                    subCallback(err, '');
                }
                if (results[0]) {
                    subCallback(null, results[0]);
                } else {
                    subCallback(null, {
                        // hdpOuOe: 0,
                        // oneTwoDoubleChance: 0,
                        // others: 0,
                        today: 0,
                        live: 0,
                        parlay: 0,
                        sexy: 0,
                        // lotus: 0,
                        sa: 0,
                        dg: 0,
                        ag: 0,
                        ambGame: 0,
                        allbet: 0,
                        slotXO: 0,
                        ambLotto: 0,
                        m2: 0,
                        creditLimit: 0,
                    })
                }
            });
        }], function (err, asyncResponse) {

            if (err) {
                callback(err, null);
            } else {

                if (asyncResponse[1]) {
                    let o = {
                        today: _.max([asyncResponse[0].today, asyncResponse[1].today]),
                        live: _.max([asyncResponse[0].live, asyncResponse[1].live]),
                        parlay: _.max([asyncResponse[0].parlay, asyncResponse[1].parlay]),
                        sexy: _.max([asyncResponse[0].sexy, asyncResponse[1].sexy]),
                        // lotus: _.max([asyncResponse[0].lotus, asyncResponse[1].lotus]),
                        sa: _.max([asyncResponse[0].sa, asyncResponse[1].sa]),
                        dg: _.max([asyncResponse[0].dg, asyncResponse[1].dg]),
                        ag: _.max([asyncResponse[0].ag, asyncResponse[1].ag]),
                        ambGame: _.max([asyncResponse[0].ambGame, asyncResponse[1].ambGame]),
                        // allbet: _.max([asyncResponse[0].allbet, asyncResponse[1].allbet]),
                        slotXO: _.max([asyncResponse[0].slotXO, asyncResponse[1].slotXO]),
                        ambLotto: _.max([asyncResponse[0].ambLotto, asyncResponse[1].ambLotto]),
                        m2: _.max([asyncResponse[0].m2, asyncResponse[1].m2]),
                        creditLimit: asyncResponse[0].creditLimit + asyncResponse[1].creditLimit
                    };
                    callback(null, o);
                } else {
                    callback(null, asyncResponse[0]);
                }
            }

        })

    },
    getMaxShareUsageByIds: function (groupIds, callback) {
        async.parallel([subCallback => {

            AgentGroupModel.aggregate([
                {
                    $match: {
                        parentId: {$in: groupIds}
                    }
                },
                {
                    "$group": {
                        "_id": {
                            id: '$parentId'
                        },
                        "today": {$max: {$add: ['$shareSetting.sportsBook.today.parent', '$shareSetting.sportsBook.today.own']}},
                        "live": {$max: {$add: ['$shareSetting.sportsBook.live.parent', '$shareSetting.sportsBook.live.own']}},
                        "parlay": {$max: {$add: ['$shareSetting.step.parlay.parent', '$shareSetting.step.parlay.own']}},
                        "step": {$max: {$add: ['$shareSetting.step.step.parent', '$shareSetting.step.step.own']}},
                        "sexy": {$max: {$add: ['$shareSetting.casino.sexy.parent', '$shareSetting.casino.sexy.own']}},
                        // "lotus": {$max: {$add: ['$shareSetting.casino.lotus.parent', '$shareSetting.casino.lotus.own']}},
                        "sa": {$max: {$add: ['$shareSetting.casino.sa.parent', '$shareSetting.casino.sa.own']}},
                        "dg": {$max: {$add: ['$shareSetting.casino.dg.parent', '$shareSetting.casino.dg.own']}},
                        "ag": {$max: {$add: ['$shareSetting.casino.ag.parent', '$shareSetting.casino.ag.own']}},
                        "pt": {$max: {$add: ['$shareSetting.casino.pt.parent', '$shareSetting.casino.pt.own']}},
                        "ambGame": {$max: {$add: ['$shareSetting.multi.amb.parent', '$shareSetting.multi.amb.own']}},
                        // "allbet": {$max: {$add: ['$shareSetting.casino.allbet.parent', '$shareSetting.casino.allbet.own']}},
                        "slotXO": {$max: {$add: ['$shareSetting.game.slotXO.parent', '$shareSetting.game.slotXO.own']}},
                        "ambLotto": {$max: {$add: ['$shareSetting.lotto.amb.parent', '$shareSetting.lotto.amb.own']}},
                        "m2": {$max: {$add: ['$shareSetting.other.m2.parent', '$shareSetting.other.m2.own']}},
                        "count": {$sum: 1}
                    }
                },
                {
                    $project: {
                        _id: 0,
                        groupId: '$_id.id',
                        today: '$today',
                        live: '$live',
                        parlay: '$parlay',
                        step: '$step',
                        sexy: '$sexy',
                        // lotus: '$lotus',
                        sa: '$sa',
                        dg: '$dg',
                        ag: '$ag',
                        pt: '$pt',
                        ambGame: '$ambGame',
                        // allbet: '$allbet',
                        slotXO: '$slotXO',
                        ambLotto: '$ambLotto',
                        m2: '$m2',
                        count: '$count'
                    }
                }
            ], function (err, results) {

                if (err) {
                    subCallback(err, '');
                } else {
                    subCallback(null, results);
                }
            });
        }, subCallback => {


            MemberModel.aggregate([
                {
                    $match: {
                        group: {$in: groupIds}
                    }
                },
                {
                    "$group": {
                        "_id": {
                            id: '$group'
                        },
                        "today": {$max: '$shareSetting.sportsBook.today.parent'},
                        "live": {$max: '$shareSetting.sportsBook.live.parent'},
                        "parlay": {$max: '$shareSetting.step.parlay.parent'},
                        "step": {$max: '$shareSetting.step.step.parent'},
                        "sexy": {$max: '$shareSetting.casino.sexy.parent'},
                        // "lotus": {$max: '$shareSetting.casino.lotus.parent'},
                        "sa": {$max: '$shareSetting.casino.sa.parent'},
                        "dg": {$max: '$shareSetting.casino.dg.parent'},
                        "ag": {$max: '$shareSetting.casino.ag.parent'},
                        "pt": {$max: '$shareSetting.casino.pt.parent'},
                        "ambGame": {$max: '$shareSetting.multi.amb.parent'},
                        // "allbet": {$max: '$shareSetting.casino.allbet.parent'},
                        "slotXO": {$max: '$shareSetting.game.slotXO.parent'},
                        "ambLotto": {$max: '$shareSetting.lotto.amb.parent'},
                        "m2": {$max: '$shareSetting.other.m2.parent'},
                        "count": {$sum: 1}
                    }
                },
                {
                    $project: {
                        _id: 0,
                        groupId: '$_id.id',
                        // hdpOuOe: '$hdpOuOe',
                        // oneTwoDoubleChance: '$oneTwoDoubleChance',
                        // others: '$others',
                        today: '$today',
                        live: '$live',
                        parlay: '$parlay',
                        step: '$step',
                        sexy: '$sexy',
                        // lotus: '$lotus',
                        sa: '$sa',
                        dg: '$dg',
                        ag: '$ag',
                        pt: '$pt',
                        ambGame: '$ambGame',
                        // allbet: '$allbet',
                        slotXO: '$slotXO',
                        ambLotto: '$ambLotto',
                        m2: '$m2',
                        count: '$count'
                    }
                }
            ], function (err, results) {

                if (err) {
                    subCallback(err, '');
                } else {
                    subCallback(null, results);
                }

            });
        }], function (err, asyncResponse) {

            if (err) {
                callback(err, null);
            } else {

                let agentList = asyncResponse[0];
                let memberList = asyncResponse[1];

                let uniqueData = _.uniq(_.union(agentList, memberList), false, function (item, key, a) {
                    return item.groupId.toString();
                });

                let prepareResults = _.map(uniqueData, (agentMax) => {

                    let memberMax = _.filter(memberList, (memberObj) => {
                        return agentMax.groupId.toString() === memberObj.groupId.toString();
                    })[0];

                    if (memberMax) {
                        return {
                            groupId: agentMax.groupId.toString(),
                            today: _.max([agentMax.today, memberMax.today]),
                            live: _.max([agentMax.live, memberMax.live]),
                            parlay: _.max([agentMax.parlay, memberMax.parlay]),
                            step: _.max([agentMax.step, memberMax.step]),
                            sexy: _.max([agentMax.sexy, memberMax.sexy]),
                            // lotus: _.max([agentMax.lotus, memberMax.lotus]),
                            sa: _.max([agentMax.sa, memberMax.sa]),
                            dg: _.max([agentMax.dg, memberMax.dg]),
                            ag: _.max([agentMax.ag, memberMax.ag]),
                            pt: _.max([agentMax.pt, memberMax.pt]),
                            ambGame: _.max([agentMax.ambGame, memberMax.ambGame]),
                            // allbet: _.max([asyncResponse[0].allbet, asyncResponse[1].allbet]),
                            slotXO: _.max([agentMax.slotXO, memberMax.slotXO]),
                            ambLotto: _.max([agentMax.ambLotto, memberMax.ambLotto]),
                            m2: _.max([agentMax.m2, memberMax.m2]),
                        };
                    } else {
                        return {
                            groupId: agentMax.groupId.toString(),
                            today: agentMax.today,
                            live: agentMax.live,
                            parlay: agentMax.parlay,
                            step: agentMax.step,
                            sexy: agentMax.sexy,
                            // lotus: agentMax.lotus,
                            sa: agentMax.sa,
                            dg: agentMax.dg,
                            ag: agentMax.ag,
                            pt: agentMax.pt,
                            ambGame: agentMax.ambGame,
                            // allbet: '$allbet',
                            slotXO: agentMax.slotXO,
                            ambLotto: agentMax.ambLotto,
                            m2: agentMax.m2
                        }
                    }

                });
                callback(null, prepareResults);

            }

        })

    },
    getSexyStatus: function (agentGroupId, callback) {

        AgentGroupModel.findById(agentGroupId).select('_id limitSetting parentId').populate({
            path: 'parentId',
            select: '_id limitSetting parentId',
            populate: {
                path: 'parentId',
                select: '_id limitSetting parentId',
                populate: {
                    path: 'parentId',
                    select: '_id limitSetting parentId',
                    populate: {
                        path: 'parentId',
                        select: '_id limitSetting parentId',
                        populate: {
                            path: 'parentId',
                            select: '_id limitSetting parentId',
                            populate: {
                                path: 'parentId',
                                select: '_id limitSetting parentId',
                            }
                        }
                    }
                }
            }
        }).exec(function (err, response) {
            if (err) {
                callback(err, null);
            } else {
                let result = {
                    isEnable: true,
                };
                callback(null, isDisabled(result, response))
            }
        });

        function isDisabled(result, data) {
            if (!data.limitSetting.casino.sexy.isEnable) {
                result.isEnable = false;
            }
            if (data.parentId) {
                return isDisabled(result, data.parentId);
            } else {
                return result;
            }
        }
    },
    getAmbGameStatus: function (agentGroupId, callback) {

        AgentGroupModel.findById(agentGroupId).select('_id limitSetting parentId').populate({
            path: 'parentId',
            select: '_id limitSetting parentId',
            populate: {
                path: 'parentId',
                select: '_id limitSetting parentId',
                populate: {
                    path: 'parentId',
                    select: '_id limitSetting parentId',
                    populate: {
                        path: 'parentId',
                        select: '_id limitSetting parentId',
                        populate: {
                            path: 'parentId',
                            select: '_id limitSetting parentId',
                            populate: {
                                path: 'parentId',
                                select: '_id limitSetting parentId',
                            }
                        }
                    }
                }
            }
        }).exec(function (err, response) {
            if (err) {
                callback(err, null);
            } else {
                let result = {
                    isEnable: true,
                };
                callback(null, isDisabled(result, response))
            }
        });

        function isDisabled(result, data) {
            if (!data.limitSetting.multi.amb.isEnable) {
                result.isEnable = false;
            }
            if (data.parentId) {
                return isDisabled(result, data.parentId);
            } else {
                return result;
            }
        }
    },
    getLotusStatus: function (agentGroupId, callback) {

        AgentGroupModel.findById(agentGroupId).select('_id limitSetting parentId').populate({
            path: 'parentId',
            select: '_id limitSetting parentId',
            populate: {
                path: 'parentId',
                select: '_id limitSetting parentId',
                populate: {
                    path: 'parentId',
                    select: '_id limitSetting parentId',
                    populate: {
                        path: 'parentId',
                        select: '_id limitSetting parentId',
                        populate: {
                            path: 'parentId',
                            select: '_id limitSetting parentId',
                            populate: {
                                path: 'parentId',
                                select: '_id limitSetting parentId',
                            }
                        }
                    }
                }
            }
        }).exec(function (err, response) {
            if (err) {
                callback(err, null);
            } else {
                let result = {
                    isEnable: true,
                };
                callback(null, isDisabled(result, response))
            }
        });

        function isDisabled(result, data) {
            if (!data.limitSetting.casino.lotus.isEnable) {
                result.isEnable = false;
            }
            if (data.parentId) {
                return isDisabled(result, data.parentId);
            } else {
                return result;
            }
        }
    },
    getAllBetStatus: function (agentGroupId, callback) {

        AgentGroupModel.findById(agentGroupId).select('_id limitSetting parentId').populate({
            path: 'parentId',
            select: '_id limitSetting parentId',
            populate: {
                path: 'parentId',
                select: '_id limitSetting parentId',
                populate: {
                    path: 'parentId',
                    select: '_id limitSetting parentId',
                    populate: {
                        path: 'parentId',
                        select: '_id limitSetting parentId',
                        populate: {
                            path: 'parentId',
                            select: '_id limitSetting parentId',
                            populate: {
                                path: 'parentId',
                                select: '_id limitSetting parentId',
                            }
                        }
                    }
                }
            }
        }).exec(function (err, response) {
            if (err) {
                callback(err, null);
            } else {
                let result = {
                    isEnable: true,
                };
                callback(null, isDisabled(result, response))
            }
        });

        function isDisabled(result, data) {
            if (!data.limitSetting.casino.allbet.isEnable) {
                result.isEnable = false;
            }
            if (data.parentId) {
                return isDisabled(result, data.parentId);
            } else {
                return result;
            }
        }
    },
    getSaGameStatus: function (agentGroupId, callback) {

        AgentGroupModel.findById(agentGroupId).select('_id limitSetting parentId').populate({
            path: 'parentId',
            select: '_id limitSetting parentId',
            populate: {
                path: 'parentId',
                select: '_id limitSetting parentId',
                populate: {
                    path: 'parentId',
                    select: '_id limitSetting parentId',
                    populate: {
                        path: 'parentId',
                        select: '_id limitSetting parentId',
                        populate: {
                            path: 'parentId',
                            select: '_id limitSetting parentId',
                            populate: {
                                path: 'parentId',
                                select: '_id limitSetting parentId',
                            }
                        }
                    }
                }
            }
        }).exec(function (err, response) {
            if (err) {
                callback(err, null);
            } else {
                let result = {
                    isEnable: true,
                };
                callback(null, isDisabled(result, response))
            }
        });

        function isDisabled(result, data) {
            if (!data.limitSetting.casino.sa.isEnable) {
                result.isEnable = false;
            }
            if (data.parentId) {
                return isDisabled(result, data.parentId);
            } else {
                return result;
            }
        }
    },
    getAGGameStatus: function (agentGroupId, callback) {

        AgentGroupModel.findById(agentGroupId).select('_id limitSetting parentId').populate({
            path: 'parentId',
            select: '_id limitSetting parentId',
            populate: {
                path: 'parentId',
                select: '_id limitSetting parentId',
                populate: {
                    path: 'parentId',
                    select: '_id limitSetting parentId',
                    populate: {
                        path: 'parentId',
                        select: '_id limitSetting parentId',
                        populate: {
                            path: 'parentId',
                            select: '_id limitSetting parentId',
                            populate: {
                                path: 'parentId',
                                select: '_id limitSetting parentId',
                            }
                        }
                    }
                }
            }
        }).exec(function (err, response) {
            if (err) {
                callback(err, null);
            } else {
                let result = {
                    isEnable: true,
                };
                callback(null, isDisabled(result, response))
            }
        });

        function isDisabled(result, data) {
            if (!data.limitSetting.casino.ag.isEnable) {
                result.isEnable = false;
            }
            if (data.parentId) {
                return isDisabled(result, data.parentId);
            } else {
                return result;
            }
        }
    },
    getSportBookStatus: function (agentGroupId, callback) {

        AgentGroupModel.findById(agentGroupId).select('_id limitSetting parentId').populate({
            path: 'parentId',
            select: '_id limitSetting parentId',
            populate: {
                path: 'parentId',
                select: '_id limitSetting parentId',
                populate: {
                    path: 'parentId',
                    select: '_id limitSetting parentId',
                    populate: {
                        path: 'parentId',
                        select: '_id limitSetting parentId',
                        populate: {
                            path: 'parentId',
                            select: '_id limitSetting parentId',
                            populate: {
                                path: 'parentId',
                                select: '_id limitSetting parentId',
                            }
                        }
                    }
                }
            }
        }).exec(function (err, response) {
            if (err) {
                callback(err, null);
            } else {
                let result = {
                    isEnable: true,
                };
                callback(null, isDisabled(result, response))
            }
        });

        function isDisabled(result, data) {
            if (!data.limitSetting.sportsBook.isEnable) {
                result.isEnable = false;
            }
            if (data.parentId) {
                return isDisabled(result, data.parentId);
            } else {
                return result;
            }
        }
    },
    getAmbLottoStatus: function (agentGroupId, callback) {

        AgentGroupModel.findById(agentGroupId).select('_id limitSetting parentId').populate({
            path: 'parentId',
            select: '_id limitSetting parentId',
            populate: {
                path: 'parentId',
                select: '_id limitSetting parentId',
                populate: {
                    path: 'parentId',
                    select: '_id limitSetting parentId',
                    populate: {
                        path: 'parentId',
                        select: '_id limitSetting parentId',
                        populate: {
                            path: 'parentId',
                            select: '_id limitSetting parentId',
                            populate: {
                                path: 'parentId',
                                select: '_id limitSetting parentId',
                            }
                        }
                    }
                }
            }
        }).exec(function (err, response) {
            if (err) {
                callback(err, null);
            } else {
                let result = {
                    isEnable: true,
                };
                callback(null, isDisabled(result, response))
            }
        });

        function isDisabled(result, data) {
            if (!data.limitSetting.lotto.amb.isEnable) {
                result.isEnable = false;
            }
            if (data.parentId) {
                return isDisabled(result, data.parentId);
            } else {
                return result;
            }
        }
    },
    getLive22Status: function (agentGroupId, callback) {

        AgentGroupModel.findById(agentGroupId).select('_id limitSetting parentId').populate({
            path: 'parentId',
            select: '_id limitSetting parentId',
            populate: {
                path: 'parentId',
                select: '_id limitSetting parentId',
                populate: {
                    path: 'parentId',
                    select: '_id limitSetting parentId',
                    populate: {
                        path: 'parentId',
                        select: '_id limitSetting parentId',
                        populate: {
                            path: 'parentId',
                            select: '_id limitSetting parentId',
                            populate: {
                                path: 'parentId',
                                select: '_id limitSetting parentId',
                            }
                        }
                    }
                }
            }
        }).exec(function (err, response) {
            if (err) {
                callback(err, null);
            } else {
                let result = {
                    isEnable: true,
                };
                callback(null, isDisabled(result, response))
            }
        });

        function isDisabled(result, data) {
            if (!data.limitSetting.game.slotXO.isEnable) {
                result.isEnable = false;
            }
            if (data.parentId) {
                return isDisabled(result, data.parentId);
            } else {
                return result;
            }
        }
    },
    getUpLineStatus: function (agentGroupId, callback) {

        AgentGroupModel.findById(mongoose.Types.ObjectId(agentGroupId)).select('_id lock suspend parentId limitSetting name_lower').populate({
            path: 'parentId',
            select: '_id lock suspend parentId limitSetting name_lower',
            populate: {
                path: 'parentId',
                select: '_id lock suspend parentId limitSetting name_lower',
                populate: {
                    path: 'parentId',
                    select: '_id lock suspend parentId limitSetting name_lower',
                    populate: {
                        path: 'parentId',
                        select: '_id lock suspend parentId limitSetting name_lower',
                        populate: {
                            path: 'parentId',
                            select: '_id lock suspend parentId limitSetting name_lower',
                        }
                    }
                }
            }
        }).exec(function (err, response) {
            if (err) {
                callback(err, null);
            } else {

                if (response) {
                    let result = {
                        lock: false,
                        suspend: false,
                        sportBookEnable: true,
                        stepEnable: true,
                        parlayEnable: true,
                        casinoSaEnable: true,
                        casinoAgEnable: true,
                        casinoSexyEnable: true,
                        casinoDgEnable: true,
                        casinoPtEnable: true,
                        gameSlotEnable: true,
                        gameCardEnable: true,
                        ambLottoEnable: true,
                        ambLottoPPEnable: true,
                        ambLottoLaosEnable: true,
                        otherM2Enable: true,
                        downline: []
                    };
                    callback(null, isLockOrSuspend(result, response));
                } else {
                    callback('not found', null);
                }
            }
        });

        function isLockOrSuspend(result, data) {

            console.log(data.name_lower+ ' , '+data.limitSetting.multi.amb.isEnable)
            result.downline.push(data.name_lower);
            if (data.lock) {
                result.lock = true;
            }

            if (data.suspend) {
                result.suspend = true;
            }

            if (!data.limitSetting.sportsBook.isEnable) {
                result.sportBookEnable = false;
            }

            if (!data.limitSetting.step.step.isEnable) {
                result.stepEnable = false;
            }

            if (!data.limitSetting.step.parlay.isEnable) {
                result.parlayEnable = false;
            }

            if (!data.limitSetting.casino.sa.isEnable) {
                result.casinoSaEnable = false;
            }

            if (!data.limitSetting.casino.ag.isEnable) {
                result.casinoAgEnable = false;
            }

            if (!data.limitSetting.casino.sexy.isEnable) {
                result.casinoSexyEnable = false;
            }

            if (!data.limitSetting.casino.dg.isEnable) {
                result.casinoDgEnable = false;
            }

            if (!data.limitSetting.casino.pt.isEnable) {
                result.casinoPtEnable = false;
            }

            if (!data.limitSetting.game.slotXO.isEnable) {
                result.gameSlotEnable = false;
            }

            if (!data.limitSetting.multi.amb.isEnable) {
                result.gameCardEnable = false;
            }

            if (!data.limitSetting.lotto.amb.isEnable) {
                result.ambLottoEnable = false;
            }

            if (!data.limitSetting.lotto.pp.isEnable) {
                result.ambLottoPPEnable = false;
            }

            if (!data.limitSetting.lotto.laos.isEnable) {
                result.ambLottoLaosEnable = false;
            }

            if (!data.limitSetting.other.m2.isEnable) {
                result.otherM2Enable = false;
            }

            if (data.parentId) {
                return isLockOrSuspend(result, data.parentId);
            } else {
                result.downline.shift();
                result.downline = result.downline.reverse().join('|');
                return result;
            }
        }
    },
    rollbackCalculateAgentMemberBalance: function (betTransaction, ref, updateCallback) {

        console.log('rollbackCalculateAgentMemberBalance');

        const winLose = betTransaction.commission;

        let memberReceiveAmount;

        if (betTransaction.game === 'CASINO') {
            if (betTransaction.betResult === WIN) {
                memberReceiveAmount = betTransaction.memberCredit + (winLose.member.totalWinLoseCom * -1);
            } else if (betTransaction.betResult === DRAW) {
                memberReceiveAmount = betTransaction.memberCredit;
            } else {
                memberReceiveAmount = betTransaction.memberCredit + (winLose.member.totalWinLoseCom * -1);
            }
        } else {
            if (betTransaction.gameType === 'PARLAY' || betTransaction.gameType === 'MIX_STEP') {
                console.log('betResult : ', betTransaction.betResult)
                console.log('betTransaction.memberCredit : ', betTransaction.memberCredit)
                console.log('winLose.member.totalWinLoseCom : ', winLose.member.totalWinLoseCom)

                console.log('betResult : ', betTransaction.betResult)
                if (betTransaction.betResult === WIN) {
                    memberReceiveAmount = betTransaction.memberCredit + ((winLose.member.totalWinLoseCom * -1));
                } else {
                    memberReceiveAmount = betTransaction.memberCredit + winLose.member.totalWinLoseCom * -1;
                }
            } else {

                if (betTransaction.betResult === WIN || betTransaction.betResult === HALF_WIN) {
                    memberReceiveAmount = betTransaction.memberCredit + (winLose.member.totalWinLoseCom * -1);
                } else if (betTransaction.betResult === DRAW) {
                    memberReceiveAmount = betTransaction.memberCredit;
                } else {
                    memberReceiveAmount = betTransaction.memberCredit + (winLose.member.totalWinLoseCom * -1);
                }

            }
        }

        console.log('updateBalance : ', betTransaction.source)
        console.log('memberReceiveAmount : ', memberReceiveAmount);

        MemberService.updateBalance2(betTransaction.memberUsername, memberReceiveAmount, betTransaction.betId, 'Roll Back', ref, (err, response) => {
            if (err) {
                updateCallback(err, null);
            } else {
                updateCallback(null, response);
            }
        });

    },
    refundAgentMemberBalance: function (betTransaction, ref, updateCallback) {

        console.log('refundAgentMemberBalance');

        const winLose = betTransaction.commission;

        async.series([callback => {
            let updateWinLoseAgentList = [];


            async.each(updateWinLoseAgentList, (agentBalance, callbackWL) => {
                if (agentBalance.group && agentBalance.amount !== 0) {
                    AgentGroupModel.findOneAndUpdate({_id: agentBalance.group}, {$inc: {balance: agentBalance.amount}})
                        .exec(function (err, creditResponse) {
                            if (err) {
                                callbackWL(err);
                            }
                            else {
                                console.log('refund agent : ' + agentBalance.group + '  ,  balance : ' + agentBalance.amount);
                                callbackWL();
                            }
                        });
                } else {
                    callbackWL();
                }
            }, (err) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, '');
                }
            });

        }, callback => {

            let memberReceiveAmount = 0;
            let memberCredit = Math.abs(betTransaction.memberCredit);

            if (betTransaction.game === 'CASINO') {
                if (betTransaction.betResult === WIN) {
                    memberReceiveAmount = winLose.member.totalWinLoseCom * -1;
                } else if (betTransaction.betResult === DRAW) {
                    memberReceiveAmount = 0;
                } else {
                    memberReceiveAmount = winLose.member.totalWinLoseCom * -1;
                }
            } else {

                //TODO:
                if (betTransaction.gameType === 'PARLAY' || betTransaction.gameType === 'MIX_STEP') {
                    if (betTransaction.betResult === WIN) {
                        memberReceiveAmount = (winLose.member.totalWinLoseCom * -1);
                    } else {
                        memberReceiveAmount = winLose.member.totalWinLoseCom * -1;
                    }
                } else {

                    if (betTransaction.betResult === WIN || betTransaction.betResult === HALF_WIN) {
                        memberReceiveAmount = winLose.member.totalWinLoseCom * -1;
                    } else if (betTransaction.betResult === DRAW) {
                        memberReceiveAmount = 0;
                    } else {
                        memberReceiveAmount = winLose.member.totalWinLoseCom * -1;
                    }

                }
            }
            console.log('betResult : ', betTransaction.betResult)
            console.log('memberCredit : ', memberCredit)
            console.log('winLose.member.totalWinLoseCom : ', winLose.member.totalWinLoseCom)

            console.log('updateBalance : ', betTransaction.source)
            console.log('memberReceiveAmount : ', memberReceiveAmount);

            MemberService.updateBalance2(betTransaction.memberUsername, memberReceiveAmount, betTransaction.betId, 'REFUND', betTransaction.betId, (err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            });


        }], function (err, response) {
            if (err) {
                updateCallback(err, null);
            } else {
                updateCallback(null, response[1]);
            }
        });
    },
    prepareShareReceive: function (winLose, betResult, betTransaction) {


        let winLoseInfo;
        const amount = betTransaction.amount;

        if (betResult !== DRAW) {

            let memberCommission = validateCommission(betResult, betTransaction.commission.member.commission);

            let superAdminShare = betTransaction.commission.superAdmin.shareReceive;
            let superAdminCommission = validateCommission(betResult, betTransaction.commission.superAdmin.commission || 0);

            let companyShare = betTransaction.commission.company.shareReceive;
            let companyCommission = validateCommission(betResult, betTransaction.commission.company.commission || 0);

            let shareHolderShare = betTransaction.commission.shareHolder.shareReceive;
            let shareHolderCommission = validateCommission(betResult, betTransaction.commission.shareHolder.commission || 0);

            let seniorShare = betTransaction.commission.senior.shareReceive || 0;
            let seniorCommission = validateCommission(betResult, betTransaction.commission.senior.commission || 0);

            let masterAgentShare = betTransaction.commission.masterAgent.shareReceive || 0;
            let masterAgentCommission = validateCommission(betResult, betTransaction.commission.masterAgent.commission || 0);

            let agentShare = betTransaction.commission.agent.shareReceive || 0;
            let agentCommission = validateCommission(betResult, betTransaction.commission.agent.commission || 0);

            function validateCommission(betResult, commission) {
                return betResult === HALF_LOSE || betResult === HALF_WIN ? (commission / 2) : commission;
            }

            //SUPER ADMIN
            let superAdminTotalUpperShare = 0;
            let superAdminOwnAndChildShare = companyShare + shareHolderShare + seniorShare + masterAgentShare + agentShare;

            let superAdminReceiveCommission = superAdminCommission;
            let superAdminGiveCommission = companyCommission;

            // console.log('TOTAL W/L : ', winLose);
            let superAdminWinLose = roundTo(0 - (winLose * NumberUtils.convertPercentZeroDefault(betTransaction.commission.superAdmin.shareReceive)), 5);
            let superAdminFinalCom = calculateCommission(amount,
                superAdminShare,
                superAdminTotalUpperShare,
                superAdminReceiveCommission,
                superAdminOwnAndChildShare,
                superAdminGiveCommission);

            // console.log('superAdmin share : ', betTransaction.commission.superAdmin.shareReceive, ' %');
            // console.log('superAdmin com % : ', betTransaction.commission.superAdmin.commission, ' %');
            // console.log('superAdmin W/L : ', superAdminWinLose);
            // console.log('superAdmin COM : ', superAdminFinalCom);
            // console.log('superAdmin W/L + COM : ', superAdminWinLose + (superAdminFinalCom));
            // console.log('');
            // console.log("==================================");

            //COMPANY
            let companyTotalUpperShare = superAdminShare;
            let companyOwnAndChildShare = shareHolderShare + seniorShare + masterAgentShare + agentShare;

            let companyReceiveCommission = companyCommission;
            let companyGiveCommission = shareHolderCommission;

            // console.log('TOTAL W/L : ', winLose);
            let companyWinLose = roundTo(0 - (winLose * NumberUtils.convertPercentZeroDefault(betTransaction.commission.company.shareReceive)), 5);
            let companyFinalCom = calculateCommission(amount,
                companyShare,
                companyTotalUpperShare,
                companyReceiveCommission,
                companyOwnAndChildShare,
                companyGiveCommission);

            // console.log('company share : ', betTransaction.commission.company.shareReceive, ' %');
            // console.log('company com % : ', betTransaction.commission.company.commission, ' %');
            // console.log('company W/L : ', companyWinLose);
            // console.log('company COM : ', companyFinalCom);
            // console.log('company W/L + COM : ', companyWinLose + (companyFinalCom));
            // console.log('');
            // console.log("==================================");

            //SHARE HOLDER
            let shareHolderTotalUpperShare = superAdminShare + companyShare;
            let shareHolderOwnAndChildShare = seniorShare + masterAgentShare + agentShare;
            let shareHolderReceiveCommission = shareHolderCommission;
            let shareHolderGiveCommission = seniorCommission;

            let shareHolderWinLose = roundTo(0 - (winLose * NumberUtils.convertPercentZeroDefault(betTransaction.commission.shareHolder.shareReceive)), 5);
            let shareHolderFinalCom = calculateCommission(amount,
                shareHolderShare,
                shareHolderTotalUpperShare,
                shareHolderReceiveCommission,
                shareHolderOwnAndChildShare,
                shareHolderGiveCommission);

            // console.log('shareHolder share : ', betTransaction.commission.shareHolder.shareReceive, ' %');
            // console.log('shareHolder com % : ', betTransaction.commission.shareHolder.commission, ' %');
            // console.log('shareHolder W/L : ', shareHolderWinLose);
            // console.log('shareHolder COM : ', shareHolderFinalCom);
            // // console.log('shareHolder COM LOSE : ', calculateStep3(1000, shareHolderShare, shareHolderGiveCommission));
            // console.log('shareHolder W/L + COM : ', shareHolderWinLose + (shareHolderFinalCom));
            // console.log('');
            // console.log("==================================")
            //SENIOR
            let seniorTotalUpperShare = superAdminShare + companyShare + shareHolderShare;
            let seniorOwnAndChildShare = masterAgentShare + agentShare;
            let seniorReceiveCommission = seniorCommission;
            let seniorGiveCommission = betTransaction.memberParentGroup.type === 'SENIOR' ? memberCommission :
                betTransaction.commission.masterAgent.group ? masterAgentCommission : agentCommission;

            // console.log('betTransaction.memberParentGroup.type : ', betTransaction.memberParentGroup.type)
            let seniorWinLose = roundTo(0 - (winLose * NumberUtils.convertPercentZeroDefault(betTransaction.commission.senior.shareReceive)), 5);
            let seniorFinalCom = calculateCommission(amount,
                seniorShare,
                seniorTotalUpperShare,
                seniorReceiveCommission,
                seniorOwnAndChildShare,
                seniorGiveCommission);

            // console.log('senior share : ', betTransaction.commission.senior.shareReceive, ' %');
            // console.log('senior com % : ', betTransaction.commission.senior.commission, ' %');
            // console.log('senior W/L : ', seniorWinLose);
            // console.log('senior COM : ', seniorFinalCom);
            // // console.log('senior COM LOSE : ', calculateStep3(1000, seniorShare, seniorGiveCommission));
            // console.log('senior W/L + COM : ', seniorWinLose + seniorFinalCom);
            // console.log('');
            // console.log("==================================");

            //MASTER AGENT
            let masterAgentTotalUpperShare = superAdminShare + companyShare + shareHolderShare + seniorShare;
            let masterAgentOwnAndChildShare = agentShare;
            let masterAgentReceiveCommission = masterAgentCommission;
            let masterAgentGiveCommission = betTransaction.memberParentGroup.type === 'MASTER_AGENT' ? memberCommission :
                betTransaction.commission.masterAgent.group ? agentCommission : 0;

            let masterAgentWinLose = roundTo(0 - (winLose * NumberUtils.convertPercentZeroDefault(betTransaction.commission.masterAgent.shareReceive)), 5);
            let masterAgentFinalCom = calculateCommission(amount,
                masterAgentShare,
                masterAgentTotalUpperShare,
                masterAgentReceiveCommission,
                masterAgentOwnAndChildShare,
                masterAgentGiveCommission);

            // console.log('masterAgent share : ', betTransaction.commission.masterAgent.shareReceive, ' %');
            // console.log('masterAgent com % : ', betTransaction.commission.masterAgent.commission, ' %');
            // console.log('masterAgent W/L : ', masterAgentWinLose);
            // console.log('masterAgent COM : ', masterAgentFinalCom);
            // // console.log('masterAgent COM LOSE : ', calculateStep3(1000, masterAgentShare, masterAgentGiveCommission));
            // console.log('masterAgent W/L + COM : ', masterAgentWinLose + masterAgentFinalCom);
            // console.log('');
            // console.log("==================================");
            //AGENT
            let agentTotalUpperShare = superAdminShare + companyShare + shareHolderShare + seniorShare + masterAgentShare;

            let agentOwnAndChildShare = 100 - (superAdminShare + companyShare + shareHolderShare + seniorShare + masterAgentShare + agentShare);
            let agentReceiveCommission = agentCommission;
            let agentGiveCommission = betTransaction.memberParentGroup.type === 'AGENT' ? memberCommission : 0;
            let agentWinLose = roundTo(0 - (winLose * NumberUtils.convertPercentZeroDefault(betTransaction.commission.agent.shareReceive)), 5);
            let agentFinalCom = calculateCommission(amount,
                agentShare,
                agentTotalUpperShare,
                agentReceiveCommission,
                agentOwnAndChildShare,
                agentGiveCommission);

            // console.log('agent share : ', betTransaction.commission.agent.shareReceive, ' %');
            // console.log('agent com % : ', betTransaction.commission.agent.commission, ' %');
            // console.log('agent W/L : ', agentWinLose);
            // console.log('agent COM : ', agentFinalCom);
            // // console.log('agent COM LOSE : ', calculateStep3(1000, agentShare, agentGiveCommission));
            // console.log('agent W/L + COM : ', agentWinLose + agentFinalCom);
            // console.log('');
            // console.log("==================================");
            // console.log('member W/L : ', winLose);
            // console.log('member COM : ', Math.abs(winLose * NumberUtils.convertPercentZeroDefault(memberCommission)));
            // console.log('member W/L + COM : ', winLose + Math.abs(winLose * NumberUtils.convertPercentZeroDefault(betTransaction.commission.member.commission)));


            function calculateCommission(betPrice, ownShare, totalUpperShare, receiveCommission, ownAndChildShare, giveCommission) {

                let step1 = roundTo(Number.parseFloat(betPrice * (NumberUtils.convertPercentZeroDefault(totalUpperShare)) * (receiveCommission / 100)), 5);
                // console.log('step1 : ' + betPrice + ' x ' + NumberUtils.convertPercentZeroDefault(totalUpperShare) + ' x ' + (receiveCommission / 100) + ' = ' + step1);


                let step2 = roundTo(Number.parseFloat(betPrice * (NumberUtils.convertPercentZeroDefault(totalUpperShare)) * (giveCommission / 100)), 5);
                // console.log('step2 : ' + betPrice + ' x ' + NumberUtils.convertPercentZeroDefault(totalUpperShare) + ' x ' + (giveCommission / 100) + ' = ' + step2);

                // let step3 = betPrice * NumberUtils.convertPercentZeroDefault(ownShare) * NumberUtils.convertPercentZeroDefault(giveCommission);
                let step3 = roundTo(calculateStep3(betPrice, ownShare, giveCommission), 5);
                // console.log('step3 : ' + betPrice + ' x ' + NumberUtils.convertPercentZeroDefault(ownShare) + ' x ' + NumberUtils.convertPercentZeroDefault(giveCommission) + ' = ' + step3);


                // console.log('step1 : ' + step1 + ' , step2 : ' + step2 + ' , step3 : ' + step3)

                let result12 = roundTo((step1 - step2), 5);
                let finalResult = roundTo((result12 - step3), 5);
                return finalResult
            }

            function calculateStep3(betPrice, ownShare, giveCommission) {
                return betPrice * NumberUtils.convertPercentZeroDefault(ownShare) * NumberUtils.convertPercentZeroDefault(giveCommission);
            }


            winLoseInfo = {
                betId: betTransaction.betId,
                betAmount: amount,
                memberCredit: betTransaction.memberCredit,
                memberUsername: betTransaction.memberUsername,
                betResult: betResult,
                superAdmin: {
                    group: betTransaction.commission.superAdmin.group,
                    winLoseCom: roundTo(superAdminFinalCom, 5),
                    winLose: roundTo(superAdminWinLose, 5),
                    totalWinLoseCom: roundTo((superAdminWinLose + superAdminFinalCom), 5)
                },
                company: {
                    group: betTransaction.commission.company.group,
                    winLoseCom: roundTo(companyFinalCom, 5),
                    winLose: roundTo(companyWinLose, 5),
                    totalWinLoseCom: roundTo((companyWinLose + companyFinalCom), 5)
                },
                shareHolder: {
                    group: betTransaction.commission.shareHolder.group,
                    winLoseCom: roundTo(shareHolderFinalCom, 5),
                    winLose: roundTo(shareHolderWinLose, 5),
                    totalWinLoseCom: roundTo((shareHolderWinLose + shareHolderFinalCom), 5)
                },
                senior: {
                    group: betTransaction.commission.senior.group,
                    winLoseCom: roundTo(seniorFinalCom, 5),
                    winLose: roundTo(seniorWinLose, 5),
                    totalWinLoseCom: roundTo((seniorWinLose + seniorFinalCom), 5)
                },
                masterAgent: {
                    group: betTransaction.commission.masterAgent.group,
                    winLoseCom: roundTo(masterAgentFinalCom, 5),
                    winLose: roundTo(masterAgentWinLose, 5),
                    totalWinLoseCom: roundTo((masterAgentWinLose + masterAgentFinalCom), 5)
                },
                agent: {
                    group: betTransaction.commission.agent.group,
                    winLoseCom: roundTo(agentFinalCom, 5),
                    winLose: roundTo(agentWinLose, 5),
                    totalWinLoseCom: roundTo((agentWinLose + agentFinalCom), 5)
                },
                member: {
                    id: betTransaction.memberId,
                    winLoseCom: roundTo(Math.abs(amount * NumberUtils.convertPercentZeroDefault(memberCommission)), 5),
                    winLose: roundTo(winLose, 5),
                    totalWinLoseCom: roundTo((winLose + Math.abs(amount * NumberUtils.convertPercentZeroDefault(memberCommission))), 5)
                }
            };
        } else {
            winLoseInfo = {
                betId: betTransaction.betId,
                betAmount: amount,
                memberCredit: betTransaction.memberCredit,
                memberUsername: betTransaction.memberUsername,
                betResult: betResult,
                superAdmin: {
                    winLoseCom: 0,
                    winLose: 0,
                    totalWinLoseCom: 0
                },
                company: {
                    winLoseCom: 0,
                    winLose: 0,
                    totalWinLoseCom: 0
                },
                shareHolder: {
                    winLoseCom: 0,
                    winLose: 0,
                    totalWinLoseCom: 0
                },
                senior: {
                    winLoseCom: 0,
                    winLose: 0,
                    totalWinLoseCom: 0
                },
                masterAgent: {
                    winLoseCom: 0,
                    winLose: 0,
                    totalWinLoseCom: 0
                },
                agent: {
                    winLoseCom: 0,
                    winLose: 0,
                    totalWinLoseCom: 0
                },
                member: {
                    id: betTransaction.memberId,
                    winLoseCom: 0,
                    winLose: 0,
                    totalWinLoseCom: 0
                }
            };
        }
        return winLoseInfo;


    },
    prepareShareReceiveLottoLaos: function (betResult, betTransaction, winBetType) {
        // console.log('prepareShareReceiveLotto : result ', betResult)

        let winLoseInfo;
        const amount = betTransaction.amount;

        if (betResult !== DRAW) {

            let memberCommission = validateCommission(betResult, betTransaction.commission.member.commission);


            let superAdminPayout,companyPayout,shareHolderPayout,seniorPayout,masterAgentPayout,agentPayout,memberPayout;


            if (betTransaction.lotto.laos.betType == 'L_4TOP') {

                const payoutIndex = winBetType == 'L_4TOP' ? 0 : winBetType == 'L_4TOD' ? 1 : winBetType == 'L_3TOP' ? 2
                            : winBetType == 'L_3TOD' ? 3 : 4;

                console.log('payoutIndex : ',payoutIndex);
                superAdminPayout = betTransaction.lotto.laos.payoutLaos.superAdmin[payoutIndex];
                companyPayout = betTransaction.lotto.laos.payoutLaos.company[payoutIndex];
                shareHolderPayout = betTransaction.lotto.laos.payoutLaos.shareHolder[payoutIndex];
                seniorPayout = betTransaction.lotto.laos.payoutLaos.senior[payoutIndex];
                masterAgentPayout = betTransaction.lotto.laos.payoutLaos.masterAgent[payoutIndex];
                agentPayout = betTransaction.lotto.laos.payoutLaos.agent[payoutIndex];
                memberPayout = betTransaction.lotto.laos.payoutLaos.member[payoutIndex];
            }else {
                superAdminPayout = betTransaction.commission.superAdmin.payout;
                companyPayout = betTransaction.commission.company.payout;
                shareHolderPayout = betTransaction.commission.shareHolder.payout;
                seniorPayout = betTransaction.commission.senior.payout;
                masterAgentPayout = betTransaction.commission.masterAgent.payout;
                agentPayout = betTransaction.commission.agent.payout;
                memberPayout = betTransaction.commission.member.payout;
            }

            let superAdminShare = betTransaction.commission.superAdmin.shareReceive;
            let superAdminCommission = validateCommission(betResult, betTransaction.commission.superAdmin.commission || 0);

            let companyShare = betTransaction.commission.company.shareReceive;
            let companyCommission = validateCommission(betResult, betTransaction.commission.company.commission || 0);

            let shareHolderShare = betTransaction.commission.shareHolder.shareReceive;
            let shareHolderCommission = validateCommission(betResult, betTransaction.commission.shareHolder.commission || 0);

            let seniorShare = betTransaction.commission.senior.shareReceive || 0;
            let seniorCommission = validateCommission(betResult, betTransaction.commission.senior.commission || 0);

            let masterAgentShare = betTransaction.commission.masterAgent.shareReceive || 0;
            let masterAgentCommission = validateCommission(betResult, betTransaction.commission.masterAgent.commission || 0);

            let agentShare = betTransaction.commission.agent.shareReceive || 0;
            let agentCommission = validateCommission(betResult, betTransaction.commission.agent.commission || 0);

            function validateCommission(betResult, commission) {
                return betResult === HALF_LOSE || betResult === HALF_WIN ? (commission / 2) : commission;
            }

            //SUPER ADMIN
            let superAdminTotalUpperShare = 0;
            let superAdminOwnAndChildShare = companyShare + shareHolderShare + seniorShare + masterAgentShare + agentShare;


            let superAdminReceiveCommission = superAdminCommission;
            let superAdminGiveCommission = companyCommission;

            let superAdminReceivePayout = superAdminPayout;
            let superAdminGivePayout = companyPayout;

            let superAdminWinLose = calculatePayout(amount,
                superAdminShare,
                superAdminTotalUpperShare,
                superAdminReceivePayout,
                superAdminGivePayout,
                betResult);

            //roundTo(0 - (winLose * NumberUtils.convertPercentZeroDefault(betTransaction.commission.superAdmin.shareReceive)), 3);
            let superAdminFinalCom = calculateCommission(amount,
                superAdminShare,
                superAdminTotalUpperShare,
                superAdminReceiveCommission,
                superAdminOwnAndChildShare,
                superAdminGiveCommission);

            console.log('superAdmin share : ', betTransaction.commission.superAdmin.shareReceive, ' %');
            console.log('superAdmin com % : ', betTransaction.commission.superAdmin.commission, ' %');
            console.log('superAdmin W/L : ', superAdminWinLose);
            console.log('superAdmin COM : ', superAdminFinalCom);
            console.log('superAdmin W/L + COM : ', superAdminWinLose + (superAdminFinalCom));
            console.log('');
            console.log("==================================");

            //COMPANY
            let companyTotalUpperShare = superAdminShare;
            let companyOwnAndChildShare = shareHolderShare + seniorShare + masterAgentShare + agentShare;

            let companyReceiveCommission = companyCommission;
            let companyGiveCommission = shareHolderCommission;

            let companyReceivePayout = companyPayout;
            let companyGivePayout = shareHolderPayout;

            let companyWinLose = calculatePayout(amount,
                companyShare,
                companyTotalUpperShare,
                companyReceivePayout,
                companyGivePayout,
                betResult);

            //roundTo(0 - (winLose * NumberUtils.convertPercentZeroDefault(betTransaction.commission.company.shareReceive)), 3);
            let companyFinalCom = calculateCommission(amount,
                companyShare,
                companyTotalUpperShare,
                companyReceiveCommission,
                companyOwnAndChildShare,
                companyGiveCommission);

            console.log('company share : ', betTransaction.commission.company.shareReceive, ' %');
            console.log('company com % : ', betTransaction.commission.company.commission, ' %');
            console.log('company W/L : ', companyWinLose);
            console.log('company COM : ', companyFinalCom);
            console.log('company W/L + COM : ', companyWinLose + (companyFinalCom));
            console.log('');
            console.log("==================================");
            //SHARE HOLDER
            let shareHolderTotalUpperShare = superAdminShare + companyShare;
            let shareHolderOwnAndChildShare = seniorShare + masterAgentShare + agentShare;

            let shareHolderReceiveCommission = shareHolderCommission;
            let shareHolderGiveCommission = seniorCommission;


            let shareHolderReceivePayout = shareHolderPayout;
            let shareHolderGivePayout = seniorPayout;

            let shareHolderWinLose = calculatePayout(amount,
                shareHolderShare,
                shareHolderTotalUpperShare,
                shareHolderReceivePayout,
                shareHolderGivePayout,
                betResult);

            // let shareHolderWinLose = roundTo(0 - (winLose * NumberUtils.convertPercentZeroDefault(betTransaction.commission.shareHolder.shareReceive)), 3);
            let shareHolderFinalCom = calculateCommission(amount,
                shareHolderShare,
                shareHolderTotalUpperShare,
                shareHolderReceiveCommission,
                shareHolderOwnAndChildShare,
                shareHolderGiveCommission);

            console.log('shareHolder share : ', betTransaction.commission.shareHolder.shareReceive, ' %');
            console.log('shareHolder com % : ', betTransaction.commission.shareHolder.commission, ' %');
            console.log('shareHolder W/L : ', shareHolderWinLose);
            console.log('shareHolder COM : ', shareHolderFinalCom);
            // console.log('shareHolder COM LOSE : ', calculateStep3(1000, shareHolderShare, shareHolderGiveCommission));
            console.log('shareHolder W/L + COM : ', shareHolderWinLose + (shareHolderFinalCom));
            console.log('');
            console.log("==================================");

            //SENIOR
            let seniorTotalUpperShare = superAdminShare + companyShare + shareHolderShare;
            let seniorOwnAndChildShare = masterAgentShare + agentShare;

            let seniorReceiveCommission = seniorCommission;
            let seniorGiveCommission = betTransaction.memberParentGroup.type === 'SENIOR' ? memberCommission :
                betTransaction.commission.masterAgent.group ? masterAgentCommission : agentCommission;


            let seniorReceivePayout = seniorPayout;
            let seniorGivePayout = betTransaction.memberParentGroup.type === 'SENIOR' ? memberPayout :
                betTransaction.commission.masterAgent.group ? masterAgentPayout : agentPayout;

            let seniorWinLose = calculatePayout(amount,
                seniorShare,
                seniorTotalUpperShare,
                seniorReceivePayout,
                seniorGivePayout,
                betResult);

            // let seniorWinLose = roundTo(0 - (winLose * NumberUtils.convertPercentZeroDefault(betTransaction.commission.senior.shareReceive)), 3);
            let seniorFinalCom = calculateCommission(amount,
                seniorShare,
                seniorTotalUpperShare,
                seniorReceiveCommission,
                seniorOwnAndChildShare,
                seniorGiveCommission);

            console.log('senior share : ', betTransaction.commission.senior.shareReceive, ' %');
            console.log('senior com % : ', betTransaction.commission.senior.commission, ' %');
            console.log('senior W/L : ', seniorWinLose);
            console.log('senior COM : ', seniorFinalCom);
            // console.log('senior COM LOSE : ', calculateStep3(1000, seniorShare, seniorGiveCommission));
            console.log('senior W/L + COM : ', seniorWinLose + seniorFinalCom);
            console.log('');
            console.log("==================================");


            //MASTER AGENT
            let masterAgentTotalUpperShare = superAdminShare + companyShare + shareHolderShare + seniorShare;
            let masterAgentOwnAndChildShare = agentShare;

            let masterAgentReceiveCommission = masterAgentCommission;
            let masterAgentGiveCommission = betTransaction.memberParentGroup.type === 'MASTER_AGENT' ? memberCommission :
                betTransaction.commission.masterAgent.group ? agentCommission : 0;

            let masterAgentReceivePayout = (masterAgentPayout || 0);

            let masterAgentGivePayout = (betTransaction.memberParentGroup.type === 'MASTER_AGENT' ? memberPayout :
                betTransaction.commission.masterAgent.group ? agentPayout : 0);

            let masterAgentWinLose = !betTransaction.commission.masterAgent.group ? 0 : calculatePayout(amount,
                    masterAgentShare,
                    masterAgentTotalUpperShare,
                    masterAgentReceivePayout,
                    masterAgentGivePayout,
                    betResult);

            // let masterAgentWinLose = roundTo(0 - (winLose * NumberUtils.convertPercentZeroDefault(betTransaction.commission.masterAgent.shareReceive)), 3);
            let masterAgentFinalCom = !betTransaction.commission.masterAgent.group ? 0 : calculateCommission(amount,
                    masterAgentShare,
                    masterAgentTotalUpperShare,
                    masterAgentReceiveCommission,
                    masterAgentOwnAndChildShare,
                    masterAgentGiveCommission);

            console.log('masterAgent share : ', betTransaction.commission.masterAgent.shareReceive, ' %');
            console.log('masterAgent com % : ', betTransaction.commission.masterAgent.commission, ' %');
            console.log('masterAgent W/L : ', masterAgentWinLose);
            console.log('masterAgent COM : ', masterAgentFinalCom);
            // console.log('masterAgent COM LOSE : ', calculateStep3(1000, masterAgentShare, masterAgentGiveCommission));
            console.log('masterAgent W/L + COM : ', masterAgentWinLose + masterAgentFinalCom);
            console.log('');
            console.log("==================================");


            //AGENT
            let agentTotalUpperShare = superAdminShare + companyShare + shareHolderShare + seniorShare + masterAgentShare;

            let agentOwnAndChildShare = 100 - (superAdminShare + companyShare + shareHolderShare + seniorShare + masterAgentShare + agentShare);

            let agentReceiveCommission = agentCommission;
            let agentGiveCommission = betTransaction.memberParentGroup.type === 'AGENT' ? memberCommission : 0;

            let agentReceivePayout = (agentPayout || 0);
            let agentGivePayout = (betTransaction.memberParentGroup.type === 'AGENT' ? memberPayout : 0);

            // console.log('agent : ', agentGivePayout);


            let agentWinLose = !betTransaction.commission.agent.group ? 0 : calculatePayout(amount,
                    agentShare,
                    agentTotalUpperShare,
                    agentReceivePayout,
                    agentGivePayout,
                    betResult);


            // let agentWinLose = roundTo(0 - (winLose * NumberUtils.convertPercentZeroDefault(betTransaction.commission.agent.shareReceive)), 3);
            let agentFinalCom = !betTransaction.commission.agent.group ? 0 : calculateCommission(amount,
                    agentShare,
                    agentTotalUpperShare,
                    agentReceiveCommission,
                    agentOwnAndChildShare,
                    agentGiveCommission);

            console.log('agent share : ', betTransaction.commission.agent.shareReceive, ' %');
            console.log('agent com % : ', betTransaction.commission.agent.commission, ' %');
            console.log('agent W/L : ', agentWinLose);
            console.log('agent COM : ', agentFinalCom);
            // console.log('agent COM LOSE : ', calculateStep3(1000, agentShare, agentGiveCommission));
            console.log('agent W/L + COM : ', agentWinLose + agentFinalCom);
            console.log('');


            let memberWinLoseWL, memberCommissionWL;
            if (betResult === WIN) {
                memberWinLoseWL = roundTo(amount * memberPayout, 3) - (( amount - roundTo(amount * NumberUtils.convertPercentZeroDefault(memberCommission), 3)));

                memberCommissionWL = roundTo(Math.abs(amount * NumberUtils.convertPercentZeroDefault(memberCommission)), 3);
                // -discount
                memberWinLoseWL -= memberCommissionWL;
            } else {
                memberWinLoseWL = amount * -1;

                memberCommissionWL = roundTo(Math.abs(amount * NumberUtils.convertPercentZeroDefault(memberCommission)), 3);

            }

            console.log("==================================");
            console.log('member W/L : ', memberWinLoseWL);
            console.log('member COM : ', memberCommissionWL);
            console.log('member W/L + COM : ', memberWinLoseWL + memberCommissionWL);


            function calculateCommission(betPrice, ownShare, totalUpperShare, receiveCommission, ownAndChildShare, giveCommission) {
                // console.log('calculateCommission')
                let step1 = roundTo(Number.parseFloat(betPrice * (NumberUtils.convertPercentZeroDefault(totalUpperShare)) * (receiveCommission / 100)), 3);
                // console.log('step1 : ' + betPrice + ' x ' + NumberUtils.convertPercentZeroDefault(totalUpperShare) + ' x ' + (receiveCommission / 100) + ' = ' + step1);


                let step2 = roundTo(Number.parseFloat(betPrice * (NumberUtils.convertPercentZeroDefault(totalUpperShare)) * (giveCommission / 100)), 3);
                // console.log('step2 : ' + betPrice + ' x ' + NumberUtils.convertPercentZeroDefault(totalUpperShare) + ' x ' + (giveCommission / 100) + ' = ' + step2);

                // let step3 = betPrice * NumberUtils.convertPercentZeroDefault(ownShare) * NumberUtils.convertPercentZeroDefault(giveCommission);
                let step3 = roundTo(calculateStep3(betPrice, ownShare, giveCommission), 3);
                // console.log('step3 : ' + betPrice + ' x ' + NumberUtils.convertPercentZeroDefault(ownShare) + ' x ' + NumberUtils.convertPercentZeroDefault(giveCommission) + ' = ' + step3);


                // console.log('step1 : ' + step1 + ' , step2 : ' + step2 + ' , step3 : ' + step3)

                let result12 = roundTo((step1 - step2), 3);
                let finalResult = roundTo((result12 - step3), 3);
                return finalResult
            }

            function calculateStep3(betPrice, ownShare, giveCommission) {
                return betPrice * NumberUtils.convertPercentZeroDefault(ownShare) * NumberUtils.convertPercentZeroDefault(giveCommission);
            }


            function calculatePayout(betPrice, ownShare, totalUpperShare, receivePayout, givePayout, betResult) {
                // console.log('calculateWLPayout')


                if (betResult === LOSE) {
                    betPrice *= -1;
                    givePayout = 1;
                    receivePayout = 1;
                }
                let step1 = roundTo(Number.parseFloat(betPrice * (NumberUtils.convertPercentZeroDefault(totalUpperShare)) * receivePayout), 3);
                // console.log('step1 : ' + betPrice + ' x ' + NumberUtils.convertPercentZeroDefault(totalUpperShare) + ' x ' + (receivePayout) + ' = ' + step1);


                let step2 = roundTo(Number.parseFloat(betPrice * (NumberUtils.convertPercentZeroDefault(totalUpperShare)) * (givePayout)), 3);
                // console.log('step2 : ' + betPrice + ' x ' + NumberUtils.convertPercentZeroDefault(totalUpperShare) + ' x ' + (givePayout) + ' = ' + step2);

                // let step3 = betPrice * NumberUtils.convertPercentZeroDefault(ownShare) * NumberUtils.convertPercentZeroDefault(giveCommission);
                let step3 = roundTo(calculateStep3Payout(betPrice, ownShare, givePayout), 3);
                // console.log('step3 : ' + betPrice + ' x ' + NumberUtils.convertPercentZeroDefault(ownShare) + ' : ' + ownShare + ' x ' + givePayout + ' = ' + step3);


                // console.log('step1 : ' + step1 + ' , step2 : ' + step2 + ' , step3 : ' + step3)

                let result12 = roundTo((step1 - step2), 3);
                let finalResult = roundTo((result12 - step3), 3);

                //-cost
                if (betResult === WIN) {
                    finalResult += roundTo(amount * NumberUtils.convertPercentZeroDefault(ownShare), 3);
                }
                return finalResult
            }

            function calculateStep3Payout(betPrice, ownShare, givePayout) {
                return betPrice * NumberUtils.convertPercentZeroDefault(ownShare) * givePayout;
            }


            winLoseInfo = {
                betId: betTransaction.betId,
                betAmount: amount,
                memberCredit: betTransaction.memberCredit,
                betResult: betResult,
                superAdmin: {
                    group: betTransaction.commission.superAdmin.group,
                    winLoseCom: roundTo(superAdminFinalCom, 3),
                    winLose: roundTo(superAdminWinLose, 3),
                    totalWinLoseCom: roundTo((superAdminWinLose + superAdminFinalCom), 3)
                },
                company: {
                    group: betTransaction.commission.company.group,
                    winLoseCom: roundTo(companyFinalCom, 3),
                    winLose: roundTo(companyWinLose, 3),
                    totalWinLoseCom: roundTo((companyWinLose + companyFinalCom), 3)
                },
                shareHolder: {
                    group: betTransaction.commission.shareHolder.group,
                    winLoseCom: roundTo(shareHolderFinalCom, 3),
                    winLose: roundTo(shareHolderWinLose, 3),
                    totalWinLoseCom: roundTo((shareHolderWinLose + shareHolderFinalCom), 3)
                },
                senior: {
                    group: betTransaction.commission.senior.group,
                    winLoseCom: roundTo(seniorFinalCom, 3),
                    winLose: roundTo(seniorWinLose, 3),
                    totalWinLoseCom: roundTo((seniorWinLose + seniorFinalCom), 3)
                },
                masterAgent: {
                    group: betTransaction.commission.masterAgent.group,
                    winLoseCom: roundTo(masterAgentFinalCom, 3),
                    winLose: roundTo(masterAgentWinLose, 3),
                    totalWinLoseCom: roundTo((masterAgentWinLose + masterAgentFinalCom), 3)
                },
                agent: {
                    group: betTransaction.commission.agent.group,
                    winLoseCom: roundTo(agentFinalCom, 3),
                    winLose: roundTo(agentWinLose, 3),
                    totalWinLoseCom: roundTo((agentWinLose + agentFinalCom), 3)
                },
                member: {
                    id: betTransaction.memberId,
                    winLoseCom: memberCommissionWL,
                    winLose: memberWinLoseWL,
                    totalWinLoseCom: roundTo((memberWinLoseWL + memberCommissionWL), 3)
                }
            };
        } else {
            winLoseInfo = {
                betId: betTransaction.betId,
                betAmount: amount,
                memberCredit: betTransaction.memberCredit,
                betResult: betResult,
                superAdmin: {
                    winLoseCom: 0,
                    winLose: 0,
                    totalWinLoseCom: 0
                },
                company: {
                    winLoseCom: 0,
                    winLose: 0,
                    totalWinLoseCom: 0
                },
                shareHolder: {
                    winLoseCom: 0,
                    winLose: 0,
                    totalWinLoseCom: 0
                },
                senior: {
                    winLoseCom: 0,
                    winLose: 0,
                    totalWinLoseCom: 0
                },
                masterAgent: {
                    winLoseCom: 0,
                    winLose: 0,
                    totalWinLoseCom: 0
                },
                agent: {
                    winLoseCom: 0,
                    winLose: 0,
                    totalWinLoseCom: 0
                },
                member: {
                    id: betTransaction.memberId,
                    winLoseCom: 0,
                    winLose: 0,
                    totalWinLoseCom: 0
                }
            };
        }
        return winLoseInfo;

    },
    prepareShareReceiveLotto: function (betResult, betTransaction) {
        // console.log('prepareShareReceiveLotto : result ', betResult)

        let winLoseInfo;
        const amount = betTransaction.amount;

        if (betResult !== DRAW) {

            let memberCommission = validateCommission(betResult, betTransaction.commission.member.commission);
            let memberPayout = betTransaction.commission.member.payout;

            let superAdminShare = betTransaction.commission.superAdmin.shareReceive;
            let superAdminPayout = betTransaction.commission.superAdmin.payout;
            let superAdminCommission = validateCommission(betResult, betTransaction.commission.superAdmin.commission || 0);

            let companyShare = betTransaction.commission.company.shareReceive;
            let companyPayout = betTransaction.commission.company.payout;
            let companyCommission = validateCommission(betResult, betTransaction.commission.company.commission || 0);

            let shareHolderShare = betTransaction.commission.shareHolder.shareReceive;
            let shareHolderPayout = betTransaction.commission.shareHolder.payout;
            let shareHolderCommission = validateCommission(betResult, betTransaction.commission.shareHolder.commission || 0);

            let seniorShare = betTransaction.commission.senior.shareReceive || 0;
            let seniorPayout = betTransaction.commission.senior.payout;
            let seniorCommission = validateCommission(betResult, betTransaction.commission.senior.commission || 0);

            let masterAgentShare = betTransaction.commission.masterAgent.shareReceive || 0;
            let masterAgentPayout = betTransaction.commission.masterAgent.payout;
            let masterAgentCommission = validateCommission(betResult, betTransaction.commission.masterAgent.commission || 0);

            let agentShare = betTransaction.commission.agent.shareReceive || 0;
            let agentPayout = betTransaction.commission.agent.payout;
            let agentCommission = validateCommission(betResult, betTransaction.commission.agent.commission || 0);

            function validateCommission(betResult, commission) {
                return betResult === HALF_LOSE || betResult === HALF_WIN ? (commission / 2) : commission;
            }

            //SUPER ADMIN
            let superAdminTotalUpperShare = 0;
            let superAdminOwnAndChildShare = companyShare + shareHolderShare + seniorShare + masterAgentShare + agentShare;


            let superAdminReceiveCommission = superAdminCommission;
            let superAdminGiveCommission = companyCommission;

            let superAdminReceivePayout = superAdminPayout;
            let superAdminGivePayout = companyPayout;

            let superAdminWinLose = calculatePayout(amount,
                superAdminShare,
                superAdminTotalUpperShare,
                superAdminReceivePayout,
                superAdminGivePayout,
                betResult);

            //roundTo(0 - (winLose * NumberUtils.convertPercentZeroDefault(betTransaction.commission.superAdmin.shareReceive)), 3);
            let superAdminFinalCom = calculateCommission(amount,
                superAdminShare,
                superAdminTotalUpperShare,
                superAdminReceiveCommission,
                superAdminOwnAndChildShare,
                superAdminGiveCommission);

            // console.log('superAdmin share : ', betTransaction.commission.superAdmin.shareReceive, ' %');
            // console.log('superAdmin com % : ', betTransaction.commission.superAdmin.commission, ' %');
            // console.log('superAdmin W/L : ', superAdminWinLose);
            // console.log('superAdmin COM : ', superAdminFinalCom);
            // console.log('superAdmin W/L + COM : ', superAdminWinLose + (superAdminFinalCom));
            // console.log('');
            // console.log("==================================");

            //COMPANY
            let companyTotalUpperShare = superAdminShare;
            let companyOwnAndChildShare = shareHolderShare + seniorShare + masterAgentShare + agentShare;

            let companyReceiveCommission = companyCommission;
            let companyGiveCommission = shareHolderCommission;

            let companyReceivePayout = companyPayout;
            let companyGivePayout = shareHolderPayout;

            let companyWinLose = calculatePayout(amount,
                companyShare,
                companyTotalUpperShare,
                companyReceivePayout,
                companyGivePayout,
                betResult);

            //roundTo(0 - (winLose * NumberUtils.convertPercentZeroDefault(betTransaction.commission.company.shareReceive)), 3);
            let companyFinalCom = calculateCommission(amount,
                companyShare,
                companyTotalUpperShare,
                companyReceiveCommission,
                companyOwnAndChildShare,
                companyGiveCommission);

            // console.log('company share : ', betTransaction.commission.company.shareReceive, ' %');
            // console.log('company com % : ', betTransaction.commission.company.commission, ' %');
            // console.log('company W/L : ', companyWinLose);
            // console.log('company COM : ', companyFinalCom);
            // console.log('company W/L + COM : ', companyWinLose + (companyFinalCom));
            // console.log('');
            // console.log("==================================");
            //SHARE HOLDER
            let shareHolderTotalUpperShare = superAdminShare + companyShare;
            let shareHolderOwnAndChildShare = seniorShare + masterAgentShare + agentShare;

            let shareHolderReceiveCommission = shareHolderCommission;
            let shareHolderGiveCommission = seniorCommission;


            let shareHolderReceivePayout = shareHolderPayout;
            let shareHolderGivePayout = seniorPayout;

            let shareHolderWinLose = calculatePayout(amount,
                shareHolderShare,
                shareHolderTotalUpperShare,
                shareHolderReceivePayout,
                shareHolderGivePayout,
                betResult);

            // let shareHolderWinLose = roundTo(0 - (winLose * NumberUtils.convertPercentZeroDefault(betTransaction.commission.shareHolder.shareReceive)), 3);
            let shareHolderFinalCom = calculateCommission(amount,
                shareHolderShare,
                shareHolderTotalUpperShare,
                shareHolderReceiveCommission,
                shareHolderOwnAndChildShare,
                shareHolderGiveCommission);

            // console.log('shareHolder share : ', betTransaction.commission.shareHolder.shareReceive, ' %');
            // console.log('shareHolder com % : ', betTransaction.commission.shareHolder.commission, ' %');
            // console.log('shareHolder W/L : ', shareHolderWinLose);
            // console.log('shareHolder COM : ', shareHolderFinalCom);
            // // console.log('shareHolder COM LOSE : ', calculateStep3(1000, shareHolderShare, shareHolderGiveCommission));
            // console.log('shareHolder W/L + COM : ', shareHolderWinLose + (shareHolderFinalCom));
            // console.log('');
            // console.log("==================================")
            //SENIOR
            let seniorTotalUpperShare = superAdminShare + companyShare + shareHolderShare;
            let seniorOwnAndChildShare = masterAgentShare + agentShare;

            let seniorReceiveCommission = seniorCommission;
            let seniorGiveCommission = betTransaction.memberParentGroup.type === 'SENIOR' ? memberCommission :
                betTransaction.commission.masterAgent.group ? masterAgentCommission : agentCommission;


            let seniorReceivePayout = seniorPayout;
            let seniorGivePayout = betTransaction.memberParentGroup.type === 'SENIOR' ? memberPayout :
                betTransaction.commission.masterAgent.group ? masterAgentPayout : agentPayout;

            let seniorWinLose = calculatePayout(amount,
                seniorShare,
                seniorTotalUpperShare,
                seniorReceivePayout,
                seniorGivePayout,
                betResult);

            // let seniorWinLose = roundTo(0 - (winLose * NumberUtils.convertPercentZeroDefault(betTransaction.commission.senior.shareReceive)), 3);
            let seniorFinalCom = calculateCommission(amount,
                seniorShare,
                seniorTotalUpperShare,
                seniorReceiveCommission,
                seniorOwnAndChildShare,
                seniorGiveCommission);

            // console.log('senior share : ', betTransaction.commission.senior.shareReceive, ' %');
            // console.log('senior com % : ', betTransaction.commission.senior.commission, ' %');
            // console.log('senior W/L : ', seniorWinLose);
            // console.log('senior COM : ', seniorFinalCom);
            // // console.log('senior COM LOSE : ', calculateStep3(1000, seniorShare, seniorGiveCommission));
            // console.log('senior W/L + COM : ', seniorWinLose + seniorFinalCom);
            // console.log('');
            // console.log("==================================");


            //MASTER AGENT
            let masterAgentTotalUpperShare = superAdminShare + companyShare + shareHolderShare + seniorShare;
            let masterAgentOwnAndChildShare = agentShare;

            let masterAgentReceiveCommission = masterAgentCommission;
            let masterAgentGiveCommission = betTransaction.memberParentGroup.type === 'MASTER_AGENT' ? memberCommission :
                betTransaction.commission.masterAgent.group ? agentCommission : 0;

            let masterAgentReceivePayout = (masterAgentPayout || 0);

            let masterAgentGivePayout = (betTransaction.memberParentGroup.type === 'MASTER_AGENT' ? memberPayout :
                betTransaction.commission.masterAgent.group ? agentPayout : 0);

            let masterAgentWinLose = !betTransaction.commission.masterAgent.group ? 0 : calculatePayout(amount,
                    masterAgentShare,
                    masterAgentTotalUpperShare,
                    masterAgentReceivePayout,
                    masterAgentGivePayout,
                    betResult);

            // let masterAgentWinLose = roundTo(0 - (winLose * NumberUtils.convertPercentZeroDefault(betTransaction.commission.masterAgent.shareReceive)), 3);
            let masterAgentFinalCom = !betTransaction.commission.masterAgent.group ? 0 : calculateCommission(amount,
                    masterAgentShare,
                    masterAgentTotalUpperShare,
                    masterAgentReceiveCommission,
                    masterAgentOwnAndChildShare,
                    masterAgentGiveCommission);

            // console.log('masterAgent share : ', betTransaction.commission.masterAgent.shareReceive, ' %');
            // console.log('masterAgent com % : ', betTransaction.commission.masterAgent.commission, ' %');
            // console.log('masterAgent W/L : ', masterAgentWinLose);
            // console.log('masterAgent COM : ', masterAgentFinalCom);
            // // console.log('masterAgent COM LOSE : ', calculateStep3(1000, masterAgentShare, masterAgentGiveCommission));
            // console.log('masterAgent W/L + COM : ', masterAgentWinLose + masterAgentFinalCom);
            // console.log('');
            // console.log("==================================");


            //AGENT
            let agentTotalUpperShare = superAdminShare + companyShare + shareHolderShare + seniorShare + masterAgentShare;

            let agentOwnAndChildShare = 100 - (superAdminShare + companyShare + shareHolderShare + seniorShare + masterAgentShare + agentShare);

            let agentReceiveCommission = agentCommission;
            let agentGiveCommission = betTransaction.memberParentGroup.type === 'AGENT' ? memberCommission : 0;

            let agentReceivePayout = (agentPayout || 0);
            let agentGivePayout = (betTransaction.memberParentGroup.type === 'AGENT' ? memberPayout : 0);

            // console.log('agent : ', agentGivePayout);


            let agentWinLose = !betTransaction.commission.agent.group ? 0 : calculatePayout(amount,
                    agentShare,
                    agentTotalUpperShare,
                    agentReceivePayout,
                    agentGivePayout,
                    betResult);


            // let agentWinLose = roundTo(0 - (winLose * NumberUtils.convertPercentZeroDefault(betTransaction.commission.agent.shareReceive)), 3);
            let agentFinalCom = !betTransaction.commission.agent.group ? 0 : calculateCommission(amount,
                    agentShare,
                    agentTotalUpperShare,
                    agentReceiveCommission,
                    agentOwnAndChildShare,
                    agentGiveCommission);

            // console.log('agent share : ', betTransaction.commission.agent.shareReceive, ' %');
            // console.log('agent com % : ', betTransaction.commission.agent.commission, ' %');
            // console.log('agent W/L : ', agentWinLose);
            // console.log('agent COM : ', agentFinalCom);
            // // console.log('agent COM LOSE : ', calculateStep3(1000, agentShare, agentGiveCommission));
            // console.log('agent W/L + COM : ', agentWinLose + agentFinalCom);
            // console.log('');


            let memberWinLoseWL, memberCommissionWL;
            if (betResult === WIN) {
                memberWinLoseWL = roundTo(amount * betTransaction.commission.member.payout, 3) - (( amount - roundTo(amount * NumberUtils.convertPercentZeroDefault(memberCommission), 3)));

                memberCommissionWL = roundTo(Math.abs(amount * NumberUtils.convertPercentZeroDefault(memberCommission)), 3);
                // -discount
                memberWinLoseWL -= memberCommissionWL;
            } else {
                memberWinLoseWL = amount * -1;

                memberCommissionWL = roundTo(Math.abs(amount * NumberUtils.convertPercentZeroDefault(memberCommission)), 3);

            }

            // console.log("==================================");
            // console.log('member W/L : ', memberWinLoseWL);
            // console.log('member COM : ', memberCommissionWL);
            // console.log('member W/L + COM : ', memberWinLoseWL + memberCommissionWL);


            function calculateCommission(betPrice, ownShare, totalUpperShare, receiveCommission, ownAndChildShare, giveCommission) {
                // console.log('calculateCommission')
                let step1 = roundTo(Number.parseFloat(betPrice * (NumberUtils.convertPercentZeroDefault(totalUpperShare)) * (receiveCommission / 100)), 3);
                // console.log('step1 : ' + betPrice + ' x ' + NumberUtils.convertPercentZeroDefault(totalUpperShare) + ' x ' + (receiveCommission / 100) + ' = ' + step1);


                let step2 = roundTo(Number.parseFloat(betPrice * (NumberUtils.convertPercentZeroDefault(totalUpperShare)) * (giveCommission / 100)), 3);
                // console.log('step2 : ' + betPrice + ' x ' + NumberUtils.convertPercentZeroDefault(totalUpperShare) + ' x ' + (giveCommission / 100) + ' = ' + step2);

                // let step3 = betPrice * NumberUtils.convertPercentZeroDefault(ownShare) * NumberUtils.convertPercentZeroDefault(giveCommission);
                let step3 = roundTo(calculateStep3(betPrice, ownShare, giveCommission), 3);
                // console.log('step3 : ' + betPrice + ' x ' + NumberUtils.convertPercentZeroDefault(ownShare) + ' x ' + NumberUtils.convertPercentZeroDefault(giveCommission) + ' = ' + step3);


                // console.log('step1 : ' + step1 + ' , step2 : ' + step2 + ' , step3 : ' + step3)

                let result12 = roundTo((step1 - step2), 3);
                let finalResult = roundTo((result12 - step3), 3);
                return finalResult
            }

            function calculateStep3(betPrice, ownShare, giveCommission) {
                return betPrice * NumberUtils.convertPercentZeroDefault(ownShare) * NumberUtils.convertPercentZeroDefault(giveCommission);
            }


            function calculatePayout(betPrice, ownShare, totalUpperShare, receivePayout, givePayout, betResult) {
                // console.log('calculateWLPayout')


                if (betResult === LOSE) {
                    betPrice *= -1;
                    givePayout = 1;
                    receivePayout = 1;
                }
                let step1 = roundTo(Number.parseFloat(betPrice * (NumberUtils.convertPercentZeroDefault(totalUpperShare)) * receivePayout), 3);
                // console.log('step1 : ' + betPrice + ' x ' + NumberUtils.convertPercentZeroDefault(totalUpperShare) + ' x ' + (receivePayout) + ' = ' + step1);


                let step2 = roundTo(Number.parseFloat(betPrice * (NumberUtils.convertPercentZeroDefault(totalUpperShare)) * (givePayout)), 3);
                // console.log('step2 : ' + betPrice + ' x ' + NumberUtils.convertPercentZeroDefault(totalUpperShare) + ' x ' + (givePayout) + ' = ' + step2);

                // let step3 = betPrice * NumberUtils.convertPercentZeroDefault(ownShare) * NumberUtils.convertPercentZeroDefault(giveCommission);
                let step3 = roundTo(calculateStep3Payout(betPrice, ownShare, givePayout), 3);
                // console.log('step3 : ' + betPrice + ' x ' + NumberUtils.convertPercentZeroDefault(ownShare) + ' : ' + ownShare + ' x ' + givePayout + ' = ' + step3);


                // console.log('step1 : ' + step1 + ' , step2 : ' + step2 + ' , step3 : ' + step3)

                let result12 = roundTo((step1 - step2), 3);
                let finalResult = roundTo((result12 - step3), 3);

                //-cost
                if (betResult === WIN) {
                    finalResult += roundTo(amount * NumberUtils.convertPercentZeroDefault(ownShare), 3);
                }
                return finalResult
            }

            function calculateStep3Payout(betPrice, ownShare, givePayout) {
                return betPrice * NumberUtils.convertPercentZeroDefault(ownShare) * givePayout;
            }


            winLoseInfo = {
                betId: betTransaction.betId,
                betAmount: amount,
                memberCredit: betTransaction.memberCredit,
                betResult: betResult,
                superAdmin: {
                    group: betTransaction.commission.superAdmin.group,
                    winLoseCom: roundTo(superAdminFinalCom, 3),
                    winLose: roundTo(superAdminWinLose, 3),
                    totalWinLoseCom: roundTo((superAdminWinLose + superAdminFinalCom), 3)
                },
                company: {
                    group: betTransaction.commission.company.group,
                    winLoseCom: roundTo(companyFinalCom, 3),
                    winLose: roundTo(companyWinLose, 3),
                    totalWinLoseCom: roundTo((companyWinLose + companyFinalCom), 3)
                },
                shareHolder: {
                    group: betTransaction.commission.shareHolder.group,
                    winLoseCom: roundTo(shareHolderFinalCom, 3),
                    winLose: roundTo(shareHolderWinLose, 3),
                    totalWinLoseCom: roundTo((shareHolderWinLose + shareHolderFinalCom), 3)
                },
                senior: {
                    group: betTransaction.commission.senior.group,
                    winLoseCom: roundTo(seniorFinalCom, 3),
                    winLose: roundTo(seniorWinLose, 3),
                    totalWinLoseCom: roundTo((seniorWinLose + seniorFinalCom), 3)
                },
                masterAgent: {
                    group: betTransaction.commission.masterAgent.group,
                    winLoseCom: roundTo(masterAgentFinalCom, 3),
                    winLose: roundTo(masterAgentWinLose, 3),
                    totalWinLoseCom: roundTo((masterAgentWinLose + masterAgentFinalCom), 3)
                },
                agent: {
                    group: betTransaction.commission.agent.group,
                    winLoseCom: roundTo(agentFinalCom, 3),
                    winLose: roundTo(agentWinLose, 3),
                    totalWinLoseCom: roundTo((agentWinLose + agentFinalCom), 3)
                },
                member: {
                    id: betTransaction.memberId,
                    winLoseCom: memberCommissionWL,
                    winLose: memberWinLoseWL,
                    totalWinLoseCom: roundTo((memberWinLoseWL + memberCommissionWL), 3)
                }
            };
        } else {
            winLoseInfo = {
                betId: betTransaction.betId,
                betAmount: amount,
                memberCredit: betTransaction.memberCredit,
                betResult: betResult,
                superAdmin: {
                    winLoseCom: 0,
                    winLose: 0,
                    totalWinLoseCom: 0
                },
                company: {
                    winLoseCom: 0,
                    winLose: 0,
                    totalWinLoseCom: 0
                },
                shareHolder: {
                    winLoseCom: 0,
                    winLose: 0,
                    totalWinLoseCom: 0
                },
                senior: {
                    winLoseCom: 0,
                    winLose: 0,
                    totalWinLoseCom: 0
                },
                masterAgent: {
                    winLoseCom: 0,
                    winLose: 0,
                    totalWinLoseCom: 0
                },
                agent: {
                    winLoseCom: 0,
                    winLose: 0,
                    totalWinLoseCom: 0
                },
                member: {
                    id: betTransaction.memberId,
                    winLoseCom: 0,
                    winLose: 0,
                    totalWinLoseCom: 0
                }
            };
        }
        return winLoseInfo;

    },
    logoutDownlineMemberPartnerGame: function (groupId, callback) {

        findAllSubGroupAndOwnName(groupId, [], (err, memberList) => {
            console.log('findAllSubGroupAndOwnName : ', memberList);


            async.parallel([callback => {

                SexyService.callKickUsers(memberList, (err, response) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, response);
                    }
                });

            }, callback => {

                _.forEach(memberList, name => {
                    SaGameService.callKickUser(name, (err, response) => {
                        if (err) {
                            // callback(err, null);
                        } else {
                            // callback(null, response);
                        }
                    });
                });
                callback(null, '');
            }, callback => {

                // DreamGameService.callKickUser(response.username_lower, false, (err, response) => {
                //     if (err) {
                //         callback(err, null);
                //     } else {
                //         callback(null, response);
                //     }
                // });

                callback(null, '');
            }], (err, results) => {

                if (err) {
                    callback(err, null);
                } else {
                    callback(null, results);
                }

            });

        });

    }
}


function findAllSubGroupAndOwnName(groupId, listOfAgent, callback) {

    console.log('find11 : ', groupId);

    AgentGroupModel.findOne({_id: mongoose.Types.ObjectId(groupId)})
        .populate([{
            path: 'childGroups',
            select: 'childGroups childMembers',
            populate: [{
                path: 'childGroups',
                model: 'AgentGroup',
                select: 'childGroups childMembers'
            }, {
                path: 'childMembers',
                model: 'Member',
                select: 'username_lower'
            }]
        }, {
            path: 'childMembers',
            model: 'Member',
            select: 'username_lower'
        }])
        .select('_id childGroups childMembers')
        .exec((err, response) => {

            listOfAgent.push({
                id: response._id, members: response.childMembers.map(item => {
                    return item.username_lower
                })
            });

            async.each(response.childGroups, (child, callback1) => {

                listOfAgent.push({
                    id: child._id, members: child.childMembers.map(item => {
                        return item.username_lower
                    })
                });

                async.each(child.childGroups, (child2, callback2) => {

                    listOfAgent.push({
                        id: child2._id, members: child2.childMembers.map(item => {
                            return item.username_lower
                        })
                    });

                    findAllSubGroupAndOwnName(child2._id, listOfAgent, (err, response) => {
                        callback2(null, true);
                    });

                }, (err) => {
                    callback1(null, true);
                });
            }, (err) => {

                let memberNameList = [];
                _.each(listOfAgent, function (item) {
                    memberNameList = memberNameList.concat(item.members);
                });

                memberNameList = memberNameList.filter(item => {
                    return item
                })
                callback(null, memberNameList);
            });
        });
}