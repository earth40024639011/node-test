/**
 * Created by boonsongrit on 9/18/17.
 */

const moment = require('moment');
const querystring = require('querystring');
const crypto = require('crypto');
const request = require('request');
const _ = require('underscore');
const base64url = require('base64url');
const utf8 = require('utf8');


const getClientKey = require('../controllers/getClientKey');
const {
    DS_API_URL = 'https://api.sandsys.pw',
    DS_GAME_URL = 'https://link.sandsys.pw',
    DS_AGENT_ACCOUNT = 'AMB0001UAT',
    DS_CHANNEL = '92423733',
    DS_CERT = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDLcaVoSY2LJprDXoq9/g7JeIoyrKz9mjUS+DeJ/41zt3l6qQNAB8OsxZdkZBnBAtoJOUh2jkMBTiniKQFilMkifvqhfZVA491ux3XB2FHDypaL/ycoS/jTmgt1LXI9ts1G9qs9CneLReaVDOgRzLxLwGbl9WbRf4s6eukDdnd2ZQIDAQAB',
    DS_IMAGE_URL = 'https://amb-slot.s3-ap-southeast-1.amazonaws.com/ds-game'
} = getClientKey.getKey();

let API_URL = DS_API_URL;
let GAME_URL = DS_GAME_URL;
let AGENT_ACCOUNT = DS_AGENT_ACCOUNT;
let CHANNEL = DS_CHANNEL;
let CERT = DS_CERT;
let IMAGE_URL = DS_IMAGE_URL;

const CLIENT_NAME         = process.env.CLIENT_NAME          || 'SPORTBOOK88';

function callRegisterMember(username,callback) {
    let headers = {
        'Content-Type': 'application/json',
        'X-Ds-Signature' :CERT
    };



    let options = {
        url: API_URL+'/v1/member/create',
        method: 'POST',
        headers: headers,
        json:{
            "channel": CHANNEL,
            "agent": AGENT_ACCOUNT,
            "account": username.toLowerCase(),
        }
    };

    console.log('register ==== ',options)

    request(options, (error, response, body) => {

        if (error) {
            callback(error, null);
        } else {
            console.log('body === ', body)
            callback(null,body);
        }
    });

}


function callLoginGame(username,token,gameId,callback) {
    let headers = {
        'Content-Type': 'application/json',
        'X-Ds-Signature' :CERT
    };



    let options = {
        url: API_URL+'/v1/member/login_game',
        method: 'POST',
        headers: headers,
        json:{
            "channel": CHANNEL,
            "agent": AGENT_ACCOUNT,
            "account": username.toLowerCase(),
            "token":token,
            "game_id":gameId,
            "oper":'520c7eb3',
            "lang":"th_th"
        }
    };

    console.log('login === ',options);

    request(options, (error, response, body) => {

        if (error) {
            callback(error, null);
        } else {
            console.log('login body == ',body)
            callback(null,body);
        }
    });

}


function callMemberBetRecords(callback) {

    let headers = {
        'Content-Type': 'application/json',
        'X-Ds-Signature' :CERT
    };

    let options = {
        url: API_URL+'/v1/record/get_bet_records',
        method: 'POST',
        headers: headers,
        json:{
            "channel": CHANNEL,
            "finish_time": {
                "start_time": "2019-10-30T08:54:14+07:00",
                "end_time": "2019-10-31T11:47:08+07:00"
            },
            "index": 0,
            "limit":5000
        }
    };

    console.log(options)

    request(options, (error, response, body) => {

        if (error) {
            callback(error, null);
        } else {
            console.log(body)
            callback(null,body);
        }
    });

}

function callBetRecordURL(username,token,gameId,callback) {

    let headers = {
        'Content-Type': 'application/json',
        'X-Ds-Signature' :CERT
    };

    let options = {
        url: API_URL+'/v1/record/get_bet_detail_page',
        method: 'POST',
        headers: headers,
        json:{
            "channel": CHANNEL,
            "finish_time": {
                "start_time": "2018-10-19T08:54:14+01:00",
                "end_time": "2018-10-20T11:47:08+01:00"
            },
            "account": username.toLowerCase(),
            "token":token,
            "game_id":gameId,
            "lang":"en_us"
        }
    };

    console.log(options)

    request(options, (error, response, body) => {

        if (error) {
            callback(error, null);
        } else {
            console.log(body)
            callback(null,body);
        }
    });

}



function callGetGameList() {

    let gameList = [
        {
            GameType: 'Fishing',
            GameId: '1001',
            GameName: getGameName('1001'),
            ImageUrl: IMAGE_URL+'/1001.png',
            Provider:"DS"
        },
        {
            GameType: 'Fishing',
            GameId: '1002',
            GameName: getGameName('1002'),
            ImageUrl: IMAGE_URL+'/1002.png',
            Provider:"DS"
        },
        // {
        //     Type: 'POKER',
        //     GameId: '2001',
        //     GameName: getGameName('2001'),
        //     ImageUrl: IMAGE_URL+'/2001.png'
        // },
        // {
        //     Type: 'POKER',
        //     GameId: '2002',
        //     GameName: getGameName('2002'),
        //     ImageUrl: IMAGE_URL+'/2002.png'
        // },
        // {
        //     Type: 'POKER',
        //     GameId: '2003',
        //     GameName: getGameName('2003'),
        //     ImageUrl: IMAGE_URL+'/2003.png'
        // },
        // {
        //     Type: 'POKER',
        //     GameId: '2004',
        //     GameName: getGameName('2004'),
        //     ImageUrl: IMAGE_URL+'/2004.png'
        // },
        // {
        //     Type: 'POKER',
        //     GameId: '2005',
        //     GameName: getGameName('2005'),
        //     ImageUrl: IMAGE_URL+'/2005.png'
        // },
        // {
        //     Type: 'POKER',
        //     GameId: '2006',
        //     GameName: getGameName('2006'),
        //     ImageUrl: IMAGE_URL+'/2006.png'
        // },
        // {
        //     Type: 'POKER',
        //     GameId: '2007',
        //     GameName: getGameName('2007'),
        //     ImageUrl: IMAGE_URL+'/2007.png'
        // },
        // {
        //     Type: 'POKER',
        //     GameId: '2008',
        //     GameName: getGameName('2008'),
        //     ImageUrl: IMAGE_URL+'/2008.png'
        // },
        // {
        //     Type: 'POKER',
        //     GameId: '2010',
        //     GameName: getGameName('2010'),
        //     ImageUrl: IMAGE_URL+'/2010.png'
        // },
        // {
        //     Type: 'POKER',
        //     GameId: '2011',
        //     GameName: getGameName('2011'),
        //     ImageUrl: IMAGE_URL+'/2011.png'
        // },
        {
            GameType: 'Slots',
            GameId: '3042',
            GameName: getGameName('3042'),
            ImageUrl: IMAGE_URL+'/3042.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3041',
            GameName: getGameName('3041'),
            ImageUrl: IMAGE_URL+'/3041.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3040',
            GameName: getGameName('3040'),
            ImageUrl: IMAGE_URL+'/3040.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3039',
            GameName: getGameName('3039'),
            ImageUrl: IMAGE_URL+'/3039.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3038',
            GameName: getGameName('3038'),
            ImageUrl: IMAGE_URL+'/3038.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3037',
            GameName: getGameName('3037'),
            ImageUrl: IMAGE_URL+'/3037.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3036',
            GameName: getGameName('3036'),
            ImageUrl: IMAGE_URL+'/3036.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3035',
            GameName: getGameName('3035'),
            ImageUrl: IMAGE_URL+'/3035.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3034',
            GameName: getGameName('3034'),
            ImageUrl: IMAGE_URL+'/3034.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3001',
            GameName: getGameName('3001'),
            ImageUrl: IMAGE_URL+'/3001.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3002',
            GameName: getGameName('3002'),
            ImageUrl: IMAGE_URL+'/3002.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3003',
            GameName: getGameName('3003'),
            ImageUrl: IMAGE_URL+'/3003.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3004',
            GameName: getGameName('3004'),
            ImageUrl: IMAGE_URL+'/3004.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3005',
            GameName: getGameName('3005'),
            ImageUrl: IMAGE_URL+'/3005.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3006',
            GameName: getGameName('3006'),
            ImageUrl: IMAGE_URL+'/3006.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3007',
            GameName: getGameName('3007'),
            ImageUrl: IMAGE_URL+'/3007.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3008',
            GameName: getGameName('3008'),
            ImageUrl: IMAGE_URL+'/3008.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3009',
            GameName: getGameName('3009'),
            ImageUrl: IMAGE_URL+'/3009.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3010',
            GameName: getGameName('3010'),
            ImageUrl: IMAGE_URL+'/3010.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3011',
            GameName: getGameName('3011'),
            ImageUrl: IMAGE_URL+'/3011.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3013',
            GameName: getGameName('3013'),
            ImageUrl: IMAGE_URL+'/3013.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3014',
            GameName: getGameName('3014'),
            ImageUrl: IMAGE_URL+'/3014.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3015',
            GameName: getGameName('3015'),
            ImageUrl: IMAGE_URL+'/3015.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3016',
            GameName: getGameName('3016'),
            ImageUrl: IMAGE_URL+'/3016.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3017',
            GameName: getGameName('3017'),
            ImageUrl: IMAGE_URL+'/3017.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3018',
            GameName: getGameName('3018'),
            ImageUrl: IMAGE_URL+'/3018.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3019',
            GameName: getGameName('3019'),
            ImageUrl: IMAGE_URL+'/3019.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3020',
            GameName: getGameName('3020'),
            ImageUrl: IMAGE_URL+'/3020.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3021',
            GameName: getGameName('3021'),
            ImageUrl: IMAGE_URL+'/3021.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3023',
            GameName: getGameName('3023'),
            ImageUrl: IMAGE_URL+'/3023.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3024',
            GameName: getGameName('3024'),
            ImageUrl: IMAGE_URL+'/3024.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3025',
            GameName: getGameName('3025'),
            ImageUrl: IMAGE_URL+'/3025.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3026',
            GameName: getGameName('3026'),
            ImageUrl: IMAGE_URL+'/3026.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3027',
            GameName: getGameName('3027'),
            ImageUrl: IMAGE_URL+'/3027.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3029',
            GameName: getGameName('3029'),
            ImageUrl: IMAGE_URL+'/3029.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3030',
            GameName: getGameName('3030'),
            ImageUrl: IMAGE_URL+'/3030.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3031',
            GameName: getGameName('3031'),
            ImageUrl: IMAGE_URL+'/3031.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3032',
            GameName: getGameName('3032'),
            ImageUrl: IMAGE_URL+'/3032.png',
            Provider:"DS"
        },
        {
            GameType: 'Slots',
            GameId: '3033',
            GameName: getGameName('3033'),
            ImageUrl: IMAGE_URL+'/3033.png',
            Provider:"DS"
        },
    ];

    return gameList;
}


function getGameName(mType) {
    switch (mType){
        case "1001":
            return "Ocean Lord";
        case "1002":
            return "Let’s Shoot";
        case "2001":
            return "Golden Card Dragon & Tiger";
        case "2002":
            return "Hundred NiuNiu";
        case "2003":
            return "Win 3 Cards";
        case "2004":
            return "Two-Eight Bar";
        case "2005":
            return "Three Facecard";
        case "2006":
            return "Baccarat";
        case "2007":
            return "Chinese Poker";
        case "2008":
            return "Black Jack";
        case "2010":
            return "Red Envelope";
        case "2011":
            return "Banker NiuNiu";
        case "3001":
            return "Diamond Mogul";
        case "3002":
            return "Over Dragon’s Gate";
        case "3003":
            return "Get Money";
        case "3004":
            return "Great Lion";
        case "3005":
            return "Ultra Treasure";
        case "3006":
            return "Egypt Oracle";
        case "3007":
            return "Caishen Coming";
        case "3008":
            return "Candy Dynasty";
        case "3009":
            return "Rich Lion";
        case "3010":
            return "Monkey King";
        case "3011":
            return "Bust Treasury";
        case "3013":
            return "Get High";
        case "3014":
            return "Arab";
        case "3015":
            return "Wolf Legend";
        case "3016":
            return "Crystal Fruits";
        case "3017":
            return "Golf";
        case "3018":
            return "Tiger Lord";
        case "3019":
            return "Zeus";
        case "3020":
            return "Dracula";
        case "3021":
            return "Pirate King";
        case "3023":
            return "Stone Hominid";
        case "3024":
            return "T-Rex";
        case "3025":
            return "Greatest Circus";
        case "3026":
            return "Midas Touch";
        case "3027":
            return "Maya King";
        case "3029":
            return "Rich Dragon";
        case "3030":
            return "JIN HOU YE";
        case "3031":
            return "Fushen Coming";
        case "3032":
            return "Pandaria";
        case "3033":
            return "Rich Now";
        case "3034":
            return "777";
        case "3035":
            return "Triple Monkey";
        case "3036":
            return "ZHAO YUN";
        case "3037":
            return "Phoenix";
        case "3038":
            return "LU LING QI";
        case "3039":
            return "Booming Gems";
        case "3040":
            return "Many Beauties";
        case "3041":
            return "More Beauties";
        case "3042":
            return "Doggy Wealth";
        default:
            return mType;
    }
}

function callGetTransactionHistoryResult(account,gameId,agent,callback) {
    let headers = {
        'Content-Type': 'application/json',
        'X-Ds-Signature' :CERT
    };



    let options = {
        url: API_URL+'/v1/member/bet_history',
        method: 'POST',
        headers: headers,
        json:{
            "channel": CHANNEL,
            "agent": agent,
            "account": account.toLowerCase(),
            "game_id":gameId,
            "lang":"en_us"
        }
    };

    console.log(options);

    request(options, (error, response, body) => {
        if (error) {
            callback(error, null);
        } else {
            console.log(body);
            callback(null,body);
        }
    });

}

module.exports = {
    callRegisterMember,
    callLoginGame,
    callMemberBetRecords,
    callBetRecordURL,
    callGetGameList,callGetTransactionHistoryResult
}
