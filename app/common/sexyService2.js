/**
 * Created by boonsongrit on 9/18/17.
 */

const moment = require('moment');
const dateFormat = require('dateformat');
const querystring = require('querystring');
const crypto = require('crypto');
const request = require('request');
const parseString = require('xml2js').parseString;
const uuidv1 = require('uuid/v1');



const _ = require('underscore');
const getClientKey = require('../controllers/getClientKey');
const {
    SEXY_HOST: HOST,
    SEXY_AGENT_ID: AGENT_ID,
    SEXY_CERT: CERT
} = getClientKey.getKey();

const CLIENT_NAME         = process.env.CLIENT_NAME || 'SPORTBOOK88';

function callCreateMember(username,limit,callback) {

    console.log('limit : ',limit)
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };


    let limitIds;
    if(limit == 1){
        limitIds = '260901';
    }else if(limit == 2){
        limitIds = '260901,260903';
    }else if(limit == 3){
        limitIds = '260901,260903,260905';
    }else if(limit == 4){
        limitIds = '260901,260903,260905,260906';
    }else if(limit == 5){
        limitIds = '260901,260903,260905,260906,260908';
    }

    let form = {
        betLimit:{
            "SEXYBCRT" :{
                "LIVE":{
                    "limitId":limitIds
                }
            },
            "VENUS" :{
                "LIVE":{
                    "limitId":limitIds
                }
            },
            "SV388":{
                "LIVE":{
                    "maxbet":2000,
                    "minbet":1,
                    "mindraw":1,
                    "matchlimit":2000,
                    "maxdraw":2000
                }
            }
        },
        cert:CERT,
        agentId: AGENT_ID,
        userId:username.toLowerCase(),
        currency:'THB'
    };

    let options = {
        url: HOST + '/wallet/createMember',
        method: 'POST',
        headers: headers,
        body: `betLimit={"SEXYBCRT":{"LIVE":{"limitId":[${limitIds}]}}}&cert=${CERT}&agentId=${AGENT_ID}&userId=${username.toLowerCase()}&currency=THB`
    };


    console.log(options)

    request(options, (error, response, body) => {
        console.log("=== : ",body)
        if (error) {
            callback('', null);
        } else {

            let responseBody = JSON.parse(body);

            if(responseBody.status == '0000' || responseBody.status == '1001'){
                callback(null, responseBody);
            }else {
                callback(body, null);
            }
        }
    });

}




function callUpdateBetLimit(username,limit,callback) {

    console.log('limit : ',limit)
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };


    let limitIds;
    if(limit == 1){
        limitIds = '260901';
    }else if(limit == 2){
        limitIds = '260901,260903';
    }else if(limit == 3){
        limitIds = '260901,260903,260905';
    }else if(limit == 4){
        limitIds = '260901,260903,260905,260906';
    }else if(limit == 5){
        limitIds = '260901,260903,260905,260906,260908';
    }


    let options = {
        url: HOST + '/wallet/updateBetLimit',
        method: 'POST',
        headers: headers,
        body: `betLimit={"SEXYBCRT":{"LIVE":{"limitId":[${limitIds}]}}}&cert=${CERT}&agentId=${AGENT_ID}&userId=${username.toLowerCase()}`
    };


    console.log(options)

    request(options, (error, response, body) => {
        console.log("=== : ",body)
        if (error) {
            callback('', null);
        } else {

            let responseBody = JSON.parse(body);

            if(responseBody.status == '0000' || responseBody.status == '1001'){
                callback(null, responseBody);
            }else {
                callback(body, null);
            }
        }
    });

}


function callLogin(username,callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    let body = {
        cert:CERT,
        agentId: AGENT_ID,
        userId:username.toLowerCase()
    };


    let options = {
        url: HOST + '/wallet/login',
        method: 'POST',
        headers: headers,
        body: querystring.stringify(body)
    };

    console.log(options)

    request(options, (error, response, body) => {
        if (error) {
            callback('', null);
        } else {

            let responseBody = JSON.parse(body);

            if(responseBody.status != '0000'){
                callback(body, null);
            }else {
                callback(null, responseBody);
            }
        }
    });

}



function callGetTransactionHistoryResult(username,platform,platformTxId,callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    let body = {
        cert:CERT,
        agentId: AGENT_ID,
        userId:username.toLowerCase(),
        platform:platform,
        platformTxId:platformTxId
    };


    let options = {
        url: HOST + '/wallet/getTransactionHistoryResult',
        method: 'POST',
        headers: headers,
        body: querystring.stringify(body)
    };

    console.log(options);

    request(options, (error, response, body) => {
        if (error) {
            callback('', null);
        } else {
            console.log(body)

            let responseBody = JSON.parse(body);

            if(responseBody.status != '0000'){
                callback(body, null);
            }else {
                callback(null, responseBody);
            }
        }
    });

}

function callDoLoginAndLaunchGame(username,limit,callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    let body = {
        cert:CERT,
        agentId: AGENT_ID,
        userId:username.toLowerCase(),
        platform:'SEXYBCRT',
        gameType:'LIVE',
        gameCode:'MX-LIVE-001'
    };


    let options = {
        url: HOST + '/wallet/doLoginAndLaunchGame',
        method: 'POST',
        headers: headers,
        body: querystring.stringify(body)
    };

    console.log(options)

    request(options, (error, response, body) => {
        if (error) {
            callback('', null);
        } else {

            let responseBody = JSON.parse(body);

            if(responseBody.status != '0000'){
                callback(body, null);
            }else {
                callUpdateBetLimit(username.toLowerCase(),limit,(err,limitResponse)=>{

                });
                callback(null, responseBody);
            }
        }
    });

}

function callKickUsers(usernameList, callback) {


    const headers = {
        'Content-Type': 'application/json'
    };

    const option = {
        url: `${HOST}/api/${WEBSITE}/logout?cert=${CERT}&user=${usernameList.join(',')}`,
        method: 'GET',
        headers: headers
    };

    request(option, (err, response, body) => {
        if (err) {
            callback(body, null);
        } else {
            let resultBody = JSON.parse(body);
            callback(null, resultBody);
        }
    });


}

``

function cancelTransaction(txID, callback) {

    const headers = {
        'Content-Type': 'application/json'
    };
    const option = {
        url: `${HOST}/api/${WEBSITE}/resubmitCancelbetNotification?cert=${CERT}&transactionNo=${txID}`,
        method: 'GET',
        headers: headers
    };
    console.log(option,txID,'iuuu');
    request(option, (err,response, body) => {
        console.log(response,body,'oooooononnnn')
        if (err) {
            callback(body, null);
        } else {
            let resultBody = JSON.parse(body);
            callback(null, resultBody);
        }
    });

}



module.exports = {
    callKickUsers,
    cancelTransaction,
    callCreateMember,
    callLogin,
    callDoLoginAndLaunchGame,
    callGetTransactionHistoryResult
}
