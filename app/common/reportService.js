const config = require('config');
const jwt = require('jsonwebtoken');
const async = require("async");
const MemberModel = require('../models/member.model.js');
const BetTransactionModel = require('../models/betTransaction.model.js');
const WebSiteModel = require('../models/website.model');
const mongoose = require('mongoose');
const DateUtils = require('../common/dateUtils');
const MemberService = require('../common/memberService');
const moment = require('moment');
const _ = require('underscore');
const roundTo = require('round-to');
const request = require('request');
const AWS = require('aws-sdk');
const {Pool} = require('pg');
const {Client} = require('@elastic/elasticsearch')
const {wlESQueryBuilder, wlMatchESQueryBuilder,agentPaymentESQueryBuilder} = require('./elasticsearch/query/wl_report')

const _ELS_INDEX_NAME = process.env.ELS_INDEX_NAME;

var sqs = new AWS.SQS({
    accessKeyId: "AKIATUBQYIAFNEW34FE7",
    secretAccessKey: "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",
    region: 'ap-southeast-1'
});

const esClient = new Client({
    cloud: {
        id: 'sportbook:YXAtc291dGhlYXN0LTEuYXdzLmZvdW5kLmlvJDQwNDY3N2ZjZWU0NTQ2NTg5ZDc1ZGQwMGUxMTYwZDMzJGQ3NDMzN2VmOTQ4NjRiNGY5YjA1MDhlYjU4ZDBlZWI3'
    },
    auth: {
        username: 'elastic',
        password: 'awZMM4jlvfpmqgklVXaTE3Nc'
    }
});



function isElasticEnabled(callback) {

    WebSiteModel.findOne({},(err,response) => {
        if(response) {
            callback(response.isElastic);
        }else {
            callback(false);
        }
    });
}

function getAgentPrefix(groupType) {
    if (groupType === 'SUPER_ADMIN') {
        return 'SA';
    } else if (groupType === 'COMPANY') {
        return 'CO';
    } else if (groupType === 'SHARE_HOLDER') {
        return 'SH';
    } else if (groupType === 'SENIOR') {
        return 'SR';
    } else if (groupType === 'MASTER_AGENT') {
        return 'MA';
    } else if (groupType === 'AGENT') {
        return 'AG';
    }
}


function getChildLv(groupType) {
    if (groupType === 'SUPER_ADMIN') {
        return 'lv_3';
    } else if (groupType === 'COMPANY') {
        return 'lv_4';
    } else if (groupType === 'SHARE_HOLDER') {
        return 'lv_5';
    } else if (groupType === 'SENIOR') {
        return 'lv_6';
    } else if (groupType === 'MASTER_AGENT') {
        return 'lv_7';
    } else {
        return 'lv_2';
    }
}

function getCurrentLv(parentType) {
    if (!parentType) {
        return 'lv_1';
    } else if (parentType === 'SUPER_ADMIN') {
        return 'lv_2';
    } else if (parentType === 'COMPANY') {
        return 'lv_3';
    } else if (parentType === 'SHARE_HOLDER') {
        return 'lv_4';
    } else if (parentType === 'SENIOR') {
        return 'lv_5';
    } else if (parentType === 'MASTER_AGENT') {
        return 'lv_6';
    } else {
        return 'lv_7';
    }
}

function getGame (games) {
    let gameStr = games.split(',').map((value) => {
        let FOOTBALL = ['FOOTBALL', 'STEP', 'PARLAY'];
        let LOTTO = ['LOTTO', 'LOTTO_LAOS', 'LOTTO_PP'];
        if (FOOTBALL.includes(value)) return `'FOOTBALL'`
        else if (LOTTO.includes(value)) return `'LOTTO'`
        else return `'${value}'`
    });

    return _.uniq(gameStr).join(',');
}

function getSource (games) {

    let allSource = {}
    let gameStr = games.split(',').map((value) => {
        let FOOTBALL = {
            'FOOTBALL': 'TODAY',
            'STEP': 'MIX_STEP',
            'PARLAY': 'PARLAY'
        };
        let LOTTO = {
            'LOTTO': 'AMB_LOTTO',
            'LOTTO_LAOS': 'AMB_LOTTO_LAOS',
            'LOTTO_PP': 'AMB_LOTTO_PP'
        };
        if (_.has(FOOTBALL, value)) return `'${FOOTBALL[value]}'`
        else if (_.has(LOTTO, value)) return `'${LOTTO[value]}'`
        else '';
    });

    return _.uniq(gameStr.filter(e => !_.isEmpty(e))).join(',');
}


async function getAmbReport(dateFrom, dateTo, groupId,callback) {

    let dateList = DateUtils.enumerateReportBetweenDatesTask(moment(dateFrom, "DD/MM/YYYY"), moment(dateTo, "DD/MM/YYYY").add(1, 'd'), '11:00:00', '11:00:00', 24);

    // let dateList =DateUtils.enumerateNextDaysBetweenDates(moment(req.query.dateFrom, "DD/MM/YYYY"), moment(req.query.dateTo, "DD/MM/YYYY"), 'DD/MM/YYYY');
    let tasks = [];

    console.log(dateList);

    _.forEach(dateList, dateStr => {
        tasks.push(getWinLoseAccountPerDayES(dateStr,groupId));
    });

    async.parallel(tasks, (err, results) => {
        if (err) {
            console.log('err : ', err)
        }
        let summaryDataPerDay = summaryFunction(results);

        callback(null, summaryDataPerDay);
    });

    function getWinLoseAccountPerDayES(dateRange,groupId) {
        console.log('dateStr : ' + dateRange);

        return function (callbackTask) {

            let ES_QUERY = {
                "size": 0,
                "query": {
                    "bool": {
                        "must": [
                            {
                                "range": {
                                    "game_date": {
                                        "gte": moment(dateRange[0], 'YYYY-MM-DDTHH:mm:ss').utc(true).toDate(),
                                        "lt": moment(dateRange[1], 'YYYY-MM-DDTHH:mm:ss').utc(true).toDate()
                                    }
                                }
                            },
                            {
                                "term": {
                                    "lv_2": "CO"+groupId
                                }
                            },
                            {
                                "terms": {
                                    "status": ["DONE", "REJECTED", "CANCELLED"]
                                }
                            }
                        ]
                    }
                },
                "aggs": {
                    "report": {
                        "terms": {
                            "field": "source",
                            "size": 100
                        },
                        "aggs": {
                            "amount": {"sum": {"field": "amount"}},
                            "validAmount": {"sum": {"field": "valid_amount"}},
                            "memberWinLose": {"sum": {"field": "com_mb_wl"}},
                            "memberWinLoseCom": {"sum": {"field": "com_mb_wl_com"}},
                            "memberTotalWinLoseCom": {"sum": {"field": "com_mb_total_wl_com"}},

                            "superAdminWinLose": {"sum": {"field": "com_sa_wl"}},
                            "superAdminWinLoseCom": {"sum": {"field": "com_sa_wl_com"}},
                            "superAdminTotalWinLoseCom": {"sum": {"field": "com_sa_total_wl_com"}}
                        }
                    }
                }
            };

            esClient.search({
                index: _ELS_INDEX_NAME,
                body: ES_QUERY
            }).then(elsResponse => {

                let esBuckets = elsResponse.body.aggregations.report.buckets;

                callbackTask(null, esBuckets)
            })


        }


    }

    function summaryFunction(transactions) {

        transactions = transactions.filter(item => {
            return item && item.length > 0;
        });

        transactions = [].concat(...transactions);

        return _.chain(transactions).groupBy(key => {
            return key.key;
        }).map((value, key) => {

            return _.reduce(value, function (memo, obj) {


                return {
                    source: key,
                    amount: roundTo(memo.amount + obj.amount.value,2),
                    validAmount: roundTo(memo.validAmount + obj.validAmount.value,2),
                    stackCount: roundTo(memo.stackCount + obj.doc_count,2),
                    // grossComm: memo.grossComm + obj.grossComm.value,
                    memberWinLose: roundTo(memo.memberWinLose + obj.memberWinLose.value,2),
                    memberWinLoseCom: roundTo(memo.memberWinLoseCom + obj.memberWinLoseCom.value,2),
                    memberTotalWinLoseCom: roundTo(memo.memberTotalWinLoseCom + obj.memberTotalWinLoseCom.value,2),

                    superAdminWinLose: roundTo(memo.superAdminWinLose + obj.superAdminWinLose.value,2),
                    superAdminWinLoseCom: roundTo(memo.superAdminWinLoseCom + obj.superAdminWinLoseCom.value,2),
                    superAdminTotalWinLoseCom: roundTo(memo.superAdminTotalWinLoseCom + obj.superAdminTotalWinLoseCom.value,2)

                }
            }, {
                amount: 0,
                validAmount: 0,
                stackCount: 0,
                // grossComm: 0,
                memberWinLose: 0,
                memberWinLoseCom: 0,
                memberTotalWinLoseCom: 0,
                superAdminWinLose: 0,
                superAdminWinLoseCom: 0,
                superAdminTotalWinLoseCom: 0
            });
        }).value();
    }
}

async function queryMixUseWinLose(groupId, groupType, parentType, dateFrom, dateTo, timeFrom, timeTo, games, callback) {

    let prefixKey = getAgentPrefix(groupType);

    let groupByLv = getChildLv(parentType);

    let currentLv = getCurrentLv(parentType);

    let matchLevel = prefixKey + groupId;

    let dateFromString = `${dateFrom.split('/')[2]}-${dateFrom.split('/')[1]}-${dateFrom.split('/')[0]}T${timeFrom}.000Z`;
    let dateToString = `${dateTo.split('/')[2]}-${dateTo.split('/')[1]}-${dateTo.split('/')[0]}T${timeTo}.000Z`;

    // "grossComm": {$sum: {$multiply: ['$validAmount', {$divide: ['$commission.' + group + '.commission', 100]}]}},
    let grossComCol = 'com_'+prefixKey.toLowerCase()+'_commission';

    let ES_QUERY = wlESQueryBuilder({
        startDate: dateFromString,
        endDate: dateToString,
        matchLevel: matchLevel,
        groupByLevel: groupByLv,
        termLevel: currentLv,
        grossComCol : grossComCol,
        games
    });

    console.log('ES_QUERY', JSON.stringify(ES_QUERY))


    const { body } = await esClient.search({
        index: _ELS_INDEX_NAME,
        body: ES_QUERY
    })

    let esBuckets = body.aggregations.report.buckets;

    let list = esBuckets.map((value, index) => {

        let esResult = value;

        let key = esResult.key;
        // console.log(esResult)
        return {
            type : isTypeAgent(key) ? 'A_GROUP' : 'M_GROUP',
            group: isTypeAgent(key) ? key.slice(2) : key,
            member: !isTypeAgent(key) ? key : null,
            currency: "THB",
            stackCount: esResult.doc_count,
            grossComm: esResult.gross_comm.value,
            amount: esResult.amount.value,
            validAmount: esResult.valid_amount.value,
            memberWinLose: esResult.member_winlose.value,
            memberWinLoseCom: esResult.member_winlose_com.value,
            memberTotalWinLoseCom: esResult.member_winlose.value + esResult.member_winlose_com.value,// esResult.member_total_winlose_com.value,

            agentWinLose: esResult.agent_winlose.value,
            agentWinLoseCom: esResult.agent_winlose_com.value,
            agentTotalWinLoseCom:  esResult.agent_winlose.value + esResult.agent_winlose_com.value,// esResult.agent_total_winlose_com.value,

            masterAgentWinLose: esResult.masteragent_winlose.value,
            masterAgentWinLoseCom: esResult.masteragent_winlose_com.value,
            masterAgentTotalWinLoseCom: esResult.masteragent_winlose.value + esResult.masteragent_winlose_com.value,// esResult.masteragent_total_winlose_com.value,

            seniorWinLose: esResult.senior_winlose.value,
            seniorWinLoseCom: esResult.senior_winlose_com.value,
            seniorTotalWinLoseCom: esResult.senior_winlose.value + esResult.senior_winlose_com.value,// esResult.senior_total_winlose_com.value,

            shareHolderWinLose: esResult.shareholder_winlose.value,
            shareHolderWinLoseCom: esResult.shareholder_winlose_com.value,
            shareHolderTotalWinLoseCom: esResult.shareholder_winlose.value + esResult.shareholder_winlose_com.value,// esResult.shareholder_total_winlose_com.value,

            companyWinLose: esResult.company_winlose.value,
            companyWinLoseCom: esResult.company_winlose_com.value,
            companyTotalWinLoseCom: esResult.company_winlose.value + esResult.company_winlose_com.value,// esResult.company_total_winlose_com.value,

            superAdminWinLose: esResult.superadmin_winlose.value,
            superAdminWinLoseCom: esResult.superadmin_winlose_com.value,
            superAdminTotalWinLoseCom: esResult.superadmin_winlose.value + esResult.superadmin_winlose_com.value,// esResult.superadmin_total_winlose_com.value,
        };

    });

    callback(null, list);
    // callback(null, []);
    function isTypeAgent(id) {
        return id.startsWith('SA') || id.startsWith('CO') || id.startsWith('SH') || id.startsWith('SR') || id.startsWith('MA') || id.startsWith('AG');
    }
}

async function queryMixUseWinLoseMatch(groupId, groupType,parentType, queryParams, callback) {

    console.log('index : ',_ELS_INDEX_NAME)
    let prefixKey = getAgentPrefix(groupType);


    let matchLevel = prefixKey + groupId;

    let currentLv = getCurrentLv(parentType);

    let dateFromString = `${queryParams.dateFrom.split('/')[2]}-${queryParams.dateFrom.split('/')[1]}-${queryParams.dateFrom.split('/')[0]}T${queryParams.timeFrom}.000Z`;
    let dateToString = `${queryParams.dateTo.split('/')[2]}-${queryParams.dateTo.split('/')[1]}-${queryParams.dateTo.split('/')[0]}T${queryParams.timeTo}.000Z`;

    // "grossComm": {$sum: {$multiply: ['$validAmount', {$divide: ['$commission.' + group + '.commission', 100]}]}},
    let grossComCol = 'com_'+prefixKey.toLowerCase()+'_commission';

    let ES_QUERY = wlMatchESQueryBuilder({
        startDate: dateFromString,
        endDate: dateToString,
        matchLevel: matchLevel,
        // groupByLevel: groupByLv,
        termLevel: currentLv,
        grossComCol : grossComCol
    });

    console.log('ES_QUERY', JSON.stringify(ES_QUERY))

    const { body } = await esClient.search({
        index: _ELS_INDEX_NAME,
        body: ES_QUERY
    })

    let esBuckets = body.aggregations.report.buckets;

    let list = esBuckets.map((value, index) => {

        let esResult = value;

        let key = esResult.key;
        // console.log(esResult)
        return {
            key : key,//esResult.source,
            // type : isTypeAgent(key) ? 'A_GROUP' : 'M_GROUP',
            // group: isTypeAgent(key) ? key.slice(2) : key,
            // member: !isTypeAgent(key) ? key : null,
            currency: "THB",
            stackCount: esResult.doc_count,
            grossComm: esResult.gross_comm.value,
            amount: esResult.amount.value,
            validAmount: esResult.valid_amount.value,
            memberWinLose: esResult.member_winlose.value,
            memberWinLoseCom: esResult.member_winlose_com.value,
            memberTotalWinLoseCom: esResult.member_winlose.value + esResult.member_winlose_com.value,// esResult.member_total_winlose_com.value,

            agentWinLose: esResult.agent_winlose.value,
            agentWinLoseCom: esResult.agent_winlose_com.value,
            agentTotalWinLoseCom:  esResult.agent_winlose.value + esResult.agent_winlose_com.value,// esResult.agent_total_winlose_com.value,

            masterAgentWinLose: esResult.masteragent_winlose.value,
            masterAgentWinLoseCom: esResult.masteragent_winlose_com.value,
            masterAgentTotalWinLoseCom: esResult.masteragent_winlose.value + esResult.masteragent_winlose_com.value,// esResult.masteragent_total_winlose_com.value,

            seniorWinLose: esResult.senior_winlose.value,
            seniorWinLoseCom: esResult.senior_winlose_com.value,
            seniorTotalWinLoseCom: esResult.senior_winlose.value + esResult.senior_winlose_com.value,// esResult.senior_total_winlose_com.value,

            shareHolderWinLose: esResult.shareholder_winlose.value,
            shareHolderWinLoseCom: esResult.shareholder_winlose_com.value,
            shareHolderTotalWinLoseCom: esResult.shareholder_winlose.value + esResult.shareholder_winlose_com.value,// esResult.shareholder_total_winlose_com.value,

            companyWinLose: esResult.company_winlose.value,
            companyWinLoseCom: esResult.company_winlose_com.value,
            companyTotalWinLoseCom: esResult.company_winlose.value + esResult.company_winlose_com.value,// esResult.company_total_winlose_com.value,

            superAdminWinLose: esResult.superadmin_winlose.value,
            superAdminWinLoseCom: esResult.superadmin_winlose_com.value,
            superAdminTotalWinLoseCom: esResult.superadmin_winlose.value + esResult.superadmin_winlose_com.value,// esResult.superadmin_total_winlose_com.value,
        };

    });

    callback(null, list);

}

async function queryMixUsePaymentReport(groupId, groupType, parentType, lastPaymentDate, callback) {

    console.log(`queryMixUsePaymentReport : ${groupId} ${groupType} ${parentType} ${lastPaymentDate}`);

    let prefixKey = getAgentPrefix(groupType);

    let groupByLv = getChildLv(parentType);

    let currentLv = getCurrentLv(parentType);

    let matchLevel = prefixKey + groupId;



    let startYear = Number.parseInt(lastPaymentDate.getUTCFullYear());
    let startMonth = Number.parseInt(lastPaymentDate.getUTCMonth());
    let startDay = Number.parseInt(lastPaymentDate.getUTCDate());


    function diff_days(dt2, dt1) {

        let timeDiff = Math.abs(dt2.getTime() - dt1.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        return diffDays;

    }

    let diffDay = diff_days(lastPaymentDate,DateUtils.getCurrentDate());
    // console.log('diffDay : ',diffDay)

    let currentAmbDay = moment().hours() >= 11 ? moment().utc(true).date() : moment().utc(true).date() - 1;
    let currentAmbMonth = moment().hours() >= 11 ? moment().utc(true).month() : moment().utc(true).month();
    let currentAmbYear = moment().hours() >= 11 ? moment().utc(true).year() : moment().utc(true).year();


    if ( (diffDay > 60  || (((startDay + 1) == currentAmbDay) && (startMonth == currentAmbMonth) && (startYear == currentAmbYear)))) {
        callback(null, []);
        return;
    }

    let dateFromString,dateToString;

    if (moment().hours() >= 11) {
        dateFromString = moment([startYear, startMonth, startDay]).add(1, 'd').utc(true).format('DD-MM-YYYYT11:00:00.000Z');
        dateToString = moment().add(0, 'd').utc(true).format('DD-MM-YYYYT11:00:00.000Z');
    } else {
        dateFromString = moment([startYear, startMonth, startDay]).add(1, 'd').utc(true).format('DD-MM-YYYYT11:00:00.000Z');
        dateToString = moment().add(-1, 'd').utc(true).format('DD-MM-YYYYT11:00:00.000Z');
    }



    let ES_QUERY = agentPaymentESQueryBuilder({
        startDate: dateFromString,
        endDate: dateToString,
        termLevel: groupByLv,
        matchLevel: matchLevel,

    });

    console.log('_ELS_INDEX_NAME : ',_ELS_INDEX_NAME);
    console.log('ES_QUERY', JSON.stringify(ES_QUERY));


    const { body } = await esClient.search({
        index: _ELS_INDEX_NAME,
        body: ES_QUERY
    })

    console.log(body)

    let esBuckets = body.aggregations.report.buckets;

    let list = esBuckets.map((value, index) => {

        let esResult = value;

        let key = esResult.key;
        // console.log(esResult)
        return {

            group: matchLevel,
            currency: "THB",
            stackCount: esResult.doc_count,
            amount: esResult.amount.value,
            validAmount: esResult.valid_amount.value,
            memberWinLose: esResult.member_winlose.value,
            memberWinLoseCom: esResult.member_winlose_com.value,
            memberTotalWinLoseCom: esResult.member_winlose.value + esResult.member_winlose_com.value,// esResult.member_total_winlose_com.value,

            agentWinLose: esResult.agent_winlose.value,
            agentWinLoseCom: esResult.agent_winlose_com.value,
            agentTotalWinLoseCom:  esResult.agent_winlose.value + esResult.agent_winlose_com.value,// esResult.agent_total_winlose_com.value,

            masterAgentWinLose: esResult.masteragent_winlose.value,
            masterAgentWinLoseCom: esResult.masteragent_winlose_com.value,
            masterAgentTotalWinLoseCom: esResult.masteragent_winlose.value + esResult.masteragent_winlose_com.value,// esResult.masteragent_total_winlose_com.value,

            seniorWinLose: esResult.senior_winlose.value,
            seniorWinLoseCom: esResult.senior_winlose_com.value,
            seniorTotalWinLoseCom: esResult.senior_winlose.value + esResult.senior_winlose_com.value,// esResult.senior_total_winlose_com.value,

            shareHolderWinLose: esResult.shareholder_winlose.value,
            shareHolderWinLoseCom: esResult.shareholder_winlose_com.value,
            shareHolderTotalWinLoseCom: esResult.shareholder_winlose.value + esResult.shareholder_winlose_com.value,// esResult.shareholder_total_winlose_com.value,

            companyWinLose: esResult.company_winlose.value,
            companyWinLoseCom: esResult.company_winlose_com.value,
            companyTotalWinLoseCom: esResult.company_winlose.value + esResult.company_winlose_com.value,// esResult.company_total_winlose_com.value,

            superAdminWinLose: esResult.superadmin_winlose.value,
            superAdminWinLoseCom: esResult.superadmin_winlose_com.value,
            superAdminTotalWinLoseCom: esResult.superadmin_winlose.value + esResult.superadmin_winlose_com.value,// esResult.superadmin_total_winlose_com.value,
        };

    });

    callback(null, list);

}

function sendToQueue(betId,callback) {

    callback(null,{});

}


const QUEUE_URL = {
    "CSR_": 'https://sqs.ap-southeast-1.amazonaws.com/249210355722/sportbook-csr',
    "FASTBET98": 'https://sqs.ap-southeast-1.amazonaws.com/249210355722/sportbook-fastbet',
    "AMB_SPORTBOOK" : 'https://sqs.ap-southeast-1.amazonaws.com/249210355722/sportbook-amb',
    "MGM": 'https://sqs.ap-southeast-1.amazonaws.com/249210355722/sportbook-mgm',
    "SPORTBOOK88" : 'https://sqs.ap-southeast-1.amazonaws.com/249210355722/sportbook',
    "TEST2": 'https://sqs.ap-southeast-1.amazonaws.com/249210355722/test2',
    "MIGRATION_AMBBET": 'https://sqs.ap-southeast-2.amazonaws.com/249210355722/migration-ambbet'
}

function sendToQueueBatch(entries, client ,callback) {

    var queueUrl = QUEUE_URL[client];

    // SQS message parameters
    var params = {
        QueueUrl: queueUrl,
        Entries: entries
    };

    sqs.sendMessageBatch(params, function (err, data) {
        if (err) {
            console.log('error:', "failed to send message" + err);
            callback(500, null)

        } else {
            callback(null, data)
        }
    });
}

function sendToQueueNew(message, client ,callback) {

    var queueUrl = QUEUE_URL[client];

    // SQS message parameters
    var params = {
        QueueUrl: queueUrl,
        MessageBody: message
    };

    sqs.sendMessage(params, function (err, data) {
        if (err) {
            console.log('error:', "failed to send message" + err);
            callback(500, null)

        } else {
            callback(null, data)
        }
    });
}

function getWinLoseReport(groupId, groupType, dateFrom, dateTo, timeFrom, timeTo, games, callback) {

    let dateList = DateUtils.enumerateReportBetweenDates2(moment(dateFrom, "DD/MM/YYYY"), moment(dateTo, "DD/MM/YYYY"), timeFrom, timeTo);

    let tasks = [];

    _.forEach(dateList, (dateRange) => {
        tasks.push(getWinLoseAccountPerDay(groupId, groupType, dateRange, games));
    });

    async.parallel(tasks, (err, results) => {
        if (err) {
            console.log('err : ', err)
        }

        let summaryDataPerDay = summaryFunction(results);

        callback(null, summaryDataPerDay);
    });
    // callback(null, []);
}

function getWinLoseReportMember2(groupId, groupType, dateFrom, dateTo, timeFrom, timeTo, games, username, callback) {


    MemberService.findByUserName(username, (err, memberResponse) => {

        let dateList = DateUtils.enumerateReportBetweenDatesMember(moment(dateFrom, "DD/MM/YYYY"), moment(dateTo, "DD/MM/YYYY"), timeFrom, timeTo);

        let tasks = [];

        _.forEach(dateList, dateRange => {
            tasks.push(getWinLoseAccountMemberPerDay2(groupId, groupType, dateRange, games, memberResponse._id));
        });

        async.parallel(tasks, (err, results) => {
            if (err) {
                console.log('err 11 : ', err);
            }
            console.log('parallel DONE ')
            let summaryDataPerDay = summaryFunctionMember(results);
            //
            callback(null, summaryDataPerDay);
        })

    });

}

function getWinLoseReportMember(groupId, groupType, dateFrom, dateTo, timeFrom, timeTo, games, callback) {

    let dateList = DateUtils.enumerateReportBetweenDates2(moment(dateFrom, "DD/MM/YYYY"), moment(dateTo, "DD/MM/YYYY"), timeFrom, timeTo);

    let tasks = [];

    _.forEach(dateList, dateRange => {
        tasks.push(getWinLoseAccountMemberPerDay(groupId, groupType, dateRange, games));
    });

    async.parallel(tasks, (err, results) => {
        if (err) {
            console.log('err 11 : ', err)
        }

        console.log('parallel DONE ');
        let summaryDataPerDay = summaryFunctionMember(results);
        //
        callback(null, summaryDataPerDay);
    })

}

function getWinLoseMatch(groupId, groupType, queryParams, callback) {

    let dateList = DateUtils.enumerateReportBetweenDates2(moment(queryParams.dateFrom, "DD/MM/YYYY"), moment(queryParams.dateTo, "DD/MM/YYYY"), queryParams.timeFrom, queryParams.timeTo);

    let tasks = [];

    _.forEach(dateList, dateRange => {
        tasks.push(getWinLoseMatchPerDay(groupId, groupType, dateRange, queryParams));
    });

    async.parallel(tasks, (err, results) => {
        if (err) {
            console.log('err 11 : ', err);
        }

        let summaryDataPerDay = summaryFunctionWlMatch(results);

        callback(null, summaryDataPerDay);
    });

}

function getWinLoseAccountPerDay(groupId, groupType, dateRange, games) {

    let group = '';
    let child;

    if (groupType === 'SUPER_ADMIN') {
        group = 'superAdmin';
        child = 'company';
    } else if (groupType === 'COMPANY') {
        group = 'company';
        child = 'shareHolder';
    } else if (groupType === 'SHARE_HOLDER') {
        group = 'shareHolder';
        child = 'senior';
    } else if (groupType === 'SENIOR') {
        group = "senior";
        child = 'agent';
    } else if (groupType === 'MASTER_AGENT') {
        group = "masterAgent";
        child = 'agent';
    } else if (groupType === 'AGENT') {
        group = "agent";
        child = '';
    }

    return function (callbackTask) {

        let condition = {};

        let group_id = {};
        let andCondition;

        if (groupType === 'SENIOR') {

            group_id = {
                groupId: '$groupId'
            };

            andCondition = [
                {
                    'gameDate': {
                        "$gte":moment(dateRange[0], "YYYY-MM-DDTHH:mm:ss.000").utc(true).toDate(),
                        "$lt": moment(dateRange[1], "YYYY-MM-DDTHH:mm:ss.000").utc(true).toDate()
                    }
                },
                {['commission.' + group + '.group']: mongoose.Types.ObjectId(groupId)},
                {
                    $or: [
                        {['commission.masterAgent.parentGroup']: mongoose.Types.ObjectId(groupId)},
                        {
                            '$and': [
                                // {['commission.masterAgent.parentGroup']: {$ne: mongoose.Types.ObjectId(groupId)}},
                                {['commission.agent.parentGroup']: mongoose.Types.ObjectId(groupId)}
                            ]
                        }]
                },
                {'memberParentGroup': {$ne: mongoose.Types.ObjectId(groupId)}},
                {'status': {$in: ['DONE', 'REJECTED', 'CANCELLED']}}
            ];


        } else {

            group_id = {
                groupId: '$commission.' + child + '.group'
            };

            andCondition = [
                {
                    'gameDate': {
                        "$gte": moment(dateRange[0], 'YYYY-MM-DDTHH:mm:ss').utc(true).toDate(),
                        "$lt": moment(dateRange[1], 'YYYY-MM-DDTHH:mm:ss').utc(true).toDate()
                    }
                },
                {['commission.' + group + '.group']: mongoose.Types.ObjectId(groupId)},
                {['commission.' + child + '.parentGroup']: mongoose.Types.ObjectId(groupId)},
                // {'memberParentGroup': {$ne: mongoose.Types.ObjectId(groupId)}},
                {'status': {$in: ['DONE', 'REJECTED', 'CANCELLED']}},
            ];

            console.log(andCondition)

        }

        if (games) {

            let gamesCondition = {
                '$or': games.split(',').map(function (game) {
                    if (game === 'FOOTBALL') {
                        return {'$and': [{'game': 'FOOTBALL'}, {gameType: 'TODAY'}]}

                    } else if (game === 'STEP') {
                        return {'$and': [{'game': 'FOOTBALL'}, {gameType: 'MIX_STEP'}]}
                    } else if (game === 'PARLAY') {
                        return {'$and': [{'game': 'FOOTBALL'}, {gameType: 'PARLAY'}]}
                    } else if (game === 'CASINO') {
                        return {'$and': [{'game': 'CASINO'}]}
                    } else if (game === 'GAME') {
                        return {'$and': [{'game': 'GAME'}]}
                    } else if (game === 'LOTTO') {
                        return {'$and': [{'game': 'LOTTO'}, {'source': 'AMB_LOTTO'}]}
                    } else if (game === 'LOTTO_LAOS') {
                        return {'$and': [{'game': 'LOTTO'}, {'source': 'AMB_LOTTO_LAOS'}]}
                    } else if (game === 'LOTTO_PP') {
                        return {'$and': [{'game': 'LOTTO'}, {'source': 'AMB_LOTTO_PP'}]}
                    } else if (game === 'M2') {
                        return {'$and': [{'game': 'M2'}]}
                    } else if (game === 'MULTI_PLAYER') {
                        return {'$and': [{'game': 'MULTI_PLAYER'}]}
                    }
                })
            };

            if (games.split(',').length != 10) {
                andCondition.push(gamesCondition);
            }

        }

        // if(sources) {
        //     console.log(sources.split(',').map(function (source) {return source}));
        //     andCondition.push({'source':{$in:[sources.split(',').map(function (source) {return source})]}});
        // }

        condition['$and'] = andCondition;


        let aggregatePipeline = [
            {
                $match: condition
            },
        ];


        if (groupType == 'SENIOR') {
            aggregatePipeline.push(
                {
                    $addFields: {
                        groupId: {$cond: [{$eq: ['$commission.masterAgent.group', null]}, '$commission.agent.group', '$commission.masterAgent.group']}
                    }
                }
            );
        }

        aggregatePipeline.push({
            "$group": {
                _id: group_id,
                "stackCount": {$sum: 1},
                "amount": {$sum: '$amount'},
                "validAmount": {$sum: '$validAmount'},
                "grossComm": {$sum: {$multiply: ['$validAmount', {$divide: ['$commission.' + group + '.commission', 100]}]}},
                "memberWinLose": {$sum: {$toDecimal: '$commission.member.winLose'}},
                "memberWinLoseCom": {$sum: {$toDecimal: '$commission.member.winLoseCom'}},
                // "memberTotalWinLoseCom": {$sum: {$toDecimal: '$commission.member.totalWinLoseCom'}},

                "agentWinLose": {$sum: {$toDecimal: '$commission.agent.winLose'}},
                "agentWinLoseCom": {$sum: {$toDecimal: '$commission.agent.winLoseCom'}},
                // "agentTotalWinLoseCom": {$sum: {$toDecimal: '$commission.agent.totalWinLoseCom'}},

                "masterAgentWinLose": {$sum: {$toDecimal: '$commission.masterAgent.winLose'}},
                "masterAgentWinLoseCom": {$sum: {$toDecimal: '$commission.masterAgent.winLoseCom'}},
                // "masterAgentTotalWinLoseCom": {$sum: {$toDecimal: '$commission.masterAgent.totalWinLoseCom'}},

                "seniorWinLose": {$sum: {$toDecimal: '$commission.senior.winLose'}},
                "seniorWinLoseCom": {$sum: {$toDecimal: '$commission.senior.winLoseCom'}},
                // "seniorTotalWinLoseCom": {$sum: {$toDecimal: '$commission.senior.totalWinLoseCom'}},

                "shareHolderWinLose": {$sum: {$toDecimal: '$commission.shareHolder.winLose'}},
                "shareHolderWinLoseCom": {$sum: {$toDecimal: '$commission.shareHolder.winLoseCom'}},
                // "shareHolderTotalWinLoseCom": {$sum: {$toDecimal: '$commission.shareHolder.totalWinLoseCom'}},

                "companyWinLose": {$sum: {$toDecimal: '$commission.company.winLose'}},
                "companyWinLoseCom": {$sum: {$toDecimal: '$commission.company.winLoseCom'}},
                // "companyTotalWinLoseCom": {$sum: {$toDecimal: '$commission.company.totalWinLoseCom'}},

                "superAdminWinLose": {$sum: {$toDecimal: '$commission.superAdmin.winLose'}},
                "superAdminWinLoseCom": {$sum: {$toDecimal: '$commission.superAdmin.winLoseCom'}},
                // "superAdminTotalWinLoseCom": {$sum: {$toDecimal: '$commission.superAdmin.totalWinLoseCom'}},
            }
        });

        aggregatePipeline.push(
            {
                $project: {
                    _id: 1,
                    type: 'A_GROUP',
                    group: '$_id.groupId',
                    currency: "THB",
                    stackCount: '$stackCount',
                    grossComm: '$grossComm',
                    amount: '$amount',
                    validAmount: '$validAmount',

                    memberWinLose: {$toDouble: '$memberWinLose'},
                    memberWinLoseCom: {$toDouble: '$memberWinLoseCom'},
                    memberTotalWinLoseCom:{$toDouble: { $add: [ "$memberWinLose", "$memberWinLoseCom" ] }},

                    agentWinLose: {$toDouble: '$agentWinLose'},
                    agentWinLoseCom: {$toDouble: '$agentWinLoseCom'},
                    agentTotalWinLoseCom: {$toDouble: { $add: [ "$agentWinLose", "$agentWinLoseCom" ] }},

                    masterAgentWinLose: {$toDouble: '$masterAgentWinLose'},
                    masterAgentWinLoseCom: {$toDouble: '$masterAgentWinLoseCom'},
                    masterAgentTotalWinLoseCom: {$toDouble:{ $add: [ "$masterAgentWinLose", "$masterAgentWinLoseCom" ] }},

                    seniorWinLose: {$toDouble: '$seniorWinLose'},
                    seniorWinLoseCom: {$toDouble: '$seniorWinLoseCom'},
                    seniorTotalWinLoseCom: {$toDouble:{ $add: [ "$seniorWinLose", "$seniorWinLoseCom" ] }},

                    shareHolderWinLose: {$toDouble: '$shareHolderWinLose'},
                    shareHolderWinLoseCom: {$toDouble: '$shareHolderWinLoseCom'},
                    shareHolderTotalWinLoseCom: {$toDouble:{ $add: [ "$shareHolderWinLose", "$shareHolderWinLoseCom" ] }},

                    companyWinLose: {$toDouble: '$companyWinLose'},
                    companyWinLoseCom: {$toDouble: '$companyWinLoseCom'},
                    companyTotalWinLoseCom: {$toDouble:{ $add: [ "$companyWinLose", "$companyWinLoseCom" ] }},

                    superAdminWinLose: {$toDouble: '$superAdminWinLose'},
                    superAdminWinLoseCom: {$toDouble: '$superAdminWinLoseCom'},
                    superAdminTotalWinLoseCom: {$toDouble:{ $add: [ "$superAdminWinLose", "$superAdminWinLoseCom" ] }}

                }
            }
        );


        BetTransactionModel.aggregate(aggregatePipeline).read('secondaryPreferred').option({maxTimeMS: 200000}).exec((err, results) => {
            // separateAggregate(condition,group,child,(err, results) => {
            if (err) {
                callbackTask(err + ' ::::: ' + dateRange, null);
            } else {
                callbackTask(null, results);
            }
        });

    }

}

function getWinLoseAccountMemberPerDay2(groupId, groupType, dateRange, games, memberId) {
    // console.log('dateStr : ' + dateStr + ' , game : ' + games);


    let group = '';
    let child;

    if (groupType === 'SUPER_ADMIN') {
        group = 'superAdmin';
        child = 'company';
    } else if (groupType === 'COMPANY') {
        group = 'company';
        child = 'shareHolder';
    } else if (groupType === 'SHARE_HOLDER') {
        group = 'shareHolder';
        child = 'senior';
    } else if (groupType === 'SENIOR') {
        group = "senior";
        child = 'agent';
    } else if (groupType === 'MASTER_AGENT') {
        group = "masterAgent";
        child = 'agent';
    } else if (groupType === 'AGENT') {
        group = "agent";
        child = '';
    }


    return function (callbackTask) {

        let condition = {};
        let orCondition = [
            {
                'gameDate': {
                    "$gte":moment(dateRange[0], "YYYY-MM-DDTHH:mm:ss.000").utc(true).toDate(),
                    "$lt": moment(dateRange[1], "YYYY-MM-DDTHH:mm:ss.000").utc(true).toDate()
                }
            },
            {'memberId': mongoose.Types.ObjectId(memberId)},
            {'status': {$in: ['DONE', 'REJECTED', 'CANCELLED']}},
        ];

        // console.log(orCondition)
        if (games) {

            let gamesCondition = {
                '$or': games.split(',').map(function (game) {
                    if (game === 'FOOTBALL') {
                        return {'$and': [{'game': 'FOOTBALL'}, {gameType: 'TODAY'}]}
                    } else if (game === 'STEP') {
                        return {'$and': [{'game': 'FOOTBALL'}, {gameType: 'MIX_STEP'}]}
                    } else if (game === 'PARLAY') {
                        return {'$and': [{'game': 'FOOTBALL'}, {gameType: 'PARLAY'}]}
                    } else if (game === 'CASINO') {
                        return {'$and': [{'game': 'CASINO'}]}
                    } else if (game === 'GAME') {
                        return {'$and': [{'game': 'GAME'}]}
                    } else if (game === 'LOTTO') {
                        return {'$and': [{'game': 'LOTTO'}, {'source': 'AMB_LOTTO'}]}
                    } else if (game === 'LOTTO_LAOS') {
                        return {'$and': [{'game': 'LOTTO'}, {'source': 'AMB_LOTTO_LAOS'}]}
                    } else if (game === 'LOTTO_PP') {
                        return {'$and': [{'game': 'LOTTO'}, {'source': 'AMB_LOTTO_PP'}]}
                    } else if (game === 'M2') {
                        return {'$and': [{'game': 'M2'}]}
                    } else if (game === 'MULTI_PLAYER') {
                        return {'$and': [{'game': 'MULTI_PLAYER'}]}
                    }
                })

            };

            if (games.split(',').length != 10) {
                orCondition.push(gamesCondition);
            }

        }

        condition['$and'] = orCondition;


        BetTransactionModel.aggregate([
            {
                $match: condition
            },
            {
                "$group": {
                    "_id": {
                        commission: '$memberParentGroup',
                        member: '$memberId'
                    },
                    // "currency": {$first: "$currency"},
                    "stackCount": {$sum: 1},
                    "amount": {$sum: '$amount'},
                    "validAmount": {$sum: '$validAmount'},
                    "grossComm": {$sum: {$multiply: ['$validAmount', {$divide: ['$commission.' + group + '.commission', 100]}]}},


                    "memberWinLose": {$sum: {$toDecimal: '$commission.member.winLose'}},
                    "memberWinLoseCom": {$sum: {$toDecimal: '$commission.member.winLoseCom'}},
                    "memberTotalWinLoseCom": {$sum: {$toDecimal: '$commission.member.totalWinLoseCom'}},

                    "agentWinLose": {$sum: {$toDecimal: '$commission.agent.winLose'}},
                    "agentWinLoseCom": {$sum: {$toDecimal: '$commission.agent.winLoseCom'}},
                    "agentTotalWinLoseCom": {$sum: {$toDecimal: '$commission.agent.totalWinLoseCom'}},

                    "masterAgentWinLose": {$sum: {$toDecimal: '$commission.masterAgent.winLose'}},
                    "masterAgentWinLoseCom": {$sum: {$toDecimal: '$commission.masterAgent.winLoseCom'}},
                    "masterAgentTotalWinLoseCom": {$sum: {$toDecimal: '$commission.masterAgent.totalWinLoseCom'}},

                    "seniorWinLose": {$sum: {$toDecimal: '$commission.senior.winLose'}},
                    "seniorWinLoseCom": {$sum: {$toDecimal: '$commission.senior.winLoseCom'}},
                    "seniorTotalWinLoseCom": {$sum: {$toDecimal: '$commission.senior.totalWinLoseCom'}},

                    "shareHolderWinLose": {$sum: {$toDecimal: '$commission.shareHolder.winLose'}},
                    "shareHolderWinLoseCom": {$sum: {$toDecimal: '$commission.shareHolder.winLoseCom'}},
                    "shareHolderTotalWinLoseCom": {$sum: {$toDecimal: '$commission.shareHolder.totalWinLoseCom'}},

                    "companyWinLose": {$sum: {$toDecimal: '$commission.company.winLose'}},
                    "companyWinLoseCom": {$sum: {$toDecimal: '$commission.company.winLoseCom'}},
                    "companyTotalWinLoseCom": {$sum: {$toDecimal: '$commission.company.totalWinLoseCom'}},

                    "superAdminWinLose": {$sum: {$toDecimal: '$commission.superAdmin.winLose'}},
                    "superAdminWinLoseCom": {$sum: {$toDecimal: '$commission.superAdmin.winLoseCom'}},
                    "superAdminTotalWinLoseCom": {$sum: {$toDecimal: '$commission.superAdmin.totalWinLoseCom'}},

                    // toDecimal(
                }
            },
            {
                $project: {
                    _id: 0,
                    type: 'M_GROUP',
                    member: '$_id.member',
                    group: '$_id.commission',
                    currency: "THB",
                    stackCount: '$stackCount',
                    grossComm: '$grossComm',
                    amount: '$amount',
                    validAmount: '$validAmount',
                    memberWinLose: {$toDouble: '$memberWinLose'},
                    memberWinLoseCom: {$toDouble: '$memberWinLoseCom'},
                    memberTotalWinLoseCom: {$toDouble: '$memberTotalWinLoseCom'},
                    agentWinLose: {$toDouble: '$agentWinLose'},
                    agentWinLoseCom: {$toDouble: '$agentWinLoseCom'},
                    agentTotalWinLoseCom: {$toDouble: '$agentTotalWinLoseCom'},
                    masterAgentWinLose: {$toDouble: '$masterAgentWinLose'},
                    masterAgentWinLoseCom: {$toDouble: '$masterAgentWinLoseCom'},
                    masterAgentTotalWinLoseCom: {$toDouble: '$masterAgentTotalWinLoseCom'},
                    seniorWinLose: {$toDouble: '$seniorWinLose'},
                    seniorWinLoseCom: {$toDouble: '$seniorWinLoseCom'},
                    seniorTotalWinLoseCom: {$toDouble: '$seniorTotalWinLoseCom'},
                    shareHolderWinLose: {$toDouble: '$shareHolderWinLose'},
                    shareHolderWinLoseCom: {$toDouble: '$shareHolderWinLoseCom'},
                    shareHolderTotalWinLoseCom: {$toDouble: '$shareHolderTotalWinLoseCom'},
                    companyWinLose: {$toDouble: '$companyWinLose'},
                    companyWinLoseCom: {$toDouble: '$companyWinLoseCom'},
                    companyTotalWinLoseCom: {$toDouble: '$companyTotalWinLoseCom'},
                    superAdminWinLose: {$toDouble: '$superAdminWinLose'},
                    superAdminWinLoseCom: {$toDouble: '$superAdminWinLoseCom'},
                    superAdminTotalWinLoseCom: {$toDouble: '$superAdminTotalWinLoseCom'}

                }
            }
        ]).exec((err, results) => {
            if (err) {
                callbackTask(err, null);
            } else {
                callbackTask(null, results);
            }
        });

    }

}

function getWinLoseAccountMemberPerDay(groupId, groupType, dateRange, games) {
    // console.log('dateStr : ' + dateStr + ' , game : ' + games);


    let group = '';
    let child;

    if (groupType === 'SUPER_ADMIN') {
        group = 'superAdmin';
        child = 'company';
    } else if (groupType === 'COMPANY') {
        group = 'company';
        child = 'shareHolder';
    } else if (groupType === 'SHARE_HOLDER') {
        group = 'shareHolder';
        child = 'senior';
    } else if (groupType === 'SENIOR') {
        group = "senior";
        child = 'agent';
    } else if (groupType === 'MASTER_AGENT') {
        group = "masterAgent";
        child = 'agent';
    } else if (groupType === 'AGENT') {
        group = "agent";
        child = '';
    }


    return function (callbackTask) {

        let condition = {};
        let orCondition = [
            {
                'gameDate': {
                    "$gte":moment(dateRange[0], "YYYY-MM-DDTHH:mm:ss.000").utc(true).toDate(),
                    "$lt": moment(dateRange[1], "YYYY-MM-DDTHH:mm:ss.000").utc(true).toDate()
                }
            },
            {'memberParentGroup': mongoose.Types.ObjectId(groupId)},
            {'status': {$in: ['DONE', 'REJECTED', 'CANCELLED']}}
        ];

        // console.log(orCondition)
        if (games) {

            let gamesCondition = {
                '$or': games.split(',').map(function (game) {
                    if (game === 'FOOTBALL') {
                        return {'$and': [{'game': 'FOOTBALL'}, {gameType: 'TODAY'}]}

                    } else if (game === 'STEP') {
                        return {'$and': [{'game': 'FOOTBALL'}, {gameType: 'MIX_STEP'}]}
                    } else if (game === 'PARLAY') {
                        return {'$and': [{'game': 'FOOTBALL'}, {gameType: 'PARLAY'}]}
                    } else if (game === 'CASINO') {
                        return {'$and': [{'game': 'CASINO'}]}
                    } else if (game === 'GAME') {
                        return {'$and': [{'game': 'GAME'}]}
                    } else if (game === 'LOTTO') {
                        return {'$and': [{'game': 'LOTTO'}, {'source': 'AMB_LOTTO'}]}
                    } else if (game === 'LOTTO_LAOS') {
                        return {'$and': [{'game': 'LOTTO'}, {'source': 'AMB_LOTTO_LAOS'}]}
                    } else if (game === 'LOTTO_PP') {
                        return {'$and': [{'game': 'LOTTO'}, {'source': 'AMB_LOTTO_PP'}]}
                    } else if (game === 'M2') {
                        return {'$and': [{'game': 'M2'}]}
                    } else if (game === 'MULTI_PLAYER') {
                        return {'$and': [{'game': 'MULTI_PLAYER'}]}
                    }
                })

            };

            if (games.split(',').length != 10) {
                orCondition.push(gamesCondition);
            }

        }

        condition['$and'] = orCondition;


        BetTransactionModel.aggregate([
            {
                $match: condition
            },
            {
                "$group": {
                    "_id": {
                        commission: '$memberParentGroup',
                        member: '$memberId'
                    },
                    // "currency": {$first: "$currency"},
                    "stackCount": {$sum: 1},
                    "amount": {$sum: '$amount'},
                    "validAmount": {$sum: '$validAmount'},
                    "grossComm": {$sum: {$multiply: ['$validAmount', {$divide: ['$commission.' + group + '.commission', 100]}]}},


                    "memberWinLose": {$sum: {$toDecimal: '$commission.member.winLose'}},
                    "memberWinLoseCom": {$sum: {$toDecimal: '$commission.member.winLoseCom'}},
                    // "memberTotalWinLoseCom": {$sum: {$toDecimal: '$commission.member.totalWinLoseCom'}},

                    "agentWinLose": {$sum: {$toDecimal: '$commission.agent.winLose'}},
                    "agentWinLoseCom": {$sum: {$toDecimal: '$commission.agent.winLoseCom'}},
                    // "agentTotalWinLoseCom": {$sum: {$toDecimal: '$commission.agent.totalWinLoseCom'}},

                    "masterAgentWinLose": {$sum: {$toDecimal: '$commission.masterAgent.winLose'}},
                    "masterAgentWinLoseCom": {$sum: {$toDecimal: '$commission.masterAgent.winLoseCom'}},
                    // "masterAgentTotalWinLoseCom": {$sum: {$toDecimal: '$commission.masterAgent.totalWinLoseCom'}},

                    "seniorWinLose": {$sum: {$toDecimal: '$commission.senior.winLose'}},
                    "seniorWinLoseCom": {$sum: {$toDecimal: '$commission.senior.winLoseCom'}},
                    // "seniorTotalWinLoseCom": {$sum: {$toDecimal: '$commission.senior.totalWinLoseCom'}},

                    "shareHolderWinLose": {$sum: {$toDecimal: '$commission.shareHolder.winLose'}},
                    "shareHolderWinLoseCom": {$sum: {$toDecimal: '$commission.shareHolder.winLoseCom'}},
                    // "shareHolderTotalWinLoseCom": {$sum: {$toDecimal: '$commission.shareHolder.totalWinLoseCom'}},

                    "companyWinLose": {$sum: {$toDecimal: '$commission.company.winLose'}},
                    "companyWinLoseCom": {$sum: {$toDecimal: '$commission.company.winLoseCom'}},
                    // "companyTotalWinLoseCom": {$sum: {$toDecimal: '$commission.company.totalWinLoseCom'}},

                    "superAdminWinLose": {$sum: {$toDecimal: '$commission.superAdmin.winLose'}},
                    "superAdminWinLoseCom": {$sum: {$toDecimal: '$commission.superAdmin.winLoseCom'}},
                    // "superAdminTotalWinLoseCom": {$sum: {$toDecimal: '$commission.superAdmin.totalWinLoseCom'}},

                    // toDecimal(
                }
            },
            {
                $project: {
                    _id: 0,
                    type: 'M_GROUP',
                    member: '$_id.member',
                    group: '$_id.commission',
                    currency: "THB",
                    stackCount: '$stackCount',
                    grossComm: '$grossComm',
                    amount: '$amount',
                    validAmount: '$validAmount',
                    memberWinLose: {$toDouble: '$memberWinLose'},
                    memberWinLoseCom: {$toDouble: '$memberWinLoseCom'},
                    memberTotalWinLoseCom:{$toDouble: { $add: [ "$memberWinLose", "$memberWinLoseCom" ] }},

                    agentWinLose: {$toDouble: '$agentWinLose'},
                    agentWinLoseCom: {$toDouble: '$agentWinLoseCom'},
                    agentTotalWinLoseCom: {$toDouble: { $add: [ "$agentWinLose", "$agentWinLoseCom" ] }},

                    masterAgentWinLose: {$toDouble: '$masterAgentWinLose'},
                    masterAgentWinLoseCom: {$toDouble: '$masterAgentWinLoseCom'},
                    masterAgentTotalWinLoseCom: {$toDouble:{ $add: [ "$masterAgentWinLose", "$masterAgentWinLoseCom" ] }},

                    seniorWinLose: {$toDouble: '$seniorWinLose'},
                    seniorWinLoseCom: {$toDouble: '$seniorWinLoseCom'},
                    seniorTotalWinLoseCom: {$toDouble:{ $add: [ "$seniorWinLose", "$seniorWinLoseCom" ] }},

                    shareHolderWinLose: {$toDouble: '$shareHolderWinLose'},
                    shareHolderWinLoseCom: {$toDouble: '$shareHolderWinLoseCom'},
                    shareHolderTotalWinLoseCom: {$toDouble:{ $add: [ "$shareHolderWinLose", "$shareHolderWinLoseCom" ] }},

                    companyWinLose: {$toDouble: '$companyWinLose'},
                    companyWinLoseCom: {$toDouble: '$companyWinLoseCom'},
                    companyTotalWinLoseCom: {$toDouble:{ $add: [ "$companyWinLose", "$companyWinLoseCom" ] }},

                    superAdminWinLose: {$toDouble: '$superAdminWinLose'},
                    superAdminWinLoseCom: {$toDouble: '$superAdminWinLoseCom'},
                    superAdminTotalWinLoseCom: {$toDouble:{ $add: [ "$superAdminWinLose", "$superAdminWinLoseCom" ] }}

                }
            }
        ]).read('secondaryPreferred').option({maxTimeMS: 200000}).exec((err, results) => {
            // separateAggregate(condition,group,child,(err, results) => {
            if (err) {
                callbackTask(err, null);
            } else {
                callbackTask(null, results);
            }
        });

    }

}

function getWinLoseMatchPerDay(groupId, groupType, dateRange, queryParams) {

    return function (callbackTask) {

        let source = queryParams.source;
        let groupBy = queryParams.groupBy;
        let game = queryParams.game;
        let league = queryParams.league;
        let matchId = queryParams.matchId;
        let currentLv = queryParams.lv;

        let group = '';

        if (groupType === 'SUPER_ADMIN') {
            group = 'superAdmin';
        } else if (groupType === 'COMPANY') {
            group = 'company';
        } else if (groupType === 'SHARE_HOLDER') {
            group = 'shareHolder';
        } else if (groupType === 'SENIOR') {
            group = "senior";
        } else if (groupType === 'MASTER_AGENT') {
            group = "masterAgent";
        } else if (groupType === 'AGENT') {
            group = "agent";
        }

        let condition = {};
        condition['$and'] = [
            {
                'gameDate': {
                    "$gte":moment(dateRange[0], "YYYY-MM-DDTHH:mm:ss.000").utc(true).toDate(),
                    "$lt": moment(dateRange[1], "YYYY-MM-DDTHH:mm:ss.000").utc(true).toDate()
                }
            },
            {['commission.' + group + '.group']: mongoose.Types.ObjectId(groupId)},
            {'status': {$in: ['DONE', 'REJECTED', 'CANCELLED']}}
        ];


        let groupBody = {
            "source": {$first: '$source'}
        };

        if (!currentLv) {

            groupBody._id = {key: '$source'};
            groupBody.source = {$first: '$source'}

        } else {
            if (source === 'FOOTBALL' || source === 'BASKETBALL' || source === 'MUAY_THAI' || source === 'M2_MUAY' || source === 'TENNIS'
                || source === 'ESPORT' || source === 'TABLE_TENNIS' || source === 'VOLLEYBALL' || source === 'RUGBY'
                || source === 'AMERICAN_FOOTBALL' || source === 'CRICKET' || source === 'CYCLING' || source === 'DARTS') {

                if (currentLv == 1) {

                    groupBody._id = {key: {$cond: [{$eq: ['$gameType', 'TODAY']}, '$hdp.leagueId', {$arrayElemAt: ["$parlay.matches.leagueId", 0]}]}};
                    groupBody.leagueId = {$first: {$cond: [{$eq: ['$gameType', 'TODAY']}, '$hdp.leagueId', {$arrayElemAt: ["$parlay.matches.leagueId", 0]}]}};
                    groupBody.leagueName = {$first: {$cond: [{$eq: ['$gameType', 'TODAY']}, '$hdp.leagueName', {$arrayElemAt: ["$parlay.matches.leagueName", 0]}]}};
                    game = groupBy;
                    condition.$and.push({source: game});
                } else if (currentLv == 2) {

                    groupBody._id = {key: {$cond: [{$eq: ['$gameType', 'TODAY']}, '$hdp.matchId', {$arrayElemAt: ["$parlay.matches.matchId", 0]}]}};
                    groupBody.matchId = {$first: {$cond: [{$eq: ['$gameType', 'TODAY']}, '$hdp.matchId', {$arrayElemAt: ["$parlay.matches.matchId", 0]}]}};
                    groupBody.matchName = {$first: {$cond: [{$eq: ['$gameType', 'TODAY']}, '$hdp.matchName', {$arrayElemAt: ["$parlay.matches.matchName", 0]}]}};

                    league = groupBy;
                    condition.$and.push({source: game});
                    condition.$and.push({$or: [{'hdp.leagueId': league}, {'parlay.matches.0.leagueId': league}]});

                } else if (currentLv == 3) {

                    groupBody._id = {key: '$memberId'};
                    groupBody.memberId = {$first: '$memberId'};

                    matchId = groupBy;

                    condition.$and.push({source: game});
                    condition.$and.push({'$or': [{'hdp.leagueId': league}, {'parlay.matches.0.leagueId': league}]},
                        {'$or': [{'hdp.matchId': matchId}, {'parlay.matches.0.matchId': matchId}]})

                }
            } else if (source === 'SEXY_BACCARAT' || source === 'LOTUS' || source === 'ALL_BET' || source === 'SA_GAME' || source === 'DREAM_GAME' || source === 'AG_GAME' || source === 'AMB_GAME'|| source === 'AMB_GAME2' || source === 'PRETTY_GAME') {
                if (currentLv == 1) {
                    groupBody._id = {key: '$memberId'};
                    groupBody.memberId = {$first: '$memberId'};

                    condition.$and.push({source: source});
                }
            } else if (source === 'SLOT_XO' || source === 'DS_GAME' || source === 'GANAPATI' || source === 'PG_SLOT') {
                if (currentLv == 1) {
                    groupBody._id = {key: '$memberId'};
                    groupBody.memberId = {$first: '$memberId'};

                    condition.$and.push({source: source});
                }
            } else if (source === 'LIVE22') {
                if (currentLv == 1) {
                    groupBody._id = {key: '$memberId'};
                    groupBody.memberId = {$first: '$memberId'};

                    condition.$and.push({source: source});
                }
            } else if (source === 'AMEBA') {
                if (currentLv == 1) {
                    groupBody._id = {key: '$memberId'};
                    groupBody.memberId = {$first: '$memberId'};

                    condition.$and.push({source: source});
                }
            } else if (source === 'SPADE_GAME') {
                if (currentLv == 1) {
                    groupBody._id = {key: '$memberId'};
                    groupBody.memberId = {$first: '$memberId'};

                    condition.$and.push({source: source});
                }
            } else if (source === 'AMB_LOTTO' || source === 'AMB_LOTTO_LAOS' || source === 'AMB_LOTTO_PP') {
                if (currentLv == 1) {
                    groupBody._id = {key: '$memberId'};
                    groupBody.memberId = {$first: '$memberId'};

                    condition.$and.push({source: source});
                }
            } else {
                groupBody._id = {key: '$source'};
            }
        }

        groupBody.stackCount = {$sum: 1},
            groupBody.amount = {$sum: '$amount'},
            groupBody.validAmount = {$sum: '$validAmount'},
            groupBody.grossComm = {$sum: {$multiply: ['$validAmount', {$divide: ['$commission.' + group + '.commission', 100]}]}},
            groupBody.memberWinLose = {$sum: {$toDecimal: '$commission.member.winLose'}},
            groupBody.memberWinLoseCom = {$sum: {$toDecimal: '$commission.member.winLoseCom'}},
            // groupBody.memberTotalWinLoseCom = {$sum: {$toDecimal: '$commission.member.totalWinLoseCom'}},

            groupBody.agentWinLose = {$sum: {$toDecimal: '$commission.agent.winLose'}},
            groupBody.agentWinLoseCom = {$sum: {$toDecimal: '$commission.agent.winLoseCom'}},
            // groupBody.agentTotalWinLoseCom = {$sum: {$toDecimal: '$commission.agent.totalWinLoseCom'}},

            groupBody.masterAgentWinLose = {$sum: {$toDecimal: '$commission.masterAgent.winLose'}},
            groupBody.masterAgentWinLoseCom = {$sum: {$toDecimal: '$commission.masterAgent.winLoseCom'}},
            // groupBody.masterAgentTotalWinLoseCom = {$sum: {$toDecimal: '$commission.masterAgent.totalWinLoseCom'}},

            groupBody.seniorWinLose = {$sum: {$toDecimal: '$commission.senior.winLose'}},
            groupBody.seniorWinLoseCom = {$sum: {$toDecimal: '$commission.senior.winLoseCom'}},
            // groupBody.seniorTotalWinLoseCom = {$sum: {$toDecimal: '$commission.senior.totalWinLoseCom'}},

            groupBody.shareHolderWinLose = {$sum: {$toDecimal: '$commission.shareHolder.winLose'}},
            groupBody.shareHolderWinLoseCom = {$sum: {$toDecimal: '$commission.shareHolder.winLoseCom'}},
            // groupBody.shareHolderTotalWinLoseCom = {$sum: {$toDecimal: '$commission.shareHolder.totalWinLoseCom'}},

            groupBody.companyWinLose = {$sum: {$toDecimal: '$commission.company.winLose'}},
            groupBody.companyWinLoseCom = {$sum: {$toDecimal: '$commission.company.winLoseCom'}},
            // groupBody.companyTotalWinLoseCom = {$sum: {$toDecimal: '$commission.company.totalWinLoseCom'}},

            groupBody.superAdminWinLose = {$sum: {$toDecimal: '$commission.superAdmin.winLose'}},
            groupBody.superAdminWinLoseCom = {$sum: {$toDecimal: '$commission.superAdmin.winLoseCom'}},
            // groupBody.superAdminTotalWinLoseCom = {$sum: {$toDecimal: '$commission.superAdmin.totalWinLoseCom'}}


            BetTransactionModel.aggregate([
                {
                    $match: condition
                },
                {
                    "$group": groupBody
                },
                {
                    $project: {
                        _id: 0,
                        key: '$_id.key',
                        stackCount: '$stackCount',
                        grossComm: '$grossComm',
                        leagueName: '$leagueName',
                        matchName: '$matchName',
                        currency: "$currency",
                        source: '$source',
                        member: '$memberId',
                        amount: '$amount',
                        validAmount: '$validAmount',
                        memberWinLose: {$toDouble: '$memberWinLose'},
                        memberWinLoseCom: {$toDouble: '$memberWinLoseCom'},
                        memberTotalWinLoseCom:{$toDouble: { $add: [ "$memberWinLose", "$memberWinLoseCom" ] }},

                        agentWinLose: {$toDouble: '$agentWinLose'},
                        agentWinLoseCom: {$toDouble: '$agentWinLoseCom'},
                        agentTotalWinLoseCom: {$toDouble: { $add: [ "$agentWinLose", "$agentWinLoseCom" ] }},

                        masterAgentWinLose: {$toDouble: '$masterAgentWinLose'},
                        masterAgentWinLoseCom: {$toDouble: '$masterAgentWinLoseCom'},
                        masterAgentTotalWinLoseCom: {$toDouble:{ $add: [ "$masterAgentWinLose", "$masterAgentWinLoseCom" ] }},

                        seniorWinLose: {$toDouble: '$seniorWinLose'},
                        seniorWinLoseCom: {$toDouble: '$seniorWinLoseCom'},
                        seniorTotalWinLoseCom: {$toDouble:{ $add: [ "$seniorWinLose", "$seniorWinLoseCom" ] }},

                        shareHolderWinLose: {$toDouble: '$shareHolderWinLose'},
                        shareHolderWinLoseCom: {$toDouble: '$shareHolderWinLoseCom'},
                        shareHolderTotalWinLoseCom: {$toDouble:{ $add: [ "$shareHolderWinLose", "$shareHolderWinLoseCom" ] }},

                        companyWinLose: {$toDouble: '$companyWinLose'},
                        companyWinLoseCom: {$toDouble: '$companyWinLoseCom'},
                        companyTotalWinLoseCom: {$toDouble:{ $add: [ "$companyWinLose", "$companyWinLoseCom" ] }},

                        superAdminWinLose: {$toDouble: '$superAdminWinLose'},
                        superAdminWinLoseCom: {$toDouble: '$superAdminWinLoseCom'},
                        superAdminTotalWinLoseCom: {$toDouble:{ $add: [ "$superAdminWinLose", "$superAdminWinLoseCom" ] }}
                    }
                }
            ]).read('secondaryPreferred').option({maxTimeMS: 200000}).exec((err, results) => {

                if (err) {
                    callbackTask(err, null);
                } else {
                    console.log(results)
                    callbackTask(null, results);
                }
            });

    }

}

function summaryFunction(transactions) {

    transactions = transactions.filter(item => {
        return item && item.length > 0;
    });

    transactions = [].concat(...transactions);

    return _.chain(transactions).groupBy(key => {
        return key.group;
    }).map((value, key) => {

        return _.reduce(value, function (memo, obj) {

            return {
                group: key.split('|')[0],
                type: value[0].type,
                currency: value[0].currency,
                amount: memo.amount + obj.amount,
                validAmount: memo.validAmount + obj.validAmount,
                stackCount: memo.stackCount + obj.stackCount,
                grossComm: memo.grossComm + obj.grossComm,
                memberWinLose: memo.memberWinLose + obj.memberWinLose,
                memberWinLoseCom: memo.memberWinLoseCom + obj.memberWinLoseCom,
                memberTotalWinLoseCom: memo.memberTotalWinLoseCom + obj.memberTotalWinLoseCom,

                agentWinLose: memo.agentWinLose + obj.agentWinLose,
                agentWinLoseCom: memo.agentWinLoseCom + obj.agentWinLoseCom,
                agentTotalWinLoseCom: memo.agentTotalWinLoseCom + obj.agentTotalWinLoseCom,

                masterAgentWinLose: memo.masterAgentWinLose + obj.masterAgentWinLose,
                masterAgentWinLoseCom: memo.masterAgentWinLoseCom + obj.masterAgentWinLoseCom,
                masterAgentTotalWinLoseCom: memo.masterAgentTotalWinLoseCom + obj.masterAgentTotalWinLoseCom,

                seniorWinLose: memo.seniorWinLose + obj.seniorWinLose,
                seniorWinLoseCom: memo.seniorWinLoseCom + obj.seniorWinLoseCom,
                seniorTotalWinLoseCom: memo.seniorTotalWinLoseCom + obj.seniorTotalWinLoseCom,

                shareHolderWinLose: memo.shareHolderWinLose + obj.shareHolderWinLose,
                shareHolderWinLoseCom: memo.shareHolderWinLoseCom + obj.shareHolderWinLoseCom,
                shareHolderTotalWinLoseCom: memo.shareHolderTotalWinLoseCom + obj.shareHolderTotalWinLoseCom,

                companyWinLose: memo.companyWinLose + obj.companyWinLose,
                companyWinLoseCom: memo.companyWinLoseCom + obj.companyWinLoseCom,
                companyTotalWinLoseCom: memo.companyTotalWinLoseCom + obj.companyTotalWinLoseCom,

                superAdminWinLose: memo.superAdminWinLose + obj.superAdminWinLose,
                superAdminWinLoseCom: memo.superAdminWinLoseCom + obj.superAdminWinLoseCom,
                superAdminTotalWinLoseCom: memo.superAdminTotalWinLoseCom + obj.superAdminTotalWinLoseCom

            }
        }, {
            amount: 0,
            validAmount: 0,
            stackCount: 0,
            grossComm: 0,
            memberWinLose: 0,
            memberWinLoseCom: 0,
            memberTotalWinLoseCom: 0,
            agentWinLose: 0,
            agentWinLoseCom: 0,
            agentTotalWinLoseCom: 0,
            masterAgentWinLose: 0,
            masterAgentWinLoseCom: 0,
            masterAgentTotalWinLoseCom: 0,
            seniorWinLose: 0,
            seniorWinLoseCom: 0,
            seniorTotalWinLoseCom: 0,
            shareHolderWinLose: 0,
            shareHolderWinLoseCom: 0,
            shareHolderTotalWinLoseCom: 0,
            companyWinLose: 0,
            companyWinLoseCom: 0,
            companyTotalWinLoseCom: 0,
            superAdminWinLose: 0,
            superAdminWinLoseCom: 0,
            superAdminTotalWinLoseCom: 0
        });

    }).value();


}

function summaryFunctionMember(transactions) {

    transactions = transactions.filter(item => {
        return item && item.length > 0;
    });

    transactions = [].concat(...transactions);

    return _.chain(transactions).groupBy(key => {
        return key.member;
    }).map((value, key) => {

        return _.reduce(value, function (memo, obj) {

            return {
                member: key.split('|')[0],
                group: value[0].group,
                type: value[0].type,
                currency: value[0].currency,
                amount: memo.amount + obj.amount,
                validAmount: memo.validAmount + obj.validAmount,
                stackCount: memo.stackCount + obj.stackCount,
                grossComm: memo.grossComm + obj.grossComm,
                memberWinLose: memo.memberWinLose + obj.memberWinLose,
                memberWinLoseCom: memo.memberWinLoseCom + obj.memberWinLoseCom,
                memberTotalWinLoseCom: memo.memberTotalWinLoseCom + obj.memberTotalWinLoseCom,

                agentWinLose: memo.agentWinLose + obj.agentWinLose,
                agentWinLoseCom: memo.agentWinLoseCom + obj.agentWinLoseCom,
                agentTotalWinLoseCom: memo.agentTotalWinLoseCom + obj.agentTotalWinLoseCom,

                masterAgentWinLose: memo.masterAgentWinLose + obj.masterAgentWinLose,
                masterAgentWinLoseCom: memo.masterAgentWinLoseCom + obj.masterAgentWinLoseCom,
                masterAgentTotalWinLoseCom: memo.masterAgentTotalWinLoseCom + obj.masterAgentTotalWinLoseCom,

                seniorWinLose: memo.seniorWinLose + obj.seniorWinLose,
                seniorWinLoseCom: memo.seniorWinLoseCom + obj.seniorWinLoseCom,
                seniorTotalWinLoseCom: memo.seniorTotalWinLoseCom + obj.seniorTotalWinLoseCom,

                shareHolderWinLose: memo.shareHolderWinLose + obj.shareHolderWinLose,
                shareHolderWinLoseCom: memo.shareHolderWinLoseCom + obj.shareHolderWinLoseCom,
                shareHolderTotalWinLoseCom: memo.shareHolderTotalWinLoseCom + obj.shareHolderTotalWinLoseCom,

                companyWinLose: memo.companyWinLose + obj.companyWinLose,
                companyWinLoseCom: memo.companyWinLoseCom + obj.companyWinLoseCom,
                companyTotalWinLoseCom: memo.companyTotalWinLoseCom + obj.companyTotalWinLoseCom,

                superAdminWinLose: memo.superAdminWinLose + obj.superAdminWinLose,
                superAdminWinLoseCom: memo.superAdminWinLoseCom + obj.superAdminWinLoseCom,
                superAdminTotalWinLoseCom: memo.superAdminTotalWinLoseCom + obj.superAdminTotalWinLoseCom

            }
        }, {
            amount: 0,
            validAmount: 0,
            stackCount: 0,
            grossComm: 0,
            memberWinLose: 0,
            memberWinLoseCom: 0,
            memberTotalWinLoseCom: 0,
            agentWinLose: 0,
            agentWinLoseCom: 0,
            agentTotalWinLoseCom: 0,
            masterAgentWinLose: 0,
            masterAgentWinLoseCom: 0,
            masterAgentTotalWinLoseCom: 0,
            seniorWinLose: 0,
            seniorWinLoseCom: 0,
            seniorTotalWinLoseCom: 0,
            shareHolderWinLose: 0,
            shareHolderWinLoseCom: 0,
            shareHolderTotalWinLoseCom: 0,
            companyWinLose: 0,
            companyWinLoseCom: 0,
            companyTotalWinLoseCom: 0,
            superAdminWinLose: 0,
            superAdminWinLoseCom: 0,
            superAdminTotalWinLoseCom: 0
        });

    }).value();


}

function summaryFunctionWlMatch(transactions) {

    transactions = transactions.filter(item => {
        return item && item.length > 0;
    });

    return _.reduce(transactions, function (preList, nextList) {

        let uniqueData = _.uniq(_.union(preList, nextList), false, function (item, key, a) {
            return item.key.toString();
        });


        return _.map(uniqueData, (uniqueObj) => {

            let preData = _.filter(preList, (preObj) => {
                return uniqueObj.key.toString() === preObj.key.toString();
            })[0];

            let next = _.filter(nextList, (nextObj) => {
                return uniqueObj.key.toString() === nextObj.key.toString();
            })[0];


            if (preData && next) {
                return {
                    key: preData.key,
                    leagueName: preData.leagueName,
                    matchName: preData.matchName,
                    currency: preData.currency,
                    source: preData.source,
                    member: preData.member,

                    amount: preData.amount + next.amount,
                    validAmount: preData.validAmount + next.validAmount,
                    stackCount: preData.stackCount + next.stackCount,
                    grossComm: preData.grossComm + next.grossComm,

                    memberWinLose: roundTo(preData.memberWinLose, 5) + roundTo(next.memberWinLose, 5),
                    memberWinLoseCom: roundTo(preData.memberWinLoseCom, 5) + roundTo(next.memberWinLoseCom, 5),
                    memberTotalWinLoseCom: roundTo(preData.memberTotalWinLoseCom, 5) + roundTo(next.memberTotalWinLoseCom, 5),

                    agentWinLose: roundTo(preData.agentWinLose, 5) + roundTo(next.agentWinLose, 5),
                    agentWinLoseCom: roundTo(preData.agentWinLoseCom, 5) + roundTo(next.agentWinLoseCom, 5),
                    agentTotalWinLoseCom: roundTo(preData.agentTotalWinLoseCom, 5) + roundTo(next.agentTotalWinLoseCom, 5),

                    masterAgentWinLose: roundTo(preData.masterAgentWinLose, 5) + roundTo(next.masterAgentWinLose, 5),
                    masterAgentWinLoseCom: roundTo(preData.masterAgentWinLoseCom, 5) + roundTo(next.masterAgentWinLoseCom, 5),
                    masterAgentTotalWinLoseCom: roundTo(preData.masterAgentTotalWinLoseCom, 5) + roundTo(next.masterAgentTotalWinLoseCom, 5),

                    seniorWinLose: roundTo(preData.seniorWinLose, 5) + roundTo(next.seniorWinLose, 5),
                    seniorWinLoseCom: roundTo(preData.seniorWinLoseCom, 5) + roundTo(next.seniorWinLoseCom, 5),
                    seniorTotalWinLoseCom: roundTo(preData.seniorTotalWinLoseCom, 5) + roundTo(next.seniorTotalWinLoseCom, 5),

                    shareHolderWinLose: roundTo(preData.shareHolderWinLose, 5) + roundTo(next.shareHolderWinLose, 5),
                    shareHolderWinLoseCom: roundTo(preData.shareHolderWinLoseCom, 5) + roundTo(next.shareHolderWinLoseCom, 5),
                    shareHolderTotalWinLoseCom: roundTo(preData.shareHolderTotalWinLoseCom, 5) + roundTo(next.shareHolderTotalWinLoseCom, 5),

                    companyWinLose: roundTo(preData.companyWinLose, 5) + roundTo(next.companyWinLose, 5),
                    companyWinLoseCom: roundTo(preData.companyWinLoseCom, 5) + roundTo(next.companyWinLoseCom, 5),
                    companyTotalWinLoseCom: roundTo(preData.companyTotalWinLoseCom, 5) + roundTo(next.companyTotalWinLoseCom, 5),

                    superAdminWinLose: roundTo(preData.superAdminWinLose, 5) + roundTo(next.superAdminWinLose, 5),
                    superAdminWinLoseCom: roundTo(preData.superAdminWinLoseCom, 5) + roundTo(next.superAdminWinLoseCom, 5),
                    superAdminTotalWinLoseCom: roundTo(preData.superAdminTotalWinLoseCom, 5) + roundTo(next.superAdminTotalWinLoseCom, 5)
                }
            } else if (preData) {
                return preData;
            } else if (next) {
                return next;
            }

        });

    });


}

module.exports = {
    getWinLoseReport,
    getWinLoseReportMember,
    getWinLoseMatch,
    getWinLoseReportMember2,
    sendToQueue,
    sendToQueueBatch,
    sendToQueueNew,
    queryMixUseWinLose,
    queryMixUseWinLoseMatch,
    queryMixUsePaymentReport,
    getAmbReport,
    isElasticEnabled
}
