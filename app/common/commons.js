const FootballModel     = require('../models/football.model');
const FootballSBOModel  = require('../models/footballSBO.model');
const BasketballModel   = require('../models/basketball.model');
const MuaythaiSBOModel  = require('../models/muaythaiSBO.model');
const MuayThaiLocalModel= require('../models/muaythaiLocal.model');
const BasketballSBOModel= require('../models/basketballSBO.model');
const TennisSBOModel    = require('../models/tennisSBO.model');
const ESportSBOModel    = require('../models/eSportSBO.model');
const TableTennisSBOModel= require('../models/tableTennisSBO.model');
const VolleyballSBOModel= require('../models/volleyballSBO.model');
const RugbySBOModel     = require('../models/rugbySBO.model');
const SpacialOddsModel  = require('../models/spacialOdds.model');
const AdjustmentByMatch = require('../models/oddAdjustmentByMatch.model');

const Redis             = require('../controllers/sbo/1_common/1_redis/1_1_redis');
const parallel          = require("async/parallel");
const waterfall         = require("async/waterfall");
const httpRequest       = require('request');
const moment            = require('moment');
const RedisService      = require('./redisService');
const async             = require("async");
const request           = require('request');
const _                 = require('underscore');
const roundTo           = require('round-to');
const MemberService     = require('./memberService');

const CACHE_IP = process.env.CACHE_IP || 'isn-backend.api-hub.com';
const URL_CACHE_HDP_SINGLE_MATCH = `http://${CACHE_IP}:8001/cache/hdp_single_match`;
const URL_CACHE_LIVE_SINGLE_MATCH = `http://${CACHE_IP}:8001/cache/live_single_match`;
const URL_CACHE_HDP_SINGLE_MATCH_BASKET_BALL = `http://${CACHE_IP}:8001/basketball/cache/hdp_single_match`;
const HUNDRED = 100;

const URL_HDP = `http://${CACHE_IP}:8001/cache/hdp`;
const URL_LIVE = `http://${CACHE_IP}:8001/cache/live`;
const CLIENT_NAME        = process.env.CLIENT_NAME || 'SPORTBOOK88';
module.exports = {
    cal_oe,
    cal_ou,
    cal_ah,
    cal_bp_live_single_match,
    cal_bp_live_single_match_sbo,
    cal_bp_live_single_match_sbo_special,
    cal_bp_hdp_single_match,
    cal_bp_hdp_single_match_sbo,
    cal_bp_hdp_single_match_muaythai_sbo,
    cal_bp_hdp_single_match_basketball_sbo,
    cal_bp_hdp_single_match_muaythai_local,
    cal_bp_hdp_single_match_eSport_sbo,
    cal_bp_hdp_single_match_tennis_sbo,
    cal_bp_hdp_single_match_table_tennis_sbo,
    cal_bp_hdp_single_match_volleyball_sbo,
    cal_bp_hdp_single_match_rugby_sbo,
    cal_bp_live_single_match_muaythai_local,
    cal_bp_hdp_single_match_spacial_odds,
    getBetObjectByKey,
    switch_type,
    switch_type_adjustmentByMatchList,
    switch_min_max,
    cal_price,
    cal_x12,
    cal_ml,
    cal_bp_parlay,
    cal_bp_parlay_sbo,
    cal_bp_mix_parlay_sbo,
    cal_bp_live_mix_parlay_sbo,
    convertPriceToEU,
    cal_eu_ou,
    cal_eu_oe,
    cal_eu_ah,
    cal_eu_ou_sbo,
    cal_eu_oe_sbo,
    cal_eu_ah_sbo,
    cal_eu_ml,
    switch_type_parlay,
    switch_type_parlay_sbo,
    call_master_on_ticket,
    call_master_on_ticket_sbo,
    call_master_on_ticket_sbo2,
    call_master_on_ticket_mix_parlay_sbo,
    call_master_on_ticket_mix_step_sbo,
    call_master_on_ticket_sbo_special,
    cal_commission_type,
    calculateBetPriceByTime,
    calculateBetPrice,
    call_ods_hdp,
    cal_bp_live_single_match_robot,
    calculateBetPriceForBasketball,
    getBetObjectByKeyForBasketball,
    cal_bp_hdp_single_match_for_basket_ball,
    switch_type_for_basket_ball,
    switch_min_max_for_basket_ball,
    cal_bp_parlay_for_basket_ball,
    switch_type_parlay_for_basket_ball,
    configMatch,
    configMatchMixStep,
    rule,
    rulePriceHDP,
    getMatchDetails,
    cal_bp_hdp_single_match_sbo_mix_step,
    cal_bp_live_single_match_sbo_mix_step,
    cal_bp_hdp_single_match_basketball_sbo_mix_step,
    cal_bp_hdp_single_match_muaythai_sbo_mix_step,
    oddTimeSeries,
    oddTimeSeriesByMatchId
};

function oddTimeSeries(callback)  {
    Redis.getByKey('sboTodaySnap', (error, result) => {
        if (error) {
            callback(null, []);
        } else {
            callback(null, result);
        }
    })
}

function oddTimeSeriesByMatchId(matchId, callback)  {
    Redis.getByKey('sboTodaySnap', (error, result) => {
        if (error) {
            callback(null, []);
        } else {
            const filterd = _.chain(result)
                .filter(obj => {
                    return _.isEqual(matchId, obj.id)
                })
                .value();
            callback(null, filterd);
        }
    })
}

//Mix Step
function cal_bp_hdp_single_match_sbo_mix_step(matchId, oddType, oddKey, commissionSetting, oddAdjust, memberId, key, callback)  {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const league_key = parseInt(_.first(matchId.split(':')));
    const match_key  = parseInt(_.last(matchId.split(':')));

    let indexRound;
    const value = oddKey;
    const prefix= oddType;
    const prefixLowerCase = prefix.toLowerCase();

    console.log('value : ', value);
    console.log('prefix : ', prefix);
    console.log('key : ', key);

    parallel([
            (callback) => {
                FootballSBOModel.find({
                    k: league_key
                })
                    .lean()
                    // .sort('sk')
                    .select('k n r rp')
                    .exec((error, response) => {
                        if (error) {
                            callback(error, null);
                        } else {
                            callback(null, response);
                        }
                    });
            },

            (callback) => {
                Redis.getByKey('sboToday', (error, response) => {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            },

            (callback) => {
                MemberService.findById(memberId, 'username', (error, response)=>{
                    MemberService.getOddAdjustment(response.username, (err, result) => {
                        if (err) {
                            callback(error, null);
                        } else {
                            callback(null, result);
                        }
                    });
                });
            },
        ],
        (error, parallelResult) => {
            if (error) {
                callback(error, null);
            } else {
                const [
                    footballSBOModel,
                    sboToday,
                    oddAdjust2
                ] = parallelResult;

                const sboObjLeague = _.chain(footballSBOModel)
                    .filter(sboLeague => {
                        return _.isEqual(parseInt(sboLeague.k), league_key)
                    })
                    .first()
                    .value();

                console.log(JSON.stringify(sboObjLeague));
                if (!_.isUndefined(sboObjLeague)) {

                    let matchDate = new Date();
                    let matchN = {};
                    let indexRound;
                    let bpArray = [];

                    let bp = null;
                    try {
                        bp = _.first(_.chain(sboToday)
                            .flatten(true)
                            .findWhere(JSON.parse(`{"k":${league_key}}`))
                            .pick('m')
                            .map(obj => {
                                return obj;
                            })
                            .flatten(true)
                            .findWhere(JSON.parse(`{"id":"${league_key}:${match_key}"}`))
                            .tap(obj => {
                                matchDate = obj.d;
                                matchN = obj.n;
                                bpArray = obj.bp;
                            })
                            .pick('bp')
                            .map(obj => {
                                // console.log('obj', obj);
                                return obj;
                            })
                            .flatten(true)
                            .map(obj => {
                                // console.log('obj', obj);
                                return obj[prefixLowerCase];
                            })
                            .filter(obj => {
                                return !_.isUndefined(obj);
                            })
                            .filter((obj, index) => {
                                try {
                                    // console.log('SSSS ', JSON.stringify(obj));
                                    const result = _.isEqual(obj[key].toLowerCase(), value);
                                    if (result) {
                                        indexRound = index;
                                        // console.log('indexRound => ', indexRound);
                                    }
                                    return result;
                                } catch (e) {
                                    console.error(e);
                                    return false;
                                }
                            })
                            .map(obj => {
                                const bpObj = {};
                                bpObj[prefixLowerCase] = obj;
                                return bpObj
                            })
                            .value());
                    } catch (e) {
                        console.error(e);
                        callback(5003, null);
                    }

                    if( _.isUndefined(bp) || _.isNull(bp)){
                        callback(5003, null);
                    }

                    let model_result = {
                        matchId: `${league_key}:${match_key}`,
                        league_name: sboObjLeague.n.en,
                        league_name_n: sboObjLeague.n,
                        name: matchN,
                        date: matchDate
                    };

                    let odd = _.filter(oddAdjust2, obj => {
                        return _.isEqual(obj.matchId, model_result.matchId);
                    });
                    // console.log(JSON.stringify(bpArray[indexRound]));
                    switch_type(
                        prefixLowerCase,
                        bpArray[indexRound],
                        sboObjLeague.rp.hdp,
                        model_result,
                        configMatchMixStep("8"),
                        commissionSetting,
                        odd,
                        (err, result)=>{
                            if(err){
                                callback(err, null);
                            } else {
                                callback(null, result);
                            }
                        });
                } else {

                    httpRequest.get(
                        `http://lb-sbo-master.api-hub.com/external/sboLeague/${league_key}`,
                        (error, result) => {
                            if (error) {
                                console.error(error);
                            } else {
                                const model = JSON.parse(result.body).result;
                                const footballSBOModel = new FootballSBOModel(model);
                                footballSBOModel.save((error, data) => {
                                    if (error) {
                                        console.error(error);
                                    } else {
                                        console.log(data);
                                    }
                                });
                            }
                        });
                    callback(5003, null);
                }
            }
        });
}
function cal_bp_live_single_match_sbo_mix_step(matchId, oddType, oddKey, commissionSetting, oddAdjust, key, callback)  {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const league_key = parseInt(_.first(matchId.split(':')));
    const match_key  = parseInt(_.last(matchId.split(':')));

    let indexRound;
    const value = oddKey;
    const prefix= oddType;
    const prefixLowerCase = prefix.toLowerCase();
    const predicate = JSON.parse(`{"${key}":"${value}"}`);
    console.log('value : ', value);
    console.log('prefix : ', prefix);
    console.log('key : ', key);

    parallel([
            (callback) => {
                FootballSBOModel.find({
                    k: league_key
                })
                    .lean()
                    // .sort('sk')
                    .select('k n r rl rp')
                    .exec((error, response) => {
                        if (error) {
                            callback(error, null);
                        } else {
                            callback(null, response);
                        }
                    });
            },

            (callback) => {
                Redis.getByKey('sboLiveStep', (error, response) => {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            }
        ],
        (error, parallelResult) => {
            if (error) {
                callback(error, null);
            } else {
                const [
                    footballSBOModel,
                    sboLive
                ] = parallelResult;

                const sboObjLeague = _.chain(footballSBOModel)
                    .filter(sboLeague => {
                        return _.isEqual(parseInt(sboLeague.k), league_key)
                    })
                    .first()
                    .value();

                if (_.isUndefined(sboObjLeague)) {
                    httpRequest.get(
                        `http://lb-sbo-master.api-hub.com/external/sboLeague/${league_key}`,
                        (error, result) => {
                            if (error) {
                                console.error(error);
                            } else {
                                const model = JSON.parse(result.body).result;
                                const footballSBOModel = new FootballSBOModel(model);
                                footballSBOModel.save((error, data) => {
                                    if (error) {
                                        console.error(error);
                                    } else {
                                        console.log(data);
                                    }
                                });
                            }
                        });

                    callback(5003, null);
                } else {
                    let matchDate = new Date();
                    let matchN = {};
                    let detail = {};
                    let indexRound;
                    let bpArray = [];
                    let isHalfTime = false;
                    let h = 0;
                    let a = 0;
                    let bp = null;

                    try {
                        bp = _.first(_.chain(sboLive)
                            .flatten(true)
                            .findWhere(JSON.parse(`{"k":${league_key}}`))
                            .pick('m')
                            .map(obj => {
                                return obj;
                            })
                            .flatten(true)
                            .findWhere(JSON.parse(`{"id":"${league_key}:${match_key}"}`))
                            .tap(obj => {
                                if (!_.isUndefined(obj)) {
                                    console.log('obj ', JSON.stringify(obj));
                                    h = obj.i.h;
                                    a = obj.i.a;
                                    matchDate = obj.d;
                                    matchN = obj.n;
                                    bpArray = obj.bpl;
                                    detail.h = h;
                                    detail.a = a;
                                    detail.s = "";
                                    detail.hrc = obj.i.hrc;
                                    detail.arc = obj.i.arc;
                                    detail.mt = obj.i.mt;
                                    detail.lt = obj.i.lt;
                                    isHalfTime = obj.i.isHalfTime;
                                    detail.rowID = _.isUndefined(obj.i.rowID) ? "" : obj.i.rowID;
                                }
                            })
                            .pick('bpl')
                            .map(obj => {
                                return obj;
                            })
                            .flatten(true)
                            .map(obj => {
                                // console.log('obj', JSON.stringify(obj));
                                return obj[prefixLowerCase]
                            })
                            .filter(obj => {
                                return !_.isUndefined(obj);
                            })
                            .filter((obj, index) => {
                                try {
                                    // console.log('SSSS ', JSON.stringify(obj));
                                    const result = _.isEqual(obj[key].toLowerCase(), value);
                                    if (result) {
                                        indexRound = index;
                                        // console.log('indexRound => ', indexRound);
                                    }
                                    return result;
                                } catch (e) {
                                    console.error(e);
                                    return false;
                                }
                            })
                            .map(obj => {
                                const bpObj = {};
                                bpObj[prefixLowerCase] = obj;
                                return bpObj
                            })
                            .value());
                    } catch (e) {
                        console.error(e);
                        callback(5003, null);
                        return;
                    }

                    // console.log('====================');
                    // console.log(bp);
                    // console.log('====================');
                    // console.log('====================');
                    // console.log(bpArray);
                    // console.log('====================');

                    if( _.isUndefined(bp) || _.isNull(bp)){
                        callback(5003, null);
                        return;
                    }

                    let model_result = {
                        matchId: `${league_key}:${match_key}`,
                        league_name: sboObjLeague.n.en,
                        league_name_n: sboObjLeague.n,
                        name: matchN,
                        date: matchDate,
                        isHalfTime: isHalfTime,
                        detail: detail,
                        score: {
                            h: h,
                            a: a
                        }
                    };

                    console.log('model_result ',JSON.stringify(model_result));
                    let odd = _.filter(oddAdjust, obj => {
                        return _.isEqual(obj.matchId, model_result.matchId);
                    });

                    switch_type(
                        prefixLowerCase,
                        bpArray[indexRound],
                        sboObjLeague.rp.live,
                        model_result,
                        configMatchMixStep('8'),
                        commissionSetting,
                        odd,
                        (err, result)=>{
                            if(err){
                                callback(err, null);
                            } else {
                                callback(null, model_result);
                            }
                        });
                }
            }
        });
}
function cal_bp_hdp_single_match_basketball_sbo_mix_step(matchId, oddType, oddKey, commissionSetting, oddAdjust, memberId, matchType, key, callback)  {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const league_key = parseInt(_.first(matchId.split(':')));
    const match_key  = parseInt(_.last(matchId.split(':')));

    let indexRound;
    const value = oddKey;
    const prefix= oddType;
    const prefixLowerCase = prefix.toLowerCase();
    const predicate = JSON.parse(`{"${key}":"${value}"}`);

    console.log('value : ', value);
    console.log('prefix : ', prefix);
    console.log('key : ', key);

    parallel([
            (callback) => {
                Redis.getByKey(`BasketballSBOModel${CLIENT_NAME}`, (error, result) => {
                    if (error || _.isNull(result) || _.isUndefined(result)) {
                        BasketballSBOModel.find({})
                            .lean()
                            // .sort('sk')
                            .select('k n r')
                            .exec((error, response) => {
                                if (error) {
                                    callback(error, null);
                                } else {
                                    Redis.setByKey(`BasketballSBOModel${CLIENT_NAME}`, response, (error, result) => {

                                    });
                                    callback(null, response);
                                }
                            });
                    } else {
                        callback(null, result);
                    }
                });
            },

            (callback) => {
                Redis.getByKey('sboTodayBasketball', (error, response) => {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            },

            (callback) => {
                MemberService.findById(memberId, 'username', (error, response)=>{
                    MemberService.getOddAdjustment(response.username, (err, result) => {
                        if (err) {
                            callback(error, null);
                        } else {
                            callback(null, result);
                        }
                    });
                });
            },
        ],
        (error, parallelResult) => {
            if (error) {
                callback(error, null);
            } else {
                const [
                    basketballSBOModel,
                    sboTodayBasketball,
                    oddAdjust2
                ] = parallelResult;

                const sboObjLeague = _.chain(basketballSBOModel)
                    .filter(sboLeague => {
                        return _.isEqual(parseInt(sboLeague.k), league_key)
                    })
                    .first()
                    .value();

                if (!_.isUndefined(sboObjLeague)) {
                    let matchDate = new Date();
                    let matchN = {};
                    let indexRound;
                    let bpArray = [];
                    let rule = sboObjLeague.r;

                    let bp = null;
                    try {
                        bp = _.first(_.chain(sboTodayBasketball)
                            .flatten(true)
                            .findWhere(JSON.parse(`{"k":${league_key}}`))
                            .pick('m')
                            .map(obj => {
                                return obj;
                            })
                            .flatten(true)
                            .findWhere(JSON.parse(`{"id":"${league_key}:${match_key}"}`))
                            .tap(obj => {
                                matchDate = obj.d;
                                matchN = obj.n;
                                bpArray = obj.bp;
                            })
                            .pick('bp')
                            .map(obj => {
                                // console.log('obj', obj);
                                return obj;
                            })
                            .flatten(true)
                            .map(obj => {
                                // console.log('obj', obj);
                                return obj[prefixLowerCase];
                            })
                            .filter(obj => {
                                return !_.isUndefined(obj);
                            })
                            .filter((obj, index) => {
                                try {
                                    // console.log('SSSS ', JSON.stringify(obj));
                                    const result = _.isEqual(obj[key].toLowerCase(), value);
                                    if (result) {
                                        indexRound = index;
                                        // console.log('indexRound => ', indexRound);
                                    }
                                    return result;
                                } catch (e) {
                                    console.error(e);
                                    return false;
                                }
                            })
                            .map(obj => {
                                const bpObj = {};
                                bpObj[prefixLowerCase] = obj;
                                return bpObj
                            })
                            .value());
                    } catch (e) {
                        console.error(e);
                        callback(5003, null);
                    }

                    // console.log('====================');
                    // console.log(bp);
                    // console.log('====================');
                    // console.log('====================');
                    // console.log(bpArray);
                    // console.log('====================');

                    if( _.isUndefined(bp) || _.isNull(bp)){
                        callback(5003, null);
                    }

                    let model_result = {
                        matchId: `${league_key}:${match_key}`,
                        league_name: sboObjLeague.n.en,
                        league_name_n: sboObjLeague.n,
                        name: matchN,
                        date: matchDate
                    };

                    let odd = _.filter(oddAdjust2, obj => {
                        return _.isEqual(obj.matchId, model_result.matchId);
                    });
                    // console.log(JSON.stringify(bpArray[indexRound]));
                    switch_type(
                        prefixLowerCase,
                        bpArray[indexRound],
                        rulePriceHDP(),
                        model_result,
                        configMatchMixStep("8"),
                        commissionSetting,
                        odd,
                        (err, result)=>{
                            if(err){
                                callback(err, null);
                            } else {
                                callback(null, result);
                            }
                        });

                } else {
                    httpRequest.get(
                        `http://lb-sbo-master.api-hub.com/basketball/external/sboLeague/${league_key}`,
                        (error, result) => {
                            if (error) {
                                console.error(error);
                            } else {
                                const model = JSON.parse(result.body).result;
                                const muayThaiModel = new MuaythaiSBOModel(model);
                                muayThaiModel.save((error, data) => {
                                    if (error) {
                                        console.error(error);
                                    } else {
                                        console.log(data);
                                    }
                                });
                            }
                        });
                    callback(5003, null);
                }
            }
        });
}
function cal_bp_hdp_single_match_muaythai_sbo_mix_step(matchId, oddType, oddKey, commissionSetting, oddAdjust, memberId, key, callback)  {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const league_key = parseInt(_.first(matchId.split(':')));
    const match_key  = parseInt(_.last(matchId.split(':')));

    let indexRound;
    const value = oddKey;
    const prefix= oddType;
    const prefixLowerCase = prefix.toLowerCase();
    const predicate = JSON.parse(`{"${key}":"${value}"}`);

    console.log('value  : ', value);
    console.log('prefix : ', prefix);
    console.log('key    : ', key);

    parallel([
            (callback) => {
                Redis.getByKey(`MuaythaiSBOModel${CLIENT_NAME}`, (error, result) => {
                    if (error || _.isNull(result) || _.isUndefined(result)) {
                        MuaythaiSBOModel.find({})
                            .lean()
                            // .sort('sk')
                            .select('k n r')
                            .exec((error, response) => {
                                if (error) {
                                    callback(error, null);
                                } else {
                                    Redis.setByKey(`MuaythaiSBOModel${CLIENT_NAME}`, response, (error, result) => {

                                    });
                                    callback(null, response);
                                }
                            });
                    } else {
                        callback(null, result);
                    }
                });
            },

            (callback) => {
                Redis.getByKey('sboTodayMuayThai', (error, response) => {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            },

            (callback) => {
                MemberService.findById(memberId, 'username', (error, response)=>{
                    MemberService.getOddAdjustment(response.username, (err, result) => {
                        if (err) {
                            callback(error, null);
                        } else {
                            callback(null, result);
                        }
                    });
                });
            },
        ],
        (error, parallelResult) => {
            if (error) {
                callback(error, null);
            } else {
                const [
                    muayThaiModel,
                    sboTodayMuayThai,
                    oddAdjust2
                ] = parallelResult;

                const sboObjLeague = _.chain(muayThaiModel)
                    .filter(sboLeague => {
                        return _.isEqual(parseInt(sboLeague.k), league_key)
                    })
                    .first()
                    .value();

                if (!_.isUndefined(sboObjLeague)) {
                    let matchDate = new Date();
                    let matchN = {};
                    let indexRound;
                    let bpArray = [];

                    let bp = null;
                    try {
                        bp = _.first(_.chain(sboTodayMuayThai)
                            .flatten(true)
                            .findWhere(JSON.parse(`{"k":${league_key}}`))
                            .pick('m')
                            .map(obj => {
                                return obj;
                            })
                            .flatten(true)
                            .findWhere(JSON.parse(`{"id":"${league_key}:${match_key}"}`))
                            .tap(obj => {
                                matchDate = obj.d;
                                matchN = obj.n;
                                bpArray = obj.bp;
                            })
                            .pick('bp')
                            .map(obj => {
                                // console.log('obj', obj);
                                return obj;
                            })
                            .flatten(true)
                            .map(obj => {
                                // console.log('obj', obj);
                                return obj[prefixLowerCase];
                            })
                            .filter(obj => {
                                return !_.isUndefined(obj);
                            })
                            .filter((obj, index) => {
                                try {
                                    // console.log('SSSS ', JSON.stringify(obj));
                                    const result = _.isEqual(obj[key].toLowerCase(), value);
                                    if (result) {
                                        indexRound = index;
                                        // console.log('indexRound => ', indexRound);
                                    }
                                    return result;
                                } catch (e) {
                                    console.error(e);
                                    return false;
                                }
                            })
                            .map(obj => {
                                const bpObj = {};
                                bpObj[prefixLowerCase] = obj;
                                return bpObj
                            })
                            .value());
                    } catch (e) {
                        console.error(e);
                        callback(5003, null);
                    }

                    // console.log('====================');
                    // console.log(bp);
                    // console.log('====================');
                    // console.log('====================');
                    // console.log(bpArray);
                    // console.log('====================');

                    if( _.isUndefined(bp) || _.isNull(bp)){
                        callback(5003, null);
                    }

                    let model_result = {
                        matchId: `${league_key}:${match_key}`,
                        league_name: sboObjLeague.n.en,
                        league_name_n: sboObjLeague.n,
                        name: matchN,
                        date: matchDate
                    };

                    let odd = _.filter(oddAdjust2, obj => {
                        return _.isEqual(obj.matchId, model_result.matchId);
                    });

                    switch_type(
                        prefixLowerCase,
                        bpArray[indexRound],
                        rulePriceHDP(),
                        model_result,
                        configMatchMixStep("3"),
                        commissionSetting,
                        odd,
                        (err, result)=>{
                            if(err){
                                callback(err, null);
                            } else {
                                if(err){
                                    callback(err, null);
                                } else {
                                    callback(null, result);
                                }
                            }
                        });

                } else {
                    httpRequest.get(
                        `http://lb-sbo-master.api-hub.com/muaythai/external/sboLeague/${league_key}`,
                        (error, result) => {
                            if (error) {
                                console.error(error);
                            } else {
                                const model = JSON.parse(result.body).result;
                                const muayThaiModel = new MuaythaiSBOModel(model);
                                muayThaiModel.save((error, data) => {
                                    if (error) {
                                        console.error(error);
                                    } else {
                                        console.log(data);
                                    }
                                });
                            }
                        });
                    callback(5003, null);
                }
            }
        });
}

function getMatchDetails(matchId,callback) {
    console.log('getMatchDetails : ',matchId)
    const id = matchId;

    const league_key = parseInt(_.first(id.split(':')));
    const match_key = parseInt(_.last(id.split(':')));

    parallel([
            (callback) => {
                Redis.getByKey('sboLive', (error, result) => {
                    if (error) {
                        callback(null, false);
                    } else {
                        callback(null, result);
                    }
                })
            }
        ],
        (error, parallelResult) => {
            if (error) {
                callback(error,null);
            } else {
                const [
                    sboLive
                ] = parallelResult;

                const match = _.chain(sboLive)
                    .flatten(true)
                    .findWhere(JSON.parse(`{"k":${league_key}}`))
                    .pick('m')
                    .map(obj => {
                        return obj;
                    })
                    .flatten(true)
                    .findWhere(JSON.parse(`{"id":"${league_key}:${match_key}"}`))
                    .value();


                if (_.isUndefined(match)) {
                    callback('Match id[${id}] not found.',null);
                    return;
                }

                const result = {};
                result.i = match.i;
                result.isWaitingForScore = _.isEmpty(match.bpl);
                callback(null,result);
            }
        });
}


function cal_bp_hdp_single_match_basketball_sbo(matchId, oddType, oddKey, commissionSetting, oddAdjust, memberId, matchType, key, callback)  {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const league_key = parseInt(_.first(matchId.split(':')));
    const match_key  = parseInt(_.last(matchId.split(':')));

    let indexRound;
    const value = oddKey;
    const prefix= oddType;
    const prefixLowerCase = prefix.toLowerCase();
    const predicate = JSON.parse(`{"${key}":"${value}"}`);

    console.log('value : ', value);
    console.log('prefix : ', prefix);
    console.log('key : ', key);

    parallel([
            (callback) => {
                MemberService.getLimitSetting(memberId, (errLimitSetting, dataLimitSetting)=>{
                    if(errLimitSetting){
                        callback(errLimitSetting, null);
                    } else {
                        callback(null, dataLimitSetting.limitSetting);
                    }
                });
            },

            (callback) => {
                Redis.getByKey(`BasketballSBOModel${CLIENT_NAME}`, (error, result) => {
                    if (error || _.isNull(result) || _.isUndefined(result)) {
                        BasketballSBOModel.find({})
                            .lean()
                            // .sort('sk')
                            .select('k n r')
                            .exec((error, response) => {
                                if (error) {
                                    callback(error, null);
                                } else {
                                    Redis.setByKey(`BasketballSBOModel${CLIENT_NAME}`, response, (error, result) => {

                                    });
                                    callback(null, response);
                                }
                            });
                    } else {
                        callback(null, result);
                    }
                });
            },

            (callback) => {
                Redis.getByKey('sboTodayBasketball', (error, response) => {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            },

            (callback) => {
                MemberService.findById(memberId, 'username', (error, response)=>{
                    MemberService.getOddAdjustment(response.username, (err, result) => {
                        if (err) {
                            callback(error, null);
                        } else {
                            callback(null, result);
                        }
                    });
                });
            },
        ],
        (error, parallelResult) => {
            if (error) {
                callback(error, null);
            } else {
                const [
                    limitSetting,
                    basketballSBOModel,
                    sboTodayBasketball,
                    oddAdjust2
                ] = parallelResult;

                const sboObjLeague = _.chain(basketballSBOModel)
                    .filter(sboLeague => {
                        return _.isEqual(parseInt(sboLeague.k), league_key)
                    })
                    .first()
                    .value();

                if (!_.isUndefined(sboObjLeague)) {
                    let matchDate = new Date();
                    let matchN = {};
                    let indexRound;
                    let bpArray = [];
                    let rule = sboObjLeague.r;

                    let bp = null;
                    try {
                        bp = _.first(_.chain(sboTodayBasketball)
                            .flatten(true)
                            .findWhere(JSON.parse(`{"k":${league_key}}`))
                            .pick('m')
                            .map(obj => {
                                return obj;
                            })
                            .flatten(true)
                            .findWhere(JSON.parse(`{"id":"${league_key}:${match_key}"}`))
                            .tap(obj => {
                                matchDate = obj.d;
                                matchN = obj.n;
                                bpArray = obj.bp;
                            })
                            .pick('bp')
                            .map(obj => {
                                // console.log('obj', obj);
                                return obj;
                            })
                            .flatten(true)
                            .map(obj => {
                                // console.log('obj', obj);
                                return obj[prefixLowerCase];
                            })
                            .filter(obj => {
                                return !_.isUndefined(obj);
                            })
                            .filter((obj, index) => {
                                try {
                                    // console.log('SSSS ', JSON.stringify(obj));
                                    const result = _.isEqual(obj[key].toLowerCase(), value);
                                    if (result) {
                                        indexRound = index;
                                        // console.log('indexRound => ', indexRound);
                                    }
                                    return result;
                                } catch (e) {
                                    console.error(e);
                                    return false;
                                }
                            })
                            .map(obj => {
                                const bpObj = {};
                                bpObj[prefixLowerCase] = obj;
                                return bpObj
                            })
                            .value());
                    } catch (e) {
                        console.error(e);
                        callback(5003, null);
                    }

                    // console.log('====================');
                    // console.log(bp);
                    // console.log('====================');
                    // console.log('====================');
                    // console.log(bpArray);
                    // console.log('====================');

                    if( _.isUndefined(bp) || _.isNull(bp)){
                        callback(5003, null);
                    }

                    let model_result = {
                        matchId: `${league_key}:${match_key}`,
                        league_name: sboObjLeague.n.en,
                        league_name_n: sboObjLeague.n,
                        name: matchN,
                        date: matchDate
                    };

                    let odd = _.filter(oddAdjust2, obj => {
                        return _.isEqual(obj.matchId, model_result.matchId);
                    });
                    // console.log(JSON.stringify(bpArray[indexRound]));
                    switch_type(
                        prefixLowerCase,
                        bpArray[indexRound],
                        rulePriceHDP(),
                        model_result,
                        configMatch(),
                        commissionSetting,
                        odd,
                        (err, result)=>{
                            if(err){
                                callback(err, null);
                            } else {
                                let result_new_array = [result];
                                let betPrice = _.chain(result_new_array)
                                    .flatten(true)
                                    .map(betPrice => {
                                        return _.findWhere(betPrice, predicate);
                                    })
                                    .reject(betPrice => {
                                        return _.isUndefined(betPrice)
                                    })
                                    .map(betPrice => {
                                        return getBetObjectByKey(prefix, key, betPrice);
                                    })
                                    .first()
                                    .value();

                                switch_min_max(
                                    prefixLowerCase,
                                    rule,
                                    result,
                                    (err, dataResult)=>{
                                        dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.hdpOuOe.maxPerBet);
                                        calculateBetPriceByTime(dataResult.max, now, new Date(matchDate), (err, data)=>{
                                            dataResult.max = data;
                                        });
                                        calculateBetPrice(dataResult.max, indexRound, (err, data)=>{
                                            dataResult.max = data;
                                        });
                                        dataResult.maxPerMatch = sboObjLeague.r.pm;
                                        console.log('dataResult ', JSON.stringify(dataResult));
                                        callback(null, dataResult);
                                    });
                            }
                        });

                } else {
                    httpRequest.get(
                        `http://lb-sbo-master.api-hub.com/basketball/external/sboLeague/${league_key}`,
                        (error, result) => {
                            if (error) {
                                console.error(error);
                            } else {
                                const model = JSON.parse(result.body).result;
                                const muayThaiModel = new MuaythaiSBOModel(model);
                                muayThaiModel.save((error, data) => {
                                    if (error) {
                                        console.error(error);
                                    } else {
                                        console.log(data);
                                    }
                                });
                            }
                        });
                    callback(5003, null);
                }
            }
        });
}

function cal_bp_hdp_single_match_muaythai_sbo(matchId, oddType, oddKey, commissionSetting, oddAdjust, memberId, matchType, key, callback)  {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const league_key = parseInt(_.first(matchId.split(':')));
    const match_key  = parseInt(_.last(matchId.split(':')));

    const value = oddKey;
    const prefix= oddType;
    const prefixLowerCase = prefix.toLowerCase();
    const predicate = JSON.parse(`{"${key}":"${value}"}`);

    console.log('value : ', value);
    console.log('prefix : ', prefix);
    console.log('key : ', key);

    parallel([
            (callback) => {
                MemberService.getLimitSetting(memberId, (errLimitSetting, dataLimitSetting)=>{
                    if(errLimitSetting){
                        callback(errLimitSetting, null);
                    } else {
                        callback(null, dataLimitSetting.limitSetting);
                    }
                });
            },

            (callback) => {
                Redis.getByKey(`MuaythaiSBOModel${CLIENT_NAME}`, (error, result) => {
                    if (error || _.isNull(result) || _.isUndefined(result)) {
                        MuaythaiSBOModel.find({})
                            .lean()
                            // .sort('sk')
                            .select('k n r')
                            .exec((error, response) => {
                                if (error) {
                                    callback(error, null);
                                } else {
                                    Redis.setByKey(`MuaythaiSBOModel${CLIENT_NAME}`, response, (error, result) => {

                                    });
                                    callback(null, response);
                                }
                            });
                    } else {
                        callback(null, result);
                    }
                });
            },

            (callback) => {
                Redis.getByKey('sboTodayMuayThai', (error, response) => {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            },

            (callback) => {
                MemberService.findById(memberId, 'username', (error, response)=>{
                    MemberService.getOddAdjustment(response.username, (err, result) => {
                        if (err) {
                            callback(error, null);
                        } else {
                            callback(null, result);
                        }
                    });
                });
            },
        ],
        (error, parallelResult) => {
            if (error) {
                callback(error, null);
            } else {
                const [
                    limitSetting,
                    muayThaiModel,
                    sboTodayMuayThai,
                    oddAdjust2
                ] = parallelResult;

                const sboObjLeague = _.chain(muayThaiModel)
                    .filter(sboLeague => {
                        return _.isEqual(parseInt(sboLeague.k), league_key)
                    })
                    .first()
                    .value();

                if (!_.isUndefined(sboObjLeague)) {
                    let matchDate = new Date();
                    let matchN = {};
                    let indexRound;
                    let bpArray = [];
                    let rule = sboObjLeague.r;

                    let bp = null;
                    try {
                        bp = _.first(_.chain(sboTodayMuayThai)
                            .flatten(true)
                            .findWhere(JSON.parse(`{"k":${league_key}}`))
                            .pick('m')
                            .map(obj => {
                                return obj;
                            })
                            .flatten(true)
                            .findWhere(JSON.parse(`{"id":"${league_key}:${match_key}"}`))
                            .tap(obj => {
                                matchDate = obj.d;
                                matchN = obj.n;
                                bpArray = obj.bp;
                            })
                            .pick('bp')
                            .map(obj => {
                                // console.log('obj', obj);
                                return obj;
                            })
                            .flatten(true)
                            .map(obj => {
                                // console.log('obj', obj);
                                return obj[prefixLowerCase];
                            })
                            .filter(obj => {
                                return !_.isUndefined(obj);
                            })
                            .filter((obj, index) => {
                                try {
                                    // console.log('SSSS ', JSON.stringify(obj));
                                    const result = _.isEqual(obj[key].toLowerCase(), value);
                                    if (result) {
                                        indexRound = index;
                                        // console.log('indexRound => ', indexRound);
                                    }
                                    return result;
                                } catch (e) {
                                    console.error(e);
                                    return false;
                                }
                            })
                            .map(obj => {
                                const bpObj = {};
                                bpObj[prefixLowerCase] = obj;
                                return bpObj
                            })
                            .value());
                    } catch (e) {
                        console.error(e);
                        callback(5003, null);
                    }

                    // console.log('====================');
                    // console.log(bp);
                    // console.log('====================');
                    // console.log('====================');
                    // console.log(bpArray);
                    // console.log('====================');

                    if( _.isUndefined(bp) || _.isNull(bp)){
                        callback(5003, null);
                    }

                    let model_result = {
                        matchId: `${league_key}:${match_key}`,
                        league_name: sboObjLeague.n.en,
                        league_name_n: sboObjLeague.n,
                        name: matchN,
                        date: matchDate
                    };

                    let odd = _.filter(oddAdjust2, obj => {
                        return _.isEqual(obj.matchId, model_result.matchId);
                    });
                    // console.log(JSON.stringify(bpArray[indexRound]));
                    switch_type(
                        prefixLowerCase,
                        bpArray[indexRound],
                        rulePriceHDP(),
                        model_result,
                        configMatch(),
                        commissionSetting,
                        odd,
                        (err, result)=>{
                            if(err){
                                callback(err, null);
                            } else {
                                let result_new_array = [result];
                                let betPrice = _.chain(result_new_array)
                                    .flatten(true)
                                    .map(betPrice => {
                                        return _.findWhere(betPrice, predicate);
                                    })
                                    .reject(betPrice => {
                                        return _.isUndefined(betPrice)
                                    })
                                    .map(betPrice => {
                                        return getBetObjectByKey(prefix, key, betPrice);
                                    })
                                    .first()
                                    .value();

                                switch_min_max(
                                    prefixLowerCase,
                                    rule,
                                    result,
                                    (err, dataResult)=>{
                                        dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.hdpOuOe.maxPerBet);
                                        calculateBetPriceByTime(dataResult.max, now, new Date(matchDate), (err, data)=>{
                                            dataResult.max = data;
                                        });
                                        calculateBetPrice(dataResult.max, indexRound, (err, data)=>{
                                            dataResult.max = data;
                                        });
                                        dataResult.maxPerMatch = sboObjLeague.r.pm;
                                        console.log('dataResult ', JSON.stringify(dataResult));
                                        callback(null, dataResult);
                                    });
                            }
                        });

                } else {
                    httpRequest.get(
                        `http://lb-sbo-master.api-hub.com/muaythai/external/sboLeague/${league_key}`,
                        (error, result) => {
                            if (error) {
                                console.error(error);
                            } else {
                                const model = JSON.parse(result.body).result;
                                const muayThaiModel = new MuaythaiSBOModel(model);
                                muayThaiModel.save((error, data) => {
                                    if (error) {
                                        console.error(error);
                                    } else {
                                        console.log(data);
                                    }
                                });
                            }
                        });
                    callback(5003, null);
                }
            }
        });
}

function cal_bp_hdp_single_match_tennis_sbo(matchId, oddType, oddKey, commissionSetting, oddAdjust, memberId, matchType, key, callback)  {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const league_key = parseInt(_.first(matchId.split(':')));
    const match_key  = parseInt(_.last(matchId.split(':')));

    const value = oddKey;
    const prefix= oddType;
    const prefixLowerCase = prefix.toLowerCase();
    const predicate = JSON.parse(`{"${key}":"${value}"}`);

    console.log('value : ', value);
    console.log('prefix : ', prefix);
    console.log('key : ', key);

    parallel([
            (callback) => {
                MemberService.getLimitSetting(memberId, (errLimitSetting, dataLimitSetting)=>{
                    if(errLimitSetting){
                        callback(errLimitSetting, null);
                    } else {
                        callback(null, dataLimitSetting.limitSetting);
                    }
                });
            },

            (callback) => {
                TennisSBOModel.find({
                    k: league_key
                })
                    .lean()
                    // .sort('sk')
                    .select('k n r')
                    .exec((error, response) => {
                        if (error) {
                            callback(error, null);
                        } else {
                            callback(null, response);
                        }
                    });
            },

            (callback) => {
                Redis.getByKey('sboTodayTennis', (error, response) => {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            },

            (callback) => {
                MemberService.findById(memberId, 'username', (error, response)=>{
                    MemberService.getOddAdjustment(response.username, (err, result) => {
                        if (err) {
                            callback(error, null);
                        } else {
                            callback(null, result);
                        }
                    });
                });
            },
        ],
        (error, parallelResult) => {
            if (error) {
                callback(error, null);
            } else {
                const [
                    limitSetting,
                    tennisSBOModel,
                    sboTodayTennis,
                    oddAdjust2
                ] = parallelResult;

                const sboObjLeague = _.chain(tennisSBOModel)
                    .filter(sboLeague => {
                        return _.isEqual(parseInt(sboLeague.k), league_key)
                    })
                    .first()
                    .value();

                if (!_.isUndefined(sboObjLeague)) {
                    let matchDate = new Date();
                    let matchN = {};
                    let indexRound;
                    let bpArray = [];
                    let rule = sboObjLeague.r;

                    let bp = null;
                    try {
                        bp = _.first(_.chain(sboTodayTennis)
                            .flatten(true)
                            .findWhere(JSON.parse(`{"k":${league_key}}`))
                            .pick('m')
                            .map(obj => {
                                return obj;
                            })
                            .flatten(true)
                            .findWhere(JSON.parse(`{"id":"${league_key}:${match_key}"}`))
                            .tap(obj => {
                                matchDate = obj.d;
                                matchN = obj.n;
                                bpArray = obj.bp;
                            })
                            .pick('bp')
                            .map(obj => {
                                // console.log('obj', obj);
                                return obj;
                            })
                            .flatten(true)
                            .map(obj => {
                                // console.log('obj', obj);
                                return obj[prefixLowerCase];
                            })
                            .filter(obj => {
                                return !_.isUndefined(obj);
                            })
                            .filter((obj, index) => {
                                try {
                                    // console.log('SSSS ', JSON.stringify(obj));
                                    const result = _.isEqual(obj[key].toLowerCase(), value);
                                    if (result) {
                                        indexRound = index;
                                        // console.log('indexRound => ', indexRound);
                                    }
                                    return result;
                                } catch (e) {
                                    console.error(e);
                                    return false;
                                }
                            })
                            .map(obj => {
                                const bpObj = {};
                                bpObj[prefixLowerCase] = obj;
                                return bpObj
                            })
                            .value());
                    } catch (e) {
                        console.error(e);
                        callback(5003, null);
                    }

                    // console.log('====================');
                    // console.log(bp);
                    // console.log('====================');
                    // console.log('====================');
                    // console.log(bpArray);
                    // console.log('====================');

                    if( _.isUndefined(bp) || _.isNull(bp)){
                        callback(5003, null);
                    }

                    let model_result = {
                        matchId: `${league_key}:${match_key}`,
                        league_name: sboObjLeague.n.en,
                        league_name_n: sboObjLeague.n,
                        name: matchN,
                        date: matchDate
                    };

                    let odd = _.filter(oddAdjust2, obj => {
                        return _.isEqual(obj.matchId, model_result.matchId);
                    });
                    // console.log(JSON.stringify(bpArray[indexRound]));
                    switch_type(
                        prefixLowerCase,
                        bpArray[indexRound],
                        rulePriceHDP(),
                        model_result,
                        configMatch(),
                        commissionSetting,
                        odd,
                        (err, result)=>{
                            if(err){
                                callback(err, null);
                            } else {
                                let result_new_array = [result];
                                let betPrice = _.chain(result_new_array)
                                    .flatten(true)
                                    .map(betPrice => {
                                        return _.findWhere(betPrice, predicate);
                                    })
                                    .reject(betPrice => {
                                        return _.isUndefined(betPrice)
                                    })
                                    .map(betPrice => {
                                        return getBetObjectByKey(prefix, key, betPrice);
                                    })
                                    .first()
                                    .value();

                                switch_min_max(
                                    prefixLowerCase,
                                    rule,
                                    result,
                                    (err, dataResult)=>{
                                        dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.hdpOuOe.maxPerBet);
                                        calculateBetPriceByTime(dataResult.max, now, new Date(matchDate), (err, data)=>{
                                            dataResult.max = data;
                                        });
                                        calculateBetPrice(dataResult.max, indexRound, (err, data)=>{
                                            dataResult.max = data;
                                        });
                                        dataResult.maxPerMatch = sboObjLeague.r.pm;
                                        console.log('dataResult ', JSON.stringify(dataResult));
                                        callback(null, dataResult);
                                    });
                            }
                        });

                } else {
                    httpRequest.get(
                        `http://lb-sbo-master.api-hub.com/tennis/external/sboLeague/${league_key}`,
                        (error, result) => {
                            if (error) {
                                console.error(error);
                            } else {
                                const model = JSON.parse(result.body).result;
                                const muayThaiModel = new MuaythaiSBOModel(model);
                                muayThaiModel.save((error, data) => {
                                    if (error) {
                                        console.error(error);
                                    } else {
                                        console.log(data);
                                    }
                                });
                            }
                        });
                    callback(5003, null);
                }
            }
        });
}
function cal_bp_hdp_single_match_eSport_sbo(matchId, oddType, oddKey, commissionSetting, oddAdjust, memberId, matchType, key, callback)  {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const league_key = parseInt(_.first(matchId.split(':')));
    const match_key  = parseInt(_.last(matchId.split(':')));

    const value = oddKey;
    const prefix= oddType;
    const prefixLowerCase = prefix.toLowerCase();
    const predicate = JSON.parse(`{"${key}":"${value}"}`);

    console.log('value : ', value);
    console.log('prefix : ', prefix);
    console.log('key : ', key);

    parallel([
            (callback) => {
                MemberService.getLimitSetting(memberId, (errLimitSetting, dataLimitSetting)=>{
                    if(errLimitSetting){
                        callback(errLimitSetting, null);
                    } else {
                        callback(null, dataLimitSetting.limitSetting);
                    }
                });
            },

            (callback) => {
                ESportSBOModel.find({
                    k: league_key
                })
                    .lean()
                    // .sort('sk')
                    .select('k n r')
                    .exec((error, response) => {
                        if (error) {
                            callback(error, null);
                        } else {
                            callback(null, response);
                        }
                    });
            },

            (callback) => {
                Redis.getByKey('sboTodayESport', (error, response) => {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            },

            (callback) => {
                MemberService.findById(memberId, 'username', (error, response)=>{
                    MemberService.getOddAdjustment(response.username, (err, result) => {
                        if (err) {
                            callback(error, null);
                        } else {
                            callback(null, result);
                        }
                    });
                });
            },
        ],
        (error, parallelResult) => {
            if (error) {
                callback(error, null);
            } else {
                const [
                    limitSetting,
                    eSportSBOModel,
                    sboTodayESport,
                    oddAdjust2
                ] = parallelResult;

                const sboObjLeague = _.chain(eSportSBOModel)
                    .filter(sboLeague => {
                        return _.isEqual(parseInt(sboLeague.k), league_key)
                    })
                    .first()
                    .value();

                if (!_.isUndefined(sboObjLeague)) {
                    let matchDate = new Date();
                    let matchN = {};
                    let indexRound;
                    let bpArray = [];
                    let rule = sboObjLeague.r;

                    let bp = null;
                    try {
                        bp = _.first(_.chain(sboTodayESport)
                            .flatten(true)
                            .findWhere(JSON.parse(`{"k":${league_key}}`))
                            .pick('m')
                            .map(obj => {
                                return obj;
                            })
                            .flatten(true)
                            .findWhere(JSON.parse(`{"id":"${league_key}:${match_key}"}`))
                            .tap(obj => {
                                matchDate = obj.d;
                                matchN = obj.n;
                                bpArray = obj.bp;
                            })
                            .pick('bp')
                            .map(obj => {
                                // console.log('obj', obj);
                                return obj;
                            })
                            .flatten(true)
                            .map(obj => {
                                // console.log('obj', obj);
                                return obj[prefixLowerCase];
                            })
                            .filter(obj => {
                                return !_.isUndefined(obj);
                            })
                            .filter((obj, index) => {
                                try {
                                    // console.log('SSSS ', JSON.stringify(obj));
                                    const result = _.isEqual(obj[key].toLowerCase(), value);
                                    if (result) {
                                        indexRound = index;
                                        // console.log('indexRound => ', indexRound);
                                    }
                                    return result;
                                } catch (e) {
                                    console.error(e);
                                    return false;
                                }
                            })
                            .map(obj => {
                                const bpObj = {};
                                bpObj[prefixLowerCase] = obj;
                                return bpObj
                            })
                            .value());
                    } catch (e) {
                        console.error(e);
                        callback(5003, null);
                    }

                    // console.log('====================');
                    // console.log(bp);
                    // console.log('====================');
                    // console.log('====================');
                    // console.log(bpArray);
                    // console.log('====================');

                    if( _.isUndefined(bp) || _.isNull(bp)){
                        callback(5003, null);
                    }

                    let model_result = {
                        matchId: `${league_key}:${match_key}`,
                        league_name: sboObjLeague.n.en,
                        league_name_n: sboObjLeague.n,
                        name: matchN,
                        date: matchDate
                    };

                    let odd = _.filter(oddAdjust2, obj => {
                        return _.isEqual(obj.matchId, model_result.matchId);
                    });
                    // console.log(JSON.stringify(bpArray[indexRound]));
                    switch_type(
                        prefixLowerCase,
                        bpArray[indexRound],
                        rulePriceHDP(),
                        model_result,
                        configMatch(),
                        commissionSetting,
                        odd,
                        (err, result)=>{
                            if(err){
                                callback(err, null);
                            } else {
                                let result_new_array = [result];
                                let betPrice = _.chain(result_new_array)
                                    .flatten(true)
                                    .map(betPrice => {
                                        return _.findWhere(betPrice, predicate);
                                    })
                                    .reject(betPrice => {
                                        return _.isUndefined(betPrice)
                                    })
                                    .map(betPrice => {
                                        return getBetObjectByKey(prefix, key, betPrice);
                                    })
                                    .first()
                                    .value();

                                switch_min_max(
                                    prefixLowerCase,
                                    rule,
                                    result,
                                    (err, dataResult)=>{
                                        dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.hdpOuOe.maxPerBet);
                                        calculateBetPriceByTime(dataResult.max, now, new Date(matchDate), (err, data)=>{
                                            dataResult.max = data;
                                        });
                                        calculateBetPrice(dataResult.max, indexRound, (err, data)=>{
                                            dataResult.max = data;
                                        });
                                        dataResult.maxPerMatch = sboObjLeague.r.pm;
                                        console.log('dataResult ', JSON.stringify(dataResult));
                                        callback(null, dataResult);
                                    });
                            }
                        });

                } else {
                    httpRequest.get(
                        `http://lb-sbo-master.api-hub.com/eSport/external/sboLeague/${league_key}`,
                        (error, result) => {
                            if (error) {
                                console.error(error);
                            } else {
                                const model = JSON.parse(result.body).result;
                                const muayThaiModel = new MuaythaiSBOModel(model);
                                muayThaiModel.save((error, data) => {
                                    if (error) {
                                        console.error(error);
                                    } else {
                                        console.log(data);
                                    }
                                });
                            }
                        });
                    callback(5003, null);
                }
            }
        });
}

function cal_bp_hdp_single_match_table_tennis_sbo(matchId, oddType, oddKey, commissionSetting, oddAdjust, memberId, matchType, key, callback)  {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const league_key = parseInt(_.first(matchId.split(':')));
    const match_key  = parseInt(_.last(matchId.split(':')));

    const value = oddKey;
    const prefix= oddType;
    const prefixLowerCase = prefix.toLowerCase();
    const predicate = JSON.parse(`{"${key}":"${value}"}`);

    console.log('value : ', value);
    console.log('prefix : ', prefix);
    console.log('key : ', key);

    parallel([
            (callback) => {
                MemberService.getLimitSetting(memberId, (errLimitSetting, dataLimitSetting)=>{
                    if(errLimitSetting){
                        callback(errLimitSetting, null);
                    } else {
                        callback(null, dataLimitSetting.limitSetting);
                    }
                });
            },

            (callback) => {
                TableTennisSBOModel.find({
                    k: league_key
                })
                    .lean()
                    // .sort('sk')
                    .select('k n r')
                    .exec((error, response) => {
                        if (error) {
                            callback(error, null);
                        } else {
                            callback(null, response);
                        }
                    });
            },

            (callback) => {
                Redis.getByKey('sboTodayTableTennis', (error, response) => {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            },

            (callback) => {
                MemberService.findById(memberId, 'username', (error, response)=>{
                    MemberService.getOddAdjustment(response.username, (err, result) => {
                        if (err) {
                            callback(error, null);
                        } else {
                            callback(null, result);
                        }
                    });
                });
            },
        ],
        (error, parallelResult) => {
            if (error) {
                callback(error, null);
            } else {
                const [
                    limitSetting,
                    tableTennisSBOModel,
                    sboTodayTableTennis,
                    oddAdjust2
                ] = parallelResult;

                const sboObjLeague = _.chain(tableTennisSBOModel)
                    .filter(sboLeague => {
                        return _.isEqual(parseInt(sboLeague.k), league_key)
                    })
                    .first()
                    .value();

                if (!_.isUndefined(sboObjLeague)) {
                    let matchDate = new Date();
                    let matchN = {};
                    let indexRound;
                    let bpArray = [];
                    let rule = sboObjLeague.r;

                    let bp = null;
                    try {
                        bp = _.first(_.chain(sboTodayTableTennis)
                            .flatten(true)
                            .findWhere(JSON.parse(`{"k":${league_key}}`))
                            .pick('m')
                            .map(obj => {
                                return obj;
                            })
                            .flatten(true)
                            .findWhere(JSON.parse(`{"id":"${league_key}:${match_key}"}`))
                            .tap(obj => {
                                matchDate = obj.d;
                                matchN = obj.n;
                                bpArray = obj.bp;
                            })
                            .pick('bp')
                            .map(obj => {
                                // console.log('obj', obj);
                                return obj;
                            })
                            .flatten(true)
                            .map(obj => {
                                // console.log('obj', obj);
                                return obj[prefixLowerCase];
                            })
                            .filter(obj => {
                                return !_.isUndefined(obj);
                            })
                            .filter((obj, index) => {
                                try {
                                    // console.log('SSSS ', JSON.stringify(obj));
                                    const result = _.isEqual(obj[key].toLowerCase(), value);
                                    if (result) {
                                        indexRound = index;
                                        // console.log('indexRound => ', indexRound);
                                    }
                                    return result;
                                } catch (e) {
                                    console.error(e);
                                    return false;
                                }
                            })
                            .map(obj => {
                                const bpObj = {};
                                bpObj[prefixLowerCase] = obj;
                                return bpObj
                            })
                            .value());
                    } catch (e) {
                        console.error(e);
                        callback(5003, null);
                    }

                    // console.log('====================');
                    // console.log(bp);
                    // console.log('====================');
                    // console.log('====================');
                    // console.log(bpArray);
                    // console.log('====================');

                    if( _.isUndefined(bp) || _.isNull(bp)){
                        callback(5003, null);
                    }

                    let model_result = {
                        matchId: `${league_key}:${match_key}`,
                        league_name: sboObjLeague.n.en,
                        league_name_n: sboObjLeague.n,
                        name: matchN,
                        date: matchDate
                    };

                    let odd = _.filter(oddAdjust2, obj => {
                        return _.isEqual(obj.matchId, model_result.matchId);
                    });
                    // console.log(JSON.stringify(bpArray[indexRound]));
                    switch_type(
                        prefixLowerCase,
                        bpArray[indexRound],
                        rulePriceHDP(),
                        model_result,
                        configMatch(),
                        commissionSetting,
                        odd,
                        (err, result)=>{
                            if(err){
                                callback(err, null);
                            } else {
                                let result_new_array = [result];
                                let betPrice = _.chain(result_new_array)
                                    .flatten(true)
                                    .map(betPrice => {
                                        return _.findWhere(betPrice, predicate);
                                    })
                                    .reject(betPrice => {
                                        return _.isUndefined(betPrice)
                                    })
                                    .map(betPrice => {
                                        return getBetObjectByKey(prefix, key, betPrice);
                                    })
                                    .first()
                                    .value();

                                switch_min_max(
                                    prefixLowerCase,
                                    rule,
                                    result,
                                    (err, dataResult)=>{
                                        dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.hdpOuOe.maxPerBet);
                                        calculateBetPriceByTime(dataResult.max, now, new Date(matchDate), (err, data)=>{
                                            dataResult.max = data;
                                        });
                                        calculateBetPrice(dataResult.max, indexRound, (err, data)=>{
                                            dataResult.max = data;
                                        });
                                        dataResult.maxPerMatch = sboObjLeague.r.pm;
                                        console.log('dataResult ', JSON.stringify(dataResult));
                                        callback(null, dataResult);
                                    });
                            }
                        });

                } else {
                    httpRequest.get(
                        `http://lb-sbo-master.api-hub.com/tableTennis/external/sboLeague/${league_key}`,
                        (error, result) => {
                            if (error) {
                                console.error(error);
                            } else {
                                const model = JSON.parse(result.body).result;
                                const muayThaiModel = new MuaythaiSBOModel(model);
                                muayThaiModel.save((error, data) => {
                                    if (error) {
                                        console.error(error);
                                    } else {
                                        console.log(data);
                                    }
                                });
                            }
                        });
                    callback(5003, null);
                }
            }
        });
}
function cal_bp_hdp_single_match_volleyball_sbo(matchId, oddType, oddKey, commissionSetting, oddAdjust, memberId, matchType, key, callback)  {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const league_key = parseInt(_.first(matchId.split(':')));
    const match_key  = parseInt(_.last(matchId.split(':')));

    const value = oddKey;
    const prefix= oddType;
    const prefixLowerCase = prefix.toLowerCase();
    const predicate = JSON.parse(`{"${key}":"${value}"}`);

    console.log('value : ', value);
    console.log('prefix : ', prefix);
    console.log('key : ', key);

    parallel([
            (callback) => {
                MemberService.getLimitSetting(memberId, (errLimitSetting, dataLimitSetting)=>{
                    if(errLimitSetting){
                        callback(errLimitSetting, null);
                    } else {
                        callback(null, dataLimitSetting.limitSetting);
                    }
                });
            },

            (callback) => {
                VolleyballSBOModel.find({
                    k: league_key
                })
                    .lean()
                    // .sort('sk')
                    .select('k n r')
                    .exec((error, response) => {
                        if (error) {
                            callback(error, null);
                        } else {
                            callback(null, response);
                        }
                    });
            },

            (callback) => {
                Redis.getByKey('sboTodayVolleyball', (error, response) => {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            },

            (callback) => {
                MemberService.findById(memberId, 'username', (error, response)=>{
                    MemberService.getOddAdjustment(response.username, (err, result) => {
                        if (err) {
                            callback(error, null);
                        } else {
                            callback(null, result);
                        }
                    });
                });
            },
        ],
        (error, parallelResult) => {
            if (error) {
                callback(error, null);
            } else {
                const [
                    limitSetting,
                    volleyballSBOModel,
                    sboTodayVolleyball,
                    oddAdjust2
                ] = parallelResult;

                const sboObjLeague = _.chain(volleyballSBOModel)
                    .filter(sboLeague => {
                        return _.isEqual(parseInt(sboLeague.k), league_key)
                    })
                    .first()
                    .value();

                if (!_.isUndefined(sboObjLeague)) {
                    let matchDate = new Date();
                    let matchN = {};
                    let indexRound;
                    let bpArray = [];
                    let rule = sboObjLeague.r;

                    let bp = null;
                    try {
                        bp = _.first(_.chain(sboTodayVolleyball)
                            .flatten(true)
                            .findWhere(JSON.parse(`{"k":${league_key}}`))
                            .pick('m')
                            .map(obj => {
                                return obj;
                            })
                            .flatten(true)
                            .findWhere(JSON.parse(`{"id":"${league_key}:${match_key}"}`))
                            .tap(obj => {
                                matchDate = obj.d;
                                matchN = obj.n;
                                bpArray = obj.bp;
                            })
                            .pick('bp')
                            .map(obj => {
                                // console.log('obj', obj);
                                return obj;
                            })
                            .flatten(true)
                            .map(obj => {
                                // console.log('obj', obj);
                                return obj[prefixLowerCase];
                            })
                            .filter(obj => {
                                return !_.isUndefined(obj);
                            })
                            .filter((obj, index) => {
                                try {
                                    // console.log('SSSS ', JSON.stringify(obj));
                                    const result = _.isEqual(obj[key].toLowerCase(), value);
                                    if (result) {
                                        indexRound = index;
                                        // console.log('indexRound => ', indexRound);
                                    }
                                    return result;
                                } catch (e) {
                                    console.error(e);
                                    return false;
                                }
                            })
                            .map(obj => {
                                const bpObj = {};
                                bpObj[prefixLowerCase] = obj;
                                return bpObj
                            })
                            .value());
                    } catch (e) {
                        console.error(e);
                        callback(5003, null);
                    }

                    // console.log('====================');
                    // console.log(bp);
                    // console.log('====================');
                    // console.log('====================');
                    // console.log(bpArray);
                    // console.log('====================');

                    if( _.isUndefined(bp) || _.isNull(bp)){
                        callback(5003, null);
                    }

                    let model_result = {
                        matchId: `${league_key}:${match_key}`,
                        league_name: sboObjLeague.n.en,
                        league_name_n: sboObjLeague.n,
                        name: matchN,
                        date: matchDate
                    };

                    let odd = _.filter(oddAdjust2, obj => {
                        return _.isEqual(obj.matchId, model_result.matchId);
                    });
                    // console.log(JSON.stringify(bpArray[indexRound]));
                    switch_type(
                        prefixLowerCase,
                        bpArray[indexRound],
                        rulePriceHDP(),
                        model_result,
                        configMatch(),
                        commissionSetting,
                        odd,
                        (err, result)=>{
                            if(err){
                                callback(err, null);
                            } else {
                                let result_new_array = [result];
                                let betPrice = _.chain(result_new_array)
                                    .flatten(true)
                                    .map(betPrice => {
                                        return _.findWhere(betPrice, predicate);
                                    })
                                    .reject(betPrice => {
                                        return _.isUndefined(betPrice)
                                    })
                                    .map(betPrice => {
                                        return getBetObjectByKey(prefix, key, betPrice);
                                    })
                                    .first()
                                    .value();

                                switch_min_max(
                                    prefixLowerCase,
                                    rule,
                                    result,
                                    (err, dataResult)=>{
                                        dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.hdpOuOe.maxPerBet);
                                        calculateBetPriceByTime(dataResult.max, now, new Date(matchDate), (err, data)=>{
                                            dataResult.max = data;
                                        });
                                        calculateBetPrice(dataResult.max, indexRound, (err, data)=>{
                                            dataResult.max = data;
                                        });
                                        dataResult.maxPerMatch = sboObjLeague.r.pm;
                                        console.log('dataResult ', JSON.stringify(dataResult));
                                        callback(null, dataResult);
                                    });
                            }
                        });

                } else {
                    httpRequest.get(
                        `http://lb-sbo-master.api-hub.com/volleyball/external/sboLeague/${league_key}`,
                        (error, result) => {
                            if (error) {
                                console.error(error);
                            } else {
                                const model = JSON.parse(result.body).result;
                                const muayThaiModel = new MuaythaiSBOModel(model);
                                muayThaiModel.save((error, data) => {
                                    if (error) {
                                        console.error(error);
                                    } else {
                                        console.log(data);
                                    }
                                });
                            }
                        });
                    callback(5003, null);
                }
            }
        });
}

function cal_bp_hdp_single_match_spacial_odds(matchId, oddType, oddKey, commissionSetting, oddAdjust, memberId, matchType, key, callback)  {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const league_key = parseInt(_.first(matchId.split(':')));
    const match_key  = parseInt(_.last(matchId.split(':')));

    const value = oddKey;
    const prefix= oddType;
    const prefixLowerCase = prefix.toLowerCase();
    const predicate = JSON.parse(`{"${key}":"${value}"}`);

    console.log('value : ', value);
    console.log('prefix : ', prefix);
    console.log('key : ', key);

    parallel([
            (callback) => {
                MemberService.getLimitSetting(memberId, (errLimitSetting, dataLimitSetting)=>{
                    if(errLimitSetting){
                        callback(errLimitSetting, null);
                    } else {
                        callback(null, dataLimitSetting.limitSetting);
                    }
                });
            },

            (callback) => {
                SpacialOddsModel.find({
                    k: league_key
                })
                    .lean()
                    // .sort('sk')
                    .select('k n r')
                    .exec((error, response) => {
                        if (error) {
                            callback(error, null);
                        } else {
                            callback(null, response);
                        }
                    });
            },

            (callback) => {
                Redis.getByKey(`spacialOdds${CLIENT_NAME}`, (error, response) => {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            },

            (callback) => {
                MemberService.findById(memberId, 'username', (error, response)=>{
                    MemberService.getOddAdjustment(response.username, (err, result) => {
                        if (err) {
                            callback(error, null);
                        } else {
                            callback(null, result);
                        }
                    });
                });
            },
        ],
        (error, parallelResult) => {
            if (error) {
                callback(error, null);
            } else {
                const [
                    limitSetting,
                    spacialOddsModel,
                    todaySpacialOdds,
                    oddAdjust2
                ] = parallelResult;

                const sboObjLeague = _.chain(spacialOddsModel)
                    .filter(sboLeague => {
                        return _.isEqual(parseInt(sboLeague.k), league_key)
                    })
                    .first()
                    .value();

                if (!_.isUndefined(sboObjLeague)) {
                    let matchDate = new Date();
                    let matchN = {};
                    let indexRound;
                    let bpArray = [];
                    let rule = sboObjLeague.r;

                    let bp = null;
                    try {
                        bp = _.first(_.chain(todaySpacialOdds)
                            .flatten(true)
                            .findWhere(JSON.parse(`{"k":${league_key}}`))
                            .pick('m')
                            .map(obj => {
                                return obj;
                            })
                            .flatten(true)
                            .findWhere(JSON.parse(`{"id":"${league_key}:${match_key}"}`))
                            .tap(obj => {
                                matchDate = obj.d;
                                matchN = obj.n;
                                bpArray = obj.bp;
                            })
                            .pick('bp')
                            .map(obj => {
                                // console.log('obj', obj);
                                return obj;
                            })
                            .flatten(true)
                            .map(obj => {
                                // console.log('obj', obj);
                                return obj[prefixLowerCase];
                            })
                            .filter(obj => {
                                return !_.isUndefined(obj);
                            })
                            .filter((obj, index) => {
                                try {
                                    // console.log('SSSS ', JSON.stringify(obj));
                                    const result = _.isEqual(obj[key].toLowerCase(), value);
                                    if (result) {
                                        indexRound = index;
                                        // console.log('indexRound => ', indexRound);
                                    }
                                    return result;
                                } catch (e) {
                                    console.error(e);
                                    return false;
                                }
                            })
                            .map(obj => {
                                const bpObj = {};
                                bpObj[prefixLowerCase] = obj;
                                return bpObj
                            })
                            .value());
                    } catch (e) {
                        console.error(e);
                        callback(5003, null);
                    }

                    // console.log('====================');
                    // console.log(bp);
                    // console.log('====================');
                    // console.log('====================');
                    // console.log(bpArray);
                    // console.log('====================');

                    if( _.isUndefined(bp) || _.isNull(bp)){
                        callback(5003, null);
                    }

                    let model_result = {
                        matchId: `${league_key}:${match_key}`,
                        league_name: sboObjLeague.n.en,
                        league_name_n: sboObjLeague.n,
                        name: matchN,
                        date: matchDate
                    };

                    let odd = _.filter(oddAdjust2, obj => {
                        return _.isEqual(obj.matchId, model_result.matchId);
                    });
                    // console.log(JSON.stringify(bpArray[indexRound]));
                    switch_type(
                        prefixLowerCase,
                        bpArray[indexRound],
                        rulePriceHDP(),
                        model_result,
                        configMatch(),
                        commissionSetting,
                        odd,
                        (err, result)=>{
                            if(err){
                                callback(err, null);
                            } else {
                                let result_new_array = [result];
                                let betPrice = _.chain(result_new_array)
                                    .flatten(true)
                                    .map(betPrice => {
                                        return _.findWhere(betPrice, predicate);
                                    })
                                    .reject(betPrice => {
                                        return _.isUndefined(betPrice)
                                    })
                                    .map(betPrice => {
                                        return getBetObjectByKey(prefix, key, betPrice);
                                    })
                                    .first()
                                    .value();

                                switch_min_max(
                                    prefixLowerCase,
                                    rule,
                                    result,
                                    (err, dataResult)=>{
                                        dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.hdpOuOe.maxPerBet);
                                        calculateBetPriceByTime(dataResult.max, now, new Date(matchDate), (err, data)=>{
                                            dataResult.max = data;
                                        });
                                        calculateBetPrice(dataResult.max, indexRound, (err, data)=>{
                                            dataResult.max = data;
                                        });
                                        dataResult.maxPerMatch = sboObjLeague.r.pm;
                                        console.log('dataResult ', JSON.stringify(dataResult));
                                        callback(null, dataResult);
                                    });
                            }
                        });

                } else {
                    Redis.deleteByKey(`SpacialOddsModel${CLIENT_NAME}`, (error, result) => {

                    });
                    const model = {};
                    model.k = 999999999;
                    model.n = {
                        en : "Spacial Odds",
                        th : "ราคาพิเศษ",
                        cn : "Spacial Odds",
                        tw : "Spacial Odds",
                    };
                    const spacialOddsModel = new SpacialOddsModel(model);
                    spacialOddsModel.save((error, data) => {
                        if (error) {
                            console.error(error);
                        } else {
                            console.log(data);
                        }
                    });
                    callback(5003, null);
                }
            }
        });
}

function cal_bp_hdp_single_match_rugby_sbo(matchId, oddType, oddKey, commissionSetting, oddAdjust, memberId, matchType, key, callback)  {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const league_key = parseInt(_.first(matchId.split(':')));
    const match_key  = parseInt(_.last(matchId.split(':')));

    const value = oddKey;
    const prefix= oddType;
    const prefixLowerCase = prefix.toLowerCase();
    const predicate = JSON.parse(`{"${key}":"${value}"}`);

    console.log('value : ', value);
    console.log('prefix : ', prefix);
    console.log('key : ', key);

    parallel([
            (callback) => {
                MemberService.getLimitSetting(memberId, (errLimitSetting, dataLimitSetting)=>{
                    if(errLimitSetting){
                        callback(errLimitSetting, null);
                    } else {
                        callback(null, dataLimitSetting.limitSetting);
                    }
                });
            },

            (callback) => {
                Redis.getByKey(`RugbySBOModel${CLIENT_NAME}`, (error, result) => {
                    if (error || _.isNull(result) || _.isUndefined(result)) {
                        RugbySBOModel.find({})
                            .lean()
                            // .sort('sk')
                            .select('k n r')
                            .exec((error, response) => {
                                if (error) {
                                    callback(error, null);
                                } else {
                                    Redis.setByKey(`RugbySBOModel${CLIENT_NAME}`, response, (error, result) => {

                                    });
                                    callback(null, response);
                                }
                            });
                    } else {
                        callback(null, result);
                    }
                });
            },

            (callback) => {
                Redis.getByKey('sboTodayRugby', (error, response) => {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            },

            (callback) => {
                MemberService.findById(memberId, 'username', (error, response)=>{
                    MemberService.getOddAdjustment(response.username, (err, result) => {
                        if (err) {
                            callback(error, null);
                        } else {
                            callback(null, result);
                        }
                    });
                });
            },
        ],
        (error, parallelResult) => {
            if (error) {
                callback(error, null);
            } else {
                const [
                    limitSetting,
                    rugbySBOModel,
                    sboTodayRugby,
                    oddAdjust2
                ] = parallelResult;

                const sboObjLeague = _.chain(rugbySBOModel)
                    .filter(sboLeague => {
                        return _.isEqual(parseInt(sboLeague.k), league_key)
                    })
                    .first()
                    .value();

                if (!_.isUndefined(sboObjLeague)) {
                    let matchDate = new Date();
                    let matchN = {};
                    let indexRound;
                    let bpArray = [];
                    let rule = sboObjLeague.r;

                    let bp = null;
                    try {
                        bp = _.first(_.chain(sboTodayRugby)
                            .flatten(true)
                            .findWhere(JSON.parse(`{"k":${league_key}}`))
                            .pick('m')
                            .map(obj => {
                                return obj;
                            })
                            .flatten(true)
                            .findWhere(JSON.parse(`{"id":"${league_key}:${match_key}"}`))
                            .tap(obj => {
                                matchDate = obj.d;
                                matchN = obj.n;
                                bpArray = obj.bp;
                            })
                            .pick('bp')
                            .map(obj => {
                                // console.log('obj', obj);
                                return obj;
                            })
                            .flatten(true)
                            .map(obj => {
                                // console.log('obj', obj);
                                return obj[prefixLowerCase];
                            })
                            .filter(obj => {
                                return !_.isUndefined(obj);
                            })
                            .filter((obj, index) => {
                                try {
                                    // console.log('SSSS ', JSON.stringify(obj));
                                    const result = _.isEqual(obj[key].toLowerCase(), value);
                                    if (result) {
                                        indexRound = index;
                                        // console.log('indexRound => ', indexRound);
                                    }
                                    return result;
                                } catch (e) {
                                    console.error(e);
                                    return false;
                                }
                            })
                            .map(obj => {
                                const bpObj = {};
                                bpObj[prefixLowerCase] = obj;
                                return bpObj
                            })
                            .value());
                    } catch (e) {
                        console.error(e);
                        callback(5003, null);
                    }

                    // console.log('====================');
                    // console.log(bp);
                    // console.log('====================');
                    // console.log('====================');
                    // console.log(bpArray);
                    // console.log('====================');

                    if( _.isUndefined(bp) || _.isNull(bp)){
                        callback(5003, null);
                    }

                    let model_result = {
                        matchId: `${league_key}:${match_key}`,
                        league_name: sboObjLeague.n.en,
                        league_name_n: sboObjLeague.n,
                        name: matchN,
                        date: matchDate
                    };

                    let odd = _.filter(oddAdjust2, obj => {
                        return _.isEqual(obj.matchId, model_result.matchId);
                    });
                    // console.log(JSON.stringify(bpArray[indexRound]));
                    switch_type(
                        prefixLowerCase,
                        bpArray[indexRound],
                        rulePriceHDP(),
                        model_result,
                        configMatch(),
                        commissionSetting,
                        odd,
                        (err, result)=>{
                            if(err){
                                callback(err, null);
                            } else {
                                let result_new_array = [result];
                                let betPrice = _.chain(result_new_array)
                                    .flatten(true)
                                    .map(betPrice => {
                                        return _.findWhere(betPrice, predicate);
                                    })
                                    .reject(betPrice => {
                                        return _.isUndefined(betPrice)
                                    })
                                    .map(betPrice => {
                                        return getBetObjectByKey(prefix, key, betPrice);
                                    })
                                    .first()
                                    .value();

                                switch_min_max(
                                    prefixLowerCase,
                                    rule,
                                    result,
                                    (err, dataResult)=>{
                                        dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.hdpOuOe.maxPerBet);
                                        calculateBetPriceByTime(dataResult.max, now, new Date(matchDate), (err, data)=>{
                                            dataResult.max = data;
                                        });
                                        calculateBetPrice(dataResult.max, indexRound, (err, data)=>{
                                            dataResult.max = data;
                                        });
                                        dataResult.maxPerMatch = sboObjLeague.r.pm;
                                        console.log('dataResult ', JSON.stringify(dataResult));
                                        callback(null, dataResult);
                                    });
                            }
                        });

                } else {
                    httpRequest.get(
                        `http://lb-sbo-master.api-hub.com/rugby/external/sboLeague/${league_key}`,
                        (error, result) => {
                            if (error) {
                                console.error(error);
                            } else {
                                const model = JSON.parse(result.body).result;
                                const muayThaiModel = new MuaythaiSBOModel(model);
                                muayThaiModel.save((error, data) => {
                                    if (error) {
                                        console.error(error);
                                    } else {
                                        console.log(data);
                                    }
                                });
                            }
                        });
                    callback(5003, null);
                }
            }
        });
}

function cal_bp_hdp_single_match_muaythai_local(matchId, oddType, oddKey, commissionSetting, oddAdjust, memberId, matchType, key, callback)  {
    const league_key = parseInt(_.first(matchId.split(':')));
    const match_key  = parseInt(_.last(matchId.split(':')));

    const value = oddKey;
    const prefix= oddType;
    const prefixLowerCase = prefix.toLowerCase();
    const predicate = JSON.parse(`{"${key}":"${value}"}`);

    console.log('value : ', value);
    console.log('prefix : ', prefix);
    console.log('key : ', key);

    parallel([
            (callback) => {
                MuayThaiLocalModel.find({})
                    .lean()
                    // .sort('sk')
                    .exec((error, response) => {
                        if (error) {
                            callback(error, null);
                        } else {
                            callback(null, response);
                        }
                    });
            }
        ],
        (error, parallelResult) => {
            if (error) {
                callback(error, null);
            } else {
                const [
                    muayThaiModel
                ] = parallelResult;

                const sboObjLeague = _.chain(muayThaiModel)
                    .filter(sboLeague => {
                        return _.isEqual(parseInt(sboLeague.k), league_key)
                    })
                    .first()
                    .value();

                if (!_.isUndefined(sboObjLeague)) {
                    let matchDate = new Date();
                    let matchN = {};
                    const indexRound = 0;
                    let bpArray = [];
                    let rule;
                    let maxPerMatch;
                    let bp = null;

                    try {
                        bp = _.first(_.chain(sboObjLeague.m)
                            .findWhere(JSON.parse(`{"k":${match_key}}`))
                            .tap(obj => {
                                rule        = obj.r;
                                maxPerMatch = obj.pm;
                                matchDate   = obj.d;
                                matchN      = obj.n;
                                bpArray     = obj.bp;
                            })
                            .pick('bp')
                            .map(obj => {
                                return obj;
                            })
                            .flatten(true)
                            .map(obj => {
                                return obj[prefixLowerCase]
                            })
                            .filter(obj => {
                                try {
                                    return _.isEqual(obj[key], value);
                                } catch (e) {
                                    return false;
                                }
                            })
                            .map(obj => {
                                const bpObj = {};
                                bpObj[prefixLowerCase] = obj;
                                return bpObj
                            })
                            .value());
                    } catch (e) {
                        console.error(e);
                        callback(5003, null);
                    }

                    if( _.isUndefined(bp) || _.isNull(bp)){
                        callback(5003, null);
                    }

                    let model_result = {
                        matchId: `${league_key}:${match_key}`,
                        league_name: sboObjLeague.n.en,
                        league_name_n: sboObjLeague.n,
                        name: matchN,
                        date: matchDate
                    };

                    // console.log(JSON.stringify(bpArray[indexRound]));
                    switch_type(
                        prefixLowerCase,
                        bpArray[indexRound],
                        rulePriceHDP(),
                        model_result,
                        configMatch(),
                        commissionSetting,
                        [],
                        (err, result)=>{
                            if(err){
                                callback(err, null);
                            } else {
                                result.min = rule[prefixLowerCase].mi;
                                result.max = rule[prefixLowerCase].ma;
                                result.maxPerMatch = maxPerMatch;
                                // console.log('result ', JSON.stringify(result));
                                callback(null, result);
                            }
                        });

                } else {
                    callback(5003, null);
                }
            }
        });
}
function cal_bp_live_single_match_muaythai_local(matchId, oddType, oddKey, commissionSetting, oddAdjust, memberId, matchType, key, callback)  {
    const league_key = parseInt(_.first(matchId.split(':')));
    const match_key  = parseInt(_.last(matchId.split(':')));

    const value = oddKey;
    const prefix= 'AH'; // Only have ah
    const prefixLowerCase = prefix.toLowerCase();

    console.log('value : ', value);
    console.log('prefix : ', prefix);
    console.log('key : ', key);

    parallel([
            (callback) => {
                MuayThaiLocalModel.find({
                    k: league_key
                })
                    .lean()
                    // .sort('sk')
                    .exec((error, response) => {
                        if (error) {
                            callback(error, null);
                        } else {
                            callback(null, response);
                        }
                    });
            }
        ],
        (error, parallelResult) => {
            if (error) {
                callback(error, null);
            } else {
                const [
                    muayThaiModel
                ] = parallelResult;

                const sboObjLeague = _.chain(muayThaiModel)
                    .filter(sboLeague => {
                        return _.isEqual(parseInt(sboLeague.k), league_key)
                    })
                    .first()
                    .value();

                if (!_.isUndefined(sboObjLeague)) {
                    let matchDate = new Date();
                    let matchN = {};
                    const indexRound = 0;
                    let bpArray = [];
                    let rule;
                    let maxPerMatch;
                    let bp = null;

                    try {
                        bp = _.first(_.chain(sboObjLeague.m)
                            .findWhere(JSON.parse(`{"k":${match_key}}`))
                            .tap(obj => {
                                rule        = obj.rl;
                                maxPerMatch = obj.pm;
                                matchDate   = obj.d;
                                matchN      = obj.n;
                                bpArray     = obj.bp;
                            })
                            .pick('bp')
                            .map(obj => {
                                return obj;
                            })
                            .flatten(true)
                            .map(obj => {
                                return obj[prefixLowerCase]
                            })
                            .filter(obj => {
                                try {
                                    return _.isEqual(obj[key], value);
                                } catch (e) {
                                    return false;
                                }
                            })
                            .map(obj => {
                                const bpObj = {};
                                bpObj[prefixLowerCase] = obj;
                                return bpObj
                            })
                            .value());
                    } catch (e) {
                        console.error(e);
                        callback(5003, null);
                    }

                    if( _.isUndefined(bp) || _.isNull(bp)){
                        callback(5003, null);
                    }

                    let model_result = {
                        matchId: `${league_key}:${match_key}`,
                        league_name: sboObjLeague.n.en,
                        league_name_n: sboObjLeague.n,
                        name: matchN,
                        date: matchDate
                    };

                    // console.log(JSON.stringify(bpArray[indexRound]));
                    switch_type(
                        prefixLowerCase,
                        bpArray[indexRound],
                        rulePriceHDP(),
                        model_result,
                        configMatch(),
                        commissionSetting,
                        [],
                        (err, result)=>{
                            if(err){
                                callback(err, null);
                            } else {
                                result.min = rule[prefixLowerCase].mi;
                                result.max = rule[prefixLowerCase].ma;
                                result.maxPerMatch = maxPerMatch;
                                // console.log('result ', JSON.stringify(result));
                                callback(null, result);
                            }
                        });

                } else {
                    callback(5003, null);
                }
            }
        });
}




function cal_bp_hdp_single_match_sbo(matchId, oddType, oddKey, commissionSetting, oddAdjust, memberId, matchType, key, callback)  {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const league_key = parseInt(_.first(matchId.split(':')));
    const match_key  = parseInt(_.last(matchId.split(':')));

    let indexRound;
    const value = oddKey;
    const prefix= oddType;
    const prefixLowerCase = prefix.toLowerCase();
    const predicate = JSON.parse(`{"${key}":"${value}"}`);
    const isToday = !_.isEqual(matchType, 'EARLY');

    console.log('value : ', value);
    console.log('prefix : ', prefix);
    console.log('key : ', key);

    const oddTypeList = ['TG', 'DC', 'CS', 'FHCS', 'FTHT', 'FGLG', 'TTKO', 'TC'];
    parallel([
            (callback) => {
                MemberService.getLimitSetting(memberId, (errLimitSetting, dataLimitSetting)=>{
                    if(errLimitSetting){
                        callback(errLimitSetting, null);
                    } else {
                        callback(null, dataLimitSetting.limitSetting);
                    }
                });
            },

            (callback) => {
                // Redis.getByKey(`FootballSBOModel${CLIENT_NAME}`, (error, result) => {
                //     if (error || _.isNull(result) || _.isUndefined(result)) {
                //         FootballSBOModel.find({})
                //             .lean()
                //             .sort('sk')
                //             .select('k n r rl rem rp')
                //             .exec((error, response) => {
                //                 if (error) {
                //                     callback(error, null);
                //                 } else {
                //                     Redis.setByKey(`FootballSBOModel${CLIENT_NAME}`, response, (error, result) => {
                //
                //                     });
                //                     callback(null, response);
                //                 }
                //             });
                //     } else {
                //         callback(null, result);
                //     }
                // });
                parallel([
                        (callback) => {
                            Redis.getByKey(`FootballSBOModelToday${CLIENT_NAME}`, (error, result) => {
                                if (error) {
                                    callback(null, false);
                                } else {
                                    callback(null, result);
                                }
                            })
                        },

                        (callback) => {
                            Redis.getByKey(`FootballSBOModelEarlyMarket${CLIENT_NAME}`, (error, result) => {
                                if (error) {
                                    callback(null, false);
                                } else {
                                    callback(null, result);
                                }
                            })
                        }
                    ],
                    (error, parallelResult) => {
                        if (error) {
                            callback(error, null);
                        } else {
                            const [
                                footballSBOModelToday,
                                footballSBOModelEarlyMarket
                            ] = parallelResult;
                            let sbo = footballSBOModelToday;
                            if (!isToday) {
                                sbo = footballSBOModelEarlyMarket;
                            }
                            callback(null, sbo);
                        }
                });
            },

            (callback) => {
                Redis.getByKey('sboToday', (error, response) => {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            },

            (callback) => {
                Redis.getByKey('sboEarlyMarket', (error, response) => {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            },

            (callback) => {
                Redis.getByKey('sboMoreToday', (error, response) => {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            },

            (callback) => {
                MemberService.findById(memberId, 'username', (error, response)=>{
                    MemberService.getOddAdjustment(response.username, (err, result) => {
                        if (err) {
                            callback(error, null);
                        } else {
                            callback(null, result);
                        }
                    });
                });
            },

            (callback) => {
                if (_.isEqual(CLIENT_NAME, 'SPORTBOOK88')) {
                    MemberService.findById(memberId, 'username', (error, response)=>{
                        MemberService.getAdjustmentByMatch(response.username, (err, data) => {
                            if (err) {
                                callback(err, null);
                            } else {
                                callback(null, data);
                                console.log('***** ', data);
                            }
                        });
                    });
                } else {

                    AdjustmentByMatch.find({
                    })
                        .lean()
                        .exec((error, response) => {
                            if (error) {
                                callback(error, null);
                            } else {
                                callback(null, response);
                                console.log('***** ', response);
                            }
                        });
                }
                // AdjustmentByMatch.find({
                // })
                //     .lean()
                //     .exec((error, response) => {
                //         if (error) {
                //             callback(error, null);
                //         } else {
                //             callback(null, response);
                //         }
                //     });
            }
        ],
        (error, parallelResult) => {
            if (error) {
                callback(error, null);
            } else {
                const [
                    limitSetting,
                    footballSBOModel,
                    sboToday,
                    sboEarlyMarket,
                    sboMoreToday,
                    oddAdjust2,
                    adjustmentByMatchList
                ] = parallelResult;

                const sboObjLeague = _.chain(footballSBOModel)
                    .filter(sboLeague => {
                        return _.isEqual(parseInt(sboLeague.k), league_key)
                    })
                    .first()
                    .value();

                if (_.isUndefined(sboObjLeague)) {
                    httpRequest.get(
                        `http://lb-sbo-master.api-hub.com/external/sboLeague/${league_key}`,
                        (error, result) => {
                            if (error) {
                                console.error(error);
                            } else {
                                const model = JSON.parse(result.body).result;
                                const footballSBOModel = new FootballSBOModel(model);
                                footballSBOModel.save((error, data) => {
                                    if (error) {
                                        console.error(error);
                                    } else {
                                        console.log(data);
                                    }
                                });
                            }
                        });
                    callback(5003, null);

                } else {
                    if (!_.contains(oddTypeList, prefix)) {
                        let matchDate = new Date();
                        let matchN = {};
                        let indexRound;
                        let bpArray = [];

                        let sbo = sboToday;
                        let rule = sboObjLeague.r;
                        if (!isToday) {
                            sbo = sboEarlyMarket;
                            rule = sboObjLeague.rem;
                        }

                        let bp = null;
                        try {
                            bp = _.first(_.chain(sbo)
                                .flatten(true)
                                .findWhere(JSON.parse(`{"k":${league_key}}`))
                                .pick('m')
                                .map(obj => {
                                    return obj;
                                })
                                .flatten(true)
                                .findWhere(JSON.parse(`{"id":"${league_key}:${match_key}"}`))
                                .tap(obj => {
                                    matchDate = obj.d;
                                    matchN = obj.n;
                                    bpArray = obj.bp;
                                })
                                .pick('bp')
                                .map(obj => {
                                    // console.log('obj', obj);
                                    return obj;
                                })
                                .flatten(true)
                                .map(obj => {
                                    // console.log('obj', obj);
                                    return obj[prefixLowerCase];
                                })
                                .filter(obj => {
                                    return !_.isUndefined(obj);
                                })
                                .filter((obj, index) => {
                                    try {
                                        // console.log('SSSS ', JSON.stringify(obj));
                                        const result = _.isEqual(obj[key].toLowerCase(), value);
                                        if (result) {
                                            indexRound = index;
                                            // console.log('indexRound => ', indexRound);
                                        }
                                        return result;
                                    } catch (e) {
                                        console.error(e);
                                        return false;
                                    }
                                })
                                .map(obj => {
                                    const bpObj = {};
                                    bpObj[prefixLowerCase] = obj;
                                    return bpObj
                                })
                                .value());
                        } catch (e) {
                            console.error(e);
                            callback(5003, null);
                            return;
                        }

                        // console.log('====================');
                        // console.log(bp);
                        // console.log('====================');
                        // console.log('====================');
                        // console.log(bpArray);
                        // console.log('====================');

                        if( _.isUndefined(bp) || _.isNull(bp)){
                            callback(5003, null);
                            return;
                        }

                        let model_result = {
                            matchId: `${league_key}:${match_key}`,
                            league_name: sboObjLeague.n.en,
                            league_name_n: sboObjLeague.n,
                            name: matchN,
                            date: matchDate
                        };

                        let odd = _.filter(oddAdjust2, obj => {
                            return _.isEqual(obj.matchId, model_result.matchId);
                        });
                        // console.log(JSON.stringify(bpArray[indexRound]));

                        let adjustmentByMatch = _.chain(adjustmentByMatchList)
                            .filter(obj => {
                                return _.isEqual(`${league_key}:${match_key}`, obj.id)
                            })
                            .first()
                            .value();

                        let ahAdjustmentByMatch = {home: 0, away: 0};
                        let ouAdjustmentByMatch = {o: 0, u: 0};
                        let ah1stAdjustmentByMatch = {home: 0, away: 0};
                        let ou1stAdjustmentByMatch = {o: 0, u: 0};

                        if (!_.isUndefined(adjustmentByMatch)) {
                            const {
                                ah,
                                ah1st,
                                ou,
                                ou1st
                            } = adjustmentByMatch;

                            adjustmentByMatch = {};
                            adjustmentByMatch.ahAdjustmentByMatch = {};
                            adjustmentByMatch.ahAdjustmentByMatch.home = ah.h;
                            adjustmentByMatch.ahAdjustmentByMatch.away = ah.a;

                            adjustmentByMatch.ouAdjustmentByMatch = {};
                            adjustmentByMatch.ouAdjustmentByMatch.o = ou.o;
                            adjustmentByMatch.ouAdjustmentByMatch.u = ou.u;

                            adjustmentByMatch.ah1stAdjustmentByMatch = {};
                            adjustmentByMatch.ah1stAdjustmentByMatch.home = ah1st.h;
                            adjustmentByMatch.ah1stAdjustmentByMatch.away = ah1st.a;

                            adjustmentByMatch.ou1stAdjustmentByMatch = {};
                            adjustmentByMatch.ou1stAdjustmentByMatch.o = ou1st.o;
                            adjustmentByMatch.ou1stAdjustmentByMatch.u = ou1st.u;
                        } else {
                            adjustmentByMatch = {};
                            adjustmentByMatch.ahAdjustmentByMatch = {home: 0, away: 0};
                            adjustmentByMatch.ouAdjustmentByMatch = {o: 0, u: 0};
                            adjustmentByMatch.ah1stAdjustmentByMatch = {home: 0, away: 0};
                            adjustmentByMatch.ou1stAdjustmentByMatch = {o: 0, u: 0};
                        }

                        switch_type_adjustmentByMatchList(
                            prefixLowerCase,
                            bpArray[indexRound],
                            sboObjLeague.rp.hdp,
                            model_result,
                            configMatch(),
                            commissionSetting,
                            odd,
                            adjustmentByMatch,
                            (err, result)=>{
                                if(err){
                                    callback(err, null);
                                } else {
                                    let result_new_array = [result];
                                    let betPrice = _.chain(result_new_array)
                                        .flatten(true)
                                        .map(betPrice => {
                                            return _.findWhere(betPrice, predicate);
                                        })
                                        .reject(betPrice => {
                                            return _.isUndefined(betPrice)
                                        })
                                        .map(betPrice => {
                                            return getBetObjectByKey(prefix, key, betPrice);
                                        })
                                        .first()
                                        .value();

                                    switch_min_max(
                                        prefixLowerCase,
                                        rule,
                                        result,
                                        (err, dataResult)=>{

                                            if(_.isEqual(prefixLowerCase, 'x12') || _.isEqual(prefixLowerCase, 'x121st')) {

                                                dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet);
                                                if(dataResult.a){
                                                    dataResult.max = dataResult.max / dataResult.a;
                                                } else if(dataResult.h){
                                                    dataResult.max = dataResult.max / dataResult.h;
                                                } else if(dataResult.d){
                                                    dataResult.max = dataResult.max / dataResult.d;
                                                }
                                            } else {
                                                dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.hdpOuOe.maxPerBet);
                                            }

                                            if (isToday) {
                                                calculateBetPriceByTime(dataResult.max, now, new Date(matchDate), (err, data)=>{
                                                    dataResult.max = data;
                                                });
                                            }
                                            calculateBetPrice(dataResult.max, indexRound, (err, data)=>{
                                                dataResult.max = data;
                                            });

                                            dataResult.maxPerMatch = sboObjLeague.r.pm;
                                            // console.log('dataResult ', JSON.stringify(dataResult));
                                            callback(null, dataResult);
                                        })
                                }
                            });
                    } else {//INDEX
                        const moreToday = _.findWhere(sboMoreToday, JSON.parse(`{"id":"${matchId}"}`));
                        // console.log('moreToday => ', JSON.stringify(moreToday));

                        let matchDate = new Date();
                        let matchN = {};

                        _.chain(sboToday)
                            .flatten(true)
                            .findWhere(JSON.parse(`{"k":${league_key}}`))
                            .pick('m')
                            .map(obj => {
                                return obj;
                            })
                            .flatten(true)
                            .findWhere(JSON.parse(`{"id":"${league_key}:${match_key}"}`))
                            .tap(obj => {
                                console.log(obj);
                                matchDate = obj.d;
                                matchN = obj.n;
                            });

                        if (!_.isUndefined(moreToday)) {
                            const bp = _.first(_.chain(moreToday.more)
                                .flatten(true)
                                .map(obj => {
                                    return obj[prefixLowerCase]
                                })
                                .filter(obj => {
                                    return _.isEqual(obj[key], value);
                                })
                                .map(betPrice => {
                                    return getBetObjectByKey(prefixLowerCase, key, betPrice);
                                })
                                .value());
                            console.log('   bp => ', JSON.stringify(bp));
                            if (!_.isUndefined(bp)) {
                                // const {
                                //     mi,
                                //     ma
                                // } = sboObjLeague.r[prefix];
                                // bp.min = 99;//mi;
                                // bp.max = 88888;//ma;

                                let model_result = {
                                    matchId: `${league_key}:${match_key}`,
                                    league_name: sboObjLeague.n.en,
                                    league_name_n: sboObjLeague.n,
                                    name: matchN,
                                    date: matchDate,
                                    min: 19,
                                    max: 5999,
                                    maxPerMatch: 12000
                                };
                                // console.log('   model_result => ', JSON.stringify(model_result));
                                callback(null, model_result);
                            } else {
                                callback(5003, null);
                            }
                        } else {
                            callback(5003, null);
                        }
                    }
                }
            }
        });
}
function cal_bp_live_single_match_sbo(matchId, oddType, oddKey, commissionSetting, oddAdjust, limitSetting, key, callback)  {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const league_key = parseInt(_.first(matchId.split(':')));
    const match_key  = parseInt(_.last(matchId.split(':')));

    let indexRound;
    const value = oddKey;
    const prefix= oddType;
    const prefixLowerCase = prefix.toLowerCase();
    const predicate = JSON.parse(`{"${key}":"${value}"}`);
    // console.log('value : ', value);
    // console.log('prefix : ', prefix);
    // console.log('key : ', key);

    parallel([
            (callback) => {
                waterfall([
                        (callback) => {
                            Redis.getByKey('sboLive', (error, response) => {
                                if (error) {
                                    callback(error, null);
                                } else {
                                    callback(null, response);
                                }
                            });
                        }
                    ],
                    (error, sboLive) => {
                        if (error) {
                            Redis.getByKey(`FootballSBOModelLive${CLIENT_NAME}`, (error, result) => {
                                if (error || _.isNull(result) || _.isUndefined(result)) {
                                    FootballSBOModel.find({})
                                        .lean()
                                        // .sort('sk')
                                        .select('k n r rl rem rp')
                                        .exec((error, response) => {
                                            if (error) {
                                                callback(error, null);
                                            } else {
                                                Redis.setByKey(`FootballSBOModelLive${CLIENT_NAME}`, response, (error, result) => {

                                                });
                                                callback(null, response);
                                            }
                                        });
                                } else {
                                    callback(null, result);
                                }
                            });
                        } else {
                            const sboLiveK = _.chain(sboLive)
                                .flatten(true)
                                .pluck('k')
                                .value();

                            Redis.getByKey(`FootballSBOModelLive${CLIENT_NAME}`, (error, result) => {
                                if (error || _.isNull(result) || _.isUndefined(result)) {
                                    FootballSBOModel.find({
                                        k: {
                                            $in: sboLiveK
                                        }
                                    })
                                        .lean()
                                        // .sort('sk')
                                        .select('k n r rl rem rp')
                                        .exec((error, response) => {
                                            if (error) {
                                                callback(error, null);
                                            } else {
                                                Redis.setByKey(`FootballSBOModelLive${CLIENT_NAME}`, response, (error, result) => {

                                                });
                                                callback(null, response);
                                            }
                                        });
                                } else {
                                    callback(null, result);
                                }
                            });
                        }
                    });
            },

            (callback) => {
                Redis.getByKey('sboLive', (error, response) => {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            },

            (callback) => {
                Redis.getByKey('sboLiveInfo', (error, response) => {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            },

            (callback) => {
                AdjustmentByMatch.find({
                })
                    .lean()
                    .exec((error, response) => {
                        if (error) {
                            callback(error, null);
                        } else {
                            callback(null, response);
                        }
                    });
            }
        ],
        (error, parallelResult) => {
            if (error) {
                callback(error, null);
            } else {
                const [
                    footballSBOModel,
                    sboLive,
                    sboLiveInfo,
                    adjustmentByMatchList
                ] = parallelResult;

                const now_date = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
                const secondsDiff = moment(now_date).diff(sboLiveInfo.updatedDate, 'seconds');
                if (secondsDiff > 9) {
                    callback(5003, null);

                } else {
                    const sboObjLeague = _.chain(footballSBOModel)
                        .filter(sboLeague => {
                            return _.isEqual(parseInt(sboLeague.k), league_key)
                        })
                        .first()
                        .value();

                    if (_.isUndefined(sboObjLeague)) {
                        Redis.deleteByKey(`FootballSBOModel${CLIENT_NAME}`, (error, result) => {

                        });
                        Redis.deleteByKey(`FootballSBOModelLive${CLIENT_NAME}`, (error, result) => {

                        });
                        httpRequest.get(
                            `http://lb-sbo-master.api-hub.com/external/sboLeague/${league_key}`,
                            (error, result) => {
                                if (error) {
                                    console.error(error);
                                } else {
                                    const model = JSON.parse(result.body).result;
                                    const footballSBOModel = new FootballSBOModel(model);
                                    footballSBOModel.save((error, data) => {
                                        if (error) {
                                            console.error(error);
                                        } else {
                                            console.log(data);
                                        }
                                    });
                                }
                            });

                        callback(5003, null);
                    } else {
                        let matchDate = new Date();
                        let matchN = {};
                        let detail = {};
                        let indexRound;
                        let bpArray = [];
                        let isHalfTime = false;
                        let h = 0;
                        let a = 0;
                        let bp = null;

                        try {
                            bp = _.first(_.chain(sboLive)
                                .flatten(true)
                                .findWhere(JSON.parse(`{"k":${league_key}}`))
                                .pick('m')
                                .map(obj => {
                                    return obj;
                                })
                                .flatten(true)
                                .findWhere(JSON.parse(`{"id":"${league_key}:${match_key}"}`))
                                .tap(obj => {
                                    if (!_.isUndefined(obj)) {
                                        console.log('obj ', JSON.stringify(obj));
                                        h = obj.i.h;
                                        a = obj.i.a;
                                        matchDate = obj.d;
                                        matchN = obj.n;
                                        bpArray = obj.bpl;
                                        detail.h = h;
                                        detail.a = a;
                                        detail.s = "";
                                        detail.hrc = obj.i.hrc;
                                        detail.arc = obj.i.arc;
                                        detail.mt = obj.i.mt;
                                        detail.lt = obj.i.lt;
                                        isHalfTime = obj.i.isHalfTime;
                                        detail.rowID = _.isUndefined(obj.i.rowID) ? "" : obj.i.rowID;
                                    }
                                })
                                .pick('bpl')
                                .map(obj => {
                                    return obj;
                                })
                                .flatten(true)
                                .map(obj => {
                                    // console.log('obj', JSON.stringify(obj));
                                    return obj[prefixLowerCase]
                                })
                                .filter(obj => {
                                    return !_.isUndefined(obj);
                                })
                                .filter((obj, index) => {
                                    try {
                                        // console.log('obj => ', JSON.stringify(obj));
                                        const result = _.isEqual(obj[key].toLowerCase(), value);

                                        // console.log('value => ', value);
                                        // console.log('obj[key].toLowerCase() => ', obj[key].toLowerCase());
                                        // console.log('result => ', result);
                                        if (result) {
                                            indexRound = index;
                                            // console.log('indexRound => ', indexRound);
                                        }
                                        return result;
                                    } catch (e) {
                                        console.error(e);
                                        return false;
                                    }
                                })
                                .map(obj => {
                                    const bpObj = {};
                                    bpObj[prefixLowerCase] = obj;
                                    return bpObj
                                })
                                .value());
                        } catch (e) {
                            console.error(e);
                            callback(5003, null);
                            return;
                        }

                        // console.log('====================');
                        // console.log(bp);
                        // console.log('====================');
                        // console.log('====================');
                        // console.log(bpArray);
                        // console.log('====================');

                        if( _.isUndefined(bp) || _.isNull(bp)){
                            callback(5003, null);
                            return;
                        }
                        let adjustmentByMatch = _.chain(adjustmentByMatchList)
                            .filter(obj => {
                                // console.log(`match.id ${match.id} obj.id ${obj.id}`);
                                return _.isEqual(`${league_key}:${match_key}`, obj.id)
                            })
                            .first()
                            .value();

                        let ahAdjustmentByMatch = {home: 0, away: 0};
                        let ouAdjustmentByMatch = {o: 0, u: 0};
                        let ah1stAdjustmentByMatch = {home: 0, away: 0};
                        let ou1stAdjustmentByMatch = {o: 0, u: 0};

                        if (!_.isUndefined(adjustmentByMatch)) {
                            const {
                                ah,
                                ah1st,
                                ou,
                                ou1st
                            } = adjustmentByMatch;

                            adjustmentByMatch = {};
                            adjustmentByMatch.ahAdjustmentByMatch = {};
                            adjustmentByMatch.ahAdjustmentByMatch.home = ah.h;
                            adjustmentByMatch.ahAdjustmentByMatch.away = ah.a;

                            adjustmentByMatch.ouAdjustmentByMatch = {};
                            adjustmentByMatch.ouAdjustmentByMatch.o = ou.o;
                            adjustmentByMatch.ouAdjustmentByMatch.u = ou.u;

                            adjustmentByMatch.ah1stAdjustmentByMatch = {};
                            adjustmentByMatch.ah1stAdjustmentByMatch.home = ah1st.h;
                            adjustmentByMatch.ah1stAdjustmentByMatch.away = ah1st.a;

                            adjustmentByMatch.ou1stAdjustmentByMatch = {};
                            adjustmentByMatch.ou1stAdjustmentByMatch.o = ou1st.o;
                            adjustmentByMatch.ou1stAdjustmentByMatch.u = ou1st.u;
                        } else {
                            adjustmentByMatch = {};
                            adjustmentByMatch.ahAdjustmentByMatch = {home: 0, away: 0};
                            adjustmentByMatch.ouAdjustmentByMatch = {o: 0, u: 0};
                            adjustmentByMatch.ah1stAdjustmentByMatch = {home: 0, away: 0};
                            adjustmentByMatch.ou1stAdjustmentByMatch = {o: 0, u: 0};
                        }

                        let model_result = {
                            matchId: `${league_key}:${match_key}`,
                            league_name: sboObjLeague.n.en,
                            league_name_n: sboObjLeague.n,
                            name: matchN,
                            date: matchDate,
                            isHalfTime: isHalfTime,
                            detail: detail
                        };

                        // console.log('model_result ',JSON.stringify(model_result));
                        let odd = _.filter(oddAdjust, obj => {
                            return _.isEqual(obj.matchId, model_result.matchId);
                        });
                        // console.log('=================++++');
                        switch_type_adjustmentByMatchList(
                            prefixLowerCase,
                            // bpArray[indexRound],
                            bp,
                            sboObjLeague.rp.live,
                            model_result,
                            configMatch(),
                            commissionSetting,
                            odd,
                            adjustmentByMatch,
                            (err, result)=>{
                                if(err){
                                    callback(err, null);
                                } else {
                                    let result_new_array = [result];
                                    let betPrice = _.chain(result_new_array)
                                        .flatten(true)
                                        .map(betPrice => {
                                            return _.findWhere(betPrice, predicate);
                                        })
                                        .reject(betPrice => {
                                            return _.isUndefined(betPrice)
                                        })
                                        .map(betPrice => {
                                            return getBetObjectByKey(prefix, key, betPrice);
                                        })
                                        .first()
                                        .value();

                                    switch_min_max(
                                        prefixLowerCase,
                                        sboObjLeague.rl,
                                        result,
                                        (err, dataResult)=>{

                                            if(_.isEqual(prefixLowerCase, 'x12') || _.isEqual(prefixLowerCase, 'x121st')) {

                                                dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet);
                                                if(dataResult.a){
                                                    dataResult.max = dataResult.max / dataResult.a;
                                                } else if(dataResult.h){
                                                    dataResult.max = dataResult.max / dataResult.h;
                                                } else if(dataResult.d){
                                                    dataResult.max = dataResult.max / dataResult.d;
                                                }
                                            } else {
                                                dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.hdpOuOe.maxPerBet);
                                            }

                                            calculateBetPrice(dataResult.max, indexRound, (err, data)=>{
                                                dataResult.max = data;
                                            });

                                            dataResult.score = {};
                                            dataResult.score.h = h;
                                            dataResult.score.a = a;

                                            dataResult.maxPerMatch = sboObjLeague.r.pm;
                                            // console.log('dataResult ', JSON.stringify(dataResult));
                                            callback(null, dataResult);
                                        })
                                }
                            });
                    }
                }
            }
        });
}

function cal_bp_live_single_match_sbo_special(matchId, oddType, oddKey, commissionSetting, oddAdjust, limitSetting, key, specialType, callback)  {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const league_key = parseInt(_.first(matchId.split(':')));
    const match_key  = parseInt(_.last(matchId.split(':')));

    let type;
    if (_.isEqual(specialType, '1ST_GOAL')) {
        type = '1st Goal';
    } else if (_.isEqual(specialType, '2ND_GOAL')) {
        type = '2nd Goal';
    } else if (_.isEqual(specialType, '3RD_GOAL')) {
        type = '3rd Goal';
    } else if (_.isEqual(specialType, '4TH_GOAL')) {
        type = '4th Goal';
    } else if (_.isEqual(specialType, '5TH_GOAL')) {
        type = '5th Goal';
    } else if (_.isEqual(specialType, '6TH_GOAL')) {
        type = '6th Goal';
    } else if (_.isEqual(specialType, '7TH_GOAL')) {
        type = '7th Goal';
    } else if (_.isEqual(specialType, '8TH_GOAL')) {
        type = '8th Goal';
    } else if (_.isEqual(specialType, '9TH_GOAL')) {
        type = '9th Goal';
    } else if (_.isEqual(specialType, 'TOTAL_CORNER')) {
        type = 'Total Corners';
    }

    let indexRound;
    const value = oddKey;
    const prefix= oddType;
    const prefixLowerCase = prefix.toLowerCase();
    const predicate = JSON.parse(`{"${key}":"${value}"}`);
    console.log('value : ', value);
    console.log('prefix : ', prefix);
    console.log('key : ', key);

    parallel([
            (callback) => {
                FootballSBOModel.find({
                    k: league_key
                })
                    .lean()
                    // .sort('sk')
                    .select('k n r rl rp')
                    .exec((error, response) => {
                        if (error) {
                            callback(error, null);
                        } else {
                            callback(null, response);
                        }
                    });
            },

            (callback) => {
                Redis.getByKey('sboLive', (error, response) => {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            }
        ],
        (error, parallelResult) => {
            if (error) {
                callback(error, null);
            } else {
                const [
                    footballSBOModel,
                    sboLive
                ] = parallelResult;

                const sboObjLeague = _.chain(footballSBOModel)
                    .filter(sboLeague => {
                        return _.isEqual(parseInt(sboLeague.k), league_key)
                    })
                    .first()
                    .value();

                if (!_.isUndefined(sboObjLeague)) {
                    let matchDate = new Date();
                    let matchN = {};
                    let detail = {};
                    let indexRound;
                    let bpArray = [];
                    let isHalfTime = false;
                    let h = 0;
                    let a = 0;
                    let bp = null;
                    // let leagueNameSpecial = {};
                    try {
                        bp = _.first(_.chain(sboLive)
                            .filter(league => {
                                return league.isSpecial && _.isEqual(league.nn.en, type);
                            })
                            // .each(obj => {
                            //     leagueNameSpecial.nn = obj.nn;
                            // })
                            .flatten(true)
                            .findWhere(JSON.parse(`{"k":${league_key}}`))
                            .pick('m')
                            .map(obj => {
                                return obj;
                            })
                            .flatten(true)
                            .findWhere(JSON.parse(`{"id":"${league_key}:${match_key}"}`))
                            .tap(obj => {
                                if (!_.isUndefined(obj)) {
                                    console.log('obj ', JSON.stringify(obj));
                                    h = obj.i.h;
                                    a = obj.i.a;
                                    matchDate = obj.d;
                                    matchN = obj.n;
                                    bpArray = obj.bpl;
                                    detail.h = h;
                                    detail.a = a;
                                    detail.s = "";
                                    detail.hrc = obj.i.hrc;
                                    detail.arc = obj.i.arc;
                                    detail.mt = obj.i.mt;
                                    detail.lt = obj.i.lt;
                                    isHalfTime = obj.i.isHalfTime;
                                    detail.rowID = _.isUndefined(obj.i.rowID) ? "" : obj.i.rowID;
                                }
                            })
                            .pick('bpl')
                            .map(obj => {
                                return obj;
                            })
                            .flatten(true)
                            .map(obj => {
                                // console.log('obj', JSON.stringify(obj));
                                return obj[prefixLowerCase]
                            })
                            .filter(obj => {
                                return !_.isUndefined(obj);
                            })
                            .filter((obj, index) => {
                                try {
                                    // console.log('SSSS ', JSON.stringify(obj));
                                    const result = _.isEqual(obj[key].toLowerCase(), value);
                                    if (result) {
                                        indexRound = index;
                                        // console.log('indexRound => ', indexRound);
                                    }
                                    return result;
                                } catch (e) {
                                    console.error(e);
                                    return false;
                                }
                            })
                            .map(obj => {
                                const bpObj = {};
                                bpObj[prefixLowerCase] = obj;
                                return bpObj
                            })
                            .value());
                    } catch (e) {
                        console.error(e);
                        callback(5003, null);
                        return;
                    }

                    // console.log('====================');
                    // console.log(bp);
                    // console.log('====================');
                    // console.log('====================');
                    // console.log(bpArray);
                    // console.log('====================');

                    if( _.isUndefined(bp) || _.isNull(bp)){
                        callback(5003, null);
                        return;
                    }
                    // console.log('leagueNameSpecial => ',leagueNameSpecial);
                    let model_result = {
                        matchId: `${league_key}:${match_key}`,
                        league_name: sboObjLeague.n.en,
                        league_name_n: sboObjLeague.n,
                        name: matchN,
                        date: matchDate,
                        isHalfTime: isHalfTime,
                        detail: detail
                    };

                    console.log('model_result ',JSON.stringify(model_result));
                    let odd = _.filter(oddAdjust, obj => {
                        return _.isEqual(obj.matchId, model_result.matchId);
                    });

                    switch_type(
                        prefixLowerCase,
                        bpArray[indexRound],
                        sboObjLeague.rp.live,
                        model_result,
                        configMatch(),
                        commissionSetting,
                        odd,
                        (err, result)=>{
                            if(err){
                                callback(err, null);
                            } else {
                                switch_min_max(
                                    prefixLowerCase,
                                    sboObjLeague.rl,
                                    result,
                                    (err, dataResult)=>{

                                        // if(_.isEqual(prefixLowerCase, 'x12') || _.isEqual(prefixLowerCase, 'x121st')) {
                                        //
                                        //     dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet);
                                        //     if(dataResult.a){
                                        //         dataResult.max = dataResult.max / dataResult.a;
                                        //     } else if(dataResult.h){
                                        //         dataResult.max = dataResult.max / dataResult.h;
                                        //     } else if(dataResult.d){
                                        //         dataResult.max = dataResult.max / dataResult.d;
                                        //     }
                                        // } else {
                                        //     dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.hdpOuOe.maxPerBet);
                                        // }
                                        //
                                        // calculateBetPrice(dataResult.max, indexRound, (err, data)=>{
                                        //     dataResult.max = data;
                                        // });

                                        dataResult.score = {};
                                        dataResult.score.h = h;
                                        dataResult.score.a = a;
                                        dataResult.min = 10;
                                        dataResult.max = 5000;
                                        dataResult.maxPerMatch = sboObjLeague.r.pm;
                                        // console.log('dataResult ', JSON.stringify(dataResult));
                                        callback(null, dataResult);
                                    })
                            }
                        });
                } else {
                    httpRequest.get(
                        `http://lb-sbo-master.api-hub.com/external/sboLeague/${league_key}`,
                        (error, result) => {
                            if (error) {
                                console.error(error);
                            } else {
                                const model = JSON.parse(result.body).result;
                                const footballSBOModel = new FootballSBOModel(model);
                                footballSBOModel.save((error, data) => {
                                    if (error) {
                                        console.error(error);
                                    } else {
                                        console.log(data);
                                    }
                                });
                            }
                        });
                    callback(5003, null);
                }
            }
        });
}

function cal_bp_parlay_sbo(matchId, oddType, oddKey, commissionSetting, oddAdjust, key, callback)  {
    const league_key = parseInt(_.first(matchId.split(':')));
    const match_key  = parseInt(_.last(matchId.split(':')));

    const value = oddKey;
    const prefix= oddType;
    const prefixLowerCase = prefix.toLowerCase();

    console.log('value : ', value);
    console.log('prefix : ', prefix);
    console.log('key : ', key);
    parallel([
            (callback) => {
                Redis.getByKey(`FootballSBOModel${CLIENT_NAME}`, (error, result) => {
                    if (error || _.isNull(result) || _.isUndefined(result)) {
                        FootballSBOModel.find({})
                            .lean()
                            // .sort('sk')
                            .select('k n r rl rem rp')
                            .exec((error, response) => {
                                if (error) {
                                    callback(error, null);
                                } else {
                                    Redis.setByKey(`FootballSBOModel${CLIENT_NAME}`, response, (error, result) => {

                                    });
                                    callback(null, response);
                                }
                            });
                    } else {
                        callback(null, result);
                    }
                });
                // FootballSBOModel.find({
                //     k: league_key
                // })
                //     .lean()
                //     .sort('sk')
                //     .select('k n rp')
                //     .exec((error, response) => {
                //         if (error) {
                //             callback(error, null);
                //         } else {
                //             callback(null, response);
                //         }
                //     });
            },

            (callback) => {
                Redis.getByKey('sboParlay', (error, response) => {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            }
        ],
        (error, parallelResult) => {
            if (error) {
                callback(error, null);
            } else {
                const [
                    footballSBOModel,
                    sboParlay
                ] = parallelResult;

                const sboObjLeague = _.chain(footballSBOModel)
                    .filter(sboLeague => {
                        return _.isEqual(parseInt(sboLeague.k), league_key)
                    })
                    .first()
                    .value();

                if (_.isUndefined(sboObjLeague)) {
                    httpRequest.get(
                        `http://lb-sbo-master.api-hub.com/external/sboLeague/${league_key}`,
                        (error, result) => {
                            if (error) {
                                console.error(error);
                            } else {
                                const model = JSON.parse(result.body).result;
                                const footballSBOModel = new FootballSBOModel(model);
                                footballSBOModel.save((error, data) => {
                                    if (error) {
                                        console.error(error);
                                    } else {
                                        console.log(data);
                                    }
                                });
                            }
                        });
                    callback(5003, null);

                } else {
                    let matchDate = new Date();
                    let matchN = {};
                    let indexRound;
                    let bpArray = [];

                    let bp = null;
                    try {
                        bp = _.first(_.chain(sboParlay)
                            .flatten(true)
                            .findWhere(JSON.parse(`{"k":${league_key}}`))
                            .pick('m')
                            .map(obj => {
                                return obj;
                            })
                            .flatten(true)
                            .findWhere(JSON.parse(`{"id":"${league_key}:${match_key}"}`))
                            .tap(obj => {
                                matchDate = obj.d;
                                matchN = obj.n;
                                bpArray = obj.bp;
                            })
                            .pick('bp')
                            .map(obj => {
                                // console.log('obj', obj);
                                return obj;
                            })
                            .flatten(true)
                            .map(obj => {
                                // console.log('obj', obj);
                                return obj[prefixLowerCase];
                            })
                            .filter(obj => {
                                return !_.isUndefined(obj);
                            })
                            .filter((obj, index) => {
                                try {
                                    // console.log('SSSS ', JSON.stringify(obj));
                                    const result = _.isEqual(obj[key].toLowerCase(), value);
                                    if (result) {
                                        indexRound = index;
                                        // console.log('indexRound => ', indexRound);
                                    }
                                    return result;
                                } catch (e) {
                                    console.error(e);
                                    return false;
                                }
                            })
                            .map(obj => {
                                const bpObj = {};
                                bpObj[prefixLowerCase] = obj;
                                return bpObj
                            })
                            .value());
                    } catch (e) {
                        console.error(e);
                        callback(5003, null);
                    }

                    // console.log('====================');
                    // console.log(bp);
                    // console.log('====================');
                    // console.log('====================');
                    // console.log(bpArray);
                    // console.log('====================');

                    if( _.isUndefined(bp) || _.isNull(bp)){
                        callback(5003, null);
                    }

                    let model_result = {
                        matchId: `${league_key}:${match_key}`,
                        league_name: sboObjLeague.n.en,
                        league_name_n: sboObjLeague.n,
                        name: matchN,
                        date: matchDate
                    };

                    // console.log(JSON.stringify(bpArray[indexRound]));
                    switch_type_parlay_sbo(
                        prefixLowerCase,
                        bpArray[indexRound],
                        sboObjLeague.rp.hdp,
                        model_result,
                        configMatch(),
                        commissionSetting,
                        oddAdjust,
                        (err, result)=>{
                            if(err){
                                callback(err, null);
                            } else {
                                callback(null, result);
                            }
                        });
                }
            }
        });
}

function cal_bp_mix_parlay_sbo(matchId, oddType, oddKey, commissionSetting, oddAdjust, key, callback)  {
    const league_key = parseInt(_.first(matchId.split(':')));
    const match_key  = parseInt(_.last(matchId.split(':')));

    const value = oddKey;
    const prefix= oddType;
    const prefixLowerCase = prefix.toLowerCase();

    console.log('value  : ', value);
    console.log('prefix : ', prefix);
    console.log('key    : ', key);
    parallel([
            (callback) => {
                Redis.getByKey(`FootballSBOModel${CLIENT_NAME}`, (error, result) => {
                    if (error || _.isNull(result) || _.isUndefined(result)) {
                        FootballSBOModel.find({})
                            .lean()
                            // .sort('sk')
                            .select('k n r rl rem rp')
                            .exec((error, response) => {
                                if (error) {
                                    callback(error, null);
                                } else {
                                    Redis.setByKey(`FootballSBOModel${CLIENT_NAME}`, response, (error, result) => {

                                    });
                                    callback(null, response);
                                }
                            });
                    } else {
                        callback(null, result);
                    }
                });
                //
                // FootballSBOModel.find({
                //     k: league_key
                // })
                //     .lean()
                //     .sort('sk')
                //     .select('k n rp')
                //     .exec((error, response) => {
                //         if (error) {
                //             callback(error, null);
                //         } else {
                //             callback(null, response);
                //         }
                //     });
            },

            (callback) => {
                Redis.getByKey('sboParlay', (error, response) => {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            }
        ],
        (error, parallelResult) => {
            if (error) {
                callback(error, null);
            } else {
                const [
                    footballSBOModel,
                    sboParlay
                ] = parallelResult;

                const sboObjLeague = _.chain(footballSBOModel)
                    .filter(sboLeague => {
                        return _.isEqual(parseInt(sboLeague.k), league_key)
                    })
                    .first()
                    .value();

                if (_.isUndefined(sboObjLeague)) {
                    httpRequest.get(
                        `http://lb-sbo-master.api-hub.com/external/sboLeague/${league_key}`,
                        (error, result) => {
                            if (error) {
                                console.error(error);
                            } else {
                                const model = JSON.parse(result.body).result;
                                const footballSBOModel = new FootballSBOModel(model);
                                footballSBOModel.save((error, data) => {
                                    if (error) {
                                        console.error(error);
                                    } else {
                                        console.log(data);
                                    }
                                });
                            }
                        });
                    callback(5003, null);

                } else {
                    let matchDate = new Date();
                    let matchN = {};
                    let indexRound;
                    let bpArray = [];

                    let bp = null;
                    try {
                        bp = _.first(_.chain(sboParlay)
                            .flatten(true)
                            .findWhere(JSON.parse(`{"k":${league_key}}`))
                            .pick('m')
                            .map(obj => {
                                return obj;
                            })
                            .flatten(true)
                            .findWhere(JSON.parse(`{"id":"${league_key}:${match_key}"}`))
                            .tap(obj => {
                                matchDate = obj.d;
                                matchN = obj.n;
                                bpArray = obj.bp;
                            })
                            .pick('bp')
                            .map(obj => {
                                // console.log('obj', obj);
                                return obj;
                            })
                            .flatten(true)
                            .map(obj => {
                                // console.log('obj', obj);
                                return obj[prefixLowerCase];
                            })
                            .filter(obj => {
                                return !_.isUndefined(obj);
                            })
                            .filter((obj, index) => {
                                try {
                                    // console.log('SSSS ', JSON.stringify(obj));
                                    const result = _.isEqual(obj[key].toLowerCase(), value);
                                    if (result) {
                                        indexRound = index;
                                        // console.log('indexRound => ', indexRound);
                                    }
                                    return result;
                                } catch (e) {
                                    console.error(e);
                                    return false;
                                }
                            })
                            .map(obj => {
                                const bpObj = {};
                                bpObj[prefixLowerCase] = obj;
                                return bpObj
                            })
                            .value());
                    } catch (e) {
                        console.error(e);
                        callback(5003, null);
                    }

                    // console.log('====================');
                    // console.log(bp);
                    // console.log('====================');
                    // console.log('====================');
                    // console.log(bpArray);
                    // console.log('====================');

                    if( _.isUndefined(bp) || _.isNull(bp)){
                        callback(5003, null);
                    }

                    let model_result = {
                        matchId: `${league_key}:${match_key}`,
                        league_name: sboObjLeague.n.en,
                        league_name_n: sboObjLeague.n,
                        name: matchN,
                        date: matchDate
                    };

                    // console.log(JSON.stringify(bpArray[indexRound]));
                    switch_type_parlay_sbo(
                        prefixLowerCase,
                        bpArray[indexRound],
                        sboObjLeague.rp.hdp,
                        model_result,
                        configMatch(),
                        commissionSetting,
                        oddAdjust,
                        (err, result)=>{
                            if(err){
                                callback(err, null);
                            } else {
                                callback(null, result);
                            }
                        });
                }
            }
        });
}
function cal_bp_live_mix_parlay_sbo(matchId, oddType, oddKey, commissionSetting, oddAdjust, key, callback)  {
    const league_key = parseInt(_.first(matchId.split(':')));
    const match_key  = parseInt(_.last(matchId.split(':')));

    const value = oddKey;
    const prefix= oddType;
    const prefixLowerCase = prefix.toLowerCase();
    const predicate = JSON.parse(`{"${key}":"${value}"}`);
    console.log('value  : ', value);
    console.log('prefix : ', prefix);
    console.log('key    : ', key);

    parallel([
            (callback) => {
                Redis.getByKey(`FootballSBOModel${CLIENT_NAME}`, (error, result) => {
                    if (error || _.isNull(result) || _.isUndefined(result)) {
                        FootballSBOModel.find({})
                            .lean()
                            // .sort('sk')
                            .select('k n r rl rem rp')
                            .exec((error, response) => {
                                if (error) {
                                    callback(error, null);
                                } else {
                                    Redis.setByKey(`FootballSBOModel${CLIENT_NAME}`, response, (error, result) => {

                                    });
                                    callback(null, response);
                                }
                            });
                    } else {
                        callback(null, result);
                    }
                });
                // FootballSBOModel.find({
                //     k: league_key
                // })
                //     .lean()
                //     // .sort('sk')
                //     .select('k n r rl rp')
                //     .exec((error, response) => {
                //         if (error) {
                //             callback(error, null);
                //         } else {
                //             callback(null, response);
                //         }
                //     });
            },

            (callback) => {
                Redis.getByKey('sboLiveStep', (error, response) => {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            }
        ],
        (error, parallelResult) => {
            if (error) {
                callback(error, null);
            } else {
                const [
                    footballSBOModel,
                    sboLive
                ] = parallelResult;

                const sboObjLeague = _.chain(footballSBOModel)
                    .filter(sboLeague => {
                        return _.isEqual(parseInt(sboLeague.k), league_key)
                    })
                    .first()
                    .value();

                if (_.isUndefined(sboObjLeague)) {
                    httpRequest.get(
                        `http://lb-sbo-master.api-hub.com/external/sboLeague/${league_key}`,
                        (error, result) => {
                            if (error) {
                                console.error(error);
                            } else {
                                const model = JSON.parse(result.body).result;
                                const footballSBOModel = new FootballSBOModel(model);
                                footballSBOModel.save((error, data) => {
                                    if (error) {
                                        console.error(error);
                                    } else {
                                        console.log(data);
                                    }
                                });
                            }
                        });
                    callback(5003, null);

                } else {
                    let matchDate = new Date();
                    let matchN = {};
                    let detail = {};
                    let indexRound;
                    let bpArray = [];
                    let isHalfTime = false;
                    let h = 0;
                    let a = 0;
                    let bp = null;

                    try {
                        bp = _.first(_.chain(sboLive)
                            .flatten(true)
                            .findWhere(JSON.parse(`{"k":${league_key}}`))
                            .pick('m')
                            .map(obj => {
                                return obj;
                            })
                            .flatten(true)
                            .findWhere(JSON.parse(`{"id":"${league_key}:${match_key}"}`))
                            .tap(obj => {
                                if (!_.isUndefined(obj)) {
                                    console.log('obj ', JSON.stringify(obj));
                                    h = obj.i.h;
                                    a = obj.i.a;
                                    matchDate = obj.d;
                                    matchN = obj.n;
                                    bpArray = obj.bpl;
                                    detail.h = h;
                                    detail.a = a;
                                    detail.s = "";
                                    detail.hrc = obj.i.hrc;
                                    detail.arc = obj.i.arc;
                                    detail.mt = obj.i.mt;
                                    detail.lt = obj.i.lt;
                                    isHalfTime = obj.i.isHalfTime;
                                    detail.rowID = _.isUndefined(obj.i.rowID) ? "" : obj.i.rowID;
                                }
                            })
                            .pick('bpl')
                            .map(obj => {
                                return obj;
                            })
                            .flatten(true)
                            .map(obj => {
                                // console.log('obj', JSON.stringify(obj));
                                return obj[prefixLowerCase]
                            })
                            .filter(obj => {
                                return !_.isUndefined(obj);
                            })
                            .filter((obj, index) => {
                                try {
                                    // console.log('SSSS ', JSON.stringify(obj));
                                    const result = _.isEqual(obj[key].toLowerCase(), value);
                                    if (result) {
                                        indexRound = index;
                                        // console.log('indexRound => ', indexRound);
                                    }
                                    return result;
                                } catch (e) {
                                    console.error(e);
                                    return false;
                                }
                            })
                            .map(obj => {
                                const bpObj = {};
                                bpObj[prefixLowerCase] = obj;
                                return bpObj
                            })
                            .value());
                    } catch (e) {
                        console.error(e);
                        callback(5003, null);
                        return;
                    }

                    // console.log('====================');
                    // console.log(bp);
                    // console.log('====================');
                    // console.log('====================');
                    // console.log(bpArray);
                    // console.log('====================');

                    if( _.isUndefined(bp) || _.isNull(bp)){
                        callback(5003, null);
                    }

                    let model_result = {
                        matchId: `${league_key}:${match_key}`,
                        league_name: sboObjLeague.n.en,
                        league_name_n: sboObjLeague.n,
                        name: matchN,
                        date: matchDate,
                        detail:detail
                    };

                    // console.log(JSON.stringify(bpArray[indexRound]));
                    switch_type_parlay(
                        prefixLowerCase,
                        bpArray[indexRound],
                        sboObjLeague.rp.live,
                        model_result,
                        configMatch(),
                        commissionSetting,
                        oddAdjust,
                        (err, result)=>{
                            if(err){
                                callback(err, null);
                            } else {
                                callback(null, result);
                            }
                        });
                }
            }
        });
}

function switch_type_parlay_sbo(type_lower, bp, rp_hdp, modal, CONFIG_MTACH, commission_type, odd_adjust, callback) {

    let ou1st = {o: 0, u: 0};
    let ou = {o: 0, u: 0};
    let oe = {o: 0, e: 0};
    let oe1st = {o: 0, e: 0};
    let ah = {home: 0, away: 0};
    let ah1st = {home: 0, away: 0};

    if(_.size(odd_adjust) > 0){
        _.each(odd_adjust, (o)=>{
            if(o.prefix === 'ah' && bp.ah){
                if(o.key === 'hpk' && o.value === bp.ah.hpk){
                    ah = {
                        home: -o.count,
                        away: o.count
                    }
                } else if(o.key === 'apk' && o.value === bp.ah.apk){
                    ah = {
                        home: o.count,
                        away: -o.count
                    }
                }
            } else if(o.prefix === 'ah1st' && bp.ah1st){

                if(o.key === 'hpk' && o.value === bp.ah1st.hpk){
                    ah1st = {
                        home: -o.count,
                        away: o.count
                    }
                } else if(o.key === 'apk' && o.value === bp.ah1st.apk){
                    ah1st = {
                        home: o.count,
                        away: -o.count
                    }
                }
            } else if(o.prefix === 'ou' && bp.ou){
                // console.log(o.value , bp.ou.opk);
                if(o.key === 'opk' && o.value === bp.ou.opk){
                    ou = {
                        o: -o.count,
                        u: o.count
                    }
                } else if(o.key === 'upk' && o.value === bp.ou.upk){
                    ou = {
                        o: o.count,
                        u: -o.count
                    }
                }
            } else if(o.prefix === 'ou1st' && bp.ou1st){
                if(o.key === 'opk' && o.value === bp.ou1st.opk){
                    ou1st = {
                        o: -o.count,
                        u: o.count
                    }
                } else if(o.key === 'upk' && o.value === bp.ou1st.upk){
                    ou1st = {
                        o: o.count,
                        u: -o.count
                    }
                }
            } else if(o.prefix === 'oe' && bp.oe){
                if(o.key === 'ok' && o.value === bp.oe.ok){
                    oe = {
                        o: -o.count,
                        e: o.count
                    }
                } else if(o.key === 'ek' && o.value === bp.oe.ek){
                    oe = {
                        o: o.count,
                        e: -o.count
                    }
                }
            } else if(o.prefix === 'oe1st' && bp.oe1st){
                if(o.key === 'ok' && o.value === bp.oe1st.ok){
                    oe1st = {
                        o: -o.count,
                        e: o.count
                    }
                } else if(o.key === 'ek' && o.value === bp.oe1st.ek){
                    oe1st = {
                        o: o.count,
                        e: -o.count
                    }
                }
            }
        });
    }

    // console.log(type_lower, bp, rp_hdp, modal);
    if (bp) {
        switch (type_lower) {
            case "ah":
                // console.log('ah : ', type_lower);
                cal_eu_ah_sbo(bp.ah.h, bp.ah.a, bp.ah.hp, bp.ah.ap, rp_hdp.ah.f, rp_hdp.ah.u, CONFIG_MTACH.ah, commission_type, ah, (err, data) => {
                    if (data) {
                        modal.ah = {
                            hk: bp.ah.hk,
                            h: bp.ah.h,
                            ak: bp.ah.ak,
                            a: bp.ah.a,
                            hpk: bp.ah.hpk,
                            hp: data.hp,
                            apk: bp.ah.apk,
                            ap: data.ap
                        };

                        // cal_price(modal.ah.hp, modal.ah.ap, (err, data)=>{
                        //     modal.s = data;
                        // })
                    }
                });
                break;
            case "ah1st":
                // console.log('ah1st : ', type_lower);
                cal_eu_ah_sbo(bp.ah1st.h, bp.ah1st.a, bp.ah1st.hp, bp.ah1st.ap, rp_hdp.ah1st.f, rp_hdp.ah1st.u, CONFIG_MTACH.ah1st, commission_type, ah1st, (err, data) => {
                    if (data) {
                        modal.ah1st = {
                            hk: bp.ah1st.hk,
                            h: bp.ah1st.h,
                            ak: bp.ah1st.ak,
                            a: bp.ah1st.a,
                            hpk: bp.ah1st.hpk,
                            hp: data.hp,
                            apk: bp.ah1st.apk,
                            ap: data.ap
                        };

                        // cal_price(modal.ah1st.hp, modal.ah1st.ap, (err, data)=>{
                        //     modal.s = data;
                        // })
                    }
                });
                break;
            case "ou":
                // console.log('ou : ', type_lower);
                cal_eu_ou_sbo(bp.ou.op, bp.ou.up, rp_hdp.ou.f, rp_hdp.ou.u, CONFIG_MTACH.ou, commission_type, ou, (err, data) => {
                    if (data) {
                        modal.ou = {
                            ok: bp.ou.ok,
                            o: bp.ou.o,
                            uk: bp.ou.uk,
                            u: bp.ou.u,
                            opk: bp.ou.opk,
                            op: data.op,
                            upk: bp.ou.upk,
                            up: data.up
                        };

                        // cal_price(modal.ou.op, modal.ou.up, (err, data)=>{
                        //     modal.s = data;
                        // })
                    }
                });
                break;
            case "ou1st":
                // console.log('ou1st : ', type_lower);
                cal_eu_ou_sbo(bp.ou1st.op, bp.ou1st.up, rp_hdp.ou1st.f, rp_hdp.ou1st.u, CONFIG_MTACH.ou1st, commission_type, ou1st, (err, data) => {
                    if (data) {
                        modal.ou1st = {
                            ok: bp.ou1st.ok,
                            o: bp.ou1st.o,
                            uk: bp.ou1st.uk,
                            u: bp.ou1st.u,
                            opk: bp.ou1st.opk,
                            op: data.op,
                            upk: bp.ou1st.upk,
                            up: data.up
                        };

                        // cal_price(modal.ou1st.op, modal.ou1st.up, (err, data)=>{
                        //     modal.s = data;
                        // })
                    }
                });
                break;
            case "x12":
                // console.log('x12 : ', type_lower, bp.x12);
                cal_x12(bp.x12.h, bp.x12.a, bp.x12.d, CONFIG_MTACH.x12, (err, data) => {
                    if (data) {
                        modal.x12 = {
                            hk: bp.x12.hk,
                            h: data.h,
                            ak: bp.x12.ak,
                            a: data.a,
                            dk: bp.x12.dk,
                            d: data.d
                        };

                        // cal_price(modal.ou1st.op, modal.ou1st.up, (err, data)=>{
                        //     modal.s = data;
                        // })
                    }
                });
                break;
            case "x121st":
                // console.log('x121st : ', type_lower);
                cal_x12(bp.x121st.h, bp.x121st.a, bp.x121st.d, CONFIG_MTACH.x121st, (err, data) => {
                    if (data) {
                        modal.x121st = {
                            hk: bp.x121st.hk,
                            h: data.h,
                            ak: bp.x121st.ak,
                            a: data.a,
                            dk: bp.x121st.dk,
                            d: data.d
                        };

                        // cal_price(modal.ou1st.op, modal.ou1st.up, (err, data)=>{
                        //     modal.s = data;
                        // })
                    }
                });

                break;
            case "oe":
                // console.log('oe : ', type_lower);
                if (bp.oe) {
                    cal_eu_oe_sbo(bp.oe.o, bp.oe.e, rp_hdp.oe.f, rp_hdp.oe.u, CONFIG_MTACH.oe, commission_type, oe, (err, data) => {
                        if (data) {
                            modal.oe = {
                                ok: bp.oe.ok,
                                o: data.o,
                                ek: bp.oe.ek,
                                e: data.e
                            };
                        }

                        // cal_price(modal.oe.o, modal.oe.e, (err, data)=>{
                        //     modal.s = data;
                        // })
                    });
                }
                break;
            case "oe1st":
                // console.log('oe1st : ', type_lower);
                cal_eu_oe_sbo(bp.oe1st.o, bp.oe1st.e, rp_hdp.oe1st.f, rp_hdp.oe1st.u, CONFIG_MTACH.oe1st, commission_type, oe1st, (err, data) => {
                    if (data) {
                        modal.oe1st = {
                            ok: bp.oe1st.ok,
                            o: data.o,
                            ek: bp.oe1st.ek,
                            e: data.e
                        };
                    }

                    // cal_price(modal.oe1st.o, modal.oe1st.e, (err, data)=>{
                    //     modal.s = data;
                    // })
                });
                break;
        }

        callback(null, modal);
    } else {
        callback('Betprice is null', null);
    }
}


function cal_bp_hdp_single_match(matchId, type, key,commission_type, odd_adjust, memberId, callback)  {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const k = matchId.split(':');
    console.log(type);
    console.log(key);
    console.log(commission_type);
    let limitSetting;
    let indexRound;

    MemberService.getLimitSetting(memberId, (errLimitSetting, dataLimitSetting)=>{
        if(errLimitSetting){
            return res.send({
                message: "error",
                result: errLimitSetting,
                code: 998
            });
        } else {
            limitSetting = dataLimitSetting.limitSetting;
        }
    });

    async.series([(cb) => {
        FootballModel.findOne({k: k[0]},
            (err, data) => {

                // console.log(data);
                if (err) {
                    cb(err, null)
                } else if (data) {
                    cb(null, data)
                } else {
                    cb('league is null', null)
                }
            })
    }, (cb) => {

        // console.log(`${URL_CACHE_HDP_SINGLE_MATCH}/${matchId}`);

        const headers = {
            'Content-Type': 'application/json'
        };

        const option = {
            url: `${URL_CACHE_HDP_SINGLE_MATCH}/${matchId}`,
            method: 'GET',
            headers: headers
        };

        // console.log(option);

        request(option, (err, response, body) => {
            if (err) {
                cb(err, null)
            } else {
                let result = JSON.parse(body);

                // console.log(result);

                if (result) {
                    cb(null, result);
                } else {
                    cb(null, []);
                }
            }
        });
    }], (err, results) => {
        if (err) {
            callback(err, null)
        } else {
            const type_lower = type.toLowerCase();
            // console.log(type_lower);
            const rp_hdp = results[0].rp.hdp;
            let match_cache = results[1];
            let bp = _.first(_.filter(match_cache.result, league => {
                return _.contains(league.allKey, key)
            }));

            console.log(JSON.stringify(bp));
            let model_result = {
                matchId: `${match_cache.league_key}:${match_cache.match_key}`,
                league_name: match_cache.league_name,
                league_name_n: match_cache.league_name_n,
                name: match_cache.name,
                date: match_cache.date
            };

            let isToday = false;
            let sr = {};
            let srem = {};

            const resultBP = _.first(_.chain(results)
                .filter(league => {
                    return _.isEqual(league.k, parseInt(k[0]))
                })
                .flatten(true)
                .pluck('m')
                .flatten(true)
                .filter(matchAPI => {
                    return _.isEqual(matchAPI.id, matchId)
                })
                .each(matchAPI => {
                    // console.log('matchAPI =======>', matchAPI.isToday);

                    isToday = matchAPI.isToday;

                    if(matchAPI.sr && matchAPI.sr.isOn){
                        sr = matchAPI.sr;
                    }

                    if(matchAPI.srem && matchAPI.srem.isOn){
                        srem = matchAPI.srem;
                    }

                    _.each(matchAPI.bp, (betPrice, index) => {
                        if(betPrice.allKey.indexOf(key) > -1){
                            indexRound = index;
                        }
                    })
                })
                .pluck('bp')
                .flatten(true)
                .filter(betPrice => {
                    return _.contains(betPrice.allKey, key);
                })
                .map(betPrice => {
                    return _.pick(betPrice, 'rp', 'sr', 'srem')
                })
                .flatten(true)
                .value());

            if (_.isUndefined(resultBP) || _.isUndefined(resultBP.rp)) {
                callback(5003, null);
            } else {
                switch_type(type_lower, bp, rp_hdp, model_result, resultBP.rp, commission_type, odd_adjust, (err, result) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        if(isToday){

                            let min_max = {};
                            // console.log('sr ====> ', resultBP.sr);
                            if(sr && !_.isUndefined(sr.pm) && sr.isOn){
                                min_max = sr;
                            } else {
                                min_max = _.first(results).r;
                            }

                            switch_min_max(type_lower, min_max, result, (err, dataResult)=>{
                                // console.log('match_cache ====> ', JSON.stringify(match_cache));

                                if(_.isEqual(type_lower, 'x12') || _.isEqual(type_lower, 'x121st')){
                                    dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet);

                                    dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet);

                                    if(dataResult.a){
                                        dataResult.max = dataResult.max / dataResult.a;
                                    } else if(dataResult.h){
                                        dataResult.max = dataResult.max / dataResult.h;
                                    } else if(dataResult.d){
                                        dataResult.max = dataResult.max / dataResult.d;
                                    }
                                } else {
                                    dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.hdpOuOe.maxPerBet);
                                }

                                if(isToday){
                                    calculateBetPriceByTime(dataResult.max, now, new Date(match_cache.date), (err, data)=>{
                                        dataResult.max = data;
                                    })
                                }

                                calculateBetPrice(dataResult.max, indexRound, (err, data)=>{
                                    dataResult.max = data;
                                });

                                dataResult.maxPerMatch = _.first(results).r.pm;

                                callback(null, dataResult);
                            })
                        } else {

                            let min_max = {};

                            if(srem && !_.isUndefined(srem.pm) && srem.isOn){
                                min_max = srem;
                            } else {
                                min_max = _.first(results).rem;
                            }


                            switch_min_max(type_lower, min_max, result, (err, dataResult)=>{
                                // console.log('match_cache ====> ', JSON.stringify(match_cache));

                                if(_.isEqual(type_lower, 'x12') || _.isEqual(type_lower, 'x121st')){
                                    dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet);

                                    dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet);

                                    if(dataResult.a){
                                        dataResult.max = dataResult.max / dataResult.a;
                                    } else if(dataResult.h){
                                        dataResult.max = dataResult.max / dataResult.h;
                                    } else if(dataResult.d){
                                        dataResult.max = dataResult.max / dataResult.d;
                                    }
                                } else {
                                    dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.hdpOuOe.maxPerBet);
                                }

                                if(isToday){
                                    calculateBetPriceByTime(dataResult.max, now, new Date(match_cache.date), (err, data)=>{
                                        dataResult.max = data;
                                    })
                                }

                                calculateBetPrice(dataResult.max, indexRound, (err, data)=>{
                                    dataResult.max = data;
                                })


                                dataResult.maxPerMatch = _.first(results).r.pm;

                                callback(null, dataResult);
                            })
                        }

                        // callback(null, result);
                    }
                })
            }
        }
    })
}
function cal_bp_live_single_match(matchId, type, key, commission_type, odd_adjust, limitSetting, callback)  {
    // console.log(matchId, type, key);

    const k = matchId.split(':');
    let indexRound;

    async.series([(cb) => {
        FootballModel.findOne({k: k[0]},
            (err, data) => {
                // console.log(data);
                if (err) {
                    cb(err, null)
                } else if (data) {
                    cb(null, data)
                } else {
                    cb('league is null', null)
                }
            })
    }, (cb) => {

        // console.log(`${URL_CACHE_LIVE_SINGLE_MATCH}/${matchId}`);

        const headers = {
            'Content-Type': 'application/json'
        };

        const option = {
            url: `${URL_CACHE_LIVE_SINGLE_MATCH}/${matchId}`,
            method: 'GET',
            headers: headers
        };

        // console.log(`${URL_CACHE_LIVE_SINGLE_MATCH}/${matchId}`);

        request(option, (err, response, body) => {
            if (err) {
                cb(err, null)
            } else {
                // console.log(body);
                let result = JSON.parse(body);

                // console.log(result);

                if (result) {
                    cb(null, result);
                } else {
                    cb(null, []);
                }
            }
        });
    }], (err, results) => {
        // console.log(results);
        if (err) {
            callback(err, null)
        } else {
            const type_lower = type.toLowerCase();
            const rp_live = results[0].rp.live;
            let match_cache = results[1];
            // console.log('rp_live     ===> ', JSON.stringify(rp_live));
            // console.log('match_cache ===> ', JSON.stringify(match_cache));
            let bp = _.first(_.filter(match_cache.result, league => {
                // console.log(league.allKey, key);
                return _.contains(league.allKey, key)
            }));

            let model_result = {
                matchId: `${match_cache.league_key}:${match_cache.match_key}`,
                league_name: match_cache.league_name,
                league_name_n: match_cache.league_name_n,
                name: match_cache.name,
                date: match_cache.date,
                isHalfTime:match_cache.isHalfTime,
                detail:match_cache.detail
            };

            let srl = {};

            const resultBPL = _.first(_.chain(results)
                .filter(league => {
                    return _.isEqual(league.k, parseInt(k[0]))
                })
                .flatten(true)
                .pluck('m')
                .flatten(true)
                .filter(matchAPI => {
                    return _.isEqual(matchAPI.id, matchId)
                })
                .each(matchAPI => {
                    // let round = 0;
                    // const r = _.first(matchAPI.bpl).r;
                    //
                    // if(matchAPI.srl && matchAPI.srl.isOn){
                    //     srl = matchAPI.srl;
                    // }
                    _.each(matchAPI.bpl, (betPrice, index) => {
                        // r = r;
                        // calculateBetPrice(r.ah.ma, round, (err, data)=>{
                        //     r.ah.ma = data;
                        // })
                        //
                        // calculateBetPrice(r.ou.ma, round, (err, data)=>{
                        //     r.ou.ma = data;
                        // })
                        //
                        // calculateBetPrice(r.x12.ma, round, (err, data)=>{
                        //     r.x12.ma = data;
                        // })
                        //
                        // calculateBetPrice(r.oe.ma, round, (err, data)=>{
                        //     r.oe.ma = data;
                        // })
                        //
                        // calculateBetPrice(r.ah1st.ma, round, (err, data)=>{
                        //     r.ah1st.ma = data;
                        // })
                        //
                        // calculateBetPrice(r.ou1st.ma, round, (err, data)=>{
                        //     r.ou1st.ma = data;
                        // })
                        //
                        // calculateBetPrice(r.x121st.ma, round, (err, data)=>{
                        //     r.x121st.ma = data;
                        // })
                        //
                        // calculateBetPrice(r.oe1st.ma, round, (err, data)=>{
                        //     r.oe1st.ma = data;
                        // })
                        //
                        // round++;

                        // console.log('======>', allKey.indexOf(key));

                        if(betPrice.allKey.indexOf(key) > -1){
                            // console.log(index);
                            indexRound = index;
                        }

                    })
                })
                .pluck('bpl')
                .flatten(true)
                .filter(betPrice => {
                    return _.contains(betPrice.allKey, key);
                })
                .map(betPrice => {
                    return _.pick(betPrice, 'rp', 'srl')
                })
                .flatten(true)
                .value());

            // console.log('resultBPL => ', JSON.stringify(resultBPL));
            // console.log('bp => ', JSON.stringify(bp));

            if (_.isUndefined(resultBPL) || _.isUndefined(resultBPL.rp)) {
                callback(5003, null);
            } else {
                switch_type(type_lower, bp, rp_live, model_result, resultBPL.rp, commission_type, odd_adjust, (err, result) => {
                    if (err) {
                        callback(err, null);
                    } else {

                        let min_max = {};

                        if(srl && !_.isUndefined(srl.pm) && srl.isOn){
                            min_max = srl;
                        } else {
                            min_max = _.first(results).rl;
                        }

                        switch_min_max(type_lower, min_max, result, (err, dataResult)=>{
                            dataResult.score = {
                                h: match_cache.detail.h,
                                a: match_cache.detail.a
                            };

                            // console.log('limitSetting ===> ', limitSetting);
                            if(_.isEqual(type_lower, 'x12') || _.isEqual(type_lower, 'x121st')){
                                // console.log('dataResult ====> ', dataResult)

                                dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet);
                                // console.log('2. MAX ====> ', dataResult.max);
                                if(dataResult.a){
                                    dataResult.max = dataResult.max / dataResult.a;
                                } else if(dataResult.h){
                                    dataResult.max = dataResult.max / dataResult.h;
                                } else if(dataResult.d){
                                    dataResult.max = dataResult.max / dataResult.d;
                                }
                            } else {
                                // console.log('===========>', dataResult.max, limitSetting.sportsBook.hdpOuOe.maxPerBet);
                                dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.hdpOuOe.maxPerBet);
                            }

                            calculateBetPrice(dataResult.max, indexRound, (err, data)=>{
                                dataResult.max = data;
                            })

                            dataResult.maxPerMatch = _.first(results).r.pm;
                            dataResult.detail = match_cache.detail;
                            console.log(JSON.stringify(dataResult));
                            callback(null, dataResult);
                        })
                    }
                })
            }
        }
    })
}

function cal_bp_live_single_match_robot(matchId, type, key, commission_type, odd_adjust, maxPerBet, callback)  {
    // console.log(matchId, type, key);

    const k = matchId.split(':');
    let indexRound;

    async.series([(cb) => {
        FootballModel.findOne({k: k[0]},
            (err, data) => {
                // console.log(data);
                if (err) {
                    cb(err, null)
                } else if (data) {
                    cb(null, data)
                } else {
                    cb('league is null', null)
                }
            })
    }, (cb) => {

        // console.log(`${URL_CACHE_LIVE_SINGLE_MATCH}/${matchId}`);

        const headers = {
            'Content-Type': 'application/json'
        };

        const option = {
            url: `${URL_CACHE_LIVE_SINGLE_MATCH}/${matchId}`,
            method: 'GET',
            headers: headers
        };

        // console.log(`${URL_CACHE_LIVE_SINGLE_MATCH}/${matchId}`);

        request(option, (err, response, body) => {
            if (err) {
                cb(err, null)
            } else {
                // console.log(body);
                let result = JSON.parse(body);

                // console.log(result);

                if (result) {
                    cb(null, result);
                } else {
                    cb(null, []);
                }
            }
        });
    }], (err, results) => {
        // console.log(results);
        if (err) {
            callback(err, null)
        } else {
            const type_lower = type.toLowerCase();
            const rp_live = results[0].rp.live;
            let match_cache = results[1];
            // console.log('rp_live     ===> ', JSON.stringify(rp_live));
            // console.log('match_cache ===> ', JSON.stringify(match_cache));
            let bp = _.first(_.filter(match_cache.result, league => {
                console.log(league.allKey, key);
                return _.contains(league.allKey, key)
            }));

            let model_result = {
                matchId: `${match_cache.league_key}:${match_cache.match_key}`,
                league_name: match_cache.league_name,
                league_name_n: match_cache.league_name_n,
                name: match_cache.name,
                date: match_cache.date,
                isHalfTime:match_cache.isHalfTime,
                detail:match_cache.detail
            };

            let srl = {};

            const resultBPL = _.first(_.chain(results)
                .filter(league => {
                    return _.isEqual(league.k, parseInt(k[0]))
                })
                .flatten(true)
                .pluck('m')
                .flatten(true)
                .filter(matchAPI => {
                    return _.isEqual(matchAPI.id, matchId)
                })
                .each(matchAPI => {
                    // let round = 0;
                    // const r = _.first(matchAPI.bpl).r;
                    //
                    // if(matchAPI.srl && matchAPI.srl.isOn){
                    //     srl = matchAPI.srl;
                    // }
                    _.each(matchAPI.bpl, (betPrice, index) => {
                        // r = r;
                        // calculateBetPrice(r.ah.ma, round, (err, data)=>{
                        //     r.ah.ma = data;
                        // })
                        //
                        // calculateBetPrice(r.ou.ma, round, (err, data)=>{
                        //     r.ou.ma = data;
                        // })
                        //
                        // calculateBetPrice(r.x12.ma, round, (err, data)=>{
                        //     r.x12.ma = data;
                        // })
                        //
                        // calculateBetPrice(r.oe.ma, round, (err, data)=>{
                        //     r.oe.ma = data;
                        // })
                        //
                        // calculateBetPrice(r.ah1st.ma, round, (err, data)=>{
                        //     r.ah1st.ma = data;
                        // })
                        //
                        // calculateBetPrice(r.ou1st.ma, round, (err, data)=>{
                        //     r.ou1st.ma = data;
                        // })
                        //
                        // calculateBetPrice(r.x121st.ma, round, (err, data)=>{
                        //     r.x121st.ma = data;
                        // })
                        //
                        // calculateBetPrice(r.oe1st.ma, round, (err, data)=>{
                        //     r.oe1st.ma = data;
                        // })
                        //
                        // round++;

                        // console.log('======>', allKey.indexOf(key));

                        if(betPrice.allKey.indexOf(key) > -1){
                            // console.log(index);
                            indexRound = index;
                        }

                    })
                })
                .pluck('bpl')
                .flatten(true)
                .filter(betPrice => {
                    return _.contains(betPrice.allKey, key);
                })
                .map(betPrice => {
                    return _.pick(betPrice, 'rp', 'srl')
                })
                .flatten(true)
                .value());

            // console.log('resultBPL => ', JSON.stringify(resultBPL));
            // console.log('bp => ', JSON.stringify(bp));

            if (_.isUndefined(resultBPL) || _.isUndefined(resultBPL.rp)) {
                callback(5003, null);
            } else {
                switch_type(type_lower, bp, rp_live, model_result, resultBPL.rp, commission_type, odd_adjust, (err, result) => {
                    if (err) {
                        callback(err, null);
                    } else {

                        let min_max = {};

                        if(srl && !_.isUndefined(srl.pm) && srl.isOn){
                            min_max = srl;
                        } else {
                            min_max = _.first(results).rl;
                        }

                        switch_min_max(type_lower, min_max, result, (err, dataResult)=>{
                            dataResult.score = {
                                h: match_cache.detail.h,
                                a: match_cache.detail.a
                            };

                            // console.log('limitSetting ===> ', limitSetting);
                            if(_.isEqual(type_lower, 'x12') || _.isEqual(type_lower, 'x121st')){
                                // console.log('dataResult ====> ', dataResult)

                                dataResult.max = Math.min(dataResult.max, maxPerBet);
                                // console.log('2. MAX ====> ', dataResult.max);
                                if(dataResult.a){
                                    dataResult.max = dataResult.max / dataResult.a;
                                } else if(dataResult.h){
                                    dataResult.max = dataResult.max / dataResult.h;
                                } else if(dataResult.d){
                                    dataResult.max = dataResult.max / dataResult.d;
                                }
                            } else {
                                // console.log('===========>', dataResult.max, limitSetting.sportsBook.hdpOuOe.maxPerBet);
                                dataResult.max = Math.min(dataResult.max, maxPerBet);
                            }

                            calculateBetPrice(dataResult.max, indexRound, (err, data)=>{
                                dataResult.max = data;
                            })

                            dataResult.maxPerMatch = _.first(results).r.pm;
                            dataResult.detail = match_cache.detail;
                            callback(null, dataResult);
                        })
                    }
                })
            }
        }
    })
}

function cal_price(home, away, callback) {
    // console.log(home, away);
    if(home && away){
        if(parseFloat(home) > 0 && parseFloat(away) > 0){
            let homeHdp = 0;
            let awayHdp = 0;

            homeHdp = 1 - parseFloat(home);
            awayHdp = 1 - parseFloat(away);

            let result = (homeHdp + awayHdp) * 100;

            callback(null, result.toFixed(0));

        }else{
            let hdpHome = parseFloat(home) < 0 ? Math.abs(parseFloat(home)) : parseFloat(home);
            let hdpAway = parseFloat(away) < 0 ? Math.abs(parseFloat(away)) : parseFloat(away);
            let result = 0;

            result = hdpHome - hdpAway;

            if(result < 0){
                result = Math.abs(result);
            }

            result = result * 100;

            callback(null, result.toFixed(0));
        }

    }else {
        return 0;
    }
}
function getBetObjectByKey(prefix, key, bpArray)  {
    let obj = {};
    switch (prefix.replace(/1st/g, '')) {
        case 'ml':
            if (_.isEqual(key, 'hk')) {
                obj = _.pick(bpArray, 'hk', 'h');
            } else {
                obj = _.pick(bpArray, 'ak', 'a');
            }
            break;

        case 'ah':
            if (_.isEqual(key, 'hk') || _.isEqual(key, 'hpk')) {
                obj = _.pick(bpArray, 'hk', 'h', 'hpk', 'hp');
            } else {
                obj = _.pick(bpArray, 'ak', 'a', 'apk', 'ap');
            }
            break;
        case 'ou':
            if (_.isEqual(key, 'ok') || _.isEqual(key, 'opk')) {
                obj = _.pick(bpArray, 'ok', 'o', 'opk', 'op');
            } else {
                obj = _.pick(bpArray, 'uk', 'u', 'upk', 'up');
            }
            break;
        case 'x12':
            if (_.isEqual(key, 'hk') || _.isEqual(key, 'h')) {
                obj = _.pick(bpArray, 'hk', 'h');
            } else if (_.isEqual(key, 'ak') || _.isEqual(key, 'a')) {
                obj = _.pick(bpArray, 'ak', 'a');
            } else {
                obj = _.pick(bpArray, 'dk', 'd');
            }
            break;
        case 'oe':
            if (_.isEqual(key, 'ok') || _.isEqual(key, 'o')) {
                obj = _.pick(bpArray, 'ok', 'o');
            } else {
                obj = _.pick(bpArray, 'ek', 'e');
            }
            break;

        case 'dc':
            if (_.isEqual(key, 'hdk')) {
                obj = _.pick(bpArray, 'hd', 'hdk');
            } else if (_.isEqual(key, 'hak')) {
                obj = _.pick(bpArray, 'ha', 'hak');
            } else {
                obj = _.pick(bpArray, 'ad', 'adk');
            }
            break;

        case 'tg':
            if (_.isEqual(key, 'tg01k')) {
                obj = _.pick(bpArray, 'tg01', 'tg01k');
            } else if (_.isEqual(key, 'tg02k')) {
                obj = _.pick(bpArray, 'tg02', 'tg02k');
            } else if (_.isEqual(key, 'tg03k')) {
                obj = _.pick(bpArray, 'tg03', 'tg03k');
            } else  {
                obj = _.pick(bpArray, 'tg04', 'tg04k');
            }
            break;

        case 'cs':
            if (_.isEqual(key, 'd00k')) {
                obj = _.pick(bpArray, 'd00', 'd00k');

            } else if (_.isEqual(key, 'a01k')) {
                obj = _.pick(bpArray, 'a01', 'a01k');

            } else if (_.isEqual(key, 'a02k')) {
                obj = _.pick(bpArray, 'a02', 'a02k');

            } else if (_.isEqual(key, 'a03k')) {
                obj = _.pick(bpArray, 'a03', 'a03k');

            } else if (_.isEqual(key, 'a04k')) {
                obj = _.pick(bpArray, 'a04', 'a04k');

            } else if (_.isEqual(key, 'h10k')) {
                obj = _.pick(bpArray, 'h10', 'h10k');

            } else if (_.isEqual(key, 'd11k')) {
                obj = _.pick(bpArray, 'd11', 'd11k');

            } else if (_.isEqual(key, 'a12k')) {
                obj = _.pick(bpArray, 'a12', 'a12k');

            } else if (_.isEqual(key, 'a13k')) {
                obj = _.pick(bpArray, 'a13', 'a13k');

            } else if (_.isEqual(key, 'a14k')) {
                obj = _.pick(bpArray, 'a14', 'a14k');

            } else if (_.isEqual(key, 'h20k')) {
                obj = _.pick(bpArray, 'h20', 'h20k');

            } else if (_.isEqual(key, 'h21k')) {
                obj = _.pick(bpArray, 'h21', 'h21k');

            } else if (_.isEqual(key, 'd22k')) {
                obj = _.pick(bpArray, 'd22', 'd22k');

            } else if (_.isEqual(key, 'a23k')) {
                obj = _.pick(bpArray, 'a23', 'a23k');

            } else if (_.isEqual(key, 'a24k')) {
                obj = _.pick(bpArray, 'a24', 'a24k');

            } else if (_.isEqual(key, 'h30k')) {
                obj = _.pick(bpArray, 'h30', 'h30k');

            } else if (_.isEqual(key, 'h31k')) {
                obj = _.pick(bpArray, 'h31', 'h31k');

            } else if (_.isEqual(key, 'h32k')) {
                obj = _.pick(bpArray, 'h32', 'h32k');

            } else if (_.isEqual(key, 'd33k')) {
                obj = _.pick(bpArray, 'd33', 'd33k');

            } else if (_.isEqual(key, 'a34k')) {
                obj = _.pick(bpArray, 'a34', 'a34k');

            } else if (_.isEqual(key, 'h40k')) {
                obj = _.pick(bpArray, 'h40', 'h40k');

            } else if (_.isEqual(key, 'h41k')) {
                obj = _.pick(bpArray, 'h41', 'h41k');

            } else if (_.isEqual(key, 'h42k')) {
                obj = _.pick(bpArray, 'h42', 'h42k');

            } else if (_.isEqual(key, 'h43k')) {
                obj = _.pick(bpArray, 'h43', 'h43k');

            } else if (_.isEqual(key, 'd44k')) {
                obj = _.pick(bpArray, 'd44', 'd44k');

            } else {
                obj = _.pick(bpArray, 'aos', 'aosk');
            }
            break;

        case 'fhcs':
            if (_.isEqual(key, 'd00k')) {
                obj = _.pick(bpArray, 'd00', 'd00k');

            } else if (_.isEqual(key, 'a01k')) {
                obj = _.pick(bpArray, 'a01', 'a01k');

            } else if (_.isEqual(key, 'a02k')) {
                obj = _.pick(bpArray, 'a02', 'a02k');

            } else if (_.isEqual(key, 'a03k')) {
                obj = _.pick(bpArray, 'a03', 'a03k');

            } else if (_.isEqual(key, 'a04k')) {
                obj = _.pick(bpArray, 'a04', 'a04k');

            } else if (_.isEqual(key, 'h10k')) {
                obj = _.pick(bpArray, 'h10', 'h10k');

            } else if (_.isEqual(key, 'd11k')) {
                obj = _.pick(bpArray, 'd11', 'd11k');

            } else if (_.isEqual(key, 'a12k')) {
                obj = _.pick(bpArray, 'a12', 'a12k');

            } else if (_.isEqual(key, 'a13k')) {
                obj = _.pick(bpArray, 'a13', 'a13k');

            } else if (_.isEqual(key, 'a14k')) {
                obj = _.pick(bpArray, 'a14', 'a14k');

            } else if (_.isEqual(key, 'h20k')) {
                obj = _.pick(bpArray, 'h20', 'h20k');

            } else if (_.isEqual(key, 'h21k')) {
                obj = _.pick(bpArray, 'h21', 'h21k');

            } else if (_.isEqual(key, 'd22k')) {
                obj = _.pick(bpArray, 'd22', 'd22k');

            } else if (_.isEqual(key, 'a23k')) {
                obj = _.pick(bpArray, 'a23', 'a23k');

            } else if (_.isEqual(key, 'a24k')) {
                obj = _.pick(bpArray, 'a24', 'a24k');

            } else if (_.isEqual(key, 'h30k')) {
                obj = _.pick(bpArray, 'h30', 'h30k');

            } else if (_.isEqual(key, 'h31k')) {
                obj = _.pick(bpArray, 'h31', 'h31k');

            } else if (_.isEqual(key, 'h32k')) {
                obj = _.pick(bpArray, 'h32', 'h32k');

            } else if (_.isEqual(key, 'd33k')) {
                obj = _.pick(bpArray, 'd33', 'd33k');

            } else if (_.isEqual(key, 'a34k')) {
                obj = _.pick(bpArray, 'a34', 'a34k');

            } else if (_.isEqual(key, 'h40k')) {
                obj = _.pick(bpArray, 'h40', 'h40k');

            } else if (_.isEqual(key, 'h41k')) {
                obj = _.pick(bpArray, 'h41', 'h41k');

            } else if (_.isEqual(key, 'h42k')) {
                obj = _.pick(bpArray, 'h42', 'h42k');

            } else if (_.isEqual(key, 'h43k')) {
                obj = _.pick(bpArray, 'h43', 'h43k');

            } else if (_.isEqual(key, 'd44k')) {
                obj = _.pick(bpArray, 'd44', 'd44k');

            } else {
                obj = _.pick(bpArray, 'aos', 'aosk');
            }
            break;

        case 'ftht':
            if (_.isEqual(key, 'hhk')) {
                obj = _.pick(bpArray, 'hh', 'hhk');

            } else if (_.isEqual(key, 'hdk')) {
                obj = _.pick(bpArray, 'hd', 'hdk');

            } else if (_.isEqual(key, 'hak')) {
                obj = _.pick(bpArray, 'ha', 'hak');

            } else if (_.isEqual(key, 'dhk')) {
                obj = _.pick(bpArray, 'dh', 'dhk');

            } else if (_.isEqual(key, 'ddk')) {
                obj = _.pick(bpArray, 'dd', 'ddk');

            } else if (_.isEqual(key, 'dak')) {
                obj = _.pick(bpArray, 'da', 'dak');

            } else if (_.isEqual(key, 'ahk')) {
                obj = _.pick(bpArray, 'ah', 'ahk');

            } else if (_.isEqual(key, 'adk')) {
                obj = _.pick(bpArray, 'ad', 'adk');

            } else {
                obj = _.pick(bpArray, 'aa', 'aak');
            }
            break;

        case 'fglg':
            if (_.isEqual(key, 'hfk')) {
                obj = _.pick(bpArray, 'hf', 'hfk');

            } else if (_.isEqual(key, 'hlk')) {
                obj = _.pick(bpArray, 'hl', 'hlk');

            } else if (_.isEqual(key, 'afk')) {
                obj = _.pick(bpArray, 'af', 'afk');

            } else if (_.isEqual(key, 'alk')) {
                obj = _.pick(bpArray, 'al', 'alk');

            } else  {
                obj = _.pick(bpArray, 'ng', 'ngk');
            }
            break;
    }
    return obj;
}

function cal_ou(OP, UP, F, U, CONFIG_MTACH, commission_type, odd_adjust, callback) {
    // console.log(OP, UP, F, U, CONFIG_MTACH, commission_type, odd_adjust);

    if(OP > 0 || UP>0 || OP < 0 || UP<0){
        const f_ou1st = parseInt(F);
        let op = parseFloat(OP).toFixed(2);
        // console.log('op : ', op);

        op = parseFloat(op * HUNDRED) + parseFloat(f_ou1st)  + parseInt(odd_adjust.o);

        // console.log('1. op : ', op);

        if(CONFIG_MTACH.h){
            op = parseFloat(op)+parseFloat(CONFIG_MTACH.h);
        }

        // console.log('2. op : ', op);

        const u_ou1st = parseInt(U);
        let up = parseFloat(UP).toFixed(2);
        up = parseFloat(up * HUNDRED) + parseFloat(u_ou1st)  + parseInt(odd_adjust.u);

        // console.log('1. up : ', up);

        if(CONFIG_MTACH.a){
            up = parseFloat(up)+parseFloat(CONFIG_MTACH.a);
        }

        // console.log('2. up : ', up);

        cal_commission_type(op, up, commission_type, (e, d)=>{

            op = d.tor;
            up = d.long;
        });

        let modal = {
            op: '',
            up: ''
        };

        if (op > 100 || op < -100) {
            // callback(null, convertPriceByOver(parseFloat(op/HUNDRED)))
            modal.op = convertPriceByOver(parseFloat(op / HUNDRED));
        } else {
            // callback(null, parseFloat(op/HUNDRED).toFixed(2).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0])
            modal.op = parseFloat(op / HUNDRED).toFixed(2).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
        }

        if (up > 100 || up < -100) {
            // callback(null, convertPriceByOver(parseFloat(up/HUNDRED)))
            modal.up = convertPriceByOver(parseFloat(up / HUNDRED));
        } else {
            // callback(null, parseFloat(up/HUNDRED).toFixed(2).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0])
            modal.up = parseFloat(up / HUNDRED).toFixed(2).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
        }

        callback(null, modal);

    } else {
        callback(null, '');
    }


    // console.log('modal => ', modal);

}
function cal_oe(O, E, F, U, CONFIG_MTACH, commission_type, odd_adjust, callback) {
    // console.log(O, E, F, U, CONFIG_MTACH, commission_type, odd_adjust);

    if(O>0 || E>0 || O<0 || E<0){
        // console.log(O, E, F, U);
        const f_oe1st = parseInt(F);
        const u_oe1st = parseInt(U);
        let o = parseFloat(O).toFixed(2);
        let e = parseFloat(E).toFixed(2);

        if (o === e || o > e) {
            o = parseFloat(o * HUNDRED) + parseFloat(u_oe1st);
            e = parseFloat(e * HUNDRED) + parseFloat(f_oe1st);

            cal_commission_type(e, o, commission_type, (eer, d)=>{
                e = d.tor + parseInt(odd_adjust.e);
                o = d.long + parseInt(odd_adjust.o);
            });
        } else if (o < e) {
            o = parseFloat(o * HUNDRED) + parseFloat(f_oe1st);
            e = parseFloat(e * HUNDRED) + parseFloat(u_oe1st);

            // console.log('2 ============== O : ', parseInt(o), ' ------------- E :', parseInt(e), '--------', commission_type);

            cal_commission_type(o, e, commission_type, (eer, d)=>{

                // console.log('TOR : ', d.tor, ', Long : ', d.long);
                o = d.tor + parseInt(odd_adjust.o);
                e = d.long  + parseInt(odd_adjust.e);

                // console.log('1 ============== O : ', parseInt(o), ' ------------- E :', parseInt(e));

            });
        }

        if(CONFIG_MTACH.a || CONFIG_MTACH.h){

            // console.log('------------ O : ', parseInt(o), ' ------------- E :', parseInt(e));

            o = parseInt(o)+parseInt(CONFIG_MTACH.h);
            e = parseInt(e)+parseInt(CONFIG_MTACH.a);
        }


        let modal = {
            o: '',
            e: ''
        };

        if (o > 100 || o < -100) {
            modal.o = convertPriceByOver(parseFloat(o / HUNDRED));
        } else {
            modal.o = parseFloat(o / HUNDRED).toFixed(2).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
        }

        if (e > 100 || e < -100) {
            modal.e = convertPriceByOver(parseFloat(e / HUNDRED));
        } else {
            modal.e = parseFloat(e / HUNDRED).toFixed(2).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
        }

        callback(null, modal);
    } else {
        callback(null, '')
    }
}
function cal_ah(H, A, HP, AP, F, U, CONFIG_MTACH, commission_type, odd_adjust, callback) {
    // console.log(H, A, HP, AP, F, U, CONFIG_MTACH, commission_type, odd_adjust);
    // console.log('=============================');
    // console.log(`H ${H} : A ${A}`);
    // console.log(`HP ${HP} : AP ${AP}`);
    // console.log(`Tor ${F} : Long ${U}`);

    if(HP>0 || AP>0 || HP<0 || AP<0){
        // console.log(H, A, HP, AP, F, U, CONFIG_MTACH);
        const f_ah1st = parseInt(F);
        const u_ah1st = parseInt(U);
        const h = parseFloat(H);
        const a = parseFloat(A);
        // console.log(`h ${h} : a ${a}`);
        let hp = parseFloat(HP).toFixed(2);
        let ap = parseFloat(AP).toFixed(2);
        // console.log(`hp ${hp} : ap ${ap}`);
        // console.log('-------------------', h);

        if (H === '0' && A === '0') {
            if (hp === ap) {
                // console.log('------ 1 ------');
                hp = parseFloat(hp * HUNDRED) + parseFloat(f_ah1st);
                ap = parseFloat(ap * HUNDRED) + parseFloat(u_ah1st);

                cal_commission_type(hp, ap, commission_type, (e, d)=>{
                    hp = d.tor  + parseInt(odd_adjust.home);
                    ap = d.long  + parseInt(odd_adjust.away);
                });
            } else if (hp < 0 || ap < 0) {
                if (hp < 0) {
                    // console.log('------ 2 ------');
                    hp = parseFloat(hp * HUNDRED) + parseFloat(u_ah1st);
                    ap = parseFloat(ap * HUNDRED) + parseFloat(f_ah1st);

                    cal_commission_type(ap, hp, commission_type, (e, d)=>{
                        ap = d.tor  + parseInt(odd_adjust.away);
                        hp = d.long  + parseInt(odd_adjust.home);
                    });
                } else if (ap < 0) {
                    // console.log('------ 3 ------');
                    hp = parseFloat(hp * HUNDRED) + parseFloat(f_ah1st);
                    ap = parseFloat(ap * HUNDRED) + parseFloat(u_ah1st);

                    cal_commission_type(hp, ap, commission_type, (e, d)=>{
                        hp = d.tor  + parseInt(odd_adjust.home);
                        ap = d.long  + parseInt(odd_adjust.away);
                    });
                }
            } else if (hp > 0 && ap > 0) {
                if (hp > ap) {
                    // console.log('------ 4 ------');
                    hp = parseFloat(hp * HUNDRED) + parseFloat(u_ah1st);
                    ap = parseFloat(ap * HUNDRED) + parseFloat(f_ah1st);

                    cal_commission_type(ap, hp, commission_type, (e, d)=>{
                        ap = d.tor  + parseInt(odd_adjust.away);
                        hp = d.long  + parseInt(odd_adjust.home);
                    });
                } else {
                    // console.log('------ 5 ------');
                    hp = parseFloat(hp * HUNDRED) + parseFloat(f_ah1st);
                    ap = parseFloat(ap * HUNDRED) + parseFloat(u_ah1st);

                    cal_commission_type(hp, ap, commission_type, (e, d)=>{
                        hp = d.tor  + parseInt(odd_adjust.home);
                        ap = d.long  + parseInt(odd_adjust.away);
                    });
                }
            }
        } else if (H.includes('-')) {
            // console.log(`H[${H}] is tor`);
            hp = parseFloat(hp * HUNDRED) + parseFloat(f_ah1st);
            ap = parseFloat(ap * HUNDRED) + parseFloat(u_ah1st);

            cal_commission_type(hp, ap, commission_type, (e, d)=>{
                hp = d.tor  + parseInt(odd_adjust.home);
                ap = d.long  + parseInt(odd_adjust.away);
            });
            // console.log(`hp ${hp} : ap ${ap}`);
        } else {
            // console.log(`H[${A}] is long`);
            hp = parseFloat(hp * HUNDRED) + parseFloat(u_ah1st);
            ap = parseFloat(ap * HUNDRED) + parseFloat(f_ah1st);

            cal_commission_type(ap, hp, commission_type, (e, d)=>{
                ap = d.tor  + parseInt(odd_adjust.away);
                hp = d.long  + parseInt(odd_adjust.home);
            });
            // console.log(`hp ${hp} : ap ${ap}`);
        }

        // console.log('---- ap : ', ap, ', ----- hp', hp);

        if(CONFIG_MTACH.a || CONFIG_MTACH.h){
            hp = parseFloat(hp)+parseFloat(CONFIG_MTACH.h);
            ap = parseFloat(ap)+parseFloat(CONFIG_MTACH.a);
        }

        let modal = {
            hp: '',
            ap: ''
        };

        // console.log(hp, ap);

        if (hp > 100 || hp < -100) {
            modal.hp = convertPriceByOver(parseFloat(hp / HUNDRED));
        } else {
            modal.hp = parseFloat(hp / HUNDRED).toFixed(2).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
        }

        if (ap > 100 || ap < -100) {
            modal.ap = convertPriceByOver(parseFloat(ap / HUNDRED));
        } else {
            modal.ap = parseFloat(ap / HUNDRED).toFixed(2).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
        }

        callback(null, modal);
    } else {
        callback(null, '')
    }

}
function cal_x12(H, A, D, CONFIG_MTACH, callback) {

    if(H>0 || A>0 || D>0 || H<0 || A<0 || D<0){
        // console.log(H, A, D, CONFIG_MTACH);
        const h_x12 = parseFloat(H);
        const d_x12 = parseFloat(D);
        const a_x12 = parseFloat(A);

        let h = parseFloat(h_x12)+(parseFloat(CONFIG_MTACH.h)/HUNDRED);
        let d = parseFloat(d_x12)+(parseFloat(CONFIG_MTACH.d)/HUNDRED);
        let a = parseFloat(a_x12)+(parseFloat(CONFIG_MTACH.a)/HUNDRED);

        // console.log(h, d, a);

        let modal = {
            h: h.toFixed(2).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0],
            d: d.toFixed(2).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0],
            a: a.toFixed(2).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]
        };

        callback(null, modal);
    } else {
        callback(null, '')
    }

}

function cal_ml(H, A, CONFIG_MTACH, callback) {
    // console.log("H ", H);
    // console.log("A ", A);
    // console.log("CONFIG_MTACH ", CONFIG_MTACH);

    if(H>0 || A>0 || H<0 || A<0){
        // console.log(H, A, D, CONFIG_MTACH);
        const h_x12 = parseFloat(H);
        const a_x12 = parseFloat(A);

        let h = parseFloat(h_x12)+(parseFloat(CONFIG_MTACH.h)/HUNDRED);
        let a = parseFloat(a_x12)+(parseFloat(CONFIG_MTACH.a)/HUNDRED);

        // console.log(h, d, a);

        let modal = {
            h: h.toFixed(2).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0],
            a: a.toFixed(2).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]
        };

        callback(null, modal);
    } else {
        callback(null, '')
    }

}

function cal_eu_ml(H, A, CONFIG_MTACH, callback) {

    if(H>0 || A>0 || H<0 || A<0){
        // console.log(H, A, D, CONFIG_MTACH);
        const h_x12 = parseFloat(H);
        const a_x12 = parseFloat(A);

        let h = parseFloat(h_x12)+(parseFloat(CONFIG_MTACH.h)/HUNDRED);
        let a = parseFloat(a_x12)+(parseFloat(CONFIG_MTACH.a)/HUNDRED);

        // console.log(h, d, a);

        let modal = {
            h: convertPriceToEU(parseFloat(h / HUNDRED)),
            a: convertPriceToEU(parseFloat(a / HUNDRED)),
        };

        callback(null, modal);
    } else {
        callback(null, '')
    }

}

function convertPriceByOver(price) {
    let convert_price = 0;

    if (price > 0) {
        convert_price = HUNDRED / price;
        convert_price = convert_price / HUNDRED;
        convert_price = convert_price.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
        return '-' + convert_price;
    } else if (price < 0) {
        convert_price = HUNDRED / price;
        convert_price = convert_price / HUNDRED;
        convert_price = convert_price.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
        return convert_price.replace('-', '');
    }
}
function switch_type(type_lower, bp, rp_hdp, modal, CONFIG_MTACH, commission_type, odd_adjust, callback) {
    // console.log('=================++++');
    // console.log('111111111   :   ',odd_adjust)
    // console.log('type_lower ', type_lower);
    // console.log('bp ', bp);
    // console.log('rp_hdp ', rp_hdp);
    // console.log('modal  ', modal);

    if (bp) {
        let ou1st = {o: 0, u: 0};
        let ou = {o: 0, u: 0};
        let oe = {o: 0, e: 0};
        let oe1st = {o: 0, e: 0};
        let ah = {home: 0, away: 0};
        let ah1st = {home: 0, away: 0};
        let ml = {home: 0, away: 0};
        if(_.size(odd_adjust) > 0){
            _.each(odd_adjust, (o)=>{
                if(o.prefix === 'ah' && bp.ah){

                    // console.log(o.value , bp);
                    if(o.key === 'hpk' && o.value === bp.ah.hpk){
                        ah.home += -o.count;
                        ah.away += o.count;
                    } else if(o.key === 'apk' && o.value === bp.ah.apk){
                        ah.home += o.count;
                        ah.away += -o.count;
                    }
                } else if(o.prefix === 'ah1st' && bp.ah1st){

                    if(o.key === 'hpk' && o.value === bp.ah1st.hpk){
                        ah1st.home += -o.count;
                        ah1st.away += o.count;
                    } else if(o.key === 'apk' && o.value === bp.ah1st.apk){
                        ah1st.home += o.count;
                        ah1st.away += -o.count;
                    }
                } else if(o.prefix === 'ou' && bp.ou){
                    if(o.key === 'opk' && o.value === bp.ou.opk){
                        ou.o += -o.count;
                        ou.u += o.count;
                    } else if(o.key === 'upk' && o.value === bp.ou.upk){
                        ou.o += o.count;
                        ou.u += -o.count;
                    }
                } else if(o.prefix === 'ou1st' && bp.ou1st){
                    if(o.key === 'opk' && o.value === bp.ou1st.opk){
                        ou1st.o += -o.count;
                        ou1st.u += o.count;
                    } else if(o.key === 'upk' && o.value === bp.ou1st.upk){
                        ou1st.o += o.count;
                        ou1st.u += -o.count;
                    }
                } else if(o.prefix === 'oe' && bp.oe){
                    if(o.key === 'ok' && o.value === bp.oe.ok){
                        oe.o += -o.count;
                        oe.e += o.count;
                    } else if(o.key === 'ek' && o.value === bp.oe.ek){
                        oe.o += o.count;
                        oe.e += -o.count;
                    }
                } else if(o.prefix === 'oe1st' && bp.oe1st){
                    if(o.key === 'ok' && o.value === bp.oe1st.ok){
                        oe1st.o += -o.count;
                        oe1st.e += o.count;
                    } else if(o.key === 'ek' && o.value === bp.oe1st.ek){
                        oe1st.o += o.count;
                        oe1st.e += -o.count;
                    }
                } else if(o.prefix === 'ml' && bp.ml){

                    // console.log(o.value , bp);
                    if(o.key === 'hk' && o.value === bp.ml.hk){
                        ah.home += -o.count;
                        ah.away += o.count;
                    } else if(o.key === 'ak' && o.value === bp.ml.ak){
                        ah.home += o.count;
                        ah.away += -o.count;
                    }
                }
            });
        }

        switch (type_lower) {
            case "ah":
                // console.log('ah : ', type_lower);
                cal_ah(bp.ah.h, bp.ah.a, bp.ah.hp, bp.ah.ap, rp_hdp.ah.f, rp_hdp.ah.u, CONFIG_MTACH.ah, commission_type, ah, (err, data) => {
                    if (data) {
                        modal.ah = {
                            hk: bp.ah.hk,
                            h: bp.ah.h,
                            ak: bp.ah.ak,
                            a: bp.ah.a,
                            hpk: bp.ah.hpk,
                            hp: data.hp,
                            apk: bp.ah.apk,
                            ap: data.ap
                        };

                        cal_price(modal.ah.hp, modal.ah.ap, (err, data)=>{
                            modal.s = data;
                        })
                    }
                });
                break;
            case "ah1st":
                // console.log('ah1st : ', type_lower);
                cal_ah(bp.ah1st.h, bp.ah1st.a, bp.ah1st.hp, bp.ah1st.ap, rp_hdp.ah1st.f, rp_hdp.ah1st.u, CONFIG_MTACH.ah1st, commission_type, ah1st, (err, data) => {
                    if (data) {
                        modal.ah1st = {
                            hk: bp.ah1st.hk,
                            h: bp.ah1st.h,
                            ak: bp.ah1st.ak,
                            a: bp.ah1st.a,
                            hpk: bp.ah1st.hpk,
                            hp: data.hp,
                            apk: bp.ah1st.apk,
                            ap: data.ap
                        };

                        cal_price(modal.ah1st.hp, modal.ah1st.ap, (err, data)=>{
                            modal.s = data;
                        })
                    }
                });
                break;
            case "ou":
                // console.log('=================++++');
                // console.log('ou : ', type_lower);
                // console.log(JSON.stringify(bp));
                // console.log('=================++++');
                cal_ou(bp.ou.op, bp.ou.up, rp_hdp.ou.f, rp_hdp.ou.u, CONFIG_MTACH.ou, commission_type, ou, (err, data) => {
                    if (data) {
                        modal.ou = {
                            ok: bp.ou.ok,
                            o: bp.ou.o,
                            uk: bp.ou.uk,
                            u: bp.ou.u,
                            opk: bp.ou.opk,
                            op: data.op,
                            upk: bp.ou.upk,
                            up: data.up
                        };

                        cal_price(modal.ou.op, modal.ou.up, (err, data)=>{
                            modal.s = data;
                        })
                    }
                });
                break;
            case "ou1st":
                // console.log('ou1st : ', type_lower);
                cal_ou(bp.ou1st.op, bp.ou1st.up, rp_hdp.ou1st.f, rp_hdp.ou1st.u, CONFIG_MTACH.ou1st, commission_type, ou1st, (err, data) => {
                    if (data) {
                        modal.ou1st = {
                            ok: bp.ou1st.ok,
                            o: bp.ou1st.o,
                            uk: bp.ou1st.uk,
                            u: bp.ou1st.u,
                            opk: bp.ou1st.opk,
                            op: data.op,
                            upk: bp.ou1st.upk,
                            up: data.up
                        };

                        cal_price(modal.ou1st.op, modal.ou1st.up, (err, data)=>{
                            modal.s = data;
                        })
                    }
                });
                break;
            case "x12":
                // console.log('x12 : ', type_lower, bp.x12);
                cal_x12(bp.x12.h, bp.x12.a, bp.x12.d, CONFIG_MTACH.x12, (err, data) => {
                    if (data) {
                        modal.x12 = {
                            hk: bp.x12.hk,
                            h: data.h,
                            ak: bp.x12.ak,
                            a: data.a,
                            dk: bp.x12.dk,
                            d: data.d
                        };

                        // cal_price(modal.x12.op, modal.x12.up, (err, data)=>{
                        //     modal.s = data;
                        // })
                    }
                });
                break;
            case "x121st":
                // console.log('x121st : ', type_lower);
                cal_x12(bp.x121st.h, bp.x121st.a, bp.x121st.d, CONFIG_MTACH.x121st, (err, data) => {
                    if (data) {
                        modal.x121st = {
                            hk: bp.x121st.hk,
                            h: data.h,
                            ak: bp.x121st.ak,
                            a: data.a,
                            dk: bp.x121st.dk,
                            d: data.d
                        };

                        // cal_price(modal.x121st.op, modal.x121st.up, (err, data)=>{
                        //     modal.s = data;
                        // })
                    }
                });

                break;
            case "oe":
                // console.log('oe : ', type_lower);
                if (bp.oe) {
                    cal_oe(bp.oe.o, bp.oe.e, rp_hdp.oe.f, rp_hdp.oe.u, CONFIG_MTACH.oe, commission_type, oe, (err, data) => {
                        if (data) {
                            modal.oe = {
                                ok: bp.oe.ok,
                                o: data.o,
                                ek: bp.oe.ek,
                                e: data.e
                            };
                        }

                        cal_price(modal.oe.o, modal.oe.e, (err, data)=>{
                            modal.s = data;
                        })
                    });
                }
                break;
            case "oe1st":
                // console.log('oe1st : ', type_lower);
                cal_oe(bp.oe1st.o, bp.oe1st.e, rp_hdp.oe1st.f, rp_hdp.oe1st.u, CONFIG_MTACH.oe1st, commission_type, oe1st, (err, data) => {
                    if (data) {
                        modal.oe1st = {
                            ok: bp.oe1st.ok,
                            o: data.o,
                            ek: bp.oe1st.ek,
                            e: data.e
                        };
                    }

                    cal_price(modal.oe1st.o, modal.oe1st.e, (err, data)=>{
                        modal.s = data;
                    })
                });
                break;

            case "ml":
                // console.log('ml : ', type_lower);
                cal_ml(bp.ml.h, bp.ml.a, CONFIG_MTACH.ml, (err, data) => {
                    // console.log("data ", data);
                    if (data) {
                        modal.ml = {
                            hk: bp.ml.hk,
                            h: data.h,
                            ak: bp.ml.ak,
                            a: data.a
                        };
                    }
                });

                break;
        }
        // console.log('modal =====> ', JSON.stringify(modal));
        callback(null, modal);
    } else {
        callback('Betprice is null', null);
    }
}
function switch_type_adjustmentByMatchList(type_lower, bp, rp_hdp, modal, CONFIG_MTACH, commission_type, odd_adjust, adjustmentByMatch, callback) {
    // console.log('=================++++');
    // console.log('111111111   :   ',odd_adjust)
    // console.log('type_lower ', type_lower);
    // console.log('bp ', bp);
    // console.log('rp_hdp ', rp_hdp);
    // console.log('modal  ', modal);
    console.log('adjustmentByMatch => ', adjustmentByMatch);
    if (bp) {
        let ou1st = {o: 0, u: 0};
        let ou = {o: 0, u: 0};
        let oe = {o: 0, e: 0};
        let oe1st = {o: 0, e: 0};
        let ah = {home: 0, away: 0};
        let ah1st = {home: 0, away: 0};
        let ml = {home: 0, away: 0};
        if(_.size(odd_adjust) > 0){
            _.each(odd_adjust, (o)=>{
                if(o.prefix === 'ah' && bp.ah){

                    // console.log(o.value , bp);
                    if(o.key === 'hpk' && o.value === bp.ah.hpk){
                        ah.home += -o.count;
                        ah.away += o.count;
                    } else if(o.key === 'apk' && o.value === bp.ah.apk){
                        ah.home += o.count;
                        ah.away += -o.count;
                    }
                } else if(o.prefix === 'ah1st' && bp.ah1st){

                    if(o.key === 'hpk' && o.value === bp.ah1st.hpk){
                        ah1st.home += -o.count;
                        ah1st.away += o.count;
                    } else if(o.key === 'apk' && o.value === bp.ah1st.apk){
                        ah1st.home += o.count;
                        ah1st.away += -o.count;
                    }
                } else if(o.prefix === 'ou' && bp.ou){
                    if(o.key === 'opk' && o.value === bp.ou.opk){
                        ou.o += -o.count;
                        ou.u += o.count;
                    } else if(o.key === 'upk' && o.value === bp.ou.upk){
                        ou.o += o.count;
                        ou.u += -o.count;
                    }
                } else if(o.prefix === 'ou1st' && bp.ou1st){
                    if(o.key === 'opk' && o.value === bp.ou1st.opk){
                        ou1st.o += -o.count;
                        ou1st.u += o.count;
                    } else if(o.key === 'upk' && o.value === bp.ou1st.upk){
                        ou1st.o += o.count;
                        ou1st.u += -o.count;
                    }
                } else if(o.prefix === 'oe' && bp.oe){
                    if(o.key === 'ok' && o.value === bp.oe.ok){
                        oe.o += -o.count;
                        oe.e += o.count;
                    } else if(o.key === 'ek' && o.value === bp.oe.ek){
                        oe.o += o.count;
                        oe.e += -o.count;
                    }
                } else if(o.prefix === 'oe1st' && bp.oe1st){
                    if(o.key === 'ok' && o.value === bp.oe1st.ok){
                        oe1st.o += -o.count;
                        oe1st.e += o.count;
                    } else if(o.key === 'ek' && o.value === bp.oe1st.ek){
                        oe1st.o += o.count;
                        oe1st.e += -o.count;
                    }
                } else if(o.prefix === 'ml' && bp.ml){

                    // console.log(o.value , bp);
                    if(o.key === 'hk' && o.value === bp.ml.hk){
                        ah.home += -o.count;
                        ah.away += o.count;
                    } else if(o.key === 'ak' && o.value === bp.ml.ak){
                        ah.home += o.count;
                        ah.away += -o.count;
                    }
                }
            });
        }

        switch (type_lower) {
            case "ah":
                ah = {
                    home: ah.home + adjustmentByMatch.ahAdjustmentByMatch.home,
                    away: ah.away + adjustmentByMatch.ahAdjustmentByMatch.away
                };
                cal_ah(bp.ah.h, bp.ah.a, bp.ah.hp, bp.ah.ap, rp_hdp.ah.f, rp_hdp.ah.u, CONFIG_MTACH.ah, commission_type, ah, (err, data) => {
                    if (data) {
                        modal.ah = {
                            hk: bp.ah.hk,
                            h: bp.ah.h,
                            ak: bp.ah.ak,
                            a: bp.ah.a,
                            hpk: bp.ah.hpk,
                            hp: data.hp,
                            apk: bp.ah.apk,
                            ap: data.ap
                        };

                        cal_price(modal.ah.hp, modal.ah.ap, (err, data)=>{
                            modal.s = data;
                        })
                    }
                });
                break;
            case "ah1st":
                ah1st = {
                    home: ah1st.home + adjustmentByMatch.ah1stAdjustmentByMatch.home,
                    away: ah1st.away + adjustmentByMatch.ah1stAdjustmentByMatch.away
                };
                cal_ah(bp.ah1st.h, bp.ah1st.a, bp.ah1st.hp, bp.ah1st.ap, rp_hdp.ah1st.f, rp_hdp.ah1st.u, CONFIG_MTACH.ah1st, commission_type, ah1st, (err, data) => {
                    if (data) {
                        modal.ah1st = {
                            hk: bp.ah1st.hk,
                            h: bp.ah1st.h,
                            ak: bp.ah1st.ak,
                            a: bp.ah1st.a,
                            hpk: bp.ah1st.hpk,
                            hp: data.hp,
                            apk: bp.ah1st.apk,
                            ap: data.ap
                        };

                        cal_price(modal.ah1st.hp, modal.ah1st.ap, (err, data)=>{
                            modal.s = data;
                        })
                    }
                });
                break;
            case "ou":
                ou = {
                    o: ou.o + adjustmentByMatch.ouAdjustmentByMatch.o,
                    u: ou.u + adjustmentByMatch.ouAdjustmentByMatch.u
                };
                cal_ou(bp.ou.op, bp.ou.up, rp_hdp.ou.f, rp_hdp.ou.u, CONFIG_MTACH.ou, commission_type, ou, (err, data) => {
                    if (data) {
                        modal.ou = {
                            ok: bp.ou.ok,
                            o: bp.ou.o,
                            uk: bp.ou.uk,
                            u: bp.ou.u,
                            opk: bp.ou.opk,
                            op: data.op,
                            upk: bp.ou.upk,
                            up: data.up
                        };

                        cal_price(modal.ou.op, modal.ou.up, (err, data)=>{
                            modal.s = data;
                        })
                    }
                });
                break;
            case "ou1st":
                ou1st = {
                    o: ou1st.o + adjustmentByMatch.ou1stAdjustmentByMatch.o,
                    u: ou1st.u + adjustmentByMatch.ou1stAdjustmentByMatch.u
                };
                cal_ou(bp.ou1st.op, bp.ou1st.up, rp_hdp.ou1st.f, rp_hdp.ou1st.u, CONFIG_MTACH.ou1st, commission_type, ou1st, (err, data) => {
                    if (data) {
                        modal.ou1st = {
                            ok: bp.ou1st.ok,
                            o: bp.ou1st.o,
                            uk: bp.ou1st.uk,
                            u: bp.ou1st.u,
                            opk: bp.ou1st.opk,
                            op: data.op,
                            upk: bp.ou1st.upk,
                            up: data.up
                        };

                        cal_price(modal.ou1st.op, modal.ou1st.up, (err, data)=>{
                            modal.s = data;
                        })
                    }
                });
                break;
            case "x12":
                // console.log('x12 : ', type_lower, bp.x12);
                cal_x12(bp.x12.h, bp.x12.a, bp.x12.d, CONFIG_MTACH.x12, (err, data) => {
                    if (data) {
                        modal.x12 = {
                            hk: bp.x12.hk,
                            h: data.h,
                            ak: bp.x12.ak,
                            a: data.a,
                            dk: bp.x12.dk,
                            d: data.d
                        };

                        // cal_price(modal.x12.op, modal.x12.up, (err, data)=>{
                        //     modal.s = data;
                        // })
                    }
                });
                break;
            case "x121st":
                // console.log('x121st : ', type_lower);
                cal_x12(bp.x121st.h, bp.x121st.a, bp.x121st.d, CONFIG_MTACH.x121st, (err, data) => {
                    if (data) {
                        modal.x121st = {
                            hk: bp.x121st.hk,
                            h: data.h,
                            ak: bp.x121st.ak,
                            a: data.a,
                            dk: bp.x121st.dk,
                            d: data.d
                        };

                        // cal_price(modal.x121st.op, modal.x121st.up, (err, data)=>{
                        //     modal.s = data;
                        // })
                    }
                });

                break;
            case "oe":
                // console.log('oe : ', type_lower);
                if (bp.oe) {
                    cal_oe(bp.oe.o, bp.oe.e, rp_hdp.oe.f, rp_hdp.oe.u, CONFIG_MTACH.oe, commission_type, oe, (err, data) => {
                        if (data) {
                            modal.oe = {
                                ok: bp.oe.ok,
                                o: data.o,
                                ek: bp.oe.ek,
                                e: data.e
                            };
                        }

                        cal_price(modal.oe.o, modal.oe.e, (err, data)=>{
                            modal.s = data;
                        })
                    });
                }
                break;
            case "oe1st":
                // console.log('oe1st : ', type_lower);
                cal_oe(bp.oe1st.o, bp.oe1st.e, rp_hdp.oe1st.f, rp_hdp.oe1st.u, CONFIG_MTACH.oe1st, commission_type, oe1st, (err, data) => {
                    if (data) {
                        modal.oe1st = {
                            ok: bp.oe1st.ok,
                            o: data.o,
                            ek: bp.oe1st.ek,
                            e: data.e
                        };
                    }

                    cal_price(modal.oe1st.o, modal.oe1st.e, (err, data)=>{
                        modal.s = data;
                    })
                });
                break;

            case "ml":
                // console.log('ml : ', type_lower);
                cal_ml(bp.ml.h, bp.ml.a, CONFIG_MTACH.ml, (err, data) => {
                    // console.log("data ", data);
                    if (data) {
                        modal.ml = {
                            hk: bp.ml.hk,
                            h: data.h,
                            ak: bp.ml.ak,
                            a: data.a
                        };
                    }
                });

                break;
        }
        // console.log('modal =====> ', JSON.stringify(modal));
        callback(null, modal);
    } else {
        callback('Betprice is null', null);
    }
}
function switch_min_max(type_lower, r, model, callback) {
    // console.log('############# switch_min_max #############');
    // console.log(`${model.min} : ${model.max}`);
    // console.log('=======>', type_lower, ' : ', JSON.stringify(r), ' : ', model, '<==========');
    switch (type_lower) {
        case "ah":
            // console.log('ah : ', type_lower);
            model.min = r.ah.mi;
            model.max = r.ah.ma;
            break;
        case "ah1st":
            // console.log('ah1st : ', type_lower);
            model.min = r.ah1st.mi;
            model.max = r.ah1st.ma;
            break;
        case "ou":
            // console.log('ou : ', type_lower);
            model.min = r.ou.mi;
            model.max = r.ou.ma;
            break;
        case "ou1st":
            // console.log('ou1st : ', type_lower);
            model.min = r.ou1st.mi;
            model.max = r.ou1st.ma;
            break;
        case "x12":
            // console.log('x12 : ', type_lower, bp.x12);
            model.min = r.x12.mi;
            model.max = r.x12.ma;
            break;
        case "x121st":
            // console.log('x121st : ', type_lower);
            model.min = r.x121st.mi;
            model.max = r.x121st.ma;
            break;
        case "oe":
            // console.log('oe : ', type_lower);
            model.min = r.oe.mi;
            model.max = r.oe.ma;
            break;
        case "oe1st":
            // console.log('oe1st : ', type_lower);
            model.min = r.oe1st.mi;
            model.max = r.oe1st.ma;
            break;

        case "ml":
            // console.log('oe1st : ', type_lower);
            model.min = r.ml.mi;
            model.max = r.ml.ma;
            break;
    }
    // console.log(`${model.min} : ${model.max}`);
    // console.log('############# switch_min_max #############');
    callback(null, model);
}

function cal_bp_parlay(matchId, type, key, commission_type, odd_adjust, callback)  {
    const k = matchId.split(':');

    async.series([(cb) => {
        FootballModel.findOne({k: k[0]},
            (err, data) => {

                // console.log(data);
                if (err) {
                    cb(err, null)
                } else if (data) {
                    cb(null, data)
                } else {
                    cb('league is null', null)
                }
            })
    }, (cb) => {

        // console.log(`${URL_CACHE_HDP_SINGLE_MATCH}/${matchId}`);

        const headers = {
            'Content-Type': 'application/json'
        };

        const option = {
            url: `${URL_CACHE_HDP_SINGLE_MATCH}/${matchId}`,
            method: 'GET',
            headers: headers
        };

        // console.log(option);

        request(option, (err, response, body) => {
            if (err) {
                cb(err, null)
            } else {
                let result = JSON.parse(body);

                // console.log(result);

                if (result) {
                    cb(null, result);
                } else {
                    cb(null, []);
                }
            }
        });
    }], (err, results) => {
        if (err) {
            callback(err, null)
        } else {
            const type_lower = type.toLowerCase();
            // console.log(type_lower);
            const rp_hdp = results[0].rp.hdp;
            let match_cache = results[1];
            let bp = _.first(_.filter(match_cache.result, league => {
                return _.contains(league.allKey, key)
            }));

            let model_result = {
                matchId: `${match_cache.league_key}:${match_cache.match_key}`,
                league_name: match_cache.league_name,
                league_name_n: match_cache.league_name_n,
                name: match_cache.name,
                date: match_cache.date
            };

            const resultBP = _.first(_.chain(results)
                .filter(league => {
                    return _.isEqual(league.k, parseInt(k[0]))
                })
                .flatten(true)
                .pluck('m')
                .flatten(true)
                .filter(matchAPI => {
                    return _.isEqual(matchAPI.id, matchId)
                })
                .pluck('bp')
                .flatten(true)
                .filter(betPrice => {
                    return _.contains(betPrice.allKey, key);
                })
                .pluck('rp')
                .flatten(true)
                .value());

            switch_type_parlay(type_lower, bp, rp_hdp, model_result, resultBP, commission_type, odd_adjust, (err, result) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, result);
                }
            })
        }
    })
}
function convertPriceToEU(price) {
    let convert_price = 0;

    // console.log('----- 1 -----', price);

    if (price > 0) {
        convert_price = price + 1;
        // console.log('----- 2 -----', convert_price);
        convert_price = convert_price.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];

        // console.log('----- 3 -----', convert_price);
        return convert_price;
    } else if (price < 0) {
        convert_price = 1 / price.toString().replace('-', '');
        convert_price = convert_price + 1;
        // console.log('----- 2 -----', convert_price);
        convert_price = convert_price.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
        // console.log('----- 3 -----', convert_price);
        return convert_price.replace('-', '');
    }
}
function cal_eu_ou_sbo(OP, UP, F, U, CONFIG_MTACH, commission_type, odd_adjust, callback) {
    // console.log(OP, UP, F, U, CONFIG_MTACH);

    if(OP>0 || UP>0 || OP<0 || UP<0){
        const f_ou1st = parseInt(F);
        let op = parseFloat(OP).toFixed(2);
        op = parseFloat(op * HUNDRED) + parseFloat(f_ou1st)   + parseInt(odd_adjust.o);

        // console.log('1. op : ', op);

        if(CONFIG_MTACH.h){
            op = parseFloat(op)+parseFloat(CONFIG_MTACH.h);
        }

        // console.log('2. op : ', op);

        const u_ou1st = parseInt(U);
        let up = parseFloat(UP).toFixed(2);
        up = parseFloat(up * HUNDRED) + parseFloat(u_ou1st)   + parseInt(odd_adjust.u);

        // console.log('1. up : ', up);

        if(CONFIG_MTACH.a){
            up = parseFloat(up)+parseFloat(CONFIG_MTACH.a);
        }

        // console.log('2. up : ', up);
        cal_commission_type(op, up, commission_type, (e, d)=>{

            op = d.tor;
            up = d.long;
        });

        // console.log('---- op : ', op, ', ---- up : ', up);

        let modal = {
            op: '',
            up: ''
        };

        modal.op = parseFloat(op / HUNDRED).toFixed(2).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
        modal.up = parseFloat(up / HUNDRED).toFixed(2).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];

        callback(null, modal);
    } else {
        callback(null, '')
    }

}
function cal_eu_oe_sbo(O, E, F, U, CONFIG_MTACH, commission_type, odd_adjust, callback) {

    if(O>0 || E>0 || O<0 || E<0){
        // console.log(O, E, F, U);
        const f_oe1st = parseInt(F);
        const u_oe1st = parseInt(U);
        let o = parseFloat(O).toFixed(2);
        let e = parseFloat(E).toFixed(2);

        if (o === e || o > e) {
            o = parseFloat(o * HUNDRED) + parseFloat(u_oe1st);
            e = parseFloat(e * HUNDRED) + parseFloat(f_oe1st);
            cal_commission_type(e, o, commission_type, (err, d)=>{
                e = d.tor + parseInt(odd_adjust.e);
                o = d.long + parseInt(odd_adjust.o);;
            });

        } else if (o < e) {
            o = parseFloat(o * HUNDRED) + parseFloat(f_oe1st);
            e = parseFloat(e * HUNDRED) + parseFloat(u_oe1st);
            cal_commission_type(o, e, commission_type, (err, d)=>{
                o = d.tor + parseInt(odd_adjust.o);;
                e = d.long + parseInt(odd_adjust.e);
            });
        }

        if(CONFIG_MTACH.a || CONFIG_MTACH.h){
            o = parseFloat(o)+parseFloat(CONFIG_MTACH.h);
            e = parseFloat(e)+parseFloat(CONFIG_MTACH.a);
        }

        let modal = {
            o: '',
            e: ''
        };

        modal.o = parseFloat(o / HUNDRED).toFixed(2).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
        modal.e = parseFloat(e / HUNDRED).toFixed(2).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];

        callback(null, modal);
    } else {
        callback(null, '')
    }

}
function cal_eu_ah_sbo(H, A, HP, AP, F, U, CONFIG_MTACH,commission_type, odd_adjust, callback) {
    // console.log('cal_eu_ah_sbo');
    // console.log('H : ', H,', A: ', A,', HP:', HP,', AP:', AP,', F:', F,', U:', U,', CONFIG_MTACH: ',  CONFIG_MTACH,', commission_type: ',  commission_type,', odd_adjust: ', odd_adjust);

    if(HP>0 || AP>0 || HP<0 || AP<0){
        // console.log(H, A, HP, AP, F, U, CONFIG_MTACH);
        const f_ah1st = parseInt(F);
        const u_ah1st = parseInt(U);
        const h = parseFloat(H);
        const a = parseFloat(A);
        let hp = parseFloat(HP).toFixed(2);
        let ap = parseFloat(AP).toFixed(2);

        // console.log('-------------------', h);

        if (H === '0' && A === '0') {
            if (hp === ap) {
                // console.log('------ 1 ------');
                hp = parseFloat(hp * HUNDRED) + parseFloat(f_ah1st);
                ap = parseFloat(ap * HUNDRED) + parseFloat(u_ah1st);

                cal_commission_type(hp, ap, commission_type, (e, d)=>{
                    hp = d.tor + parseInt(odd_adjust.home);
                    ap = d.long + parseInt(odd_adjust.away);
                });
            } else if (hp < 0 || ap < 0) {
                if (hp < 0) {
                    // console.log('------ 2 ------');
                    hp = parseFloat(hp * HUNDRED) + parseFloat(u_ah1st);
                    ap = parseFloat(ap * HUNDRED) + parseFloat(f_ah1st);
                    cal_commission_type(ap, hp, commission_type, (e, d)=>{
                        ap = d.tor + parseInt(odd_adjust.away);
                        hp = d.long + parseInt(odd_adjust.home);
                    });
                } else if (ap < 0) {
                    // console.log('------ 3 ------');
                    hp = parseFloat(hp * HUNDRED) + parseFloat(f_ah1st);
                    ap = parseFloat(ap * HUNDRED) + parseFloat(u_ah1st);
                    cal_commission_type(hp, ap, commission_type, (e, d)=>{
                        hp = d.tor + parseInt(odd_adjust.home);
                        ap = d.long + parseInt(odd_adjust.away);
                    });
                }
            } else if (hp > 0 && ap > 0) {
                if (hp > ap) {
                    // console.log('------ 4 ------');
                    hp = parseFloat(hp * HUNDRED) + parseFloat(u_ah1st);
                    ap = parseFloat(ap * HUNDRED) + parseFloat(f_ah1st);
                    cal_commission_type(ap, hp, commission_type, (e, d)=>{
                        ap = d.tor + parseInt(odd_adjust.away);
                        hp = d.long + parseInt(odd_adjust.home);
                    });
                } else {
                    // console.log('------ 5 ------');
                    hp = parseFloat(hp * HUNDRED) + parseFloat(f_ah1st);
                    ap = parseFloat(ap * HUNDRED) + parseFloat(u_ah1st);
                    cal_commission_type(hp, ap, commission_type, (e, d)=>{
                        hp = d.tor + parseInt(odd_adjust.home);
                        ap = d.long + parseInt(odd_adjust.away);
                    });
                }
            }
        } else if (H.includes('-')) {
            // console.log('------ 6 ------');
            hp = parseFloat(hp * HUNDRED) + parseFloat(f_ah1st);
            ap = parseFloat(ap * HUNDRED) + parseFloat(u_ah1st);
            cal_commission_type(hp, ap, commission_type, (e, d)=>{
                hp = d.tor + parseInt(odd_adjust.home);
                ap = d.long + parseInt(odd_adjust.away);
            });
        } else {
            // console.log('------ 7 ------');
            hp = parseFloat(hp * HUNDRED) + parseFloat(u_ah1st);
            ap = parseFloat(ap * HUNDRED) + parseFloat(f_ah1st);

            // console.log('---- ap : ', ap, ', ----- hp', hp);

            cal_commission_type(ap, hp, commission_type, (e, d)=>{
                ap = d.tor + parseInt(odd_adjust.away);
                hp = d.long + parseInt(odd_adjust.home);
            });
        }


        if(CONFIG_MTACH.a || CONFIG_MTACH.h){
            hp = parseFloat(hp)+parseFloat(CONFIG_MTACH.h);
            ap = parseFloat(ap)+parseFloat(CONFIG_MTACH.a);
        }

        let modal = {
            hp: '',
            ap: ''
        };

        // console.log(hp, ap);

        modal.hp = parseFloat(hp / HUNDRED).toFixed(2).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
        modal.ap = parseFloat(ap / HUNDRED).toFixed(2).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];

        callback(null, modal);
    } else {
        callback(null, '')
    }

}


function cal_eu_ou(OP, UP, F, U, CONFIG_MTACH, commission_type, odd_adjust, callback) {
    // console.log(OP, UP, F, U, CONFIG_MTACH);

    if(OP>0 || UP>0 || OP<0 || UP<0){
        const f_ou1st = parseInt(F);
        let op = parseFloat(OP).toFixed(2);
        op = parseFloat(op * HUNDRED) + parseFloat(f_ou1st)   + parseInt(odd_adjust.o);

        // console.log('1. op : ', op);

        if(CONFIG_MTACH.h){
            op = parseFloat(op)+parseFloat(CONFIG_MTACH.h);
        }

        // console.log('2. op : ', op);

        const u_ou1st = parseInt(U);
        let up = parseFloat(UP).toFixed(2);
        up = parseFloat(up * HUNDRED) + parseFloat(u_ou1st)   + parseInt(odd_adjust.u);

        // console.log('1. up : ', up);

        if(CONFIG_MTACH.a){
            up = parseFloat(up)+parseFloat(CONFIG_MTACH.a);
        }

        // console.log('2. up : ', up);
        cal_commission_type(op, up, commission_type, (e, d)=>{

            op = d.tor;
            up = d.long;
        });

        // console.log('---- op : ', op, ', ---- up : ', up);

        let modal = {
            op: '',
            up: ''
        };

        modal.op = convertPriceToEU(parseFloat(op / HUNDRED));
        modal.up = convertPriceToEU(parseFloat(up / HUNDRED));

        callback(null, modal);
    } else {
        callback(null, '')
    }

}
function cal_eu_oe(O, E, F, U, CONFIG_MTACH, commission_type, odd_adjust, callback) {

    if(O>0 || E>0 || O<0 || E<0){
        // console.log(O, E, F, U);
        const f_oe1st = parseInt(F);
        const u_oe1st = parseInt(U);
        let o = parseFloat(O).toFixed(2);
        let e = parseFloat(E).toFixed(2);

        if (o === e || o > e) {
            o = parseFloat(o * HUNDRED) + parseFloat(u_oe1st);
            e = parseFloat(e * HUNDRED) + parseFloat(f_oe1st);
            cal_commission_type(e, o, commission_type, (err, d)=>{
                e = d.tor + parseInt(odd_adjust.e);
                o = d.long + parseInt(odd_adjust.o);;
            });

        } else if (o < e) {
            o = parseFloat(o * HUNDRED) + parseFloat(f_oe1st);
            e = parseFloat(e * HUNDRED) + parseFloat(u_oe1st);
            cal_commission_type(o, e, commission_type, (err, d)=>{
                o = d.tor + parseInt(odd_adjust.o);;
                e = d.long + parseInt(odd_adjust.e);
            });
        }

        if(CONFIG_MTACH.a || CONFIG_MTACH.h){
            o = parseFloat(o)+parseFloat(CONFIG_MTACH.h);
            e = parseFloat(e)+parseFloat(CONFIG_MTACH.a);
        }

        let modal = {
            o: '',
            e: ''
        };

        modal.o = convertPriceToEU(parseFloat(o / HUNDRED));
        modal.e = convertPriceToEU(parseFloat(e / HUNDRED));

        callback(null, modal);
    } else {
        callback(null, '')
    }

}
function cal_eu_ah(H, A, HP, AP, F, U, CONFIG_MTACH,commission_type, odd_adjust, callback) {

    // console.log('H : ', H,', A: ', A,', HP:', HP,', AP:', AP,', F:', F,', U:', U,', CONFIG_MTACH: ',  CONFIG_MTACH,', commission_type: ',  commission_type,', odd_adjust: ', odd_adjust);

    if(HP>0 || AP>0 || HP<0 || AP<0){
        // console.log(H, A, HP, AP, F, U, CONFIG_MTACH);
        const f_ah1st = parseInt(F);
        const u_ah1st = parseInt(U);
        const h = parseFloat(H);
        const a = parseFloat(A);
        let hp = parseFloat(HP).toFixed(2);
        let ap = parseFloat(AP).toFixed(2);

        // console.log('-------------------', h);

        if (h === 0 && a === 0) {
            if (hp === ap) {
                // console.log('------ 1 ------');
                hp = parseFloat(hp * HUNDRED) + parseFloat(f_ah1st);
                ap = parseFloat(ap * HUNDRED) + parseFloat(u_ah1st);

                cal_commission_type(hp, ap, commission_type, (e, d)=>{
                    hp = d.tor + parseInt(odd_adjust.home);
                    ap = d.long + parseInt(odd_adjust.away);
                });
            } else if (hp < 0 || ap < 0) {
                if (hp < 0) {
                    // console.log('------ 2 ------');
                    hp = parseFloat(hp * HUNDRED) + parseFloat(u_ah1st);
                    ap = parseFloat(ap * HUNDRED) + parseFloat(f_ah1st);
                    cal_commission_type(ap, hp, commission_type, (e, d)=>{
                        ap = d.tor + parseInt(odd_adjust.away);
                        hp = d.long + parseInt(odd_adjust.home);
                    });
                } else if (ap < 0) {
                    // console.log('------ 3 ------');
                    hp = parseFloat(hp * HUNDRED) + parseFloat(f_ah1st);
                    ap = parseFloat(ap * HUNDRED) + parseFloat(u_ah1st);
                    cal_commission_type(hp, ap, commission_type, (e, d)=>{
                        hp = d.tor + parseInt(odd_adjust.home);
                        ap = d.long + parseInt(odd_adjust.away);
                    });
                }
            } else if (hp > 0 && ap > 0) {
                if (hp > ap) {
                    // console.log('------ 4 ------');
                    hp = parseFloat(hp * HUNDRED) + parseFloat(u_ah1st);
                    ap = parseFloat(ap * HUNDRED) + parseFloat(f_ah1st);
                    cal_commission_type(ap, hp, commission_type, (e, d)=>{
                        ap = d.tor + parseInt(odd_adjust.away);
                        hp = d.long + parseInt(odd_adjust.home);
                    });
                } else {
                    // console.log('------ 5 ------');
                    hp = parseFloat(hp * HUNDRED) + parseFloat(f_ah1st);
                    ap = parseFloat(ap * HUNDRED) + parseFloat(u_ah1st);
                    cal_commission_type(hp, ap, commission_type, (e, d)=>{
                        hp = d.tor + parseInt(odd_adjust.home);
                        ap = d.long + parseInt(odd_adjust.away);
                    });
                }
            }
        } else if (h < 0) {
            // console.log('------ 6 ------');
            hp = parseFloat(hp * HUNDRED) + parseFloat(f_ah1st);
            ap = parseFloat(ap * HUNDRED) + parseFloat(u_ah1st);
            cal_commission_type(hp, ap, commission_type, (e, d)=>{
                hp = d.tor + parseInt(odd_adjust.home);
                ap = d.long + parseInt(odd_adjust.away);
            });
        } else {
            // console.log('------ 7 ------');
            hp = parseFloat(hp * HUNDRED) + parseFloat(u_ah1st);
            ap = parseFloat(ap * HUNDRED) + parseFloat(f_ah1st);

            // console.log('---- ap : ', ap, ', ----- hp', hp);

            cal_commission_type(ap, hp, commission_type, (e, d)=>{
                ap = d.tor + parseInt(odd_adjust.away);
                hp = d.long + parseInt(odd_adjust.home);
            });
        }


        if(CONFIG_MTACH.a || CONFIG_MTACH.h){
            hp = parseFloat(hp)+parseFloat(CONFIG_MTACH.h);
            ap = parseFloat(ap)+parseFloat(CONFIG_MTACH.a);
        }

        let modal = {
            hp: '',
            ap: ''
        };

        // console.log(hp, ap);

        modal.hp = convertPriceToEU(parseFloat(hp / HUNDRED));
        modal.ap = convertPriceToEU(parseFloat(ap / HUNDRED));

        callback(null, modal);
    } else {
        callback(null, '')
    }

}

function switch_type_parlay(type_lower, bp, rp_hdp, modal, CONFIG_MTACH, commission_type, odd_adjust, callback) {

    let ou1st = {o: 0, u: 0};
    let ou = {o: 0, u: 0};
    let oe = {o: 0, e: 0};
    let oe1st = {o: 0, e: 0};
    let ah = {home: 0, away: 0};
    let ah1st = {home: 0, away: 0};

    if(_.size(odd_adjust) > 0){
        _.each(odd_adjust, (o)=>{
            if(o.prefix === 'ah' && bp.ah){
                if(o.key === 'hpk' && o.value === bp.ah.hpk){
                    ah = {
                        home: -o.count,
                        away: o.count
                    }
                } else if(o.key === 'apk' && o.value === bp.ah.apk){
                    ah = {
                        home: o.count,
                        away: -o.count
                    }
                }
            } else if(o.prefix === 'ah1st' && bp.ah1st){

                if(o.key === 'hpk' && o.value === bp.ah1st.hpk){
                    ah1st = {
                        home: -o.count,
                        away: o.count
                    }
                } else if(o.key === 'apk' && o.value === bp.ah1st.apk){
                    ah1st = {
                        home: o.count,
                        away: -o.count
                    }
                }
            } else if(o.prefix === 'ou' && bp.ou){
                // console.log(o.value , bp.ou.opk);
                if(o.key === 'opk' && o.value === bp.ou.opk){
                    ou = {
                        o: -o.count,
                        u: o.count
                    }
                } else if(o.key === 'upk' && o.value === bp.ou.upk){
                    ou = {
                        o: o.count,
                        u: -o.count
                    }
                }
            } else if(o.prefix === 'ou1st' && bp.ou1st){
                if(o.key === 'opk' && o.value === bp.ou1st.opk){
                    ou1st = {
                        o: -o.count,
                        u: o.count
                    }
                } else if(o.key === 'upk' && o.value === bp.ou1st.upk){
                    ou1st = {
                        o: o.count,
                        u: -o.count
                    }
                }
            } else if(o.prefix === 'oe' && bp.oe){
                if(o.key === 'ok' && o.value === bp.oe.ok){
                    oe = {
                        o: -o.count,
                        e: o.count
                    }
                } else if(o.key === 'ek' && o.value === bp.oe.ek){
                    oe = {
                        o: o.count,
                        e: -o.count
                    }
                }
            } else if(o.prefix === 'oe1st' && bp.oe1st){
                if(o.key === 'ok' && o.value === bp.oe1st.ok){
                    oe1st = {
                        o: -o.count,
                        e: o.count
                    }
                } else if(o.key === 'ek' && o.value === bp.oe1st.ek){
                    oe1st = {
                        o: o.count,
                        e: -o.count
                    }
                }
            }
        });
    }

    // console.log(type_lower, bp, rp_hdp, modal);
    if (bp) {
        switch (type_lower) {
            case "ah":
                // console.log('ah : ', type_lower);
                cal_eu_ah(bp.ah.h, bp.ah.a, bp.ah.hp, bp.ah.ap, rp_hdp.ah.f, rp_hdp.ah.u, CONFIG_MTACH.ah, commission_type, ah, (err, data) => {
                    if (data) {
                        modal.ah = {
                            hk: bp.ah.hk,
                            h: bp.ah.h,
                            ak: bp.ah.ak,
                            a: bp.ah.a,
                            hpk: bp.ah.hpk,
                            hp: data.hp,
                            apk: bp.ah.apk,
                            ap: data.ap
                        };

                        // cal_price(modal.ah.hp, modal.ah.ap, (err, data)=>{
                        //     modal.s = data;
                        // })
                    }
                });
                break;
            case "ah1st":
                // console.log('ah1st : ', type_lower);
                cal_eu_ah(bp.ah1st.h, bp.ah1st.a, bp.ah1st.hp, bp.ah1st.ap, rp_hdp.ah1st.f, rp_hdp.ah1st.u, CONFIG_MTACH.ah1st, commission_type, ah1st, (err, data) => {
                    if (data) {
                        modal.ah1st = {
                            hk: bp.ah1st.hk,
                            h: bp.ah1st.h,
                            ak: bp.ah1st.ak,
                            a: bp.ah1st.a,
                            hpk: bp.ah1st.hpk,
                            hp: data.hp,
                            apk: bp.ah1st.apk,
                            ap: data.ap
                        };

                        // cal_price(modal.ah1st.hp, modal.ah1st.ap, (err, data)=>{
                        //     modal.s = data;
                        // })
                    }
                });
                break;
            case "ou":
                // console.log('ou : ', type_lower);
                cal_eu_ou(bp.ou.op, bp.ou.up, rp_hdp.ou.f, rp_hdp.ou.u, CONFIG_MTACH.ou, commission_type, ou, (err, data) => {
                    if (data) {
                        modal.ou = {
                            ok: bp.ou.ok,
                            o: bp.ou.o,
                            uk: bp.ou.uk,
                            u: bp.ou.u,
                            opk: bp.ou.opk,
                            op: data.op,
                            upk: bp.ou.upk,
                            up: data.up
                        };

                        // cal_price(modal.ou.op, modal.ou.up, (err, data)=>{
                        //     modal.s = data;
                        // })
                    }
                });
                break;
            case "ou1st":
                // console.log('ou1st : ', type_lower);
                cal_eu_ou(bp.ou1st.op, bp.ou1st.up, rp_hdp.ou1st.f, rp_hdp.ou1st.u, CONFIG_MTACH.ou1st, commission_type, ou1st, (err, data) => {
                    if (data) {
                        modal.ou1st = {
                            ok: bp.ou1st.ok,
                            o: bp.ou1st.o,
                            uk: bp.ou1st.uk,
                            u: bp.ou1st.u,
                            opk: bp.ou1st.opk,
                            op: data.op,
                            upk: bp.ou1st.upk,
                            up: data.up
                        };

                        // cal_price(modal.ou1st.op, modal.ou1st.up, (err, data)=>{
                        //     modal.s = data;
                        // })
                    }
                });
                break;
            case "x12":
                // console.log('x12 : ', type_lower, bp.x12);
                cal_x12(bp.x12.h, bp.x12.a, bp.x12.d, CONFIG_MTACH.x12, (err, data) => {
                    if (data) {
                        modal.x12 = {
                            hk: bp.x12.hk,
                            h: data.h,
                            ak: bp.x12.ak,
                            a: data.a,
                            dk: bp.x12.dk,
                            d: data.d
                        };

                        // cal_price(modal.ou1st.op, modal.ou1st.up, (err, data)=>{
                        //     modal.s = data;
                        // })
                    }
                });
                break;
            case "x121st":
                // console.log('x121st : ', type_lower);
                cal_x12(bp.x121st.h, bp.x121st.a, bp.x121st.d, CONFIG_MTACH.x121st, (err, data) => {
                    if (data) {
                        modal.x121st = {
                            hk: bp.x121st.hk,
                            h: data.h,
                            ak: bp.x121st.ak,
                            a: data.a,
                            dk: bp.x121st.dk,
                            d: data.d
                        };

                        // cal_price(modal.ou1st.op, modal.ou1st.up, (err, data)=>{
                        //     modal.s = data;
                        // })
                    }
                });

                break;
            case "oe":
                // console.log('oe : ', type_lower);
                if (bp.oe) {
                    cal_eu_oe(bp.oe.o, bp.oe.e, rp_hdp.oe.f, rp_hdp.oe.u, CONFIG_MTACH.oe, commission_type, oe, (err, data) => {
                        if (data) {
                            modal.oe = {
                                ok: bp.oe.ok,
                                o: data.o,
                                ek: bp.oe.ek,
                                e: data.e
                            };
                        }

                        // cal_price(modal.oe.o, modal.oe.e, (err, data)=>{
                        //     modal.s = data;
                        // })
                    });
                }
                break;
            case "oe1st":
                // console.log('oe1st : ', type_lower);
                cal_eu_oe(bp.oe1st.o, bp.oe1st.e, rp_hdp.oe1st.f, rp_hdp.oe1st.u, CONFIG_MTACH.oe1st, commission_type, oe1st, (err, data) => {
                    if (data) {
                        modal.oe1st = {
                            ok: bp.oe1st.ok,
                            o: data.o,
                            ek: bp.oe1st.ek,
                            e: data.e
                        };
                    }

                    // cal_price(modal.oe1st.o, modal.oe1st.e, (err, data)=>{
                    //     modal.s = data;
                    // })
                });
                break;
        }

        callback(null, modal);
    } else {
        callback('Betprice is null', null);
    }
}

function call_master_on_ticket(betId, match, callback) {
    request.post({
            url: `http://${process.env.MASTER_IP||'128.199.82.68'}:8001/sportbook/ticket/insert_ticket`,
            method: "POST",
            json: true,
            body: {
                betId: betId,
                clientName: process.env.CLIENT_NAME||'AMB_SPORTBOOK',
                match: match
            },
            timeout: 15000
        },
        function (err, response, body) {

            if(err){
                callback(err, null);
            } else if(body.code === 0){
                callback(null, 'success');
            } else {
                callback(body.result, null);
            }

        });

}

function call_master_on_ticket_sbo(betId, match, callback) {
    // console.log('call_master_on_ticket_sbo => ',JSON.stringify(match));
    request.post({
            url: `http://lb-sbo-master.api-hub.com/ticket/insert`,
            method: "POST",
            json: true,
            body: {
                betId: betId,
                clientName: process.env.CLIENT_NAME || 'AMB_SPORTBOOK',
                match: match
            },
            timeout: 15000
        },
        function (err, response, body) {

            if (err) {
                callback(err, null);
            } else if (body.code === 0) {
                callback(null, 'success');
            } else {
                callback(body.result, null);
            }

        });
}

function call_master_on_ticket_sbo2(betId, match, username, callback) {
    console.log('call_master_on_ticket_sbo2 => ', process.env.CLIENT_NAME , ' ',JSON.stringify(match));
    request.post({
            url: `http://lb-sbo-master.api-hub.com/ticket/insert`,
            method: "POST",
            json: true,
            body: {
                betId: betId,
                clientName: process.env.CLIENT_NAME || 'AMB_SPORTBOOK',
                match: match,
                username: username
            },
            timeout: 15000
        },
        function (err, response, body) {

            if (err) {
                callback(err, null);
            } else if (body.code === 0) {
                callback(null, 'success');
            } else {
                callback(body.result, null);
            }

        });
}


function call_master_on_ticket_sbo_special(betId, match, callback) {
    console.log('call_master_on_ticket_sbo_special => ',JSON.stringify(match));
    request.post({
            url: `http://lb-sbo-master.api-hub.com/ticketSpecial/insert`,
            method: "POST",
            json: true,
            body: {
                betId: betId,
                clientName: process.env.CLIENT_NAME || 'AMB_SPORTBOOK',
                match: match
            },
            timeout: 15000
        },
        function (err, response, body) {

            if (err) {
                callback(err, null);
            } else if (body.code === 0) {
                callback(null, 'success');
            } else {
                callback(body.result, null);
            }

        });
}

function call_master_on_ticket_mix_parlay_sbo(betId, matchList, callback) {
    console.log('call_master_on_ticket_mix_parlay_sbo => ',JSON.stringify(matchList));
    request.post({
            url: `http://lb-sbo-master.api-hub.com/ticket/insertMixParlay`,
            method: "POST",
            json: true,
            body: {
                betId: betId,
                clientName: process.env.CLIENT_NAME||'AMB_SPORTBOOK',
                match: matchList
            },
            timeout: 15000
        },
        function (err, response, body) {

            if(err){
                callback(err, null);
            } else if(body.code === 0){
                callback(null, 'success');
            } else {
                callback(body.result, null);
            }

        });

}

function call_master_on_ticket_mix_step_sbo(betId, matchList, callback) {
    console.log('call_master_on_ticket_mix_parlay_sbo => ',JSON.stringify(matchList));
    request.post({
            url: `http://lb-sbo-master.api-hub.com/ticket/insertMixStep`,
            method: "POST",
            json: true,
            body: {
                betId: betId,
                clientName: process.env.CLIENT_NAME||'AMB_SPORTBOOK',
                match: matchList
            },
            timeout: 15000
        },
        function (err, response, body) {

            if(err){
                callback(err, null);
            } else if(body.code === 0){
                callback(null, 'success');
            } else {
                callback(body.result, null);
            }

        });

}

function cal_commission_type(tor, long, type, callback) {

    // console.log('---------------  cal_commission_type  ------------------', tor, long, type);

    if (!_.isEqual(CLIENT_NAME, 'IRON')) {
        if(type === 'A'){
            // tor = tor + 1;
            // long = long + 1;
        } else if(type === 'B'){
            // tor = tor + 1;
            long = long - 1;
        } else if(type === 'C'){
            tor = tor - 1;
            long = long - 1;
        } else if(type === 'D'){
            tor = tor - 1;
            long = long - 2;
        } else if(type === 'E'){
            tor = tor - 2;
            long = long - 2;
        } else if(type === 'F'){
            tor = tor - 2;
            long = long - 3;
        }

    } else {
        if(type === 'A'){

        } else if(type === 'B'){
            tor = tor - 1;

        } else if(type === 'C'){
            tor = tor - 2;

        } else if(type === 'D'){
            tor = tor - 3;

        } else if(type === 'E'){
            tor = tor - 3;
            long = long - 1;
        } else if(type === 'F'){
            tor = tor - 3;
            long = long - 2;
        }
    }
    // console.log('================  cal_commission_type  ================', tor, long, type);

    callback(null, {tor: tor, long: long});
}
function calculateBetPriceByTime(min_max_price, current, match, callback){
    if (min_max_price <= 20000) {
        callback(null, min_max_price);
    } else {
        // console.log(min_max_price, current, match);
        var diff =(current.getTime() - match.getTime()) / 1000;
        diff /= (60 * 60);

        const gapHour = Math.abs(Math.round(diff));// match.getHours() - current.getHours();

        let percent = 0;
        // if (gapHour > 7) {
        //     percent = 60;
        // } else if (1 < gapHour && gapHour <= 7) {
        //     percent = 20;
        // } else {
        //     percent = 0;
        // }
        // console.log(`gapHour is ${gapHour} ${100 - percent}`);
        // console.log(roundTo(parseFloat(decrease(min_max_price, percent)), 0));
        // console.log(decrease(min_max_price, percent));

        if (12 <= gapHour) {
            percent = 70;
        } else if (11 === gapHour || 10 === gapHour || 9 === gapHour || 8 === gapHour || 7 === gapHour) {
            percent = 65;
        } else if (6 === gapHour || 5 === gapHour || 4 === gapHour ) {
            percent = 50;
        } else if (3 === gapHour || 2 === gapHour ) {
            percent = 35;
        } else {
            percent = 0;
        }

        callback(null, roundTo(parseFloat(decrease(min_max_price, percent)), 0));
    }
};

function calculateBetPrice(price, round, callback){
    let percent = 0;
    switch (round) {
        case 0:
            percent = 0;
            break;
        case 1:
            percent = 40;
            break;
        case 2:
            percent = 80;
            break;
        case 3:
            percent = 90;
            break;
        case 4:
            percent = 90;
            break;
        case 5:
            percent = 90;
            break;
    }

    callback(null, roundTo(parseFloat(decrease(price, percent)), 0))
};

const decrease = (price, percent) => {
    const result = (price * ( (100 - percent) / 100)).toFixed(2);
    // console.log(`${price} - ${percent}% = ${result}`);
    return result;
};

function call_ods_hdp(body, callback){
    async.waterfall([
            (callback) => {
                // console.log(URL_HDP);
                request.get(
                    URL_HDP,
                    (error, response) => {
                        if (error) {
                            callback(error, null);
                        } else {
                            callback(null, response);
                        }
                    });
            }
        ],
        (error, response) => {
            if (error) {
                callback(error, null)
            } else {
                const result = JSON.parse(response.body).result;

                const out = [];

                _.each(body, obj => {
                    const {
                        matchId,
                        betId,
                        oddType,
                        oddKey,
                        odd
                    } = obj;

                    const [
                        league_key,
                        match_key ] = matchId.split(':');

                    let betPrice = _.chain(result)
                        .findWhere(JSON.parse(`{"k":${parseInt(league_key)}}`))
                        .pick('m')
                        .map(obj => {
                            return obj
                        })
                        .flatten(true)
                        .findWhere(JSON.parse(`{"k":${parseInt(match_key)}}`))
                        .pick('bp')
                        .map(obj => {
                            return obj
                        })
                        .flatten(true)
                        .filter(obj => {
                            return _.contains(obj.allKey, oddKey)
                        })
                        .first()
                        .pick(oddType.toLowerCase())
                        .map(obj => {
                            return obj
                        })
                        .flatten(true)
                        .first()
                        .value();

                    let key;
                    _.findKey(betPrice, function (v, k) {
                        if (v === oddKey) {
                            key = k;
                            return true;
                        }
                    });

                    // console.log();

                    const modelOut = {};
                    modelOut.betId = betId;
                    modelOut.in = obj;
                    modelOut.out = _.pick(betPrice, key, key.replace(/k/, ''));
                    out.push(modelOut);
                });

                callback(error, out);
            }
        }
    );
};
function call_ods_live(body, callback){
    async.waterfall([
            (callback) => {
                // console.log(URL_LIVE);
                request.get(
                    URL_LIVE,
                    (error, response) => {
                        if (error) {
                            callback(error, null);
                        } else {
                            callback(null, response);
                        }
                    });
            }
        ],
        (error, response) => {
            if (error) {
                callback(error, null)
            } else {
                const result = JSON.parse(response.body).result;

                const out = [];

                _.each(body, obj => {
                    const {
                        matchId,
                        betId,
                        oddType,
                        oddKey
                    } = obj;

                    const [
                        league_key,
                        match_key ] = matchId.split(':');

                    let betPrice = _.chain(result)
                        .findWhere(JSON.parse(`{"k":${parseInt(league_key)}}`))
                        .pick('m')
                        .map(obj => {
                            return obj
                        })
                        .flatten(true)
                        .findWhere(JSON.parse(`{"k":${parseInt(match_key)}}`))
                        .pick('bp')
                        .map(obj => {
                            return obj
                        })
                        .flatten(true)
                        .filter(obj => {
                            return _.contains(obj.allKey, oddKey)
                        })
                        .first()
                        .pick(oddType.toLowerCase())
                        .map(obj => {
                            return obj
                        })
                        .flatten(true)
                        .first()
                        .value();

                    let key;
                    _.findKey(betPrice, function (v, k) {
                        if (v === oddKey) {
                            key = k;
                            return true;
                        }
                    });

                    // console.log();

                    const modelOut = {};
                    modelOut.betId = betId;
                    modelOut.in = obj;
                    modelOut.out = _.pick(betPrice, key, key.replace(/k/, ''));
                    out.push(modelOut);
                });

                callback(error, out);
            }
        }
    );
};
const convertMinMaxByCurrency = (value, currency, membercurrency) => {
    // console.log('###########################');
    // console.log('[BF]value      : ', value);
    // console.log('membercurrency : ', membercurrency);

    let minMax = value;
    if (_.isUndefined(membercurrency) || _.isNull(membercurrency)) {
        return minMax;
    } else {
        const predicate = JSON.parse(`{"currencyName":"${membercurrency}"}`);
        const obj = _.chain(currency)
            .findWhere(predicate)
            .pick('currencyTHB')
            .value();
        if (!_.isUndefined(obj.currencyTHB)) {
            console.log('               : ',obj.currencyTHB);
            minMax = Math.ceil(value / obj.currencyTHB);
            // console.log('[AF]value      : ', minMax);
            // console.log('###########################');
            return minMax;
        } else {
            return minMax;
        }
    }

};

function calculateBetPriceForBasketball(price, round, callback){
    let percent = 0;
    switch (round) {
        case 0:
            percent = 0;
            break;
        case 1:
            percent = 40;
            break;
        case 2:
            percent = 80;
            break;
        case 3:
            percent = 90;
            break;
        case 4:
            percent = 90;
            break;
        case 5:
            percent = 90;
            break;
    }

    callback(null, roundTo(parseFloat(decrease(price, percent)), 0))
};
function getBetObjectByKeyForBasketball(prefix, key, bpArray)  {
    let obj = {};
    switch (prefix) {
        case 'ah':
            if (_.isEqual(key, 'hk') || _.isEqual(key, 'hpk')) {
                obj = _.pick(bpArray, 'hk', 'h', 'hpk', 'hp');
            } else {
                obj = _.pick(bpArray, 'ak', 'a', 'apk', 'ap');
            }
            break;
        case 'ou':
            if (_.isEqual(key, 'ok') || _.isEqual(key, 'opk')) {
                obj = _.pick(bpArray, 'ok', 'o', 'opk', 'op');
            } else {
                obj = _.pick(bpArray, 'uk', 'u', 'upk', 'up');
            }
            break;
        case 'ml':
            if (_.isEqual(key, 'hk') || _.isEqual(key, 'h')) {
                obj = _.pick(bpArray, 'hk', 'h');
            } else  {
                obj = _.pick(bpArray, 'ak', 'a');
            }
            break;
        case 'oe':
            if (_.isEqual(key, 'ok') || _.isEqual(key, 'o')) {
                obj = _.pick(bpArray, 'ok', 'o');
            } else {
                obj = _.pick(bpArray, 'ek', 'e');
            }
            break;
        case 't1ou':
            if (_.isEqual(key, 'ok') || _.isEqual(key, 'opk')) {
                obj = _.pick(bpArray, 'ok', 'o', 'opk', 'op');
            } else {
                obj = _.pick(bpArray, 'uk', 'u', 'upk', 'up');
            }
            break;
        case 't2ou':
            if (_.isEqual(key, 'ok') || _.isEqual(key, 'opk')) {
                obj = _.pick(bpArray, 'ok', 'o', 'opk', 'op');
            } else {
                obj = _.pick(bpArray, 'uk', 'u', 'upk', 'up');
            }
            break;
    }
    return obj;
};
function cal_bp_hdp_single_match_for_basket_ball(matchId, type, key, commission_type, odd_adjust, memberId, callback)  {
    console.log(matchId, type, key, commission_type, odd_adjust, memberId);
    const k = matchId.split(':');
    const membercurrency = 'THB';//req.userInfo.currency;

    async.series([(cb) => {
        BasketballModel.findOne({k: k[0]},
            (err, data) => {

                if (err) {
                    cb(err, null)
                } else if (data) {
                    cb(null, data)
                } else {
                    cb('league is null', null)
                }
            })
    }, (cb) => {

        const headers = {
            'Content-Type': 'application/json'
        };

        const option = {
            url: `${URL_CACHE_HDP_SINGLE_MATCH_BASKET_BALL}/${matchId}`,
            method: 'GET',
            headers: headers
        };

        request(option, (err, response, body) => {
            if (err) {
                cb(err, null)
            } else {
                let result = JSON.parse(body);

                if (result) {
                    cb(null, result);
                } else {
                    cb(null, []);
                }
            }
        });
    },
        (cb) => {
            RedisService.getCurrency((error, result) => {
                if (error) {
                    cb(null, false);
                } else {
                    cb(null, result);
                }
            })

            // const copy = [{"currency":1,"currencyName":"MYR","currencyTHB":8},{"currency":1,"currencyName":"HDK","currencyTHB":4},{"currency":1,"currencyName":"CNY","currencyTHB":5},{"currency":1,"currencyName":"IDR","currencyTHB":425},{"currency":1,"currencyName":"VND","currencyTHB":715},{"currency":1,"currencyName":"USD","currencyTHB":33}];
            //
            // cb(null, copy);
        },
        (cb)=>{
            MemberService.getLimitSetting(memberId, (errLimitSetting, dataLimitSetting)=>{
                if(errLimitSetting){
                    cb(errLimitSetting, null)
                } else {
                    cb(null, dataLimitSetting.limitSetting)
                }
            });
        }], (err, results) => {
        if (err) {
            callback(err, null)
        } else {

            const basketballDB = results[0];
            let match_cache = results[1];
            let currency = results[2];
            let limitSetting = results[3];
            const type_lower = type.toLowerCase();

            let model_result = {
                matchId: `${match_cache.league_key}:${match_cache.match_key}`,
                league_name: match_cache.league_name,
                league_name_n: match_cache.league_name_n,
                name: match_cache.name,
                date: match_cache.date
            };

            let index = -1;
            let point = 0;
            const bp =  _.first(_.filter(match_cache.result, league => {
                index++;
                if (_.contains(league.allKey, key)) {
                    point = index;
                }
                return _.contains(league.allKey, key)
            }));
            // let model_result = {};

            let rp_hdp = {
                hdp: {
                    ah:{
                        f: 0,
                        u: 0
                    },
                    ou:{
                        f: 0,
                        u: 0
                    },
                    oe:{
                        f: 0,
                        u: 0
                    },
                    ml:{
                        f: 0,
                        u: 0
                    },
                    t1ou:{
                        f: 0,
                        u: 0
                    },
                    t2ou:{
                        f: 0,
                        u: 0
                    }
                }
            }

            let CONFIG_MTACH = {
                ah: {
                    a: 0,
                    h: 0
                },
                ou: {
                    a: 0,
                    h: 0
                },
                oe: {
                    a: 0,
                    h: 0
                },
                ml: {
                    a: 0,
                    h: 0
                },
                t1ou: {
                    a: 0,
                    h: 0
                },
                t2ou: {
                    a: 0,
                    h: 0
                },
            };


            console.log('basketballDB ===> ', JSON.stringify(basketballDB));


            switch_type_for_basket_ball(type_lower, bp, rp_hdp.hdp, model_result, CONFIG_MTACH, commission_type, odd_adjust,(err, result)=>{
                if(err){
                    callback(err, null)
                } else {
                    let min_max = basketballDB.r;

                    switch_min_max_for_basket_ball(type_lower, min_max, result, (err, dataResult)=>{
                        if(_.isEqual(type_lower, 'ml')){

                            dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet);
                            if(dataResult.a){
                                dataResult.max = dataResult.max / dataResult.a;
                            } else if(dataResult.h){
                                dataResult.max = dataResult.max / dataResult.h;
                            } else if(dataResult.d){
                                dataResult.max = dataResult.max / dataResult.d;
                            }
                        } else {
                            dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.hdpOuOe.maxPerBet);
                        }

                        calculateBetPrice(dataResult.max, bp[type_lower], (err, data)=>{
                            dataResult.max = data;
                        })

                        dataResult.min = convertMinMaxByCurrency(dataResult.min, currency, membercurrency);
                        dataResult.max = convertMinMaxByCurrency(dataResult.max, currency, membercurrency);
                        dataResult.maxPerMatch = basketballDB.r.pm;
                        callback(null, dataResult);
                    })
                }
            })
        }
    })
};
function switch_type_for_basket_ball(type_lower, bp, rp_hdp, modal, CONFIG_MTACH, commission_type, odd_adjust, callback) {
    // console.log('111111111   :   ',odd_adjust)
    // console.log(type_lower, bp, rp_hdp, modal);

    if (bp) {

        let ou = {o: 0, u: 0};
        let oe = {o: 0, e: 0};
        let ah = {home: 0, away: 0};
        let t1ou = {home: 0, away: 0};
        let t2ou = {home: 0, away: 0};

        if(_.size(odd_adjust) > 0){
            _.each(odd_adjust, (o)=>{
                if(o.prefix === 'ah' && bp.ah){

                    // console.log(o.value , bp);
                    if(o.key === 'hpk' && o.value === bp.ah.hpk){
                        ah.home += -o.count;
                        ah.away += o.count;
                    } else if(o.key === 'apk' && o.value === bp.ah.apk){
                        ah.home += o.count;
                        ah.away += -o.count;
                    }
                } else if(o.prefix === 'ou' && bp.ou){
                    if(o.key === 'opk' && o.value === bp.ou.opk){
                        ou.o += -o.count;
                        ou.u += o.count;
                    } else if(o.key === 'upk' && o.value === bp.ou.upk){
                        ou.o += o.count;
                        ou.u += -o.count;
                    }
                } else if(o.prefix === 't1ou' && bp.t1ou){
                    if(o.key === 'opk' && o.value === bp.t1ou.opk){
                        t1ou.o += -o.count;
                        t1ou.u += o.count;
                    } else if(o.key === 'upk' && o.value === bp.t1ou.upk){
                        t1ou.o += o.count;
                        t1ou.u += -o.count;
                    }
                } else if(o.prefix === 't2ou' && bp.t2ou){
                    if(o.key === 'opk' && o.value === bp.t2ou.opk){
                        t2ou.o += -o.count;
                        t2ou.u += o.count;
                    } else if(o.key === 'upk' && o.value === bp.t2ou.upk){
                        t2ou.o += o.count;
                        t2ou.u += -o.count;
                    }
                } else if(o.prefix === 'oe' && bp.oe){
                    if(o.key === 'ok' && o.value === bp.oe.ok){
                        oe.o += -o.count;
                        oe.e += o.count;
                    } else if(o.key === 'ek' && o.value === bp.oe.ek){
                        oe.o += o.count;
                        oe.e += -o.count;
                    }
                }
            });
        }


        switch (type_lower) {
            case "ah":
                // console.log('ah : ', type_lower);
                cal_ah(bp.ah.h, bp.ah.a, bp.ah.hp, bp.ah.ap, rp_hdp.ah.f, rp_hdp.ah.u, CONFIG_MTACH.ah, commission_type, ah, (err, data) => {
                    if (data) {
                        modal.ah = {
                            hk: bp.ah.hk,
                            h: bp.ah.h,
                            ak: bp.ah.ak,
                            a: bp.ah.a,
                            hpk: bp.ah.hpk,
                            hp: data.hp,
                            apk: bp.ah.apk,
                            ap: data.ap
                        };

                        cal_price(modal.ah.hp, modal.ah.ap, (err, data)=>{
                            modal.s = data;
                        })
                    }
                });
                break;
            case "ou":
                // console.log('ou : ', type_lower);
                cal_ou(bp.ou.op, bp.ou.up, rp_hdp.ou.f, rp_hdp.ou.u, CONFIG_MTACH.ou, commission_type, ou, (err, data) => {
                    if (data) {
                        modal.ou = {
                            ok: bp.ou.ok,
                            o: bp.ou.o,
                            uk: bp.ou.uk,
                            u: bp.ou.u,
                            opk: bp.ou.opk,
                            op: data.op,
                            upk: bp.ou.upk,
                            up: data.up
                        };

                        cal_price(modal.ou.op, modal.ou.up, (err, data)=>{
                            modal.s = data;
                        })
                    }
                });
                break;
            case "oe":
                // console.log('oe : ', type_lower);
                if (bp.oe) {
                    cal_oe(bp.oe.o, bp.oe.e, rp_hdp.oe.f, rp_hdp.oe.u, CONFIG_MTACH.oe, commission_type, oe, (err, data) => {
                        if (data) {
                            modal.oe = {
                                ok: bp.oe.ok,
                                o: data.o,
                                ek: bp.oe.ek,
                                e: data.e
                            };
                        }

                        cal_price(modal.oe.o, modal.oe.e, (err, data)=>{
                            modal.s = data;
                        })
                    });
                }
                break;
            case "ml":
                // console.log('oe1st : ', type_lower);
                cal_ml(bp.ml.h, bp.ml.a, CONFIG_MTACH.ml, (err, data) => {
                    if (data) {
                        modal.ml = {
                            hk: bp.ml.hk,
                            h: data.h,
                            ak: bp.ml.ak,
                            a: data.a
                        };
                    }
                });
                break;
            case "t1ou":
                // console.log('ou : ', type_lower);
                cal_ou(bp.t1ou.op, bp.t1ou.up, rp_hdp.t1ou.f, rp_hdp.t1ou.u, CONFIG_MTACH.t1ou, commission_type, t1ou, (err, data) => {
                    if (data) {
                        modal.t1ou = {
                            ok: bp.t1ou.ok,
                            o: bp.t1ou.o,
                            uk: bp.t1ou.uk,
                            u: bp.t1ou.u,
                            opk: bp.t1ou.opk,
                            op: data.op,
                            upk: bp.t1ou.upk,
                            up: data.up
                        };

                        cal_price(modal.t1ou.op, modal.t1ou.up, (err, data)=>{
                            modal.s = data;
                        })
                    }
                });
                break;
            case "t2ou":
                // console.log('ou : ', type_lower);
                cal_ou(bp.t2ou.op, bp.t2ou.up, rp_hdp.t2ou.f, rp_hdp.t2ou.u, CONFIG_MTACH.t2ou, commission_type, t2ou, (err, data) => {
                    if (data) {
                        modal.t2ou = {
                            ok: bp.t2ou.ok,
                            o: bp.t2ou.o,
                            uk: bp.t2ou.uk,
                            u: bp.t2ou.u,
                            opk: bp.t2ou.opk,
                            op: data.op,
                            upk: bp.t2ou.upk,
                            up: data.up
                        };

                        cal_price(modal.t2ou.op, modal.t2ou.up, (err, data)=>{
                            modal.s = data;
                        })
                    }
                });
                break;
        }
        // console.log('modal =====> ', JSON.stringify(modal));
        callback(null, modal);
    } else {
        callback('Betprice is null', null);
    }
}
function switch_min_max_for_basket_ball(type_lower, r, model, callback) {
    // console.log('############# switch_min_max #############');
    // console.log(type_lower, r, model);
    // console.log('=======>', type_lower, ' : ', JSON.stringify(r), ' : ', model, '<==========');
    switch (type_lower) {
        case "ah":
            // console.log('ah : ', type_lower);
            model.min = r.ah.mi;
            model.max = r.ah.ma;
            break;
        case "ou":
            // console.log('ou : ', type_lower);
            model.min = r.ou.mi;
            model.max = r.ou.ma;
            break;
        case "ml":
            // console.log('x12 : ', type_lower, bp.x12);
            model.min = r.ml.mi;
            model.max = r.ml.ma;
            break;
        case "oe":
            // console.log('oe : ', type_lower);
            model.min = r.oe.mi;
            model.max = r.oe.ma;
            break;
        case "t1ou":
            // console.log('oe1st : ', type_lower);
            model.min = r.t1ou.mi;
            model.max = r.t1ou.ma;
            break;
        case "t2ou":
            // console.log('oe1st : ', type_lower);
            model.min = r.t2ou.mi;
            model.max = r.t2ou.ma;
            break;
    }
    // console.log(`${model.min} : ${model.max}`);
    // console.log('############# switch_min_max #############');
    callback(null, model);
}

function cal_bp_parlay_for_basket_ball(matchId, type, key, commission_type, odd_adjust, callback)  {
    console.log(matchId, type, key, commission_type, odd_adjust);
    const k = matchId.split(':');
    const membercurrency = 'THB';//req.userInfo.currency;

    async.series([(cb) => {
        BasketballModel.findOne({k: k[0]},
            (err, data) => {

                if (err) {
                    cb(err, null)
                } else if (data) {
                    cb(null, data)
                } else {
                    cb('league is null', null)
                }
            })
    }, (cb) => {

        const headers = {
            'Content-Type': 'application/json'
        };

        const option = {
            url: `${URL_CACHE_HDP_SINGLE_MATCH_BASKET_BALL}/${matchId}`,
            method: 'GET',
            headers: headers
        };

        request(option, (err, response, body) => {
            if (err) {
                cb(err, null)
            } else {
                let result = JSON.parse(body);

                if (result) {
                    cb(null, result);
                } else {
                    cb(null, []);
                }
            }
        });
    }], (err, results) => {
        if (err) {
            callback(err, null)
        } else {

            const basketballDB = results[0];
            let match_cache = results[1];
            const type_lower = type.toLowerCase();

            let model_result = {
                matchId: `${match_cache.league_key}:${match_cache.match_key}`,
                league_name: match_cache.league_name,
                league_name_n: match_cache.league_name_n,
                name: match_cache.name,
                date: match_cache.date
            };

            let index = -1;
            let point = 0;
            const bp =  _.first(_.filter(match_cache.result, league => {
                index++;
                if (_.contains(league.allKey, key)) {
                    point = index;
                }
                return _.contains(league.allKey, key)
            }));
            // let model_result = {};

            let rp_hdp = {
                hdp: {
                    ah:{
                        f: 0,
                        u: 0
                    },
                    ou:{
                        f: 0,
                        u: 0
                    },
                    oe:{
                        f: 0,
                        u: 0
                    },
                    ml:{
                        f: 0,
                        u: 0
                    },
                    t1ou:{
                        f: 0,
                        u: 0
                    },
                    t2ou:{
                        f: 0,
                        u: 0
                    }
                }
            }

            let CONFIG_MTACH = {
                ah: {
                    a: 0,
                    h: 0
                },
                ou: {
                    a: 0,
                    h: 0
                },
                oe: {
                    a: 0,
                    h: 0
                },
                ml: {
                    a: 0,
                    h: 0
                },
                t1ou: {
                    a: 0,
                    h: 0
                },
                t2ou: {
                    a: 0,
                    h: 0
                },
            };

            switch_type_parlay_for_basket_ball(type_lower, bp, rp_hdp.hdp, model_result, CONFIG_MTACH, commission_type, (err, result) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, result);
                }
            })
        }
    })
};
function switch_type_parlay_for_basket_ball(type_lower, bp, rp_hdp, modal, CONFIG_MTACH, commission_type, odd_adjust, callback) {
    if (bp) {

        let ou = {o: 0, u: 0};
        let oe = {o: 0, e: 0};
        let ah = {home: 0, away: 0};
        let t1ou = {home: 0, away: 0};
        let t2ou = {home: 0, away: 0};

        if(_.size(odd_adjust) > 0){
            _.each(odd_adjust, (o)=>{
                if(o.prefix === 'ah' && bp.ah){

                    // console.log(o.value , bp);
                    if(o.key === 'hpk' && o.value === bp.ah.hpk){
                        ah.home += -o.count;
                        ah.away += o.count;
                    } else if(o.key === 'apk' && o.value === bp.ah.apk){
                        ah.home += o.count;
                        ah.away += -o.count;
                    }
                } else if(o.prefix === 'ou' && bp.ou){
                    if(o.key === 'opk' && o.value === bp.ou.opk){
                        ou.o += -o.count;
                        ou.u += o.count;
                    } else if(o.key === 'upk' && o.value === bp.ou.upk){
                        ou.o += o.count;
                        ou.u += -o.count;
                    }
                } else if(o.prefix === 't1ou' && bp.t1ou){
                    if(o.key === 'opk' && o.value === bp.t1ou.opk){
                        t1ou.o += -o.count;
                        t1ou.u += o.count;
                    } else if(o.key === 'upk' && o.value === bp.t1ou.upk){
                        t1ou.o += o.count;
                        t1ou.u += -o.count;
                    }
                } else if(o.prefix === 't2ou' && bp.t2ou){
                    if(o.key === 'opk' && o.value === bp.t2ou.opk){
                        t2ou.o += -o.count;
                        t2ou.u += o.count;
                    } else if(o.key === 'upk' && o.value === bp.t2ou.upk){
                        t2ou.o += o.count;
                        t2ou.u += -o.count;
                    }
                } else if(o.prefix === 'oe' && bp.oe){
                    if(o.key === 'ok' && o.value === bp.oe.ok){
                        oe.o += -o.count;
                        oe.e += o.count;
                    } else if(o.key === 'ek' && o.value === bp.oe.ek){
                        oe.o += o.count;
                        oe.e += -o.count;
                    }
                }
            });
        }

        switch (type_lower) {
            case "ah":
                // console.log('ah : ', type_lower);
                cal_eu_ah(bp.ah.h, bp.ah.a, bp.ah.hp, bp.ah.ap, rp_hdp.ah.f, rp_hdp.ah.u, CONFIG_MTACH.ah, commission_type, ah, (err, data) => {
                    if (data) {
                        modal.ah = {
                            hk: bp.ah.hk,
                            h: bp.ah.h,
                            ak: bp.ah.ak,
                            a: bp.ah.a,
                            hpk: bp.ah.hpk,
                            hp: data.hp,
                            apk: bp.ah.apk,
                            ap: data.ap
                        };
                    }
                });
                break;
            case "ou":
                // console.log('ou : ', type_lower);
                cal_eu_ou(bp.ou.op, bp.ou.up, rp_hdp.ou.f, rp_hdp.ou.u, CONFIG_MTACH.ou, commission_type, ou, (err, data) => {
                    if (data) {
                        modal.ou = {
                            ok: bp.ou.ok,
                            o: bp.ou.o,
                            uk: bp.ou.uk,
                            u: bp.ou.u,
                            opk: bp.ou.opk,
                            op: data.op,
                            upk: bp.ou.upk,
                            up: data.up
                        };
                    }
                });
                break;
            case "oe":
                // console.log('oe : ', type_lower);
                if (bp.oe) {
                    cal_eu_oe(bp.oe.o, bp.oe.e, rp_hdp.oe.f, rp_hdp.oe.u, CONFIG_MTACH.oe, commission_type, oe, (err, data) => {
                        if (data) {
                            modal.oe = {
                                ok: bp.oe.ok,
                                o: data.o,
                                ek: bp.oe.ek,
                                e: data.e
                            };
                        }
                    });
                }
                break;
            case "ml":
                // console.log('oe1st : ', type_lower);
                cal_eu_ml(bp.ml.h, bp.ml.a, CONFIG_MTACH.ml, (err, data) => {
                    if (data) {
                        modal.ml = {
                            hk: bp.ml.hk,
                            h: data.h,
                            ak: bp.ml.ak,
                            a: data.a
                        };
                    }
                });
                break;
            case "t1ou":
                // console.log('ou : ', type_lower);
                cal_eu_ou(bp.t1ou.op, bp.t1ou.up, rp_hdp.t1ou.f, rp_hdp.t1ou.u, CONFIG_MTACH.t1ou, commission_type, t1ou, (err, data) => {
                    if (data) {
                        modal.t1ou = {
                            ok: bp.t1ou.ok,
                            o: bp.t1ou.o,
                            uk: bp.t1ou.uk,
                            u: bp.t1ou.u,
                            opk: bp.t1ou.opk,
                            op: data.op,
                            upk: bp.t1ou.upk,
                            up: data.up
                        };
                    }
                });
                break;
            case "t2ou":
                // console.log('ou : ', type_lower);
                cal_eu_ou(bp.t2ou.op, bp.t2ou.up, rp_hdp.t2ou.f, rp_hdp.t2ou.u, CONFIG_MTACH.t2ou, commission_type, t2ou, (err, data) => {
                    if (data) {
                        modal.t2ou = {
                            ok: bp.t2ou.ok,
                            o: bp.t2ou.o,
                            uk: bp.t2ou.uk,
                            u: bp.t2ou.u,
                            opk: bp.t2ou.opk,
                            op: data.op,
                            upk: bp.t2ou.upk,
                            up: data.up
                        };
                    }
                });
                break;
        }
        console.log('modal =====> ', JSON.stringify(modal));
        callback(null, modal);
    } else {
        callback('Betprice is null', null);
    }
}

function configMatch () {
    const configMatch = {};
    configMatch.ah = {};
    configMatch.ah.h = "0";
    configMatch.ah.a = "0";

    configMatch.ou = {};
    configMatch.ou.h = "0";
    configMatch.ou.a = "0";

    configMatch.x12 = {};
    configMatch.x12.h = "0";
    configMatch.x12.a = "0";
    configMatch.x12.d = "0";

    configMatch.oe = {};
    configMatch.oe.h = "0";
    configMatch.oe.a = "0";

    configMatch.ah1st = {};
    configMatch.ah1st.h = "0";
    configMatch.ah1st.a = "0";

    configMatch.ou1st = {};
    configMatch.ou1st.h = "0";
    configMatch.ou1st.a = "0";

    configMatch.x121st = {};
    configMatch.x121st.h = "0";
    configMatch.x121st.a = "0";
    configMatch.x121st.d = "0";

    configMatch.oe1st = {};
    configMatch.oe1st.h = "0";
    configMatch.oe1st.a = "0";

    configMatch.ml = {};
    configMatch.ml.h = "0";
    configMatch.ml.a = "0";
    return configMatch;
}

function configMatchMixStep (odds) {
    const configMatch = {};
    configMatch.ah = {};
    configMatch.ah.h = `-${odds}`;
    configMatch.ah.a = `-${odds}`;

    configMatch.ou = {};
    configMatch.ou.h = `-${odds}`;
    configMatch.ou.a = `-${odds}`;

    configMatch.x12 = {};
    configMatch.x12.h = `-${odds}`;
    configMatch.x12.a = `-${odds}`;
    configMatch.x12.d = `-${odds}`;

    configMatch.oe = {};
    configMatch.oe.h = `-${odds}`;
    configMatch.oe.a = `-${odds}`;

    configMatch.ah1st = {};
    configMatch.ah1st.h = `-${odds}`;
    configMatch.ah1st.a = `-${odds}`;

    configMatch.ou1st = {};
    configMatch.ou1st.h = `-${odds}`;
    configMatch.ou1st.a = `-${odds}`;

    configMatch.x121st = {};
    configMatch.x121st.h = `-${odds}`;
    configMatch.x121st.a = `-${odds}`;
    configMatch.x121st.d = `-${odds}`;

    configMatch.oe1st = {};
    configMatch.oe1st.h = `-${odds}`;
    configMatch.oe1st.a = `-${odds}`;

    configMatch.ml = {};
    configMatch.ml.h = `-${odds}`;
    configMatch.ml.a = `-${odds}`;
    return configMatch;
}

function rule () {
    const r = {};
    r.ah = {};
    r.ah.ma = {};
    r.ah.mi = {};

    r.ou = {};
    r.ou.ma = {};
    r.ou.mi = {};

    r.x12 = {};
    r.x12.ma = {};
    r.x12.mi = {};

    r.oe = {};
    r.oe.ma = {};
    r.oe.mi = {};

    r.ah1st = {};
    r.ah1st.ma = {};
    r.ah1st.mi = {};

    r.ou1st = {};
    r.ou1st.ma = {};
    r.ou1st.mi = {};

    r.x121st = {};
    r.x121st.ma = {};
    r.x121st.mi = {};

    r.oe1st = {};
    r.oe1st.ma = {};
    r.oe1st.mi = {};

    r.ml = {};
    r.ml.ma = {};
    r.ml.mi = {};
    return r;
}

function rulePriceHDP() {

    const r = {};
    r.ah = {};
    r.ah.f = "0";
    r.ah.u = "0";

    r.ou = {};
    r.ou.f = "0";
    r.ou.u = "0";

    r.x12 = {};
    r.x12.f = "0";
    r.x12.u = "0";

    r.oe = {};
    r.oe.f = "0";
    r.oe.u = "0";

    r.ah1st = {};
    r.ah1st.f = "0";
    r.ah1st.u = "0";

    r.ou1st = {};
    r.ou1st.f = "0";
    r.ou1st.u = "0";

    r.x121st = {};
    r.x121st.f = "0";
    r.x121st.u = "0";

    r.oe1st = {};
    r.oe1st.f = "0";
    r.oe1st.u = "0";

    r.ml = {};
    r.ml.f = "0";
    r.ml.u = "0";
    return r;
}