const MemberModel = require('../models/member.model');
const request = require('request');
const _ = require('underscore');
const mongoose = require('mongoose');
const BetTransactionModel = require('../models/betTransaction.model.js');
const async = require("async");
const moment = require('moment');
const querystring = require('querystring');
const crypto = require("crypto");
const AgentGroupModel = require('../models/agentGroup.model.js');
const MemberService = require('../common/memberService');
const DateUtils = require('../common/dateUtils');
const NumberUtils = require('../common/numberUtils1');
const AgentService = require('../common/agentService');
const roundTo = require('round-to');

const jwt    = require('jsonwebtoken');

// const AMEBA_URL = process.env.AMEBA_URL || 'https://api-snd.fafafa3388.com';
// const AMEBA_SITE_ID = process.env.AMEBA_SITE_ID || '2046';
// const AMEBA_SECRET_KEY = process.env.AMEBA_SECRET_KEY || 'sYsXvKV/yKegH694Nf+35YetNg20PXFW';
//
// const AMEBA_IMAGE_URL = process.env.AMEBA_IMAGE_URL || 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/ameba-game';

const getClientKey = require('../controllers/getClientKey');
let {
    AMEBA_URL = 'https://api-snd.fafafa3388.com',
    AMEBA_SITE_ID = '2046',
    AMEBA_SECRET_KEY = 'sYsXvKV/yKegH694Nf+35YetNg20PXFW',
    AMEBA_IMAGE_URL = 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/ameba-game'
} = getClientKey.getKey();

const CLIENT_NAME         = process.env.CLIENT_NAME          || 'SPORTBOOK88';

function generateToken(data) {
    console.log('SECRET KEY ================ ',AMEBA_SECRET_KEY);
    return  jwt.sign(data, AMEBA_SECRET_KEY, {expiresIn: "1h"});
}

function getRequestBody(req,callback) {

        jwt.verify(req.header('authorization').split(' ')[1], AMEBA_SECRET_KEY.trim(),(err, result) => {
            if(err){
                console.log('jwt.verify err : ',AMEBA_SECRET_KEY);
                callback(err,null);
            }else {
                callback(null,result);
            }
        });
}


function callGetGameList(callback) {

    let apiName = '/dms/api';

    let data = {
        action: "get_game_list",
        site_id: AMEBA_SITE_ID
    };

    console.log('SITE ID ============== ',AMEBA_SITE_ID);

    let requestToken = generateToken(data);

    let headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+requestToken
    };

    let options = {
        url: AMEBA_URL + apiName,
        method: 'POST',
        headers: headers,
        json: data,
    };

    console.log(options)
    request(options,function (err, response, body) {
            // if (error) return console.error('error(%s):', resourceName, error)
            console.log(body)
            if (err) {
                callback(body, null);
            } else {

                if(body.error_code == "OK"){
                    callback(null, body.games);
                }else {
                    callback(body.error_code, null);
                }
            }
        }
    );

}

function callCreateAccount(username, callback) {



    let apiName = '/ams/api';

    let data = {
        action: "create_account",
        site_id: AMEBA_SITE_ID,
        account_name:  username.toLowerCase(),
        currency: "THB"
    }

    let requestToken = generateToken(data);

    let headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+requestToken
    };

    let options = {
        url: AMEBA_URL + apiName,
        method: 'POST',
        headers: headers,
        json: data,
    };

    console.log(options)
    request(options,function (err, response, body) {
            // if (error) return console.error('error(%s):', resourceName, error)
            console.log(body)
            if (err) {
                callback(body, null);
            } else {
                callback(null, body);
            }
        }
    );

}

function callRegisterToken(username,gameId, callback) {


    let apiName = '/ams/api';

    let data = {
        action: "register_token",
        site_id: AMEBA_SITE_ID,
        account_name:  username.toLowerCase(),
        game_id:gameId,
        lang : "thTH",
        sessionid: ""
    };

    let requestToken = generateToken(data);

    let headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+requestToken
    };

    let options = {
        url: AMEBA_URL + apiName,
        method: 'POST',
        headers: headers,
        json: data,
    };

    console.log(options)
    request(options,function (err, response, body) {
            // if (error) return console.error('error(%s):', resourceName, error)
            console.log(body)
            if (err) {
                callback(body, null);
            } else {

                if(body.error_code == "OK"){
                    callback(null, body.game_url);
                }else {
                    callback(body.error_code, null);
                }
            }
        }
    );

}


function getGameName(gameType) {

    switch (Number.parseInt(gameType)) {
        case 1 :
            return "Baby Monkey";
        case 2 :
            return "Baby Monkey 2";
        case 3 :
            return "Lucky777";
        case 4 :
            return "Pop Rock";
        case 5 :
            return "FaFaFa";
        case 6 :
            return "Lucky Boy";
        case 7 :
            return "Gemstone";
        case 8 :
            return "Pop Rock 2";
        case 9 :
            return "Super Star";
        case 10 :
            return "Money Tree";
        case 11 :
            return "Alliance";
        case 12 :
            return "Lucky Dice";
        case 13 :
            return "Lucky 8";
        case 14 :
            return "Big Panda";
        case 15 :
            return "Dragon 8";
        case 16 :
            return "Dragon Ball";
        case 17 :
            return "Alliance 2";
        case 18 :
            return "Galaxy";
        case 19 :
            return "Always Fa";
        case 20 :
            return "UFO";
        case 22 :
            return "Ocean World";
        case 23 :
            return "World Tour";
        case 24 :
            return "Mahjong";
        case 25 :
            return "Easy Fa";
        case 26 :
            return "Magic Ball";
        case 27 :
            return "Captain's Treasure";
        case 28 :
            return "Zhao Cai Jin Bao";
        case 29 :
            return "Elements";
        case 31 :
            return "Take Win";
        case 32 :
            return "All The Best";
        case 37 :
            return "Funky Monkey";
        case 36 :
            return "Fortune Lion";
        case 38 :
            return "Gemstone 2";
        case 39 :
            return "Nian Nian You Yu";
        case 40 :
            return "Candy Pop";
        case 41 :
            return "Break Away";
        case 42 :
            return "Funky Monkey Super";
        case 43 :
            return "RuYiBang";
        case 132 :
            return "Koi";
        default :
            return
    }

}

function getGameImage(gameId) {
    switch (Number.parseInt(gameId)){
        case 1 :
            return AMEBA_IMAGE_URL+ '/1.png';
        case 2 :
            return AMEBA_IMAGE_URL+ '/2.png';
        case 3 :
            return AMEBA_IMAGE_URL+ '/3.png';
        case 4 :
            return AMEBA_IMAGE_URL+ '/4.png';
        case 5 :
            return AMEBA_IMAGE_URL+ '/5.png';
        case 6 :
            return AMEBA_IMAGE_URL+ '/6.png';
        case 7 :
            return AMEBA_IMAGE_URL+ '/7.png';
        case 8 :
            return AMEBA_IMAGE_URL+ '/8.png';
        case 9 :
            return AMEBA_IMAGE_URL+ '/9.png';
        case 10 :
            return AMEBA_IMAGE_URL+ '/10.png';
        case 11 :
            return AMEBA_IMAGE_URL+ '/11.png';
        case 12 :
            return AMEBA_IMAGE_URL+ '/12.png';
        case 13 :
            return AMEBA_IMAGE_URL+ '/13.png';
        case 14 :
            return AMEBA_IMAGE_URL+ '/14.png';
        case 15 :
            return AMEBA_IMAGE_URL+ '/15.png';
        case 16 :
            return AMEBA_IMAGE_URL+ '/16.png';
        case 17 :
            return AMEBA_IMAGE_URL+ '/17.png';
        case 18 :
            return AMEBA_IMAGE_URL+ '/18.png';
        case 19 :
            return AMEBA_IMAGE_URL+ '/19.png';
        case 20 :
            return AMEBA_IMAGE_URL+ '/20.png';
        case 21:
            return AMEBA_IMAGE_URL+ '/21.png';
        case 22 :
            return AMEBA_IMAGE_URL+ '/22.png';
        case 23 :
            return AMEBA_IMAGE_URL+ '/23.png';
        case 24 :
            return AMEBA_IMAGE_URL+ '/24.png';
        case 25 :
            return AMEBA_IMAGE_URL+ '/25.png';
        case 26 :
            return AMEBA_IMAGE_URL+ '/26.png';
        case 27 :
            return AMEBA_IMAGE_URL+ '/27.png';
        case 28 :
            return AMEBA_IMAGE_URL+ '/28.png';
        case 29 :
            return AMEBA_IMAGE_URL+ '/29.png';
        case 30 :
            return AMEBA_IMAGE_URL+ '/30.png';
        case 31 :
            return AMEBA_IMAGE_URL+ '/31.png';
        case 32 :
            return AMEBA_IMAGE_URL+ '/32.png';
        case 33 :
            return AMEBA_IMAGE_URL+ '/33.png';
        case 34 :
            return AMEBA_IMAGE_URL+ '/34.png';
        case 35 :
            return AMEBA_IMAGE_URL+ '/35.png';
        case 36 :
            return AMEBA_IMAGE_URL+ '/36.png';
        case 37 :
            return AMEBA_IMAGE_URL+ '/37.png';
        case 38 :
            return AMEBA_IMAGE_URL+ '/38.png';
        case 39 :
            return AMEBA_IMAGE_URL+ '/39.png';
        case 40 :
            return AMEBA_IMAGE_URL+ '/40.png';
        case 41 :
            return AMEBA_IMAGE_URL+ '/41.png';
        case 42 :
            return AMEBA_IMAGE_URL+ '/42.png';
        case 43 :
            return AMEBA_IMAGE_URL+ '/43.png';
        case 106 :
            return AMEBA_IMAGE_URL+ '/106.png';
        case 132 :
            return AMEBA_IMAGE_URL+ '/132.png';
        default :
            return
    }
}

module.exports = {callCreateAccount,callRegisterToken,callGetGameList,getRequestBody,getGameName,getGameImage};