const config = require('config');
const jwt = require('jsonwebtoken');
const async = require("async");

const BetTransactionModel = require('../models/betTransaction.model.js');
const mongoose = require('mongoose');
const DateUtils = require('../common/dateUtils');
const moment = require('moment');
const _ = require('underscore');
const roundTo = require('round-to');


function getWinLoseReportMember(groupId, groupType, dateFrom, dateTo,timeFrom,timeTo, games, callback) {

    let dateList = DateUtils.enumerateReportBetweenDates2(moment(dateFrom, "DD/MM/YYYY"), moment(dateTo, "DD/MM/YYYY"), timeFrom,timeTo);

    let tasks = [];

    _.forEach(dateList, dateRange => {
        tasks.push(getWinLoseAccountMemberPerDay(groupId, groupType, dateRange,games));
    });


    async.parallel(tasks, (err, results) => {
        if (err) {
            console.log('err 11 : ', err)
        }

        let summaryDataPerDay = summaryFunctionMember(results);
        //
        callback(null, summaryDataPerDay);
    })

}


function getWinLoseMatch(groupId,groupType, queryParams, callback) {

    let dateList = DateUtils.enumerateReportBetweenDates2(moment(queryParams.dateFrom, "DD/MM/YYYY"), moment(queryParams.dateTo, "DD/MM/YYYY"), queryParams.timeFrom,queryParams.timeTo);

    let tasks = [];

    _.forEach(dateList, dateRange => {
        tasks.push(getWinLoseMatchPerDay(groupId, groupType, dateRange,queryParams));
    });


    async.parallel(tasks, (err, results) => {
        if (err) {
            console.log('err 11 : ', err);
        }

        let summaryDataPerDay = summaryFunctionWlMatch(results);

        callback(null,summaryDataPerDay);
    });

}
function getWinLoseAccountMemberPerDay(groupId, groupType, dateRange,games) {
    // console.log('dateStr : ' + dateStr + ' , game : ' + games);


    let group = '';
    let child;

    if (groupType === 'SUPER_ADMIN') {
        group = 'superAdmin';
        child = 'company';
    } else if (groupType === 'COMPANY') {
        group = 'company';
        child = 'shareHolder';
    } else if (groupType === 'SHARE_HOLDER') {
        group = 'shareHolder';
        child = 'senior';
    } else if (groupType === 'SENIOR') {
        group = "senior";
        child = 'agent';
    } else if (groupType === 'MASTER_AGENT') {
        group = "masterAgent";
        child = 'agent';
    } else if (groupType === 'AGENT') {
        group = "agent";
        child = '';
    }


    return function (callbackTask) {

        let condition = {};
        let orCondition = [
            {
                'gameDate': {
                    "$gte": moment(dateRange[0], "YYYY-MM-DDTHH:mm:ss.000").utc(true).toDate(),
                    "$lt": moment(dateRange[1], "YYYY-MM-DDTHH:mm:ss.000").utc(true).toDate()
                }
            },
            {'memberParentGroup': mongoose.Types.ObjectId(groupId)},
            {'status': {$in: ['DONE', 'REJECTED', 'CANCELLED']}}
        ];

        // console.log(orCondition)
        if (games) {

            let gamesCondition = {
                '$or': games.split(',').map(function (game) {
                    if (game === 'FOOTBALL') {
                        return {'$and': [{'game': 'FOOTBALL'}, {gameType: 'TODAY'}]}

                    } else if (game === 'STEP') {
                        return {'$and': [{'game': 'FOOTBALL'}, {gameType: 'MIX_STEP'}]}

                    } else if (game === 'PARLAY') {
                        return {'$and': [{'game': 'FOOTBALL'}, {gameType: 'PARLAY'}]}

                    } else if (game === 'CASINO') {
                        return {'$and': [{'game': 'CASINO'}]}

                    } else if (game === 'GAME') {
                        return {'$and': [{'game': 'GAME'}]}

                    } else if (game === 'LOTTO') {
                        return {'$and': [{'game': 'LOTTO'}]}
                    } else if (game === 'M2') {
                        return {'$and': [{'game': 'M2'}]}
                    }
                })

            };

            if (games.split(',').length != 7) {
                orCondition.push(gamesCondition);
            }

        }

        condition['$and'] = orCondition;


        BetTransactionModel.aggregate([
            {
                $match: condition
            },
            {
                "$group": {
                    "_id": {
                        commission: '$memberParentGroup',
                        member: '$memberId'
                    },
                    // "currency": {$first: "$currency"},
                    "stackCount": {$sum: 1},
                    "amount": {$sum: '$amount'},
                    "validAmount": {$sum: '$validAmount'},
                    "grossComm": {$sum: {$multiply: ['$validAmount', {$divide: ['$commission.' + group + '.commission', 100]}]}},
                    "memberWinLose": {$sum: '$commission.member.winLose'},
                    "memberWinLoseCom": {$sum: '$commission.member.winLoseCom'},
                    "memberTotalWinLoseCom": {$sum: '$commission.member.totalWinLoseCom'},

                    "agentWinLose": {$sum: '$commission.agent.winLose'},
                    "agentWinLoseCom": {$sum: '$commission.agent.winLoseCom'},
                    "agentTotalWinLoseCom": {$sum: '$commission.agent.totalWinLoseCom'},

                    "masterAgentWinLose": {$sum: '$commission.masterAgent.winLose'},
                    "masterAgentWinLoseCom": {$sum: '$commission.masterAgent.winLoseCom'},
                    "masterAgentTotalWinLoseCom": {$sum: '$commission.masterAgent.totalWinLoseCom'},

                    "seniorWinLose": {$sum: '$commission.senior.winLose'},
                    "seniorWinLoseCom": {$sum: '$commission.senior.winLoseCom'},
                    "seniorTotalWinLoseCom": {$sum: '$commission.senior.totalWinLoseCom'},

                    "shareHolderWinLose": {$sum: '$commission.shareHolder.winLose'},
                    "shareHolderWinLoseCom": {$sum: '$commission.shareHolder.winLoseCom'},
                    "shareHolderTotalWinLoseCom": {$sum: '$commission.shareHolder.totalWinLoseCom'},

                    "companyWinLose": {$sum: '$commission.company.winLose'},
                    "companyWinLoseCom": {$sum: '$commission.company.winLoseCom'},
                    "companyTotalWinLoseCom": {$sum: '$commission.company.totalWinLoseCom'},

                    "superAdminWinLose": {$sum: '$commission.superAdmin.winLose'},
                    "superAdminWinLoseCom": {$sum: '$commission.superAdmin.winLoseCom'},
                    "superAdminTotalWinLoseCom": {$sum: '$commission.superAdmin.totalWinLoseCom'}
                }
            },
            {
                $project: {
                    _id: 0,
                    type: 'M_GROUP',
                    member: '$_id.member',
                    group: '$_id.commission',
                    currency: "THB",
                    stackCount: '$stackCount',
                    grossComm: '$grossComm',
                    amount: '$amount',
                    validAmount: '$validAmount',
                    memberWinLose: '$memberWinLose',
                    memberWinLoseCom: '$memberWinLoseCom',
                    memberTotalWinLoseCom: '$memberTotalWinLoseCom',
                    agentWinLose: '$agentWinLose',
                    agentWinLoseCom: '$agentWinLoseCom',
                    agentTotalWinLoseCom: '$agentTotalWinLoseCom',
                    masterAgentWinLose: '$masterAgentWinLose',
                    masterAgentWinLoseCom: '$masterAgentWinLoseCom',
                    masterAgentTotalWinLoseCom: '$masterAgentTotalWinLoseCom',
                    seniorWinLose: '$seniorWinLose',
                    seniorWinLoseCom: '$seniorWinLoseCom',
                    seniorTotalWinLoseCom: '$seniorTotalWinLoseCom',
                    shareHolderWinLose: '$shareHolderWinLose',
                    shareHolderWinLoseCom: '$shareHolderWinLoseCom',
                    shareHolderTotalWinLoseCom: '$shareHolderTotalWinLoseCom',
                    companyWinLose: '$companyWinLose',
                    companyWinLoseCom: '$companyWinLoseCom',
                    companyTotalWinLoseCom: '$companyTotalWinLoseCom',
                    superAdminWinLose: '$superAdminWinLose',
                    superAdminWinLoseCom: '$superAdminWinLoseCom',
                    superAdminTotalWinLoseCom: '$superAdminTotalWinLoseCom',

                }
            }
        ]).read('secondaryPreferred').exec((err, results) => {
            // separateAggregate(condition,group,child,(err, results) => {
            if (err) {
                callbackTask(err, null);
            } else {
                callbackTask(null, results);
            }
        });

    }

}


function summaryFunctionWlMatch(transactions) {

    transactions = transactions.filter(item => {
        return item && item.length > 0;
    });

    return _.reduce(transactions, function (preList, nextList) {

        let uniqueData = _.uniq(_.union(preList, nextList), false, function (item, key, a) {
            return item.key.toString();
        });


        return _.map(uniqueData, (uniqueObj) => {

            let preData = _.filter(preList, (preObj) => {
                return uniqueObj.key.toString() === preObj.key.toString();
            })[0];

            let next = _.filter(nextList, (nextObj) => {
                return uniqueObj.key.toString() === nextObj.key.toString();
            })[0];


            if (preData && next) {
                return {
                    key: preData.key,
                    leagueName: preData.leagueName,
                    matchName: preData.matchName,
                    currency: preData.currency,
                    source: preData.source,
                    member: preData.member,

                    amount: preData.amount + next.amount,
                    validAmount: preData.validAmount + next.validAmount,
                    stackCount: preData.stackCount + next.stackCount,
                    grossComm: preData.grossComm + next.grossComm,

                    memberWinLose: roundTo(preData.memberWinLose + next.memberWinLose,3),
                    memberWinLoseCom: roundTo(preData.memberWinLoseCom + next.memberWinLoseCom,3),
                    memberTotalWinLoseCom: roundTo(preData.memberTotalWinLoseCom + next.memberTotalWinLoseCom,3),

                    agentWinLose: roundTo(preData.agentWinLose + next.agentWinLose,3),
                    agentWinLoseCom: roundTo(preData.agentWinLoseCom + next.agentWinLoseCom,3),
                    agentTotalWinLoseCom: roundTo(preData.agentTotalWinLoseCom + next.agentTotalWinLoseCom,3),

                    masterAgentWinLose: roundTo(preData.masterAgentWinLose + next.masterAgentWinLose,3),
                    masterAgentWinLoseCom: roundTo(preData.masterAgentWinLoseCom + next.masterAgentWinLoseCom,3),
                    masterAgentTotalWinLoseCom: roundTo(preData.masterAgentTotalWinLoseCom + next.masterAgentTotalWinLoseCom,3),

                    seniorWinLose: roundTo(preData.seniorWinLose + next.seniorWinLose,3),
                    seniorWinLoseCom: roundTo(preData.seniorWinLoseCom + next.seniorWinLoseCom,3),
                    seniorTotalWinLoseCom: roundTo(preData.seniorTotalWinLoseCom + next.seniorTotalWinLoseCom,3),

                    shareHolderWinLose: roundTo(preData.shareHolderWinLose + next.shareHolderWinLose,3),
                    shareHolderWinLoseCom: roundTo(preData.shareHolderWinLoseCom + next.shareHolderWinLoseCom,3),
                    shareHolderTotalWinLoseCom: roundTo(preData.shareHolderTotalWinLoseCom + next.shareHolderTotalWinLoseCom,3),

                    companyWinLose: roundTo(preData.companyWinLose + next.companyWinLose,3),
                    companyWinLoseCom: roundTo(preData.companyWinLoseCom + next.companyWinLoseCom,3),
                    companyTotalWinLoseCom: roundTo(preData.companyTotalWinLoseCom + next.companyTotalWinLoseCom,3),

                    superAdminWinLose: roundTo(preData.superAdminWinLose + next.superAdminWinLose,3),
                    superAdminWinLoseCom: roundTo(preData.superAdminWinLoseCom + next.superAdminWinLoseCom,3),
                    superAdminTotalWinLoseCom: roundTo(preData.superAdminTotalWinLoseCom + next.superAdminTotalWinLoseCom,3)
                }
            }else if(preData){
                return preData;
            }else if(next){
                return next;
            }

        });

    });


}


module.exports = {
    getWinLoseReportMember, getWinLoseMatch
}
