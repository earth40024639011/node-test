const MemberModel = require('../models/member.model');
const request = require('request');
const _ = require('underscore');
const mongoose = require('mongoose');
const BetTransactionModel = require('../models/betTransaction.model.js');
const async = require("async");
const moment = require('moment');
const querystring = require('querystring');
const Crypto = require("crypto");
const AgentGroupModel = require('../models/agentGroup.model.js');
const MemberService = require('../common/memberService');
const DateUtils = require('../common/dateUtils');
const NumberUtils = require('../common/numberUtils1');
const AgentService = require('../common/agentService');
const roundTo = require('round-to');


const DRAW = "DRAW";
const WIN = "WIN";
const HALF_WIN = "HALF_WIN";
const LOSE = "LOSE";
const HALF_LOSE = "HALF_LOSE";


function cancelByMatchId(matchId) {

}

function cancelByHDP() {

    let condition = {'hdp.matchId': matchId, status: 'RUNNING'};

    if (type === 'HT') {
        condition['hdp.oddType'] = {'$in': ['AH1ST', 'OU1ST', 'OE1ST', 'X121ST']}
    }

    BetTransactionModel.find(condition, function (err, betResponse) {
        if (err) {
            callback(err, null);
            return;
        }

        async.each(betResponse, (betObj, asyncCallback) => {

            async.series([callback => {

                let body = {
                    status: status,
                    remark: remark,
                    cancelDate:DateUtils.getCurrentDate(),
                    cancelByType:'API'
                };

                BetTransactionModel.update({_id: betObj._id}, body, (err, data) => {
                    if (err) {
                        callback(null, 'success');
                    } else {
                        callback('fail', null);
                    }
                });

            }], function (err, asyncResponse) {

                MemberService.updateBalance2(betObj.memberUsername, Math.abs(betObj.memberCredit), betObj.betId, 'CANCEL_MATCH', betObj.betId, function (err, creditResponse) {

                    if (err) {
                        asyncCallback('fail', null);
                    } else {
                        asyncCallback(null, 'success');
                    }
                });

            });

        }, (err) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, betResponse);
            }
        });//end forEach Match

    }).populate({path: 'memberId', select: 'username_lower'})
        .populate({path: 'memberParentGroup', select: 'type endpoint'})

}


function calculateParlayOdds(betResult, odd) {
    if (betResult === WIN) {
        return odd;
    } else if (betResult === HALF_WIN) {
        return 1 + ((odd - 1) / 2);
    } else if (betResult === DRAW) {
        return 1;
    } else if (betResult === HALF_LOSE) {
        return 1;
    }
    return odd;
}

module.exports = {getCredit}