const redis = require("ioredis");

// redis.createClient({host: redisIp, port: redisPort});
// client_redis.select(1);
// const CLIENT_NAME = process.env.CLIENT_NAME || 'SPORTBOOK88';
// if (_.isEqual(CLIENT_NAME, 'SPORTBOOK88')) {

// }

var redisIp = "redis-18419.c1.ap-southeast-1-1.ec2.cloud.redislabs.com";

var redis_client = new redis({
  port: 18419,
  host: redisIp,
  family: 4,
  db: 0,
  password: "CDEmLaNwEUwUECIYDyaygH1EpV1TkzTT",
});

module.exports = {
  getProductList: () => {
    return new Promise((resolve, reject) => {
      redis_client.get(`productList`, (error, result) => {
        if (error) {
          console.log(error);
          resolve(error);
        } else {
          var dataLists = JSON.parse(result);
          if (dataLists.length == 0) {
            resolve();
          } else {
            resolve(dataLists);
          }
        }
      });
    });
  },
  addProduct: (obj) => {
    return new Promise((resolve, reject) => {
      redis_client.get(`productList`, (error, result) => {
        if (error) {
          console.log(error);
          resolve(error);
        } else {
          if (result.length === 0) {
            let dataLists = [];
            dataLists.push({
              productName: obj.productName,
              productCode: obj.productCode,
              isActive: obj.isActive,
              lists: [],
            });
            redis_client.set(`productList`, JSON.stringify(dataLists));
          } else {
            let dataLists = JSON.parse(result);
            dataLists.push({
              productName: obj.productName,
              productCode: obj.productCode,
              isActive: obj.isActive,
              lists: [],
            });
            redis_client.set(`productList`, JSON.stringify(dataLists));
            resolve(dataLists);
          }
        }
      });
    });
  },
  addGame: (productCode, obj) => {
    return new Promise((resolve, reject) => {
      redis_client.get(`productList`, (error, result) => {
        if (error) {
          console.log(error);
          resolve(error);
        } else {
          let dataLists = JSON.parse(result);
          dataLists.filter((value) => {
            if (value.productCode == productCode) {
              value.lists.push({
                productCode: productCode,
                seq: obj.seqNo,
                gameId: obj.gameId,
                gameCode: obj.gameCode,
                gameName: obj.gameName,
                imgUrl: obj.gameImage,
                gameType: obj.gameType,
                isActive: obj.isActive,
                mode: obj.mode,
              });
            }
          });
          redis_client.set(`productList`, JSON.stringify(dataLists));
          resolve(dataLists);
        }
      });
    });
  },
  editProduct: (productCode, obj) => {
    return new Promise((resolve, reject) => {
      redis_client.get(`productList`, (error, result) => {
        if (error) {
          console.log(error);
          resolve(error);
        } else {
          let dataLists = JSON.parse(result);
          dataLists.filter((value) => {
            if (value.productCode === productCode) {
              value.isActive = obj.isActive;
            }
          });
          redis_client.set(`productList`, JSON.stringify(dataLists));
          resolve(dataLists);
        }
      });
    });
  },
  editGame: (productCode, gameId, obj) => {
    return new Promise((resolve, reject) => {
      redis_client.get(`productList`, (error, result) => {
        if (error) {
          console.log(error);
          resolve(error);
        } else {
          let dataLists = JSON.parse(result);
          dataLists.forEach((value) => {
            if (value.productCode === productCode) {
              value.lists.filter((v) => {
                if (v.gameId == gameId) {
                  v.seq = obj.seqNo;
                  v.gameId = obj.gameId;
                  v.gameCode = obj.gameCode;
                  v.gameName = obj.gameName;
                  v.imgUrl = obj.gameImage;
                  v.gameType = obj.gameType;
                  v.isActive = obj.isActive;
                  v.mode = obj.mode;
                }
              });
            }
          });
          redis_client.set(`productList`, JSON.stringify(dataLists));
          resolve(dataLists);
        }
      });
    });
  },
};
