const redis     = require("ioredis");
const _         = require('underscore');
let redisIp   = process.env.REDIS_IP || "redis.api-hub.com";
const redisPort = process.env.REDIS_PORT || "6379";

let client_redis = new redis({
    port: redisPort,          // Redis port
    host: redisIp,   // Redis host
    family: 4,           // 4 (IPv4) or 6 (IPv6)
    db: 8
});
// redis.createClient({host: redisIp, port: redisPort});
// client_redis.select(1);
const CLIENT_NAME = process.env.CLIENT_NAME || 'SPORTBOOK88';
if (_.isEqual(CLIENT_NAME, 'SPORTBOOK88')) {
    redisIp = "redis-19323.c1.ap-southeast-1-1.ec2.cloud.redislabs.com";
    client_redis = new redis({
        port: 19323,
        host: redisIp,
        family: 4,
        db: 0,
        password: "tm5ooPZeVqFrxbWYQy3DZEauC2hRt2pq"
    });
}
module.exports = {
    footballHDP : (callback) => {
        client_redis.get(`isnHDP`, (error, result) => {
            if (error || _.isNull(result)) {
                callback('error', null);
            } else {
                console.log(`Got isnLive data from redis`);
                callback(null, JSON.parse(result));
            }
        });
    },
    footballLive : (callback) => {
        client_redis.get(`isnLive`, (error, result) => {
            if (error || _.isNull(result)) {
                callback('error', null);
            } else {
                console.log(`Got isnLive data from redis`);
                callback(null, JSON.parse(result));
            }
        });
    },

    setIsMaintenance : (clientName, isMaintenance, callback) => {
        client_redis.get(`maintenance`, (error, result) => {
            if (error || _.isNull(result)) {
                const obj = {};
                obj.AMB_SPORTBOOK = true;
                obj.SPORTBOOK88 = true;
                client_redis.set(`maintenance`, JSON.stringify(obj));
                callback('error', null);
            } else {
                console.log(`Got maintenance data from redis`);
                const obj = JSON.parse(result);
                obj[clientName] = isMaintenance ;
                client_redis.set(`maintenance`, JSON.stringify(obj));
                callback(null, 'success');
            }
        });
    },

    getIsMaintenance : (clientName, callback) => {
        client_redis.get(`xxx_maintenance`, (error, result) => {
            if (error || _.isNull(result)) {
                callback('error', null);
            } else {
                console.log(`Got maintenance ${clientName} data from redis ${JSON.parse(result)[clientName]}`);
                try {
                    callback(null, JSON.parse(result)[clientName]);
                } catch (e) {
                    console.error('getIsMaintenance ' + e);
                    callback(null, false);
                }
            }
        });
    },

    getIsSexyBacaratMaintenance : (clientName, callback) => {
        client_redis.get(`xxx_sexyBacaratMaintenance`, (error, result) => {
            if (error || _.isNull(result)) {
                callback('error', null);
            } else {
                console.log(`Got sexyBacaratMaintenance ${clientName} data from redis ${JSON.parse(result)[clientName]}`);
                try {
                    callback(null, JSON.parse(result)[clientName]);
                } catch (e) {
                    console.error('getIsSexyBacaratMaintenance ' + e);
                    callback(null, false);
                }
            }
        });
    },

    getIsSABacaratMaintenance : (clientName, callback) => {
        client_redis.get(`xxx_SABacaratMaintenance`, (error, result) => {
            if (error || _.isNull(result)) {
                callback('error', null);
            } else {
                console.log(`Got xxx_SABacaratMaintenance ${clientName} data from redis ${JSON.parse(result)[clientName]}`);
                try {
                    callback(null, JSON.parse(result)[clientName]);
                } catch (e) {
                    console.error('xxx_SABacaratMaintenance ' + e);
                    callback(null, false);
                }
            }
        });
    },

    getIsLotusBacaratMaintenance : (clientName, callback) => {
        client_redis.get(`xxx_lotusBacaratMaintenance`, (error, result) => {
            if (error || _.isNull(result)) {
                callback('error', null);
            } else {
                console.log(`Got xxx_lotusBacaratMaintenance ${clientName} data from redis ${JSON.parse(result)[clientName]}`);
                try {
                    callback(null, JSON.parse(result)[clientName]);
                } catch (e) {
                    console.error('xxx_lotusBacaratMaintenance ' + e);
                    callback(null, false);
                }
            }
        });
    },

    getIsSlotMaintenance : (clientName, callback) => {
        client_redis.get(`xxx_slotMaintenance`, (error, result) => {
            if (error || _.isNull(result)) {
                callback('error', null);
            } else {
                console.log(`Got slotMaintenance ${clientName} data from redis ${JSON.parse(result)[clientName]}`);
                try {
                    callback(null, JSON.parse(result)[clientName]);
                } catch (e) {
                    console.error('getIsSlotMaintenance ' + e);
                    callback(null, false);
                }
            }
        });
    },

    getCurrency : (callback) => {
        const currency = [{"currency":1,"currencyName":"MYR","currencyTHB":8},{"currency":1,"currencyName":"HDK","currencyTHB":4},{"currency":1,"currencyName":"CNY","currencyTHB":5},{"currency":1,"currencyName":"IDR","currencyTHB":425},{"currency":1,"currencyName":"VND","currencyTHB":715},{"currency":1,"currencyName":"USD","currencyTHB":33}];
        client_redis.get(`xxx_currency`, (error, result) => {
            if (error || _.isNull(result)) {
                callback(null, currency);
            } else {
                try {
                    callback(null, JSON.parse(result));
                } catch (e) {
                    console.error('getCurrency ' + e);
                    callback(null, currency);
                }
            }
        });
    },

    basketballHDP : (callback) => {
        client_redis.get(`188BasketballHDP`, (error, result) => {
            if (error || _.isNull(result)) {
                callback('error', null);
            } else {
                console.log(`Got 188BasketballHDP data from redis`);
                callback(null, JSON.parse(result));
            }
        });
    },
    basketballLive : (callback) => {
        client_redis.get(`188BasketballLive`, (error, result) => {
            if (error || _.isNull(result)) {
                callback('error', null);
            } else {
                console.log(`Got 188BasketballLive data from redis`);
                callback(null, JSON.parse(result));
            }
        });
    },
    tennisHDP : (callback) => {
        client_redis.get(`188TennisHDP`, (error, result) => {
            if (error || _.isNull(result)) {
                callback('error', null);
            } else {
                console.log(`Got 188TennisHDP data from redis`);
                callback(null, JSON.parse(result));
            }
        });
    },
    tennisLive : (callback) => {
        client_redis.get(`188TennisLive`, (error, result) => {
            if (error || _.isNull(result)) {
                callback('error', null);
            } else {
                console.log(`Got 188TennisLive data from redis`);
                callback(null, JSON.parse(result));
            }
        });
    }
};

