const MemberModel = require('../models/member.model');
const request = require('request');
const _ = require('underscore');
const mongoose = require('mongoose');
const BetTransactionModel = require('../models/betTransaction.model.js');
const async = require("async");
const moment = require('moment');
const querystring = require('querystring');
const Crypto = require("crypto");
const AgentGroupModel = require('../models/agentGroup.model.js');
const MemberService = require('../common/memberService');
const DateUtils = require('../common/dateUtils');
const NumberUtils = require('../common/numberUtils1');
const AgentService = require('../common/agentService');
const roundTo = require('round-to');


const VT_IMAGE_URL = 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/new-game';
const CURRENCY = 'THB';

const getClientKey = require('../controllers/getClientKey');
const {
    GANAPATI_URL: FORWARD_GAME = 'https://tw-dev.ganapati.tech:8080/',
    GANAPATI_AGENT_NAME: AGENT_NAME= 'askmebet',
    GANAPATI_AGENT_CODE: AGENT_CODE= 'ambbet'
} = getClientKey.getKey();

const CLIENT_NAME         = process.env.CLIENT_NAME          || 'SPORTBOOK88';
const URL_API = 'http://dev.virtual-technology.com:8088/';
const key = 'NeGqWKgwp07QXPdnh0HsEgkpEeb25SamoBU3bFRh1IQ=';
const iv = 'RH/nP5lzsAC7+LRidCmsGw==';
const data = {
    "accountId":"UserID000001"
};

function forwardToGame(token, gameCode, callback) {
    let url = `${FORWARD_GAME}${AGENT_NAME}/launchGame?game=${gameCode}&currency=${CURRENCY}&locale=th&launchToken=${token}&brand=${AGENT_CODE}`;
    callback(null, url);
}

function encrypt(data,ikey,iIv) {
    let buff = new Buffer(iIv, 'base64');
    let stringIV = buff.toString('ascii');
    buff = new Buffer(ikey, 'base64');
    let stringKey = buff.toString('ascii');

    let cipher = crypto.createCipheriv('aes-256-cbc', stringKey,stringIV)
    let encryptedPassword = cipher.update(JSON.stringify(data), 'utf8', 'base64');
    encryptedPassword += cipher.final('base64');
    return encryptedPassword;
}


function decrypt(data,ikey,iIv) {
    let buff = new Buffer(iIv, 'base64');
    let stringIV = buff.toString('ascii');
    buff = new Buffer(ikey, 'base64');
    let stringKey = buff.toString('ascii');

    let decipher = crypto.createDecipheriv('aes-256-cbc', stringKey,stringIV)
    let decryptedPassword = decipher.update(JSON.stringify(data), 'base64', 'utf8');
    decryptedPassword += decipher.final('utf8');
    return decryptedPassword;
}

let encryptedPassword = encrypt(data,key,iv)
let decryptedPassword = decrypt(encryptedPassword,key,iv)

function register() {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };
}


function getGameName(id) {
    switch (id) {
        case "Aladin":
            return "Aladin";
        case "AliceAdv":
            return "AliceAdv";
        case "Arcadia":
            return "Arcadia";
        case "ArtificialGirl":
            return "ArtificialGirl";
        case "AsTheGodsWill":
            return "AsTheGodsWill";
        case "AzurLane":
            return "AzurLane";
        case "BarBarSheep":
            return "BarBarSheep";
        case "BattleGrounds":
            return "BattleGrounds";
        case "BeanStalk":
            return "BeanStalk";
        case "BreakAway":
            return "BreakAway";
        case "BuffaloBlitz":
            return "BuffaloBlitz";
        case "Chaoji8":
            return "Chaoji8";
        case "Circus":
            return "Circus";
        case "CrossyRoad":
            return "CrossyRoad";
        case "DiamondSlot":
            return "DiamondSlot";
        case "DuoFu10c5Treasures":
            return "DuoFu10c5Treasures";
        case "DuoFu10c88Fortune":
            return "DuoFu10c88Fortune";
        case "DuoFu10cDancingDrum":
            return "DuoFu10cDancingDrum";
        case "DuoFu10cDiamondEternity":
            return "DuoFu10cDiamondEternity";
        case "DuoFu10cFlowerOfRiches":
            return "DuoFu10cFlowerOfRiches";
        case "DuoFuDuoCai5Treasures":
            return "DuoFuDuoCai5Treasures";
        case "DuoFuDuoCai88Fortune":
            return "DuoFuDuoCai88Fortune";
        case "DuoFuDuoCaiDancingDrum":
            return "DuoFuDuoCaiDancingDrum";
        case "DuoFuDuoCaiDiamondEternity":
            return "DuoFuDuoCaiDiamondEternity";
        case "DuoFuDuoCaiFlowerOfRiches":
            return "DuoFuDuoCaiFlowerOfRiches";
        case "EgyptCity":
            return "EgyptCity";
        case "FIFA2018":
            return "FIFA2018";
        case "FaFaFa":
            return "FaFaFa";
        case "FallingSakura":
            return "FallingSakura";
        case "FiftyLions":
            return "FiftyLions";
        case "FishParty":
            return "FishParty";
        case "FishingExpert":
            return "FishingExpert";
        case "FiveChildTeasingMaitreya":
            return "FiveChildTeasingMaitreya";
        case "FiveDragons.jpg":
            return "FiveDragons";
        case "FiveFlowers.jpg":
            return "FiveFlowers";
        case "FortuneKing":
            return "FortuneKing";
        case "FoxSpirit":
            return "FoxSpirit";
        case "FunkyMonkey":
            return "FunkyMonkey";
        case "Geisha":
            return "Geisha";
        case "GreatBlue":
            return "GreatBlue";
        case "Halloween":
            return "Halloween";
        case "Horizon":
            return "Horizon";
        case "HungryHungryShark":
            return "HungryHungryShark";
        case "IceWind":
            return "IceWind";
        case "JuFu10cBloomingRiches":
            return "JuFu10cBloomingRiches";
        case "JuFu10cEternalDiamond":
            return "JuFu10cEternalDiamond";
        case "JuFu10cFountainOfWealth":
            return "JuFu10cFountainOfWealth";
        case "JuFu10cGoldenTale":
            return "JuFu10cGoldenTale";
        case "JuFu10cRhythmOfFortune":
            return "JuFu10cRhythmOfFortune";
        case "JuFuBloomingRiches":
            return "JuFuBloomingRiches";
        case "JuFuEternalDiamond":
            return "JuFuEternalDiamond";
        case "JuFuFountainOfWealth":
            return "JuFuFountainOfWealth";
        case "JuFuGoldenTale":
            return "JuFuGoldenTale";
        case "JuFuRhythmOfFortune":
            return "JuFuRhythmOfFortune";
        case "JurassicWorld":
            return "JurassicWorld";
        case "KingsOfCash":
            return "KingsOfCash";
        case "LineBrownDaydream":
            return "LineBrownDaydream";
        case "LionDance2":
            return "LionDance2";
        case "LongLongLong":
            return "LongLongLong";
        case "LuckyTwins":
            return "LuckyTwins";
        case "MagicCandy":
            return "MagicCandy";
        case "MapleStory":
            return "MapleStory";
        case "MarvelClassic":
            return "MarvelClassic";
        case "MarvelTsum":
            return "MarvelTsum";
        case "MightMagic":
            return "MightMagic";
        case "Military":
            return "Military";
        case "Mojin2":
            return "Mojin2";
        case "MoneyFarm":
            return "MoneyFarm";
        case "MysticNine":
            return "MysticNine";
        case "Ninja":
            return "Ninja";
        case "Odin":
            return "Odin";
        case "Onmologist":
            return "Onmologist";
        case "Pirates":
            return "Pirates";
        case "PixelWar":
            return "PixelWar";
        case "PokerCity":
            return "PokerCity";
        case "PurePlatinum":
            return "PurePlatinum";
        case "Rich":
            return "Rich";
        case "SevenLuckyGod":
            return "SevenLuckyGod";
        case "SixLions":
            return "SixLions";
        case "Slot2":
            return "Slot2";
        case "SlotJumanji":
            return "SlotJumanji";
        case "SpecialChef":
            return "SpecialChef";
        case "SunQuest":
            return "SunQuest";
        case "TaiWangSiShen":
            return "TaiWangSiShen";
        case "TaikoDrumMaster":
            return "TaikoDrumMaster";
        case "Tarzan":
            return "Tarzan";
        case "TravelFrog":
            return "TravelFrog";
        case "Triple7":
            return "Triple7";
        case "TripleMonkey":
            return "TripleMonkey";
        case "TripleMonkey10C":
            return "TripleMonkey10C";
        case "Tropics":
            return "Tropics";
        case "TrueBreakAway":
            return "TrueBreakAway";
        case "TrueZhaoCaiJinBao":
            return "TrueZhaoCaiJinBao";
        case "VT":
            return "VT";
        case "WarHammer40K":
            return "WarHammer40K";
        case "WaterMargin":
            return "WaterMargin";
        case "WuFuTongZi":
            return "WuFuTongZi";
        case "WuLuCaiShen":
            return "WuLuCaiShen";
        case "ZhaoCaiJinBao":
            return "ZhaoCaiJinBao";
        case "ZhaoCaiTongZi":
            return "ZhaoCaiTongZi";
        case "ZombieHunter":
            return "ZombieHunter";
        default:
            return id;
    }
}


let gameList = [
    {
        Type: 'Slot',
        GameId: 'Aladin',
        GameName: getGameName('Aladin'),
        ImageUrl: VT_IMAGE_URL+'/Aladin.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'AliceAdv',
        GameName: getGameName('AliceAdv'),
        ImageUrl: VT_IMAGE_URL+'/AliceAdv.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'Arcadia',
        GameName: getGameName('Arcadia'),
        ImageUrl: VT_IMAGE_URL+'/Arcadia.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'ArtificialGirl',
        GameName: getGameName('ArtificialGirl'),
        ImageUrl: VT_IMAGE_URL+'/ArtificialGirl.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'AsTheGodsWill',
        GameName: getGameName('AsTheGodsWill'),
        ImageUrl: VT_IMAGE_URL+'/AsTheGodsWill.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'AzurLane',
        GameName: getGameName('AzurLane'),
        ImageUrl: VT_IMAGE_URL+'/AzurLane.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'BarBarSheep',
        GameName: getGameName('BarBarSheep'),
        ImageUrl: VT_IMAGE_URL+'/BarBarSheep.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'BattleGrounds',
        GameName: getGameName('BattleGrounds'),
        ImageUrl: VT_IMAGE_URL+'/BattleGrounds.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'BeanStalk',
        GameName: getGameName('BeanStalk'),
        ImageUrl: VT_IMAGE_URL+'/BeanStalk.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'BreakAway',
        GameName: getGameName('BreakAway'),
        ImageUrl: VT_IMAGE_URL+'/BreakAway.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'BuffaloBlitz',
        GameName: getGameName('BuffaloBlitz'),
        ImageUrl: VT_IMAGE_URL+'/BuffaloBlitz.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'Chaoji8',
        GameName: getGameName('Chaoji8'),
        ImageUrl: VT_IMAGE_URL+'/Chaoji8.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'Circus',
        GameName: getGameName('Circus'),
        ImageUrl: VT_IMAGE_URL+'/Circus.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'CrossyRoad',
        GameName: getGameName('CrossyRoad'),
        ImageUrl: VT_IMAGE_URL+ '/CrossyRoad.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'DiamondSlot',
        GameName: getGameName('DiamondSlot'),
        ImageUrl: VT_IMAGE_URL+'/DiamondSlot.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'DuoFu10c5Treasures',
        GameName: getGameName('DuoFu10c5Treasures'),
        ImageUrl: VT_IMAGE_URL+'/DuoFu10c5Treasures.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'DuoFu10c88Fortune',
        GameName: getGameName('DuoFu10c88Fortune'),
        ImageUrl: VT_IMAGE_URL+'/DuoFu10c88Fortune.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'DuoFu10cDancingDrum',
        GameName: getGameName('DuoFu10cDancingDrum'),
        ImageUrl: VT_IMAGE_URL+'/DuoFu10cDancingDrum.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'DuoFu10cDiamondEternity',
        GameName: getGameName('DuoFu10cDiamondEternity'),
        ImageUrl: VT_IMAGE_URL+'/DuoFu10cDiamondEternity.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'DuoFu10cFlowerOfRiches',
        GameName: getGameName('DuoFu10cFlowerOfRiches'),
        ImageUrl: VT_IMAGE_URL+'/DuoFu10cFlowerOfRiches.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'DuoFuDuoCai5Treasures',
        GameName: getGameName('DuoFuDuoCai5Treasures'),
        ImageUrl: VT_IMAGE_URL+'/DuoFuDuoCai5Treasures.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'DuoFuDuoCai88Fortune',
        GameName: getGameName('DuoFuDuoCai88Fortune'),
        ImageUrl: VT_IMAGE_URL+'/DuoFuDuoCai88Fortune.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'DuoFuDuoCaiDancingDrum',
        GameName: getGameName('DuoFuDuoCaiDancingDrum'),
        ImageUrl: VT_IMAGE_URL+'/DuoFuDuoCaiDancingDrum.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'DuoFuDuoCaiDiamondEternity',
        GameName: getGameName('DuoFuDuoCaiDiamondEternity'),
        ImageUrl: VT_IMAGE_URL+'/DuoFuDuoCaiDiamondEternity.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'DuoFuDuoCaiFlowerOfRiches',
        GameName: getGameName('DuoFuDuoCaiFlowerOfRiches'),
        ImageUrl: VT_IMAGE_URL+'/DuoFuDuoCaiFlowerOfRiches.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'EgyptCity',
        GameName: getGameName('EgyptCity'),
        ImageUrl: VT_IMAGE_URL+'/EgyptCity.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'FIFA2018',
        GameName: getGameName('FIFA2018'),
        ImageUrl: VT_IMAGE_URL+'/FIFA2018.jpg'
    },{
        Type: 'Slot',
        GameId: 'FaFaFa',
        GameName: getGameName('FaFaFa'),
        ImageUrl: VT_IMAGE_URL+'/FaFaFa.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'FallingSakura',
        GameName: getGameName('FallingSakura'),
        ImageUrl: VT_IMAGE_URL+'/FallingSakura.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'FiftyLions',
        GameName: getGameName('FiftyLions'),
        ImageUrl: VT_IMAGE_URL+'/FiftyLions.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'FishParty',
        GameName: getGameName('FishParty'),
        ImageUrl: VT_IMAGE_URL+'/FishParty.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'FishingExpert',
        GameName: getGameName('FishingExpert'),
        ImageUrl: VT_IMAGE_URL+'/FishingExpert.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'FiveChildTeasingMaitreya',
        GameName: getGameName('FiveChildTeasingMaitreya'),
        ImageUrl: VT_IMAGE_URL+'/FiveChildTeasingMaitreya.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'FiveDragons',
        GameName: getGameName('FiveDragons'),
        ImageUrl: VT_IMAGE_URL+'/FiveDragons.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'FiveFlowers',
        GameName: getGameName('FiveFlowers'),
        ImageUrl: VT_IMAGE_URL+'/FiveFlowers.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'FortuneKing',
        GameName: getGameName('FortuneKing'),
        ImageUrl: VT_IMAGE_URL+'/FortuneKing.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'FoxSpirit',
        GameName: getGameName('FoxSpirit'),
        ImageUrl: VT_IMAGE_URL+'/FoxSpirit.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'FunkyMonkey',
        GameName: getGameName('FunkyMonkey'),
        ImageUrl: VT_IMAGE_URL+'/FunkyMonkey.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'Geisha',
        GameName: getGameName('Geisha'),
        ImageUrl: VT_IMAGE_URL+'/Geisha.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'GreatBlue',
        GameName: getGameName('GreatBlue'),
        ImageUrl: VT_IMAGE_URL+'/GreatBlue.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'Halloween',
        GameName: getGameName('Halloween'),
        ImageUrl: VT_IMAGE_URL+'/Halloween.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'Horizon',
        GameName: getGameName('Horizon'),
        ImageUrl: VT_IMAGE_URL+'/Horizon.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'HungryHungryShark',
        GameName: getGameName('HungryHungryShark'),
        ImageUrl: VT_IMAGE_URL+'/HungryHungryShark.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'IceWind',
        GameName: getGameName('IceWind'),
        ImageUrl: VT_IMAGE_URL+'/IceWind.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'JuFu10cBloomingRiches',
        GameName: getGameName('JuFu10cBloomingRiches'),
        ImageUrl: VT_IMAGE_URL+'/JuFu10cBloomingRiches.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'JuFu10cEternalDiamond',
        GameName: getGameName('JuFu10cEternalDiamond'),
        ImageUrl: VT_IMAGE_URL+'/JuFu10cEternalDiamond.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'JuFu10cFountainOfWealth',
        GameName: getGameName('JuFu10cFountainOfWealth'),
        ImageUrl: VT_IMAGE_URL+'/JuFu10cFountainOfWealth.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'JuFu10cGoldenTale',
        GameName: getGameName('JuFu10cGoldenTale'),
        ImageUrl: VT_IMAGE_URL+'/JuFu10cGoldenTale.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'JuFu10cRhythmOfFortune',
        GameName: getGameName('JuFu10cRhythmOfFortune'),
        ImageUrl: VT_IMAGE_URL+'/JuFu10cRhythmOfFortune.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'JuFuBloomingRiches',
        GameName: getGameName('JuFuBloomingRiches'),
        ImageUrl: VT_IMAGE_URL+'/JuFuBloomingRiches.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'JuFuEternalDiamond',
        GameName: getGameName('JuFuEternalDiamond'),
        ImageUrl: VT_IMAGE_URL+'/JuFuEternalDiamond.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'JuFuFountainOfWealth',
        GameName: getGameName('JuFuFountainOfWealth'),
        ImageUrl: VT_IMAGE_URL+'/JuFuFountainOfWealth.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'JuFuGoldenTale',
        GameName: getGameName('JuFuGoldenTale'),
        ImageUrl: VT_IMAGE_URL+'/JuFuGoldenTale.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'JuFuRhythmOfFortune',
        GameName: getGameName('JuFuRhythmOfFortune'),
        ImageUrl: VT_IMAGE_URL+'/JuFuRhythmOfFortune.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'JurassicWorld',
        GameName: getGameName('JurassicWorld'),
        ImageUrl: VT_IMAGE_URL+'/JurassicWorld.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'KingsOfCash',
        GameName: getGameName('KingsOfCash'),
        ImageUrl: VT_IMAGE_URL+'/KingsOfCash.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'LineBrownDaydream',
        GameName: getGameName('LineBrownDaydream'),
        ImageUrl: VT_IMAGE_URL+'/LineBrownDaydream.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'LionDance2',
        GameName: getGameName('LionDance2'),
        ImageUrl: VT_IMAGE_URL+'/LionDance2.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'LongLongLong',
        GameName: getGameName('LongLongLong'),
        ImageUrl: VT_IMAGE_URL+'/LongLongLong.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'LuckyTwins',
        GameName: getGameName('LuckyTwins'),
        ImageUrl: VT_IMAGE_URL+'/LuckyTwins.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'MagicCandy',
        GameName: getGameName('MagicCandy'),
        ImageUrl: VT_IMAGE_URL+'/MagicCandy.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'MapleStory',
        GameName: getGameName('MapleStory'),
        ImageUrl: VT_IMAGE_URL+'/MapleStory.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'MarvelClassic',
        GameName: getGameName('MarvelClassic'),
        ImageUrl: VT_IMAGE_URL+'/MarvelClassic.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'MarvelTsum',
        GameName: getGameName('MarvelTsum'),
        ImageUrl: VT_IMAGE_URL+'/MarvelTsum.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'MightMagic',
        GameName: getGameName('MightMagic'),
        ImageUrl: VT_IMAGE_URL+'/MightMagic.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'Military',
        GameName: getGameName('Military'),
        ImageUrl: VT_IMAGE_URL+'/Military.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'Mojin2',
        GameName: getGameName('Mojin2'),
        ImageUrl: VT_IMAGE_URL+'/Mojin2.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'MoneyFarm',
        GameName: getGameName('MoneyFarm'),
        ImageUrl: VT_IMAGE_URL+'/MoneyFarm.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'MysticNine',
        GameName: getGameName('MysticNine'),
        ImageUrl: VT_IMAGE_URL+'/MysticNine.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'Ninja',
        GameName: getGameName('Ninja'),
        ImageUrl: VT_IMAGE_URL+'/Ninja.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'Odin',
        GameName: getGameName('Odin'),
        ImageUrl: VT_IMAGE_URL+'/Odin.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'Onmologist',
        GameName: getGameName('Onmologist'),
        ImageUrl: VT_IMAGE_URL+'/Onmologist.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'Pirates',
        GameName: getGameName('Pirates'),
        ImageUrl: VT_IMAGE_URL+'/Pirates.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'PixelWar',
        GameName: getGameName('PixelWar'),
        ImageUrl: VT_IMAGE_URL+'/PixelWar.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'PokerCity',
        GameName: getGameName('PokerCity'),
        ImageUrl: VT_IMAGE_URL+'/PokerCity.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'PurePlatinum',
        GameName: getGameName('PurePlatinum'),
        ImageUrl: VT_IMAGE_URL+'/PurePlatinum.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'Rich',
        GameName: getGameName('Rich'),
        ImageUrl: VT_IMAGE_URL+'/Rich.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'SevenLuckyGod',
        GameName: getGameName('SevenLuckyGod'),
        ImageUrl: VT_IMAGE_URL+'/SevenLuckyGod.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'SixLions',
        GameName: getGameName('SixLions'),
        ImageUrl: VT_IMAGE_URL+'/SixLions.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'Slot2',
        GameName: getGameName('Slot2'),
        ImageUrl: VT_IMAGE_URL+'/Slot2.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'SlotJumanji',
        GameName: getGameName('SlotJumanji'),
        ImageUrl: VT_IMAGE_URL+'/SlotJumanji.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'SpecialChef',
        GameName: getGameName('SpecialChef'),
        ImageUrl: VT_IMAGE_URL+'/SpecialChef.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'SunQuest',
        GameName: getGameName('SunQuest'),
        ImageUrl: VT_IMAGE_URL+'/SunQuest.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'TaiWangSiShen',
        GameName: getGameName('TaiWangSiShen'),
        ImageUrl: VT_IMAGE_URL+'/TaiWangSiShen.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'TaikoDrumMaster',
        GameName: getGameName('TaikoDrumMaster'),
        ImageUrl: VT_IMAGE_URL+'/TaikoDrumMaster.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'Tarzan',
        GameName: getGameName('Tarzan'),
        ImageUrl: VT_IMAGE_URL+'/Tarzan.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'TravelFrog',
        GameName: getGameName('TravelFrog'),
        ImageUrl: VT_IMAGE_URL+'/TravelFrog.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'Triple7',
        GameName: getGameName('Triple7'),
        ImageUrl: VT_IMAGE_URL+'/Triple7.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'TripleMonkey',
        GameName: getGameName('TripleMonkey'),
        ImageUrl: VT_IMAGE_URL+'/TripleMonkey.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'TripleMonkey10C',
        GameName: getGameName('TripleMonkey10C'),
        ImageUrl: VT_IMAGE_URL+'/TripleMonkey10C.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'Tropics',
        GameName: getGameName('Tropics'),
        ImageUrl: VT_IMAGE_URL+'/Tropics.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'TrueBreakAway',
        GameName: getGameName('TrueBreakAway'),
        ImageUrl: VT_IMAGE_URL+'/TrueBreakAway.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'TrueZhaoCaiJinBao',
        GameName: getGameName('TrueZhaoCaiJinBao'),
        ImageUrl: VT_IMAGE_URL+'/TrueZhaoCaiJinBao.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'VT',
        GameName: getGameName('VT'),
        ImageUrl: VT_IMAGE_URL+'/VT.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'WarHammer40K',
        GameName: getGameName('WarHammer40K'),
        ImageUrl: VT_IMAGE_URL+'/WarHammer40K.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'WaterMargin',
        GameName: getGameName('WaterMargin'),
        ImageUrl: VT_IMAGE_URL+'/WaterMargin.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'WuFuTongZi',
        GameName: getGameName('WuFuTongZi'),
        ImageUrl: VT_IMAGE_URL+'/WuFuTongZi.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'WuLuCaiShen',
        GameName: getGameName('WuLuCaiShen'),
        ImageUrl: VT_IMAGE_URL+'/WuLuCaiShen.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'ZhaoCaiJinBao',
        GameName: getGameName('ZhaoCaiJinBao'),
        ImageUrl: VT_IMAGE_URL+'/ZhaoCaiJinBao.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'ZhaoCaiTongZi',
        GameName: getGameName('ZhaoCaiTongZi'),
        ImageUrl: VT_IMAGE_URL+'/ZhaoCaiTongZi.jpg'
    },
    {
        Type: 'Slot',
        GameId: 'ZombieHunter',
        GameName: getGameName('ZombieHunter'),
        ImageUrl: VT_IMAGE_URL+'/ZombieHunter.jpg'
    },
]

module.exports = {callGetGameList, forwardToGame}