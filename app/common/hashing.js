const crypto = require('crypto');
const config = require('config');


module.exports = {
    encrypt256 : (password, callback) => {
        let hash = crypto.createHash('sha256')
            .update(password+'')
            .digest('base64');
        callback(null, hash);
    },
    encrypt256_1 : (password) => {
        let hash = crypto.createHash('sha256')
            .update(password+'')
            .digest('base64');
        return hash;
    },
    makeSignature : (secretKey,message) => {
        const hmac = crypto.createHmac('sha256', secretKey);
        hmac.update(message);
        return hmac.digest('hex');
    },
    sha1Hmac : (key,data) => {
         return crypto.createHmac('sha1', key).update(data).digest('hex');

    }
};