const moment = require('moment');
const dateFormat = require('dateformat');
const querystring = require('querystring');
const crypto = require('crypto');
const request = require('request');
const parseString = require('xml2js').parseString;
const _ = require('underscore');

const getClientKey = require('../controllers/getClientKey');
const {
    SAG_URL = 'http://api.sai.slgaming.net/api/api.aspx',
    SAG_LOGIN_URL = 'https://www.sai.slgaming.net/app.aspx',
    SAG_SECRET_KEY = '43FA9A9B376E4EBDBD41A97FF3F9F31C',
    SAG_ENCRYPT_KEY = 'g9G16nTs',
    SAG_MD5_KEY = 'GgaIMaiNNtg',
    SAG_LOBBY_CODE = 'A886'
} = getClientKey.getKey();

let API_URL = SAG_URL;
let LOGIN_URL = SAG_LOGIN_URL;
let SECRET_KEY = SAG_SECRET_KEY;
let ENCRYPT_KEY = SAG_ENCRYPT_KEY;
let MD5_KEY = SAG_MD5_KEY;
let LOBBY_CODE = SAG_LOBBY_CODE;

const CLIENT_NAME         = process.env.CLIENT_NAME          || 'SPORTBOOK88';

function getGameTypeDetail(gameType) {

    switch (gameType) {
        case "bac" :
            return "Baccarat";
        case "dtx" :
            return "Dragon & Tiger";
        case "sicbo" :
            return "Sic Bo";
        case "rot" :
            return "Roulette";
        case "ftan" :
            return "Fan Tan";
        case "slot" :
            return "EGame";
        case "minigame" :
            return "Mini Game";
        case "multiplayer" :
            return "Fishermen Gold";
        default :
            return gameType
    }

}


function getGameCardResultSuit(type) {

    switch (type) {
        case "1" :
            return "spades";
        case "2" :
            return "hearts";
        case "3" :
            return "clubs";
        case "4" :
            return "diamonds";
        default :
            return type
    }

}
function getGameCardResultRank(type) {

    switch (type) {
        case "1" :
            return "A";
        case "2" :
            return "2";
        case "3" :
            return "3";
        case "4" :
            return "4";
        case "5" :
            return "5";
        case "6" :
            return "6";
        case "7" :
            return "7";
        case "8" :
            return "8";
        case "9" :
            return "9";
        case "10" :
            return "10";
        case "11" :
            return "J";
        case "12" :
            return "Q";
        case "13" :
            return "K";
        default :
            return type
    }

}
function getBetTypeDetailBA(betTypeBA) {

    switch (betTypeBA) {
        case "0" :
            return "Tie";
        case "1" :
            return "Player";
        case "2" :
            return "Banker";
        case "3" :
            return "Player Pair";
        case "4" :
            return "Banker Pair";
        case "25" :
            return "NC.Tie";
        case "26" :
            return "NC.Player";
        case "27" :
            return "NC.Banker";
        case "28" :
            return "NC. Player Pair";
        case "29" :
            return "NC. Banker Pair";
        case "30" :
            return "SuperSix";
        case "36" :
            return "Player Natural";
        case "37" :
            return "Banker Natural";
        case "40" :
            return "NC. Player Natural";
        case "41" :
            return "NC. Banker Natural";
        case "42" :
            return "Cow Cow Player";
        case "43" :
            return "Cow Cow Banker";
        case "44" :
            return "Cow Cow Tie";
        default :
            return betTypeBA
    }

}

function getBetResultDG(bool,betTypeDG) {
    if(bool === 'true') {
        switch (betTypeDG) {
            case "DTRTie" :
                return "Tie game";
            case "DTRDragonWin" :
                return "Dragon win";
            case "DTRTigerWin" :
                return "Tiger win";
            default :
                return betTypeDG
        }

    }

}

function getBetTypeResultBA(bool, betTypeBA) {
// console.log(bool,betTypeBA,'ojijihf;oerhf;oiwervoierjgio')
    if (bool === 'true') {
        switch (betTypeBA) {
            case "BRTie" :
                return "Tie";
            case "BRPlayerWin" :
                return "Player";
            case "BRBankerWin" :
                return "Banker";
            case "BRPlayerPair" :
                return "Player Pair";
            case "BRBankerPair" :
                return "Banker Pair";
            case "BRSSTie" :
                return "NC.Tie";
            case "BRSSPlayerWin" :
                return "NC.Player";
            case "BRSSBankerWin" :
                return "NC.Banker";
            case "BRSSPlayerPair" :
                return "NC. Player Pair";
            case "BRSSBankerPair" :
                return "NC. Banker Pair";
            case "BRSSSuperSix" :
                return "SuperSix";
            case "BRPlayerNatural" :
                return "Player Natural";
            case "BRBankerNatural" :
                return "Banker Natural";
            case "BRSSPlayerNatural" :
                return "NC. Player Natural";
            case "BRSSBankerNatural" :
                return "NC. Banker Natural";
            case "BRCowPlayerWin" :
                return "Cow Cow Player";
            case "BRCowBankerWin" :
                return "Cow Cow Banker";
            case "BRCowTie" :
                return "Cow Cow Tie";
            default :
                return betTypeBA
        }
    }

}
function getBetTypeDetailDG(betTypeDG) {

    switch (betTypeDG) {
        case "0" :
            return "Tie";
        case "1" :
            return "Dragon";
        case "2" :
            return "Tiger";
        default :
            return betTypeDG
    }

}
function getBetTypeDetailSB(betTypeSB) {

    switch (betTypeSB) {
        case "0" :
            return "Small";
        case "1" :
            return "Big";
        case "2" :
            return "Odd";
        case "3" :
            return "Even";
        case "4" :
            return "Number 1";
        case "5" :
            return "Number 2";
        case "6" :
            return "Number 3";
        case "7" :
            return "Number 4";
        case "8" :
            return "Number 5";
        case "9" :
            return "Number 6";
        case "10" :
            return "All 1";
        case "11" :
            return "All 2";
        case "12" :
            return "All 3";
        case "13" :
            return "All 4";
        case "14" :
            return "All 5";
        case "15" :
            return "All 6";
        case "16" :
            return "All same";
        case "17" :
            return "Point 4";
        case "18" :
            return "Point 5";
        case "19" :
            return "Point 6";
        case "20" :
            return "Point 7";
        case "21" :
            return "Point 8";
        case "22" :
            return "Point 9";
        case "23" :
            return "Point 10";
        case "24" :
            return "Point 11";
        case "25" :
            return "Point 12";
        case "26" :
            return "Point 13";
        case "27" :
            return "Point 14";
        case "28" :
            return "Point 15";
        case "29" :
            return "Point 16";
        case "30" :
            return "Point 17";
        case "31" :
            return "Specific double 1, 2";
        case "32" :
            return "Specific double 1, 3";
        case "33" :
            return "Specific double 1, 4";
        case "34" :
            return "Specific double 1, 5";
        case "35" :
            return "Specific double 1, 6";
        case "36" :
            return "Specific double 2, 3";
        case "37" :
            return "Specific double 2, 4";
        case "38" :
            return "Specific double 2, 5";
        case "39" :
            return "Specific double 2, 6";
        case "40" :
            return "Specific double 3, 4";
        case "41" :
            return "Specific double 3, 5";
        case "42" :
            return "Specific double 3, 6";
        case "43" :
            return "Specific double 4, 5";
        case "44" :
            return "Specific double 4, 6";
        case "45" :
            return "Specific double 5, 6";
        case "46" :
            return "Pair 1";
        case "47" :
            return "Pair 2";
        case "48" :
            return "Pair 3";
        case "49" :
            return "Pair 4";
        case "50" :
            return "Pair 5";
        case "51" :
            return "Pair 6";
        case "52" :
            return "Three Odd";
        case "53" :
            return "Two Odd One Even";
        case "54" :
            return "Two Even One Odd";
        case "55" :
            return "Three Even";
        case "56" :
            return "1 2 3 4";
        case "57" :
            return "2 3 4 5";
        case "58" :
            return "2 3 5 6";
        case "59" :
            return "3 4 5 6";
        case "60" :
            return "112";
        case "61" :
            return "113";
        case "62" :
            return "114";
        case "63" :
            return "115";
        case "64" :
            return "116";
        case "65" :
            return "221";
        case "66" :
            return "223";
        case "67" :
            return "224";
        case "68" :
            return "225";
        case "69" :
            return "226";
        case "70" :
            return "331";
        case "71" :
            return "332";
        case "72" :
            return "334";
        case "73" :
            return "335";
        case "74" :
            return "336";
        case "75" :
            return "441";
        case "76" :
            return "442";
        case "77" :
            return "443";
        case "78" :
            return "445";
        case "79" :
            return "446";
        case "80" :
            return "551";
        case "81" :
            return "552";
        case "82" :
            return "553";
        case "83" :
            return "554";
        case "84" :
            return "556";
        case "85" :
            return "661";
        case "86" :
            return "662";
        case "87" :
            return "663";
        case "88" :
            return "664";
        case "89" :
            return "665";
        case "90" :
            return "126";
        case "91" :
            return "135";
        case "92" :
            return "234";
        case "93" :
            return "256";
        case "94" :
            return "346";
        case "95" :
            return "123";
        case "96" :
            return "136";
        case "97" :
            return "145";
        case "98" :
            return "235";
        case "99" :
            return "356";
        case "100" :
            return "124";
        case "101" :
            return "146";
        case "102" :
            return "236";
        case "103" :
            return "245";
        case "104" :
            return "456";
        case "105" :
            return "125";
        case "106" :
            return "134";
        case "107" :
            return "156";
        case "108" :
            return "246";
        case "109" :
            return "345";
        default :
            return betTypeSB
    }

}



function getGameNameByHostId(hostId) {


    if (hostId) {
        switch (Number.parseInt(hostId)) {
            case 401 :
                return "EGame";
            case 402 :
                return "Mini game";
            case 601 :
                return "Baccarat 1";
            case 602 :
                return "Baccarat 2";
            case 603 :
                return "Baccarat 3";
            case 604 :
                return "Baccarat 4";
            case 605 :
                return "Baccarat 5";
            case 606 :
                return "Baccarat 6";
            case 607 :
                return "Baccarat 7";
            case 608 :
                return "Baccarat 8";
            case 609 :
                return "Baccarat 9";
            case 610 :
                return "Baccarat 10";
            case 611 :
                return "Baccarat 11";
            case 612 :
                return "Baccarat 12";
            case 613 :
                return "Baccarat 13";
            case 614 :
                return "Baccarat 14";
            case 615 :
                return "Baccarat 15";
            case 616 :
                return "Baccarat 16";
            case 630 :
                return "Roulette";
            case 631 :
                return "Sic Bo";
            case 632 :
                return "Fan Tan";
            case 640 :
                return "Dragon & Tiger";
            default:
                return "";
        }
    } else {
        return "";
    }

}

function encodeDesCBC(textToEncode) {
    let key = Buffer.from(ENCRYPT_KEY);
    var cipher = crypto.createCipheriv('des-cbc', key, key);
    var encryptedString = cipher.update(textToEncode, 'ascii', 'base64');
    encryptedString += cipher.final('base64');
    return encryptedString;
}

function decodeDesCBC(textToDecode) {
    try {
        let key = Buffer.from(ENCRYPT_KEY);
        var decipher = crypto.createDecipheriv('des-cbc', key, key);
        var decryptedString = decipher.update(textToDecode, 'base64', 'ascii');
        decryptedString += decipher.final('ascii');
        return decryptedString;
    } catch (exception) {
        throw exception;
    }
}

function buildSignature(qs, time) {
    console.log('---- buildSignature ---- ');
    console.log(qs + MD5_KEY + time + SECRET_KEY)
    return crypto.createHash('md5').update(qs + MD5_KEY + time + SECRET_KEY).digest("hex");
}

function getUserBalance(username, currency, callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    let method = "LoginRequest";
    let time = moment().utc(true).format('YYYYMMDDHHmmss');


    // GetUserBalance.aspx

    let queryString = querystring.stringify({
        username: method,
        currency: SECRET_KEY
    });

    let desEncrypt = encodeDesCBC(queryString);
    let queryStringEncode = encodeURIComponent(desEncrypt);
    let signature = buildSignature(queryString, time);


    let options = {
        url: API_URL + '?q=' + queryStringEncode + "&s=" + signature,
        method: 'POST',
        headers: headers
    };

    console.log(options);

    request(options, (error, response, body) => {
        // console.log('ressssssss')
        // console.log(response,body,error)
        if (error) {
            callback('', null);
        } else {
            console.log(body)
            // let json = JSON.parse(body);

            // console.log(body)
            callback(null, body)
        }
    });
}

function getBetDetailForTransactionID(date, transactionId,callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    let method = "GetAllBetDetailsForTransactionID";
    let time = moment().utc(true).format('YYYYMMDDHHmmss');
    let dateBet = moment(date).utc(true).format('YYYY-MM-DD');



    let queryString = querystring.stringify({

        method: method,
        Key: SECRET_KEY,
        Time: time,
        TransactionID: transactionId
    });
    console.log(queryString,'<--------')   ;

    let desEncrypt = encodeDesCBC(queryString);
    let queryStringEncode = encodeURIComponent(desEncrypt);
    let signature = buildSignature(queryString,time);



    let options = {
        url: API_URL+ '?q=' +queryStringEncode+"&s="+signature,
        method: 'POST',
        headers: headers
    };

    console.log(options);

    request(options, (error, response, body) => {
        if (error) {
            callback('', null);
        } else {
            callback(null, body)
            console.log(body);
            // let aaa = getGameTypeDetail('bac');
            // console.log(aaa,'eeeee');
            // parseString(body, function (err, result) {

                // if(result === null) {
                //     console.log('errrrrrrr')
                //
                // } else {
                //     let responseMsg = result.GetAllBetDetailsResponse;
                //     console.log(responseMsg,'popopopopopo');
                //
                //
                //     let list = responseMsg.BetDetailList[0].BetDetail;
                //
                //     console.log('====> ', list, ' <======');
                //
                //
                //
                //     list = _.map(list,(item)=>{
                //
                //         if (item.GameType[0] === 'bac') {
                //             item.BetType = getBetTypeDetailBA(item.BetType[0])
                //         } else if (item.GameType[0] === 'dtx') {
                //             item.BetType = getBetTypeDetailDG(item.BetType[0])
                //         }
                //         else if (item.GameType[0] === 'sicbo') {
                //             item.BetType = getBetTypeDetailSB(item.BetType[0])
                //         }
                //         return {
                //             betType : item.BetType,
                //             gameType : item.GameType[0],
                //             betAmount : item.BetAmount[0],
                //             gameID: item.GameID[0],
                //             Balance:item.Balance[0],
                //             Rolling:item.Rolling[0],
                //             ResultAmount:item.ResultAmount[0]
                //
                //         };
                //     });
                //     console.log(list,'=======<');
                //     filterBetID(list, betID, (_responseCallback)=>{
                //         let jsonResponse = {
                //             betDetail: _responseCallback
                //         };
                //
                //         callback(null, result)
                //         // callback(null, body)
                //     })
                // }

            // });
        }
    });
}

function getBetDetail(username,date, betID,callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    let method = "GetAllBetDetailsDV";
    let time = moment().utc(true).format('YYYYMMDDHHmmss');
    let dateBet = moment(date).utc(true).format('YYYY-MM-DD');


    let queryString = querystring.stringify({

        method: method,
        Key: SECRET_KEY,
        Time: time,
        Username: username,
        Date: dateBet
    });
    console.log(queryString, '<--------');

    let desEncrypt = encodeDesCBC(queryString);
    let queryStringEncode = encodeURIComponent(desEncrypt);
    let signature = buildSignature(queryString, time);


    let options = {
        url: API_URL + '?q=' + queryStringEncode + "&s=" + signature,
        method: 'POST',
        headers: headers
    };
    console.log(options);
    request(options, (error, response, body) => {
        if (error) {
            callback('', null);
        } else {
            let aaa = getGameTypeDetail('bac');
            console.log(aaa, 'eeeee');
            parseString(body, function (err, result) {
                console.log(result);

                if (result === null) {
                    console.log('errrrrrrr')

                } else {
                    let responseMsg = result.GetAllBetDetailsResponse;
                    console.log(responseMsg, 'popopopopopo');


                    let list = responseMsg.BetDetailList[0].BetDetail;

                    console.log('====> ', list, ' <======');


                    list = _.map(list, (item) => {

                        if (item.GameType[0] === 'bac') {
                            item.BetType = getBetTypeDetailBA(item.BetType[0])
                        } else if (item.GameType[0] === 'dtx') {
                            item.BetType = getBetTypeDetailDG(item.BetType[0])
                        }
                        else if (item.GameType[0] === 'sicbo') {
                            item.BetType = getBetTypeDetailSB(item.BetType[0])
                        }
                        return {
                            betType: item.BetType,
                            gameType: item.GameType[0],
                            betAmount: item.BetAmount[0],
                            gameID: item.GameID[0],
                            Balance: item.Balance[0],
                            Rolling: item.Rolling[0],
                            ResultAmount: item.ResultAmount[0]

                        };
                    });
                    console.log(list, '=======<');
                    filterBetID(list, betID, (_responseCallback) => {
                        let jsonResponse = {
                            betDetail: _responseCallback
                        };

                        callback(null, jsonResponse)
                        // callback(null, body)
                    })


                }

            });
        }
    });
}


function getBetResult(username, betID, fromTime, callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    let method = "GetUserBetItemDV";
    let time = moment().utc(true).format('YYYYMMDDHHmmss');
    let formDate = moment(fromTime).add(-1, 'day').utc(true).format('YYYY-MM-DD HH:mm:ss');
    let toDate = moment(fromTime).add(+1,'day').utc(true).format('YYYY-MM-DD HH:mm:ss');


    let queryString = querystring.stringify({

        method: method,
        Key: SECRET_KEY,
        Time: time,
        Username: username,
        FromTime: formDate,
        ToTime: toDate,
        Offset: '0'
    });
    // console.log(queryString,'<--------')   ;

    let desEncrypt = encodeDesCBC(queryString);
    let queryStringEncode = encodeURIComponent(desEncrypt);
    let signature = buildSignature(queryString, time);


    let options = {
        url: API_URL + '?q=' + queryStringEncode + "&s=" + signature,
        method: 'POST',
        headers: headers
    };

    console.log(options);

    request(options, (error, response, body) => {
        if (error) {
            callback('', null);
        } else {
            parseString(body, function (err, result) {
                console.log(result, 'yyy');

                if (result === null) {
                    console.log('errrrrrrr')

                } else if (result.GetUserBetItemResponse.ErrorMsgId[0] === '111') {

                    let list = {
                        massage: 'เกินเวลา 7วัน ในการดู',
                        code: 111,
                    }
                    callback(null, list)

                }

                else if (result.GetUserBetItemResponse.ErrorMsgId[0] === '0') {

                    let responseMsg = result.GetUserBetItemResponse;
                    console.log(responseMsg, 'popopopopopo');
                    //
                    // let list = responseMsg.UserBetItemList[0].UserBetItem;
                    let detailResult = responseMsg.UserBetItemList[0].UserBetItem;

                    let detailBetList = responseMsg.UserBetItemList[0].UserBetItem;

                    detailBetList = _.map(detailBetList, (item) => {

                        if (item.GameType[0] === 'bac') {
                            item.BetType = getBetTypeDetailBA(item.BetType[0])
                        } else if (item.GameType[0] === 'dtx') {
                            item.BetType = getBetTypeDetailDG(item.BetType[0])
                        }
                        else if (item.GameType[0] === 'sicbo') {
                            item.BetType = getBetTypeDetailSB(item.BetType[0])
                        }
                        return {
                            betType: item.BetType,
                            gameType: item.GameType[0],
                            betAmount: item.BetAmount[0],
                            gameID: item.GameID[0],
                            Balance: item.Balance[0],
                            Rolling: item.Rolling[0],
                            ResultAmount: item.ResultAmount[0]

                        };
                    });

                    detailResult = _.map(detailResult, (item) => {
                        console.log(item,'<========');
                        if (item.GameType[0] === 'bac') {
                            // try {
                                item.BetType = getBetTypeDetailBA(item.BetType[0]);
                                // getGameCardResultSuit
                                item.GameType = getGameTypeDetail(item.GameType[0]);

                                item.GameResult[0].BaccaratResult[0].BankerCard1[0].Suit = getGameCardResultSuit(item.GameResult[0].BaccaratResult[0].BankerCard1[0].Suit[0]);
                                item.GameResult[0].BaccaratResult[0].BankerCard1[0].Rank = getGameCardResultRank(item.GameResult[0].BaccaratResult[0].BankerCard1[0].Rank[0]);
                                //
                                item.GameResult[0].BaccaratResult[0].BankerCard2[0].Suit = getGameCardResultSuit(item.GameResult[0].BaccaratResult[0].BankerCard2[0].Suit[0]);
                                item.GameResult[0].BaccaratResult[0].BankerCard2[0].Rank = getGameCardResultRank(item.GameResult[0].BaccaratResult[0].BankerCard2[0].Rank[0]);
                                //
                                if(item.GameResult[0].BaccaratResult[0].BankerCard3) {
                                    item.GameResult[0].BaccaratResult[0].BankerCard3[0].Suit = getGameCardResultSuit(item.GameResult[0].BaccaratResult[0].BankerCard3[0].Suit[0]);
                                    item.GameResult[0].BaccaratResult[0].BankerCard3[0].Rank = getGameCardResultRank(item.GameResult[0].BaccaratResult[0].BankerCard3[0].Rank[0]);
                                }
                                item.GameResult[0].BaccaratResult[0].PlayerCard1[0].Suit = getGameCardResultSuit(item.GameResult[0].BaccaratResult[0].PlayerCard1[0].Suit[0]);
                                item.GameResult[0].BaccaratResult[0].PlayerCard1[0].Rank = getGameCardResultRank(item.GameResult[0].BaccaratResult[0].PlayerCard1[0].Rank[0]);


                                item.GameResult[0].BaccaratResult[0].PlayerCard2[0].Suit = getGameCardResultSuit(item.GameResult[0].BaccaratResult[0].PlayerCard2[0].Suit[0]);
                                item.GameResult[0].BaccaratResult[0].PlayerCard2[0].Rank = getGameCardResultRank(item.GameResult[0].BaccaratResult[0].PlayerCard2[0].Rank[0]);

                                if(item.GameResult[0].BaccaratResult[0].PlayerCard3) {
                                    item.GameResult[0].BaccaratResult[0].PlayerCard3[0].Suit = getGameCardResultSuit(item.GameResult[0].BaccaratResult[0].PlayerCard3[0].Suit[0]);
                                    item.GameResult[0].BaccaratResult[0].PlayerCard3[0].Rank = getGameCardResultRank(item.GameResult[0].BaccaratResult[0].PlayerCard3[0].Rank[0]);
                                }
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRBankerNatural = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRBankerNatural[0], 'BRBankerNatural')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRBankerPair = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRBankerPair[0], 'BRBankerPair')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRBankerWin = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRBankerWin[0], 'BRBankerWin')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowBankerNatural = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowBankerNatural[0], 'BRCowBankerNatural')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowBankerPair = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowBankerPair[0], 'BRCowBankerPair')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowBankerWin = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowBankerWin[0], 'BRCowBankerWin')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowPlayerNatural = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowPlayerNatural[0], 'BRCowPlayerNatural')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowPlayerPair = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowPlayerPair[0], 'BRCowPlayerPair')

                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowPlayerWin = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowPlayerWin[0], 'BRCowPlayerWin')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowTie = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowTie[0], 'BRCowTie')

                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerNatural = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerNatural[0], 'BRPlayerNatural')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerPair = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerPair[0], 'BRPlayerPair')

                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerWin = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerWin[0], 'BRPlayerWin')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSBankerNatural = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSBankerNatural[0], 'BRSSBankerNatural')

                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSBankerPair = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSBankerPair[0], 'BRSSBankerPair')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSBankerWin = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSBankerWin[0], 'BRSSBankerWin')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSPlayerNatural = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSPlayerNatural[0], 'BRSSPlayerNatural')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSPlayerPair = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSPlayerPair[0], 'BRSSPlayerPair')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSPlayerWin = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSPlayerWin[0], 'BRSSPlayerWin')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSSuperSix = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSSuperSix[0], 'BRSSSuperSix')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSTie = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSTie[0], 'BRSSTie')
                                item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRTie = getBetTypeResultBA(item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRTie[0], 'BRTie')

                                console.log('try========1',item)



                            if(item.GameResult[0].BaccaratResult[0].BankerCard3 && !item.GameResult[0].BaccaratResult[0].PlayerCard3){
                                return {
                                    // betType : item.BetType,
                                    // // gameType : item.GameType,
                                    // betAmount : item.BetAmount[0],
                                    gameID: item.GameID[0],
                                    // Balance:item.Balance[0],
                                    // Rolling:item.Rolling[0],
                                    // ResultAmount:item.ResultAmount[0],
                                    // // gamedetail:item.GameResult[0],
                                    // gameType: item.GameType,
                                    GameResult: {
                                        ResultDetail: {
                                            BRBankerNatural: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRBankerNatural,
                                            BRBankerPair: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRBankerPair,
                                            BRBankerWin: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRBankerWin,
                                            BRCowBankerNatural: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowBankerNatural,
                                            BRCowBankerPair: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowBankerPair,
                                            BRCowBankerWin: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowBankerWin,
                                            BRCowPlayerNatural: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowPlayerNatural,
                                            BRCowPlayerPair: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowPlayerPair,
                                            BRCowPlayerWin: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowPlayerWin,
                                            BRCowTie: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowTie,
                                            BRPlayerNatural: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerNatural,
                                            BRPlayerPair: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerPair,
                                            BRPlayerWin: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerWin,
                                            BRSSBankerNatural: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSBankerNatural,
                                            BRSSBankerPair: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSBankerPair,
                                            BRSSBankerWin: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSBankerWin,
                                            BRSSPlayerNatural: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSPlayerNatural,
                                            BRSSPlayerPair: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSPlayerPair,
                                            BRSSPlayerWin: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSPlayerWin,
                                            BRSSSuperSix: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSSuperSix,
                                            BRSSTie: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSTie,
                                            BRTie: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRTie
                                        },
                                        BankerCard1: {
                                            Rank: item.GameResult[0].BaccaratResult[0].BankerCard1[0].Rank,
                                            Suit: item.GameResult[0].BaccaratResult[0].BankerCard1[0].Suit
                                        },
                                        BankerCard2: {
                                            Rank: item.GameResult[0].BaccaratResult[0].BankerCard2[0].Rank,
                                            Suit: item.GameResult[0].BaccaratResult[0].BankerCard2[0].Suit
                                        },
                                        BankerCard3:{
                                            Rank:item.GameResult[0].BaccaratResult[0].BankerCard3[0].Rank,
                                            Suit:item.GameResult[0].BaccaratResult[0].BankerCard3[0].Suit
                                        },
                                        PlayerCard1: {
                                            Rank: item.GameResult[0].BaccaratResult[0].PlayerCard1[0].Rank,
                                            Suit: item.GameResult[0].BaccaratResult[0].PlayerCard1[0].Suit
                                        },
                                        PlayerCard2: {
                                            Rank: item.GameResult[0].BaccaratResult[0].PlayerCard2[0].Rank,
                                            Suit: item.GameResult[0].BaccaratResult[0].PlayerCard2[0].Suit
                                        },
                                        // PlayerCard3:{
                                        //     Rank:item.GameResult[0].BaccaratResult[0].PlayerCard3[0].Rank,
                                        //     Suit:item.GameResult[0].BaccaratResult[0].PlayerCard3[0].Suit
                                        // }
                                    }

                                };
                            }else if(item.GameResult[0].BaccaratResult[0].PlayerCard3 && !item.GameResult[0].BaccaratResult[0].BankerCard3) {
                                return {
                                    // betType : item.BetType,
                                    // // gameType : item.GameType,
                                    // betAmount : item.BetAmount[0],
                                    gameID: item.GameID[0],
                                    // Balance:item.Balance[0],
                                    // Rolling:item.Rolling[0],
                                    // ResultAmount:item.ResultAmount[0],
                                    // // gamedetail:item.GameResult[0],
                                    // gameType: item.GameType,
                                    GameResult: {
                                        ResultDetail: {
                                            BRBankerNatural: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRBankerNatural,
                                            BRBankerPair: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRBankerPair,
                                            BRBankerWin: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRBankerWin,
                                            BRCowBankerNatural: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowBankerNatural,
                                            BRCowBankerPair: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowBankerPair,
                                            BRCowBankerWin: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowBankerWin,
                                            BRCowPlayerNatural: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowPlayerNatural,
                                            BRCowPlayerPair: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowPlayerPair,
                                            BRCowPlayerWin: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowPlayerWin,
                                            BRCowTie: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowTie,
                                            BRPlayerNatural: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerNatural,
                                            BRPlayerPair: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerPair,
                                            BRPlayerWin: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerWin,
                                            BRSSBankerNatural: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSBankerNatural,
                                            BRSSBankerPair: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSBankerPair,
                                            BRSSBankerWin: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSBankerWin,
                                            BRSSPlayerNatural: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSPlayerNatural,
                                            BRSSPlayerPair: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSPlayerPair,
                                            BRSSPlayerWin: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSPlayerWin,
                                            BRSSSuperSix: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSSuperSix,
                                            BRSSTie: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSTie,
                                            BRTie: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRTie
                                        },
                                        BankerCard1: {
                                            Rank: item.GameResult[0].BaccaratResult[0].BankerCard1[0].Rank,
                                            Suit: item.GameResult[0].BaccaratResult[0].BankerCard1[0].Suit
                                        },
                                        BankerCard2: {
                                            Rank: item.GameResult[0].BaccaratResult[0].BankerCard2[0].Rank,
                                            Suit: item.GameResult[0].BaccaratResult[0].BankerCard2[0].Suit
                                        },
                                        // BankerCard3:{
                                        //     Rank:item.GameResult[0].BaccaratResult[0].BankerCard3.Rank,
                                        //     Suit:item.GameResult[0].BaccaratResult[0].BankerCard3.Suit
                                        // },
                                        PlayerCard1: {
                                            Rank: item.GameResult[0].BaccaratResult[0].PlayerCard1[0].Rank,
                                            Suit: item.GameResult[0].BaccaratResult[0].PlayerCard1[0].Suit
                                        },
                                        PlayerCard2: {
                                            Rank: item.GameResult[0].BaccaratResult[0].PlayerCard2[0].Rank,
                                            Suit: item.GameResult[0].BaccaratResult[0].PlayerCard2[0].Suit
                                        },
                                        PlayerCard3:{
                                            Rank:item.GameResult[0].BaccaratResult[0].PlayerCard3[0].Rank,
                                            Suit:item.GameResult[0].BaccaratResult[0].PlayerCard3[0].Suit
                                        }
                                    }

                                };
                            }
                            else if(item.GameResult[0].BaccaratResult[0].PlayerCard3 && item.GameResult[0].BaccaratResult[0].BankerCard3){
                                return {
                                    // betType : item.BetType,
                                    // // gameType : item.GameType,
                                    // betAmount : item.BetAmount[0],
                                    gameID: item.GameID[0],
                                    // Balance:item.Balance[0],
                                    // Rolling:item.Rolling[0],
                                    // ResultAmount:item.ResultAmount[0],
                                    // // gamedetail:item.GameResult[0],
                                    // gameType: item.GameType,
                                    GameResult: {
                                        ResultDetail: {
                                            BRBankerNatural: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRBankerNatural,
                                            BRBankerPair: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRBankerPair,
                                            BRBankerWin: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRBankerWin,
                                            BRCowBankerNatural: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowBankerNatural,
                                            BRCowBankerPair: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowBankerPair,
                                            BRCowBankerWin: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowBankerWin,
                                            BRCowPlayerNatural: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowPlayerNatural,
                                            BRCowPlayerPair: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowPlayerPair,
                                            BRCowPlayerWin: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowPlayerWin,
                                            BRCowTie: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowTie,
                                            BRPlayerNatural: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerNatural,
                                            BRPlayerPair: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerPair,
                                            BRPlayerWin: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerWin,
                                            BRSSBankerNatural: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSBankerNatural,
                                            BRSSBankerPair: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSBankerPair,
                                            BRSSBankerWin: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSBankerWin,
                                            BRSSPlayerNatural: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSPlayerNatural,
                                            BRSSPlayerPair: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSPlayerPair,
                                            BRSSPlayerWin: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSPlayerWin,
                                            BRSSSuperSix: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSSuperSix,
                                            BRSSTie: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSTie,
                                            BRTie: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRTie
                                        },
                                        BankerCard1: {
                                            Rank: item.GameResult[0].BaccaratResult[0].BankerCard1[0].Rank,
                                            Suit: item.GameResult[0].BaccaratResult[0].BankerCard1[0].Suit
                                        },
                                        BankerCard2: {
                                            Rank: item.GameResult[0].BaccaratResult[0].BankerCard2[0].Rank,
                                            Suit: item.GameResult[0].BaccaratResult[0].BankerCard2[0].Suit
                                        },
                                        BankerCard3:{
                                            Rank:item.GameResult[0].BaccaratResult[0].BankerCard3[0].Rank,
                                            Suit:item.GameResult[0].BaccaratResult[0].BankerCard3[0].Suit
                                        },
                                        PlayerCard1: {
                                            Rank: item.GameResult[0].BaccaratResult[0].PlayerCard1[0].Rank,
                                            Suit: item.GameResult[0].BaccaratResult[0].PlayerCard1[0].Suit
                                        },
                                        PlayerCard2: {
                                            Rank: item.GameResult[0].BaccaratResult[0].PlayerCard2[0].Rank,
                                            Suit: item.GameResult[0].BaccaratResult[0].PlayerCard2[0].Suit
                                        },
                                        PlayerCard3:{
                                            Rank:item.GameResult[0].BaccaratResult[0].PlayerCard3[0].Rank,
                                            Suit:item.GameResult[0].BaccaratResult[0].PlayerCard3[0].Suit
                                        }
                                    }

                                };
                            }
                            else {
                                return {
                                    // betType : item.BetType,
                                    // // gameType : item.GameType,
                                    // betAmount : item.BetAmount[0],
                                    gameID: item.GameID[0],
                                    // Balance:item.Balance[0],
                                    // Rolling:item.Rolling[0],
                                    // ResultAmount:item.ResultAmount[0],
                                    // // gamedetail:item.GameResult[0],
                                    // gameType: item.GameType,
                                    GameResult: {
                                        ResultDetail: {
                                            BRBankerNatural: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRBankerNatural,
                                            BRBankerPair: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRBankerPair,
                                            BRBankerWin: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRBankerWin,
                                            BRCowBankerNatural: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowBankerNatural,
                                            BRCowBankerPair: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowBankerPair,
                                            BRCowBankerWin: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowBankerWin,
                                            BRCowPlayerNatural: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowPlayerNatural,
                                            BRCowPlayerPair: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowPlayerPair,
                                            BRCowPlayerWin: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowPlayerWin,
                                            BRCowTie: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRCowTie,
                                            BRPlayerNatural: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerNatural,
                                            BRPlayerPair: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerPair,
                                            BRPlayerWin: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerWin,
                                            BRSSBankerNatural: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSBankerNatural,
                                            BRSSBankerPair: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSBankerPair,
                                            BRSSBankerWin: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSBankerWin,
                                            BRSSPlayerNatural: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSPlayerNatural,
                                            BRSSPlayerPair: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSPlayerPair,
                                            BRSSPlayerWin: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSPlayerWin,
                                            BRSSSuperSix: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSSuperSix,
                                            BRSSTie: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRSSTie,
                                            BRTie: item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRTie
                                        },
                                        BankerCard1: {
                                            Rank: item.GameResult[0].BaccaratResult[0].BankerCard1[0].Rank,
                                            Suit: item.GameResult[0].BaccaratResult[0].BankerCard1[0].Suit
                                        },
                                        BankerCard2: {
                                            Rank: item.GameResult[0].BaccaratResult[0].BankerCard2[0].Rank,
                                            Suit: item.GameResult[0].BaccaratResult[0].BankerCard2[0].Suit
                                        },
                                        // BankerCard3:{
                                        //     Rank:item.GameResult[0].BaccaratResult[0].BankerCard3.Rank,
                                        //     Suit:item.GameResult[0].BaccaratResult[0].BankerCard3.Suit
                                        // },
                                        PlayerCard1: {
                                            Rank: item.GameResult[0].BaccaratResult[0].PlayerCard1[0].Rank,
                                            Suit: item.GameResult[0].BaccaratResult[0].PlayerCard1[0].Suit
                                        },
                                        PlayerCard2: {
                                            Rank: item.GameResult[0].BaccaratResult[0].PlayerCard2[0].Rank,
                                            Suit: item.GameResult[0].BaccaratResult[0].PlayerCard2[0].Suit
                                        },
                                        // PlayerCard3:{
                                        //     Rank:item.GameResult[0].BaccaratResult[0].PlayerCard3[0].Rank,
                                        //     Suit:item.GameResult[0].BaccaratResult[0].PlayerCard3[0].Suit
                                        // }
                                    }

                                };
                            }


                            // if(){
                            //
                            // }
                            // BRPlayerWin:item.GameResult[0].BaccaratResult[0].ResultDetail[0].BRPlayerWin[0],
                        }
                        else if (item.GameType[0] === 'dtx') {
                            item.BetType = getBetTypeDetailDG(item.BetType[0])

                            // getGameCardResultSuit
                            item.GameType = getGameTypeDetail(item.GameType[0]);
                            item.GameResult[0].DragonTigerResult[0].DragonCard[0].Suit = getGameCardResultSuit(item.GameResult[0].DragonTigerResult[0].DragonCard[0].Suit[0]);
                            item.GameResult[0].DragonTigerResult[0].TigerCard[0].Rank = getGameCardResultRank(item.GameResult[0].DragonTigerResult[0].TigerCard[0].Rank[0]);

                            item.GameResult[0].DragonTigerResult[0].ResultDetail[0].DTRDragonWin = getBetResultDG(item.GameResult[0].DragonTigerResult[0].ResultDetail[0].DTRDragonWin[0], 'DTRDragonWin')
                            item.GameResult[0].DragonTigerResult[0].ResultDetail[0].DTRTie = getBetResultDG(item.GameResult[0].DragonTigerResult[0].ResultDetail[0].DTRTie[0], 'DTRTie')
                            item.GameResult[0].DragonTigerResult[0].ResultDetail[0].DTRTigerWin = getBetResultDG(item.GameResult[0].DragonTigerResult[0].ResultDetail[0].DTRTigerWin[0], 'DTRTigerWin')

                            return {
                                betType: item.BetType,
                                gameType: item.GameType,
                                betAmount: item.BetAmount[0],
                                gameID: item.GameID[0],
                                Balance: item.Balance[0],
                                Rolling: item.Rolling[0],
                                ResultAmount: item.ResultAmount[0],
                                gamedetail: item.GameResult[0],
                                GameResult: {
                                    ResultDetail: {
                                        DTRDragonWin: item.GameResult[0].DragonTigerResult[0].ResultDetail[0].DTRDragonWin,
                                        DTRTie: item.GameResult[0].DragonTigerResult[0].ResultDetail[0].DTRTie,
                                        DTRTigerWin: item.GameResult[0].DragonTigerResult[0].ResultDetail[0].DTRTigerWin,
                                    },
                                    DragonCard: {
                                        Rank: item.GameResult[0].DragonTigerResult[0].DragonCard[0].Rank[0],
                                        Suit: item.GameResult[0].DragonTigerResult[0].DragonCard[0].Suit[0]
                                    },
                                    TigerCard: {
                                        Rank: item.GameResult[0].DragonTigerResult[0].TigerCard[0].Rank[0],
                                        Suit: item.GameResult[0].DragonTigerResult[0].TigerCard[0].Suit[0]
                                    }
                                }

                            };
                        }
                        else if (item.GameType[0] === 'sicbo') {
                            item.BetType = getBetTypeDetailSB(item.BetType[0])
                            // getGameCardResultSuit
                            item.GameType = getGameTypeDetail(item.GameType[0]);

                            return {
                                betType: item.BetType,
                                gameType: item.GameType,
                                betAmount: item.BetAmount[0],
                                gameID: item.GameID[0],
                                Balance: item.Balance[0],
                                Rolling: item.Rolling[0],
                                ResultAmount: item.ResultAmount[0],
                                gamedetail: item.GameResult[0],
                                GameResult: {
                                    Dice1: item.GameResult[0].SicboResult[0].Dice1[0],
                                    Dice2: item.GameResult[0].SicboResult[0].Dice2[0],
                                    Dice3: item.GameResult[0].SicboResult[0].Dice3[0]

                                }

                            };
                        }

                    });
                    // console.log(list1, '===11111====<');
                    // console.log(list, '=======<');
                    filterBetID(detailBetList,detailResult,betID, (_responseCallback,_responseCallback1) => {
                        // console.log(_responseCallback, '=======<');
                        let jsonResponse = {
                            betDetail: _responseCallback,
                        };
                        let jsonResponse2 = {
                            betResult: _responseCallback1,
                        };
                        console.log(jsonResponse,jsonResponse2, 'vvbvbb');
                        callback(null, jsonResponse,jsonResponse2);
                    });


                    // callback(null,list);
                    // console.log(list,'poippipip');


                }

            });
        }
    });
}

// function getGameTypeBet(obj,betTypeGame,BetType) {
//
//     let list = _.map(obj,(o)=>{
//     if(betTypeGame === 'bac') {
//         o.BetType = getBetTypeDetailBA(o.BetType)
//     } else if(betTypeGame === 'dtx') {
//         o.BetType = getBetTypeDetailDG(o.BetType)
//     }
//
//     });
//
//     callback(list)
// }

function filterBetID(obj,obj2, betID, callback) {
    let detailBetList = _.filter(obj, (l) => {
        return betID === l.gameID
    });
    let detailResult = _.filter(obj2, (n) => {
        return betID === n.gameID
    })[0];

    callback(detailBetList,detailResult)
}


function callLoginRequest(username, limit, callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    let method = "LoginRequest";
    let time = moment().utc(true).format('YYYYMMDDHHmmss');


    let queryString = querystring.stringify({
        method: method,
        Key: SECRET_KEY,
        Time: time,
        Username: username.toLowerCase(),
        CurrencyType: 'THB'
    });

    console.log('sagame queryString', queryString);

    let desEncrypt = encodeDesCBC(queryString);
    let queryStringEncode = encodeURIComponent(desEncrypt);
    let signature = buildSignature(queryString, time);

    let options = {
        url: API_URL + '?q=' + queryStringEncode + "&s=" + signature,
        method: 'POST',
        headers: headers
    };

    console.log(options);
    request(options, (error, response, body) => {
        console.log('sagame callLoginRequest() error ', error);
        console.log('sagame callLoginRequest() response ', response);
        console.log('sagame callLoginRequest() body ', body);
        if (error) {
            callback('', null);
        } else {

            parseString(body, function (err, result) {

                callSetBetLimit(username.toLowerCase(),limit,1,(err,response)=>{
                    console.log(response)
                });

                callSetBetLimit(username.toLowerCase(),limit,2,(err,response)=>{
                    console.log(response)
                });

                callSetBetLimit(username.toLowerCase(),limit,3,(err,response)=>{
                    console.log(response)
                });

                callSetBetLimit(username.toLowerCase(),limit,4,(err,response)=>{
                    console.log(response)
                });

                console.log(result)
                if (!result.LoginRequestResponse) {
                    try {
                        let responseMsg = result.APIResponse;

                        let jsonResponse = {
                            errMsgId: responseMsg.ErrorMsgId[0],
                            errMsg: responseMsg.ErrorMsg[0],
                        }
                        callback(null, jsonResponse)
                    } catch (err) {
                        callback(99, null);
                    }
                } else {
                    let responseMsg = result.LoginRequestResponse;

                    try {
                        let jsonResponse = {
                            errMsgId: responseMsg.ErrorMsgId[0],
                            errMsg: responseMsg.ErrorMsg[0],
                            token: responseMsg.Token[0],
                            displayName: responseMsg.DisplayName[0]
                        }
                        callback(null, jsonResponse)
                    } catch (err) {
                        callback(99, null);
                    }
                }


            });

        }
    });

}


function callLaunchingLiveGameURL(username, gameId, client, token) {

    let x = '';
    if (gameId == '601') {
        x = '';
    } else {
        x = ',defaulttable=' + gameId;
    }

    let queryString = querystring.stringify({
        username: username,
        token: token,
        lobby: LOBBY_CODE,
        lang: 'th',
        // returnurl:'https://sportbook88.com',//optional
        // mobile:client === 'MB',
        mobilepremium: client === 'MB', // Start the client in premium HTML5 mobile version, mobile parameter will be ignored.
        h5web: true,
        options: 'hidemultiplayer=1,hideslot=1' + x
        // GameCode:'', //refer to Section 10​. EGame game code, FishermenGold for Fishermen Gold game
        // Lang:'th',//fishermen gold
        // Mobile:''//fishermen gold
    });

    //bar 601 , sicbo 631 , dragon 640
    // To go into table directly (601~616/630~632/640)

    let url = LOGIN_URL + '?' + queryString;
    return url;

}

function callRegUserInfo(username, callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    let method = "RegUserInfo";
    let time = moment().utc(true).format('YYYYMMDDHHmmss');


    let queryString = querystring.stringify({
        method: method,
        Key: SECRET_KEY,
        Time: time,
        Username: username.toLowerCase(),
        CurrencyType: 'THB'
    });

    let desEncrypt = encodeDesCBC(queryString);
    let queryStringEncode = encodeURIComponent(desEncrypt);
    let signature = buildSignature(queryString, time);

    let options = {
        url: API_URL + '?q=' + queryStringEncode + "&s=" + signature,
        method: 'POST',
        headers: headers
    };

    request(options, (error, response, body) => {
        if (error) {
            callback('', null);
        } else {
            callback(null, body)
        }
    });
}

function callVerifyUsername(username, callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    let method = "VerifyUsername";
    let time = moment().utc(true).format('YYYYMMDDHHmmss');


    let queryString = querystring.stringify({
        method: method,
        Key: SECRET_KEY,
        Time: time,
        Username: username.toLowerCase()
    });

    let desEncrypt = encodeDesCBC(queryString);
    let queryStringEncode = encodeURIComponent(desEncrypt);
    let signature = buildSignature(queryString, time);


    let options = {
        url: API_URL + '?q=' + queryStringEncode + "&s=" + signature,
        method: 'POST',
        headers: headers
    };

    console.log(options);

    request(options, (error, response, body) => {
        // console.log('ressssssss')
        // console.log(response,body,error)
        if (error) {
            callback('', null);
        } else {
            console.log(body)
            // let json = JSON.parse(body);

            // console.log(body)
            callback(null, body)
        }
    });
}

function callGetUserStatusDV(username, callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    let method = "GetUserStatusDV";
    let time = moment().utc(true).format('YYYYMMDDHHmmss');


    let queryString = querystring.stringify({
        method: method,
        Key: SECRET_KEY,
        Time: time,
        Username: username.toLowerCase()
    });

    let desEncrypt = encodeDesCBC(queryString);
    let queryStringEncode = encodeURIComponent(desEncrypt);
    let signature = buildSignature(queryString, time);


    let options = {
        url: API_URL + '?q=' + queryStringEncode + "&s=" + signature,
        method: 'POST',
        headers: headers
    };

    console.log(options);

    request(options, (error, response, body) => {
        // console.log('ressssssss')
        // console.log(response,body,error)
        if (error) {
            callback('', null);
        } else {
            console.log(body)
            // let json = JSON.parse(body);

            // console.log(body)
            callback(null, body)
        }
    });
}

function callQueryBetLimit(callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    let method = "QueryBetLimit";
    let time = moment().utc(true).format('YYYYMMDDHHmmss');


    let queryString = querystring.stringify({
        method: method,
        Key: SECRET_KEY,
        Time: time,
        Currency: 'THB'
    });

    let desEncrypt = encodeDesCBC(queryString);
    let queryStringEncode = encodeURIComponent(desEncrypt);
    let signature = buildSignature(queryString, time);


    let options = {
        url: API_URL + '?q=' + queryStringEncode + "&s=" + signature,
        method: 'POST',
        headers: headers
    };

    console.log(options);

    request(options, (error, response, body) => {
        if (error) {
            callback('', null);
        } else {

            parseString(body, function (err, result) {

                console.log(result)
                let responseMsg = result.QueryBetLimitResponse;

                let jsonResponse = {
                    errMsgId: responseMsg.ErrorMsg[0],
                    errMsg: responseMsg.ErrorMsgId[0],
                    betLimitList: responseMsg.BetLimitList[0]
                }
                callback(null, jsonResponse)
            });
            // callback(null, body)
        }
    });
}

function callGetUserMaxBalance(username, callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    let method = "GetUserMaxBalance";
    let time = moment().utc(true).format('YYYYMMDDHHmmss');


    let queryString = querystring.stringify({
        method: method,
        Key: SECRET_KEY,
        Time: time,
        Username: username.toLowerCase()
    });

    let desEncrypt = encodeDesCBC(queryString);
    let queryStringEncode = encodeURIComponent(desEncrypt);
    let signature = buildSignature(queryString, time);


    let options = {
        url: API_URL + '?q=' + queryStringEncode + "&s=" + signature,
        method: 'POST',
        headers: headers
    };

    console.log(options);

    request(options, (error, response, body) => {
        if (error) {
            callback('', null);
        } else {

            parseString(body, function (err, result) {

                let responseMsg = result.GetUserMaxWinResponse;

                let jsonResponse = {
                    errMsgId: responseMsg.ErrorMsgId[0],
                    // errMsg : responseMsg.ErrorMsg[0],
                    maxBalance: responseMsg.MaxBalance[0]
                }
                callback(null, jsonResponse)
            });
            // callback(null, body)
        }
    });
}

function callSetUserMaxBalance(username, callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    let method = "SetUserMaxBalance";
    let time = moment().utc(true).format('YYYYMMDDHHmmss');


    let queryString = querystring.stringify({
        method: method,
        Key: SECRET_KEY,
        Time: time,
        Username: username.toLowerCase(),
        MaxBalance: 500000
    });

    let desEncrypt = encodeDesCBC(queryString);
    let queryStringEncode = encodeURIComponent(desEncrypt);
    let signature = buildSignature(queryString, time);


    let options = {
        url: API_URL + '?q=' + queryStringEncode + "&s=" + signature,
        method: 'POST',
        headers: headers
    };

    console.log(options);

    request(options, (error, response, body) => {
        if (error) {
            callback('', null);
        } else {

            parseString(body, function (err, result) {

                console.log(result)
                let responseMsg = result.SetUserMaxWinResponse;

                let jsonResponse = {
                    errMsgId: responseMsg.ErrorMsgId[0],
                }
                callback(null, jsonResponse)
            });
            // callback(null, body)
        }
    });
}

function callSetBetLimit(username,limit, type,callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    let method = "SetBetLimit";
    let time = moment().utc(true).format('YYYYMMDDHHmmss');

    // 2199023255552  5-3000
    //128           50-5000
    //2048          100-10000
    //8589934592    200-50000
    //4194304       1000-100000
    //33554432      10000-200000

    let set1 , set2 ,set3 ,set4 ,set5;

    if (limit == 2) {
        set1 = 2199023255552;
        set2 = 2048;
    } else if (limit == 3) {
        set1 = 2199023255552;
        set2 = 2048;
        set3 = 8589934592;
    }
    else if (limit == 4) {
        set1 = 2199023255552;
        set2 = 2048;
        set3 = 8589934592;
        set4 = 4194304;
    } else if (limit == 5) {
        set1 = 2199023255552;
        set2 = 2048;
        set3 = 8589934592;
        set4 = 4194304;
        set5 = 33554432;
    } else {
        set1 = 2199023255552;
    }

    let queryString = querystring.stringify({
        method: method,
        Key: SECRET_KEY,
        Time: time,
        Username: username.toLowerCase(),
        Currency: "THB",
        Set1:set1,
        Set2:set2,
        Set3:set3,
        Set4:set4,
        Set5:set5,
        Gametype: type == 2 ? 'roulette' :type == 3 ? 'moneywheel':type == 4 ? 'squeezebaccarat': ''
    });

    let desEncrypt = encodeDesCBC(queryString);
    let queryStringEncode = encodeURIComponent(desEncrypt);
    let signature = buildSignature(queryString, time);


    let options = {
        url: API_URL + '?q=' + queryStringEncode + "&s=" + signature,
        method: 'POST',
        headers: headers
    };

    console.log(options);

    request(options, (error, response, body) => {
        if (error) {
            callback('', null);
        } else {

            parseString(body, function (err, result) {

                console.log(result)
                let responseMsg = result.SetBetLimitResponse;

                let jsonResponse = {
                    errMsgId: responseMsg.ErrorMsgId[0],
                    // errMsg : responseMsg.ErrorMsg[0],
                    // maxBalance : responseMsg.MaxBalance[0]
                }
                callback(null, jsonResponse)
            });
            // callback(null, body)
        }
    });
}


function callKickUser(username, callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    let method = "KickUser";
    let time = moment().utc(true).format('YYYYMMDDHHmmss');


    let queryString = querystring.stringify({
        method: method,
        Key: SECRET_KEY,
        Time: time,
        Username: username.toLowerCase()
    });

    let desEncrypt = encodeDesCBC(queryString);
    let queryStringEncode = encodeURIComponent(desEncrypt);
    let signature = buildSignature(queryString, time);

    let options = {
        url: API_URL + '?q=' + queryStringEncode + "&s=" + signature,
        method: 'POST',
        headers: headers
    };


    request(options, (error, response, body) => {

        if (error) {
            callback('', null);
        } else {

            parseString(body, function (err, result) {

                if (!result.KickUserResponse) {

                    let responseMsg = result.APIResponse;

                    let jsonResponse = {
                        errMsgId: responseMsg.ErrorMsgId[0],
                        errMsg: responseMsg.ErrorMsg[0],
                    }
                    callback(null, jsonResponse)
                } else {
                    let responseMsg = result.KickUserResponse;

                    let jsonResponse = {
                        errMsgId: responseMsg.ErrorMsgId[0],
                        errMsg: responseMsg.ErrorMsg[0]
                    }
                    callback(null, jsonResponse)
                }

            });

        }
    });
}


module.exports = {
    getGameTypeDetail,
    getGameNameByHostId,
    encodeDesCBC,
    decodeDesCBC,
    buildSignature,
    getBetDetail,
    getBetTypeDetailBA,
    getBetTypeDetailDG,
    callLaunchingLiveGameURL,
    callRegUserInfo,
    callLoginRequest,
    callQueryBetLimit,
    callGetUserMaxBalance,
    callSetUserMaxBalance,
    callKickUser,
    getBetTypeDetailSB,
    callSetBetLimit,
    getBetDetailForTransactionID,
    // getBetResult
    getBetResult
}
