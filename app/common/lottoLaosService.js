const MemberModel = require('../models/member.model');
const request = require('request');
const _ = require('underscore');
const mongoose = require('mongoose');
const LottoGameListModel = require('../models/lottoLaosGameList.model.js');
const async = require("async");
const moment = require('moment');
const querystring = require('querystring');
const crypto = require("crypto");


const getClientKey = require('../controllers/getClientKey');
let {
  AMB_LOTTO_URL = 'https://amblotto.api-hub.com/',
  AMB_LOTTO_CLIENT = 'sportbook88',
  AMB_LOTTO_API_KEY = '38af2640-8cd4-489b-9672-ddf2337782de'
} = getClientKey.getKey();

const CLIENT_NAME         = process.env.CLIENT_NAME          || 'SPORTBOOK88';


function login(username,client,callback) {

    const headers = {
        'Content-Type': 'application/json'
    };

    let data = {
        "api_company_user": AMB_LOTTO_CLIENT,
        "api_customer_user": username,
        "apiKey": AMB_LOTTO_API_KEY,
        "isMobile": client === 'MOBILE'
    };


    let options = {
        url:  `${AMB_LOTTO_URL}member/agent/api/createMember`,
        method: 'POST',
        headers: headers,
        form: data,
        rejectUnauthorized: false,
    };

    console.log(options)

    request(options, (error, response, body) => {

        if (error) {
            callback(error, null);
        } else {

            console.log(body)
            let json = JSON.parse(body);

            console.log(json)

            callback(null, json);

        }
    });
}

function cancelById(betObj, callback) {

    const headers = {
        'Content-Type': 'application/json'
    };


    let cancelBetBody = {
        "gameId": betObj.lotto.laos.gameId,
        "transactionId": betObj.lotto.laos.txId,
        "customerUsername": betObj.memberId.username,
        "agentUsername": "ambth",
        "tran": {
            "type": betObj.lotto.laos.type,
            "betType": betObj.lotto.laos.betType,
            "betNumber": betObj.lotto.laos.betNumber,
            "betValue": betObj.lotto.laos.betAmount
        },
        "apiKey": AMB_LOTTO_API_KEY
    };



    const option = {
        url: `${AMB_LOTTO_URL}lao_transaction/api/callback/cancelById`,
        method: 'POST',
        headers: headers,
        body: JSON.stringify(cancelBetBody),
        rejectUnauthorized: false
    };

    console.log(option)

    request(option, (err, response, body) => {
        if (err) {
            callback(err, null);
            // return res.send({code: 999, message: 'Login slot xo fail.', result: err})
        } else {

            console.log(body)
            const responseBody = JSON.parse(body);

            try {
                if (responseBody.code === 0) {

                    callback(null, responseBody);

                } else {
                    callback(responseBody, null);
                }
            } catch (e) {
                callback(e, null);
            }


        }
    });
}


function cancelAll(betObjList, callback) {

    const headers = {
        'Content-Type': 'application/json'
    };


    let trans = _.map(betObjList, item => {
        return {
            "type": item.lotto.laos.type,
            "betType": item.lotto.laos.betType,
            "betNumber": item.lotto.laos.betNumber,
            "betValue": item.lotto.laos.betAmount
        };
    });


    let cancelBetBody = {
        "gameId": betObjList[0].lotto.laos.gameId,
        "betId": betObjList[0].lotto.laos.betId,
        "customerUsername": betObjList[0].memberId.username,
        "agentUsername": "ambth",
        "trans": trans,
        "apiKey": AMB_LOTTO_API_KEY
    };



    const option = {
        url: `${AMB_LOTTO_URL}lao_transaction/api/callback/cancelAll`,
        method: 'POST',
        headers: headers,
        body: JSON.stringify(cancelBetBody),
        rejectUnauthorized: false
    };
    console.log(option)

    request(option, (err, response, body) => {
        if (err) {
            // return res.send({code: 999, message: 'Login slot xo fail.', result: err})
        } else {

            console.log('response body : ',body)
            const responseBody = JSON.parse(body);

            try {
                if (responseBody.code === 0) {

                    callback(null, responseBody);

                } else {
                    callback(responseBody, null);
                }
            } catch (e) {
                callback(e, null);
            }


        }
    });
}


function getGameList(callback) {


    LottoGameListModel.find({}, (err, response) => {

        let gameList = _.map(response, item => {
            return {
                id: item.id,
                date: item.day + '/' + item.month + '/' + item.year
            }
        });

        callback(null, gameList)

    }).limit(5).sort({'createdDate':-1});

}


function getGameById(id,callback) {


    LottoGameListModel.findById(mongoose.Types.ObjectId(id), (err, response) => {
       if(err){
           callback(err,null);
       }else{
           callback(null, response)
       }
    });

}

function getGameByGameId(id,callback) {


    LottoGameListModel.findOne({id:mongoose.Types.ObjectId(id)}, (err, response) => {
        if(err){
            callback(err,null);
        }else{
            callback(null, response)
        }
    });

}


function getLottoCommission(agentObj, betType) {
    return getLottoLimit(agentObj, betType) ? getLottoLimit(agentObj, betType).discount : null;
}


function getLottoPayout(agentObj, betType) {
    return getLottoLimit(agentObj, betType) ? getLottoLimit(agentObj, betType).payout : null;
}

function getLottoLimit(agentObj, betType) {
    switch (betType) {
        case 'L_4TOP' :
            return agentObj.limitSetting.lotto.laos.L_4TOP;
        case 'L_4TOD' :
            return agentObj.limitSetting.lotto.laos.L_4TOD;
        case 'L_3TOP' :
            return agentObj.limitSetting.lotto.laos.L_3TOP;
        case 'L_3TOD' :
            return agentObj.limitSetting.lotto.laos.L_3TOD;
        case 'L_2FB' :
            return agentObj.limitSetting.lotto.laos.L_2FB;
        case 'T_4TOP' :
            return agentObj.limitSetting.lotto.laos.T_4TOP;
        case 'T_4TOD' :
            return agentObj.limitSetting.lotto.laos.T_4TOD;
        case 'T_3TOP' :
            return agentObj.limitSetting.lotto.laos.T_3TOP;
        case 'T_3TOD' :
            return agentObj.limitSetting.lotto.laos.T_3TOD;
        case 'T_2TOP' :
            return agentObj.limitSetting.lotto.laos.T_2TOP;
        case 'T_2TOD' :
            return agentObj.limitSetting.lotto.laos.T_2TOD;
        case 'T_2BOT' :
            return agentObj.limitSetting.lotto.laos.T_2BOT;
        case 'T_1TOP' :
            return agentObj.limitSetting.lotto.laos.T_1TOP;
        case 'T_1BOT' :
            return agentObj.limitSetting.lotto.laos.T_1BOT;
        default :
            return null;
    }
}

function prepareBetType(betType) {

    switch (betType){
        case "_1TOP":
            return "1 ตัวบน";
        case "_1BOT":
            return "1 ตัวล่าง";
        case "_2TOP":
            return "2 ตัวบน";
        case "_2BOT":
            return "2 ตัวล่าง";
        case "_2TOD":
            return "2 ตัวโต๊ด";
        case "_2TOP_OE":
            return "2 ตัวบนคี่คู่";
        case "_2TOP_OU":
            return "2 ตัวบนสูงต่ำ";
        case "_2BOT_OE":
            return "2 ตัวล่างคี่คู่";
        case "_2BOT_OU":
            return "2 ตัวล่างสูงต่ำ";
        case "_3TOP":
            return "3 ตัวบน";
        case "_3BOT":
            return "3 ตัวล่าง";
        case "_3TOD":
            return "3 ตัวโต๊ด";
        case "_3TOP_OE":
            return "3 ตัวบนคี่คู่";
        case "_3TOP_OU":
            return "3 ตัวบนสูงต่ำ";
        case "_4TOP":
            return "4 ตัวบน";
        case "_4TOD":
            return "4 ตัวโต๊ด";
        case "_5TOP":
            return "5 ตัวบน";
        case "_5TOD":
            return "5 ตัวโต๊ด";
        case "_6TOP":
            return "6 ตัวบน";
        case "_6TOD":
            return "6 ตัวโต๊ด";
        default:
            return betType;
    }
}

function sortTodNumber(text) {
    return text.split('').sort().join('');
}

function generateTodList(numberString) {

    let a = permutator(numberString.split(''))
    return arrayToString(a);

}

function convertDateStr(dateObj) {
    let startYear = Number.parseInt(dateObj.getUTCFullYear());
    let startMonth = Number.parseInt(dateObj.getUTCMonth());
    let startDay = Number.parseInt(dateObj.getUTCDate());

    return moment([startYear, startMonth, startDay]).utc(true)
        .second(dateObj.getUTCSeconds()).minute(dateObj.getUTCMinutes()).hour(dateObj.getUTCHours()).format('DD-MM-YYYY HH:mm');
}


function permutator(inputArr) {
    var results = [];

    function permute(arr, memo) {
        var cur, memo = memo || [];

        for (var i = 0; i < arr.length; i++) {
            cur = arr.splice(i, 1);
            if (arr.length === 0) {
                results.push(memo.concat(cur));
            }
            permute(arr.slice(), memo.concat(cur));
            arr.splice(i, 0, cur[0]);
        }

        return results;
    }

    return permute(inputArr);
}

function arrayToString(arr) {

    var tmp = []

    for(var i=0; i<arr.length; i++){


        var str = "";

        for(var x=0; x<arr[i].length; x++){

            str = str+arr[i][x];

        }

        tmp.push(str);
    }

    return tmp;
}

function isTOD(betType) {
    return _.contains(['L_4TOD','L_3TOD','T_4TOD','T_3TOD','T_2TOD'],betType);
}


module.exports = {
    getLottoCommission,
    getLottoPayout,
    getLottoLimit,
    getGameList,
    cancelById,
    getGameByGameId,
    cancelAll,
    login,
    getGameById,
    prepareBetType,
    convertDateStr,
    sortTodNumber,
    generateTodList,
    isTOD
}