const config = require('config');
const jwt = require('jsonwebtoken');
const async = require("async");

const BetTransactionModel = require('../models/betTransaction.model.js');
const mongoose = require('mongoose');
const DateUtils = require('../common/dateUtils');
const moment = require('moment');
const _ = require('underscore');

const MemberModel = require('../models/member.model');

function getWinLoseReportMember(memberId, dateFrom, callback) {


    async.parallel([callback => {


        async.parallel([callback => {

            getMemberOutstanding(memberId,dateFrom,'FOOTBALL',(err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            });

        },callback => {
            getMemberOutstanding(memberId,dateFrom,'STEP',(err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            });

        },callback => {
            getMemberOutstanding(memberId,dateFrom,'PARLAY',(err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            });

        },callback => {
            getMemberOutstanding(memberId,dateFrom,'GAME',(err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            });

        },callback => {
            getMemberOutstanding(memberId,dateFrom,'CASINO',(err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            });

        },callback => {
            getMemberOutstanding(memberId,dateFrom,'LOTTO',(err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            });

        },callback => {
            getMemberOutstanding(memberId,dateFrom,'M2',(err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            });

        },callback => {
            getMemberOutstanding(memberId,dateFrom,'MULTI_PLAYER',(err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            });

        }], (err, results) => {

            if(err){
                callback(err, null);
            }else {
                callback(null,_.indexBy(results, 'game'));
            }

        });

    },callback => {

        async.parallel([callback => {

            getMemberWinLose(memberId,dateFrom,'FOOTBALL',(err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            });

        },callback => {
            getMemberWinLose(memberId,dateFrom,'STEP',(err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            });

        },callback => {
            getMemberWinLose(memberId,dateFrom,'PARLAY',(err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            });

        },callback => {
            getMemberWinLose(memberId,dateFrom,'GAME',(err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            });

        },callback => {
            getMemberWinLose(memberId,dateFrom,'CASINO',(err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            });

        },callback => {
            getMemberWinLose(memberId,dateFrom,'LOTTO',(err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            });

        },callback => {
            getMemberWinLose(memberId,dateFrom,'M2',(err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            });

        },callback => {
            getMemberWinLose(memberId,dateFrom,'MULTI_PLAYER',(err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            });

        }], (err, results) => {

            if(err){
                callback(err, null);
            }else {
                callback(null,_.indexBy(results, 'game'));
            }

        });
    }],(err,results) => {

        if(err){
            callback(err, null);
        }else {

            let outstanding = results[0];
            let prepareResults = results[1];

            prepareResults.FOOTBALL.outstanding = outstanding.FOOTBALL.amount;
            prepareResults.STEP.outstanding = outstanding.STEP.amount;
            prepareResults.PARLAY.outstanding = outstanding.PARLAY.amount;
            prepareResults.GAME.outstanding = outstanding.GAME.amount;
            prepareResults.CASINO.outstanding = outstanding.CASINO.amount;
            prepareResults.LOTTO.outstanding = outstanding.LOTTO.amount;
            prepareResults.M2.outstanding = outstanding.M2.amount;
            prepareResults.MULTI_PLAYER.outstanding = outstanding.MULTI_PLAYER.amount;

            callback(null, prepareResults);
        }
    });




}

function getMemberWinLose(memberId, dateFrom, game, callback) {

    let condition = {};
    condition['$and'] = [
        {
            'settleDate': {
                "$gte": dateFrom
            }
        },
        {'memberId': mongoose.Types.ObjectId(memberId)},
        {'status': {$in: ['DONE', 'REJECTED', 'CANCELLED']}}
    ];

    if (game === 'FOOTBALL') {
        condition.$and.push({'game': 'FOOTBALL'});
        condition.$and.push({'gameType': 'TODAY'});
    } else if (game === 'STEP') {
        condition.$and.push({'game': 'FOOTBALL'});
        condition.$and.push({'gameType': 'MIX_STEP'});
    } else if (game === 'PARLAY') {
        condition.$and.push({'game': 'FOOTBALL'});
        condition.$and.push({'gameType': 'PARLAY'});
    } else if (game === 'CASINO') {
        condition.$and.push({'game': 'CASINO'});
    } else if (game === 'GAME') {
        condition.$and.push({'game': 'GAME'});
    } else if (game === 'LOTTO') {
        condition.$and.push({'game': 'LOTTO'});
    } else if (game === 'M2') {
        condition.$and.push({'game': 'M2'});
    } else if (game === 'MULTI_PLAYER') {
        condition.$and.push({'game': 'MULTI_PLAYER'});
    }

    BetTransactionModel.aggregate([
        {
            $match: condition
        },
        {
            "$group": {
                "_id": {
                    game: '$game'
                },
                "amount": {$sum: '$amount'},
                "validAmount": {$sum: '$validAmount'},
                "wlTurnAmount": {$sum: {$abs: '$commission.member.totalWinLoseCom'}},
            }
        },
        {
            $project: {
                _id: 0,
                game: game,
                amount: '$amount',
                validAmount: '$validAmount',
                wlTurnAmount: '$wlTurnAmount',

            }
        }
    ]).read('secondaryPreferred').exec((err, results) => {

        if (err) {
            callback(err, null);
        } else {

            let data = results && results[0] ? results[0] :
                {
                    game: game,
                    amount: 0,
                    validAmount: 0,
                    wlTurnAmount: 0,
                };

            callback(null, data);
        }
    });
}

function getWinLoseReportMember2(username, dateFrom,dateTo,timeFrom ,timeTo, callback) {


    MemberModel.findOne({username_lower: username.toLowerCase()})
        .exec( (err, memberResponse )=> {
            if (err) {
                callback(err, null);
            } else {

                if(memberResponse) {

                    let memberId = memberResponse._id;

                    async.parallel([callback => {

                        getMemberWinLose2(memberId, dateFrom, dateTo, timeFrom ,timeTo,'FOOTBALL', (err, response) => {
                            if (err) {
                                callback(err, null);
                            } else {
                                callback(null, response);
                            }
                        });

                    }, callback => {
                        getMemberWinLose2(memberId, dateFrom, dateTo,timeFrom ,timeTo,'STEP', (err, response) => {
                            if (err) {
                                callback(err, null);
                            } else {
                                callback(null, response);
                            }
                        });

                    }, callback => {
                        getMemberWinLose2(memberId, dateFrom, dateTo,timeFrom ,timeTo,'PARLAY', (err, response) => {
                            if (err) {
                                callback(err, null);
                            } else {
                                callback(null, response);
                            }
                        });

                    }, callback => {
                        getMemberWinLose2(memberId, dateFrom, dateTo,timeFrom ,timeTo,'GAME', (err, response) => {
                            if (err) {
                                callback(err, null);
                            } else {
                                callback(null, response);
                            }
                        });

                    }, callback => {
                        getMemberWinLose2(memberId, dateFrom, dateTo,timeFrom ,timeTo,'CASINO', (err, response) => {
                            if (err) {
                                callback(err, null);
                            } else {
                                callback(null, response);
                            }
                        });

                    }, callback => {
                        getMemberWinLose2(memberId, dateFrom, dateTo,timeFrom ,timeTo,'LOTTO', (err, response) => {
                            if (err) {
                                callback(err, null);
                            } else {
                                callback(null, response);
                            }
                        });

                    }, callback => {
                        getMemberWinLose2(memberId, dateFrom,dateTo, timeFrom ,timeTo,'M2', (err, response) => {
                            if (err) {
                                callback(err, null);
                            } else {
                                callback(null, response);
                            }
                        });

                    }, callback => {
                        getMemberWinLose2(memberId, dateFrom,dateTo, timeFrom ,timeTo,'MULTI_PLAYER', (err, response) => {
                            if (err) {
                                callback(err, null);
                            } else {
                                callback(null, response);
                            }
                        });

                    }], (err, results) => {

                        if (err) {
                            callback(err, null);
                        } else {

                            callback(null, _.indexBy(results, 'game'));
                        }
                    });
                }else {
                    callback(1004, null);
                }
            }
        });



}

function getMemberWinLose2(memberId, dateFrom,dateTo, timeFrom ,timeTo,game, callback) {


    let now = moment(dateFrom, 'DD/MM/YYYY').second(timeFrom.split(':')[2]).minute(timeFrom.split(':')[1]).hour(timeFrom.split(':')[0]).utc(true).toDate();
    let end = moment(dateTo, 'DD/MM/YYYY').second(timeTo.split(':')[2]).minute(timeTo.split(':')[1]).hour(timeTo.split(':')[0]).utc(true).toDate();



    let condition = {};
    condition['$and'] = [
        {
            'settleDate': {
                "$gte": now,
                "$lt": end
            }
        },
        {'memberId': mongoose.Types.ObjectId(memberId)},
        {'status': {$in: ['DONE', 'REJECTED', 'CANCELLED']}}
    ];

    if (game === 'FOOTBALL') {
        condition.$and.push({'game': 'FOOTBALL'});
        condition.$and.push({'gameType': 'TODAY'});
    } else if (game === 'STEP') {
        condition.$and.push({'game': 'FOOTBALL'});
        condition.$and.push({'gameType': 'MIX_STEP'});
    } else if (game === 'PARLAY') {
        condition.$and.push({'game': 'FOOTBALL'});
        condition.$and.push({'gameType': 'PARLAY'});
    } else if (game === 'CASINO') {
        condition.$and.push({'game': 'CASINO'});
    } else if (game === 'GAME') {
        condition.$and.push({'game': 'GAME'});
    } else if (game === 'LOTTO') {
        condition.$and.push({'game': 'LOTTO'});
    } else if (game === 'M2') {
        condition.$and.push({'game': 'M2'});
    } else if (game === 'MULTI_PLAYER') {
        condition.$and.push({'game': 'MULTI_PLAYER'});
    }

    BetTransactionModel.aggregate([
        {
            $match: condition
        },
        {
            "$group": {
                "_id": {
                    game: '$game'
                },
                "amount": {$sum: '$amount'},
                "count":{$sum:1},
                "validAmount": {$sum: '$validAmount'},
                "wlAmount":{$sum: '$commission.member.winLose'},
                "wlCom":{$sum: '$commission.member.winLoseCom'},
                "totalWlCom": {$sum: '$commission.member.totalWinLoseCom'},
            }
        },
        {
            $project: {
                _id: 0,
                game: game,
                amount: '$amount',
                count: '$count',
                validAmount: '$validAmount',
                wlAmount: '$wlAmount',
                wlCom:'$wlCom',
                totalWlCom: '$totalWlCom',

            }
        }
    ])
        .exec((err, results) => {

            if (err) {
                callback(err, null);
            } else {

                let data = results && results[0] ? results[0] :
                    {
                        game: game,
                        amount: 0,
                        count:0,
                        validAmount: 0,
                        wlAmount: 0,
                        wlCom: 0,
                        totalWlCom:0
                    };

                callback(null, data);
            }
        });
}

function getMemberOutstanding(memberId, dateFrom, game, callback) {

    let condition = {};
    condition['$and'] = [
        {
            'createdDate': {
                $gte: moment().add(-7, 'd').utc(true).toDate()
            }
        },
        {'memberId': mongoose.Types.ObjectId(memberId)},
        {'status': {$in: ['RUNNING', 'WAITING']}}
    ];

    if (game === 'FOOTBALL') {
        condition.$and.push({'game': 'FOOTBALL'});
        condition.$and.push({'gameType': 'TODAY'});
    } else if (game === 'STEP') {
        condition.$and.push({'game': 'FOOTBALL'});
        condition.$and.push({'gameType': 'MIX_STEP'});
    } else if (game === 'PARLAY') {
        condition.$and.push({'game': 'FOOTBALL'});
        condition.$and.push({'gameType': 'PARLAY'});
    } else if (game === 'CASINO') {
        condition.$and.push({'game': 'CASINO'});
    } else if (game === 'GAME') {
        condition.$and.push({'game': 'GAME'});
    } else if (game === 'LOTTO') {
        condition.$and.push({'game': 'LOTTO'});
    } else if (game === 'M2') {
        condition.$and.push({'game': 'M2'});
    } else if (game === 'MULTI_PLAYER') {
        condition.$and.push({'game': 'MULTI_PLAYER'});
    }

    BetTransactionModel.aggregate([
        {
            $match: condition
        },
        {
            "$group": {
                "_id": {
                    game: '$game'
                },
                "amount": {$sum: '$amount'},
                "validAmount": {$sum: '$validAmount'}
            }
        },
        {
            $project: {
                _id: 0,
                game: game,
                amount: '$amount',
                validAmount: '$validAmount'

            }
        }
    ]).read('secondaryPreferred').exec((err, results) => {

        if (err) {
            callback(err, null);
        } else {

            let data = results && results[0] ? results[0] :
                {
                    game: game,
                    amount: 0,
                    validAmount: 0
                };

            callback(null, data);
        }
    });
}

module.exports = {
    getWinLoseReportMember,
    getWinLoseReportMember2
};
