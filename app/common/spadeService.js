const MemberModel = require('../models/member.model');
const request = require('request');
const _ = require('underscore');
const mongoose = require('mongoose');
const async = require("async");
const moment = require('moment');
const querystring = require('querystring');
const crypto = require("crypto");
const uuidv4 = require('uuid/v4');

const jwt = require('jsonwebtoken');

const getClientKey = require('../controllers/getClientKey');
let {
    SPADE_LOGIN_URL = 'http://test.bikimex.com:10023',
    SPADE_API_URL = 'https://api-egame-staging.sgplay.net/api',
    SPADE_MERCHANT_CODE = 'AKMEBET',
    SPADE_IMAGE_URL = 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/spade-game'
} = getClientKey.getKey();


function generateSerialNo() {
    return uuidv4();
}

function getRequestBody(req) {
    return jwt.verify(req.header('authorization').split(' ')[1], AMEBA_SECRET_KEY)
}

function getMerchantCode() {
    console.log('merchant code =========== ', SPADE_MERCHANT_CODE)
    return SPADE_MERCHANT_CODE;
}


function callGetGameList() {

    // FISHING	F-SF01 	Fishing God
    // Slot	S-CH01	Mr Chu Tycoon
    // Slot	S-GK01	Brothers kingdom
    // Slot	S-PG01	Prosperity Gods
    // Slot	S-CP01	Candy Pop
    // Slot	S-GF01	Golden Fist
    // Slot	S-LY02	FaFaFa2
    // Slot	S-GA01 	Gangster Axe


    let gameList = [
        // {
        //     Type: 'Slots',
        //     GameId: 'S-MM01',
        //     GameName: getGameName('S-MM01'),
        //     ImageUrl: SPADE_IMAGE_URL+'/S-MM01.jpg'
        // },
        // {
        //     Type: 'Slots',
        //     GameId: 'S-DE01',
        //     GameName: getGameName('S-DE01'),
        //     ImageUrl: SPADE_IMAGE_URL+'/S-DE01.jpg'
        // },
        // {
        //     Type: 'Slots',
        //     GameId: 'S-BA01',
        //     GameName: getGameName('S-BA01'),
        //     ImageUrl: SPADE_IMAGE_URL+'/S-BA01.jpg'
        // },
        // {
        //     Type: 'Slots',
        //     GameId: 'S-LS02',
        //     GameName: getGameName('S-LS02'),
        //     ImageUrl: SPADE_IMAGE_URL+'/S-LS02.jpg'
        // },
        {
            Type: 'Slots',
            GameId: 'S-HE01',
            GameName: getGameName('S-HE01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-HE01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-SB01',
            GameName: getGameName('S-SB01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-SB01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-DF02',
            GameName: getGameName('S-DF02'),
            ImageUrl: SPADE_IMAGE_URL+'/S-DF02.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-ML01',
            GameName: getGameName('S-ML01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-ML01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-TP02',
            GameName: getGameName('S-TP02'),
            ImageUrl: SPADE_IMAGE_URL+'/S-TP02.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-GP01',
            GameName: getGameName('S-GP01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-GP01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-CH01',
            GameName: getGameName('S-CH01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-CH01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-GK01',
            GameName: getGameName('S-GK01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-GK01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-PG01',
            GameName: getGameName('S-PG01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-PG01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-CP01',
            GameName: getGameName('S-CP01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-CP01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-GF01',
            GameName: getGameName('S-GF01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-GF01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-LY02',
            GameName: getGameName('S-LY02'),
            ImageUrl: SPADE_IMAGE_URL+'/S-LY02.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-GA01',
            GameName: getGameName('S-GA01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-GA01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-PW02',
            GameName: getGameName('S-PW02'),
            ImageUrl:SPADE_IMAGE_URL+ '/S-PW02.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-WP02',
            GameName: getGameName('S-WP02'),
            ImageUrl: SPADE_IMAGE_URL+'/S-WP02.jpg',
            Provider:"Spade"
        },
        // {
        //     Type: 'Slots',
        //     GameId: 'S-DM01',
        //     GameName: getGameName('S-DM01'),
        //     ImageUrl: SPADE_IMAGE_URL+'/S-DM01.jpg'
        // },
        {
            Type: 'Slots',
            GameId: 'S-FL02',
            GameName: getGameName('S-FL02'),
            ImageUrl: SPADE_IMAGE_URL+'/S-FL02.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-FM02',
            GameName: getGameName('S-FM02'),
            ImageUrl:SPADE_IMAGE_URL+'/S-FM02.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-PP01',
            GameName: getGameName('S-PP01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-PP01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-SH01',
            GameName: getGameName('S-SH01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-SH01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-TZ01',
            GameName: getGameName('S-TZ01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-TZ01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-FG01',
            GameName: getGameName('S-FG01'),
            ImageUrl:SPADE_IMAGE_URL+ '/S-FG01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-TW01',
            GameName: getGameName('S-TW01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-TW01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-NT01',
            GameName: getGameName('S-NT01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-NT01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-ZE01',
            GameName: getGameName('S-ZE01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-ZE01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-HY01',
            GameName: getGameName('S-HY01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-HY01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-GC03',
            GameName: getGameName('S-GC03'),
            ImageUrl: SPADE_IMAGE_URL+'/S-GC03.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-DG04',
            GameName: getGameName('S-DG04'),
            ImageUrl: SPADE_IMAGE_URL+'/S-DG04.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-EG03',
            GameName: getGameName('S-EG03'),
            ImageUrl: SPADE_IMAGE_URL+'/S-EG03.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-FC03',
            GameName: getGameName('S-FC03'),
            ImageUrl:SPADE_IMAGE_URL+'/S-FC03.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-GS04',
            GameName: getGameName('S-GS04'),
            ImageUrl: SPADE_IMAGE_URL+'/S-GS04.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-IL03',
            GameName: getGameName('S-IL03'),
            ImageUrl: SPADE_IMAGE_URL+'/S-IL03.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-WC03',
            GameName: getGameName('S-WC03'),
            ImageUrl: SPADE_IMAGE_URL+'/S-WC03.jpg',
            Provider:"Spade"
        },
        // {
        //     Type: 'Slots',
        //     GameId: 'S-IM03',
        //     GameName:getGameName('S-IM03'),
        //     ImageUrl: SPADE_IMAGE_URL+'/S-IM03.jpg'
        // },
        // {
        //     Type: 'Slots',
        //     GameId: 'S-LE03',
        //     GameName: getGameName('S-LE03'),
        //     ImageUrl: SPADE_IMAGE_URL+'/S-LE03.jpg'
        // },
        // {
        //     Type: 'Slots',
        //     GameId: 'S-LH03',
        //     GameName: getGameName('S-LH03'),
        //     ImageUrl: SPADE_IMAGE_URL+'/S-LH03.jpg'
        // },
        // {
        //     Type: 'Slots',
        //     GameId: 'S-SG04',
        //     GameName: getGameName('S-SG04'),
        //     ImageUrl: SPADE_IMAGE_URL+'/S-SG04.jpg'
        // },
        // {
        //     Type: 'Slots',
        //     GameId: 'S-SP03',
        //     GameName: getGameName('S-SP03'),
        //     ImageUrl: SPADE_IMAGE_URL+'/S-SP03.jpg'
        // },
        {
            Type: 'Slots',
            GameId: 'S-WM03',
            GameName: getGameName('S-WM03'),
            ImageUrl: SPADE_IMAGE_URL+'/S-WM03.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-BC01',
            GameName: getGameName('S-BC01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-BC01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-FO01',
            GameName: getGameName('S-FO01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-FO01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-GL02',
            GameName: getGameName('S-GL02'),
            ImageUrl: SPADE_IMAGE_URL+'/S-GL02.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-MR01',
            GameName: getGameName('S-MR01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-MR01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-PH02',
            GameName: getGameName('S-PH02'),
            ImageUrl: SPADE_IMAGE_URL+'/S-PH02.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-PO01',
            GameName: getGameName('S-PO01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-PO01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-WP01',
            GameName: getGameName('S-WP01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-WP01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-AL01',
            GameName: getGameName('S-AL01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-AL01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-CS01',
            GameName: getGameName('S-CS01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-LE02.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-DF01',
            GameName: getGameName('S-DF01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-LE02.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-FD01',
            GameName: getGameName('S-FD01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-FD01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-GO01',
            GameName: getGameName('S-GO01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-GO01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-IL02',
            GameName: getGameName('S-IL02'),
            ImageUrl: SPADE_IMAGE_URL+'/S-IL02.jpg',
            Provider:"Spade"
        },
        // {
        //     Type: 'Slots',
        //     GameId: 'S-IM02',
        //     GameName: getGameName('S-IM02'),
        //     ImageUrl: SPADE_IMAGE_URL+'/S-IM02.jpg'
        // },
        // {
        //     Type: 'Slots',
        //     GameId: 'S-LE02',
        //     GameName: getGameName('S-LE02'),
        //     ImageUrl:SPADE_IMAGE_URL+ '/S-LE02.jpg'
        // },
        {
            Type: 'Slots',
            GameId: 'S-LK01',
            GameName: getGameName('S-LK01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-LK01.jpg',
            Provider:"Spade"
        },
        // {
        //     Type: 'Slots',
        //     GameId: 'S-SM01',
        //     GameName: getGameName('S-SM01'),
        //     ImageUrl: SPADE_IMAGE_URL+'/S-SM01.jpg'
        // },
        // {
        //     Type: 'Slots',
        //     GameId: 'S-SM02',
        //     GameName: getGameName('S-SM02'),
        //     ImageUrl: SPADE_IMAGE_URL+'/S-SM02.jpg'
        // },
        {
            Type: 'Slots',
            GameId: 'S-AT02',
            GameName: getGameName('S-AT02'),
            ImageUrl: SPADE_IMAGE_URL+'/S-AT02.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-CM01',
            GameName: getGameName('S-CM01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-CM01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-DG03',
            GameName: getGameName('S-DG03'),
            ImageUrl: SPADE_IMAGE_URL+'/S-DG03.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-EG02',
            GameName: getGameName('S-EG02'),
            ImageUrl: SPADE_IMAGE_URL+'/S-EG02.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-FC02',
            GameName: getGameName('S-FC02'),
            ImageUrl: SPADE_IMAGE_URL+'/S-FC02.jpg',
            Provider:"Spade"
        },
        // {
        //     Type: 'Slots',
        //     GameId: 'S-GC02',
        //     GameName: getGameName('S-GC02'),
        //     ImageUrl: SPADE_IMAGE_URL+'/S-GC02.jpg'
        // },
        // {
        //     Type: 'Slots',
        //     GameId: 'S-GG01',
        //     GameName: getGameName('S-GG01'),
        //     ImageUrl: SPADE_IMAGE_URL+'/S-GG01.jpg'
        // },
        {
            Type: 'Slots',
            GameId: 'S-GS03',
            GameName: getGameName('S-GS03'),
            ImageUrl:SPADE_IMAGE_URL+'/S-GS03.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-HH01',
            GameName: getGameName('S-HH01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-HH01.jpg',
            Provider:"Spade"
        },
        // {
        //     Type: 'Slots',
        //     GameId: 'S-HL01',
        //     GameName: getGameName('S-HL01'),
        //     ImageUrl: SPADE_IMAGE_URL+'/S-HL01.jpg'
        // },
        // {
        //     Type: 'Slots',
        //     GameId: 'S-JF02',
        //     GameName: getGameName('S-JF02'),
        //     ImageUrl: SPADE_IMAGE_URL+'/S-JF02.jpg'
        // },
        // {
        //     Type: 'Slots',
        //     GameId: 'S-LH02',
        //     GameName: getGameName('S-HF01'),
        //     ImageUrl: SPADE_IMAGE_URL+'/S-LH02.jpg'
        // },
        // {
        //     Type: 'Slots',
        //     GameId: 'S-LI02',
        //     GameName: getGameName('S-LI02'),
        //     ImageUrl: SPADE_IMAGE_URL+'/S-LI02.jpg'
        // },
        {
            Type: 'Slots',
            GameId: 'S-LY01',
            GameName: getGameName('S-LY01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-LY01.jpg',
            Provider:"Spade"
        },
        // {
        //     Type: 'Slots',
        //     GameId: 'S-PT01',
        //     GameName: getGameName('S-PT01'),
        //     ImageUrl: SPADE_IMAGE_URL+'/S-PT01.jpg'
        // },
        // {
        //     Type: 'Slots',
        //     GameId: 'S-SG03',
        //     GameName: getGameName('S-SG03'),
        //     ImageUrl: SPADE_IMAGE_URL+'/S-SG03.jpg'
        // },
        // {
        //     Type: 'Slots',
        //     GameId: 'S-SP02',
        //     GameName: getGameName('S-SP02'),
        //     ImageUrl: SPADE_IMAGE_URL+'/S-SP02.jpg'
        // },
        // {
        //     Type: 'Slots',
        //     GameId: 'S-WC02',
        //     GameName: getGameName('S-WC02'),
        //     ImageUrl: SPADE_IMAGE_URL+'/S-WC02.jpg'
        // },
        {
            Type: 'Slots',
            GameId: 'S-WM02',
            GameName: getGameName('S-WM02'),
            ImageUrl: SPADE_IMAGE_URL+'/S-WM02.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-LF01',
            GameName: getGameName('S-LF01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-LF01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-PK01',
            GameName: getGameName('S-PK01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-PK01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-GP02',
            GameName: getGameName('S-GP02'),
            ImageUrl: SPADE_IMAGE_URL+'/S-GP02.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-HF01',
            GameName: getGameName('S-HF01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-HF01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-BB01',
            GameName: getGameName('S-BB01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-BB01.jpg',
            Provider:"Spade"
        },
        // {
        //     Type: 'Slots',
        //     GameId: 'S-BF02',
        //     GameName: getGameName('S-BF02'),
        //     ImageUrl: SPADE_IMAGE_URL+'/S-BF02.jpg'
        // },
        {
            Type: 'Slots',
            GameId: 'S-CC01',
            GameName: getGameName('S-CC01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-CC01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-CY01',
            GameName: getGameName('S-CY01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-CC01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-DV01',
            GameName: getGameName('S-DV01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-DV01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-DX01',
            GameName: getGameName('S-DX01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-DX01.jpg',
            Provider:"Spade"
        },
        // {
        //     Type: 'Slots',
        //     GameId: 'S-FB02',
        //     GameName: getGameName('S-FB02'),
        //     ImageUrl: SPADE_IMAGE_URL+'/S-FB02.jpg'
        // },
        // {
        //     Type: 'Slots',
        //     GameId: 'S-FZ02',
        //     GameName: getGameName('S-FZ02'),
        //     ImageUrl: SPADE_IMAGE_URL+'/S-FZ02.jpg'
        // },
        {
            Type: 'Slots',
            GameId: 'S-GW01',
            GameName: getGameName('S-GW01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-GW01.jpg',
            Provider:"Spade"
        },
        // {
        //     Type: 'Slots',
        //     GameId: 'S-JT01',
        //     GameName: getGameName('S-JT01'),
        //     ImageUrl: SPADE_IMAGE_URL+'/S-JT01.jpg'
        // },
        {
            Type: 'Slots',
            GameId: 'S-LC01',
            GameName: getGameName('S-LC01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-LC01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-LM01',
            GameName:getGameName('S-LM01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-LM01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-LS01',
            GameName: getGameName('S-LS01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-LS01.jpg',
            Provider:"Spade"
        },
        // {
        //     Type: 'Slots',
        //     GameId: 'S-MH02',
        //     GameName: getGameName('S-MH02'),
        //     ImageUrl: SPADE_IMAGE_URL+'/S-MH02.jpg'
        // },
        // {
        //     Type: 'Slots',
        //     GameId: 'S-MP02',
        //     GameName: getGameName('S-MP02'),
        //     ImageUrl: SPADE_IMAGE_URL+'/S-MP02.jpg'
        // },
        // {
        //     Type: 'Slots',
        //     GameId: 'S-RG02',
        //     GameName:getGameName('S-RG02'),
        //     ImageUrl: SPADE_IMAGE_URL+'/S-RG02.jpg'
        // },
        {
            Type: 'Slots',
            GameId: 'S-RK01',
            GameName: getGameName('S-RK01'),
            ImageUrl: SPADE_IMAGE_URL+'/S-RK01.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-SA02',
            GameName: getGameName('S-SA02'),
            ImageUrl: SPADE_IMAGE_URL+'/S-SA02.jpg',
            Provider:"Spade"
        },
        {
            Type: 'Slots',
            GameId: 'S-SG02',
            GameName: getGameName('S-SG02'),
            ImageUrl: SPADE_IMAGE_URL+'/S-SG02.jpg',
            Provider:"Spade"
        }
        // {
        //     Type: 'Slots',
        //     GameId: 'S-SK01',
        //     GameName: getGameName('S-SK01'),
        //     ImageUrl: SPADE_IMAGE_URL+'/S-SK01.jpg'
        // },
        // {
        //     Type: 'Slots',
        //     GameId: 'S-TP01',
        //     GameName: getGameName('S-TP01'),
        //     ImageUrl: SPADE_IMAGE_URL+'/S-TP01.jpg'
        // },
        // {
        //     Type: 'Slots',
        //     GameId: 'S-TS02',
        //     GameName: getGameName('S-TS02'),
        //     ImageUrl: SPADE_IMAGE_URL+'/S-TS02.jpg'
        // }
        // {
        //     Type: 'FISHING',
        //     GameId: 'F-SF01',
        //     GameName: getGameName('F-SF01'),
        //     ImageUrl: SPADE_IMAGE_URL+'/F-SF01 .jpg'
        // },
        // {
        //     Type: 'Arcade',
        //     GameId: 'A-RB01',
        //     GameName: getGameName('A-RB01'),
        //     ImageUrl: SPADE_IMAGE_URL+'/A-RB01.jpg'
        // },
        // {
        //     Type: 'Arcade',
        //     GameId: 'A-DN02',
        //     GameName: getGameName('A-DN02'),
        //     ImageUrl: SPADE_IMAGE_URL+'/A-DN02.jpg'
        // },
        // {
        //     Type: 'Arcade',
        //     GameId: 'A-GJ01',
        //     GameName: getGameName('A-GJ01'),
        //     ImageUrl: SPADE_IMAGE_URL+'/A-GJ01.jpg'
        // },
        // {
        //     Type: 'Arcade',
        //     GameId: 'A-GL02',
        //     GameName: getGameName('A-GL02'),
        //     ImageUrl: SPADE_IMAGE_URL+'/A-GL02.jpg'
        // },
        // {
        //     Type: 'Arcade',
        //     GameId: 'A-RW02',
        //     GameName: getGameName('A-RW02'),
        //     ImageUrl: SPADE_IMAGE_URL+'/A-RW02.jpg'
        // },
        // {
        //     Type: 'Arcade',
        //     GameId: 'A-PF02',
        //     GameName: getGameName('A-PF02'),
        //     ImageUrl: SPADE_IMAGE_URL+'/A-PF02.jpg'
        // },
        // {
        //     Type: 'Arcade',
        //     GameId: 'A-SC02',
        //     GameName: getGameName('A-SC02'),
        //     ImageUrl: SPADE_IMAGE_URL+'/A-SC02.jpg'
        // },
        // {
        //     Type: 'Arcade',
        //     GameId: 'A-SH02',
        //     GameName: getGameName('A-SH02'),
        //     ImageUrl: SPADE_IMAGE_URL+'/A-SH02.jpg'
        // },
        // {
        //     Type: 'Arcade',
        //     GameId: 'A-DB06',
        //     GameName:getGameName('A-DB06'),
        //     ImageUrl: SPADE_IMAGE_URL+'/A-DB06.jpg'
        // },
        // {
        //     Type: 'Arcade',
        //     GameId: 'A-AP02',
        //     GameName: getGameName('A-AP02'),
        //     ImageUrl: SPADE_IMAGE_URL+'/A-AP02.jpg'
        // },
        // {
        //     Type: 'Arcade',
        //     GameId: 'A-DB04',
        //     GameName: getGameName('A-DB04'),
        //     ImageUrl: SPADE_IMAGE_URL+'/A-DB04.jpg'
        // },
        // {
        //     Type: 'Arcade',
        //     GameId: 'A-DB05',
        //     GameName: getGameName('A-DB05'),
        //     ImageUrl: SPADE_IMAGE_URL+'/A-DB05.jpg'
        // },
        // {
        //     Type: 'Arcade',
        //     GameId: 'A-DD02',
        //     GameName: getGameName('A-DD02'),
        //     ImageUrl: SPADE_IMAGE_URL+'/A-DD02.jpg'
        // },
        // {
        //     Type: 'Arcade',
        //     GameId: 'A-DE02',
        //     GameName: getGameName('A-DE02'),
        //     ImageUrl: SPADE_IMAGE_URL+'/A-DE02.jpg'
        // },
        // {
        //     Type: 'Arcade',
        //     GameId: 'A-JW02',
        //     GameName: getGameName('A-JW02'),
        //     ImageUrl: SPADE_IMAGE_URL+'/A-JW02.jpg'
        // },
        // {
        //     Type: 'Arcade',
        //     GameId: 'A-LB02',
        //     GameName: getGameName('A-LB02'),
        //     ImageUrl: SPADE_IMAGE_URL+'/A-LB02.jpg'
        // },
        // {
        //     Type: 'Arcade',
        //     GameId: 'A-MT02',
        //     GameName: getGameName('A-MT02'),
        //     ImageUrl: SPADE_IMAGE_URL+'/A-MT02.jpg'
        // },
        // {
        //     Type: 'Table',
        //     GameId: 'T-HC02',
        //     GameName: getGameName('T-HC02'),
        //     ImageUrl: SPADE_IMAGE_URL+'/T-HC02.jpg'
        // },
        // {
        //     Type: 'Table',
        //     GameId: 'T-CH02',
        //     GameName: getGameName('T-CH02'),
        //     ImageUrl: SPADE_IMAGE_URL+'/T-CH02.jpg'
        // },
        // {
        //     Type: 'Table',
        //     GameId: 'T-BK02',
        //     GameName: getGameName('T-BK02'),
        //     ImageUrl: SPADE_IMAGE_URL+'/T-BK02.jpg'
        // },
        // {
        //     Type: 'Table',
        //     GameId: 'T-DT03',
        //     GameName: getGameName('T-DT03'),
        //     ImageUrl: SPADE_IMAGE_URL+'/T-DT03.jpg'
        // },
        // {
        //     Type: 'Table',
        //     GameId: 'T-SB02',
        //     GameName: getGameName('T-SB02'),
        //     ImageUrl: SPADE_IMAGE_URL+'/T-SB02.jpg'
        // },
        // {
        //     Type: 'Table',
        //     GameId: 'T-SD03',
        //     GameName: getGameName('T-SD03'),
        //     ImageUrl: SPADE_IMAGE_URL+'/T-SD03.jpg'
        // },
        // {
        //     Type: 'Table',
        //     GameId: 'T-RT02',
        //     GameName: getGameName('T-RT02'),
        //     ImageUrl: SPADE_IMAGE_URL+'/T-RT02.jpg'
        // }
        ];

    return gameList;
}


function getGameName(id) {
    switch (id){
        case "F-SF01 ":
            return "Fishing God";
        case "S-MM01":
            return "Money Mouse";
        case "S-DE01":
            return "Sweet Bakery";
        case "S-BA01":
            return "Dancing Fever";
        case "S-LS02":
            return "Magical Lamp";
        case "S-HE01":
            return "Heroes";
        case "S-SB01":
            return "Sweet Bakery";
        case "S-DF02":
            return "Dancing Fever";
        case "S-ML01":
            return "Magical Lamp";
        case "S-TP02":
            return "Triple Panda";
        case "S-GP01":
            return "Gold Panther";
        case "S-CH01":
            return "Mr Chu Tycoon";
        case "S-GK01":
            return "Brothers kingdom";
        case "S-PG01":
            return "Prosperity Gods";
        case "S-CP01":
            return "Candy Pop";
        case "S-GF01":
            return "Golden Fist";
        case "S-LY02":
            return "FaFaFa2";
        case "S-GA01":
            return "Gangster Axe";
        case "S-WP02":
            return "Wow Prosperity";
        case "S-DM01":
            return "Ming Dynasty";
        case "S-FL02":
            return "First Love";
        case "S-FM02":
            return "Golden Monkey";
        case "S-PP01":
            return "Pan Fairy";
        case "S-SH01":
            return "ShangHai 008";
        case "S-TZ01":
            return "Jungle King";
        case "S-FG01":
            return "Fist of Gold";
        case "S-TW01":
            return "Tiger Warrior";
        case "S-NT01":
            return "Sea Emperor";
        case "S-ZE01":
            return "ZEUS";
        case "S-HY01":
            return "Ho Yeah Monkey";
        case "S-GC03":
            return "Golden Chicken";
        case "S-DG04":
            return "Dragon Gold SA";
        case "S-EG03":
            return "Emperor Gate SA";
        case "S-FC03":
            return "Big Prosperity SA";
        case "S-GS04":
            return "Great Stars SA";
        case "S-IL03":
            return "Iceland SA";
        case "S-IM03":
            return "Indian Myth SA";
        case "S-LE03":
            return "Lion Emperor SA";
        case "S-LH03":
            return "King The Lion Heart SA";
        case "S-SG04":
            return "Shougen War SA";
        case "S-SP03":
            return "Spartan SA";
        case "S-WC03":
            return "Wong Choy SA";
        case "S-WM03":
            return "5 Fortune SA";
        case "S-BC01":
            return "Baby Cai Shen";
        case "S-FO01":
            return "168 Fortunes";
        case "S-GL02":
            return "Golden Lotus SE";
        case "S-MR01":
            return "Mermaid";
        case "S-PH02":
            return "King Pharaoh";
        case "S-PO01":
            return "Pocket Mon Go";
        case "S-WP01":
            return "Wong Po";
        case "S-AL01":
            return "Alibaba";
        case "S-CS01":
            return "Cai Shen 888";
        case "S-DF01":
            return "Double Fortunes";
        case "S-FD01":
            return "5 Fortune Dragons";
        case "S-GO01":
            return "God's Kitchen";
        case "S-IL02":
            return "Adventure Iceland";
        case "S-IM02":
            return "Adventure Indian Myth";
        case "S-LE02":
            return "Adventure Lion Emperor";
        case "S-LK01":
            return "Lucky Koi";
        case "S-SM01":
            return "Soccer Mania";
        case "S-SM02":
            return "Space Monkey";
        case "S-AT02":
            return "Amazing Thailand";
        case "S-CM01":
            return "Drunken Jungle";
        case "S-DG03":
            return "Dragon Gold";
        case "S-EG02":
            return "Emperor Gate";
        case "S-FC02":
            return "Big Prosperity";
        case "S-GC02":
            return "Great China";
        case "S-GG01":
            return "Aqua Cash";
        case "S-GS03":
            return "Great Stars";
        case "S-HH01":
            return "Honey Hunter";
        case "S-HL01":
            return "Lucky Tank";
        case "S-JF02":
            return "Japan Fortune";
        case "S-LH02":
            return "King The Lion Heart";
        case "S-LI02":
            return "Lava Island";
        case "S-LY01":
            return "Fafafa";
        case "S-PT01":
            return "Hua Mulanf";
        case "S-SG03":
            return "Shougen War";
        case "S-SP02":
            return "Spartan";
        case "S-WC02":
            return "Wong Choy";
        case "S-WM02":
            return "5 Fortune";
        case "S-LF01":
            return "Lucky Feng Shui";
        case "S-PK01":
            return "Pirate King";
        case "S-GP02":
            return "Golf Champions";
        case "S-HF01":
            return "Highway Fortune";
        case "S-BB01":
            return "Festive Lion";
        case "S-BF02":
            return "Big Foot";
        case "S-CC01":
            return "Master Chef";
        case "S-CY01":
            return "Cai Yuan Guang Jin";
        case "S-DV01":
            return "Daddys Vacation";
        case "S-DX01":
            return "Da Fu Xiao Fu";
        case "S-FB02":
            return "World Cup Golden Boot";
        case "S-FZ02":
            return "Father vs Zombies";
        case "S-GW01":
            return "Golden Whale";
        case "S-JT01":
            return "Jack The Pirate";
        case "S-LC01":
            return "Lucky Cai Shen";
        case "S-LM01":
            return "Lucky Meow";
        case "S-LS01":
            return "Lucky Strike";
        case "S-MH02":
            return "The Curse Magic Hammer";
        case "S-MP02":
            return "Monster Tunnel";
        case "S-RG02":
            return "Rising Gems";
        case "S-RK01":
            return "Railway King";
        case "S-SA02":
            return "Spin Stone";
        case "S-SG02":
            return "Santa Gifts";
        case "S-SK01":
            return "Safari King";
        case "S-TP01":
            return "Fun Paradise";
        case "S-TS02":
            return "The Songs of China";
        case "A-RB01":
            return "Rob Stars";
        case "A-DN02":
            return "Derby Night";
        case "A-GJ01":
            return "Golden Journey";
        case "A-GL02":
            return "Goblin Treasure";
        case "A-RW02":
            return "Richie Wheel";
        case "A-PF02":
            return "PinBall Fortune";
        case "A-SC02":
            return "Dragon Clan";
        case "A-SH02":
            return "Shinobi Hamster";
        case "A-DB06":
            return "3 Kingdom The Dragon Boat";
        case "A-AP02":
            return "Animal Paradise";
        case "A-DB04":
            return "Bicycle Race";
        case "A-DB05":
            return "Dirt Bike";
        case "A-DD02":
            return "Dog Racing";
        case "A-DE02":
            return "Derby Express";
        case "A-JW02":
            return "Magic Journey";
        case "A-LB02":
            return "Lucky Baby";
        case "A-MT02":
            return "Monkey Thunder Bolt";
        case "T-HC02":
            return "Hulu Cock";
        case "T-CH02":
            return "Lucky Cup";
        case "T-BK02":
            return "Belangkai";
        case "T-DT03":
            return "Dragon Tiger";
        case "T-SB02":
            return "Sic Bo";
        case "T-SD03":
            return "Sedie";
        case "T-RT02":
            return "Roulette Euro";
        default:
            return id;
    }
}

function callGameLobbyPageUrl(username, client,gameId) {
    let apiUrl = SPADE_LOGIN_URL;
    let acctId = username;
    let lang = 'th_TH';
    let token = uuidv4();
    let isMobile = client == 'MB';
    console.log('-----------MERCHANT CODE------------', SPADE_MERCHANT_CODE)
    if(gameId) {
        return `${apiUrl}/?acctId=${acctId}&language=${lang}&token=${token}&mobile=${isMobile}&game=${gameId}&minigame=false&menumode=on`
    }else {
        return `${apiUrl}/?acctId=${acctId}&language=${lang}&token=${token}&lobby=SG&mobile=${isMobile}`
    }

}


function callCreateToken(username, callback) {


    let apiName = '/createToken';

    let data = {
        acctId: username.toLowerCase(),
        merchantCode: SPADE_MERCHANT_CODE,
        action: "xx",
        serialNo: generateSerialNo()
    }


    let headers = {
        'Content-Type': 'application/json'
    };

    let options = {
        url: SPADE_API_URL + apiName,
        method: 'POST',
        headers: headers,
        json: data,
    };

    console.log(options)
    request(options, function (err, response, body) {
            // if (error) return console.error('error(%s):', resourceName, error)
            console.log(body)
            if (err) {
                callback(body, null);
            } else {
                callback(null, body);
            }
        }
    );

}

function callRegisterToken(username, callback) {


    let apiName = '/ams/api';

    let data = {
        action: "register_token",
        site_id: AMEBA_SITE_ID,
        account_name: username.toLowerCase(),
        game_id: 1,
        lang: "thTH",
        sessionid: ""
    };

    let requestToken = generateToken(data);

    let headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + requestToken
    };

    let options = {
        url: AMEBA_URL + apiName,
        method: 'POST',
        headers: headers,
        json: data,
    };

    console.log(options)
    request(options, function (err, response, body) {
            // if (error) return console.error('error(%s):', resourceName, error)
            console.log(body)
            if (err) {
                callback(body, null);
            } else {

                if (body.error_code == "OK") {
                    callback(null, body.game_url);
                } else {
                    callback(body.error_code, null);
                }
            }
        }
    );

}

module.exports = {
    callGameLobbyPageUrl,
    getMerchantCode,
    callGetGameList,
    callCreateToken,
    getGameName
};