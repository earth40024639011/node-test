/**
 * Created by boonsongrit on 9/18/17.
 */

const moment = require('moment');
const dateFormat = require('dateformat');
const querystring = require('querystring');
const crypto = require('crypto');
const request = require('request');
const parseString = require('xml2js').parseString;
const uuidv1 = require('uuid/v1');

const _ = require('underscore');
const getClientKey = require('../controllers/getClientKey');
const {
    DG_API_URL = 'https://api.dg99web.com',
    DG_API_KEY = 'db2e7427d8e7450fad30caadf1218a6b',
    DG_AGENT_NAME = 'DGTE0101BR'
} = getClientKey.getKey();

let API_URL = DG_API_URL;
let API_KEY = DG_API_KEY;
let AGENT_NAME = DG_AGENT_NAME;

const CLIENT_NAME         = process.env.CLIENT_NAME          || 'SPORTBOOK88';

function generateRandomString() {
    return uuidv1();
}

function getGameTypeDetail(gameType) {

    switch (gameType) {
        case "bac" :
            return "Baccarat";
        case "dtx" :
            return "Dragon & Tiger";
        case "sicbo" :
            return "Sic Bo";
        case "rot" :
            return "Roulette";
        case "ftan" :
            return "Fan Tan";
        case "slot" :
            return "EGame";
        case "minigame" :
            return "Mini Game";
        case "multiplayer" :
            return "Fishermen Gold";
        default :
            return gameType
    }

}


function getGameNameByHostId(hostId) {


    if (hostId) {
        switch (Number.parseInt(hostId)) {
            case 401 :
                return "EGame";
            case 402 :
                return "Mini game";
            case 601 :
                return "Baccarat 1";
            case 602 :
                return "Baccarat 2";
            case 603 :
                return "Baccarat 3";
            case 604 :
                return "Baccarat 4";
            case 605 :
                return "Baccarat 5";
            case 606 :
                return "Baccarat 6";
            case 607 :
                return "Baccarat 7";
            case 608 :
                return "Baccarat 8";
            case 609 :
                return "Baccarat 9";
            case 610 :
                return "Baccarat 10";
            case 611 :
                return "Baccarat 11";
            case 612 :
                return "Baccarat 12";
            case 613 :
                return "Baccarat 13";
            case 614 :
                return "Baccarat 14";
            case 615 :
                return "Baccarat 15";
            case 616 :
                return "Baccarat 16";
            case 630 :
                return "Roulette";
            case 631 :
                return "Sic Bo";
            case 632 :
                return "Fan Tan";
            case 640 :
                return "Dragon & Tiger";
            default:
                return "";
        }
    } else {
        return "";
    }

}


function buildKey(random) {
    return crypto.createHash('md5').update(AGENT_NAME + API_KEY + random).digest("hex");
}

function getErrorMessage(code) {

    switch (code){
        case 0 :
            return "Operation Successful";
        case 1 :
            return "Parameter Error";
        case 2 :
            return "Token Verification Failed";
        case 3 :
            return "Command Not Find";
        case 4 :
            return "Illegal Operation";
        case 10 :
            return "Date format error";
        case 11 :
            return "Data format error";
        case 97 :
            return "Permission denied";
        case 98 :
            return "Operation failed";
        case 99 :
            return "Unknown Error";
        case 100 :
            return "Account is locked";
        case 101 :
            return "Account format error";
        case 102 :
            return "Account does not exist";
        case 103 :
            return "This account is taken";
        case 104 :
            return "Password format error";
        case 105 :
            return "Password wrong";
        case 106 :
            return "New & Old Password is the same";
        case 107 :
            return "Member account unavailable";
        case 108 :
            return "Login Error";
        case 109 :
            return "Signup Error";
        case 110 :
            return "This account has been signed in";
        case 111 :
            return "This account has been signed out";
        case 112 :
            return "This account is not signed in";
        case 113 :
            return "The Agent account inputted is not an Agent account";
        case 114 :
            return "Member not found";
        case 116 :
            return "Account occupied";
        case 117 :
            return "Can not find branch of member";
        case 118 :
            return "Can not find the specified Agent";
        case 119 :
            return "Insufficent funds during Agent withdrawal";
        case 120 :
            return "Insufficient balance";
        case 121 :
            return "Profit limit must be greater than or equal to 0";
        case 150 :
            return "Ran out of free demo accounts";
        case 300 :
            return "Ran out of free demo accounts";
        case 320 :
            return "Wrong API key";
        case 321 :
            return "Limit Group Not Found";
        case 322 :
            return "Currency Name Not Found";
        case 323 :
            return "Use serial numbers for Transfer";
        case 324 :
            return "Transfer failed";
        case 325 :
            return "Agent Status Unavailable";
        case 326 :
            return "Members Agent No video group";
        case 400 :
            return "Client IP Restricted";
        case 401 :
            return "Network latency";
        case 402 :
            return "The connection is closed";
        case 403 :
            return "Clients limited sources";
        case 404 :
            return "Resource requested does not exist";
        case 405 :
            return "Too frequent requests";
        case 406 :
            return "Request timed out";
        case 407 :
            return "Can not find game address";
        case 500 :
            return "Null pointer exception";
        case 501 :
            return "System Error";
        case 502 :
            return "The system is busy";
        case 503 :
            return "Data operation error";

        default :
            return code
    }
}


function callRegisterNewMember(username, callback) {


    console.log('API_URL : ',API_URL)
    console.log('API_KEY : ',API_KEY)
    console.log('AGENT_NAME : ',AGENT_NAME)

    const randomString = generateRandomString();
    const key = buildKey(randomString);

    let body = {
        token: key,
        random: randomString,
        data: "A",
        member: {
            username: username.toLowerCase(),
            password: buildKey("xxx"),
            currencyName: "THB",
            winLimit: 0
        }
    };

    console.log(body)

    request.post({
            url: `${API_URL}/user/signup/${AGENT_NAME}`,
            json: true,
            body: body
        },
        function (err, response, body) {
            // if (error) return console.error('error(%s):', resourceName, error)
            console.log(body)
            if (err) {
                callback(body, null);
            } else {
                callback(null, body);
            }
        }
    );
}


function callMemberLogin(username,device,limit, callback) {

    const randomString = generateRandomString();
    const key = buildKey(randomString);

    let body = {
        token: key,
        random: randomString,
        lang: "th",
        device: device,
        member: {
            username: username.toLowerCase(),
            password:""
        }
    };

    console.log('====== callMemberLogin ======')
    console.log('call : ',`${API_URL}/user/login/${AGENT_NAME}`)
    console.log('requestBody : ',body)
    request.post({
            url: `${API_URL}/user/login/${AGENT_NAME}`,
            json: true,
            body: body
        },
        function (err, response, body) {

        //1 web, 2 ios ,3 android ,4 wap html5
            console.log('responseBody : ',body)

            if (err) {
                callback('', null);
            } else {
                if(body.codeId == 0){

                    callUpdateLimit(username.toLowerCase(),limit,(err,limitCallback) => {
                        console.log(limitCallback)
                    })

                    let url;
                    if(device == 1 || device == 5){
                        url = body.list[0] + body.token
                    }else {
                        url = body.list[1] + body.token + '&language=th'
                    }

                    callback(null, url);
                }else {
                    body.message = getErrorMessage(body.codeId)
                    callback(body, null);
                }

            }
        }
    );

}



function callLaunchingLiveGameURL(username, gameId, client, token) {

    let x = '';
    if (gameId == '601') {
        x = '';
    } else {
        x = ',defaulttable=' + gameId;
    }

    let queryString = querystring.stringify({
        username: username,
        token: token,
        lobby: LOBBY_CODE,
        lang: 'th',
        // returnurl:'https://sportbook88.com',//optional
        // mobile:client === 'MB',
        mobilepremium: client === 'MB', // Start the client in premium HTML5 mobile version, mobile parameter will be ignored.
        h5web: true,
        options: 'hidemultiplayer=1,hideslot=1' + x
        // GameCode:'', //refer to Section 10​. EGame game code, FishermenGold for Fishermen Gold game
        // Lang:'th',//fishermen gold
        // Mobile:''//fishermen gold
    });

    //bar 601 , sicbo 631 , dragon 640
    // To go into table directly (601~616/630~632/640)

    let url = LOGIN_URL + '?' + queryString;
    return url;

}




function callKickUser(username,isEnabled, callback) {


    console.log('API_URL : ',API_URL)
    console.log('API_KEY : ',API_KEY)
    console.log('AGENT_NAME : ',AGENT_NAME)

    const randomString = generateRandomString();
    const key = buildKey(randomString);

    let body = {
        token: key,
        random: randomString,
        member: {
            username: username.toLowerCase(),
            password: buildKey("xxx"),
            status: isEnabled ? 1 : 0,
            winLimit: 0
        }
    };

    console.log(body)

    request.post({
            url: `${API_URL}/user/update/${AGENT_NAME}`,
            json: true,
            body: body
        },
        function (err, response, body) {
            // if (error) return console.error('error(%s):', resourceName, error)
            console.log(body)
            if (err) {
                callback(body, null);
            } else {
                callback(null, body);
            }
        }
    );

}


function callUpdateLimit(username,limit, callback) {


    const randomString = generateRandomString();
    const key = buildKey(randomString);

    let limitId;
    if(limit == 1){
        limitId = 'B';
    }else if(limit == 2){
        limitId = 'D';
    }else if(limit == 3){
        limitId = 'F';
    }else if(limit == 4){
        limitId = 'G';
    }else if(limit == 5){
        limitId = 'A';
    }


    let body = {
        token: key,
        random: randomString,
        data: limitId,
        member: {
            username: username.toLowerCase()
        }
    };

    console.log(body)

    request.post({
            url: `${API_URL}/game/updateLimit/${AGENT_NAME}`,
            json: true,
            body: body
        },
        function (err, response, body) {
            // if (error) return console.error('error(%s):', resourceName, error)
            console.log(body);
            if (err) {
                callback(body, null);
            } else {
                callback(null, body);
            }
        }
    );

}

function callReportBet(callback) {

    const randomString = generateRandomString();
    const key = buildKey(randomString);


    let body = {
        token: key,
        random: randomString,
    };

    console.log(body);
    console.log(body,'<============');

    request.post({
            url: `${API_URL}/game/getReport/${AGENT_NAME}`,
            json: true,
            body: body
        },
        function (err, response, body) {
            // if (error) return console.error('error(%s):', resourceName, error)
            console.log(body);
            if (err) {
                callback(body, null);
            } else {
                callback(null, body);
            }
        }
    );

}

module.exports = {
    getGameTypeDetail,
    getGameNameByHostId,
    callLaunchingLiveGameURL,
    callRegisterNewMember,
    callMemberLogin,
    callKickUser,
    callReportBet

}
