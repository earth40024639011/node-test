const moment = require('moment');

module.exports = {

    calculatePayout : function (amount, odd, oddType, priceType, callback) {
        console.log('calculatePayout : ' + amount + ' , ' + odd + ' , ' + priceType)
        let payout = 0;
        if (priceType === 'MY') {
            if (odd < 0) {
                payout = amount * 2;
            } else {
                if (oddType === 'X12' || oddType === 'X121ST' || oddType === 'ML') {
                    payout = (amount * odd);
                } else {
                    payout = (amount * odd) + amount;
                }
            }
        } else if (priceType === 'HK') {
            if (oddType === 'X12' || oddType === 'X121ST' || oddType === 'ML') {
                payout = (amount * odd);
            } else {
                payout = (amount * odd) + amount;
            }
        } else if (priceType === 'EU') {
            payout = (amount * odd);
        } else if (priceType === 'ID') {
            if (odd < 0) {
                payout = amount * 2;
            } else {
                if (oddType === 'X12' || oddType === 'X121ST'|| oddType === 'ML') {
                    payout = (amount * odd);
                } else {
                    payout = (amount * odd) + amount;
                }
            }
        }
        callback(payout.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]);
    },
};
