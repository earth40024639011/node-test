/**
 * Created by boonsongrit on 9/18/17.
 */

const moment = require('moment');
const dateFormat = require('dateformat');
const _ = require('underscore');

module.exports = {

    getCurrentAmbDateString: function () {
        if (moment().hours() >= 11) {
            return moment().utc(true).format('dddd').toUpperCase();
        } else {
            return moment().add(-1, 'days').utc(true).format('dddd').toUpperCase();
        }
    },
    getCurrentAmbDate: function () {
        if (moment().hours() >= 11) {
            return moment().utc(true);
        } else {
            return moment().add(-1, 'days').utc(true);
        }
    },
    dateFormat: function (date, format) {
        return dateFormat(date, format);
    },
    convertDateTime: function (dateStr,timeStr) {
        let hour = timeStr.split(':')[0];
        let minute = timeStr.split(':')[1];
        let second = timeStr.split(':')[2];

        return moment(dateStr, "DD/MM/YYYY").millisecond(0).second(second).minute(minute).hour(hour).utc(true).toDate()
    },
    getCurrentDate: function () {
        return new Date(new Date().getTime() + 1000 * 60 * 60 * 7)
    },
    convertThaiDate: function (date) {
        return new Date(date.getTime() + 1000 * 60 * 60 * 7)
    },
    enumerateNextDaysBetweenDates: function (startDate, endDate, format) {
        let now = moment(startDate, format), dates = [];

        while (now.isBefore(endDate) || now.isSame(endDate)) {
            dates.push(now.add(0, 'days').format(format));
            now.add(1, 'days');
        }
        return dates;
    },
    enumerateBackDaysBetweenDates: function (startDate, endDate, format) {
        let now = moment(endDate, format), dates = [];

        while (now.isAfter(startDate) || now.isSame(startDate)) {
            dates.push(now.add(0, 'days').format(format));
            now.add(-1, 'days');
        }
        return dates;
    },
    enumerateReportBetweenDates: function (startDate, endDate, timeFrom, timeTo) {

        let now = moment(startDate, 'DD/MM/YYYY'), dates = [];

        if (startDate.isSame(endDate)) {
            dates.push([now.format('YYYY-MM-DDT' + timeFrom + '.000'), now.format('YYYY-MM-DDT' + timeTo + '.000')]);
            return dates;
        }

        while (now.isBefore(endDate) || now.isSame(endDate)) {

            if (dates.length == 0) {

                let timeFromSec = hmsToSeconds(timeFrom);
                console.log('timeFromSec : ', timeFromSec)
                console.log('hmsToSeconds : ', hmsToSeconds('18:00:00'));


                if (timeFromSec > hmsToSeconds('18:00:00')) {
                    dates.push([now.add(0, 'days').format('YYYY-MM-DDT' + timeFrom + '.000'), now.add(1, 'days').format('YYYY-MM-DDT11:00:00.000')]);
                } else {
                    dates.push([now.add(0, 'days').format('YYYY-MM-DDT' + timeFrom + '.000'), now.add(0, 'days').format('YYYY-MM-DDT18:00:00.000')]);

                    let nextTo = moment(now).add(1, 'd');
                    if (nextTo.isSame(endDate)) {
                        dates.push([now.add(0, 'days').format('YYYY-MM-DDT18:00:00.000'), now.add(1, 'days').format('YYYY-MM-DDT' + timeTo + '.000')]);
                        break;
                    } else {
                        dates.push([now.add(0, 'days').format('YYYY-MM-DDT18:00:00.000'), now.add(1, 'days').format('YYYY-MM-DDT11:00:00.000')]);
                    }

                }

            } else {

                let nextTo = moment(now).add(1, 'd');
                dates.push([now.add(0, 'days').format('YYYY-MM-DDT11:00:00.000'), now.add(0, 'days').format('YYYY-MM-DDT18:00:00.000')]);

                if (nextTo.isSame(endDate)) {
                    dates.push([now.add(0, 'days').format('YYYY-MM-DDT18:00:00.000'), now.add(1, 'days').format('YYYY-MM-DDT' + timeTo + '.000')]);
                    break;
                } else {
                    dates.push([now.add(0, 'days').format('YYYY-MM-DDT18:00:00.000'), now.add(1, 'days').format('YYYY-MM-DDT11:00:00.000')]);
                }

            }
        }

        function hmsToSeconds(s) {
            var b = s.split(':');
            return b[0] * 3600 + b[1] * 60 + (+b[2] || 0);
        }

        return dates;
    },
    enumerateReportBetweenDatesTask: function (startDate, endDate, timeFrom, timeTo,task) {

        let dates = [];
        let now = moment(startDate, 'DD/MM/YYYY').second(timeFrom.split(':')[2]).minute(timeFrom.split(':')[1]).hour(timeFrom.split(':')[0]);
        let end = moment(endDate, 'DD/MM/YYYY').second(timeTo.split(':')[2]).minute(timeTo.split(':')[1]).hour(timeTo.split(':')[0]);

        const duration = moment.duration(end.diff(now));
        const hours = duration.asHours();

        const hourPerTask = task || 24;
        const taskCount = hours / hourPerTask;


        let beginCount = 1;

        while (beginCount < taskCount) {

            dates.push([now.format('YYYY-MM-DDTHH:mm:ss'), now.add(hourPerTask, 'h').format('YYYY-MM-DDTHH:mm:ss')]);
            beginCount++;
        }
        dates.push([now.format('YYYY-MM-DDTHH:mm:ss'), moment(endDate, 'DD/MM/YYYY').format('YYYY-MM-DDT' + timeTo)]);


        return dates;
    },
    enumerateReportBetweenDates2: function (startDate, endDate, timeFrom, timeTo) {

        console.log('hourPerTask : ',process.env.REPORT_TASK_HOUR)
        let dates = [];
        let now = moment(startDate, 'DD/MM/YYYY').second(timeFrom.split(':')[2]).minute(timeFrom.split(':')[1]).hour(timeFrom.split(':')[0]);
        let end = moment(endDate, 'DD/MM/YYYY').second(timeTo.split(':')[2]).minute(timeTo.split(':')[1]).hour(timeTo.split(':')[0]);

        const duration = moment.duration(end.diff(now));
        const hours = duration.asHours();

        let hourPerTask = process.env.REPORT_TASK_HOUR || 24;

        const taskCount = hours / hourPerTask;


        let beginCount = 1;

            while (beginCount < taskCount) {

                dates.push([now.format('YYYY-MM-DDTHH:mm:ss'), now.add(hourPerTask, 'h').format('YYYY-MM-DDTHH:mm:ss')]);
                beginCount++;
            }
            dates.push([now.format('YYYY-MM-DDTHH:mm:ss'), moment(endDate, 'DD/MM/YYYY').format('YYYY-MM-DDT' + timeTo)]);


        return dates;
    },
    enumerateReportBetweenDatesMember: function (startDate, endDate, timeFrom, timeTo) {

        console.log('hourPerTask : ',process.env.REPORT_TASK_HOUR)
        let dates = [];
        let now = moment(startDate, 'DD/MM/YYYY').second(timeFrom.split(':')[2]).minute(timeFrom.split(':')[1]).hour(timeFrom.split(':')[0]);
        let end = moment(endDate, 'DD/MM/YYYY').second(timeTo.split(':')[2]).minute(timeTo.split(':')[1]).hour(timeTo.split(':')[0]);

        const duration = moment.duration(end.diff(now));
        const hours = duration.asHours();

        let hourPerTask =  60;

        const taskCount = hours / hourPerTask;


        let beginCount = 1;

        while (beginCount < taskCount) {

            dates.push([now.format('YYYY-MM-DDTHH:mm:ss'), now.add(hourPerTask, 'h').format('YYYY-MM-DDTHH:mm:ss')]);
            beginCount++;
        }
        dates.push([now.format('YYYY-MM-DDTHH:mm:ss'), moment(endDate, 'DD/MM/YYYY').format('YYYY-MM-DDT' + timeTo)]);


        return dates;
    }
};
