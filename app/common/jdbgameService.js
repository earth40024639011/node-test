/**
 * Created by boonsongrit on 9/18/17.
 */

const moment = require('moment');
const querystring = require('querystring');
const crypto = require('crypto');
const request = require('request');
const _ = require('underscore');
const base64url = require('base64url');
const utf8 = require('utf8');

const API_URL =  'http://api.jygrq.com';

const ENCRYPT_KEY = 'f1b1072fac101fc4';
const DC = 'PT';
const IV = '003fbb0cd5a52617';

const IMAGE_URL = process.env.SPADE_IMAGE_URL || 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/jdb-game';

function encodeDesCBC(textToEncode) {
    console.log('KEY : ',ENCRYPT_KEY);
    console.log('DC : ',DC);
    console.log('IV : ',IV);
    try {

        var cipher = crypto.createCipheriv("aes-128-cbc",  new Buffer(ENCRYPT_KEY), new Buffer(IV));
        var encryptedString = cipher.update(textToEncode, 'utf8', 'base64');
        encryptedString += cipher.final('base64');
        return  base64url.fromBase64(encryptedString)
    } catch (exception) {
        throw exception;
    }
}


function decodeDesCBC(textToDecode) {
    console.log('textToDecode : ',textToDecode)

    try {
        var decipher = crypto.createDecipheriv('aes-128-cbc', new Buffer(ENCRYPT_KEY), new Buffer(IV));
        decipher.setAutoPadding(false);
        let dec = decipher.update(textToDecode, 'base64','utf8');
        return dec.replace(/\u0000/g,'');

    } catch (exception) {
        throw exception;
    }
}



function callGetToken(username,balance, gameId,mId,callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };


    let queryString = JSON.stringify({
        action:21,
        ts: new Date().getTime(),
        parent:"ptthbag",
        uid:username,
        balance:balance,
        gType:gameId,
        mType:mId,
    });


    console.log('queryString : ', queryString);
    let desEncrypt = encodeDesCBC(queryString);


    let options = {
        url: API_URL + '/apiRequest.do?dc=' + DC + "&x=" +desEncrypt,
        method: 'POST',
        headers: headers
    };

    console.log(options)

    request(options, (error, response, body) => {
        if (error) {
            callback('', null);
        } else {
            callback(null,JSON.parse(body));
        }
    });

}


function callDemoAccount(username,balance, callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    let queryString = JSON.stringify({
        action:47,
        ts: new Date().getTime()
    });


    console.log('requestBody : ', queryString);
    let desEncrypt = encodeDesCBC(queryString);


    let options = {
        url: API_URL + '/apiRequest.do?dc=' + DC + "&x=" + desEncrypt,
        method: 'POST',
        headers: headers
    };

    console.log(options)

    request(options, (error, response, body) => {
        if (error) {
            callback('', null);
        } else {
            console.log('responseBody : ',body)
            callback(null,body);

        }
    });

}


function callLaunchingLiveGameURL(username, gameId, client, token) {

    let x = '';
    if (gameId == '601') {
        x = '';
    } else {
        x = ',defaulttable=' + gameId;
    }

    let queryString = querystring.stringify({
        username: username,
        token: token,
        lobby: LOBBY_CODE,
        lang: 'th',
        // returnurl:'https://sportbook88.com',//optional
        // mobile:client === 'MB',
        mobilepremium: client === 'MB', // Start the client in premium HTML5 mobile version, mobile parameter will be ignored.
        h5web: true,
        options: 'hidemultiplayer=1,hideslot=1' + x
        // GameCode:'', //refer to Section 10​. EGame game code, FishermenGold for Fishermen Gold game
        // Lang:'th',//fishermen gold
        // Mobile:''//fishermen gold
    });

    //bar 601 , sicbo 631 , dragon 640
    // To go into table directly (601~616/630~632/640)

    let url = LOGIN_URL + '?' + queryString;
    return url;

}

function callKickUser(username, callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    let method = "KickUser";
    let time = moment().utc(true).format('YYYYMMDDHHmmss');


    let queryString = querystring.stringify({
        method: method,
        Key: SECRET_KEY,
        Time: time,
        Username: username.toLowerCase()
    });

    // let desEncrypt = encodeDesCBC(queryString);
    // let queryStringEncode = encodeURIComponent(desEncrypt);
    // let signature = buildSignature(queryString, time);
    //
    // let options = {
    //     url: API_URL + '?q=' + queryStringEncode + "&s=" + signature,
    //     method: 'POST',
    //     headers: headers
    // };
    //
    //
    // request(options, (error, response, body) => {
    //
    //     if (error) {
    //         callback('', null);
    //     } else {
    //
    //         parseString(body, function (err, result) {
    //
    //             if (!result.KickUserResponse) {
    //
    //                 let responseMsg = result.APIResponse;
    //
    //                 let jsonResponse = {
    //                     errMsgId: responseMsg.ErrorMsgId[0],
    //                     errMsg: responseMsg.ErrorMsg[0],
    //                 }
    //                 callback(null, jsonResponse)
    //             } else {
    //                 let responseMsg = result.KickUserResponse;
    //
    //                 let jsonResponse = {
    //                     errMsgId: responseMsg.ErrorMsgId[0],
    //                     errMsg: responseMsg.ErrorMsg[0]
    //                 }
    //                 callback(null, jsonResponse)
    //             }
    //
    //         });
    //
    //     }
    // });
}


function callGetGameList() {

    // FISHING	F-SF01 	Fishing God
    // Slot	S-CH01	Mr Chu Tycoon
    // Slot	S-GK01	Brothers kingdom
    // Slot	S-PG01	Prosperity Gods
    // Slot	S-CP01	Candy Pop
    // Slot	S-GF01	Golden Fist
    // Slot	S-LY02	FaFaFa2
    // Slot	S-GA01 	Gangster Axe


    let gameList = [
        {
            Type: 'FISH',
            GameId: '7',
            MId: '7001',
            GameName: getGameName('7001'),
            ImageUrl: IMAGE_URL+'/jdb_7001.jpg'
        },
        {
            Type: 'FISH',
            GameId: '7',
            MId: '7002',
            GameName: getGameName('7002'),
            ImageUrl: IMAGE_URL+'/jdb_7002.jpg'
        },
        {
            Type: 'FISH',
            GameId: '7',
            MId: '7003',
            GameName: getGameName('7003'),
            ImageUrl: IMAGE_URL+'/jdb_7003.jpg'
        },
        {
            Type: 'FISH',
            GameId: '7',
            MId: '7004',
            GameName: getGameName('7004'),
            ImageUrl: IMAGE_URL+'/jdb_7004.jpg'
        }
    ];

    return gameList;
}


function getGameName(mType) {
    switch (mType){
        case "7001":
            return "Dragon Fishing";
        case "7002":
            return "Dragon Fishing II";
        case "7003":
            return "Cai Shen Fishing";
        case "7004":
            return "Five Dragons Fishing";
        default:
            return mType;
    }
}

module.exports = {
    callGetToken,
    callDemoAccount,
    encodeDesCBC,
    decodeDesCBC,
    callLaunchingLiveGameURL,
    callKickUser,
    callGetGameList
}
