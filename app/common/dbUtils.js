const moment = require('moment');

module.exports = {

    todayCondition : function () {
        return {
            "$gte":  new Date(moment().add(0, 'days').format('YYYY-MM-DDT00:00:00.000')),
            "$lt": new Date(moment().add(1, 'days').format('YYYY-MM-DDT00:00:00.000'))
        };
    },
};
