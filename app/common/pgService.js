const MemberModel = require('../models/member.model');
const request = require('request');
const _ = require('underscore');
const mongoose = require('mongoose');
const BetTransactionModel = require('../models/betTransaction.model.js');
const async = require("async");
const moment = require('moment');
const querystring = require('querystring');
const Crypto = require("crypto");
const AgentGroupModel = require('../models/agentGroup.model.js');
const MemberService = require('../common/memberService');
const DateUtils = require('../common/dateUtils');
const NumberUtils = require('../common/numberUtils1');
const AgentService = require('../common/agentService');
const roundTo = require('round-to');


const getClientKey = require('../controllers/getClientKey');
const {
  PGSOFT_IMAGE_URL: PGSOFT_IMAGE_URL = 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/pg-soft',
  PG_URL: FORWARD_GAME = 'https://m.pg-redirect.net/',
  PG_TOKEN: OPERATOR_TOKEN = 'fd8e66ef36cbe310501481de441cd557'
} = getClientKey.getKey();


function forwardToGame(token, gameCode, callback) {
    let url = `${FORWARD_GAME}${gameCode}/index.html?language=th&bet_type=1&operator_token=${OPERATOR_TOKEN}&operator_player_session=${token}`;
    callback(null, url);
}



function callGetGameList() {
    let gameList = [
        {
            Type: 'Slot',
            GameId: '62',
            GameCode: 'gem-saviour-conquest',
            GameName: getGameName('62'),
            ImageUrl: PGSOFT_IMAGE_URL+'/gem-saviour-conquest.png'
        },
        {
            Type: 'Slot',
            GameId: '20',
            GameCode: 'reel-love',
            GameName: getGameName('20'),
            ImageUrl: PGSOFT_IMAGE_URL+'/reel-love.png'
        },
        {
            Type: 'Slot',
            GameId: '1',
            GameCode: 'diaochan',
            GameName: getGameName('1'),
            ImageUrl: PGSOFT_IMAGE_URL+'/diaochan.png'
        },
        {
            Type: 'Slot',
            GameId: '3',
            GameCode: 'fortune-gods',
            GameName: getGameName('3'),
            ImageUrl: PGSOFT_IMAGE_URL+'/FortuneGods.png'
        },
        {
            Type: 'Slot',
            GameId: '24',
            GameCode: 'win-win-won',
            GameName: getGameName('24'),
            ImageUrl: PGSOFT_IMAGE_URL+'/win-win-won.png'
        },
        {
            Type: 'Slot',
            GameId: '6',
            GameCode: 'medusa2',
            GameName: getGameName('6'),
            ImageUrl: PGSOFT_IMAGE_URL+'/medusa2.png'
        },
        {
            Type: 'Slot',
            GameId: '8',
            GameCode: 'peas-fairy',
            GameName: getGameName('8'),
            ImageUrl: PGSOFT_IMAGE_URL+'/peas-fairy.png'
        },
        {
            Type: 'Slot',
            GameId: '26',
            GameCode: 'fortune-tree',
            GameName: getGameName('26'),
            ImageUrl: PGSOFT_IMAGE_URL+'/fortune-tree.png'
        },
        {
            Type: 'Slot',
            GameId: '10',
            GameCode: 'joker-wild',
            GameName: getGameName('10'),
            ImageUrl: PGSOFT_IMAGE_URL+'/joker-wild.png'
        },
        {
            Type: 'Slot',
            GameId: '7',
            GameCode: 'medusa',
            GameName: getGameName('7'),
            ImageUrl: PGSOFT_IMAGE_URL+'/medusa.png'
        },
        {
            Type: 'Slot',
            GameId: '12',
            GameCode: 'blackjack-eu',
            GameName: getGameName('12'),
            ImageUrl: PGSOFT_IMAGE_URL+'/blackjack-eu.png'
        },
        {
            Type: 'Slot',
            GameId: '11',
            GameCode: 'blackjack-us',
            GameName: getGameName('11'),
            ImageUrl: PGSOFT_IMAGE_URL+'/blackjack-us.png'
        },
        {
            Type: 'Slot',
            GameId: '25',
            GameCode: 'plushie-frenzy',
            GameName: getGameName('25'),
            ImageUrl: PGSOFT_IMAGE_URL+'/plushie-frenzy.png'
        },
        {
            Type: 'Slot',
            GameId: '17',
            GameCode: 'wizdom-wonders',
            GameName: getGameName('17'),
            ImageUrl: PGSOFT_IMAGE_URL+'/wizdom-wonders.png'
        },
        {
            Type: 'Slot',
            GameId: '2',
            GameCode: 'gem-saviour',
            GameName: getGameName('2'),
            ImageUrl: PGSOFT_IMAGE_URL+'/gem-saviour.png'
        },
        {
            Type: 'Slot',
            GameId: '19',
            GameCode: 'steam-punk',
            GameName: getGameName('19'),
            ImageUrl: PGSOFT_IMAGE_URL+'/steam-punk.png'
        },
        {
            Type: 'Slot',
            GameId: '4',
            GameCode: 'summon-conquer',
            GameName: getGameName('4'),
            ImageUrl: PGSOFT_IMAGE_URL+'/summon-conquer.png'
        },
        {
            Type: 'Slot',
            GameId: '18',
            GameCode: 'hood-wolf',
            GameName: getGameName('18'),
            ImageUrl: PGSOFT_IMAGE_URL+'/hood-wolf.png'
        },
        {
            Type: 'Slot',
            GameId: '28',
            GameCode: 'hotpot',
            GameName: getGameName('28'),
            ImageUrl: PGSOFT_IMAGE_URL+'/hotpot.png'
        },
        {
            Type: 'Slot',
            GameId: '29',
            GameCode: 'dragon-legend',
            GameName: getGameName('29'),
            ImageUrl: PGSOFT_IMAGE_URL+'/dragon-legend.png'
        },
        {
            Type: 'Slot',
            GameId: '35',
            GameCode: 'mr-hallow-win',
            GameName: getGameName('35'),
            ImageUrl: PGSOFT_IMAGE_URL+'/mr-hallow-win.png'
        },
        {
            Type: 'Slot',
            GameId: '34',
            GameCode: 'legend-of-hou-yi',
            GameName: getGameName('34'),
            ImageUrl: PGSOFT_IMAGE_URL+'/legend-of-hou-yi.png'
        },
        {
            Type: 'Slot',
            GameId: '27',
            GameCode: 'restaurant-craze',
            GameName: getGameName('27'),
            ImageUrl: PGSOFT_IMAGE_URL+'/restaurant-craze.png'
        },
        {
            Type: 'Slot',
            GameId: '36',
            GameCode: 'prosperity-lion',
            GameName: getGameName('36'),
            ImageUrl: PGSOFT_IMAGE_URL+'/prosperity-lion.png'
        },
        {
            Type: 'Slot',
            GameId: '33',
            GameCode: 'hip-hop-panda',
            GameName: getGameName('33'),
            ImageUrl: PGSOFT_IMAGE_URL+'/hip-hop-panda.png'
        },
        {
            Type: 'Slot',
            GameId: '37',
            GameCode: 'santas-gift-rush',
            GameName: getGameName('37'),
            ImageUrl: PGSOFT_IMAGE_URL+'/santas-gift-rush.png'
        },
        {
            Type: 'Slot',
            GameId: '31',
            GameCode: 'baccarat-deluxe',
            GameName: getGameName('31'),
            ImageUrl: PGSOFT_IMAGE_URL+'/baccarat-deluxe.png'
        },
        {
            Type: 'Slot',
            GameId: '38',
            GameCode: 'gem-saviour-sword',
            GameName: getGameName('38'),
            ImageUrl: PGSOFT_IMAGE_URL+'/gem-saviour-sword.png'
        },
        {
            Type: 'Slot',
            GameId: '39',
            GameCode: 'piggy-gold',
            GameName: getGameName('39'),
            ImageUrl: PGSOFT_IMAGE_URL+'/piggy-gold.png'
        },
        {
            Type: 'Slot',
            GameId: '41',
            GameCode: 'symbols-of-egypt',
            GameName: getGameName('41'),
            ImageUrl: PGSOFT_IMAGE_URL+'/symbols-of-egypt.png'
        },
        {
            Type: 'Slot',
            GameId: '44',
            GameCode: 'emperors-favour',
            GameName: getGameName('44'),
            ImageUrl: PGSOFT_IMAGE_URL+'/emperors-favour.png'
        },
        {
            Type: 'Slot',
            GameId: '42',
            GameCode: 'ganesha-gold',
            GameName: getGameName('42'),
            ImageUrl: PGSOFT_IMAGE_URL+'/ganesha-gold.png'
        },
        {
            Type: 'Slot',
            GameId: '43',
            GameCode: 'three-monkeys',
            GameName: getGameName('43'),
            ImageUrl: PGSOFT_IMAGE_URL+'/three-monkeys.png'
        },
        {
            Type: 'Slot',
            GameId: '40',
            GameCode: 'jungle-delight',
            GameName: getGameName('40'),
            ImageUrl: PGSOFT_IMAGE_URL+'/jungle-delight.png'
        },
        {
            Type: 'Slot',
            GameId: '45',
            GameCode: 'tomb-of-treasure',
            GameName: getGameName('45'),
            ImageUrl: PGSOFT_IMAGE_URL+'/tomb-of-treasure.png'
        },
        {
            Type: 'Slot',
            GameId: '48',
            GameCode: 'double-fortune',
            GameName: getGameName('48'),
            ImageUrl: PGSOFT_IMAGE_URL+'/double-fortune.png'
        },
        {
            Type: 'Slot',
            GameId: '52',
            GameCode: 'wild-inferno',
            GameName: getGameName('52'),
            ImageUrl: PGSOFT_IMAGE_URL+'/wild-inferno.png'
        },
        {
            Type: 'Slot',
            GameId: '53',
            GameCode: 'the-great-icescape',
            GameName: getGameName('53'),
            ImageUrl: PGSOFT_IMAGE_URL+'/the-great-icescape.png'
        },
        {
            Type: 'Slot',
            GameId: '50',
            GameCode: 'journey-to-the-wealth',
            GameName: getGameName('50'),
            ImageUrl: PGSOFT_IMAGE_URL+'/journey-to-the-wealth.png'
        },
        {
            Type: 'Slot',
            GameId: '54',
            GameCode: 'captains-bounty',
            GameName: getGameName('54'),
            ImageUrl: PGSOFT_IMAGE_URL+'/captains-bounty.png'
        },
        {
            Type: 'Slot',
            GameId: '21',
            GameCode: 'tiki-go',
            GameName: getGameName('21'),
            ImageUrl: PGSOFT_IMAGE_URL+'/tiki-go.png'
        },
        {
            Type: 'Slot',
            GameId: '60',
            GameCode: 'leprechaun-riches',
            GameName: getGameName('60'),
            ImageUrl: PGSOFT_IMAGE_URL+'/leprechaun-riches.png'
        },
        {
            Type: 'Slot',
            GameId: '61',
            GameCode: 'flirting-scholar',
            GameName: getGameName('61'),
            ImageUrl: PGSOFT_IMAGE_URL+'/flirting-scholar.png'
        },
        {
            Type: 'Slot',
            GameId: '59',
            GameCode: 'ninja-vs-samurai',
            GameName: getGameName('59'),
            ImageUrl: PGSOFT_IMAGE_URL+'/ninja-vs-samurai.png'
        },
        {
            Type: 'Slot',
            GameId: '64',
            GameCode: 'muay-thai-champion',
            GameName: getGameName('64'),
            ImageUrl: PGSOFT_IMAGE_URL+'/muay-thai-champion.png'
        },
        {
            Type: 'Slot',
            GameId: '63',
            GameCode: 'dragon-tiger-luck',
            GameName: getGameName('63'),
            ImageUrl: PGSOFT_IMAGE_URL+'/dragon-tiger-luck.png'
        },
        {
            Type: 'Slot',
            GameId: '65',
            GameCode: 'mahjong-ways',
            GameName: getGameName('65'),
            ImageUrl: PGSOFT_IMAGE_URL+'/mahjong-ways.png'
        },
        {
            Type: 'Slot',
            GameId: '57',
            GameCode: 'dragon-hatch',
            GameName: getGameName('57'),
            ImageUrl: PGSOFT_IMAGE_URL+'/dragon-hatch.png'
        },
        {
            Type: 'Slot',
            GameId: '68',
            GameCode: 'fortune-mouse',
            GameName: getGameName('68'),
            ImageUrl: PGSOFT_IMAGE_URL+'/fortune-mouse.png'
        },
    ];

    return gameList;
}

function getGameName(id) {
    switch (id){
        case "62":
            return "Gem Saviour Conquest";
        case "20":
            return "Reel Love";
        case "1":
            return "Honey Trap of Diao Chan";
        case "3":
            return "Fortune Gods";
        case "24":
            return "Win Win Won";
        case "6":
            return "Medusa II";
        case "8":
            return "Peas Fairy";
        case "26":
            return "Tree of Fortune";
        case "10":
            return "Joker Wild";
        case "7":
            return "Medusa";
        case "12":
            return "European Blackjack";
        case "11":
            return "American Blackjack";
        case "25":
            return "Plushie Frenzy";
        case "17":
            return "Wizdom Wonders";
        case "2":
            return "Gem Saviour";
        case "19":
            return "Steampunk: Wheel of Destiny";
        case "4":
            return "Summon & Conquer";
        case "18":
            return "Hood vs Wolf";
        case "28":
            return "Hotpot";
        case "29":
            return "Dragon Legend";
        case "35":
            return "Mr. Hallow-Win";
        case "34":
            return "Legend of Hou Yi";
        case "27":
            return "Restaurant Craze";
        case "36":
            return "Prosperity Lion";
        case "33":
            return "Hip Hop Panda";
        case "37":
            return "Santa's Gift Rush";
        case "31":
            return "Baccarat Deluxe";
        case "38":
            return "Gem Saviour Sword";
        case "39":
            return "Piggy Gold";
        case "41":
            return "Symbols of Egypt";
        case "44":
            return "Emperor's Favour";
        case "42":
            return "Ganesha Gold";
        case "43":
            return "Three Monkeys ";
        case "40":
            return "Jungle Delight";
        case "45":
            return "Tomb of Treasure";
        case "48":
            return "Double Fortune";
        case "52":
            return "Wild Inferno";
        case "53":
            return "The Great Icescape";
        case "50":
            return "Journey to the Wealth";
        case "54":
            return "Captain's Bounty";
        case "21":
            return "Tiki Go";
        case "60":
            return "Leprechaun Riches";
        case "61":
            return "Flirting Scholar";
        case "59":
            return "Ninja vs Samurai";
        case "64":
            return "Muay Thai Champion";
        case "63":
            return "Dragon Tiger Luck";
        case "65":
            return "Mahjong Ways";
        case "57":
            return "Dragon Hatch";
        case "68":
            return "Fortune Mouse";
        default:
            return id;
    }
}

module.exports = {callGetGameList, getGameName, forwardToGame}