const crypto = require('crypto');
const config = require('config')
const jwt = require('jsonwebtoken')

module.exports = {
    buildSignature : function (FunctionName,requestDatetime,playerID) {
        let s;
        if(playerID){
           s = FunctionName+requestDatetime+config.get('Live22.API.OperatorID')+config.get('Live22.API.Secret')+playerID;
        }else{
            s = FunctionName+requestDatetime+config.get('Live22.API.OperatorID')+config.get('Live22.API.Secret');
        }
        console.log(s)
        return crypto.createHash('md5').update(s).digest("hex");
    },
    getIP : function (req) {
        let clientIp = req.headers['cf-connecting-ip'] || req.headers['x-forwarded-for'] || req.connection.remoteAddress;
        try {
            clientIp = clientIp.split(':').pop();
        } catch (e) {
            clientIp = '9.8.7.6'
        }
        return clientIp;
    },
    decodeJWT : function (req) {
        if (req.headers.authorization && (req.headers.authorization.split(' ')[0].toLowerCase() === 'bearer')) {
            return jwt.decode(req.headers.authorization.split(' ')[1]);
        } else if (req.query && req.query.token) {
            return jwt.decode(req.query.token);
        }
        return null;
    }
};
