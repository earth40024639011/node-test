/**
 * Created by boonsongrit on 9/18/17.
 */

const moment = require('moment');
const dateFormat = require('dateformat');
const querystring = require('querystring');
const crypto = require('crypto');
const request = require('request');


const API_URL = process.env.ALLBET_URL || 'https://platform-api.apidemo.net:8443/';
const AGENT_NAME = process.env.ALLBET_AGENT_NAME || 'vrn5aa';
const PROPERTY_ID = process.env.PROPERTY_ID || '0267566';
const DES_KEY = process.env.sd || 'YR16Awz75AO0F/a5CptRnLkLNPWkuuaZ';
const MD5_KEY = process.env.sd || 'naft1+dzLFacbzAP1iKRdVzWmiyIvo+9d8yJShRwdkI=';
const SHA1_KEY = '8UwOq6b7JqH9UWo43iGEhetyhOSs3/93FGJKpgVEe2qJzYRCidPegaqVEDUEmrZUl049X45dpnYMqxCr961Xzg==';


function getGameTypeDetail(gameType) {

    console.log('getGameTypeDetail gameType : ', gameType);

    switch (Number.parseInt(gameType)) {
        case 101 :
            return "Normal Baccarat";
        case 102 :
            return "VIP Baccarat";
        case 103 :
            return "Rapid Baccarat";
        case 104 :
            return "Jing Mi Baccarat";
        case 201 :
            return "Sic Bo";
        case 301 :
            return "Dragon Tiger";
        case 401 :
            return "Roulette";
        case 501 :
            return "Baccarat in the EUR hall";
        case 601 :
            return "Roulette in the EUR hall";
        case 701 :
            return "Backjack in the EUR hall";
        case 106 :
            return "Treasure Baccarat";
    }

}

function getCommissionDetail(type) {

    console.log('type : ', type);

    switch (Number.parseInt(type)) {
        case 100 :
            return "Commission";
        case 101 :
            return "Non-commission";
    }

}


function getBetTypeDetail(gameType,betType) {

    console.log('betType : ', betType);

    if([101,102,103,104,106].includes(Number.parseInt(gameType))) {
        switch (Number.parseInt(betType)) {
            case 1001 :
                return "Banker";
            case 1002 :
                return "Player";
            case 1003 :
                return "Tie";
            case 1004 :
                return "Big";
            case 1005 :
                return "Small";
            case 1006 :
                return "Banker Pair";
            case 1007 :
                return "Player Pair";
        }
    }else if([301].includes(Number.parseInt(gameType))){
        switch (Number.parseInt(betType)) {
            case 2001 :
                return "Dragon";
            case 2002 :
                return "Tiger";
            case 2003 :
                return "Tie";
        }
    }else if([201].includes(Number.parseInt(gameType))){
        switch (Number.parseInt(betType)) {
            case 3001 :
                return "Small";
            case 3002 :
                return "Odd";
            case 3003 :
                return "Even";
            case 3004 :
                return "Big";
            //more
        }
    }else if([401].includes(Number.parseInt(gameType))){
        switch (Number.parseInt(betType)) {
            case 4001 :
                return "Small";
            case 4002 :
                return "Even";
            case 4003 :
                return "Red";
            case 4004 :
                return "Black";
            case 4005 :
                return "Odd";
            case 4006 :
                return "Big";
                //more
        }
    }
}


function generateRandomNumber() {
    return new Date().getTime();
}


const NULL_IV = Buffer.from('AAAAAAAAAAA=', 'base64').toString('utf8');

function encryptX(data, key, iv) {
    try {
        let algo = 'des-ede3-cbc';
        let cipher = crypto.createCipheriv(algo, key, iv);
        let encoding = 'base64';
        let result = cipher.update(data, "utf8", encoding);
        result += cipher.final(encoding);
        return result;
    } catch (err) {
        console.log(err)
        return null;
    }
}


function encrypt(data) {
    let key = Buffer.from(DES_KEY, 'base64');
    return encryptX(data, key, NULL_IV);
}

function decryptX(data, key, iv) {
    try {
        let algo = 'des-ede3-cbc';
        let cipher = crypto.createDecipheriv(algo, key, iv);
        let encoding = 'base64';
        let result = cipher.update(data, encoding, "utf8");
        result += cipher.final("utf8");
        return result;
    } catch (err) {
        console.log(err)
        return null;
    }
}

function decrypt(data) {
    let key = Buffer.from(DES_KEY, 'base64');
    return decryptX(data, key, NULL_IV);
}

function buildSignature(name) {
    return crypto.createHash('md5').update(name+MD5_KEY).digest("base64");
}

function isValidSign(queryParams, reqSign) {
    let data = encrypt(queryParams, DES_KEY, NULL_IV);
    let sign = buildSignature(data + MD5_KEY);
    console.log('queryParams : ',queryParams)
    console.log('data : ',data)
    console.log('sign : ', sign)
    console.log('reqSign : ', reqSign)
    return sign === reqSign;
}




function createClient(username, callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    let random = generateRandomNumber();


    let queryString = querystring.stringify({
        random: random,
        agent: AGENT_NAME,
        client: username.toLowerCase(),
        password: 999999,
        vipHandicaps: 12,
        orHandicaps: '1',
        orHallRebate: 0,
        laxHallRebate: 0,
        lstHallRebate: 0,
        extraMark: ''
    });


    console.log(queryString)
    let data = encrypt(queryString);
    let sign = buildSignature(data);

    let options = {
        url: API_URL +'/create_client?' + querystring.stringify({
            propertyId: PROPERTY_ID,
            data: data,
            sign: sign
        }),
        method: 'GET',
        headers: headers
    };

    console.log(options)
    request(options, (error, response, body) => {
        // console.log(response,body,error)
        if (error) {
            callback('', null);
        } else {
            let json = JSON.parse(body);

            // console.log(body)
            callback(null, json)
        }
    });
}


function forwardGameFunction(userInfo, random, callback) {


    let queryString = querystring.stringify({
        random: random,
        client: userInfo.username_lower,
        password: 999999,
        gameHall: 100,
        language: 'th'
    });

    let data = encrypt(queryString);
    let sign = buildSignature(data);

    let options = {
        url: API_URL  + 'forward_game?' + querystring.stringify({
            propertyId: PROPERTY_ID,
            data: data,
            sign: sign
        }),
        method: 'GET',
        headers: {'Content-Type': 'application/json'}
    };

    console.log(options)
    request(options, (error, response, body) => {
        // console.log(response,body,error)
        if (error) {
            callback(err, null);
        } else {
            console.log(body)
            let json = JSON.parse(body);

            callback(null, json);
        }
    });
}

function modifyClient(username,limitA,limitB,limitC,callback) {
    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };

    let apiName = 'modify_client';


    let random = generateRandomNumber();

    let handicapList = [];
    if(limitA){
        handicapList.push(1);
    }
    if(limitB){
        handicapList.push(31);
    }
    if(limitC){
        handicapList.push(34);
    }

    //A 1 , C2 31, F2 34   VIP_O 12
    let queryString = querystring.stringify({
        client: username,
        random: random,
        orHandicaps: handicapList.join(','),
        vipHandicaps: 12
    });


    console.log(queryString)
    let data = encrypt(queryString);
    let sign = buildSignature(data);

    let options = {
        url: API_URL + apiName + '?' + querystring.stringify({
            propertyId: PROPERTY_ID,
            data: data,
            sign: sign
        }),
        method: 'GET',
        headers: headers
    };

    console.log(options)
    request(options, (error, response, body) => {
        // console.log(response,body,error)
        if (error) {
            callback('', null);
        } else {
            console.log(body)
            let json = JSON.parse(body);

            // console.log(body)
            callback(null, json)
        }
    });
}

module.exports = {getGameTypeDetail,getCommissionDetail,getBetTypeDetail,modifyClient,encrypt,decrypt,buildSignature,isValidSign,generateRandomNumber,createClient,forwardGameFunction}
