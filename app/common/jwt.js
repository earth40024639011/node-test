const config = require('config');
const jwt    = require('jsonwebtoken');

module.exports = {
    generateToken: (data) => {
        return  jwt.sign({
            data: data
        }, "asda12312312", {expiresIn: "100000h"});
    },
    generateRefreshToken: (data) => {
        return  jwt.sign({
            data: data
        }, "asda12312312", {expiresIn: "100000h"});
    },
    verify : function (token) {
        if(token.split(' ').length > 1) {
            return jwt.verify(token.split(' ')[1], "asda12312312")
        }
        return jwt.verify(token, "asda12312312")
    }
};
