const MemberModel = require('../models/member.model');
const request = require('request');
const _ = require('underscore');
const mongoose = require('mongoose');
const BetTransactionModel = require('../models/betTransaction.model.js');
const async = require("async");
const moment = require('moment');
const querystring = require('querystring');
const Crypto = require("crypto");
const AgentGroupModel = require('../models/agentGroup.model.js');
const MemberService = require('../common/memberService');
const DateUtils = require('../common/dateUtils');
const NumberUtils = require('../common/numberUtils1');
const AgentService = require('../common/agentService');
const roundTo = require('round-to');


const getClientKey = require('../controllers/getClientKey');
const {
    SLOT_XO_APP_ID = 'TFB8',
    SLOT_XO_HOST = 'http://api688.net/seamless',
    SLOT_XO_FORWARD_GAME = 'http://www.gwc688.net/playGame?',
    SLOT_XO_SECRET_KEY = 'qc8rx7anfkqn4'
} = getClientKey.getKey();

const CLIENT_NAME         = process.env.CLIENT_NAME          || 'SPORTBOOK88';

// if (!_.isEqual(CLIENT_NAME, 'SPORTBOOK88')) {
//     SLOT_XO_APP_ID = process.env.SLOT_XO_APP_ID || 'TFB8';
//     SLOT_XO_HOST = process.env.SLOT_XO_HOST || 'http://api688.net/seamless';
//     SLOT_XO_FORWARD_GAME = process.env.SLOT_XO_FORWARD_GAME || 'http://www.gwc688.net/playGame?';
//     SLOT_XO_SECRET_KEY = process.env.SLOT_XO_SECRET_KEY || 'qc8rx7anfkqn4';
// }

const DRAW = "DRAW";
const WIN = "WIN";
const HALF_WIN = "HALF_WIN";
const LOSE = "LOSE";
const HALF_LOSE = "HALF_LOSE";



function createUser(username, callback) {


    let method = 'CU';

    let timestamp = moment().unix();

    let parameters = `Method=${method}&Timestamp=${timestamp}&Username=${username}`;

    generateSignature(parameters, (err, data) => {

        if (err) {
            callback(err, null);
            return;
        }
        const headers = {
            'Content-Type': 'application/x-www-form-urlencoded'
        };

        let form = {
            Method: method,
            Timestamp: timestamp,
            Username: username
        };

        const option = {
            url: `${SLOT_XO_HOST}?AppID=${SLOT_XO_APP_ID}&Signature=${encodeURIComponent(data)}`,
            method: 'POST',
            headers: headers,
            body: querystring.stringify(form)
        };

        request(option, (err, response, body) => {
            if (err) {
                callback(err, null);
            } else {

                let responseBody = JSON.parse(response.body);

                console.log(responseBody)

                try {
                    if (response.statusCode == 200 && (responseBody.Status === 'Created' || responseBody.Status === 'OK' )){
                        callback(null, responseBody);
                    } else {
                        callback(responseBody, null);
                    }
                } catch (e) {
                    callback(e, null);
                }
            }
        });
    })
}

function transferCredit(username, amount, callback) {


    let method = 'TC';

    let timestamp = moment().unix();

    let parameters = `Amount=${amount}&Method=${method}&Timestamp=${timestamp}&Username=${username}`;

    generateSignature(parameters, (err, data) => {

        if (err) {
            callback(err, null);
            return;
        }
        const headers = {
            'Content-Type': 'application/x-www-form-urlencoded'
        };

        let form = {
            Amount: amount,
            Method: method,
            Timestamp: timestamp,
            Username: username
        };

        const option = {
            url: `${SLOT_XO_HOST}?AppID=${SLOT_XO_APP_ID}&Signature=${encodeURIComponent(data)}`,
            method: 'POST',
            headers: headers,
            body: querystring.stringify(form)
        };

        request(option, (err, response, body) => {

            if (err) {
                callback(err, null);
            } else {

                let responseBody = JSON.parse(response.body);

                try {
                    if (response.statusCode === 200) {

                        transferTransaction(username,amount,(err,response)=>{
                            callback(null, responseBody);
                        });

                    } else {
                        callback(responseBody.Message, null);
                    }
                } catch (e) {
                    callback(e, null);
                }
            }
        });
    })
}

function getCredit(username, callback) {


    let method = 'GC';

    let timestamp = moment().unix();

    let parameters = `Method=${method}&Timestamp=${timestamp}&Username=${username}`;

    generateSignature(parameters, (err, data) => {

        if (err) {
            callback(err, null);
            return;
        }
        const headers = {
            'Content-Type': 'application/x-www-form-urlencoded'
        };

        let form = {
            Method: method,
            Timestamp: timestamp,
            Username: username
        };

        const option = {
            url: `${SLOT_XO_HOST}?AppID=${SLOT_XO_APP_ID}&Signature=${encodeURIComponent(data)}`,
            method: 'POST',
            headers: headers,
            body: querystring.stringify(form)
        };

        request(option, (err, response, body) => {
            if (err) {
                callback(err, null);
            } else {

                let responseBody = JSON.parse(response.body);

                try {
                    if (response.statusCode === 200) {
                        callback(null, responseBody.Credit);
                    } else {
                        callback(responseBody, null);
                    }
                } catch (e) {
                    callback(e, null);
                }
            }
        });
    })
}



function getRetrieveTransactions(username,startDate,endDate,nextId, callback) {


    let method = 'TS';

    let timestamp = moment().unix();

    let parameters = '';
    let form = {};

    if (nextId) {
        parameters = `EndDate=${endDate}&Method=${method}&NextId=${nextId}&StartDate=${startDate}&Timestamp=${timestamp}`;

        form = {
            Method: method,
            StartDate: startDate,
            NextId: nextId,
            EndDate: endDate,
            Timestamp: timestamp
        }
    } else {
        parameters = `EndDate=${endDate}&Method=${method}&StartDate=${startDate}&Timestamp=${timestamp}`;

        form = {
            Method: method,
            StartDate: startDate,
            EndDate: endDate,
            Timestamp: timestamp
        }
    }

    generateSignature(parameters, (err, data) => {

        if (err) {
            callback(err, null);
            return;
        }
        const headers = {
            'Content-Type': 'application/x-www-form-urlencoded'
        };

        const option = {
            url: `${SLOT_XO_HOST}?AppID=${SLOT_XO_APP_ID}&Signature=${encodeURIComponent(data)}`,
            method: 'POST',
            headers: headers,
            body: querystring.stringify(form)
        };

        request(option, (err, response, body) => {
            if (err) {
                callback(err, null);
            } else {

                let responseBody = JSON.parse(response.body);

                try {
                    if (response.statusCode === 200) {
                        callback(null, responseBody);
                    } else {
                        callback(responseBody, null);
                    }
                } catch (e) {
                    callback(e, null);
                }
            }
        });
    })
}




function getTotalTransactions(username,startDate,endDate, callback) {

    let method = 'TRX';

    let timestamp = moment().unix();
    let parameters = '';
    let form = {};

    if (username) {
        parameters = `EndDate=${endDate}&Method=${method}&StartDate=${startDate}&Timestamp=${timestamp}&Username=${username}`;

        form = {
            Method: method,
            Timestamp: timestamp,
            Username: username,
            StartDate: startDate,
            EndDate: endDate
        }
    } else {
        parameters = `EndDate=${endDate}&Method=${method}&StartDate=${startDate}&Timestamp=${timestamp}`;

        form = {
            Method: method,
            Timestamp: timestamp,
            StartDate: startDate,
            EndDate: endDate
        }
    }

    generateSignature(parameters, (err, data) => {

        const headers = {
            'Content-Type': 'application/x-www-form-urlencoded'
        };


        const option = {
            url: `${SLOT_XO_HOST}?AppID=${SLOT_XO_APP_ID}&Signature=${encodeURIComponent(data)}`,
            method: 'POST',
            headers: headers,
            body: querystring.stringify(form)
        };

        request(option, (err, response, body) => {
            if (err) {
                callback(err, null);
            } else {

                let responseBody = JSON.parse(response.body);

                try {
                    if (response.statusCode === 200) {
                        callback(null, responseBody);
                    } else {
                        callback(responseBody, null);
                    }
                } catch (e) {
                    callback(e, null);
                }
            }
        });
    })
}

function transferTransaction(username, amount, callback) {


    const winLose = amount * -1;
    async.waterfall([callback => {

        MemberService.findByUserName(username, function (err, memberInfo) {
            if (err)
                callback(err, null);
            else
                callback(null, memberInfo);
        });

    }, (memberInfo, callback) => {

        AgentGroupModel.findById(memberInfo.group).select('_id type parentId shareSetting commissionSetting').populate({
            path: 'parentId',
            select: '_id type parentId shareSetting commissionSetting name',
            populate: {
                path: 'parentId',
                select: '_id type parentId shareSetting commissionSetting name',
                populate: {
                    path: 'parentId',
                    select: '_id type parentId shareSetting commissionSetting name',
                    populate: {
                        path: 'parentId',
                        select: '_id type parentId shareSetting commissionSetting name',
                        populate: {
                            path: 'parentId',
                            select: '_id type parentId shareSetting commissionSetting name',
                            populate: {
                                path: 'parentId',
                                select: '_id type parentId shareSetting commissionSetting name',
                            }
                        }
                    }
                }
            }

        }).exec(function (err, response) {
            if (err)
                callback(err, null);
            else
                callback(null, memberInfo, response);
        });

    }], function (err, memberInfo, agentGroups) {


        let body = {
            memberId: memberInfo._id,
            memberParentGroup: memberInfo.group._id,
            betId: generateBetId(),
            amount: Math.abs(winLose),
            memberCredit: winLose * -1,
            payout: Math.abs(winLose),
            gameType: 'TODAY',
            acceptHigherPrice: false,
            priceType: 'TH',
            game: 'GAME',
            source: 'SLOT_XO',
            status: 'DONE',
            commission: {},
            gameDate: DateUtils.getCurrentDate(),
            createdDate: DateUtils.getCurrentDate(),
            currency : memberInfo.currency
            // ipAddress: req.body.ip
        };


        prepareCommission(memberInfo, body, agentGroups, null, 0, 0);

        generateBetId(function (betId) {
            body.betId = betId;
        });


        //init
        if (!body.commission.senior) {
            body.commission.senior = {};
        }

        if (!body.commission.masterAgent) {
            body.commission.masterAgent = {};
        }
        if (!body.commission.agent) {
            body.commission.agent = {};
        }


        let betResult = winLose > 0 ? WIN : winLose < 0 ? LOSE : DRAW;

        const shareReceive = AgentService.prepareShareReceive(winLose, betResult, body);

        body.commission.superAdmin.winLoseCom = shareReceive.superAdmin.winLoseCom;
        body.commission.superAdmin.winLose = shareReceive.superAdmin.winLose;
        body.commission.superAdmin.totalWinLoseCom = shareReceive.superAdmin.totalWinLoseCom;

        body.commission.company.winLoseCom = shareReceive.company.winLoseCom;
        body.commission.company.winLose = shareReceive.company.winLose;
        body.commission.company.totalWinLoseCom = shareReceive.company.totalWinLoseCom;

        body.commission.shareHolder.winLoseCom = shareReceive.shareHolder.winLoseCom;
        body.commission.shareHolder.winLose = shareReceive.shareHolder.winLose;
        body.commission.shareHolder.totalWinLoseCom = shareReceive.shareHolder.totalWinLoseCom;

        body.commission.senior.winLoseCom = shareReceive.senior.winLoseCom;
        body.commission.senior.winLose = shareReceive.senior.winLose;
        body.commission.senior.totalWinLoseCom = shareReceive.senior.totalWinLoseCom;

        body.commission.masterAgent.winLoseCom = shareReceive.masterAgent.winLoseCom;
        body.commission.masterAgent.winLose = shareReceive.masterAgent.winLose;
        body.commission.masterAgent.totalWinLoseCom = shareReceive.masterAgent.totalWinLoseCom;

        body.commission.agent.winLoseCom = shareReceive.agent.winLoseCom;
        body.commission.agent.winLose = shareReceive.agent.winLose;
        body.commission.agent.totalWinLoseCom = shareReceive.agent.totalWinLoseCom;

        body.commission.member.winLoseCom = shareReceive.member.winLoseCom;
        body.commission.member.winLose = shareReceive.member.winLose;
        body.commission.member.totalWinLoseCom = shareReceive.member.totalWinLoseCom;
        body.validAmount = Math.abs(winLose);
        body.isEndScore = true;


        let model = new BetTransactionModel(body);
        model.save(function (err, betResponse) {

            if (err) {
                callback(err, null);
            } else {
                updateAgentBalance(shareReceive,function (err,response) {
                    if(err){
                        callback(err, null);
                    }else {
                        callback(null, betResponse);
                    }
                });
            }

        });

    });

}

function updateAgentBalance(shareReceive, updateCallback) {

    console.log('updateAgentMemberBalance');
    console.log('shareReceive : ', shareReceive);
    async.series([callback => {
        let updateWinLoseAgentList = [];

        updateWinLoseAgentList.push({
            group: shareReceive.superAdmin.group,
            amount: shareReceive.superAdmin.totalWinLoseCom
        });
        updateWinLoseAgentList.push({
            group: shareReceive.company.group,
            amount: (shareReceive.superAdmin.totalWinLoseCom * -1 )
        });
        updateWinLoseAgentList.push({
            group: shareReceive.shareHolder.group,
            amount: (shareReceive.superAdmin.totalWinLoseCom * -1 ) + (shareReceive.company.totalWinLoseCom * -1)
        });
        updateWinLoseAgentList.push({
            group: shareReceive.senior.group,
            amount: (shareReceive.superAdmin.totalWinLoseCom * -1 ) + (shareReceive.company.totalWinLoseCom * -1 ) + (shareReceive.shareHolder.totalWinLoseCom * -1)
        });
        updateWinLoseAgentList.push({
            group: shareReceive.masterAgent.group,
            amount: (shareReceive.superAdmin.totalWinLoseCom * -1 ) + (shareReceive.company.totalWinLoseCom * -1 ) + (shareReceive.shareHolder.totalWinLoseCom * -1) + (shareReceive.senior.totalWinLoseCom * -1)
        });
        updateWinLoseAgentList.push({
            group: shareReceive.agent.group,
            amount: (shareReceive.superAdmin.totalWinLoseCom * -1 ) + (shareReceive.company.totalWinLoseCom * -1 ) + (shareReceive.shareHolder.totalWinLoseCom * -1) + (shareReceive.senior.totalWinLoseCom * -1) + (shareReceive.masterAgent.totalWinLoseCom * -1)
        });

        console.log("updateWinLoseAgent");
        async.each(updateWinLoseAgentList, (agentBalance, callbackWL) => {
            if (agentBalance.group && agentBalance.amount !== 0) {
                AgentGroupModel.findOneAndUpdate({_id: agentBalance.group}, {$inc: {balance: agentBalance.amount}})
                    .exec(function (err, creditResponse) {
                        if (err) {
                            callbackWL(err);
                        }
                        else {
                            callbackWL();
                        }
                    });
            } else {
                callbackWL();
            }
        }, (err) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, '');
            }
        });
    }], function (err, response) {
        if (err) {
            updateCallback(err, null);
        } else {
            updateCallback(null, response);
        }
    });
}


function prepareCommission(memberInfo, body, currentAgent, overAgent, remaining, overAllShare) {


    if (currentAgent && (currentAgent.type === 'SUPER_ADMIN' || currentAgent.parentId)) {


        console.log('==============================================');
        console.log('process : ' + currentAgent.type + ' : ' + currentAgent.name);


        let money = body.amount;


        if (!overAgent) {
            overAgent = {};
            overAgent.type = 'member';
            overAgent.shareSetting = {};
            overAgent.shareSetting.game = {};
            overAgent.shareSetting.game.slotXO = {};
            overAgent.shareSetting.game.slotXO.parent = memberInfo.shareSetting.game.slotXO.parent;
            overAgent.shareSetting.game.slotXO.own = 0;
            overAgent.shareSetting.game.slotXO.remaining = 0;

            body.commission.member = {};
            body.commission.member.parent = memberInfo.shareSetting.game.slotXO.parent;
            body.commission.member.commission = memberInfo.commissionSetting.game.slotXO;

        }

        console.log('');
        console.log('---- setting ----');
        console.log('ส่วนแบ่งที่ได้มามีทั้งหมด : ' + currentAgent.shareSetting.game.slotXO.own + ' %');
        console.log('ขอส่วนแบ่งจาก : ' + overAgent.type + ' ' + overAgent.shareSetting.game.slotXO.parent + ' %');
        console.log('ให้ส่วนแบ่ง : ' + overAgent.type + ' ที่เหลือไป ' + overAgent.shareSetting.game.slotXO.own + ' %');
        console.log('remaining จาก : ' + overAgent.type + ' : ' + overAgent.shareSetting.game.slotXO.remaining + ' %');
        console.log('โดนบังคับเสียขั้นต่ำ : ' + currentAgent.shareSetting.game.slotXO.min + ' %');
        console.log('---- end setting ----');
        console.log('');


        // console.log('และได้รับ remaining ไว้ : '+overAgent.shareSetting.game.slotXO.remaining+' %');

        let currentPercentReceive = overAgent.shareSetting.game.slotXO.parent;
        console.log('% ที่ได้ : ' + currentPercentReceive);
        console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');
        let totalRemaining = remaining;

        if (overAgent.shareSetting.game.slotXO.remaining > 0) {
            // console.log()
            // console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');

            if (overAgent.shareSetting.game.slotXO.remaining > totalRemaining) {
                console.log('รับ remaining ทั้งหมดไว้: ' + totalRemaining + ' %');
                currentPercentReceive += totalRemaining;
                totalRemaining = 0;
            } else {
                totalRemaining = remaining - overAgent.shareSetting.game.slotXO.remaining;
                console.log('หักค่า remaining ที่ได้รับมากับที่เซตรับไว้ = ' + remaining + ' - ' + overAgent.shareSetting.game.slotXO.remaining + ' = ' + totalRemaining);
                currentPercentReceive += overAgent.shareSetting.game.slotXO.remaining;
            }

        }

        console.log('รวม % ที่ได้รับ : ' + currentPercentReceive);


        let unUseShare = currentAgent.shareSetting.game.slotXO.own -
            (overAgent.shareSetting.game.slotXO.parent + overAgent.shareSetting.game.slotXO.own);

        console.log('เหลือส่วนแบ่งที่ไม่ได้ใช้ : ' + unUseShare + ' %');
        totalRemaining = (unUseShare + totalRemaining);
        console.log('ส่วนแบ่งที่ไม่ได้ใช้ บวกกับค่า remaining ท่ี่เหลือจะเป็น : ' + totalRemaining + ' %');

        console.log('จากที่โดนบังคับเสียขั้นต่ำ : ' + currentAgent.shareSetting.game.slotXO.min + ' %');
        console.log('overAllShare : ', overAllShare);
        if (currentAgent.shareSetting.game.slotXO.min > 0 && ((overAllShare + currentPercentReceive) < currentAgent.shareSetting.game.slotXO.min)) {

            if (currentPercentReceive < currentAgent.shareSetting.game.slotXO.min) {
                console.log('% ที่ได้รับ < ขั้นต่ำที่ถูกตั้ง min ไว้');
                let percent = currentAgent.shareSetting.game.slotXO.min - (currentPercentReceive + overAllShare);
                console.log('หักออกไป ' + percent + ' % : แล้วไปเพิ่มในส่วนที่ต้องรับผิดชอบ ');
                totalRemaining = totalRemaining - percent;
                if (totalRemaining < 0) {
                    totalRemaining = 0;
                }
                currentPercentReceive += percent;
            }
        } else {
            // currentPercentReceive += totalRemaining;
        }


        if (currentAgent.type === 'SUPER_ADMIN') {
            currentPercentReceive += totalRemaining;
        }

        let getMoney = (money * NumberUtils.convertPercent(currentPercentReceive)).toFixed(2);
        overAllShare = overAllShare + currentPercentReceive;

        console.log('ยอดแทง ' + money + ' ได้ทั้งหมด : ' + currentPercentReceive + ' %');

        console.log('รวม % + % remaining  จะเป็นสัดส่วนที่ได้ทั้งหมด = ' + getMoney);
        console.log('ค่าที่จะถูกคืนกลับไป (remaining) : ' + totalRemaining + ' %');

        console.log('จำนวน % ที่ถูกแบ่งไปแล้วทั้งหมด : ', overAllShare);

        //set commission
        let agentCommission = currentAgent.commissionSetting.game.slotXO;


        switch (currentAgent.type) {
            case 'SUPER_ADMIN':
                body.commission.superAdmin = {};
                body.commission.superAdmin.group = currentAgent._id;
                body.commission.superAdmin.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.superAdmin.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.superAdmin.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.superAdmin.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.superAdmin.shareReceive = Math.abs(currentPercentReceive);
                body.commission.superAdmin.commission = agentCommission;
                body.commission.superAdmin.amount = getMoney;
                break;
            case 'COMPANY':
                body.commission.company = {};
                body.commission.company.parentGroup = currentAgent.parentId;
                body.commission.company.group = currentAgent._id;
                body.commission.company.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.company.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.company.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.company.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.company.shareReceive = Math.abs(currentPercentReceive);
                body.commission.company.commission = agentCommission;
                body.commission.company.amount = getMoney;
                break;
            case 'SHARE_HOLDER':
                body.commission.shareHolder = {};
                body.commission.shareHolder.parentGroup = currentAgent.parentId;
                body.commission.shareHolder.group = currentAgent._id;
                body.commission.shareHolder.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.shareHolder.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.shareHolder.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.shareHolder.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.shareHolder.shareReceive = currentPercentReceive;
                body.commission.shareHolder.commission = agentCommission;
                body.commission.shareHolder.amount = getMoney;
                break;
            case 'SENIOR':
                body.commission.senior = {};
                body.commission.senior.parentGroup = currentAgent.parentId;
                body.commission.senior.group = currentAgent._id;
                body.commission.senior.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.senior.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.senior.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.senior.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.senior.shareReceive = currentPercentReceive;
                body.commission.senior.commission = agentCommission;
                body.commission.senior.amount = getMoney;
                break;
            case 'MASTER_AGENT':
                body.commission.masterAgent = {};
                body.commission.masterAgent.parentGroup = currentAgent.parentId;
                body.commission.masterAgent.group = currentAgent._id;
                body.commission.masterAgent.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.masterAgent.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.masterAgent.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.masterAgent.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.masterAgent.shareReceive = currentPercentReceive;
                body.commission.masterAgent.commission = agentCommission;
                body.commission.masterAgent.amount = getMoney;
                break;
            case 'AGENT':
                body.commission.agent = {};
                body.commission.agent.parentGroup = currentAgent.parentId;
                body.commission.agent.group = currentAgent._id;
                body.commission.agent.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.agent.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.agent.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.agent.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.agent.shareReceive = currentPercentReceive;
                body.commission.agent.commission = agentCommission;
                body.commission.agent.amount = getMoney;
                break;
        }

        prepareCommission(memberInfo, body, currentAgent.parentId, currentAgent, totalRemaining, overAllShare);
    }
}




function generateSignature(parameters, callback) {
    console.log('parameters ==========> ', parameters);

    let key = new Buffer(SLOT_XO_SECRET_KEY, 'utf8');
    let hmac = Crypto.createHmac("sha1", key);
    let hash2 = hmac.update(parameters, 'utf8');
    let digest = hash2.digest("base64");

    callback(null, digest)
}

function buildSignature11(val) {
    return Crypto.createHash('md5').update(val).digest("hex");
}

function forwardToGame(token, gameCode, language, redirectUrl, isMobile, callback) {
    let url = `${SLOT_XO_FORWARD_GAME}token=${token}&appID=${SLOT_XO_APP_ID}&gameCode=${gameCode}&language=${language}&mobile=${isMobile}&redirectUrl=${redirectUrl}`;
    callback(null, url);
}

function gameList(callback) {
    let timestamp = moment().unix();
    let parameters = `appid=${SLOT_XO_APP_ID}&timestamp=${timestamp}`+SLOT_XO_SECRET_KEY;

    console.log('parameter ==== ',parameters);

    let hash = buildSignature11(parameters);

    const headers = {
        'Content-Type': 'application/json'
    };

    let form = {
        AppID: SLOT_XO_APP_ID,
        Hash : hash,
        Timestamp: timestamp
    };

    console.log(SLOT_XO_HOST, '------------ ',form)

    // let x = {"Error":"0","Description":"OK","ListGames":[{"GameType":"Fishing","GameCode":"xkhy6baryz7xs","GameName":"Fish Hunter 2 EX - Newbie","GameAlias":"FH2R0","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312809,"DefaultWidth":1280,"DefaultHeight":720,"Image1":"//img.gwccdn.net/gameimages/landscape/xkhy6baryz7xs.png"},{"GameType":"Fishing","GameCode":"qq5ocdypyeboy","GameName":"Fish Hunter 2 EX - Novice","GameAlias":"FH2R1","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312809,"DefaultWidth":1280,"DefaultHeight":720,"Image1":"//img.gwccdn.net/gameimages/landscape/qq5ocdypyeboy.png"},{"GameType":"Fishing","GameCode":"g54rso4yefdrq","GameName":"Fish Hunter 2 EX - Pro","GameAlias":"FH2R2","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312809,"DefaultWidth":1280,"DefaultHeight":720,"Image1":"//img.gwccdn.net/gameimages/landscape/g54rso4yefdrq.png"},{"GameType":"Fishing","GameCode":"ary5bxi9z165r","GameName":"Fish Hunter 2 EX - My Club","GameAlias":"FH2R3","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312809,"DefaultWidth":1280,"DefaultHeight":720,"Image1":"//img.gwccdn.net/gameimages/landscape/ary5bxi9z165r.png"},{"GameType":"Fishing","GameCode":"b8rzo7uzqt4sw","GameName":"Fish Hunting: Golden Toad","GameAlias":"FSGF","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312809,"DefaultWidth":1280,"DefaultHeight":720,"Image1":"//img.gwccdn.net/gameimages/landscape/b8rzo7uzqt4sw.png"},{"GameType":"Fishing","GameCode":"8d7r1okge7nrk","GameName":"Fish Hunting: Da Sheng Nao Hai","GameAlias":"FSGF1","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312809,"DefaultWidth":1280,"DefaultHeight":720,"Image1":"//img.gwccdn.net/gameimages/landscape/8d7r1okge7nrk.png"},{"GameType":"Fishing","GameCode":"nzkseaudcbosc","GameName":"Fish Hunting: Li Kui Pi Yu","GameAlias":"FSGF2","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312809,"DefaultWidth":1280,"DefaultHeight":720,"Image1":"//img.gwccdn.net/gameimages/landscape/nzkseaudcbosc.png"},{"GameType":"Fishing","GameCode":"wi17jwsu4de7c","GameName":"Fish Hunting: Yao Qian Shu","GameAlias":"FishMoneyTree","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312809,"DefaultWidth":1280,"DefaultHeight":720,"Image1":"//img.gwccdn.net/gameimages/landscape/wi17jwsu4de7c.png"},{"GameType":"Fishing","GameCode":"1jeqx59c7ztqg","GameName":"Fish Hunter Monster Awaken","GameAlias":"FHMA","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-102,"DefaultWidth":1280,"DefaultHeight":720,"Image1":"//img.gwccdn.net/gameimages/landscape/1jeqx59c7ztqg.png"},{"GameType":"Fishing","GameCode":"4omkmmpnwqokn","GameName":"Fish Hunter Spongebob","GameAlias":"FishSpongebob","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312809,"DefaultWidth":1280,"DefaultHeight":720,"Image1":"//img.gwccdn.net/gameimages/landscape/4omkmmpnwqokn.png"},{"GameType":"Fishing","GameCode":"st5cmuqnaxycn","GameName":"Fish Hunting: Happy Fish 5","GameAlias":"FishHunterHappyFish5","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312809,"DefaultWidth":1280,"DefaultHeight":720,"Image1":"//img.gwccdn.net/gameimages/landscape/st5cmuqnaxycn.png"},{"GameType":"Fishing","GameCode":"ddpg1amgc71gk","GameName":"Insect Paradise","GameAlias":"FishHunterInsectParadise","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312809,"DefaultWidth":1280,"DefaultHeight":720,"Image1":"//img.gwccdn.net/gameimages/landscape/ddpg1amgc71gk.png"},{"GameType":"Fishing","GameCode":"p63ornyjba8oa","GameName":"Fishermans Wharf","GameAlias":"FishermansWharf","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312809,"DefaultWidth":1280,"DefaultHeight":720,"Image1":"//img.gwccdn.net/gameimages/landscape/p63ornyjba8oa.png"},{"GameType":"Fishing","GameCode":"xq9ohbyf9m79o","GameName":"Bird Paradise","GameAlias":"FishHunterBirdHunter","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312809,"DefaultWidth":1280,"DefaultHeight":720,"Image1":"//img.gwccdn.net/gameimages/landscape/xq9ohbyf9m79o.png"},{"GameType":"Fishing","GameCode":"kk8nqm3cfwtng","GameName":"Fish Haiba","GameAlias":"FHBH5","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-2703,"DefaultWidth":1280,"DefaultHeight":720,"Image1":"//img.gwccdn.net/gameimages/landscape/kk8nqm3cfwtng.png"},{"GameType":"Slot","GameCode":"fwria11mjbrwh","GameName":"Three Kingdoms Quest","GameAlias":"Sanguo","Specials":"hot","SupportedPlatForms":"Desktop,Mobile","Order":40,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/fwria11mjbrwh.png"},{"GameType":"Slot","GameCode":"8rqwot18etnuw","GameName":"Thunder God","GameAlias":"Leishen","Specials":"hot","SupportedPlatForms":"Desktop,Mobile","Order":45,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/8rqwot18etnuw.png"},{"GameType":"Slot","GameCode":"kf41ymtxfos1r","GameName":"Ocean Paradise","GameAlias":"Dolphin","Specials":"","SupportedPlatForms":"Desktop,Mobile","Order":51,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/kf41ymtxfos1r.png"},{"GameType":"Slot","GameCode":"ebudnqj68h6d4","GameName":"Happy Party","GameAlias":"HappyParty","Specials":"","SupportedPlatForms":"Desktop,Mobile","Order":52,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/ebudnqj68h6d4.png"},{"GameType":"Slot","GameCode":"xtpy4bx49xhx1","GameName":"African","GameAlias":"African","Specials":"","SupportedPlatForms":"Desktop,Mobile","Order":95,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/xtpy4bx49xhx1.png"},{"GameType":"Slot","GameCode":"1q36p58phmt6y","GameName":"Genie","GameAlias":"Aladdin","Specials":"","SupportedPlatForms":"Desktop,Mobile","Order":94,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/1q36p58phmt6y.png"},{"GameType":"Slot","GameCode":"xico36z59dh9h","GameName":"Laura Quest","GameAlias":"Laura","Specials":"","SupportedPlatForms":"Desktop,Mobile","Order":1009,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/xico36z59dh9h.png"},{"GameType":"Slot","GameCode":"dhdirsn3m3xia","GameName":"Lucky God","GameAlias":"GodOfFortune","Specials":"hot","SupportedPlatForms":"Desktop,Mobile","Order":50,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/dhdirsn3m3xia.png"},{"GameType":"Slot","GameCode":"rh8iwwntk3mie","GameName":"Dolphin Reef","GameAlias":"DolphinReef","Specials":"","SupportedPlatForms":"Desktop,Mobile","Order":326,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/rh8iwwntk3mie.png"},{"GameType":"Slot","GameCode":"axt5pxf7sk35y","GameName":"Highway Kings","GameAlias":"HighwayKings","Specials":"","SupportedPlatForms":"Desktop,Mobile","Order":327,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/axt5pxf7sk35y.png"},{"GameType":"Slot","GameCode":"69xaiyrbo4dae","GameName":"A Night Out","GameAlias":"ANightOut","Specials":"","SupportedPlatForms":"Desktop,Mobile","Order":300,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/69xaiyrbo4dae.png"},{"GameType":"Slot","GameCode":"oqt9p9876m39y","GameName":"Azteca","GameAlias":"Azteca","Specials":"","SupportedPlatForms":"Desktop,Mobile","Order":332,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/oqt9p9876m39y.png"},{"GameType":"Slot","GameCode":"jbzd1cjsgh4dk","GameName":"Sparta","GameAlias":"Sparta","Specials":"","SupportedPlatForms":"Desktop,Mobile","Order":331,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/jbzd1cjsgh4dk.png"},{"GameType":"Slot","GameCode":"4d5kdkpqi6sk4","GameName":"Safari Heat","GameAlias":"SafariHeat","Specials":"","SupportedPlatForms":"Desktop,Mobile","Order":90,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/4d5kdkpqi6sk4.png"},{"GameType":"Slot","GameCode":"u6d7fsg355x7a","GameName":"Panther Moon","GameAlias":"PantherMoon","Specials":"","SupportedPlatForms":"Desktop,Mobile","Order":91,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/u6d7fsg355x7a.png"},{"GameType":"Slot","GameCode":"t656f48j75z6a","GameName":"Great Blue","GameAlias":"GreatBlue","Specials":"","SupportedPlatForms":"Desktop,Mobile","Order":334,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/t656f48j75z6a.png"},{"GameType":"Slot","GameCode":"z1pc5tp4zqhm1","GameName":"Silver Bullet","GameAlias":"SilverBullet","Specials":null,"SupportedPlatForms":"Desktop,Mobile","Order":300,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/z1pc5tp4zqhm1.png"},{"GameType":"Slot","GameCode":"bes8675wqiigs","GameName":"Captain's Treasure","GameAlias":"CaptainsTreasure","Specials":null,"SupportedPlatForms":"Desktop,Mobile","Order":300,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/bes8675wqiigs.png"},{"GameType":"Slot","GameCode":"bwwza4umpbwsh","GameName":"Bonus Bear","GameAlias":"BonusBear","Specials":null,"SupportedPlatForms":"Desktop,Mobile","Order":300,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/bwwza4umpbwsh.png"},{"GameType":"Slot","GameCode":"s6xhiogba5dhe","GameName":"Football","GameAlias":"Football","Specials":null,"SupportedPlatForms":"Desktop,Mobile","Order":350,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/s6xhiogba5dhe.png"},{"GameType":"Slot","GameCode":"5864tji8w113w","GameName":"Thai Paradise","GameAlias":"ThaiParadise","Specials":null,"SupportedPlatForms":"Desktop,Mobile","Order":92,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/5864tji8w113w.png"},{"GameType":"Slot","GameCode":"qieoeyodyyyoc","GameName":"Captain's Treasure Pro","GameAlias":"CaptainsTreasurePro","Specials":null,"SupportedPlatForms":"Desktop,Mobile","Order":300,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/qieoeyodyyyoc.png"},{"GameType":"Slot","GameCode":"79mafnrjt48aa","GameName":"Pan Jin Lian","GameAlias":"PanJinLian","Specials":"hot,new","SupportedPlatForms":"Desktop,Mobile","Order":30,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/79mafnrjt48aa.png"},{"GameType":"Slot","GameCode":"k3anse3yrrunq","GameName":"MoneyBangBang","GameAlias":"MoneyBangBang","Specials":"hot,new","SupportedPlatForms":"Desktop,Mobile","Order":25,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/k3anse3yrrunq.png"},{"GameType":"Slot","GameCode":"pirtanombyroh","GameName":"Huga","GameAlias":"Huga","Specials":"hot,new","SupportedPlatForms":"Desktop,Mobile","Order":15,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/pirtanombyroh.png"},{"GameType":"Slot","GameCode":"ne4gq55cpitgg","GameName":"Beanstalk","GameAlias":"Beanstalk","Specials":"hot,new","SupportedPlatForms":"Desktop,Mobile","Order":20,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/ne4gq55cpitgg.png"},{"GameType":"Slot","GameCode":"kia1eetdryo1c","GameName":"Alice","GameAlias":"Alice","Specials":"hot,new","SupportedPlatForms":"Desktop,Mobile","Order":12,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/kia1eetdryo1c.png"},{"GameType":"Slot","GameCode":"u17q53q45xcp1","GameName":"White Snake","GameAlias":"WhiteSnake","Specials":"hot,new","SupportedPlatForms":"Desktop,Mobile","Order":11,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/u17q53q45xcp1.png"},{"GameType":"Slot","GameCode":"ef1uyxt98o6ur","GameName":"Lucky God Progressive","GameAlias":"GodOfFortunePlus","Specials":"hot,new","SupportedPlatForms":"Desktop,Mobile","Order":3,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/ef1uyxt98o6ur.png"},{"GameType":"Slot","GameCode":"9xpa7brfxj7zo","GameName":"Mammamia","GameAlias":"Mammamia","Specials":"hot,new","SupportedPlatForms":"Desktop,Mobile","Order":14,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/9xpa7brfxj7zo.png"},{"GameType":"Slot","GameCode":"naagsa5ycfugq","GameName":"Ancient Egypt","GameAlias":"AncientEgypt","Specials":"hot,new","SupportedPlatForms":"Desktop,Mobile","Order":3,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/naagsa5ycfugq.png"},{"GameType":"Slot","GameCode":"i4rc816e388c6","GameName":"Robin Hood","GameAlias":"RobinHood","Specials":"hot,new","SupportedPlatForms":"Desktop,Mobile","Order":3,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/i4rc816e388c6.png"},{"GameType":"Slot","GameCode":"nh9swadbc3use","GameName":"HighwayKings Progressive","GameAlias":"HighwayKingsPro","Specials":"hot,new","SupportedPlatForms":"Desktop,Mobile","Order":3,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/nh9swadbc3use.png"},{"GameType":"Slot","GameCode":"9upe5bm4xph81","GameName":"Monkey King","GameAlias":"MonkeyKing","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":56,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/9upe5bm4xph81.png"},{"GameType":"Slot","GameCode":"tqi9778i7mi6o","GameName":"Miami","GameAlias":"Miami","Specials":"hot,new","SupportedPlatForms":"Desktop,Mobile","Order":3,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/tqi9778i7mi6o.png"},{"GameType":"Slot","GameCode":"9mqe9bhroi78s","GameName":"Golden Monkey King","GameAlias":"GoldenMonkeyKing","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":56,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/9mqe9bhroi78s.png"},{"GameType":"Slot","GameCode":"byz81hmsq748k","GameName":"Supreme Caishen","GameAlias":"Caishen","Specials":"hot,new","SupportedPlatForms":"Desktop,Mobile","Order":3,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/byz81hmsq748k.png"},{"GameType":"Slot","GameCode":"113qm5xnhxoqn","GameName":"Aladdin","GameAlias":"GenieHuga","Specials":"hot","SupportedPlatForms":"Desktop,Mobile","Order":45,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/113qm5xnhxoqn.png"},{"GameType":"Slot","GameCode":"5m6k9j7rwspjs","GameName":"Roma","GameAlias":"Roma","Specials":"hot,new","SupportedPlatForms":"Desktop,Mobile","Order":-3,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/5m6k9j7rwspjs.png"},{"GameType":"Slot","GameCode":"igg7tisz4ukhw","GameName":"Egypt Queen","GameAlias":"EgyptQueen","Specials":"hot,new","SupportedPlatForms":"Desktop,Mobile","Order":-3,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/igg7tisz4ukhw.png"},{"GameType":"Slot","GameCode":"w4ypzw6o48mpq","GameName":"Dragon Phoenix","GameAlias":"DragonPhoenix","Specials":"hot,new","SupportedPlatForms":"Desktop,Mobile","Order":-3,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/w4ypzw6o48mpq.png"},{"GameType":"Slot","GameCode":"xbxy1yegyhnyk","GameName":"Jungle Island","GameAlias":"JungleIsland","Specials":"hot,new","SupportedPlatForms":"Desktop,Mobile","Order":-3,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/xbxy1yegyhnyk.png"},{"GameType":"Slot","GameCode":"ywozehuuqbazc","GameName":"Golden Island","GameAlias":"GoldenIsland","Specials":"hot","SupportedPlatForms":"Desktop,Mobile","Order":45,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/ywozehuuqbazc.png"},{"GameType":"Slot","GameCode":"foff4ikkjprr1","GameName":"Water Margin","GameAlias":"WaterMargin","Specials":"hot,new","SupportedPlatForms":"Desktop,Mobile","Order":-3,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/foff4ikkjprr1.png"},{"GameType":"Slot","GameCode":"bcizh7dipjiso","GameName":"Mulan","GameAlias":"Mulan","Specials":"hot","SupportedPlatForms":"Desktop,Mobile","Order":45,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/bcizh7dipjiso.png"},{"GameType":"Slot","GameCode":"55hj8ghaugxj6","GameName":"Happy Buddha","GameAlias":"HappyBuddha","Specials":"hot","SupportedPlatForms":"Desktop,Mobile","Order":45,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/55hj8ghaugxj6.png"},{"GameType":"Slot","GameCode":"jpiuhpbifei1o","GameName":"Golden Rooster","GameAlias":"GoldenRooster","Specials":"hot","SupportedPlatForms":"Desktop,Mobile","Order":45,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/jpiuhpbifei1o.png"},{"GameType":"Slot","GameCode":"ruufkzk1kpefn","GameName":"SilverBullet Progressive","GameAlias":"SilverBulletPlus","Specials":"hot,new","SupportedPlatForms":"Desktop,Mobile","Order":3,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/ruufkzk1kpefn.png"},{"GameType":"Slot","GameCode":"awn5jciusna5c","GameName":"Captains Treasure Progressive","GameAlias":"CaptainsTreasurePlus","Specials":"hot,new","SupportedPlatForms":"Desktop,Mobile","Order":3,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/awn5jciusna5c.png"},{"GameType":"Slot","GameCode":"3hj4fkfji4z4a","GameName":"Lucky God Progressive 2","GameAlias":"GodOfFortunePlus2","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":3,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/3hj4fkfji4z4a.png"},{"GameType":"Slot","GameCode":"4akkze7ywgukq","GameName":"Crypto Mania","GameAlias":"CryptoManiaGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-700,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/4akkze7ywgukq.png"},{"GameType":"Slot","GameCode":"86burqb38a9ua","GameName":"Bushido Blade","GameAlias":"BushidoBladeGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-700,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/86burqb38a9ua.png"},{"GameType":"Slot","GameCode":"o7f9ih8t6559e","GameName":"Empress Regnant","GameAlias":"EmpressRegnantGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-1301,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/o7f9ih8t6559e.png"},{"GameType":"Slot","GameCode":"8u9r4tj48chd1","GameName":"Dynamite Reels","GameAlias":"DynamiteReelsGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-700,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/8u9r4tj48chd1.png"},{"GameType":"Slot","GameCode":"wtupmzq14xepn","GameName":"Lions Dance","GameAlias":"LionsDanceGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-1301,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/wtupmzq14xepn.png"},{"GameType":"Slot","GameCode":"d8cso3u8ct1iw","GameName":"Phoenix 888","GameAlias":"Phoenix888GW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-1301,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/d8cso3u8ct1iw.png"},{"GameType":"Slot","GameCode":"3yfmucpss64mk","GameName":"Dragon Power Flame","GameAlias":"DragonPowerFlameGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-1310,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/3yfmucpss64mk.png"},{"GameType":"Slot","GameCode":"itzp5iqk3xrc1","GameName":"Wild Spirit","GameAlias":"WildSpirit","Specials":"hot","SupportedPlatForms":"Desktop,Mobile","Order":45,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/itzp5iqk3xrc1.png"},{"GameType":"Slot","GameCode":"7rw3tfwz11kaw","GameName":"Arctic Treasure","GameAlias":"ArcticTreasure","Specials":"hot","SupportedPlatForms":"Desktop,Mobile","Order":45,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/7rw3tfwz11kaw.png"},{"GameType":"Slot","GameCode":"aodmmxp1sqamn","GameName":"Zhao Cai Jin Bao","GameAlias":"ZhaoCaiJinBao","Specials":"hot","SupportedPlatForms":"Desktop,Mobile","Order":45,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/aodmmxp1sqamn.png"},{"GameType":"Slot","GameCode":"o3nxzh9o685xq","GameName":"Fei Long Zai Tian","GameAlias":"FeiLongZaiTian","Specials":"hot","SupportedPlatForms":"Desktop,Mobile","Order":45,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/o3nxzh9o685xq.png"},{"GameType":"Slot","GameCode":"ufc6t3z8hu17w","GameName":"Santa Surprise","GameAlias":"SantaSurprise","Specials":"hot","SupportedPlatForms":"Desktop,Mobile","Order":45,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/ufc6t3z8hu17w.png"},{"GameType":"Slot","GameCode":"j6j1rkbjfaz1a","GameName":"Five Tiger Generals","GameAlias":"FiveTigerGenerals","Specials":"hot","SupportedPlatForms":"Desktop,Mobile","Order":45,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/j6j1rkbjfaz1a.png"},{"GameType":"Slot","GameCode":"9w6aa6u5xbhzh","GameName":"Golden Dragon","GameAlias":"GoldenDragon","Specials":"hot","SupportedPlatForms":"Desktop,Mobile","Order":45,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/9w6aa6u5xbhzh.png"},{"GameType":"Slot","GameCode":"wpu7pzg74mj7y","GameName":"Lucky Drum","GameAlias":"LuckyDrum","Specials":"hot","SupportedPlatForms":"Desktop,Mobile","Order":45,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/wpu7pzg74mj7y.png"},{"GameType":"Slot","GameCode":"tbfxuhxs694xk","GameName":"Lucky Panda","GameAlias":"LuckyPanda","Specials":"hot","SupportedPlatForms":"Desktop,Mobile","Order":45,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/tbfxuhxs694xk.png"},{"GameType":"Slot","GameCode":"gd3rn1kqj7gr4","GameName":"Queen","GameAlias":"Queen","Specials":"hot","SupportedPlatForms":"Desktop,Mobile","Order":45,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/gd3rn1kqj7gr4.png"},{"GameType":"Slot","GameCode":"wfo7bzs95uq7r","GameName":"Archer","GameAlias":"Archer","Specials":"hot","SupportedPlatForms":"Desktop,Mobile","Order":45,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/wfo7bzs95uq7r.png"},{"GameType":"Slot","GameCode":"jsguaktmfyw1h","GameName":"Hercules","GameAlias":"Hercules","Specials":"hot","SupportedPlatForms":"Desktop,Mobile","Order":45,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/jsguaktmfyw1h.png"},{"GameType":"Slot","GameCode":"bmr8675wqiigs","GameName":"Witch's Brew","GameAlias":"WitchsBrewGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312810,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/bmr8675wqiigs.png"},{"GameType":"Slot","GameCode":"bzgza4umpbwsh","GameName":"Third Prince's Journey","GameAlias":"NezhaGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312811,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/bzgza4umpbwsh.png"},{"GameType":"Slot","GameCode":"s77hiogba5dhe","GameName":"Peach Banquet","GameAlias":"PeachBanquetGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312812,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/s77hiogba5dhe.png"},{"GameType":"Slot","GameCode":"dxxsh3dfmjpio","GameName":"Tai Shang Lao Jun","GameAlias":"TaiShangLaoJunGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312813,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/dxxsh3dfmjpio.png"},{"GameType":"Slot","GameCode":"hcu3p8r71kj3y","GameName":"Power Stars","GameAlias":"PowerStarsGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-214017,"DefaultWidth":1105,"DefaultHeight":737,"Image1":"//img.gwccdn.net/gameimages/landscape/hcu3p8r71kj3y.png"},{"GameType":"Slot","GameCode":"7cz37fritkfao","GameName":"Lucky Rooster","GameAlias":"LuckyRooster","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-213918,"DefaultWidth":1105,"DefaultHeight":737,"Image1":"//img.gwccdn.net/gameimages/landscape/7cz37fritkfao.png"},{"GameType":"Slot","GameCode":"1wt58azdhdo6c","GameName":"Wild Fairies","GameAlias":"WildFairyGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312830,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/1wt58azdhdo6c.png"},{"GameType":"Slot","GameCode":"pxcw1fyibefoo","GameName":"Wild Protectors","GameAlias":"WildProtectorsGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312840,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/pxcw1fyibefoo.png"},{"GameType":"Slot","GameCode":"7tccifcktqre1","GameName":"Chinese Boss","GameAlias":"ChineseBossGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-1300,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/7tccifcktqre1.png"},{"GameType":"Slot","GameCode":"zygj7oqga9nck","GameName":"Caishen Riches","GameAlias":"CaishenRichesGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-700,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/zygj7oqga9nck.png"},{"GameType":"Slot","GameCode":"srd3xusx3ughr","GameName":"Enter The KTV","GameAlias":"EnterTheKTVGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-700,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/srd3xusx3ughr.png"},{"GameType":"Slot","GameCode":"1ru5x5zx7us6r","GameName":"Lightning God","GameAlias":"LightningGodGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312708,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/1ru5x5zx7us6r.png"},{"GameType":"Slot","GameCode":"dkzdo35rcipfs","GameName":"China","GameAlias":"ChinaGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312704,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/dkzdo35rcipfs.png"},{"GameType":"Slot","GameCode":"q9gi4yybyadoe","GameName":"Wild Giant Panda","GameAlias":"WildGiantPandaGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312705,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/q9gi4yybyadoe.png"},{"GameType":"Slot","GameCode":"wykepsq659qp4","GameName":"Four Dragons","GameAlias":"FourDragonsGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312706,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/wykepsq659qp4.png"},{"GameType":"Slot","GameCode":"rsjogw1ukbeic","GameName":"Four Tigers","GameAlias":"FourTigersGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-1900,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/rsjogw1ukbeic.png"},{"GameType":"Slot","GameCode":"5ii9zgw5unc3h","GameName":"Neptune Treasure","GameAlias":"NeptuneTreasureGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312707,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/5ii9zgw5unc3h.png"},{"GameType":"Slot","GameCode":"xmzfobaryz7xs","GameName":"Lord Of The Ocean","GameAlias":"LordOfTheOceanGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-213902,"DefaultWidth":1105,"DefaultHeight":737,"Image1":"//img.gwccdn.net/gameimages/landscape/xmzfobaryz7xs.png"},{"GameType":"Slot","GameCode":"qxoindypyeboy","GameName":"Geisha","GameAlias":"GeishaGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-213903,"DefaultWidth":1105,"DefaultHeight":737,"Image1":"//img.gwccdn.net/gameimages/landscape/qxoindypyeboy.png"},{"GameType":"Slot","GameCode":"aij68ciusna5c","GameName":"Columbus","GameAlias":"ColumbusGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-2401,"DefaultWidth":1105,"DefaultHeight":737,"Image1":"//img.gwccdn.net/gameimages/landscape/aij68ciusna5c.png"},{"GameType":"Slot","GameCode":"satj3o6ya8dcq","GameName":"Just Jewels","GameAlias":"JustJewelsGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-2400,"DefaultWidth":1105,"DefaultHeight":737,"Image1":"//img.gwccdn.net/gameimages/landscape/satj3o6ya8dcq.png"},{"GameType":"Slot","GameCode":"k9gz4ebbrau1e","GameName":"Fifty Dragons","GameAlias":"FiftyDragonsGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-2700,"DefaultWidth":1105,"DefaultHeight":737,"Image1":"//img.gwccdn.net/gameimages/landscape/k9gz4ebbrau1e.png"},{"GameType":"Slot","GameCode":"8nsbhokge7nrk","GameName":"Queen Of The Nile","GameAlias":"QueenOfTheNileGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-213912,"DefaultWidth":1105,"DefaultHeight":737,"Image1":"//img.gwccdn.net/gameimages/landscape/8nsbhokge7nrk.png"},{"GameType":"Slot","GameCode":"ww3a8wsu4de7c","GameName":"Sizzling Hot","GameAlias":"SizzlingHotGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-213914,"DefaultWidth":1105,"DefaultHeight":737,"Image1":"//img.gwccdn.net/gameimages/landscape/ww3a8wsu4de7c.png"},{"GameType":"Slot","GameCode":"43bx3e7ywgukq","GameName":"Dolphin Pearl","GameAlias":"DolphinPearlGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-213916,"DefaultWidth":1105,"DefaultHeight":737,"Image1":"//img.gwccdn.net/gameimages/landscape/43bx3e7ywgukq.png"},{"GameType":"Slot","GameCode":"hf5hx8w9u1q3r","GameName":"Book Of Ra Deluxe","GameAlias":"BookOfRaDeluxeGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-214017,"DefaultWidth":1280,"DefaultHeight":720,"Image1":"//img.gwccdn.net/gameimages/landscape/hf5hx8w9u1q3r.png"},{"GameType":"Slot","GameCode":"ioheiiqk3xrc1","GameName":"Book Of Ra","GameAlias":"BookOfRaGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-2701,"DefaultWidth":1105,"DefaultHeight":737,"Image1":"//img.gwccdn.net/gameimages/landscape/ioheiiqk3xrc1.png"},{"GameType":"Slot","GameCode":"7f9h9fwz11kaw","GameName":"Lucky Lady Charm","GameAlias":"LuckyLadyCharmGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-2702,"DefaultWidth":1105,"DefaultHeight":737,"Image1":"//img.gwccdn.net/gameimages/landscape/7f9h9fwz11kaw.png"},{"GameType":"Slot","GameCode":"84igeq3a8r9d6","GameName":"Nugget Hunter","GameAlias":"NuggetHunterGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-213917,"DefaultWidth":1105,"DefaultHeight":737,"Image1":"//img.gwccdn.net/gameimages/landscape/84igeq3a8r9d6.png"},{"GameType":"Slot","GameCode":"fk9yoi4wkifrs","GameName":"Fifty Lions","GameAlias":"FiftyLionsGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-213915,"DefaultWidth":1105,"DefaultHeight":737,"Image1":"//img.gwccdn.net/gameimages/landscape/fk9yoi4wkifrs.png"},{"GameType":"Slot","GameCode":"ateqfxp1sqamn","GameName":"Dolphin Treasure","GameAlias":"DolphinTreasureGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-213916,"DefaultWidth":1105,"DefaultHeight":737,"Image1":"//img.gwccdn.net/gameimages/landscape/ateqfxp1sqamn.png"},{"GameType":"Slot","GameCode":"ie9eti6w4zfcs","GameName":"Ancient Artifact","GameAlias":"AncientArtifactGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-214018,"DefaultWidth":1105,"DefaultHeight":737,"Image1":"//img.gwccdn.net/gameimages/landscape/ie9eti6w4zfcs.png"},{"GameType":"Slot","GameCode":"oajk3h9o685xq","GameName":"Money Vault","GameAlias":"MoneyVaultGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312810,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/oajk3h9o685xq.png"},{"GameType":"Slot","GameCode":"ur8593z8hu17w","GameName":"Burning Pearl","GameAlias":"BurningPearlGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312810,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/ur8593z8hu17w.png"},{"GameType":"Slot","GameCode":"j9nzkkbjfaz1a","GameName":"Horus Eye","GameAlias":"HorusEyeGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312810,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/j9nzkkbjfaz1a.png"},{"GameType":"Slot","GameCode":"4tyxfmpnwqokn","GameName":"Octagon Gem","GameAlias":"OctagonGemGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-213917,"DefaultWidth":1105,"DefaultHeight":737,"Image1":"//img.gwccdn.net/gameimages/landscape/4tyxfmpnwqokn.png"},{"GameType":"Slot","GameCode":"soojfuqnaxycn","GameName":"Hot Fruits","GameAlias":"HotFruitsGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-213917,"DefaultWidth":1105,"DefaultHeight":737,"Image1":"//img.gwccdn.net/gameimages/landscape/soojfuqnaxycn.png"},{"GameType":"Slot","GameCode":"9ii7s6u5xbhzh","GameName":"Yggdrasil","GameAlias":"YggdrasilGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312813,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/9ii7s6u5xbhzh.png"},{"GameType":"Slot","GameCode":"wcaadzg74mj7y","GameName":"Lady Hawk","GameAlias":"LadyHawkGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312813,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/wcaadzg74mj7y.png"},{"GameType":"Slot","GameCode":"gn1bc1kqj7gr4","GameName":"Bagua","GameAlias":"BaguaGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312828,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/gn1bc1kqj7gr4.png"},{"GameType":"Slot","GameCode":"n1ydr5mncpogn","GameName":"Chilli Hunter","GameAlias":"ChilliHunterGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312829,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/n1ydr5mncpogn.png"},{"GameType":"Slot","GameCode":"z7k6mqf3z495a","GameName":"Crypto Mania Bingo","GameAlias":"CryptoManiaBingoGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312842,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/z7k6mqf3z495a.png"},{"GameType":"Slot","GameCode":"cz3wgrounyetc","GameName":"Neptune Treasure Bingo","GameAlias":"NeptuneTreasureBingoGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312843,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/cz3wgrounyetc.png"},{"GameType":"Slot","GameCode":"quofrdenycyyn","GameName":"Bagua 2","GameAlias":"Bagua2GW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312831,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/quofrdenycyyn.png"},{"GameType":"Slot","GameCode":"gsttgo1debywc","GameName":"Octagon Gem 2","GameAlias":"OctagonGem2GW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312832,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/gsttgo1debywc.png"},{"GameType":"Slot","GameCode":"fqho1inijjfwo","GameName":"Dragon Of The Eastern Sea","GameAlias":"DragonOfTheEasternSeaGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312841,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/fqho1inijjfwo.png"},{"GameType":"Slot","GameCode":"tocki7xk7xwq1","GameName":"Burning Pearl Bingo","GameAlias":"BurningPearlBingoGW","Specials":"new,hot","SupportedPlatForms":"Desktop,Mobile","Order":-312844,"DefaultWidth":960,"DefaultHeight":600,"Image1":"//img.gwccdn.net/gameimages/landscape/tocki7xk7xwq1.png"}]}

    request.post({
        url: `${SLOT_XO_HOST}/list-games`,
        body: form,
        json:true
    }, function(error, response, body){
        console.log('---d---',response.body);
        callback(null, response.body);
    });

}

// function getGameHistory(callback) {
//     let timestamp = moment().unix();
//     let parameters = `{"appid":"AppID","Username":"DEMO","GameCode":"xea1rt9gebhwy","RoundID":"uhax9gb5upqg q","Language":"en","Hash":"792d2414e3b0c77e9ddea9e8aa924364","Timestamp":1528210236}`+SLOT_XO_SECRET_KEY;
//     let parameters = `appid=${SLOT_XO_APP_ID}&timestamp=${timestamp}`+SLOT_XO_SECRET_KEY;
//
//     console.log(parameters);
//
//     let hash = buildSignature(parameters);
//
//     const headers = {
//         'Content-Type': 'application/json'
//     };
//
//     let form = {
//         AppID: SLOT_XO_APP_ID,
//         Hash : hash,
//         Timestamp: timestamp
//     };
//
//     console.log(form)
//
//     request.post({
//         url: `${SLOT_XO_HOST}/list-games`,
//         body: form,
//         json:true
//     }, function(error, response, body){
//         // console.log('---d---',response.body);
//         callback(null, response.body);
//     });
//
// }

function getGameName(item) {
    switch (item){
        case "fwria11mjbrwh":
            return "Three Kingdoms Quest";
        case "8rqwot18etnuw":
            return "Thunder God";
        case "kf41ymtxfos1r":
            return "Ocean Paradise";
        case "ebudnqj68h6d4":
            return "Happy Party";
        case "xtpy4bx49xhx1":
            return "African";
        case "1q36p58phmt6y":
            return "Genie";
        case "dhdirsn3m3xia":
            return "Lucky God";
        case "rh8iwwntk3mie":
            return "Dolphin Reef";
        case "axt5pxf7sk35y":
            return "Highway Kings";
        case "69xaiyrbo4dae":
            return "A Night Out";
        case "oqt9p9876m39y":
            return "Azteca";
        case "jbzd1cjsgh4dk":
            return "Sparta";
        case "4d5kdkpqi6sk4":
            return "Safari Heat";
        case "u6d7fsg355x7a":
            return "Panther Moon";
        case "t656f48j75z6a":
            return "Great Blue";
        case "z1pc5tp4zqhm1":
            return "Silver Bullet";
        case "bes8675wqiigs":
            return "Captain's Treasure";
        case "bwwza4umpbwsh":
            return "bwwza4umpbwsh";
        case "s6xhiogba5dhe":
            return "Football";
        case "5864tji8w113w":
            return "Thai Paradise";
        case "qieoeyodyyyoc":
            return "Captain's Treasure Pro";
        case "79mafnrjt48aa":
            return "Pan Jin Lian";
        case "k3anse3yrrunq":
            return "MoneyBangBang";
        case "pirtanombyroh":
            return "Huga";
        case "ne4gq55cpitgg":
            return "Beanstalk";
        case "kia1eetdryo1c":
            return "Alice";
        case "u17q53q45xcp1":
            return "White Snake";
        case "ef1uyxt98o6ur":
            return "Lucky God Progressive";
        case "9xpa7brfxj7zo":
            return "Mammamia";
        case "naagsa5ycfugq":
            return "Ancient Egypt";
        case "i4rc816e388c6":
            return "Robin Hood";
        case "nh9swadbc3use":
            return "HighwayKings Progressive";
        case "9upe5bm4xph81":
            return "Monkey King";
        case "tqi9778i7mi6o":
            return "Miami";
        case "9mqe9bhroi78s":
            return "Golden Monkey King";
        case "byz81hmsq748k":
            return "Supreme Caishen";
        case "113qm5xnhxoqn":
            return "Aladdin";
        case "5m6k9j7rwspjs":
            return "Roma";
        case "igg7tisz4ukhw":
            return "Egypt Queen";
        case "w4ypzw6o48mpq":
            return "Dragon Phoenix";
        case "xbxy1yegyhnyk":
            return "Jungle Island";
        case "ywozehuuqbazc":
            return "Golden Island";
        case "foff4ikkjprr1":
            return "Water Margin";
        case "bcizh7dipjiso":
            return "Mulan";
        case "55hj8ghaugxj6":
            return "Happy Buddha";
        case "jpiuhpbifei1o":
            return "Golden Rooster";
        case "ruufkzk1kpefn":
            return "SilverBullet Progressive";
        case "awn5jciusna5c":
            return "Captains Treasure Progressive";
        case "3hj4fkfji4z4a":
            return "Lucky God Progressive 2";
        case "4akkze7ywgukq":
            return "Crypto Mania";
        case "86burqb38a9ua":
            return "Bushido Blade";
        case "o7f9ih8t6559e":
            return "Empress Regnant";
        case "8u9r4tj48chd1":
            return "Dynamite Reels";
        case "wtupmzq14xepn":
            return "Lions Dance";
        case "d8cso3u8ct1iw":
            return "Phoenix 888";
        case "3yfmucpss64mk":
            return "Dragon Power Flame";
        case "itzp5iqk3xrc1":
            return "Wild Spirit";
        case "7rw3tfwz11kaw":
            return "Arctic Treasure";
        case "aodmmxp1sqamn":
            return "Zhao Cai Jin Bao";
        case "o3nxzh9o685xq":
            return "Fei Long Zai Tian";
        case "ufc6t3z8hu17w":
            return "Santa Surprise";
        case "j6j1rkbjfaz1a":
            return "Five Tiger Generals";
        case "9w6aa6u5xbhzh":
            return "Golden Dragon";
        case "wpu7pzg74mj7y":
            return "Lucky Drum";
        case "tbfxuhxs694xk":
            return "Lucky Panda";
        case "gd3rn1kqj7gr4":
            return "Queen";
        case "wfo7bzs95uq7r":
            return "Archer";
        case "jsguaktmfyw1h":
            return "Hercules";
        case "bmr8675wqiigs":
            return "Witch's Brew";
        case "bzgza4umpbwsh":
            return "Third Prince's Journey";
        case "s77hiogba5dhe":
            return "Peach Banquet";
        case "dxxsh3dfmjpio":
            return "Tai Shang Lao Jun";
        case "7cz37fritkfao":
            return "Lucky Rooster";
        case "7tccifcktqre1":
            return "Chinese Boss";
        case "zygj7oqga9nck":
            return "Caishen Riches";
        case "srd3xusx3ughr":
            return "Enter The KTV";
        case "1ru5x5zx7us6r":
            return "Lightning God";
        case "dkzdo35rcipfs":
            return "China";
        case "q9gi4yybyadoe":
            return "Wild Giant Panda";
        case "wykepsq659qp4":
            return "Four Dragons";
        case "rsjogw1ukbeic":
            return "Four Tigers";
        case "5ii9zgw5unc3h":
            return "Neptune Treasure";
        case "xmzfobaryz7xs":
            return "Lord Of The Ocean";
        case "qxoindypyeboy":
            return "Geisha";
        case "aij68ciusna5c":
            return "Columbus";
        case "satj3o6ya8dcq":
            return "Just Jewels";
        case "8nsbhokge7nrk":
            return "Queen Of The Nile";
        case "ww3a8wsu4de7c":
            return "Sizzling Hot";
        case "43bx3e7ywgukq":
            return "Dolphin Pearl";
        case "ioheiiqk3xrc1":
            return "Book Of Ra";
        case "7f9h9fwz11kaw":
            return "Lucky Lady Charm";
        case "84igeq3a8r9d6":
            return "Nugget Hunter";
        case "fk9yoi4wkifrs":
            return "Fifty Lions";
        case "ateqfxp1sqamn":
            return "Dolphin Treasure";
        case "ie9eti6w4zfcs":
            return "Ancient Artifact";
        case "oajk3h9o685xq":
            return "Money Vault";
        case "ur8593z8hu17w":
            return "Burning Pearl";
        case "j9nzkkbjfaz1a":
            return "Horus Eye";
        case "4tyxfmpnwqokn":
            return "Octagon Gem";
        case "soojfuqnaxycn":
            return "Hot Fruits";
        case "9ii7s6u5xbhzh":
            return "Yggdrasil";
        case "wcaadzg74mj7y":
            return "Lady Hawk";
        case "xkhy6baryz7xs":
            return "Fish Hunter 2 EX - Newbie";
        case "qq5ocdypyeboy":
            return "Fish Hunter 2 EX - Novice";
        case "g54rso4yefdrq":
            return "Fish Hunter 2 EX - Pro";
        case "ary5bxi9z165r":
            return "Fish Hunter 2 EX - My Club";
        case "b8rzo7uzqt4sw":
            return "Fish Hunting: Golden Toad";
        case "8d7r1okge7nrk":
            return "Fish Hunting: Da Sheng Nao Hai";
        case "nzkseaudcbosc":
            return "Fish Hunting: Li Kui Pi Yu";
        case "wi17jwsu4de7c":
            return "Fish Hunting: Yao Qian Shu";
        case "1jeqx59c7ztqg":
            return "Fish Hunter Monster Awaken";
        case "4omkmmpnwqokn":
            return "Fish Hunter Spongebob";
        case "st5cmuqnaxycn":
            return "Fish Hunting: Happy Fish 5";
        case "ddpg1amgc71gk":
            return "Insect Paradise";
        case "p63ornyjba8oa":
            return "Fishermans Wharf";
        case "xq9ohbyf9m79o":
            return "Bird Paradise";
        case "kk8nqm3cfwtng":
            return "Fish Haiba";
        case "dc7sh3dfmjpio":
            return "Dragon Tiger";
        case "3uim5ppkiqwk1":
            return "Belangkai";
        case "j3wngk3efrzn6":
            return "Baccarat";
        case "yr1zy9u9xt6zr":
            return "HuLu";
        case "hxb3p8r71kj3y":
            return "Sicbo";
        default:
            return item;
    }
}

function buildSignature(val) {
    let str = val+SLOT_XO_SECRET_KEY;
    // console.log('str ======== ',str);
    return Crypto.createHash('md5').update(str).digest("hex");
}
function random(low, high) {
    return Math.floor(Math.random() * (high - low + 1) + low)
}

function generateBetId() {
    return 'BET_XO' + new Date().getTime() + random(1000000, 9999999);
}

module.exports = {getCredit, transferCredit,createUser,getRetrieveTransactions,getTotalTransactions, gameList, forwardToGame, getGameName, buildSignature, generateBetId}