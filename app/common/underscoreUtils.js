const FootballModel = require('../models/football.model');
const async = require("async");
const request = require('request');
const _ = require('underscore');
const CACHE_IP = process.env.CACHE_IP || '139.59.98.183';
const URL_CACHE_HDP_SINGLE_MATCH = `http://${CACHE_IP}:8001/cache/hdp_single_match`;
const URL_CACHE_LIVE_SINGLE_MATCH = `http://${CACHE_IP}:8001/cache/live_single_match`;
const HUNDRED = 100;


module.exports = {

    findKeyByValue(obj,value, callback)  {
        let key;
        _.findKey(obj, function (v, k) {
            if (v === value) {
                key = k;
                return true;
            }
        });
        callback(key);
    }
};

