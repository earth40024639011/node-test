const _ = require('underscore');

const request = require('request');
const crypto = require('crypto');

const getClientKey = require('../controllers/getClientKey');
let {
    AMB_GAME_API_2: AMB_GAME_API = 'https://ap.ambpoker-api.com',
    AMB_GAME_SECRET_KEY_2: AMB_GAME_SECRET_KEY = '63feb7edf27d09c3bc4982a5a2280e4a',
    AMB_GAME_AGENT_2: AMB_GAME_AGENT = 'ambseamless'
} = getClientKey.getKey();

function encryptRequest(jsonStr) {
    console.log(`encryptRequest : input : ${jsonStr} , key : ${AMB_GAME_SECRET_KEY}`)
    return crypto.pbkdf2Sync(jsonStr, AMB_GAME_SECRET_KEY, 1000, 64,
        'sha512').toString('base64')
}



function callGameList(callback) {


    let headers = {
        'Content-Type': 'application/json'
    };


    let options = {
        url: AMB_GAME_API + '/seamless/games',
        method: 'GET',
        headers: headers,
        json: true,
    };

    console.log(options)
    request(options, (err, response, body) => {

        if (err) {
            callback(err, null);

        } else {


            let responseBody = body;

            if (responseBody.status.code != 0) {
                callback(err, null);
            } else {

                let gameList = _.filter(responseBody.data.games, (f) => {
                    return f.isActivated;
                });


                gameList = _.map(gameList, (item) => {
                    return {
                        gameId: item.gameId,
                        gameName: item.name,
                        type: item.type,
                        role: item.role,
                        imageUrl: item.thumbnail,
                        isActive: item.isActivated
                    }
                });

                callback(null, gameList);

            }

        }
    });

}

function callCreateMember(username,callback) {

    const body = {
        username: username,
        password:"",
        agent: AMB_GAME_AGENT
    };

    console.log('body : ',body)


    let headers = {
        'Content-Type': 'application/json',
        'x-amb-signature':encryptRequest(JSON.stringify(body))
    };

    let options = {
        url: AMB_GAME_API + '/seamless/create',
        method: 'POST',
        headers: headers,
        json: body,
    };

    console.log(options);

    request(options, function (err, response, body) {
        console.log('body ', body)
        if (err) {
            callback(err, null);
        } else {

            let responseBody = body;

            if (responseBody.status.code != 0) {
                callback(responseBody.status, null);
            } else {

                callback(null, 'success')
            }

        }
    });

}



function callLaunchGame(username,gameId,callback) {

    const body = {
        username: username,
        gameId: gameId,
        agent: AMB_GAME_AGENT
    };

    console.log('body : ', body)

    let headers = {
        'Content-Type': 'application/json',
        'x-amb-signature': encryptRequest(JSON.stringify(body))
    };

    let options = {
        url: AMB_GAME_API + '/seamless/launch/game',
        method: 'POST',
        headers: headers,
        json: body,
    };

    console.log(options)

    request(options, function (err, response, body) {
        console.log('body ', body)
        if (err) {
            callback(err, null);
        } else {

            let responseBody = body;

            if (responseBody.status.code != 0) {
                callback(responseBody.status, null);
            } else {

                callback(null, responseBody.data.url)
            }

        }
    });

}


function callGetReportDetails(username,roundId,callback) {

    const body = {
        username: username,
        agent: AMB_GAME_AGENT,
        roundId: roundId,

    };

    console.log('body : ', body)

    let headers = {
        'Content-Type': 'application/json',
        'x-amb-signature': encryptRequest(JSON.stringify(body))
    };

    let options = {
        url: AMB_GAME_API + '/seamless/report',
        method: 'POST',
        headers: headers,
        json: body,
    };

    console.log(options);

    request(options, function (err, response, body) {
        console.log('body ', body)
        if (err) {
            callback(err, null);
        } else {

            let responseBody = body;

            if (responseBody.status.code != 0) {
                callback(responseBody.status, null);
            } else {

                callback(null, responseBody.data.url)
            }

        }
    });

}

function getBetKeyName(gameName,key) {

    console.log(gameName,key    )

    if(gameName == 'TEEKAI' || gameName == 'POKDENG' || gameName == 'TEEKAI3CARD') {
        return `[${getResultTkPd(key.split('|')[0])} ${getResultTkPd(key.split('|')[1])} ${key.split('|')[2] ? getResultTkPd(key.split('|')[2]):''}]`;
    }else if(gameName == '13CARDSPOKER'){
        return _.map(key.split('|'),(k)=>{
            return getResultTkPd(k)
        }).join(',');
    }else if(gameName == 'NTPP'){
        return `[${getBetNtpp(key)}]`;
    }else if(gameName == 'HILO'){
        return `[${getBetHilo(key)}]`;
    }else if(gameName == 'BATTLEKING' || gameName == 'BATTLEROYAL'){
        return `[${getBetBoardGame(key)}]`;
    }else if(gameName == 'PUNPAE'){
        return `[${getBetPunpae(key)}]`;
    }
    return '';
}

function getResult(gameName,key) {
    let list = [];
    if(gameName == 'TEEKAI' || gameName == 'POKDENG' || gameName == 'TEEKAI3CARD') {

        list = _.map(key.split('|'),(k)=>{
            return getResultTkPd(k)
        }).join(',');

    }else if(gameName == '13CARDSPOKER'){
        // return _.map(key.split('|'),(k)=>{
        //     return getResultTkPd(k)
        // }).join(',');
        return [];
    }else if(gameName == 'NTPP'){
        list = _.map(key.split('|'),(k)=>{
            return getResultNtpp(k)
        }).join(',');
    }else if(gameName == 'HILO'){
        list = _.map(key.split('|'),(k)=>{
            return getResultHilo(k)
        }).join(',');
    }else if(gameName == 'BATTLEKING' || gameName == 'BATTLEROYAL'){
        list = _.map(key.split('|'),(k)=>{
            return getBetBoardGame(k)
        }).join(',');
    }else if(gameName == 'PUNPAE'){
        list = _.map(key.split('|'),(k)=>{
            return getBetPunpae(k)
        }).join(',');
    }

    return list;
}


function getResultNtpp(betResult) {

    switch (betResult) {
        case "1" :
            return "Crab";
        case "2" :
            return "Fish";
        case "3" :
            return "Calabash";
        case "4" :
            return "Tiger";
        case "5" :
            return "Chicken";
        case "6" :
            return "Shrimp";
        default :
            return betResult
    }

}

function getBetBoardGame(betResult) {

    switch (betResult) {
        case "0" :
            return "Rock ค้อน";
        case "1" :
            return "Scissor กรรไกร";
        case "2" :
            return "Paper กระดาษ";
        default :
            return betResult
    }

}

function getBetPunpae(betResult) {

    switch (betResult) {
        case "head" :
            return "Head หัว";
        case "tail" :
            return "Tail ก้อย";
        default :
            return betResult
    }

}

function getBetNtpp(key) {

    switch (key) {
        case "point_1" :
            return "Crab";
        case "point_2" :
            return "Fish";
        case "point_3" :
            return "Calabash";
        case "point_4" :
            return "Tiger";
        case "point_5" :
            return "Chicken";
        case "point_6" :
            return "Shrimp";
        case "point_12" :
            return "Crab + Fish";
        case "point_13" :
            return "Crab + Fish";
        case "point_14" :
            return "Crab + Tiger";
        case "point_15" :
            return "Crab + Chicken";
        case "point_16" :
            return "Crab + Shrimp";
        case "point_23" :
            return "Fish + Calabash";
        case "point_24" :
            return "Fish + Tiger";
        case "point_25" :
            return "Fish + Chicken";
        case "point_26" :
            return "Fish + Shrimp";
        case "point_34" :
            return "Calabash + Tiger";
        case "point_35" :
            return "Calabash + Chicken";
        case "point_36" :
            return "Calabash + Shrimp";
        case "point_45" :
            return "Tiger + Chicken";
        case "point_46" :
            return "Tiger + Shrimp";
        case "point_56" :
            return "Chicken + Shrimp";
        default :
            return key
    }

}

function getResultHilo(betResult) {

    switch (betResult) {
        case "1" :
            return "One";
        case "2" :
            return "Two";
        case "3" :
            return "Three";
        case "4" :
            return "Four";
        case "5" :
            return "Five";
        case "6" :
            return "Six";
        default :
            return betResult
    }

}

function getBetHilo(betGame) {

    switch (betGame) {
        case "point_1" :
            return "One";
        case "point_2" :
            return "Two";
        case "point_3" :
            return "Three";
        case "point_4" :
            return "Four";
        case "point_5" :
            return "Five";
        case "point_6" :
            return "Six";
        case "point_12" :
            return "One + Two";
        case "point_13" :
            return "One + Three";
        case "point_14" :
            return "One + Four";
        case "point_15" :
            return "One + Five";
        case "point_16" :
            return "One + Six";
        case "point_23" :
            return "Two + Three";
        case "point_24" :
            return "Two + Four";
        case "point_25" :
            return "Two + Five";
        case "point_26" :
            return "Two + Six";
        case "point_34" :
            return "Three + Four";
        case "point_35" :
            return "Three + Five";
        case "point_36" :
            return "Three + Six";
        case "point_45" :
            return "Four + Five";
        case "point_46" :
            return "Four + Six";
        case "point_56" :
            return "Five + Six";
        case "triple_123" :
            return "One + Two + Three";
        case "triple_456" :
            return "Four + Five + Six";
        case "value_small" :
            return "Small";
        case "valuepoint_small_1" :
            return "Small + One";
        case "valuepoint_small_2" :
            return "Small + Two";
        case "valuepoint_small_3" :
            return "Small + Three";
        case "value_big" :
            return "Big";
        case "valuepoint_big_4" :
            return "Big + Four";
        case "valuepoint_big_5" :
            return "Big + Five";
        case "valuepoint_big_6" :
            return "Big + Six";
        case "hilo" :
            return "11 Hilo";
        default :
            return betGame
    }

}


function getResultTkPd(betGame) {

    switch (betGame) {
        case "0" :
            return "A-club";
        case "1" :
            return "2-club";
        case "2" :
            return "3-club";
        case "3" :
            return "4-club";
        case "4" :
            return "5-club";
        case "5" :
            return "6-club";
        case "6" :
            return "7-club";
        case "7" :
            return "8-club";
        case "8" :
            return "9-club";
        case "9" :
            return "10-club";
        case "10" :
            return "J-club";
        case "11" :
            return "Q-club";
        case "12" :
            return "k-club";
        case "13" :
            return "A-Diamond";
        case "14" :
            return "2-Diamond";
        case "15" :
            return "3-Diamond";
        case "16" :
            return "4-Diamond";
        case "17" :
            return "5-Diamond";
        case "18" :
            return "6-Diamond";
        case "19" :
            return "7-Diamond";
        case "20" :
            return "8-Diamond";
        case "21" :
            return "9-Diamond";
        case "22" :
            return "10-Diamond";
        case "23" :
            return "J-Diamond";
        case "24" :
            return "Q-Diamond";
        case "25" :
            return "K-Diamond";
        case "26" :
            return "A-Heart";
        case "27" :
            return "2-Heart";
        case "28" :
            return "3-Heart";
        case "29" :
            return "4-Heart";
        case "30" :
            return "5-Heart";
        case "31" :
            return "6-Heart";
        case "32" :
            return "7-Heart";
        case "33" :
            return "8-Heart";
        case "34" :
            return "9-Heart";
        case "35" :
            return "10-Heart";
        case "36" :
            return "J-Heart";
        case "37" :
            return "Q-Heart";
        case "38" :
            return "K-Heart";
        case "39" :
            return "A-Spade";
        case "40" :
            return "2-Spade";
        case "41" :
            return "3-Spade";
        case "42" :
            return "4-Spade";
        case "43" :
            return "5-Spade";
        case "44" :
            return "6-Spade";
        case "45" :
            return "7-Spade";
        case "46" :
            return "8-Spade";
        case "47" :
            return "9-Spade";
        case "48" :
            return "10-Spade";
        case "49" :
            return "J-Spade";
        case "50" :
            return "Q-Spade";
        case "51" :
            return "K-Spade";
        default :
            return betGame
    }

}




module.exports = {getBetKeyName,getResult,callCreateMember,callLaunchGame,callGameList,callGetReportDetails};