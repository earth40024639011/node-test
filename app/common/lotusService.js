const MemberModel = require('../models/member.model');
const request = require('request');
const _ = require('underscore');
const mongoose = require('mongoose');
const BetTransactionModel = require('../models/betTransaction.model.js');
const async = require("async");
const moment = require('moment');
const querystring = require('querystring');
const crypto = require("crypto");
const AgentGroupModel = require('../models/agentGroup.model.js');
const MemberService = require('../common/memberService');
const DateUtils = require('../common/dateUtils');
const NumberUtils = require('../common/numberUtils1');
const AgentService = require('../common/agentService');
const roundTo = require('round-to');


const LOTUS_URL = process.env.LOTUS_URL || 'https://api02.lotusgameportal.com/api/';
const LOTUS_AGENT_ID = process.env.LOTUS_AGENT_ID || 'sportbook';
const LOTUS_API_PWD = process.env.LOTUS_API_PWD || 'Jn8COZ';


function getHash(user) {
    let tmp;
    if (user) {
        tmp = LOTUS_AGENT_ID + '|' + LOTUS_API_PWD + '|' + user;
    } else {
        tmp = LOTUS_AGENT_ID + '|' + LOTUS_API_PWD;
    }

    return crypto.createHash('md5').update(tmp).digest("hex");
}


function prepareResult(memberId, playerId, roundId, callback) {

    getHistoryById(playerId, (err, response) => {

        let transaction = _.filter(response.detail, item => {
            // console.log(item.game_id + ' : '+roundId)
            return item.game_id === roundId;
        })[0];

        if (transaction) {

            let bettingResult = _.map(transaction.betting_log, item => {
                return {
                    bet: getBettingDetail(transaction.game_code, item.slot),
                    betAmount: item.cash
                }
            });

            let obj = {
                memberId: memberId,
                roundId: roundId,
                result: bettingResult
            };

            let condition = {
                memberId:  mongoose.Types.ObjectId(obj.memberId),
                source : 'LOTUS',
                'baccarat.lotus.roundId' : obj.roundId
            };

            let updateBody = {
                $set: {
                    'baccarat.lotus.betting':obj.result,
                    'baccarat.lotus.result':transaction.result
                }
            };


            BetTransactionModel.update(condition, updateBody, (err, data) => {
                callback(null, data);
            });
        } else {
            callback(null, response);
        }

    });


}
function getHistoryById(username, callback) {


    let startDate = moment().add(-1,'d').format('YYYY-MM-DD');
    let startTime = moment().add(-555, 'm').format('HH:mm:ss');
    let endDate = moment().format('YYYY-MM-DD');
    let endTime = moment().add(1, 'h').format('HH:mm:ss');


    let headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Cache-Control': 'no-cache'
    };

    const userId = username;

    let options = {
        url: LOTUS_URL + 'history?' + querystring.stringify(
            {
                agent_id: LOTUS_AGENT_ID,
                api_pwd: LOTUS_API_PWD,
                account: userId,
                startDate: startDate,
                startTime: startTime,
                endDate: endDate,
                endTime: endTime,
                hash: getHash(userId),
            }),
        method: 'GET',
        headers: headers
    };

    console.log(options)

    request(options, (err, response, body) => {
        // console.log(response,body,error)
        if (err) {
            callback(err, null);
        } else {

            const responseBody = JSON.parse(body);

            try {
                if (response.statusCode === 200) {

                    callback(null, responseBody);

                } else {
                    callback(responseBody, null);
                }
            } catch (e) {
                callback(e, null);
            }

        }
    });


}


function getBettingDetail(game, code) {

    console.log('game : ', game);
    console.log('code : ', code);
    if (game === 'baccarat') {
        switch (code) {
            case 0 :
                return "Player";
            case 1 :
                return "Banker";
            case 2 :
                return "Tie";
            case 3 :
                return "Player Pair";
            case 4 :
                return "Banker Pair";
            case 5 :
                return "Super6 Banker";
            case 6 :
                return "Super6";
            case 7 :
                return "Player 8 Natural";
            case 8 :
                return "Banker 8 Natural";
            case 9 :
                return "Player 9 Natural";
            case 10 :
                return "Banker 9 Natural";
            case 11 :
                return "Big";
            case 12 :
                return "Small";
            case 13 :
                return "Player Natural";
            case 14 :
                return "Banker Natural";
        }
    } else if (game === 'dragontiger') {
        switch (code) {
            case 0 :
                return "Dragon";
            case 1 :
                return "Tiger";
            case 2 :
                return "Tie";
        }
    } else if (game === 'roulette') {

        if (code <= 36) {
            return code;
        } else {

            switch (code) {
                case 37 :
                    return "0,1";
                case 38 :
                    return "1,4";
                case 39 :
                    return "4,7";
                case 40 :
                    return "7,10";
                case 41 :
                    return "10,13";
                case 42 :
                    return "13,16";
                case 43 :
                    return "16,19";
                case 44 :
                    return "19,22";
                case 45 :
                    return "22,25";
                case 46 :
                    return "25,28";
                case 47 :
                    return "28,31";
                case 48 :
                    return "31,34";
                case 49 :
                    return "1,2";
                case 50 :
                    return "4,5";
                case 51 :
                    return "7,8";
                case 52 :
                    return "10,11";
                case 53 :
                    return "13,14";
                case 54 :
                    return "16,17";
                case 55 :
                    return "19,20";
                case 56 :
                    return "22,23";
                case 57 :
                    return "25,26";
                case 58 :
                    return "28,29";
                case 59 :
                    return "31,32";
                case 60 :
                    return "34,35";
                case 61 :
                    return "0,2";
                case 62 :
                    return "2,5";
                case 63 :
                    return "5,8";
                case 64 :
                    return "8,11";
                case 65 :
                    return "11,14";
                case 66 :
                    return "14,17";
                case 67 :
                    return "17,20";
                case 68 :
                    return "20,23";
                case 69 :
                    return "23,26";
                case 70 :
                    return "26,29";
                case 71 :
                    return "29,32";
                case 72 :
                    return "32,35";
                case 73 :
                    return "2,3";
                case 74 :
                    return "5,6";
                case 75 :
                    return "8,9";
                case 76 :
                    return "11,12";
                case 77 :
                    return "14,15";
                case 78 :
                    return "17,18";
                case 79 :
                    return "20,21";
                case 80 :
                    return "23,24";
                case 81 :
                    return "26,27";
                case 82 :
                    return "29,30";
                case 83 :
                    return "32,33";
                case 84 :
                    return "35,36";
                case 85 :
                    return "0,3";
                case 86 :
                    return "3,6";
                case 87 :
                    return "6,9";
                case 88 :
                    return "9,12";
                case 89 :
                    return "12,15";
                case 90 :
                    return "15,18";
                case 91 :
                    return "18,21";
                case 92 :
                    return "21,24";
                case 93 :
                    return "24,27";
                case 94 :
                    return "27,30";
                case 95 :
                    return "30,33";
                case 96 :
                    return "33,36";
                case 97 :
                    return "1,2,3";
                case 98 :
                    return "2,5,6";
                case 99 :
                    return "7,8,9";
                case 100 :
                    return "10,11,12";
                case 101 :
                    return "13,14,15";
                case 102 :
                    return "16,17,18";
                case 103 :
                    return "19,20,21";
                case 104 :
                    return "22,23,24";
                case 105 :
                    return "25,26,27";
                case 106 :
                    return "28,29,30";
                case 107 :
                    return "31,32,33";
                case 108 :
                    return "34,35,36";
                case 109 :
                    return "0,1,2";
                case 110 :
                    return "1,2,4,5";
                case 111 :
                    return "4,5,7,8";
                case 112 :
                    return "7,8,10,11";
                case 113 :
                    return "10,11,13,14";
                case 114 :
                    return "13,14,16,17";
                case 115 :
                    return "16,17,19,20";
                case 116 :
                    return "19,20,22,23";
                case 117 :
                    return "22,23,25,26";
                case 118 :
                    return "25,26,28,29";
                case 119 :
                    return "28,29,31,32";
                case 120 :
                    return "31,32,34,35";
                case 121 :
                    return "0,2,3";
                case 122 :
                    return "2,3,5,6";
                case 123 :
                    return "5,6,8,9";
                case 124 :
                    return "8,9,11,12";
                case 125 :
                    return "11,12,14,15";
                case 126 :
                    return "14,15,17,18";
                case 127 :
                    return "17,18,20,21";
                case 128 :
                    return "20,21,23,24";
                case 129 :
                    return "23,24,26,27";
                case 130 :
                    return "26,27,29,30";
                case 131 :
                    return "29,30,32,33";
                case 132 :
                    return "32,33,35,36";
                case 133 :
                    return "1,2,3,4,5,6";
                case 134 :
                    return "4,5,6,7,8,9";
                case 135 :
                    return "7,8,9,10,11,12";
                case 136 :
                    return "10,11,12,13,14,15";
                case 137 :
                    return "13,14,15,16,17,18";
                case 138 :
                    return "16,17,18,19,20,21";
                case 139 :
                    return "19,20,21,22,23,24";
                case 140 :
                    return "22,23,24,25,26,27";
                case 141 :
                    return "25,26,27,28,29,30";
                case 142 :
                    return "28,29,30,31,32,33";
                case 143 :
                    return "31,32,33,34,35,36";
                case 144 :
                    return "0,1,2,3";
                case 145 :
                    return "1ST-12";
                case 146 :
                    return "2ND-12";
                case 147 :
                    return "3RD-12";
                case 148 :
                    return "1-12";
                case 149 :
                    return "13-24";
                case 150 :
                    return "25-36";
                case 151 :
                    return "LOW";
                case 152 :
                    return "HIGH";
                case 153 :
                    return "EVEN";
                case 154 :
                    return "ODD";
                case 155 :
                    return "BLACK";
                case 156 :
                    return "RED";
            }

        }
    } else if (game === 'sicbo') {
        switch (code) {
            case 0 :
                return "BIG";
            case 1 :
                return "SMALL";
            case 2 :
                return "TRIPLE-1";
            case 3 :
                return "TRIPLE-2";
            case 4 :
                return "TRIPLE-3";
            case 5 :
                return "TRIPLE-4";
            case 6 :
                return "TRIPLE-5";
            case 7 :
                return "TRIPLE-6";
            case 8 :
                return "ANYTRIPLE";
            case 9 :
                return "PAIR-1";
            case 10 :
                return "PAIR-2";
            case 11 :
                return "PAIR-3";
            case 12 :
                return "PAIR-4";
            case 13 :
                return "PAIR-5";
            case 14 :
                return "PAIR-6";
            case 15 :
                return "SUM-4";
            case 16 :
                return "SUM-17";
            case 17 :
                return "SUM-5";
            case 18 :
                return "SUM-16";
            case 19 :
                return "SUM-6";
            case 20 :
                return "SUM-15";
            case 21 :
                return "SUM-7";
            case 22 :
                return "SUM-14";
            case 23 :
                return "SUM-8";
            case 24 :
                return "SUM-13";
            case 25 :
                return "SUM-9";
            case 26 :
                return "SUM-10";
            case 27 :
                return "SUM-11";
            case 28 :
                return "SUM-12";
            case 29 :
                return "DOMINO-12";
            case 30 :
                return "DOMINO-13";
            case 31 :
                return "DOMINO-14";
            case 32 :
                return "DOMINO-15";
            case 33 :
                return "DOMINO-16";
            case 34 :
                return "DOMINO-23";
            case 35 :
                return "DOMINO-24";
            case 36 :
                return "DOMINO-25";
            case 37 :
                return "DOMINO-26";
            case 38 :
                return "DOMINO-34";
            case 39 :
                return "DOMINO-35";
            case 40 :
                return "DOMINO-36";
            case 41 :
                return "DOMINO-45";
            case 42 :
                return "DOMINO-46";
            case 43 :
                return "DOMINO-56";
            case 44 :
                return "DICE-1";
            case 45 :
                return "DICE-2";
            case 46 :
                return "DICE-3";
            case 47 :
                return "DICE-4";
            case 48 :
                return "DICE-5";
            case 49 :
                return "DICE-6";
            case 50 :
                return "ODD";
            case 51 :
                return "EVEN";
        }
    }
}


module.exports = {getHistoryById, getBettingDetail, prepareResult}