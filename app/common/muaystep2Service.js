const crypto = require('crypto');
const request = require('request');
const async = require("async");
const BetTransactionModel = require('../models/betTransaction.model');

const getClientKey = require('../controllers/getClientKey');
let {
    MUAY_STEP2_HOST: HOST = 'http://www.muaystep2.com',
    MUAY_STEP2_OPCODE: WEBSITE = 'ironbet',
    MUAY_STEP2_PRIVATE_KEY: PRIVATE_KEY = 'MuaY18',
    MUAY_STEP2_OPAGENT_ID: OPAGENTID = 'ironbetagent',
    MUAY_STEP2_PREFIX: PREFIX = 'ironbet_'
} = getClientKey.getKey();

const headers = {
    'Content-Type': 'application/json'
};


function getHash(val) {
    return crypto.createHash('md5').update(val).digest("hex");
}

function rejectTicketPartner(ref1, callback) {

    if(ref1){
        const option ={
            url: `${HOST}/sportsfundservice/reject/${WEBSITE}?inp=[${ref1}]&access=${getHash(`reject/${WEBSITE}/${PRIVATE_KEY}`)}`,
            method: 'GET',
            headers: headers
        };

        console.log('----option----', option);

        request(option, (err, response, body) => {
            if (err) {
                callback(err, null);
            } else {
                try {

                    let resultBody = JSON.parse(body);

                    console.log('result reject ticket body ==== ',resultBody);

                    if(resultBody.error.id === 0){
                        console.log('result reject ticket partner success ', resultBody);
                        callback(null, {code : 0, message: "Cancel ticket partner success", result: null});
                    }else {
                        console.log('result reject ticket partner fail ', resultBody);
                        callback(null, {code : 0, message: "Cancel ticket partner success", result: null})
                    }

                } catch (e) {
                    callback(err, null);
                }
            }
        });
    }else {
        callback(null, {code : 1, message: "BetId is empty", result: null})
    }


    // let condition = {'betId': betId, 'game': 'M2'};
    //
    // BetTransactionModel.findOne(condition)
    //     .select('memberId hdp commission amount memberCredit payout memberParentGroup betId status priceType source betResult game')
    //     .populate({path: 'memberParentGroup', select: 'type'})
    //     .exec(function (err, betObj) {
    //
    //         if(err){
    //             callback(err, null);
    //         }
    //
    //         if (!betObj) {
    //             callback(null, {code : 999, message: "data not found", result: null});
    //         }else if(betObj.hdp.ref1){
    //             const option ={
    //                 url: `${HOST}/sportsfundservice/reject/${WEBSITE}?inp=[${betId}]&access=${getHash(`reject/${WEBSITE}/${PRIVATE_KEY}`)}`,
    //                 method: 'GET',
    //                 headers: headers
    //             };
    //
    //             console.log('----option----', option);
    //
    //             request(option, (err, response, body) => {
    //                 if (err) {
    //                     callback(err, null);
    //                 } else {
    //                     try {
    //
    //                         let resultBody = JSON.parse(body);
    //
    //                         console.log('result reject ticket body ==== ',resultBody);
    //
    //                         if(resultBody.error.id === 0){
    //                             console.log('result reject ticket partner success ', resultBody);
    //                             callback(null, {code : 0, message: "Cancel ticket partner success", result: null});
    //                         }else {
    //                             console.log('result reject ticket partner fail ', resultBody);
    //                             callback(null, {code : 0, message: "Cancel ticket partner success", result: null})
    //                         }
    //
    //                     } catch (e) {
    //                         callback(err, null);
    //                     }
    //                 }
    //             });
    //         }else {
    //             callback(null, {code : 997, message: "Ref1 is empty", result: null});
    //         }
    //
    //     });

}



module.exports = {rejectTicketPartner}
