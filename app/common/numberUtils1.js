/**
 * Created by boonsongrit on 11/22/2017 AD.
 */
const moment = require('moment');
const roundTo = require('round-to');

module.exports = {

    convertPercent : function(percent) {
        return percent / 100
    },
    convertPercentZeroDefault : function(percent) {
        return (percent || 0) / 100
    },
    findBetAmountPercentage : function (amount, totalAmount) {
        return roundTo((amount/ totalAmount ) * 100, 3);
    }
};
