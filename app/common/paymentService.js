const config = require('config');
const jwt = require('jsonwebtoken');
const async = require("async");

const BetTransactionModel = require('../models/betTransaction.model.js');
const mongoose = require('mongoose');
const DateUtils = require('../common/dateUtils');
const moment = require('moment');
const _ = require('underscore');
const roundTo = require('round-to');


function getWinLoseReportMember(groupId,groupType, lastPaymentDate, callback) {

    let startYear = Number.parseInt(lastPaymentDate.getUTCFullYear());
    let startMonth = Number.parseInt(lastPaymentDate.getUTCMonth());
    let startDay = Number.parseInt(lastPaymentDate.getUTCDate());


    function diff_days(dt2, dt1) {

        let timeDiff = Math.abs(dt2.getTime() - dt1.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        return diffDays;

    }

    let diffDay = diff_days(lastPaymentDate,DateUtils.getCurrentDate());
    // console.log('diffDay : ',diffDay)

    let currentAmbDay = moment().hours() >= 11 ? moment().utc(true).date() : moment().utc(true).date() - 1;
    let currentAmbMonth = moment().hours() >= 11 ? moment().utc(true).month() : moment().utc(true).month();
    let currentAmbYear = moment().hours() >= 11 ? moment().utc(true).year() : moment().utc(true).year();

    // console.log('------------- :: ', agentId)
    // console.log('------------- lastPaymentDate :: ', lastPaymentDate)
    // console.log((startDay + 1))
    // console.log('currentAmbDay : ', currentAmbDay)
    //
    // console.log('startMonth : ', startMonth)
    // console.log('currentAmbMonth : ', currentAmbMonth)
    //
    // console.log('startYear : ', startYear)
    // console.log('currentAmbYear : ', currentAmbYear)


    if ( (diffDay > 60  || (((startDay + 1) == currentAmbDay) && (startMonth == currentAmbMonth) && (startYear == currentAmbYear)))) {
        callback(null, []);
        return;
    }

    let dateFrom , dateTo;
    let timeFrom = '11:00:00.000';
    let timeTo = '11:00:00.000';
    if (moment().hours() >= 11) {
        dateFrom = moment([startYear, startMonth, startDay]).add(1, 'd').utc(true).format('DD-MM-YYYY');
        dateTo = moment().add(0, 'd').utc(true).format('DD-MM-YYYY');
    } else {
        dateFrom = moment([startYear, startMonth, startDay]).add(1, 'd').utc(true).format('DD-MM-YYYY');
        dateTo = moment().add(-1, 'd').utc(true).format('DD-MM-YYYY');
    }


    const CLIENT_NAME = process.env.CLIENT_NAME || 'SPORTBOOK88';
    let taskHour;
     if(_.isEqual(CLIENT_NAME, 'CSR')){
         taskHour = 12;
    }else if(_.isEqual(CLIENT_NAME, 'BET123')){
         taskHour = 12;
    }else if(_.isEqual(CLIENT_NAME, 'AMBBET')){
         taskHour = 12;
    } else {
         taskHour = 24;
    }

    let dateList = DateUtils.enumerateReportBetweenDatesTask(moment(dateFrom, "DD/MM/YYYY"), moment(dateTo, "DD/MM/YYYY"), timeFrom,timeTo,taskHour);

    let tasks = [];

    _.forEach(dateList, dateRange => {
        tasks.push(getWinLoseAccountAgentPerDay(groupId, groupType, dateRange));
    });


    async.parallel(tasks, (err, results) => {
        if (err) {
            console.log('err 11 : ', err)
        }

        let summaryDataPerDay = summaryFunction(results);
        //
        callback(null, summaryDataPerDay);
    })

}


function getWinLoseAccountAgentPerDay(groupId, groupType, dateRange) {

    return function (callbackTask) {


        let condition = {
            'gameDate': {
                "$gte": moment(dateRange[0], 'YYYY-MM-DDTHH:mm:ss').utc(true).toDate(),
                "$lt": moment(dateRange[1], 'YYYY-MM-DDTHH:mm:ss').utc(true).toDate()
            }
        };

        condition['commission.' + groupType + '.group'] = mongoose.Types.ObjectId(groupId);

        condition.status = 'DONE';


        BetTransactionModel.aggregate([
            {
                $match: condition
            },
            {
                "$group": {
                    "_id": {
                        commission: '$commission.' + groupType + '.group',
                    },
                    "amount": {$sum: '$amount'},
                    "validAmount": {$sum: '$validAmount'},
                    "memberWinLose": {$sum: '$commission.member.winLose'},
                    "memberWinLoseCom": {$sum: '$commission.member.winLoseCom'},
                    "memberTotalWinLoseCom": {$sum: '$commission.member.totalWinLoseCom'},

                    "agentWinLose": {$sum: '$commission.agent.winLose'},
                    "agentWinLoseCom": {$sum: '$commission.agent.winLoseCom'},
                    "agentTotalWinLoseCom": {$sum: '$commission.agent.totalWinLoseCom'},

                    "masterAgentWinLose": {$sum: '$commission.masterAgent.winLose'},
                    "masterAgentWinLoseCom": {$sum: '$commission.masterAgent.winLoseCom'},
                    "masterAgentTotalWinLoseCom": {$sum: '$commission.masterAgent.totalWinLoseCom'},

                    "seniorWinLose": {$sum: '$commission.senior.winLose'},
                    "seniorWinLoseCom": {$sum: '$commission.senior.winLoseCom'},
                    "seniorTotalWinLoseCom": {$sum: '$commission.senior.totalWinLoseCom'},

                    "shareHolderWinLose": {$sum: '$commission.shareHolder.winLose'},
                    "shareHolderWinLoseCom": {$sum: '$commission.shareHolder.winLoseCom'},
                    "shareHolderTotalWinLoseCom": {$sum: '$commission.shareHolder.totalWinLoseCom'},

                    "companyWinLose": {$sum: '$commission.company.winLose'},
                    "companyWinLoseCom": {$sum: '$commission.company.winLoseCom'},
                    "companyTotalWinLoseCom": {$sum: '$commission.company.totalWinLoseCom'},

                    "superAdminWinLose": {$sum: '$commission.superAdmin.winLose'},
                    "superAdminWinLoseCom": {$sum: '$commission.superAdmin.winLoseCom'},
                    "superAdminTotalWinLoseCom": {$sum: '$commission.superAdmin.totalWinLoseCom'}
                }
            },
            {
                $project: {
                    _id: 0,
                    group: '$_id.commission',
                    type: 'A_GROUP',
                    amount: '$amount',
                    validAmount: '$validAmount',
                    memberWinLose: '$memberWinLose',
                    memberWinLoseCom: '$memberWinLoseCom',
                    memberTotalWinLoseCom: '$memberTotalWinLoseCom',
                    agentWinLose: '$agentWinLose',
                    agentWinLoseCom: '$agentWinLoseCom',
                    agentTotalWinLoseCom: '$agentTotalWinLoseCom',
                    masterAgentWinLose: '$masterAgentWinLose',
                    masterAgentWinLoseCom: '$masterAgentWinLoseCom',
                    masterAgentTotalWinLoseCom: '$masterAgentTotalWinLoseCom',
                    seniorWinLose: '$seniorWinLose',
                    seniorWinLoseCom: '$seniorWinLoseCom',
                    seniorTotalWinLoseCom: '$seniorTotalWinLoseCom',
                    shareHolderWinLose: '$shareHolderWinLose',
                    shareHolderWinLoseCom: '$shareHolderWinLoseCom',
                    shareHolderTotalWinLoseCom: '$shareHolderTotalWinLoseCom',
                    companyWinLose: '$companyWinLose',
                    companyWinLoseCom: '$companyWinLoseCom',
                    companyTotalWinLoseCom: '$companyTotalWinLoseCom',
                    superAdminWinLose: '$superAdminWinLose',
                    superAdminWinLoseCom: '$superAdminWinLoseCom',
                    superAdminTotalWinLoseCom: '$superAdminTotalWinLoseCom',

                }
            }
        ]).read('secondaryPreferred').option({maxTimeMS:150000}).exec((err, results) => {

            if (err) {
                callbackTask(err, null);
            } else {
                callbackTask(null, results);
            }
        });

    }
}


function summaryFunction(transactions) {

    transactions = transactions.filter(item => {
        return item && item.length > 0;
    });

    return _.reduce(transactions, function (preList, nextList) {

        let uniqueData = _.uniq(_.union(preList, nextList), false, function (item, key, a) {
            return item.group.toString();
        });

        return _.map(uniqueData, (uniqueObj) => {

            let preData = _.filter(preList, (preObj) => {
                return uniqueObj.group.toString() === preObj.group.toString();
            })[0];

            let next = _.filter(nextList, (nextObj) => {
                return uniqueObj.group.toString() === nextObj.group.toString();
            })[0];


            if (preData && next) {

                return {
                    group: preData.group,
                    type: preData.type,
                    currency: preData.currency,

                    amount: preData.amount + next.amount,
                    validAmount: preData.validAmount + next.validAmount,
                    stackCount: preData.stackCount + next.stackCount,
                    grossComm: preData.grossComm + next.grossComm,

                    memberWinLose: roundTo(preData.memberWinLose, 5) + roundTo(next.memberWinLose, 5),
                    memberWinLoseCom: roundTo(preData.memberWinLoseCom, 5) + roundTo(next.memberWinLoseCom, 5),
                    memberTotalWinLoseCom: roundTo(preData.memberTotalWinLoseCom, 5) + roundTo(next.memberTotalWinLoseCom, 5),

                    agentWinLose: roundTo(preData.agentWinLose, 5) + roundTo(next.agentWinLose, 5),
                    agentWinLoseCom: roundTo(preData.agentWinLoseCom, 5) + roundTo(next.agentWinLoseCom, 5),
                    agentTotalWinLoseCom: roundTo(preData.agentTotalWinLoseCom, 5) + roundTo(next.agentTotalWinLoseCom, 5),

                    masterAgentWinLose: roundTo(preData.masterAgentWinLose, 5) + roundTo(next.masterAgentWinLose, 5),
                    masterAgentWinLoseCom: roundTo(preData.masterAgentWinLoseCom, 5) + roundTo(next.masterAgentWinLoseCom, 5),
                    masterAgentTotalWinLoseCom: roundTo(preData.masterAgentTotalWinLoseCom, 5) + roundTo(next.masterAgentTotalWinLoseCom, 5),

                    seniorWinLose: roundTo(preData.seniorWinLose, 5) + roundTo(next.seniorWinLose, 5),
                    seniorWinLoseCom: roundTo(preData.seniorWinLoseCom, 5) + roundTo(next.seniorWinLoseCom, 5),
                    seniorTotalWinLoseCom: roundTo(preData.seniorTotalWinLoseCom, 5) + roundTo(next.seniorTotalWinLoseCom, 5),

                    shareHolderWinLose: roundTo(preData.shareHolderWinLose, 5) + roundTo(next.shareHolderWinLose, 5),
                    shareHolderWinLoseCom: roundTo(preData.shareHolderWinLoseCom, 5) + roundTo(next.shareHolderWinLoseCom, 5),
                    shareHolderTotalWinLoseCom: roundTo(preData.shareHolderTotalWinLoseCom, 5) + roundTo(next.shareHolderTotalWinLoseCom, 5),

                    companyWinLose: roundTo(preData.companyWinLose, 5) + roundTo(next.companyWinLose, 5),
                    companyWinLoseCom: roundTo(preData.companyWinLoseCom, 5) + roundTo(next.companyWinLoseCom, 5),
                    companyTotalWinLoseCom: roundTo(preData.companyTotalWinLoseCom, 5) + roundTo(next.companyTotalWinLoseCom, 5),

                    superAdminWinLose: roundTo(preData.superAdminWinLose, 5) + roundTo(next.superAdminWinLose, 5),
                    superAdminWinLoseCom: roundTo(preData.superAdminWinLoseCom, 5) + roundTo(next.superAdminWinLoseCom, 5),
                    superAdminTotalWinLoseCom: roundTo(preData.superAdminTotalWinLoseCom, 5) + roundTo(next.superAdminTotalWinLoseCom, 5)
                };
            } else if (preData) {
                return preData;
            } else if (next) {
                return next;
            }

        });

    });


}

module.exports = {
    getWinLoseReportMember
}
