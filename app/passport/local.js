const cfg = require('config');
const User = require("../models/users.model");
const MemberModel = require("../models/member.model");
const AgentService = require("../common/agentService");
const mongoose = require('mongoose');
const Hashing = require('../common/hashing');
const roundTo = require('round-to');
const DateUtils = require('../common/dateUtils');
const ip = require('ip');
const uuid = require('uuid/v4');

const LocalStrategy = require('passport-local').Strategy;

module.exports = function (passport) {


    passport.use('local', new LocalStrategy({
            usernameField: 'username',
            passwordField: 'password'
        }, function (username, password, done) {

            process.nextTick(function () {

                // find the user in the database based on their facebook id
                Hashing.encrypt256(password, function (err, encryptPass) {
                    if (err) {
                        return done(err);
                    }

                    User.findOne({
                        'username_lower': username.toLowerCase(),
                        'password': encryptPass,
                        active: true
                    }, 'contact username username_lower group permission currency', function (err, response) {
                        // if there is an error, stop everything and return that
                        // ie an error connecting to the database

                        if (err) {
                            return done(err);
                        }

                        if (!response || !response.group.active) {
                            return done(null, false, {
                                message: 'Incorrect username or password.',
                                code: 1001
                            });
                        }

                        if (response.group.lock) {
                            return done(null, false, {message: 'Account Locked.', code: 1006});
                        }

                        AgentService.getUpLineStatus(response.group._id, (err, uplineResponse) => {
                            if (err) {
                                return done(null, false, {message: err, code: 999});
                            }
                            if (uplineResponse.lock) {
                                return done(null, false, {message: 'Upline is Locked.', code: 1007});
                            }

                            let user = Object.assign({}, response)._doc;
                            user.isSuspend = uplineResponse.suspend;
                            user.uid = uuid();
                            user.downline = uplineResponse.downline;
                            return done(null, user);
                        });

                    }).populate({
                        path: 'group',
                        select: {'type': 1, 'name': 1,'name_lower': 1, '_id': 1, 'lock': 1, 'suspend': 1, 'active': 1,'currency':1}
                    })
                });

            });
        }
    ));

    passport.use('member', new LocalStrategy({
            usernameField: 'username',
            passwordField: 'password'
        }, function (username, password, done) {

            process.nextTick(function () {

                // find the user in the database based on their facebook id
                Hashing.encrypt256(password, function (err, encryptPass) {
                    if (err) {
                        return done(err);
                    }
                    let condition = {};
                    condition['$and'] = [
                        {$or: [{'username_lower': username.toLowerCase()}, {'loginName_lower': username.toLowerCase()}]},
                        {'password': encryptPass},
                        {active: true}];

                    MemberModel.aggregate([
                        {
                            $match: condition
                        },
                        {
                            $project: {
                                _id: '$_id',
                                contact: '$contact',
                                username: '$username',
                                username_lower: '$username_lower',
                                group: '$group',
                                currency: '$currency',
                                limitSetting: '$limitSetting',
                                commissionSetting: '$commissionSetting',
                                configuration: '$configuration',
                                credit: {$add: ['$creditLimit', '$balance']},
                                termAndCondition: '$termAndCondition',
                                forceChangePassword: '$forceChangePassword',
                                forceChangePasswordDate: '$forceChangePasswordDate',
                                lock: '$lock',
                                suspend:'$suspend',
                                isAutoTopUp:'$isAutoTopUp'
                            }
                        },
                    ], (err, results) => {
                        if (err) {
                            return done(err);
                        }
                        if (!results[0]) {
                            return done(null, false, {
                                message: 'Incorrect username or password.',
                                code: 1001
                            });
                        }

                        let user = results[0];
                        user.uid = uuid();
                        user.credit = roundTo(user.credit,2);

                        if (user.lock) {
                            return done(null, false, {message: 'Account Lock.', code: 1006});
                        }

                        AgentService.getUpLineStatus(user.group, (err, uplineResponse) => {
                            if (err) {
                                return done(null, false, {message: err, code: 999});
                            }
                            if (uplineResponse.lock) {
                                return done(null, false, {message: 'Upline is Locked.', code: 1007});
                            }

                            if (user.forceChangePasswordDate && DateUtils.getCurrentDate() >= user.forceChangePasswordDate.getTime()) {
                                user.forceChangePassword = true;
                            }

                            user.isSuspend = uplineResponse.suspend || user.suspend;
                            delete user.lock;

                            MemberModel.populate(user, {
                                path: 'group',
                                select: 'type name _id prefixAutoTopUp'
                            }, function (err, memberResult) {
                                if (err) {
                                    return done(null, false, {message: 'Populate failed.'});
                                } else {


                                    MemberModel.findOne(
                                        {
                                            _id: user._id
                                        },
                                    (error, response) => {
                                        if (error) {
                                            return done(null, user);
                                        } else {
                                            if (response.isAutoTopUp) {
                                                user.isAutoTopUp = true;
                                            } else {
                                                user.isAutoTopUp = false;
                                            }
                                            return done(null, user);
                                        }
                                    }).select('isAutoTopUp');

                                }
                            });
                        });


                    });
                });

            });
        }
    ));

}