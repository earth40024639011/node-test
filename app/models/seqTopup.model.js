const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    ref: {type: String, unique: true, required: true},
    createdDate:{type: Date, expires: 20, default: Date.now}
});

schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.createdDate) {
        this.createdDate = now;
    }
    next();
});

module.exports = mongoose.model('SeqTopup', schema);