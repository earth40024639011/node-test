/**
 * Created by boonsongrit on 1/26/2018 AD.
 */
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    OCode: {type: String, unique:true},
    Username: {type: String},
    GameCode: {type: String},
    Description: {type: String},
    Type: {type: String},
    Amount: {type:Number},
    Result: {type:Number},
    Time: {type: Date},
    Details: {type:String},
    AppID: {type:String},
    CurrencyCode: {type:String},
    createDate: { type: Date, default: Date.now }
});


schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.createDate) {
        //this.created = moment(now).utcOffset('+0700');
        this.createDate = now;
    }
    next();
});

schema.plugin(mongoosePaginate);

module.exports = mongoose.model('slotJokerTransaction', schema);