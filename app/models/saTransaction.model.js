const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    username:{ type: String },
    currency:{ type: String },
    amount:{ type: String },
    txnId:{ type: String },
    txnReverseId:{ type: String },
    gameType:{ type: String },
    platform:{ type: String },
    gameCode:{ type: String },
    hostId:{ type: String },
    gameId:{ type: String },
    payoutTime:{type:Date},
    action:{type:String},
    createdDate: {type: Date},
});

schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);

module.exports = mongoose.model('SaTransaction', schema);