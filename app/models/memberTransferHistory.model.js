const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    transferId: {type: String, required: true,unique:true},
    member: {type: Schema.Types.ObjectId, ref: 'Member', required: true},
    from: {type: String, enum: ['MAIN','SEXY_BACCARAT','SLOT_XO'],required: true},
    to:{type: String, enum: ['MAIN','SEXY_BACCARAT','SLOT_XO'],required: true},
    amount: {type: Number, required: false, default: 0},
    active: {type: Boolean, default: true},
    createdDate: {type: Date, default: Date.now},
    updatedDate: {type: Date}
});


schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);

module.exports = mongoose.model('MemberTransferHistory', schema);