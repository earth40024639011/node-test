const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    username:{ type: String },
    currency:{ type: String },
    amount:{ type: String },
    txnId:{ type: String },
    txnReverseId:{ type: String },
    gameType:{ type: String },
    platform:{ type: String },
    gameCode:{ type: String },
    hostId:{ type: String },
    gameId:{ type: String },
    payoutTime:{type:Date},
    action:{type:String},
    realBody:{type:String},
    createdDate: { type: Date, expires: ((60 * 60) * 24) * 5, default: Date.now }

});

schema.pre('save', function (next) {
    const now = new Date();
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);

schema.index({ username: 1,createdDate:1});
schema.index({ username : 1, gameId : 1, txnReverseId: 1},{name:'find_cancel_bet'});

module.exports = mongoose.model('SaTransaction2', schema);