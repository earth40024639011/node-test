const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    betIdKey: {type: String, required: true, unique:true},
    betId: {type: String},
    memberId: {type: Schema.Types.ObjectId, ref: 'Member', default: null},
    memberUsername : {type:String},
    leagueId: {type: String, required: false},
    matchId: {type: String, required: false},
    leagueName: {type: String, required: false},
    leagueNameN : {
        en : { type: String, required: false },
        th : { type: String, required: false },
        cn : { type: String, required: false },
        tw : { type: String, required: false }
    },
    matchName: {
        en : {
            h : { type: String },
            a : { type: String }
        },
        th : {
            h : { type: String },
            a : { type: String }
        },
        cn : {
            h : { type: String },
            a : { type: String }
        },
        tw : {
            h : { type: String },
            a : { type: String }
        }
    },
    score: {type: String, required: false},
    hScore: {type: Number, required: false},
    aScore: {type: Number, required: false},
    liveTimeBet: {type: String, default: ''},
    source:{type: String,enum: ['FOOTBALL'], required: false, default: 'FOOTBALL'},
    remark: { type: String },
    isCanceled: {type: Boolean,required: true, default:false},
    createdDate: { type: Date, expires: 90000, default: Date.now },
    roundIncorrect : { type: Number, default : 0},
    oddType: {type: String}
});


schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);
schema.index({ _id: 1, status: 1 },{unique: true});

module.exports = mongoose.model('BetLiveTransaction', schema);