const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    gameId: {type: String},
    // _2BOT_OE: {type: String, required: true},
    // _2BOT_OU: {type: String, required: true},
    _2BOT: {type: String, required: false},
    // _2TOP_OE: {type: String, required: true},
    // _2TOP_OU: {type: String, required: true},
    _2TOP: {type: String, required: false},
    // _3TOP_OE: {type: String, required: true},
    // _3TOP_OU: {type: String, required: true},
    _3TOP: {type: String, required: false},
    _4TOP: {type: String, required: false},
    _5TOP: {type: String, required: false},
    _6TOP: {type: String, required: false},
    _6TOD: [{type: String, required: false}],
    _5TOD: [{type: String, required: false}],
    _4TOD: [{type: String, required: false}],
    _3TOD: [{type: String, required: false}],
    // _3BOT: [{type: String, required: false}],
    _2TOD: [{type: String, required: false}],
    _1TOP: [{type: String, required: false}],
    _1BOT: [{type: String, required: false}],
    createdDate: {type: Date, default: Date.now},
});

schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);

module.exports = mongoose.model('LottoPPSettleHistory', schema);