const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    betId: {type: String, required: true, unique: true},
    memberId: {type: Schema.Types.ObjectId, ref: 'Member', default: null},
    username: {type: String},
    amount :{type: Number, required: true},
    memberCredit: {type: Number, required: true},
    leagueId: {type: String, required: false},
    matchId: {type: String, required: false},
    odd: {type: Number, required: false},
    oddKey: {type: String, required: false},
    oddType: {
        type: String,
        enum: ['AH', 'OU', 'OE', 'X12', 'AH1ST', 'OU1ST', 'OE1ST', 'X121ST','T1OU','T2OU','ML'],
        required: false
    },
    handicap: {type: String, required: false},
    matchType: {type: String, enum: ['NONE_LIVE', 'LIVE'], required: false},
    priceType: {type: String, enum: ['HK', 'MY', 'ID', 'EU', 'TH'], required: true},
    typeHdpOuOe : {type: String},
    key: {type: String},
    remark: {type: String},
    isAutoUpdateOdd : {type:Boolean,default:false},
    createdDate: { type: Date, default: Date.now }
});


schema.index({ createdDate: 1 }, { expireAfterSeconds: 58 });

schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.createdDate) {
        this.createdDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);


module.exports = mongoose.model('RobotTransaction', schema);