const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    memberId: {type: Schema.Types.ObjectId, ref: 'Member', default: null},
    type:{type: String, enum: ['ADD','REDUCE'],required: false},
    ref: {type: String},
    beforeCredit: {type: Number},
    afterCredit: {type: Number},
    beforeBalance: {type: Number},
    afterBalance: {type: Number},
    amount: {type: Number},
    remark:{type:String},
    createdDate:{type: Date, default: Date.now},
});

schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);

schema.index({ memberId: 1, type: 1 , ref:1,amount: 1},{unique: true});

module.exports = mongoose.model('CreditModifyHistory', schema);