const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    action: {type: String, required: true},
    matchId: {type: String, required: false},
    matchDate:{type:Date,required:false},
    leagueName: {type: String, required: false},
    leagueNameN : {
                en : { type: String, required: false },
                th : { type: String, required: false },
                cn : { type: String, required: false },
                tw : { type: String, required: false }
            },
    matchName: {
                en : {
                    h : { type: String },
                    a : { type: String }
                },
                th : {
                    h : { type: String },
                    a : { type: String }
                },
                cn : {
                    h : { type: String },
                    a : { type: String }
                },
                tw : {
                    h : { type: String },
                    a : { type: String }
                }
            },
    scoreHT:{type: String, required: false},
    scoreFT:{type: String, required: false},
    txns: [{
        username: {type: String, required: false},
        betId: {type: String, required: false},
        betAmount: {type: String, required: false},
        betTime: {type: String, required: false},
        winLose: {type:Number,required:false},
        winLoseCom: {type:Number,required:false},
        totalWinLoseCom: {type:Number,required:false},
        betResult:{type: String,enum: ['WIN','HALF_WIN','LOSE','HALF_LOSE','DRAW'],required: false},
    }],
    result:{type:String},
    resultCode : {type:String},
    createdDate: {type: Date, default: Date.now},
    updatedDate: {type: Date}
});


schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);
schema.index({ _id: 1, status: 1 },{unique: true});

module.exports = mongoose.model('ApiSettleTransaction', schema);