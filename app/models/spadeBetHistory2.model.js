const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    transferId: {type:String},
    acctId: {type:String},
    currency: {type:String},
    amount: {type: Number},
    type: {type: Number},
    channel: {type:String},
    gameCode:{type:String},
    ticketId: {type:String},
    referenceId: {type:String},
    specialGame: {
        type:{type:String},
        count:{type: Number},
        sequence:{type: Number},
    },
    refTicketIds: {type:String},
    createdDate: { type: Date, expires: ((60 * 60) * 24) * 7, default: Date.now }
});


schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);
schema.index({ acctId: 1, 'transferId': 1 ,'createdDate':1});
module.exports = mongoose.model('SpadeBetHistory2', schema);