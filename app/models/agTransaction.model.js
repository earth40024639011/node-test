const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    username:{ type: String },
    currency:{ type: String },
    amount:{ type: String },
    netAmount:{ type: String },
    validBetAmount:{ type: String },
    createdDate: {type: Date},
    settleTime: {type: Date},
    transactionID:{ type: String },
    gameType:{ type: String },
    platform:{ type: String },
    round:{ type: String },
    gameCode:{ type: String },
    tableCode:{ type: String },
    transactionType: { type: String },
    ticketStatus: { type: String },
    gameResult: { type: String },
    transactionCode: { type: String },
    deviceType: { type: String },
    playType: { type: String },
    isFinish: { type: Boolean },


});

schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);

module.exports = mongoose.model('AgTransaction', schema);