const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const themeSchema = new mongoose.Schema({
    k : { type: Number, index: { unique: true }},
    n : { type: String },
    nn : {
        en : { type: String },
        th : { type: String },
        cn : { type: String },
        tw : { type: String }
    },
    sk : { type: Number, default : 100},
    r : {
        ah : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        ahfts : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        ml : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        mls1 : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        mls2 : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        ous1 : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        ous2 : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        oufts : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        oefts : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        pm : { type: Number, default: 500000 }
    },
    rem : {
        ah : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        ahfts : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        ml : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        mls1 : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        mls2 : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        ous1 : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        ous2 : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        oufts : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        oefts : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        pm : { type: Number, default: 500000 }
    },
    rl : {
        ah : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        ahfts : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        ml : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        mls1 : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        mls2 : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        ous1 : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        ous2 : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        oufts : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        oefts : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        pm : { type: Number, default: 500000 }
    },

    rm : {
        ah : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        ahfts : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        ml : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        mls1 : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        mls2 : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        ous1 : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        ous2 : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        oufts : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        oefts : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        pm : { type: Number, default: 500000 }
    },
    rlm : {
        ah : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        ahfts : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        ml : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        mls1 : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        mls2 : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        ous1 : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        ous2 : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        oufts : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        oefts : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        pm : { type: Number, default: 500000 }
    },
    rmem : {
        ah : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        ahfts : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        ml : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        mls1 : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        mls2 : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        ous1 : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        ous2 : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        oufts : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        oefts : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 10 }
        },
        pm : { type: Number, default: 500000 }
    },
    m : [{
            id: { type: String },
            k : { type: Number },
            d : { type: Date },
            hasParlay : { type: Boolean },
            i : {
                q : { type: String },
                s : { type: String }
            },
            s : {
                isH : { type : Boolean, default : true},//HDP
                isL : { type : Boolean, default : false},//LIVE
                isE : { type : Boolean, default : false},//END
                isCr : { type : Boolean, default : false},//END
                crFtDate : { type: Date },
                crHtDate : { type: Date },
                cr : {
                    setPoint1 : {
                        h : { type: Number, default: 0 },
                        a : { type: Number, default: 0 },
                    },
                    setPoint2 : {
                        h : { type: Number, default: 0 },
                        a : { type: Number, default: 0 },
                    },
                    setPoint3 : {
                        h : { type: Number, default: 0 },
                        a : { type: Number, default: 0 },
                    },
                    setPoint4 : {
                        h : { type: Number, default: 0 },
                        a : { type: Number, default: 0 },
                    },
                    setPoint5 : {
                        h : { type: Number, default: 0 },
                        a : { type: Number, default: 0 },
                    },
                    gamePoint : {
                        h : { type: Number, default: 0 },
                        a : { type: Number, default: 0 },
                    },
                    gameSet : {
                        h : { type: Number, default: 0 },
                        a : { type: Number, default: 0 },
                    }
                },
                isC : {//Cancel
                    ft : { type: Boolean, default: false },
                    ht : { type: Boolean, default: false }
                },
                isAl : { type : Boolean, default : true},//Allow to Live
                isD : { type : Boolean, default : false}//DONE
            },
            n : {
                en : {
                    h : { type: String },
                    a : { type: String }
                },
                th : {
                    h : { type: String },
                    a : { type: String }
                },
                cn : {
                    h : { type: String },
                    a : { type: String }
                },
                tw : {
                    h : { type: String },
                    a : { type: String }
                }
            },
            isToday : { type: Boolean, default : false},
            r : {
                pm : { type: Number, default: 500000 }
            },
            rem : {
                pm : { type: Number, default: 500000 }
            },
            bp : [{
                allKey: [{ type: String }],
                isOn:{ type : Boolean, default : true},
                ah : {
                    h : {type: String},
                    hk : {type: String},
                    hp: {type: String},
                    hpk : {type: String},
                    a : {type: String},
                    ak : {type: String},
                    ap: {type: String},
                    apk : {type: String}
                },
                ahfts : {
                    h : {type: String},
                    hk : {type: String},
                    hp: {type: String},
                    hpk : {type: String},
                    a : {type: String},
                    ak : {type: String},
                    ap: {type: String},
                    apk : {type: String}
                },
                ml : {
                    h : {type: String},
                    hk : {type: String},
                    a : {type: String},
                    ak : {type: String}
                },
                mls1 : {
                    h : {type: String},
                    hk : {type: String},
                    a : {type: String},
                    ak : {type: String}
                },
                mls2 : {
                    h : {type: String},
                    hk : {type: String},
                    a : {type: String},
                    ak : {type: String}
                },
                ous1 : {
                    o : {type: String},
                    ok : {type: String},
                    op: {type: String},
                    opk : {type: String},
                    u : {type: String},
                    uk : {type: String},
                    up: {type: String},
                    upk : {type: String}
                },
                ous2 : {
                    o : {type: String},
                    ok : {type: String},
                    op: {type: String},
                    opk : {type: String},
                    u : {type: String},
                    uk : {type: String},
                    up: {type: String},
                    upk : {type: String}
                },
                oufts : {
                    o : {type: String},
                    ok : {type: String},
                    op: {type: String},
                    opk : {type: String},
                    u : {type: String},
                    uk : {type: String},
                    up: {type: String},
                    upk : {type: String}
                },
                oefts : {
                    o : {type: String},
                    ok : {type: String},
                    e : {type: String},
                    ek : {type: String}
                }
            }],
            bpl : [{
                allKey: [{ type: String }],
                isOn:{ type : Boolean, default : true},
                ah : {
                    h : {type: String},
                    hk : {type: String},
                    hp: {type: String},
                    hpk : {type: String},
                    a : {type: String},
                    ak : {type: String},
                    ap: {type: String},
                    apk : {type: String}
                },
                ahfts : {
                    h : {type: String},
                    hk : {type: String},
                    hp: {type: String},
                    hpk : {type: String},
                    a : {type: String},
                    ak : {type: String},
                    ap: {type: String},
                    apk : {type: String}
                },
                ml : {
                    h : {type: String},
                    hk : {type: String},
                    a : {type: String},
                    ak : {type: String}
                },
                mls1 : {
                    h : {type: String},
                    hk : {type: String},
                    a : {type: String},
                    ak : {type: String}
                },
                mls2 : {
                    h : {type: String},
                    hk : {type: String},
                    a : {type: String},
                    ak : {type: String}
                },
                ous1 : {
                    o : {type: String},
                    ok : {type: String},
                    op: {type: String},
                    opk : {type: String},
                    u : {type: String},
                    uk : {type: String},
                    up: {type: String},
                    upk : {type: String}
                },
                ous2 : {
                    o : {type: String},
                    ok : {type: String},
                    op: {type: String},
                    opk : {type: String},
                    u : {type: String},
                    uk : {type: String},
                    up: {type: String},
                    upk : {type: String}
                },
                oufts : {
                    o : {type: String},
                    ok : {type: String},
                    op: {type: String},
                    opk : {type: String},
                    u : {type: String},
                    uk : {type: String},
                    up: {type: String},
                    upk : {type: String}
                },
                oefts : {
                    o : {type: String},
                    ok : {type: String},
                    e : {type: String},
                    ek : {type: String}
                }
            }]
        }]
});

themeSchema.pre('save', (next) => {
    if (!this.created) {
        this.created = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    }
    next();
});

module.exports = mongoose.model('TennisTrash', themeSchema);