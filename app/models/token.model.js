const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    username : {type: String, unique: true},
    uniqueId: {type:String, default: null},
    type:{type:String,enum: ['AGENT','MEMBER']},
    createdAt: { type: Date, expires: 60}
});

schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);

module.exports = mongoose.model('Token', schema);