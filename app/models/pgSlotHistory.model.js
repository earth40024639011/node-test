const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    operator_token: {type: String},
    secret_key: {type: String},
    operator_player_session: {type: String},
    game_id: {type: String},
    parent_bet_id: {type: String},
    bet_id: {type: String},
    player_name: {type: String},
    currency_code: {type: String},
    transfer_amount: {type: Number},
    transaction_id: {type: String},
    bet_transaction_id: {type: String},
    wallet_type: {type: String},
    bet_type: {type: String},
    updated_time: {type: String},
    is_end_round: {type: String},
    is_feature: {type: String},
    is_minus_count: {type: String},
    is_wager: {type: String},
    platform: {type: String},
    create_time: {type: String},
    is_parent_zero_stake: {type: String},
    jackpot_rtp_contribution_amount: {type: Number},
    jackpot_win_amount: {type: Number},
    jackpot_pool_id: {type: Number},
    jackpot_type: {type: Number},
    createdDate: { type: Date, expires: ((60 * 60) * 24) * 15, default: Date.now }
});

schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.createdDate) {
        this.createdDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);

module.exports = mongoose.model('PgSlotHistory', schema);