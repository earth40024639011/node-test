const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    matchId: {type: String},
    homeScoreHT: {type: Number},
    awayScoreHT: {type: Number},
    homeScoreFT: {type: Number},
    awayScoreFT: {type: Number},
    type: {type: String},
    createdDate: { type: Date, expires: ((60 * 60) * 24) * 15, default: Date.now },
    actionBy : {type: String, ref: 'User'},
    round: {type: Number},
    result: {type: String},
    remark: {type: String},
    username:{type:String},
    md5: {type: String, default: ''}
});

schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);

module.exports = mongoose.model('EndScoreHistory', schema);