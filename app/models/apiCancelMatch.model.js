const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    matchId: {type: String, required: true},
    txns: [{
        username: {type: String, required: false},
        betId: {type: String, required: false},
        betAmount: {type: String, required: false},
        betTime: {type: String, required: false},
    }],
    createdDate: {type: Date, default: Date.now},
    updatedDate: {type: Date}
});


schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);
schema.index({_id: 1, status: 1}, {unique: true});

module.exports = mongoose.model('ApiCancelMatch', schema);