const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const schema = new mongoose.Schema({
    country: {type: Schema.Types.ObjectId, ref: 'Country'},
    countryCode: {type:String,require:true},
    subCompanySuffixChar : {type:String,require:true},
    modifyDate: {type: Date, default: Date.now},
    createdDate:{type: Date, default: Date.now}
});

schema.pre('save', function (next) {
    let now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.createdDate) {
        this.createdDate = now;
    }
    next();
});




module.exports = mongoose.model('Sequence', schema);