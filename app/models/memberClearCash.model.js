const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    username:{ type: String },
    beforeCredit:{ type: Number },
    afterCredit:{ type: Number },
    beforeBalance:{ type: Number },
    afterBalance:{ type: Number },
    totalBeforeCredit:{ type: Number },
    totalAfterCredit:{ type: Number },
    actionBy : {type: String, ref: 'User', require: true},
    createdDate: { type: Date, expires: ((60 * 60) * 24) * 15, default: Date.now }
});

schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);
schema.index({ username: 1,createdDate:1});

module.exports = mongoose.model('MemberClearCash', schema);