const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    betId: {type: String},
    message:{type:String},
    createdDate: {type: Date, default: Date.now},
    updatedDate: {type: Date}
});


schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    next();
});


module.exports = mongoose.model('LogLive22', schema);