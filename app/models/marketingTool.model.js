const mongoose = require('mongoose');



const schema = new mongoose.Schema({

    newBanner: [{
        img: { type: String,required: true },
        url: { type: String }
    }],
    promotionBanner: [{
        img: { type: String,required: true},
        url: { type: String }
    }],
    gameImage: [{
        img: { type: String ,required: true},
        url: { type: String }
    }],
    videoUrl: [{
        url: { type: String ,required: true},
    }],
    isActive: { type: Boolean, default: true },
    createdDate: {type: Date, default: Date.now},
    updatedDate: {type: Date},

});


schema.pre('save', function (next) {
    var now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.createdDate) {
        this.createdDate = now;
    }
    this.updatedDate = now;
    next();
});


module.exports = mongoose.model('Marketingtool', schema);