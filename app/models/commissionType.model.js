const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    type: {type: String, unique: true, required: true},
    torValue: {type: Number, required: true},
    longValue: {type: Number, required: true},
    updatedDate: {type: Date},
    createdDate: {type: Date, default: Date.now},

});

schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);

module.exports = mongoose.model('CommissionType', schema);