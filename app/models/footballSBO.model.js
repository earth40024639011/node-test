const mongoose = require('mongoose');
const Schema   = mongoose.Schema;

const themeSchema = new Schema({
    k : { type: Number, index: { unique: true }},
    n : {
        en : { type: String },
        th : { type: String },
        cn : { type: String },
        tw : { type: String }
    },
    sk : { type: Number, default : 30},
    r : {
        ah : {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        ou : {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        x12 : {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        oe : {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        ah1st : {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        ou1st : {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        x121st : {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        oe1st : {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        tg: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        dc: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        cs: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        fhcs: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        ftht: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        fglg: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        tc: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        fc: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        lc: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        httg: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        ttko: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        ffk: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        lfk: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        fgk: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        lgk: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        fos: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        los: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        ftn: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        ltn: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        fsub: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        lsub: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        htcs: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        atcs: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        arc: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        apt: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        aog: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        et: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        btts: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        min15g0015: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        min15g1530: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        min15g4560: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        min15g6075: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        min15c0015: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        min15c1530: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        min15c4560: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        min15c6075: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        pm : { type: Number, default: 999999 }
    },
    rem : {
        ah : {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 0 }
        },
        ou : {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 0 }
        },
        x12 : {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 0 }
        },
        oe : {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 0 }
        },
        ah1st : {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 0 }
        },
        ou1st : {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 0 }
        },
        x121st : {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 0 }
        },
        oe1st : {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 0 }
        },
        tg: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        dc: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        cs: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        fhcs: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        ftht: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        fglg: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        tc: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        fc: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        lc: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        httg: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        ttko: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        ffk: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        lfk: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        fgk: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        lgk: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        fos: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        los: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        ftn: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        ltn: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        fsub: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        lsub: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        htcs: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        atcs: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        arc: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        apt: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        aog: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        et: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        btts: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        min15g0015: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        min15g1530: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        min15g4560: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        min15g6075: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        min15c0015: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        min15c1530: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        min15c4560: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        min15c6075: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        pm : { type: Number, default: 500000 }
    },
    rl : {
        ah : {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        ou : {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        x12 : {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        oe : {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        ah1st : {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        ou1st : {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        x121st : {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        oe1st : {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        tg: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        dc: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        cs: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        fhcs: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        ftht: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        fglg: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        tc: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        fc: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        lc: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        httg: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        ttko: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        ffk: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        lfk: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        fgk: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        lgk: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        fos: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        los: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        ftn: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        ltn: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        fsub: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        lsub: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        htcs: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        atcs: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        arc: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        apt: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        aog: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        et: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        btts: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        min15g0015: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        min15g1530: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        min15g4560: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        min15g6075: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        min15c0015: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        min15c1530: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        min15c4560: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        min15c6075: {
            ma : { type: Number, default: 10000 },
            mi : { type: Number, default: 20 }
        },
        ttc : {
            ma : { type: Number, default: 5000 },
            mi : { type: Number, default: 10 }
        },
        ttc1st : {
            ma : { type: Number, default: 5000 },
            mi : { type: Number, default: 10 }
        },
        ng : {
            ma : { type: Number, default: 5000 },
            mi : { type: Number, default: 10 }
        },
        pm : { type: Number, default: 100000 }
    },
    rp : {
        hdp : {
            ah : {
                f : { type: String, default : "0" },
                u : { type: String, default : "0"  }
            },
            ou : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            },
            x12 : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            },
            oe : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            },
            ah1st : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            },
            ou1st : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            },
            x121st : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            },
            oe1st : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            }
        },
        live : {
            ah : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            },
            ou : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            },
            x12 : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            },
            oe : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            },
            ah1st : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            },
            ou1st : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            },
            x121st : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            },
            oe1st : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            }
        }
    },
    m : [{
        id: { type: String },
        k188: { type: String },
        liveTv: { type: Object},
        k : { type: Number },
        d : { type: Date },
        hasParlay : { type: Boolean },
        i : {
            lt : { type: String },
            mt : { type: String },
            h : { type: String },
            a : { type: String },
            s : { type: String },
            hrc : { type: String },
            arc : { type: String },
        },
        s : {
            isH : { type : Boolean, default : true},
            isL : { type : Boolean, default : false},
            isE : { type : Boolean, default : false},
            isCr : { type : Boolean, default : false},
            cr : {
                ft : {
                    h : { type: Number },
                    a : { type: Number },
                },
                ht : {
                    h : { type: Number },
                    a : { type: Number },
                }
            },
            isC : {//Cancel
                ft : { type: Boolean, default: false },
                ht : { type: Boolean, default: false },
                ft_remark : { type: String, default : ''  },
                ht_remark : { type: String, default : ''  }
            },
            isAl : { type : Boolean, default : true},//Allow to Live
            isD : { type : Boolean, default : false}//DONE
        },
        n : {
            en : {
                h : { type: String },
                a : { type: String }
            },
            th : {
                h : { type: String },
                a : { type: String }
            },
            cn : {
                h : { type: String },
                a : { type: String }
            },
            tw : {
                h : { type: String },
                a : { type: String }
            }
        },
        isToday : { type: Boolean, default : false},
        isPopular : { type: Boolean, default : false },

        bp : [{
            allKey: [{ type: String }],
            ah : {
                h : {type: String},
                hk : {type: String},
                hp: {type: String},
                hpk : {type: String},
                a : {type: String},
                ak : {type: String},
                ap: {type: String},
                apk : {type: String}
            },
            ou : {
                o : {type: String},
                ok : {type: String},
                op: {type: String},
                opk : {type: String},
                u : {type: String},
                uk : {type: String},
                up: {type: String},
                upk : {type: String}
            },
            x12 : {
                h : {type: String},
                hk : {type: String},
                a : {type: String},
                ak : {type: String},
                d : {type: String},
                dk : {type: String}
            },
            oe : {
                o : {type: String},
                ok : {type: String},
                e : {type: String},
                ek : {type: String}
            },
            ah1st : {
                h : {type: String},
                hk : {type: String},
                hp: {type: String},
                hpk : {type: String},
                a : {type: String},
                ak : {type: String},
                ap: {type: String},
                apk : {type: String}
            },
            ou1st : {
                o : {type: String},
                ok : {type: String},
                op: {type: String},
                opk : {type: String},
                u : {type: String},
                uk : {type: String},
                up: {type: String},
                upk : {type: String}
            },
            x121st : {
                h : {type: String},
                hk : {type: String},
                a : {type: String},
                ak : {type: String},
                d : {type: String},
                dk : {type: String}
            },
            oe1st : {
                o : {type: String},
                ok : {type: String},
                e : {type: String},
                ek : {type: String}
            }
        }],
        bpl : [{
            allKey: [{ type: String }],
            ah : {
                h : {type: String},
                hk : {type: String},
                hp: {type: String},
                hpk : {type: String},
                a : {type: String},
                ak : {type: String},
                ap: {type: String},
                apk : {type: String}
            },
            ou : {
                o : {type: String},
                ok : {type: String},
                op: {type: String},
                opk : {type: String},
                u : {type: String},
                uk : {type: String},
                up: {type: String},
                upk : {type: String}
            },
            x12 : {
                h : {type: String},
                hk : {type: String},
                a : {type: String},
                ak : {type: String},
                d : {type: String},
                dk : {type: String}
            },
            oe : {
                o : {type: String},
                ok : {type: String},
                e : {type: String},
                ek : {type: String}
            },
            ah1st : {
                h : {type: String},
                hk : {type: String},
                hp: {type: String},
                hpk : {type: String},
                a : {type: String},
                ak : {type: String},
                ap: {type: String},
                apk : {type: String}
            },
            ou1st : {
                o : {type: String},
                ok : {type: String},
                op: {type: String},
                opk : {type: String},
                u : {type: String},
                uk : {type: String},
                up: {type: String},
                upk : {type: String}
            },
            x121st : {
                h : {type: String},
                hk : {type: String},
                a : {type: String},
                ak : {type: String},
                d : {type: String},
                dk : {type: String}
            },
            oe1st : {
                o : {type: String},
                ok : {type: String},
                e : {type: String},
                ek : {type: String}
            }
        }]
    }]
});

themeSchema.pre('save', (next) => {
    if (!this.created) {
        this.created = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    }
    next();
});

module.exports = mongoose.model('FootballSBO', themeSchema);