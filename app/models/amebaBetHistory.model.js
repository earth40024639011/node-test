const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({

    action: { type: String },
    siteId: { type: Number },
    accountName: { type: String },
    betAmt: { type: String },
    payoutAmt: { type: String },
    gameId: { type: Number },
    roundId: { type: String },
    txId: { type: String },
    sessionId: { type: String },
    free:{ type: Boolean },
    rebateAmt: { type: String },
    prizeType: { type: String },
    prizeAmt: { type: String },
    sumPayoutAmt: {type:String},
    time :{type:String},
    jp :{
        pcConAmt: { type: String },
        jcConAmt: { type: String },
        winId: { type: String },
        pcWinAmt: { type: String },
        jcWinAmt: { type: String },
        winLv: { type: Number },
        directPay: { type: Boolean },
    },
    createdDate: {type: Date, default: Date.now}
});

schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);

module.exports = mongoose.model('AmebaBetHistory', schema);