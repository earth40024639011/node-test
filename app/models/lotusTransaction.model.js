const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({

    gameId:{ type: String },
    userId:{ type: String },
    userCash:{ type: String },
    action:{ type: String },
    transactionId:{ type: String },
    roundId:{ type: String },
    gameName:{ type: String },
    referenceId:{ type: String },
    createdDate: {type: Date},

});

schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);

module.exports = mongoose.model('LotusTransaction', schema);