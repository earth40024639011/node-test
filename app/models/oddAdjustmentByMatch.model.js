const mongoose = require('mongoose');
const Schema   = mongoose.Schema;

const themeSchema = new Schema({
    id : { type: String, index: { unique: true }},
    ah : {
        h : { type: Number, default : 0 },
        a : { type: Number, default : 0  }
    },
    ou : {
        o : { type: Number, default : 0  },
        u : { type: Number, default : 0  }
    },
    ah1st : {
        h : { type: Number, default : 0  },
        a : { type: Number, default : 0  }
    },
    ou1st : {
        o : { type: Number, default : 0  },
        u : { type: Number, default : 0  }
    },
    created : { type: Date, expires: 60000, default: Date.now }
});

themeSchema.pre('save', (next) => {
    if (!this.created) {
        this.created = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    }
    next();
});

module.exports = mongoose.model('OddAdjustmentByMatch', themeSchema);