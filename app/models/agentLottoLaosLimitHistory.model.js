const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
// const uniqueArrayPlugin = require('mongoose-unique-array');

const schema = new mongoose.Schema({
    agent:{type: Schema.Types.ObjectId, ref: 'AgentGroup'},
    updateBy:{type: Schema.Types.ObjectId, ref: 'User'},
    createdDate : {type:Date},
    name: {type: String},
    hour: {type: Number},
    min: {type: Number},
    isEnableHotNumber:{type:Boolean},
    po : {
        L_4TOP: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number},
            }]
        },
        L_4TOD: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        L_3TOP: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        L_3TOD: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        L_2FB: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        T_4TOP: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        T_4TOD: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        T_3TOP: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        T_3TOD: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        T_2TOP: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        T_2TOD: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        T_2BOT: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        T_1TOP: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        T_1BOT: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        }
    }
});


schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);
// schema.plugin(uniqueArrayPlugin);
schema.index({ _id: 1, status: 1 },{unique: true});

module.exports = mongoose.model('AgentLottoLaosLimitHistory', schema);