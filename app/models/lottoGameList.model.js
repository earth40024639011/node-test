const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    day: {type: String, required: true},
    month: {type: String, required: true},
    year: {type: String, required: true},
    id: {type: String, required: true},
    createdDate: {type: Date, default: Date.now},
});

schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);
schema.index({date: 1, month: 1,year:1,id:1}, {unique: true});

module.exports = mongoose.model('LottoGameList', schema);