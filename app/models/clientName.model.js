const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const themeSchema = new mongoose.Schema({
    clientName : { type: String }
});

themeSchema.pre('save', (next) => {
    if (!this.created) {
        this.created = new Date();
    }
    next();
});

module.exports = mongoose.model('ClientName', themeSchema);