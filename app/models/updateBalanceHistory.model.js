const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    memberId: {type: Schema.Types.ObjectId, ref: 'Member', default: null},
    amount: {type: Number, required: true},
    betId: {type: String, required: false},
    description: {type: String, required: false},
    beforeCredit: {type: Number},
    ref1: {type: String, required: false},
    createdDate: {type: Date, default: Date.now},
    status : {type: String, enum: ['SUCCESS','FAIL'],required: true}
});


schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);
schema.index({ _id: 1, status: 1 },{unique: true});

module.exports = mongoose.model('UpdateBalanceHistory', schema);