const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const Schema = mongoose.Schema;

autoIncrement.initialize(mongoose.connection);


const schema = new mongoose.Schema({
    username: {type: String},
    transactionId: {type: Number,unique : true, default:1},
    createdDate:{type: Date, default: Date.now}
});

schema.plugin(autoIncrement.plugin, {
    model: 'SeqBettingTransaction',
    field: 'transactionId',
    startAt: 10000000,
    incrementBy: 1
});

schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.createdDate) {
        this.createdDate = now;
    }
    next();
});

module.exports = mongoose.model('SeqBettingTransaction', schema);