const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
// const uniqueArrayPlugin = require('mongoose-unique-array');

const schema = new mongoose.Schema({
    agent:{type: Schema.Types.ObjectId, ref: 'AgentGroup',unique:true},
    name: {type: String},
    hour: {type: Number},
    min: {type: Number},
    isEnableHotNumber:{type:Boolean},
    po : {
        _1TOP: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number},
            }]
        },
        _1BOT: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        _2TOP: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        _2BOT: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        _2TOD: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        _2TOP_OE: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        _2TOP_OU: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        _2BOT_OE: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        _2BOT_OU: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        _3TOP: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        _3BOT: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        _3TOD: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        _3TOP_OE: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        _3TOP_OU: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        _4TOP: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        _4TOD: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        _5TOP: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        _6TOP: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        }
    },
    hn_po : {
        _1TOP: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number},
            }]
        },
        _1BOT: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        _2TOP: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        _2BOT: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        _2TOD: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        _2TOP_OE: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        _2TOP_OU: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        _2BOT_OE: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        _2BOT_OU: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        _3TOP: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        _3BOT: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        _3TOD: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        _3TOP_OE: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        _3TOP_OU: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        _4TOP: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        _4TOD: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        _5TOP: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        },
        _6TOP: {
            limit: {type: Number,default:0},
            bets: [{
                betNumber: {type: String},
                limit: {type: Number},
                betAmount: {type: Number}
            }]
        }
    }
});


schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);
// schema.plugin(uniqueArrayPlugin);
schema.index({ _id: 1, status: 1 },{unique: true});

module.exports = mongoose.model('AgentLottoLimit', schema);