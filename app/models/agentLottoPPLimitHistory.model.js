const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
// const uniqueArrayPlugin = require('mongoose-unique-array');

const schema = new mongoose.Schema({
    agent:{type: Schema.Types.ObjectId, ref: 'AgentGroup'},
    updateBy:{type: Schema.Types.ObjectId, ref: 'User'},
    name: {type: String},
    hour: {type: Number},
    min: {type: Number},
    isEnableHotNumber:{type:Boolean},
    po : {
        _1TOP: {
            limit: {type: Number,default:0}
        },
        _1BOT: {
            limit: {type: Number,default:0}
        },
        _2TOP: {
            limit: {type: Number,default:0}
        },
        _2BOT: {
            limit: {type: Number,default:0}
        },
        _2TOD: {
            limit: {type: Number,default:0}
        },
        _2TOP_OE: {
            limit: {type: Number,default:0}
        },
        _2TOP_OU: {
            limit: {type: Number,default:0}
        },
        _2BOT_OE: {
            limit: {type: Number,default:0}
        },
        _2BOT_OU: {
            limit: {type: Number,default:0}
        },
        _3TOP: {
            limit: {type: Number,default:0}
        },
        _3BOT: {
            limit: {type: Number,default:0}
        },
        _3TOD: {
            limit: {type: Number,default:0}
        },
        _3TOP_OE: {
            limit: {type: Number,default:0}
        },
        _3TOP_OU: {
            limit: {type: Number,default:0}
        },
        _4TOP: {
            limit: {type: Number,default:0}
        },
        _4TOD: {
            limit: {type: Number,default:0}
        },
        _5TOP: {
            limit: {type: Number,default:0}
        },
        _6TOP: {
            limit: {type: Number,default:0}
        }
    },
    hn_po : {
        _1TOP: {
            limit: {type: Number,default:0}
        },
        _1BOT: {
            limit: {type: Number,default:0}
        },
        _2TOP: {
            limit: {type: Number,default:0}
        },
        _2BOT: {
            limit: {type: Number,default:0}
        },
        _2TOD: {
            limit: {type: Number,default:0}
        },
        _2TOP_OE: {
            limit: {type: Number,default:0}
        },
        _2TOP_OU: {
            limit: {type: Number,default:0}
        },
        _2BOT_OE: {
            limit: {type: Number,default:0}
        },
        _2BOT_OU: {
            limit: {type: Number,default:0}
        },
        _3TOP: {
            limit: {type: Number,default:0}
        },
        _3BOT: {
            limit: {type: Number,default:0}
        },
        _3TOD: {
            limit: {type: Number,default:0}
        },
        _3TOP_OE: {
            limit: {type: Number,default:0}
        },
        _3TOP_OU: {
            limit: {type: Number,default:0}
        },
        _4TOP: {
            limit: {type: Number,default:0}
        },
        _4TOD: {
            limit: {type: Number,default:0}
        },
        _5TOP: {
            limit: {type: Number,default:0}
        },
        _6TOP: {
            limit: {type: Number,default:0}
        }
    },
    createdDate : {type:Date}
});


schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);
// schema.plugin(uniqueArrayPlugin);

module.exports = mongoose.model('AgentLottoPPLimitHistory', schema);