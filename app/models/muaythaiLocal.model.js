const mongoose      = require('mongoose');
const Schema        = mongoose.Schema;
const autoIncrement = require('mongoose-auto-increment');
autoIncrement.initialize(mongoose.connection);

const themeSchema = new Schema({
    k : { type: Number, index: { unique: true }, required: true},
    n : {
        en : { type: String },
        th : { type: String },
        cn : { type: String },
        tw : { type: String }
    },
    sk : { type: Number, default : 30},
    m : [{
        id: { type: String },
        k : { type: Number },
        d : { type: Date },
        i : {
            lt : { type: String },
            mt : { type: String },
            h : { type: String },
            a : { type: String }
        },
        s : {
            isH : { type : Boolean, default : true},//HDP
            isL : { type : Boolean, default : false},//LIVE
            isE : { type : Boolean, default : false},//END
            isC : { type : Boolean, default : false},//Cancel
            isCr : { type : Boolean, default : false},//END
            cr : {
                winner: {
                    type: { type: String }
                },
                round: {
                    ou: { type: Number }
                },
                isKO : {
                    type : Boolean
                }
            }
        },
        n : {
            en : {
                h : { type: String },
                a : { type: String }
            },
            th : {
                h : { type: String },
                a : { type: String }
            },
            cn : {
                h : { type: String },
                a : { type: String }
            },
            tw : {
                h : { type: String },
                a : { type: String }
            }
        },
        bp : [{
            ah : {
                h : {type: String},
                hk : {type: String},
                hp: {type: String},
                hpk : {type: String},
                a : {type: String},
                ak : {type: String},
                ap: {type: String},
                apk : {type: String}
            },
            ou : {
                o : {type: String},
                ok : {type: String},
                op: {type: String},
                opk : {type: String},
                u : {type: String},
                uk : {type: String},
                up: {type: String},
                upk : {type: String}
            }
        }],
        r : {
            ah : {
                ma : { type: Number, default: 5000 },
                mi : { type: Number, default: 10 }
            },
            ou : {
                ma : { type: Number, default: 5000 },
                mi : { type: Number, default: 10 }
            }
        },
        rl : {
            ah : {
                ma : { type: Number, default: 5000 },
                mi : { type: Number, default: 10 }
            },
            ou : {
                ma : { type: Number, default: 5000 },
                mi : { type: Number, default: 10 }
            }
        },
        pm : { type: Number, default: 999999 }
    }]
});

themeSchema.pre('save', (next) => {
    if (!this.created) {
        this.created = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    }
    next();
});

themeSchema.plugin(autoIncrement.plugin, {
    model: 'MuaythaiLocal',
    field: 'k',
    startAt: 1,
    incrementBy: 1
});

module.exports = mongoose.model('MuaythaiLocal', themeSchema);