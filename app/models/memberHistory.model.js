const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    id:{type: String},
    username: {type: String, required: false},
    username_lower: {type: String, required: false},
    loginName: {type: String, required: false},
    loginName_lower: {type: String, required: false},
    password: {type: String, required: false},
    contact: {type: String, required: false},
    phoneNo: {type: String, required: false},
    transactionCode: {type: String, required: false},
    group: {type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
    currency: {type: String, enum: ['THB','MYR','HDK','CNY','IDR','USD']},
    creditLimit: {type: Number, required: false},
    balance: {type: Number, required: false, default: 0},
    shareSetting: {
        sportsBook: {
            today: {
                parent: {type: Number}
            },
            live: {
                parent: {type: Number}
            }
        },
        step: {
            parlay: {
                parent: {type: Number}
            },
            step: {
                parent: {type: Number}
            },
        },
        casino: {
            sexy: {
                parent: {type: Number}
            },
            ag: {
                parent: {type: Number}
            },
            sa: {
                parent: {type: Number}
            },
            dg: {
                parent: {type: Number}
            },
            pt: {
                parent: {type: Number}
            }
        },
        multi: {
            amb: {
                parent: {type: Number}
            }
        },
        game: {
            slotXO: {
                parent: {type: Number}
            }
        },
        lotto: {
            amb: {
                parent: {type: Number}
            },
            pp: {
                parent: {type: Number}
            },
            laos: {
                parent: {type: Number}
            }
        }
    },
    limitSetting: {
        sportsBook: {
            hdpOuOe: {
                maxPerBet: {type: Number},
                maxPerMatch: {type: Number},
            },
            oneTwoDoubleChance: {
                maxPerBet: {type: Number},
                maxPerMatch: {type: Number},
            },
            others: {
                maxPerBet: {type: Number},
                maxPerMatch: {type: Number},
            },
            outright: {
                maxPerBet: {type: Number},
                maxPerMatch: {type: Number},
            },
            isEnable: {type: Boolean, default: true},
        },
        step: {
            parlay: {
                maxPerBet: {type: Number},
                minPerBet: {type: Number},
                maxBetPerDay: {type: Number},
                maxPayPerBill: {type: Number},
                maxMatchPerBet: {type: Number},
                minMatchPerBet: {type: Number},
                isEnable: {type: Boolean, default: true},
            },
            step: {
                maxPerBet: {type: Number},
                minPerBet: {type: Number},
                maxBetPerDay: {type: Number},
                maxPayPerBill: {type: Number},
                maxMatchPerBet: {type: Number},
                minMatchPerBet: {type: Number},
                isEnable: {type: Boolean, default: true},
            }
        },
        casino : {
            isWlEnable:{type:Boolean},
            winPerDay:{type:Number},
            lossPerDay:{type:Number},
            sexy : {
                isEnable: {type: Boolean, default: true},
                limit: {type: Number, enum: [1, 2, 3, 4, 5]}
            },
            ag : {
                isEnable: {type: Boolean, default: true},
                limit: {type: Number, enum: [1, 2, 3, 4, 5]}
            },
            sa : {
                isEnable: {type: Boolean, default: true},
                limit: {type: Number, enum: [1, 2, 3, 4, 5]},
            },
            dg : {
                isEnable: {type: Boolean, default: true},
                limit: {type: Number, enum: [1, 2, 3, 4, 5]},
            },
            pt : {
                isEnable: {type: Boolean, default: true},
                limit: {type: Number, enum: [1, 2, 3, 4, 5]},
            },
        },
        multi : {
            amb: {
                isEnable: {type: Boolean, default: true}
            }
        },
        game : {
            slotXO: {
                isEnable: {type: Boolean, default: true}
            }
        },
        lotto: {
            amb: {
                isEnable: {type: Boolean, default: true},
                hour: {type: Number},
                minute: {type: Number},
                _6TOP : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _5TOP : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _4TOP : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _4TOD : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _3TOP : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _3TOD : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _3BOT : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _3TOP_OE : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _3TOP_OU : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _2TOP : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _2TOD : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _2BOT : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _2TOP_OE : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _2TOP_OU : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _2BOT_OE : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _2BOT_OU : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _1TOP : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _1BOT : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                }
            },
            pp: {
                isEnable: {type: Boolean, default: true},
                hour: {type: Number},
                minute: {type: Number},
                _6TOP : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _5TOP : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _4TOP : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _4TOD : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _3TOP : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _3TOD : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _3BOT : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _3TOP_OE : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _3TOP_OU : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _2TOP : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _2TOD : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _2BOT : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _2TOP_OE : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _2TOP_OU : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _2BOT_OE : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _2BOT_OU : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _1TOP : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                _1BOT : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                }
            },
            laos: {
                isEnable: {type: Boolean, default: true},
                hour: {type: Number},
                minute: {type: Number},
                L_4TOP : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                L_4TOD : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                L_3TOP : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                L_3TOD : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                L_2FB : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                T_4TOP : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                T_4TOD : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                T_3TOP : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                T_3TOD : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                T_2TOP : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                T_2TOD : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                T_2BOT : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                T_1TOP : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                },
                T_1BOT : {
                    payout : {type: Number},
                    discount : {type: Number},
                    max: {type: Number},
                }
            }
        }
    },
    commissionSetting: {
        sportsBook: {
            typeHdpOuOe: {
                type: String,
                enum: ['A', 'B', 'C', 'D', 'E', 'F'],
                required: false
            },
            hdpOuOe: {type: Number},
            oneTwoDoubleChance: {type: Number},
            others: {type: Number},
        },
        parlay:{
            com : {type: Number},
        },
        step:{
            com2: {type: Number},
            com34: {type: Number},
            com56: {type: Number},
            com78: {type: Number},
            com910: {type: Number},
            com1112: {type: Number},

            com3: {type: Number},
            com4: {type: Number},
            com5: {type: Number},
            com6: {type: Number},
            com7: {type: Number},
            com8: {type: Number},
            com9: {type: Number},
            com10: {type: Number},
            com11: {type: Number},
            com12: {type: Number},
        },
        casino: {
            sexy: {type: Number},
            ag: {type: Number},
            sa: {type: Number},
            dg: {type: Number},
            pt: {type: Number}
        },
        multi: {
            amb: {type: Number}
        },
        game: {
            slotXO: {type: Number}
        },
        lotto: {
            amb: {type: Number}
        }
    },
    configuration: {
        sportsBook: {
            locale: {type: String,enum: ['th-TH','id-ID','en-US','zh-CN','zh-TW','vi-V'],required: false},
            odd: {type: String,enum: ['HK','MY','ID','EU'],required: false},
            views: {type: String,enum: ['SINGLE', 'DOUBLE'],required: false},
            acceptAnyOdd: {type: Boolean, default: false},
            defaultPrice: {
                check: {type: Boolean, default: false},
                price: {type: Number}
            },
            last: {type: Boolean, default: false},
        }
    },
    passwordLock: {type: Boolean, default: false},
    suspend: {type: Boolean, default: false},
    lock: {type: Boolean, default: false},
    active: {type: Boolean, default: true},
    ipAddress: {type: String},
    lastLogin: {type: Date},
    forceChangePassword: {type: Boolean, default: true},
    forceChangePasswordDate: {type: Date},
    termAndCondition: {type: Boolean, default: false},
    isAutoChangeOdd : {type: Boolean, default: false},
    remark: {type: String},
    createdDate: {type: Date, default: Date.now},
    createdBy: {type: Schema.Types.ObjectId, ref: 'User'}
});


schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);

module.exports = mongoose.model('MemberHistory', schema);