const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    betId: {type: String, required: true,unique:true},
    playerId: {type: Schema.Types.ObjectId, ref: 'Member', default: null},
    memberParentGroup:{type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
    parlay:{
        matches: [{
            leagueId: {type: String, required: false},
            matchId: {type: String, required: false},
            leagueName: {type: String, required: false},
            leagueNameN : {
                en : { type: String, required: false },
                th : { type: String, required: false },
                cn : { type: String, required: false },
                tw : { type: String, required: false }
            },
            matchName: {
                en : {
                    h : { type: String },
                    a : { type: String }
                },
                th : {
                    h : { type: String },
                    a : { type: String }
                },
                cn : {
                    h : { type: String },
                    a : { type: String }
                },
                tw : {
                    h : { type: String },
                    a : { type: String }
                }
            },
            matchDate:{type:Date,required:false},
            odd: {type: Number, required: false},
            oddKey: {type: String, required: false},
            oddType: {type: String,enum: ['AH','OU','OE','X12','AH1ST','OU1ST','OE1ST','X121ST'],required: false},
            handicap: {type: String, required: false},
            bet: {type: String, enum: ['HOME','AWAY','UNDER','OVER','DRAW','ODD','EVEN'],required: false},
            score: {type: String, required: false},
            halfScore: {type: String, required: false},
            fullScore: {type: String, required: false},
            matchType: {type: String, enum: ['NONE_LIVE','LIVE'],required: false},
            betResult:{type: String,enum: ['WIN','HALF_WIN','LOSE','HALF_LOSE','DRAW'],required: false},
            isCancel: {type: Boolean, default: false},
        }],
        odds : {type: Number, required: false}
    },
    hdp:{
        leagueId: {type: String, required: false},
        matchId: {type: String, required: false},
        leagueName: {type: String, required: false},
        leagueNameN : {
            en : { type: String, required: false },
            th : { type: String, required: false },
            cn : { type: String, required: false },
            tw : { type: String, required: false }
        },
        matchName: {
            en : {
                h : { type: String },
                a : { type: String }
            },
            th : {
                h : { type: String },
                a : { type: String }
            },
            cn : {
                h : { type: String },
                a : { type: String }
            },
            tw : {
                h : { type: String },
                a : { type: String }
            }
        },
        matchDate:{type:Date,required:false},
        odd: {type: Number, required: false},
        oddKey: {type: String, required: false},
        oddType: {type: String,enum: ['AH','OU','OE','X12','AH1ST','OU1ST','OE1ST','X121ST','ML','T1OU','T2OU'],required: false},
        handicap: {type: String, required: false},
        bet: {type: String, enum: ['HOME','AWAY','UNDER','OVER','DRAW','ODD','EVEN'],required: false},
        score: {type: String, required: false},
        halfScore: {type: String, required: false},
        fullScore: {type: String, required: false},
        matchType: {type: String, enum: ['NONE_LIVE','LIVE'],required: false},
    },
    amount: {type: Number, required: true},
    playerBalance: {type: Number, required: true},
    gameType : {type: String,enum: ['TODAY','PARLAY'],required: true},
    priceType: {type: String,enum: ['HK','MY','ID','EU','TH'],required: true},
    currency: {type: String, enum: ['THB','MYR','HDK','CNY','IDR','USD']},
    game:{type: String,enum: ['FOOTBALL','CASINO','GAME'],required: true},
    source:{type: String,enum: ['FOOTBALL','SEXY_BACCARAT','SLOT_XO','BASKETBALL'],required: false},
    status:{type: String,enum: ['DONE','RUNNING','WAITING', 'REJECTED','CANCELLED'],required: true},
    remark: { type: String },
    gameDate: {type: Date,required: true},
    createdDate: {type: Date, default: Date.now},
    updatedDate: {type: Date},
    ipAddress:{type:String}
});

schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);

module.exports = mongoose.model('ApiBetHistory', schema);