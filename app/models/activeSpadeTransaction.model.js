const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    betId: {type: String, required: true,unique:true},
    memberId: {type: Schema.Types.ObjectId, ref: 'Member', default: null},
    memberParentGroup:{type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
    memberUsername : {type:String},
    slot : {
        transferId: {type:String},
        acctId: {type:String},
        currency: {type:String},
        amount: {type: Number},
        type: {type: Number},
        channel: {type:String},
        gameCode:{type:String},
        gameName:{type:String},
        ticketId: {type:String},
        referenceId: {type:String},
        specialGame: {
            type:{type:String},
            count:{type: Number},
            sequence:{type: Number},
        },
        refTicketIds: [{type:String}]
    },
    amount: {type: Number, required: true},
    memberCredit: {type: Number, required: true},
    validAmount: {type: Number, required: false},
    commission : {
        superAdmin:{
            parentGroup:{type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
            group:{type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
            parent: {type: Number},
            own: {type: Number},
            remaining: {type: Number},
            min: {type: Number},
            shareReceive: {type: Number},
            commission:{type: Number, required: false},
            amount:{type: Number, required: false},
            payout:{type: Number, required: false},
            winLose: {type: Number, required: false},
            winLoseCom: {type: Number, required: false},
            totalWinLoseCom: {type: Number, required: false},
        },
        company:{
            parentGroup:{type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
            group:{type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
            parent: {type: Number},
            own: {type: Number},
            remaining: {type: Number},
            min: {type: Number},
            shareReceive: {type: Number},
            commission:{type: Number, required: false},
            amount:{type: Number, required: false},
            payout:{type: Number, required: false},
            winLose: {type: Number, required: false},
            winLoseCom: {type: Number, required: false},
            totalWinLoseCom: {type: Number, required: false},
        },
        shareHolder:{
            parentGroup:{type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
            group:{type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
            parent: {type: Number},
            own: {type: Number},
            remaining: {type: Number},
            min: {type: Number},
            shareReceive: {type: Number},
            commission:{type: Number, required: false},
            amount:{type: Number, required: false},
            payout:{type: Number, required: false},
            winLose: {type: Number, required: false},
            winLoseCom: {type: Number, required: false},
            totalWinLoseCom: {type: Number, required: false},
        },
        api:{
            parentGroup:{type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
            group:{type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
            parent: {type: Number},
            own: {type: Number},
            remaining: {type: Number},
            min: {type: Number},
            shareReceive: {type: Number},
            commission:{type: Number, required: false},
            amount:{type: Number, required: false},
            payout:{type: Number, required: false},
            winLose: {type: Number, required: false},
            winLoseCom: {type: Number, required: false},
            totalWinLoseCom: {type: Number, required: false},
        },
        senior:{
            parentGroup:{type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
            group:{type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
            parent: {type: Number},
            own: {type: Number},
            remaining: {type: Number},
            min: {type: Number},
            shareReceive: {type: Number},
            commission:{type: Number, required: false},
            amount:{type: Number, required: false},
            payout:{type: Number, required: false},
            winLose: {type: Number, required: false},
            winLoseCom: {type: Number, required: false},
            totalWinLoseCom: {type: Number, required: false},
        },
        masterAgent:{
            parentGroup:{type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
            group:{type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
            parent: {type: Number},
            own: {type: Number},
            remaining: {type: Number},
            min: {type: Number},
            shareReceive: {type: Number},
            commission:{type: Number, required: false},
            amount:{type: Number, required: false},
            payout:{type: Number, required: false},
            winLose: {type: Number, required: false},
            winLoseCom: {type: Number, required: false},
            totalWinLoseCom: {type: Number, required: false},
        },
        agent:{
            parentGroup:{type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
            group:{type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
            parent: {type: Number},
            own: {type: Number},
            remaining: {type: Number},
            min: {type: Number},
            shareReceive: {type: Number},
            commission:{type: Number, required: false},
            amount:{type: Number, required: false},
            payout:{type: Number, required: false},
            winLose: {type: Number, required: false},
            winLoseCom: {type: Number, required: false},
            totalWinLoseCom: {type: Number, required: false},
        },
        member:{
            parent: {type: Number},
            commission:{type: Number, required: false},
            amount:{type: Number, required: false},
            payout:{type: Number, required: false},
            winLose: {type: Number, required: false},
            winLoseCom: {type: Number, required: false},
            totalWinLoseCom: {type: Number, required: false},
        }
    },
    status:{type: String,enum: ['DONE','RUNNING','WAITING', 'REJECTED','CANCELLED'],required: true},
    active: {type: Boolean, default: true},
    gameDate: {type: Date,required: true},
    createdDate: {type: Date, default: Date.now},
    updatedDate: {type: Date}
});


schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);
schema.index({ memberUsername: 1, 'slot.transferId': 1 ,'slot.txId':1});

module.exports = mongoose.model('ActiveSpadeTransaction', schema);