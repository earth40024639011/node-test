const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
const autoIncrement = require('mongoose-auto-increment');
autoIncrement.initialize(mongoose.connection);


const schema = new mongoose.Schema({
    announcementId:{type:Number, unique: true, required: true},
    key:{type: String, required: false},
    type: {type: String,enum: ['ALL','AGENT','MEMBER']},
    message:{type: String},
    message_th:{type: String},
    createdBy: {type: Schema.Types.ObjectId, ref: 'User', default: null},
    updatedBy: {type: Schema.Types.ObjectId, ref: 'User', default: null},
    createdDate: {type: Date},
    updatedDate: {type: Date},
    // lang: {type: String,enum: ['EN','TH']},
    active: {type: Boolean, default: true}
});

schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    if (!this.createdDate) {
        this.createdDate = now;
    }
    next();
});

schema.plugin(autoIncrement.plugin, {
    model: 'Announcement',
    field: 'announcementId',
    startAt: 1,
    incrementBy: 1
});

schema.plugin(mongoosePaginate);

module.exports = mongoose.model('Announcement', schema);