const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new mongoose.Schema({
    userId: {type: Schema.Types.ObjectId, ref: 'Member', required: true},
    createdDate: {type: Date, default: Date.now},
});


schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    next();
});


module.exports = mongoose.model('MemberMuayStep2', schema);