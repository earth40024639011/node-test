const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    username: {type: String,required: true},
    amount: {type: Number, required: true},
    betId: {type: String, required: false},
    description: {type: String, required: false},
    ref: {type: String, required: true},
    beforeCredit: {type: Number},
    status : {type: String, enum: ['SUCCESS','FAIL'],required: true},
    createdDate: { type: Date, expires: ((60 * 60) * 24) * 30, default: Date.now }
});


schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (this.username) {
        this.username = this.username.toLowerCase();
    }
    next();
});



schema.plugin(mongoosePaginate);
schema.index({ betId: 1});
schema.index({ username: 1,createdDate:1});
schema.index({ username : 1, amount : 1, ref: 1 ,status:1},{unique: true,name:'uniqueIndex'});

module.exports = mongoose.model('UpdateBalanceHistory2', schema);