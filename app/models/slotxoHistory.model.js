const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    betId: {type: String},
    settleId: {type: String},
    cancelId: {type: String},
    withdrawId: {type: String},
    depositId: {type: String},
    amount: {type: Number},
    startBalance: {type: String},
    endBalance: {type: String},
    gameCode: { type: String},
    roundId: { type: String},
    hash: { type: String, required: true},
    username: { type: String, required: true},
    timestamp: {type: Number, required: true},
    createdDate: { type: Date, expires: ((60 * 60) * 24) * 2, default: Date.now },
    description: { type: String},
    type: { type: String}
});

schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);

module.exports = mongoose.model('SlotxoBetHistory', schema);