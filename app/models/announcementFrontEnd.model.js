const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    key: {type: String,unique :true,required: true},
    message:{type: String, required: true},
    createdBy: {type: Schema.Types.ObjectId, ref: 'User', default: null},
    createdDate: {type: Date},
    updatedDate: {type: Date},
    active: {type: Boolean, default: true}
});

schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    if (!this.createdDate) {
        this.createdDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);

module.exports = mongoose.model('AnnouncementFrontEnd', schema);