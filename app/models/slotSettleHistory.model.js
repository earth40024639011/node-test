const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({

    requestDateTime: {type: Date},
    tranDateTime: {type: Date},
    betId: {type: Number},
    gameId: {type: Number},
    gameType: { type: String },
    playerId: { type: String },
    betAmount: {type: Number},
    jackpotContribution : {type: Number},
    currency: { type: String },
    exchangeRate: {type: Number},
    resultId: { type: String },
    result: { type: String },
    resultType: { type: Number },
    payout: {type: Number},
    winLose: {type: Number},
    createdDate: {type: Date, default: Date.now},

});

schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);

module.exports = mongoose.model('SlotSettleHistory', schema);