const mongoose = require('mongoose');
const Schema   = mongoose.Schema;

const themeSchema = new Schema({
    action      : {type: String, enum: ['LEAGUE_CREATE','LEAGUE_UPDATE','MATCH_CREATE','MATCH_UPDATE','MATCH_DELETE']},
    reqBodyObj  : { type: Object, required: true},
    resBodyObj  : { type: Object, required: true},
    previousObj : { type: Object, required: true},
    currentObj  : { type: Object, required: true},
    createdDate : {type: Date, default: Date.now},
    actionBy    : {type: Schema.Types.ObjectId, ref: 'User'}
});

themeSchema.pre('save', (next) => {
    if (!this.created) {
        this.created = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    }
    next();
});

module.exports = mongoose.model('SpacialOddsActionLog', themeSchema);