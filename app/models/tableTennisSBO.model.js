const mongoose = require('mongoose');
const Schema   = mongoose.Schema;

const themeSchema = new Schema({
    k : { type: Number, index: { unique: true }},
    n : {
        en : { type: String },
        th : { type: String },
        cn : { type: String },
        tw : { type: String }
    },
    sk : { type: Number, default : 30},
    r : {
        ah : {
            ma : { type: Number, default: 5000 },
            mi : { type: Number, default: 10 }
        },
        ml : {
            ma : { type: Number, default: 5000 },
            mi : { type: Number, default: 10 }
        },
        pm : { type: Number, default: 999999 }
    }
});

themeSchema.pre('save', (next) => {
    if (!this.created) {
        this.created = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    }
    next();
});

module.exports = mongoose.model('TableTennisSBO', themeSchema);