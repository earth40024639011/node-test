const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    playerId: {type: String},
    sessionId: {type: String},
    amount: {type: Number},
    currency: {type: String},
    transactionId: {type: String},
    game: {type: String},
    gameRound: {type: String},
    roundEnd: { type: Boolean},
    extra: { type: Object},
    createdDate: { type: Date, expires: ((60 * 60) * 24) * 15, default: Date.now }
    // playerId: {type: String},
    // sessionId: {type: String},
    // withdrawAmount: {type: Number},
    // depositAmount: {type: Number},
    // currency: {type: String},
    // transactionId: {type: String},
    // game: {type: String},
    // gameRound: {type: String},
    // roundEnd: { type: Boolean},
    // extra: { type: Object},
    // description: { type: String},
    // createdDate: { type: Date, expires: ((60 * 60) * 24) * 15, default: Date.now }
});

schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.createdDate) {
        this.createdDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);

module.exports = mongoose.model('GanapatiHistory', schema);