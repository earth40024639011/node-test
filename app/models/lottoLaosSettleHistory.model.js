const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    gameId: {type: String},
    L_4TOP: {type: String, required: true},
    L_4TOD: [{type: String, required: false}],
    L_3TOP: {type: String, required: true},
    L_3TOD: [{type: String, required: false}],
    L_2FB:  [{type: String, required: false}],
    T_4TOP: {type: String, required: true},
    T_4TOD: [{type: String, required: false}],
    T_3TOP: {type: String, required: true},
    T_3TOD: [{type: String, required: false}],
    T_2TOP: {type: String, required: true},
    T_2TOD: [{type: String, required: false}],
    T_2BOT: {type: String, required: true},
    T_1TOP: [{type: String, required: false}],
    T_1BOT: [{type: String, required: false}],
    createdDate: {type: Date, default: Date.now},
});

schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);

module.exports = mongoose.model('LottoLaosSettleHistory', schema);