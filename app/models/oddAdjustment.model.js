const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new mongoose.Schema({
    username: { type: String, required: true },
    matchId : { type: String, required: true },
    prefix : { type: String, required: true },
    key : { type: String, required: true },
    value : { type: String, required: true },
    count : { type: Number, default: 0 },
    amount: {type: Number, required: true},
    maxBetPrice: {type: Number, required: true},
    createdDate: { type: Date, expires: 600, default: Date.now }
});

schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.createdDate) {
        this.createdDate = now;
    }
    next();
});

module.exports = mongoose.model('OddAdjustment', schema);