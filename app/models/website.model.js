const mongoose = require('mongoose');
const schema = new mongoose.Schema({
    logo: { type: String },
    depositChannel: [{
        img: { type: String },
        url: { type: String }
    }],
    isActive: { type: Boolean, default: true },
    createdDate: {type: Date, default: Date.now},
    updatedDate: {type: Date},
    isWithdraw:{type:Boolean, default:false},
    isElastic:{type:Boolean, default:false},
    topOne: {
        img: { type: String },
        url: { type: String }
    },
    topTwo: {
        img: { type: String },
        url: { type: String }
    },
    middleOne: {
        img: { type: String },
        url: { type: String }
    },
    middleTwo: {
        img: { type: String },
        url: { type: String }
    },
    middleThree: {
        img: { type: String },
        url: { type: String }
    },
    middleFour: {
        img: { type: String },
        url: { type: String }
    },
    bottomOne: {
        img: { type: String },
        url: { type: String }
    },
    lottery: {
        img: { type: String },
        url: { type: String }
    },
    game: {
        img: { type: String },
        url: { type: String }
    },
    casino: {
        img: { type: String },
        url: { type: String }
    },
    lottoBanner: {
        img: { type: String },
        url: { type: String }
    },
    gameBanner: {
        img: { type: String },
        url: { type: String }
    },
    casinoBanner: {
        img: { type: String },
        url: { type: String }
    },
    ambGameBanner: {
        img: { type: String },
        url: { type: String }
    },
    sportBanner: {
        img: { type: String },
        url: { type: String }
    },
    agentBanner1: {
        img: { type: String }
    },
    agentBanner2: {
        img: { type: String }
    },
    agentLogo: {
        img: { type: String }
    },
    memberGameBanner1 : {
        img : { type: String },
        gameId : { type: String }
    },
    memberGameBanner2 : {
        img : { type: String },
        gameId : { type: String }
    },
    memberGameBanner3 : {
        img : { type: String },
        gameId : { type: String }
    },
    memberGameBanner4 : {
        img : { type: String },
        gameId : { type: String }
    },
    memberGameBanner5 : {
        img : { type: String },
        gameId : { type: String }
    },
    maintenance : {
        img : { type: String }
    },
    ambGame: {
        img: { type: String },
        url: { type: String }
    }
});


schema.pre('save', function (next) {
    var now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.createdDate) {
        this.createdDate = now;
    }
    this.updatedDate = now;
    next();
});


module.exports = mongoose.model('Website', schema);