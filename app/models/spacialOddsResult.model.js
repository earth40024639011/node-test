const mongoose = require('mongoose');
const Schema   = mongoose.Schema;

const themeSchema = new Schema({
    id: { type: String },
    k:  { type: Number },
    d : { type: Date },
    n : {
        en : {
            h : { type: String },
            a : { type: String }
        },
        th : {
            h : { type: String },
            a : { type: String }
        },
        cn : {
            h : { type: String },
            a : { type: String }
        },
        tw : {
            h : { type: String },
            a : { type: String }
        }
    },
    ht : { type: String },
    ft : { type: String },
    moreSize : { type: Number, default: 0 },
    createdDate: { type: Date, expires: ((60 * 60) * 24) * 7, default: Date.now }
});

themeSchema.pre('save', (next) => {
    if (!this.createdDate) {
        this.createdDate = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    }
    next();
});

module.exports = mongoose.model('SpacialOddsResult', themeSchema);