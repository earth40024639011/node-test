const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    type: {type: String,enum: ['AGENT','MEMBER']},
    amount:{type: Number, required: true},
    remainingBalance : {type: Number, required: true},
    member: {type: Schema.Types.ObjectId, ref: 'Member', default: null},
    agent: {type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
    currency: {type: String,enum: ['THB']},
    group: {type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
    createdBy: {type: Schema.Types.ObjectId, ref: 'User', default: null},
    createdDate: {type: Date},
    updatedDate: {type: Date},
    startDate: {type: Date},
    endDate: {type: Date},
});

schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    if (!this.createdDate) {
        this.createdDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);

module.exports = mongoose.model('PaymentHistory', schema);