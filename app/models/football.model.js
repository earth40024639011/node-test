const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const themeSchema = new mongoose.Schema({
    k : { type: Number, index: { unique: true }},
    n : { type: String },
    nn : {
        en : { type: String },
        th : { type: String },
        cn : { type: String },
        tw : { type: String }
    },
    sk : { type: Number, default : 30},
    rm : {
        ah : {
            ma : { type: Number },
            mi : { type: Number }
        },
        ou : {
            ma : { type: Number },
            mi : { type: Number }
        },
        x12 : {
            ma : { type: Number },
            mi : { type: Number }
        },
        oe : {
            ma : { type: Number },
            mi : { type: Number }
        },
        ah1st : {
            ma : { type: Number },
            mi : { type: Number }
        },
        ou1st : {
            ma : { type: Number },
            mi : { type: Number }
        },
        x121st : {
            ma : { type: Number },
            mi : { type: Number }
        },
        oe1st : {
            ma : { type: Number },
            mi : { type: Number }
        },
        pm : { type: Number }
    },
    rmem : {
        ah : {
            ma : { type: Number },
            mi : { type: Number }
        },
        ou : {
            ma : { type: Number },
            mi : { type: Number }
        },
        x12 : {
            ma : { type: Number },
            mi : { type: Number }
        },
        oe : {
            ma : { type: Number },
            mi : { type: Number }
        },
        ah1st : {
            ma : { type: Number },
            mi : { type: Number }
        },
        ou1st : {
            ma : { type: Number },
            mi : { type: Number }
        },
        x121st : {
            ma : { type: Number },
            mi : { type: Number }
        },
        oe1st : {
            ma : { type: Number },
            mi : { type: Number }
        },
        pm : { type: Number }
    },
    rlm : {
        ah : {
            ma : { type: Number },
            mi : { type: Number }
        },
        ou : {
            ma : { type: Number },
            mi : { type: Number }
        },
        x12 : {
            ma : { type: Number },
            mi : { type: Number }
        },
        oe : {
            ma : { type: Number },
            mi : { type: Number }
        },
        ah1st : {
            ma : { type: Number },
            mi : { type: Number }
        },
        ou1st : {
            ma : { type: Number },
            mi : { type: Number }
        },
        x121st : {
            ma : { type: Number },
            mi : { type: Number }
        },
        oe1st : {
            ma : { type: Number },
            mi : { type: Number }
        },
        pm : { type: Number }
    },
    rp : {
        hdp : {
            ah : {
                f : { type: String, default : "0" },
                u : { type: String, default : "0"  }
            },
            ou : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            },
            x12 : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            },
            oe : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            },
            ah1st : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            },
            ou1st : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            },
            x121st : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            },
            oe1st : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            }
        },
        live : {
            ah : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            },
            ou : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            },
            x12 : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            },
            oe : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            },
            ah1st : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            },
            ou1st : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            },
            x121st : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            },
            oe1st : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            }
        }
    },
    r : {
        ah : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 0 }
        },
        ou : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 0 }
        },
        x12 : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 0 }
        },
        oe : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 0 }
        },
        ah1st : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 0 }
        },
        ou1st : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 0 }
        },
        x121st : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 0 }
        },
        oe1st : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 0 }
        },
        pm : { type: Number, default: 500000 }
    },
    rem : {
        ah : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 0 }
        },
        ou : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 0 }
        },
        x12 : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 0 }
        },
        oe : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 0 }
        },
        ah1st : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 0 }
        },
        ou1st : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 0 }
        },
        x121st : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 0 }
        },
        oe1st : {
            ma : { type: Number, default: 50000 },
            mi : { type: Number, default: 0 }
        },
        pm : { type: Number, default: 500000 }
    },
    rl : {
        ah : {
            ma : { type: Number, default: 500000 },
            mi : { type: Number, default: 180 }
        },
        ou : {
            ma : { type: Number, default: 500000 },
            mi : { type: Number, default: 180 }
        },
        x12 : {
            ma : { type: Number, default: 500000 },
            mi : { type: Number, default: 180 }
        },
        oe : {
            ma : { type: Number, default: 500000 },
            mi : { type: Number, default: 180 }
        },
        ah1st : {
            ma : { type: Number, default: 500000 },
            mi : { type: Number, default: 180 }
        },
        ou1st : {
            ma : { type: Number, default: 500000 },
            mi : { type: Number, default: 180 }
        },
        x121st : {
            ma : { type: Number, default: 500000 },
            mi : { type: Number, default: 180 }
        },
        oe1st : {
            ma : { type: Number, default: 500000 },
            mi : { type: Number, default: 180 }
        },
        pm : { type: Number, default: 5000000 }
    },
    m : [{
        id: { type: String },
        k188: { type: String },
        liveTv: { type: Object},
        k : { type: Number },
        d : { type: Date },
        hasParlay : { type: Boolean },
        i : {
            lt : { type: String },
            mt : { type: String },
            h : { type: String },
            a : { type: String },
            s : { type: String },
            hrc : { type: String },
            arc : { type: String },
        },
        s : {
            isH : { type : Boolean, default : true},//HDP
            isL : { type : Boolean, default : false},//LIVE
            isE : { type : Boolean, default : false},//END
            isCr : { type : Boolean, default : false},//END
            cr : {
                ft : {
                    h : { type: Number },
                    a : { type: Number },
                },
                ht : {
                    h : { type: Number },
                    a : { type: Number },
                }
            },
            isC : {//Cancel
                ft : { type: Boolean, default: false },
                ht : { type: Boolean, default: false },
                ft_remark : { type: String, default : ''  },
                ht_remark : { type: String, default : ''  }
            },
            isAl : { type : Boolean, default : true},//Allow to Live
            isD : { type : Boolean, default : false}//DONE
        },
        n : {
            en : {
                h : { type: String },
                a : { type: String }
            },
            th : {
                h : { type: String },
                a : { type: String }
            },
            cn : {
                h : { type: String },
                a : { type: String }
            },
            tw : {
                h : { type: String },
                a : { type: String }
            }
        },
        isToday : { type: Boolean, default : false},
        isPopular : { type: Boolean, default : false },
        r : {
            pm : { type: Number, default: 500000 }
        },
        rm : {
            pm : { type: Number, default: 500000 }
        },
        rem : {
            pm : { type: Number, default: 500000 }
        },
        rmem : {
            pm : { type: Number, default: 500000 }
        },
        rl : {
            pm : { type: Number, default: 500000 }
        },
        rlm : {
            pm : { type: Number, default: 500000 }
        },
        //Spacial rule
        sr : {
            isOn : { type: Boolean },
            ah : {
                ma : { type: Number },
                mi : { type: Number }
            },
            ou : {
                ma : { type: Number },
                mi : { type: Number }
            },
            x12 : {
                ma : { type: Number },
                mi : { type: Number }
            },
            oe : {
                ma : { type: Number },
                mi : { type: Number }
            },
            ah1st : {
                ma : { type: Number },
                mi : { type: Number }
            },
            ou1st : {
                ma : { type: Number },
                mi : { type: Number }
            },
            x121st : {
                ma : { type: Number },
                mi : { type: Number }
            },
            oe1st : {
                ma : { type: Number },
                mi : { type: Number }
            },
            pm : { type: Number }
        },//Today
        srem : {
            isOn : { type: Boolean },
            ah : {
                ma : { type: Number },
                mi : { type: Number }
            },
            ou : {
                ma : { type: Number },
                mi : { type: Number }
            },
            x12 : {
                ma : { type: Number },
                mi : { type: Number }
            },
            oe : {
                ma : { type: Number },
                mi : { type: Number }
            },
            ah1st : {
                ma : { type: Number },
                mi : { type: Number }
            },
            ou1st : {
                ma : { type: Number },
                mi : { type: Number }
            },
            x121st : {
                ma : { type: Number },
                mi : { type: Number }
            },
            oe1st : {
                ma : { type: Number },
                mi : { type: Number }
            },
            pm : { type: Number }
        },//Earnly Maket
        srl : {
            isOn : { type: Boolean },
            ah : {
                ma : { type: Number },
                mi : { type: Number }
            },
            ou : {
                ma : { type: Number },
                mi : { type: Number }
            },
            x12 : {
                ma : { type: Number },
                mi : { type: Number }
            },
            oe : {
                ma : { type: Number },
                mi : { type: Number }
            },
            ah1st : {
                ma : { type: Number },
                mi : { type: Number }
            },
            ou1st : {
                ma : { type: Number },
                mi : { type: Number }
            },
            x121st : {
                ma : { type: Number },
                mi : { type: Number }
            },
            oe1st : {
                ma : { type: Number },
                mi : { type: Number }
            },
            pm : { type: Number }
        },//Live

        bp : [{
            allKey: [{ type: String }],
            isOn:{ type : Boolean, default : true},
            isOnM:{ type : Boolean, default : true},
            ah : {
                h : {type: String},
                hk : {type: String},
                hp: {type: String},
                hpk : {type: String},
                a : {type: String},
                ak : {type: String},
                ap: {type: String},
                apk : {type: String}
            },
            ou : {
                o : {type: String},
                ok : {type: String},
                op: {type: String},
                opk : {type: String},
                u : {type: String},
                uk : {type: String},
                up: {type: String},
                upk : {type: String}
            },
            x12 : {
                h : {type: String},
                hk : {type: String},
                a : {type: String},
                ak : {type: String},
                d : {type: String},
                dk : {type: String}
            },
            oe : {
                o : {type: String},
                ok : {type: String},
                e : {type: String},
                ek : {type: String}
            },
            ah1st : {
                h : {type: String},
                hk : {type: String},
                hp: {type: String},
                hpk : {type: String},
                a : {type: String},
                ak : {type: String},
                ap: {type: String},
                apk : {type: String}
            },
            ou1st : {
                o : {type: String},
                ok : {type: String},
                op: {type: String},
                opk : {type: String},
                u : {type: String},
                uk : {type: String},
                up: {type: String},
                upk : {type: String}
            },
            x121st : {
                h : {type: String},
                hk : {type: String},
                a : {type: String},
                ak : {type: String},
                d : {type: String},
                dk : {type: String}
            },
            oe1st : {
                o : {type: String},
                ok : {type: String},
                e : {type: String},
                ek : {type: String}
            },
            r : {
                isLock : { type: Boolean, default : false },
                ah : {
                    ma : { type: Number, default: 50000 },
                    mi : { type: Number, default: 0 }
                },
                ou : {
                    ma : { type: Number, default: 50000 },
                    mi : { type: Number, default: 0 }
                },
                x12 : {
                    ma : { type: Number, default: 50000 },
                    mi : { type: Number, default: 0 }
                },
                oe : {
                    ma : { type: Number, default: 50000 },
                    mi : { type: Number, default: 0 }
                },
                ah1st : {
                    ma : { type: Number, default: 50000 },
                    mi : { type: Number, default: 0 }
                },
                ou1st : {
                    ma : { type: Number, default: 50000 },
                    mi : { type: Number, default: 0 }
                },
                x121st : {
                    ma : { type: Number, default: 50000 },
                    mi : { type: Number, default: 0 }
                },
                oe1st : {
                    ma : { type: Number, default: 50000 },
                    mi : { type: Number, default: 0 }
                }
            },
            rem : {
                isLock : { type: Boolean, default : false },
                ah : {
                    ma : { type: Number, default: 50000 },
                    mi : { type: Number, default: 0 }
                },
                ou : {
                    ma : { type: Number, default: 50000 },
                    mi : { type: Number, default: 0 }
                },
                x12 : {
                    ma : { type: Number, default: 50000 },
                    mi : { type: Number, default: 0 }
                },
                oe : {
                    ma : { type: Number, default: 50000 },
                    mi : { type: Number, default: 0 }
                },
                ah1st : {
                    ma : { type: Number, default: 50000 },
                    mi : { type: Number, default: 0 }
                },
                ou1st : {
                    ma : { type: Number, default: 50000 },
                    mi : { type: Number, default: 0 }
                },
                x121st : {
                    ma : { type: Number, default: 50000 },
                    mi : { type: Number, default: 0 }
                },
                oe1st : {
                    ma : { type: Number, default: 50000 },
                    mi : { type: Number, default: 0 }
                }
            },
            rp : {
                ah : {
                    h : { type: String, default : "0" },
                    a : { type: String, default : "0"  }
                },
                ou : {
                    h : { type: String, default : "0"  },
                    a : { type: String, default : "0"  }
                },
                x12 : {
                    h : { type: String, default : "0"  },
                    a : { type: String, default : "0"  },
                    d : { type: String, default : "0"  }
                },
                oe : {
                    h : { type: String, default : "0"  },
                    a : { type: String, default : "0"  }
                },
                ah1st : {
                    h : { type: String, default : "0"  },
                    a : { type: String, default : "0"  }
                },
                ou1st : {
                    h : { type: String, default : "0"  },
                    a : { type: String, default : "0"  }
                },
                x121st : {
                    h : { type: String, default : "0"  },
                    a : { type: String, default : "0"  },
                    d : { type: String, default : "0"  }
                },
                oe1st : {
                    h : { type: String, default : "0"  },
                    a : { type: String, default : "0"  }
                }
            }
        }],
        bpl : [{
            allKey: [{ type: String }],
            isOn:{ type : Boolean, default : true},
            isOnM:{ type : Boolean, default : true},
            ah : {
                h : {type: String},
                hk : {type: String},
                hp: {type: String},
                hpk : {type: String},
                a : {type: String},
                ak : {type: String},
                ap: {type: String},
                apk : {type: String}
            },
            ou : {
                o : {type: String},
                ok : {type: String},
                op: {type: String},
                opk : {type: String},
                u : {type: String},
                uk : {type: String},
                up: {type: String},
                upk : {type: String}
            },
            x12 : {
                h : {type: String},
                hk : {type: String},
                a : {type: String},
                ak : {type: String},
                d : {type: String},
                dk : {type: String}
            },
            oe : {
                o : {type: String},
                ok : {type: String},
                e : {type: String},
                ek : {type: String}
            },
            ah1st : {
                h : {type: String},
                hk : {type: String},
                hp: {type: String},
                hpk : {type: String},
                a : {type: String},
                ak : {type: String},
                ap: {type: String},
                apk : {type: String}
            },
            ou1st : {
                o : {type: String},
                ok : {type: String},
                op: {type: String},
                opk : {type: String},
                u : {type: String},
                uk : {type: String},
                up: {type: String},
                upk : {type: String}
            },
            x121st : {
                h : {type: String},
                hk : {type: String},
                a : {type: String},
                ak : {type: String},
                d : {type: String},
                dk : {type: String}
            },
            oe1st : {
                o : {type: String},
                ok : {type: String},
                e : {type: String},
                ek : {type: String}
            },
            r : {
                isLock : { type: Boolean, default : false },
                ah : {
                    ma : { type: Number, default: 50000 },
                    mi : { type: Number, default: 0 }
                },
                ou : {
                    ma : { type: Number, default: 50000 },
                    mi : { type: Number, default: 0 }
                },
                x12 : {
                    ma : { type: Number, default: 50000 },
                    mi : { type: Number, default: 0 }
                },
                oe : {
                    ma : { type: Number, default: 50000 },
                    mi : { type: Number, default: 0 }
                },
                ah1st : {
                    ma : { type: Number, default: 50000 },
                    mi : { type: Number, default: 0 }
                },
                ou1st : {
                    ma : { type: Number, default: 50000 },
                    mi : { type: Number, default: 0 }
                },
                x121st : {
                    ma : { type: Number, default: 50000 },
                    mi : { type: Number, default: 0 }
                },
                oe1st : {
                    ma : { type: Number, default: 50000 },
                    mi : { type: Number, default: 0 }
                }
            },
            rp : {
                ah : {
                    h : { type: String, default : "0" },
                    a : { type: String, default : "0"  }
                },
                ou : {
                    h : { type: String, default : "0"  },
                    a : { type: String, default : "0"  }
                },
                x12 : {
                    h : { type: String, default : "0"  },
                    a : { type: String, default : "0"  },
                    d : { type: String, default : "0"  }
                },
                oe : {
                    h : { type: String, default : "0"  },
                    a : { type: String, default : "0"  }
                },
                ah1st : {
                    h : { type: String, default : "0"  },
                    a : { type: String, default : "0"  }
                },
                ou1st : {
                    h : { type: String, default : "0"  },
                    a : { type: String, default : "0"  }
                },
                x121st : {
                    h : { type: String, default : "0"  },
                    a : { type: String, default : "0"  },
                    d : { type: String, default : "0"  }
                },
                oe1st : {
                    h : { type: String, default : "0"  },
                    a : { type: String, default : "0"  }
                }
            }
        }]
    }]
});

themeSchema.pre('save', (next) => {
    if (!this.created) {
        this.created = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    }
    next();
});

module.exports = mongoose.model('Football', themeSchema);