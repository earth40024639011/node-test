const mongoose = require('mongoose');
const Schema   = mongoose.Schema;

const themeSchema = new Schema({
    id : { type: String, index: { unique: true }},
    created : { type: Date, expires: 60000, default: Date.now }
});

themeSchema.pre('save', (next) => {
    if (!this.created) {
        this.created = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    }
    next();
});

module.exports = mongoose.model('CloseMatch', themeSchema);