const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const themeSchema = new mongoose.Schema({
    k : { type: Number, index: { unique: true }},
    n : { type: String },
    nn : {
        en : { type: String },
        th : { type: String },
        cn : { type: String },
        tw : { type: String }
    },
    sk : { type: Number, default : 100},
    r : {
        ah : {
            ma : { type: Number },
            mi : { type: Number }
        },
        ou : {
            ma : { type: Number },
            mi : { type: Number }
        },
        oe : {
            ma : { type: Number },
            mi : { type: Number }
        },
        ml : {
            ma : { type: Number },
            mi : { type: Number }
        },
        t1ou : {
            ma : { type: Number },
            mi : { type: Number }
        },
        t2ou : {
            ma : { type: Number },
            mi : { type: Number }
        },
        pm : { type: Number }
    },
    rl : {
        ah : {
            ma : { type: Number },
            mi : { type: Number }
        },
        ou : {
            ma : { type: Number },
            mi : { type: Number }
        },
        oe : {
            ma : { type: Number },
            mi : { type: Number }
        },
        ml : {
            ma : { type: Number },
            mi : { type: Number }
        },
        t1ou : {
            ma : { type: Number },
            mi : { type: Number }
        },
        t2ou : {
            ma : { type: Number },
            mi : { type: Number }
        },
        pm : { type: Number }
    },
    rem : {
        ah : {
            ma : { type: Number  },
            mi : { type: Number }
        },
        ou : {
            ma : { type: Number  },
            mi : { type: Number }
        },
        oe : {
            ma : { type: Number  },
            mi : { type: Number }
        },
        ml : {
            ma : { type: Number  },
            mi : { type: Number }
        },
        t1ou : {
            ma : { type: Number  },
            mi : { type: Number }
        },
        t2ou : {
            ma : { type: Number  },
            mi : { type: Number }
        },
        pm : { type: Number }
    },
    rp : {
        hdp : {
            ah : {
                f : { type: String, default : "0" },
                u : { type: String, default : "0"  }
            },
            ou : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            },
            oe : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            },
            ml : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            },
            t1ou : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            },
            t2ou : {
                f : { type: String, default : "0"  },
                u : { type: String, default : "0"  }
            }
        }
    },
    rm : {
        ah : {
            ma : { type: Number },
            mi : { type: Number }
        },
        ou : {
            ma : { type: Number },
            mi : { type: Number }
        },
        oe : {
            ma : { type: Number },
            mi : { type: Number }
        },
        ml : {
            ma : { type: Number },
            mi : { type: Number }
        },
        t1ou : {
            ma : { type: Number },
            mi : { type: Number }
        },
        t2ou : {
            ma : { type: Number },
            mi : { type: Number }
        },
        pm : { type: Number }
    },
    rlm : {
        ah : {
            ma : { type: Number },
            mi : { type: Number }
        },
        ou : {
            ma : { type: Number },
            mi : { type: Number }
        },
        oe : {
            ma : { type: Number },
            mi : { type: Number }
        },
        ml : {
            ma : { type: Number },
            mi : { type: Number }
        },
        t1ou : {
            ma : { type: Number },
            mi : { type: Number }
        },
        t2ou : {
            ma : { type: Number },
            mi : { type: Number }
        },
        pm : { type: Number }
    },
    rmem : {
        ah : {
            ma : { type: Number  },
            mi : { type: Number }
        },
        ou : {
            ma : { type: Number  },
            mi : { type: Number }
        },
        oe : {
            ma : { type: Number  },
            mi : { type: Number }
        },
        ml : {
            ma : { type: Number  },
            mi : { type: Number }
        },
        t1ou : {
            ma : { type: Number  },
            mi : { type: Number }
        },
        t2ou : {
            ma : { type: Number  },
            mi : { type: Number }
        },
        pm : { type: Number }
    },
    m : [{
            id: { type: String },
            k : { type: Number },
            d : { type: Date },
            hasParlay : { type: Boolean },
            i : {
                q : { type: String },
                s : { type: String }
            },
            s : {
                isH : { type : Boolean, default : true},//HDP
                isL : { type : Boolean, default : false},//LIVE
                isE : { type : Boolean, default : false},//END
                isCr : { type : Boolean, default : false},//END
                crFtDate : { type: Date },
                crHtDate : { type: Date },
                cr : {
                    ft : {
                        h : { type: Number, default: 0 },
                        a : { type: Number, default: 0 },
                    }
                },
                isC : {//Cancel
                    ft : { type: Boolean, default: false },
                    ht : { type: Boolean, default: false }
                },
                isAl : { type : Boolean, default : true},//Allow to Live
                isD : { type : Boolean, default : false}//DONE
            },
            n : {
                en : {
                    h : { type: String },
                    a : { type: String }
                },
                th : {
                    h : { type: String },
                    a : { type: String }
                },
                cn : {
                    h : { type: String },
                    a : { type: String }
                },
                tw : {
                    h : { type: String },
                    a : { type: String }
                }
            },
            isToday : { type: Boolean, default : false},
            r : {
                pm : { type: Number }
            },
            rem : {
                pm : { type: Number }
            },
            bp : [{
                allKey: [{ type: String }],
                isOn:{ type : Boolean, default : true},
                ah : {
                    h : {type: String},
                    hk : {type: String},
                    hp: {type: String},
                    hpk : {type: String},
                    a : {type: String},
                    ak : {type: String},
                    ap: {type: String},
                    apk : {type: String}
                },
                ou : {
                    o : {type: String},
                    ok : {type: String},
                    op: {type: String},
                    opk : {type: String},
                    u : {type: String},
                    uk : {type: String},
                    up: {type: String},
                    upk : {type: String}
                },
                oe : {
                    o : {type: String},
                    ok : {type: String},
                    e : {type: String},
                    ek : {type: String}
                },
                ml : {
                    h : {type: String},
                    hk : {type: String},
                    a : {type: String},
                    ak : {type: String}
                },
                t1ou : {
                    o : {type: String},
                    ok : {type: String},
                    op: {type: String},
                    opk : {type: String},
                    u : {type: String},
                    uk : {type: String},
                    up: {type: String},
                    upk : {type: String}
                },
                t2ou : {
                    o : {type: String},
                    ok : {type: String},
                    op: {type: String},
                    opk : {type: String},
                    u : {type: String},
                    uk : {type: String},
                    up: {type: String},
                    upk : {type: String}
                }
            }],
            bpl : [{
                allKey: [{ type: String }],
                isOn:{ type : Boolean, default : true},
                ah : {
                    h : {type: String},
                    hk : {type: String},
                    hp: {type: String},
                    hpk : {type: String},
                    a : {type: String},
                    ak : {type: String},
                    ap: {type: String},
                    apk : {type: String}
                },
                ou : {
                    o : {type: String},
                    ok : {type: String},
                    op: {type: String},
                    opk : {type: String},
                    u : {type: String},
                    uk : {type: String},
                    up: {type: String},
                    upk : {type: String}
                },
                oe : {
                    o : {type: String},
                    ok : {type: String},
                    e : {type: String},
                    ek : {type: String}
                },
                ml : {
                    h : {type: String},
                    hk : {type: String},
                    a : {type: String},
                    ak : {type: String}
                },
                t1ou : {
                    o : {type: String},
                    ok : {type: String},
                    op: {type: String},
                    opk : {type: String},
                    u : {type: String},
                    uk : {type: String},
                    up: {type: String},
                    upk : {type: String}
                },
                t2ou : {
                    o : {type: String},
                    ok : {type: String},
                    op: {type: String},
                    opk : {type: String},
                    u : {type: String},
                    uk : {type: String},
                    up: {type: String},
                    upk : {type: String}
                }
            }]
        }]
});

themeSchema.pre('save', (next) => {
    if (!this.created) {
        this.created = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    }
    next();
});

module.exports = mongoose.model('Basketball', themeSchema);