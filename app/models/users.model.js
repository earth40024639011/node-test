const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    username: {type: String, unique: true, required: true},
    username_lower: {type: String, unique: true, required: true},
    password: {type: String, required: true},
    contact: {type: String, required: false},
    transactionCode: {type: String, required: false},
    group: {type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
    permission: [{type: String, enum: ['ALL','STOCK_MANAGEMENT', 'MEMBER_MANAGEMENT','MEMBER_MANAGEMENT_VIEW', 'REPORT','OLD_REPORT','PAYMENT','CASH','CORRECT_SCORE']}],
    resetPassword: {type: Boolean, default: false},
    updatedDate: {type: Date},
    createdDate: {type: Date, default: Date.now},
    ipAddress:{type:String},
    lastLogin: {type: Date},
    tokenUniqueId: {type:String, default: null},
    passwordLock:{type: Boolean, default: false},
    active: {type: Boolean, default: true},
    isAutoTopUp: {type: Boolean, default: false}
});

schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);

module.exports = mongoose.model('User', schema);