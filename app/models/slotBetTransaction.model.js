const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({

    requestDateTime: {type: Date,required: true},
    tranDateTime: {type: Date,required: true},
    betId: {type: Number, required: true},
    gameId: {type: Number, required: true},
    gameType: { type: String },
    playerId: { type: String },
    betAmount: {type: Number, required: true},
    jackpotContribution : {type: Number, required: true},
    currency: { type: String },
    exchangeRate: {type: Number, required: true},
    status:{ type: String },
});

schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);

module.exports = mongoose.model('SlotBetTransaction', schema);