const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const themeSchema = new mongoose.Schema({
    version : { type: String }
});

themeSchema.pre('save', (next) => {
    if (!this.created) {
        this.created = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    }
    next();
});

module.exports = mongoose.model('Version', themeSchema);