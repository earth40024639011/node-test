var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new mongoose.Schema({
        countryCode: {type: String, required: true},
        bankCode: {type: String, required: true},
        bankName: { type: String, required: true},
        bankNameTH:{type: String},
        bankShortName: { type: String, required: true},
        active:{type:Boolean,default:true},
        modifyDate:{type: Date},
        createdDate:{type: Date, default: Date.now}
});

schema.index({ countryCode: 1, bankCode: 1 }, { unique: true });
schema.pre('save', function (next) {
    var now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.createdDate) {
        this.createdDate = now;
    }
    next();
});

module.exports = mongoose.model('Bank', schema);