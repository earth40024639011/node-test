var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new mongoose.Schema({
        description: {type: String},
        createdDate:{type: Date, default: Date.now}
});

schema.index({ countryCode: 1, bankCode: 1 }, { unique: true });
schema.pre('save', function (next) {
    var now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.createdDate) {
        this.createdDate = now;
    }
    next();
});

module.exports = mongoose.model('LogError', schema);