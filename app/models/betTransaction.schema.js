const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const schema = new mongoose.Schema({
    betId: {type: String, required: true,unique:true},
    memberId: {type: Schema.Types.ObjectId, ref: 'Member', default: null},
    memberParentGroup:{type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
    memberUsername : {type:String},
    parlay:{
        matches: [{
            leagueId: {type: String, required: false},
            matchId: {type: String, required: false},
            leagueName: {type: String, required: false},
            leagueNameN : {
                en : { type: String, required: false },
                th : { type: String, required: false },
                cn : { type: String, required: false },
                tw : { type: String, required: false }
            },
            matchName: {
                en : {
                    h : { type: String },
                    a : { type: String }
                },
                th : {
                    h : { type: String },
                    a : { type: String }
                },
                cn : {
                    h : { type: String },
                    a : { type: String }
                },
                tw : {
                    h : { type: String },
                    a : { type: String }
                }
            },
            matchDate:{type:Date,required:false},
            odd: {type: Number, required: false},
            oddKey: {type: String, required: false},
            oddType: {type: String,enum:
                    [
                        'AH','OU','OE','X12','AH1ST',
                        'OU1ST','OE1ST','X121ST','ML','T1OU',
                        'T2OU', 'TG', 'DC', 'CS', 'FHCS',
                        'FTHT', 'FGLG', 'TTKO', 'TC',
                        'TTC', 'TTC1ST', '1STG', '2NDG', '3RDG', '4THG', '5THG', '6THG', '7THG', '8THG', '9THG'
                    ], required: false},
            handicap: {type: String, required: false},
            bet: {type: String, enum: [
                'HOME',
                'AWAY',
                'UNDER',
                'OVER',
                'DRAW',
                'ODD',
                'EVEN',
                'TG01',
                'TG23',
                'TG46',
                'TG7OVER',
                'DC1X',//Home Win OR Draw
                'DC12',//Draw
                'DC2X', //Away Win OR Draw
                'CS00',//Haft time and Full time
                'CS01',
                'CS02',
                'CS03',
                'CS04',
                'CS10',
                'CS11',
                'CS12',
                'CS13',
                'CS14',
                'CS20',
                'CS21',
                'CS22',
                'CS23',
                'CS24',
                'CS30',
                'CS31',
                'CS32',
                'CS33',
                'CS34',
                'CS40',
                'CS41',
                'CS42',
                'CS43',
                'CS44',
                'CSAOS',
                'FTHT_HH',
                'FTHT_HD',
                'FTHT_HA',
                'FTHT_DH',
                'FTHT_DD',
                'FTHT_DA',
                'FTHT_AH',
                'FTHT_AD',
                'FTHT_AA',
                'FGLGFH',
                'FGLGHL',
                'FGLGAF',
                'FGLGAL',
                'FGLGNG'//No goal
                ],required: false},
            score: {type: String, required: false},
            halfScore: {type: String, required: false},
            fullScore: {type: String, required: false},
            matchType: {type: String, enum: ['NONE_LIVE','LIVE'],required: false},
            betResult:{type: String,enum: ['WIN','HALF_WIN','LOSE','HALF_LOSE','DRAW'],required: false},
            isCancel: {type: Boolean, default: false},
            sportType:{type: String, enum: ['FOOTBALL','BASKETBALL', 'MUAY_THAI', 'TENNIS', 'ESPORT', 'TABLE_TENNIS', 'VOLLEYBALL', 'RUGBY', "AMERICAN_FOOTBALL", "CRICKET", "CYCLING", "DARTS"], required: false},
            liveTimeBet:{type: String, default: ''},
            md5: {type: String, default: ''},
            md5d: {type: String, default: ''},
            settleDate: {type: Date},
            remark:{type:String},
            keyIn:{type: String, default: ''}
        }],
        odds : {type: Number, required: false},
        keyInAll:{type: String, default: ''}
    },
    hdp:{
        leagueId: {type: String, required: false},
        matchId: {type: String, required: false},
        leagueName: {type: String, required: false},
        leagueNameN : {
            en : { type: String, required: false },
            th : { type: String, required: false },
            cn : { type: String, required: false },
            tw : { type: String, required: false }
        },
        matchName: {
            en : {
                h : { type: String },
                a : { type: String }
            },
            th : {
                h : { type: String },
                a : { type: String }
            },
            cn : {
                h : { type: String },
                a : { type: String }
            },
            tw : {
                h : { type: String },
                a : { type: String }
            }
        },
        matchDate:{type:Date,required:false},
        odd: {type: Number, required: false},
        oldOdd : {type:Number,required:false},
        nextOdd: {type: Number, required: false},
        oddDistance: {type: Number, required: false},
        isRobot : {type:Boolean,default:false},
        robotDate: {type: Date, required: false},
        autoUpdateOdd :  {type:Boolean,default:false},
        oddKey: {type: String, required: false},
        oddType: {type: String,enum: [
                'AH','OU','OE','X12','AH1ST',
                'OU1ST','OE1ST','X121ST','ML','T1OU',
                'T2OU', 'TG', 'DC', 'CS', 'FHCS',
                'FTHT', 'FGLG', 'TTKO', 'TC',
                'TTC', 'TTC1ST', '1STG', '2NDG', '3RDG', '4THG', '5THG', '6THG', '7THG', '8THG', '9THG'
            ], required: false},
        handicap: {type: String, required: false},
        bet: {type: String, enum: [
                'HOME',
                'AWAY',
                'UNDER',
                'OVER',
                'DRAW',
                'ODD',
                'EVEN',
                'TG01',
                'TG23',
                'TG46',
                'TG7OVER',
                'DC1X',//Home Win OR Draw
                'DC12',//Draw
                'DC2X', //Away Win OR Draw
                'CS00',//Haft time and Full time
                'CS01',
                'CS02',
                'CS03',
                'CS04',
                'CS10',
                'CS11',
                'CS12',
                'CS13',
                'CS14',
                'CS20',
                'CS21',
                'CS22',
                'CS23',
                'CS24',
                'CS30',
                'CS31',
                'CS32',
                'CS33',
                'CS34',
                'CS40',
                'CS41',
                'CS42',
                'CS43',
                'CS44',
                'CSAOS',
                'FTHT_HH',
                'FTHT_HD',
                'FTHT_HA',
                'FTHT_DH',
                'FTHT_DD',
                'FTHT_DA',
                'FTHT_AH',
                'FTHT_AD',
                'FTHT_AA',
                'FGLGFH',
                'FGLGHL',
                'FGLGAF',
                'FGLGAL',
                'FGLGNG'//No goal
            ],required: false},
        score: {type: String, required: false},
        halfScore: {type: String, required: false},
        fullScore: {type: String, required: false},
        matchType: {type: String, enum: ['NONE_LIVE','LIVE'],required: false},
        liveTimeBet: {type: String, default: ''},
        md5: {type: String, default: ''},
        md5d: {type: String, default: ''}
    },
    baccarat : {
        sexy : {
            currency: {type: String, required: false},
            dealerDomain: {type: String, required: false},
            source: {type: String, required: false},
            tableId: {type: Number, required: false},
            roundId: {type: String, required: false},
            roundStartTime: {type: Date, required: false},
            result: [{type: String, required: false}],
            txns: [{
                txId: {type: String, required: false},
                gameType: {type: String, required: false},
                betAmount: {type: Number, required: false},
                betTime: {type: Date, required: false},
                updateTime: {type: Date, required: false},
                category: {type: String, required: false},
                winLose: {type: Number},
                odds: {type: Number},
                status: {type: String, enum:['LOSE', 'WIN', 'TIE', 'VOID', 'CANCEL', 'BET', 'UNSETTLE', 'VOIDROUND']},
            }]
        },
        lotus : {
            gameId: {type: String, required: false},
            userId: {type: String, required: false},
            winLose: {type: Number, required: false},
            roundId: {type: String, required: false},
            gameName: {type: String, required: false},
            txns: [{
                betAmount: {type: Number, required: false},
                transactionId: {type: String, required: false},
            }],
            betting: [{
                bet:{type: String, required: false},
                betAmount:{type: Number, required: false},
            }],
            results: [{type: String, required: false}],
            result:{type: String, required: false}
        },
        allbet : {
            gameRoundId:{ type: String},
            betNum:{ type: String},
            tranType:{ type: Number},
            client:{ type: String},
            betTime:{ type: String},
            gameType:{ type: String},
            commission:{ type: String},
            tableName:{ type: String},
            enterType:{ type: Number},
            validAmount: {type:Number},
            winOrLoss: {type:Number},
            gameResult : {type:String},
            gameResult2 : {type:String},
            gameRoundStartTime:{ type: Date},
            gameRoundEndTime:{ type: Date},
            txns:[{
                tranId:{ type: String},
                betType: {type: String},
                betAmount:{ type: Number},
            }]
        },
        sa : {
            gameId: {type: String, required: false},
            gameType: {type: String, required: false},
            hostId: {type: String, required: false},
            payoutTime :{ type: Date},
            winLose:{type: Number, required: false},
            txns: [{
                txnId: {type: String, required: false},
                betAmount: {type: Number, required: false}
            }]
        },
        dg : {
            ticketId: {type: String, required: false},
            gameType: {type: String, required: false},
            hostId: {type: String, required: false},
            payoutTime :{ type: Date},
            winLose:{type: Number, required: false},
            txns: [{
                data: {type: String, required: false},
                betAmount: {type: Number, required: false}
            }]
        },
        ag : {
            gameId: {type: String, required: false},
            gameType: {type: String, required: false},
            hostId: {type: String, required: false},
            payoutTime :{ type: Date},
            winLose:{type: Number, required: false},
            betTime: {type: Date, required: false},
            result : {type: String, required: false},
            validBetAmount : {type: Number, required: false},
            txns: [{
                txnId: {type: String, required: false},
                betAmount: {type: Number, required: false},
                playType : {type: String, required: false}
            }],
            resultTransaction: [{
                txnId: {type: String, required: false},
                netAmount: {type: Number, required: false},
                validAmount : {type: Number, required: false},
                billNo : {type: String, required: false},
                playType : {type: String, required: false},
                isFinish : {type: String, required: false}
            }]
        },
        pretty : {
            gameId: {type: String, required: false},
            gameType: {type: String, required: false},
            clientType: {type: String, required: false},
            payoutTime :{ type: Date},
            winLose:{type: Number, required: false},
            txns: [{
                txnId: {type: String, required: false},
                betAmount: {type: Number, required: false},
                betType: {type: String, required: false}
            }],
            result:{type: String, required: false}
        }
    },
    slot : {
        live22 : {
            requestDateTime: {type: Date,required: false},
            tranDateTime: {type: Date,required: false},
            betId: {type: Number, required: false},
            gameId: {type: Number, required: false},
            gameType: { type: String },
            gameName: { type: String },
            jackpotContribution : {type: Number, required: false},
            currency: { type: String },
            exchangeRate: {type: Number, required: false},
            result: { type: String },
            resultId: { type: Number },
            resultType: { type: Number },
            betAmount: {type: Number, required: false},
            payout: {type: Number},
            winLose: {type: Number}
        },
        ameba : {
            siteId: {type: Number},
            accountName: {type: String},
            payoutAmt: {type: Number},
            gameId: {type: Number},
            gameName: { type: String },
            roundId: {type: String},
            rebateAmt: {type: String},
            prizeType: {type: String},
            priceAmt: {type: String},
            sumPayoutAmt: {type:String},
            time :{type:String},
            txns: [{
                txId: {type: String},
                betAmt: {type: Number},
                sessionId: {type: String},
                free: {type: Boolean},
            }],
            jp: {
                pcConAmt: {type: String},
                jcConAmt: {type: String},
                winId: {type: String},
                pcWinAmt: {type: String},
                jcWinAmt: {type: String},
                winLv: {type: Number},
                directPay: {type: Boolean},
            }
        },
        spade : {
            transferId: {type:String},
            acctId: {type:String},
            currency: {type:String},
            amount: {type: Number},
            type: {type: Number},
            channel: {type:String},
            gameCode:{type:String},
            gameName:{type:String},
            ticketId: {type:String},
            referenceId: {type:String},
            specialGame: {
                type:{type:String},
                count:{type: Number},
                sequence:{type: Number},
            },
            refTicketIds: [{type:String}],
        },
        xo : {
            gameCode: { type: String, required: false},
            gameName : {type: String, required: false},
            settleId : { type: String, required: false},
            roundId: { type: String, required: false},
            txns: [{
                txId: {type: String},
                betAmt: {type: Number},
                timestamp: {type: String},
                hash: {type: String},
            }],
            winLoss : {type: Number},
            description : {type: String},
            type : {type: String}
        },
        ganapati : {
            transactionId: { type: String, required: false},
            amount : {type: Number, required: false},
            gameName: { type: String, required: false},
            gameRound : {type: String},
            winLoss : {type: Number},
            roundEnd : {type: Boolean}
        },
        ds: {
            transId: {type: String},
            agent: {type: String},
            account: {type: String},
            amount: {type: Number},
            gameId:{type:String},
            gameName:{type:String},
            winLoss:{type:Number}
        },
        pg: {
            transactionId: { type: String, required: false},
            amount : {type: Number, required: false},
            gameName: { type: String, required: false},
            pgBetId : {type: String},
            winLoss : {type: Number}
        }
    },
    multi:{
        amb : {
            roundId:{type:String},
            gameId: {type: String},
            gameName: {type:String},
            rank: {type: String},
            currency:{type:String},
            type:{type:String},
            txns:[{
                amount: {type: Number},
                refNo:{type:String},
            }],
            bet:[{
                key:{type:String},
                amount:{type: Number},
            }],
            results:[{type:String}]
        },
    },
    lotto : {
        amb : {
            betId: {type: String, required: false},
            gameId: {type: String, required: false},
            type: { type: String },
            txId: { type: String },
            betTime: {type: Date,required: false},
            betType:{type: String,enum: ['_1TOP','_1BOT','_2TOP','_2BOT','_2TOD','_2TOP_OE','_2TOP_OU',
                '_2BOT_OE','_2BOT_OU',
                '_3TOP','_3BOT','_3TOD','_3TOP_OE','_3TOP_OU',
                '_4TOP','_4TOD','_5TOP','_6TOP']},
            betNumber: { type: String },
            result: { type: String },
            betAmount: {type: Number, required: false},
            payout: {type: Number},
            winLose: {type: Number},
        },
        laos : {
            betId: {type: String, required: false},
            gameId: {type: String, required: false},
            type: { type: String },
            txId: { type: String },
            betTime: {type: Date,required: false},
            betType:{type: String,enum: ['L_4TOP','L_4TOD','L_3TOP','L_3TOD','L_2FB'
                ,'T_4TOP', 'T_4TOD','T_3TOP', 'T_3TOD', 'T_2TOP', 'T_2TOD', 'T_2BOT',
                'T_1TOP', 'T_1BOT']},
            betNumber: { type: String },
            result: { type: String },
            betAmount: {type: Number, required: false},
            payout: {type: Number},
            winLose: {type: Number},
            payoutLaos:[{
                superAdmin:[{type: Number}],
                company:[{type: Number}],
                shareHolder:[{type: Number}],
                senior:[{type: Number}],
                masterAgent:[{type: Number}],
                agent:[{type: Number}],
                member:[{type: Number}]
            }]
        },
        pp : {
            betId: {type: String, required: false},
            gameId: {type: String, required: false},
            type: { type: String },
            txId: { type: String },
            betTime: {type: Date,required: false},
            betType:{type: String,enum: ['_1TOP','_1BOT','_2TOP','_2BOT','_2TOD','_2TOP_OE','_2TOP_OU',
                '_2BOT_OE','_2BOT_OU',
                '_3TOP','_3BOT','_3TOD','_3TOP_OE','_3TOP_OU',
                '_4TOP','_4TOD','_5TOP','_6TOP']},
            betNumber: { type: String },
            result: { type: String },
            betAmount: {type: Number, required: false},
            payout: {type: Number},
            winLose: {type: Number},
        },
    },
    amount: {type: Number, required: true},
    memberCredit: {type: Number, required: true},
    validAmount: {type: Number, required: false},
    payout: {type: Number, required: true},
    gameType : {type: String,enum: ['TODAY','PARLAY', "MIX_STEP"],required: true},
    acceptHigherPrice: {type: Boolean, default: true},
    priceType: {type: String,enum: ['HK','MY','ID','EU','TH'],required: true},
    currency: {type: String, enum: ['THB','MYR','HDK','CNY','IDR','USD']},
    commission : {
        superAdmin:{
            parentGroup:{type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
            group:{type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
            parent: {type: Number},
            own: {type: Number},
            remaining: {type: Number},
            min: {type: Number},
            shareReceive: {type: Number},
            commission:{type: Number, required: false},
            amount:{type: Number, required: false},
            payout:{type: Number, required: false},
            winLose: {type: Number, required: false},
            winLoseCom: {type: Number, required: false},
            totalWinLoseCom: {type: Number, required: false},
        },
        company:{
            parentGroup:{type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
            group:{type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
            parent: {type: Number},
            own: {type: Number},
            remaining: {type: Number},
            min: {type: Number},
            shareReceive: {type: Number},
            commission:{type: Number, required: false},
            amount:{type: Number, required: false},
            payout:{type: Number, required: false},
            winLose: {type: Number, required: false},
            winLoseCom: {type: Number, required: false},
            totalWinLoseCom: {type: Number, required: false},
        },
        shareHolder:{
            parentGroup:{type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
            group:{type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
            parent: {type: Number},
            own: {type: Number},
            remaining: {type: Number},
            min: {type: Number},
            shareReceive: {type: Number},
            commission:{type: Number, required: false},
            amount:{type: Number, required: false},
            payout:{type: Number, required: false},
            winLose: {type: Number, required: false},
            winLoseCom: {type: Number, required: false},
            totalWinLoseCom: {type: Number, required: false},
        },
        api:{
            parentGroup:{type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
            group:{type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
            parent: {type: Number},
            own: {type: Number},
            remaining: {type: Number},
            min: {type: Number},
            shareReceive: {type: Number},
            commission:{type: Number, required: false},
            amount:{type: Number, required: false},
            payout:{type: Number, required: false},
            winLose: {type: Number, required: false},
            winLoseCom: {type: Number, required: false},
            totalWinLoseCom: {type: Number, required: false},
        },
        senior:{
            parentGroup:{type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
            group:{type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
            parent: {type: Number},
            own: {type: Number},
            remaining: {type: Number},
            min: {type: Number},
            shareReceive: {type: Number},
            commission:{type: Number, required: false},
            amount:{type: Number, required: false},
            payout:{type: Number, required: false},
            winLose: {type: Number, required: false},
            winLoseCom: {type: Number, required: false},
            totalWinLoseCom: {type: Number, required: false},
        },
        masterAgent:{
            parentGroup:{type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
            group:{type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
            parent: {type: Number},
            own: {type: Number},
            remaining: {type: Number},
            min: {type: Number},
            shareReceive: {type: Number},
            commission:{type: Number, required: false},
            amount:{type: Number, required: false},
            payout:{type: Number, required: false},
            winLose: {type: Number, required: false},
            winLoseCom: {type: Number, required: false},
            totalWinLoseCom: {type: Number, required: false},
        },
        agent:{
            parentGroup:{type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
            group:{type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
            parent: {type: Number},
            own: {type: Number},
            remaining: {type: Number},
            min: {type: Number},
            shareReceive: {type: Number},
            commission:{type: Number, required: false},
            amount:{type: Number, required: false},
            payout:{type: Number, required: false},
            winLose: {type: Number, required: false},
            winLoseCom: {type: Number, required: false},
            totalWinLoseCom: {type: Number, required: false},
        },
        member:{
            // group:{type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
            parent: {type: Number},
            commission:{type: Number, required: false},
            amount:{type: Number, required: false},
            payout:{type: Number, required: false},
            winLose: {type: Number, required: false},
            winLoseCom: {type: Number, required: false},
            totalWinLoseCom: {type: Number, required: false},
        }
    },
    active: {type: Boolean, default: true},
    game:{type: String,enum: ['FOOTBALL','CASINO','GAME','LOTTO','M2','MULTI_PLAYER'],required: true},
    source:{type: String,enum: ['FOOTBALL','BASKETBALL','SEXY_BACCARAT','SA_GAME','AG_GAME','LOTUS','DREAM_GAME','SLOT_XO','GANAPATI','LIVE22','AMEBA','SPADE_GAME','AMB_GAME','DS_GAME','PRETTY_GAME', "PG_SLOT",
        'AMB_LOTTO','AMB_LOTTO_LAOS','AMB_LOTTO_PP','ALL_BET', 'MUAY_THAI', "MIX_STEP", 'TENNIS', 'ESPORT','M2_MUAY', 'TABLE_TENNIS', 'VOLLEYBALL', 'RUGBY', "AMERICAN_FOOTBALL", "CRICKET", "CYCLING", "DARTS"],required: false},
    status:{type: String,enum: ['DONE','RUNNING','WAITING', 'REJECTED','CANCELLED'],required: true},
    remark: { type: String },
    isEndScore:{type: Boolean,required: true,default:false},
    betResult:{type: String,enum: ['WIN','HALF_WIN','LOSE','HALF_LOSE','DRAW'],required: false},
    gameDate: {type: Date,required: true},
    createdDate: {type: Date, default: Date.now},
    updatedDate: {type: Date},
    ipAddress:{type:String},
    isPrinted:{ type: Boolean, default:false },
    cancelByType:{type: String,enum: ['AGENT','MEMBER','API', 'PARTNER'],required: false},
    cancelByAgent : {type: String, ref: 'User', default: null},
    cancelDate:{type: Date},
    isOfflineCash:{ type: Boolean, default:false },
    ref1: {type: String, required: false},
    settleDate: {type: Date, required: false},
    codeOfflineTicket: {type: String, required: false},
    isOfflineCashSuccess:{ type: Boolean, default:false },
    shortBetKey:{type:String},
    queueUpdateCount:{type: Number, default: 0},
    queueLastUpdate : {type: Date}
});


schema.pre('save', function (next) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.updatedDate) {
        this.updatedDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);

module.exports = schema;