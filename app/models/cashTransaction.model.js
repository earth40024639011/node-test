const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');


const schema = new mongoose.Schema({
        ref: {type: String, unique: true, required: true},
        agent:{type: Schema.Types.ObjectId, ref: 'AgentGroup', default: null},
        memberId: {type: Schema.Types.ObjectId, ref: 'Member', default: null},
        member_name: {type: String},
        transferType: {type: String,enum: ['ATM','IB','MB','KIOSK','CS', 'API_PARTNER'], required: false},
        fromBank : {type: Schema.Types.ObjectId, ref: 'Bank'},
        fromBankAccountName :{ type: String, required: false},
        fromBankAccountNumber: { type: String, required: false},
        processDateTime:{type: Date,required: true},
        amount:{ type: Number, required: true},
        toAgentBankId : {type: Schema.Types.ObjectId, ref: 'AgentBank'},
        status: {type: String,enum: ['WAITING_CHECKING','WAITING_ADD_CREDIT','WAITING_TRANSFER','COMPLETE','REJECTED'], required: true},
        checkBy : {type: Schema.Types.ObjectId, ref: 'User', default: null},
        checkDate:{type: Date},
        addCreditBy : {type: Schema.Types.ObjectId, ref: 'User', default: null},
        addCreditDate:{type: Date},
        transferBy : {type: Schema.Types.ObjectId, ref: 'User', default: null},
        transferDate:{type: Date},
        cancelBy : {type: Schema.Types.ObjectId, ref: 'User', default: null},
        cancelDate:{type: Date},
        active:{type:Boolean,default:true},
        tranType :  {type: String,enum: ['DEPOSIT','WITHDRAW'], required: true},
        type:{type:String,enum: ['MANUAL','CASH', 'AUTO']},
        modifyDate:{type: Date},
        createdDate:{type: Date, default: Date.now},
        depositSlip:{ type: String, required: false }
});

// schema.index({ ref: 1, status: 1 }, { unique: true });

schema.pre('save', function (next) {
    var now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    if (!this.createdDate) {
        this.createdDate = now;
    }
    next();
});


schema.plugin(mongoosePaginate);

module.exports = mongoose.model('CashTransaction', schema);