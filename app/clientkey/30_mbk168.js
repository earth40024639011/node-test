const client = {

    //---------------//
    LICENSE: '5e3041183173f51a5311da16:5e3041873173f51a5311da1b:5e3046db9d25241aa26d15ac:5e30476751c2e11a98cd38cd:5e3047cd1fe2d31a9294d4a5:5e6e8117f203701a1130d426:5e6e82405883f91a00d9752e:5e6f26ac4566e01a4fb46fdb:5e7b13408bb291343e6e605f',
    ODD_ADJUSTMENT: 2,
    MAX_BACK_LIST: 40000000,
    MASTER_IP: 'isn-backend.api-hub.com',

    //AMBBO (Auto)
    URL_AMBBO: 'https://ambbo.co/login/auto',
    URL_ENCRYPTED: 'https://ambbet-topup.serverless-hub.com/secure/encrypted',
    URL_PAYMENT_GATEWAY_UPDATE_PASSWORD: 'http://topup-casino.api-hub.com:22110/external/customer/update-password',

    //M2 Sport
    MUAY_STEP2_HOST: 'http://www.muaystep2.com',
    MUAY_STEP2_OPCODE: 'mbk168sgw',
    MUAY_STEP2_PRIVATE_KEY: 'MuaY18',
    MUAY_STEP2_OPAGENT_ID: 'mbk168sgwagent',
    MUAY_STEP2_PREFIX: 'mbk168sgw_',

    //Pretty Gaming  *****
    PRETTY_API_URL: 'https://api-prod.aghippo168.com/apiRoute/member/loginRequest',
    PRETTY_SECRET_KEY: '4d367851-d545-3f59-a5a6-c11a6cbd38b3',
    PRETTY_AGENT: 'mbk168',

    //Sexy bacara
    SEXY_HOST: 'https://api.onlinegames22.com',
    SEXY_AGENT_ID: 'mbk168',
    SEXY_CERT: 'iqTdDqIdFQus0kTG6Mj',

    //SA Game
    SAG_URL: 'http://api.sa-apisvr.net/api/api.aspx',
    SAG_LOGIN_URL: 'https://mbk.sa-api7.net/app.aspx',
    SAG_SECRET_KEY: '9A8177D378364A4DA610F0B0E29418F3',
    SAG_ENCRYPT_KEY: 'g9G16nTs',
    SAG_MD5_KEY: 'GgaIMaiNNtg',
    SAG_LOBBY_CODE: 'A2253',

    //AG Game
    AG_LOGIN_URL: 'EB7_AGIN',
    AG_ENCRYPT_KEY: '7rPaAz8C',
    AG_MD5_KEY: 'HX4vtEcTaFHd',
    AG_PASS: 'aggpass123',
    AG_URL: 'https://gi.serverless-hub.com/',
    CREATE_SESSION_URL: 'http://swapi.agingames.com:3381/',
    FORWARD_GAME_URL: 'https://gci.serverless-hub.com/',

    //DG Game
    DG_API_URL: 'https://api.dg99web.com',
    DG_API_KEY: 'b6b64a2f37f14fe58a4d9b2fc875777b',
    DG_AGENT_NAME: 'DG10040115',

    //Dragon Soft
    DS_API_URL: 'https://api.sysonline.club',
    DS_GAME_URL: 'https://link.sandsys.pw',
    DS_AGENT_ACCOUNT: 'AMB1028THB',
    DS_CHANNEL: '50894494',
    DS_CERT: 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDEXKEuRNJDt0Pe5BxmWuJDi+qa69qozqmHkznDivasoFypK8rm3QZctuTLCqKrYmd0ELbfe2VqXKnfPIaj18F8upILXMCPcDBDW4zevenP4hfCJ6wQujNDU5ljvSE/+MXwleBOMmMf+PqIpZs7doAmiu3CL97sfpNOp3KPYcLmTwIDAQAB',
    DS_IMAGE_URL: 'https://amb-slot.s3-ap-southeast-1.amazonaws.com/ds-game',

    //PGSlot   *****
    PGSOFT_IMAGE_URL: 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/pg-soft',
    PG_URL: 'https://m.pgjksonc.club/',
    PG_TOKEN: 'F8BDACB7-7F03-4253-A109-C614E7AADEE9',
    PG_SECRETKEY: '237DE5B252C34FFF9B5F659CD040F9D4',

    //Ameba   ****
    AMEBA_URL: 'https://api.fafafa3388.com',
    AMEBA_SITE_ID: '2805',
    AMEBA_SECRET_KEY: 'vLcTuVM1PGARJdhxYV4CGazJHosw+W8T',
    AMEBA_IMAGE_URL: 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/ameba-game',

    //SlotXO
    SLOT_XO_FORWARD_GAME: 'http://www.gwc688.net/playGame?',
    SLOT_XO_HOST: 'http://api688.net/seamless',
    SLOT_XO_SECRET_KEY: 'xeyustzp4g3ao',
    SLOT_XO_APP_ID: 'F29H',

    //Ganapati
    GANAPATI_URL: 'https://api.ayitang.com/',
    GANAPATI_SECRET_KEY: 'K5XRSXYeZCrpkrvuPVfZdc9fg',
    GANAPATI_AGENT_NAME: 'askmebet',
    GANAPATI_AGENT_CODE: 'mbk168',

    //Live22
    GAMING_SOFT_URL: 'https://cw.889tech.net/CommonWalletOPWS.svc/OPAPI/',
    GAMING_SOFT_OPERATOR_ID: 'mbk168THB',
    GAMING_SOFT_SECRET: 'B063',

    //Spade Slot  
    SPADE_LOGIN_URL: 'http://lobby.bigmoose88.com/MBK168/auth',
    SPADE_API_URL: 'http://merchantapi.hugedolphin.com/api',
    SPADE_MERCHANT_CODE: 'MBK168',

    //AMB Game Card  
    AMB_GAME_API: 'https://api.ambgames.com',
    AMB_GAME_PARTNER: '5e424ca457a006008d7e3b26',
    AMB_GAME_GAME_URL: 'https://games.ambpoker.com',

    //Lotto  
    AMB_LOTTO_URL: 'https://amblotto.api-hub.com/',
    AMB_LOTTO_CLIENT: 'mbk168',
    AMB_LOTTO_API_KEY: '51ed042a-d0cc-4779-9608-59a0b6c71fc0',

    AMB_GAME_API_2: 'https://ap.ambpoker-api.com',
    AMB_GAME_SECRET_KEY_2: '75eafb6d4f35238e552fb8a181716c73',
    AMB_GAME_AGENT_2: 'mbk'

}


module.exports = client;