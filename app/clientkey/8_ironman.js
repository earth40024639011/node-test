const client = {

    //---------------//
    LICENSE: '5ca82660e2bc3b1acdef8b97:5cdf59039b46bc1756c5017e:5dda297318598b40397afba7:5e2bf09b8b8ff729abf97fc5',
    ODD_ADJUSTMENT: 2,
    MAX_BACK_LIST: 40000000,
    MASTER_IP: 'isn-backend.api-hub.com',

    //AMBBO (Auto)
    URL_AMBBO: 'https://ambbo.co/login/auto',
    URL_ENCRYPTED: 'https://ambbet-topup.serverless-hub.com/secure/encrypted',
    URL_PAYMENT_GATEWAY_UPDATE_PASSWORD: 'http://topup-casino.api-hub.com:22110/external/customer/update-password',

    //M2 Sport
    MUAY_STEP2_HOST: 'http://www.muaystep2.com',
    MUAY_STEP2_OPCODE: 'ironbet',
    MUAY_STEP2_PRIVATE_KEY: 'MuaY18',
    MUAY_STEP2_OPAGENT_ID: 'ironbetagent',
    MUAY_STEP2_PREFIX: 'ironbet_',

    //Pretty Gaming
    PRETTY_API_URL: 'https://api-prod.aghippo168.com/apiRoute/member/loginRequest',
    PRETTY_SECRET_KEY: '938dc25b-987a-433b-fb0a-f8d8e384b685',
    PRETTY_AGENT: 'ironman',

    //Sexy bacara
    SEXY_HOST: 'https://api.onlinegames22.com',
    SEXY_AGENT_ID: 'ironmanbet',
    SEXY_CERT: 'F7DlyYlYqK3cO5t3k46',

    //SA Game
    SAG_URL: 'http://api.sa-apisvr.net/api/api.aspx',
    SAG_LOGIN_URL: 'https://irm.sa-api3.net/app.aspx',
    SAG_SECRET_KEY: '9CF98013E47D4100BF7629CB190DACFE',
    SAG_ENCRYPT_KEY: 'g9G16nTs',
    SAG_MD5_KEY: 'GgaIMaiNNtg',
    SAG_LOBBY_CODE: 'A935',

    //AG Game
    AG_LOGIN_URL: 'EB7_AGIN',
    AG_ENCRYPT_KEY: '7rPaAz8C',
    AG_MD5_KEY: 'HX4vtEcTaFHd',
    AG_PASS: 'aggpass123',
    AG_URL: 'https://gi.serverless-hub.com/',
    CREATE_SESSION_URL: 'http://swapi.agingames.com:3381/',
    FORWARD_GAME_URL: 'https://gci.serverless-hub.com/',

    //DG Game
    DG_API_URL: 'https://api.dg99web.com',
    DG_API_KEY: 'a424e907f9334c47917c464d22047a4e',
    DG_AGENT_NAME: 'DG04020807',

    //Dragoon Soft
    DS_API_URL: 'https://api.sysonline.club',
    DS_GAME_URL: 'https://link.sandsys.pw',
    DS_AGENT_ACCOUNT: 'AMB1007THB',
    DS_CHANNEL: '57139685',
    DS_CERT: 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDBDQH38+NHjVc8MriWT1bsCOJe3IcG12ALqzbyDpedv4z+9QKZWI6NL39AYGbaRLwSrmr9KArIlGT8Chcpnp4bFABqyyYdO59HsaYqBHwq4Ossx+f0rPIEpmmTSsQXiRAcjrye7srWV799WPhXvWZf/PQIZu0zmuukeowhY3Ub5QIDAQAB',
    DS_IMAGE_URL: 'https://amb-slot.s3-ap-southeast-1.amazonaws.com/ds-game',

    //PGSlot
    PGSOFT_IMAGE_URL: 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/pg-soft',
    PG_URL: 'https://m.pgjksonc.club/',
    PG_TOKEN: '1126FE0E-95A7-46C5-900E-1FE875264798',
    PG_SECRETKEY: 'FD8E5FAAFCFF4D4BBFF0E4A83239A3B0',

    //Ameba
    AMEBA_URL: 'https://api.fafafa3388.com',
    AMEBA_SITE_ID: '2244',
    AMEBA_SECRET_KEY: '5Mzikl+EEKjFap/e3r4pBTliTeGRaMU7',
    AMEBA_IMAGE_URL: 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/ameba-game',

    //SlotXO
    SLOT_XO_FORWARD_GAME: 'http://www.gwc688.net/playGame?',
    SLOT_XO_HOST: 'http://api688.net/seamless',
    SLOT_XO_SECRET_KEY: 'qc8rqfuif6p14',
    SLOT_XO_APP_ID: 'F1YT',

    //Ganapati
    GANAPATI_URL: 'https://api.ayitang.com/',
    GANAPATI_SECRET_KEY: 'K5XRSXYeZCrpkrvuPVfZdc9fg',
    GANAPATI_AGENT_NAME: 'askmebet',
    GANAPATI_AGENT_CODE: 'ironman',

    //Live22
    GAMING_SOFT_URL: 'https://cw.889tech.net/CommonWalletOPWS.svc/OPAPI/',
    GAMING_SOFT_OPERATOR_ID: 'ironmanbetTHB',
    GAMING_SOFT_SECRET: '843C',

    //Spade Slot
    SPADE_LOGIN_URL: 'http://lobby.bigmoose88.com/ironman/auth',
    SPADE_API_URL: 'http://merchantapi.hugedolphin.com/api',
    SPADE_MERCHANT_CODE: 'IRONMAN',

    //AMB Game Card
    AMB_GAME_API: 'https://api.ambgames.com',
    AMB_GAME_PARTNER: '5d5431f74b98e600109c12b4',
    AMB_GAME_GAME_URL: 'https://games.ambpoker.com',

    //Lotto
    AMB_LOTTO_URL: 'https://amblotto.api-hub.com/',
    AMB_LOTTO_CLIENT: 'ironmanbet',
    AMB_LOTTO_API_KEY: 'f28da5f1-9d65-4d70-826f-bb2ff60989a3',

    AMB_GAME_API_2: 'https://ap.ambpoker-api.com',
    AMB_GAME_SECRET_KEY_2: 'e53dda244e74d4e52c8c6c74cace6f30',
    AMB_GAME_AGENT_2: 'iro'

}


module.exports = client;