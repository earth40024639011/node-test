const client = {

    //---------------//
    LICENSE: '5bf52116e60f036bf68df87b:5c45c2ed362d6a6ca9b25515:5de00fcbe55573755aee06f7:5df7aabf33f17e2348941039:5dfdf0873af52c5fef613e93:5e312bfa22c1dd4e02c4f019',
    ODD_ADJUSTMENT: 2,
    MAX_BACK_LIST: 40000000,
    MASTER_IP: 'isn-backend.api-hub.com',

    //AMBBO (Auto)
    URL_AMBBO: 'https://ambbo.co/login/auto',
    URL_ENCRYPTED: 'https://ambbet-topup.serverless-hub.com/secure/encrypted',
    URL_PAYMENT_GATEWAY_UPDATE_PASSWORD: 'http://topup-casino.api-hub.com:22110/external/customer/update-password',

    //M2 Sport
    MUAY_STEP2_HOST: 'http://www.muaystep2.com',
    MUAY_STEP2_OPCODE: 'ambsgw',
    MUAY_STEP2_PRIVATE_KEY: 'MuaY18',
    MUAY_STEP2_OPAGENT_ID: 'ambsgwagent',
    MUAY_STEP2_PREFIX: 'ambsgw_',

    // //Pretty Gaming
    // PRETTY_API_URL: 'https://production-pt-gaming-api.secure-restapi.com/apirouting.aspx',
    // PRETTY_SECRET_KEY: '6B195B8A671C997D',
    // PRETTY_AGENT: 'amb',

    //Pretty Gaming
    PRETTY_API_URL: 'https://api-prod.aghippo168.com/apiRoute/member/loginRequest',
    PRETTY_SECRET_KEY: '57cc0202-aa6d-393f-05d0-4ad51b751600',
    PRETTY_AGENT: 'ambbet',

    //Sexy bacara
    SEXY_HOST: 'https://api.onlinegames22.com',
    SEXY_AGENT_ID: 'ambbet',
    SEXY_CERT: 'jofYdyzWMNh0gDxyUvu',

    //SA Game
    SAG_URL: 'http://api.sa-apisvr.net/api/api.aspx',
    SAG_LOGIN_URL: 'https://amb.sa-api3.net/app.aspx',
    SAG_SECRET_KEY: '6313C1813CC24BDE998FACBFDE72101D',
    SAG_ENCRYPT_KEY: 'g9G16nTs',
    SAG_MD5_KEY: 'GgaIMaiNNtg',
    SAG_LOBBY_CODE: 'A886',

    //AG Game
    AG_LOGIN_URL: 'EB7_AGIN',
    AG_ENCRYPT_KEY: '7rPaAz8C',
    AG_MD5_KEY: 'HX4vtEcTaFHd',
    AG_PASS: 'aggpass123',
    AG_URL: 'https://gi.serverless-hub.com/',
    CREATE_SESSION_URL: 'http://swapi.agingames.com:3381/',
    FORWARD_GAME_URL: 'https://gci.serverless-hub.com/',

    //DG Game
    DG_API_URL: 'https://api.dg99web.com',
    DG_API_KEY: 'f5a578d2314745d697fae6219a3d7eeb',
    DG_AGENT_NAME: 'DG04020801',

    //Dragoon Soft
    DS_API_URL: 'https://api.sysonline.club',
    DS_GAME_URL: 'https://link.sandsys.pw',
    DS_AGENT_ACCOUNT: 'AMB1001THB',
    DS_CHANNEL: '21084261',
    DS_CERT: 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCaABZqjlxbTV7DzM4PFo2Vm0Wntw0XtGqTQvxv9/kukRXnLIhbQLu7V91+UCjlgCnxESmX2MgU6i32XtrFU1ZWq0K9DykvX/hs4HAeq9HY9xxLqF9mAGfZaMWKnSgjfZ1xIAm6HUqzpCbNdOwYEUfduX05cCLlztB1yaIFnQYqHwIDAQAB',
    DS_IMAGE_URL: 'https://amb-slot.s3-ap-southeast-1.amazonaws.com/ds-game',

    //PGSlot
    PGSOFT_IMAGE_URL: 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/pg-soft',
    PG_URL: 'https://m.pgjksonc.club/',
    PG_TOKEN: '8BACA837-341A-4460-B4A9-C6A755F6D9E6',
    PG_SECRETKEY: '659E157EA0914EECBE17B0F800FD9734',

    //Ameba
    AMEBA_URL: 'https://api.fafafa3388.com',
    AMEBA_SITE_ID: '2046',
    AMEBA_SECRET_KEY: 'yl0hfDRScpizzfUAKros49pf4s+VmnAK',
    AMEBA_IMAGE_URL: 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/ameba-game',

    //SlotXO
    SLOT_XO_FORWARD_GAME: 'http://www.gwc688.net/playGame?',
    SLOT_XO_HOST: 'http://api688.net/seamless',
    SLOT_XO_SECRET_KEY: '44s6igsato9ew',
    SLOT_XO_APP_ID: 'F1YS',

    //Ganapati
    GANAPATI_URL: 'https://api.ayitang.com/',
    GANAPATI_SECRET_KEY: '6B195B8A671C997D',
    GANAPATI_AGENT_NAME: 'askmebet',
    GANAPATI_AGENT_CODE: 'ambbet',

    //Live22
    GAMING_SOFT_URL: 'https://cw.889tech.net/CommonWalletOPWS.svc/OPAPI/',
    GAMING_SOFT_OPERATOR_ID: 'ambbetTHB',
    GAMING_SOFT_SECRET: '9C30',

    //Spade Slot
    SPADE_LOGIN_URL: 'http://lobby.bigmoose88.com/ambbet/auth',
    SPADE_API_URL: 'http://merchantapi.hugedolphin.com/api',
    SPADE_MERCHANT_CODE: 'AMBBET',

    //AMB Game Card
    AMB_GAME_API: 'https://api.ambgames.com',
    AMB_GAME_PARTNER: '5d26182ee4e8ab00489dbbff',
    AMB_GAME_GAME_URL: 'https://games.ambpoker.com',

    //Lotto
    AMB_LOTTO_URL: 'https://amblotto.api-hub.com/',
    AMB_LOTTO_CLIENT: 'ambth',
    AMB_LOTTO_API_KEY: '9c8eb5c6-75bb-4b16-b0b5-95a4faa76552',

    AMB_GAME_API_2: 'https://ap.ambpoker-api.com',
    AMB_GAME_SECRET_KEY_2: '4311a26d7c522cdc7a6d5292033252bc',
    AMB_GAME_AGENT_2: 'amb'

}


module.exports = client;