const client = {

    //---------------//
    LICENSE: '5e3041183173f51a5311da16:5e3041873173f51a5311da1b:5e3046db9d25241aa26d15ac:5e30476751c2e11a98cd38cd:5e3047cd1fe2d31a9294d4a5:5e6e8117f203701a1130d426:5e6e82405883f91a00d9752e:5e6f26ac4566e01a4fb46fdb:5e7b13408bb291343e6e605f',
    ODD_ADJUSTMENT: 2,
    MAX_BACK_LIST: 40000000,
    MASTER_IP: 'isn-backend.api-hub.com',

    //AMBBO (Auto)
    URL_AMBBO: 'https://ambbo.co/login/auto',
    URL_ENCRYPTED: 'https://ambbet-topup.serverless-hub.com/secure/encrypted',
    URL_PAYMENT_GATEWAY_UPDATE_PASSWORD: 'http://topup-casino.api-hub.com:22110/external/customer/update-password',

    //M2 Sport
    MUAY_STEP2_HOST: 'http://www.muaystep2.com',
    MUAY_STEP2_OPCODE: 'sandsgw',
    MUAY_STEP2_PRIVATE_KEY: 'MuaY18',
    MUAY_STEP2_OPAGENT_ID: 'sandsgwagent',
    MUAY_STEP2_PREFIX: 'sandsgw_',

    //Pretty Gaming  *****
    PRETTY_API_URL: 'https://api-prod.aghippo168.com/apiRoute/member/loginRequest',
    PRETTY_SECRET_KEY: 'd66d95b0-c8e5-b93c-0942-a31b39ef4a17',
    PRETTY_AGENT: 'sand1988',

    //Sexy bacara
    SEXY_HOST: 'https://api.onlinegames22.com',
    SEXY_AGENT_ID: 'sand1988',
    SEXY_CERT: 'JNiCSQJsr7y0sYjjPIX',

    //SA Game
    SAG_URL: 'http://api.sa-apisvr.net/api/api.aspx',
    SAG_LOGIN_URL: 'https://sd8.sa-api7.net/app.aspx',
    SAG_SECRET_KEY: 'C1DE0272F71C4DE28733F7DE810936A4',
    SAG_ENCRYPT_KEY: 'g9G16nTs',
    SAG_MD5_KEY: 'GgaIMaiNNtg',
    SAG_LOBBY_CODE: 'A2121',

    //AG Game
    AG_LOGIN_URL: 'EB7_AGIN',
    AG_ENCRYPT_KEY: '7rPaAz8C',
    AG_MD5_KEY: 'HX4vtEcTaFHd',
    AG_PASS: 'aggpass123',
    AG_URL: 'https://gi.serverless-hub.com/',
    CREATE_SESSION_URL: 'http://swapi.agingames.com:3381/',
    FORWARD_GAME_URL: 'https://gci.serverless-hub.com/',

    //DG Game
    DG_API_URL: 'https://api.dg99web.com',
    DG_API_KEY: '0f589ffe9dc44d5c86443c7015b5df79',
    DG_AGENT_NAME: 'DG10040110',

    //Dragon Soft
    DS_API_URL: 'https://api.sysonline.club',
    DS_GAME_URL: 'https://link.sandsys.pw',
    DS_AGENT_ACCOUNT: 'AMB1025THB',
    DS_CHANNEL: '74954175',
    DS_CERT: 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC16y8NEg4UehxdDeJXBh27Kg+u8+cx+08ZjqJNKQjhuNInfycsB64LaP88zUDBYCDUrHmcrNzuDdt3wryZtWlf/9vMz4yu201cHDL5JbVhTChndltd+OiFgwQEaa+Aed4QBk2cbuOwulZYnBNeZTZ1CajKuJoEt0RhqRhBBhDqiQIDAQAB',
    DS_IMAGE_URL: 'https://amb-slot.s3-ap-southeast-1.amazonaws.com/ds-game',

    //PGSlot   *****
    PGSOFT_IMAGE_URL: 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/pg-soft',
    PG_URL: 'https://m.pgjksonc.club/',
    PG_TOKEN: '9B51482A-87D7-4D93-AC58-69A521E251EE',
    PG_SECRETKEY: '3C876EC4646D4A0F88BBD7A096AC7980',

    //Ameba   ****
    AMEBA_URL: 'https://api.fafafa3388.com',
    AMEBA_SITE_ID: '2752',
    AMEBA_SECRET_KEY: 'fAlHCySiM6i/5GfvJPp75cFlceG72Dfu',
    AMEBA_IMAGE_URL: 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/ameba-game',

    //SlotXO
    SLOT_XO_FORWARD_GAME: 'http://www.gwc688.net/playGame?',
    SLOT_XO_HOST: 'http://api688.net/seamless',
    SLOT_XO_SECRET_KEY: 'xeyopnyetpreo',
    SLOT_XO_APP_ID: 'F25R',

    //Ganapati
    GANAPATI_URL: 'https://api.ayitang.com/',
    GANAPATI_SECRET_KEY: 'K5XRSXYeZCrpkrvuPVfZdc9fg',
    GANAPATI_AGENT_NAME: 'askmebet',
    GANAPATI_AGENT_CODE: 'sand',

    //Live22
    GAMING_SOFT_URL: 'https://cw.889tech.net/CommonWalletOPWS.svc/OPAPI/',
    GAMING_SOFT_OPERATOR_ID: 'sand1988THB',
    GAMING_SOFT_SECRET: 'A14C',

    //Spade Slot  
    SPADE_LOGIN_URL: 'http://lobby.bigmoose88.com/SAND198/auth',
    SPADE_API_URL: 'http://merchantapi.hugedolphin.com/api',
    SPADE_MERCHANT_CODE: 'SAND198',

    //AMB Game Card  
    AMB_GAME_API: 'https://api.ambgames.com',
    AMB_GAME_PARTNER: '5df8a056ec50fc00201b443d',
    AMB_GAME_GAME_URL: 'https://games.ambpoker.com',

    //Lotto  
    AMB_LOTTO_URL: 'https://amblotto.api-hub.com/',
    AMB_LOTTO_CLIENT: 'sand1988',
    AMB_LOTTO_API_KEY: '2b52b000-805f-4917-acb7-1ce92360e07e',


    AMB_GAME_API_2: 'https://ap.ambpoker-api.com',
    AMB_GAME_SECRET_KEY_2: '22be14f5927587f0757de95ce3025847',
    AMB_GAME_AGENT_2: 'sand'

}


module.exports = client;