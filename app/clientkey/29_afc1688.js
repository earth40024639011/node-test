const client = {

    //---------------//
    LICENSE: '5e3041183173f51a5311da16:5e3041873173f51a5311da1b:5e3046db9d25241aa26d15ac:5e30476751c2e11a98cd38cd:5e3047cd1fe2d31a9294d4a5:5e6e8117f203701a1130d426:5e6e82405883f91a00d9752e:5e6f26ac4566e01a4fb46fdb:5e7b13408bb291343e6e605f:5eaad2c39595091aa21ff27d:5e9d5e5deaf5cf70b6a189c5:5e9d8625681bab70b028aecf:5e7b13408bb291343e6e605f:5ec67027fed9ed34033a507f',
    ODD_ADJUSTMENT: 2,
    MAX_BACK_LIST: 40000000,
    MASTER_IP: 'isn-backend.api-hub.com',

    //AMBBO (Auto)
    URL_AMBBO: 'https://ambbo.co/login/auto',
    URL_ENCRYPTED: 'https://ambbet-topup.serverless-hub.com/secure/encrypted',
    URL_PAYMENT_GATEWAY_UPDATE_PASSWORD: 'http://topup-casino.api-hub.com:22110/external/customer/update-password',

    //M2 Sport
    MUAY_STEP2_HOST: 'http://www.muaystep2.com',
    MUAY_STEP2_OPCODE: 'afc1688sgw',
    MUAY_STEP2_PRIVATE_KEY: 'MuaY18',
    MUAY_STEP2_OPAGENT_ID: 'afc1688sgwagent',
    MUAY_STEP2_PREFIX: 'afc1688sgw_',

    //Pretty Gaming
    PRETTY_API_URL: 'https://api-prod.aghippo168.com/apiRoute/member/loginRequest',
    PRETTY_SECRET_KEY: '468835fb-69d5-66ee-7c81-8e2bf02383a1',
    PRETTY_AGENT: 'afc1688',

    //Sexy bacara
    SEXY_HOST: 'https://api.onlinegames22.com',
    SEXY_AGENT_ID: 'afc1688',
    SEXY_CERT: 'Kl0hNGzyx3LVMuPtMVo',

    //SA Game
    SAG_URL: 'http://api.sa-apisvr.net/api/api.aspx',
    SAG_LOGIN_URL: 'https://afc.sa-api7.net/app.aspx',
    SAG_SECRET_KEY: 'A9283CBBDB3841FE91734F95E323E008',
    SAG_ENCRYPT_KEY: 'g9G16nTs',
    SAG_MD5_KEY: 'GgaIMaiNNtg',
    SAG_LOBBY_CODE: 'A2178',

    //AG Game
    AG_LOGIN_URL: 'EB7_AGIN',
    AG_ENCRYPT_KEY: '7rPaAz8C',
    AG_MD5_KEY: 'HX4vtEcTaFHd',
    AG_PASS: 'aggpass123',
    AG_URL: 'https://gi.serverless-hub.com/',
    CREATE_SESSION_URL: 'http://swapi.agingames.com:3381/',
    FORWARD_GAME_URL: 'https://gci.serverless-hub.com/',

    //DG Game
    DG_API_URL: 'https://api.dg99web.com',
    DG_API_KEY: 'c6a242377beb4fa9b410f535097f4ba9',
    DG_AGENT_NAME: 'DG10040114',

    //Dragoon Soft
    DS_API_URL: 'https://api.sysonline.club',
    DS_GAME_URL: 'https://link.sandsys.pw',
    DS_AGENT_ACCOUNT: 'AMB1027THB',
    DS_CHANNEL: '56334378',
    DS_CERT: 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDM7kXbGJW8vnUjSmimXuDtcnYdottMQ7UA/8YwT3cFRkkh+SGXV9YTZXd34B5r5PLYUEaqk78DV7u5TyjFhXxPCZp1M9DgsC/Q+ZDppAkvCrbtAG69NVCIsBYw9XR0zvwoIxa/gBew5iXkZ766uo9PL+zG8Yx13RuGL3HeIyzYNwIDAQAB',
    DS_IMAGE_URL: 'https://amb-slot.s3-ap-southeast-1.amazonaws.com/ds-game',

    //PGSlot
    PGSOFT_IMAGE_URL: 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/pg-soft',
    PG_URL: 'https://m.pgjksonc.club/',
    PG_TOKEN: '7262804D-6A81-445D-9A2A-7903244B1094',
    PG_SECRETKEY: 'A8FA26FD33424D7AB6AB36C78A1A226B',

    //Ameba
    AMEBA_URL: 'https://api.fafafa3388.com',
    AMEBA_SITE_ID: '2789',
    AMEBA_SECRET_KEY: 'BmF+P8Iu1Q+H1ApCh7uE7mmuewNjEjw8',
    AMEBA_IMAGE_URL: 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/ameba-game',

    //SlotXO
    SLOT_XO_FORWARD_GAME: 'http://www.gwc688.net/playGame?',
    SLOT_XO_HOST: 'http://api688.net/seamless',
    SLOT_XO_SECRET_KEY: 'xeyuz3cob84eo',
    SLOT_XO_APP_ID: 'F27G',

    //Ganapati
    GANAPATI_URL: 'https://api.ayitang.com/',
    GANAPATI_SECRET_KEY: 'K5XRSXYeZCrpkrvuPVfZdc9fg',
    GANAPATI_AGENT_NAME: 'askmebet',
    GANAPATI_AGENT_CODE: 'afc1688',

    //Live22
    GAMING_SOFT_URL: 'https://cw.889tech.net/CommonWalletOPWS.svc/OPAPI/',
    GAMING_SOFT_OPERATOR_ID: 'afc1688THB',
    GAMING_SOFT_SECRET: 'B900',

    //Spade Slot
    SPADE_LOGIN_URL: 'http://lobby.bigmoose88.com/AFC1688/auth',
    SPADE_API_URL: 'http://merchantapi.hugedolphin.com/api',
    SPADE_MERCHANT_CODE: 'AFC1688',

    //AMB Game Card
    AMB_GAME_API: 'https://api.ambgames.com',
    AMB_GAME_PARTNER: '5e16dc84c6bf130012788d86',
    AMB_GAME_GAME_URL: 'https://games.ambpoker.com',

    //Lotto
    AMB_LOTTO_URL: 'https://amblotto.api-hub.com/',
    AMB_LOTTO_CLIENT: 'afc1688',
    AMB_LOTTO_API_KEY: '2971c05c-1c35-4108-b194-e58b39545078',

    AMB_GAME_API_2: 'https://ap.ambpoker-api.com',
    AMB_GAME_SECRET_KEY_2: 'd2ec30bcb8b527df702b12706be0bb67',
    AMB_GAME_AGENT_2: 'afc'

}


module.exports = client;