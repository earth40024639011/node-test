const client = {

    //---------------//
    LICENSE: '5e3041183173f51a5311da16:5e3041873173f51a5311da1b:5e3046db9d25241aa26d15ac:5e30476751c2e11a98cd38cd:5e3047cd1fe2d31a9294d4a5:5e6e8117f203701a1130d426:5e6e82405883f91a00d9752e:5e6f26ac4566e01a4fb46fdb:5e7b13408bb291343e6e605f',
    ODD_ADJUSTMENT: 2,
    MAX_BACK_LIST: 40000000,
    MASTER_IP: 'isn-backend.api-hub.com',

    //AMBBO (Auto)
    URL_AMBBO: 'https://ambbo.co/login/auto',
    URL_ENCRYPTED: 'https://ambbet-topup.serverless-hub.com/secure/encrypted',
    URL_PAYMENT_GATEWAY_UPDATE_PASSWORD: 'http://topup-casino.api-hub.com:22110/external/customer/update-password',

    //M2 Sport
    MUAY_STEP2_HOST: 'http://www.muaystep2.com',
    MUAY_STEP2_OPCODE: 'iprobetsgw',
    MUAY_STEP2_PRIVATE_KEY: 'MuaY18',
    MUAY_STEP2_OPAGENT_ID: 'iprobetsgwagent',
    MUAY_STEP2_PREFIX: 'iprobetsgw_',

    //Pretty Gaming  *****
    PRETTY_API_URL: 'https://api-prod.aghippo168.com/apiRoute/member/loginRequest',
    PRETTY_SECRET_KEY: 'b0000e31-8b63-fdbe-6250-fbfa2af49edd',
    PRETTY_AGENT: 'iprobet',

    //Sexy bacara
    SEXY_HOST: 'https://api.onlinegames22.com',
    SEXY_AGENT_ID: 'iprobet',
    SEXY_CERT: 'Rv36TWNgedOwezuwkd8',

    //SA Game
    SAG_URL: 'http://api.sa-apisvr.net/api/api.aspx',
    SAG_LOGIN_URL: 'https://ipb.sa-api6.net/app.aspx',
    SAG_SECRET_KEY: '3A3E57AF7AD843E78A91FF138C834519',
    SAG_ENCRYPT_KEY: 'g9G16nTs',
    SAG_MD5_KEY: 'GgaIMaiNNtg',
    SAG_LOBBY_CODE: 'A1905',

    //AG Game
    AG_LOGIN_URL: 'EB7_AGIN',
    AG_ENCRYPT_KEY: '7rPaAz8C',
    AG_MD5_KEY: 'HX4vtEcTaFHd',
    AG_PASS: 'aggpass123',
    AG_URL: 'https://gi.serverless-hub.com/',
    CREATE_SESSION_URL: 'http://swapi.agingames.com:3381/',
    FORWARD_GAME_URL: 'https://gci.serverless-hub.com/',

    //DG Game
    DG_API_URL: 'https://api.dg99web.com',
    DG_API_KEY: '8f707aba13f244fe9c6f7ccd6a10431f',
    DG_AGENT_NAME: 'DG10040104',

    //Dragon Soft
    DS_API_URL: 'https://api.sysonline.club',
    DS_GAME_URL: 'https://link.sandsys.pw',
    DS_AGENT_ACCOUNT: 'AMB1021THB',
    DS_CHANNEL: '33388920',
    DS_CERT: 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDh9eTB+SPQ3+eqGvUo/VJ7QgYhjBJTcBt30jsOinSRtL81jLNbEUuZVxin7tkNX77fdb3tMWbyK9/yHdrBtB9rEU/aqYpOj534vHVuN1LLOSDvih34aCVavGHLtEyXKIsHl6gjgyimECO3VrPDtLHz4qmqyqQaPZUmoMP4zY/TNQIDAQAB',
    DS_IMAGE_URL: 'https://amb-slot.s3-ap-southeast-1.amazonaws.com/ds-game',

    //PGSlot   *****
    PGSOFT_IMAGE_URL: 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/pg-soft',
    PG_URL: 'https://m.pgjksonc.club/',
    PG_TOKEN: '20BA2F82-B316-45A0-B318-85AB31280A5A',
    PG_SECRETKEY: 'AF5D6D9C2EAE489E9F8E283979C181CE',

    //Ameba   ****
    AMEBA_URL: 'https://api.fafafa3388.com',
    AMEBA_SITE_ID: '2688',
    AMEBA_SECRET_KEY: 'iErVoRJ0ec5eqQIpareXj5tm4ez4xe1+',
    AMEBA_IMAGE_URL: 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/ameba-game',

    //SlotXO
    SLOT_XO_FORWARD_GAME: 'http://www.gwc688.net/playGame?',
    SLOT_XO_HOST: 'http://api688.net/seamless',
    SLOT_XO_SECRET_KEY: '7gekashurs7kq',
    SLOT_XO_APP_ID: 'F1ZN',

    //Ganapati
    GANAPATI_URL: 'https://api.ayitang.com/',
    GANAPATI_SECRET_KEY: 'K5XRSXYeZCrpkrvuPVfZdc9fg',
    GANAPATI_AGENT_NAME: 'askmebet',
    GANAPATI_AGENT_CODE: 'iprobet',

    //Live22
    GAMING_SOFT_URL: 'https://cw.889tech.net/CommonWalletOPWS.svc/OPAPI/',
    GAMING_SOFT_OPERATOR_ID: 'iprobetTHB',
    GAMING_SOFT_SECRET: '8655',

    //Spade Slot  
    SPADE_LOGIN_URL: 'http://lobby.bigmoose88.com/IPROBET/auth',
    SPADE_API_URL: 'http://merchantapi.hugedolphin.com/api',
    SPADE_MERCHANT_CODE: 'IPROBET',

    //AMB Game Card  
    AMB_GAME_API: 'https://api.ambgames.com',
    AMB_GAME_PARTNER: '5dca531d03062800177ba10e',
    AMB_GAME_GAME_URL: 'https://games.ambpoker.com',

    //Lotto  
    AMB_LOTTO_URL: 'https://amblotto.api-hub.com/',
    AMB_LOTTO_CLIENT: 'iprobet',
    AMB_LOTTO_API_KEY: '2f5df167-46b3-454b-a145-12886c8e7f15',

    AMB_GAME_API_2: 'https://ap.ambpoker-api.com',
    AMB_GAME_SECRET_KEY_2: '3672cd3270fa6f16a4d747357ec14c2c',
    AMB_GAME_AGENT_2: 'ipro'

}


module.exports = client;