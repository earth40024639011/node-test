const client = {

    //---------------//
    LICENSE: '5bf52116e60f036bf68df87b:5c45c2ed362d6a6ca9b25515:5de00fcbe55573755aee06f7:5df7aabf33f17e2348941039:5dfdf0873af52c5fef613e93:5e312bfa22c1dd4e02c4f019',
    ODD_ADJUSTMENT: 2,
    MAX_BACK_LIST: 40000000,
    MASTER_IP: 'isn-backend.api-hub.com',

    //AMBBO (Auto)
    URL_AMBBO: 'https://ambbo.co/login/auto',
    URL_ENCRYPTED: 'https://ambbet-topup.serverless-hub.com/secure/encrypted',
    URL_PAYMENT_GATEWAY_UPDATE_PASSWORD: 'http://topup-casino.api-hub.com:22110/external/customer/update-password',

    //M2 Sport
    MUAY_STEP2_HOST: 'http://www.muaystep2.com',
    MUAY_STEP2_OPCODE: 'loveballbetsgw',
    MUAY_STEP2_PRIVATE_KEY: 'MuaY18',
    MUAY_STEP2_OPAGENT_ID: 'loveballbetsgwagent',
    MUAY_STEP2_PREFIX: 'loveballbetsgw_',

    //Pretty Gaming
    PRETTY_API_URL: 'https://api-prod.aghippo168.com/apiRoute/member/loginRequest',
    PRETTY_SECRET_KEY: '0a568718-6abf-085c-efe5-048a31634a96',
    PRETTY_AGENT: '5gbet',

    //Sexy bacara
    SEXY_HOST: 'https://api.onlinegames22.com',
    SEXY_AGENT_ID: '5gbet',
    SEXY_CERT: 'Biun4dJobbnOBZ0Rgel',

    //SA Game
    SAG_URL: 'http://api.sa-apisvr.net/api/api.aspx',
    SAG_LOGIN_URL: 'https://5gb.sa-api5.com/app.aspx',
    SAG_SECRET_KEY: '42A31438F6F94872867F466DDD60075C',
    SAG_ENCRYPT_KEY: 'g9G16nTs',
    SAG_MD5_KEY: 'GgaIMaiNNtg',
    SAG_LOBBY_CODE: 'A1427',

    //AG Game
    AG_LOGIN_URL: 'EB7_AGIN',
    AG_ENCRYPT_KEY: '7rPaAz8C',
    AG_MD5_KEY: 'HX4vtEcTaFHd',
    AG_PASS: 'aggpass123',
    AG_URL: 'https://gi.serverless-hub.com/',
    CREATE_SESSION_URL: 'http://swapi.agingames.com:3381/',
    FORWARD_GAME_URL: 'https://gci.serverless-hub.com/',

    //DG Game
    DG_API_URL: 'https://api.dg99web.com',
    DG_API_KEY: '50167198c2054190a2c1e6ce2cc2c3ad',
    DG_AGENT_NAME: 'DG04020816',

    //Dragoon Soft
    DS_API_URL: 'https://api.sysonline.club',
    DS_GAME_URL: 'https://link.sandsys.pw',
    DS_AGENT_ACCOUNT: 'AMB1016THB',
    DS_CHANNEL: '81769441',
    DS_CERT: 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDlIhhnezdS7hee7jVtGq9Dh5OT81DK9UdX5IMj3DH1SgW539rd+0QyVMtBaZXEdanOkIQKpdK3xJH5xi4ty2rcnFAj3tWVi3a0bRGK/weRDS+r0/mtDbN+18un+VKM1FW/PgO/qxg1XIB3AvUPMWK97CVSQC0aOgwCYVEsCIb5IwIDAQAB',
    DS_IMAGE_URL: 'https://amb-slot.s3-ap-southeast-1.amazonaws.com/ds-game',

    //PGSlot
    PGSOFT_IMAGE_URL: 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/pg-soft',
    PG_URL: 'https://m.pgjksonc.club/',
    PG_TOKEN: '97CFDCE7-E95A-4060-B614-4E66E95A67B0',
    PG_SECRETKEY: '9BBA297990EF4444B96F0B85435C62D0',

    //Ameba
    AMEBA_URL: 'https://api.fafafa3388.com',
    AMEBA_SITE_ID: '2460',
    AMEBA_SECRET_KEY: '1jeeOokkwwEu2+dxshVyvJravn9fEMvM',
    AMEBA_IMAGE_URL: 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/ameba-game',

    //SlotXO
    SLOT_XO_FORWARD_GAME: 'http://www.gwc688.net/playGame?',
    SLOT_XO_HOST: 'http://api688.net/seamless',
    SLOT_XO_SECRET_KEY: '7gepud8xzrg4q',
    SLOT_XO_APP_ID: 'F1Z6',

    //Ganapati
    GANAPATI_URL: 'https://api.ayitang.com/',
    GANAPATI_SECRET_KEY: 'K5XRSXYeZCrpkrvuPVfZdc9fg',
    GANAPATI_AGENT_NAME: 'askmebet',
    GANAPATI_AGENT_CODE: 'loveballbet',

    //Live22
    GAMING_SOFT_URL: 'https://cw.889tech.net/CommonWalletOPWS.svc/OPAPI/',
    GAMING_SOFT_OPERATOR_ID: '5gbetTHB',
    GAMING_SOFT_SECRET: 'A720',

    //Spade Slot
    SPADE_LOGIN_URL: 'http://lobby.bigmoose88.com/5G/auth',
    SPADE_API_URL: 'http://merchantapi.hugedolphin.com/api',
    SPADE_MERCHANT_CODE: '5G',

    //AMB Game Card
    AMB_GAME_API: 'https://api.ambgames.com',
    AMB_GAME_PARTNER: '5d6e6ff9390ed100101970d3',
    AMB_GAME_GAME_URL: 'https://games.ambpoker.com',

    //Lotto
    AMB_LOTTO_URL: 'https://amblotto.api-hub.com/',
    AMB_LOTTO_CLIENT: '5gbet',
    AMB_LOTTO_API_KEY: '9aae5156-bee2-4cc9-b72f-92f9a6d46cfe',

    AMB_GAME_API_2: 'https://ap.ambpoker-api.com',
    AMB_GAME_SECRET_KEY_2: 'a6b38f0eccd4864d1a07bfc9173913c8',
    AMB_GAME_AGENT_2: '5gb'

}


module.exports = client;