const client = {

    //---------------//
    LICENSE: '5ca82660e2bc3b1acdef8b97:5cdf59039b46bc1756c5017e:5dda297318598b40397afba7:5e2bf09b8b8ff729abf97fc5',
    ODD_ADJUSTMENT: 2,
    MAX_BACK_LIST: 40000000,
    MASTER_IP: 'isn-backend.api-hub.com',

    //AMBBO (Auto)
    URL_AMBBO: 'https://ambbo.co/login/auto',
    URL_ENCRYPTED: 'https://ambbet-topup.serverless-hub.com/secure/encrypted',
    URL_PAYMENT_GATEWAY_UPDATE_PASSWORD: 'http://topup-casino.api-hub.com:22110/external/customer/update-password',

    //M2 Sport
    MUAY_STEP2_HOST: 'http://www.muaystep2.com',
    MUAY_STEP2_OPCODE: 'goal168sgw',
    MUAY_STEP2_PRIVATE_KEY: 'MuaY18',
    MUAY_STEP2_OPAGENT_ID: 'goal168sgwagent',
    MUAY_STEP2_PREFIX: 'goal168sgw_',

    //Pretty Gaming
    PRETTY_API_URL: 'https://api-prod.aghippo168.com/apiRoute/member/loginRequest',
    PRETTY_SECRET_KEY: '6513c6b9-6ec0-3acb-1e05-a26c5dc60f27',
    PRETTY_AGENT: 'goal168',

    //Sexy bacara
    SEXY_HOST: 'https://api.onlinegames22.com',
    SEXY_AGENT_ID: 'goal168',
    SEXY_CERT: '9V3jVYV0lzDBcT3HYHp',

    //SA Game
    SAG_URL: 'http://api.sa-apisvr.net/api/api.aspx',
    SAG_LOGIN_URL: 'https://goa.sa-api3.net/app.aspx',
    SAG_SECRET_KEY: 'B0A4EE19250743D8A9836866E1A14440',
    SAG_ENCRYPT_KEY: 'g9G16nTs',
    SAG_MD5_KEY: 'GgaIMaiNNtg',
    SAG_LOBBY_CODE: 'A934',

    //AG Game
    AG_LOGIN_URL: 'EB7_AGIN',
    AG_ENCRYPT_KEY: '7rPaAz8C',
    AG_MD5_KEY: 'HX4vtEcTaFHd',
    AG_PASS: 'aggpass123',
    AG_URL: 'https://gi.serverless-hub.com/',
    CREATE_SESSION_URL: 'http://swapi.agingames.com:3381/',
    FORWARD_GAME_URL: 'https://gci.serverless-hub.com/',

    //DG Game
    DG_API_URL: 'https://api.dg99web.com',
    DG_API_KEY: '50eeb28f0a08483ca3bfea8073f14f9a',
    DG_AGENT_NAME: 'DG04020804',

    //Dragoon Soft
    DS_API_URL: 'https://api.sysonline.club',
    DS_GAME_URL: 'https://link.sandsys.pw',
    DS_AGENT_ACCOUNT: 'AMB1005THB',
    DS_CHANNEL: '83289145',
    DS_CERT: 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC5eaJoPYr4kkHgnS/MNPsCbqPvfgq4a9HpolulHCTLrabjw3IRr0zVutzu524nK9Kq5aWrvnwjuURyqGXwvc8oS/8sAHDIuk9mPlNChhkKyCJwLJ49HeVsZLrFIg4o7Sig0xsD4o+TkM/mc3jFKNtmq6Ug4HAW/37KoAhqiZ7HUwIDAQAB',
    DS_IMAGE_URL: 'https://amb-slot.s3-ap-southeast-1.amazonaws.com/ds-game',

    //PGSlot
    PGSOFT_IMAGE_URL: 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/pg-soft',
    PG_URL: 'https://m.pgjksonc.club/',
    PG_TOKEN: 'DE57BBE4-9CE4-44D1-AA14-35A31744B1C6',
    PG_SECRETKEY: 'D407F688941E4BAD9FE2CE09A67D1D90',

    //Ameba
    AMEBA_URL: 'https://api.fafafa3388.com',
    AMEBA_SITE_ID: '2238',
    AMEBA_SECRET_KEY: 'HkcRRG8BpCl1VvyVvIFEb8b+eVWt8HYF',
    AMEBA_IMAGE_URL: 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/ameba-game',

    //SlotXO
    SLOT_XO_FORWARD_GAME: 'http://www.gwc688.net/playGame?',
    SLOT_XO_HOST: 'http://api688.net/seamless',
    SLOT_XO_SECRET_KEY: 'aghxdjg81r136',
    SLOT_XO_APP_ID: 'F1YV',

    //Ganapati
    GANAPATI_URL: 'https://api.ayitang.com/',
    GANAPATI_SECRET_KEY: 'K5XRSXYeZCrpkrvuPVfZdc9fg',
    GANAPATI_AGENT_NAME: 'askmebet',
    GANAPATI_AGENT_CODE: 'goal168',

    //Live22
    GAMING_SOFT_URL: 'https://cw.889tech.net/CommonWalletOPWS.svc/OPAPI/',
    GAMING_SOFT_OPERATOR_ID: 'goal168THB',
    GAMING_SOFT_SECRET: '9555',

    //Spade Slot
    SPADE_LOGIN_URL: 'http://lobby.bigmoose88.com/goal168/auth',
    SPADE_API_URL: 'http://merchantapi.hugedolphin.com/api',
    SPADE_MERCHANT_CODE: 'GOAL168',

    //AMB Game Card
    AMB_GAME_API: 'https://api.ambgames.com',
    AMB_GAME_PARTNER: '5d5432054b98e600109c12b6',
    AMB_GAME_GAME_URL: 'https://games.ambpoker.com',

    //Lotto
    AMB_LOTTO_URL: 'https://amblotto.api-hub.com/',
    AMB_LOTTO_CLIENT: 'goal168',
    AMB_LOTTO_API_KEY: '24b8da39-e6c6-401f-9277-ab078f83bdef',

    AMB_GAME_API_2: 'https://ap.ambpoker-api.com',
    AMB_GAME_SECRET_KEY_2: 'fab9ed76ec798a59c52e46ef39034a07',
    AMB_GAME_AGENT_2: 'goa'

}


module.exports = client;