const client = {

    //---------------//
    LICENSE: '5e05ef2a4016f851a574eb08:5e0f16d92109ee5c06c4951e:5e1059f98a51696b7f6863a5:5e05ef2a4016f851a574eb08:5e244eb91e45d51c69799e66:5e244fa51265871c4d1bce97:5e22e4565d432c182adde4f7:5e257282aef2b11ff89edb8c:5e38d6ff715f82466ade99aa:5e38d6e6c88fdd4680074543:5e38d719c74efb474880d14e:5e38d6d0c74efb474880d149:5e63737ee8052c143c3a04a7:5e63728eb2c0ea1458f2b65d:5e637307dee3b316a9bb0991:5e637331dee3b316a9bb0997:5e63735d30877b16b58eafdd:5e637658b2c0ea1458f2b87c',
    ODD_ADJUSTMENT: 2,
    MAX_BACK_LIST: 40000000,
    MASTER_IP: 'isn-backend.api-hub.com',

    //AMBBO (Auto)
    URL_AMBBO: 'https://ambbo.co/login/auto',
    URL_ENCRYPTED: 'https://ambbet-topup.serverless-hub.com/secure/encrypted',
    URL_PAYMENT_GATEWAY_UPDATE_PASSWORD: 'http://topup-casino.api-hub.com:22110/external/customer/update-password',

    //M2 Sport
    MUAY_STEP2_HOST: 'http://www.muaystep2.com',
    MUAY_STEP2_OPCODE: 'fastbet98sgw',
    MUAY_STEP2_PRIVATE_KEY: 'MuaY18',
    MUAY_STEP2_OPAGENT_ID: 'fastbet98sgwagent',
    MUAY_STEP2_PREFIX: 'fastbet98sgw_',

    //Pretty Gaming
    PRETTY_API_URL: 'https://api-prod.aghippo168.com/apiRoute/member/loginRequest',
    PRETTY_SECRET_KEY: '6e99365b-83b6-2631-dba3-35ed2169e809',
    PRETTY_AGENT: 'fastbet98',

    //Sexy bacara
    SEXY_HOST: 'https://api.onlinegames22.com',
    SEXY_AGENT_ID: 'fast98',
    SEXY_CERT: '4WkfGjC3fhdXbKEwUt3',

    //SA Game
    SAG_URL: 'http://api.sa-apisvr.net/api/api.aspx',
    SAG_LOGIN_URL: 'https://amb.sa-api3.net/app.aspx',
    SAG_SECRET_KEY: 'E4CC61420A5F47E0AECC73E5DFA58042',
    SAG_ENCRYPT_KEY: 'g9G16nTs',
    SAG_MD5_KEY: 'GgaIMaiNNtg',
    SAG_LOBBY_CODE: 'A1949',

    //AG Game
    AG_LOGIN_URL: 'EB7_AGIN',
    AG_ENCRYPT_KEY: '7rPaAz8C',
    AG_MD5_KEY: 'HX4vtEcTaFHd',
    AG_PASS: 'aggpass123',
    AG_URL: 'https://gi.serverless-hub.com/',
    CREATE_SESSION_URL: 'http://swapi.agingames.com:3381/',
    FORWARD_GAME_URL: 'https://gci.serverless-hub.com/',

    //DG Game
    DG_API_URL: 'https://api.dg99web.com',
    DG_API_KEY: 'a3198cda2810402299a1b0874b374e32',
    DG_AGENT_NAME: 'DG10040108',

    //Dragoon Soft
    DS_API_URL: 'https://api.sysonline.club',
    DS_GAME_URL: 'https://link.sandsys.pw',
    DS_AGENT_ACCOUNT: 'AMB1022THB',
    DS_CHANNEL: '13479492',
    DS_CERT: 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC30bBpfoosEJ1BnxYJBYkTGtIiRUqWmgy980pejXZUVJhxDN03OsqD0fBLWgTBLehLrQfA+xqpuKdodg36NZSkto0rxrwEP+xfFIpVc+DutBW7KGIvIlc4En+deJQgQbKMjINqXmnKiRQx3gS1vxmVaENvv/gRp4oWIfHkNL1vqQIDAQAB',
    DS_IMAGE_URL: 'https://amb-slot.s3-ap-southeast-1.amazonaws.com/ds-game',

    //PGSlot
    PGSOFT_IMAGE_URL: 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/pg-soft',
    PG_URL: 'https://m.pgjksonc.club/',
    PG_TOKEN: 'C236E700-A6E9-4F58-B4C8-550AA8CC48FA',
    PG_SECRETKEY: 'BA886A3931AB4EB8909B45357AA20831',

    //Ameba
    AMEBA_URL: 'https://api.fafafa3388.com',
    AMEBA_SITE_ID: '2704',
    AMEBA_SECRET_KEY: '0dEx0d4LTks/OQqIFK4yytRHYl/qzOEG',
    AMEBA_IMAGE_URL: 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/ameba-game',

    //SlotXO
    SLOT_XO_FORWARD_GAME: 'http://www.gwc688.net/playGame?',
    SLOT_XO_HOST: 'http://api688.net/seamless',
    SLOT_XO_SECRET_KEY: 'jhibpsdrpmxyn',
    SLOT_XO_APP_ID: 'F21Z',

    //Ganapati
    GANAPATI_URL: 'https://api.ayitang.com/',
    GANAPATI_SECRET_KEY: 'K5XRSXYeZCrpkrvuPVfZdc9fg',
    GANAPATI_AGENT_NAME: 'askmebet',
    GANAPATI_AGENT_CODE: 'fastbet',

    //Live22
    GAMING_SOFT_URL: 'https://cw.889tech.net/CommonWalletOPWS.svc/OPAPI/',
    GAMING_SOFT_OPERATOR_ID: 'fastbet98THB',
    GAMING_SOFT_SECRET: '9EDD',

    //Spade Slot
    SPADE_LOGIN_URL: 'http://lobby.bigmoose88.com/FAST98/auth',
    SPADE_API_URL: 'http://merchantapi.hugedolphin.com/api',
    SPADE_MERCHANT_CODE: 'FAST98',

    //AMB Game Card
    AMB_GAME_API: 'https://api.ambgames.com',
    AMB_GAME_PARTNER: '5dd81776289c3b0018d68fd6',
    AMB_GAME_GAME_URL: 'https://games.ambpoker.com',

    //Lotto
    AMB_LOTTO_URL: 'https://amblotto.api-hub.com/',
    AMB_LOTTO_CLIENT: 'fastbet98',
    AMB_LOTTO_API_KEY: 'e25ef3c3-64e6-4098-80da-313f87d2b6df',

    AMB_GAME_API_2: 'https://ap.ambpoker-api.com',
    AMB_GAME_SECRET_KEY_2: 'aa60d52dd6ca7d57d94af72d56028a97',
    AMB_GAME_AGENT_2: 'fast'

}


module.exports = client;