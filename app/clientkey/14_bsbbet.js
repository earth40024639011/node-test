const client = {

    //---------------//
    LICENSE: '5bf52116e60f036bf68df87b:5c45c2ed362d6a6ca9b25515:5de00fcbe55573755aee06f7:5df7aabf33f17e2348941039:5dfdf0873af52c5fef613e93:5e312bfa22c1dd4e02c4f019',
    ODD_ADJUSTMENT: 2,
    MAX_BACK_LIST: 40000000,
    MASTER_IP: 'isn-backend.api-hub.com',

    //AMBBO (Auto)
    URL_AMBBO: 'https://ambbo.co/login/auto',
    URL_ENCRYPTED: 'https://ambbet-topup.serverless-hub.com/secure/encrypted',
    URL_PAYMENT_GATEWAY_UPDATE_PASSWORD: 'http://topup-casino.api-hub.com:22110/external/customer/update-password',

    //M2 Sport
    MUAY_STEP2_HOST: 'http://www.muaystep2.com',
    MUAY_STEP2_OPCODE: 'bsbbetsgw',
    MUAY_STEP2_PRIVATE_KEY: 'MuaY18',
    MUAY_STEP2_OPAGENT_ID: 'bsbbetsgwagent',
    MUAY_STEP2_PREFIX: 'bsbbetsgw_',

    //Pretty Gaming
    PRETTY_API_URL: 'https://api-prod.aghippo168.com/apiRoute/member/loginRequest',
    PRETTY_SECRET_KEY: 'fd7c2967-cc00-6318-c0c1-8f0f9ea37529',
    PRETTY_AGENT: 'bsbbet',

    //Sexy bacara
    SEXY_HOST: 'https://api.onlinegames22.com',
    SEXY_AGENT_ID: 'bsbbet',
    SEXY_CERT: '3yVEFGoTafj4BuhaOsc',

    //SA Game
    SAG_URL: 'http://api.sa-apisvr.net/api/api.aspx',
    SAG_LOGIN_URL: 'https://bb2.sa-api5.com/app.aspx',
    SAG_SECRET_KEY: 'C2B61ADF20C940938F1EA118AC73BCFE',
    SAG_ENCRYPT_KEY: 'g9G16nTs',
    SAG_MD5_KEY: 'GgaIMaiNNtg',
    SAG_LOBBY_CODE: 'A1426',

    //AG Game
    AG_LOGIN_URL: 'EB7_AGIN',
    AG_ENCRYPT_KEY: '7rPaAz8C',
    AG_MD5_KEY: 'HX4vtEcTaFHd',
    AG_PASS: 'aggpass123',
    AG_URL: 'https://gi.serverless-hub.com/',
    CREATE_SESSION_URL: 'http://swapi.agingames.com:3381/',
    FORWARD_GAME_URL: 'https://gci.serverless-hub.com/',

    //DG Game
    DG_API_URL: 'https://api.dg99web.com',
    DG_API_KEY: '7ebd2613ab5140cfb1643f95ac76f68c',
    DG_AGENT_NAME: 'DG04020814',

    //Dragoon Soft
    DS_API_URL: 'https://api.sysonline.club',
    DS_GAME_URL: 'https://link.sandsys.pw',
    DS_AGENT_ACCOUNT: 'AMB1012THB',
    DS_CHANNEL: '93870835',
    DS_CERT: 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDtuuNMUDhNcMSDIrDZ2vFXhi8TAKaH/GNRcezG8w46gjobCDj7ezxmdXVzFhEQ/0iODq28eBKQB30evU7Bl5v+ZMnGS/jKKNbjmZ3T9tNSYiadZR/b9uASLO0Ysi/uScXqSmsHIhyyiDTcG0cBfWfit7MRQEfaoE5y6q669p8etwIDAQAB',
    DS_IMAGE_URL: 'https://amb-slot.s3-ap-southeast-1.amazonaws.com/ds-game',

    //PGSlot
    PGSOFT_IMAGE_URL: 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/pg-soft',
    PG_URL: 'https://m.pgjksonc.club/',
    PG_TOKEN: '3EEE647F-82DB-46BE-BF13-1CCD81037040',
    PG_SECRETKEY: '62F7182DA30E41298A55FCF57C146171',

    //Ameba
    AMEBA_URL: 'https://api.fafafa3388.com',
    AMEBA_SITE_ID: '2378',
    AMEBA_SECRET_KEY: 'nSukKMPw3J7OWAph1w0JErNZA7iMwyp6',
    AMEBA_IMAGE_URL: 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/ameba-game',

    //SlotXO
    SLOT_XO_FORWARD_GAME: 'http://www.gwc688.net/playGame?',
    SLOT_XO_HOST: 'http://api688.net/seamless',
    SLOT_XO_SECRET_KEY: 'hgcpdb88srn56',
    SLOT_XO_APP_ID: 'F1Z5',

    //Ganapati
    GANAPATI_URL: 'https://api.ayitang.com/',
    GANAPATI_SECRET_KEY: 'K5XRSXYeZCrpkrvuPVfZdc9fg',
    GANAPATI_AGENT_NAME: 'askmebet',
    GANAPATI_AGENT_CODE: 'bsbbet',

    //Live22
    GAMING_SOFT_URL: 'https://cw.889tech.net/CommonWalletOPWS.svc/OPAPI/',
    GAMING_SOFT_OPERATOR_ID: 'bsbbetTHB',
    GAMING_SOFT_SECRET: '9A69',

    //Spade Slot
    SPADE_LOGIN_URL: 'http://lobby.bigmoose88.com/BSBBET/auth',
    SPADE_API_URL: 'http://merchantapi.hugedolphin.com/api',
    SPADE_MERCHANT_CODE: 'BSBBET',

    //AMB Game Card
    AMB_GAME_API: 'https://api.ambgames.com',
    AMB_GAME_PARTNER: '5d6e7087390ed100101970e4',
    AMB_GAME_GAME_URL: 'https://games.ambpoker.com',

    //Lotto
    AMB_LOTTO_URL: 'https://amblotto.api-hub.com/',
    AMB_LOTTO_CLIENT: 'bsbbet',
    AMB_LOTTO_API_KEY: '14aa3c3a-2b8e-4a45-85a8-23a108f4aef8',

    AMB_GAME_API_2: 'https://ap.ambpoker-api.com',
    AMB_GAME_SECRET_KEY_2: 'a9497f8401e363859d4ee278f660f91b',
    AMB_GAME_AGENT_2: 'bsb'

}


module.exports = client;