const client = {

    //---------------//
    LICENSE: '5e3041183173f51a5311da16:5e3041873173f51a5311da1b:5e3046db9d25241aa26d15ac:5e30476751c2e11a98cd38cd:5e3047cd1fe2d31a9294d4a5:5e6e8117f203701a1130d426:5e6e82405883f91a00d9752e:5e6f26ac4566e01a4fb46fdb:5e7b13408bb291343e6e605f',
    ODD_ADJUSTMENT: 2,
    MAX_BACK_LIST: 40000000,
    MASTER_IP: 'isn-backend.api-hub.com',

    //AMBBO (Auto)
    URL_AMBBO: 'https://ambbo.co/login/auto',
    URL_ENCRYPTED: 'https://ambbet-topup.serverless-hub.com/secure/encrypted',
    URL_PAYMENT_GATEWAY_UPDATE_PASSWORD: 'http://topup-casino.api-hub.com:22110/external/customer/update-password',

    //M2 Sport
    MUAY_STEP2_HOST: 'http://www.muaystep2.com',
    MUAY_STEP2_OPCODE: 'mahagame66sgw',
    MUAY_STEP2_PRIVATE_KEY: 'MuaY18',
    MUAY_STEP2_OPAGENT_ID: 'mahagame66sgwagent',
    MUAY_STEP2_PREFIX: 'mahagame66sgw_',

    //Pretty Gaming  *****
    PRETTY_API_URL: 'https://api-prod.aghippo168.com/apiRoute/member/loginRequest',
    PRETTY_SECRET_KEY: '4d664b50-609e-aab9-f911-1f4c92a5046c',
    PRETTY_AGENT: 'mahagame66',

    //Sexy bacara
    SEXY_HOST: 'https://api.onlinegames22.com',
    SEXY_AGENT_ID: 'mahagame',
    SEXY_CERT: '5iSgHWnOytdAscVtRGT',

    //SA Game
    SAG_URL: 'http://api.sa-apisvr.net/api/api.aspx',
    SAG_LOGIN_URL: 'https://mah.sa-api6.net/app.aspx;',
    SAG_SECRET_KEY: '12D113B148524E6A8BC00DCA82EF2F86',
    SAG_ENCRYPT_KEY: 'g9G16nTs',
    SAG_MD5_KEY: 'GgaIMaiNNtg',
    SAG_LOBBY_CODE: 'A1831',

    //AG Game
    AG_LOGIN_URL: 'EB7_AGIN',
    AG_ENCRYPT_KEY: '7rPaAz8C',
    AG_MD5_KEY: 'HX4vtEcTaFHd',
    AG_PASS: 'aggpass123',
    AG_URL: 'https://gi.serverless-hub.com/',
    CREATE_SESSION_URL: 'http://swapi.agingames.com:3381/',
    FORWARD_GAME_URL: 'https://gci.serverless-hub.com/',

    //DG Game
    DG_API_URL: 'https://api.dg99web.com',
    DG_API_KEY: 'fcace6fe0c3549bdac9f242d74dfcb0a',
    DG_AGENT_NAME: 'DG10040107',

    //Dragon Soft
    DS_API_URL: 'https://api.sysonline.club',
    DS_GAME_URL: 'https://link.sandsys.pw',
    DS_AGENT_ACCOUNT: 'AMB1019THB',
    DS_CHANNEL: '53999500',
    DS_CERT: 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCxmHUe8yL2D15WY7tH+6fzdywXPNw+x1mpKdu9oTKrvHvK2yaBM6O8W6nsOMSxz87If/68hQRzI0Ud8/w44J3qSuEmcKI1uwMoewcl8+560+z/bH8sInKFNSvei/F1VWNPgwdpSvr+1Ck5uInRkA0k4gKyu8Kggs/JQv+KWuCxeQIDAQAB',
    DS_IMAGE_URL: 'https://amb-slot.s3-ap-southeast-1.amazonaws.com/ds-game',

    //PGSlot   *****
    PGSOFT_IMAGE_URL: 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/pg-soft',
    PG_URL: 'https://m.pgjksonc.club/',
    PG_TOKEN: '8BF35C01-D1D5-42AF-BBF9-71DA89E6D8F9',
    PG_SECRETKEY: 'CA914362AB214ED59FCF228377B93C44',

    //Ameba   ****
    AMEBA_URL: 'https://api.fafafa3388.com',
    AMEBA_SITE_ID: '2646',
    AMEBA_SECRET_KEY: 'jbBN5btFSgjCwAW0s6GTWvB6YCxs1rbv',
    AMEBA_IMAGE_URL: 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/ameba-game',

    //SlotXO
    SLOT_XO_FORWARD_GAME: 'http://www.gwc688.net/playGame?',
    SLOT_XO_HOST: 'http://api688.net/seamless',
    SLOT_XO_SECRET_KEY: 'hgckewh5fs3m6',
    SLOT_XO_APP_ID: 'F1ZM',

    //Ganapati
    GANAPATI_URL: 'https://api.ayitang.com/',
    GANAPATI_SECRET_KEY: 'K5XRSXYeZCrpkrvuPVfZdc9fg',
    GANAPATI_AGENT_NAME: 'askmebet',
    GANAPATI_AGENT_CODE: 'mahagame',

    //Live22
    GAMING_SOFT_URL: 'https://cw.889tech.net/CommonWalletOPWS.svc/OPAPI/',
    GAMING_SOFT_OPERATOR_ID: 'mahagame66THB',
    GAMING_SOFT_SECRET: '95D1',

    //Spade Slot  
    SPADE_LOGIN_URL: 'http://lobby.bigmoose88.com/MAHA66/auth',
    SPADE_API_URL: 'http://merchantapi.hugedolphin.com/api',
    SPADE_MERCHANT_CODE: 'MAHA66',

    //AMB Game Card  
    AMB_GAME_API: 'https://api.ambgames.com',
    AMB_GAME_PARTNER: '5dbc184f9c82d900109f958e',
    AMB_GAME_GAME_URL: 'https://games.ambpoker.com',

    //Lotto  
    AMB_LOTTO_URL: 'https://amblotto.api-hub.com/',
    AMB_LOTTO_CLIENT: 'mahagame',
    AMB_LOTTO_API_KEY: '92cada39-ae2d-4e00-baf1-d1564699bc50',

    AMB_GAME_API_2: 'https://ap.ambpoker-api.com',
    AMB_GAME_SECRET_KEY_2: '3ebdc85c220b9fee289b547504235ee3',
    AMB_GAME_AGENT_2: 'mh66'

}


module.exports = client;