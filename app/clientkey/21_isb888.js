const client = {

    //---------------//
    LICENSE: '5e3041183173f51a5311da16:5e3041873173f51a5311da1b:5e3046db9d25241aa26d15ac:5e30476751c2e11a98cd38cd:5e3047cd1fe2d31a9294d4a5:5e6e8117f203701a1130d426:5e6e82405883f91a00d9752e:5e6f26ac4566e01a4fb46fdb:5e7b13408bb291343e6e605f',
    ODD_ADJUSTMENT: 2,
    MAX_BACK_LIST: 40000000,
    MASTER_IP: 'isn-backend.api-hub.com',

    //AMBBO (Auto)
    URL_AMBBO: 'https://ambbo.co/login/auto',
    URL_ENCRYPTED: 'https://ambbet-topup.serverless-hub.com/secure/encrypted',
    URL_PAYMENT_GATEWAY_UPDATE_PASSWORD: 'http://topup-casino.api-hub.com:22110/external/customer/update-password',

    //M2 Sport
    MUAY_STEP2_HOST: 'http://www.muaystep2.com',
    MUAY_STEP2_OPCODE: 'isb888sgw',
    MUAY_STEP2_PRIVATE_KEY: 'MuaY18',
    MUAY_STEP2_OPAGENT_ID: 'isb888sgwagent',
    MUAY_STEP2_PREFIX: 'isb888sgw_',

    //Pretty Gaming
    PRETTY_API_URL: 'https://api-prod.aghippo168.com/apiRoute/member/loginRequest',
    PRETTY_SECRET_KEY: '502bfc54-8fd5-1c34-10cb-87c38e1f13b3',
    PRETTY_AGENT: 'isb888',

    //Sexy bacara
    SEXY_HOST: 'https://api.onlinegames22.com',
    SEXY_AGENT_ID: 'isb888',
    SEXY_CERT: 'xqUeWlSdCHaGLufD8Aa',

    //SA Game
    SAG_URL: 'http://api.sa-apisvr.net/api/api.aspx',
    SAG_LOGIN_URL: 'https://isb.sa-api6.net/app.aspx',
    SAG_SECRET_KEY: 'D2767ADDF6A3415880E939B500589809',
    SAG_ENCRYPT_KEY: 'g9G16nTs',
    SAG_MD5_KEY: 'GgaIMaiNNtg',
    SAG_LOBBY_CODE: 'A1830',

    //AG Game
    AG_LOGIN_URL: 'EB7_AGIN',
    AG_ENCRYPT_KEY: '7rPaAz8C',
    AG_MD5_KEY: 'HX4vtEcTaFHd',
    AG_PASS: 'aggpass123',
    AG_URL: 'https://gi.serverless-hub.com/',
    CREATE_SESSION_URL: 'http://swapi.agingames.com:3381/',
    FORWARD_GAME_URL: 'https://gci.serverless-hub.com/',

    //DG Game
    DG_API_URL: 'https://api.dg99web.com',
    DG_API_KEY: 'd32384b1e59b491d872a93aa014543b2',
    DG_AGENT_NAME: 'DG10040106',

    //Dragon Soft
    DS_API_URL: 'https://api.sysonline.club',
    DS_GAME_URL: 'https://link.sandsys.pw',
    DS_AGENT_ACCOUNT: 'AMB1020THB',
    DS_CHANNEL: '00034913',
    DS_CERT: 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC9+3iG1HRX+iO+kzbe1kv0AZNT2YRNhhhwk91UFkGz/ggd1piNWJ2H//omYOzjzNQ0de7z4qymu51Q1odYJFRuaeA9Wd8XuPS/ezTDpJT8Bxgs1U69Ak5QwBU7UbTPCvK2abYC7NvbOqtCmiXin9e627Rb/xha5MvU+E7HJPifewIDAQAB',
    DS_IMAGE_URL: 'https://amb-slot.s3-ap-southeast-1.amazonaws.com/ds-game',

    //PGSlot
    PGSOFT_IMAGE_URL: 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/pg-soft',
    PG_URL: 'https://m.pgjksonc.club/',
    PG_TOKEN: '2B52EFD5-7325-4B10-B97B-AF68FE48094A',
    PG_SECRETKEY: '0E311EF4EBE64C3898262D14C2AB3F0B',

    //Ameba
    AMEBA_URL: 'https://api.fafafa3388.com',
    AMEBA_SITE_ID: '2647',
    AMEBA_SECRET_KEY: '1TVgV+nOFxL0aTL2LWmdy7urtSqtvY30',
    AMEBA_IMAGE_URL: 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/ameba-game',

    //SlotXO
    SLOT_XO_FORWARD_GAME: 'http://www.gwc688.net/playGame?',
    SLOT_XO_HOST: 'http://api688.net/seamless',
    SLOT_XO_SECRET_KEY: 'dfof3k5d4if8q',
    SLOT_XO_APP_ID: 'F1ZL',

    //Ganapati
    GANAPATI_URL: 'https://api.ayitang.com/',
    GANAPATI_SECRET_KEY: 'K5XRSXYeZCrpkrvuPVfZdc9fg',
    GANAPATI_AGENT_NAME: 'askmebet',
    GANAPATI_AGENT_CODE: 'isb',

    //Live22
    GAMING_SOFT_URL: 'https://cw.889tech.net/CommonWalletOPWS.svc/OPAPI/',
    GAMING_SOFT_OPERATOR_ID: 'isb888THB',
    GAMING_SOFT_SECRET: 'BE7F',

    //Spade Slot
    SPADE_LOGIN_URL: 'http://lobby.bigmoose88.com/ISB888/auth',
    SPADE_API_URL: 'http://merchantapi.hugedolphin.com/api',
    SPADE_MERCHANT_CODE: 'ISB888',

    //AMB Game Card
    AMB_GAME_API: 'https://api.ambgames.com',
    AMB_GAME_PARTNER: '5dbc184f9c82d900109f958e',
    AMB_GAME_GAME_URL: 'https://games.ambpoker.com',

    //Lotto
    AMB_LOTTO_URL: 'https://amblotto.api-hub.com/',
    AMB_LOTTO_CLIENT: 'isb888',
    AMB_LOTTO_API_KEY: '479569cc-516a-47f3-9e80-440c4aa62f06',

    AMB_GAME_API_2: 'https://ap.ambpoker-api.com',
    AMB_GAME_SECRET_KEY_2: 'ee23dca40c73ffb5841e1057f30129cf',
    AMB_GAME_AGENT_2: 'isb8'

}


module.exports = client;