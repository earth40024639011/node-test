const client = {

    //---------------//
    LICENSE: '5ca82660e2bc3b1acdef8b97:5cdf59039b46bc1756c5017e:5dda297318598b40397afba7:5e2bf09b8b8ff729abf97fc5',
    ODD_ADJUSTMENT: 2,
    MAX_BACK_LIST: 40000000,
    MASTER_IP: 'isn-backend.api-hub.com',

    //AMBBO (Auto)
    URL_AMBBO: 'https://ambbo.co/login/auto',
    URL_ENCRYPTED: 'https://ambbet-topup.serverless-hub.com/secure/encrypted',
    URL_PAYMENT_GATEWAY_UPDATE_PASSWORD: 'http://topup-casino.api-hub.com:22110/external/customer/update-password',

    //M2 Sport
    MUAY_STEP2_HOST: 'http://www.muaystep2.com',
    MUAY_STEP2_OPCODE: 'mgmsgw',
    MUAY_STEP2_PRIVATE_KEY: 'MuaY18',
    MUAY_STEP2_OPAGENT_ID: 'mgmsgwagent',
    MUAY_STEP2_PREFIX: 'mgmsgw_',

    //Pretty Gaming
    PRETTY_API_URL: 'https://api-prod.aghippo168.com/apiRoute/member/loginRequest',
    PRETTY_SECRET_KEY: 'ada2d7d3-1a29-5b7a-22d2-0adcfa144b73',
    PRETTY_AGENT: 'mgm99win',

    //Sexy bacara
    SEXY_HOST: 'https://api.onlinegames22.com',
    SEXY_AGENT_ID: 'mgm',
    SEXY_CERT: 'maOQxbF9p6i3MAO2742',

    //SA Game
    SAG_URL: 'http://api.sa-apisvr.net/api/api.aspx',
    SAG_LOGIN_URL: 'https://mgm.sa-api3.net/app.aspx',
    SAG_SECRET_KEY: '3742C7428D064013B55344E5EDCA9DC9',
    SAG_ENCRYPT_KEY: 'g9G16nTs',
    SAG_MD5_KEY: 'GgaIMaiNNtg',
    SAG_LOBBY_CODE: 'A937',

    //AG Game
    AG_LOGIN_URL: 'EB7_AGIN',
    AG_ENCRYPT_KEY: '7rPaAz8C',
    AG_MD5_KEY: 'HX4vtEcTaFHd',
    AG_PASS: 'aggpass123',
    AG_URL: 'https://gi.serverless-hub.com/',
    CREATE_SESSION_URL: 'http://swapi.agingames.com:3381/',
    FORWARD_GAME_URL: 'https://gci.serverless-hub.com/',

    //DG Game
    DG_API_URL: 'https://api.dg99web.com',
    DG_API_KEY: 'b6f17e19a8f44a37af3c6a343f9f6b60',
    DG_AGENT_NAME: 'DG04020808',

    //Dragoon Soft
    DS_API_URL: 'https://api.sysonline.club',
    DS_GAME_URL: 'https://link.sandsys.pw',
    DS_AGENT_ACCOUNT: 'AMB1002THB',
    DS_CHANNEL: '41504971',
    DS_CERT: 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDHlzNTtnBQ5hcXrR+Jq/PCUDSYT+h3j7giX7p3PKp35/vHsT7XXJw/a6edwytM6zWe+bloeJU+DSu7OWlZpT/VgrJCuFRW7McaHD628kH/U+Tl3VdJ0mDoPe4NHFaTxWSSN/SOags/m/fZJnbmQWRKPxVTlOFtY0Bh/B+6aQqzOwIDAQAB',
    DS_IMAGE_URL: 'https://amb-slot.s3-ap-southeast-1.amazonaws.com/ds-game',

    //PGSlot
    PGSOFT_IMAGE_URL: 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/pg-soft',
    PG_URL: 'https://m.pgjksonc.club/',
    PG_TOKEN: 'B4DB67CC-8BC4-42FE-A163-D26E5AD5F789',
    PG_SECRETKEY: '1610B27B9F1842F2B077BEAE01E891A0',

    //Ameba
    AMEBA_URL: 'https://api.fafafa3388.com',
    AMEBA_SITE_ID: '2243',
    AMEBA_SECRET_KEY: 'KmjqF9sFJ3PV+YJDrTKYSmqGkS/lOwSE',
    AMEBA_IMAGE_URL: 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/ameba-game',

    //SlotXO
    SLOT_XO_FORWARD_GAME: 'http://www.gwc688.net/playGame?',
    SLOT_XO_HOST: 'http://api688.net/seamless',
    SLOT_XO_SECRET_KEY: 'rtxtet3kx5r8a',
    SLOT_XO_APP_ID: 'F1YU',

    //Ganapati
    GANAPATI_URL: 'https://api.ayitang.com/',
    GANAPATI_SECRET_KEY: 'K5XRSXYeZCrpkrvuPVfZdc9fg',
    GANAPATI_AGENT_NAME: 'askmebet',
    GANAPATI_AGENT_CODE: 'mgm',

    //Live22
    GAMING_SOFT_URL: 'https://cw.889tech.net/CommonWalletOPWS.svc/OPAPI/',
    GAMING_SOFT_OPERATOR_ID: 'mgm99winTHB',
    GAMING_SOFT_SECRET: '8E63',

    //Spade Slot
    SPADE_LOGIN_URL: 'http://lobby.bigmoose88.com/mgm99/auth',
    SPADE_API_URL: 'http://merchantapi.hugedolphin.com/api',
    SPADE_MERCHANT_CODE: 'MGM99',

    //AMB Game Card
    AMB_GAME_API: 'https://api.ambgames.com',
    AMB_GAME_PARTNER: '5d39211a26bc2200175850aa',
    AMB_GAME_GAME_URL: 'https://games.ambpoker.com',

    //Lotto
    AMB_LOTTO_URL: 'https://amblotto.api-hub.com/',
    AMB_LOTTO_CLIENT: 'mgm99win',
    AMB_LOTTO_API_KEY: '55038179-fe09-4e28-b438-59a38d791a90',

    AMB_GAME_API_2: 'https://ap.ambpoker-api.com',
    AMB_GAME_SECRET_KEY_2: '8fc32f08da987f97e2bac2f83ee417ca',
    AMB_GAME_AGENT_2: 'mgm'

}


module.exports = client;