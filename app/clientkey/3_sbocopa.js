const client = {

    //---------------//
    LICENSE: '5bf52116e60f036bf68df87b:5c45c2ed362d6a6ca9b25515:5de00fcbe55573755aee06f7:5df7aabf33f17e2348941039:5dfdf0873af52c5fef613e93:5e312bfa22c1dd4e02c4f019',
    ODD_ADJUSTMENT: 2,
    MAX_BACK_LIST: 40000000,
    MASTER_IP: 'isn-backend.api-hub.com',

    //AMBBO (Auto)
    URL_AMBBO: 'https://ambbo.co/login/auto',
    URL_ENCRYPTED: 'https://ambbet-topup.serverless-hub.com/secure/encrypted',
    URL_PAYMENT_GATEWAY_UPDATE_PASSWORD: 'http://topup-casino.api-hub.com:22110/external/customer/update-password',

    //M2 Sport
    MUAY_STEP2_HOST: 'http://www.muaystep2.com',
    MUAY_STEP2_OPCODE: 'dd88betsgw',
    MUAY_STEP2_PRIVATE_KEY: 'MuaY18',
    MUAY_STEP2_OPAGENT_ID: 'dd88betsgwagent',
    MUAY_STEP2_PREFIX: 'dd88betsgw_',

    //Pretty Gaming
    PRETTY_API_URL: 'https://api-prod.aghippo168.com/apiRoute/member/loginRequest',
    PRETTY_SECRET_KEY: '3120ada2-7b9a-94b5-39ea-517d39f74b00',
    PRETTY_AGENT: 'sbocopa',

    //Sexy bacara
    SEXY_HOST: 'https://api.onlinegames22.com',
    SEXY_AGENT_ID: 'sbocopa',
    SEXY_CERT: 'xMAayLoz5Cydy28y5NO',

    //SA Game
    SAG_URL: 'http://api.sa-apisvr.net/api/api.aspx',
    SAG_LOGIN_URL: 'https://sbc.sa-api3.net/app.aspx',
    SAG_SECRET_KEY: '57D20FDA918B428AB3C02ABD697B53B8',
    SAG_ENCRYPT_KEY: 'g9G16nTs',
    SAG_MD5_KEY: 'GgaIMaiNNtg',
    SAG_LOBBY_CODE: 'A938',

    //AG Game
    AG_LOGIN_URL: 'EB7_AGIN',
    AG_ENCRYPT_KEY: '7rPaAz8C',
    AG_MD5_KEY: 'HX4vtEcTaFHd',
    AG_PASS: 'aggpass123',
    AG_URL: 'https://gi.serverless-hub.com/',
    CREATE_SESSION_URL: 'http://swapi.agingames.com:3381/',
    FORWARD_GAME_URL: 'https://gci.serverless-hub.com/',

    //DG Game
    DG_API_URL: 'https://api.dg99web.com',
    DG_API_KEY: '64834ccd52ee464fb1cc7fb3a449c92e',
    DG_AGENT_NAME: 'DG04020806',

    //Dragoon Soft
    DS_API_URL: 'https://api.sysonline.club',
    DS_GAME_URL: 'https://link.sandsys.pw',
    DS_AGENT_ACCOUNT: 'AMB1014THB',
    DS_CHANNEL: '78875126',
    DS_CERT: 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDm21XbXe6xJgSaHqpbusFcF/lLs/r1L2j+P/PE+3hHXEDXLth4/zbEoOvMMxycyPifBBsXMF83P0wbl6jsBKuSB6UUYPeYWBOt8e4vNGFLmX1msWwUVp++wZrkI3QVzkU7Flm8AiRJNYig6B84HXKY8ESBzddHu7WNfvN2QSvLPQIDAQAB',
    DS_IMAGE_URL: 'https://amb-slot.s3-ap-southeast-1.amazonaws.com/ds-game',

    //PGSlot
    PGSOFT_IMAGE_URL: 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/pg-soft',
    PG_URL: 'https://m.pgjksonc.club/',
    PG_TOKEN: '65EB2896-1165-471E-BB34-7789D1CAB854',
    PG_SECRETKEY: '5090226C3E6B4B7A8FB7E2533DCB479C',

    //Ameba
    AMEBA_URL: 'https://api.fafafa3388.com',
    AMEBA_SITE_ID: '2241',
    AMEBA_SECRET_KEY: 'L8rZkSETo/AlblM3407SOKHhDwE1+Qp4',

    //SlotXO
    SLOT_XO_FORWARD_GAME: 'http://www.gwc688.net/playGame?',
    SLOT_XO_HOST: 'http://api688.net/seamless',
    SLOT_XO_SECRET_KEY: 'nisnk6y5jzhws',
    SLOT_XO_APP_ID: 'F1YY',

    //Ganapati
    GANAPATI_URL: 'https://api.ayitang.com/',
    GANAPATI_SECRET_KEY: 'K5XRSXYeZCrpkrvuPVfZdc9fg',
    GANAPATI_AGENT_NAME: 'askmebet',
    GANAPATI_AGENT_CODE: 'sbocopa',

    //Live22
    GAMING_SOFT_URL: 'https://cw.889tech.net/CommonWalletOPWS.svc/OPAPI/',
    GAMING_SOFT_OPERATOR_ID: 'sbocopaTHB',
    GAMING_SOFT_SECRET: 'A21F',

    //Spade Slot
    SPADE_LOGIN_URL: 'http://lobby.bigmoose88.com/sbocopa/auth',
    SPADE_API_URL: 'http://merchantapi.hugedolphin.com/api',
    SPADE_MERCHANT_CODE: 'SBOCOPA',

    //AMB Game Card
    AMB_GAME_API: 'https://api.ambgames.com',
    AMB_GAME_PARTNER: '5d30a921d2fe60001775eecd',
    AMB_GAME_GAME_URL: 'https://games.ambpoker.com',
    AMEBA_IMAGE_URL: 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/ameba-game',

    //Lotto
    AMB_LOTTO_URL: 'https://amblotto.api-hub.com/',
    AMB_LOTTO_CLIENT: 'sbocopa',
    AMB_LOTTO_API_KEY: '10debb64-427b-4d1f-bd3b-d46345fe780b',

    AMB_GAME_API_2: 'https://ap.ambpoker-api.com',
    AMB_GAME_SECRET_KEY_2: '5484b2d924ab297a32dc330427535663',
    AMB_GAME_AGENT_2: 'sbo'
}


module.exports = client;