const client = {

    //---------------//
    LICENSE: '5bf52116e60f036bf68df87b:5c45c2ed362d6a6ca9b25515:5de00fcbe55573755aee06f7:5df7aabf33f17e2348941039:5dfdf0873af52c5fef613e93:5e312bfa22c1dd4e02c4f019',
    ODD_ADJUSTMENT: 2,
    MAX_BACK_LIST: 40000000,
    MASTER_IP: 'isn-backend.api-hub.com',

    //AMBBO (Auto)
    URL_AMBBO: 'https://ambbo.co/login/auto',
    URL_ENCRYPTED: 'https://ambbet-topup.serverless-hub.com/secure/encrypted',
    URL_PAYMENT_GATEWAY_UPDATE_PASSWORD: 'http://topup-casino.api-hub.com:22110/external/customer/update-password',

    //M2 Sport
    MUAY_STEP2_HOST: 'http://www.muaystep2.com',
    MUAY_STEP2_OPCODE: 'bet123sgw',
    MUAY_STEP2_PRIVATE_KEY: 'MuaY18',
    MUAY_STEP2_OPAGENT_ID: 'bet123sgwagent',
    MUAY_STEP2_PREFIX: 'bet123sgw_',

    //Pretty Gaming
    PRETTY_API_URL: 'https://api-prod.aghippo168.com/apiRoute/member/loginRequest',
    PRETTY_SECRET_KEY: 'e3c355fa-7077-f1bb-781a-6d1794801fcd',
    PRETTY_AGENT: '123bet',

    //Sexy bacara
    SEXY_HOST: 'https://api.onlinegames22.com',
    SEXY_AGENT_ID: '123bet',
    SEXY_CERT: 'OWJD8JYhVlwVIvsak6Z',

    //SA Game
    SAG_URL: 'http://api.sa-apisvr.net/api/api.aspx',
    SAG_LOGIN_URL: 'https://123.sa-api4.net/app.aspx',
    SAG_SECRET_KEY: 'EF3F6F6743B54723BBFE2E5D091636F8',
    SAG_ENCRYPT_KEY: 'g9G16nTs',
    SAG_MD5_KEY: 'GgaIMaiNNtg',
    SAG_LOBBY_CODE: 'A1197',

    //AG Game
    AG_LOGIN_URL: 'EB7_AGIN',
    AG_ENCRYPT_KEY: '7rPaAz8C',
    AG_MD5_KEY: 'HX4vtEcTaFHd',
    AG_PASS: 'aggpass123',
    AG_URL: 'https://gi.serverless-hub.com/',
    CREATE_SESSION_URL: 'http://swapi.agingames.com:3381/',
    FORWARD_GAME_URL: 'https://gci.serverless-hub.com/',

    //DG Game
    DG_API_URL: 'https://api.dg99web.com',
    DG_API_KEY: '49d1168a79f64534828d45c3402ab3f5',
    DG_AGENT_NAME: 'DG04020810',

    //Dragoon Soft
    DS_API_URL: 'https://api.sysonline.club',
    DS_GAME_URL: 'https://link.sandsys.pw',
    DS_AGENT_ACCOUNT: 'AMB1009THB',
    DS_CHANNEL: '15506156',
    DS_CERT: 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDAI3OMVmWu6XK+sQzfg8II+Wt6oE0G7JiVWliSbtTsouFvfKwQhAU8jjavnA+FWfSfIQot7DdvXdVHhQXqHYKsIU73+p6agMVdCE8cUGw9BeO62OmBPuRfTcLOpUptY5+jD6HVp3uKtLIqz86gD9SF/ep5FAHEIsy9lEBLT/eoQQIDAQAB',
    DS_IMAGE_URL: 'https://amb-slot.s3-ap-southeast-1.amazonaws.com/ds-game',

    //PGSlot
    PGSOFT_IMAGE_URL: 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/pg-soft',
    PG_URL: 'https://m.pgjksonc.club/',
    PG_TOKEN: 'A559F291-681B-43E3-8F12-F910EF93F452',
    PG_SECRETKEY: 'EBD3DCA968154059BB2DDCFEBFB96EBF',

    //Ameba
    AMEBA_URL: 'https://api.fafafa3388.com',
    AMEBA_SITE_ID: '2321',
    AMEBA_SECRET_KEY: 'WhJDg70oIzuOAxnPTk/w/9XeJlGMS+ne',
    AMEBA_IMAGE_URL: 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/ameba-game',

    //SlotXO
    SLOT_XO_FORWARD_GAME: 'http://www.gwc688.net/playGame?',
    SLOT_XO_HOST: 'http://api688.net/seamless',
    SLOT_XO_SECRET_KEY: '8tdoaz4nn4obe',
    SLOT_XO_APP_ID: 'F1Z2',

    //Ganapati
    GANAPATI_URL: 'https://api.ayitang.com/',
    GANAPATI_SECRET_KEY: 'K5XRSXYeZCrpkrvuPVfZdc9fg',
    GANAPATI_AGENT_NAME: 'askmebet',
    GANAPATI_AGENT_CODE: 'bet123',

    //Live22
    GAMING_SOFT_URL: 'https://cw.889tech.net/CommonWalletOPWS.svc/OPAPI/',
    GAMING_SOFT_OPERATOR_ID: '123betTHB',
    GAMING_SOFT_SECRET: '8175',

    //Spade Slot
    SPADE_LOGIN_URL: 'http://lobby.bigmoose88.com/123bet/auth',
    SPADE_API_URL: 'http://merchantapi.hugedolphin.com/api',
    SPADE_MERCHANT_CODE: '123BET',

    //AMB Game Card
    AMB_GAME_API: 'https://api.ambgames.com',
    AMB_GAME_PARTNER: '5d6e6f21f93cf70017f7150a',
    AMB_GAME_GAME_URL: 'https://games.ambpoker.com',

    //Lotto
    AMB_LOTTO_URL: 'https://amblotto.api-hub.com/',
    AMB_LOTTO_CLIENT: '123bet',
    AMB_LOTTO_API_KEY: 'b51a6192-4d9d-4bb4-8c5d-e21148f1a52',

    AMB_GAME_API_2: 'https://ap.ambpoker-api.com',
    AMB_GAME_SECRET_KEY_2: '60cb77230e610cd401349f95b12892f6',
    AMB_GAME_AGENT_2: '12b'

}


module.exports = client;