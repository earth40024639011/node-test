const client = {

    //---------------//
    LICENSE: '5e3041183173f51a5311da16:5e3041873173f51a5311da1b:5e3046db9d25241aa26d15ac:5e30476751c2e11a98cd38cd:5e3047cd1fe2d31a9294d4a5:5e6e8117f203701a1130d426:5e6e82405883f91a00d9752e:5e6f26ac4566e01a4fb46fdb:5e7b13408bb291343e6e605f',
    ODD_ADJUSTMENT: 2,
    MAX_BACK_LIST: 40000000,
    MASTER_IP: 'isn-backend.api-hub.com',

    //AMBBO (Auto)
    URL_AMBBO: 'https://ambbo.co/login/auto',
    URL_ENCRYPTED: 'https://ambbet-topup.serverless-hub.com/secure/encrypted',
    URL_PAYMENT_GATEWAY_UPDATE_PASSWORD: 'http://topup-casino.api-hub.com:22110/external/customer/update-password',

    //M2 Sport
    MUAY_STEP2_HOST: 'http://www.muaystep2.com',
    MUAY_STEP2_OPCODE: 'goal6969sgw',
    MUAY_STEP2_PRIVATE_KEY: 'MuaY18',
    MUAY_STEP2_OPAGENT_ID: 'goal6969sgwagent',
    MUAY_STEP2_PREFIX: 'goal6969sgw_',

    //Pretty Gaming  *****
    PRETTY_API_URL: 'https://api-prod.aghippo168.com/apiRoute/member/loginRequest',
    PRETTY_SECRET_KEY: 'f0de3ab2-6ab6-40f2-734c-18e81f4f14ef',
    PRETTY_AGENT: 'goal6969',

    //Sexy bacara
    SEXY_HOST: 'https://api.onlinegames22.com',
    SEXY_AGENT_ID: 'goal6969',
    SEXY_CERT: 'KykE9WAYKArBThVWiSY',

    //SA Game
    SAG_URL: 'http://api.sa-apisvr.net/api/api.aspx',
    SAG_LOGIN_URL: 'https://go6.sa-api5.net/app.aspx',
    SAG_SECRET_KEY: 'DA411229B0B145778CFC804B511C355B',
    SAG_ENCRYPT_KEY: 'g9G16nTs',
    SAG_MD5_KEY: 'GgaIMaiNNtg',
    SAG_LOBBY_CODE: 'A1738',

    //AG Game
    AG_LOGIN_URL: 'EB7_AGIN',
    AG_ENCRYPT_KEY: '7rPaAz8C',
    AG_MD5_KEY: 'HX4vtEcTaFHd',
    AG_PASS: 'aggpass123',
    AG_URL: 'https://gi.serverless-hub.com/',
    CREATE_SESSION_URL: 'http://swapi.agingames.com:3381/',
    FORWARD_GAME_URL: 'https://gci.serverless-hub.com/',

    //DG Game
    DG_API_URL: 'https://api.dg99web.com',
    DG_API_KEY: 'd9bc7a462cc14f6a8b9892da36fe9e0d',
    DG_AGENT_NAME: 'DG10040105',

    //Dragon Soft
    DS_API_URL: 'https://api.sysonline.club',
    DS_GAME_URL: 'https://link.sandsys.pw',
    DS_AGENT_ACCOUNT: 'AMB1018THB',
    DS_CHANNEL: '96247682',
    DS_CERT: 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDmXaN7UJmuZxQZDtWCcmEks9zkZn+g2CAZYZQumIGJMU22iZU/Qa9AVdkSlAUy/ozHrnJqWGJg0MmmwvfwilQrmN9PrzKC2zoKNCl/ocwuuaoE9/Tr/qEEqXpIy5cOxEXLFpP2Q8QnLyg+t5JtFyrP3SNkKgsTIW3HD1NjuiQerwIDAQAB',
    DS_IMAGE_URL: 'https://amb-slot.s3-ap-southeast-1.amazonaws.com/ds-game',

    //PGSlot   *****
    PGSOFT_IMAGE_URL: 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/pg-soft',
    PG_URL: 'https://m.pgjksonc.club/',
    PG_TOKEN: '8BE5192C-B09A-4A55-938B-241FEE0A6DCA',
    PG_SECRETKEY: '1324EECE379D479BA50771803B4851B0',

    //Ameba   ****
    AMEBA_URL: 'https://api.fafafa3388.com',
    AMEBA_SITE_ID: '2488',
    AMEBA_SECRET_KEY: 'feEK8aWKlMPAO6b8J5mI5zYulJV+8diB',
    AMEBA_IMAGE_URL: 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/ameba-game',

    //SlotXO
    SLOT_XO_FORWARD_GAME: 'http://www.gwc688.net/playGame?',
    SLOT_XO_HOST: 'http://api688.net/seamless',
    SLOT_XO_SECRET_KEY: 'a199jj6kuawja',
    SLOT_XO_APP_ID: 'F1Z8',

    //Ganapati
    GANAPATI_URL: 'https://api.ayitang.com/',
    GANAPATI_SECRET_KEY: 'K5XRSXYeZCrpkrvuPVfZdc9fg',
    GANAPATI_AGENT_NAME: 'askmebet',
    GANAPATI_AGENT_CODE: 'goal69',

    //Live22
    GAMING_SOFT_URL: 'https://cw.889tech.net/CommonWalletOPWS.svc/OPAPI/',
    GAMING_SOFT_OPERATOR_ID: 'goal6969THB',
    GAMING_SOFT_SECRET: 'B437',

    //Spade Slot  
    SPADE_LOGIN_URL: 'http://lobby.bigmoose88.com/GOAL696/auth',
    SPADE_API_URL: 'http://merchantapi.hugedolphin.com/api',
    SPADE_MERCHANT_CODE: 'GOAL696',

    //AMB Game Card  
    AMB_GAME_API: 'https://api.ambgames.com',
    AMB_GAME_PARTNER: '5da45b29db8c1c003d3e1832',
    AMB_GAME_GAME_URL: 'https://games.ambpoker.com',

    //Lotto  
    AMB_LOTTO_URL: 'https://amblotto.api-hub.com/',
    AMB_LOTTO_CLIENT: 'goal6969',
    AMB_LOTTO_API_KEY: '84932048-4329-4d17-bcc6-e177c870c040',

    AMB_GAME_API_2: 'https://ap.ambpoker-api.com',
    AMB_GAME_SECRET_KEY_2: 'af0aba1dcc06c2e5caa46c678e4fd279',
    AMB_GAME_AGENT_2: 'go69'

}


module.exports = client;