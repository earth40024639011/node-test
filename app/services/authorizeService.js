const express = require('express');
const router = express.Router();
const jwt = require('../common/jwt');
const TokenModel = require("../models/token.model");
const RedisService = require('../common/redisService');
const CLIENT_NAME = process.env.CLIENT_NAME || 'SPORTBOOK88';
const async = require("async");

module.exports = {

    isValidToken: (username,uid, callback) => {
        TokenModel.findOne({username: username}, function (err, response) {
            if (err) {
                callback(err, null);
                return;
            }
            if(response) {
                if(response.uniqueId !== uid){
                    callback(1005, null);
                }else {
                    callback(null, response);
                }
            } else {
                callback(1003, response);
            }

        });

        // async.waterfall([
        //         (callback) => {
        //             RedisService.getIsMaintenance(CLIENT_NAME, (error, isMaintenance) => {
        //                 if (error) {
        //                     callback(error, null);
        //                 } else {
        //                     callback(null, isMaintenance);
        //                 }
        //             })
        //         },
        //         (isMaintenance, callback) => {
        //             if (isMaintenance) {
        //                 TokenModel.remove({}, (err, data) => {
        //                     if (err) {
        //                         callback(null, isMaintenance);
        //                     } else {
        //                         callback(null, isMaintenance);
        //                     }
        //                 });
        //             }
        //         }
        //     ],
        //     (error, result) => {
        //         TokenModel.findOne({username: username}, function (err, response) {
        //             if (err) {
        //                 callback(err, null);
        //             }
        //             if(response) {
        //                 if(response.uniqueId !== uid){
        //                     callback(1005, null);
        //                 }else {
        //                     callback(null, response);
        //                 }
        //             } else {
        //                 callback(1003, response);
        //             }
        //
        //         });
        //     }
        // );
    },
};