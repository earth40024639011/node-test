const express = require('express');
const router = express.Router();
const request = require('request');
const moment = require('moment');
const crypto = require('crypto');
const async = require("async");
const config = require('config');
const MemberService = require('../../common/memberService');
const Joi = require('joi');
const DateUtils = require('../../common/dateUtils');
const roundTo = require('round-to');
const mongoose = require('mongoose');
const AgentGroupModel = require('../../models/agentGroup.model.js');
const NumberUtils = require('../../common/numberUtils1');
const AgentService = require('../../common/agentService');
const MemberModel = require("../../models/member.model");
const _ = require('underscore');
const SpadeService = require('../../common/spadeService');
const ReportService = require('../../common/reportService');

const SpadeBetHistoryModel = require('../../models/spadeBetHistory2.model.js');
const BetTransactionModel = require('../../models/betTransaction.model.js');

const ActiveSpadeTransactionModel = require('../../models/activeSpadeTransaction2.model.js');

const DRAW = "DRAW";
const WIN = "WIN";
const HALF_WIN = "HALF_WIN";
const LOSE = "LOSE";
const HALF_LOSE = "HALF_LOSE";


function random(low, high) {
    return Math.floor(Math.random() * (high - low + 1) + low)
}
function generateBetId() {
    return 'BET_GS' + new Date().getTime()+random(1000000,9999999);
}

router.get('/getGameList', (req, res) => {

    return res.send({data: SpadeService.callGetGameList(), code: 0});

});


const transferSchema = Joi.object().keys({
    transferId: Joi.string().required(),
    acctId: Joi.string().required(),
    currency: Joi.string().required(),
    amount: Joi.number().required(),
    type: Joi.number().required(),
    channel: Joi.string().required(),
    gameCode: Joi.string().required(),
    ticketId: Joi.string().allow(''),
    referenceId: Joi.string().allow(''),
    specialGame: {
        type: Joi.string(),
        count: Joi.number(),
        sequence: Joi.number()
    },
    refTicketIds: Joi.array().items(Joi.string()),
    merchantCode: Joi.string().required(),
    serialNo: Joi.string().required()
});

//2. get_balance
router.post('/callback', (req, res) => {

    // getBodyFromToken(req.headers)
    let requestBody = req.body;


    console.log(" ================ request Header ================ ");
    console.log(JSON.stringify(req.headers))
    console.log(" ================ request Body ================ ");
    console.log(requestBody)


    let body = {
        transferId: requestBody.transferId,
        acctId: requestBody.acctId,
        currency: requestBody.currency,
        amount: requestBody.amount,
        type: requestBody.type,
        channel: requestBody.channel,
        gameCode: requestBody.gameCode,
        ticketId: requestBody.ticketId,
        referenceId: requestBody.referenceId,
        specialGame: requestBody.specialGame,
        refTicketIds: requestBody.refTicketIds,
        createdDate: DateUtils.getCurrentDate()
    };

    let model = new SpadeBetHistoryModel(body);
    model.save((err, agentResponse) => {

        if (err) {
            // callback(err, null);
        } else {
            // callback(null, agentResponse);
        }
    });

    if (SpadeService.getMerchantCode() != requestBody.merchantCode) {
        return res.send({
            merchantCode: requestBody.merchantCode,
            msg: "Merchant Not Found",
            code: 10113,
            serialNo: requestBody.serialNo
        });
    }

    let API_ACTION = req.header('API');


    if (API_ACTION === 'authorize') {

        // let validateAgent = Joi.validate(requestBody, callbackGetBalanceSchema);
        // if (validateAgent.error) {
        //     return res.send({
        //         codeId: CODE_PARAMETER_ERROR,
        //         token: requestBody.token
        //     });
        // }
        console.log('SPADE call authorizeFunction');
        authorizeFunction(requestBody, res);

    } else if (API_ACTION === 'getBalance') {

        // let validateAgent = Joi.validate(requestBody, callbackGetBalanceSchema);
        // if (validateAgent.error) {
        //     return res.send({
        //         codeId: CODE_PARAMETER_ERROR,
        //         token: requestBody.token
        //     });
        // }

        getBalanceFunction(requestBody, res);

    } else if (API_ACTION === 'transfer') {

        console.log(requestBody);
        let validate = Joi.validate(requestBody, transferSchema);
        if (validate.error) {

            console.log(validate.error.details)
            return res.send({
                merchantCode: SpadeService.getMerchantCode(),
                msg: "Missing Parameters",
                code: 105,
                serialNo: requestBody.serialNo
            });
        }

        const PLACE_BET = 1, CANCEL_BET = 2, PAYOUT = 4, BONUS = 7;

        if (requestBody.type === PLACE_BET) {

            betFunction(requestBody, res)

        } else if (requestBody.type === CANCEL_BET) {

            cancelFunction(requestBody, res)

        } else if (requestBody.type === PAYOUT) {

            settleFunction(requestBody, res);
        } else {
            return res.send({
                merchantCode: requestBody.merchantCode,
                msg: "Invalid Parameters",
                code: 106,
                serialNo: requestBody.serialNo
            });
        }

    } else {
        return res.send({
            merchantCode: requestBody.merchantCode,
            msg: "Invalid Parameters",
            code: 106,
            serialNo: requestBody.serialNo
        });
    }

});

function authorizeFunction(requestBody, res) {
    console.log('findByUserNameForPartnerService ', requestBody);
    MemberService.findByUserNameForPartnerService(requestBody.acctId, function (err, memberResponse) {
        console.log('SPADE err >>', err);
        if (err) {
            return res.send({
                merchantCode: requestBody.merchantCode,
                msg: "System Error",
                code: 1,
                serialNo: requestBody.serialNo
            });
        }
      console.log('SPADE memberResponse >>', memberResponse);
        if (memberResponse) {

            let credit = roundTo(memberResponse.creditLimit + memberResponse.balance, 2);
          console.log('SPADE credit >>', credit);
            return res.send({
                acctInfo: {
                    userName: memberResponse.username_lower,
                    currency: "THB",
                    acctId: requestBody.acctId,
                    balance: credit,
                    siteId: SpadeService.getMerchantCode()
                },
                merchantCode: requestBody.merchantCode,
                msg: "success",
                code: 0,
                serialNo: requestBody.serialNo
            });
        } else {
          console.log('SPADE !memberResponse >>', {merchantCode: requestBody.merchantCode, msg: "Invalid Acct ID", code: 113, serialNo: requestBody.serialNo});

            return res.send({
                merchantCode: requestBody.merchantCode,
                msg: "Invalid Acct ID",
                code: 113,
                serialNo: requestBody.serialNo
            });
        }
    });
}

function getBalanceFunction(requestBody, res) {

    MemberService.findByUserNameForPartnerService(requestBody.acctId, function (err, memberResponse) {
        if (err) {
            return res.send({
                merchantCode: requestBody.merchantCode,
                msg: "System Error",
                code: 1,
                serialNo: requestBody.serialNo
            });
        }
        if (memberResponse) {
            let credit = roundTo(memberResponse.creditLimit + memberResponse.balance, 2);

            return res.send({
                acctInfo: {
                    userName: memberResponse.username_lower,
                    currency: "THB",
                    acctId: requestBody.acctId,
                    balance: credit
                },
                merchantCode: requestBody.merchantCode,
                msg: "success",
                code: 0,
                serialNo: requestBody.serialNo
            });
        } else {
            return res.send({
                merchantCode: requestBody.merchantCode,
                msg: "Invalid Acct ID",
                code: 113,
                serialNo: requestBody.serialNo
            });
        }
    });
}


function betFunction(requestBody, res) {


    let betAmount = roundTo(Number.parseFloat(requestBody.amount), 2);

    async.series([callback => {

        MemberService.findByUserNameForPartnerService(requestBody.acctId, function (err, memberResponse) {
          console.log('spade error > ', err);
            if (err) {
                callback(err, null);
                return;
            }
            if (memberResponse) {

                let condition = {
                    'memberUsername': memberResponse.username_lower,
                    'slot.transferId': requestBody.transferId
                };

                ActiveSpadeTransactionModel.findOne(condition).lean().exec(function (err, response) {
                  console.log('spade error 2 > ', err);
                  console.log('spade response > ', response);
                    if (err) {
                        callback(err, null);
                        return
                    }
                    if (response) {
                        callback(900409, memberResponse)
                    } else {
                        if (roundTo((memberResponse.creditLimit + memberResponse.balance), 2) < betAmount) {
                            callback(900605, memberResponse);
                        } else {
                            callback(null, memberResponse);
                        }
                    }
                });

            } else {
                callback(900404, null);
            }
        });

    }], (err, asyncResponse) => {

        if (err) {
            if (err == 900404) {
                return res.send({
                    transferId: requestBody.transferId,
                    acctId: requestBody.acctId,
                    balance: 0,
                    merchantCode: requestBody.merchantCode,
                    msg: "Invalid Acct ID",
                    code: 113,
                    serialNo: requestBody.serialNo
                });
            } else if (err == 900409) {
                let memberInfo = asyncResponse[0];
                let credit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2)

                return res.send({
                    transferId: requestBody.transferId,
                    acctId: requestBody.acctId,
                    balance: credit,
                    merchantCode: requestBody.merchantCode,
                    msg: "Duplicated Serial NO.",
                    code: 107,
                    serialNo: requestBody.serialNo
                });

            } else if (err == 900605) {
                let memberInfo = asyncResponse[0];
                let credit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2);

                return res.send({
                    transferId: requestBody.transferId,
                    acctId: requestBody.acctId,
                    balance: credit,
                    merchantCode: requestBody.merchantCode,
                    msg: "Insufficient Balance",
                    code: 50110,
                    serialNo: requestBody.serialNo
                });

            } else {
                return res.send({
                    merchantCode: requestBody.merchantCode,
                    msg: "System Error",
                    code: 1,
                    serialNo: requestBody.serialNo
                });
            }
        }

        let memberInfo = asyncResponse[0];
        let credit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2);


        AgentGroupModel.findById(memberInfo.group).select('_id type parentId shareSetting commissionSetting').populate({
            path: 'parentId',
            select: '_id type parentId shareSetting commissionSetting name',
            populate: {
                path: 'parentId',
                select: '_id type parentId shareSetting commissionSetting name',
                populate: {
                    path: 'parentId',
                    select: '_id type parentId shareSetting commissionSetting name',
                    populate: {
                        path: 'parentId',
                        select: '_id type parentId shareSetting commissionSetting name',
                        populate: {
                            path: 'parentId',
                            select: '_id type parentId shareSetting commissionSetting name',
                            populate: {
                                path: 'parentId',
                                select: '_id type parentId shareSetting commissionSetting name',
                            }
                        }
                    }
                }
            }

        }).exec(function (err, agentGroups) {

            let body = {
                memberId: memberInfo._id,
                memberParentGroup: memberInfo.group,
                memberUsername: memberInfo.username_lower,
                betId: generateBetId(),
                slot: {
                    transferId: requestBody.transferId,
                    acctId: requestBody.acctId,
                    currency: requestBody.currency,
                    amount: requestBody.amount,
                    type: requestBody.type,
                    channel: requestBody.channel,
                    gameCode: requestBody.gameCode,
                    gameName : SpadeService.getGameName(requestBody.gameCode),
                    ticketId: requestBody.ticketId,
                    referenceId: requestBody.referenceId,
                },
                amount: betAmount,
                memberCredit: betAmount * -1,
                payout: betAmount,
                commission: {},
                status: 'RUNNING',
                gameDate: DateUtils.getCurrentDate(),
                createdDate: DateUtils.getCurrentDate(),
                currency: memberInfo.currency
            };

            prepareCommission(memberInfo, body, agentGroups, null, 0, 0);

            let betTransactionModel = new ActiveSpadeTransactionModel(body);

            betTransactionModel.save(function (err, response) {
                console.log('active save ============= ',response);

                if (err) {
                    if (err.code == 11000) {
                        return res.send({
                            transferId: requestBody.transferId,
                            acctId: requestBody.acctId,
                            balance: credit,
                            merchantCode: requestBody.merchantCode,
                            msg: "Duplicated Serial NO.",
                            code: 107,
                            serialNo: requestBody.serialNo
                        });
                    }
                    return res.send({
                        merchantCode: requestBody.merchantCode,
                        msg: "System Error",
                        code: 1,
                        serialNo: requestBody.serialNo
                    });
                }

                MemberService.updateBalance2(memberInfo.username_lower, (betAmount * -1), response.betId, 'SPADE_GAME', 'BET|'+requestBody.transferId, function (err, updateBalanceResponse) {
                    if (err) {
                        return res.send({
                            merchantCode: requestBody.merchantCode,
                            msg: "System Error",
                            code: 1,
                            serialNo: requestBody.serialNo
                        });
                    } else {

                        return res.send({
                            transferId: requestBody.transferId,
                            merchantTxId: response.betId,
                            acctId: requestBody.acctId,
                            balance: updateBalanceResponse.newBalance,
                            merchantCode: requestBody.merchantCode,
                            msg: "success",
                            code: 0,
                            serialNo: requestBody.serialNo
                        });

                    }
                });
            });
        });

    });
}

function cancelFunction(requestBody, res) {

    async.series([callback => {

        MemberService.findByUserNameForPartnerService(requestBody.acctId, function (err, memberResponse) {
            if (err) {
                callback(err, null);
                return;
            }
            if (memberResponse) {
                callback(null, memberResponse);
            } else {
                callback(900404, null);
            }
        });

    }], (err, asyncResponse) => {

        if (err) {
            if (err == 900404) {
                return res.send({
                    transferId: requestBody.transferId,
                    acctId: requestBody.acctId,
                    balance: 0,
                    merchantCode: requestBody.merchantCode,
                    msg: "Invalid Acct ID",
                    code: 113,
                    serialNo: requestBody.serialNo
                });
            } else {
                return res.send({
                    merchantCode: requestBody.merchantCode,
                    msg: "System Error",
                    code: 1,
                    serialNo: requestBody.serialNo
                });
            }
        }


        let memberInfo = asyncResponse[0];
        let credit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2);


        let condition = {
            'memberUsername': memberInfo.username_lower,
            'slot.transferId': requestBody.referenceId
        };


        ActiveSpadeTransactionModel.findOne(condition).exec(function (err, activeAmebaResponse) {
            if (err) {
                return res.send({
                    merchantCode: requestBody.merchantCode,
                    msg: "System Error",
                    code: 1,
                    serialNo: requestBody.serialNo
                });
            } else {

                if (!activeAmebaResponse) {
                    return res.send({
                        merchantCode: requestBody.merchantCode,
                        msg: "Reference No Not found",
                        code: 109,
                        serialNo: requestBody.serialNo
                    });
                }

                if (activeAmebaResponse.status === 'CANCELLED') {
                    return res.send({
                        transferId: requestBody.transferId,
                        acctId: requestBody.acctId,
                        balance: credit,
                        merchantCode: requestBody.merchantCode,
                        msg: "success",
                        code: 0,
                        serialNo: requestBody.serialNo
                    });
                }


                MemberService.updateBalance2(activeAmebaResponse.memberUsername, Math.abs(activeAmebaResponse.memberCredit), activeAmebaResponse.betId, 'SPADE_GAME', 'CANCEL_BET|'+requestBody.referenceId, function (err, creditResponse) {
                    if (err) {
                        return res.send({
                            merchantCode: requestBody.merchantCode,
                            msg: "System Error",
                            code: 1,
                            serialNo: requestBody.serialNo
                        });
                    } else {


                        ActiveSpadeTransactionModel.update({_id: activeAmebaResponse._id}, {status: 'CANCELLED'}, function (err, response) {

                            if (response.nModified == 1) {

                                //TODO queue

                                let updateBetTranBody = {
                                    'commission.superAdmin.winLoseCom': 0,
                                    'commission.superAdmin.winLose': 0,
                                    'commission.superAdmin.totalWinLoseCom': 0,

                                    'commission.company.winLoseCom': 0,
                                    'commission.company.winLose': 0,
                                    'commission.company.totalWinLoseCom': 0,

                                    'commission.shareHolder.winLoseCom': 0,
                                    'commission.shareHolder.winLose': 0,
                                    'commission.shareHolder.totalWinLoseCom': 0,

                                    'commission.senior.winLoseCom': 0,
                                    'commission.senior.winLose': 0,
                                    'commission.senior.totalWinLoseCom': 0,

                                    'commission.masterAgent.winLoseCom': 0,
                                    'commission.masterAgent.winLose': 0,
                                    'commission.masterAgent.totalWinLoseCom': 0,

                                    'commission.agent.winLoseCom': 0,
                                    'commission.agent.winLose': 0,
                                    'commission.agent.totalWinLoseCom': 0,

                                    'commission.member.winLoseCom': 0,
                                    'commission.member.winLose': 0,
                                    'commission.member.totalWinLoseCom': 0,
                                    'status': 'CANCELLED',
                                    'updatedDate':DateUtils.getCurrentDate(),
                                };

                                BetTransactionModel.update({
                                    betId: activeAmebaResponse.betId
                                }, updateBetTranBody, (err, data) => {
                                    if (err) {
                                        return res.send({
                                            merchantCode: requestBody.merchantCode,
                                            msg: "System Error",
                                            code: 1,
                                            serialNo: requestBody.serialNo
                                        });
                                    } else {

                                            return res.send({
                                                transferId: requestBody.transferId,
                                                acctId: requestBody.acctId,
                                                balance: creditResponse.newBalance,
                                                merchantCode: requestBody.merchantCode,
                                                msg: "success",
                                                code: 0,
                                                serialNo: requestBody.serialNo
                                            });
                                    }
                                });
                            } else {
                                return res.send({
                                    merchantCode: requestBody.merchantCode,
                                    msg: "System Error",
                                    code: 1,
                                    serialNo: requestBody.serialNo
                                });
                            }
                        });
                    }
                });

            }
        });


    });

}

function settleFunction(requestBody, res) {
    console.log('SPADE settleFunction');
    let betAmount = roundTo(Number.parseFloat(requestBody.amount), 2);

    async.series([callback => {
      console.log('SPADE BEF findByUserNameForPartnerService');
        MemberService.findByUserNameForPartnerService(requestBody.acctId, function (err, memberResponse) {
          console.log('SPADE memberResponse', memberResponse);
            if (err) {
                callback(err, null);
                return;
            }
            if (memberResponse) {
                callback(null, memberResponse);
            } else {
                callback(900404, null);
            }
        });

    }], (err, asyncResponse) => {

        if (err) {
            if (err == 900404) {
                return res.send({
                    transferId: requestBody.transferId,
                    acctId: requestBody.acctId,
                    balance: 0,
                    merchantCode: requestBody.merchantCode,
                    msg: "Invalid Acct ID",
                    code: 113,
                    serialNo: requestBody.serialNo
                });
            } else {
                return res.send({
                    merchantCode: requestBody.merchantCode,
                    msg: "System Error",
                    code: 1,
                    serialNo: requestBody.serialNo
                });
            }
        }

        let memberInfo = asyncResponse[0];

        let credit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2);

        async.series([callback => {

            let condition = {
                'memberUsername': memberInfo.username_lower,
                'slot.transferId': requestBody.referenceId
            };

            ActiveSpadeTransactionModel.findOne(condition).lean().exec(function (err, mainObj) {

                if (err) {

                    callback(err, null);
                    return
                }

                if (!mainObj) {
                    callback(900415, null);
                } else {
                    if (requestBody.specialGame && requestBody.specialGame.sequence > 0) {

                        let body = {
                            memberId: memberInfo._id,
                            memberParentGroup: memberInfo.group,
                            memberUsername: memberInfo.username_lower,
                            betId: generateBetId(),
                            slot: {
                                transferId: requestBody.transferId,
                                acctId: requestBody.acctId,
                                currency: requestBody.currency,
                                amount: requestBody.amount,
                                type: requestBody.type,
                                channel: requestBody.channel,
                                gameCode: requestBody.gameCode,
                                gameName : SpadeService.getGameName(requestBody.gameCode),
                                ticketId: requestBody.ticketId,
                                // referenceId: requestBody.referenceId,
                                // specialGame: requestBody.specialGame,
                                // refTicketIds: requestBody.refTicketIds
                            },
                            amount: 0,
                            memberCredit: 0,
                            payout: betAmount,
                            commission: mainObj.commission,
                            status: 'RUNNING',
                            gameDate: DateUtils.getCurrentDate(),
                            createdDate: DateUtils.getCurrentDate(),
                            currency: memberInfo.currency
                        };


                        console.log('ActiveSpadeTransactionModel', body);
                        console.log('===== ActiveSpadeTransactionModel ========');
                        let betTransactionModel = new ActiveSpadeTransactionModel(body);
                        console.log('betTransactionModel', betTransactionModel);
                        betTransactionModel.save(function (err, betObj) {

                            if (err) {
                                callback(err, null);
                            } else {
                                callback(null, betObj);
                            }
                        });
                    } else {

                        if (mainObj.status === 'DONE') {
                            callback(900409, null)
                        } else {
                            callback(null, mainObj)
                        }

                    }

                }

            });


        }], (err, asyncResponse) => {

            let activeBetObj = asyncResponse[0];

            if (err) {
                if (err == 900415) {
                    return res.send({
                        merchantCode: requestBody.merchantCode,
                        msg: "Reference No Not found",
                        code: 109,
                        serialNo: requestBody.serialNo
                    });
                }

                if (err == 900409) {
                    return res.send({
                        transferId: requestBody.transferId,
                        acctId: requestBody.acctId,
                        balance: credit,
                        merchantCode: requestBody.merchantCode,
                        msg: "success",
                        code: 0,
                        serialNo: requestBody.serialNo
                    });
                }
            }

            let winLose = betAmount - activeBetObj.amount;
            let betResult = winLose > 0 ? WIN : winLose < 0 ? LOSE : DRAW;

            const shareReceive = AgentService.prepareShareReceive(winLose, betResult, activeBetObj);

            updateAgentMemberBalance(shareReceive, activeBetObj.amount, requestBody.transferId, function (err, updateBalanceResponse) {
                if (err) {
                    if (err == 888) {
                        return res.send({
                            transferId: requestBody.transferId,
                            acctId: requestBody.acctId,
                            balance: credit,
                            merchantCode: requestBody.merchantCode,
                            msg: "success",
                            code: 0,
                            serialNo: requestBody.serialNo
                        });
                    }
                    return res.send({
                        merchantCode: requestBody.merchantCode,
                        msg: "System Error",
                        code: 1,
                        serialNo: requestBody.serialNo
                    });
                } else {


                    let updateBody = {
                        $set: {
                            'slot.specialGame': requestBody.specialGame,
                            'slot.ticketId': requestBody.ticketId,
                            'slot.referenceId': requestBody.referenceId,
                            'slot.refTicketIds': requestBody.refTicketIds,
                            'commission.superAdmin.winLoseCom': shareReceive.superAdmin.winLoseCom,
                            'commission.superAdmin.winLose': shareReceive.superAdmin.winLose,
                            'commission.superAdmin.totalWinLoseCom': shareReceive.superAdmin.totalWinLoseCom,

                            'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                            'commission.company.winLose': shareReceive.company.winLose,
                            'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                            'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                            'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                            'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                            'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                            'commission.senior.winLose': shareReceive.senior.winLose,
                            'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                            'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                            'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                            'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                            'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                            'commission.agent.winLose': shareReceive.agent.winLose,
                            'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                            'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                            'commission.member.winLose': shareReceive.member.winLose,
                            'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                            'validAmount': betAmount,
                            'betResult': betResult,
                            'isEndScore': true,
                            'status': 'DONE'
                        }
                    };

                    ActiveSpadeTransactionModel.update({_id: activeBetObj._id}, updateBody, function (err, response) {

                        if (response.nModified == 1) {

                            //TODO queue process
                            let body = {
                                memberId: activeBetObj.memberId,
                                memberParentGroup: activeBetObj.memberParentGroup,
                                memberUsername: activeBetObj.memberUsername,
                                betId: activeBetObj.betId,
                                slot: {spade: activeBetObj.slot},
                                amount: activeBetObj.amount,
                                memberCredit: activeBetObj.amount * -1,
                                payout: activeBetObj.amount,
                                gameType: 'TODAY',
                                acceptHigherPrice: false,
                                priceType: 'TH',
                                game: 'GAME',
                                source: 'SPADE_GAME',
                                status: 'DONE',
                                commission: activeBetObj.commission,
                                gameDate: DateUtils.getCurrentDate(),
                                createdDate: DateUtils.getCurrentDate(),
                                validAmount: activeBetObj.amount,
                                betResult: betResult,
                                isEndScore: true,
                                currency: memberInfo.currency
                            };

                            body.commission.superAdmin.winLoseCom = shareReceive.superAdmin.winLoseCom;
                            body.commission.superAdmin.winLose = shareReceive.superAdmin.winLose;
                            body.commission.superAdmin.totalWinLoseCom = shareReceive.superAdmin.totalWinLoseCom;

                            body.commission.company.winLoseCom = shareReceive.company.winLoseCom;
                            body.commission.company.winLose = shareReceive.company.winLose;
                            body.commission.company.totalWinLoseCom = shareReceive.company.totalWinLoseCom;

                            body.commission.shareHolder.winLoseCom = shareReceive.shareHolder.winLoseCom;
                            body.commission.shareHolder.winLose = shareReceive.shareHolder.winLose;
                            body.commission.shareHolder.totalWinLoseCom = shareReceive.shareHolder.totalWinLoseCom;

                            body.commission.senior.winLoseCom = shareReceive.senior.winLoseCom;
                            body.commission.senior.winLose = shareReceive.senior.winLose;
                            body.commission.senior.totalWinLoseCom = shareReceive.senior.totalWinLoseCom;

                            body.commission.masterAgent.winLoseCom = shareReceive.masterAgent.winLoseCom;
                            body.commission.masterAgent.winLose = shareReceive.masterAgent.winLose;
                            body.commission.masterAgent.totalWinLoseCom = shareReceive.masterAgent.totalWinLoseCom;

                            body.commission.agent.winLoseCom = shareReceive.agent.winLoseCom;
                            body.commission.agent.winLose = shareReceive.agent.winLose;
                            body.commission.agent.totalWinLoseCom = shareReceive.agent.totalWinLoseCom;

                            body.commission.member.winLoseCom = shareReceive.member.winLoseCom;
                            body.commission.member.winLose = shareReceive.member.winLose;
                            body.commission.member.totalWinLoseCom = shareReceive.member.totalWinLoseCom;

                            body.slot.spade.specialGame = requestBody.specialGame;
                            body.slot.spade.referenceId = requestBody.referenceId;
                            body.slot.spade.ticketId = requestBody.ticketId;
                            body.slot.spade.refTicketIds = requestBody.refTicketIds;
                            // body.slot.ameba.winLose = requestBody.WinLose;
                            body.updatedDate =  DateUtils.getCurrentDate();
                            body.settleDate =  DateUtils.getCurrentDate();

                            let betTransactionModel = new BetTransactionModel(body);
                            betTransactionModel.save((err, betObj) => {
                                if (err) {
                                    if (err.code == 11000) {
                                        return res.send({
                                            transferId: requestBody.transferId,
                                            acctId: requestBody.acctId,
                                            balance: credit,
                                            merchantCode: requestBody.merchantCode,
                                            msg: "success",
                                            code: 0,
                                            serialNo: requestBody.serialNo
                                        });
                                    }


                                    // let LogLive22Model = new LogLive22Model({
                                    //     betId:body.betId,
                                    //     message:err,
                                    //     createdDate:DateUtils.getCurrentDate()
                                    // });
                                    // LogLive22Model.save((err, betObj) => {
                                    //
                                    // });

                                    return res.send({
                                        merchantCode: requestBody.merchantCode,
                                        msg: "System Error",
                                        code: 1,
                                        serialNo: requestBody.serialNo
                                    });
                                } else {

                                        return res.send({
                                            transferId: requestBody.transferId,
                                            merchantTxId: response.betId,
                                            acctId: requestBody.acctId,
                                            balance: updateBalanceResponse.newBalance,
                                            merchantCode: requestBody.merchantCode,
                                            msg: "success",
                                            code: 0,
                                            serialNo: requestBody.serialNo
                                        });

                                }


                            });

                        } else {
                            return res.send({
                                transferId: requestBody.transferId,
                                merchantTxId: response.betId,
                                acctId: requestBody.acctId,
                                balance: updateBalanceResponse.newBalance,
                                merchantCode: requestBody.merchantCode,
                                msg: "success",
                                code: 0,
                                serialNo: requestBody.serialNo
                            });
                        }
                    });
                }
            });

        });
    });

}


router.get('/login', (req, res) => {

    async.series([callback => {

        MemberService.findByUserName(req.userInfo.username, function (err, memberResponse) {
            if (err) {
                callback(err, null);
            } else {

                if (memberResponse.suspend) {
                    callback(5011, null);
                } else if (memberResponse.limitSetting.game.slotXO.isEnable) {

                    AgentService.getLive22Status(memberResponse.group._id, (err, response) => {
                        console.log('response : ', response)
                        if (!response.isEnable) {
                            callback(1088, null);
                        } else {
                            callback(null, memberResponse);
                        }
                    });
                } else {
                    callback(1088, null);
                }


            }
        });

    }, callback => {

        AgentService.getUpLineStatus(req.userInfo.group._id, function (err, status) {
            console.log('upline status : ' + status);
            if (err) {
                callback(err, null);
            } else {
                if (status.suspend) {
                    callback(5011, null);
                } else {
                    callback(null, status);
                }
            }
        });

    }], (err, asyncResponse) => {

        if (err) {
            if (err == 1088) {
                return res.send({
                    code: 1088,
                    message: 'Service Locked, Please contact your upline.'
                });
            }
            if (err == 5011) {
                return res.send({
                    message: "Account or Upline was suspend.",
                    result: null,
                    code: 5011
                });
            }
            return res.send({code: 999, message: err});
        }
        let memberInfo = asyncResponse[0];

        // call api

        let gameId;
        if (req.query.GameId) {
            gameId = req.query.GameId;
        } else {
            gameId = 0;
        }

        let client = req.query.client;
        let url = SpadeService.callGameLobbyPageUrl(memberInfo.username_lower, client, gameId);

        console.log('-------------URL-------------', url)
        return res.send({message: 'success', code: 0, url: url});


    });

});


function prepareCommission(memberInfo, body, currentAgent, overAgent, remaining, overAllShare) {


    if (currentAgent && (currentAgent.type === 'SUPER_ADMIN' || currentAgent.parentId)) {


        console.log('==============================================');
        console.log('process : ' + currentAgent.type + ' : ' + currentAgent.name);


        let money = body.amount;


        if (!overAgent) {
            overAgent = {};
            overAgent.type = 'member';
            overAgent.shareSetting = {};
            overAgent.shareSetting.game = {};
            overAgent.shareSetting.game.slotXO = {};
            overAgent.shareSetting.game.slotXO.parent = memberInfo.shareSetting.game.slotXO.parent;
            overAgent.shareSetting.game.slotXO.own = 0;
            overAgent.shareSetting.game.slotXO.remaining = 0;

            body.commission.member = {};
            body.commission.member.parent = memberInfo.shareSetting.game.slotXO.parent;
            body.commission.member.commission = memberInfo.commissionSetting.game.slotXO;

        }

        console.log('');
        console.log('---- setting ----');
        console.log('ส่วนแบ่งที่ได้มามีทั้งหมด : ' + currentAgent.shareSetting.game.slotXO.own + ' %');
        console.log('ขอส่วนแบ่งจาก : ' + overAgent.type + ' ' + overAgent.shareSetting.game.slotXO.parent + ' %');
        console.log('ให้ส่วนแบ่ง : ' + overAgent.type + ' ที่เหลือไป ' + overAgent.shareSetting.game.slotXO.own + ' %');
        console.log('remaining จาก : ' + overAgent.type + ' : ' + overAgent.shareSetting.game.slotXO.remaining + ' %');
        console.log('โดนบังคับเสียขั้นต่ำ : ' + currentAgent.shareSetting.game.slotXO.min + ' %');
        console.log('---- end setting ----');
        console.log('');


        // console.log('และได้รับ remaining ไว้ : '+overAgent.shareSetting.game.slotXO.remaining+' %');

        let currentPercentReceive = overAgent.shareSetting.game.slotXO.parent;
        console.log('% ที่ได้ : ' + currentPercentReceive);
        console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');
        let totalRemaining = remaining;

        if (overAgent.shareSetting.game.slotXO.remaining > 0) {
            // console.log()
            // console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');

            if (overAgent.shareSetting.game.slotXO.remaining > totalRemaining) {
                console.log('รับ remaining ทั้งหมดไว้: ' + totalRemaining + ' %');
                currentPercentReceive += totalRemaining;
                totalRemaining = 0;
            } else {
                totalRemaining = remaining - overAgent.shareSetting.game.slotXO.remaining;
                console.log('หักค่า remaining ที่ได้รับมากับที่เซตรับไว้ = ' + remaining + ' - ' + overAgent.shareSetting.game.slotXO.remaining + ' = ' + totalRemaining);
                currentPercentReceive += overAgent.shareSetting.game.slotXO.remaining;
            }

        }

        console.log('รวม % ที่ได้รับ : ' + currentPercentReceive);


        let unUseShare = currentAgent.shareSetting.game.slotXO.own -
            (overAgent.shareSetting.game.slotXO.parent + overAgent.shareSetting.game.slotXO.own);

        console.log('เหลือส่วนแบ่งที่ไม่ได้ใช้ : ' + unUseShare + ' %');
        totalRemaining = (unUseShare + totalRemaining);
        console.log('ส่วนแบ่งที่ไม่ได้ใช้ บวกกับค่า remaining ท่ี่เหลือจะเป็น : ' + totalRemaining + ' %');

        console.log('จากที่โดนบังคับเสียขั้นต่ำ : ' + currentAgent.shareSetting.game.slotXO.min + ' %');
        console.log('overAllShare : ', overAllShare);
        if (currentAgent.shareSetting.game.slotXO.min > 0 && ((overAllShare + currentPercentReceive) < currentAgent.shareSetting.game.slotXO.min)) {

            if (currentPercentReceive < currentAgent.shareSetting.game.slotXO.min) {
                console.log('% ที่ได้รับ < ขั้นต่ำที่ถูกตั้ง min ไว้');
                let percent = currentAgent.shareSetting.game.slotXO.min - (currentPercentReceive + overAllShare);
                console.log('หักออกไป ' + percent + ' % : แล้วไปเพิ่มในส่วนที่ต้องรับผิดชอบ ');
                totalRemaining = totalRemaining - percent;
                if (totalRemaining < 0) {
                    totalRemaining = 0;
                }
                currentPercentReceive += percent;
            }
        } else {
            // currentPercentReceive += totalRemaining;
        }


        if (currentAgent.type === 'SUPER_ADMIN') {
            currentPercentReceive += totalRemaining;
        }

        let getMoney = (money * NumberUtils.convertPercent(currentPercentReceive)).toFixed(2);
        overAllShare = overAllShare + currentPercentReceive;

        console.log('ยอดแทง ' + money + ' ได้ทั้งหมด : ' + currentPercentReceive + ' %');

        console.log('รวม % + % remaining  จะเป็นสัดส่วนที่ได้ทั้งหมด = ' + getMoney);
        console.log('ค่าที่จะถูกคืนกลับไป (remaining) : ' + totalRemaining + ' %');

        console.log('จำนวน % ที่ถูกแบ่งไปแล้วทั้งหมด : ', overAllShare);

        //set commission
        let agentCommission = currentAgent.commissionSetting.game.slotXO;


        switch (currentAgent.type) {
            case 'SUPER_ADMIN':
                body.commission.superAdmin = {};
                body.commission.superAdmin.group = currentAgent._id;
                body.commission.superAdmin.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.superAdmin.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.superAdmin.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.superAdmin.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.superAdmin.shareReceive = Math.abs(currentPercentReceive);
                body.commission.superAdmin.commission = agentCommission;
                body.commission.superAdmin.amount = getMoney;
                break;
            case 'COMPANY':
                body.commission.company = {};
                body.commission.company.parentGroup = currentAgent.parentId;
                body.commission.company.group = currentAgent._id;
                body.commission.company.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.company.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.company.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.company.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.company.shareReceive = Math.abs(currentPercentReceive);
                body.commission.company.commission = agentCommission;
                body.commission.company.amount = getMoney;
                break;
            case 'SHARE_HOLDER':
                body.commission.shareHolder = {};
                body.commission.shareHolder.parentGroup = currentAgent.parentId;
                body.commission.shareHolder.group = currentAgent._id;
                body.commission.shareHolder.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.shareHolder.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.shareHolder.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.shareHolder.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.shareHolder.shareReceive = currentPercentReceive;
                body.commission.shareHolder.commission = agentCommission;
                body.commission.shareHolder.amount = getMoney;
                break;
            case 'SENIOR':
                body.commission.senior = {};
                body.commission.senior.parentGroup = currentAgent.parentId;
                body.commission.senior.group = currentAgent._id;
                body.commission.senior.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.senior.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.senior.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.senior.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.senior.shareReceive = currentPercentReceive;
                body.commission.senior.commission = agentCommission;
                body.commission.senior.amount = getMoney;
                break;
            case 'MASTER_AGENT':
                body.commission.masterAgent = {};
                body.commission.masterAgent.parentGroup = currentAgent.parentId;
                body.commission.masterAgent.group = currentAgent._id;
                body.commission.masterAgent.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.masterAgent.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.masterAgent.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.masterAgent.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.masterAgent.shareReceive = currentPercentReceive;
                body.commission.masterAgent.commission = agentCommission;
                body.commission.masterAgent.amount = getMoney;
                break;
            case 'AGENT':
                body.commission.agent = {};
                body.commission.agent.parentGroup = currentAgent.parentId;
                body.commission.agent.group = currentAgent._id;
                body.commission.agent.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.agent.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.agent.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.agent.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.agent.shareReceive = currentPercentReceive;
                body.commission.agent.commission = agentCommission;
                body.commission.agent.amount = getMoney;
                break;
        }

        prepareCommission(memberInfo, body, currentAgent.parentId, currentAgent, totalRemaining, overAllShare);
    }
}

function updateAgentMemberBalance(shareReceive, betAmount, ref1, updateCallback) {

    console.log('updateAgentMemberBalance');
    console.log('shareReceive : ', shareReceive);

    let balance = betAmount + shareReceive.member.totalWinLoseCom;

    try {
        console.log('update member balance : ', balance);

        MemberService.updateBalance2(shareReceive.memberUsername, balance, shareReceive.betId, 'SPADE_GAME', 'SETTLE|'+ref1, function (err, updateBalanceResponse) {
            if (err) {
                updateCallback(err, null);
            } else {
                updateCallback(null, updateBalanceResponse);
            }
        });
    } catch (err) {
        MemberService.updateBalance2(shareReceive.memberUsername, balance, shareReceive.betId, 'SLOT_SPADE_SETTLE_ERROR ' + shareReceive.member.id + ' : ' + balance, '', function (err, updateBalanceResponse) {
            if (err) {
                updateCallback(err, null);
            } else {
                updateCallback(null, updateBalanceResponse);
            }
        });
    }

}

module.exports = router;
