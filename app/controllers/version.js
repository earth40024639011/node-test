const fs = require('fs');
const express = require('express');
const router = express.Router();
const NodeCache = require( "node-cache" );
const _         = require('underscore');
const cache = new NodeCache( { stdTTL: 0, checkperiod: 0 } );

const mongoose          = require('mongoose');
const ObjectId          = mongoose.Types.ObjectId;
const VersionModel      = require('../models/version.model');
const ClientNameModel   = require('../models/clientName.model');
const RedisService      = require('../common/redisService');
const BetTransactionModel = require('../models/betTransaction.model');

const AgentGroupModel = require('../models/agentGroup.model.js');
const LOG = 'VERSION';

const CLIENT_NAME = process.env.CLIENT_NAME || 'SPORTBOOK88';
router.get('/', (req, res) => {
    ClientNameModel.findOne({
            clientName: CLIENT_NAME
        },
        (err, data)=>{
            console.log(data);
            if (err) {
                res.sendStatus(500);
            } else if (_.isNull(data)) {
                res.sendStatus(500);
            } else if (!_.isNull(data) && _.isEqual(CLIENT_NAME, data.clientName)) {
                return res.send({
                    code: 0,
                    message: "success",
                    CLIENT_NAME: data.clientName
                });
            } else {
                res.sendStatus(500);
            }
        });

    // try{
    //     const contents = cache.get( "version", true );
    //     return res.send(contents);
    // } catch( err ){
    //     fs.readFile('../version.txt', 'utf8',
    //         (err, contents) =>{
    //             if(err){
    //                 return res.send({message: "error", result: err.message, code: err.code});
    //             } else {
    //                 cache.set( "version", contents);
    //                 return res.send(contents);
    //             }
    //         });
    // }

    // BetTransactionModel.find({ "memberId": ObjectId("5e2d1ddd22c1dd4e02c1f8fb"), "hdp.oddType": "DC" })
    //     // .populate({path: 'memberParentGroup', select: 'type'})
    //     // .populate({path: 'memberId', select: 'username'})
    //     .exec(function (err, transactionList) {
    //         let totalB = 0;
    //         let totalL = 0;
    //         _.each(transactionList, transaction => {
    //             const {
    //                 betId,
    //                 betResult,
    //                 memberCredit,
    //                 commission
    //             } = transaction;
    //             // console.log(JSON.stringify(transaction));
    //             if (_.isEqual(betResult, 'WIN')) {
    //                 console.log(`${betId} ${commission.member.totalWinLoseCom}`);
    //                 totalB += commission.member.totalWinLoseCom;
    //             } else {
    //                 console.log(`${betId} ${memberCredit}`);
    //                 totalL += memberCredit;
    //             }
    //         });
    //         console.log(totalB);
    //         console.log(totalL);
    //         console.log(totalB + totalL);
    //     });
});

router.get('/hash/:agent', (req, res) => {
    const agent = req.params.agent;
    AgentGroupModel.findOne({
        $or : [
            {
                name: agent.toLowerCase()
            },
            {
                name_lower: agent
            }
        ]
    })
        .lean()
        .select('_id')
        .exec((error, data) => {
            return res.send({
                code: 0,
                message: "success",
                agent,
                data,
                error
            });
        });
});

router.get('/db', (req, res) => {
    VersionModel.findOne({
        },
        (err, data)=>{
            return res.send(data.version);
        })
});
router.get('/set/:version', (req, res) => {
    VersionModel.findOne({
        },
        (err, data)=>{
            const version = req.params.version;
            console.log(data._id);
            console.log(version);
            VersionModel.update({
                    _id : new ObjectId(data._id)
                },
                {
                    $set: {
                        'version': version
                    }
                },
                {
                    multi: false
                },
                (error, data) => {
                    if (error) {
                        console.error(`[${LOG}][2.1] => ${error}`);
                        return res.send({
                            code: 999,
                            message: "error",
                            result: error,
                        });
                    } else if (data.nModified === 0) {
                        console.log(`[${LOG}][2.2] Update fail version [${version}]`);
                        return res.send({
                            code: 998,
                            message: `fail`,
                            result:`Update fail version [${version}]`,

                        });
                    } else {
                        console.log(`[${LOG}][2.3] Update success version [${version}]`);
                        return res.send({
                            code: 0,
                            message: "success",
                            result : version
                        });
                    }
                });
        })
});

router.get('/redis', (req, res) => {
    RedisService.getCurrency((error, result) => {
        return res.send(_.first(result).currencyName);
    });
});
module.exports = router;