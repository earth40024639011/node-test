const express = require('express');
const router = express.Router();
const request = require('request');
const _  = require('underscore');
// -----------------------------------------------------------
const crypto = require('crypto');
const async = require("async");
const Joi       = require('joi');
const DateUtils = require('../../common/dateUtils');
const MemberMuayStep2Model = require('../../models/memberMuayStep2.model.js');
const MemberModel                = require('../../models/member.model');
const Muaystep2Service              = require('../../common/muaystep2Service');
const BetTransactionModel = require('../../models/betTransaction.model');
const AgentService = require('../../common/agentService');
const MemberService = require('../../common/memberService');
const ApiService = require('../../common/apiService');

const getClientKey = require('../getClientKey');
let {
  MUAY_STEP2_HOST: HOST = 'http://www.muaystep2.com',
  MUAY_STEP2_OPCODE: WEBSITE = 'ironbet',
  MUAY_STEP2_PRIVATE_KEY: PRIVATE_KEY = 'MuaY18',
  MUAY_STEP2_OPAGENT_ID: OPAGENTID = 'ironbetagent',
  MUAY_STEP2_PREFIX: PREFIX = 'ironbet_'
} = getClientKey.getKey();

const MY_HOST = 'https://sportbook88.api-hub.com';

const headers = {
    'Content-Type': 'application/json'
};

// -----------------------------------------------------------



function getHash(val) {
    return crypto.createHash('md5').update(val).digest("hex");
}


function addPlayerMuayStep2(userInfo) {

    let memberMuayStep2 = new MemberMuayStep2Model(
        {
            userId : userInfo._id,
            createdDate: DateUtils.getCurrentDate(),
        }
    );

    memberMuayStep2.save((err, response) => {
        if (err) {
            console.log('-------------add memberMuayStep2 fail------------', err);
        } else {
            console.log('-------------add memberMuayStep2 Success------------', response);
        }
    });
}

function login(req, callback) {

    let userInfo = req.userInfo;

    async.waterfall([callback => {

        //Find Player Registered MuayStep2
        MemberMuayStep2Model.findOne({userId: userInfo._id})
            .exec(function (err, member) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, member);
                }
            });


    }, (member, callback) => {

        // console.log('member === ',member);
        if(!member){
            // Register Player
            let url =  `${HOST}/sportsfundservice/registerplayer/${WEBSITE}/opAgentId/${OPAGENTID}/agentCode/${userInfo.group._id}/agentName/${userInfo.group.name}/parentAgentCode/${userInfo.group._id}/player/${PREFIX}${userInfo._id}/lang/th/thai?access=${getHash(`registerplayer/${WEBSITE}/opAgentId/${OPAGENTID}/agentCode/${userInfo.group._id}/agentName/${userInfo.group.name}/parentAgentCode/${userInfo.group._id}/player/${PREFIX}${userInfo._id}/lang/th/thai/${PRIVATE_KEY}`)}`

            const option = {
                url: url,
                method: 'GET',
                headers: headers
            };

            console.log('--------REGISTER-------', option);

            request(option, (err, response, body) => {
                if (err) {
                    callback(null, {isRegistered : false});
                } else {

                    try {

                        let resultBody = JSON.parse(body);

                        // console.log('register ======== ',resultBody);

                        addPlayerMuayStep2(userInfo);

                        callback(null, {isRegistered : true});

                    } catch (e) {
                        callback(null, {isRegistered : false});
                    }
                }
            });
        }else {
            callback(null, {isRegistered : true});
        }


    }], function (err, isRegistered) {
        // console.log('-------is register-------',isRegistered);
        if(isRegistered){
            //Login to Muay Step2

            const option = {
                url: `${HOST}/sportsfundservice/login/${WEBSITE}/opAgentId/${OPAGENTID}/player/${PREFIX}${userInfo._id}?access=${getHash(`login/${WEBSITE}/opAgentId/${OPAGENTID}/player/${PREFIX}${userInfo._id}/${PRIVATE_KEY}`)}`,
                method: 'GET',
                headers: headers
            };

            console.log('--------LOGIN-------', option);

            request(option, (err, response, body) => {
                if (err) {
                    callback(err,  {
                        code: 1,
                        result : null,
                        message : 'Login Fail'
                    });
                } else {

                    try {

                        let resultBody = JSON.parse(body);

                        // console.log('-----result-----',resultBody);

                        callback(err,  {
                            code: 0,
                            result : resultBody,
                            message : 'Login Success'
                        });

                    } catch (e) {
                        callback(e,  {
                            code: 1,
                            result : null,
                            message : 'Login Fail'
                        });
                    }
                }
            });

        }else {

            callback(err,  {
                code: 1,
                result : null,
                message : 'Login Fail'
            });
        }

    });
}


// 1.1
router.get('/ping', function (req, res) {

   let url =  `${HOST}/sportsfundservice/ping/${WEBSITE}?access=${getHash(`ping/${WEBSITE}/${PRIVATE_KEY}`)}`

    const option = {
        url: url,
        method: 'GET',
        headers: headers
    };

    request(option, (err, response, body) => {
        if (err) {
            return res.send(err);
        } else {

            try {

                let resultBody = JSON.parse(body);

                return res.send({
                    code: 0,
                    result : resultBody,
                    message : 'ping success'
                });
            } catch (e) {
                return res.send({
                    code: 1,
                    result : null,
                    message : 'fail'
                });
            }
        }
    });
});


// 1.2
router.get('/checkStatusServer', function (req, res) {
    console.log('-------------HOST---------------', HOST);
    console.log('-------------WEBSITE---------------', WEBSITE);
    console.log('-------------PRIVATE KEY---------------', PRIVATE_KEY);

    const option = {
        url: `${HOST}/sportsfundservice/checkstatus/${WEBSITE}?access=${getHash(`checkstatus/${WEBSITE}/${PRIVATE_KEY}`)}`,
        method: 'GET',
        headers: headers
    };

    console.log('--------OPTION-------', option);


    request(option, (err, response, body) => {
        if (err) {
            return res.send(err);
        } else {

            try {

                let resultBody = JSON.parse(body);

                console.log('--------result-------',resultBody);

                if(resultBody.isUM){
                    return res.send({
                        code: 0,
                        result : {
                            isMaintenance : resultBody.isUM,
                            startDate : resultBody.umStartTime,
                            endDate : resultBody.umEndTime
                        },
                        message : 'success'
                    });
                }else {
                    return res.send({
                        code: 0,
                        result : {
                            isMaintenance : resultBody.isUM,
                            startDate : 0,
                            endDate : 0
                        },
                        message : 'success'
                    });
                }

            } catch (e) {
                return res.send({
                    code: 1,
                    result : null,
                    message : 'fail'
                });
            }
        }
    });
});

// 1.3
router.get('/login', function (req, res) {

    console.log('-----------------login---------------------')

    login(req, function (err, result) {
        console.log(err, '---response---', result);
        if(err){
            return res.send(err)
        }else {
            return res.send(result)
        }
    })

    // console.log('user info ==== ',userInfo);




});

// 1.4.1
router.get('/gameList/:token', function (req, res) {

    const option = {
        url: `${HOST}/api_play/api/api/gamelist/${req.params.token}`,
        method: 'GET',
        headers: headers
    };

    request(option, (err, response, body) => {
        if (err) {
            return res.send(err);
        } else {

            try {

                let resultBody = JSON.parse(body);

                // console.log('-----resultBody--------',resultBody.res[0].list_all[0]);

                if (resultBody.code === 1){
                    let todayMatchList = _.chain(resultBody.res)
                        .filter(data => {
                            if(data.list_all.length > 0){
                                return true;
                            }
                        })
                        .each(data => {
                            data.list_all = _.filter(data.list_all, obj => {
                                return _.isEqual(obj.round, '0 : 0');
                            });

                        })
                        .value();

                    todayMatchList = _.filter(todayMatchList, data => {
                        return !_.isEmpty(data.list_all);
                    });


                    if(todayMatchList.length > 0){
                        for(var i = 0 ; i < todayMatchList.length ; i++){
                            if(todayMatchList[i].list_all.length > 0){
                                todayMatchList[i].cat_type = todayMatchList[i].list_all[0].sub_cat_id;
                            }

                        }
                    }
                    // console.log(todayMatchList);
                    return res.send({
                        code: 0,
                        result : todayMatchList,
                        message : 'Success'
                    });
                }else {
                    return res.send({
                        code: resultBody.code,
                        result : [],
                        message : resultBody.msg
                    });
                }


            } catch (e) {
                return res.send({
                    code: 1,
                    result : null,
                    message : 'fail'
                });
            }
        }
    });
});


// 1.4.2
router.get('/gameListLive/:token', function (req, res) {

    const option = {
        url: `${HOST}/api_play/api/api/gamelist/${req.params.token}`,
        method: 'GET',
        headers: headers
    };

    request(option, (err, response, body) => {
        if (err) {
            return res.send(err);
        } else {

            try {

                let resultBody = JSON.parse(body);

                if (resultBody.code === 1){
                    let liveMatchList = _.chain(resultBody.res)
                        .filter(data => {
                            if(data.list_all.length > 0){
                                return new Date(data.list_all[0].fight_date).getUTCDate() == DateUtils.getCurrentDate().getUTCDate();
                            }
                        })
                        .each(data => {
                            data.list_all = _.filter(data.list_all, obj => {
                                return !_.isEqual(obj.round, '0 : 0');
                            });

                        })
                        .value();

                    liveMatchList = _.filter(liveMatchList, data => {
                        return data.list_all.length > 0
                    });

                    if(liveMatchList.length > 0){
                        for(var i = 0 ; i < liveMatchList.length ; i++){
                            if(liveMatchList[i].list_all.length > 0){
                                liveMatchList[i].cat_type = liveMatchList[i].list_all[0].sub_cat_id;
                            }

                        }
                    }

                    // console.log(liveMatchList);

                    return res.send({
                        code: 0,
                        result : liveMatchList,
                        message : 'Success'
                    });
                }else {
                    return res.send({
                        code: resultBody.code,
                        result : [],
                        message : resultBody.msg
                    });
                }

            } catch (e) {
                return res.send({
                    code: 1,
                    result : null,
                    message : 'fail'
                });
            }
        }
    });
});

// 1.4.3
router.get('/gameListEarlyMarket/:token', function (req, res) {

    const option = {
        url: `${HOST}/api_play/api/api/gamelist/${req.params.token}`,
        method: 'GET',
        headers: headers
    };

    request(option, (err, response, body) => {
        if (err) {
            return res.send(err);
        } else {

            try {

                let resultBody = JSON.parse(body);
                console.log('-------------',resultBody);

                if (resultBody.code === 1){
                    let liveMatchList = _.chain(resultBody.res)
                        .filter(data => {
                            if(data.list_all.length > 0){
                                return new Date(data.list_all[0].fight_date).getUTCDate() != DateUtils.getCurrentDate().getUTCDate();
                            }
                        })
                        .each(data => {
                            data.list_all = _.filter(data.list_all, obj => {
                                return _.isEqual(obj.round, '0 : 0');
                            });

                        })
                        .value();

                    liveMatchList = _.filter(liveMatchList, data => {
                        return data.list_all.length > 0
                    })

                    return res.send({
                        code: 0,
                        result : liveMatchList,
                        message : 'Success'
                    });
                }else {
                    return res.send({
                        code: resultBody.code,
                        result : [],
                        message : resultBody.msg
                    });
                }


            } catch (e) {
                return res.send({
                    code: 1,
                    result : null,
                    message : 'fail'
                });
            }
        }
    });
});


// 1.5
router.get('/resultList/:date', function (req, res) {

    const option = {
        url: `${HOST}/sportsfundservice/resultlistapi/${WEBSITE}/${req.params.date}?access=${getHash(`resultlistapi/${WEBSITE}/${req.params.date}/${PRIVATE_KEY}`)}`,
        method: 'GET',
        headers: headers
    };

    console.log('----option----', option);
    request(option, (err, response, body) => {
        if (err) {
            return res.send(err);
        } else {

            try {

                let resultBody = JSON.parse(body);

                return res.send({
                    code: 0,
                    result : resultBody,
                    message : 'success'
                });
            } catch (e) {
                return res.send({
                    code: 1,
                    result : null,
                    message : 'fail'
                });
            }
        }
    });
});

const resultSchema = Joi.object().keys({
    date: Joi.string().required()
});

// 1.5.1
router.post('/result', function (req, res) {
    let validate = Joi.validate(req.body, resultSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    console.log('data ========',req.body.date);
    const option = {
        url: `${HOST}/sportsfundservice/rs_list/${WEBSITE}/${req.body.date}?access=${getHash(`rs_list/${WEBSITE}/${req.body.date}/${PRIVATE_KEY}`)}`,
        method: 'GET',
        headers: headers
    };

    console.log('----option----', option);
    request(option, (err, response, body) => {
        if (err) {
            return res.send(err);
        } else {

            try {

                let resultBody = JSON.parse(body);

                return res.send({
                    code: 0,
                    result : resultBody,
                    message : 'success'
                });
            } catch (e) {
                return res.send({
                    code: 1,
                    result : null,
                    message : 'fail'
                });
            }
        }
    });
});


function findMaxBet(memberInfo, oddType) {

    switch (oddType.toUpperCase()) {
        case 'AH':
        case 'AH1ST':
        case 'OU':
        case 'OU1ST':
        case 'OE':
        case 'OE1ST':
        case 'T1OU':
        case 'T2OU':
            return memberInfo.limitSetting.other.m2.maxPerBetHDP;
        case 'X12':
        case 'X121ST':
            return memberInfo.limitSetting.other.m2.maxPerBetHDP;
    }
}


const addBetSchema = Joi.object().keys({
    token: Joi.string().required(),
    boxingId: Joi.string().required(),
    type: Joi.string().required(), // single , multi
    team: Joi.string().required(), //HOME, AWAY
    odd: Joi.number().required()
});


// 1.6
router.post('/findEventById', function (req, res) {

    let validate = Joi.validate(req.body, addBetSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }
    // console.log(JSON.stringify(req.body));

    const userInfo = req.userInfo;

    async.waterfall([callback => {

        MemberModel.findById(userInfo._id, (err, response) => {
            if (err)
                callback(err, null);

            if (response) {

                callback(null, response);

            }
        });


    }, (memberInfo, callback) => {

        const hdc = 0;
        let team = req.body.team == 'HOME'? 'red' : 'blue';

        const option = {
            url: `${HOST}/api_play/api/api/findeventbyid/${req.body.token}?boxing_id=${req.body.boxingId}&play_type=${req.body.type}&choose_team=${team}&price=${req.body.odd}&hdc=${hdc}`,
            method: 'GET',
            headers: headers
        };

        console.log('----option----', option);

        request(option, (err, response, body) => {
            if (err) {
                return res.send(err);
            } else {

                try {

                    let resultBody = JSON.parse(body);

                    // console.log('result find by id ---------- ',resultBody);

                    if(resultBody && resultBody.code === 1){
                        console.log('result =========== ',resultBody);
                        if (req.body.type === 'single'){
                            if(resultBody.price_max > memberInfo.limitSetting.other.m2.maxPerBetHDP){
                                resultBody.price_max = memberInfo.limitSetting.other.m2.maxPerBetHDP
                            }

                            if(parseInt(resultBody.price_min) < memberInfo.limitSetting.other.m2.minPerBetHDP){
                                resultBody.price_min = memberInfo.limitSetting.other.m2.minPerBetHDP
                            }
                        }else {
                            if(resultBody.price_max > memberInfo.limitSetting.other.m2.maxPerBet){
                                resultBody.price_max = memberInfo.limitSetting.other.m2.maxPerBet
                            }

                            if(parseInt(resultBody.price_min) < memberInfo.limitSetting.other.m2.minPerBet){
                                resultBody.price_min = memberInfo.limitSetting.other.m2.minPerBet
                            }

                            resultBody.minBet = memberInfo.limitSetting.other.m2.minPerBet;
                            resultBody.maxPayout = memberInfo.limitSetting.other.m2.maxPayPerBill
                        }

                        callback(null , resultBody);

                    }else {
                        callback(999, null);
                    }

                } catch (e) {
                    callback(999, null);
                }
            }
        });

    }], function (err, response) {
        if(err){
            return res.send({
                code: 999,
                result : null,
                message : 'Fail'
            });
        }else {
            return res.send({
                code: 0,
                result : response,
                message : 'Success'
            });
        }

    });


});

// 1.7
router.get('/placeBet/:token/:type/:amount', function (req, res) {

    const option = {
        url: `${HOST}/api_play/api/api/placebet/${req.params.token}?play_type=${req.params.type}&play_type=${req.params.type}&income=${req.params.amount}`,
        method: 'GET',
        headers: headers
    };

    console.log('----option----', option);

    request(option, (err, response, body) => {
        if (err) {
            return res.send(err);
        } else {

            try {

                let resultBody = JSON.parse(body);

                console.log('--------------result-----------------',resultBody);

                return res.send({
                    code: 0,
                    result : resultBody,
                    message : 'success'
                });
            } catch (e) {
                return res.send({
                    code: 1,
                    result : null,
                    message : 'fail'
                });
            }
        }
    });
});


// 1.8
router.get('/betResult/:token', function (req, res) {

    const option = {
        url: `${HOST}/api_play/api/api/betresult/${req.params.token}`,
        method: 'GET',
        headers: headers
    };

    console.log('----option----', option);

    request(option, (err, response, body) => {
        if (err) {
            return res.send(err);
        } else {

            try {

                let resultBody = JSON.parse(body);

                return res.send({
                    code: 0,
                    result : resultBody,
                    message : 'success'
                });
            } catch (e) {
                return res.send({
                    code: 1,
                    result : null,
                    message : 'fail'
                });
            }
        }
    });
});

// 1.9
router.get('/reject/:betId', function (req, res) {

    if(!req.params.betId){
        return res.send({
            code: 999,
            result : null,
            message : 'Bet Id is empty!'
        });
    }

    async.waterfall([callback => {

        let condition = {'betId': req.params.betId, 'game': 'M2'};

        BetTransactionModel.findOne(condition)
            .select('memberId memberUsername hdp commission amount memberCredit payout memberParentGroup betId status priceType source betResult game')
            .populate({path: 'memberParentGroup', select: 'type'})
            .exec(function (err, betObj) {

                if (!betObj) {
                    return res.send({message: "data not found", results: null, code: 999});
                }

                if (betObj.status === 'CANCELLED' || betObj.status === 'REJECTED') {
                    callback(null, betObj);
                }else {
                    if (betObj.status === 'DONE'){

                        async.series([subCallback => {

                            //refundBalance
                            AgentService.refundAgentMemberBalance(betObj, '', (err, response) => {
                                if (err) {
                                    subCallback(998, null);
                                } else {
                                    subCallback(null, response);
                                }
                            });
                        }, callback => {

                            let updateBody = {
                                $set: {
                                    'cancelDate': DateUtils.getCurrentDate(),
                                    'cancelByType': 'AGENT',
                                    'cancelByAgent': 'AGENT',
                                    status: 'CANCELLED',
                                    'commission.superAdmin.winLoseCom': 0,
                                    'commission.superAdmin.winLose': 0,
                                    'commission.superAdmin.totalWinLoseCom': 0,

                                    'commission.company.winLoseCom': 0,
                                    'commission.company.winLose': 0,
                                    'commission.company.totalWinLoseCom': 0,

                                    'commission.shareHolder.winLoseCom': 0,
                                    'commission.shareHolder.winLose': 0,
                                    'commission.shareHolder.totalWinLoseCom': 0,

                                    'commission.senior.winLoseCom': 0,
                                    'commission.senior.winLose': 0,
                                    'commission.senior.totalWinLoseCom': 0,

                                    'commission.masterAgent.winLoseCom': 0,
                                    'commission.masterAgent.winLose': 0,
                                    'commission.masterAgent.totalWinLoseCom': 0,

                                    'commission.agent.winLoseCom': 0,
                                    'commission.agent.winLose': 0,
                                    'commission.agent.totalWinLoseCom': 0,

                                    'commission.member.winLoseCom': 0,
                                    'commission.member.winLose': 0,
                                    'commission.member.totalWinLoseCom': 0,
                                }
                            };
                            // console.log('updateBody ===', updateBody);
                            // console.log('betObj ===', betObj);
                            BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                                if (err) {
                                    callback(997, null);
                                } else {
                                    callback(null, response);
                                }
                            });
                        }], function (err, response) {

                            if (err) {
                                callback(err, null);
                            }else {
                                callback(null, betObj);
                            }


                        });
                    }else {

                        let transaction = betObj;

                        console.log('transaction ======= ', transaction);

                        async.series([callback => {

                            MemberService.updateBalance2(transaction.memberUsername, Math.abs(transaction.memberCredit), transaction.betId, 'CANCEL_TICKET', transaction.betId, function (err, creditResponse) {

                                if (err) {
                                    callback(996, null);
                                } else {
                                    callback(null, 'success');
                                }
                            });


                        }], function (err, response) {

                            if (err) {
                                callback(err, null);
                            }else {

                                let body = {
                                    status: 'CANCELLED',
                                    remark: req.body.remark,
                                    cancelByType:'AGENT',
                                    cancelByAgent:'AGENT',
                                    cancelDate:DateUtils.getCurrentDate()
                                };

                                BetTransactionModel.update({_id: transaction._id}, body, (err, data) => {
                                    if (err) {
                                        callback(995, null);
                                    } else if (data.nModified === 0) {
                                        callback(995, null);
                                    } else {

                                        if (transaction.memberParentGroup.type === 'API') {

                                            ApiService.cancelBet(transaction.memberParentGroup.endpoint, req.body.remark, transaction, (err, response) => {
                                                if (err) {
                                                    callback(994, null);
                                                } else {
                                                    callback(null, betObj);
                                                }

                                            });
                                        } else {
                                            callback(null, betObj);
                                        }
                                    }
                                });
                            }

                        });
                    }
                }

            });

    }, (betObj, callback) => {

        if(betObj && betObj.ref1){
            Muaystep2Service.rejectTicketPartner(betObj.ref1, (err, result) => {
                console.log('result ================== ',result);
                if(err){

                    callback(993, null);

                }else if(result.code === 999){

                    callback(993, null);

                }else {
                    callback(null, result);
                }
            });
        }else {
            callback(992, null);
        }


    }], function (err, result) {

        if(err){
            if(err === 999){
                return res.send({
                    code: 999,
                    message: "Data not found",
                    result: null
                });
            }

            if(err === 998){
                return res.send({
                    code: 998,
                    message: "refund agent member balance fail",
                    result: null
                });
            }
            if(err === 997){
                return res.send({
                    code: 997,
                    message: "update bet transaction fail",
                    result: null
                });
            }
            if(err === 996){
                return res.send({
                    code: 996,
                    message: "update balance fail",
                    result: null
                });
            }
            if(err === 995){
                return res.send({
                    code: 995,
                    message: "update bet transaction fail",
                    result: null
                });
            }
            if(err === 994){
                return res.send({
                    code: 994,
                    message: "cancel bet fail",
                    result: null
                });
            }
            if(err === 993){
                return res.send({
                    code: 993,
                    message: "reject ticket partner fail",
                    result: null
                });
            }
        }else {
            return res.send({
                code: 0,
                message: "reject success",
                result: result
            });
        }

    });

});


const betListSchema = Joi.object().keys({
    startDate: Joi.string().required(),
    endDate: Joi.string().required()
});

// 1.10
router.post('/pullCustomerBetList', function (req, res) {

    let validate = Joi.validate(req.body, betListSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    let userInfo = req.userInfo;

    const option ={
        url: `${HOST}/sportsfundservice/pullcustomerbetlist/${WEBSITE}/user/${PREFIX}${userInfo._id}?startdate=${req.body.startDate}&enddate=${req.body.endDate}&access=${getHash(`pullcustomerbetlist/${WEBSITE}/user/${PREFIX}${userInfo._id}/${PRIVATE_KEY}`)}`,
        method: 'GET',
        headers: headers
    };

    console.log('----option----', option);

    request(option, (err, response, body) => {
        if (err) {
            return res.send(err);
        } else {
            // console.log('result === ',body);
            try {

                let resultBody = JSON.parse(body);

                return res.send({
                    code: 0,
                    result : resultBody,
                    message : 'success'
                });
            } catch (e) {
                return res.send({
                    code: 1,
                    result : null,
                    message : 'fail'
                });
            }
        }
    });
});

// 1.11
router.get('/updatePlayerStatus/:status', function (req, res) {

    let userInfo = req.userInfo;

    const option ={
        url: `${HOST}/sportsfundservice/updateplayerstatus/${WEBSITE}/opAgentId/${OPAGENTID}/agentCode/${userInfo.group._id}/player/${PREFIX}${userInfo._id}/status/${req.params.status}?access=${getHash(`updateplayerstatus/${WEBSITE}/opAgentId/${OPAGENTID}/agentCode/${userInfo.group._id}/player/${PREFIX}${userInfo._id}/status/${req.params.status}/${PRIVATE_KEY}`)}`,
        method: 'GET',
        headers: headers
    };

    console.log('----option----', option);

    request(option, (err, response, body) => {
        if (err) {
            return res.send(err);
        } else {
            // console.log('result === ',body);
            try {

                let resultBody = JSON.parse(body);

                return res.send({
                    code: 0,
                    result : resultBody,
                    message : 'success'
                });
            } catch (e) {
                return res.send({
                    code: 1,
                    result : null,
                    message : 'fail'
                });
            }
        }
    });
});


// 1.12
router.get('/match/count/:token', function (req, res) {

    const option = {
        url: `${HOST}/api_play/api/api/gamelist/${req.params.token}`,
        method: 'GET',
        headers: headers
    };

    request(option, (err, response, body) => {
        if (err) {
            return res.send(err);
        } else {

            try {

                let resultBody = JSON.parse(body);
                let matchCount = {
                    live : 0,
                    today : 0,
                    earlyMarket : 0,
                    all : 0
                };

                if (resultBody.code === 1){

                    //Today ---------------------------------------------------------------------------------------
                    let todayMatchList = _.chain(resultBody.res)
                        .filter(data => {
                            if(data.list_all.length > 0){
                                return new Date(data.list_all[0].fight_date).getUTCDate() == DateUtils.getCurrentDate().getUTCDate();
                            }
                        })
                        .each(data => {
                            data.list_all = _.filter(data.list_all, obj => {
                                return _.isEqual(obj.round, '0 : 0');
                            });

                        })
                        .value();

                    todayMatchList = _.filter(todayMatchList, data => {
                        return data.list_all.length > 0
                    });

                    if(todayMatchList.length > 1){
                        todayMatchList.forEach(data => {
                            matchCount.today += data.list_all.length;
                        })
                    }else{
                        if(todayMatchList.length > 0){
                            matchCount.today = todayMatchList[0].list_all.length;
                        }
                    }


                    //Live ---------------------------------------------------------------------------------------
                    let liveMatchList = _.chain(resultBody.res)
                        .filter(data => {
                            if(data.list_all.length > 0){
                                return new Date(data.list_all[0].fight_date).getUTCDate() == DateUtils.getCurrentDate().getUTCDate();
                            }
                        })
                        .each(data => {
                            data.list_all = _.filter(data.list_all, obj => {
                                return !_.isEqual(obj.round, '0 : 0');
                            });

                        })
                        .value();

                    liveMatchList = _.filter(liveMatchList, data => {
                        return data.list_all.length > 0
                    });

                    if(liveMatchList.length > 1){
                        liveMatchList.forEach(data => {
                            matchCount.live += data.list_all.length;
                        })
                    }else{
                        if(liveMatchList.length > 0){
                            matchCount.live = liveMatchList[0].list_all.length;
                        }
                    }

                    //Early Market ---------------------------------------------------------------------------------------
                    let earlyMarketMatchList = _.chain(resultBody.res)
                        .filter(data => {
                            if(data.list_all.length > 0){
                                return new Date(data.list_all[0].fight_date).getUTCDate() != DateUtils.getCurrentDate().getUTCDate();
                            }
                        })
                        .each(data => {
                            data.list_all = _.filter(data.list_all, obj => {
                                return _.isEqual(obj.round, '0 : 0');
                            });

                        })
                        .value();

                    earlyMarketMatchList = _.filter(earlyMarketMatchList, data => {
                        return data.list_all.length > 0
                    });

                    if(earlyMarketMatchList.length > 1){
                        earlyMarketMatchList.forEach(data => {
                            matchCount.earlyMarket += data.list_all.length;
                        })
                    }else{
                        if(earlyMarketMatchList.length > 0){
                            matchCount.earlyMarket = earlyMarketMatchList[0].list_all.length;
                        }
                    }

                    matchCount.all = matchCount.today + matchCount.earlyMarket + matchCount.live;

                    return res.send({
                        code: 0,
                        result : matchCount,
                        message : 'Success'
                    });
                }else {
                    return res.send({
                        code: 999,
                        result : null,
                        message : 'Count Match Fail!'
                    });
                }


            } catch (e) {
                return res.send({
                    code: 1,
                    result : null,
                    message : 'fail'
                });
            }
        }
    });
});

// 1.12
router.get('/removeMatchParlay/:token/:matchId', function (req, res) {

    const option = {
        url: `${HOST}/api_play/api/api/betremove/${req.params.token}?boxing_id=${req.params.matchId}`,
        method: 'GET',
        headers: headers
    };

    console.log('-----option------',option);

    request(option, (err, response, body) => {
        if (err) {
            return res.send(err);
        } else {

            try {

                let resultBody = JSON.parse(body);

                console.log('result----------------------',resultBody);

                if (resultBody.code === 1){

                    return res.send({
                        code: 0,
                        result : resultBody,
                        message : 'Success'
                    });
                }else {
                    return res.send({
                        code: 999,
                        result : null,
                        message : 'Remove Match Fail!'
                    });
                }


            } catch (e) {
                return res.send({
                    code: 1,
                    result : null,
                    message : 'fail'
                });
            }
        }
    });
});




module.exports = router;