const express   = require('express');
const router    = express.Router();
const async     = require("async");
const Joi       = require('joi');
const mongoose  = require('mongoose');
const _         = require('underscore');
const roundTo   = require('round-to');
const request = require('request');
const moment = require('moment');

const MemberModel                = require('../../models/member.model');
const BetTransactionModel        = require('../../models/betTransaction.model.js');
const AgentGroupModel            = require('../../models/agentGroup.model.js');
const Commons                    = require('../../common/commons');
const _Utils                     = require('../../common/underscoreUtils');
const FormulaUtils               = require('../../common/formula');
const MemberService              = require('../../common/memberService');
const AgentService               = require("../../common/agentService");
const DateUtils                  = require('../../common/dateUtils');
const ApiService                 = require('../../common/apiService');
const RedisService               = require('../../common/redisService');
const Utility                       = require('../../common/utlity');
const Muaystep2Service              = require('../../common/muaystep2Service');
const CLIENT_NAME                = process.env.CLIENT_NAME || 'SPORTBOOK88';

const getClientKey = require('../getClientKey');
let {
  MUAY_STEP2_HOST: HOST = 'http://www.muaystep2.com'
} = getClientKey.getKey();

const headers = {
    'Content-Type': 'application/json'
};

function generateBetId() {
    return 'BET_M2' + new Date().getTime();
}

function calculateParlayPayout(amount, odd, callback) {
    let maxPayout = 2000000.1;
    let max = roundTo(maxPayout / odd, 2);
    if (amount > max) {
        callback(5012, null);
    } else {
        callback(null, '');
        // callback((amount * odd).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]);
    }
}

const addBetSchema = Joi.object().keys({
    token: Joi.string().required(),
    memberId: Joi.string().required(),
    odd: Joi.number().required(),
    oddType: Joi.string().required(), //ah, ou
    matchType: Joi.string().required(),//live, non_live
    acceptAnyOdd: Joi.boolean().required(),
    round: Joi.string().required(),
    priceType: Joi.string().required(),//MY, HK
    amount: Joi.number().required(),
    payout: Joi.number().required(),
    source: Joi.string().required(),
    playType: Joi.string().required(),//single, multi
    bet: Joi.string().allow(''),//HOME, AWAY
});


router.post('/hdp', function (req, res) {

    let validate = Joi.validate(req.body, addBetSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }
    console.log('----------body---------', JSON.stringify(req.body));
    const source         = 'M2_MUAY';
    const game         = 'M2';
    const userInfo       = req.userInfo;
    const membercurrency = userInfo.currency;
    // const matchType = 'NONE_LIVE';
    let isOfflineCash = false;
    // console.log('-----userInfo-----', userInfo);
    async.series([
            (callback) => {
                MemberService.getOddAdjustment(userInfo.username, (err, result) => {
                    if (err) {
                        callback(error, null);
                    } else {
                        callback(null, result);
                    }
                });
            },

            (callback) => {
                MemberService.getLimitSetting(userInfo._id, (err, result) => {
                    if (err) {
                        callback(error, null);
                    } else {
                        callback(null, result.limitSetting);
                    }
                });
            },

            // (callback) => {
            //     RedisService.getCurrency((error, result) => {
            //         if (error) {
            //             callback(null, false);
            //         } else {
            //             callback(null, result);
            //         }
            //     })
            // },
            (callback) => {
                if (_.isEqual(CLIENT_NAME, 'AMB_SPORTBOOK')) {
                    AgentService.getOfflineCashStatus(userInfo.username, (error, isOfflineCash) => {
                        if (error) {
                            callback(error, false);
                        } else {
                            callback(null, isOfflineCash);
                        }
                    });
                } else {
                    callback(null, false);
                }
            }
        ],
        (error, resultOddAndLimit) => {
            if (error) {
                return res.send({
                    code: 9999,
                    message: error
                });
            } else {

                const [
                    oddAdjust,
                    limitSetting,
                    currency,
                    isOfflineCashResult
                ] = resultOddAndLimit;
                isOfflineCash = isOfflineCashResult;
                async.series([callback => {

                    //TODO: duplicate find member
                    MemberModel.findById(req.body.memberId, (err, response) => {
                        if (err)
                            callback(err, null);
                        // console.log('response===', response);
                        if (response.suspend) {
                            callback(5011, null);
                        } else {
                            AgentService.getUpLineStatus(userInfo.group._id, (err, status) => {
                                if (err) {
                                    callback(err, null);
                                } else {
                                    if (status.suspend) {
                                        callback(5011, null);
                                    } else {
                                        callback(null, status);
                                    }
                                }
                            });
                        }
                    });


                }, callback => {

                    if (userInfo.group.type === 'API') {
                        callback(null, 0);
                    } else {
                        MemberService.getCredit(userInfo._id, (err, credit) => {
                            if (err) {
                                callback(err, null);
                            } else {
                                if (credit < req.body.amount) {
                                    callback(5005, null);
                                } else {
                                    callback(null, credit);
                                }
                            }
                        });
                    }

                }, callback => {

                    FormulaUtils.calculatePayout(req.body.amount, req.body.odd, req.body.oddType, req.body.priceType.toUpperCase(), (payout) => {
                        if (req.body.payout != payout) {
                            callback(5004, null);
                        } else {
                            callback(null, payout);
                        }
                    });

                }], (err, results) => {

                    if (err) {
                        if (err === 5004) {
                            return res.send({
                                message: "payout not matched.",
                                result: null,
                                code: 5004
                            });
                        }
                        if (err === 5005) {
                            return res.send({
                                message: "credit exceed.",
                                result: null,
                                code: 5005
                            });
                        }
                        if (err === 5011) {
                            return res.send({
                                message: "Account or Upline was suspend.",
                                result: null,
                                code: 5011
                            });
                        }
                        return res.send({message: "error", result: err, code: 999});
                    }

                    async.waterfall([callback => {

                        MemberModel.findById(req.body.memberId, (err, response) => {
                            if (err)
                                callback(err, null);

                            if (response) {
                                //wait win --------------------------------------------------------------------------------
                                let maxBet = findMaxBet(response, req.body.oddType);
                                console.log('maxBet------------------------',maxBet);
                                if (req.body.amount > maxBet) {
                                    callback(5001, null);
                                } else {
                                    callback(null, response);

                                }
                            }
                        });

                    }, (memberInfo, callback) => {

                        //Bet to Muay Step2

                        const option = {
                            url: `${HOST}/api_play/api/api/placebet/${req.body.token}?play_type=${req.body.playType}&income=${req.body.amount}`,
                            method: 'GET',
                            headers: headers
                        };

                        console.log('----option----', option);

                        request(option, (err, response, body) => {
                            if (err) {
                                return res.send(err);
                            } else {

                                try {

                                    let resultBody = JSON.parse(body);

                                    console.log('result bet to muay step2 === ', resultBody)

                                    if(resultBody.code === 1){
                                        callback(null, memberInfo, resultBody);
                                    }else if(resultBody.error[0].code === 13){
                                        return res.send({
                                            message: "Odd change.",
                                            result: resultBody,
                                            code: 5003
                                        });

                                    }else if(resultBody.error[0].code === 11){
                                        return res.send({
                                            message: "Match close price..",
                                            result: resultBody,
                                            code: 5006
                                        });

                                    }else {
                                        callback(98, null);
                                    }

                                } catch (e) {
                                    callback(98, null);
                                }
                            }
                        });

                    }, (memberInfo, betResult, callback) => {

                        AgentGroupModel.findById(userInfo.group._id).select('_id type parentId shareSetting commissionSetting').populate({
                            path: 'parentId',
                            select: '_id type parentId shareSetting commissionSetting name',
                            populate: {
                                path: 'parentId',
                                select: '_id type parentId shareSetting commissionSetting name',
                                populate: {
                                    path: 'parentId',
                                    select: '_id type parentId shareSetting commissionSetting name',
                                    populate: {
                                        path: 'parentId',
                                        select: '_id type parentId shareSetting commissionSetting name',
                                        populate: {
                                            path: 'parentId',
                                            select: '_id type parentId shareSetting commissionSetting name',
                                            populate: {
                                                path: 'parentId',
                                                select: '_id type parentId shareSetting commissionSetting name',
                                            }
                                        }
                                    }
                                }
                            }

                        }).exec(function (err, response) {
                            if (err)
                                callback(err, null);
                            else
                                callback(null, memberInfo, betResult, response);
                        });

                    }, (memberInfo, betResult, agentGroups, callback) => {


                        const oddType = req.body.oddType.toUpperCase();

                        if (oddType === 'AH' ||
                            oddType === 'AH1ST' ||
                            oddType === 'OU' ||
                            oddType === 'OU1ST' ||
                            oddType === 'OE' ||
                            oddType === 'OE1ST') {

                            let condition = {
                                memberId: mongoose.Types.ObjectId(req.body.memberId),
                                'hdp.matchId': `${betResult.res[0].detail[0].boxing_cat_name_show.boxing_cat_id}:${betResult.res[0].detail[0].boxing_id}`
                            };
                            condition['$or'] = [
                                {
                                    "status": "RUNNING"
                                },
                                {
                                    "status": "WAITING"
                                },
                                {
                                    "status": "DONE"
                                }
                            ];
                            BetTransactionModel.aggregate([
                                {
                                    $match: condition
                                },
                                {
                                    $project: {_id: 0}
                                },
                                {
                                    "$group": {
                                        "_id": {
                                            id: '$_id'
                                        },
                                        "amount": {$sum: '$amount'}
                                    }
                                }
                            ], function (err, results) {

                                if (err) {
                                    callback(err, '');
                                    return;
                                }

                                let totalBet = results[0] ? results[0].amount : 0;

                                console.log(memberInfo.limitSetting.other.m2.maxBetPerMatchHDP, 'totalbet ===',totalBet);

                                if ((totalBet + Number.parseFloat(req.body.amount)) > convertMinMaxByCurrency(memberInfo.limitSetting.other.m2.maxBetPerMatchHDP, currency, membercurrency)) {
                                    callback(5002, null);
                                } else {
                                    callback(null, memberInfo, betResult, agentGroups, totalBet);
                                }
                            });
                        } else {
                            callback(null, memberInfo, betResult, agentGroups, 0);
                        }

                }], (err, memberInfo, betResult, agentGroups, totalBet) => {

                        // console.log('---------2---------')
                        // --------------------------------------------------------------------------------------------------------------------------------------------



                        if (err) {
                            console.log(err);
                            if (err === 5001) {
                                return res.send({
                                    message: "amount exceeded (per bet)",
                                    result: null,
                                    code: 5001
                                });
                            }
                            else if (err === 5002) {
                                return res.send({
                                    message: "Your total bet has exceeded the maximum bet per match3.",
                                    result: null,
                                    code: 5002
                                });
                            }
                            else if (err === 98) {
                                return res.send({
                                    message: "Fail To Betting.",
                                    result: null,
                                    code: 98
                                });
                            }


                            else if (err === 11) {
                                return res.send({
                                    message: "Match is close.",
                                    result: null,
                                    code: 5006
                                });
                            }
                            else if (err === 13) {
                                return res.send({
                                    message: "odd was already changed.",
                                    result: null,
                                    code: 5003
                                });
                            }
                            else if (err === 23) {
                                return res.send({
                                    message: "Your total bet has exceeded the maximum bet per match1.",
                                    result: null,
                                    code: 5002
                                });
                            }
                            else if (err === 24) {
                                return res.send({
                                    message: "Your total bet has exceeded the maximum bet per match2.",
                                    result: null,
                                    code: 5002
                                });
                            }
                            else if (err === 999) {
                                return res.send({
                                    message: "Under Maintenance.",
                                    result: null,
                                    code: 999
                                });
                            }
                            else if (err === 4101) {
                                return res.send({
                                    message: "Invalid Agent.",
                                    result: null,
                                    code: 4101
                                });
                            }

                            return res.send({message: "error", result: err, code: 999});
                        }


                        let memberCredit = roundTo(req.body.odd < 0 ? req.body.amount * req.body.odd : (req.body.amount * -1), 2);
                        let processDate = DateUtils.getCurrentDate();
                        // console.log('bet result ==== ', betResult);
                        let body = {
                            betId: generateBetId(),
                            memberId: req.body.memberId,
                            memberParentGroup: memberInfo.group,
                            memberUsername: memberInfo.username_lower,
                            hdp: {
                                leagueId: betResult.res[0].detail[0].boxing_cat_name_show.boxing_cat_id,
                                matchId: `${betResult.res[0].detail[0].boxing_cat_name_show.boxing_cat_id}:${betResult.res[0].detail[0].boxing_id}`,
                                leagueName: betResult.res[0].detail[0].boxing_cat_name_show.boxing_cat_name_en,
                                leagueNameN: {
                                    en : betResult.res[0].detail[0].boxing_cat_name_show.boxing_cat_name_en,
                                    th : betResult.res[0].detail[0].boxing_cat_name_show.boxing_cat_name_th,
                                    cn : betResult.res[0].detail[0].boxing_cat_name_show.boxing_cat_name_en,
                                    tw : betResult.res[0].detail[0].boxing_cat_name_show.boxing_cat_name_en
                                },
                                matchName: {
                                    en : {
                                        h : betResult.res[0].detail[0].team_red_show.boxer_name_en,
                                        a : betResult.res[0].detail[0].team_blue_show.boxer_name_en
                                    },
                                    th : {
                                        h : betResult.res[0].detail[0].team_red_show.boxer_name_th,
                                        a : betResult.res[0].detail[0].team_blue_show.boxer_name_th
                                    },
                                    cn : {
                                        h : betResult.res[0].detail[0].team_red_show.boxer_name_en,
                                        a : betResult.res[0].detail[0].team_blue_show.boxer_name_en
                                    },
                                    tw : {
                                        h : betResult.res[0].detail[0].team_red_show.boxer_name_en,
                                        a : betResult.res[0].detail[0].team_blue_show.boxer_name_en
                                    }
                                },
                                matchDate: DateUtils.convertThaiDate(moment(betResult.res[0].detail[0].fight_datetime).toDate()),
                                oddKey: '',
                                odd: req.body.odd,
                                oddType: req.body.oddType,
                                handicap: 0,
                                bet: req.body.bet,
                                score: req.body.round,
                                matchType: req.body.matchType,
                                rowID: ""//hdpInfo.detail.rowID
                            },
                            amount: req.body.amount,
                            memberCredit: memberCredit,
                            payout: req.body.payout,
                            gameType: 'TODAY',
                            acceptHigherPrice: req.body.acceptAnyOdd,
                            priceType: req.body.priceType.toUpperCase(),
                            game: game,
                            source: source,
                            status: 'RUNNING',
                            commission: {},
                            gameDate: DateUtils.convertThaiDate(moment(betResult.res[0].detail[0].fight_datetime).toDate()),
                            createdDate: processDate,
                            ipAddress: Utility.getIP(req),
                            currency: memberInfo.currency,
                            isOfflineCash: isOfflineCash,
                            active: !isOfflineCash,
                            liveTimeBet: req.body.round,
                            ref1: betResult.res[0].bet_id
                        };

                        prepareCommission(memberInfo, body, agentGroups, null, 0, 0);

                        console.log('BODY ------: ', JSON.stringify(body));

                        async.waterfall([callback => {
                            console.log('============== 1 : SAVE TRANSACTION ============');
                            if (userInfo.group.type === 'API') {

                                body.username = memberInfo.username;
                                body.gameType = 'TODAY';
                                body.sportType = source;

                                AgentGroupModel.findById(memberInfo.group, 'endpoint', function (err, agentResponse) {

                                    ApiService.placeBet(agentResponse.endpoint, body, (err, apiResponse) => {

                                        if (err) {
                                            if (err.code) {
                                                callback(err.code, null);
                                            } else {
                                                callback(999, null);
                                            }
                                        } else {
                                            let model = new BetTransactionModel(body);
                                            model.save(function (err, betResponse) {

                                                if (err) {
                                                    callback(999, null);
                                                } else {
                                                    let betResponse1 = Object.assign({}, betResponse)._doc;

                                                    betResponse1.playerBalance = apiResponse.balance;
                                                    callback(null, betResponse1);
                                                }
                                            });
                                        }

                                    });
                                });
                            } else {

                                // console.log('body : ', JSON.stringify(body));
                                let model = new BetTransactionModel(body);
                                model.save((err, betResponse) => {
                                    if (err) {
                                        console.error(err);
                                        callback(999, null);
                                    } else {

                                        callback(null, betResponse);
                                    }
                                });
                            }

                        }, (betResponse, callback) => {

                            if (userInfo.group.type === 'API') {
                                callback(null, betResponse, {});
                            } else {
                                MemberService.updateBalance2(memberInfo.username_lower, memberCredit, betResponse.betId, 'SPORTS_BOOK_BET', betResponse.betId, function (err, response) {
                                    if (err) {
                                        callback(5008, null);
                                    } else {
                                        callback(null, betResponse, response);
                                    }
                                });
                            }

                        }, (betResponse, balance, callback) => {

                            callback(null, betResponse, balance, 'robotResponse');

                        }], function (err, betResponse, balance, robotResponse) {

                            let hdpInfo = {
                                leagueName: betResult.res[0].detail[0].boxing_cat_name_show.boxing_cat_name_en,
                                leagueNameN: {
                                    en : betResult.res[0].detail[0].boxing_cat_name_show.boxing_cat_name_en,
                                    th : betResult.res[0].detail[0].boxing_cat_name_show.boxing_cat_name_th,
                                    cn : betResult.res[0].detail[0].boxing_cat_name_show.boxing_cat_name_en,
                                    tw : betResult.res[0].detail[0].boxing_cat_name_show.boxing_cat_name_en
                                },
                                matchId: betResult.res[0].detail[0].boxing_id,
                                matchName: {
                                    en : {
                                        h : betResult.res[0].detail[0].team_red_show.boxer_name_en,
                                        a : betResult.res[0].detail[0].team_blue_show.boxer_name_en
                                    },
                                    th : {
                                        h : betResult.res[0].detail[0].team_red_show.boxer_name_th,
                                        a : betResult.res[0].detail[0].team_blue_show.boxer_name_th
                                    },
                                    cn : {
                                        h : betResult.res[0].detail[0].team_red_show.boxer_name_en,
                                        a : betResult.res[0].detail[0].team_blue_show.boxer_name_en
                                    },
                                    tw : {
                                        h : betResult.res[0].detail[0].team_red_show.boxer_name_en,
                                        a : betResult.res[0].detail[0].team_blue_show.boxer_name_en
                                    }
                                },
                                matchDate: DateUtils.convertThaiDate(moment(betResult.res[0].detail[0].fight_datetime).toDate()),
                                oddType: req.body.oddType,
                                odd: req.body.odd,
                                handicap: 0,
                                bet: req.body.bet
                            }

                            if (err) {

                                // Reject ticket partner
                                Muaystep2Service.rejectTicketPartner(betResult.res[0].bet_id, (err, result) => {

                                });

                                if (err == 5007) {
                                    return res.send({
                                        message: "createOddAdjustment failed.",
                                        result: hdpInfo,
                                        code: 5007
                                    });
                                } else if (err == 5008) {
                                    return res.send({
                                        message: "update credit failed",
                                        result: hdpInfo,
                                        code: 5008
                                    });
                                } else if (err == 1003) {
                                    return res.send({
                                        message: "Insufficient Balance",
                                        code: 1003
                                    });
                                } else {
                                    return res.send({
                                        message: "Internal Server Error",
                                        result: err,
                                        code: 999
                                    });
                                }

                            } else {

                                MemberService.getCredit(userInfo._id, function (err, credit) {
                                    if (err) {
                                        return res.send({
                                            message: "get credit fail",
                                            result: err,
                                            code: 999
                                        });
                                    } else {

                                        return res.send({
                                            code: 0,
                                            message: "success",
                                            result: {
                                                betId: betResponse.betId,
                                                hdpInfo: hdpInfo,
                                                creditLimit: userInfo.group.type === 'API' ? betResponse.playerBalance : credit
                                            }
                                        });
                                    }
                                });
                            }
                        });



                        // ---------------------------------------------------------------------------------------------------------------------





                    });
                });
            }
        }
    );
});


const addMixParlaySchema = Joi.object().keys({
    token: Joi.string().required(),
    memberId: Joi.string().required(),
    matches: Joi.array().items({
        matchId: Joi.string().required(),
        odd: Joi.number().required(),
        oddType: Joi.string().required(),
        bet: Joi.string().allow(''),
        round: Joi.string().required(),
        matchType: Joi.string().required()
    }),
    odds: Joi.number().required(),
    amount: Joi.number().required(),
    payout: Joi.number().required()
});

router.post('/mixParlay', function (req, res) {

    let validate = Joi.validate(req.body, addMixParlaySchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }


    const userInfo = req.userInfo;
    const source         = 'M2_MUAY';
    const game         = 'M2';
    let isOfflineCash = false;
    let oddAdjust;
    const matchStatus = 'RUNNING';
    const playType = 'multi';

    MemberService.getOddAdjustment(req.userInfo.username, (err, data) => {
        if (err) {
            console.log('Get Odd Adjest error');
        } else {
            oddAdjust = data;
        }
    });


    async.series([callback => {

        MemberModel.findById(req.body.memberId, function (err, response) {
            if (err) {
                callback(err, null);
                return;
            }

            if (response.suspend) {
                callback(5011, null);
            } else if(!response.limitSetting.other.m2.isEnable){
                callback(5013,null);
            } else {
                AgentService.getUpLineStatus(userInfo.group._id, function (err, status) {

                    if (err) {
                        callback(err, null);
                    } else {
                        if (status.suspend) {
                            callback(5011, null);
                        } else if(!status.parlayEnable){
                            callback(5013,null);
                        }  else {
                            callback(null, status);
                        }
                    }
                });
            }
        });

    }, callback => {

        if (userInfo.group.type === 'API') {
            callback(null, 0);
        } else {
            MemberService.getCredit(userInfo._id, function (err, credit) {
                if (err) {
                    callback(err, null);
                } else {
                    if (credit < req.body.amount) {
                        callback(5005, null);
                    } else {
                        callback(null, credit);
                    }
                }
            });
        }

    }, callback => {
        callback(null, 0);

    }, (callback) => {
        if (_.isEqual(CLIENT_NAME, 'AMB_SPORTBOOK') ||
            _.isEqual(CLIENT_NAME, 'FASTBET98') ||
            _.isEqual(CLIENT_NAME, 'FAST98')||
            _.isEqual(CLIENT_NAME, 'MGM') ||
            _.isEqual(CLIENT_NAME, 'CSR') ||
            _.isEqual(CLIENT_NAME, 'AFC1688') ||
            _.isEqual(CLIENT_NAME, 'SPORTBOOK88') ||
            _.isEqual(CLIENT_NAME, 'IRON')) {
            AgentService.getOfflineCashStatus(userInfo.username, (error, isOfflineCashResult) => {
                if (error) {
                    callback(error, false);
                } else {
                    callback(null, isOfflineCashResult);
                    isOfflineCash = isOfflineCashResult;
                }
            });
        } else {
            callback(null, false);
        }
    }], (err, results) =>{

        if (err) {
            if (err === 5004) {
                return res.send({message: "Payout not matched.", result: null, code: 5004});

            } else if (err === 5005) {
                return res.send({message: "Credit exceed.", result: null, code: 5005});

            } else if (err === 5011) {
                return res.send({
                    message: "Account or Upline was suspend.",
                    result: null,
                    code: 5011
                });
            }
            else if (err === 5013) {
                return res.send({message: "Service Lock.", result: null, code: 5013});
            }
            return res.send({message: "error", result: err, code: 999});
        }

        async.waterfall([callback => {

            MemberModel.findById(req.body.memberId, function (err, response) {
                if (err) {
                    callback(err, null);
                } else {

                    let matchCount = req.body.matches.length;
                    let payout = roundTo(req.body.amount / req.body.odds, 2);

                    console.log('payout : ', payout);

                    let maxBet = response.limitSetting.other.m2.maxPerBet;
                    let minBet = response.limitSetting.other.m2.minPerBet;
                    let maxPayout = response.limitSetting.other.m2.maxPayPerBill;
                    let maxMatchPerBet = response.limitSetting.other.m2.maxMatchPerBet;
                    let minMatchPerBet = response.limitSetting.other.m2.minMatchPerBet;

                    if (req.body.amount > maxBet) {
                        callback(5001, null);
                    } else if (req.body.amount < minBet) {
                        callback(5001, null);
                    } else if (matchCount < minMatchPerBet) {
                        callback(5020, null);
                    } else if (matchCount > maxMatchPerBet) {
                        callback(5021, null);
                    } else if (payout > maxPayout) {
                        callback(5012, null);
                    } else {
                        callback(null, response);
                    }
                }
            });

        }, (memberInfo, callback) => {


            let todayDate;
            if (moment().hours() >= 11) {
                todayDate = moment().utc(true);
            } else {
                todayDate = moment().add(-1, 'days').utc(true);
            }

            let condition = {};
            condition['createdDate'] = {
                "$gte": new Date(todayDate.add(0, 'days').utc(true).format('YYYY-MM-DDT11:00:00.000')),
                "$lt": new Date(todayDate.add(1, 'd').utc(true).format('YYYY-MM-DDT11:00:00.000'))
            };
            condition['memberId'] = mongoose.Types.ObjectId(req.body.memberId);
            condition['$or'] = [{'status': 'DONE'}, {'status': 'RUNNING'}];
            BetTransactionModel.aggregate([
                {
                    $match: condition
                },
                {
                    $project: {_id: 0}
                },
                {
                    "$group": {
                        "_id": {
                            id: '$_id'
                        },
                        "amount": {$sum: '$amount'}
                    }
                }
            ], function (err, results) {

                if (err) {
                    callback(err, '');
                } else {
                    callback(null, memberInfo, results[0] ? results[0].amount : 0);
                }
            });

        }, (memberInfo, totalBet, callback) => {

            //Bet to Muay Step2

            const option = {
                url: `${HOST}/api_play/api/api/placebet/${req.body.token}?play_type=${playType}&income=${req.body.amount}`,
                method: 'GET',
                headers: headers
            };

            console.log('----option----', option);

            request(option, (err, response, body) => {
                if (err) {
                    return res.send(err);
                } else {

                    try {

                        let resultBody = JSON.parse(body);

                        console.log('result bet Parlay to muay step2 === ', resultBody)

                        if(resultBody.code === 1){
                            callback(null, memberInfo, totalBet, resultBody.res[0]);
                        }else if(resultBody.error[0].code === 13){
                            return res.send({
                                message: "Odd change.",
                                result: resultBody,
                                code: 5003
                            });

                        }else if(resultBody.error[0].code === 11){
                            return res.send({
                                message: "Match close price..",
                                result: resultBody,
                                code: 5006
                            });

                        }else {
                            console.log('---------ddddd---------', resultBody)
                            callback(98, null);
                        }

                    } catch (e) {
                        console.log('---------aaaaaaa---------', e)
                        callback(98, null);
                    }
                }
            });

        }, (memberInfo, totalBet, matches, callback) => {

            AgentGroupModel.findById(userInfo.group._id).select('_id type parentId shareSetting commissionSetting').populate({
                path: 'parentId',
                select: '_id type parentId shareSetting commissionSetting name',
                populate: {
                    path: 'parentId',
                    select: '_id type parentId shareSetting commissionSetting name',
                    populate: {
                        path: 'parentId',
                        select: '_id type parentId shareSetting commissionSetting name',
                        populate: {
                            path: 'parentId',
                            select: '_id type parentId shareSetting commissionSetting name',
                            populate: {
                                path: 'parentId',
                                select: '_id type parentId shareSetting commissionSetting name',
                                populate: {
                                    path: 'parentId',
                                    select: '_id type parentId shareSetting commissionSetting name',
                                }
                            }
                        }
                    }
                }
            }).exec(function (err, response) {
                if (err)
                    callback(err, null);
                else
                    callback(null, memberInfo, totalBet, matches, response);
            });


        }], (err, memberInfo, totalBet, matches, agentGroups) => {

            if (err) {
                if (err === 5001) {
                    return res.send({
                        message: "amount exceeded (per bet)",
                        result: null,
                        code: 5001
                    });
                }
                else if (err === 5002) {
                    return res.send({
                        message: "amount exceeded (per match)",
                        result: null,
                        code: 5001
                    });
                }
                else if (err === 5003) {
                    return res.send({
                        message: "odd was already changed.",
                        result: matches,
                        code: 5003
                    });
                } else if (err === 5012) {
                    return res.send({
                        message: "Over maximum bet amount.",
                        result: matches,
                        code: 5012
                    });
                } else if (err === 5020) {
                    return res.send({
                        message: "Match Count < min setting.",
                        result: matches,
                        code: 5020
                    });
                } else if (err === 5021) {
                    return res.send({
                        message: "Match Count > max setting.",
                        result: matches,
                        code: 5021
                    });
                }
                else if (err === 9999) {
                    return res.send({
                        message: "err oddMatch.length != req.body.matches.length",
                        result: matches,
                        code: 9999
                    });
                }
                else if (err === 98) {
                    return res.send({
                        message: "Fail To Betting.",
                        result: null,
                        code: 98
                    });
                }
                else if (err === 11) {
                    return res.send({
                        message: "Match is close.",
                        result: null,
                        code: 5006
                    });
                }
                else if (err === 13) {
                    return res.send({
                        message: "odd was already changed.",
                        result: null,
                        code: 5003
                    });
                }
                else if (err === 23) {
                    return res.send({
                        message: "Your total bet has exceeded the maximum bet per match.",
                        result: null,
                        code: 5002
                    });
                }
                else if (err === 24) {
                    return res.send({
                        message: "Your total bet has exceeded the maximum bet per match.",
                        result: null,
                        code: 5002
                    });
                }
                return res.send({message: "error", result: err, code: 999});
            }

            console.log('totalBetParlay ::: ',totalBet);
            console.log((totalBet + req.body.amount) > memberInfo.limitSetting.other.m2.maxBetPerDay)

            if((totalBet + req.body.amount) > memberInfo.limitSetting.other.m2.maxBetPerDay){
                return res.send({
                    message: "amount exceeded (per day)",
                    result: matches,
                    code: 5022
                });
            }

            // console.log('matches-------------------------------',matches);
            let matchTransform = [];
            _.each(matches.detail, matchInfo => {
                matchTransform.push({
                    leagueId: matchInfo.boxing_cat_name_show.boxing_cat_id,
                    matchId: `${matchInfo.boxing_cat_name_show.boxing_cat_id}:${matchInfo.boxing_id}`,
                    leagueName: matchInfo.boxing_cat_name_show.boxing_cat_name_en,
                    leagueNameN: {
                        en : matchInfo.boxing_cat_name_show.boxing_cat_name_en,
                        th : matchInfo.boxing_cat_name_show.boxing_cat_name_th,
                        cn : matchInfo.boxing_cat_name_show.boxing_cat_name_en,
                        tw : matchInfo.boxing_cat_name_show.boxing_cat_name_en
                    },
                    matchName: {
                        en : {
                            h : matchInfo.team_red_show.boxer_name_en,
                            a : matchInfo.team_blue_show.boxer_name_en
                        },
                        th : {
                            h : matchInfo.team_red_show.boxer_name_th,
                            a : matchInfo.team_blue_show.boxer_name_th
                        },
                        cn : {
                            h : matchInfo.team_red_show.boxer_name_en,
                            a : matchInfo.team_blue_show.boxer_name_en
                        },
                        tw : {
                            h : matchInfo.team_red_show.boxer_name_en,
                            a : matchInfo.team_blue_show.boxer_name_en
                        }
                    },
                    matchDate: DateUtils.convertThaiDate(moment(matchInfo.fight_datetime).toDate()),
                    odd: matchInfo.price,
                    oddType: 'AH',
                    handicap: 0,
                    bet: matchInfo.choose_team == 'red'? 'HOME' : 'AWAY',
                    score: matchInfo.round,
                    // ref1: matchInfo.bet_id,
                    matchType: matchInfo.round == '0 : 0'? 'NONE_LIVE' : 'LIVE'
                });
            });

            let oddCount = 1;
            _.each(matchTransform, obj => {
                oddCount *= parseFloat(obj.odd);
                // console.log(oddCount);
                console.log('######### => ', obj.odd);
            });
            // console.log(req.body.amount);
            oddCount = roundTo.down(oddCount, 3);
            // console.log('oddCount ======', oddCount);
            let payoutNew = roundTo(req.body.amount * oddCount, 2);
            // console.log(payoutNew);

            let body = {
                betId: generateBetId(),
                memberId: req.body.memberId,
                memberParentGroup: memberInfo.group,
                memberUsername: memberInfo.username_lower,
                parlay: {
                    matches: matchTransform,
                    odds: oddCount
                },
                amount: req.body.amount,
                memberCredit: (req.body.amount * -1),
                payout: payoutNew,
                gameType: 'PARLAY',
                acceptHigherPrice: false,
                priceType: 'EU',
                game: game,
                source: source,
                status: matchStatus,
                commission: {},
                gameDate: findGameDate(matchTransform),
                createdDate: DateUtils.getCurrentDate(),
                ipAddress: Utility.getIP(req),
                currency: memberInfo.currency,
                isOfflineCash: isOfflineCash,
                active: !isOfflineCash,
                ref1: matches.bet_id,
            };

            // console.log('body === ',body);
            prepareCommission(memberInfo, body, agentGroups, null, 0, 0);

            let model = new BetTransactionModel(body);
            model.save(function (err, betResponse) {

                if (err) {
                    return res.send({message: "error", result: err, code: 999});
                }

                if (userInfo.group.type === 'API') {

                    let body = Object.assign({}, betResponse)._doc;
                    body.username = memberInfo.username;
                    body.gameType = 'PARLAY';
                    body.sportType = 'M2';

                    AgentGroupModel.findById(memberInfo.group, 'endpoint', function (err, agentResponse) {

                        ApiService.placeBet(agentResponse.endpoint, body, (err, response) => {
                            if (err) {
                                return res.send({
                                    code: 999,
                                    message: "error",
                                    result: err
                                });
                            } else {

                                return res.send({
                                    code: 0,
                                    message: "success",
                                    result: {
                                        betId: betResponse.betId,
                                        parlayInfo: betResponse.parlay
                                    }
                                });
                            }

                        });
                    });
                } else {
                    async.waterfall([callback => {

                        MemberService.updateBalance2(memberInfo.username_lower, (req.body.amount * -1), betResponse.betId, 'SPORTS_BOOK_BET', betResponse.betId, function (err, response) {
                            if (err) {
                                callback(5008, null);
                            } else {
                                callback(null, response);
                            }
                        });

                    }, (updateBalanceResponse, callback) => {

                        MemberService.getCredit(userInfo._id, function (err, credit) {
                            if (err) {
                                callback(999, null);
                            } else {
                                callback(null, updateBalanceResponse, credit);
                            }
                        });

                    }], function (err, balanceResponse, credit) {

                        if (err) {
                            return res.send({message: "error", result: err, code: 999});
                        }
                        return res.send(
                            {
                                code: 0,
                                message: "success",
                                result: {
                                    betId: betResponse.betId,
                                    parlayInfo: betResponse.parlay,
                                    creditLimit: credit
                                }
                            }
                        );

                    });
                }
            });

        });

    });


});

function findGameDate(matches) {
    let matchDate = '';
    for(let i=0;i<matches.length;i++){
        if(matchDate){
            if(new Date(matchDate) < new Date(matches[i].matchDate)){
                matchDate = matches[i].matchDate
            }
        }else {
            matchDate = matches[i].matchDate;
        }
    }
    return matchDate;
}



function findMaxBet(memberInfo, oddType) {

    switch (oddType.toUpperCase()) {
        case 'AH':
        case 'AH1ST':
        case 'OU':
        case 'OU1ST':
        case 'OE':
        case 'OE1ST':
        case 'T1OU':
        case 'T2OU':
            return memberInfo.limitSetting.other.m2.maxPerBetHDP;
        case 'X12':
        case 'X121ST':
            return memberInfo.limitSetting.other.m2.maxPerBetHDP;
    }
}

// function checkOddProcess(bpResponse, priceType, oddType, oddKey, odd, handicap, callback) {
//
//
//     console.log('----- :::::: ', oddType);
//     console.log('----- :::::: ', oddKey);
//
//     let handicapPrefix, oddPrefix;
//     let type = oddType.toLowerCase();
//     let isOddNotMatch = false;
//     let oddValue, handicapValue;
//     let key = '';
//     let bet = '';
//
//     if (['TG', 'DC', 'CS', 'FHCS', 'FTHT', 'FGLG', 'TTKO', 'TC'].includes(oddType.toUpperCase())) {
//         key = oddKey;
//     }
//
//     if (oddType === 'AH' || oddType === 'AH1ST') {
//
//         _Utils.findKeyByValue(bpResponse[type], oddKey, function (keyResponse) {
//             key = keyResponse;
//         });
//
//         if (key === 'hpk') {
//             handicapPrefix = 'h';
//             oddPrefix = 'hp';
//             bet = 'HOME';
//         } else {
//             handicapPrefix = 'a';
//             oddPrefix = 'ap';
//             bet = 'AWAY';
//         }
//
//         if (!_.isUndefined(bpResponse[type])) {
//             console.log(bpResponse[type][oddPrefix] + '  1:  ' + odd);
//
//             oddValue = bpResponse[type][oddPrefix];
//             oddValue = convertOdd(priceType, oddValue, oddType);
//             handicapValue = bpResponse[type][handicapPrefix];
//             if (oddValue !== odd) {
//                 isOddNotMatch = true;
//             }
//             if (handicapValue !== handicap) {
//                 isOddNotMatch = true;
//             }
//         } else {
//             isOddNotMatch = true;
//         }
//
//     } else if (oddType === 'OU' || oddType === 'OU1ST' || oddType === 'T1OU' || oddType === 'T2OU') {
//
//         _Utils.findKeyByValue(bpResponse[type], oddKey, function (keyResponse) {
//             key = keyResponse;
//         });
//
//
//         if (key === 'opk') {
//             handicapPrefix = 'o';
//             oddPrefix = 'op';
//             bet = 'OVER';
//         } else {
//             handicapPrefix = 'u';
//             oddPrefix = 'up';
//             bet = 'UNDER';
//         }
//
//         if (!_.isUndefined(bpResponse[type])) {
//             oddValue = bpResponse[type][oddPrefix];
//             oddValue = convertOdd(priceType, oddValue, oddType);
//             handicapValue = bpResponse[type][handicapPrefix];
//             if (oddValue !== odd) {
//                 isOddNotMatch = true;
//             }
//             if (handicapValue !== handicap) {
//                 isOddNotMatch = true;
//             }
//         } else {
//             isOddNotMatch = true;
//         }
//
//     } else if (oddType === 'X12' || oddType === 'X121ST' || oddType === 'ML') {
//
//         _Utils.findKeyByValue(bpResponse[type], oddKey, function (keyResponse) {
//             key = keyResponse;
//         });
//
//         if (key === 'hk') {
//             oddPrefix = 'h';
//             bet = 'HOME';
//         } else if (key === 'ak') {
//             oddPrefix = 'a';
//             bet = 'AWAY';
//         } else if (key === 'dk') {
//             oddPrefix = 'd';
//             bet = 'DRAW';
//         }
//
//         if (!_.isUndefined(bpResponse[type])) {
//             oddValue = bpResponse[type][oddPrefix];
//             oddValue = convertOdd(priceType, oddValue, oddType);
//             if (oddValue !== odd) {
//                 isOddNotMatch = true;
//             }
//         } else {
//             isOddNotMatch = true;
//         }
//
//     } else if (oddType === 'OE' || oddType === 'OE1ST') {
//
//         _Utils.findKeyByValue(bpResponse[type], oddKey, function (keyResponse) {
//             key = keyResponse;
//         });
//
//         if (key === 'ok') {
//             oddPrefix = 'o';
//             bet = 'ODD';
//         } else if (key === 'ek') {
//             oddPrefix = 'e';
//             bet = 'EVEN';
//         }
//
//         if (!_.isUndefined(bpResponse[type])) {
//             oddValue = bpResponse[type][oddPrefix];
//             oddValue = convertOdd(priceType, oddValue, oddType);
//             if (oddValue !== odd) {
//                 isOddNotMatch = true;
//             }
//         } else {
//             isOddNotMatch = true;
//         }
//     } else if (oddType === 'OE' || oddType === 'OE1ST') {
//
//         _Utils.findKeyByValue(bpResponse[type], oddKey, function (keyResponse) {
//             key = keyResponse;
//         });
//
//         if (key === 'ok') {
//             oddPrefix = 'o';
//             bet = 'ODD';
//         } else if (key === 'ek') {
//             oddPrefix = 'e';
//             bet = 'EVEN';
//         }
//
//         if (!_.isUndefined(bpResponse[type])) {
//             oddValue = bpResponse[type][oddPrefix];
//             oddValue = convertOdd(priceType, oddValue, oddType);
//             if (oddValue !== odd) {
//                 isOddNotMatch = true;
//             }
//         } else {
//             isOddNotMatch = true;
//         }
//     } else if (oddType === 'TG') {
//         if (key === 'tg01k') {
//             bet = 'TG01';
//         } else if (key === 'tg02k') {
//             bet = 'TG23';
//         } else if (key === 'tg03k') {
//             bet = 'TG46';
//         } else {
//             bet = 'TG7OVER';
//         }
//         oddValue = odd;
//         handicapValue = handicap;
//         isOddNotMatch = false;
//
//     } else if (oddType === 'DC') {
//         if (key === 'hdk') {
//             bet = 'DC1X';
//         } else if (key === 'hak') {
//             bet = 'DC12';
//         } else {
//             bet = 'DC2X';
//         }
//         oddValue = odd;
//         handicapValue = handicap;
//         isOddNotMatch = false;
//
//     } else if (oddType === 'CS' || oddType === 'FHCS') {
//         if (key === 'd00k') {
//             bet = 'CS00';
//         } else if (key === 'a01k') {
//             bet = 'CS01';
//         } else if (key === 'a02k') {
//             bet = 'CS02';
//         } else if (key === 'a03k') {
//             bet = 'CS03';
//         } else if (key === 'a04k') {
//             bet = 'CS04';
//         } else if (key === 'h10k') {
//             bet = 'CS10';
//         } else if (key === 'd11k') {
//             bet = 'CS11';
//         } else if (key === 'a12k') {
//             bet = 'CS12';
//         } else if (key === 'a13k') {
//             bet = 'CS13';
//         } else if (key === 'a14k') {
//             bet = 'CS14';
//         } else if (key === 'h20k') {
//             bet = 'CS20';
//         } else if (key === 'h21k') {
//             bet = 'CS21';
//         } else if (key === 'd22k') {
//             bet = 'CS22';
//         } else if (key === 'a23k') {
//             bet = 'CS23';
//         } else if (key === 'a24k') {
//             bet = 'CS24';
//         } else if (key === 'h30k') {
//             bet = 'CS30';
//         } else if (key === 'h31k') {
//             bet = 'CS31';
//         } else if (key === 'h32k') {
//             bet = 'CS32';
//         } else if (key === 'd33k') {
//             bet = 'CS33';
//         } else if (key === 'a34k') {
//             bet = 'CS34';
//         } else if (key === 'h40k') {
//             bet = 'CS40';
//         } else if (key === 'h41k') {
//             bet = 'CS41';
//         } else if (key === 'h42k') {
//             bet = 'CS42';
//         } else if (key === 'h3k') {
//             bet = 'CS43';
//         } else if (key === 'd44k') {
//             bet = 'CS44';
//         } else {
//             bet = 'CSAOS';
//         }
//         oddValue = odd;
//         handicapValue = handicap;
//         isOddNotMatch = false;
//
//     } else if (oddType === 'FTHT') {
//         if (key === 'hhk') {
//             bet = 'FTHT_HH';
//         } else if (key === 'hdk') {
//             bet = 'FTHT_HD';
//         } else if (key === 'hak') {
//             bet = 'FTHT_HA';
//         } else if (key === 'dhk') {
//             bet = 'FTHT_DH';
//         } else if (key === 'ddk') {
//             bet = 'FTHT_DD';
//         } else if (key === 'dak') {
//             bet = 'FTHT_DA';
//         } else if (key === 'ahk') {
//             bet = 'FTHT_AH';
//         } else if (key === 'adk') {
//             bet = 'FTHT_AD';
//         } else {
//             bet = 'FTHT_AA';
//         }
//         oddValue = odd;
//         handicapValue = handicap;
//         isOddNotMatch = false;
//
//     } else if (oddType === 'FGLG') {
//         if (key === 'hfk') {
//             bet = 'FGLGFH';
//         } else if (key === 'hlk') {
//             bet = 'FGLGHL';
//         } else if (key === 'afk') {
//             bet = 'FGLGAF';
//         } else if (key === 'alk') {
//             bet = 'FGLGAL';
//         } else {
//             bet = 'FGLGNG';
//         }
//         oddValue = odd;
//         handicapValue = handicap;
//         isOddNotMatch = false;
//
//     } else if (oddType === 'TTKO') {
//         if (key === 'hk') {
//             bet = 'HOME';
//         } else {
//             bet = 'AWAY';
//         }
//         oddValue = odd;
//         handicapValue = handicap;
//         isOddNotMatch = false;
//
//     } else {
//         console.log('oddType not match')
//     }
//     console.log('input_odd : ', odd + '  , oddValue : ' + oddValue);
//     console.log('input_handicap : ', handicap + '  , handicapValue : ' + handicapValue);
//     console.log('isOddNotMatch : ', isOddNotMatch);
//     callback(isOddNotMatch, key, oddValue, handicapValue, bet);
//
// }


function prepareCommission(memberInfo, body, currentAgent, overAgent, remaining, overAllShare) {


    if (currentAgent && (currentAgent.type === 'SUPER_ADMIN' || currentAgent.parentId)) {


        console.log('==============================================');
        console.log('process : ' + currentAgent.type + ' : ' + currentAgent.name);
        console.log('currenctAgent : ',currentAgent)


        let currentAgentShareSettingParent;
        let currentAgentShareSettingOwn;
        let currentAgentShareSettingRemaining;
        let currentAgentShareSettingMin;

        let overAgentShareSettingParent;
        let overAgentShareSettingOwn;
        let overAgentShareSettingRemaining;
        let overAgentShareSettingMin;

        let agentCommission;


        if (body.gameType === 'PARLAY') {
            currentAgentShareSettingParent = currentAgent.shareSetting.other.m2.parent;
            currentAgentShareSettingOwn = currentAgent.shareSetting.other.m2.own;
            currentAgentShareSettingRemaining = currentAgent.shareSetting.other.m2.remaining;
            currentAgentShareSettingMin = currentAgent.shareSetting.other.m2.min;

            let parlayMatchCount = body.parlay.matches.length;

            if (overAgent) {
                overAgentShareSettingParent = overAgent.shareSetting.other.m2.parent;
                overAgentShareSettingOwn = overAgent.shareSetting.other.m2.own;
                overAgentShareSettingRemaining = overAgent.shareSetting.other.m2.remaining;
                overAgentShareSettingMin = overAgent.shareSetting.other.m2.min;
            } else {
                overAgent = {};
                overAgent.type = 'member';
                overAgentShareSettingParent = memberInfo.shareSetting.other.m2.parent;
                overAgentShareSettingOwn = 0;
                overAgentShareSettingRemaining = 0;

                body.commission.member = {};
                body.commission.member.parent = memberInfo.shareSetting.other.m2.parent;


                // if (parlayMatchCount == 2) {
                    body.commission.member.commission = memberInfo.commissionSetting.other.m2;
                // } else if (parlayMatchCount >= 3 && parlayMatchCount <= 4) {
                //     body.commission.member.commission = memberInfo.commissionSetting.step.com34;
                // } else if (parlayMatchCount >= 5 && parlayMatchCount <= 6) {
                //     body.commission.member.commission = memberInfo.commissionSetting.step.com56;
                // } else if (parlayMatchCount >= 7 && parlayMatchCount <= 8) {
                //     body.commission.member.commission = memberInfo.commissionSetting.step.com78;
                // } else if (parlayMatchCount >= 9 && parlayMatchCount <= 10) {
                //     body.commission.member.commission = memberInfo.commissionSetting.step.com910;
                // } else if (parlayMatchCount >= 11 && parlayMatchCount <= 12) {
                //     body.commission.member.commission = memberInfo.commissionSetting.step.com1112;
                // }else {
                //     body.commission.member.commission = 0;
                // }
            }

            // if (parlayMatchCount == 2) {
                agentCommission = currentAgent.commissionSetting.other.m2;
            // } else if (parlayMatchCount >= 3 && parlayMatchCount <= 4) {
            //     agentCommission = currentAgent.commissionSetting.step.com34;
            // } else if (parlayMatchCount >= 5 && parlayMatchCount <= 6) {
            //     agentCommission = currentAgent.commissionSetting.step.com56;
            // } else if (parlayMatchCount >= 7 && parlayMatchCount <= 8) {
            //     agentCommission = currentAgent.commissionSetting.step.com78;
            // } else if (parlayMatchCount >= 9 && parlayMatchCount <= 10) {
            //     agentCommission = currentAgent.commissionSetting.step.com910;
            // } else if (parlayMatchCount >= 11 && parlayMatchCount <= 12) {
            //     agentCommission = currentAgent.commissionSetting.step.com1112;
            // }


        } else {


            if (body.hdp.matchType === 'LIVE') {
                currentAgentShareSettingParent = currentAgent.shareSetting.other.m2.parent;
                currentAgentShareSettingOwn = currentAgent.shareSetting.other.m2.own;
                currentAgentShareSettingRemaining = currentAgent.shareSetting.other.m2.remaining;
                currentAgentShareSettingMin = currentAgent.shareSetting.other.m2.min;

                if (overAgent) {
                    overAgentShareSettingParent = overAgent.shareSetting.other.m2.parent;
                    overAgentShareSettingOwn = overAgent.shareSetting.other.m2.own;
                    overAgentShareSettingRemaining = overAgent.shareSetting.other.m2.remaining;
                    overAgentShareSettingMin = overAgent.shareSetting.other.m2.min;
                } else {
                    overAgent = {};
                    overAgent.type = 'member';
                    overAgentShareSettingParent = memberInfo.shareSetting.other.m2.parent;
                    overAgentShareSettingOwn = 0;
                    overAgentShareSettingRemaining = 0;

                    body.commission.member = {};
                    body.commission.member.parent = memberInfo.shareSetting.other.m2.parent;
                    body.commission.member.commission = memberInfo.commissionSetting.other.m2;
                }
            } else if (body.hdp.matchType === 'NONE_LIVE') {
                currentAgentShareSettingParent = currentAgent.shareSetting.other.m2.parent;
                currentAgentShareSettingOwn = currentAgent.shareSetting.other.m2.own;
                currentAgentShareSettingRemaining = currentAgent.shareSetting.other.m2.remaining;
                currentAgentShareSettingMin = currentAgent.shareSetting.other.m2.min;

                if (overAgent) {
                    overAgentShareSettingParent = overAgent.shareSetting.other.m2.parent;
                    overAgentShareSettingOwn = overAgent.shareSetting.other.m2.own;
                    overAgentShareSettingRemaining = overAgent.shareSetting.other.m2.remaining;
                    overAgentShareSettingMin = overAgent.shareSetting.other.m2.min;
                } else {
                    overAgent = {};
                    overAgent.type = 'member';
                    overAgentShareSettingParent = memberInfo.shareSetting.other.m2.parent;
                    overAgentShareSettingOwn = 0;
                    overAgentShareSettingRemaining = 0;

                    body.commission.member = {};
                    body.commission.member.parent = memberInfo.shareSetting.other.m2.parent;
                    body.commission.member.commission = memberInfo.commissionSetting.other.m2;
                }
            }

            if (['AH', 'OU', 'OE', 'AH1ST', 'OU1ST', 'OE1ST'].includes(body.hdp.oddType.toUpperCase())) {
                agentCommission = currentAgent.commissionSetting.other.m2;

                // if (memberInfo.commissionSetting.sportsBook.typeHdpOuOe === 'A') {
                //     agentCommission = currentAgent.commissionSetting.other.m2;
                // }
                // if (memberInfo.commissionSetting.sportsBook.typeHdpOuOe === 'B') {
                //     agentCommission = currentAgent.commissionSetting.other.m2;
                // }
                // if (memberInfo.commissionSetting.sportsBook.typeHdpOuOe === 'C') {
                //     agentCommission = currentAgent.commissionSetting.other.m2;
                // }
                // if (memberInfo.commissionSetting.sportsBook.typeHdpOuOe === 'D') {
                //     agentCommission = currentAgent.commissionSetting.other.m2;
                // }
                // if (memberInfo.commissionSetting.sportsBook.typeHdpOuOe === 'E') {
                //     agentCommission = currentAgent.commissionSetting.other.m2;
                // }
                // if (memberInfo.commissionSetting.sportsBook.typeHdpOuOe === 'F') {
                //     agentCommission = currentAgent.commissionSetting.other.m2;
                // }

            } else {
                body.commission.member.commission = memberInfo.commissionSetting.other.m2;
                agentCommission = currentAgent.commissionSetting.other.m2;
            }
        }

        let money = body.amount;

        console.log('');
        console.log('---- setting ----');
        console.log('ส่วนแบ่งที่ได้มามีทั้งหมด : ' + currentAgentShareSettingOwn + ' %');
        console.log('ขอส่วนแบ่งจาก : ' + overAgent.type + ' ' + overAgentShareSettingParent + ' %');
        console.log('ให้ส่วนแบ่ง : ' + overAgent.type + ' ที่เหลือไป ' + overAgentShareSettingOwn + ' %');
        console.log('remaining จาก : ' + overAgent.type + ' : ' + overAgentShareSettingRemaining + ' %');
        console.log('โดนบังคับเสียขั้นต่ำ : ' + currentAgentShareSettingMin + ' %');
        console.log('---- end setting ----');
        console.log('');


        // console.log('และได้รับ remaining ไว้ : '+overAgent.shareSetting.sportsBook.hdpOuOe.remaining+' %');

        let currentPercentReceive = overAgentShareSettingParent;
        console.log('% ที่ได้ : ' + currentPercentReceive);
        console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');
        let totalRemaining = remaining;

        if (overAgentShareSettingRemaining > 0) {
            // console.log()
            // console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');

            if (overAgentShareSettingRemaining > totalRemaining) {
                console.log('รับ remaining ทั้งหมดไว้: ' + totalRemaining + ' %');
                currentPercentReceive += totalRemaining;
                totalRemaining = 0;
            } else {
                totalRemaining = remaining - overAgentShareSettingRemaining;
                console.log('หักค่า remaining ที่ได้รับมากับที่เซตรับไว้ = ' + remaining + ' - ' + overAgentShareSettingRemaining + ' = ' + totalRemaining);
                currentPercentReceive += overAgentShareSettingRemaining;
            }

        }

        console.log('รวม % ที่ได้รับ : ' + currentPercentReceive);


        let unUseShare = currentAgentShareSettingOwn - (overAgentShareSettingParent + overAgentShareSettingOwn);

        console.log('เหลือส่วนแบ่งที่ไม่ได้ใช้ : ' + unUseShare + ' %');
        totalRemaining = (unUseShare + totalRemaining);
        console.log('ส่วนแบ่งที่ไม่ได้ใช้ บวกกับค่า remaining ท่ี่เหลือจะเป็น : ' + totalRemaining + ' %');

        console.log('จากที่โดนบังคับเสียขั้นต่ำ : ' + currentAgentShareSettingMin + ' %');
        console.log('overAllShare : ', overAllShare)
        if (currentAgentShareSettingMin > 0 && ((overAllShare + currentPercentReceive) < currentAgentShareSettingMin)) {

            if (currentPercentReceive < currentAgentShareSettingMin) {
                console.log('% ที่ได้รับ < ขั้นต่ำที่ถูกตั้ง min ไว้');
                let percent = currentAgentShareSettingMin - (currentPercentReceive + overAllShare);
                console.log('หักออกไป ' + percent + ' % : แล้วไปเพิ่มในส่วนที่ต้องรับผิดชอบ ');
                totalRemaining = totalRemaining - percent;
                if (totalRemaining < 0) {
                    totalRemaining = 0;
                }
                currentPercentReceive += percent;
            }
        } else {
            // currentPercentReceive += totalRemaining;
        }


        if (currentAgent.type === 'SUPER_ADMIN') {
            currentPercentReceive += totalRemaining;
        }
        let getMoney = (money * convertPercent(currentPercentReceive)).toFixed(2);
        overAllShare = overAllShare + currentPercentReceive;

        console.log('ยอดแทง ' + money + ' ได้ทั้งหมด : ' + currentPercentReceive + ' %');

        console.log('รวม % + % remaining  จะเป็นสัดส่วนที่ได้ทั้งหมด = ' + getMoney);
        console.log('ค่าที่จะถูกคืนกลับไป (remaining) : ' + totalRemaining + ' %');
        console.log('จำนวน % ที่ถูกแบ่งไปแล้วทั้งหมด : ', overAllShare);
        console.log('agentCommission : ', agentCommission)

        switch (currentAgent.type) {
            case 'SUPER_ADMIN':
                body.commission.superAdmin = {};
                body.commission.superAdmin.group = currentAgent._id;
                body.commission.superAdmin.parent = currentAgent.shareSetting.other.m2.parent;
                body.commission.superAdmin.own = currentAgent.shareSetting.other.m2.own;
                body.commission.superAdmin.remaining = currentAgent.shareSetting.other.m2.remaining;
                body.commission.superAdmin.min = currentAgent.shareSetting.other.m2.min;
                body.commission.superAdmin.shareReceive = Math.abs(currentPercentReceive);
                body.commission.superAdmin.commission = agentCommission;
                body.commission.superAdmin.amount = getMoney;
                break;
            case 'COMPANY':
                body.commission.company = {};
                body.commission.company.parentGroup = currentAgent.parentId;
                body.commission.company.group = currentAgent._id;
                body.commission.company.parent = currentAgentShareSettingParent;
                body.commission.company.own = currentAgentShareSettingOwn;
                body.commission.company.remaining = currentAgentShareSettingRemaining;
                body.commission.company.min = currentAgentShareSettingMin;
                body.commission.company.shareReceive = Math.abs(currentPercentReceive);
                body.commission.company.commission = agentCommission;
                body.commission.company.amount = getMoney;
                break;
            case 'SHARE_HOLDER':
                body.commission.shareHolder = {};
                body.commission.shareHolder.parentGroup = currentAgent.parentId;
                body.commission.shareHolder.group = currentAgent._id;
                body.commission.shareHolder.parent = currentAgentShareSettingParent;
                body.commission.shareHolder.own = currentAgentShareSettingOwn;
                body.commission.shareHolder.remaining = currentAgentShareSettingRemaining;
                body.commission.shareHolder.min = currentAgentShareSettingMin;
                body.commission.shareHolder.shareReceive = currentPercentReceive;
                body.commission.shareHolder.commission = agentCommission;
                body.commission.shareHolder.amount = getMoney;
                break;
            case 'API':
                body.commission.api = {};
                body.commission.api.parentGroup = currentAgent.parentId;
                body.commission.api.group = currentAgent._id;
                body.commission.api.parent = currentAgentShareSettingParent;
                body.commission.api.own = currentAgentShareSettingOwn;
                body.commission.api.remaining = currentAgentShareSettingRemaining;
                body.commission.api.min = currentAgentShareSettingMin;
                body.commission.api.shareReceive = currentPercentReceive;
                body.commission.api.commission = agentCommission;
                body.commission.api.amount = getMoney;
                break;
            case 'SENIOR':
                body.commission.senior = {};
                body.commission.senior.parentGroup = currentAgent.parentId;
                body.commission.senior.group = currentAgent._id;
                body.commission.senior.parent = currentAgentShareSettingParent;
                body.commission.senior.own = currentAgentShareSettingOwn;
                body.commission.senior.remaining = currentAgentShareSettingRemaining;
                body.commission.senior.min = currentAgentShareSettingMin;
                body.commission.senior.shareReceive = currentPercentReceive;
                body.commission.senior.commission = agentCommission;
                body.commission.senior.amount = getMoney;
                break;
            case 'MASTER_AGENT':
                body.commission.masterAgent = {};
                body.commission.masterAgent.parentGroup = currentAgent.parentId;
                body.commission.masterAgent.group = currentAgent._id;
                body.commission.masterAgent.parent = currentAgentShareSettingParent;
                body.commission.masterAgent.own = currentAgentShareSettingOwn;
                body.commission.masterAgent.remaining = currentAgentShareSettingRemaining;
                body.commission.masterAgent.min = currentAgentShareSettingMin;
                body.commission.masterAgent.shareReceive = currentPercentReceive;
                body.commission.masterAgent.commission = agentCommission;
                body.commission.masterAgent.amount = getMoney;
                break;
            case 'AGENT':
                body.commission.agent = {};
                body.commission.agent.parentGroup = currentAgent.parentId;
                body.commission.agent.group = currentAgent._id;
                body.commission.agent.parent = currentAgentShareSettingParent;
                body.commission.agent.own = currentAgentShareSettingOwn;
                body.commission.agent.remaining = currentAgentShareSettingRemaining;
                body.commission.agent.min = currentAgentShareSettingMin;
                body.commission.agent.shareReceive = currentPercentReceive;
                body.commission.agent.commission = agentCommission;
                body.commission.agent.amount = getMoney;
                break;
        }

        prepareCommission(memberInfo, body, currentAgent.parentId, currentAgent, totalRemaining, overAllShare);
    }
}

function convertPercent(percent) {
    return percent / 100
}

function convertOdd(priceType, odd, oddType) {

    if (priceType === 'MY') {
        return odd;
    } else if (priceType === 'HK') {
        if (!odd.includes('-')) {
            return odd;
        }
        odd = odd.replace('-', '');
        odd = parseFloat(odd) * 100;

        let tmp = 100 / odd;
        return tmp.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
    } else if (priceType === 'EU') {
        if (!odd.includes('-')) {
            if (oddType === 'X12' || oddType === 'X121ST') {
                return odd;
            }
            let t = parseFloat(odd) + 1.00;
            if (!t.toString().includes('.')) {
                t = t.toString() + '.00';
                return t;
            } else {

                if (t.toString().length == 3) {
                    t = t.toString() + '0';
                    return t;
                } else {
                    return t.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
                }

            }
        }
        odd = odd.replace('-', '');
        odd = parseFloat(odd) * 100;
        let tmp = (100 / odd) + 1.00;
        return tmp.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];

    } else if (priceType === 'ID') {
        if (oddType === 'X12' || oddType === 'X121ST') {
            return odd;
        }


        let tmp = parseFloat(odd) * -1;
        tmp = 1 / tmp;

        if (!tmp.toString().includes('.')) {
            tmp = tmp.toString() + '.00';
            return tmp;
        } else {

            if (tmp.toString().length == 3) {
                tmp = tmp.toString() + '0';
                return tmp;
            } else {
                return tmp.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
            }

        }
    }
}

const convertMinMaxByCurrency = (value, currency, membercurrency) => {
    // console.log('###########################');
    // console.log('[BF]value      : ', value);
    // console.log('membercurrency : ', membercurrency);

    let minMax = value;
    if (_.isUndefined(membercurrency) || _.isNull(membercurrency)) {
        return minMax;
    } else {
        const predicate = JSON.parse(`{"currencyName":"${membercurrency}"}`);
        const obj = _.chain(currency)
            .findWhere(predicate)
            .pick('currencyTHB')
            .value();
        if (!_.isUndefined(obj.currencyTHB)) {
            // console.log('               : ',obj.currencyTHB);

            if (_.isEqual(membercurrency, "IDR") || _.isEqual(membercurrency, "VND")) {
                minMax = Math.ceil(value * obj.currencyTHB);
            } else {
                minMax = Math.ceil(value / obj.currencyTHB);
            }
            // console.log('[AF]value      : ', minMax);
            // console.log('###########################');
            return minMax;
        } else {
            return minMax;
        }
    }

};


module.exports = router;