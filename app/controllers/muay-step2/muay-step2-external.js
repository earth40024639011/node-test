const MemberModel = require('../../models/member.model');
const BetTransactionModel = require('../../models/betTransaction.model.js');
const UserModel = require('../../models/users.model.js');
const AgentService = require('../../common/agentService');
const MemberService = require('../../common/memberService');
const ApiService = require('../../common/apiService');
const express = require('express')
const router = express.Router()
const async = require("async");
const Joi = require('joi');
const mongoose = require('mongoose');
const Hashing = require('../../common/hashing');
const config = require('config');
const _ = require('underscore');
const DateUtils = require('../../common/dateUtils');
const crypto = require('crypto');

const PRIVATE_KEY = 'Muaystep2';


function getHash(val) {
    return crypto.createHash('md5').update(val).digest("hex");
}


const cancelTicketSchema = Joi.object().keys({
    access: Joi.string().required(),
    ticketId: Joi.string().required(),
    remark: Joi.string().required()
});

router.post('/cancelTicket', function (req, res) {

    let validate = Joi.validate(req.body, cancelTicketSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    let myAccess = getHash(`external/cancelTicket/${req.body.ticketId}/${PRIVATE_KEY}`);

    console.log('-----------------------------', myAccess)
    console.log('-----------------------------', req.body.access)

    if(myAccess != req.body.access){
        return res.send({
            message: "Signature not match",
            results: null,
            code: 5002
        });
    }

    let condition = {'ref1': req.body.ticketId, 'game': 'M2'};

    console.log('------condition------',condition);

    BetTransactionModel.findOne(condition)
        .select('memberId memberUsername hdp commission amount memberCredit payout memberParentGroup betId status priceType source betResult game')
        .populate({path: 'memberParentGroup', select: 'type'})
        .exec(function (err, betObj) {

            if (!betObj) {
                return res.send({message: "data not found", results: err, code: 5004});
            }

            if (betObj.status === 'CANCELLED' || betObj.status === 'REJECTED') {
                return res.send({message: "cancel already", results: err, code: 5001});
            }


            if (betObj.status === 'DONE'){

                async.series([subCallback => {

                    //refundBalance
                    AgentService.refundAgentMemberBalance(betObj, '', (err, response) => {
                        if (err) {

                            console.log('refundAgentMemberBalance err===', err)
                            subCallback(err, null);
                        } else {
                            console.log('refundAgentMemberBalance res===', response)
                            subCallback(null, response);
                        }
                    });
                }, callback => {

                    let updateBody = {
                        $set: {
                            'cancelDate': DateUtils.getCurrentDate(),
                            'cancelByType': 'PARTNER',
                            'cancelByAgent': 'PARTNER',
                            status: 'CANCELLED',
                            'commission.superAdmin.winLoseCom': 0,
                            'commission.superAdmin.winLose': 0,
                            'commission.superAdmin.totalWinLoseCom': 0,

                            'commission.company.winLoseCom': 0,
                            'commission.company.winLose': 0,
                            'commission.company.totalWinLoseCom': 0,

                            'commission.shareHolder.winLoseCom': 0,
                            'commission.shareHolder.winLose': 0,
                            'commission.shareHolder.totalWinLoseCom': 0,

                            'commission.senior.winLoseCom': 0,
                            'commission.senior.winLose': 0,
                            'commission.senior.totalWinLoseCom': 0,

                            'commission.masterAgent.winLoseCom': 0,
                            'commission.masterAgent.winLose': 0,
                            'commission.masterAgent.totalWinLoseCom': 0,

                            'commission.agent.winLoseCom': 0,
                            'commission.agent.winLose': 0,
                            'commission.agent.totalWinLoseCom': 0,

                            'commission.member.winLoseCom': 0,
                            'commission.member.winLose': 0,
                            'commission.member.totalWinLoseCom': 0,
                        }
                    };
                    console.log('updateBody ===', updateBody);
                    console.log('betObj ===', betObj);
                    BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, response);
                        }
                    });
                }], function (err, response) {

                    if (err) {
                        return res.send({message: "Cancel Ticket Fail", results: err, code: 5005});
                    }

                    return res.send(
                        {
                            code: 0,
                            message: "Success",
                            result: null
                        }
                    );
                });
            }else {

                let transaction = betObj;

                console.log('transaction ======= ', transaction);

                async.series([callback => {

                    MemberService.updateBalance2(transaction.memberUsername, Math.abs(transaction.memberCredit), transaction.betId, 'CANCEL_TICKET', transaction.betId, function (err, creditResponse) {

                        if (err) {
                            callback('fail', null);
                        } else {
                            callback(null, 'success');
                        }
                    });


                }], function (err, response) {

                    if (err) {
                        return res.send({message: "Cancel Ticket Fail", result: err, code: 5005});
                    }

                    let body = {
                        status: 'CANCELLED',
                        remark: req.body.remark,
                        cancelByType:'PARTNER',
                        cancelByAgent:'PARTNER',
                        cancelDate:DateUtils.getCurrentDate()
                    };

                    BetTransactionModel.update({_id: transaction._id}, body, (err, data) => {
                        if (err) {
                            return res.send({message: "Cancel Ticket Fail", result: err, code: 5005});
                        } else if (data.nModified === 0) {
                            return res.send({message: "Cancel Ticket Fail", result: null, code: 5005});
                        } else {

                            if (transaction.memberParentGroup.type === 'API') {

                                ApiService.cancelBet(transaction.memberParentGroup.endpoint, req.body.remark, transaction, (err, response) => {
                                    if (err) {
                                        return res.send({message: "Cancel Ticket Fail", result: err, code: 5005});
                                    } else {
                                        return res.send({message: "Success", result: req.body.ticketId, code: 0});
                                    }

                                });
                            } else {
                                return res.send({message: "Success",result: req.body.ticketId, code: 0});
                            }
                        }
                    });
                });
            }


        });

});

module.exports = router;