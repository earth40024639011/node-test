const express = require('express');
const router = express.Router();
const request = require('request');
const moment = require('moment');
const crypto = require('crypto');
const querystring = require('querystring');
const async = require("async");
const config = require('config');
const MemberService = require('../../common/memberService');
const Joi = require('joi');
const DateUtils = require('../../common/dateUtils');
const roundTo = require('round-to');
const mongoose = require('mongoose');
const _ = require('underscore');
const AgentGroupModel = require('../../models/agentGroup.model.js');
const NumberUtils = require('../../common/numberUtils1');
const AgentService = require('../../common/agentService');
const SaGamingService = require('../../common/sagameService');
const MemberModel = require("../../models/member.model");

const xml2js = require('xml2js');

const SaTransactionModel = require('../../models/saTransaction.model.js');
const BetTransactionModel = require('../../models/betTransaction.model.js');


const DRAW = "DRAW";
const WIN = "WIN";
const HALF_WIN = "HALF_WIN";
const LOSE = "LOSE";
const HALF_LOSE = "HALF_LOSE";


function random(low, high) {
    return Math.floor(Math.random() * (high - low + 1) + low)
}
function generateBetId() {
    return 'BET' + new Date().getTime() + random(1000000, 9999999);
}

function jsonToSAGameResponseFormat(json) {

    var builder = new xml2js.Builder();
    return builder.buildObject({
        RequestResponse: json
    });
}


//2. get_balance
router.post('/callback/GetUserBalance', (req, res) => {

    let reqQueryString = SaGamingService.decodeDesCBC(decodeURIComponent(req.body))
    let requestBody = querystring.parse(reqQueryString);


    MemberService.getCreditSecondaryModeByName(requestBody.username, (err, credit) => {

        if (err) {
            if (err == 888) {
                return res.send(jsonToSAGameResponseFormat({
                    username: requestBody.username,
                    currency: requestBody.currency,
                    amount: 0,
                    error: 1000
                }));
            }
            return res.send(jsonToSAGameResponseFormat({
                username: requestBody.username,
                currency: requestBody.currency,
                amount: 0,
                error: 1000
            }));
        }

        return res.send(jsonToSAGameResponseFormat({
            username: requestBody.username,
            currency: requestBody.currency,
            amount: credit,
            error: 0
        }));

    });


});


router.post('/callback/PlaceBet', (req, res) => {

    let reqQueryString = SaGamingService.decodeDesCBC(decodeURIComponent(req.body))
    const requestBody = querystring.parse(reqQueryString);
    // const requestBody = req.body;


    saveSaTransactionLog(requestBody, 'PLACE_BET', (err, saveAllBetTransResponse) => {

    });

    let betAmount = requestBody.amount;


    let createdDate = DateUtils.getCurrentDate();
    let findUserTime;
    let findBetTime;

    async.series([callback => {

        MemberService.findByUserNameForPartnerService(requestBody.username, function (err, memberResponse) {
            if (err) {
                callback(err, null);
                return;
            }
            findUserTime = DateUtils.getCurrentDate();

            if (memberResponse) {

                let credit = roundTo((memberResponse.creditLimit + memberResponse.balance), 2);
                if (credit < betAmount) {
                    callback(1003, credit);
                } else {
                    callback(null, memberResponse);
                }
            } else {
                callback(1004, null);
            }
        });

    }], (err, asyncResponse) => {

        if (err) {
            if (err === 1004) {

                return res.send(jsonToSAGameResponseFormat({
                    username: requestBody.username,
                    currency: requestBody.currency,
                    amount: 0,
                    error: 1000
                }));
            }
            if (err === 1003) {
                return res.send(jsonToSAGameResponseFormat({
                    username: requestBody.username,
                    currency: requestBody.currency,
                    amount: asyncResponse[0],
                    error: 1004
                }));
            }
            return res.send(jsonToSAGameResponseFormat({
                username: requestBody.username,
                currency: requestBody.currency,
                amount: 0,
                error: 9999
            }));
        }

        let memberInfo = asyncResponse[0];


        let condition = {
            'gameDate': {$gte: new Date(moment().add(-5, 'minute').format('YYYY-MM-DDTHH:mm:ss.000'))},
            'memberId': mongoose.Types.ObjectId(memberInfo._id),
            'game': 'CASINO',
            'source': 'SA_GAME',
            'baccarat.sa.gameId': requestBody.gameid
        };


        BetTransactionModel.findOne(condition, (err, response) => {
            findBetTime = DateUtils.getCurrentDate();

            if (response) {

                let sameTxId = _.filter(response.baccarat.sa.txns, item => {
                    return item.txnId === requestBody.txnid;
                });

                if (sameTxId.length > 0) {
                    return res.send(jsonToSAGameResponseFormat({
                        username: requestBody.username,
                        currency: requestBody.currency,
                        amount: 0,
                        error: 1005
                    }));
                }


                let update = {
                    $inc: {
                        'amount': betAmount,
                        "memberCredit": (betAmount * -1),
                        "payout": betAmount
                    },
                    $push: {
                        'baccarat.sa.txns': {
                            txnId: requestBody.txnid,
                            betAmount: betAmount,
                        }
                    }
                };

                MemberService.updateBalance(memberInfo._id, (requestBody.amount * -1), response.betId, 'SA_BET', requestBody.txnid, function (err, updateBalanceResponse) {
                    if (err) {
                        if (err == 888) {
                            return res.send(jsonToSAGameResponseFormat({
                                username: requestBody.username,
                                currency: requestBody.currency,
                                amount: 0,
                                error: 1005
                            }));
                        }
                        return res.send(jsonToSAGameResponseFormat({
                            username: requestBody.username,
                            currency: requestBody.currency,
                            amount: 0,
                            error: 9999
                        }));
                    } else {

                        BetTransactionModel.update({_id: response._id}, update, (err, updateTransResponse) => {
                            if (err) {
                                return res.send(jsonToSAGameResponseFormat({
                                    username: requestBody.username,
                                    currency: requestBody.currency,
                                    amount: 0,
                                    error: 9999
                                }));
                            }

                            return res.send(jsonToSAGameResponseFormat({
                                username: requestBody.username,
                                currency: requestBody.currency,
                                amount: updateBalanceResponse.newBalance,
                                error: 0
                            }));
                        });
                    }
                });

            } else {

                let body = {
                    memberId: memberInfo._id,
                    memberParentGroup: memberInfo.group,
                    memberUsername: memberInfo.username_lower,
                    betId: generateBetId(),
                    baccarat: {
                        sa: {
                            gameId: requestBody.gameid,
                            gameType: SaGamingService.getGameTypeDetail(requestBody.gametype),
                            hostId: SaGamingService.getGameNameByHostId(requestBody.hostid),
                            txns: [{
                                txnId: requestBody.txnid,
                                betAmount: betAmount
                            }]
                        }
                    },
                    amount: betAmount,
                    memberCredit: betAmount * -1,
                    payout: betAmount,
                    gameType: 'TODAY',
                    acceptHigherPrice: false,
                    priceType: 'TH',
                    game: 'CASINO',
                    source: 'SA_GAME',
                    status: 'RUNNING',
                    commission: {},
                    gameDate: DateUtils.getCurrentDate(),
                    createdDate: createdDate,
                    isEndScore: false,
                    currency: memberInfo.currency,
                    ipAddress: '',
                    remark: 'start : ' + createdDate + ' , findUser : ' + findUserTime + ' , findBet : ' + findBetTime
                };

                let betTransactionModel = new BetTransactionModel(body);

                betTransactionModel.save((err, response) => {

                    // BetTransactionModel.findOneAndUpdate({},body,{upsert:true},(err, response) => {

                    // console.log('betId : '+response.betId + ' , res : ',response)


                    if (err) {
                        return res.send(jsonToSAGameResponseFormat({
                            username: requestBody.username,
                            currency: requestBody.currency,
                            amount: 0,
                            error: 9999
                        }));
                    }

                    MemberService.updateBalance(memberInfo._id, (betAmount * -1), response.betId, 'SA_BET', requestBody.txnid, function (err, updateBalanceResponse) {
                        if (err) {
                            if (err == 888) {
                                BetTransactionModel.remove({_id: response._id}, function (err, removeResult) {
                                    console.log('betId : ', response.betId);
                                    console.log(removeResult);
                                });
                                return res.send(jsonToSAGameResponseFormat({
                                    username: requestBody.username,
                                    currency: requestBody.currency,
                                    amount: 0,
                                    error: 9999
                                }));
                            }

                            return res.send(jsonToSAGameResponseFormat({
                                username: requestBody.username,
                                currency: requestBody.currency,
                                amount: 0,
                                error: 9999
                            }));


                        } else {


                            console.log('return : ', jsonToSAGameResponseFormat({
                                username: requestBody.username,
                                currency: requestBody.currency,
                                amount: updateBalanceResponse.newBalance,
                                error: 0
                            }))

                            return res.send(jsonToSAGameResponseFormat({
                                username: requestBody.username,
                                currency: requestBody.currency,
                                amount: updateBalanceResponse.newBalance,
                                error: 0
                            }));


                        }
                    });
                });
            }

        });
    });


});

router.post('/callback/PlaceBetCancel', (req, res) => {


    // let reqQueryString = SaGamingService.decodeDesCBC(decodeURIComponent(req.body))
    // const requestBody = querystring.parse(reqQueryString);
    const requestBody = req.body;

    console.log('body : ', requestBody);


    saveSaTransactionLog(requestBody, 'PLACE_BET_CANCEL', (err, saveAllBetTransResponse) => {

    });


    async.series([callback => {

        MemberService.findByUserNameForPartnerService(requestBody.username, function (err, memberResponse) {
            if (err) {
                callback(err, null);
                return;
            }

            if (memberResponse) {
                callback(null, memberResponse);
            } else {
                callback(1004, null);
            }
        });

    }], (err, asyncResponse) => {

        if (err) {
            if (err === 1004) {
                return res.send(jsonToSAGameResponseFormat({
                    username: requestBody.username,
                    currency: requestBody.currency,
                    amount: 0,
                    error: 1000
                }));
            }
            return res.send(jsonToSAGameResponseFormat({
                username: requestBody.username,
                currency: requestBody.currency,
                amount: 0,
                error: 9999
            }));
        }

        let memberInfo = asyncResponse[0];

        let betAmount = Number.parseFloat(requestBody.amount);

        let condition = {
            'memberId': mongoose.Types.ObjectId(memberInfo._id),
            'game': 'CASINO',
            'source': 'SA_GAME',
            'baccarat.sa.gameId': requestBody.gameid,
            'baccarat.sa.txns.txnId': {$in: requestBody.txn_reverse_id},
        };

        console.log(condition)

        BetTransactionModel.findOne(condition).exec(function (err, betObj) {


            if (!betObj) {
                return res.send(jsonToSAGameResponseFormat({
                    username: requestBody.username,
                    currency: requestBody.currency,
                    amount: 0,
                    error: 1005
                }));
            }

            // if (betObj.status !== 'RUNNING') {
            //     return res.send(jsonToSAGameResponseFormat({
            //         username: requestBody.username,
            //         currency: requestBody.currency,
            //         amount: 0,
            //         error: 0
            //     }));
            // }

            let cancelItems = _.filter(betObj.baccarat.sa.txns, (item) => {
                return requestBody.txn_reverse_id == item.txnId;
            });


            if (cancelItems.length == 0) {
                let credit = roundTo(memberInfo.creditLimit + memberInfo.balance, 2);

                return res.send(jsonToSAGameResponseFormat({
                    username: requestBody.username,
                    currency: requestBody.currency,
                    amount: credit,
                    error: 0
                }));
            }


            let updateCondition = {
                $pull: {'baccarat.sa.txns': {"txnId": requestBody.txn_reverse_id}},
                $inc: {
                    amount: betAmount * -1,
                    memberCredit: betAmount,
                    validAmount: betAmount * -1
                }
            };

            console.log(updateCondition)

            BetTransactionModel.update({
                _id: betObj._id,
                'baccarat.sa.txns.txnId': requestBody.txn_reverse_id
            }, updateCondition, function (err, cancelResponse) {
                if (err) {
                    return res.send(jsonToSAGameResponseFormat({
                        username: requestBody.username,
                        currency: requestBody.currency,
                        amount: 0,
                        error: 9999
                    }));
                }

                console.log('cancelResponse : ', cancelResponse)

                if (cancelResponse.nModified == 1) {

                    MemberService.updateBalance(memberInfo._id, betAmount, betObj.betId, 'SA_CANCEL_BET', requestBody.txnid, function (err, updateBalanceResponse) {

                        if (err) {

                            if (err == 888) {
                                return res.send(jsonToSAGameResponseFormat({
                                    username: requestBody.username,
                                    currency: requestBody.currency,
                                    amount: 0,
                                    error: 0
                                }));
                            }

                            return res.send(jsonToSAGameResponseFormat({
                                username: requestBody.username,
                                currency: requestBody.currency,
                                amount: 0,
                                error: 9999
                            }));


                        } else {

                            BetTransactionModel.remove({
                                _id: betObj._id,
                                "baccarat.sa.txns": {$size: 0}
                            }, function (err, removeResponse) {
                                console.log("remove res : ", removeResponse)
                                console.log(err)
                            });


                            console.log("====== : ",betObj.status)
                            if (betObj.status == 'DONE') {

                                try {

                                    let winLose = 0;

                                    console.log('old result : ', betObj.betResult)
                                    console.log('old wl : ', betObj.commission.member.winLose)
                                    console.log('cancel amt : ', betAmount);
                                    if (betObj.betResult == WIN) {
                                        winLose = betObj.commission.member.winLose + betAmount;
                                    } else if (betObj.betResult == LOSE) {
                                        winLose = betObj.commission.member.winLose + betAmount;
                                    } else if (betObj.betResult == DRAW) {
                                        winLose = betObj.commission.member.winLose + betAmount;
                                    }

                                    console.log('new wl : ', winLose)
                                    let betResult = winLose > 0 ? WIN : winLose < 0 ? LOSE : DRAW;

                                    const shareReceive = AgentService.prepareShareReceive(winLose, betResult, betObj);


                                    let updateBody = {
                                        'commission.superAdmin.winLoseCom': shareReceive.superAdmin.winLoseCom,
                                        'commission.superAdmin.winLose': shareReceive.superAdmin.winLose,
                                        'commission.superAdmin.totalWinLoseCom': shareReceive.superAdmin.totalWinLoseCom,

                                        'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                                        'commission.company.winLose': shareReceive.company.winLose,
                                        'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                                        'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                                        'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                                        'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                                        'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                                        'commission.senior.winLose': shareReceive.senior.winLose,
                                        'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                                        'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                                        'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                                        'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                                        'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                                        'commission.agent.winLose': shareReceive.agent.winLose,
                                        'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                                        'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                                        'commission.member.winLose': shareReceive.member.winLose,
                                        'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                                        'betResult': betResult

                                    };


                                    BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, cancelResponse) {

                                        console.log(cancelResponse)

                                        return res.send(jsonToSAGameResponseFormat({
                                            username: requestBody.username,
                                            currency: requestBody.currency,
                                            amount: updateBalanceResponse.newBalance,
                                            error: 0
                                        }));

                                    });

                                } catch (err) {

                                    console.log(err)
                                    return res.send(jsonToSAGameResponseFormat({
                                        username: requestBody.username,
                                        currency: requestBody.currency,
                                        amount: updateBalanceResponse.newBalance,
                                        error: 0
                                    }));
                                }

                            } else {

                                return res.send(jsonToSAGameResponseFormat({
                                    username: requestBody.username,
                                    currency: requestBody.currency,
                                    amount: updateBalanceResponse.newBalance,
                                    error: 0
                                }));
                            }
                        }
                    });
                } else {
                    return res.send(jsonToSAGameResponseFormat({
                        username: requestBody.username,
                        currency: requestBody.currency,
                        amount: 0,
                        error: 1005
                    }));
                }
            });


        });
    });
});

router.post('/callback/PlayerWin', (req, res) => {


    // let reqQueryString = SaGamingService.decodeDesCBC(decodeURIComponent(req.body))
    // const requestBody = querystring.parse(reqQueryString);
    const requestBody = req.body;


    console.log('body : ', requestBody);


    saveSaTransactionLog(requestBody, 'PLAYER_WIN', (err, saveAllBetTransResponse) => {

    });


    async.series([callback => {

        MemberService.findByUserNameForPartnerService(requestBody.username, function (err, memberResponse) {
            if (err) {
                callback(err, null);
                return;
            }
            if (memberResponse) {
                callback(null, memberResponse);
            } else {
                callback(1004, null);
            }
        });

    }], (err, asyncResponse) => {

        if (err) {
            if (err === 1004) {
                return res.send(jsonToSAGameResponseFormat({
                    username: requestBody.username,
                    currency: requestBody.currency,
                    amount: 0,
                    error: 1000
                }));
            }
            return res.send(jsonToSAGameResponseFormat({
                username: requestBody.username,
                currency: requestBody.currency,
                amount: 0,
                error: 9999
            }));
        }

        let memberInfo = asyncResponse[0];


        let condition = {
            'memberId': mongoose.Types.ObjectId(memberInfo._id),
            'game': 'CASINO',
            'source': 'SA_GAME',
            'baccarat.sa.gameId': requestBody.gameid
        };


        BetTransactionModel.findOne(condition)
            .populate({path: 'memberParentGroup', select: 'type'})
            .exec(function (err, betObj) {


                if (!betObj) {
                    return res.send(jsonToSAGameResponseFormat({
                        username: requestBody.username,
                        currency: requestBody.currency,
                        amount: 0,
                        error: 1005
                    }));
                }

                if (betObj.status === 'DONE') {

                    MemberService.getCredit(memberInfo._id, (err, credit) => {

                        return res.send(jsonToSAGameResponseFormat({
                            username: requestBody.username,
                            currency: requestBody.currency,
                            amount: credit,
                            error: 0
                        }));
                    });

                } else {

                    AgentGroupModel.findById(memberInfo.group).select('_id type parentId shareSetting commissionSetting').populate({
                        path: 'parentId',
                        select: '_id type parentId shareSetting commissionSetting name',
                        populate: {
                            path: 'parentId',
                            select: '_id type parentId shareSetting commissionSetting name',
                            populate: {
                                path: 'parentId',
                                select: '_id type parentId shareSetting commissionSetting name',
                                populate: {
                                    path: 'parentId',
                                    select: '_id type parentId shareSetting commissionSetting name',
                                    populate: {
                                        path: 'parentId',
                                        select: '_id type parentId shareSetting commissionSetting name',
                                        populate: {
                                            path: 'parentId',
                                            select: '_id type parentId shareSetting commissionSetting name',
                                        }
                                    }
                                }
                            }
                        }

                    }).exec(function (err, agentGroups) {

                        let betAmount = betObj.amount;
                        let resultCash = roundTo(Number.parseFloat(requestBody.amount), 2);
                        let winLose = resultCash - betAmount;
                        let betResult = winLose > 0 ? WIN : winLose < 0 ? LOSE : DRAW;

                        let body = betObj;

                        prepareCommission(memberInfo, body, agentGroups, null, 0, 0);

                        //init
                        if (!body.commission.senior) {
                            body.commission.senior = {};
                        }

                        if (!body.commission.masterAgent) {
                            body.commission.masterAgent = {};
                        }

                        if (!body.commission.agent) {
                            body.commission.agent = {};
                        }

                        const shareReceive = AgentService.prepareShareReceive(winLose, betResult, betObj);

                        body.commission.superAdmin.winLoseCom = shareReceive.superAdmin.winLoseCom;
                        body.commission.superAdmin.winLose = shareReceive.superAdmin.winLose;
                        body.commission.superAdmin.totalWinLoseCom = shareReceive.superAdmin.totalWinLoseCom;

                        body.commission.company.winLoseCom = shareReceive.company.winLoseCom;
                        body.commission.company.winLose = shareReceive.company.winLose;
                        body.commission.company.totalWinLoseCom = shareReceive.company.totalWinLoseCom;

                        body.commission.shareHolder.winLoseCom = shareReceive.shareHolder.winLoseCom;
                        body.commission.shareHolder.winLose = shareReceive.shareHolder.winLose;
                        body.commission.shareHolder.totalWinLoseCom = shareReceive.shareHolder.totalWinLoseCom;

                        body.commission.senior.winLoseCom = shareReceive.senior.winLoseCom;
                        body.commission.senior.winLose = shareReceive.senior.winLose;
                        body.commission.senior.totalWinLoseCom = shareReceive.senior.totalWinLoseCom;

                        body.commission.masterAgent.winLoseCom = shareReceive.masterAgent.winLoseCom;
                        body.commission.masterAgent.winLose = shareReceive.masterAgent.winLose;
                        body.commission.masterAgent.totalWinLoseCom = shareReceive.masterAgent.totalWinLoseCom;

                        body.commission.agent.winLoseCom = shareReceive.agent.winLoseCom;
                        body.commission.agent.winLose = shareReceive.agent.winLose;
                        body.commission.agent.totalWinLoseCom = shareReceive.agent.totalWinLoseCom;

                        body.commission.member.winLoseCom = shareReceive.member.winLoseCom;
                        body.commission.member.winLose = shareReceive.member.winLose;
                        body.commission.member.totalWinLoseCom = shareReceive.member.totalWinLoseCom;
                        body.validAmount = betResult === DRAW ? 0 : body.amount;

                        updateAgentMemberBalance(shareReceive, betAmount, requestBody.txnid, function (err, updateBalanceResponse) {
                            if (err) {

                                if (err == 888) {
                                    return res.send(jsonToSAGameResponseFormat({
                                        username: requestBody.username,
                                        currency: requestBody.currency,
                                        amount: 0,
                                        error: 0
                                    }));
                                }

                                return res.send(jsonToSAGameResponseFormat({
                                    username: requestBody.username,
                                    currency: requestBody.currency,
                                    amount: 0,
                                    error: 9999
                                }));

                            } else {

                                let newBalance = updateBalanceResponse.newBalance;

                                let updateBody = {
                                    $set: {
                                        'baccarat.sa.winLose': winLose,
                                        'commission': body.commission,
                                        'validAmount': body.validAmount,
                                        'betResult': betResult,
                                        'isEndScore': true,
                                        'status': 'DONE',
                                        'settleDate': DateUtils.getCurrentDate()
                                    }
                                };


                                BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                                    if (err) {
                                        return res.send(jsonToSAGameResponseFormat({
                                            username: requestBody.username,
                                            currency: requestBody.currency,
                                            amount: 0,
                                            error: 9999
                                        }));
                                    } else {
                                        return res.send(jsonToSAGameResponseFormat({
                                            username: requestBody.username,
                                            currency: requestBody.currency,
                                            amount: newBalance,
                                            error: 0
                                        }));
                                    }
                                });
                            }
                        });
                    });
                }

            });
    });


});

router.post('/callback/PlayerLost', (req, res) => {

    // let reqQueryString = SaGamingService.decodeDesCBC(decodeURIComponent(req.body));
    // const requestBody = querystring.parse(reqQueryString);
    const requestBody = req.body;

    // console.log('body : ', requestBody)

    saveSaTransactionLog(requestBody, 'PLAYER_LOST', (err, saveAllBetTransResponse) => {

    });

    async.series([callback => {

        MemberService.findByUserNameForPartnerService(requestBody.username, function (err, memberResponse) {
            if (err) {
                callback(err, null);
                return;
            }
            if (memberResponse) {
                callback(null, memberResponse);
            } else {
                callback(1004, null);
            }
        });

    }], (err, asyncResponse) => {


        if (err) {
            if (err === 1004) {
                return res.send(jsonToSAGameResponseFormat({
                    username: requestBody.username,
                    currency: requestBody.currency,
                    amount: 0,
                    error: 1000
                }));
            }
            return res.send(jsonToSAGameResponseFormat({
                username: requestBody.username,
                currency: requestBody.currency,
                amount: 0,
                error: 9999
            }));
        }

        let memberInfo = asyncResponse[0];


        let condition = {
            'memberId': mongoose.Types.ObjectId(memberInfo._id),
            'game': 'CASINO',
            'source': 'SA_GAME',
            'baccarat.sa.gameId': requestBody.gameid
        };

        BetTransactionModel.findOne(condition)
            .populate({path: 'memberParentGroup', select: 'type'})
            .exec(function (err, betObj) {


                if (!betObj) {
                    return res.send(jsonToSAGameResponseFormat({
                        username: requestBody.username,
                        currency: requestBody.currency,
                        amount: 0,
                        error: 1005
                    }));
                }

                if (betObj.status === 'DONE') {

                    MemberService.getCredit(memberInfo._id, (err, credit) => {

                        return res.send(jsonToSAGameResponseFormat({
                            username: requestBody.username,
                            currency: requestBody.currency,
                            amount: credit,
                            error: 0
                        }));
                    })

                } else {

                    AgentGroupModel.findById(memberInfo.group).select('_id type parentId shareSetting commissionSetting').populate({
                        path: 'parentId',
                        select: '_id type parentId shareSetting commissionSetting name',
                        populate: {
                            path: 'parentId',
                            select: '_id type parentId shareSetting commissionSetting name',
                            populate: {
                                path: 'parentId',
                                select: '_id type parentId shareSetting commissionSetting name',
                                populate: {
                                    path: 'parentId',
                                    select: '_id type parentId shareSetting commissionSetting name',
                                    populate: {
                                        path: 'parentId',
                                        select: '_id type parentId shareSetting commissionSetting name',
                                        populate: {
                                            path: 'parentId',
                                            select: '_id type parentId shareSetting commissionSetting name',
                                        }
                                    }
                                }
                            }
                        }

                    }).exec(function (err, agentGroups) {


                        let betAmount = betObj.amount;
                        let winLose = roundTo(Number.parseFloat(betObj.memberCredit), 2);
                        let betResult = LOSE;

                        let body = betObj;

                        prepareCommission(memberInfo, body, agentGroups, null, 0, 0);

                        //init
                        if (!body.commission.senior) {
                            body.commission.senior = {};
                        }

                        if (!body.commission.masterAgent) {
                            body.commission.masterAgent = {};
                        }

                        if (!body.commission.agent) {
                            body.commission.agent = {};
                        }

                        const shareReceive = AgentService.prepareShareReceive(winLose, betResult, betObj);

                        body.commission.superAdmin.winLoseCom = shareReceive.superAdmin.winLoseCom;
                        body.commission.superAdmin.winLose = shareReceive.superAdmin.winLose;
                        body.commission.superAdmin.totalWinLoseCom = shareReceive.superAdmin.totalWinLoseCom;

                        body.commission.company.winLoseCom = shareReceive.company.winLoseCom;
                        body.commission.company.winLose = shareReceive.company.winLose;
                        body.commission.company.totalWinLoseCom = shareReceive.company.totalWinLoseCom;

                        body.commission.shareHolder.winLoseCom = shareReceive.shareHolder.winLoseCom;
                        body.commission.shareHolder.winLose = shareReceive.shareHolder.winLose;
                        body.commission.shareHolder.totalWinLoseCom = shareReceive.shareHolder.totalWinLoseCom;

                        body.commission.senior.winLoseCom = shareReceive.senior.winLoseCom;
                        body.commission.senior.winLose = shareReceive.senior.winLose;
                        body.commission.senior.totalWinLoseCom = shareReceive.senior.totalWinLoseCom;

                        body.commission.masterAgent.winLoseCom = shareReceive.masterAgent.winLoseCom;
                        body.commission.masterAgent.winLose = shareReceive.masterAgent.winLose;
                        body.commission.masterAgent.totalWinLoseCom = shareReceive.masterAgent.totalWinLoseCom;

                        body.commission.agent.winLoseCom = shareReceive.agent.winLoseCom;
                        body.commission.agent.winLose = shareReceive.agent.winLose;
                        body.commission.agent.totalWinLoseCom = shareReceive.agent.totalWinLoseCom;

                        body.commission.member.winLoseCom = shareReceive.member.winLoseCom;
                        body.commission.member.winLose = shareReceive.member.winLose;
                        body.commission.member.totalWinLoseCom = shareReceive.member.totalWinLoseCom;

                        updateAgentMemberBalance(shareReceive, betAmount, requestBody.txnid, function (err, updateBalanceResponse) {
                            if (err) {
                                if (err == 888) {
                                    return res.send(jsonToSAGameResponseFormat({
                                        username: requestBody.username,
                                        currency: requestBody.currency,
                                        amount: 0,
                                        error: 0
                                    }));
                                }
                                return res.send(jsonToSAGameResponseFormat({
                                    username: requestBody.username,
                                    currency: requestBody.currency,
                                    amount: 0,
                                    error: 9999
                                }));
                            } else {

                                let newBalance = updateBalanceResponse.newBalance;

                                let updateBody = {
                                    $set: {
                                        'baccarat.sa.payoutTime': requestBody.PayoutTime,
                                        'commission': body.commission,
                                        'betResult': betResult,
                                        'isEndScore': true,
                                        'status': 'DONE',
                                        'validAmount': betAmount,
                                        'settleDate': DateUtils.getCurrentDate()
                                    }
                                };


                                BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                                    if (err) {
                                        return res.send(jsonToSAGameResponseFormat({
                                            username: requestBody.username,
                                            currency: requestBody.currency,
                                            amount: 0,
                                            error: 9999
                                        }));
                                    } else {
                                        return res.send(jsonToSAGameResponseFormat({
                                            username: requestBody.username,
                                            currency: requestBody.currency,
                                            amount: newBalance,
                                            error: 0
                                        }));
                                    }
                                });
                            }
                        });
                    });
                }

            });

    });

});

//2.1. token_forward_game
router.get('/queryBetLimit', (req, res) => {


    SaGamingService.callQueryBetLimit((err, loginResponse) => {
        // SaGamingService.callLoginRequest(userInfo.username_lower, (err, loginResponse) => {

        if (err) {
            return res.send({message: 'fail', code: 999});
        } else {
            console.log(loginResponse)
            return res.send({message: 'success', code: 0, result: loginResponse});
        }
    });


});

router.get('/getUserMaxBalance', (req, res) => {

    SaGamingService.callGetUserMaxBalance('show2', (err, loginResponse) => {
        // SaGamingService.callLoginRequest(userInfo.username_lower, (err, loginResponse) => {

        if (err) {
            return res.send({message: 'fail', code: 999});
        } else {
            console.log(loginResponse)
            return res.send({message: 'success', code: 0, result: loginResponse});
        }
    });


});

router.get('/getBetDetail2', (req, res) => {
    //
    let dateInfo = req.query.Date;
    let tranId = req.query.tranId;
        // SaGamingService.getBetDetail('show2', (err, loginResponse) => {
            SaGamingService.getBetDetailForTransactionID(dateInfo, tranId,(err, loginResponse) => {

         // let betDetail = loginResponse.betDetail.filter(loginResponse.betDetail.gameId);
        if (err)
            return res.send({message: 'fail', code: 999});


            return res.send({message: 'success',
                code: 0,
                result: loginResponse
            });

    });


});

router.get('/getBetDetail', (req, res) => {
    //
    let userInfo = req.query.Username;
    let dateInfo = req.query.Date;
    let betID = req.query.betID;
    // SaGamingService.getBetDetail('show2', (err, loginResponse) => {
    SaGamingService.getBetDetail(userInfo,dateInfo, betID,(err, loginResponse) => {

        // let betDetail = loginResponse.betDetail.filter(loginResponse.betDetail.gameId);
        if (err) {
            return res.send({message: 'fail', code: 999});


        } else if(loginResponse.betDetail.gameType === '') {
            console.log('กดเกิน')
        }
        else {
            console.log(loginResponse);
            return res.send({message: 'success',
                code: 0,
                result: loginResponse
            });
        }
    });


});

router.get('/getBetResult', (req, res) => {
    //
    let userInfo = req.query.Username;
    let betID = req.query.betID;
    // let dateto = req.query.dateto;
    let datefrom = req.query.Date;
    console.log(userInfo,'dateto',datefrom,'datefrom');

    SaGamingService.getBetResult(userInfo,betID,datefrom,(err, loginResponse,loginResponse2) => {
        if (err) {
            return res.send({message: 'fail', code: 999});
        } else {
            console.log(loginResponse,'oioioiou');
            return res.send({message: 'success',
                code: 0,
                result: {
                    detail:loginResponse,
                    results:loginResponse2


                }

            });
        }
    });


});

router.get('/patch', (req, res) => {
    //
    let userInfo = req.query.username;
    let dateInfo = req.query.date;
    let betID = req.query.betID;
    // SaGamingService.getBetDetail('show2', (err, loginResponse) => {


    let condition = {
        gameDate: {
            "$gte": new Date(moment().utc(true).format('2019-12-01T00:30:00.000')),
            "$lt": new Date(moment().utc().format('2019-12-12T00:00:00.000')),
        },
        status: 'RUNNING', game: 'CASINO', source: 'SA_GAME'
    };

    console.log(condition);

    BetTransactionModel.find(condition).exec(function (err, list) {


            async.each(list, (response, callbackWL) => {


                let bb = Math.abs(response.memberCredit);//req.body.amount;
                // let bb = req.body.amount;

                MemberService.updateBalance(response.memberId, bb, req.body.betId, 'ROLLBACK_BALANCE' + new Date(), '', function (err, updateMemberResponse) {

                    BetTransactionModel.deleteOne({ _id: response._id }, function (err,r) {
                        //     console.log(r)
                        callbackWL(null,'');
                    })
                });

                // SaGamingService.getBetDetail2(betObj.memberUsername, dateInfo, betID, (err, loginResponse) => {
                //
                //     // let betDetail = loginResponse.betDetail.filter(loginResponse.betDetail.gameId);
                //     if (err) {
                //         return res.send({message: 'fail', code: 999});
                //
                //
                //     }
                //     console.log(loginResponse);
                //     return res.send({
                //         message: 'success',
                //         code: 0,
                //         result: loginResponse
                //     });
                //
                // });

            }, (err) => {

                return res.send(
                    {
                        code: 0,
                        message: "success",
                        result: 'success'
                    }
                );
            });

        });
});

// router.get('/getBetResult', (req, res) => {
//     //
//     let userInfo = req.query.Username;
//     let betID = req.query.betID;
//     // let dateto = req.query.dateto;
//     let datefrom = req.query.datefrom;
//     console.log(userInfo, 'dateto', datefrom, 'datefrom');
//
//
//     // SaGamingService.getBetDetail('show2', (err, loginResponse) => {
//     SaGamingService.getBetResult(userInfo, betID, datefrom, (err, loginResponse) => {
//         // let betDetail = loginResponse.betDetail.filter(loginResponse.betDetail.gameId);
//         if (err) {
//             return res.send({message: 'fail', code: 999});
//
//
//         } else {
//             console.log(loginResponse, 'oioioiou');
//             return res.send({
//                 message: 'success',
//                 code: 0,
//                 result: loginResponse
//             });
//         }
//     });
//
//
// });

router.post('/setUserMaxBalance', (req, res) => {


    SaGamingService.callSetUserMaxBalance(req.body.username, (err, loginResponse) => {
        // SaGamingService.callLoginRequest(userInfo.username_lower, (err, loginResponse) => {

        if (err) {
            return res.send({message: 'fail', code: 999});
        } else {
            console.log(loginResponse)
            return res.send({message: 'success', code: 0, result: loginResponse});
        }
    });


});

//2.1. token_forward_game
router.get('/login', (req, res) => {


    console.log(req.query);

    let userInfo = req.userInfo;


    async.waterfall([callback => {
        try {
            AgentService.getUpLineStatus(userInfo.group._id, function (err, status) {
                if (err) {
                    callback(err, null);
                } else {
                    if (status.suspend) {
                        callback(5011, null);
                    } else {
                        callback(null, status);
                    }
                }
            });
        } catch (err) {
            callback(9929, null);
        }

    }, (uplineStatus, callback) => {

        try {
            MemberService.findById(userInfo._id, 'creditLimit balance limitSetting group suspend username_lower', function (err, memberResponse) {
                if (err) {
                    callback(err, null);
                } else {

                    if (memberResponse.suspend) {
                        callback(5011, null);
                    } else if (memberResponse.limitSetting.casino.sa.isEnable) {
                        AgentService.getSaGameStatus(memberResponse.group._id, (err, response) => {
                            if (!response.isEnable) {
                                callback(1088, null);
                            } else {
                                callback(null, uplineStatus, memberResponse);
                            }
                        });
                    } else {
                        callback(1088, null);
                    }
                }
            });
        } catch (err) {
            callback(999, null);
        }

    }], function (err, uplineStatus, memberInfo) {

        if (err) {
            if (err == 1088) {
                return res.send({
                    code: 1088,
                    message: 'Service Locked, Please contact your upline.'
                });
            }
            if (err == 5011) {
                return res.send({
                    message: "Account or Upline was suspend.",
                    result: null,
                    code: 5011
                });
            }
            return res.send({code: 999, message: err});
        }


        let client = req.query.client;

        //701 - 710
        SaGamingService.callLoginRequest(memberInfo.username_lower, memberInfo.limitSetting.casino.sa.limit, (err, loginResponse) => {
            console.log('SaGamingService.callLoginRequest()', loginResponse);
            if (err) {
                return res.send({message: 'fail', code: 999});
            } else {
                //bar 601 , sicbo 631 , dragon 640
                if (loginResponse.errMsgId == '0') {
                    const url = SaGamingService.callLaunchingLiveGameURL(loginResponse.displayName,/*req.query.gameId*/'', client, loginResponse.token);
                    return res.send({message: 'success', code: 0, url: url});
                } else {
                    return res.send({message: loginResponse.errMsg, code: 999});
                }


            }
        });

    });

});

function prepareCommission(memberInfo, body, currentAgent, overAgent, remaining, overAllShare) {


    if (currentAgent && (currentAgent.type === 'SUPER_ADMIN' || currentAgent.parentId)) {


        console.log('==============================================');
        console.log('process : ' + currentAgent.type + ' : ' + currentAgent.name);


        let money = body.amount;


        if (!overAgent) {
            overAgent = {};
            overAgent.type = 'member';
            overAgent.shareSetting = {};
            overAgent.shareSetting.casino = {};
            overAgent.shareSetting.casino.sa = {};
            overAgent.shareSetting.casino.sa.parent = memberInfo.shareSetting.casino.sa.parent;
            overAgent.shareSetting.casino.sa.own = 0;
            overAgent.shareSetting.casino.sa.remaining = 0;

            body.commission.member = {};
            body.commission.member.parent = memberInfo.shareSetting.casino.sa.parent;
            body.commission.member.commission = memberInfo.commissionSetting.casino.sa;

        }

        console.log('');
        console.log('---- setting ----');
        console.log('ส่วนแบ่งที่ได้มามีทั้งหมด : ' + currentAgent.shareSetting.casino.sa.own + ' %');
        console.log('ขอส่วนแบ่งจาก : ' + overAgent.type + ' ' + overAgent.shareSetting.casino.sa.parent + ' %');
        console.log('ให้ส่วนแบ่ง : ' + overAgent.type + ' ที่เหลือไป ' + overAgent.shareSetting.casino.sa.own + ' %');
        console.log('remaining จาก : ' + overAgent.type + ' : ' + overAgent.shareSetting.casino.sa.remaining + ' %');
        console.log('โดนบังคับเสียขั้นต่ำ : ' + currentAgent.shareSetting.casino.sa.min + ' %');
        console.log('---- end setting ----');
        console.log('');


        // console.log('และได้รับ remaining ไว้ : '+overAgent.shareSetting.casino.sa.remaining+' %');

        let currentPercentReceive = overAgent.shareSetting.casino.sa.parent;
        console.log('% ที่ได้ : ' + currentPercentReceive);
        console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');
        let totalRemaining = remaining;

        if (overAgent.shareSetting.casino.sa.remaining > 0) {
            // console.log()
            // console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');

            if (overAgent.shareSetting.casino.sa.remaining > totalRemaining) {
                console.log('รับ remaining ทั้งหมดไว้: ' + totalRemaining + ' %');
                currentPercentReceive += totalRemaining;
                totalRemaining = 0;
            } else {
                totalRemaining = remaining - overAgent.shareSetting.casino.sa.remaining;
                console.log('หักค่า remaining ที่ได้รับมากับที่เซตรับไว้ = ' + remaining + ' - ' + overAgent.shareSetting.casino.sa.remaining + ' = ' + totalRemaining);
                currentPercentReceive += overAgent.shareSetting.casino.sa.remaining;
            }

        }

        console.log('รวม % ที่ได้รับ : ' + currentPercentReceive);


        let unUseShare = currentAgent.shareSetting.casino.sa.own -
            (overAgent.shareSetting.casino.sa.parent + overAgent.shareSetting.casino.sa.own);

        console.log('เหลือส่วนแบ่งที่ไม่ได้ใช้ : ' + unUseShare + ' %');
        totalRemaining = (unUseShare + totalRemaining);
        console.log('ส่วนแบ่งที่ไม่ได้ใช้ บวกกับค่า remaining ท่ี่เหลือจะเป็น : ' + totalRemaining + ' %');

        console.log('จากที่โดนบังคับเสียขั้นต่ำ : ' + currentAgent.shareSetting.casino.sa.min + ' %');
        console.log('overAllShare : ', overAllShare);
        if (currentAgent.shareSetting.casino.sa.min > 0 && ((overAllShare + currentPercentReceive) < currentAgent.shareSetting.casino.sa.min)) {

            if (currentPercentReceive < currentAgent.shareSetting.casino.sa.min) {
                console.log('% ที่ได้รับ < ขั้นต่ำที่ถูกตั้ง min ไว้');
                let percent = currentAgent.shareSetting.casino.sa.min - (currentPercentReceive + overAllShare);
                console.log('หักออกไป ' + percent + ' % : แล้วไปเพิ่มในส่วนที่ต้องรับผิดชอบ ');
                totalRemaining = totalRemaining - percent;
                if (totalRemaining < 0) {
                    totalRemaining = 0;
                }
                currentPercentReceive += percent;
            }
        } else {
            // currentPercentReceive += totalRemaining;
        }


        if (currentAgent.type === 'SUPER_ADMIN') {
            currentPercentReceive += totalRemaining;
        }

        let getMoney = (money * NumberUtils.convertPercent(currentPercentReceive)).toFixed(2);
        overAllShare = overAllShare + currentPercentReceive;

        console.log('ยอดแทง ' + money + ' ได้ทั้งหมด : ' + currentPercentReceive + ' %');

        console.log('รวม % + % remaining  จะเป็นสัดส่วนที่ได้ทั้งหมด = ' + getMoney);
        console.log('ค่าที่จะถูกคืนกลับไป (remaining) : ' + totalRemaining + ' %');

        console.log('จำนวน % ที่ถูกแบ่งไปแล้วทั้งหมด : ', overAllShare);

        //set commission
        let agentCommission = currentAgent.commissionSetting.casino.sa;


        switch (currentAgent.type) {
            case 'SUPER_ADMIN':
                body.commission.superAdmin = {};
                body.commission.superAdmin.group = currentAgent._id;
                body.commission.superAdmin.parent = currentAgent.shareSetting.casino.sa.parent;
                body.commission.superAdmin.own = currentAgent.shareSetting.casino.sa.own;
                body.commission.superAdmin.remaining = currentAgent.shareSetting.casino.sa.remaining;
                body.commission.superAdmin.min = currentAgent.shareSetting.casino.sa.min;
                body.commission.superAdmin.shareReceive = Math.abs(currentPercentReceive);
                body.commission.superAdmin.commission = agentCommission;
                body.commission.superAdmin.amount = getMoney;
                break;
            case 'COMPANY':
                body.commission.company = {};
                body.commission.company.parentGroup = currentAgent.parentId;
                body.commission.company.group = currentAgent._id;
                body.commission.company.parent = currentAgent.shareSetting.casino.sa.parent;
                body.commission.company.own = currentAgent.shareSetting.casino.sa.own;
                body.commission.company.remaining = currentAgent.shareSetting.casino.sa.remaining;
                body.commission.company.min = currentAgent.shareSetting.casino.sa.min;
                body.commission.company.shareReceive = Math.abs(currentPercentReceive);
                body.commission.company.commission = agentCommission;
                body.commission.company.amount = getMoney;
                break;
            case 'SHARE_HOLDER':
                body.commission.shareHolder = {};
                body.commission.shareHolder.parentGroup = currentAgent.parentId;
                body.commission.shareHolder.group = currentAgent._id;
                body.commission.shareHolder.parent = currentAgent.shareSetting.casino.sa.parent;
                body.commission.shareHolder.own = currentAgent.shareSetting.casino.sa.own;
                body.commission.shareHolder.remaining = currentAgent.shareSetting.casino.sa.remaining;
                body.commission.shareHolder.min = currentAgent.shareSetting.casino.sa.min;
                body.commission.shareHolder.shareReceive = currentPercentReceive;
                body.commission.shareHolder.commission = agentCommission;
                body.commission.shareHolder.amount = getMoney;
                break;
            case 'SENIOR':
                body.commission.senior = {};
                body.commission.senior.parentGroup = currentAgent.parentId;
                body.commission.senior.group = currentAgent._id;
                body.commission.senior.parent = currentAgent.shareSetting.casino.sa.parent;
                body.commission.senior.own = currentAgent.shareSetting.casino.sa.own;
                body.commission.senior.remaining = currentAgent.shareSetting.casino.sa.remaining;
                body.commission.senior.min = currentAgent.shareSetting.casino.sa.min;
                body.commission.senior.shareReceive = currentPercentReceive;
                body.commission.senior.commission = agentCommission;
                body.commission.senior.amount = getMoney;
                break;
            case 'MASTER_AGENT':
                body.commission.masterAgent = {};
                body.commission.masterAgent.parentGroup = currentAgent.parentId;
                body.commission.masterAgent.group = currentAgent._id;
                body.commission.masterAgent.parent = currentAgent.shareSetting.casino.sa.parent;
                body.commission.masterAgent.own = currentAgent.shareSetting.casino.sa.own;
                body.commission.masterAgent.remaining = currentAgent.shareSetting.casino.sa.remaining;
                body.commission.masterAgent.min = currentAgent.shareSetting.casino.sa.min;
                body.commission.masterAgent.shareReceive = currentPercentReceive;
                body.commission.masterAgent.commission = agentCommission;
                body.commission.masterAgent.amount = getMoney;
                break;
            case 'AGENT':
                body.commission.agent = {};
                body.commission.agent.parentGroup = currentAgent.parentId;
                body.commission.agent.group = currentAgent._id;
                body.commission.agent.parent = currentAgent.shareSetting.casino.sa.parent;
                body.commission.agent.own = currentAgent.shareSetting.casino.sa.own;
                body.commission.agent.remaining = currentAgent.shareSetting.casino.sa.remaining;
                body.commission.agent.min = currentAgent.shareSetting.casino.sa.min;
                body.commission.agent.shareReceive = currentPercentReceive;
                body.commission.agent.commission = agentCommission;
                body.commission.agent.amount = getMoney;
                break;
        }

        prepareCommission(memberInfo, body, currentAgent.parentId, currentAgent, totalRemaining, overAllShare);
    }
}

function updateAgentMemberBalance(shareReceive, betAmount, ref, updateCallback) {

    console.log('updateAgentMemberBalance');
    console.log('shareReceive : ', shareReceive);


    let balance = betAmount + shareReceive.member.totalWinLoseCom;

    try {
        console.log('update member balance : ', balance);

        MemberService.updateBalance(shareReceive.member.id, balance, shareReceive.betId, 'SA_SETTLE', ref, function (err, updateBalanceResponse) {
            if (err) {
                updateCallback(err, null);
            } else {
                updateCallback(null, updateBalanceResponse);
            }
        });
    } catch (err) {
        MemberService.updateBalance(shareReceive.member.id, balance, shareReceive.betId, 'SA_SETTLE_ERROR ' + shareReceive.member.id + ' : ' + balance, ref, function (err, updateBalanceResponse) {
            if (err) {
                updateCallback(err, null);
            } else {
                updateCallback(null, updateBalanceResponse);
            }
        });
    }

}

function saveSaTransactionLog(request, action, callback) {

    let model = new SaTransactionModel(
        {
            username: request.username,
            currency: request.currency,
            amount: request.amount,
            txnId: request.txnid,
            gameType: request.gametype,
            platform: request.platform,
            gameCode: request.gamecode,
            hostId: request.hostid,
            gameId: request.gameid,
            txnReverseId: request.txn_reverse_id,
            payoutTime: request.PayoutTime,
            action: action,
            createdDate: DateUtils.getCurrentDate(),
        }
    );


    model.save((err, response) => {
        // console.log(err)
        // console.log('save allbet settle transaction')
        callback(null, 'success');
    });
}


router.post('/callback/cancel-1', (req, res) => {


    // let reqQueryString = SaGamingService.decodeDesCBC(decodeURIComponent(req.body))
    const requestBody = req.body;


    console.log(requestBody)
    async.waterfall([callback => {

        BetTransactionModel.findOne({
            'gameDate': {$gte: new Date(moment().add(-10, 'd').format('YYYY-MM-DDTHH:mm:ss.000'))},
            betId: requestBody.betId
        }, function (err, memberResponse) {
            console.log(memberResponse)
            if (err) {
                callback(err, null);
                return;
            }
            if (memberResponse) {
                callback(null, memberResponse);
            } else {
                callback(1004, null);
            }
        }).populate('memberId');

    }, (betObj, callback) => {

        let condition = {
            username: betObj.memberId.username_lower,
            gameId: betObj.baccarat.sa.gameId,
            txnReverseId: betObj.baccarat.sa.txns[0].txnId

        };

        console.log(condition)
        SaTransactionModel.findOne(condition, function (err, memberResponse) {
            if (err) {
                callback(err, null);
                return;
            }
            if (memberResponse) {
                callback(null, betObj, memberResponse);
            } else {
                callback(1004, null);
            }
        });

    }], (err, betObj, cancelHis) => {


        if (err) {
            console.log()
            return res.send({code: 1004, message: "Invalid User Id"});
        }


        let cancelObj = cancelHis;
        let memberInfo = betObj.memberId;

        let betAmount = cancelObj.amount;

        let condition = {
            'memberId': mongoose.Types.ObjectId(memberInfo._id),
            'game': 'CASINO',
            'source': 'SA_GAME',
            'baccarat.sa.gameId': cancelObj.gameId,
            'baccarat.sa.txns.txnId': {$in: cancelObj.txnReverseId},
        };

        BetTransactionModel.findOne(condition).exec(function (err, betObj) {


            console.log('betObj : ', betObj)
            if (!betObj) {
                return res.send(jsonToSAGameResponseFormat({
                    username: requestBody.username,
                    currency: requestBody.currency,
                    amount: 0,
                    error: 1005
                }));
            }

            if (betObj.status !== 'RUNNING') {
                return res.send(jsonToSAGameResponseFormat({
                    username: requestBody.username,
                    currency: requestBody.currency,
                    amount: 0,
                    error: 0
                }));
            }

            let updateCondition = {
                $pull: {'baccarat.sa.txns': {"txnId": cancelObj.txnReverseId}},
                $inc: {
                    amount: betAmount * -1,
                    memberCredit: betAmount,
                    validAmount: betAmount * -1
                }
            };

            BetTransactionModel.update({
                _id: betObj._id,
                'baccarat.sa.txns.txnId': cancelObj.txnReverseId
            }, updateCondition, {safe: true}, function (err, cancelResponse) {
                if (err) {
                    return res.send(jsonToSAGameResponseFormat({
                        username: requestBody.username,
                        currency: requestBody.currency,
                        amount: 0,
                        error: 9999
                    }));
                }

                console.log('cancelResponse : ', cancelResponse)

                if (cancelResponse.nModified == 1) {
                    MemberService.updateBalance(memberInfo._id, betAmount, betObj.betId, 'SA_CANCEL_BET', cancelObj.txnid, function (err, updateBalanceResponse) {

                        if (err) {
                            if (err == 888) {
                                return res.send(jsonToSAGameResponseFormat({
                                    username: requestBody.username,
                                    currency: requestBody.currency,
                                    amount: 0,
                                    error: 0
                                }));
                            }

                            return res.send(jsonToSAGameResponseFormat({
                                username: requestBody.username,
                                currency: requestBody.currency,
                                amount: 0,
                                error: 9999
                            }));

                        } else {

                            BetTransactionModel.remove({
                                _id: betObj._id,
                                "baccarat.sa.txns": {$size: 0}
                            }, function (err, removeResponse) {
                                console.log("remove res : ", removeResponse)
                                console.log(err)
                            });
                            return res.send(jsonToSAGameResponseFormat({
                                username: requestBody.username,
                                currency: requestBody.currency,
                                amount: updateBalanceResponse.newBalance,
                                error: 0
                            }));
                        }
                    });
                } else {
                    return res.send(jsonToSAGameResponseFormat({
                        username: requestBody.username,
                        currency: requestBody.currency,
                        amount: 0,
                        error: 1005
                    }));
                }
            });

        });
    });
});


router.post('/repair-sexy', function (req, res) {

    // let condition = {memberId: mongoose.Types.ObjectId(req.body.memberId), status: 'DONE',betResult:'WIN','gameType': 'PARLAY'};
    //
    let condition = {memberId: mongoose.Types.ObjectId('5d076ce9af29f947a2bb7420')};

    condition['gameDate'] = {
        "$gte": new Date('2019-06-18T11:00:00.000'),
        "$lt": new Date('2019-06-19T00:45:00.000')
    };

    // let condition = { "createdDate": { "$gte": new Date(moment('30/01/2018', "DD/MM/YYYY").format('YYYY-MM-DDT11:00:00.000'))}
    // ,status: 'DONE',betResult:'WIN','gameType': 'PARLAY'};

    BetTransactionModel.find(condition)
    // .select('memberId parlay commission amount baccarat')
        .populate([{path: 'memberParentGroup', select: 'type _id'}, {path: 'memberId'}])
        .exec(function (err, response) {


            if (!response) {
                return res.send({message: "data not found", results: err, code: 999});
            }

            AgentGroupModel.findById(response[0].memberParentGroup._id).select('_id type parentId shareSetting commissionSetting').populate({
                path: 'parentId',
                select: '_id type parentId shareSetting commissionSetting name',
                populate: {
                    path: 'parentId',
                    select: '_id type parentId shareSetting commissionSetting name',
                    populate: {
                        path: 'parentId',
                        select: '_id type parentId shareSetting commissionSetting name',
                        populate: {
                            path: 'parentId',
                            select: '_id type parentId shareSetting commissionSetting name',
                            populate: {
                                path: 'parentId',
                                select: '_id type parentId shareSetting commissionSetting name',
                                populate: {
                                    path: 'parentId',
                                    select: '_id type parentId shareSetting commissionSetting name',
                                }
                            }
                        }
                    }
                }

            }).exec(function (err, agentGroups) {

                async.each(response, (betObj, betListCallback) => {


                    const memberWinLose = betObj.commission.member.winLose;

                    let body = betObj;

                    prepareCommission(betObj.memberId, body, agentGroups, null, 0, 0);

                    let winLose = memberWinLose;

                    let betResult = winLose > 0 ? WIN : winLose < 0 ? LOSE : DRAW;

                    let shareReceive = AgentService.prepareShareReceive(winLose, betResult, betObj);


                    body.commission.superAdmin.winLoseCom = shareReceive.superAdmin.winLoseCom;
                    body.commission.superAdmin.winLose = shareReceive.superAdmin.winLose;
                    body.commission.superAdmin.totalWinLoseCom = shareReceive.superAdmin.totalWinLoseCom;

                    body.commission.company.winLoseCom = shareReceive.company.winLoseCom;
                    body.commission.company.winLose = shareReceive.company.winLose;
                    body.commission.company.totalWinLoseCom = shareReceive.company.totalWinLoseCom;

                    body.commission.shareHolder.winLoseCom = shareReceive.shareHolder.winLoseCom;
                    body.commission.shareHolder.winLose = shareReceive.shareHolder.winLose;
                    body.commission.shareHolder.totalWinLoseCom = shareReceive.shareHolder.totalWinLoseCom;

                    body.commission.senior.winLoseCom = shareReceive.senior.winLoseCom;
                    body.commission.senior.winLose = shareReceive.senior.winLose;
                    body.commission.senior.totalWinLoseCom = shareReceive.senior.totalWinLoseCom;

                    body.commission.masterAgent.winLoseCom = shareReceive.masterAgent.winLoseCom;
                    body.commission.masterAgent.winLose = shareReceive.masterAgent.winLose;
                    body.commission.masterAgent.totalWinLoseCom = shareReceive.masterAgent.totalWinLoseCom;

                    body.commission.agent.winLoseCom = shareReceive.agent.winLoseCom;
                    body.commission.agent.winLose = shareReceive.agent.winLose;
                    body.commission.agent.totalWinLoseCom = shareReceive.agent.totalWinLoseCom;

                    body.commission.member.winLoseCom = shareReceive.member.winLoseCom;
                    body.commission.member.winLose = shareReceive.member.winLose;
                    body.commission.member.totalWinLoseCom = shareReceive.member.totalWinLoseCom;
                    let updateBody = {
                        $set: {
                            'commission': body.commission
                        }
                    };

                    BetTransactionModel.update({
                        _id: betObj._id
                    }, updateBody, function (err, response) {
                        if (err) {
                            betListCallback(err, null);
                        } else {
                            betListCallback(null, response);
                        }
                    });


                }, (err) => {
                    if (err) {
                        return res.send({message: "data not found", results: err, code: 999});
                    } else {
                        console.log(response.length);
                        return res.send(
                            {
                                code: 0,
                                message: "success",
                                result: 'success'
                            }
                        );
                    }
                });//end forEach Match

            });
        });
});


module.exports = router;
