const AgentGroupModel = require('../../models/agentGroup.model');
const MemberModel = require('../../models/member.model');
const BetTransactionModel = require('../../models/betTransaction.model.js');
const AnnouncementModel = require('../../models/announcement.model.js');
const UserModel = require('../../models/users.model');
const express = require('express')
const router = express.Router()
const async = require("async");
const Joi = require('joi');
const mongoose = require('mongoose');
const config = require('config');
const Hashing = require('../../common/hashing');
const logger = require('../../../utils/logger');
const _ = require('underscore');
const zpad = require('zpad');
const naturalSort = require("javascript-natural-sort");



//TODO duplicate code
function getCreditLimitUsage(groupId, callback) {

    async.series([(callback) => {
        AgentGroupModel.aggregate([
            {
                $match: {
                    parentId: mongoose.Types.ObjectId(groupId)
                }
            },
            {
                $project: {_id: 0}
            },
            {
                "$group": {
                    "_id": {
                        id: '$_id'
                    },
                    "creditLimit": {$sum: '$creditLimit'}
                }
            }
        ], function (err, results) {

            if (err) {
                callback(err, '');
            }
            callback(null, results[0] ? results[0].creditLimit : 0);
        });
    }, (callback) => {
        MemberModel.aggregate([
            {
                $match: {
                    group: mongoose.Types.ObjectId(groupId)
                }
            },
            {
                $project: {_id: 0}
            },
            {
                "$group": {
                    "_id": {
                        id: '$_id'
                    },
                    "creditLimit": {$sum: '$creditLimit'}
                }
            }
        ], function (err, results) {

            if (err) {
                callback(err, '');
            }
            callback(null, results[0] ? results[0].creditLimit : 0);
        });
    }], function (err, results) {

        if (err) {
            callback(err, '');
        }
        callback(null, results[0] + results[1]);

    });

}

const addMemberSchema = Joi.object().keys({
    username: Joi.string().required(),
    creditLimit: Joi.number().required(),
    shareSetting: Joi.object().allow(),
    limitSetting: Joi.object().allow(),
    commissionSetting: Joi.object().allow()
});

router.post('/', function (req, res) {

    let validate = Joi.validate(req.body, addMemberSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    const userInfo = req.userInfo;


    let body = {
        username: req.body.username,
        username_lower: req.body.username.toLowerCase(),
        contact: req.body.contact,
        group: userInfo.group._id,
        currency: 'THB',
        creditLimit: req.body.creditLimit,
        balance: 0,
        active: true,
        shareSetting: {
            sportsBook: {
                hdpOuOe: {
                    parent: req.body.shareSetting.sportsBook.hdpOuOe.parent
                },
                oneTwoDoubleChance: {
                    parent: req.body.shareSetting.sportsBook.oneTwoDoubleChance.parent
                },
                others: {
                    parent: req.body.shareSetting.sportsBook.others.parent
                }
            },
            casino: {
                sexy: {
                    parent: req.body.shareSetting.casino.sexy.parent
                }
            },
            game: {
                slotXO: {
                    parent: req.body.shareSetting.game.slotXO.parent
                }
            }
        },
        limitSetting: {
            sportsBook: {
                hdpOuOe: {
                    maxPerBet: req.body.limitSetting.sportsBook.hdpOuOe.maxPerBet,
                    maxPerMatch: req.body.limitSetting.sportsBook.hdpOuOe.maxPerMatch
                },
                oneTwoDoubleChance: {
                    maxPerBet: req.body.limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet,
                    // maxPerMatch: req.body.limitSetting.sportsBook.oneTwoDoubleChance.maxPerMatch
                },
                others: {
                    maxPerBet: req.body.limitSetting.sportsBook.others.maxPerBet,
                    // maxPerMatch: req.body.limitSetting.sportsBook.others.maxPerMatch
                },
                mixParlay: {
                    maxPerBet: req.body.limitSetting.sportsBook.mixParlay.maxPerBet,
                    maxPerMatch: req.body.limitSetting.sportsBook.mixParlay.maxPerMatch
                },
                outright: {
                    // maxPerBet: req.body.limitSetting.sportsBook.outright.maxPerBet,
                    maxPerMatch: req.body.limitSetting.sportsBook.outright.maxPerMatch
                }
            },
            casino: {
                sexy: {
                    isEnable: req.body.limitSetting.casino.sexy.isEnable,
                    limit: req.body.limitSetting.casino.sexy.limit,
                }
            },
            game: {
                slotXO: {
                    isEnable: req.body.limitSetting.game.slotXO.isEnable
                }
            }
        },
        commissionSetting: {
            sportsBook: {
                typeHdpOuOe: req.body.commissionSetting.sportsBook.typeHdpOuOe,
                hdpOuOe: req.body.commissionSetting.sportsBook.hdpOuOe,
                oneTwoDoubleChance: req.body.commissionSetting.sportsBook.oneTwoDoubleChance,
                others: req.body.commissionSetting.sportsBook.others
            },
            casino: {
                sexy: req.body.commissionSetting.casino.sexy,
            },
            game: {
                slotXO: req.body.commissionSetting.game.slotXO,
            }
        },
        configuration: {
            sportsBook: {
                locale: "th-TH",
                odd: "MY",
                views: "DOUBLE",
                acceptAnyOdd: false,
                defaultPrice: {
                    check: false,
                    price: 0
                },
                last: false
            }
        },
    };


    async.series([(callback) => {
        AgentGroupModel.findOne({name_lower: req.body.username.toLowerCase()}, function (err, response) {
            if (err)
                callback(err, null);
            else
                callback(null, response);

        });
    }, (callback) => {
        MemberModel.findOne({username_lower: req.body.username.toLowerCase()}, function (err, response) {
            if (err)
                callback(err, null);
            else
                callback(null, response);

        });
    }, (callback => {
        AgentGroupModel.findById(userInfo.group._id, function (err, response) {
            if (err)
                callback(err, null);
            else
                callback(null, response);
        });
    }), (callback) => {
        getCreditLimitUsage(userInfo.group._id, function (err, response) {
            if (err) {
                callback(err, '');
            } else {
                callback(null, response ? response : 0);
            }
        });
    }], (err, results) => {
        if (err) {
            return res.send({message: "error", result: err, code: 999});
        }

        if (results[0] || results[1]) {
            return res.send({message: "name was already used", result: err, code: 4001});
        }

        //check creditLimit
        let creditLimit = results[2].creditLimit;
        const creditLimitUsage = results[3];

        console.log('own credit : ', creditLimit)
        console.log('creditLimit : ' + req.body.creditLimit + ' , creditLimitUsage : ' + creditLimitUsage);
        if (req.body.creditLimit > (creditLimit - creditLimitUsage)) {
            return res.send({message: "creditLimit exceeded", result: err, code: 4002});
        }


        let model = new MemberModel(body);

        model.save(function (err, memberResponse) {

            if (err) {
                console.log(err)
                return res.send({message: "error", results: err, code: 999});
            }

            AgentGroupModel.update(
                {_id: userInfo.group._id},
                {$push: {childMembers: memberResponse._id}}, function (err, data) {
                    if (err) {
                        return res.send({message: "error", results: err, code: 999});
                    } else {
                        console.log('update parent child success');
                        return res.send(
                            {
                                code: 0,
                                message: "success",
                                result: memberResponse
                            }
                        );
                    }
                });
        });
    });

});

module.exports = router;

