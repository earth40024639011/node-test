const express = require('express');
const router = express.Router();
const request = require('request');
const moment = require('moment');
const querystring = require('querystring');
const async = require("async");
const config = require('config');
const Joi = require('joi');
const _ = require('underscore');
const crypto = require('crypto');
const roundTo = require('round-to');


// API URL : http://hosay126.com/API
// VendorID : Px4hYs01nS741
// OperatorID : AMBBET
// SecretKey : Nuqi8LWSoS
// SportsbookMemberGameURL : http://mkt.[domain]/Deposit_ProcessLogin.aspx
// SportsbookPublicGameURL : http://mkt.[domain]/vender.aspx
// SportsbookMemberMobileGameURL : http://ismart.[domain]/deposit_ProcessLogin.aspx

const IBC_URL = process.env.IBC_URL || 'http://hosay126.com/API/';
const IBC_VENDOR_ID = process.env.IBC_VENDOR_ID || 'Px4hYs01nS741';
const IBC_OPER_ID = process.env.IBC_OPER_ID || 'AMBBET';
const IBC_SECRET_KEY = process.env.IBC_SECRET_KEY || 'Nuqi8LWSoS';



router.get('/createMember', (req, res) => {


    const username = req.userInfo.username;
    let headers = {
        'Content-Type': 'application/json'
    };

    let data = {
        vendor_id:IBC_VENDOR_ID,
        vendor_member_id:username,
        operatorid:IBC_OPER_ID,
        firstname:'firstname',
        lastname:'lastname',
        username:username,
        oddstype:'a',
        currency:4,
        maxtransfer:10000000,
        mintransfer:10,
        custominfo1:'',
        custominfo2:'',
        custominfo3:'',
        custominfo4:''

    }

    console.log(data)

    let options = {
        url: IBC_URL + 'CreateMember',
        method: 'POST',
        headers: headers,
        json: data,
    };

    console.log(options)

    // console.log(options)

    request(options, (error, response, body) => {
        console.log(body)
        if (error) {
            console.log(error)
            return res.send({message: error, code: 999});
        } else {
            // let json = JSON.parse(body);

            console.log(body)
            return res.send(body);
        }
    });

});

router.get('/auth_session_token', (req, res) => {


    const username = req.userInfo.username;
    let headers = {
        'Content-Type': 'application/json'
    };

    let data = {
        secret:IBC_SECRET_KEY,
        session_token:IBC_VENDOR_ID+'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7Il9pZCI6IjU5ZTYyY2M4YWM1NjNkZDYxNDg2NmE5YyIsImNvbnRhY3QiOiJCb3kiLCJ1c2VybmFtZSI6IjUxcnJib3kiLCJ1c2VybmFtZV9sb3dlciI6IjUxcnJib3kiLCJncm91cCI6eyJfaWQiOiI1OWU2MTNmODAxYmY4MzliNzk0ZGE3MTQiLCJuYW1lIjoiNTFyciIsInR5cGUiOiJTRU5JT1IifSwiY3VycmVuY3kiOiJUSEIiLCJjb21taXNzaW9uU2V0dGluZyI6eyJzcG9ydHNCb29rIjp7Im90aGVycyI6MSwib25lVHdvRG91YmxlQ2hhbmNlIjowLjQsImhkcE91T2UiOjAuNCwidHlwZUhkcE91T2UiOiJBIn0sImNhc2lubyI6eyJzZXh5IjowfSwiZ2FtZSI6eyJzbG90WE8iOjB9fSwiY3JlZGl0IjoxMDAwMDAsInRlcm1BbmRDb25kaXRpb24iOnRydWUsImZvcmNlQ2hhbmdlUGFzc3dvcmQiOmZhbHNlLCJmb3JjZUNoYW5nZVBhc3N3b3JkRGF0ZSI6IjIwMTgtMDctMjVUMTM6MjI6NTYuMjUwWiIsInN1c3BlbmQiOmZhbHNlLCJ1aWQiOiI0YzY1Njg4MS0wNjdjLTQxZWQtYjE2Ni0xNzhkMTcxZTMwYjAiLCJpc1N1c3BlbmQiOmZhbHNlfSwiaWF0IjoxNTI3NDkyOTA2LCJleHAiOjE4ODc0OTI5MDZ9.00G5Xz_abYzFzShro7n5MJF388TEqtdhzboB6a1-C2Y',
        op:'auth'


    }

    console.log(data)

    let options = {
        url: IBC_URL + 'auth_session_token',
        method: 'POST',
        headers: headers,
        json: data,
    };

    console.log(options)

    // console.log(options)

    request(options, (error, response, body) => {
        console.log(body)
        if (error) {
            console.log(error)
            return res.send({message: error, code: 999});
        } else {
            // let json = JSON.parse(body);

            console.log(body)
            return res.send(body);
        }
    });

});


module.exports = router;