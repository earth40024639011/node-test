const express = require('express');
const router = express.Router();
const _ = require('underscore');
const async = require("async");
const hmacsha1 = require('hmacsha1');
const moment = require('moment');
const querystring = require('querystring');
const Crypto = require("crypto");
const roundTo = require('round-to');
const NumberUtils = require('../../common/numberUtils1');
const AgentService = require('../../common/agentService');
const hash = require('../../common/hashing');
const GanapatiService = require('../../common/ganapatiService');
const Joi = require('joi');
const MemberService = require('../../common/memberService');
const DateUtils = require('../../common/dateUtils');
const AgentGroupModel            = require('../../models/agentGroup.model.js');
const BetTransactionModel = require('../../models/betTransaction.model.js');
const TokenModel = require('../../models/token.model');
const GanapatiHistoryModel = require('../../models/ganapatiHistory.model');


const getClientKey = require('../getClientKey');
const {
  GANAPATI_SECRET_KEY: SECRET_KEY = 'integration'
} = getClientKey.getKey();


const DRAW = "DRAW";
const WIN = "WIN";
const HALF_WIN = "HALF_WIN";
const LOSE = "LOSE";
const HALF_LOSE = "HALF_LOSE";
const CURRENCY = "THB";



function random(low, high) {
    return Math.floor(Math.random() * (high - low + 1) + low)
}
function generateBetId() {
    return 'BET' + new Date().getTime() + random(1000000, 9999999);
}


router.get('/list-games', function (req, res) {
    let gameList = GanapatiService.callGetGameList();
    return res.send({
        code : 0,
        result : gameList,
        message : 'success'
    })
});


const forwardGame = Joi.object().keys({
    gameCode: Joi.string().required(),
    language: Joi.string().allow(''),
    redirectUrl: Joi.string().allow('')
});

router.post('/forward-to-game', function (req, res) {
    let validate = Joi.validate(req.body, forwardGame);
    if (validate.error) {
        return res.send({
            code : 999,
            message : 'Invalid parameters',
            result : ''
        })
    }
    const userInfo       = req.userInfo;
    let requestBody = req.body;

    async.waterfall([callback => {
        try {
            AgentService.getUpLineStatus(userInfo.group._id, function (err, status) {
                if (err) {
                    callback(err, null);
                } else {
                    if (status.suspend) {
                        callback(5011, null);
                    } else {
                        callback(null, status);
                    }
                }
            });
        } catch (err) {
            callback(9929, null);
        }

    }, (uplineStatus, callback) => {

        try {
            MemberService.findById(userInfo._id, 'creditLimit balance limitSetting group suspend username_lower', function (err, memberResponse) {
                if (err) {
                    callback(err, null);
                } else {

                    if (memberResponse.suspend) {
                        callback(5011, null);
                    } else if (memberResponse.limitSetting.game.slotXO.isEnable) {
                        AgentService.getLive22Status(memberResponse.group._id, (err, response) => {
                            if (!response.isEnable) {
                                callback(1088, null);
                            } else {
                                callback(null, uplineStatus, memberResponse);
                            }
                        });
                    } else {
                        callback(1088, null);
                    }
                }
            });
        } catch (err) {
            callback(999, null);
        }

    }], function (err, uplineStatus, memberInfo) {

        if (err) {
            if (err == 1088) {
                return res.send({
                    code: 1088,
                    message: 'Service Locked, Please contact your upline.'
                });
            }
            if (err == 5011) {
                return res.send({
                    message: "Account or Upline was suspend.",
                    result: null,
                    code: 5011
                });
            }
            return res.send({code: 999, message: err});
        }

        TokenModel.findOne({username: userInfo.username}, function (err, response) {
            console.log('find by token ',response);
            if(response) {
                GanapatiService.forwardToGame(response.uniqueId, requestBody.gameCode, (err, result) =>{
                    console.log('---------forwardToGame----------',result);
                    return res.send({
                        code : 0,
                        message : 'success',
                        result : result
                    })
                })
            } else {
                return res.send({
                    code : 1,
                    message : 'Token not found.',
                    result : ''
                })
            }

        });
    });

});

router.post('/callback/authenticate', function (req, res) {


    let requestBody = req.body;

    console.log('request token == ',requestBody);

    TokenModel.findOne({uniqueId: requestBody.launchToken}, function (err, response) {
        // console.log('token = ',response);
        if(response) {

            MemberService.findByUserNameForPartnerService(response.username, (errr, memberResponse) => {
                // console.log('memberResponse================= ',memberResponse);
                if (memberResponse) {

                    let credit = roundTo((memberResponse.creditLimit + memberResponse.balance), 2);

                    let obj = {
                        "balance": credit,
                        "currency": CURRENCY,
                        "playerId": memberResponse._id,
                        "sessionId": requestBody.launchToken,
                        "account": {
                            "country": "TH",
                            "birthDate": "",
                            "gender": "male",
                            "alias":response.username
                        }
                    }
                    console.log(SECRET_KEY,'===obj ============== ',obj);

                    res.header('hash',hash.sha1Hmac(SECRET_KEY,'authenticate'+JSON.stringify(obj)) );
                    return res.send(obj);

                } else {
                    return res.send({
                        "errorCode": 103,
                        "description": "Token not found.",
                        "balance": 0,
                        "currency": CURRENCY
                    });
                }

            });
        } else {
            return res.send({
                "errorCode": 103,
                "description": "Token not found.",
                "balance": 0,
                "currency": CURRENCY
            });
        }

    });

});

const getBalanceSchema = Joi.object().keys({
    playerId: Joi.string().required(),
    sessionId: Joi.string().required()
});

router.post('/callback/fetchBalance', function (req, res) {
    let validate = Joi.validate(req.body, getBalanceSchema);
    if (validate.error) {
        return res.send({
            "errorCode": 102,
            "description": "Invalid parameters.",
            "balance": 0,
            "currency": CURRENCY
        })
    }

    let requestBody = req.body;
    console.log('fetchBalance ================================== ', requestBody);

    MemberService.findById(requestBody.playerId,  (err, memberResponse) => {

        if (err) {
            return res.send({
                "errorCode": 103,
                "description": "Player not found.",
                "balance": 0,
                "currency": CURRENCY
            })
        }

        if (memberResponse) {

            let credit = roundTo((memberResponse.creditLimit + memberResponse.balance), 2);
            return res.send({
                "balance": credit,
                "currency": CURRENCY
            });

        } else {
            let obj = {
                "errorCode": 103,
                "description": "Player not found.",
                "balance": 0,
                "currency": CURRENCY
            }

            res.header('hash',hash.sha1Hmac(SECRET_KEY,'fetchBalance'+JSON.stringify(obj)) );
            return res.send(obj)
        }

    });
});


router.post('/callback/fetchBalance', function (req, res) {
    let validate = Joi.validate(req.body, getBalanceSchema);
    if (validate.error) {
        return res.send({
            "errorCode": 102,
            "description": "Invalid parameters.",
            "balance": 0,
            "currency": CURRENCY
        })
    }

    let requestBody = req.body;
    console.log('fetchBalance ================================== ', requestBody);

    MemberService.findById(requestBody.playerId,  (err, memberResponse) => {

        if (err) {
            return res.send({
                "errorCode": 103,
                "description": "Player not found.",
                "balance": 0,
                "currency": CURRENCY
            })
        }

        if (memberResponse) {

            let credit = roundTo((memberResponse.creditLimit + memberResponse.balance), 2);
            let obj = {
                "balance": credit,
                "currency": CURRENCY
            };

            res.header('hash',hash.sha1Hmac(SECRET_KEY,'fetchBalance'+JSON.stringify(obj)) );
            return res.send(obj)

        } else {
            let obj = {
                "errorCode": 103,
                "description": "Player not found.",
                "balance": 0,
                "currency": CURRENCY
            }

            res.header('hash',hash.sha1Hmac(SECRET_KEY,'fetchBalance'+JSON.stringify(obj)) );
            return res.send(obj)
        }

    });
});

const withdraw = Joi.object().keys({
    playerId: Joi.string().required(),
    sessionId: Joi.string().required(),
    amount: Joi.number().required(),
    currency: Joi.string().required(),
    transactionId: Joi.string().required(),
    game: Joi.string().required(),
    gameRound: Joi.string().required(),
    roundEnd: Joi.boolean().required(),
    extra : Joi.object().allow()
});

router.post('/callback/withdraw', function (req, res) {
    let validate = Joi.validate(req.body, withdraw);
    if (validate.error) {
        return res.send({
            "errorCode": 102,
            "description": "Invalid Input.",
            "balance": 0,
            "currency": CURRENCY
        })
    }

    let requestBody = req.body;

    let hisModel = new GanapatiHistoryModel(requestBody);
    hisModel.save((err, hisRes) => {
        if (err) {
            console.log('-------save GanapatiHistoryModel fail-------')
        } else {
            console.log('-------success-------')
            // callback(null, agentResponse);
        }
    });

    let betAmount = roundTo(Number.parseFloat(requestBody.amount), 2);

    if(betAmount < 0){
        return res.send({
            "errorCode": 102,
            "description": "Invalid Input.",
            "balance": 0,
            "currency": CURRENCY
        })
    }
    // console.log('amount = ', betAmount);

    async.series([callback => {

        MemberService.findById(requestBody.playerId, 'creditLimit balance limitSetting shareSetting commissionSetting group currency username_lower ipAddress', function (err, memberResponse) {
            if (err) {
                callback(err, null);
                return;
            }

            if (memberResponse) {
                let credit = roundTo((memberResponse.creditLimit + memberResponse.balance), 2);
                // console.log('credit ==== ',credit);
                if (typeof credit !== 'number' || _.isUndefined(credit) || _.isNaN(credit)) {
                    callback(900606, memberResponse);
                }else {
                    if (credit < betAmount) {
                        callback(900605, memberResponse);
                    } else {
                        callback(null, memberResponse);
                    }
                }
            }
        });

    }], (err, asyncResponse) => {

        if (err) {
            if (err == 900404) {
                return res.send({
                    "errorCode": 200,
                    "description": "Transaction Declined.",
                    "balance": 0,
                    "currency": CURRENCY
                })
            } else if (err == 900605) {
                let memberInfo = asyncResponse[0];
                let credit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2);

                return res.send({
                    "errorCode": 201,
                    "description": "Insufficient funds.",
                    "balance": credit,
                    "currency": CURRENCY
                })

            } else if (err == 900606) {
                let memberInfo = asyncResponse[0];
                let credit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2);

                return res.send({
                    "errorCode": 101,
                    "description": "Internal Server Error.",
                    "balance": credit,
                    "currency": CURRENCY
                })


            } else {
                return res.send({
                    "errorCode": 200,
                    "description": "Transaction Declined.",
                    "balance": 0,
                    "currency": CURRENCY
                })
            }
        }

        let memberInfo = asyncResponse[0];

        let credit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2);

        if(credit < betAmount){
            return res.send({
                "errorCode": 201,
                "description": "Insufficient funds.",
                "balance": credit,
                "currency": CURRENCY
            })
        }

        let condition = {
            'memberId': requestBody.playerId,
            'slot.ganapati.gameRound': requestBody.gameRound
        };

        BetTransactionModel.findOne(condition, (err, response) => {
            if(response){
                //duplicate transaction

                let obj = {
                    "balance": credit,
                    "currency": CURRENCY
                };

                console.log(SECRET_KEY, '-------------------duplicate---------------------', obj)

                res.header('hash',hash.sha1Hmac(SECRET_KEY,'withdraw'+JSON.stringify(obj)) );
                return res.send(obj)
            }else{
                AgentGroupModel.findById(memberInfo.group).select('_id type parentId shareSetting commissionSetting').populate({
                    path: 'parentId',
                    select: '_id type parentId shareSetting commissionSetting name',
                    populate: {
                        path: 'parentId',
                        select: '_id type parentId shareSetting commissionSetting name',
                        populate: {
                            path: 'parentId',
                            select: '_id type parentId shareSetting commissionSetting name',
                            populate: {
                                path: 'parentId',
                                select: '_id type parentId shareSetting commissionSetting name',
                                populate: {
                                    path: 'parentId',
                                    select: '_id type parentId shareSetting commissionSetting name',
                                    populate: {
                                        path: 'parentId',
                                        select: '_id type parentId shareSetting commissionSetting name',
                                    }
                                }
                            }
                        }
                    }

                }).exec(function (err, agentGroups) {

                    let body = {
                        memberId: memberInfo._id,
                        memberParentGroup: memberInfo.group,
                        memberUsername: memberInfo.username_lower,
                        betId: generateBetId(),
                        slot: {
                            ganapati : {
                                transactionId: requestBody.transactionId,
                                amount : betAmount,
                                gameName: requestBody.game,
                                gameRound : requestBody.gameRound,
                                roundEnd : requestBody.roundEnd,
                                winLoss : betAmount * -1
                            }
                        },
                        amount: betAmount,
                        memberCredit: betAmount * -1,
                        payout: 0,
                        validAmount: betAmount,
                        gameType: 'TODAY',
                        acceptHigherPrice: false,
                        priceType: 'TH',
                        game: 'GAME',
                        source: 'GANAPATI',
                        status: requestBody.roundEnd ? 'DONE' : 'RUNNING',
                        commission: {},
                        gameDate: DateUtils.getCurrentDate(),
                        createdDate: DateUtils.getCurrentDate(),
                        isEndScore: false,
                        currency: memberInfo.currency,
                        ipAddress: memberInfo.ipAddress,
                        settleDate :DateUtils.getCurrentDate()
                    };

                    prepareCommission(memberInfo, body, agentGroups, null, 0, 0);

                    if (requestBody.roundEnd){

                        let winLose = betAmount * -1;
                        let betResult =  LOSE;

                        //init
                        if (!body.commission.senior) {
                            body.commission.senior = {};
                        }

                        if (!body.commission.masterAgent) {
                            body.commission.masterAgent = {};
                        }

                        if (!body.commission.agent) {
                            body.commission.agent = {};
                        }

                        const shareReceive = AgentService.prepareShareReceive(winLose, betResult, body);


                        body.commission.superAdmin.winLoseCom = shareReceive.superAdmin.winLoseCom;
                        body.commission.superAdmin.winLose = shareReceive.superAdmin.winLose;
                        body.commission.superAdmin.totalWinLoseCom = shareReceive.superAdmin.totalWinLoseCom;

                        body.commission.company.winLoseCom = shareReceive.company.winLoseCom;
                        body.commission.company.winLose = shareReceive.company.winLose;
                        body.commission.company.totalWinLoseCom = shareReceive.company.totalWinLoseCom;

                        body.commission.shareHolder.winLoseCom = shareReceive.shareHolder.winLoseCom;
                        body.commission.shareHolder.winLose = shareReceive.shareHolder.winLose;
                        body.commission.shareHolder.totalWinLoseCom = shareReceive.shareHolder.totalWinLoseCom;

                        body.commission.senior.winLoseCom = shareReceive.senior.winLoseCom;
                        body.commission.senior.winLose = shareReceive.senior.winLose;
                        body.commission.senior.totalWinLoseCom = shareReceive.senior.totalWinLoseCom;

                        body.commission.masterAgent.winLoseCom = shareReceive.masterAgent.winLoseCom;
                        body.commission.masterAgent.winLose = shareReceive.masterAgent.winLose;
                        body.commission.masterAgent.totalWinLoseCom = shareReceive.masterAgent.totalWinLoseCom;

                        body.commission.agent.winLoseCom = shareReceive.agent.winLoseCom;
                        body.commission.agent.winLose = shareReceive.agent.winLose;
                        body.commission.agent.totalWinLoseCom = shareReceive.agent.totalWinLoseCom;

                        body.commission.member.winLoseCom = shareReceive.member.winLoseCom;
                        body.commission.member.winLose = shareReceive.member.winLose;
                        body.commission.member.totalWinLoseCom = shareReceive.member.totalWinLoseCom;
                        body.betResult = betResult;
                    }



                    let betTransactionModel = new BetTransactionModel(body);

                    betTransactionModel.save(function (err, response) {
                        if (err) {
                            if (err.code == 11000) {
                                return res.send({
                                    "errorCode": 200,
                                    "description": "Transaction Declined.",
                                    "balance": credit,
                                    "currency": CURRENCY
                                })
                            }
                            return res.send({
                                "errorCode": 101,
                                "description": "Internal Server Error.",
                                "balance": credit,
                                "currency": CURRENCY
                            })
                        }

                        MemberService.updateBalance(requestBody.playerId, (betAmount * -1), response.betId, 'BET_GANAPATI', requestBody.transactionId, function (err, updateBalanceResponse) {
                            if (err) {
                                return res.send({
                                    "errorCode": 101,
                                    "description": "Internal Server Error.",
                                    "balance": credit,
                                    "currency": CURRENCY
                                })
                            } else {
                                let obj = {
                                    "balance": updateBalanceResponse.newBalance,
                                    "currency": CURRENCY
                                };

                                res.header('hash',hash.sha1Hmac(SECRET_KEY,'withdraw'+JSON.stringify(obj)) );
                                return res.send(obj);
                            }
                        });
                    });
                });
            }
        })

    });

});


const deposit = Joi.object().keys({
    playerId: Joi.string().required(),
    sessionId: Joi.string().required(),
    amount: Joi.number().required(),
    currency: Joi.string().required(),
    transactionId: Joi.string().required(),
    game: Joi.string().required(),
    gameRound: Joi.string().required(),
    roundEnd: Joi.boolean().required(),
    extra : Joi.object().allow()
});

router.post('/callback/deposit', function (req, res) {
    let validate = Joi.validate(req.body, deposit);
    if (validate.error) {
        return res.send({
            "errorCode": 102,
            "description": "Invalid Input.",
            "balance": 0,
            "currency": CURRENCY
        })
    }

    let requestBody = req.body;

    let hisModel = new GanapatiHistoryModel(requestBody);
    hisModel.save((err, hisRes) => {
        if (err) {
            console.log('-------save GanapatiHistoryModel fail-------')
        } else {
            console.log('-------success-------')
        }
    });

    let betAmount = roundTo(Number.parseFloat(requestBody.amount), 2);

    if(betAmount < 0){
        return res.send({
            "errorCode": 102,
            "description": "Invalid Input.",
            "balance": 0,
            "currency": CURRENCY
        })
    }

    async.series([callback => {

        MemberService.findById(requestBody.playerId, 'creditLimit balance limitSetting shareSetting commissionSetting group currency username_lower ipAddress', function (err, memberResponse) {
            if (err) {
                callback(err, null);
                return;
            }else {
                callback(null, memberResponse);
            }

        });

    }], (err, asyncResponse) => {

        if (err) {
            if (err == 900404) {
                return res.send({
                    "errorCode": 102,
                    "description": "Invalid Input.",
                    "balance": 0,
                    "currency": CURRENCY
                })
            } else {
                return res.send({
                    "errorCode": 101,
                    "description": "Player not found.",
                    "balance": 0,
                    "currency": CURRENCY
                })
            }
        }

        let memberInfo = asyncResponse[0];

        let credit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2);

        async.series([callback => {

            let condition = {
                'memberId': requestBody.playerId,
                'slot.ganapati.gameRound': requestBody.gameRound
            };

            BetTransactionModel.findOne(condition).lean().exec(function (err, mainObj) {
                // console.log('active : ',mainObj);
                if (err) {
                    callback(err, null);
                    return
                }

                if (!mainObj) {
                    callback(900415, null);
                } else {

                    if (mainObj.status == 'DONE') {
                        callback(900409)
                    }else {
                        callback(null, mainObj);
                    }
                }

            });

        }], (err, asyncResponse) => {

            let activeBetObj = asyncResponse[0];

            console.log('activeBetObj======= =================',activeBetObj);
            if (err) {

                if (err == 900415) {
                    return res.send({
                        "errorCode": 200,
                        "description": "The Bet can not be found - can't settle.",
                        "balance": credit,
                        "currency": CURRENCY
                    })
                }

                if (err == 900409) {
                    return res.send({
                        "errorCode": 200,
                        "description": "The Bet was settled.",
                        "balance": credit,
                        "currency": CURRENCY
                    })
                }
            }

            let winLose = betAmount - activeBetObj.amount;
            let betResult = winLose > 0 ? 'WIN' : winLose < 0 ? 'LOSE' : 'DRAW';

            const shareReceive = AgentService.prepareShareReceive(winLose, betResult, activeBetObj);

            let updateBody = {
                $set: {

                    'slot.ganapati.roundEnd': requestBody.roundEnd,
                    'slot.ganapati.winLoss': winLose,
                    'commission.superAdmin.winLoseCom': shareReceive.superAdmin.winLoseCom,
                    'commission.superAdmin.winLose': shareReceive.superAdmin.winLose,
                    'commission.superAdmin.totalWinLoseCom': shareReceive.superAdmin.totalWinLoseCom,

                    'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                    'commission.company.winLose': shareReceive.company.winLose,
                    'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                    'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                    'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                    'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                    'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                    'commission.senior.winLose': shareReceive.senior.winLose,
                    'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                    'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                    'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                    'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                    'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                    'commission.agent.winLose': shareReceive.agent.winLose,
                    'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                    'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                    'commission.member.winLose': shareReceive.member.winLose,
                    'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                    'betResult': betResult,
                    'isEndScore': true,
                    'status': 'DONE'
                }
            };

            BetTransactionModel.update({_id: activeBetObj._id}, updateBody, function (err, response) {
                if (response.nModified == 1) {
                    MemberService.updateBalance(requestBody.playerId, betAmount, activeBetObj.betId, 'SETTLE_GANAPATI', requestBody.transactionId, function (err, creditResponse) {
                        if (err) {
                            return res.send({
                                "errorCode": 101,
                                "description": "Internal Server Error.",
                                "balance": credit,
                                "currency": CURRENCY
                            })
                        } else {
                            let obj = {
                                "balance": creditResponse.newBalance,
                                "currency": CURRENCY
                            };

                            res.header('hash',hash.sha1Hmac(SECRET_KEY,'deposit'+JSON.stringify(obj)) );
                            return res.send(obj)
                        }
                    });
                } else {
                    return res.send({
                        "errorCode": 101,
                        "description": "Internal Server Error.",
                        "balance": credit,
                        "currency": CURRENCY
                    })
                }
            });


        });
    });
});


const Bet = Joi.object().keys({
    playerId: Joi.string().required(),
    sessionId: Joi.string().required(),
    withdrawAmount: Joi.number().required(),
    depositAmount: Joi.number().required(),
    currency: Joi.string().required(),
    transactionId: Joi.string().required(),
    game: Joi.string().required(),
    gameRound: Joi.string().required(),
    roundEnd: Joi.boolean().required(),
    extra : Joi.object().allow()
});

router.post('/callback/applyTransaction', function (req, res) {
    let validate = Joi.validate(req.body, Bet);
    if (validate.error) {
        return res.send({
            "errorCode": 102,
            "description": "Invalid Input.",
            "balance": 0,
            "currency": CURRENCY
        })
    }

    let requestBody = req.body;

    // let obj = {
    //     "balance": 10000,
    //     "currency": CURRENCY
    // };
    //
    // res.header('hash',hash.sha1Hmac(SECRET_KEY,'applyTransaction'+JSON.stringify(obj)) );
    // return res.send(obj);

    console.log('applyTransaction ================ ', requestBody);

    let hisModel = new GanapatiHistoryModel(requestBody);
    hisModel.save((err, hisRes) => {
        if (err) {
            console.log('-------save GanapatiHistoryModel fail-------')
        } else {
            console.log('-------success-------')
            // callback(null, agentResponse);
        }
    });

    let betAmount = roundTo(Number.parseFloat(requestBody.withdrawAmount), 2);
    let receiveAmount = roundTo(Number.parseFloat(requestBody.depositAmount), 2);

    async.series([callback => {

        MemberService.findById(requestBody.playerId, 'creditLimit balance limitSetting shareSetting commissionSetting group currency username_lower ipAddress', function (err, memberResponse) {
            if (err) {
                callback(err, null);
                return;
            }

            if (memberResponse) {
                let credit = roundTo((memberResponse.creditLimit + memberResponse.balance), 2);
                // console.log('credit ==== ',credit);
                if (typeof credit !== 'number' || _.isUndefined(credit) || _.isNaN(credit)) {
                    callback(900606, memberResponse);
                }else {
                    if (credit < betAmount) {
                        callback(900605, memberResponse);
                    } else {
                        callback(null, memberResponse);
                    }
                }
            }
        });

    }], (err, asyncResponse) => {

        if (err) {
            if (err == 900404) {
                return res.send({
                    "errorCode": 200,
                    "description": "Transaction Declined.",
                    "balance": 0,
                    "currency": CURRENCY
                })
            } else if (err == 900605) {
                let memberInfo = asyncResponse[0];
                let credit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2);

                return res.send({
                    "errorCode": 201,
                    "description": "Insufficient funds.",
                    "balance": credit,
                    "currency": CURRENCY
                })

            } else if (err == 900606) {
                let memberInfo = asyncResponse[0];
                let credit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2);

                return res.send({
                    "errorCode": 101,
                    "description": "Internal Server Error.",
                    "balance": credit,
                    "currency": CURRENCY
                })


            } else {
                return res.send({
                    "errorCode": 200,
                    "description": "Transaction Declined.",
                    "balance": 0,
                    "currency": CURRENCY
                })
            }
        }

        let memberInfo = asyncResponse[0];
        console.log('member info = ',memberInfo);
        let credit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2);

        let condition = {
            'memberId': requestBody.playerId,
            'slot.ganapati.transactionId': requestBody.transactionId
        };

        BetTransactionModel.findOne(condition, (err, response) => {
            if(response){
                //duplicate transaction

                let obj = {
                    "balance": credit,
                    "currency": CURRENCY
                };

                console.log(SECRET_KEY, '-------------------duplicate---------------------', obj)

                res.header('hash',hash.sha1Hmac(SECRET_KEY,'applyTransaction'+JSON.stringify(obj)) );
                return res.send(obj)
            }else{
                AgentGroupModel.findById(memberInfo.group).select('_id type parentId shareSetting commissionSetting').populate({
                    path: 'parentId',
                    select: '_id type parentId shareSetting commissionSetting name',
                    populate: {
                        path: 'parentId',
                        select: '_id type parentId shareSetting commissionSetting name',
                        populate: {
                            path: 'parentId',
                            select: '_id type parentId shareSetting commissionSetting name',
                            populate: {
                                path: 'parentId',
                                select: '_id type parentId shareSetting commissionSetting name',
                                populate: {
                                    path: 'parentId',
                                    select: '_id type parentId shareSetting commissionSetting name',
                                    populate: {
                                        path: 'parentId',
                                        select: '_id type parentId shareSetting commissionSetting name',
                                    }
                                }
                            }
                        }
                    }

                }).exec(function (err, agentGroups) {

                    let body = {
                        memberId: memberInfo._id,
                        memberParentGroup: memberInfo.group,
                        memberUsername: memberInfo.username_lower,
                        betId: generateBetId(),
                        slot: {
                            ganapati : {
                                transactionId: requestBody.transactionId,
                                withdrawAmount : requestBody.withdrawAmount,
                                depositAmount : requestBody.depositAmount,
                                gameName: requestBody.game,
                                gameRound : requestBody.gameRound,
                                roundEnd : requestBody.roundEnd,
                                winLoss : receiveAmount - betAmount
                            }
                        },
                        amount: betAmount,
                        memberCredit: betAmount * -1,
                        payout: receiveAmount,
                        validAmount: betAmount,
                        gameType: 'TODAY',
                        acceptHigherPrice: false,
                        priceType: 'TH',
                        game: 'GAME',
                        source: 'GANAPATI',
                        status: 'DONE',
                        commission: {},
                        gameDate: DateUtils.getCurrentDate(),
                        createdDate: DateUtils.getCurrentDate(),
                        isEndScore: false,
                        currency: memberInfo.currency,
                        ipAddress: memberInfo.ipAddress,
                        settleDate :DateUtils.getCurrentDate()
                    };

                    prepareCommission(memberInfo, body, agentGroups, null, 0, 0);

                    let winLose = receiveAmount - betAmount;
                    let betResult = winLose > 0 ? WIN : winLose < 0 ? LOSE : DRAW;

                    //init
                    if (!body.commission.senior) {
                        body.commission.senior = {};
                    }

                    if (!body.commission.masterAgent) {
                        body.commission.masterAgent = {};
                    }

                    if (!body.commission.agent) {
                        body.commission.agent = {};
                    }

                    // console.log('body === ',body);
                    const shareReceive = AgentService.prepareShareReceive(winLose, betResult, body);


                    body.commission.superAdmin.winLoseCom = shareReceive.superAdmin.winLoseCom;
                    body.commission.superAdmin.winLose = shareReceive.superAdmin.winLose;
                    body.commission.superAdmin.totalWinLoseCom = shareReceive.superAdmin.totalWinLoseCom;

                    body.commission.company.winLoseCom = shareReceive.company.winLoseCom;
                    body.commission.company.winLose = shareReceive.company.winLose;
                    body.commission.company.totalWinLoseCom = shareReceive.company.totalWinLoseCom;

                    body.commission.shareHolder.winLoseCom = shareReceive.shareHolder.winLoseCom;
                    body.commission.shareHolder.winLose = shareReceive.shareHolder.winLose;
                    body.commission.shareHolder.totalWinLoseCom = shareReceive.shareHolder.totalWinLoseCom;

                    body.commission.senior.winLoseCom = shareReceive.senior.winLoseCom;
                    body.commission.senior.winLose = shareReceive.senior.winLose;
                    body.commission.senior.totalWinLoseCom = shareReceive.senior.totalWinLoseCom;

                    body.commission.masterAgent.winLoseCom = shareReceive.masterAgent.winLoseCom;
                    body.commission.masterAgent.winLose = shareReceive.masterAgent.winLose;
                    body.commission.masterAgent.totalWinLoseCom = shareReceive.masterAgent.totalWinLoseCom;

                    body.commission.agent.winLoseCom = shareReceive.agent.winLoseCom;
                    body.commission.agent.winLose = shareReceive.agent.winLose;
                    body.commission.agent.totalWinLoseCom = shareReceive.agent.totalWinLoseCom;

                    body.commission.member.winLoseCom = shareReceive.member.winLoseCom;
                    body.commission.member.winLose = shareReceive.member.winLose;
                    body.commission.member.totalWinLoseCom = shareReceive.member.totalWinLoseCom;
                    body.betResult = betResult;

                    console.log('body = ', body);
                    let betTransactionModel = new BetTransactionModel(body);

                    betTransactionModel.save(function (err, response) {
                        // console.log('err ===== ',err);
                        if (err) {
                            if (err.code == 11000) {
                                return res.send({
                                    "errorCode": 200,
                                    "description": "Transaction Declined.",
                                    "balance": credit,
                                    "currency": CURRENCY
                                })
                            }
                            return res.send({
                                "errorCode": 101,
                                "description": "Internal Server Error.",
                                "balance": credit,
                                "currency": CURRENCY
                            })
                        }

                        MemberService.updateBalance(requestBody.playerId, (receiveAmount-betAmount), response.betId, 'SLOT_GANAPATI', requestBody.transactionId, function (err, updateBalanceResponse) {
                            if (err) {
                                return res.send({
                                    "errorCode": 101,
                                    "description": "Internal Server Error.",
                                    "balance": credit,
                                    "currency": CURRENCY
                                })
                            } else {
                                let obj = {
                                    "balance": updateBalanceResponse.newBalance,
                                    "currency": CURRENCY
                                };

                                res.header('hash',hash.sha1Hmac(SECRET_KEY,'applyTransaction'+JSON.stringify(obj)) );
                                return res.send(obj);
                            }
                        });
                    });
                });
            }
        })

    });


});


const cancelBet = Joi.object().keys({
    playerId: Joi.string().required(),
    sessionId: Joi.string().required(),
    game: Joi.string().required(),
    gameRound: Joi.string().required(),
    description: Joi.string().required(),
});


router.post('/callback/rollback', function (req, res) {
    let validate = Joi.validate(req.body, cancelBet);
    if (validate.error) {
        return res.send({
            "errorCode": 102,
            "description": "Invalid Input.",
            "balance": 0,
            "currency": CURRENCY
        })
    }

    let requestBody = req.body;

    console.log('rollback ================ ', requestBody);

    let hisModel = new GanapatiHistoryModel(requestBody);
    hisModel.save((err, hisRes) => {
        if (err) {
            console.log('-------save GanapatiHistoryModel fail-------')
        } else {
            console.log('-------success-------')
            // callback(null, agentResponse);
        }
    });

    async.waterfall([callback => {

        MemberService.findById(requestBody.playerId, 'creditLimit balance limitSetting shareSetting commissionSetting group currency username_lower ipAddress', function (err, memberResponse) {
            if (err) {
                callback(err, null);
                return;
            }else {
                callback(null, memberResponse);
            }

        });

    }], (err, asyncResponse) => {

        let memberInfo = asyncResponse;
        let credit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2);

        let condition = {
            'memberId': mongoose.Types.ObjectId(requestBody.playerId), // may be have 1000 transaction
            'slot.ganapati.gameRound': requestBody.gameRound // // unique value for this member
        };


        BetTransactionModel.findOne(condition)
            .populate({path: 'memberParentGroup', select: 'type'})
            .exec(function (err, betObj) {

                if (!betObj) {
                    return res.send({
                        "errorCode": 101,
                        "description": "Internal Server Error.",
                        "balance": credit,
                        "currency": CURRENCY
                    })
                }

                if(betObj.status === 'CANCELLED'){
                    let obj = {
                        "balance": credit,
                        "currency": CURRENCY
                    };

                    res.header('hash',hash.sha1Hmac(SECRET_KEY,'rollback'+JSON.stringify(obj)) );
                    return res.send(obj);
                }

                let betAmount = roundTo(Number.parseFloat(betObj.amount), 2);
                let winLose = 0;

                if (betObj.betResult == WIN) {
                    winLose = betObj.commission.member.winLose - betAmount;
                } else if (betObj.betResult == LOSE) {
                    winLose = betObj.commission.member.winLose + betAmount;
                } else if (betObj.betResult == DRAW) {
                    winLose = betObj.commission.member.winLose;
                }

                let betResult = winLose > 0 ? WIN : winLose < 0 ? LOSE : DRAW;

                const shareReceive = AgentService.prepareShareReceive(winLose, betResult, betObj);


                let updateBody = {
                    'commission.superAdmin.winLoseCom': shareReceive.superAdmin.winLoseCom,
                    'commission.superAdmin.winLose': shareReceive.superAdmin.winLose,
                    'commission.superAdmin.totalWinLoseCom': shareReceive.superAdmin.totalWinLoseCom,

                    'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                    'commission.company.winLose': shareReceive.company.winLose,
                    'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                    'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                    'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                    'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                    'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                    'commission.senior.winLose': shareReceive.senior.winLose,
                    'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                    'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                    'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                    'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                    'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                    'commission.agent.winLose': shareReceive.agent.winLose,
                    'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                    'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                    'commission.member.winLose': shareReceive.member.winLose,
                    'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                    'betResult': betResult,
                    'status' : 'CANCELLED'

                };


                BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, cancelResponse) {
                    if(cancelResponse){
                        MemberService.updateBalance(memberInfo._id, betAmount, betObj.betId, 'GANAPATI_CANCEL', requestBody.gameRound, function (err, updateBalanceResponse) {
                            if(err){
                                return res.send({
                                    "errorCode": 101,
                                    "description": "Internal Server Error.",
                                    "balance": credit,
                                    "currency": CURRENCY
                                })
                            }else {
                                let obj = {
                                    "balance": updateBalanceResponse.newBalance,
                                    "currency": CURRENCY
                                };

                                res.header('hash',hash.sha1Hmac(SECRET_KEY,'rollback'+JSON.stringify(obj)) );
                                return res.send(obj);
                            }
                        })
                    }else {
                        return res.send({
                            "errorCode": 101,
                            "description": "Internal Server Error.",
                            "balance": credit,
                            "currency": CURRENCY
                        })
                    }

                });

            });


    });



});


function prepareCommission(memberInfo, body, currentAgent, overAgent, remaining, overAllShare) {

    if (currentAgent && (currentAgent.type === 'SUPER_ADMIN' || currentAgent.parentId)) {

        let money = body.amount;

        if (!overAgent) {
            overAgent = {};
            overAgent.type = 'member';
            overAgent.shareSetting = {};
            overAgent.shareSetting.game = {};
            overAgent.shareSetting.game.slotXO = {};
            overAgent.shareSetting.game.slotXO.parent = memberInfo.shareSetting.game.slotXO.parent;
            overAgent.shareSetting.game.slotXO.own = 0;
            overAgent.shareSetting.game.slotXO.remaining = 0;

            body.commission.member = {};
            body.commission.member.parent = memberInfo.shareSetting.game.slotXO.parent;
            body.commission.member.commission = memberInfo.commissionSetting.game.slotXO;

        }

        let currentPercentReceive = overAgent.shareSetting.game.slotXO.parent;
        let totalRemaining = remaining;

        if (overAgent.shareSetting.game.slotXO.remaining > 0) {

            if (overAgent.shareSetting.game.slotXO.remaining > totalRemaining) {
                currentPercentReceive += totalRemaining;
                totalRemaining = 0;
            } else {
                totalRemaining = remaining - overAgent.shareSetting.game.slotXO.remaining;
                currentPercentReceive += overAgent.shareSetting.game.slotXO.remaining;
            }

        }

        let unUseShare = currentAgent.shareSetting.game.slotXO.own -
            (overAgent.shareSetting.game.slotXO.parent + overAgent.shareSetting.game.slotXO.own);

        totalRemaining = (unUseShare + totalRemaining);

        if (currentAgent.shareSetting.game.slotXO.min > 0 && ((overAllShare + currentPercentReceive) < currentAgent.shareSetting.game.slotXO.min)) {

            if (currentPercentReceive < currentAgent.shareSetting.game.slotXO.min) {
                let percent = currentAgent.shareSetting.game.slotXO.min - (currentPercentReceive + overAllShare);
                totalRemaining = totalRemaining - percent;
                if (totalRemaining < 0) {
                    totalRemaining = 0;
                }
                currentPercentReceive += percent;
            }
        } else {
            // currentPercentReceive += totalRemaining;
        }


        if (currentAgent.type === 'SUPER_ADMIN') {
            currentPercentReceive += totalRemaining;
        }

        let getMoney = (money * NumberUtils.convertPercent(currentPercentReceive)).toFixed(2);
        overAllShare = overAllShare + currentPercentReceive;

        //set commission
        let agentCommission = currentAgent.commissionSetting.game.slotXO;


        switch (currentAgent.type) {
            case 'SUPER_ADMIN':
                body.commission.superAdmin = {};
                body.commission.superAdmin.group = currentAgent._id;
                body.commission.superAdmin.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.superAdmin.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.superAdmin.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.superAdmin.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.superAdmin.shareReceive = Math.abs(currentPercentReceive);
                body.commission.superAdmin.commission = agentCommission;
                body.commission.superAdmin.amount = getMoney;
                break;
            case 'COMPANY':
                body.commission.company = {};
                body.commission.company.parentGroup = currentAgent.parentId;
                body.commission.company.group = currentAgent._id;
                body.commission.company.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.company.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.company.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.company.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.company.shareReceive = Math.abs(currentPercentReceive);
                body.commission.company.commission = agentCommission;
                body.commission.company.amount = getMoney;
                break;
            case 'SHARE_HOLDER':
                body.commission.shareHolder = {};
                body.commission.shareHolder.parentGroup = currentAgent.parentId;
                body.commission.shareHolder.group = currentAgent._id;
                body.commission.shareHolder.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.shareHolder.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.shareHolder.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.shareHolder.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.shareHolder.shareReceive = currentPercentReceive;
                body.commission.shareHolder.commission = agentCommission;
                body.commission.shareHolder.amount = getMoney;
                break;
            case 'SENIOR':
                body.commission.senior = {};
                body.commission.senior.parentGroup = currentAgent.parentId;
                body.commission.senior.group = currentAgent._id;
                body.commission.senior.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.senior.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.senior.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.senior.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.senior.shareReceive = currentPercentReceive;
                body.commission.senior.commission = agentCommission;
                body.commission.senior.amount = getMoney;
                break;
            case 'MASTER_AGENT':
                body.commission.masterAgent = {};
                body.commission.masterAgent.parentGroup = currentAgent.parentId;
                body.commission.masterAgent.group = currentAgent._id;
                body.commission.masterAgent.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.masterAgent.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.masterAgent.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.masterAgent.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.masterAgent.shareReceive = currentPercentReceive;
                body.commission.masterAgent.commission = agentCommission;
                body.commission.masterAgent.amount = getMoney;
                break;
            case 'AGENT':
                body.commission.agent = {};
                body.commission.agent.parentGroup = currentAgent.parentId;
                body.commission.agent.group = currentAgent._id;
                body.commission.agent.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.agent.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.agent.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.agent.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.agent.shareReceive = currentPercentReceive;
                body.commission.agent.commission = agentCommission;
                body.commission.agent.amount = getMoney;
                break;
        }

        prepareCommission(memberInfo, body, currentAgent.parentId, currentAgent, totalRemaining, overAllShare);
    }
}


module.exports = router;