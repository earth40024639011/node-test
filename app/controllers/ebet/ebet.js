const express = require('express');
const router = express.Router();
const request = require('request');
const moment = require('moment');
const crypto = require('crypto');
const querystring = require('querystring');
const async = require("async");
const config = require('config');
const MemberService = require('../../common/memberService');
const Joi = require('joi');
const DateUtils = require('../../common/dateUtils');
const roundTo = require('round-to');
const mongoose = require('mongoose');
const _ = require('underscore');
const AgentGroupModel = require('../../models/agentGroup.model.js');
const NumberUtils = require('../../common/numberUtils1');
const AgentService = require('../../common/agentService');
const AllBetService = require('../../common/allbetService');
const MemberModel = require("../../models/member.model");

const BetTransactionModel = require('../../models/betTransaction.model.js');


const DRAW = "DRAW";
const WIN = "WIN";
const HALF_WIN = "HALF_WIN";
const LOSE = "LOSE";
const HALF_LOSE = "HALF_LOSE";


const API_URL = process.env.ALLBET_URL || 'https://platform-api.apidemo.net:8443/';
const AGENT_NAME = process.env.ALLBET_AGENT_NAME || 'fzdtaa';
const PROPERTY_ID = process.env.PROPERTY_ID || '8616817';


function generateBetId() {
    return 'BET' + new Date().getTime();
}


//PPP AMB


router.post('/test/encode', (req, res) => {
    console.log(req.body)
    const MD5_KEY = '6L84JhykIiBOBRAooVk+y1eZjevBIR7qmI/RatveGzM=';

    let data = AllBetService.encrypt(req.body.data);
    let sign = AllBetService.buildSignature("client=ok0029&propertyId=8616817&random=-1899059835&token=ccb5e3ab-5e49-4115-806f-e538323a5646");

    return res.send({error_code: 0, message: 'success', result: sign});


});

router.post('/callback/registerOrLogin', (req, res) => {
    console.log('registerOrLogin =========================>',req.body)

    res.send(
        { "accessToken": "eab0ec98d31f02cad6d738eab02a77f7",
            "subChannelId": 0,
            "username": "apitest01",
            "sessionToken": "8fb1c094308a69d4fe761f439a9eb515",
            "currency": "USD",
            "status": 200,
            "event": "registerOrLogin",
            "seqNo": "0ed434451657b3d01c0d397b195bfd71"}
    )


});

router.post('/callback/SyncCredit', (req, res) => {
    console.log('registerOrLogin =========================>',req.body)

    res.send(
        { "userName": "apitest01", "money": 1000,
            "status": 200,
            "event": "syncCredit",
            "seqNo": "22a0155b4ec82369f7edcd66a016b440"}
    )


});


function prepareCommission(memberInfo, body, currentAgent, overAgent, remaining, overAllShare) {


    if (currentAgent && (currentAgent.type === 'SUPER_ADMIN' || currentAgent.parentId)) {


        console.log('==============================================');
        console.log('process : ' + currentAgent.type + ' : ' + currentAgent.name);


        let money = body.amount;


        if (!overAgent) {
            overAgent = {};
            overAgent.type = 'member';
            overAgent.shareSetting = {};
            overAgent.shareSetting.casino = {};
            overAgent.shareSetting.casino.allbet = {};
            overAgent.shareSetting.casino.allbet.parent = memberInfo.shareSetting.casino.allbet.parent;
            overAgent.shareSetting.casino.allbet.own = 0;
            overAgent.shareSetting.casino.allbet.remaining = 0;

            body.commission.member = {};
            body.commission.member.parent = memberInfo.shareSetting.casino.allbet.parent;
            body.commission.member.commission = memberInfo.commissionSetting.casino.allbet;

        }

        console.log('');
        console.log('---- setting ----');
        console.log('ส่วนแบ่งที่ได้มามีทั้งหมด : ' + currentAgent.shareSetting.casino.allbet.own + ' %');
        console.log('ขอส่วนแบ่งจาก : ' + overAgent.type + ' ' + overAgent.shareSetting.casino.allbet.parent + ' %');
        console.log('ให้ส่วนแบ่ง : ' + overAgent.type + ' ที่เหลือไป ' + overAgent.shareSetting.casino.allbet.own + ' %');
        console.log('remaining จาก : ' + overAgent.type + ' : ' + overAgent.shareSetting.casino.allbet.remaining + ' %');
        console.log('โดนบังคับเสียขั้นต่ำ : ' + currentAgent.shareSetting.casino.allbet.min + ' %');
        console.log('---- end setting ----');
        console.log('');


        // console.log('และได้รับ remaining ไว้ : '+overAgent.shareSetting.casino.allbet.remaining+' %');

        let currentPercentReceive = overAgent.shareSetting.casino.allbet.parent;
        console.log('% ที่ได้ : ' + currentPercentReceive);
        console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');
        let totalRemaining = remaining;

        if (overAgent.shareSetting.casino.allbet.remaining > 0) {
            // console.log()
            // console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');

            if (overAgent.shareSetting.casino.allbet.remaining > totalRemaining) {
                console.log('รับ remaining ทั้งหมดไว้: ' + totalRemaining + ' %');
                currentPercentReceive += totalRemaining;
                totalRemaining = 0;
            } else {
                totalRemaining = remaining - overAgent.shareSetting.casino.allbet.remaining;
                console.log('หักค่า remaining ที่ได้รับมากับที่เซตรับไว้ = ' + remaining + ' - ' + overAgent.shareSetting.casino.allbet.remaining + ' = ' + totalRemaining);
                currentPercentReceive += overAgent.shareSetting.casino.allbet.remaining;
            }

        }

        console.log('รวม % ที่ได้รับ : ' + currentPercentReceive);


        let unUseShare = currentAgent.shareSetting.casino.allbet.own -
            (overAgent.shareSetting.casino.allbet.parent + overAgent.shareSetting.casino.allbet.own);

        console.log('เหลือส่วนแบ่งที่ไม่ได้ใช้ : ' + unUseShare + ' %');
        totalRemaining = (unUseShare + totalRemaining);
        console.log('ส่วนแบ่งที่ไม่ได้ใช้ บวกกับค่า remaining ท่ี่เหลือจะเป็น : ' + totalRemaining + ' %');

        console.log('จากที่โดนบังคับเสียขั้นต่ำ : ' + currentAgent.shareSetting.casino.allbet.min + ' %');
        console.log('overAllShare : ', overAllShare);
        if (currentAgent.shareSetting.casino.allbet.min > 0 && ((overAllShare + currentPercentReceive) < currentAgent.shareSetting.casino.allbet.min)) {

            if (currentPercentReceive < currentAgent.shareSetting.casino.allbet.min) {
                console.log('% ที่ได้รับ < ขั้นต่ำที่ถูกตั้ง min ไว้');
                let percent = currentAgent.shareSetting.casino.allbet.min - (currentPercentReceive + overAllShare);
                console.log('หักออกไป ' + percent + ' % : แล้วไปเพิ่มในส่วนที่ต้องรับผิดชอบ ');
                totalRemaining = totalRemaining - percent;
                if (totalRemaining < 0) {
                    totalRemaining = 0;
                }
                currentPercentReceive += percent;
            }
        } else {
            // currentPercentReceive += totalRemaining;
        }


        if (currentAgent.type === 'SUPER_ADMIN') {
            currentPercentReceive += totalRemaining;
        }

        let getMoney = (money * NumberUtils.convertPercent(currentPercentReceive)).toFixed(2);
        overAllShare = overAllShare + currentPercentReceive;

        console.log('ยอดแทง ' + money + ' ได้ทั้งหมด : ' + currentPercentReceive + ' %');

        console.log('รวม % + % remaining  จะเป็นสัดส่วนที่ได้ทั้งหมด = ' + getMoney);
        console.log('ค่าที่จะถูกคืนกลับไป (remaining) : ' + totalRemaining + ' %');

        console.log('จำนวน % ที่ถูกแบ่งไปแล้วทั้งหมด : ', overAllShare);

        //set commission
        let agentCommission = currentAgent.commissionSetting.casino.allbet;


        switch (currentAgent.type) {
            case 'SUPER_ADMIN':
                body.commission.superAdmin = {};
                body.commission.superAdmin.group = currentAgent._id;
                body.commission.superAdmin.parent = currentAgent.shareSetting.casino.allbet.parent;
                body.commission.superAdmin.own = currentAgent.shareSetting.casino.allbet.own;
                body.commission.superAdmin.remaining = currentAgent.shareSetting.casino.allbet.remaining;
                body.commission.superAdmin.min = currentAgent.shareSetting.casino.allbet.min;
                body.commission.superAdmin.shareReceive = Math.abs(currentPercentReceive);
                body.commission.superAdmin.commission = agentCommission;
                body.commission.superAdmin.amount = getMoney;
                break;
            case 'COMPANY':
                body.commission.company = {};
                body.commission.company.parentGroup = currentAgent.parentId;
                body.commission.company.group = currentAgent._id;
                body.commission.company.parent = currentAgent.shareSetting.casino.allbet.parent;
                body.commission.company.own = currentAgent.shareSetting.casino.allbet.own;
                body.commission.company.remaining = currentAgent.shareSetting.casino.allbet.remaining;
                body.commission.company.min = currentAgent.shareSetting.casino.allbet.min;
                body.commission.company.shareReceive = Math.abs(currentPercentReceive);
                body.commission.company.commission = agentCommission;
                body.commission.company.amount = getMoney;
                break;
            case 'SHARE_HOLDER':
                body.commission.shareHolder = {};
                body.commission.shareHolder.parentGroup = currentAgent.parentId;
                body.commission.shareHolder.group = currentAgent._id;
                body.commission.shareHolder.parent = currentAgent.shareSetting.casino.allbet.parent;
                body.commission.shareHolder.own = currentAgent.shareSetting.casino.allbet.own;
                body.commission.shareHolder.remaining = currentAgent.shareSetting.casino.allbet.remaining;
                body.commission.shareHolder.min = currentAgent.shareSetting.casino.allbet.min;
                body.commission.shareHolder.shareReceive = currentPercentReceive;
                body.commission.shareHolder.commission = agentCommission;
                body.commission.shareHolder.amount = getMoney;
                break;
            case 'SENIOR':
                body.commission.senior = {};
                body.commission.senior.parentGroup = currentAgent.parentId;
                body.commission.senior.group = currentAgent._id;
                body.commission.senior.parent = currentAgent.shareSetting.casino.allbet.parent;
                body.commission.senior.own = currentAgent.shareSetting.casino.allbet.own;
                body.commission.senior.remaining = currentAgent.shareSetting.casino.allbet.remaining;
                body.commission.senior.min = currentAgent.shareSetting.casino.allbet.min;
                body.commission.senior.shareReceive = currentPercentReceive;
                body.commission.senior.commission = agentCommission;
                body.commission.senior.amount = getMoney;
                break;
            case 'MASTER_AGENT':
                body.commission.masterAgent = {};
                body.commission.masterAgent.parentGroup = currentAgent.parentId;
                body.commission.masterAgent.group = currentAgent._id;
                body.commission.masterAgent.parent = currentAgent.shareSetting.casino.allbet.parent;
                body.commission.masterAgent.own = currentAgent.shareSetting.casino.allbet.own;
                body.commission.masterAgent.remaining = currentAgent.shareSetting.casino.allbet.remaining;
                body.commission.masterAgent.min = currentAgent.shareSetting.casino.allbet.min;
                body.commission.masterAgent.shareReceive = currentPercentReceive;
                body.commission.masterAgent.commission = agentCommission;
                body.commission.masterAgent.amount = getMoney;
                break;
            case 'AGENT':
                body.commission.agent = {};
                body.commission.agent.parentGroup = currentAgent.parentId;
                body.commission.agent.group = currentAgent._id;
                body.commission.agent.parent = currentAgent.shareSetting.casino.allbet.parent;
                body.commission.agent.own = currentAgent.shareSetting.casino.allbet.own;
                body.commission.agent.remaining = currentAgent.shareSetting.casino.allbet.remaining;
                body.commission.agent.min = currentAgent.shareSetting.casino.allbet.min;
                body.commission.agent.shareReceive = currentPercentReceive;
                body.commission.agent.commission = agentCommission;
                body.commission.agent.amount = getMoney;
                break;
        }

        prepareCommission(memberInfo, body, currentAgent.parentId, currentAgent, totalRemaining, overAllShare);
    }
}


function updateAgentMemberBalance(shareReceive, betAmount, ref, updateCallback) {

    console.log('updateAgentMemberBalance');
    console.log('shareReceive : ', shareReceive);

    let updateWinLoseAgentList = [];

    updateWinLoseAgentList.push({
        group: shareReceive.superAdmin.group,
        amount: roundTo(shareReceive.superAdmin.totalWinLoseCom, 3)
    });
    updateWinLoseAgentList.push({
        group: shareReceive.company.group,
        amount: roundTo((shareReceive.superAdmin.totalWinLoseCom * -1 ), 3)
    });
    updateWinLoseAgentList.push({
        group: shareReceive.shareHolder.group,
        amount: roundTo((shareReceive.superAdmin.totalWinLoseCom * -1 ) + (shareReceive.company.totalWinLoseCom * -1), 3)
    });
    updateWinLoseAgentList.push({
        group: shareReceive.senior.group,
        amount: roundTo((shareReceive.superAdmin.totalWinLoseCom * -1 ) + (shareReceive.company.totalWinLoseCom * -1 ) + (shareReceive.shareHolder.totalWinLoseCom * -1), 3)
    });
    updateWinLoseAgentList.push({
        group: shareReceive.masterAgent.group,
        amount: roundTo((shareReceive.superAdmin.totalWinLoseCom * -1 ) + (shareReceive.company.totalWinLoseCom * -1 ) + (shareReceive.shareHolder.totalWinLoseCom * -1) + (shareReceive.senior.totalWinLoseCom * -1), 3)
    });
    updateWinLoseAgentList.push({
        group: shareReceive.agent.group,
        amount: roundTo((shareReceive.superAdmin.totalWinLoseCom * -1 ) + (shareReceive.company.totalWinLoseCom * -1 ) + (shareReceive.shareHolder.totalWinLoseCom * -1) + (shareReceive.senior.totalWinLoseCom * -1) + (shareReceive.masterAgent.totalWinLoseCom * -1), 3)
    });


    updateAgentBalance(updateWinLoseAgentList);

    function updateAgentBalance(lists) {

        if (lists.length == 0) {
            let balance = betAmount + shareReceive.member.totalWinLoseCom;

            try {
                console.log('update member balance : ', balance);

                MemberService.updateBalance(shareReceive.member.id, balance, shareReceive.betId, 'AB_SETTLE', ref, function (err, updateBalanceResponse) {
                    if (err) {
                        updateCallback(err, null);
                    } else {
                        updateCallback(null, updateBalanceResponse);
                    }
                });
            } catch (err) {
                MemberService.updateBalance(shareReceive.member.id, balance, shareReceive.betId, 'AB_SETTLE_ERROR ' + shareReceive.member.id + ' : ' + balance, ref, function (err, updateBalanceResponse) {
                    if (err) {
                        updateCallback(err, null);
                    } else {
                        updateCallback(null, updateBalanceResponse);
                    }
                });
            }
        } else {

            let item = lists.pop();

            console.log('item : ', item);
            if (item.group && item.amount !== 0) {

                AgentGroupModel.findOneAndUpdate({_id: item.group}, {$inc: {balance: item.amount}})
                    .exec(function (err, creditResponse) {
                        console.log('group : ' + item.group + ' , balance : ' + item.amount);
                        updateAgentBalance(lists);

                    });
            } else {
                updateAgentBalance(lists);
            }
        }
    }
}


function saveAllBetTransaction(request, callback) {
    let allBetModel = new AllBetTransactionModel(
        {
            propertyId: request.propertyId,
            token: request.token,
            random: request.random,
            tranId: request.tranId,
            tranType: request.tranType,
            betNum: request.betNum,
            client: request.client,
            betAmount: request.betAmount,
            betType: request.betType,
            betTime: request.betTime,
            gameType: request.gameType,
            gameRoundId: request.gameRoundId,
            commission: request.commission,
            ip: request.ip,
            tableName: request.tableName,
            enterType: request.enterType,
            createdDate: DateUtils.getCurrentDate(),
        }
    );

    allBetModel.save((err, response) => {
        console.log('save allbet transaction')
        callback(null, 'success');
    });
}

function saveAllBetSettleTransaction(request, callback) {

    let allBetModel = new AllBetSettleTransactionModel(
        {
            propertyId: request.propertyId,
            random: request.random,
            tranId: request.tranId,
            tranType: request.tranType,
            betNum: request.betNum,
            client: request.client,
            betAmount: request.betAmount,
            betType: request.betType,
            betTime: request.betTime,
            gameType: request.gameType,
            gameRoundId: request.gameRoundId,
            state: request.state,
            gameResult: request.gameResult,
            gameResult2: request.gameResult2,
            validAmount: request.validAmount,
            winOrLoss: request.winOrLoss,
            currency: request.currency,
            exchangeRate: request.exchangeRate,
            gameRoundEndTime: request.gameRoundEndTime,
            gameRoundStartTime: request.gameRoundStartTime,
            tableName: request.tableName,
            commission: request.commission,
            createdDate: DateUtils.getCurrentDate(),
        }
    );

    console.log(allBetModel)

    allBetModel.save((err, response) => {
        console.log(err)
        console.log('save allbet settle transaction')
        callback(null, 'success');
    });
}


module.exports = router;
