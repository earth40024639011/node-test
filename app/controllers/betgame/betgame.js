const express = require('express');
const router = express.Router();
const request = require('request');
const moment = require('moment');
const crypto = require('crypto');
const querystring = require('querystring');
const async = require("async");
const config = require('config');
const MemberService = require('../../common/memberService');
const Joi = require('joi');
const DateUtils = require('../../common/dateUtils');
const roundTo = require('round-to');
const mongoose = require('mongoose');
const _ = require('underscore');
const AgentGroupModel = require('../../models/agentGroup.model.js');
const NumberUtils = require('../../common/numberUtils1');
const AgentService = require('../../common/agentService');
const AgGamingService = require('../../common/aggameService');
const MemberModel = require("../../models/member.model");
const AgTransaction = require("../../models/agTransaction.model");

const xml2js = require('xml2js');

const SaTransactionModel = require('../../models/saTransaction.model.js');
const BetTransactionModel = require('../../models/betTransaction.model.js');


const DRAW = "DRAW";
const WIN = "WIN";
const HALF_WIN = "HALF_WIN";
const LOSE = "LOSE";
const HALF_LOSE = "HALF_LOSE";

const CLIENT_NAME        = process.env.CLIENT_NAME || 'SPORTBOOK88';
let currency = 'THB';

if(CLIENT_NAME === 'SPORTBOOK88'){
    currency = 'CNY';
}


function random(low, high) {
    return Math.floor(Math.random() * (high - low + 1) + low)
}

function generateBetId() {
    return 'BET' + new Date().getTime() + random(1000000, 9999999);
}

function generateSid() {
    return 'EB7_AGIN' + new Date().getTime() + random(10, 99);
}

function jsonToAgGameResponseFormat(json) {
    var builder = new xml2js.Builder();
    return builder.buildObject({
        root: json
    });
}

router.post('/callback', (req, res) => {

    console.log('==================bet game===================');

    console.log('req ==================', req.body);

    return res.send(
        jsonToAgGameResponseFormat({
            method: 'ping',
            token: '-',
            success: 1,
            error_code: 0,
            error_text: '',
            time: 1423124663,
            params: '',
            signature: 'dee0dda6b4adc6c4e0f67c7e19a3ad0b',
        })
    );


});


//2.1. token_forward_game
// router.get('/login', (req, res) => {
//
//     // console.log(req.query);
//
//     let userInfo = req.userInfo;
//
//     async.waterfall([callback => {
//         try {
//             AgentService.getUpLineStatus(userInfo.group._id, function (err, status) {
//                 if (err) {
//                     callback(err, null);
//                 } else {
//                     if (status.suspend) {
//                         callback(5011, null);
//                     } else {
//                         callback(null, status);
//                     }
//                 }
//             });
//         } catch (err) {
//             callback(9929, null);
//         }
//
//     }, (uplineStatus, callback) => {
//
//         try {
//             MemberService.findById(userInfo._id, 'creditLimit balance limitSetting group suspend username_lower', function (err, memberResponse) {
//                 if (err) {
//                     callback(err, null);
//                 } else {
//
//                     if (memberResponse.suspend) {
//                         callback(5011, null);
//                     } else if (memberResponse.limitSetting.casino.ag.isEnable) {
//                         AgentService.getAGGameStatus(memberResponse.group._id, (err, response) => {
//                             if (!response.isEnable) {
//                                 callback(1088, null);
//                             } else {
//                                 callback(null, uplineStatus, memberResponse);
//                             }
//                         });
//                     } else {
//                         callback(1088, null);
//                     }
//                 }
//             });
//         } catch (err) {
//             callback(999, null);
//         }
//
//     }], function (err, uplineStatus, memberInfo) {
//
//         if (err) {
//             if (err == 1088) {
//                 return res.send({
//                     code: 1088,
//                     message: 'Service Locked, Please contact your upline.'
//                 });
//             }
//             if (err == 5011) {
//                 return res.send({
//                     message: "Account or Upline was suspend.",
//                     result: null,
//                     code: 5011
//                 });
//             }
//             return res.send({code: 999, message: 'upline fail'});
//         }
//
//         let client = req.query.client;
//         let oddType = AgGamingService.getLimit(memberInfo.limitSetting.casino.ag.limit);
//         let sid = generateSid();
//
//         // return res.send({code: 999, message: memberInfo});
//         AgGamingService.callLoginRequest(memberInfo.username_lower, req.query.prefix, oddType, currency, (err, loginResponse) => {
//             if (err) {
//                 return res.send({message: 'Login Error', code: 999});
//             } else if (loginResponse === 0 || loginResponse === '0') {
//
//                 let balance = roundTo(roundTo(memberInfo.creditLimit, 2) + roundTo(memberInfo.balance, 2), 2);
//
//                 AgGamingService.callCreateGameSession(memberInfo.username_lower, balance, userInfo.uid, req.query.prefix, (err, response) => {
//
//                     console.log('---------transfer credit---------', response);
//                     if (err) {
//                         return res.send({message: 'Transfer Credit Fail', code: 999});
//                     } else {
//
//                         console.log('--------oddtype--------',oddType);
//                         let url = AgGamingService.callLaunchingLiveGameURL(memberInfo.username_lower, sid, req.query.gameId, req.query.client, oddType, currency, req.query.website, req.query.prefix);
//
//                         return res.send({message: 'success', code: 0, url: url});
//
//                     }
//
//                 });
//             } else {
//                 console.log('response == ', loginResponse);
//                 return res.send({message: 'Login Fail', code: 999});
//             }
//         });
//
//     });
//
//
// });
//
// router.get('/rest/checkStatus', (req, res) => {
//     console.log('params ====== ', req.params);
//
//     return res.send({
//         code: 0
//     })
// });
//
// router.post('/rest/integration/postTransfer', (req, res) => {
//
//     console.log('==================postTransfer===================');
//
//     console.log('req ==================', req.body.data.record[0]);
//
//
//     const requestXML = req.body.data.record[0];
//
//     // console.log(requestXML)
//
//     if (requestXML.transactiontype[0] === 'BET') {
//
//         let requestBody = {
//             sessiontoken: requestXML.sessiontoken[0],
//             currency:requestXML.currency[0],
//             value:requestXML.value[0],
//             playname: requestXML.playname[0],
//             agentcode: requestXML.agentcode[0],
//             bettime: requestXML.bettime[0],
//             transactionid: requestXML.transactionid[0],
//             platformtype: requestXML.platformtype[0],
//             round:requestXML.round[0],
//             gametype:requestXML.gametype[0],
//             gamecode: requestXML.gamecode[0],
//             tablecode:requestXML.tablecode[0],
//             transactiontype: (requestXML.transactiontype && requestXML.transactiontype[0]) ? requestXML.transactiontype[0] : '',
//             transactioncode: (requestXML.transactioncode && requestXML.transactioncode[0]) ? requestXML.transactioncode[0] : '',
//             devicetype:requestXML.devicetype[0],
//             playtype:requestXML.playtype[0]
//
//         };
//
//         betFunction(requestBody, res);
//
//     } else if (requestXML.transactiontype[0] === 'REFUND') {
//
//         let requestBody = {
//             ticketstatus: (requestXML.ticketstatus && requestXML.ticketstatus[0]) ? requestXML.ticketstatus[0] : '',
//             sessiontoken: requestXML.sessiontoken[0],
//             currency:requestXML.currency[0],
//             value:requestXML.value[0],
//             playname: requestXML.playname[0],
//             agentcode: requestXML.agentcode[0],
//             bettime: requestXML.bettime[0],
//             transactionid: requestXML.transactionid[0],
//             platformtype: requestXML.platformtype[0],
//             round:requestXML.round[0],
//             gametype:requestXML.gametype[0],
//             gamecode: requestXML.gamecode[0],
//             tablecode:requestXML.tablecode[0],
//             transactiontype: (requestXML.transactiontype && requestXML.transactiontype[0]) ? requestXML.transactiontype[0] : '',
//             transactioncode: (requestXML.transactioncode && requestXML.transactioncode[0]) ? requestXML.transactioncode[0] : '',
//             playtype : requestXML.playtype[0]
//         };
//
//         cancelBetFunction(requestBody, res);
//
//     } else if (requestXML.transactiontype[0] === 'WIN' || requestXML.transactiontype[0] === 'LOSE') {
//
//         let requestBody = {
//             sessiontoken: requestXML.sessiontoken[0],
//             currency:requestXML.currency[0],
//             netamount:requestXML.netamount[0],
//             validbetamount:requestXML.validbetamount[0],
//             playname: requestXML.playname[0],
//             agentcode: requestXML.agentcode[0],
//             settletime: requestXML.settletime[0],
//             transactionid: requestXML.transactionid,
//             billno: (requestXML.billno && requestXML.billno[0])? requestXML.billno[0]: '',
//             gametype:requestXML.gametype[0],
//             gamecode: requestXML.gamecode[0],
//             transactiontype: (requestXML.transactiontype && requestXML.transactiontype[0]) ? requestXML.transactiontype[0] : '',
//             transactioncode: (requestXML.transactioncode && requestXML.transactioncode[0]) ? requestXML.transactioncode[0] : '',
//             ticketstatus: (requestXML.ticketstatus && requestXML.ticketstatus[0]) ? requestXML.ticketstatus[0] : '',
//             gameresult: requestXML.gameresult[0],
//             playtype: (requestXML.playtype && requestXML.playtype[0]) ? requestXML.playtype[0] : '',
//             finish:requestXML.finish[0]
//         };
//
//         settleFunction(requestBody, res)
//
//     } else {
//
//         return res.send(
//             jsonToAgGameResponseFormat({
//                 ResponseCode: 'ERROR'
//             })
//         )
//     }
//
//
// });
//
//
// function betFunction(requestBody, res) {
//     console.log('--------------------------------BET----------------------------------')
//     saveAgTransactionLog(requestBody, 'BET', (err, saveAllBetTransResponse) => {
//         if (err) {
//             console.log('-----------------saveAgTransactionLog Bet fail----------------')
//         }
//     });
//
//
//     const betAmount = roundTo(Number.parseFloat(requestBody.value), 2);
//
//     async.waterfall([callback => {
//
//         AgGamingService.checkSessionToken(requestBody.sessiontoken, (err, result) => {
//             if (result) {
//                 if (requestBody.playname.indexOf(result.username) < 0) {
//                     return res.send(
//                         jsonToAgGameResponseFormat({
//                             ResponseCode: 'INCORRECT_SESSION_TYPE'
//                         })
//                     )
//                 } else {
//                     callback(null, result.username);
//                 }
//
//             } else {
//                 return res.send(
//                     jsonToAgGameResponseFormat({
//                         ResponseCode: 'ERROR'
//                     })
//                 )
//             }
//         })
//
//     }, (username, callback) => {
//         MemberService.findByUserNameForPartnerService(username, function (err, memberResponse) {
//             if (err) {
//                 callback(err, null);
//                 return;
//             }
//
//             if (memberResponse) {
//
//                 let credit = roundTo((memberResponse.creditLimit + memberResponse.balance), 2);
//                 if (credit < betAmount) {
//                     return res.send(
//                         jsonToAgGameResponseFormat({
//                             ResponseCode: 'INSUFFICIENT_CLEARED_FUNDS'
//                         })
//                     )
//                 } else {
//                     callback(null, memberResponse);
//                 }
//             } else {
//                 return res.send(
//                     jsonToAgGameResponseFormat({
//                         ResponseCode: 'INSUFFICIENT_FUNDS'
//                     })
//                 )
//             }
//         });
//
//     }, (memberInfo, callback) => {
//
//         // console.log('isWlEnable : ',memberInfo.limitSetting.casino.isWlEnable);
//         if (memberInfo.limitSetting.casino.isWlEnable) {
//
//
//             getTodayWinLoss(memberInfo._id, (err, wlAmt) => {
//                 if (err) {
//                     callback(err, null);
//                 } else {
//
//
//                     if (wlAmt > memberInfo.limitSetting.casino.winPerDay) {
//                         callback(666, null);
//                     } else {
//                         callback(null, memberInfo, wlAmt);
//                     }
//
//                 }
//             });
//
//         } else {
//             callback(null, memberInfo, 0);
//         }
//
//     }], (err, memberInfo, winLossAmt) => {
//
//         if (err) {
//             if (err === 666) {
//                 jsonToAgGameResponseFormat({
//                     ResponseCode: 'INSUFFICIENT_CLEARED_FUNDS'
//                 })
//             }
//
//             return res.send(
//                 jsonToAgGameResponseFormat({
//                     ResponseCode: 'INVALID_DATA'
//                 })
//             )
//         }
//
//         let condition = {
//             'memberId': mongoose.Types.ObjectId(memberInfo._id),
//             'game': 'CASINO',
//             'source': 'AG_GAME',
//             'baccarat.ag.gameId': requestBody.gamecode,
//             // 'baccarat.ag.playType': AgGamingService.getPlayType(requestBody.playtype)
//         };
//
//
//         BetTransactionModel.findOne(condition, (err, response) => {
//
//             if (response) {
//                 //---------------------Update BetTransactionModel-------------------------
//
//                 let sameTransactionId = _.filter(response.baccarat.ag.txns, item => {
//                     return item.txnId === requestBody.transactionid;
//                 });
//
//                 // console.log('sameTxId====================== ', sameTxId);
//
//                 if (sameTransactionId.length > 0) {
//                     return res.send(
//                         jsonToAgGameResponseFormat({
//                             ResponseCode: 'INVALID_DATA'
//                         })
//                     )
//                 }
//
//
//                 let update = {
//                     $inc: {
//                         'amount': betAmount,
//                         "memberCredit": (betAmount * -1),
//                         "payout": betAmount,
//                         // "baccarat.ag.amountDummy": betAmount
//                     },
//                     $push: {
//                         'baccarat.ag.txns': {
//                             txnId: requestBody.transactionid,
//                             playType: AgGamingService.getPlayType(requestBody.playtype),
//                             betAmount: betAmount
//                         }
//                     }
//                 };
//
//                 MemberService.updateBalance(memberInfo._id, (requestBody.value * -1), response.betId, 'AG_BET', requestBody.transactionid, function (err, updateBalanceResponse) {
//                     if (err) {
//                         return res.send(
//                             jsonToAgGameResponseFormat({
//                                 ResponseCode: 'INVALID_TRANSACTION'
//                             })
//                         )
//                     } else {
//
//                         BetTransactionModel.update({_id: response._id}, update, (err, updateTransResponse) => {
//                             if (err) {
//                                 return res.send(
//                                     jsonToAgGameResponseFormat({
//                                         ResponseCode: 'INVALID_TRANSACTION'
//                                     })
//                                 )
//                             } else {
//                                 return res.send(
//                                     jsonToAgGameResponseFormat({
//                                         ResponseCode: 'OK',
//                                         Balance: updateBalanceResponse.newBalance
//                                     })
//                                 );
//                             }
//
//                         });
//                     }
//                 });
//
//
//             } else {
//                 //---------------------Create BetTransactionModel-------------------------
//                 let body = {
//                     memberId: memberInfo._id,
//                     memberParentGroup: memberInfo.group,
//                     memberUsername: memberInfo.username_lower,
//                     betId: generateBetId(),
//                     baccarat: {
//                         ag: {
//                             gameId: requestBody.gamecode,
//                             gameType: AgGamingService.getGameType(requestBody.gametype),
//                             hostId: AgGamingService.getRound(requestBody.round),
//                             betTime: convertDateToCurrentTime(requestBody.bettime),
//                             txns: [{
//                                 txnId: requestBody.transactionid,
//                                 playType: AgGamingService.getPlayType(requestBody.playtype),
//                                 betAmount: betAmount
//                             }]
//                         }
//                     },
//                     amount: betAmount,
//                     memberCredit: betAmount * -1,
//                     payout: betAmount,
//                     gameType: 'TODAY',
//                     acceptHigherPrice: false,
//                     priceType: 'TH',
//                     game: 'CASINO',
//                     source: 'AG_GAME',
//                     status: 'RUNNING',
//                     commission: {},
//                     gameDate: DateUtils.getCurrentDate(),
//                     createdDate: DateUtils.getCurrentDate(),
//                     isEndScore: false,
//                     currency: memberInfo.currency
//                 };
//
//                 console.log('body========= ',body);
//
//                 let betTransactionModel = new BetTransactionModel(body);
//
//                 betTransactionModel.save((err, response) => {
//
//                     if (err) {
//                         return res.send(jsonToAgGameResponseFormat({
//                             ResponseCode: 'ERROR'
//                         }));
//                     }
//
//                     MemberService.updateBalance(memberInfo._id, (betAmount * -1), response.betId, 'AG_BET', requestBody.transactionid, function (err, updateBalanceResponse) {
//                         // console.log(' ===new balance== ',updateBalanceResponse);
//                         if (err) {
//                             if (err == 888) {
//                                 BetTransactionModel.remove({_id: response._id}, function (err, removeResult) {
//                                     // console.log('betId : ', response.betId)
//                                     console.log(removeResult)
//                                 });
//                                 return res.send(jsonToAgGameResponseFormat({
//                                     ResponseCode: 'ERROR'
//                                 }));
//                             } else {
//                                 return res.send(jsonToAgGameResponseFormat({
//                                     ResponseCode: 'ERROR'
//                                 }));
//                             }
//
//
//                         } else {
//
//                             return res.send(
//                                 jsonToAgGameResponseFormat({
//                                     ResponseCode: 'OK',
//                                     Balance: updateBalanceResponse.newBalance
//                                 })
//                             );
//
//
//                         }
//                     });
//                 });
//             }
//
//         });
//
//
//     });
// }
//
//     function settleFunction(requestBody, res) {
//     // Settle
//     console.log('--------------------------------SETTLE----------------------------------')
//     saveAgTransactionLog(requestBody, 'SETTLE', (err, saveAllBetTransResponse) => {
//         if (err) {
//             console.log('-----------------saveAgTransactionLog fail----------------')
//         }
//     });
//
//     async.waterfall([callback => {
//
//         AgGamingService.checkSessionToken(requestBody.sessiontoken, (err, result) => {
//             if (result) {
//                 if (requestBody.playname.indexOf(result.username) < 0) {
//                     return res.send(
//                         jsonToAgGameResponseFormat({
//                             ResponseCode: 'INCORRECT_SESSION_TYPE'
//                         })
//                     )
//                 } else {
//                     callback(null, result.username);
//                 }
//
//             } else {
//                 return res.send(
//                     jsonToAgGameResponseFormat({
//                         ResponseCode: 'ERROR'
//                     })
//                 )
//             }
//         })
//
//     }, (username, callback) => {
//
//         MemberService.findByUserNameForPartnerService(username, function (err, memberResponse) {
//             if (err) {
//                 callback(err, null);
//                 return;
//             }
//             if (memberResponse) {
//                 callback(null, memberResponse);
//             } else {
//                 callback(1004, null);
//             }
//         });
//
//     }], (err, asyncResponse) => {
//
//         if (err) {
//             return res.send(
//                 jsonToAgGameResponseFormat({
//                     ResponseCode: 'INVALID_DATA'
//                 })
//             )
//         }
//
//         let memberInfo = asyncResponse;
//
//         let condition = {
//             'memberId': mongoose.Types.ObjectId(memberInfo._id),
//             'game': 'CASINO',
//             'source': 'AG_GAME',
//             'baccarat.ag.gameId': requestBody.gamecode,
//             // 'baccarat.ag.txns.txnId': { $in: requestBody.transactionid }
//         };
//
//         // console.log('condition======= ',condition);
//
//
//         BetTransactionModel.findOne(condition)
//             .populate({path: 'memberParentGroup', select: 'type'})
//             .exec(function (err, betObj) {
//
//                 // console.log('bet obj =============== ',betObj);
//
//                 if (!betObj) {
//                     return res.send(
//                         jsonToAgGameResponseFormat({
//                             ResponseCode: 'INVALID_TRANSACTION'
//                         })
//                     );
//                 }
//
//                 if(requestBody.finish === 'false'){
//
//                     betObj.baccarat.ag.winLose = (betObj.baccarat.ag.winLose ? betObj.baccarat.ag.winLose : 0) + roundTo(Number.parseFloat(requestBody.netamount), 2);
//                     betObj.baccarat.ag.validBetAmount = (betObj.baccarat.ag.validBetAmount ? betObj.baccarat.ag.validBetAmount : 0) + roundTo(Number.parseFloat(requestBody.validbetamount), 2);
//
//                     let playType = '';
//                     betObj.baccarat.ag.txns.forEach((data) => {
//                         if(requestBody.transactionid[0] === data.txnId){
//                             playType = data.playType ? data.playType : '';
//                         }
//                     });
//
//                     let updateBody = {
//                         $set: {
//                             'baccarat.ag.winLose': betObj.baccarat.ag.winLose,
//                             'baccarat.ag.validBetAmount': betObj.baccarat.ag.validBetAmount
//                         },
//                         $push: {
//                             'baccarat.ag.resultTransaction': {
//                                 transactionId: requestBody.transactionid,
//                                 playType: playType ? playType : AgGamingService.getPlayType(requestBody.playtype),
//                                 netAmount: roundTo(Number.parseFloat(requestBody.netamount), 2),
//                                 validAmount: roundTo(Number.parseFloat(requestBody.validbetamount), 2),
//                                 billNo: requestBody.billno,
//                                 isFinish: requestBody.finish
//                             }
//                         }
//                     };
//
//                     // console.log('-------------BetTransactionModel--------------', updateBody);
//
//                     BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
//
//                         if (err) {
//                             return res.send(
//                                 jsonToAgGameResponseFormat({
//                                     ResponseCode: 'ERROR'
//                                 })
//                             );
//                         } else {
//                             let credit = roundTo(memberInfo.creditLimit + memberInfo.balance, 2);
//                             return res.send(
//                                 jsonToAgGameResponseFormat({
//                                     ResponseCode: 'OK',
//                                     Balance: credit
//                                 })
//                             );
//                         }
//                     });
//                 }else if (requestBody.finish === 'true' && betObj.status === 'DONE') {
//
//                     MemberService.getCredit(memberInfo._id, (err, credit) => {
//                         return res.send(
//                             jsonToAgGameResponseFormat({
//                                 ResponseCode: 'OK',
//                                 Balance: credit
//                             })
//                         );
//                     });
//
//                 } else {
//
//                     AgentGroupModel.findById(memberInfo.group).select('_id type parentId shareSetting commissionSetting').populate({
//                         path: 'parentId',
//                         select: '_id type parentId shareSetting commissionSetting name',
//                         populate: {
//                             path: 'parentId',
//                             select: '_id type parentId shareSetting commissionSetting name',
//                             populate: {
//                                 path: 'parentId',
//                                 select: '_id type parentId shareSetting commissionSetting name',
//                                 populate: {
//                                     path: 'parentId',
//                                     select: '_id type parentId shareSetting commissionSetting name',
//                                     populate: {
//                                         path: 'parentId',
//                                         select: '_id type parentId shareSetting commissionSetting name',
//                                         populate: {
//                                             path: 'parentId',
//                                             select: '_id type parentId shareSetting commissionSetting name',
//                                         }
//                                     }
//                                 }
//                             }
//                         }
//
//                     }).exec(function (err, agentGroups) {
//
//                         let betAmount = betObj.amount;
//                         // let resultCash = (betObj.baccarat.ag.winLose ? betObj.baccarat.ag.winLose : 0) + roundTo(Number.parseFloat(requestBody.netamount), 2);
//                         let winLose = (betObj.baccarat.ag.winLose ? betObj.baccarat.ag.winLose : 0) + roundTo(Number.parseFloat(requestBody.netamount), 2);
//                         let validBetAmount = (betObj.baccarat.ag.validBetAmount ? betObj.baccarat.ag.validBetAmount : 0) + roundTo(Number.parseFloat(requestBody.validbetamount), 2);
//                         let betResult = winLose > 0 ? WIN : winLose < 0 ? LOSE : DRAW;
//
//                         console.log(validBetAmount, '------------betAmount--------------', betAmount);
//
//                         if(validBetAmount != betAmount){
//                             return res.send(
//                                 jsonToAgGameResponseFormat({
//                                     ResponseCode: 'ERROR'
//                                 })
//                             );
//                         }
//
//                         console.log('winlose ============================', winLose);
//
//                         let body = betObj;
//
//                         prepareCommission(memberInfo, body, agentGroups, null, 0, 0);
//
//                         //init
//                         if (!body.commission.senior) {
//                             body.commission.senior = {};
//                         }
//
//                         if (!body.commission.masterAgent) {
//                             body.commission.masterAgent = {};
//                         }
//
//                         if (!body.commission.agent) {
//                             body.commission.agent = {};
//                         }
//
//                         const shareReceive = AgentService.prepareShareReceive(winLose, betResult, betObj);
//
//                         body.commission.superAdmin.winLoseCom = shareReceive.superAdmin.winLoseCom;
//                         body.commission.superAdmin.winLose = shareReceive.superAdmin.winLose;
//                         body.commission.superAdmin.totalWinLoseCom = shareReceive.superAdmin.totalWinLoseCom;
//
//                         body.commission.company.winLoseCom = shareReceive.company.winLoseCom;
//                         body.commission.company.winLose = shareReceive.company.winLose;
//                         body.commission.company.totalWinLoseCom = shareReceive.company.totalWinLoseCom;
//
//                         body.commission.shareHolder.winLoseCom = shareReceive.shareHolder.winLoseCom;
//                         body.commission.shareHolder.winLose = shareReceive.shareHolder.winLose;
//                         body.commission.shareHolder.totalWinLoseCom = shareReceive.shareHolder.totalWinLoseCom;
//
//                         body.commission.senior.winLoseCom = shareReceive.senior.winLoseCom;
//                         body.commission.senior.winLose = shareReceive.senior.winLose;
//                         body.commission.senior.totalWinLoseCom = shareReceive.senior.totalWinLoseCom;
//
//                         body.commission.masterAgent.winLoseCom = shareReceive.masterAgent.winLoseCom;
//                         body.commission.masterAgent.winLose = shareReceive.masterAgent.winLose;
//                         body.commission.masterAgent.totalWinLoseCom = shareReceive.masterAgent.totalWinLoseCom;
//
//                         body.commission.agent.winLoseCom = shareReceive.agent.winLoseCom;
//                         body.commission.agent.winLose = shareReceive.agent.winLose;
//                         body.commission.agent.totalWinLoseCom = shareReceive.agent.totalWinLoseCom;
//
//                         body.commission.member.winLoseCom = shareReceive.member.winLoseCom;
//                         body.commission.member.winLose = shareReceive.member.winLose;
//                         body.commission.member.totalWinLoseCom = shareReceive.member.totalWinLoseCom;
//                         body.validAmount = betResult === DRAW ? 0 : body.amount;
//
//                         updateAgentMemberBalance(shareReceive, betAmount, requestBody.transactionid, function (err, updateBalanceResponse) {
//                             if (err) {
//
//                                 return res.send(
//                                     jsonToAgGameResponseFormat({
//                                         ResponseCode: 'INSUFFICIENT_FUNDS'
//                                     })
//                                 );
//
//                             } else {
//
//                                 let newBalance = updateBalanceResponse.newBalance;
//
//                                 console.log('-------------new balance--------------', newBalance);
//
//                                 let playType = '';
//                                 betObj.baccarat.ag.txns.forEach((data) => {
//                                     if(requestBody.transactionid[0] === data.txnId){
//                                         playType = data.playType ? data.playType : '';
//                                     }
//                                 });
//
//                                 let updateBody = {
//                                     $set: {
//                                         'baccarat.ag.winLose': winLose,
//                                         'baccarat.ag.result': requestBody.gameresult,
//                                         'baccarat.ag.validBetAmount': validBetAmount,
//                                         'commission': body.commission,
//                                         'validAmount': body.validAmount,
//                                         'betResult': betResult,
//                                         'isEndScore': true,
//                                         'status': 'DONE',
//                                         'settleDate': DateUtils.getCurrentDate()
//                                     },
//                                     $push: {
//                                         'baccarat.ag.resultTransaction': {
//                                             transactionId: requestBody.transactionid,
//                                             playType: playType ? playType : AgGamingService.getPlayType(requestBody.playtype),
//                                             netAmount: roundTo(Number.parseFloat(requestBody.netamount), 2),
//                                             validAmount: roundTo(Number.parseFloat(requestBody.validbetamount), 2),
//                                             billNo: requestBody.billno,
//                                             isFinish: requestBody.finish
//                                         }
//                                     }
//                                 };
//
//                                 // console.log('-------------BetTransactionModel--------------', updateBody);
//                                 BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
//                                     if (err) {
//                                         return res.send(
//                                             jsonToAgGameResponseFormat({
//                                                 ResponseCode: 'ERROR'
//                                             })
//                                         );
//                                     } else {
//                                         return res.send(
//                                             jsonToAgGameResponseFormat({
//                                                 ResponseCode: 'OK',
//                                                 Balance: newBalance
//                                             })
//                                         );
//                                     }
//                                 });
//                             }
//                         });
//                     });
//                 }
//
//             });
//
//
//     });
// }
//
//
// function cancelBetFunction(requestBody, res) {
//     console.log('--------------------------------REFUND----------------------------------', requestBody)
//     saveAgTransactionLog(requestBody, 'REFUND', (err, saveAllBetTransResponse) => {
//         if (err) {
//             console.log('-----------------saveAgTransactionLog Refund fail----------------')
//         }
//     });
//
//     async.waterfall([callback => {
//
//         AgGamingService.checkSessionToken(requestBody.sessiontoken, (err, result) => {
//             if (result) {
//                 if (requestBody.playname.indexOf(result.username) < 0) {
//                     return res.send(
//                         jsonToAgGameResponseFormat({
//                             ResponseCode: 'INCORRECT_SESSION_TYPE'
//                         })
//                     )
//                 } else {
//                     callback(null, result.username);
//                 }
//
//             } else {
//                 return res.send(
//                     jsonToAgGameResponseFormat({
//                         ResponseCode: 'ERROR'
//                     })
//                 )
//             }
//         })
//
//     }, (username, callback) => {
//
//         MemberService.findByUserNameForPartnerService(username, function (err, memberResponse) {
//             if (err) {
//                 callback(err, null);
//                 return;
//             }
//             if (memberResponse) {
//                 callback(null, memberResponse);
//             } else {
//                 callback(1004, null);
//             }
//         });
//
//     }], (err, asyncResponse) => {
//
//         if (err) {
//             return res.send(
//                 jsonToAgGameResponseFormat({
//                     ResponseCode: 'INVALID_DATA'
//                 })
//             )
//         }
//
//         let memberInfo = asyncResponse;
//
//         let condition = {
//             'memberId': mongoose.Types.ObjectId(memberInfo._id),
//             'game': 'CASINO',
//             'source': 'AG_GAME',
//             'baccarat.ag.gameId': requestBody.gamecode,
//             'baccarat.ag.txns.txnId': {$in: requestBody.transactionid},
//         };
//
//
//         BetTransactionModel.findOne(condition)
//             .populate({path: 'memberParentGroup', select: 'type'})
//             .exec(function (err, betObj) {
//
//                 // console.log('bet obj ============== ', betObj);
//
//
//                 if (!betObj) {
//                     return res.send(
//                         jsonToAgGameResponseFormat({
//                             ResponseCode: 'INVALID_TRANSACTION'
//                         })
//                     );
//                 }
//
//                 let cancelItems = _.filter(betObj.baccarat.ag.txns, (item) => {
//                     return requestBody.transactionid == item.txnId;
//                 });
//
//
//                 if (cancelItems.length == 0) {
//                     let credit = roundTo(memberInfo.creditLimit + memberInfo.balance, 2);
//
//                     return res.send(
//                         jsonToAgGameResponseFormat({
//                             ResponseCode: 'OK',
//                             Balance: credit
//                         })
//                     );
//                 }
//
//                 const betAmount = roundTo(Number.parseFloat(requestBody.value), 2);
//
//                 let updateCondition = {
//                     $pull: {
//                         'baccarat.ag.txns': {"txnId": requestBody.transactionid},
//                         'baccarat.ag.resultTransaction' : {"txnId": requestBody.transactionid}
//                     },
//                     $inc: {
//                         amount: betAmount * -1,
//                         memberCredit: betAmount,
//                         validAmount: betAmount * -1
//                     }
//                 };
//
//                 BetTransactionModel.update({
//                     _id: betObj._id,
//                     'baccarat.ag.txns.txnId': requestBody.transactionid
//                 }, updateCondition, function (err, cancelResponse) {
//                     if (err) {
//                         return res.send(jsonToAgGameResponseFormat({
//                             ResponseCode: 'INVALID_TRANSACTION'
//                         }));
//                     }
//
//                     // console.log('cancelResponse : ', cancelResponse)
//
//                     if (cancelResponse.nModified == 1) {
//
//                         MemberService.updateBalance(memberInfo._id, betAmount, betObj.betId, 'AG_CANCEL_BET', requestBody.transactionid, function (err, updateBalanceResponse) {
//
//                             if (err) {
//                                 return res.send(jsonToAgGameResponseFormat({
//                                     ResponseCode: 'INVALID_TRANSACTION'
//                                 }));
//
//
//                             } else {
//
//                                 BetTransactionModel.remove({
//                                     _id: betObj._id,
//                                     "baccarat.ag.txns": {$size: 0}
//                                 }, function (err, removeResponse) {
//                                     console.log("remove res : ", removeResponse)
//                                     console.log(err)
//                                 });
//
//
//                                 if (betObj.status == 'DONE') {
//
//                                     try {
//
//                                         let winLose = 0;
//
//                                         if (betObj.betResult == WIN) {
//                                             winLose = betObj.commission.member.winLose - betAmount;
//                                         } else if (betObj.betResult == LOSE) {
//                                             winLose = betObj.commission.member.winLose + betAmount;
//                                         } else if (betObj.betResult == DRAW) {
//                                             winLose = betObj.commission.member.winLose + betAmount;
//                                         }
//
//                                         // console.log('new wl : ', winLose)
//                                         let betResult = winLose > 0 ? WIN : winLose < 0 ? LOSE : DRAW;
//
//                                         const shareReceive = AgentService.prepareShareReceive(winLose, betResult, betObj);
//
//
//                                         let updateBody = {
//                                             'commission.superAdmin.winLoseCom': shareReceive.superAdmin.winLoseCom,
//                                             'commission.superAdmin.winLose': shareReceive.superAdmin.winLose,
//                                             'commission.superAdmin.totalWinLoseCom': shareReceive.superAdmin.totalWinLoseCom,
//
//                                             'commission.company.winLoseCom': shareReceive.company.winLoseCom,
//                                             'commission.company.winLose': shareReceive.company.winLose,
//                                             'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,
//
//                                             'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
//                                             'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
//                                             'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,
//
//                                             'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
//                                             'commission.senior.winLose': shareReceive.senior.winLose,
//                                             'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,
//
//                                             'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
//                                             'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
//                                             'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,
//
//                                             'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
//                                             'commission.agent.winLose': shareReceive.agent.winLose,
//                                             'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,
//
//                                             'commission.member.winLoseCom': shareReceive.member.winLoseCom,
//                                             'commission.member.winLose': shareReceive.member.winLose,
//                                             'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
//                                             'betResult': betResult
//
//                                         };
//
//
//                                         BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, cancelResponse) {
//
//                                             console.log(cancelResponse)
//
//                                             return res.send(
//                                                 jsonToAgGameResponseFormat({
//                                                     ResponseCode: 'OK',
//                                                     Balance: updateBalanceResponse.newBalance
//                                                 })
//                                             );
//
//                                         });
//
//                                     } catch (err) {
//                                         return res.send(
//                                             jsonToAgGameResponseFormat({
//                                                 ResponseCode: 'OK',
//                                                 Balance: updateBalanceResponse.newBalance
//                                             })
//                                         );
//                                     }
//
//                                 } else {
//                                     return res.send(
//                                         jsonToAgGameResponseFormat({
//                                             ResponseCode: 'OK',
//                                             Balance: updateBalanceResponse.newBalance
//                                         })
//                                     );
//                                 }
//                             }
//                         });
//                     } else {
//                         return res.send(
//                             jsonToAgGameResponseFormat({
//                                 ResponseCode: 'ERROR'
//                             })
//                         );
//                     }
//                 });
//
//             });
//
//
//     });
//
// }
//
//
// function convertDateToCurrentTime(dt) {
//     let d = new Date(dt);
//     let betTime = moment(d).add(18, 'hours');
//
//     return betTime
// }
//
// function prepareCommission(memberInfo, body, currentAgent, overAgent, remaining, overAllShare) {
//
//
//     if (currentAgent && (currentAgent.type === 'SUPER_ADMIN' || currentAgent.parentId)) {
//
//
//         // console.log('==============================================');
//         // console.log('process : ' + currentAgent.type + ' : ' + currentAgent.name);
//
//
//         let money = body.amount;
//
//
//         if (!overAgent) {
//             overAgent = {};
//             overAgent.type = 'member';
//             overAgent.shareSetting = {};
//             overAgent.shareSetting.casino = {};
//             overAgent.shareSetting.casino.ag = {};
//             overAgent.shareSetting.casino.ag.parent = memberInfo.shareSetting.casino.ag.parent;
//             overAgent.shareSetting.casino.ag.own = 0;
//             overAgent.shareSetting.casino.ag.remaining = 0;
//
//             body.commission.member = {};
//             body.commission.member.parent = memberInfo.shareSetting.casino.ag.parent;
//             body.commission.member.commission = memberInfo.commissionSetting.casino.ag;
//
//         }
//
//         // console.log('');
//         // console.log('---- setting ----');
//         // console.log('ส่วนแบ่งที่ได้มามีทั้งหมด : ' + currentAgent.shareSetting.casino.ag.own + ' %');
//         // console.log('ขอส่วนแบ่งจาก : ' + overAgent.type + ' ' + overAgent.shareSetting.casino.ag.parent + ' %');
//         // console.log('ให้ส่วนแบ่ง : ' + overAgent.type + ' ที่เหลือไป ' + overAgent.shareSetting.casino.ag.own + ' %');
//         // console.log('remaining จาก : ' + overAgent.type + ' : ' + overAgent.shareSetting.casino.ag.remaining + ' %');
//         // console.log('โดนบังคับเสียขั้นต่ำ : ' + currentAgent.shareSetting.casino.ag.min + ' %');
//         // console.log('---- end setting ----');
//         // console.log('');
//
//
//         // console.log('และได้รับ remaining ไว้ : '+overAgent.shareSetting.casino.ag.remaining+' %');
//
//         let currentPercentReceive = overAgent.shareSetting.casino.ag.parent;
//         // console.log('% ที่ได้ : ' + currentPercentReceive);
//         // console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');
//         let totalRemaining = remaining;
//
//         if (overAgent.shareSetting.casino.ag.remaining > 0) {
//             // console.log()
//             // console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');
//
//             if (overAgent.shareSetting.casino.ag.remaining > totalRemaining) {
//                 // console.log('รับ remaining ทั้งหมดไว้: ' + totalRemaining + ' %');
//                 currentPercentReceive += totalRemaining;
//                 totalRemaining = 0;
//             } else {
//                 totalRemaining = remaining - overAgent.shareSetting.casino.ag.remaining;
//                 // console.log('หักค่า remaining ที่ได้รับมากับที่เซตรับไว้ = ' + remaining + ' - ' + overAgent.shareSetting.casino.ag.remaining + ' = ' + totalRemaining);
//                 currentPercentReceive += overAgent.shareSetting.casino.ag.remaining;
//             }
//
//         }
//
//         // console.log('รวม % ที่ได้รับ : ' + currentPercentReceive);
//
//
//         let unUseShare = currentAgent.shareSetting.casino.ag.own -
//             (overAgent.shareSetting.casino.ag.parent + overAgent.shareSetting.casino.ag.own);
//
//         // console.log('เหลือส่วนแบ่งที่ไม่ได้ใช้ : ' + unUseShare + ' %');
//         totalRemaining = (unUseShare + totalRemaining);
//         // console.log('ส่วนแบ่งที่ไม่ได้ใช้ บวกกับค่า remaining ท่ี่เหลือจะเป็น : ' + totalRemaining + ' %');
//
//         // console.log('จากที่โดนบังคับเสียขั้นต่ำ : ' + currentAgent.shareSetting.casino.ag.min + ' %');
//         // console.log('overAllShare : ', overAllShare);
//         if (currentAgent.shareSetting.casino.ag.min > 0 && ((overAllShare + currentPercentReceive) < currentAgent.shareSetting.casino.ag.min)) {
//
//             if (currentPercentReceive < currentAgent.shareSetting.casino.ag.min) {
//                 // console.log('% ที่ได้รับ < ขั้นต่ำที่ถูกตั้ง min ไว้');
//                 let percent = currentAgent.shareSetting.casino.ag.min - (currentPercentReceive + overAllShare);
//                 // console.log('หักออกไป ' + percent + ' % : แล้วไปเพิ่มในส่วนที่ต้องรับผิดชอบ ');
//                 totalRemaining = totalRemaining - percent;
//                 if (totalRemaining < 0) {
//                     totalRemaining = 0;
//                 }
//                 currentPercentReceive += percent;
//             }
//         } else {
//             // currentPercentReceive += totalRemaining;
//         }
//
//
//         if (currentAgent.type === 'SUPER_ADMIN') {
//             currentPercentReceive += totalRemaining;
//         }
//
//         let getMoney = (money * NumberUtils.convertPercent(currentPercentReceive)).toFixed(2);
//         overAllShare = overAllShare + currentPercentReceive;
//
//         // console.log('ยอดแทง ' + money + ' ได้ทั้งหมด : ' + currentPercentReceive + ' %');
//
//         // console.log('รวม % + % remaining  จะเป็นสัดส่วนที่ได้ทั้งหมด = ' + getMoney);
//         // console.log('ค่าที่จะถูกคืนกลับไป (remaining) : ' + totalRemaining + ' %');
//
//         // console.log('จำนวน % ที่ถูกแบ่งไปแล้วทั้งหมด : ', overAllShare);
//
//         //set commission
//         let agentCommission = currentAgent.commissionSetting.casino.ag;
//
//
//         switch (currentAgent.type) {
//             case 'SUPER_ADMIN':
//                 body.commission.superAdmin = {};
//                 body.commission.superAdmin.group = currentAgent._id;
//                 body.commission.superAdmin.parent = currentAgent.shareSetting.casino.ag.parent;
//                 body.commission.superAdmin.own = currentAgent.shareSetting.casino.ag.own;
//                 body.commission.superAdmin.remaining = currentAgent.shareSetting.casino.ag.remaining;
//                 body.commission.superAdmin.min = currentAgent.shareSetting.casino.ag.min;
//                 body.commission.superAdmin.shareReceive = Math.abs(currentPercentReceive);
//                 body.commission.superAdmin.commission = agentCommission;
//                 body.commission.superAdmin.amount = getMoney;
//                 break;
//             case 'COMPANY':
//                 body.commission.company = {};
//                 body.commission.company.parentGroup = currentAgent.parentId;
//                 body.commission.company.group = currentAgent._id;
//                 body.commission.company.parent = currentAgent.shareSetting.casino.ag.parent;
//                 body.commission.company.own = currentAgent.shareSetting.casino.ag.own;
//                 body.commission.company.remaining = currentAgent.shareSetting.casino.ag.remaining;
//                 body.commission.company.min = currentAgent.shareSetting.casino.ag.min;
//                 body.commission.company.shareReceive = Math.abs(currentPercentReceive);
//                 body.commission.company.commission = agentCommission;
//                 body.commission.company.amount = getMoney;
//                 break;
//             case 'SHARE_HOLDER':
//                 body.commission.shareHolder = {};
//                 body.commission.shareHolder.parentGroup = currentAgent.parentId;
//                 body.commission.shareHolder.group = currentAgent._id;
//                 body.commission.shareHolder.parent = currentAgent.shareSetting.casino.ag.parent;
//                 body.commission.shareHolder.own = currentAgent.shareSetting.casino.ag.own;
//                 body.commission.shareHolder.remaining = currentAgent.shareSetting.casino.ag.remaining;
//                 body.commission.shareHolder.min = currentAgent.shareSetting.casino.ag.min;
//                 body.commission.shareHolder.shareReceive = currentPercentReceive;
//                 body.commission.shareHolder.commission = agentCommission;
//                 body.commission.shareHolder.amount = getMoney;
//                 break;
//             case 'SENIOR':
//                 body.commission.senior = {};
//                 body.commission.senior.parentGroup = currentAgent.parentId;
//                 body.commission.senior.group = currentAgent._id;
//                 body.commission.senior.parent = currentAgent.shareSetting.casino.ag.parent;
//                 body.commission.senior.own = currentAgent.shareSetting.casino.ag.own;
//                 body.commission.senior.remaining = currentAgent.shareSetting.casino.ag.remaining;
//                 body.commission.senior.min = currentAgent.shareSetting.casino.ag.min;
//                 body.commission.senior.shareReceive = currentPercentReceive;
//                 body.commission.senior.commission = agentCommission;
//                 body.commission.senior.amount = getMoney;
//                 break;
//             case 'MASTER_AGENT':
//                 body.commission.masterAgent = {};
//                 body.commission.masterAgent.parentGroup = currentAgent.parentId;
//                 body.commission.masterAgent.group = currentAgent._id;
//                 body.commission.masterAgent.parent = currentAgent.shareSetting.casino.ag.parent;
//                 body.commission.masterAgent.own = currentAgent.shareSetting.casino.ag.own;
//                 body.commission.masterAgent.remaining = currentAgent.shareSetting.casino.ag.remaining;
//                 body.commission.masterAgent.min = currentAgent.shareSetting.casino.ag.min;
//                 body.commission.masterAgent.shareReceive = currentPercentReceive;
//                 body.commission.masterAgent.commission = agentCommission;
//                 body.commission.masterAgent.amount = getMoney;
//                 break;
//             case 'AGENT':
//                 body.commission.agent = {};
//                 body.commission.agent.parentGroup = currentAgent.parentId;
//                 body.commission.agent.group = currentAgent._id;
//                 body.commission.agent.parent = currentAgent.shareSetting.casino.ag.parent;
//                 body.commission.agent.own = currentAgent.shareSetting.casino.ag.own;
//                 body.commission.agent.remaining = currentAgent.shareSetting.casino.ag.remaining;
//                 body.commission.agent.min = currentAgent.shareSetting.casino.ag.min;
//                 body.commission.agent.shareReceive = currentPercentReceive;
//                 body.commission.agent.commission = agentCommission;
//                 body.commission.agent.amount = getMoney;
//                 break;
//         }
//
//         prepareCommission(memberInfo, body, currentAgent.parentId, currentAgent, totalRemaining, overAllShare);
//     }
// }
//
// function updateAgentMemberBalance(shareReceive, betAmount, ref, updateCallback) {
//
//     console.log('updateAgentMemberBalance');
//     console.log('shareReceive : ', shareReceive);
//
//
//     let balance = betAmount + shareReceive.member.totalWinLoseCom;
//
//     try {
//         console.log('update member balance : ', balance);
//
//         MemberService.updateBalance(shareReceive.member.id, balance, shareReceive.betId, 'AG_SETTLE', ref, function (err, updateBalanceResponse) {
//             if (err) {
//                 updateCallback(err, null);
//             } else {
//                 updateCallback(null, updateBalanceResponse);
//             }
//         });
//     } catch (err) {
//         MemberService.updateBalance(shareReceive.member.id, balance, shareReceive.betId, 'AG_SETTLE_ERROR ' + shareReceive.member.id + ' : ' + balance, ref, function (err, updateBalanceResponse) {
//             if (err) {
//                 updateCallback(err, null);
//             } else {
//                 updateCallback(null, updateBalanceResponse);
//             }
//         });
//     }
//
// }
//
// function getTodayWinLoss(memberId, callback) {
//
//     let today;
//     if (moment().hours() >= 11) {
//         today = moment().format('DD/MM/YYYY');
//     } else {
//         today = moment().add(-1, 'd').format('DD/MM/YYYY');
//     }
//
//     let condition = {
//         'gameDate': {
//             "$gte": new Date(moment(today, "DD/MM/YYYY").format('YYYY-MM-DDT11:00:00.000'))
//         },
//         'memberId': mongoose.Types.ObjectId(memberId),
//         'game': 'CASINO',
//         'status': 'DONE'
//     };
//
//     BetTransactionModel.aggregate([
//         {
//             $match: condition
//         },
//         {
//             "$group": {
//                 "_id": {
//                     member: '$memberId',
//                 },
//                 "memberTotalWinLoseCom": {$sum: '$commission.member.totalWinLoseCom'},
//             }
//         },
//         {
//             $project: {
//                 _id: 0,
//                 memberTotalWinLoseCom: '$memberTotalWinLoseCom'
//             }
//         }
//     ], (err, results) => {
//         callback(null, results[0] ? results[0].memberTotalWinLoseCom : 0);
//     });
// }
//
// function saveAgTransactionLog(requestBody, action, callback) {
//     let model;
//     if (action === 'BET') {
//         model = new AgTransaction(
//             {
//                 username: requestBody.playname,
//                 currency: requestBody.currency,
//                 amount: requestBody.value,
//                 createdDate: DateUtils.getCurrentDate(),
//                 transactionID: requestBody.transactionid,
//                 gameType: requestBody.gametype,
//                 platform: requestBody.platformtype,
//                 round: requestBody.round,
//                 gameCode: requestBody.gamecode,
//                 tableCode: requestBody.tablecode,
//                 transactionType: requestBody.transactiontype,
//                 transactionCode: requestBody.transactioncode,
//                 deviceType: requestBody.devicetype,
//                 playType: requestBody.playtype
//             }
//         );
//     } else if(action === 'SETTLE'){
//         model = new AgTransaction(
//             {
//                 username: requestBody.playname,
//                 currency: requestBody.currency,
//                 createdDate: DateUtils.getCurrentDate(),
//                 transactionID: requestBody.transactionid,
//                 gameType: requestBody.gametype,
//                 gameCode: requestBody.gamecode,
//                 transactionType: requestBody.transactiontype,
//                 transactionCode: requestBody.transactioncode,
//                 netAmount: requestBody.netamount,
//                 validBetAmount: requestBody.validbetamount,
//                 settleTime: convertDateToCurrentTime(requestBody.settletime),
//                 ticketStatus: requestBody.ticketstatus,
//                 gameResult: requestBody.gameresult,
//                 isFinish: requestBody.finish
//             }
//         );
//     }else {
//         console.log('-------d--d-d-d-d--d---------',requestBody);
//         model = new AgTransaction(
//             {
//                 ticketStatus : requestBody.ticketstatus,
//                 currency : requestBody.currency,
//                 amount : requestBody.value,
//                 username : requestBody.playname,
//                 settleTime : requestBody.bettime,
//                 transactionID : requestBody.transactionid,
//                 platform : requestBody.platformtype,
//                 round : requestBody.round,
//                 gameType : requestBody.gametype,
//                 gameCode : requestBody.gamecode,
//                 tableCode : requestBody.tablecode,
//                 transactionType : requestBody.transactiontype,
//                 transactionCode : requestBody.transactioncode,
//                 playType : requestBody.playtype
//             }
//         );
//
//     }
//
//
//     console.log('model === ',model);
//
//     model.save((err, response) => {
//         if (err) {
//             callback(err, null);
//         } else {
//             callback(null, 'success');
//         }
//
//     });
//
//
// }

module.exports = router;
