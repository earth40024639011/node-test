const express = require('express');
const router = express.Router();
const request = require('request');
const _ = require('underscore');
const async = require("async");
const Joi = require('joi');
const Hashing = require('../../common/hashing');
const mongoose = require('mongoose');
const moment = require('moment');
const crypto = require('crypto');

const AgentService = require('../../common/agentService');
const AmbGameService = require('../../common/ambGameService');
const MemberService = require('../../common/memberService');




router.get('/getGameList', (req, res) => {

    AmbGameService.callGameList((err, gameList) => {
        if (err) {
            return res.send({data: err, code: 999});
        } else {
            return res.send({data: gameList, code: 0});
        }

    });

});


router.get('/getReportDetails', (req, res) => {

    const request = req.query;

    AmbGameService.callGetReportDetails(request.username,request.roundId,(err, url) => {
        if (err) {
            return res.send({data: err, code: 999});
        } else {
            return res.send({result:{url: url}, code: 0});
        }

    });

});



router.get('/login', function (req, res) {

    const userInfo = req.userInfo;

    let account = userInfo.username_lower;

    async.waterfall([callback => {

        AgentService.getUpLineStatus(userInfo.group._id, function (err, status) {

            if (err) {
                callback(err, null);
            } else {
                if (status.suspend) {
                    callback(5011, null);
                } else {
                    callback(null, status);
                }
            }
        });

    }, (uplineStatus, callback) => {

        MemberService.findById(userInfo._id, 'username_lower creditLimit balance limitSetting group suspend', function (err, memberResponse) {
            if (err) {
                callback(err, null);
            } else {

                if (memberResponse.suspend) {
                    callback(5011, null);
                } else if (memberResponse.limitSetting.multi.amb.isEnable) {
                    AgentService.getAmbGameStatus(memberResponse.group._id, (err, response) => {

                        if (!response) {
                            callback(1088, null);
                        } else if (!response.isEnable) {
                            callback(1088, null);
                        } else {
                            callback(null, uplineStatus, memberResponse);
                        }
                    });
                } else {
                    callback(1088, null);
                }
            }
        });

    }], function (err, uplineStatus, memberInfo) {

        if (err) {
            if (err == 1088) {
                return res.send({
                    code: 1088,
                    message: 'Service Locked, Please contact your upline.'
                });
            }
            if (err == 5011) {
                return res.send({
                    message: "Account or Upline was suspend.",
                    result: null,
                    code: 5011
                });
            }
            return res.send({code: 999, message: err});
        }


        AmbGameService.callCreateMember(memberInfo.username_lower, (err, createMemberResponse) => {

            if (err && err.code != 902) {
                return res.send({code: 999, message: err});
            }

            AmbGameService.callLaunchGame(memberInfo.username_lower, req.query.gameId, (err, launchGameUrl) => {

                if (err) {
                    return res.send({
                        code: 999,
                        message: err
                    });
                } else {
                    return res.send({
                        code: 0,
                        message: 'success',
                        url: launchGameUrl
                    });

                }
            });


        });

    });
});

//
// router.put('/logout', function (req, res) {
//
//     let {
//         account,
//     } = req.body;
//
//     const headers = {
//         'Content-Type': 'application/json'
//     };
//
//
//     const option = {
//         url: `${HOST}/api/${WEBSITE}/logout?cert=${CERT}&user=${account}`,
//         method: 'GET',
//         headers: headers
//     };
//
//     request(option, (err, response, body) => {
//         if (err) {
//             return res.send({code: 999, result: err});
//         } else {
//             let resultBody = JSON.parse(body);
//             if (resultBody.status == 0) {
//                 return res.send({code: 0, message: 'Logout failed'});
//             } else if (resultBody.status == 1) {
//                 return res.send({code: 1, message: 'Logout success', result: resultBody});
//             }
//
//         }
//     });
// });

module.exports = router;