const BasketballModel = require('../../models/basketball.model');
const express = require('express');
const router  = express.Router();
const _ = require('underscore');
const mongoose = require('mongoose');
const RedisService = require('../../common/redisService');
const async = require("async");
//[4]
router.put('/hdp_price/:id', (req, res) => {
    const league_key = req.params.id;

    if (!league_key) {
        console.log('[4.1] 900001 : ==> Parameter is incorrect.');
        return res.send({
            code: 900001,
            message: 'Parameter is incorrect.'
        });
    }

    const {
        hdp
    } = req.body;

    BasketballModel.update({
            k: league_key
        },
        {
            'rp.hdp.ah' : hdp.ah,
            'rp.hdp.ou' : hdp.ou,
            'rp.hdp.oe' : hdp.oe,
            'rp.hdp.ml' : hdp.ml,
            'rp.hdp.t1ou' : hdp.t1ou,
            'rp.hdp.t2ou' : hdp.t2ou
        },
        {
            multi: false
        },
        (err, data) => {
            if (err) {
                console.log(`[4.1] Error update league : ${league_key}`);
                console.log(err);
                return res.send({
                    message: err,
                    code: 9999
                });
            } else if (data.nModified === 0) {
                console.log(`[4.2] update league fail : ${league_key}`);
                return res.send({
                    message: `Update league fail : ${league_key}`,
                    code: 0
                });
            } else {
                console.log(`[4.3] update league success : ${league_key}`);
                return res.send({
                    code: 0,
                    message: "success"
                });
            }
        });
});
//[3]
router.get('/league', (req, res) => {
    async.series([
        (callback) => {
            BasketballModel.find({})
                .lean()
                .sort('sk')
                .exec((error, data) => {
                    if (error) {
                        callback(null, error);
                    } else {
                        callback(null, data);
                    }
                });
        },

        (callback) => {
            RedisService.getCurrency((error, result) => {
                if (error) {
                    callback(null, false);
                } else {
                    callback(null, result);
                }
            })
        }

    ], (errAsync, result)=> {
        if (errAsync) {
            return res.send({
                message: "error",
                result: errAsync,
                code: 998
            });
        } else {
            const [
                data,
                currency
            ] = result;

            const resultResponse = _.chain(data)
                .each(league => {
                    league.m = [];
                })
                // .each(league => {
                //     const {
                //         r,
                //         rl,
                //         rem
                //     } = league;
                //
                //     const currencyName = _.chain(currency)
                //         .map(obj => {
                //             return obj.currencyName;
                //         })
                //         .value();
                //
                //
                //     _.each(currencyName, objCurrencyName => {
                //         const r_obj = {};
                //         let membercurrency = objCurrencyName;
                //
                //         r_obj.ah = {};
                //         r_obj.ou = {};
                //         r_obj.x12 = {};
                //         r_obj.oe = {};
                //         r_obj.ah1st = {};
                //         r_obj.ou1st = {};
                //         r_obj.x121st = {};
                //         r_obj.oe1st = {};
                //         r_obj.pm = {};
                //
                //         r_obj.ah.ma = convertMinMaxByCurrency(r.ah.ma, currency, membercurrency);
                //         r_obj.ah.mi = convertMinMaxByCurrency(r.ah.mi, currency, membercurrency);
                //
                //         r_obj.ou.ma = convertMinMaxByCurrency(r.ou.ma, currency, membercurrency);
                //         r_obj.ou.mi = convertMinMaxByCurrency(r.ou.mi, currency, membercurrency);
                //
                //         r_obj.x12.ma = convertMinMaxByCurrency(r.x12.ma, currency, membercurrency);
                //         r_obj.x12.mi = convertMinMaxByCurrency(r.x12.mi, currency, membercurrency);
                //
                //         r_obj.oe.ma = convertMinMaxByCurrency(r.oe.ma, currency, membercurrency);
                //         r_obj.oe.mi = convertMinMaxByCurrency(r.oe.mi, currency, membercurrency);
                //
                //         r_obj.ah1st.ma = convertMinMaxByCurrency(r.ah1st.ma, currency, membercurrency);
                //         r_obj.ah1st.mi = convertMinMaxByCurrency(r.ah1st.mi, currency, membercurrency);
                //
                //         r_obj.ou1st.ma = convertMinMaxByCurrency(r.ou1st.ma, currency, membercurrency);
                //         r_obj.ou1st.mi = convertMinMaxByCurrency(r.ou1st.mi, currency, membercurrency);
                //
                //         r_obj.x121st.ma = convertMinMaxByCurrency(r.x121st.ma, currency, membercurrency);
                //         r_obj.x121st.mi = convertMinMaxByCurrency(r.x121st.mi, currency, membercurrency);
                //
                //         r_obj.oe1st.ma = convertMinMaxByCurrency(r.oe1st.ma, currency, membercurrency);
                //         r_obj.oe1st.mi = convertMinMaxByCurrency(r.oe1st.mi, currency, membercurrency);
                //
                //         r_obj.pm = convertMinMaxByCurrency(r.pm, currency, membercurrency);
                //
                //         const rl_obj = {};
                //         rl_obj.ah = {};
                //         rl_obj.ou = {};
                //         rl_obj.x12 = {};
                //         rl_obj.oe = {};
                //         rl_obj.ah1st = {};
                //         rl_obj.ou1st = {};
                //         rl_obj.x121st = {};
                //         rl_obj.oe1st = {};
                //         rl_obj.pm = {};
                //
                //         rl_obj.ah.ma = convertMinMaxByCurrency(rl.ah.ma, currency, membercurrency);
                //         rl_obj.ah.mi = convertMinMaxByCurrency(rl.ah.mi, currency, membercurrency);
                //
                //         rl_obj.ou.ma = convertMinMaxByCurrency(rl.ou.ma, currency, membercurrency);
                //         rl_obj.ou.mi = convertMinMaxByCurrency(rl.ou.mi, currency, membercurrency);
                //
                //         rl_obj.x12.ma = convertMinMaxByCurrency(rl.x12.ma, currency, membercurrency);
                //         rl_obj.x12.mi = convertMinMaxByCurrency(rl.x12.mi, currency, membercurrency);
                //
                //         rl_obj.oe.ma = convertMinMaxByCurrency(rl.oe.ma, currency, membercurrency);
                //         rl_obj.oe.mi = convertMinMaxByCurrency(rl.oe.mi, currency, membercurrency);
                //
                //         rl_obj.ah1st.ma = convertMinMaxByCurrency(rl.ah1st.ma, currency, membercurrency);
                //         rl_obj.ah1st.mi = convertMinMaxByCurrency(rl.ah1st.mi, currency, membercurrency);
                //
                //         rl_obj.ou1st.ma = convertMinMaxByCurrency(rl.ou1st.ma, currency, membercurrency);
                //         rl_obj.ou1st.mi = convertMinMaxByCurrency(rl.ou1st.mi, currency, membercurrency);
                //
                //         rl_obj.x121st.ma = convertMinMaxByCurrency(rl.x121st.ma, currency, membercurrency);
                //         rl_obj.x121st.mi = convertMinMaxByCurrency(rl.x121st.mi, currency, membercurrency);
                //
                //         rl_obj.oe1st.ma = convertMinMaxByCurrency(rl.oe1st.ma, currency, membercurrency);
                //         rl_obj.oe1st.mi = convertMinMaxByCurrency(rl.oe1st.mi, currency, membercurrency);
                //
                //         rl_obj.pm = convertMinMaxByCurrency(rl.pm, currency, membercurrency);
                //
                //         const rem_obj = {};
                //         rem_obj.ah = {};
                //         rem_obj.ou = {};
                //         rem_obj.x12 = {};
                //         rem_obj.oe = {};
                //         rem_obj.ah1st = {};
                //         rem_obj.ou1st = {};
                //         rem_obj.x121st = {};
                //         rem_obj.oe1st = {};
                //         rem_obj.pm = {};
                //
                //         rem_obj.ah.ma = convertMinMaxByCurrency(rem.ah.ma, currency, membercurrency);
                //         rem_obj.ah.mi = convertMinMaxByCurrency(rem.ah.mi, currency, membercurrency);
                //
                //         rem_obj.ou.ma = convertMinMaxByCurrency(rem.ou.ma, currency, membercurrency);
                //         rem_obj.ou.mi = convertMinMaxByCurrency(rem.ou.mi, currency, membercurrency);
                //
                //         rem_obj.x12.ma = convertMinMaxByCurrency(rem.x12.ma, currency, membercurrency);
                //         rem_obj.x12.mi = convertMinMaxByCurrency(rem.x12.mi, currency, membercurrency);
                //
                //         rem_obj.oe.ma = convertMinMaxByCurrency(rem.oe.ma, currency, membercurrency);
                //         rem_obj.oe.mi = convertMinMaxByCurrency(rem.oe.mi, currency, membercurrency);
                //
                //         rem_obj.ah1st.ma = convertMinMaxByCurrency(rem.ah1st.ma, currency, membercurrency);
                //         rem_obj.ah1st.mi = convertMinMaxByCurrency(rem.ah1st.mi, currency, membercurrency);
                //
                //         rem_obj.ou1st.ma = convertMinMaxByCurrency(rem.ou1st.ma, currency, membercurrency);
                //         rem_obj.ou1st.mi = convertMinMaxByCurrency(rem.ou1st.mi, currency, membercurrency);
                //
                //         rem_obj.x121st.ma = convertMinMaxByCurrency(rem.x121st.ma, currency, membercurrency);
                //         rem_obj.x121st.mi = convertMinMaxByCurrency(rem.x121st.mi, currency, membercurrency);
                //
                //         rem_obj.oe1st.ma = convertMinMaxByCurrency(rem.oe1st.ma, currency, membercurrency);
                //         rem_obj.oe1st.mi = convertMinMaxByCurrency(rem.oe1st.mi, currency, membercurrency);
                //
                //         rem_obj.pm = convertMinMaxByCurrency(rem.pm, currency, membercurrency);
                //
                //         league[`r_${objCurrencyName}`] = r_obj;
                //         league[`rl_${objCurrencyName}`] = rl_obj;
                //         league[`rem_${objCurrencyName}`] = rem_obj;
                //
                //     });
                // })
                .value();

            return res.send({
                code: 0,
                message: `success`,
                result: _.sortBy(resultResponse, 'sk')
            });
        }
    });
});
//[5]
router.put('/set_default_rule_league/:id', (req, res) =>{
    const league_key = req.params.id;
    if (!league_key) {
        console.log('[5.1] 900001 : ==> Parameter is incorrect.');
        return res.send({
            code: 900001,
            message: 'Parameter is incorrect.'
        });
    }

    BasketballModel.findOne({
        k: league_key
    })
        .lean()
        .exec((err, data) => {
            if (err) {
                console.log(`[5.2] Error update league : ${league_key}`);
                console.log(err);
                return res.send({
                    message: err,
                    code: 9999
                });
            }  else if (!data) {
                console.log(`[5.3] update league fail : ${league_key}`);
                return res.send({
                    message: `Update league fail : ${league_key}`,
                    code: 0
                });
            } else {
                const {
                    rm
                } = data;

                const r = req.body;

                r.ah.ma = Math.min(r.ah.ma, rm.ah.ma);
                r.ah.mi = Math.max(r.ah.mi, rm.ah.mi);

                r.ou.ma = Math.min(r.ou.ma, rm.ou.ma);
                r.ou.mi = Math.max(r.ou.mi, rm.ou.mi);

                r.oe.ma = Math.min(r.oe.ma, rm.oe.ma);
                r.oe.mi = Math.max(r.oe.mi, rm.oe.mi);

                r.ml.ma = Math.min(r.ml.ma, rm.ml.ma);
                r.ml.mi = Math.max(r.ml.mi, rm.ml.mi);

                r.t1ou.ma = Math.min(r.t1ou.ma, rm.t1ou.ma);
                r.t1ou.mi = Math.max(r.t1ou.mi, rm.t1ou.mi);

                r.t2ou.ma = Math.min(r.t2ou.ma, rm.t2ou.ma);
                r.t2ou.mi = Math.max(r.t2ou.mi, rm.t2ou.mi);

                r.pm = Math.min(r.pm, rm.pm);


                BasketballModel.update({
                        k: league_key
                    },
                    {
                        r: r
                    },
                    {
                        multi: false
                    },
                    (err, data) => {
                        if (err) {
                            console.log(`[5.4] Error update league : ${league_key}`);
                            console.log(err);
                            return res.send({
                                message: err,
                                code: 9999
                            });
                        } else if (data.nModified === 0) {
                            console.log(`[5.5] update league fail : ${league_key}`);
                            return res.send({
                                message: `Update league fail : ${league_key}`,
                                code: 0
                            });
                        } else {
                            console.log(`[5.6] update league success : ${league_key}`);
                            return res.send({
                                code: 0,
                                message: "success"
                            });
                        }
                    });
            }
        });
});
//[5.1]
router.put('/set_default_rule_early_market_league/:id', (req, res) =>{
    const league_key = req.params.id;
    if (!league_key) {
        console.log('[5.1] 900001 : ==> Parameter is incorrect.');
        return res.send({
            code: 900001,
            message: 'Parameter is incorrect.'
        });
    }

    BasketballModel.findOne({
        k: league_key
    })
        .lean()
        .exec((err, data) => {
            if (err) {
                console.log(`[5.2] Error update league : ${league_key}`);
                console.log(err);
                return res.send({
                    message: err,
                    code: 9999
                });
            }  else if (!data) {
                console.log(`[5.3] update league fail : ${league_key}`);
                return res.send({
                    message: `Update league fail : ${league_key}`,
                    code: 0
                });
            } else {
                const {
                    rmem
                } = data;

                const rem = req.body;

                rem.ah.ma = Math.min(rem.ah.ma, rmem.ah.ma);
                rem.ah.mi = Math.max(rem.ah.mi, rmem.ah.mi);

                rem.ou.ma = Math.min(rem.ou.ma, rmem.ou.ma);
                rem.ou.mi = Math.max(rem.ou.mi, rmem.ou.mi);

                rem.oe.ma = Math.min(rem.oe.ma, rmem.oe.ma);
                rem.oe.mi = Math.max(rem.oe.mi, rmem.oe.mi);

                rem.ml.ma = Math.min(rem.ml.ma, rmem.ml.ma);
                rem.ml.mi = Math.max(rem.ml.mi, rmem.ml.mi);

                rem.t1ou.ma = Math.min(rem.t1ou.ma, rmem.t1ou.ma);
                rem.t1ou.mi = Math.max(rem.t1ou.mi, rmem.t1ou.mi);

                rem.t2ou.ma = Math.min(rem.t2ou.ma, rmem.t2ou.ma);
                rem.t2ou.mi = Math.max(rem.t2ou.mi, rmem.t2ou.mi);

                rem.pm = Math.min(rem.pm, rmem.pm);

                BasketballModel.update({
                        k: league_key
                    },
                    {
                        rem: rem
                    },
                    {
                        multi: false
                    },
                    (err, data) => {
                        if (err) {
                            console.log(`[5.4] Error update league : ${league_key}`);
                            console.log(err);
                            return res.send({
                                message: err,
                                code: 9999
                            });
                        } else if (data.nModified === 0) {
                            console.log(`[5.5] update league fail : ${league_key}`);
                            return res.send({
                                message: `Update league fail : ${league_key}`,
                                code: 0
                            });
                        } else {
                            console.log(`[5.6] update league success : ${league_key}`);
                            return res.send({
                                code: 0,
                                message: "success"
                            });
                        }
                    });
            }
        });
});
//[5.2]
router.put('/set_default_rule_live_league/:id', (req, res) =>{
    const league_key = req.params.id;
    if (!league_key) {
        console.log('[5.1] 900001 : ==> Parameter is incorrect.');
        return res.send({
            code: 900001,
            message: 'Parameter is incorrect.'
        });
    }

    BasketballModel.findOne({
        k: league_key
    })
        .lean()
        .exec((err, data) => {
            if (err) {
                console.log(`[5.2] Error update league : ${league_key}`);
                console.log(err);
                return res.send({
                    message: err,
                    code: 9999
                });
            }  else if (!data) {
                console.log(`[5.3] update league fail : ${league_key}`);
                return res.send({
                    message: `Update league fail : ${league_key}`,
                    code: 0
                });
            } else {
                const {
                    rlm
                } = data;

                const rl = req.body;

                rl.ah.ma = Math.min(rl.ah.ma, rlm.ah.ma);
                rl.ah.mi = Math.max(rl.ah.mi, rlm.ah.mi);

                rl.ou.ma = Math.min(rl.ou.ma, rlm.ou.ma);
                rl.ou.mi = Math.max(rl.ou.mi, rlm.ou.mi);

                rl.oe.ma = Math.min(rl.oe.ma, rlm.oe.ma);
                rl.oe.mi = Math.max(rl.oe.mi, rlm.oe.mi);

                rl.ml.ma = Math.min(rl.ml.ma, rlm.ml.ma);
                rl.ml.mi = Math.max(rl.ml.mi, rlm.ml.mi);

                rl.t1ou.ma = Math.min(rl.t1ou.ma, rlm.t1ou.ma);
                rl.t1ou.mi = Math.max(rl.t1ou.mi, rlm.t1ou.mi);

                rl.t2ou.ma = Math.min(rl.t2ou.ma, rlm.t2ou.ma);
                rl.t2ou.mi = Math.max(rl.t2ou.mi, rlm.t2ou.mi);

                rl.pm = Math.min(rl.pm, rlm.pm);

                BasketballModel.update({
                        k: league_key
                    },
                    {
                        rl: rl
                    },
                    {
                        multi: false
                    },
                    (err, data) => {
                        if (err) {
                            console.log(`[5.4] Error update league : ${league_key}`);
                            console.log(err);
                            return res.send({
                                message: err,
                                code: 9999
                            });
                        } else if (data.nModified === 0) {
                            console.log(`[5.5] update league fail : ${league_key}`);
                            return res.send({
                                message: `Update league fail : ${league_key}`,
                                code: 0
                            });
                        } else {
                            console.log(`[5.6] update league success : ${league_key}`);
                            return res.send({
                                code: 0,
                                message: "success"
                            });
                        }
                    });
            }
        });
});


module.exports = router;