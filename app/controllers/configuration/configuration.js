const FootballModel = require('../../models/football.model');
const express = require('express');
const router  = express.Router();
const _ = require('underscore');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const async = require("async");

const RedisService = require('../../common/redisService');

//[1]
router.put('/hdp_price/:id', (req, res) => {
    const league_key = req.params.id;

    if (!league_key) {
        console.log('[1.1] 900001 : ==> Parameter is incorrect.');
        return res.send({
            code: 900001,
            message: 'Parameter is incorrect.'
        });
    }

    const {
        ah,
        ou,
        x12,
        oe,
        ah1st,
        ou1st,
        x121st,
        oe1st
    } = req.body;

    FootballModel.update({
            k: league_key
        },
        {
            'rp.hdp.ah' : ah,
            'rp.hdp.ou' : ou,
            'rp.hdp.x12' : x12,
            'rp.hdp.oe' : oe,
            'rp.hdp.ah1st' : ah1st,
            'rp.hdp.ou1st' : ou1st,
            'rp.hdp.x121st' : x121st,
            'rp.hdp.oe1st' : oe1st
        },
        {
            multi: false
        },
        (err, data) => {
            if (err) {
                console.log(`[1.1] Error update league : ${league_key}`);
                console.log(err);
                return res.send({
                    message: err,
                    code: 9999
                });
            } else if (data.nModified === 0) {
                console.log(`[1.2] update league fail : ${league_key}`);
                return res.send({
                    message: `Update league fail : ${league_key}`,
                    code: 0
                });
            } else {
                console.log(`[1.3] update league success : ${league_key}`);
                return res.send({
                    code: 0,
                    message: "success"
                });
            }
        });
});

//[2]
router.put('/live_price/:id', (req, res) => {
    const league_key = req.params.id;

    if (!league_key) {
        console.log('[2.1] 900001 : ==> Parameter is incorrect.');
        return res.send({
            code: 900001,
            message: 'Parameter is incorrect.'
        });
    }

    const {
        ah,
        ou,
        x12,
        oe,
        ah1st,
        ou1st,
        x121st,
        oe1st
    } = req.body;

    FootballModel.update({
            k: league_key
        },
        {
            'rp.live.ah' : ah,
            'rp.live.ou' : ou,
            'rp.live.x12' : x12,
            'rp.live.oe' : oe,
            'rp.live.ah1st' : ah1st,
            'rp.live.ou1st' : ou1st,
            'rp.live.x121st' : x121st,
            'rp.live.oe1st' : oe1st
        },
        {
            multi: false
        },
        (err, data) => {
            if (err) {
                console.log(`[2.1] Error update league : ${league_key}`);
                console.log(err);
                return res.send({
                    message: err,
                    code: 9999
                });
            } else if (data.nModified === 0) {
                console.log(`[2.2] update league fail : ${league_key}`);
                return res.send({
                    message: `Update league fail : ${league_key}`,
                    code: 0
                });
            } else {
                console.log(`[2.3] update league success : ${league_key}`);
                return res.send({
                    code: 0,
                    message: "success"
                });
            }
        });
});

//[3]
router.get('/league', (req, res) => {
    async.series([
        (callback) => {
            FootballModel.find({})
                .lean()
                .sort('sk')
                .exec((error, data) => {
                    if (error) {
                        callback(null, error);
                    } else {
                        callback(null, data);
                    }
                });
        },

        (callback) => {
            RedisService.getCurrency((error, result) => {
                if (error) {
                    callback(null, false);
                } else {
                    callback(null, result);
                }
            })
        }

    ], (errAsync, result)=> {
        if (errAsync) {
            return res.send({
                message: "error",
                result: errAsync,
                code: 998
            });
        } else {
            const [
                data,
                currency
            ] = result;

            const resultResponse = _.chain(data)
                .each(league => {
                    league.m = [];
                })
                .each(league => {
                    const {
                        r,
                        rl,
                        rem
                    } = league;

                    const currencyName = _.chain(currency)
                        .map(obj => {
                            return obj.currencyName;
                        })
                        .value();


                    _.each(currencyName, objCurrencyName => {
                        const r_obj = {};
                        let membercurrency = objCurrencyName;

                        r_obj.ah = {};
                        r_obj.ou = {};
                        r_obj.x12 = {};
                        r_obj.oe = {};
                        r_obj.ah1st = {};
                        r_obj.ou1st = {};
                        r_obj.x121st = {};
                        r_obj.oe1st = {};
                        r_obj.pm = {};

                        r_obj.ah.ma = convertMinMaxByCurrency(r.ah.ma, currency, membercurrency);
                        r_obj.ah.mi = convertMinMaxByCurrency(r.ah.mi, currency, membercurrency);

                        r_obj.ou.ma = convertMinMaxByCurrency(r.ou.ma, currency, membercurrency);
                        r_obj.ou.mi = convertMinMaxByCurrency(r.ou.mi, currency, membercurrency);

                        r_obj.x12.ma = convertMinMaxByCurrency(r.x12.ma, currency, membercurrency);
                        r_obj.x12.mi = convertMinMaxByCurrency(r.x12.mi, currency, membercurrency);

                        r_obj.oe.ma = convertMinMaxByCurrency(r.oe.ma, currency, membercurrency);
                        r_obj.oe.mi = convertMinMaxByCurrency(r.oe.mi, currency, membercurrency);

                        r_obj.ah1st.ma = convertMinMaxByCurrency(r.ah1st.ma, currency, membercurrency);
                        r_obj.ah1st.mi = convertMinMaxByCurrency(r.ah1st.mi, currency, membercurrency);

                        r_obj.ou1st.ma = convertMinMaxByCurrency(r.ou1st.ma, currency, membercurrency);
                        r_obj.ou1st.mi = convertMinMaxByCurrency(r.ou1st.mi, currency, membercurrency);

                        r_obj.x121st.ma = convertMinMaxByCurrency(r.x121st.ma, currency, membercurrency);
                        r_obj.x121st.mi = convertMinMaxByCurrency(r.x121st.mi, currency, membercurrency);

                        r_obj.oe1st.ma = convertMinMaxByCurrency(r.oe1st.ma, currency, membercurrency);
                        r_obj.oe1st.mi = convertMinMaxByCurrency(r.oe1st.mi, currency, membercurrency);

                        r_obj.pm = convertMinMaxByCurrency(r.pm, currency, membercurrency);

                        const rl_obj = {};
                        rl_obj.ah = {};
                        rl_obj.ou = {};
                        rl_obj.x12 = {};
                        rl_obj.oe = {};
                        rl_obj.ah1st = {};
                        rl_obj.ou1st = {};
                        rl_obj.x121st = {};
                        rl_obj.oe1st = {};
                        rl_obj.pm = {};

                        rl_obj.ah.ma = convertMinMaxByCurrency(rl.ah.ma, currency, membercurrency);
                        rl_obj.ah.mi = convertMinMaxByCurrency(rl.ah.mi, currency, membercurrency);

                        rl_obj.ou.ma = convertMinMaxByCurrency(rl.ou.ma, currency, membercurrency);
                        rl_obj.ou.mi = convertMinMaxByCurrency(rl.ou.mi, currency, membercurrency);

                        rl_obj.x12.ma = convertMinMaxByCurrency(rl.x12.ma, currency, membercurrency);
                        rl_obj.x12.mi = convertMinMaxByCurrency(rl.x12.mi, currency, membercurrency);

                        rl_obj.oe.ma = convertMinMaxByCurrency(rl.oe.ma, currency, membercurrency);
                        rl_obj.oe.mi = convertMinMaxByCurrency(rl.oe.mi, currency, membercurrency);

                        rl_obj.ah1st.ma = convertMinMaxByCurrency(rl.ah1st.ma, currency, membercurrency);
                        rl_obj.ah1st.mi = convertMinMaxByCurrency(rl.ah1st.mi, currency, membercurrency);

                        rl_obj.ou1st.ma = convertMinMaxByCurrency(rl.ou1st.ma, currency, membercurrency);
                        rl_obj.ou1st.mi = convertMinMaxByCurrency(rl.ou1st.mi, currency, membercurrency);

                        rl_obj.x121st.ma = convertMinMaxByCurrency(rl.x121st.ma, currency, membercurrency);
                        rl_obj.x121st.mi = convertMinMaxByCurrency(rl.x121st.mi, currency, membercurrency);

                        rl_obj.oe1st.ma = convertMinMaxByCurrency(rl.oe1st.ma, currency, membercurrency);
                        rl_obj.oe1st.mi = convertMinMaxByCurrency(rl.oe1st.mi, currency, membercurrency);

                        rl_obj.pm = convertMinMaxByCurrency(rl.pm, currency, membercurrency);

                        const rem_obj = {};
                        rem_obj.ah = {};
                        rem_obj.ou = {};
                        rem_obj.x12 = {};
                        rem_obj.oe = {};
                        rem_obj.ah1st = {};
                        rem_obj.ou1st = {};
                        rem_obj.x121st = {};
                        rem_obj.oe1st = {};
                        rem_obj.pm = {};

                        rem_obj.ah.ma = convertMinMaxByCurrency(rem.ah.ma, currency, membercurrency);
                        rem_obj.ah.mi = convertMinMaxByCurrency(rem.ah.mi, currency, membercurrency);

                        rem_obj.ou.ma = convertMinMaxByCurrency(rem.ou.ma, currency, membercurrency);
                        rem_obj.ou.mi = convertMinMaxByCurrency(rem.ou.mi, currency, membercurrency);

                        rem_obj.x12.ma = convertMinMaxByCurrency(rem.x12.ma, currency, membercurrency);
                        rem_obj.x12.mi = convertMinMaxByCurrency(rem.x12.mi, currency, membercurrency);

                        rem_obj.oe.ma = convertMinMaxByCurrency(rem.oe.ma, currency, membercurrency);
                        rem_obj.oe.mi = convertMinMaxByCurrency(rem.oe.mi, currency, membercurrency);

                        rem_obj.ah1st.ma = convertMinMaxByCurrency(rem.ah1st.ma, currency, membercurrency);
                        rem_obj.ah1st.mi = convertMinMaxByCurrency(rem.ah1st.mi, currency, membercurrency);

                        rem_obj.ou1st.ma = convertMinMaxByCurrency(rem.ou1st.ma, currency, membercurrency);
                        rem_obj.ou1st.mi = convertMinMaxByCurrency(rem.ou1st.mi, currency, membercurrency);

                        rem_obj.x121st.ma = convertMinMaxByCurrency(rem.x121st.ma, currency, membercurrency);
                        rem_obj.x121st.mi = convertMinMaxByCurrency(rem.x121st.mi, currency, membercurrency);

                        rem_obj.oe1st.ma = convertMinMaxByCurrency(rem.oe1st.ma, currency, membercurrency);
                        rem_obj.oe1st.mi = convertMinMaxByCurrency(rem.oe1st.mi, currency, membercurrency);

                        rem_obj.pm = convertMinMaxByCurrency(rem.pm, currency, membercurrency);

                        league[`r_${objCurrencyName}`] = r_obj;
                        league[`rl_${objCurrencyName}`] = rl_obj;
                        league[`rem_${objCurrencyName}`] = rem_obj;

                    });
                })
                .value();

            return res.send({
                code: 0,
                message: `success`,
                result: _.sortBy(resultResponse, 'sk')
            });
        }
    });
});

//[4]
router.put('/hdp_live_price/:id', (req, res) => {
    const league_key = req.params.id;

    if (!league_key) {
        console.log('[4.1] 900001 : ==> Parameter is incorrect.');
        return res.send({
            code: 900001,
            message: 'Parameter is incorrect.'
        });
    }

    const {
        hdp,
        live
    } = req.body;

    FootballModel.update({
            k: league_key
        },
        {
            'rp.hdp.ah' : hdp.ah,
            'rp.hdp.ou' : hdp.ou,
            'rp.hdp.x12' : hdp.x12,
            'rp.hdp.oe' : hdp.oe,
            'rp.hdp.ah1st' : hdp.ah1st,
            'rp.hdp.ou1st' : hdp.ou1st,
            'rp.hdp.x121st' : hdp.x121st,
            'rp.hdp.oe1st' : hdp.oe1st,
            'rp.live.ah' : live.ah,
            'rp.live.ou' : live.ou,
            'rp.live.x12' : live.x12,
            'rp.live.oe' : live.oe,
            'rp.live.ah1st' : live.ah1st,
            'rp.live.ou1st' : live.ou1st,
            'rp.live.x121st' : live.x121st,
            'rp.live.oe1st' : live.oe1st
        },
        {
            multi: false
        },
        (err, data) => {
            if (err) {
                console.log(`[4.1] Error update league : ${league_key}`);
                console.log(err);
                return res.send({
                    message: err,
                    code: 9999
                });
            } else if (data.nModified === 0) {
                console.log(`[4.2] update league fail : ${league_key}`);
                return res.send({
                    message: `Update league fail : ${league_key}`,
                    code: 0
                });
            } else {
                console.log(`[4.3] update league success : ${league_key}`);
                return res.send({
                    code: 0,
                    message: "success"
                });
            }
        });
});

//[5]
router.put('/set_default_rule_league/:id', (req, res) =>{
    const league_key = req.params.id;
    if (!league_key) {
        console.log('[5.1] 900001 : ==> Parameter is incorrect.');
        return res.send({
            code: 900001,
            message: 'Parameter is incorrect.'
        });
    }

    FootballModel.findOne({
        k: league_key
    })
        .lean()
        .exec((err, data) => {
            if (err) {
                console.log(`[5.2] Error update league : ${league_key}`);
                console.log(err);
                return res.send({
                    message: err,
                    code: 9999
                });
            }  else if (!data) {
                console.log(`[5.3] update league fail : ${league_key}`);
                return res.send({
                    message: `Update league fail : ${league_key}`,
                    code: 0
                });
            } else {
                const {
                    m,
                    rm
                } = data;

                const r = req.body;

                r.ah.ma = Math.min(r.ah.ma, rm.ah.ma);
                r.ah.mi = Math.max(r.ah.mi, rm.ah.mi);

                r.ou.ma = Math.min(r.ou.ma, rm.ou.ma);
                r.ou.mi = Math.max(r.ou.mi, rm.ou.mi);

                r.x12.ma = Math.min(r.x12.ma, rm.x12.ma);
                r.x12.mi = Math.max(r.x12.mi, rm.x12.mi);

                r.oe.ma = Math.min(r.oe.ma, rm.oe.ma);
                r.oe.mi = Math.max(r.oe.mi, rm.oe.mi);

                r.ah1st.ma = Math.min(r.ah1st.ma, rm.ah1st.ma);
                r.ah1st.mi = Math.max(r.ah1st.mi, rm.ah1st.mi);

                r.ou1st.ma = Math.min(r.ou1st.ma, rm.ou1st.ma);
                r.ou1st.mi = Math.max(r.ou1st.mi, rm.ou1st.mi);

                r.x121st.ma = Math.min(r.x121st.ma, rm.x121st.ma);
                r.x121st.mi = Math.max(r.x121st.mi, rm.x121st.mi);

                r.oe1st.ma = Math.min(r.oe1st.ma, rm.oe1st.ma);
                r.oe1st.mi = Math.max(r.oe1st.mi, rm.oe1st.mi);


                r.pm = Math.min(r.pm, rm.pm);

                _.each(m, match => {
                    match.r.pm = Math.min(match.r.pm, r.pm);
                    let round = 0;
                    _.each(match.bp, betPrice => {

                        betPrice.r.ah.ma = calculateBetPrice(r.ah.ma, round);
                        betPrice.r.ah.mi = r.ah.mi;

                        betPrice.r.ou.ma = calculateBetPrice(r.ou.ma, round);
                        betPrice.r.ou.mi = r.ou.mi;

                        betPrice.r.x12.ma = calculateBetPrice(r.x12.ma, round);
                        betPrice.r.x12.mi = r.x12.mi;

                        betPrice.r.oe.ma = calculateBetPrice(r.oe.ma, round);
                        betPrice.r.oe.mi = r.oe.mi;

                        betPrice.r.ah1st.ma = calculateBetPrice(r.ah1st.ma, round);
                        betPrice.r.ah1st.mi = r.ah1st.mi;

                        betPrice.r.ou1st.ma = calculateBetPrice(r.ou1st.ma, round);
                        betPrice.r.ou1st.mi = r.ou1st.mi;

                        betPrice.r.x121st.ma = calculateBetPrice(r.x121st.ma, round);
                        betPrice.r.x121st.mi = r.x121st.mi;

                        betPrice.r.oe1st.ma = calculateBetPrice(r.oe1st.ma, round);
                        betPrice.r.oe1st.mi = r.oe1st.mi;

                        round++;
                    })
                });

                FootballModel.update({
                        k: league_key
                    },
                    {
                        r: r,
                        m: m
                    },
                    {
                        multi: false
                    },
                    (err, data) => {
                        if (err) {
                            console.log(`[5.4] Error update league : ${league_key}`);
                            console.log(err);
                            return res.send({
                                message: err,
                                code: 9999
                            });
                        } else if (data.nModified === 0) {
                            console.log(`[5.5] update league fail : ${league_key}`);
                            return res.send({
                                message: `Update league fail : ${league_key}`,
                                code: 0
                            });
                        } else {
                            console.log(`[5.6] update league success : ${league_key}`);
                            return res.send({
                                code: 0,
                                message: "success"
                            });
                        }
                    });
            }
        });
});
//[5.1]
router.put('/set_default_rule_early_market_league/:id', (req, res) =>{
    const league_key = req.params.id;
    if (!league_key) {
        console.log('[5.1] 900001 : ==> Parameter is incorrect.');
        return res.send({
            code: 900001,
            message: 'Parameter is incorrect.'
        });
    }

    FootballModel.findOne({
        k: league_key
    })
        .lean()
        .exec((err, data) => {
            if (err) {
                console.log(`[5.2] Error update league : ${league_key}`);
                console.log(err);
                return res.send({
                    message: err,
                    code: 9999
                });
            }  else if (!data) {
                console.log(`[5.3] update league fail : ${league_key}`);
                return res.send({
                    message: `Update league fail : ${league_key}`,
                    code: 0
                });
            } else {
                const {
                    m,
                    rmem
                } = data;

                const rem = req.body;

                rem.ah.ma = Math.min(rem.ah.ma, rmem.ah.ma);
                rem.ah.mi = Math.max(rem.ah.mi, rmem.ah.mi);

                rem.ou.ma = Math.min(rem.ou.ma, rmem.ou.ma);
                rem.ou.mi = Math.max(rem.ou.mi, rmem.ou.mi);

                rem.x12.ma = Math.min(rem.x12.ma, rmem.x12.ma);
                rem.x12.mi = Math.max(rem.x12.mi, rmem.x12.mi);

                rem.oe.ma = Math.min(rem.oe.ma, rmem.oe.ma);
                rem.oe.mi = Math.max(rem.oe.mi, rmem.oe.mi);

                rem.ah1st.ma = Math.min(rem.ah1st.ma, rmem.ah1st.ma);
                rem.ah1st.mi = Math.max(rem.ah1st.mi, rmem.ah1st.mi);

                rem.ou1st.ma = Math.min(rem.ou1st.ma, rmem.ou1st.ma);
                rem.ou1st.mi = Math.max(rem.ou1st.mi, rmem.ou1st.mi);

                rem.x121st.ma = Math.min(rem.x121st.ma, rmem.x121st.ma);
                rem.x121st.mi = Math.max(rem.x121st.mi, rmem.x121st.mi);

                rem.oe1st.ma = Math.min(rem.oe1st.ma, rmem.oe1st.ma);
                rem.oe1st.mi = Math.max(rem.oe1st.mi, rmem.oe1st.mi);


                rem.pm = Math.min(rem.pm, rmem.pm);

                _.each(m, match => {
                    match.rem.pm = Math.min(match.rem.pm, rem.pm);

                    let round = 0;
                    _.each(match.bp, betPrice => {

                        betPrice.rem.ah.ma = calculateBetPrice(rem.ah.ma, round);
                        betPrice.rem.ah.mi = rem.ah.mi;

                        betPrice.rem.ou.ma = calculateBetPrice(rem.ou.ma, round);
                        betPrice.rem.ou.mi = rem.ou.mi;

                        betPrice.rem.x12.ma = calculateBetPrice(rem.x12.ma, round);
                        betPrice.rem.x12.mi = rem.x12.mi;

                        betPrice.rem.oe.ma = calculateBetPrice(rem.oe.ma, round);
                        betPrice.rem.oe.mi = rem.oe.mi;

                        betPrice.rem.ah1st.ma = calculateBetPrice(rem.ah1st.ma, round);
                        betPrice.rem.ah1st.mi = rem.ah1st.mi;

                        betPrice.rem.ou1st.ma = calculateBetPrice(rem.ou1st.ma, round);
                        betPrice.rem.ou1st.mi = rem.ou1st.mi;

                        betPrice.rem.x121st.ma = calculateBetPrice(rem.x121st.ma, round);
                        betPrice.rem.x121st.mi = rem.x121st.mi;

                        betPrice.rem.oe1st.ma = calculateBetPrice(rem.oe1st.ma, round);
                        betPrice.rem.oe1st.mi = rem.oe1st.mi;

                        round++;
                    });
                });

                FootballModel.update({
                        k: league_key
                    },
                    {
                        rem: rem,
                        m: m
                    },
                    {
                        multi: false
                    },
                    (err, data) => {
                        if (err) {
                            console.log(`[5.4] Error update league : ${league_key}`);
                            console.log(err);
                            return res.send({
                                message: err,
                                code: 9999
                            });
                        } else if (data.nModified === 0) {
                            console.log(`[5.5] update league fail : ${league_key}`);
                            return res.send({
                                message: `Update league fail : ${league_key}`,
                                code: 0
                            });
                        } else {
                            console.log(`[5.6] update league success : ${league_key}`);
                            return res.send({
                                code: 0,
                                message: "success"
                            });
                        }
                    });
            }
        });
});
//[5.2]
router.put('/set_default_rule_live_league/:id', (req, res) =>{
    const league_key = req.params.id;
    if (!league_key) {
        console.log('[5.1] 900001 : ==> Parameter is incorrect.');
        return res.send({
            code: 900001,
            message: 'Parameter is incorrect.'
        });
    }

    FootballModel.findOne({
        k: league_key
    })
        .lean()
        .exec((err, data) => {
            if (err) {
                console.log(`[5.2] Error update league : ${league_key}`);
                console.log(err);
                return res.send({
                    message: err,
                    code: 9999
                });
            }  else if (!data) {
                console.log(`[5.3] update league fail : ${league_key}`);
                return res.send({
                    message: `Update league fail : ${league_key}`,
                    code: 0
                });
            } else {
                const {
                    m,
                    rlm
                } = data;

                const rl = req.body;

                rl.ah.ma = Math.min(rl.ah.ma, rlm.ah.ma);
                rl.ah.mi = Math.max(rl.ah.mi, rlm.ah.mi);

                rl.ou.ma = Math.min(rl.ou.ma, rlm.ou.ma);
                rl.ou.mi = Math.max(rl.ou.mi, rlm.ou.mi);

                rl.x12.ma = Math.min(rl.x12.ma, rlm.x12.ma);
                rl.x12.mi = Math.max(rl.x12.mi, rlm.x12.mi);

                rl.oe.ma = Math.min(rl.oe.ma, rlm.oe.ma);
                rl.oe.mi = Math.max(rl.oe.mi, rlm.oe.mi);

                rl.ah1st.ma = Math.min(rl.ah1st.ma, rlm.ah1st.ma);
                rl.ah1st.mi = Math.max(rl.ah1st.mi, rlm.ah1st.mi);

                rl.ou1st.ma = Math.min(rl.ou1st.ma, rlm.ou1st.ma);
                rl.ou1st.mi = Math.max(rl.ou1st.mi, rlm.ou1st.mi);

                rl.x121st.ma = Math.min(rl.x121st.ma, rlm.x121st.ma);
                rl.x121st.mi = Math.max(rl.x121st.mi, rlm.x121st.mi);

                rl.oe1st.ma = Math.min(rl.oe1st.ma, rlm.oe1st.ma);
                rl.oe1st.mi = Math.max(rl.oe1st.mi, rlm.oe1st.mi);


                rl.pm = Math.min(rl.pm, rlm.pm);


                _.each(m, match => {
                    match.rl.pm = Math.min(match.rl.pm, rl.pm);
                    let round = 0;
                    _.each(match.bpl, betPrice => {

                        betPrice.r.ah.ma = calculateBetPrice(rl.ah.ma, round);
                        betPrice.r.ah.mi = rl.ah.mi;

                        betPrice.r.ou.ma = calculateBetPrice(rl.ou.ma, round);
                        betPrice.r.ou.mi = rl.ou.mi;

                        betPrice.r.x12.ma = calculateBetPrice(rl.x12.ma, round);
                        betPrice.r.x12.mi = rl.x12.mi;

                        betPrice.r.oe.ma = calculateBetPrice(rl.oe.ma, round);
                        betPrice.r.oe.mi = rl.oe.mi;

                        betPrice.r.ah1st.ma = calculateBetPrice(rl.ah1st.ma, round);
                        betPrice.r.ah1st.mi = rl.ah1st.mi;

                        betPrice.r.ou1st.ma = calculateBetPrice(rl.ou1st.ma, round);
                        betPrice.r.ou1st.mi = rl.ou1st.mi;

                        betPrice.r.x121st.ma = calculateBetPrice(rl.x121st.ma, round);
                        betPrice.r.x121st.mi = rl.x121st.mi;

                        betPrice.r.oe1st.ma = calculateBetPrice(rl.oe1st.ma, round);
                        betPrice.r.oe1st.mi = rl.oe1st.mi;

                        round++;
                    })
                });

                FootballModel.update({
                        k: league_key
                    },
                    {
                        rl: rl,
                        m: m
                    },
                    {
                        multi: false
                    },
                    (err, data) => {
                        if (err) {
                            console.log(`[5.4] Error update league : ${league_key}`);
                            console.log(err);
                            return res.send({
                                message: err,
                                code: 9999
                            });
                        } else if (data.nModified === 0) {
                            console.log(`[5.5] update league fail : ${league_key}`);
                            return res.send({
                                message: `Update league fail : ${league_key}`,
                                code: 0
                            });
                        } else {
                            console.log(`[5.6] update league success : ${league_key}`);
                            return res.send({
                                code: 0,
                                message: "success"
                            });
                        }
                    });
            }
        });
});

//[5.3]
router.put('/set_spacial_rule_match', (req, res) => {
    const {
        id,
        rule
    } = req.body;

    const league_key = parseInt(_.first(id.split(':')));
    const match_key = parseInt(_.last(id.split(':')));

    FootballModel.aggregate(
        {
            $match: {
                k: league_key
            }
        },
        {
            $unwind: '$m'
        },
        {
            $match: {
                'm.k': match_key
            }
        },
        (err, data) => {
            if (err) {
                console.log('[1.1] 999 : ==> \n', err);
                return res.send({
                    message: err,
                    code: 9999
                });

            }  else if (_.size(data) === 0) {
                console.log(`[1.2] Match : ${league_key} ${match_key} not found`);
                return res.send({
                    message: `Match id ${league_key} ${match_key} not found`,
                    code: 9998
                });

            } else {
                const {
                    r
                } = _.first(data);

                const sr = r;

                sr.pm = Math.min(sr.pm, rule.pm);

                sr.ah.ma = Math.min(sr.ah.ma, rule.ah.ma);
                sr.ah.mi = Math.max(sr.ah.mi, rule.ah.mi);

                sr.ou.ma = Math.min(sr.ou.ma, rule.ou.ma);
                sr.ou.mi = Math.max(sr.ou.mi, rule.ou.mi);

                sr.x12.ma = Math.min(sr.x12.ma, rule.x12.ma);
                sr.x12.mi = Math.max(sr.x12.mi, rule.x12.mi);

                sr.oe.ma = Math.min(sr.oe.ma, rule.oe.ma);
                sr.oe.mi = Math.max(sr.oe.mi, rule.oe.mi);

                sr.ah1st.ma = Math.min(sr.ah1st.ma, rule.ah1st.ma);
                sr.ah1st.mi = Math.max(sr.ah1st.mi, rule.ah1st.mi);

                sr.ou1st.ma = Math.min(sr.ou1st.ma, rule.ou1st.ma);
                sr.ou1st.mi = Math.max(sr.ou1st.mi, rule.ou1st.mi);

                sr.x121st.ma = Math.min(sr.x121st.ma, rule.x121st.ma);
                sr.x121st.mi = Math.max(sr.x121st.mi, rule.x121st.mi);

                sr.oe1st.ma = Math.min(sr.oe1st.ma, rule.oe1st.ma);
                sr.oe1st.mi = Math.max(sr.oe1st.mi, rule.oe1st.mi);

                sr.isOn = true;
                if (!_.isUndefined(rule.isOn)) {
                    sr.isOn = rule.isOn;
                }

                FootballModel.update({
                        k: league_key,
                        m: {
                            $elemMatch: {
                                k: match_key
                            }
                        }
                    },
                    {
                        $set: {
                            'm.$.sr': sr
                        }
                    },
                    {
                        multi: false
                    },
                    (err, data) => {
                        if (err) {
                            console.log(`[8.3.2.1] Error update match bp : ${league_key} ${match_key}`);
                            console.log(err);
                        } else if (data.nModified === 0) {
                            console.log(`[8.3.2.2] insert update bp fail : ${league_key} ${match_key}`);
                        } else {
                            console.log(`[8.3.2.3] insert update bp success : ${league_key} ${match_key}`);
                        }
                    });

                return res.send({
                    message: `success`,
                    code: 0,
                    result: sr
                });
            }
        });
});
//[5.4]
router.put('/set_spacial_rule_early_market_match', (req, res) => {
    const {
        id,
        rule
    } = req.body;

    const league_key = parseInt(_.first(id.split(':')));
    const match_key = parseInt(_.last(id.split(':')));

    FootballModel.aggregate(
        {
            $match: {
                k: league_key
            }
        },
        {
            $unwind: '$m'
        },
        {
            $match: {
                'm.k': match_key
            }
        },
        (err, data) => {
            if (err) {
                console.log('[1.1] 999 : ==> \n', err);
                return res.send({
                    message: err,
                    code: 9999
                });

            }  else if (_.size(data) === 0) {
                console.log(`[1.2] Match : ${league_key} ${match_key} not found`);
                return res.send({
                    message: `Match id ${league_key} ${match_key} not found`,
                    code: 9998
                });

            } else {
                const {
                    rem
                } = _.first(data);

                const srem = rem;

                srem.pm = Math.min(rem.pm, rule.pm);

                srem.ah.ma = Math.min(srem.ah.ma, rule.ah.ma);
                srem.ah.mi = Math.max(srem.ah.mi, rule.ah.mi);

                srem.ou.ma = Math.min(srem.ou.ma, rule.ou.ma);
                srem.ou.mi = Math.max(srem.ou.mi, rule.ou.mi);

                srem.x12.ma = Math.min(srem.x12.ma, rule.x12.ma);
                srem.x12.mi = Math.max(srem.x12.mi, rule.x12.mi);

                srem.oe.ma = Math.min(srem.oe.ma, rule.oe.ma);
                srem.oe.mi = Math.max(srem.oe.mi, rule.oe.mi);

                srem.ah1st.ma = Math.min(srem.ah1st.ma, rule.ah1st.ma);
                srem.ah1st.mi = Math.max(srem.ah1st.mi, rule.ah1st.mi);

                srem.ou1st.ma = Math.min(srem.ou1st.ma, rule.ou1st.ma);
                srem.ou1st.mi = Math.max(srem.ou1st.mi, rule.ou1st.mi);

                srem.x121st.ma = Math.min(srem.x121st.ma, rule.x121st.ma);
                srem.x121st.mi = Math.max(srem.x121st.mi, rule.x121st.mi);

                srem.oe1st.ma = Math.min(srem.oe1st.ma, rule.oe1st.ma);
                srem.oe1st.mi = Math.max(srem.oe1st.mi, rule.oe1st.mi);

                srem.isOn = true;
                if (!_.isUndefined(rule.isOn)) {
                    srem.isOn = rule.isOn;
                }

                FootballModel.update({
                        k: league_key,
                        m: {
                            $elemMatch: {
                                k: match_key
                            }
                        }
                    },
                    {
                        $set: {
                            'm.$.srem': srem
                        }
                    },
                    {
                        multi: false
                    },
                    (err, data) => {
                        if (err) {
                            console.log(`[8.3.2.1] Error update match bp : ${league_key} ${match_key}`);
                            console.log(err);
                        } else if (data.nModified === 0) {
                            console.log(`[8.3.2.2] insert update bp fail : ${league_key} ${match_key}`);
                        } else {
                            console.log(`[8.3.2.3] insert update bp success : ${league_key} ${match_key}`);
                        }
                    });

                return res.send({
                    message: `success`,
                    code: 0,
                    result: srem
                });
            }
        });
});
//[5.5]
router.put('/set_spacial_rule_live_match', (req, res) => {
    const {
        id,
        rule
    } = req.body;

    const league_key = parseInt(_.first(id.split(':')));
    const match_key = parseInt(_.last(id.split(':')));

    FootballModel.aggregate(
        {
            $match: {
                k: league_key
            }
        },
        {
            $unwind: '$m'
        },
        {
            $match: {
                'm.k': match_key
            }
        },
        (err, data) => {
            if (err) {
                console.log('[1.1] 999 : ==> \n', err);
                return res.send({
                    message: err,
                    code: 9999
                });

            }  else if (_.size(data) === 0) {
                console.log(`[1.2] Match : ${league_key} ${match_key} not found`);
                return res.send({
                    message: `Match id ${league_key} ${match_key} not found`,
                    code: 9998
                });

            } else {
                const {
                    rl
                } = _.first(data);

                const srl = rl;

                srl.pm = Math.min(srl.pm, rule.pm);

                srl.ah.ma = Math.min(srl.ah.ma, rule.ah.ma);
                srl.ah.mi = Math.max(srl.ah.mi, rule.ah.mi);

                srl.ou.ma = Math.min(srl.ou.ma, rule.ou.ma);
                srl.ou.mi = Math.max(srl.ou.mi, rule.ou.mi);

                srl.x12.ma = Math.min(srl.x12.ma, rule.x12.ma);
                srl.x12.mi = Math.max(srl.x12.mi, rule.x12.mi);

                srl.oe.ma = Math.min(srl.oe.ma, rule.oe.ma);
                srl.oe.mi = Math.max(srl.oe.mi, rule.oe.mi);

                srl.ah1st.ma = Math.min(srl.ah1st.ma, rule.ah1st.ma);
                srl.ah1st.mi = Math.max(srl.ah1st.mi, rule.ah1st.mi);

                srl.ou1st.ma = Math.min(srl.ou1st.ma, rule.ou1st.ma);
                srl.ou1st.mi = Math.max(srl.ou1st.mi, rule.ou1st.mi);

                srl.x121st.ma = Math.min(srl.x121st.ma, rule.x121st.ma);
                srl.x121st.mi = Math.max(srl.x121st.mi, rule.x121st.mi);

                srl.oe1st.ma = Math.min(srl.oe1st.ma, rule.oe1st.ma);
                srl.oe1st.mi = Math.max(srl.oe1st.mi, rule.oe1st.mi);

                srl.isOn = true;
                if (!_.isUndefined(rule.isOn)) {
                    srl.isOn = rule.isOn;
                }

                FootballModel.update({
                        k: league_key,
                        m: {
                            $elemMatch: {
                                k: match_key
                            }
                        }
                    },
                    {
                        $set: {
                            'm.$.srl': srl
                        }
                    },
                    {
                        multi: false
                    },
                    (err, data) => {
                        if (err) {
                            console.log(`[8.3.2.1] Error update match bp : ${league_key} ${match_key}`);
                            console.log(err);
                        } else if (data.nModified === 0) {
                            console.log(`[8.3.2.2] insert update bp fail : ${league_key} ${match_key}`);
                        } else {
                            console.log(`[8.3.2.3] insert update bp success : ${league_key} ${match_key}`);
                        }
                    });

                return res.send({
                    message: `success`,
                    code: 0,
                    result: srl
                });
            }
        });
});

//[6]
router.put('/hdp_by_price', (req, res) => {

    // const league_key = req.params.id;
    const {
        id,
        bp
    } = req.body;

    const league_key = id.split(':');

    // async.map(matchs, (m, callback) => {
    FootballModel.aggregate(
        {
            $match:
                {
                    k: parseInt(_.first(league_key))
                }
        },
        {
            $unwind: "$m"
        },
        {
            $match:
                {
                    "m.id": id
                }
        },
        (err, data) => {
            if(err){
                callback(err, null)
            } else if(_.size(data) > 0){

                const league = _.first(data);

                async.map(bp, (bp, cb)=>{
                    const index = _.map(league.m.bp, (obj, index)=>{
                        if (_.isEqual(obj._id, ObjectId(bp._id))) {
                            return index;
                        }
                    }).filter(isFinite);

                    console.log(index);

                    if(_.size(index) > 0){
                        const update = {};
                        update[`m.$.bp.${index[0]}.rp`] = bp.rp;
                        update[`m.$.bp.${index[0]}.r`] = bp.r;
                        update[`m.$.bp.${index[0]}.isOn`] = bp.isOn;

                        console.log(bp.rp);

                        FootballModel.update({
                            k: _.first(league_key),
                            'm.id': id,
                            'm.bp._id': ObjectId(bp._id)
                        }, update, (err, data)=>{
                            if(err){
                                cb(err, null);
                            } else {
                                cb(null, data)
                            }
                        });
                    } else {
                        cb('_id in bp is null', null);
                    }
                },(err, result)=>{
                    if(err){
                        return res.send({
                            code: 999,
                            message: "error",
                            result : err
                        });
                    } else {


                        return res.send({
                            code: 0,
                            message: "success",
                            result : 'Update success.'
                        });
                    }
                })
            } else {
                return res.send({
                    code: 999,
                    message: "Match is null"
                });
            }
        });

});

//[7]
router.put('/live_by_price', (req, res) => {

    // const league_key = req.params.id;
    const {
        id,
        bpl
    } = req.body;

    const league_key = id.split(':');

    // async.map(matchs, (m, callback) => {
    FootballModel.aggregate(
        {
            $match:
                {
                    k: parseInt(_.first(league_key))
                }
        },
        {
            $unwind: "$m"
        },
        {
            $match:
                {
                    "m.id": id
                }
        },
        (err, data) => {
            if(err){
                callback(err, null)
            } else if(_.size(data) > 0){

                const league = _.first(data);

                async.map(bpl, (bpl, cb)=>{
                    const index = _.map(league.m.bpl, (obj, index)=>{
                        if (_.isEqual(obj._id, ObjectId(bpl._id))) {
                            return index;
                        }
                    }).filter(isFinite);

                    console.log(index);

                    if(_.size(index) > 0){
                        const update = {};
                        update[`m.$.bpl.${index[0]}.rp`] = bpl.rp;
                        update[`m.$.bpl.${index[0]}.r`] = bpl.r;
                        update[`m.$.bpl.${index[0]}.isOn`] = bpl.isOn;

                        console.log(bpl.rp);

                        FootballModel.update({
                            k: _.first(league_key),
                            'm.id': id,
                            'm.bpl._id': ObjectId(bpl._id)
                        }, update, (err, data)=>{
                            if(err){
                                cb(err, null);
                            } else {
                                cb(null, data)
                            }
                        });
                    } else {
                        cb('_id in bp is null', null);
                    }
                },(err, result)=>{
                    if(err){
                        return res.send({
                            code: 999,
                            message: "error",
                            result : err
                        });
                    } else {
                        return res.send({
                            code: 0,
                            message: "success",
                            result : 'Update success.'
                        });
                    }
                })
            } else {
                return res.send({
                    code: 999,
                    message: "Match is null"
                });
            }
        });

});

const calculateBetPrice = (price, round) => {
    let percent = 0;
    switch (round) {
        case 0:
            percent = 0;
            break;
        case 1:
            percent = 40;
            break;
        case 2:
            percent = 80;
            break;
        case 3:
            percent = 90;
            break;
        case 4:
            percent = 90;
            break;
        case 5:
            percent = 90;
            break;
    }
    return decrease(price, percent);
};
const calculateBetPriceByTime = (price, current, match) => {
    const gapHour = match.getHours() - current.getHours();
    console.log(`gapHour is ${match.getHours()} - ${current.getHours()}`);
    let percent = 0;
    if (gapHour >= 7) {
        percent = 70;
    } else if (gapHour <= 6) {
        percent = 40;
    } else {
        percent = 0;
    }
    return decrease(price, percent);
};
const decrease = (price, percent) => {
    const result = (price * ( (100 - percent) / 100)).toFixed(2);
    console.log(`${price} - ${percent}% = ${result}`);
    return result;
};

const convertMinMaxByCurrency = (value, currency, membercurrency) => {
    // console.log('###########################');
    // console.log('[BF]value      : ', value);
    // console.log('membercurrency : ', membercurrency);

    let minMax = value;
    if (_.isUndefined(membercurrency) || _.isNull(membercurrency)) {
        return minMax;
    } else {
        const predicate = JSON.parse(`{"currencyName":"${membercurrency}"}`);
        const obj = _.chain(currency)
            .findWhere(predicate)
            .pick('currencyTHB')
            .value();
        if (!_.isUndefined(obj.currencyTHB)) {
            // console.log('               : ',obj.currencyTHB);

            if (_.isEqual(membercurrency, "IDR") || _.isEqual(membercurrency, "VND")) {
                minMax = Math.ceil(value * obj.currencyTHB);
            } else {
                minMax = Math.ceil(value / obj.currencyTHB);
            }
            // console.log('[AF]value      : ', minMax);
            // console.log('###########################');
            return minMax;
        } else {
            return minMax;
        }
    }

};
module.exports = router;