const express = require('express');
const router  = express.Router();
const RedisService = require('../../common/redisService');

router.get('/', (req, res) => {
    RedisService.getCurrency((error, result) => {
        return res.send({
            code: 0,
            message: "success",
            result:result
        });
    });
});

module.exports = router;