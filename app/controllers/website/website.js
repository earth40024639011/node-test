const express  = require('express');
const router   = express.Router();
const mongoose = require('mongoose');
const _        = require('underscore');
const Joi      = require('joi');
const WebsiteModel = require('../../models/website.model');
const MarketingTool = require('../../models/marketingTool.model');
const Redis        = require('../sbo/1_common/1_redis/1_1_redis');
const NodeCache = require( "node-cache" );
const cache = new NodeCache( { stdTTL: 180, checkperiod: 181 } );

const LOG = 'WEB_SITE';
const CLIENT_NAME      = process.env.CLIENT_NAME          || 'SPORTBOOK88';
//[1]
const createValidate = Joi.object().keys({
    logo: Joi.string().required(),
    topOne: Joi.array().items(
        Joi.object().keys({
            img: Joi.string().required(),
            url: Joi.string()
        })
    ).required(),
    topTwo: Joi.array().items(
        Joi.object().keys({
            img: Joi.string().required(),
            url: Joi.string()
        })
    ).required(),
    middleOne: Joi.array().items(
        Joi.object().keys({
            img: Joi.string().required(),
            url: Joi.string()
        })
    ).required(),
    middleTwo: Joi.array().items(
        Joi.object().keys({
            img: Joi.string().required(),
            url: Joi.string()
        })
    ).required(),
    middleThree: Joi.array().items(
        Joi.object().keys({
            img: Joi.string().required(),
            url: Joi.string()
        })
    ).required(),
    middleFour: Joi.array().items(
        Joi.object().keys({
            img: Joi.string().required(),
            url: Joi.string()
        })
    ).required(),
    bottomOne: Joi.array().items(
        Joi.object().keys({
            img: Joi.string().required(),
            url: Joi.string()
        })
    ).required(),
    depositChannel: Joi.array().items(
        Joi.object().keys({
            img: Joi.string().required(),
            url: Joi.string()
        })
    ).required(),
    casinoBanner: Joi.array().items(
        Joi.object().keys({
            img: Joi.string().required(),
            url: Joi.string()
        })
    ).required(),
    ambGameBanner: Joi.array().items(
        Joi.object().keys({
            img: Joi.string().required(),
            url: Joi.string()
        })
    ).required(),
    lottoBanner: Joi.array().items(
        Joi.object().keys({
            img: Joi.string().required(),
            url: Joi.string()
        })
    ).required(),
    gameBanner: Joi.array().items(
        Joi.object().keys({
            img: Joi.string().required(),
            url: Joi.string()
        })
    ).required(),
    sportBanner: Joi.array().items(
        Joi.object().keys({
            img: Joi.string().required(),
            url: Joi.string()
        })
    ).required()
});
router.post('/', (req, res) => {
    const body = req.body;
    // let validate = Joi.validate(body, createValidate);
    // if (validate.error) {
    //     return res.send({
    //         message: "validation fail",
    //         results: validate.error.details,
    //         code: 999
    //     });
    // }
    WebsiteModel
        .findOne({ isActive : true })
        .lean()
        .exec((error, result) => {
            if (error) {
                    console.error(`[${LOG}][1.1] => ${error}`);
                    return res.send({
                        code: 999,
                        message: "error",
                        result: error,
                    });
                } else if (!_.isUndefined(result) && !_.isNull(result)) {
                    const {
                        logo,
                        topOne,
                        topTwo,
                        middleOne,
                        middleTwo,
                        middleThree,
                        middleFour,
                        bottomOne,
                        lottery,
                        game,
                        casino,
                        ambGame,
                        casinoBanner,
                        lottoBanner,
                        gameBanner,
                        sportBanner,
                        ambGameBanner,
                        agentBanner1,
                        agentBanner2,
                        agentLogo,
                        memberGameBanner1,
                        memberGameBanner2,
                        memberGameBanner3,
                        memberGameBanner4,
                        memberGameBanner5,
                        maintenance
                    } = body;

                    const update = {};
                    update._id = result._id;
                    if (!_.isUndefined(logo)) {
                        update.logo = logo;
                    }
                    if (!_.isUndefined(topOne)) {
                        update.topOne = topOne;
                    }
                    if (!_.isUndefined(topTwo)) {
                        update.topTwo = topTwo;
                    }
                    if (!_.isUndefined(middleOne)) {
                        update.middleOne = middleOne;
                    }
                    if (!_.isUndefined(middleTwo)) {
                        update.middleTwo = middleTwo;
                    }
                    if (!_.isUndefined(middleThree)) {
                        update.middleThree = middleThree;
                    }
                    if (!_.isUndefined(middleFour)) {
                        update.middleFour = middleFour;
                    }
                    if (!_.isUndefined(bottomOne)) {
                        update.bottomOne = bottomOne;
                    }
                    if (!_.isUndefined(lottery)) {
                        update.lottery = lottery;
                    }
                    if (!_.isUndefined(game)) {
                        update.game = game;
                    }
                    if (!_.isUndefined(casino)) {
                        update.casino = casino;
                    }
                    if (!_.isUndefined(ambGame)) {
                        update.ambGame = ambGame;
                    }
                    if (!_.isUndefined(casinoBanner)) {
                        update.casinoBanner = casinoBanner;
                    }

                    if (!_.isUndefined(lottoBanner)) {
                        update.lottoBanner = lottoBanner;
                    }

                    if (!_.isUndefined(gameBanner)) {
                        update.gameBanner = gameBanner;
                    }

                    if (!_.isUndefined(sportBanner)) {
                        update.sportBanner = sportBanner;
                    }
                    if (!_.isUndefined(ambGameBanner)) {
                        update.ambGameBanner = ambGameBanner;
                    }

                    if (!_.isUndefined(agentBanner1)) {
                        update.agentBanner1 = agentBanner1;
                    }
                    if (!_.isUndefined(agentBanner2)) {
                        update.agentBanner2 = agentBanner2;
                    }
                    if (!_.isUndefined(agentLogo)) {
                    update.agentLogo = agentLogo;
                }
                    if (!_.isUndefined(memberGameBanner1)) {
                        update.memberGameBanner1 = memberGameBanner1;
                    }
                    if (!_.isUndefined(memberGameBanner2)) {
                        update.memberGameBanner2 = memberGameBanner2;
                    }
                    if (!_.isUndefined(memberGameBanner3)) {
                        update.memberGameBanner3 = memberGameBanner3;
                    }
                    if (!_.isUndefined(memberGameBanner4)) {
                        update.memberGameBanner4 = memberGameBanner4;
                    }
                    if (!_.isUndefined(memberGameBanner5)) {
                        update.memberGameBanner5 = memberGameBanner5;
                    }
                    if (!_.isUndefined(maintenance)) {
                        update.maintenance = maintenance;
                    }

                    WebsiteModel.findOneAndUpdate({
                            _id: mongoose.Types.ObjectId(result._id)
                        },
                        update
                        ,
                        {
                            multi: false
                        },
                        (error, data) => {
                            if (error) {
                                console.error(`[${LOG}][1.1] => ${error}`);
                                return res.send({
                                    code: 999,
                                    message: "error",
                                    result: error,
                                });
                            } else if (data.nModified === 0) {
                                console.log(`[${LOG}][1.2] Update fail id [${data._id}]`);
                                return res.send({
                                    code: 998,
                                    message: `fail`,
                                    result:`Update fail id [${id}]`,

                                });
                            } else {
                                console.log(`[${LOG}][1.3] Update success id [${data._id}]`);
                                Redis.deleteByKey(`WebsiteModel${CLIENT_NAME}`, (error, result) => {

                                });
                                return res.send({
                                    code: 0,
                                    message: "success",
                                    result : update
                                });
                            }
                        });
                } else {
                    const model = new WebsiteModel(body);
                    model.save((error, result) => {
                        if (error) {
                            console.error(`[${LOG}][1.1] => ${error}`);
                            return res.send({
                                code: 999,
                                message: "error",
                                result: error,
                            });
                        } else {
                            console.log(`[${LOG}][1.2] success [${result._id}]`);
                            Redis.deleteByKey(`WebsiteModel${CLIENT_NAME}`, (error, result) => {

                            });
                            return res.send({
                                code: 0,
                                message: "success",
                                result: result
                            });
                        }
                    });
                }
            });
});

//[2]
const updateValidate = Joi.object().keys({
    logo: Joi.string(),
    bannerTop: Joi.array().items(
        Joi.object().keys({
            img: Joi.string().required()
        })
    ),
    bannerMiddle: Joi.array().items(
        Joi.object().keys({
            img: Joi.string().required()
        })
    ),
    bannerBottom: Joi.array().items(
        Joi.object().keys({
            img: Joi.string().required()
        })
    ),
    depositChannel: Joi.array().items(
        Joi.object().keys({
            img: Joi.string().required()
        })
    ),
    casinoBanner: Joi.array().items(
        Joi.object().keys({
            img: Joi.string().required(),
            url: Joi.string()
        })
    ).required(),
    lottoBanner: Joi.array().items(
        Joi.object().keys({
            img: Joi.string().required(),
            url: Joi.string()
        })
    ).required(),
    gameBanner: Joi.array().items(
        Joi.object().keys({
            img: Joi.string().required(),
            url: Joi.string()
        })
    ).required(),
    sportBanner: Joi.array().items(
        Joi.object().keys({
            img: Joi.string().required(),
            url: Joi.string()
        })
    ).required(),
    ambGameBanner: Joi.array().items(
        Joi.object().keys({
            img: Joi.string().required(),
            url: Joi.string()
        })
    ).required(),
    agentBanner1: Joi.array().items(
        Joi.object().keys({
            img: Joi.string().required(),
        })
    ).required(),
    agentBanner2: Joi.array().items(
        Joi.object().keys({
            img: Joi.string().required()
        })
    ).required(),
    memberGameBanner1: Joi.array().items(
        Joi.object().keys({
            img: Joi.string().required(),
            url: Joi.string()
        })
    ).required(),
    memberGameBanner2: Joi.array().items(
        Joi.object().keys({
            img: Joi.string().required(),
            url: Joi.string()
        })
    ).required(),
    memberGameBanner3: Joi.array().items(
        Joi.object().keys({
            img: Joi.string().required(),
            url: Joi.string()
        })
    ).required(),
    memberGameBanner4: Joi.array().items(
        Joi.object().keys({
            img: Joi.string().required(),
            url: Joi.string()
        })
    ).required(),
    memberGameBanner5: Joi.array().items(
        Joi.object().keys({
            img: Joi.string().required(),
            url: Joi.string()
        })
    ).required(),
    maintenance: Joi.array().items(
        Joi.object().keys({
            img: Joi.string().required()
        })
    ).required()
});
router.put('/', (req, res) => {
    const body = req.body;
    let validate = Joi.validate(body, updateValidate);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }
    const {
        logo,
        bannerTop,
        bannerMiddle,
        bannerBottom,
        depositChannel,
        casinoBanner,
        lottoBanner,
        gameBanner,
        sportBanner,
        ambGameBanner,
        agentBanner1,
        agentBanner2,
        memberGameBanner1,
        memberGameBanner2,
        memberGameBanner3,
        memberGameBanner4,
        memberGameBanner5,
        maintenance
    } = body;

    const update = {};

    if (!_.isUndefined(logo)) {
        update.logo = logo;
    }

    if (!_.isUndefined(bannerTop)) {
        update.bannerTop = bannerTop;
    }

    if (!_.isUndefined(bannerMiddle)) {
        update.bannerMiddle = bannerMiddle;
    }

    if (!_.isUndefined(bannerBottom)) {
        update.bannerBottom = bannerBottom;
    }

    if (!_.isUndefined(depositChannel)) {
        update.depositChannel = depositChannel;
    }

    if (!_.isUndefined(casinoBanner)) {
        update.casinoBanner = casinoBanner;
    }

    if (!_.isUndefined(lottoBanner)) {
        update.lottoBanner = lottoBanner;
    }

    if (!_.isUndefined(gameBanner)) {
        update.gameBanner = gameBanner;
    }

    if (!_.isUndefined(sportBanner)) {
        update.sportBanner = sportBanner;
    }
    if (!_.isUndefined(agentBanner1)) {
        update.agentBanner1 = agentBanner1;
    }
    if (!_.isUndefined(agentBanner2)) {
        update.agentBanner2 = agentBanner2;
    }
    if (!_.isUndefined(memberGameBanner1)) {
        update.memberGameBanner1 = memberGameBanner1;
    }
    if (!_.isUndefined(memberGameBanner2)) {
        update.memberGameBanner2 = memberGameBanner2;
    }
    if (!_.isUndefined(memberGameBanner3)) {
        update.memberGameBanner3 = memberGameBanner3;
    }
    if (!_.isUndefined(memberGameBanner4)) {
        update.memberGameBanner4 = memberGameBanner4;
    }
    if (!_.isUndefined(memberGameBanner5)) {
        update.memberGameBanner5 = memberGameBanner5;
    }
    if (!_.isUndefined(maintenance)) {
        update.maintenance = maintenance;
    }

    WebsiteModel.findOneAndUpdate({
        },
        update
        ,
        {
            multi: false
        },
        (error, data) => {
            if (error) {
                console.error(`[${LOG}][2.1] => ${error}`);
                return res.send({
                    code: 999,
                    message: "error",
                    result: error,
                });
            } else if (data.nModified === 0) {
                console.log(`[${LOG}][2.2] Update fail id [${data._id}]`);
                return res.send({
                    code: 998,
                    message: `fail`,
                    result:`Update fail id [${id}]`,

                });
            } else {
                console.log(`[${LOG}][2.3] Update success id [${data._id}]`);
                Redis.deleteByKey(`WebsiteModel${CLIENT_NAME}`, (error, result) => {

                });
                return res.send({
                    code: 0,
                    message: "success",
                    result : update
                });
            }
        });
});

//[3]
router.get('/', (req, res) =>{
    try{
        const contents = cache.get( `WebsiteModel${CLIENT_NAME}`, true );
        return res.send(
            {
                code: 0,
                message: "success",
                result: contents
            });
    } catch( err ){
        Redis.getByKey(`WebsiteModel${CLIENT_NAME}`, (error, result) => {
            if (error || _.isNull(result) || _.isUndefined(result)) {
                WebsiteModel.findOne({
                    isActive : true
                })
                    .lean()
                    .exec((error, result) => {
                        if (error) {
                            console.error(`[${LOG}][3.1] => ${error}`);
                            return res.send({
                                code: 999,
                                message: "error",
                                result: error,
                            });
                        }  else if (!result || _.isEmpty(result)) {
                            console.log(`[${LOG}][3.2] Data not found`);
                            return res.send({
                                code: 998,
                                message: `fail`,
                                result:`Data not found`,

                            });
                        } else {
                            console.log(`[${LOG}][3.3] success`);
                            Redis.setByKey(`WebsiteModel${CLIENT_NAME}`, result, (error, result) => {

                            });
                            cache.set( `WebsiteModel${CLIENT_NAME}`, result);
                            return res.send({
                                code: 0,
                                message: "success",
                                result : result
                            });
                        }
                    });
            } else {
                cache.set( `WebsiteModel${CLIENT_NAME}`, result);
                return res.send({
                    code: 0,
                    message: "success",
                    result : result
                });
            }
        });
    }
});
//[marketing]
router.post('/marketing-tool', (req, res) => {
    const body = req.body;

    MarketingTool
        .findOne({ isActive : true })
        .lean()
        .exec((error, result) => {
            if (error) {
                console.error(`[${LOG}][1.1] => ${error}`);
                return res.send({
                    code: 999,
                    message: "error",
                    result: error,
                });
            }
            else if (!_.isUndefined(result) && !_.isNull(result)) {
                console.log(result,'poopopooop');
                const {
                    newBanner,
                    promotionBanner,
                    gameImage,
                    videoUrl
                } = body;

                const update = {};

                update._id = result._id;
                if (!_.isUndefined(newBanner)) {
                    update.newBanner = newBanner;
                }
                if (!_.isUndefined(promotionBanner)) {
                    update.promotionBanner = promotionBanner;
                }
                if (!_.isUndefined(gameImage)) {
                    update.gameImage = gameImage;
                }
                if (!_.isUndefined(videoUrl)) {
                    update.videoUrl = videoUrl;
                }
                if(update.newBanner){

                    MarketingTool.update({
                            _id: mongoose.Types.ObjectId(result._id)

                        },
                        {
                            $push:{
                                'newBanner':update.newBanner,
                            }
                        }
                        ,
                        {
                            multi: false
                        },
                        (error, data) => {
                            // console.log('vvvvvvvvv',data);
                            if (error) {
                                console.error(`[${LOG}][1.1] => ${error}`);
                                return res.send({
                                    code: 999,
                                    message: "error",
                                    result: error,
                                });
                            }
                            else if (data.nModified === 0) {
                                console.log(`[${LOG}][1.2] Update fail id [${data._id}]`);
                                return res.send({
                                    code: 998,
                                    message: `fail`,
                                    result:`Update fail id [${id}]`,

                                });
                            }
                            else {
                                console.log(`[${LOG}][1.3] Update success id [${data._id}]`);
                                Redis.deleteByKey(`WebsiteModel${CLIENT_NAME}`, (error, result) => {

                                });
                                return res.send({
                                    code: 0,
                                    message: "success",
                                    result : update
                                });

                            }
                        });
                }
                else if(update.promotionBanner) {

                    MarketingTool.update({
                            _id: mongoose.Types.ObjectId(result._id)

                        },
                        {
                            $push:{
                                'promotionBanner':update.promotionBanner,
                            }
                        }
                        ,
                        {
                            multi: false
                        },
                        (error, data) => {
                            console.log('vvvvvvvvv',data);
                            if (error) {
                                console.error(`[${LOG}][1.1] => ${error}`);
                                return res.send({
                                    code: 999,
                                    message: "error",
                                    result: error,
                                });
                            }
                            else if (data.nModified === 0) {
                                console.log(`[${LOG}][1.2] Update fail id [${data._id}]`);
                                return res.send({
                                    code: 998,
                                    message: `fail`,
                                    result:`Update fail id [${id}]`,

                                });
                            }
                            else {
                                console.log(`[${LOG}][1.3] Update success id [${data._id}]`);
                                Redis.deleteByKey(`WebsiteModel${CLIENT_NAME}`, (error, result) => {

                                });
                                return res.send({
                                    code: 0,
                                    message: "success",
                                    result : update
                                });

                            }
                        });
                }
                else if(update.gameImage) {

                    MarketingTool.update({
                            _id: mongoose.Types.ObjectId(result._id)

                        },
                        {
                            $push:{
                                'gameImage':update.gameImage,
                            }
                        }
                        ,
                        {
                            multi: false
                        },
                        (error, data) => {
                            console.log('vvvvvvvvv',data);
                            if (error) {
                                console.error(`[${LOG}][1.1] => ${error}`);
                                return res.send({
                                    code: 999,
                                    message: "error",
                                    result: error,
                                });
                            }
                            else if (data.nModified === 0) {
                                console.log(`[${LOG}][1.2] Update fail id [${data._id}]`);
                                return res.send({
                                    code: 998,
                                    message: `fail`,
                                    result:`Update fail id [${id}]`,

                                });
                            }
                            else {
                                console.log(`[${LOG}][1.3] Update success id [${data._id}]`);
                                Redis.deleteByKey(`WebsiteModel${CLIENT_NAME}`, (error, result) => {

                                });
                                return res.send({
                                    code: 0,
                                    message: "success",
                                    result : update
                                });

                            }
                        });
                }
                else if(update.videoUrl) {

                    MarketingTool.update({
                            _id: mongoose.Types.ObjectId(result._id)

                        },
                        {
                            $push:{
                                'videoUrl':update.videoUrl,
                            }
                        }
                        ,
                        {
                            multi: false
                        },
                        (error, data) => {
                            console.log('vvvvvvvvv',data);
                            if (error) {
                                console.error(`[${LOG}][1.1] => ${error}`);
                                return res.send({
                                    code: 999,
                                    message: "error",
                                    result: error,
                                });
                            }
                            else if (data.nModified === 0) {
                                console.log(`[${LOG}][1.2] Update fail id [${data._id}]`);
                                return res.send({
                                    code: 998,
                                    message: `fail`,
                                    result:`Update fail id [${id}]`,

                                });
                            }
                            else {
                                console.log(`[${LOG}][1.3] Update success id [${data._id}]`);
                                Redis.deleteByKey(`WebsiteModel${CLIENT_NAME}`, (error, result) => {

                                });
                                return res.send({
                                    code: 0,
                                    message: "success",
                                    result : update
                                });

                            }
                        });
                }

            }
            else {
                const model = new MarketingTool(body);
                model.save((error, result) => {
                    if (error) {
                        console.error(`[${LOG}][1.1] => ${error}`);
                        return res.send({
                            code: 999,
                            message: "error",
                            result: error,
                        });
                    } else {
                        console.log(`[${LOG}][1.2] success [${result._id}]`);
                        Redis.deleteByKey(`WebsiteModel${CLIENT_NAME}`, (error, result) => {

                        });
                        return res.send({
                            code: 0,
                            message: "success",
                            result: result
                        });
                    }
                });
            }
        });
});
//[marketing-get]
router.get('/marketing-tool', (req, res) => {

    MarketingTool.findOne({
        isActive : true
    })
        .lean()
        .exec((error, result) => {
            if (error) {
                console.log('22222222');
                console.error(`[${LOG}][3.1] => ${error}`);
                return res.send({
                    code: 999,
                    message: "error",
                    result: error,
                });
            }  else if (!result || _.isEmpty(result)) {
                console.log(`[${LOG}][3.2] Data not found`);
                return res.send({
                    code: 998,
                    message: `fail`,
                    result:`Data not found`,

                });
            } else {
                console.log(`[${LOG}][3.3] success`);
                return res.send({
                    code: 0,
                    message: "success",
                    result : result
                });
            }
        });
});
//[marketing-update]
router.post('/cancel-marketing', (req, res) => {
    const body = req.body;
    MarketingTool
        .findOne({ isActive : true })
        .lean()
        .exec((error, result) => {

            if (error) {
                console.error(`[${LOG}][1.1] => ${error}`);
                return res.send({
                    code: 999,
                    message: "error",
                    result: error,
                });
            }
            else if (!_.isUndefined(result) && !_.isNull(result)) {
                console.log(result,'poopopooop');
                const {
                    newBanner,
                    promotionBanner,
                    gameImage,
                    videoUrl
                } = body;

                const update = {};

                update._id = result._id;
                if (!_.isUndefined(newBanner)) {
                    update.newBanner = newBanner;
                }
                if (!_.isUndefined(promotionBanner)) {
                    update.promotionBanner = promotionBanner;
                }
                if (!_.isUndefined(gameImage)) {
                    update.gameImage = gameImage;
                }
                if (!_.isUndefined(videoUrl)) {
                    update.videoUrl = videoUrl;
                }
                if(update.newBanner){
                    MarketingTool.update({
                            _id: mongoose.Types.ObjectId(result._id)

                        },
                        {
                            $pull: {
                                "newBanner": {_id: {$in: update.newBanner._id}}
                            }
                        }
                        ,
                        {
                            multi: false
                        },
                        (error, data) => {
                            if (error) {
                                console.error(`[${LOG}][1.1] => ${error}`);
                                return res.send({
                                    code: 999,
                                    message: "error",
                                    result: error,
                                });
                            }
                            else if (data.nModified === 0) {
                                console.log(`[${LOG}][1.2] Update fail id [${data._id}]`);
                                return res.send({
                                    code: 998,
                                    message: `fail`,
                                    result:`Update fail id [${id}]`,

                                });
                            }
                            else {
                                console.log(`[${LOG}][1.3] Update success id [${data._id}]`);
                                Redis.deleteByKey(`WebsiteModel${CLIENT_NAME}`, (error, result) => {

                                });
                                return res.send({
                                    code: 0,
                                    message: "success",
                                    result : update
                                });

                            }
                        });
                }
                else if(update.promotionBanner) {

                    MarketingTool.update({
                            _id: mongoose.Types.ObjectId(result._id)

                        },
                        {
                            $pull: {
                                "promotionBanner": {_id: {$in: update.promotionBanner._id}}
                            }
                        }
                        ,
                        {
                            multi: false
                        },
                        (error, data) => {
                            console.log('vvvvvvvvv',data);
                            if (error) {
                                console.error(`[${LOG}][1.1] => ${error}`);
                                return res.send({
                                    code: 999,
                                    message: "error",
                                    result: error,
                                });
                            }
                            else if (data.nModified === 0) {
                                console.log(`[${LOG}][1.2] Update fail id [${data._id}]`);
                                return res.send({
                                    code: 998,
                                    message: `fail`,
                                    result:`Update fail id [${id}]`,

                                });
                            }
                            else {
                                console.log(`[${LOG}][1.3] Update success id [${data._id}]`);
                                Redis.deleteByKey(`WebsiteModel${CLIENT_NAME}`, (error, result) => {

                                });
                                return res.send({
                                    code: 0,
                                    message: "success",
                                    result : update
                                });

                            }
                        });
                }
                else if(update.gameImage) {

                    MarketingTool.update({
                            _id: mongoose.Types.ObjectId(result._id)

                        },
                        {
                            $pull: {
                                "gameImage": {_id: {$in: update.gameImage._id}}
                            }
                        }
                        ,
                        {
                            multi: false
                        },
                        (error, data) => {
                            console.log('vvvvvvvvv',data);
                            if (error) {
                                console.error(`[${LOG}][1.1] => ${error}`);
                                return res.send({
                                    code: 999,
                                    message: "error",
                                    result: error,
                                });
                            }
                            else if (data.nModified === 0) {
                                console.log(`[${LOG}][1.2] Update fail id [${data._id}]`);
                                return res.send({
                                    code: 998,
                                    message: `fail`,
                                    result:`Update fail id [${id}]`,

                                });
                            }
                            else {
                                console.log(`[${LOG}][1.3] Update success id [${data._id}]`);
                                Redis.deleteByKey(`WebsiteModel${CLIENT_NAME}`, (error, result) => {

                                });
                                return res.send({
                                    code: 0,
                                    message: "success",
                                    result : update
                                });

                            }
                        });
                }
                else if(update.videoUrl) {

                    MarketingTool.update({
                            _id: mongoose.Types.ObjectId(result._id)

                        },
                        {
                            $pull: {
                                "videoUrl": {_id: {$in: update.videoUrl._id}}
                            }
                        }
                        ,
                        {
                            multi: false
                        },
                        (error, data) => {
                            console.log('vvvvvvvvv',data);
                            if (error) {
                                console.error(`[${LOG}][1.1] => ${error}`);
                                return res.send({
                                    code: 999,
                                    message: "error",
                                    result: error,
                                });
                            }
                            else if (data.nModified === 0) {
                                console.log(`[${LOG}][1.2] Update fail id [${data._id}]`);
                                return res.send({
                                    code: 998,
                                    message: `fail`,
                                    result:`Update fail id [${id}]`,

                                });
                            }
                            else {
                                console.log(`[${LOG}][1.3] Update success id [${data._id}]`);
                                Redis.deleteByKey(`WebsiteModel${CLIENT_NAME}`, (error, result) => {

                                });
                                return res.send({
                                    code: 0,
                                    message: "success",
                                    result : update
                                });

                            }
                        });
                }

            }
            else {
                const model = new MarketingTool(body);
                model.save((error, result) => {
                    if (error) {
                        console.error(`[${LOG}][1.1] => ${error}`);
                        return res.send({
                            code: 999,
                            message: "error",
                            result: error,
                        });
                    } else {
                        console.log(`[${LOG}][1.2] success [${result._id}]`);
                        Redis.deleteByKey(`WebsiteModel${CLIENT_NAME}`, (error, result) => {

                        });
                        return res.send({
                            code: 0,
                            message: "success",
                            result: result
                        });
                    }
                });
            }
        });

});



module.exports = router;

