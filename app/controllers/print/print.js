const express             = require('express');
const router              = express.Router();
const _                   = require('underscore');
const BetTransactionModel = require('../../models/betTransaction.model');

const LOG = 'PRINT';

router.put('/', (req, res) => {

    const {
        betId,
        isPrinted
    } = req.body;

    BetTransactionModel.update({
            betId: betId
        },
        {
            $set: {
                'isPrinted': isPrinted
            }
        },
        {
            multi: false
        },
        (error, data) => {
            if (error) {
                console.error(`[${LOG}][2.1] => ${error}`);
                return res.send({
                    code: 999,
                    message: "error",
                    result: error,
                });
            } else if (data.nModified === 0) {
                console.log(`[${LOG}][2.2] Update fail id [${betId}]`);
                return res.send({
                    code: 998,
                    message: `fail`,
                    result:`Update fail id [${betId}]`,

                });
            } else {
                console.log(`[${LOG}][2.3] Update success id [${betId}]`);
                return res.send({
                    code: 0,
                    message: "success",
                    result : req.body
                });
            }
        });
});

module.exports = router;

