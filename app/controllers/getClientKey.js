const _         = require('underscore');
const CLIENT_NAME  = process.env.CLIENT_NAME || 'SPORTBOOK88';

const AMEBA_IMAGE_URL = 'https://s3-ap-southeast-1.amazonaws.com/amb-slot/ameba-game';
const DS_IMAGE_URL    = 'https://amb-slot.s3-ap-southeast-1.amazonaws.com/ds-game';

module.exports = {
    getKey : () => {

      if (_.isEqual(CLIENT_NAME, 'SPORTBOOK88')) {

        return require('../clientkey/5_sportbook88');

      } else if (_.isEqual(CLIENT_NAME, 'AMB_SPORTBOOK')) {

        return require('../clientkey/1_amb_sportbook');

      } else if (_.isEqual(CLIENT_NAME, 'SB234')) {

        return require('../clientkey/2_sb234');

      } else if (_.isEqual(CLIENT_NAME, 'DD88BET')) {

        return require('../clientkey/3_sbocopa');

      } else if (_.isEqual(CLIENT_NAME, 'CSR')) {

        return require('../clientkey/4_csr');

      } else if (_.isEqual(CLIENT_NAME, 'GOAL168')) {

        return require('../clientkey/7_goal168');

      } else if (_.isEqual(CLIENT_NAME, 'IRON')) {

        return require('../clientkey/8_ironman');

      } else if (_.isEqual(CLIENT_NAME, 'MGM')) {

        return require('../clientkey/9_mgm');

      } else if (_.isEqual(CLIENT_NAME, 'FOXZ')) {

        return require('../clientkey/10_foxz168');

      } else if (_.isEqual(CLIENT_NAME, 'BET123')) {

        return require('../clientkey/11_bet123');

      } else if (_.isEqual(CLIENT_NAME, 'IFM789')) {

        return require('../clientkey/12_ifm789');

      } else if (_.isEqual(CLIENT_NAME, 'ALLSTAR55')) {

        return require('../clientkey/13_viewbet');

      } else if (_.isEqual(CLIENT_NAME, 'BSBBET')) {

        return require('../clientkey/14_bsbbet');

      } else if (_.isEqual(CLIENT_NAME, 'LOVEBALL')) {

        return require('../clientkey/17_loveballbet');

      } else if (_.isEqual(CLIENT_NAME, 'WINBET')) {

        return require('../clientkey/18_winbetth');

      } else if (_.isEqual(CLIENT_NAME, 'SBO111')) {

        return require('../clientkey/19_allstar55');

      } else if (_.isEqual(CLIENT_NAME, 'GOAL6969')) {

        return require('../clientkey/20_goal6969');

      } else if (_.isEqual(CLIENT_NAME, 'ISB888')) {

        return require('../clientkey/21_isb888');

      } else if (_.isEqual(CLIENT_NAME, 'MHG66')) {

        return require('../clientkey/22_mhg66');

      } else if (_.isEqual(CLIENT_NAME, 'PROBET')) {

        return require('../clientkey/23_iprobet');

      } else if (_.isEqual(CLIENT_NAME, 'FAST98')) {

        return require('../clientkey/24_fastbet98');

      } else if (_.isEqual(CLIENT_NAME, 'IWANTBET')) {

        return require('../clientkey/25_iwantbet');

      } else if (_.isEqual(CLIENT_NAME, 'SAND')) {

        return require('../clientkey/26_sand1899');

      } else if (_.isEqual(CLIENT_NAME, 'FIN88')) {

        return require('../clientkey/27_fin88');

      } else if (_.isEqual(CLIENT_NAME, 'VRCBET')) {

        return require('../clientkey/28_vrcbet');

      } else if (_.isEqual(CLIENT_NAME, 'AFC1688')) {

        return require('../clientkey/29_afc1688');

      } else if (_.isEqual(CLIENT_NAME, 'MBK168')) {

        return require('../clientkey/30_mbk168');

      } else if (_.isEqual(CLIENT_NAME, 'MVPATM168')) {

        return require('../clientkey/31_mvpatm168');

      } else if (_.isEqual(CLIENT_NAME, 'HENG168BET')) {

        return require('../clientkey/32_heng168');

      } else if (_.isEqual(CLIENT_NAME, 'HAFABET')) {

        return require('../clientkey/33_hafabet');

      } else if (_.isEqual(CLIENT_NAME, 'SAND333')) {

        return require('../clientkey/34_sand333');

      } else if (_.isEqual(CLIENT_NAME, 'TT69BET')) {

        return require('../clientkey/35_tt69bet');

      } else if (_.isEqual(CLIENT_NAME, 'BASA888')) {

        return require('../clientkey/36_basa888');

      } else if (_.isEqual(CLIENT_NAME, 'G2GBET')) {

        return require('../clientkey/37_g2gbet');

      } else if (_.isEqual(CLIENT_NAME, 'POPZA24')) {

        return require('../clientkey/38_popza24');

      } else if (_.isEqual(CLIENT_NAME, 'UZIBET')) {

        return require('../clientkey/39_uzibet');

      } else if (_.isEqual(CLIENT_NAME, 'BWIN')) {

        return require('../clientkey/40_bwin');

      } else if (_.isEqual(CLIENT_NAME, 'BEIN88')) {

        return require('../clientkey/41_bein88');

      } else if (_.isEqual(CLIENT_NAME, 'UKINGBET')) {

        return require('../clientkey/42_ukingbet');

      } else if (_.isEqual(CLIENT_NAME, 'PREMIER888')) {

        return require('../clientkey/43_premier888');

      } else if (_.isEqual(CLIENT_NAME, 'BET789')) {

        return require('../clientkey/44_789bet');

      } else if (_.isEqual(CLIENT_NAME, 'ABCGAME88')) {

          return require('../clientkey/45_abcgame88');

      } else if (_.isEqual(CLIENT_NAME, 'AMGOAL')) {

          return require('../clientkey/46_amgoal');

      } else if (_.isEqual(CLIENT_NAME, 'OSGAME99')) {

          return require('../clientkey/47_osgame99');

      } else if (_.isEqual(CLIENT_NAME, '93LUCKY')) {

          return require('../clientkey/48_93lucky');

      } else if (_.isEqual(CLIENT_NAME, 'NAZABET')) {

          return require('../clientkey/49_nazabet');

      } else if (_.isEqual(CLIENT_NAME, 'ZAMBABET')) {

          return require('../clientkey/50_zambabet');

      } else if (_.isEqual(CLIENT_NAME, 'BET1819')) {

          return require('../clientkey/51_1819bet');

      } else if (_.isEqual(CLIENT_NAME, 'PAPABET')) {

          return require('../clientkey/52_papabet');

      } else if (_.isEqual(CLIENT_NAME, 'MCB168BET')) {

          return require('../clientkey/53_mcb168bet');

      } else if (_.isEqual(CLIENT_NAME, 'LUCKYPRO')) {

          return require('../clientkey/54_luckypro');

      } else if (_.isEqual(CLIENT_NAME, 'POGBABET')) {

          return require('../clientkey/55_pogbabet');

      } else if (_.isEqual(CLIENT_NAME, 'ME24')) {

          return require('../clientkey/56_me24');

      } else if (_.isEqual(CLIENT_NAME, 'S2KBET')) {

          return require('../clientkey/57_s2kbet');

      } else if (_.isEqual(CLIENT_NAME, 'INWBALL')) {

          return require('../clientkey/58_inwball');

      } else if (_.isEqual(CLIENT_NAME, 'N88BET')) {

          return require('../clientkey/59_n88bet');

      } else if (_.isEqual(CLIENT_NAME, 'TNT911')) {

          return require('../clientkey/60_tnt911');

      } else if (_.isEqual(CLIENT_NAME, 'NAZA')) {

          return require('../clientkey/61_naza');

      } else if (_.isEqual(CLIENT_NAME, '1FAZBET')) {

          return require('../clientkey/62_1fazbet');

      } else if (_.isEqual(CLIENT_NAME, 'MONTE168')) {

          return require('../clientkey/63_monte168');

      } else if (_.isEqual(CLIENT_NAME, 'LUCABETS')) {

          return require('../clientkey/64_lucabets');

      } else if (_.isEqual(CLIENT_NAME, '456BETT')) {

          return require('../clientkey/65_456bett');

      } else if (_.isEqual(CLIENT_NAME, 'U1BET')) {

          return require('../clientkey/66_u1bet');

      } else if (_.isEqual(CLIENT_NAME, 'LUCIABET')) {

          return require('../clientkey/67_luciabet');

      } else if (_.isEqual(CLIENT_NAME, 'KKWVIP')) {

          return require('../clientkey/68_kkwvip');

      }
    }
};