const express = require('express');
const router = express.Router();
const request = require('request');
const _  = require('underscore');
// -----------------------------------------------------------
const crypto = require('crypto');
const async = require("async");
const Joi       = require('joi');
const DateUtils = require('../../common/dateUtils');
const MemberMuayStep2Model = require('../../models/memberMuayStep2.model.js');
const MemberModel                = require('../../models/member.model');
const Muaystep2Service              = require('../../common/muaystep2Service');
const BetTransactionModel = require('../../models/betTransaction.model');
const AgentService = require('../../common/agentService');
const MemberService = require('../../common/memberService');
const ApiService = require('../../common/apiService');

const HOST = process.env.MUAY_5G_HOST || 'https://api.aspbet.com/';
const TOKEN = process.env.MUAY_5G_TOKEN || '4b2b6303956277ddfd7b8ea9cebb31db';

const headers = {
    'Content-Type': 'application/json'
};

// 1.1
router.get('/gameList/:type', function (req, res) {
    let data = {
        token : TOKEN,
        is_live : `${req.params.type}`
    };

    console.log(data);
    request.post({
        url: `${HOST}apiservice/amb/api/match_list`,
        body: data,
        rejectUnauthorized: false,
        json:true
    }, function(error, response, body){
        console.log(error,'------',body);
        if (error) {
            return res.send(error);
        } else {

            try {
                if (body.code === 0){


                    let dataList = [];
                    body.res.data_list.forEach(function (data) {
                        if(dataList.length > 0){

                            let sameId = false;
                            dataList.forEach(function (d) {
                                if(d.league_id === data.league_id){
                                    sameId = true;
                                    d.list_all.push(data)
                                }
                            });

                            if(!sameId){
                                dataList.push({
                                    league_id : data.league_id,
                                    league_name_th : data.ln_th,
                                    league_name_en : data.ln_en,
                                    cat_type : data.cat_id,
                                    list_all : [data]
                                });
                            }
                        }else {
                            dataList.push({
                                league_id : data.league_id,
                                league_name_th : data.ln_th,
                                league_name_en : data.ln_en,
                                cat_type : data.cat_id,
                                list_all : [data]
                            });
                        }
                    });

                    return res.send({
                        code: 0,
                        result : dataList,
                        message : 'Success'
                    });
                }else {
                    return res.send({
                        code: body.code,
                        result : [],
                        message : body.msg
                    });
                }


            } catch (e) {
                return res.send({
                    code: 1,
                    result : null,
                    message : 'Get match list fail'
                });
            }
        }
    });
});

const resultSchema = Joi.object().keys({
    date: Joi.string().required()
});

router.post('/result', function (req, res) {

    let validate = Joi.validate(req.body, resultSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    let data = {
        token : TOKEN,
        acc_date : `${req.body.date}`
    };

    request.post({
        url: `${HOST}apiservice/amb/api/result_list`,
        body: data,
        rejectUnauthorized: false,
        json:true
    }, function(error, response, body){
        if (error) {
            return res.send(error);
        } else {
            let dataList = [];
            body.res.data_list.forEach(function (data) {
                if(dataList.length > 0){

                    let sameId = false;
                    dataList.forEach(function (d) {
                        if(d.league_id === data.league_id){
                            sameId = true;
                            d.list_all.push(data)
                        }
                    });

                    if(!sameId){
                        dataList.push({
                            league_id : data.league_id,
                            league_name_th : data.ln_th,
                            league_name_en : data.ln_en,
                            cat_type : data.cat_id,
                            list_all : [data]
                        });
                    }
                }else {
                    dataList.push({
                        league_id : data.league_id,
                        league_name_th : data.ln_th,
                        league_name_en : data.ln_en,
                        cat_type : data.cat_id,
                        list_all : [data]
                    });
                }
            });

            return res.send({
                code: 0,
                result : dataList,
                message : 'Success'
            });
        }
    });
});


const addBetSchema = Joi.object().keys({
    maxBet: Joi.string().required(),
    team: Joi.string().required(),
    odd: Joi.number().required()
});


// 1.2
router.post('/findEventById', function (req, res) {

    let validate = Joi.validate(req.body, addBetSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }
    // console.log(JSON.stringify(req.body));

    const userInfo = req.userInfo;

    async.waterfall([callback => {

        MemberModel.findById(userInfo._id, (err, response) => {
            if (err)
                callback(err, null);

            if (response) {

                callback(null, response);

            }
        });


    }, (memberInfo, callback) => {

        let result = {
            minBet : 5,
            maxBet : parseInt(req.body.maxBet)
        };

        console.log('max bet ', memberInfo.limitSetting.other.m2.maxPerBetHDP);

        if(parseInt(req.body.maxBet) > memberInfo.limitSetting.other.m2.maxPerBetHDP){
            result.maxBet = memberInfo.limitSetting.other.m2.maxPerBetHDP ? memberInfo.limitSetting.other.m2.maxPerBetHDP : 0;
        }

        result.minBet = memberInfo.limitSetting.other.m2.minPerBetHDP ? memberInfo.limitSetting.other.m2.minPerBetHDP : 0;

        callback(null , result);

    }], function (err, response) {
        if(err){
            return res.send({
                code: 999,
                result : null,
                message : 'Fail'
            });
        }else {
            return res.send({
                code: 0,
                result : response,
                message : 'Success'
            });
        }

    });


});



module.exports = router;