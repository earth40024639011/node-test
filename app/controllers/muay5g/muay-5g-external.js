const MemberModel = require('../../models/member.model');
const BetTransactionModel = require('../../models/betTransaction.model.js');
const UserModel = require('../../models/users.model.js');
const AgentService = require('../../common/agentService');
const MemberService = require('../../common/memberService');
const ApiService = require('../../common/apiService');
const express = require('express')
const router = express.Router()
const async = require("async");
const Joi = require('joi');
const mongoose = require('mongoose');
const Hashing = require('../../common/hashing');
const config = require('config');
const _ = require('underscore');
const DateUtils = require('../../common/dateUtils');
const FormulaUtils = require('../../common/formula');
const roundTo = require('round-to');
const request = require('request');
const moment = require('moment');

const HOST = process.env.MUAY_5G_HOST || 'https://api.aspbet.com/';
const TOKEN = process.env.MUAY_5G_TOKEN || '4b2b6303956277ddfd7b8ea9cebb31db';

router.post('/bet_change_example', function (req, res) {

    if(req.body.bet_all){

    }else {
        return res.send({
            code : 1,
            msg : 'Data is empty.'
        })
    }

    console.log(`   JSON BODY => ${JSON.stringify(req.body)}`);

    let response = [];

    async.each(req.body.bet_all, (betObj, callbackWL) => {

        let condition = {
            ref1: betObj.bet_id,
            status: 'WAITING',
            // createdDate:{$gte: new Date(moment().add(-1, 'd').format('YYYY-MM-DDT11:00:00.000'))}
        };

        let status = 'RUNNING';

        async.waterfall([callback => {

            BetTransactionModel.findOne(condition, (err, response) => {
                if (err){
                    callback(err, null);
                }else{
                    callback(null, response);

                }
            });


        }, (betInfo, callback) => {

             if(!betInfo){
                callback('Bet ID not found.')
             }else {
                 if(betObj.is_complete == 1){
                     //Reject
                     status = 'REJECTED';
                     MemberService.updateBalance2(betInfo.memberUsername, Math.abs(betInfo.memberCredit), betInfo.betId, 'Match is end',betInfo.betId, function (err, creditResponse) {
                         if (err) {
                             callback(err, null);
                         } else {
                             callback(null, betInfo);
                         }
                     });
                 }else if(betObj.is_confirm == 1 && betObj.is_delete == 1){
                     //Reject
                     status = 'REJECTED';
                     MemberService.updateBalance2(betInfo.memberUsername, Math.abs(betInfo.memberCredit), betInfo.betId, '5G Reject',betInfo.betId, function (err, creditResponse) {
                         if (err) {
                             callback(err, null);
                         } else {
                             callback(null, betInfo);
                         }
                     });
                 }else {
                     //Accept
                     callback(null, betInfo);
                 }
             }

        }], function (err, betInfo) {
            if(err){
                callbackWL();
            }else {
                // console.log('----------',betInfo);
                let body = {
                    status : status,
                    remark : `5G ${status}`
                };

                BetTransactionModel.update({
                    betId: betInfo.betId
                }, body, (err, data) => {
                    if (err) {
                        console.log(err);
                        console.log('================= /update/ticket_live =================');
                        callbackWL();
                    } else if (data.nModified === 0) {
                        console.log('update ticket fail');
                        console.log('================= /update/ticket_live =================');
                        callbackWL();
                    } else {
                        console.log('================= /update/ticket_live =================');
                        response.push(betObj.bet_id);
                        callbackWL();
                    }
                });
            }

        });

    }, (err) => {

        return res.send(
            {
                "code": 0,
                "msg": "ok",
                "bet_id_all": response

            }
        );

    });

});

router.get('/count', function (req, res) {
    let data = {
        token : TOKEN,
        is_live : 'all'
    };

    request.post({
        url: `${HOST}apiservice/amb/api/match_list`,
        body: data,
        rejectUnauthorized: false,
        json:true
    }, function(error, response, body){
        if (error) {
            return res.send(error);
        } else {

            try {
                if (body.code === 0){

                    let dataList = [];
                    body.res.data_list.forEach(function (data) {
                        if(dataList.length > 0){

                            let sameId = false;
                            dataList.forEach(function (d) {
                                if(d.league_id === data.league_id){
                                    sameId = true;
                                    d.list_all.push(data)
                                }
                            });

                            if(!sameId){
                                dataList.push({
                                    league_id : data.league_id,
                                    league_name_th : data.ln_th,
                                    league_name_en : data.ln_en,
                                    cat_type : data.cat_id,
                                    list_all : [data]
                                });
                            }
                        }else {
                            dataList.push({
                                league_id : data.league_id,
                                league_name_th : data.ln_th,
                                league_name_en : data.ln_en,
                                cat_type : data.cat_id,
                                list_all : [data]
                            });
                        }
                    });

                    let countObj = {
                        cockFight : 0,
                        muay : 0
                    };

                    dataList.forEach(function (data) {
                        if(data.cat_type === '5'){
                            // muay
                            countObj.muay += data.list_all.length;
                        }else {
                            //cockfight
                            countObj.cockFight += data.list_all.length;
                        }

                    });

                    return res.send({
                        code: 0,
                        result : countObj,
                        message : 'Success'
                    });
                }else {
                    return res.send({
                        code: body.code,
                        result : [],
                        message : body.msg
                    });
                }


            } catch (e) {
                return res.send({
                    code: 1,
                    result : null,
                    message : 'Get match list fail'
                });
            }
        }
    });
});

const cancelTicketSchema = Joi.object().keys({
    ticketId: Joi.string().required(),
    remark: Joi.string().required()
});

router.post('/reject_ticket', function (req, res) {

    let validate = Joi.validate(req.body, cancelTicketSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }


    let reff = genKey(req.body.ticketId, '');

    let condition = {'ref1': req.body.ticketId, 'status' : 'RUNNING'};

    BetTransactionModel.findOne(condition)
        .select('memberId memberUsername hdp commission amount memberCredit payout memberParentGroup betId status priceType source betResult game')
        .populate({path: 'memberParentGroup', select: 'type'})
        .exec(function (err, betObj) {

            if (!betObj) {
                return res.send({message: "data not found", results: err, code: 5004});
            }

            if (betObj.status === 'CANCELLED' || betObj.status === 'REJECTED') {
                return res.send({message: "cancel already", results: err, code: 5001});
            }


            if (betObj.status === 'RUNNING'){
                let transaction = betObj;

                async.series([callback => {

                    MemberService.updateBalance2(transaction.memberUsername, Math.abs(transaction.memberCredit), transaction.betId, '5G_REJECT'+reff, transaction.betId, function (err, creditResponse) {

                        if (err) {
                            callback('fail', null);
                        } else {
                            callback(null, 'success');
                        }
                    });


                }], function (err, response) {

                    if (err) {
                        return res.send({message: "Cancel Ticket Fail", result: err, code: 5005});
                    }

                    let body = {
                        status: 'REJECTED',
                        remark: req.body.remark,
                        cancelByType:'PARTNER',
                        cancelByAgent:'PARTNER',
                        cancelDate:DateUtils.getCurrentDate()
                    };

                    BetTransactionModel.update({_id: transaction._id}, body, (err, data) => {
                        if (err) {
                            return res.send({message: "Cancel Ticket Fail", result: err, code: 5005});
                        } else if (data.nModified === 0) {
                            return res.send({message: "Cancel Ticket Fail", result: null, code: 5005});
                        } else {

                            if (transaction.memberParentGroup.type === 'API') {

                                ApiService.cancelBet(transaction.memberParentGroup.endpoint, req.body.remark, transaction, (err, response) => {
                                    if (err) {
                                        return res.send({message: "Cancel Ticket Fail", result: err, code: 5005});
                                    } else {
                                        return res.send({message: "Success", result: req.body.ticketId, code: 0});
                                    }

                                });
                            } else {
                                return res.send({message: "Success",result: req.body.ticketId, code: 0});
                            }
                        }
                    });
                });
            }else {
                return res.send({message: "Ticket is not Running", result: err, code: 5005});
            }

        });

});

const endScoreMuayThaiSchema = Joi.object().keys({
    matchId: Joi.string().required(),
    leagueId: Joi.string().required(),
    result: Joi.string().required(),
    homeScore: Joi.number().required(),
    awayScore: Joi.number().required(),
    ref: Joi.string().required()

});

router.post('/end_score', function (req, res) {

    let validate = Joi.validate(req.body, endScoreMuayThaiSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    if(req.body.result === 'wait'){
        //Change status to wait
        rollbackTicket(req.body, (err, response) => {
            if (err){
                return res.send({message: "error", results: err, code: 999});
            }else {
                return res.send({
                    code: 0,
                    message: "success",
                    result: 'success'
                });
            }
        });
    }else {
        //end score
        endScoreMuayThai(req.body, (err, response) => {
            if (err)
                return res.send({message: "error", results: err, code: 999});

            return res.send({
                code: 0,
                message: "success",
                result: 'success'
            });
        });
    }

});


router.post('/rollback-ticket', function (req, res) {

    rollbackTicketFootball(req.body, (err, response) => {
        if (err){
            return res.send({message: "error", results: err, code: 999});
        }else {
            return res.send({
                code: 0,
                message: "success",
                result: 'success'
            });
        }
    });

});

// router.get('/endGanapati', function (req, res) {
//
//     endScoreGanapati((err, response) => {
//         if (err){
//             res.send({message : 'fail'})
//         }else {
//             console.log('----------DONE----------', response)
//             res.send({message : 'success'})
//         }
//     });
//
// });

function genKey(matchId, ref) {
    return `${matchId}:${ref}`
}


// function endScoreGanapati(ganapatiCallback) {
//     let condition = {
//         'source' : 'GANAPATI',
//         'status' : 'RUNNING'
//     };
//
//     console.log(condition);
//
//     let cursor = BetTransactionModel.find(condition)
//         .batchSize(200)
//         .select('memberId hdp commission amount memberCredit payout memberParentGroup betId status priceType betResult game gameType source')
//         .populate({path: 'memberParentGroup', select: 'type endpoint secretKey'})
//         .populate({path: 'memberId', select: 'username_lower'})
//         .lean()
//         .cursor();
//
//     cursor.on('data', function (betObj) {
//         let winLose = 0 - betObj.amount;
//         let betResult = winLose > 0 ? 'WIN' : winLose < 0 ? 'LOSE' : 'DRAW';
//
//         // console.log(betObj.betId,'-------',betResult, '-------',winLose)
//
//         const shareReceive = AgentService.prepareShareReceive(winLose, betResult, betObj);
//
//         let updateBody = {
//             $set: {
//
//                 'slot.ganapati.roundEnd': true,
//                 'slot.ganapati.winLoss': winLose,
//                 'commission.superAdmin.winLoseCom': shareReceive.superAdmin.winLoseCom,
//                 'commission.superAdmin.winLose': shareReceive.superAdmin.winLose,
//                 'commission.superAdmin.totalWinLoseCom': shareReceive.superAdmin.totalWinLoseCom,
//
//                 'commission.company.winLoseCom': shareReceive.company.winLoseCom,
//                 'commission.company.winLose': shareReceive.company.winLose,
//                 'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,
//
//                 'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
//                 'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
//                 'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,
//
//                 'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
//                 'commission.senior.winLose': shareReceive.senior.winLose,
//                 'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,
//
//                 'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
//                 'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
//                 'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,
//
//                 'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
//                 'commission.agent.winLose': shareReceive.agent.winLose,
//                 'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,
//
//                 'commission.member.winLoseCom': shareReceive.member.winLoseCom,
//                 'commission.member.winLose': shareReceive.member.winLose,
//                 'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
//                 'betResult': betResult,
//                 'isEndScore': true,
//                 'status': 'DONE'
//             }
//         };
//
//         BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
//             if (response.nModified == 1) {
//                 MemberService.updateBalance(betObj.memberId, 0, betObj.betId, 'SETTLE_GANAPATI_PATCH', 'PATCH' + new Date(), function (err, creditResponse) {
//                     if (err) {
//                         console.log('------------------error-----------------')
//                     } else {
//                         console.log('------------------success-----------------', creditResponse.newBalance)
//                     }
//                 });
//             } else {
//                 console.log('------------------cant update-----------------')
//             }
//         });
//     });
//
//     cursor.on('end', function () {
//         console.log('Done!');
//
//         ganapatiCallback(null, 'success');
//     });
//
// }


function endScoreMuayThai(requestBody, endScoreCallback) {
    let matchId = `${requestBody.leagueId}:${requestBody.matchId}`;
    let result = requestBody.result;
    let reff = genKey(requestBody.matchId, requestBody.ref);


    async.parallel([(callback => {

        let condition = {
            // '$or': [{'game': 'FOOTBALL'}, {'game': 'M2'}],
            'game': 'M2',
            'hdp.matchId': matchId,
            'gameType': 'TODAY',
            'active': true,
            $or: [{'status': 'RUNNING'}, {'status': 'REJECTED'}]
            // 'source' : 'M2_MUAY'
        };


        // console.log(condition)

        let cursor = BetTransactionModel.find(condition)
            .batchSize(200)
            .select('memberId hdp commission amount memberCredit payout memberParentGroup betId status priceType betResult game gameType source memberUsername')
            .populate({path: 'memberParentGroup', select: 'type endpoint secretKey'})
            .populate({path: 'memberId', select: 'username_lower'})
            .lean()
            .cursor();

        cursor.on('data', function (betObj) {

            // console.log('-----------------',betObj);
            if (betObj.status === 'REJECTED') {
                let updateBody = {
                    $set: {
                        'isEndScore': true
                    }
                };

                BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                    if (err) {
                        // asyncCallback(err, null);
                    } else {
                        // asyncCallback(null, response);
                        console.log('xxx')
                    }
                });

            } else {
                //NOT EQUAL REJECTED

                let betResult;

                let _homeScore, _awayScore, _round;

                if (result === 'home') {
                    _round = requestBody.homeScore;
                } else if (result === 'away') {
                    _round = requestBody.awayScore;
                } else if (result === 'draw') {
                    _round = requestBody.homeScore;
                }else{
                    _round = requestBody.homeScore;
                }

                _homeScore = requestBody.homeScore;
                _awayScore = requestBody.awayScore;

                if (betObj.hdp.oddType === 'AH') {
                    // betResult = calculateHandicap(_homeScore, _awayScore, betObj.hdp.handicap, betObj.hdp.bet);
                    if(betObj.hdp.bet === 'HOME'){
                        if(result === 'home'){
                            betResult = 'WIN'
                        }else if(result === 'away'){
                            betResult = 'LOSE'
                        }else {
                            betResult = 'DRAW'
                        }

                    }else{
                        // Away
                        if(result === 'home'){
                            betResult = 'LOSE'
                        }else if(result === 'away'){
                            betResult = 'WIN'
                        }else {
                            betResult = 'DRAW'
                        }
                    }


                } else if (betObj.hdp.oddType === 'OU') {
                    // betResult = calculateOuScore(_round, 0, betObj.hdp.handicap, betObj.hdp.bet);
                    if(betObj.hdp.bet === 'OVER'){
                        if(result === 'home' || result === 'away'){
                            if(Number.parseInt(_round) > 5){
                                betResult = 'WIN';
                            }else {
                                betResult = 'LOSE'
                            }
                        }else if(result === 'draw'){
                            betResult = 'WIN'
                        }else if(result === 'refunded'){
                            if(Number.parseInt(_homeScore) == 0 && Number.parseInt(_awayScore) == 0){
                                betResult = 'DRAW'
                            }else {
                                betResult = 'WIN'
                            }

                        }else {
                            //refund
                            betResult = 'DRAW'
                        }

                    }else{
                        // Under
                        if(result === 'home' || result === 'away'){
                            if(Number.parseInt(_round) > 5){
                                betResult = 'LOSE';
                            }else {
                                betResult = 'WIN'
                            }
                        }else if(result === 'draw'){
                            betResult = 'LOSE'
                        }else if(result === 'refunded'){
                            if(Number.parseInt(_homeScore) == 0 && Number.parseInt(_awayScore) == 0){
                                betResult = 'DRAW'
                            }else {
                                betResult = 'LOSE'
                            }
                        }else {
                            //refund
                            betResult = 'DRAW'
                        }
                    }
                }


                // console.log("betResult : ", betResult);

                let amount = Math.abs(betObj.memberCredit);
                let validAmount;
                let winLose = 0;
                if (betResult === 'DRAW') {
                    winLose = 0;
                    validAmount = 0;
                } else if (betResult === 'WIN') {
                    FormulaUtils.calculatePayout(betObj.amount, betObj.hdp.odd, betObj.hdp.oddType, betObj.priceType, (payout) => {
                        winLose = Number.parseFloat(payout) - betObj.amount;
                    });
                    validAmount = betObj.amount;
                }else{
                    //lose
                    winLose = 0 - amount;
                    validAmount = betObj.amount;
                }

                const shareReceive = prepareShareReceive(winLose, betResult, betObj);

                let transaction = {
                    _id: betObj._id,
                    memberId: betObj.memberId,
                    matchId: matchId,
                    type: requestBody.type,
                    handicap: betObj.hdp.handicap,
                    oddType: betObj.hdp.oddType,
                    bet: betObj.hdp.bet,
                    betAmount: betObj.amount,
                    homeScoreFT: _homeScore,
                    awayScoreFT: _awayScore,
                    result: betResult,
                    transStatus: 'DONE',
                    validAmount: validAmount,
                    winLose: shareReceive
                };

                updateAgentMemberBalance(shareReceive, 'TODAY', reff+'_'+betObj.betId, (err, response) => {
                    if (err) {
                        // console.log('error============',err);
                        if (err == 888) {
                            // asyncCallback(null, {});
                        } else {
                            // asyncCallback(err, null);
                        }
                    } else {
                        async.series([subCallback => {

                            let updateBody = {
                                $set: {
                                    'hdp.fullScore': transaction.homeScoreFT + ':' + transaction.awayScoreFT + ':' + _round,

                                    'commission.superAdmin.winLoseCom': transaction.winLose.superAdmin.winLoseCom,
                                    'commission.superAdmin.winLose': transaction.winLose.superAdmin.winLose,
                                    'commission.superAdmin.totalWinLoseCom': transaction.winLose.superAdmin.totalWinLoseCom,

                                    'commission.company.winLoseCom': transaction.winLose.company.winLoseCom,
                                    'commission.company.winLose': transaction.winLose.company.winLose,
                                    'commission.company.totalWinLoseCom': transaction.winLose.company.totalWinLoseCom,

                                    'commission.shareHolder.winLoseCom': transaction.winLose.shareHolder.winLoseCom,
                                    'commission.shareHolder.winLose': transaction.winLose.shareHolder.winLose,
                                    'commission.shareHolder.totalWinLoseCom': transaction.winLose.shareHolder.totalWinLoseCom,

                                    'commission.api.winLoseCom': transaction.winLose.api.winLoseCom,
                                    'commission.api.winLose': transaction.winLose.api.winLose,
                                    'commission.api.totalWinLoseCom': transaction.winLose.api.totalWinLoseCom,

                                    'commission.senior.winLoseCom': transaction.winLose.senior.winLoseCom,
                                    'commission.senior.winLose': transaction.winLose.senior.winLose,
                                    'commission.senior.totalWinLoseCom': transaction.winLose.senior.totalWinLoseCom,

                                    'commission.masterAgent.winLoseCom': transaction.winLose.masterAgent.winLoseCom,
                                    'commission.masterAgent.winLose': transaction.winLose.masterAgent.winLose,
                                    'commission.masterAgent.totalWinLoseCom': transaction.winLose.masterAgent.totalWinLoseCom,

                                    'commission.agent.winLoseCom': transaction.winLose.agent.winLoseCom,
                                    'commission.agent.winLose': transaction.winLose.agent.winLose,
                                    'commission.agent.totalWinLoseCom': transaction.winLose.agent.totalWinLoseCom,

                                    'commission.member.winLoseCom': transaction.winLose.member.winLoseCom,
                                    'commission.member.winLose': transaction.winLose.member.winLose,
                                    'commission.member.totalWinLoseCom': transaction.winLose.member.totalWinLoseCom,
                                    'validAmount': transaction.validAmount,
                                    'betResult': transaction.result,
                                    'isEndScore': true,
                                    'status': 'DONE',
                                    'settleDate': DateUtils.getCurrentDate()
                                }
                            };

                            // console.log('body = ',updateBody);

                            BetTransactionModel.findOneAndUpdate({_id: transaction._id}, updateBody, {new: true}, function (err, response) {
                                if (err) {
                                    subCallback(err, null);
                                } else {
                                    let object = Object.assign({}, response)._doc;
                                    object.memberParentGroup = betObj.memberParentGroup;
                                    object.memberId = betObj.memberId;
                                    subCallback(null, response);
                                }
                            });

                        }], function (err, asyncResponse) {
                            if (err) {
                                // asyncCallback(err, null);
                            } else {
                                // keepTransaction.push(asyncResponse[0]);
                                // asyncCallback(null, 'success');
                                console.log('success 1')
                            }
                        });
                    }

                });

            }
        });

        cursor.on('end', function () {
            console.log('Done!');

            callback(null, {});
        });


    }), (callback => {

        let condition = {
            'game' : 'M2',
            'gameType': {$ne: 'TODAY'},
            'parlay.matches.matchId':matchId,
            active: true
        };

        // console.log('con 111  === ',condition);
        let cursor = BetTransactionModel.find(condition)
            .batchSize(200)
            .select('memberId parlay commission amount memberCredit payout memberParentGroup status betId betResult game gameType source memberUsername')
            .populate({path: 'memberParentGroup', select: 'type endpoint'})
            .populate({path: 'memberId', select: 'username_lower'})
            .lean()
            .cursor();


        cursor.on('data', function (betObj) {

            console.log('bet obj ==',betObj);

            const allMatch = betObj.parlay.matches;

            let filterList = _.filter(allMatch, function (response) {
                return response.matchId === matchId;
            });

            if (filterList.length === 0) {
                // betListCallback(null, 'not data found');
                return;
            }


            let unCheckMatch = _.filter(allMatch, function (response) {
                return response.matchId !== matchId && !response.betResult;
            });

            // console.log('unCheckMatch : ', unCheckMatch.length);

            const match = filterList[0];

            let _homeScore, _awayScore, _round;

            if (result === 'home') {
                _round = requestBody.homeScore;
            } else if (result === 'away') {
                _round = requestBody.awayScore;
            } else if (result === 'draw') {
                _round = requestBody.homeScore;
            }else{
                _round = requestBody.homeScore;
            }

            _homeScore = requestBody.homeScore;
            _awayScore = requestBody.awayScore;

            const fullScoreResult = _homeScore + ':' + _awayScore + ':' + _round;

            if (betObj.status === 'DONE' || betObj.status === 'REJECTED' || betObj.status === 'CANCELLED') {
                let updateBody = {
                    $set: {
                        'parlay.matches.$.fullScore': fullScoreResult
                    }
                };

                BetTransactionModel.update({
                    _id: betObj._id,
                    'parlay.matches._id': match._id
                }, updateBody, function (err, response) {
                    if (err) {
                        // betListCallback(err, null);
                    } else {
                        // betListCallback(null, response);
                    }
                });

            } else {

                let betResult;

                if (match.oddType === 'AH') {
                    // betResult = calculateHandicap(_homeScore, _awayScore, match.handicap, match.bet);
                    if(match.bet === 'HOME'){
                        if(result === 'home'){
                            betResult = 'WIN'
                        }else if(result === 'away'){
                            betResult = 'LOSE'
                        }else if(result === 'refunded'){

                            if(Number.parseInt(_homeScore) == 7 && Number.parseInt(_awayScore) == 0){
                                betResult = 'WIN'
                            }else if(Number.parseInt(_homeScore) == 0 && Number.parseInt(_awayScore) == 7){
                                betResult = 'LOSE'
                            }else {
                                betResult = 'DRAW'
                            }

                        }else {
                            betResult = 'DRAW'
                        }

                    }else{
                        // Away
                        if(result === 'home'){
                            betResult = 'LOSE'
                        }else if(result === 'away'){
                            betResult = 'WIN'
                        }else if(result === 'refunded'){

                            if(Number.parseInt(_homeScore) == 7 && Number.parseInt(_awayScore) == 0){
                                betResult = 'LOSE'
                            }else if(Number.parseInt(_homeScore) == 0 && Number.parseInt(_awayScore) == 7){
                                betResult = 'WIN'
                            }else {
                                betResult = 'DRAW'
                            }

                        }else {
                            betResult = 'DRAW'
                        }
                    }


                } else if (match.oddType === 'OU') {
                    // betResult = calculateOuScore(_round, 0, match.handicap, match.bet);
                    if(match.bet === 'OVER'){
                        if(result === 'home' || result === 'away'){
                            if(Number.parseInt(_round) > 5){
                                betResult = 'WIN';
                            }else {
                                betResult = 'LOSE'
                            }
                        }else if(result === 'draw'){
                            betResult = 'WIN'
                        }else if(result === 'refunded'){

                            if(Number.parseInt(_homeScore) == 0 && Number.parseInt(_awayScore) == 0){
                                betResult = 'DRAW'
                            }else{
                                betResult = 'WIN'
                            }
                        }else {
                            //refund
                            betResult = 'DRAW'
                        }

                    }else{
                        // Under
                        if(result === 'home' || result === 'away'){
                            if(Number.parseInt(_round) > 5){
                                betResult = 'LOSE';
                            }else {
                                betResult = 'WIN'
                            }
                        }else if(result === 'draw'){
                            betResult = 'LOSE'
                        }else if(result === 'refunded'){
                            if(Number.parseInt(_homeScore) == 0 && Number.parseInt(_awayScore) == 0){
                                betResult = 'DRAW'
                            }else{
                                betResult = 'LOSE'
                            }
                        }else {
                            //refund
                            betResult = 'DRAW'
                        }
                    }
                }

                let updateBody;
                let isCalculateReceive = false;
                let winLoseAmount = 0;
                let shareReceive;

                // console.log('bet result ===== ',betResult);
                if (betResult === 'LOSE') {
                    isCalculateReceive = true;
                    winLoseAmount = 0 - betObj.amount;
                    shareReceive = prepareShareReceive(winLoseAmount, 'LOSE', betObj);
                    updateBody = {
                        $set: {
                            'parlay.matches.$.fullScore': fullScoreResult,
                            'parlay.matches.$.betResult': betResult,

                            'commission.superAdmin.winLoseCom': shareReceive.superAdmin.winLoseCom,
                            'commission.superAdmin.winLose': shareReceive.superAdmin.winLose,
                            'commission.superAdmin.totalWinLoseCom': shareReceive.superAdmin.totalWinLoseCom,

                            'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                            'commission.company.winLose': shareReceive.company.winLose,
                            'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                            'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                            'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                            'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                            'commission.api.winLoseCom': shareReceive.api.winLoseCom,
                            'commission.api.winLose': shareReceive.api.winLose,
                            'commission.api.totalWinLoseCom': shareReceive.api.totalWinLoseCom,

                            'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                            'commission.senior.winLose': shareReceive.senior.winLose,
                            'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                            'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                            'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                            'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                            'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                            'commission.agent.winLose': shareReceive.agent.winLose,
                            'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                            'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                            'commission.member.winLose': shareReceive.member.winLose,
                            'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                            'validAmount': betObj.amount,
                            'betResult': betResult,
                            'isEndScore': true,
                            'status': 'DONE',
                            'settleDate': DateUtils.getCurrentDate()
                        }
                    };
                } else {
                    // NOT EQUALS LOSE

                    if (unCheckMatch.length == 0) {
                        // console.log('no unCheck match');
                        isCalculateReceive = true;

                        let totalUnCheckOdds = _.filter(allMatch, function (response) {
                            return response.matchId !== matchId && response.betResult;
                        }).map((item) => {
                            return calculateParlayOdds(item.betResult, item.odd);
                        });

                        // console.log(' success match');
                        // console.log(totalUnCheckOdds);


                        let sumTotalUnCheckOdds = _.reduce(totalUnCheckOdds, (memo, item) => {
                            return {sum: memo.sum * item};
                        }, {sum: 1}).sum;


                        let halfLoseCount = _.filter(allMatch, function (response) {
                            return response.betResult === 'HALF_LOSE' && response.matchId !== matchId;
                        }).length;

                        //prepare with last match result
                        sumTotalUnCheckOdds = roundTo.down(sumTotalUnCheckOdds * calculateParlayOdds(betResult, match.odd), 3);

                        if (betResult === 'HALF_LOSE') {
                            halfLoseCount += 1;
                        }

                        // console.log('sumTotalUnCheckOdds : ', sumTotalUnCheckOdds);
                        // console.log('halfLoseCount : ', halfLoseCount);

                        winLoseAmount = (subPlyAmount(betObj.amount, halfLoseCount) * sumTotalUnCheckOdds);
                        let validAmount = subPlyAmount(betObj.amount, halfLoseCount);

                        winLoseAmount = winLoseAmount - betObj.amount;

                        shareReceive = prepareShareReceive(winLoseAmount, 'WIN', betObj);
                        updateBody = {
                            $set: {
                                'parlay.matches.$.fullScore': fullScoreResult,
                                'parlay.matches.$.betResult': betResult,

                                'commission.superAdmin.winLoseCom': shareReceive.superAdmin.winLoseCom,
                                'commission.superAdmin.winLose': shareReceive.superAdmin.winLose,
                                'commission.superAdmin.totalWinLoseCom': shareReceive.superAdmin.totalWinLoseCom,

                                'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                                'commission.company.winLose': shareReceive.company.winLose,
                                'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                                'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                                'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                                'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                                'commission.api.winLoseCom': shareReceive.api.winLoseCom,
                                'commission.api.winLose': shareReceive.api.winLose,
                                'commission.api.totalWinLoseCom': shareReceive.api.totalWinLoseCom,

                                'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                                'commission.senior.winLose': shareReceive.senior.winLose,
                                'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                                'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                                'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                                'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                                'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                                'commission.agent.winLose': shareReceive.agent.winLose,
                                'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                                'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                                'commission.member.winLose': shareReceive.member.winLose,
                                'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                                'validAmount': validAmount,
                                'betResult': 'WIN',
                                'isEndScore': true,
                                'status': 'DONE',
                                'settleDate': DateUtils.getCurrentDate()
                            }
                        };
                        updateBody.$set.status = 'DONE';

                    } else {
                        updateBody = {
                            $set: {
                                'parlay.matches.$.fullScore': fullScoreResult,
                                'parlay.matches.$.betResult': betResult,
                                // 'betResult': betResult,
                            }
                        };
                        // console.log('have unCheck match : ', unCheckMatch);
                        // console.log(unCheckMatch);

                    }
                }


                // console.log('isCalculateReceive : ', isCalculateReceive)
                if (isCalculateReceive) {
                    updateAgentMemberBalance(shareReceive, 'PARLAY', reff+'_'+betObj.betId, (err, response) => {
                        if (err) {
                            if (err == 888) {
                                // betListCallback(null, {});
                            } else {
                                // betListCallback(err, null);
                            }
                        } else {
                            BetTransactionModel.findOneAndUpdate({
                                _id: betObj._id,
                                'parlay.matches._id': match._id
                            }, updateBody, {new: true}, function (err, response) {
                                if (err) {
                                    // betListCallback(err, null);
                                    console.log(err)
                                } else {
                                    let object = Object.assign({}, response)._doc;
                                    object.memberParentGroup = betObj.memberParentGroup;
                                    object.memberId = betObj.memberId;

                                    // betListCallback(null, response);
                                    console.log('DONE')

                                }
                            });
                        }
                    });
                } else {

                    BetTransactionModel.findOneAndUpdate({
                        _id: betObj._id,
                        'parlay.matches._id': match._id
                    }, updateBody, {new: true}, function (err, response) {
                        if (err) {
                            // betListCallback(err, null);
                        } else {

                            console.log('DONE')

                        }
                    });
                }
            }
        });

        cursor.on('end', function () {
            console.log('Done!!');
            callback(null, {});
        });


    })], (err, asyncResponse) => {

        if (err) {
            endScoreCallback(err, null);
            return;
        }
        endScoreCallback(null, 'success');
    });

}

function rollbackTicket(requestBody, rollbackCallback) {
    let matchId = `${requestBody.leagueId}:${requestBody.matchId}`;
    let result = requestBody.result;
    let reff = genKey(requestBody.matchId, requestBody.ref);

    async.parallel([(callback => {

        let condition = {
            'hdp.matchId': matchId,
            'gameType': 'TODAY',
            active: true,
            'status' : 'DONE',
            // createdDate:{$gte: new Date(moment().add(-1, 'd').format('YYYY-MM-DDT11:00:00.000'))}
        };

        BetTransactionModel.find(condition)
            .select('memberId hdp commission amount memberCredit payout memberParentGroup betId status priceType betResult game gameType source memberUsername')
            .populate({path: 'memberParentGroup', select: 'type'})
            .exec(function (err, response) {


                if (!response) {
                    callback(null, 'success');
                    return;
                }

                if (response.length === 0) {
                    callback(null, '');
                    return;
                }

                async.each(response, (betObj, asyncCallback) => {

                    let betResult = 'DRAW';

                    // if (req.body.result === betObj.hdp.bet) {
                    //     betResult = 'WIN'
                    // } else if (req.body.result === 'DRAW') {
                    //     betResult = 'DRAW';
                    // } else {
                    //     betResult = 'LOSE'
                    // }

                    let amount = Math.abs(betObj.memberCredit);
                    let validAmount;
                    let winLose = 0;
                    if (betResult === 'DRAW') {
                        winLose = 0;
                        validAmount = 0;
                    } else if (betResult === 'WIN') {
                        FormulaUtils.calculatePayout(betObj.amount, betObj.hdp.odd, betObj.hdp.oddType, betObj.priceType, (payout) => {
                            winLose = Number.parseFloat(payout) - betObj.amount;
                        });
                        validAmount = betObj.amount;
                    } else {
                        winLose = 0 - amount;
                        validAmount = betObj.amount;
                    }

                    const shareReceive = prepareShareReceive(winLose, betResult, betObj);

                    async.series([subCallback => {
                        //refundBalance
                        AgentService.rollbackCalculateAgentMemberBalance(betObj, reff+'_'+betObj.betId, (err, response) => {
                            if (err) {
                                subCallback(err, null);
                            } else {
                                subCallback(null, response);
                            }
                        });

                    }, subCallback => {

                        let updateBody = {
                            $set: {
                                'hdp.score': '',
                                'commission.superAdmin.winLoseCom': shareReceive.superAdmin.winLoseCom,
                                'commission.superAdmin.winLose': shareReceive.superAdmin.winLose,
                                'commission.superAdmin.totalWinLoseCom': shareReceive.superAdmin.totalWinLoseCom,

                                'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                                'commission.company.winLose': shareReceive.company.winLose,
                                'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                                'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                                'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                                'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                                'commission.api.winLoseCom': shareReceive.api.winLoseCom,
                                'commission.api.winLose': shareReceive.api.winLose,
                                'commission.api.totalWinLoseCom': shareReceive.api.totalWinLoseCom,

                                'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                                'commission.senior.winLose': shareReceive.senior.winLose,
                                'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                                'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                                'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                                'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                                'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                                'commission.agent.winLose': shareReceive.agent.winLose,
                                'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                                'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                                'commission.member.winLose': shareReceive.member.winLose,
                                'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                                'validAmount': validAmount,
                                'betResult': '',
                                'isEndScore': false,
                                'status': 'RUNNING'
                            }
                        };

                        BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                            if (err) {
                                subCallback(err, null);
                            } else {
                                subCallback(null, response);
                            }
                        });

                    }], function (err, response) {
                        if (err) {
                            asyncCallback(err, null);
                        } else {
                            asyncCallback(null, response);
                        }
                    });

                }, (err) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, 'success');
                    }
                });//end forEach Match
            });


    }), (callback => {



        let condition = {
            'game': {$in: ['FOOTBALL', 'M2']},
            'gameType': {$ne: 'TODAY'},
            active: true
        };
        condition['parlay.matches.matchId'] = matchId;

        let cursor = BetTransactionModel.find(condition)
            .batchSize(200)
            .select('memberId parlay commission amount memberCredit payout memberParentGroup status betId betResult game gameType source memberUsername')
            .populate({path: 'memberParentGroup', select: 'type endpoint'})
            .populate({path: 'memberId', select: 'username_lower'})
            .lean()
            .cursor();


        cursor.on('data', function (betObj) {

            const allMatch = betObj.parlay.matches;

            let filterList = _.filter(allMatch, function (response) {
                return response.matchId === matchId;
            });

            if (filterList.length === 0) {
                // betListCallback(null, 'not data found');
                return;
            }


            let unCheckMatch = _.filter(allMatch, function (response) {
                return response.matchId !== matchId && !response.betResult;
            });

            console.log('unCheckMatch : ', unCheckMatch.length);

            const match = filterList[0];

            let betResult = 'DRAW';

            let updateBody;
            let isCalculateReceive = false;
            let isRefundBalance = false;
            let winLoseAmount = 0;
            let shareReceive;

            let isUnCheckMatchLose = _.filter(allMatch, function (response) {
                return response.matchId !== matchId && response.betResult === 'LOSE';
            }).length > 0;

            if (isUnCheckMatchLose) {
                //มีคู่อื่นที่แพ้
                console.log('-----------have match lose--------------')
                isRefundBalance = true;
                updateBody = {
                    $set: {
                        'parlay.matches.$.fullScore': '',
                        'parlay.matches.$.betResult': '',
                        'validAmount': betObj.amount,
                        'betResult': '',
                        'isEndScore': false,
                        'status': 'DONE',
                        'settleDate': DateUtils.getCurrentDate()
                    }
                };
            } else {
                // NOT EQUALS LOSE

                if (unCheckMatch.length == 0 || betObj.betResult === 'LOSE') {
                    console.log('-----------all match is end--------------')
                    //คู่อื่นจบหมดแล้ว
                    console.log('no unCheck match');
                    isRefundBalance = true;
                    isCalculateReceive = true;

                    let totalUnCheckOdds = _.filter(allMatch, function (response) {
                        return response.matchId !== matchId && response.betResult;
                    }).map((item) => {
                        return calculateParlayOdds(item.betResult, item.odd);
                    });

                    let sumTotalUnCheckOdds = _.reduce(totalUnCheckOdds, (memo, item) => {
                        return {sum: memo.sum * item};
                    }, {sum: 1}).sum;


                    let halfLoseCount = _.filter(allMatch, function (response) {
                        return response.betResult === 'HALF_LOSE' && response.matchId !== matchId;
                    }).length;

                    //prepare with last match result
                    sumTotalUnCheckOdds = roundTo.down(sumTotalUnCheckOdds * calculateParlayOdds(betResult, match.odd), 3);

                    if (betResult === 'HALF_LOSE') {
                        halfLoseCount += 1;
                    }

                    console.log('sumTotalUnCheckOdds : ', sumTotalUnCheckOdds);
                    console.log('halfLoseCount : ', halfLoseCount);

                    winLoseAmount = (subPlyAmount(betObj.amount, halfLoseCount) * sumTotalUnCheckOdds);
                    let validAmount = subPlyAmount(betObj.amount, halfLoseCount);

                    winLoseAmount = winLoseAmount - betObj.amount;

                    shareReceive = prepareShareReceive(winLoseAmount, 'DRAW', betObj);
                    updateBody = {
                        $set: {
                            'parlay.matches.$.fullScore': '',
                            'parlay.matches.$.betResult': '',
                            'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                            'commission.company.winLose': shareReceive.company.winLose,
                            'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                            'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                            'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                            'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                            'commission.api.winLoseCom': shareReceive.api.winLoseCom,
                            'commission.api.winLose': shareReceive.api.winLose,
                            'commission.api.totalWinLoseCom': shareReceive.api.totalWinLoseCom,

                            'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                            'commission.senior.winLose': shareReceive.senior.winLose,
                            'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                            'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                            'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                            'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                            'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                            'commission.agent.winLose': shareReceive.agent.winLose,
                            'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                            'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                            'commission.member.winLose': shareReceive.member.winLose,
                            'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                            'validAmount': validAmount,
                            'betResult': '',
                            'isEndScore': false,
                            'status': 'RUNNING',
                            'settleDate': ''
                        }
                    };
                    updateBody.$set.status = 'RUNNING';

                    // console.log('update body == ',updateBody);

                } else {
                    console.log('-----------มีคู่ที่ยังไม่จบ--------------')
                    updateBody = {
                        $set: {
                            'parlay.matches.$.fullScore': '',
                            'parlay.matches.$.betResult': '',
                            'validAmount': betObj.amount,
                            'betResult': '',
                            'isEndScore': false,
                            'status': 'RUNNING',
                            'settleDate': ''

                            // 'betResult': betResult,
                        }
                    };
                    // console.log('have unCheck match : ', unCheckMatch);
                    // console.log(unCheckMatch);

                }
            }

            console.log('----isRefundBalance-----',isRefundBalance);
            async.series([callback => {
                //refundBalance
                if (isRefundBalance) {
                    AgentService.rollbackCalculateAgentMemberBalance(betObj, reff+'_'+betObj.betId, (err, response) => {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, response);
                        }
                    });
                } else {
                    callback(null, '');
                }

            }, callback => {

                BetTransactionModel.findOneAndUpdate({
                    _id: betObj._id,
                    'parlay.matches._id': match._id
                }, updateBody, {new: true}, function (err, response) {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, '');
                    }
                });

            }], function (err, asyncResponse) {
                if (err) {

                } else {
                    console.log('success 1')
                }
            });



        });

        cursor.on('end', function () {
            console.log('Done!');
            callback(null, {});
        });

    })], (err, asyncResponse) => {

        if (err){
            rollbackCallback(err, null);
        }else {
            rollbackCallback(null, 'success');
        }




    });
}

// function rollbackTicketFootball(requestBody, rollbackCallback) {
//     let matchId = `${requestBody.leagueId}:${requestBody.matchId}`;
//     let result = requestBody.result;
//     let reff = genKey(requestBody.matchId);
//
//     async.parallel([(callback => {
//
//         let condition = {
//             'hdp.matchId': matchId,
//             'gameType': 'TODAY',
//             active: true,
//             'status' : 'DONE',
//             'game' : 'FOOTBALL',
//             // createdDate:{$gte: new Date(moment().add(-1, 'd').format('YYYY-MM-DDT11:00:00.000'))}
//         };
//
//         BetTransactionModel.find(condition)
//             .select('memberId hdp commission amount memberCredit payout memberParentGroup betId status priceType betResult game gameType source')
//             .populate({path: 'memberParentGroup', select: 'type'})
//             .exec(function (err, response) {
//
//
//                 if (!response) {
//                     callback(null, 'success');
//                     return;
//                 }
//
//                 if (response.length === 0) {
//                     callback(null, '');
//                     return;
//                 }
//
//                 // console.log('res=== ',response);
//
//                 async.each(response, (betObj, asyncCallback) => {
//
//                     let betResult = 'DRAW';
//
//                     // if (req.body.result === betObj.hdp.bet) {
//                     //     betResult = 'WIN'
//                     // } else if (req.body.result === 'DRAW') {
//                     //     betResult = 'DRAW';
//                     // } else {
//                     //     betResult = 'LOSE'
//                     // }
//
//                     let amount = Math.abs(betObj.memberCredit);
//                     let validAmount;
//                     let winLose = 0;
//                     if (betResult === 'DRAW') {
//                         winLose = 0;
//                         validAmount = 0;
//                     } else if (betResult === 'WIN') {
//                         FormulaUtils.calculatePayout(betObj.amount, betObj.hdp.odd, betObj.hdp.oddType, betObj.priceType, (payout) => {
//                             winLose = Number.parseFloat(payout) - betObj.amount;
//                         });
//                         validAmount = betObj.amount;
//                     } else {
//                         winLose = 0 - amount;
//                         validAmount = betObj.amount;
//                     }
//
//                     const shareReceive = prepareShareReceive(winLose, betResult, betObj);
//
//                     async.series([subCallback => {
//                         //refundBalance
//                         AgentService.rollbackCalculateAgentMemberBalance(betObj, reff, (err, response) => {
//                             if (err) {
//                                 subCallback(err, null);
//                             } else {
//                                 subCallback(null, response);
//                             }
//                         });
//
//                     }, subCallback => {
//
//                         let updateBody = {
//                             $set: {
//                                 // 'hdp.score': '',
//                                 'commission.superAdmin.winLoseCom': shareReceive.superAdmin.winLoseCom,
//                                 'commission.superAdmin.winLose': shareReceive.superAdmin.winLose,
//                                 'commission.superAdmin.totalWinLoseCom': shareReceive.superAdmin.totalWinLoseCom,
//
//                                 'commission.company.winLoseCom': shareReceive.company.winLoseCom,
//                                 'commission.company.winLose': shareReceive.company.winLose,
//                                 'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,
//
//                                 'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
//                                 'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
//                                 'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,
//
//                                 'commission.api.winLoseCom': shareReceive.api.winLoseCom,
//                                 'commission.api.winLose': shareReceive.api.winLose,
//                                 'commission.api.totalWinLoseCom': shareReceive.api.totalWinLoseCom,
//
//                                 'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
//                                 'commission.senior.winLose': shareReceive.senior.winLose,
//                                 'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,
//
//                                 'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
//                                 'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
//                                 'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,
//
//                                 'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
//                                 'commission.agent.winLose': shareReceive.agent.winLose,
//                                 'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,
//
//                                 'commission.member.winLoseCom': shareReceive.member.winLoseCom,
//                                 'commission.member.winLose': shareReceive.member.winLose,
//                                 'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
//                                 'validAmount': validAmount,
//                                 'betResult': '',
//                                 'isEndScore': false,
//                                 'status': 'RUNNING'
//                             }
//                         };
//
//                         BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
//                             if (err) {
//                                 subCallback(err, null);
//                             } else {
//                                 subCallback(null, response);
//                             }
//                         });
//
//                     }], function (err, response) {
//                         if (err) {
//                             asyncCallback(err, null);
//                         } else {
//                             asyncCallback(null, response);
//                         }
//                     });
//
//                 }, (err) => {
//                     if (err) {
//                         callback(err, null);
//                     } else {
//                         callback(null, 'success');
//                     }
//                 });//end forEach Match
//             });
//
//
//     }), (callback => {
//
//
//
//         let condition = {
//             'game': {$in: ['FOOTBALL', 'M2']},
//             'gameType': {$ne: 'TODAY'},
//             active: true,
//             status : 'DONE'
//         };
//         condition['parlay.matches.matchId'] = matchId;
//
//         let cursor = BetTransactionModel.find(condition)
//             .batchSize(200)
//             .select('memberId parlay commission amount memberCredit payout memberParentGroup status betId betResult game gameType source')
//             .populate({path: 'memberParentGroup', select: 'type endpoint'})
//             .populate({path: 'memberId', select: 'username_lower'})
//             .lean()
//             .cursor();
//
//
//         cursor.on('data', function (betObj) {
//
//             const allMatch = betObj.parlay.matches;
//
//             let filterList = _.filter(allMatch, function (response) {
//                 return response.matchId === matchId;
//             });
//
//             if (filterList.length === 0) {
//                 // betListCallback(null, 'not data found');
//                 return;
//             }
//
//
//             let unCheckMatch = _.filter(allMatch, function (response) {
//                 return response.matchId !== matchId && !response.betResult;
//             });
//
//             console.log('unCheckMatch : ', unCheckMatch.length);
//
//             const match = filterList[0];
//
//             let betResult = 'DRAW';
//
//             let updateBody;
//             let isCalculateReceive = false;
//             let isRefundBalance = false;
//             let winLoseAmount = 0;
//             let shareReceive;
//
//             let isUnCheckMatchLose = _.filter(allMatch, function (response) {
//                 return response.matchId !== matchId && response.betResult === 'LOSE';
//             }).length > 0;
//
//             if (isUnCheckMatchLose) {
//                 //มีคู่อื่นที่แพ้
//                 console.log('-----------have match lose--------------')
//                 isRefundBalance = true;
//                 updateBody = {
//                     $set: {
//                         'parlay.matches.$.fullScore': '',
//                         'parlay.matches.$.betResult': '',
//                         'validAmount': betObj.amount,
//                         'betResult': '',
//                         'isEndScore': false,
//                         'status': 'DONE',
//                         'settleDate': DateUtils.getCurrentDate()
//                     }
//                 };
//             } else {
//                 // NOT EQUALS LOSE
//
//                 if (unCheckMatch.length == 0 || betObj.betResult === 'LOSE') {
//                     console.log('-----------all match is end--------------')
//                     //คู่อื่นจบหมดแล้ว
//                     console.log('no unCheck match');
//                     isRefundBalance = true;
//                     isCalculateReceive = true;
//
//                     let totalUnCheckOdds = _.filter(allMatch, function (response) {
//                         return response.matchId !== matchId && response.betResult;
//                     }).map((item) => {
//                         return calculateParlayOdds(item.betResult, item.odd);
//                     });
//
//                     let sumTotalUnCheckOdds = _.reduce(totalUnCheckOdds, (memo, item) => {
//                         return {sum: memo.sum * item};
//                     }, {sum: 1}).sum;
//
//
//                     let halfLoseCount = _.filter(allMatch, function (response) {
//                         return response.betResult === 'HALF_LOSE' && response.matchId !== matchId;
//                     }).length;
//
//                     //prepare with last match result
//                     sumTotalUnCheckOdds = roundTo.down(sumTotalUnCheckOdds * calculateParlayOdds(betResult, match.odd), 3);
//
//                     if (betResult === 'HALF_LOSE') {
//                         halfLoseCount += 1;
//                     }
//
//                     console.log('sumTotalUnCheckOdds : ', sumTotalUnCheckOdds);
//                     console.log('halfLoseCount : ', halfLoseCount);
//
//                     winLoseAmount = (subPlyAmount(betObj.amount, halfLoseCount) * sumTotalUnCheckOdds);
//                     let validAmount = subPlyAmount(betObj.amount, halfLoseCount);
//
//                     winLoseAmount = winLoseAmount - betObj.amount;
//
//                     shareReceive = prepareShareReceive(winLoseAmount, 'DRAW', betObj);
//                     updateBody = {
//                         $set: {
//                             'parlay.matches.$.fullScore': '',
//                             'parlay.matches.$.betResult': '',
//                             'commission.company.winLoseCom': shareReceive.company.winLoseCom,
//                             'commission.company.winLose': shareReceive.company.winLose,
//                             'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,
//
//                             'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
//                             'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
//                             'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,
//
//                             'commission.api.winLoseCom': shareReceive.api.winLoseCom,
//                             'commission.api.winLose': shareReceive.api.winLose,
//                             'commission.api.totalWinLoseCom': shareReceive.api.totalWinLoseCom,
//
//                             'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
//                             'commission.senior.winLose': shareReceive.senior.winLose,
//                             'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,
//
//                             'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
//                             'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
//                             'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,
//
//                             'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
//                             'commission.agent.winLose': shareReceive.agent.winLose,
//                             'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,
//
//                             'commission.member.winLoseCom': shareReceive.member.winLoseCom,
//                             'commission.member.winLose': shareReceive.member.winLose,
//                             'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
//                             'validAmount': validAmount,
//                             'betResult': '',
//                             'isEndScore': false,
//                             'status': 'RUNNING',
//                             'settleDate': ''
//                         }
//                     };
//                     updateBody.$set.status = 'RUNNING';
//
//                     // console.log('update body == ',updateBody);
//
//                 } else {
//                     console.log('-----------มีคู่ที่ยังไม่จบ--------------')
//                     updateBody = {
//                         $set: {
//                             'parlay.matches.$.fullScore': '',
//                             'parlay.matches.$.betResult': '',
//                             'validAmount': betObj.amount,
//                             'betResult': '',
//                             'isEndScore': false,
//                             'status': 'RUNNING',
//                             'settleDate': ''
//
//                             // 'betResult': betResult,
//                         }
//                     };
//                     // console.log('have unCheck match : ', unCheckMatch);
//                     // console.log(unCheckMatch);
//
//                 }
//             }
//
//             console.log('----isRefundBalance-----',isRefundBalance);
//             async.series([callback => {
//                 //refundBalance
//                 if (isRefundBalance) {
//                     AgentService.rollbackCalculateAgentMemberBalance(betObj, reff, (err, response) => {
//                         if (err) {
//                             callback(err, null);
//                         } else {
//                             callback(null, response);
//                         }
//                     });
//                 } else {
//                     callback(null, '');
//                 }
//
//             }, callback => {
//
//                 BetTransactionModel.findOneAndUpdate({
//                     _id: betObj._id,
//                     'parlay.matches._id': match._id
//                 }, updateBody, {new: true}, function (err, response) {
//                     if (err) {
//                         callback(err, null);
//                     } else {
//                         callback(null, '');
//                     }
//                 });
//
//             }], function (err, asyncResponse) {
//                 if (err) {
//
//                 } else {
//                     console.log('success 1')
//                 }
//             });
//
//
//
//         });
//
//         cursor.on('end', function () {
//             console.log('Done!');
//             callback(null, {});
//         });
//
//     })], (err, asyncResponse) => {
//
//         if (err){
//             rollbackCallback(err, null);
//         }else {
//             rollbackCallback(null, 'success');
//         }
//
//
//
//
//     });
// }

function subPlyAmount(amount, count) {
    if (count > 0) {
        return subPlyAmount(amount / 2, --count);
    } else {
        return amount;
    }

}

function calculateParlayOdds(betResult, odd) {
    if (betResult === 'WIN') {
        return odd;
    } else if (betResult === 'DRAW') {
        return 1;
    }
    return odd;
}

function convertPercent(percent) {
    return (percent || 0) / 100
}

function updateAgentMemberBalance(shareReceive, gameType, key, updateCallback) {

    let memberReceiveAmount;

    console.log(shareReceive.memberUsername,'====memberCredit : ' + shareReceive.memberCredit + '  , betAmount : ' + shareReceive.betAmount + ' , betResult : ' + shareReceive.betResult);

    if (gameType === 'PARLAY' || gameType === 'MIX_STEP') {
        if (shareReceive.betResult === 'WIN') {
            memberReceiveAmount = Math.abs(shareReceive.memberCredit) + shareReceive.member.totalWinLoseCom;
        } else {
            console.log('memberReceiveAmount ::: ' + shareReceive.betAmount + ' : ' + shareReceive.member.totalWinLoseCom)
            memberReceiveAmount = shareReceive.betAmount + shareReceive.member.totalWinLoseCom;
        }
    } else {

        console.log('memberReceiveAmount ::: ' + Math.abs(shareReceive.memberCredit) + ' : ' + shareReceive.member.totalWinLoseCom)
        memberReceiveAmount = roundTo(Math.abs(shareReceive.memberCredit) + shareReceive.member.totalWinLoseCom, 2);
    }

    MemberService.updateBalance2(shareReceive.memberUsername.toLowerCase(), memberReceiveAmount, shareReceive.betId, 'SPORTS_BOOK_END_SCORE', key, function (err, updateMemberResponse) {
        if (err) {
            updateCallback(err, null);
        } else {
            updateCallback(null, updateMemberResponse);
        }
    });
}

function prepareShareReceive(winLose, betResult, betTransaction) {

    let winLoseInfo;
    const amount = betTransaction.amount;

    if (betResult !== 'DRAW') {

        let memberCommission = validateCommission(betResult, betTransaction.commission.member.commission);

        let superAdminShare = betTransaction.commission.superAdmin.shareReceive;

        let companyShare = betTransaction.commission.company.shareReceive;
        let companyCommission = validateCommission(betResult, betTransaction.commission.company.commission || 0);

        let shareHolderShare = betTransaction.commission.shareHolder.shareReceive;
        let shareHolderCommission = validateCommission(betResult, betTransaction.commission.shareHolder.commission || 0);

        let apiShare = betTransaction.commission.api.shareReceive;
        let apiCommission = validateCommission(betResult, betTransaction.commission.api.commission || 0);

        let seniorShare = betTransaction.commission.senior.shareReceive || 0;
        let seniorCommission = validateCommission(betResult, betTransaction.commission.senior.commission || 0);

        let masterAgentShare = betTransaction.commission.masterAgent.shareReceive || 0;
        let masterAgentCommission = validateCommission(betResult, betTransaction.commission.masterAgent.commission || 0);

        let agentShare = betTransaction.commission.agent.shareReceive || 0;
        let agentCommission = validateCommission(betResult, betTransaction.commission.agent.commission || 0);

        function validateCommission(betResult, commission) {
            return betResult === 'HALF_LOSE' || betResult === 'HALF_WIN' ? (commission / 2) : commission;
        }


        //SUPER ADMIN
        let superAdminTotalUpperShare = 0;
        let superAdminOwnAndChildShare = companyShare + shareHolderShare + seniorShare + masterAgentShare + agentShare;

        let superAdminReceiveCommission = 0;
        let superAdminGiveCommission = companyCommission;

        // console.log('TOTAL W/L : ', winLose);
        let superAdminWinLose = roundTo(0 - (winLose * convertPercent(betTransaction.commission.superAdmin.shareReceive)), 3);
        let superAdminFinalCom = calculateCommission(amount,
            superAdminShare,
            superAdminTotalUpperShare,
            superAdminReceiveCommission,
            superAdminOwnAndChildShare,
            superAdminGiveCommission);

        // console.log('superAdmin share : ', betTransaction.commission.superAdmin.shareReceive, ' %');
        // console.log('superAdmin com % : ', betTransaction.commission.superAdmin.commission, ' %');
        // console.log('superAdmin W/L : ', superAdminWinLose);
        // console.log('superAdmin COM : ', superAdminFinalCom);
        // console.log('superAdmin W/L + COM : ', superAdminWinLose + (superAdminFinalCom));
        // console.log('');
        //
        // console.log("==================================");

        //COMPANY
        let companyTotalUpperShare = superAdminShare;
        let companyOwnAndChildShare = shareHolderShare + seniorShare + masterAgentShare + agentShare;

        let companyReceiveCommission = 0;
        let companyGiveCommission = shareHolderCommission;

        // console.log('TOTAL W/L : ', winLose);
        let companyWinLose = roundTo(0 - (winLose * convertPercent(betTransaction.commission.company.shareReceive)), 3);
        let companyFinalCom = calculateCommission(amount,
            companyShare,
            companyTotalUpperShare,
            companyReceiveCommission,
            companyOwnAndChildShare,
            companyGiveCommission);

        // console.log('company share : ', betTransaction.commission.company.shareReceive, ' %');
        // console.log('company com % : ', betTransaction.commission.company.commission, ' %');
        // console.log('company W/L : ', companyWinLose);
        // console.log('company COM : ', companyFinalCom);
        // console.log('company W/L + COM : ', companyWinLose + (companyFinalCom));
        // console.log('');
        //
        // console.log("==================================");
        //SHARE HOLDER
        let shareHolderTotalUpperShare = superAdminShare + companyShare;
        let shareHolderOwnAndChildShare = seniorShare + masterAgentShare + agentShare;
        let shareHolderReceiveCommission = shareHolderCommission;
        let shareHolderGiveCommission = seniorCommission;

        let shareHolderWinLose = roundTo(0 - (winLose * convertPercent(betTransaction.commission.shareHolder.shareReceive)), 3);
        let shareHolderFinalCom = calculateCommission(amount,
            shareHolderShare,
            shareHolderTotalUpperShare,
            shareHolderReceiveCommission,
            shareHolderOwnAndChildShare,
            shareHolderGiveCommission);

        // console.log('shareHolder share : ', betTransaction.commission.shareHolder.shareReceive, ' %');
        // console.log('shareHolder com % : ', betTransaction.commission.shareHolder.commission, ' %');
        // console.log('shareHolder W/L : ', shareHolderWinLose);
        // console.log('shareHolder COM : ', shareHolderFinalCom);
        // // console.log('shareHolder COM LOSE : ', calculateStep3(1000, shareHolderShare, shareHolderGiveCommission));
        // console.log('shareHolder W/L + COM : ', shareHolderWinLose + (shareHolderFinalCom));
        // console.log('');
        //
        // console.log("==================================");
        //API
        let apiTotalUpperShare = superAdminShare + companyShare;
        let apiOwnAndChildShare = 100 - (superAdminShare + companyShare);
        let apiReceiveCommission = apiCommission;
        let apiGiveCommission = memberCommission;

        let apiWinLose = roundTo(0 - (winLose * convertPercent(betTransaction.commission.api.shareReceive)), 3);
        let apiFinalCom = calculateCommission(amount,
            apiShare,
            apiTotalUpperShare,
            apiReceiveCommission,
            apiOwnAndChildShare,
            apiGiveCommission);

        // console.log('api share : ', betTransaction.commission.api.shareReceive, ' %');
        // console.log('api com % : ', betTransaction.commission.api.commission, ' %');
        // console.log('api W/L : ', apiWinLose);
        // console.log('api COM : ', apiFinalCom);
        // // console.log('api COM LOSE : ', calculateStep3(1000, apiShare, apiGiveCommission));
        // console.log('api W/L + COM : ', apiWinLose + (apiFinalCom));
        // console.log('');
        //
        // console.log("==================================")
        //SENIOR
        let seniorTotalUpperShare = superAdminShare + companyShare + shareHolderShare;
        let seniorOwnAndChildShare = masterAgentShare + agentShare;
        let seniorReceiveCommission = seniorCommission;
        let seniorGiveCommission = betTransaction.memberParentGroup.type === 'SENIOR' ? memberCommission :
            betTransaction.commission.masterAgent.group ? masterAgentCommission : agentCommission;

        let seniorWinLose = roundTo(0 - (winLose * convertPercent(betTransaction.commission.senior.shareReceive)), 3);
        let seniorFinalCom = calculateCommission(amount,
            seniorShare,
            seniorTotalUpperShare,
            seniorReceiveCommission,
            seniorOwnAndChildShare,
            seniorGiveCommission);

        // console.log('xxxx :: ', seniorFinalCom)
        // console.log('senior share : ', betTransaction.commission.senior.shareReceive, ' %');
        // console.log('senior com % : ', betTransaction.commission.senior.commission, ' %');
        // console.log('senior W/L : ', seniorWinLose);
        // console.log('senior COM : ', seniorFinalCom);
        // // console.log('senior COM LOSE : ', calculateStep3(1000, seniorShare, seniorGiveCommission));
        // console.log('senior W/L + COM : ', seniorWinLose + seniorFinalCom);
        // console.log('');
        //
        // console.log("==================================");

        //MASTER AGENT
        let masterAgentTotalUpperShare = superAdminShare + companyShare + shareHolderShare + seniorShare;
        let masterAgentOwnAndChildShare = agentShare;
        let masterAgentReceiveCommission = masterAgentCommission;
        let masterAgentGiveCommission = betTransaction.memberParentGroup.type === 'MASTER_AGENT' ? memberCommission :
            betTransaction.commission.masterAgent.group ? agentCommission : 0;

        let masterAgentWinLose = roundTo(0 - (winLose * convertPercent(betTransaction.commission.masterAgent.shareReceive)), 3);
        let masterAgentFinalCom = calculateCommission(amount,
            masterAgentShare,
            masterAgentTotalUpperShare,
            masterAgentReceiveCommission,
            masterAgentOwnAndChildShare,
            masterAgentGiveCommission);

        // console.log('masterAgent share : ', betTransaction.commission.masterAgent.shareReceive, ' %');
        // console.log('masterAgent com % : ', betTransaction.commission.masterAgent.commission, ' %');
        // console.log('masterAgent W/L : ', masterAgentWinLose);
        // console.log('masterAgent COM : ', masterAgentFinalCom);
        // // console.log('masterAgent COM LOSE : ', calculateStep3(1000, masterAgentShare, masterAgentGiveCommission));
        // console.log('masterAgent W/L + COM : ', masterAgentWinLose + masterAgentFinalCom);
        // console.log('');
        //
        // console.log("==================================");
        //AGENT
        let agentTotalUpperShare = superAdminShare + companyShare + shareHolderShare + seniorShare + masterAgentShare;

        let agentOwnAndChildShare = 100 - (superAdminShare + companyShare + shareHolderShare + seniorShare + masterAgentShare + agentShare);
        let agentReceiveCommission = agentCommission;
        let agentGiveCommission = betTransaction.memberParentGroup.type === 'AGENT' ? memberCommission : 0;
        let agentWinLose = roundTo(0 - (winLose * convertPercent(betTransaction.commission.agent.shareReceive)), 3);
        let agentFinalCom = calculateCommission(amount,
            agentShare,
            agentTotalUpperShare,
            agentReceiveCommission,
            agentOwnAndChildShare,
            agentGiveCommission);

        // console.log('agent share : ', betTransaction.commission.agent.shareReceive, ' %');
        // console.log('agent com % : ', betTransaction.commission.agent.commission, ' %');
        // console.log('agent W/L : ', agentWinLose);
        // console.log('agent COM : ', agentFinalCom);
        // // console.log('agent COM LOSE : ', calculateStep3(1000, agentShare, agentGiveCommission));
        // console.log('agent W/L + COM : ', agentWinLose + agentFinalCom);
        // console.log('');

        // console.log("==================================");
        // console.log('member W/L : ', winLose);
        // console.log('member COM : ', Math.abs(winLose * convertPercent(memberCommission)));
        // console.log('member W/L + COM : ', winLose + Math.abs(winLose * convertPercent(betTransaction.commission.member.commission)));


        function calculateCommission(betPrice, ownShare, totalUpperShare, receiveCommission, ownAndChildShare, giveCommission) {

            let step1 = roundTo(Number.parseFloat(betPrice * (convertPercent(totalUpperShare)) * (receiveCommission / 100)), 3);
            // console.log('step1 : ' + betPrice + ' x ' + totalUpperShare + ' x ' + (receiveCommission / 100) + ' = ' + step1);


            let step2 = roundTo(Number.parseFloat(betPrice * (convertPercent(totalUpperShare)) * (giveCommission / 100)), 3);
            // console.log('step2 : ' + betPrice + ' x ' + totalUpperShare + ' x ' + (giveCommission / 100) + ' = ' + step2);

            // let step3 = betPrice * convertPercent(ownShare) * convertPercent(giveCommission);
            let step3 = roundTo(calculateStep3(betPrice, ownShare, giveCommission), 3);
            // console.log('step3 : ' + betPrice + ' x ' + convertPercent(ownShare) + ' x ' + convertPercent(giveCommission) + ' = ' + step3);
            //
            // console.log('step3 : ' + step3);
            // console.log('step1 : ' + step1 + ' , step2 : ' + step2 + ' , step3 : ' + step3)

            let result12 = roundTo((step1 - step2), 3);
            let finalResult = roundTo((result12 - step3), 3);
            // console.log('result :: ', finalResult)

            return finalResult;
        }

        function calculateStep3(betPrice, ownShare, giveCommission) {
            return betPrice * convertPercent(ownShare) * convertPercent(giveCommission);
        }


        let memberWL = roundTo(winLose, 3);
        let memberWLCom = roundTo(Math.abs(amount * convertPercent(memberCommission)), 3);

        winLoseInfo = {
            betId: betTransaction.betId,
            betAmount: amount,
            memberCredit: betTransaction.memberCredit,
            memberUsername: betTransaction.memberUsername,
            betResult: betResult,
            superAdmin: {
                group: betTransaction.commission.superAdmin.group,
                winLoseCom: roundTo(superAdminFinalCom, 3),
                winLose: roundTo(superAdminWinLose, 3),
                totalWinLoseCom: roundTo((superAdminWinLose + superAdminFinalCom), 3)
            },
            company: {
                group: betTransaction.commission.company.group,
                winLoseCom: roundTo(companyFinalCom, 3),
                winLose: roundTo(companyWinLose, 3),
                totalWinLoseCom: roundTo((companyWinLose + companyFinalCom), 3)
            },
            shareHolder: {
                group: betTransaction.commission.shareHolder.group,
                winLoseCom: roundTo(shareHolderFinalCom, 3),
                winLose: roundTo(shareHolderWinLose, 3),
                totalWinLoseCom: roundTo((shareHolderWinLose + shareHolderFinalCom), 3)
            },
            api: {
                group: betTransaction.commission.api.group,
                winLoseCom: roundTo(apiFinalCom, 3),
                winLose: roundTo(apiWinLose, 3),
                totalWinLoseCom: roundTo((apiWinLose + apiFinalCom), 3)
            },
            senior: {
                group: betTransaction.commission.senior.group,
                winLoseCom: roundTo(seniorFinalCom, 3),
                winLose: roundTo(seniorWinLose, 3),
                totalWinLoseCom: roundTo((seniorWinLose + seniorFinalCom), 3)
            },
            masterAgent: {
                group: betTransaction.commission.masterAgent.group,
                winLoseCom: roundTo(masterAgentFinalCom, 3),
                winLose: roundTo(masterAgentWinLose, 3),
                totalWinLoseCom: roundTo((masterAgentWinLose + masterAgentFinalCom), 3)
            },
            agent: {
                group: betTransaction.commission.agent.group,
                winLoseCom: roundTo(agentFinalCom, 3),
                winLose: roundTo(agentWinLose, 3),
                totalWinLoseCom: roundTo((agentWinLose + agentFinalCom), 3)
            },
            member: {
                id: betTransaction.memberId,
                winLoseCom: memberWLCom,
                winLose: memberWL,
                totalWinLoseCom: roundTo((memberWL + memberWLCom), 3)
            }
        };

    } else {
        winLoseInfo = {
            betId: betTransaction.betId,
            betAmount: amount,
            memberCredit: betTransaction.memberCredit,
            memberUsername: betTransaction.memberUsername,
            betResult: betResult,
            superAdmin: {
                winLoseCom: 0,
                winLose: 0,
                totalWinLoseCom: 0
            },
            company: {
                winLoseCom: 0,
                winLose: 0,
                totalWinLoseCom: 0
            },
            shareHolder: {
                winLoseCom: 0,
                winLose: 0,
                totalWinLoseCom: 0
            },
            api: {
                winLoseCom: 0,
                winLose: 0,
                totalWinLoseCom: 0
            },
            senior: {
                winLoseCom: 0,
                winLose: 0,
                totalWinLoseCom: 0
            },
            masterAgent: {
                winLoseCom: 0,
                winLose: 0,
                totalWinLoseCom: 0
            },
            agent: {
                winLoseCom: 0,
                winLose: 0,
                totalWinLoseCom: 0
            },
            member: {
                id: betTransaction.memberId,
                winLoseCom: 0,
                winLose: 0,
                totalWinLoseCom: 0
            }
        };
    }
    return winLoseInfo;

}


module.exports = router;