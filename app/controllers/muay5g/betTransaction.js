const express   = require('express');
const router    = express.Router();
const async     = require("async");
const Joi       = require('joi');
const mongoose  = require('mongoose');
const _         = require('underscore');
const roundTo   = require('round-to');
const request = require('request');
const moment = require('moment');

const MemberModel                = require('../../models/member.model');
const BetTransactionModel        = require('../../models/betTransaction.model.js');
const AgentGroupModel            = require('../../models/agentGroup.model.js');
const Commons                    = require('../../common/commons');
const _Utils                     = require('../../common/underscoreUtils');
const FormulaUtils               = require('../../common/formula');
const MemberService              = require('../../common/memberService');
const AgentService               = require("../../common/agentService");
const DateUtils                  = require('../../common/dateUtils');
const ApiService                 = require('../../common/apiService');
const RedisService               = require('../../common/redisService');
const Utility                       = require('../../common/utlity');
const Muaystep2Service              = require('../../common/muaystep2Service');
const CLIENT_NAME                = process.env.CLIENT_NAME || 'SPORTBOOK88';

const HOST = process.env.MUAY_5G_HOST || 'https://api.aspbet.com/';
const TOKEN = process.env.MUAY_5G_TOKEN || '4b2b6303956277ddfd7b8ea9cebb31db';

const headers = {
    'Content-Type': 'application/json'
};

function generateBetId() {
    return 'BET_M5' + new Date().getTime();
}

function calculateParlayPayout(amount, odd, callback) {
    let maxPayout = 2000000.1;
    let max = roundTo(maxPayout / odd, 2);
    if (amount > max) {
        callback(5012, null);
    } else {
        callback(null, '');
        // callback((amount * odd).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]);
    }
}

const addBetSchema = Joi.object().keys({
    memberId: Joi.string().required(),
    odd: Joi.number().required(),
    oddType: Joi.string().required(), //ah, ou
    matchType: Joi.string().required(),//live, non_live
    acceptAnyOdd: Joi.boolean().required(),
    round: Joi.string().required(),
    priceType: Joi.string().required(),//MY, HK
    amount: Joi.number().required(),
    payout: Joi.number().required(),
    catType: Joi.string().required(),
    matchId: Joi.string().required(),
    bet: Joi.string().required(),//HOME, AWAY
    isHT: Joi.string().required(),//0 ไม่พักยก , 1 พักยก
    leagueId : Joi.string().required(),
    leagueName : Joi.string().required(),
    homeName : Joi.string().required(),
    awayName : Joi.string().required(),
});


router.post('/hdp', function (req, res) {

    let validate = Joi.validate(req.body, addBetSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    const source         = 'M2_MUAY';
    const game         = 'M2';
    const userInfo       = req.userInfo;
    const membercurrency = userInfo.currency;
    // const matchType = 'NONE_LIVE';
    let isOfflineCash = false;
    let matchListAll = [];
    // console.log('-----userInfo-----', userInfo);
    async.series([
            (callback) => {
                MemberService.getOddAdjustment(userInfo.username, (err, result) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, result);
                    }
                });
            },
            (callback) => {
                let isLive = req.body.matchType === 'LIVE' ? 1 : 0;

                getAllMatch(req.body.matchId, isLive, function (err, result) {
                    if(err){
                        callback(err, null);
                    }else {
                        let match = _.filter(result, data => {
                            return data.m_id == req.body.matchId
                        });

                        if(match[0].b === '1'){
                            return res.send({
                                code: 99,
                                message: 'โปรดรอสักครู่ กำลังปรับราคา'
                            })
                        }else{
                            matchListAll = result;
                            callback(null, result);
                        }
                    }
                });
            },

            (callback) => {
                MemberService.getLimitSetting(userInfo._id, (err, result) => {
                    if (err) {
                        callback(error, null);
                    } else {
                        callback(null, result.limitSetting);
                    }
                });
            },
            (callback) => {
                if (_.isEqual(CLIENT_NAME, 'AMB_SPORTBOOK')) {
                    AgentService.getOfflineCashStatus(userInfo.username, (error, isOfflineCash) => {
                        if (error) {
                            callback(error, false);
                        } else {
                            callback(null, isOfflineCash);
                        }
                    });
                } else {
                    callback(null, false);
                }
            }
        ],
        (error, resultOddAndLimit) => {
            if (error) {
                return res.send({
                    code: 9999,
                    message: error
                });
            } else {

                const [
                    oddAdjust,
                    limitSetting,
                    currency,
                    isOfflineCashResult
                ] = resultOddAndLimit;
                isOfflineCash = isOfflineCashResult;
                async.series([callback => {

                    //TODO: duplicate find member
                    MemberModel.findById(userInfo._id, (err, response) => {
                        if (err)
                            callback(err, null);
                        // console.log('response===', response);
                        if (response.suspend) {
                            callback(5011, null);
                        } else {
                            AgentService.getUpLineStatus(userInfo.group._id, (err, status) => {
                                if (err) {
                                    callback(err, null);
                                } else {
                                    if (status.suspend) {
                                        callback(5011, null);
                                    } else {
                                        callback(null, status);
                                    }
                                }
                            });
                        }
                    });


                }, callback => {

                    if (userInfo.group.type === 'API') {
                        callback(null, 0);
                    } else {
                        MemberService.getCredit(userInfo._id, (err, credit) => {
                            if (err) {
                                callback(err, null);
                            } else {
                                if (credit < req.body.amount) {
                                    callback(5005, null);
                                } else {
                                    callback(null, credit);
                                }
                            }
                        });
                    }

                }, callback => {

                    FormulaUtils.calculatePayout(req.body.amount, req.body.odd, req.body.oddType, req.body.priceType.toUpperCase(), (payout) => {
                        if (req.body.payout != payout) {
                            callback(5004, null);
                        } else {
                            callback(null, payout);
                        }
                    });

                }], (err, results) => {

                    if (err) {
                        if (err === 5004) {
                            return res.send({
                                message: "payout not matched.",
                                result: null,
                                code: 5004
                            });
                        }
                        if (err === 5005) {
                            return res.send({
                                message: "credit exceed.",
                                result: null,
                                code: 5005
                            });
                        }
                        if (err === 5011) {
                            return res.send({
                                message: "Account or Upline was suspend.",
                                result: null,
                                code: 5011
                            });
                        }
                        return res.send({message: "error", result: err, code: 999});
                    }

                    async.waterfall([callback => {

                        MemberModel.findById(userInfo._id, (err, response) => {
                            if (err)
                                callback(err, null);

                            if (response) {
                                let maxBet = findMaxBet(response, req.body.oddType);
                                if (req.body.amount > maxBet) {
                                    return res.send({
                                        message: "amount exceeded (per bet)",
                                        result: null,
                                        code: 5001
                                    });
                                } else {
                                    callback(null, response);

                                }
                            }
                        });

                    }, (memberInfo, callback) => {

                        AgentGroupModel.findById(userInfo.group._id).select('_id type parentId shareSetting commissionSetting').populate({
                            path: 'parentId',
                            select: '_id type parentId shareSetting commissionSetting name',
                            populate: {
                                path: 'parentId',
                                select: '_id type parentId shareSetting commissionSetting name',
                                populate: {
                                    path: 'parentId',
                                    select: '_id type parentId shareSetting commissionSetting name',
                                    populate: {
                                        path: 'parentId',
                                        select: '_id type parentId shareSetting commissionSetting name',
                                        populate: {
                                            path: 'parentId',
                                            select: '_id type parentId shareSetting commissionSetting name',
                                            populate: {
                                                path: 'parentId',
                                                select: '_id type parentId shareSetting commissionSetting name',
                                            }
                                        }
                                    }
                                }
                            }

                        }).exec(function (err, response) {
                            if (err)
                                callback(err, null);
                            else
                                callback(null, memberInfo, response);
                        });

                    }, (memberInfo, agentGroup, callback) => {


                        const oddType = req.body.oddType.toUpperCase();

                        if (oddType === 'AH' ||
                            oddType === 'AH1ST' ||
                            oddType === 'OU' ||
                            oddType === 'OU1ST' ||
                            oddType === 'OE' ||
                            oddType === 'OE1ST') {

                            let condition = {
                                memberId: mongoose.Types.ObjectId(req.body.memberId),
                                'hdp.matchId': `${req.body.leagueId}:${req.body.matchId}`
                            };
                            condition['$or'] = [
                                {
                                    "status": "RUNNING"
                                },
                                {
                                    "status": "WAITING"
                                },
                                {
                                    "status": "DONE"
                                }
                            ];
                            BetTransactionModel.aggregate([
                                {
                                    $match: condition
                                },
                                {
                                    $project: {_id: 0}
                                },
                                {
                                    "$group": {
                                        "_id": {
                                            id: '$_id'
                                        },
                                        "amount": {$sum: '$amount'}
                                    }
                                }
                            ], function (err, results) {

                                if (err) {
                                    callback(err, '');
                                    return;
                                }

                                let totalBet = results[0] ? results[0].amount : 0;

                                if ((totalBet + Number.parseFloat(req.body.amount)) > convertMinMaxByCurrency(memberInfo.limitSetting.other.m2.maxBetPerMatchHDP, currency, membercurrency)) {
                                    return res.send({
                                        message: "Your total bet has exceeded the maximum bet per match.",
                                        result: null,
                                        code: 5002
                                    });
                                } else {
                                    callback(null, memberInfo, agentGroup, totalBet);
                                }
                            });
                        } else {
                            callback(null, memberInfo, agentGroup,  0);
                        }


                    }, (memberInfo, agentGroup, totalBet, callback) => {
                        let body = {
                            commission: {},
                            hdp: {
                                oddType: req.body.oddType,
                                matchType: req.body.matchType
                            },
                            gameType: 'TODAY',
                            amount: req.body.amount
                        };

                        prepareCommission(memberInfo, body, agentGroup, null, 0, 0);

                        //Bet to Muay Step2
                        let obj = {
                            "token":TOKEN,
                            "user_id":userInfo._id,
                            "usercode":userInfo.username,
                            "m_id":req.body.matchId,
                            "cat_id":req.body.catType,
                            "hdp":req.body.oddType === 'AH' ? '0' : '4.5',
                            "is_live":req.body.matchType === 'LIVE' ? 1 : 0,
                            "is_ht": req.body.isHT,
                            "lst": req.body.round,//round
                            "price":req.body.odd,
                            "mode": req.body.oddType === 'AH' ? 'hdp_ft' : 'ou_ht',
                            "choose":req.body.bet.toLowerCase(),
                            "income":req.body.amount,
                            "percent": body.commission.company.shareReceive ? body.commission.company.shareReceive : 0
                        };

                        // console.log('obj = ',obj);
                        request.post({
                            url: `${HOST}apiservice/amb/api/bet`,
                            body: obj,
                            rejectUnauthorized: false,
                            json:true
                        }, function(error, response, body){
                            console.log('response =', body);
                            if (error) {
                                return res.send(error);
                            } else {
                                // console.log('response = ', JSON.parse(body));

                                if(body.code === 0){
                                    callback(null, memberInfo, agentGroup, body);
                                }else if(body.code === 300010){
                                    return res.send({
                                        message: "Odd change.",
                                        result: body.res,
                                        code: 5003
                                    });

                                }else {
                                    return res.send({
                                        message: "Bet Fail",
                                        result: body,
                                        code: 888
                                    });
                                }

                            }
                        });

                }, (memberInfo, agentGroup, betResult, callback) => {
                    if(matchListAll && matchListAll.length > 0){
                        let match = _.filter(matchListAll, data => {
                            return data.m_id == req.body.matchId
                        });

                        console.log(match[0].fight_dt,'=======match000=============================== ',match);

                        callback(null, memberInfo, agentGroup, betResult, match[0].fight_dt);
                    }else {
                        let isLive = req.body.matchType === 'LIVE' ? 1 : 0;

                        getAllMatch(req.body.matchId, isLive, function (err, result) {
                            if(err){
                                callback(null, memberInfo, agentGroup, betResult, DateUtils.getCurrentDate());
                            }else {
                                let match = _.filter(result, data => {
                                    return data.m_id == req.body.matchId
                                });

                                callback(null, memberInfo, agentGroup, betResult, match[0].fight_dt);
                            }
                        });
                    }


                }], (err, memberInfo, agentGroups, betResult, matchDate) => {

                        if (err) {
                            console.log(err);
                            if (err === 5001) {
                                return res.send({
                                    message: "amount exceeded (per bet)",
                                    result: null,
                                    code: 5001
                                });
                            }
                            else if (err === 5002) {
                                return res.send({
                                    message: "Your total bet has exceeded the maximum bet per match.",
                                    result: null,
                                    code: 5002
                                });
                            }
                            else if (err === 98) {
                                return res.send({
                                    message: "Fail To Betting.",
                                    result: null,
                                    code: 98
                                });
                            }


                            else if (err === 11) {
                                return res.send({
                                    message: "Match is close.",
                                    result: null,
                                    code: 5006
                                });
                            }
                            else if (err === 13) {
                                return res.send({
                                    message: "odd was already changed.",
                                    result: null,
                                    code: 5003
                                });
                            }
                            else if (err === 23) {
                                return res.send({
                                    message: "Your total bet has exceeded the maximum bet per match.",
                                    result: null,
                                    code: 5002
                                });
                            }
                            else if (err === 24) {
                                return res.send({
                                    message: "Your total bet has exceeded the maximum bet per match.",
                                    result: null,
                                    code: 5002
                                });
                            }
                            else if (err === 999) {
                                return res.send({
                                    message: "Under Maintenance.",
                                    result: null,
                                    code: 999
                                });
                            }
                            else if (err === 4101) {
                                return res.send({
                                    message: "Invalid Agent.",
                                    result: null,
                                    code: 4101
                                });
                            }

                            return res.send({message: "error", result: err, code: 999});
                        }


                        let memberCredit = roundTo(req.body.odd < 0 ? req.body.amount * req.body.odd : (req.body.amount * -1), 2);
                        let processDate = DateUtils.getCurrentDate();
                        console.log(matchDate, '==================bet result ==== ', DateUtils.convertThaiDate(moment(matchDate).toDate()));
                        let body = {
                            betId: generateBetId(),
                            memberId: req.body.memberId,
                            memberParentGroup: memberInfo.group,
                            memberUsername: memberInfo.username_lower,
                            hdp: {
                                leagueId: req.body.leagueId,
                                matchId: `${req.body.leagueId}:${req.body.matchId}`,
                                leagueName: req.body.leagueName,
                                leagueNameN: {
                                    en : req.body.leagueName,
                                    th : req.body.leagueName,
                                    cn : req.body.leagueName,
                                    tw : req.body.leagueName
                                },
                                matchName: {
                                    en : {
                                        h : req.body.homeName,
                                        a : req.body.awayName
                                    },
                                    th : {
                                        h : req.body.homeName,
                                        a : req.body.awayName
                                    },
                                    cn : {
                                        h : req.body.homeName,
                                        a : req.body.awayName
                                    },
                                    tw : {
                                        h : req.body.homeName,
                                        a : req.body.awayName
                                    }
                                },
                                matchDate: DateUtils.convertThaiDate(moment(matchDate).toDate()),
                                oddKey: '',
                                odd: req.body.odd,
                                oddType: req.body.oddType,
                                handicap: req.body.oddType === 'AH' ? '0' : '4.5',
                                bet: req.body.bet,
                                score: req.body.round,
                                matchType: req.body.matchType,
                                rowID: ""//hdpInfo.detail.rowID
                            },
                            amount: req.body.amount,
                            memberCredit: memberCredit,
                            payout: req.body.payout,
                            gameType: 'TODAY',
                            acceptHigherPrice: req.body.acceptAnyOdd,
                            priceType: req.body.priceType.toUpperCase(),
                            game: game,
                            source: source,
                            status: req.body.matchType === 'LIVE' ? 'WAITING' : 'RUNNING',
                            commission: {},
                            gameDate: DateUtils.convertThaiDate(moment(matchDate).toDate()),
                            createdDate: processDate,
                            ipAddress: Utility.getIP(req),
                            currency: memberInfo.currency,
                            isOfflineCash: isOfflineCash,
                            active: !isOfflineCash,
                            liveTimeBet: req.body.round,
                            ref1: betResult.res.bet_id
                        };

                        prepareCommission(memberInfo, body, agentGroups, null, 0, 0);

                        // console.log('BODY ------: ', JSON.stringify(body));

                        async.waterfall([callback => {
                            console.log('============== 1 : SAVE TRANSACTION ============');
                            if (userInfo.group.type === 'API') {

                                body.username = memberInfo.username;
                                body.gameType = 'TODAY';
                                body.sportType = source;

                                AgentGroupModel.findById(memberInfo.group, 'endpoint', function (err, agentResponse) {

                                    ApiService.placeBet(agentResponse.endpoint, body, (err, apiResponse) => {

                                        if (err) {
                                            if (err.code) {
                                                callback(err.code, null);
                                            } else {
                                                callback(999, null);
                                            }
                                        } else {
                                            let model = new BetTransactionModel(body);
                                            model.save(function (err, betResponse) {

                                                if (err) {
                                                    callback(999, null);
                                                } else {
                                                    let betResponse1 = Object.assign({}, betResponse)._doc;

                                                    betResponse1.playerBalance = apiResponse.balance;
                                                    callback(null, betResponse1);
                                                }
                                            });
                                        }

                                    });
                                });
                            } else {

                                // console.log('body : ', JSON.stringify(body));
                                let model = new BetTransactionModel(body);
                                model.save((err, betResponse) => {
                                    if (err) {
                                        console.error(err);
                                        callback(999, null);
                                    } else {

                                        callback(null, betResponse);
                                    }
                                });
                            }

                        }, (betResponse, callback) => {

                            if (userInfo.group.type === 'API') {
                                callback(null, betResponse, {});
                            } else {
                                MemberService.updateBalance2(memberInfo.username_lower, memberCredit, betResponse.betId, 'MUAY_5G_BET', betResponse.betId, function (err, response) {
                                    if (err) {
                                        callback(5008, null);
                                    } else {
                                        callback(null, betResponse, response);
                                    }
                                });
                            }

                        }, (betResponse, balance, callback) => {

                            callback(null, betResponse, balance, 'robotResponse');

                        }], function (err, betResponse, balance, robotResponse) {

                            let hdpInfo = {
                                leagueName: req.body.leagueName,
                                leagueNameN: {
                                    en : req.body.leagueName,
                                    th : req.body.leagueName,
                                    cn : req.body.leagueName,
                                    tw : req.body.leagueName,
                                },
                                matchId: req.body.matchId,
                                matchName: {
                                    en : {
                                        h : req.body.homeName,
                                        a : req.body.awayName
                                    },
                                    th : {
                                        h : req.body.homeName,
                                        a : req.body.awayName
                                    },
                                    cn : {
                                        h : req.body.homeName,
                                        a : req.body.awayName
                                    },
                                    tw : {
                                        h : req.body.homeName,
                                        a : req.body.awayName
                                    }
                                },
                                matchDate: DateUtils.convertThaiDate(moment(req.body.matchDate).toDate()),
                                oddType: req.body.oddType,
                                odd: req.body.odd,
                                handicap: req.body.oddType === 'AH' ? '0' : '4.5',
                                bet: req.body.bet
                            }

                            if (err) {

                                if (err == 5007) {
                                    return res.send({
                                        message: "createOddAdjustment failed.",
                                        result: hdpInfo,
                                        code: 5007
                                    });
                                } else if (err == 5008) {
                                    return res.send({
                                        message: "update credit failed",
                                        result: hdpInfo,
                                        code: 5008
                                    });
                                } else if (err == 1003) {
                                    return res.send({
                                        message: "Insufficient Balance",
                                        code: 1003
                                    });
                                } else {
                                    return res.send({
                                        message: "Internal Server Error",
                                        result: err,
                                        code: 999
                                    });
                                }

                            } else {

                                MemberService.getCredit(userInfo._id, function (err, credit) {
                                    if (err) {
                                        return res.send({
                                            message: "get credit fail",
                                            result: err,
                                            code: 999
                                        });
                                    } else {

                                        return res.send({
                                            code: 0,
                                            message: "success",
                                            result: {
                                                betId: betResponse.betId,
                                                hdpInfo: hdpInfo,
                                                creditLimit: userInfo.group.type === 'API' ? betResponse.playerBalance : credit
                                            }
                                        });
                                    }
                                });
                            }
                        });


                    });
                });
            }
        }
    );
});


router.post('/mixParlay', function (req, res) {

    if (req.body.matches && req.body.matches.length < 1){
        return res.send({
            message: "Match is empty",
            results: '',
            code: 999
        });
    }

    if(!req.body.odds || !req.body.amount){
        return res.send({
            message: "Odds or Amount is null",
            results: '',
            code: 999
        });
    }


    const userInfo = req.userInfo;
    const source         = 'M2_MUAY';
    const game         = 'M2';
    let isOfflineCash = false;
    let oddAdjust;
    const matchStatus = 'RUNNING';

    MemberService.getOddAdjustment(req.userInfo.username, (err, data) => {
        if (err) {
            console.log('Get Odd Adjust error');
        } else {
            oddAdjust = data;
        }
    });


    async.series([callback => {

        MemberModel.findById(userInfo._id, function (err, response) {
            if (err) {
                callback(err, null);
                return;
            }

            if (response.suspend) {
                callback(5011, null);
            } else if(!response.limitSetting.other.m2.isEnable){
                callback(5013,null);
            } else {
                AgentService.getUpLineStatus(userInfo.group._id, function (err, status) {

                    if (err) {
                        callback(err, null);
                    } else {
                        if (status.suspend) {
                            callback(5011, null);
                        } else if(!status.parlayEnable){
                            callback(5013,null);
                        }  else {
                            callback(null, status);
                        }
                    }
                });
            }
        });

    }, callback => {

        if (userInfo.group.type === 'API') {
            callback(null, 0);
        } else {
            MemberService.getCredit(userInfo._id, function (err, credit) {
                if (err) {
                    callback(err, null);
                } else {
                    if (credit < req.body.amount) {
                        callback(5005, null);
                    } else {
                        callback(null, credit);
                    }
                }
            });
        }

    }, callback => {
        callback(null, 0);

    }, (callback) => {
        if (_.isEqual(CLIENT_NAME, 'AMB_SPORTBOOK') ||
            _.isEqual(CLIENT_NAME, 'IRON') ||
            _.isEqual(CLIENT_NAME, 'FASTBET98') ||
            _.isEqual(CLIENT_NAME, 'FAST98')||
            _.isEqual(CLIENT_NAME, 'AFC1688') ||
            _.isEqual(CLIENT_NAME, 'SPORTBOOK88') ||
            _.isEqual(CLIENT_NAME, 'CSR') ||
            _.isEqual(CLIENT_NAME, 'MGM')) {
            AgentService.getOfflineCashStatus(userInfo.username, (error, isOfflineCashResult) => {
                if (error) {
                    callback(error, false);
                } else {
                    callback(null, isOfflineCashResult);
                    isOfflineCash = isOfflineCashResult;
                }
            });
        } else {
            callback(null, false);
        }
    }], (err, results) =>{

        if (err) {
            if (err === 5004) {
                return res.send({message: "Payout not matched.", result: null, code: 5004});

            } else if (err === 5005) {
                return res.send({message: "Credit exceed.", result: null, code: 5005});

            } else if (err === 5011) {
                return res.send({
                    message: "Account or Upline was suspend.",
                    result: null,
                    code: 5011
                });
            }
            else if (err === 5013) {
                return res.send({message: "Service Lock.", result: null, code: 5013});
            }
            return res.send({message: "error", result: err, code: 999});
        }

        async.waterfall([callback => {

            MemberModel.findById(userInfo._id, function (err, response) {
                if (err) {
                    callback(err, null);
                } else {

                    let matchCount = req.body.matches.length;
                    let payout = roundTo(req.body.amount / req.body.odds, 2);

                    console.log('payout : ', payout);

                    let maxBet = response.limitSetting.other.m2.maxPerBet;
                    let minBet = response.limitSetting.other.m2.minPerBet;
                    let maxPayout = response.limitSetting.other.m2.maxPayPerBill;
                    let maxMatchPerBet = response.limitSetting.other.m2.maxMatchPerBet;
                    let minMatchPerBet = response.limitSetting.other.m2.minMatchPerBet;

                    if (req.body.amount > maxBet) {
                        callback(5001, null);
                    } else if (req.body.amount < minBet) {
                        callback(5001, null);
                    } else if (matchCount < minMatchPerBet) {
                        callback(5020, null);
                    } else if (matchCount > maxMatchPerBet) {
                        callback(5021, null);
                    } else if (payout > maxPayout) {
                        callback(5012, null);
                    } else {
                        callback(null, response);
                    }
                }
            });

        }, (memberInfo, callback) => {


            let todayDate;
            if (moment().hours() >= 11) {
                todayDate = moment().utc(true);
            } else {
                todayDate = moment().add(-1, 'days').utc(true);
            }

            let condition = {};
            condition['createdDate'] = {
                "$gte": new Date(todayDate.add(0, 'days').utc(true).format('YYYY-MM-DDT11:00:00.000')),
                "$lt": new Date(todayDate.add(1, 'd').utc(true).format('YYYY-MM-DDT11:00:00.000'))
            };
            condition['memberId'] = mongoose.Types.ObjectId(userInfo._id);
            condition['$or'] = [{'status': 'DONE'}, {'status': 'RUNNING'}];
            BetTransactionModel.aggregate([
                {
                    $match: condition
                },
                {
                    $project: {_id: 0}
                },
                {
                    "$group": {
                        "_id": {
                            id: '$_id'
                        },
                        "amount": {$sum: '$amount'}
                    }
                }
            ], function (err, results) {

                if (err) {
                    callback(err, '');
                } else {
                    callback(null, memberInfo, results[0] ? results[0].amount : 0);
                }
            });

        }, (memberInfo, totalBet, callback) => {

            //Bet to Muay Step2
            checkOdd(req.body.matches,function (err, result) {
                if(err){
                    callback('98', '');
                }else {

                    callback(null, memberInfo, totalBet, result);
                }

            });

        }, (memberInfo, totalBet, matches, callback) => {

            AgentGroupModel.findById(userInfo.group._id).select('_id type parentId shareSetting commissionSetting').populate({
                path: 'parentId',
                select: '_id type parentId shareSetting commissionSetting name',
                populate: {
                    path: 'parentId',
                    select: '_id type parentId shareSetting commissionSetting name',
                    populate: {
                        path: 'parentId',
                        select: '_id type parentId shareSetting commissionSetting name',
                        populate: {
                            path: 'parentId',
                            select: '_id type parentId shareSetting commissionSetting name',
                            populate: {
                                path: 'parentId',
                                select: '_id type parentId shareSetting commissionSetting name',
                                populate: {
                                    path: 'parentId',
                                    select: '_id type parentId shareSetting commissionSetting name',
                                }
                            }
                        }
                    }
                }
            }).exec(function (err, response) {
                if (err)
                    callback(err, null);
                else
                    callback(null, memberInfo, totalBet, matches, response);
            });


        }], (err, memberInfo, totalBet, matches, agentGroups) => {

            if (err) {
                if (err === 5001) {
                    return res.send({
                        message: "amount exceeded (per bet)",
                        result: null,
                        code: 5001
                    });
                }
                else if (err === 5002) {
                    return res.send({
                        message: "amount exceeded (per match)",
                        result: null,
                        code: 5001
                    });
                }
                else if (err === 5003) {
                    return res.send({
                        message: "odd was already changed.",
                        result: matches.matches,
                        code: 5003
                    });
                } else if (err === 5012) {
                    return res.send({
                        message: "Over maximum bet amount.",
                        result: matches,
                        code: 5012
                    });
                } else if (err === 5020) {
                    return res.send({
                        message: "Match Count < min setting.",
                        result: matches,
                        code: 5020
                    });
                } else if (err === 5021) {
                    return res.send({
                        message: "Match Count > max setting.",
                        result: matches,
                        code: 5021
                    });
                }
                else if (err === 9999) {
                    return res.send({
                        message: "err oddMatch.length != req.body.matches.length",
                        result: matches,
                        code: 9999
                    });
                }
                else if (err === 98) {
                    return res.send({
                        message: "Fail To Betting.",
                        result: null,
                        code: 98
                    });
                }
                else if (err === 11) {
                    return res.send({
                        message: "Match is close.",
                        result: null,
                        code: 5006
                    });
                }
                else if (err === 13) {
                    return res.send({
                        message: "odd was already changed.",
                        result: null,
                        code: 5003
                    });
                }
                else if (err === 23) {
                    return res.send({
                        message: "Your total bet has exceeded the maximum bet per match.",
                        result: null,
                        code: 5002
                    });
                }
                else if (err === 24) {
                    return res.send({
                        message: "Your total bet has exceeded the maximum bet per match.",
                        result: null,
                        code: 5002
                    });
                }
                return res.send({message: "error", result: err, code: 999});
            }


            if (matches && matches.isChange) {
                return res.send({
                    message: "odd was already changed.",
                    result: matches.matches,
                    code: 5003
                });
            }

            // console.log('totalBetParlay ::: ',totalBet);
            // console.log((totalBet + req.body.amount) > memberInfo.limitSetting.other.m2.maxBetPerDay)

            if((totalBet + req.body.amount) > memberInfo.limitSetting.other.m2.maxBetPerDay){
                return res.send({
                    message: "amount exceeded (per day)",
                    result: matches.matches,
                    code: 5022
                });
            }

            // console.log('matches-------------------------------',matches);
            let matchTransform = matches.matches;

            matchTransform.forEach(function (ma) {
               ma.matchId = `${ma.leagueId}:${ma.matchId}`
            });

            // console.log(`   JSON BODY => ${JSON.stringify(matchTransform)}`);
            let oddCount = 1;
            _.each(matchTransform, obj => {
                oddCount *= parseFloat(obj.odd);
                // console.log(oddCount);
                // console.log('######### => ', obj.odd);
            });
            // console.log(req.body.amount);
            oddCount = roundTo.down(oddCount, 3);
            // console.log('oddCount =======================', matchTransform);
            let payoutNew = roundTo(req.body.amount * oddCount, 2);
            // console.log(payoutNew);

            let body = {
                betId: generateBetId(),
                memberId: userInfo._id,
                memberParentGroup: memberInfo.group,
                memberUsername: memberInfo.username_lower,
                parlay: {
                    matches: matchTransform,
                    odds: oddCount
                },
                amount: req.body.amount,
                memberCredit: (req.body.amount * -1),
                payout: payoutNew,
                gameType: 'PARLAY',
                acceptHigherPrice: false,
                priceType: 'EU',
                game: game,
                source: source,
                status: matchStatus,
                commission: {},
                gameDate: findGameDate(matchTransform),
                createdDate: DateUtils.getCurrentDate(),
                ipAddress: Utility.getIP(req),
                currency: memberInfo.currency,
                isOfflineCash: isOfflineCash,
                active: !isOfflineCash,
                ref1: '',
            };

            prepareCommission(memberInfo, body, agentGroups, null, 0, 0);

            let model = new BetTransactionModel(body);
            model.save(function (err, betResponse) {

                if (err) {
                    return res.send({message: "error", result: err, code: 999});
                }

                if (userInfo.group.type === 'API') {

                    let body = Object.assign({}, betResponse)._doc;
                    body.username = memberInfo.username;
                    body.gameType = 'PARLAY';
                    body.sportType = 'M2';

                    AgentGroupModel.findById(memberInfo.group, 'endpoint', function (err, agentResponse) {

                        ApiService.placeBet(agentResponse.endpoint, body, (err, response) => {
                            if (err) {
                                return res.send({
                                    code: 999,
                                    message: "error",
                                    result: err
                                });
                            } else {

                                return res.send({
                                    code: 0,
                                    message: "success",
                                    result: {
                                        betId: betResponse.betId,
                                        parlayInfo: betResponse.parlay
                                    }
                                });
                            }

                        });
                    });
                } else {
                    async.waterfall([callback => {

                        MemberService.updateBalance2(memberInfo.username_lower, (req.body.amount * -1), betResponse.betId, 'MUAY_5G_BET', betResponse.betId, function (err, response) {
                            if (err) {
                                callback(5008, null);
                            } else {
                                callback(null, response);
                            }
                        });

                    }, (updateBalanceResponse, callback) => {

                        MemberService.getCredit(userInfo._id, function (err, credit) {
                            if (err) {
                                callback(999, null);
                            } else {
                                callback(null, updateBalanceResponse, credit);
                            }
                        });

                    }], function (err, balanceResponse, credit) {

                        if (err) {
                            return res.send({message: "error", result: err, code: 999});
                        }
                        return res.send(
                            {
                                code: 0,
                                message: "success",
                                result: {
                                    betId: betResponse.betId,
                                    parlayInfo: betResponse.parlay,
                                    creditLimit: credit
                                }
                            }
                        );

                    });
                }
            });

        });

    });


});

function findGameDate(matches) {
    let matchDate = '';
    for(let i=0;i<matches.length;i++){
        if(matchDate){
            if(new Date(matchDate) < new Date(matches[i].matchDate)){
                matchDate = matches[i].matchDate
            }
        }else {
            matchDate = matches[i].matchDate;
        }
    }
    return DateUtils.convertThaiDate(moment(matchDate).toDate());
}

function findPercentCompany(agentGroup) {
    // console.log(`   JSON BODY => ${JSON.stringify(agentGroup)}`);
    if(agentGroup){
        if(agentGroup.parentId && agentGroup.parentId.type === 'SHARE_HOLDER'){
            return 97 - agentGroup.parentId.shareSetting.other.m2.own
        }else if(agentGroup.parentId.parentId && agentGroup.parentId.parentId.type === 'SHARE_HOLDER' ){
            return 97 - agentGroup.parentId.parentId.shareSetting.other.m2.own
        }else if(agentGroup.parentId.parentId.parentId && agentGroup.parentId.parentId.parentId.type === 'SHARE_HOLDER' ){
            return 97 - agentGroup.parentId.parentId.parentId.shareSetting.other.m2.own
        }else if(agentGroup.parentId.parentId.parentId.parentId && agentGroup.parentId.parentId.parentId.parentId.type === 'SHARE_HOLDER' ){
            return 97 - agentGroup.parentId.parentId.parentId.parentId.shareSetting.other.m2.own
        }else if(agentGroup.parentId.parentId.parentId.parentId.parentId && agentGroup.parentId.parentId.parentId.parentId.parentId.type === 'SHARE_HOLDER' ){
            return 97 - agentGroup.parentId.parentId.parentId.parentId.parentId.shareSetting.other.m2.own
        }else if(agentGroup.parentId.parentId.parentId.parentId.parentId.parentId && agentGroup.parentId.parentId.parentId.parentId.parentId.parentId.type === 'SHARE_HOLDER' ){
            return 97 - agentGroup.parentId.parentId.parentId.parentId.parentId.parentId.shareSetting.other.m2.own
        }else {
            return 0;
        }
    }else {
        return 100
    }
}


function checkOdd(matches, callback) {
    let data = {
        token : '4b2b6303956277ddfd7b8ea9cebb31db',
        is_live : '0'
    };

    request.post({
        url: `https://api.aspbet.com/apiservice/amb/api/match_list`,
        body: data,
        rejectUnauthorized: false,
        json:true
    }, function(error, response, body){
        if (error) {
            callback(error, null);
        } else {
            let match = setOdd(matches, body.res.data_list);
            callback(null, match);
        }
    });

}

function getMatchDate(matchId, isLive, callback) {
    getAllMatch(matchId, isLive, function (err, result) {
        if(result){
            let match = _.filter(result, data => {
                return data.m_id == matchId
            });

            callback(null, match[0].fight_dt);
        }else {
            callback('err', null)
        }
    });

}

function getAllMatch(matchId, isLive, callback) {
    let data = {
        token : '4b2b6303956277ddfd7b8ea9cebb31db',
        is_live : isLive
    };

    request.post({
        url: `https://api.aspbet.com/apiservice/amb/api/match_list`,
        body: data,
        rejectUnauthorized: false,
        json:true
    }, function(error, response, body){
        if (error) {
            callback(error, null);
        } else {
            // console.log('-----------------------------',body.res.data_list);
            callback(null, body.res.data_list);
        }
    });

}

function setOdd(matches, matchList) {
    let res = {
        isChange : false,
        matches : []
    };

    for(var i = 0; i < matches.length; i++){
        // console.log('ssss', matches[i])
        for(var j = 0; j< matchList.length; j++){
            // console.log('dddd', matchList[j])
            if(matches[i].matchId === matchList[j].m_id){
                if(matches[i].oddType == 'AH' && matches[i].bet == 'HOME'){
                    //home
                    if(matches[i].odd == matchList[j].hdp_ft_home_multi){
                        res.matches.push({
                            matchId : matchList[j].m_id,
                            odd : matchList[j].hdp_ft_home_multi,
                            oddType : matches[i].oddType,
                            bet : matches[i].bet,
                            leagueId : matchList[j].league_id,
                            leagueName : matchList[j].ln,
                            leagueNameN: {
                                en : matchList[j].ln_en,
                                th : matchList[j].ln_th,
                                cn : matchList[j].ln_en,
                                tw : matchList[j].ln_en
                            },
                            matchName: {
                                en : {
                                    h : matchList[j].h_en,
                                    a : matchList[j].a_en
                                },
                                th : {
                                    h : matchList[j].h_th,
                                    a : matchList[j].a_th
                                },
                                cn : {
                                    h : matchList[j].h_en,
                                    a : matchList[j].a_en
                                },
                                tw : {
                                    h : matchList[j].h_en,
                                    a : matchList[j].a_en
                                }
                            },
                            matchDate : matchList[j].fight_dt,
                            handicap : 0,
                            score : 0,
                            matchType : 'NONE_LIVE'
                        });

                    }else{
                        //odd change
                        res.isChange = true;
                        res.matches.push({
                            matchId : matchList[j].m_id,
                            odd : matchList[j].hdp_ft_home_multi,
                            oddType : matches[i].oddType,
                            bet : matches[i].bet,
                            leagueId : matchList[j].league_id,
                            leagueName : matchList[j].ln,
                            leagueNameN: {
                                en : matchList[j].ln_en,
                                th : matchList[j].ln_th,
                                cn : matchList[j].ln_en,
                                tw : matchList[j].ln_en
                            },
                            matchName: {
                                en : {
                                    h : matchList[j].h_en,
                                    a : matchList[j].a_en
                                },
                                th : {
                                    h : matchList[j].h_th,
                                    a : matchList[j].a_th
                                },
                                cn : {
                                    h : matchList[j].h_en,
                                    a : matchList[j].a_en
                                },
                                tw : {
                                    h : matchList[j].h_en,
                                    a : matchList[j].a_en
                                }
                            },
                            matchDate : matchList[j].fight_dt,
                            handicap : 0,
                            score : 0,
                            matchType : 'NONE_LIVE'
                        });


                    }
                }else if(matches[i].oddType == 'AH' && matches[i].bet == 'AWAY'){
                    //away
                    if(matches[i].odd == matchList[j].hdp_ft_away_multi){
                        res.matches.push({
                            matchId : matchList[j].m_id,
                            odd : matchList[j].hdp_ft_away_multi,
                            oddType : matches[i].oddType,
                            bet : matches[i].bet,
                            leagueId : matchList[j].league_id,
                            leagueName : matchList[j].ln,
                            leagueNameN: {
                                en : matchList[j].ln_en,
                                th : matchList[j].ln_th,
                                cn : matchList[j].ln_en,
                                tw : matchList[j].ln_en
                            },
                            matchName: {
                                en : {
                                    h : matchList[j].h_en,
                                    a : matchList[j].a_en
                                },
                                th : {
                                    h : matchList[j].h_th,
                                    a : matchList[j].a_th
                                },
                                cn : {
                                    h : matchList[j].h_en,
                                    a : matchList[j].a_en
                                },
                                tw : {
                                    h : matchList[j].h_en,
                                    a : matchList[j].a_en
                                }
                            },
                            matchDate : matchList[j].fight_dt,
                            handicap : '0',
                            score : 0,
                            matchType : 'NONE_LIVE'
                        });


                    }else{
                        //odd change
                        res.isChange = true;
                        res.matches.push({
                            matchId : matchList[j].m_id,
                            odd : matchList[j].hdp_ft_away_multi,
                            oddType : matches[i].oddType,
                            bet : matches[i].bet,
                            leagueId : matchList[j].league_id,
                            leagueName : matchList[j].ln,
                            leagueNameN: {
                                en : matchList[j].ln_en,
                                th : matchList[j].ln_th,
                                cn : matchList[j].ln_en,
                                tw : matchList[j].ln_en
                            },
                            matchName: {
                                en : {
                                    h : matchList[j].h_en,
                                    a : matchList[j].a_en
                                },
                                th : {
                                    h : matchList[j].h_th,
                                    a : matchList[j].a_th
                                },
                                cn : {
                                    h : matchList[j].h_en,
                                    a : matchList[j].a_en
                                },
                                tw : {
                                    h : matchList[j].h_en,
                                    a : matchList[j].a_en
                                }
                            },
                            matchDate : matchList[j].fight_dt,
                            handicap : "0",
                            score : 0,
                            matchType : 'NONE_LIVE'
                        });


                    }
                }else if(matches[i].oddType == 'OU' && matches[i].bet == 'OVER'){
                    //over
                    if(matches[i].odd == matchList[j].ou_ht_over_multi){
                        res.matches.push({
                            matchId : matchList[j].m_id,
                            odd : matchList[j].ou_ht_over_multi,
                            oddType : matches[i].oddType,
                            bet : matches[i].bet,
                            leagueId : matchList[j].league_id,
                            leagueName : matchList[j].ln,
                            leagueNameN: {
                                en : matchList[j].ln_en,
                                th : matchList[j].ln_th,
                                cn : matchList[j].ln_en,
                                tw : matchList[j].ln_en
                            },
                            matchName: {
                                en : {
                                    h : matchList[j].h_en,
                                    a : matchList[j].a_en
                                },
                                th : {
                                    h : matchList[j].h_th,
                                    a : matchList[j].a_th
                                },
                                cn : {
                                    h : matchList[j].h_en,
                                    a : matchList[j].a_en
                                },
                                tw : {
                                    h : matchList[j].h_en,
                                    a : matchList[j].a_en
                                }
                            },
                            matchDate : matchList[j].fight_dt,
                            handicap : '4.5',
                            score : 0,
                            matchType : 'NONE_LIVE'
                        });


                    }else{
                        //odd change
                        res.isChange = true;
                        res.matches.push({
                            matchId : matchList[j].m_id,
                            odd : matchList[j].ou_ht_over_multi,
                            oddType : matches[i].oddType,
                            bet : matches[i].bet,
                            leagueId : matchList[j].league_id,
                            leagueName : matchList[j].ln,
                            leagueNameN: {
                                en : matchList[j].ln_en,
                                th : matchList[j].ln_th,
                                cn : matchList[j].ln_en,
                                tw : matchList[j].ln_en
                            },
                            matchName: {
                                en : {
                                    h : matchList[j].h_en,
                                    a : matchList[j].a_en
                                },
                                th : {
                                    h : matchList[j].h_th,
                                    a : matchList[j].a_th
                                },
                                cn : {
                                    h : matchList[j].h_en,
                                    a : matchList[j].a_en
                                },
                                tw : {
                                    h : matchList[j].h_en,
                                    a : matchList[j].a_en
                                }
                            },
                            matchDate : matchList[j].fight_dt,
                            handicap : '4.5',
                            score : 0,
                            matchType : 'NONE_LIVE'
                        })
                    };


                }else {
                    //under
                    if(matches[i].odd == matchList[j].ou_ht_under_multi){
                        res.matches.push({
                            matchId : matchList[j].m_id,
                            odd : matchList[j].ou_ht_under_multi,
                            oddType : matches[i].oddType,
                            bet : matches[i].bet,
                            leagueId : matchList[j].league_id,
                            leagueName : matchList[j].ln,
                            leagueNameN: {
                                en : matchList[j].ln_en,
                                th : matchList[j].ln_th,
                                cn : matchList[j].ln_en,
                                tw : matchList[j].ln_en
                            },
                            matchName: {
                                en : {
                                    h : matchList[j].h_en,
                                    a : matchList[j].a_en
                                },
                                th : {
                                    h : matchList[j].h_th,
                                    a : matchList[j].a_th
                                },
                                cn : {
                                    h : matchList[j].h_en,
                                    a : matchList[j].a_en
                                },
                                tw : {
                                    h : matchList[j].h_en,
                                    a : matchList[j].a_en
                                }
                            },
                            matchDate : matchList[j].fight_dt,
                            handicap : '4.5',
                            score : 0,
                            matchType : 'NONE_LIVE'
                        });


                    }else{
                        //odd change
                        res.isChange = true;
                        res.matches.push({
                            matchId : matchList[j].m_id,
                            odd : matchList[j].ou_ht_under_multi,
                            oddType : matches[i].oddType,
                            bet : matches[i].bet,
                            leagueId : matchList[j].league_id,
                            leagueName : matchList[j].ln,
                            leagueNameN: {
                                en : matchList[j].ln_en,
                                th : matchList[j].ln_th,
                                cn : matchList[j].ln_en,
                                tw : matchList[j].ln_en
                            },
                            matchName: {
                                en : {
                                    h : matchList[j].h_en,
                                    a : matchList[j].a_en
                                },
                                th : {
                                    h : matchList[j].h_th,
                                    a : matchList[j].a_th
                                },
                                cn : {
                                    h : matchList[j].h_en,
                                    a : matchList[j].a_en
                                },
                                tw : {
                                    h : matchList[j].h_en,
                                    a : matchList[j].a_en
                                }
                            },
                            matchDate : matchList[j].fight_dt,
                            handicap : '4.5',
                            score : 0,
                            matchType : 'NONE_LIVE'
                        });


                    }
                }
            }
        }
    }

    return res;

}


function findMaxBet(memberInfo, oddType) {

    switch (oddType.toUpperCase()) {
        case 'AH':
        case 'AH1ST':
        case 'OU':
        case 'OU1ST':
        case 'OE':
        case 'OE1ST':
        case 'T1OU':
        case 'T2OU':
            return memberInfo.limitSetting.other.m2.maxPerBetHDP;
        case 'X12':
        case 'X121ST':
            return memberInfo.limitSetting.other.m2.maxPerBetHDP;
    }
}

function prepareCommission(memberInfo, body, currentAgent, overAgent, remaining, overAllShare) {


    if (currentAgent && (currentAgent.type === 'SUPER_ADMIN' || currentAgent.parentId)) {

        let currentAgentShareSettingParent;
        let currentAgentShareSettingOwn;
        let currentAgentShareSettingRemaining;
        let currentAgentShareSettingMin;

        let overAgentShareSettingParent;
        let overAgentShareSettingOwn;
        let overAgentShareSettingRemaining;
        let overAgentShareSettingMin;

        let agentCommission;


        if (body.gameType === 'PARLAY') {
            currentAgentShareSettingParent = currentAgent.shareSetting.other.m2.parent;
            currentAgentShareSettingOwn = currentAgent.shareSetting.other.m2.own;
            currentAgentShareSettingRemaining = currentAgent.shareSetting.other.m2.remaining;
            currentAgentShareSettingMin = currentAgent.shareSetting.other.m2.min;

            let parlayMatchCount = body.parlay.matches.length;

            if (overAgent) {
                overAgentShareSettingParent = overAgent.shareSetting.other.m2.parent;
                overAgentShareSettingOwn = overAgent.shareSetting.other.m2.own;
                overAgentShareSettingRemaining = overAgent.shareSetting.other.m2.remaining;
                overAgentShareSettingMin = overAgent.shareSetting.other.m2.min;
            } else {
                overAgent = {};
                overAgent.type = 'member';
                overAgentShareSettingParent = memberInfo.shareSetting.other.m2.parent;
                overAgentShareSettingOwn = 0;
                overAgentShareSettingRemaining = 0;

                body.commission.member = {};
                body.commission.member.parent = memberInfo.shareSetting.other.m2.parent;
                body.commission.member.commission = memberInfo.commissionSetting.other.m2;

            }

                agentCommission = currentAgent.commissionSetting.other.m2;

        } else {


            if (body.hdp.matchType === 'LIVE') {
                currentAgentShareSettingParent = currentAgent.shareSetting.other.m2.parent;
                currentAgentShareSettingOwn = currentAgent.shareSetting.other.m2.own;
                currentAgentShareSettingRemaining = currentAgent.shareSetting.other.m2.remaining;
                currentAgentShareSettingMin = currentAgent.shareSetting.other.m2.min;

                if (overAgent) {
                    overAgentShareSettingParent = overAgent.shareSetting.other.m2.parent;
                    overAgentShareSettingOwn = overAgent.shareSetting.other.m2.own;
                    overAgentShareSettingRemaining = overAgent.shareSetting.other.m2.remaining;
                    overAgentShareSettingMin = overAgent.shareSetting.other.m2.min;
                } else {
                    overAgent = {};
                    overAgent.type = 'member';
                    overAgentShareSettingParent = memberInfo.shareSetting.other.m2.parent;
                    overAgentShareSettingOwn = 0;
                    overAgentShareSettingRemaining = 0;

                    body.commission.member = {};
                    body.commission.member.parent = memberInfo.shareSetting.other.m2.parent;
                    body.commission.member.commission = memberInfo.commissionSetting.other.m2;
                }
            } else if (body.hdp.matchType === 'NONE_LIVE') {
                currentAgentShareSettingParent = currentAgent.shareSetting.other.m2.parent;
                currentAgentShareSettingOwn = currentAgent.shareSetting.other.m2.own;
                currentAgentShareSettingRemaining = currentAgent.shareSetting.other.m2.remaining;
                currentAgentShareSettingMin = currentAgent.shareSetting.other.m2.min;

                if (overAgent) {
                    overAgentShareSettingParent = overAgent.shareSetting.other.m2.parent;
                    overAgentShareSettingOwn = overAgent.shareSetting.other.m2.own;
                    overAgentShareSettingRemaining = overAgent.shareSetting.other.m2.remaining;
                    overAgentShareSettingMin = overAgent.shareSetting.other.m2.min;
                } else {
                    overAgent = {};
                    overAgent.type = 'member';
                    overAgentShareSettingParent = memberInfo.shareSetting.other.m2.parent;
                    overAgentShareSettingOwn = 0;
                    overAgentShareSettingRemaining = 0;

                    body.commission.member = {};
                    body.commission.member.parent = memberInfo.shareSetting.other.m2.parent;
                    body.commission.member.commission = memberInfo.commissionSetting.other.m2;
                }
            }

            if (['AH', 'OU', 'OE', 'AH1ST', 'OU1ST', 'OE1ST'].includes(body.hdp.oddType.toUpperCase())) {
                agentCommission = currentAgent.commissionSetting.other.m2;

            } else {
                body.commission.member.commission = memberInfo.commissionSetting.other.m2;
                agentCommission = currentAgent.commissionSetting.other.m2;
            }
        }

        let money = body.amount;

        let currentPercentReceive = overAgentShareSettingParent;
        let totalRemaining = remaining;

        if (overAgentShareSettingRemaining > 0) {
            if (overAgentShareSettingRemaining > totalRemaining) {
                console.log('รับ remaining ทั้งหมดไว้: ' + totalRemaining + ' %');
                currentPercentReceive += totalRemaining;
                totalRemaining = 0;
            } else {
                totalRemaining = remaining - overAgentShareSettingRemaining;
                console.log('หักค่า remaining ที่ได้รับมากับที่เซตรับไว้ = ' + remaining + ' - ' + overAgentShareSettingRemaining + ' = ' + totalRemaining);
                currentPercentReceive += overAgentShareSettingRemaining;
            }

        }

        let unUseShare = currentAgentShareSettingOwn - (overAgentShareSettingParent + overAgentShareSettingOwn);

        totalRemaining = (unUseShare + totalRemaining);

        if (currentAgentShareSettingMin > 0 && ((overAllShare + currentPercentReceive) < currentAgentShareSettingMin)) {

            if (currentPercentReceive < currentAgentShareSettingMin) {
                console.log('% ที่ได้รับ < ขั้นต่ำที่ถูกตั้ง min ไว้');
                let percent = currentAgentShareSettingMin - (currentPercentReceive + overAllShare);
                console.log('หักออกไป ' + percent + ' % : แล้วไปเพิ่มในส่วนที่ต้องรับผิดชอบ ');
                totalRemaining = totalRemaining - percent;
                if (totalRemaining < 0) {
                    totalRemaining = 0;
                }
                currentPercentReceive += percent;
            }
        } else {
            // currentPercentReceive += totalRemaining;
        }


        if (currentAgent.type === 'SUPER_ADMIN') {
            currentPercentReceive += totalRemaining;
        }
        let getMoney = (money * convertPercent(currentPercentReceive)).toFixed(2);
        overAllShare = overAllShare + currentPercentReceive;

        switch (currentAgent.type) {
            case 'SUPER_ADMIN':
                body.commission.superAdmin = {};
                body.commission.superAdmin.group = currentAgent._id;
                body.commission.superAdmin.parent = currentAgent.shareSetting.other.m2.parent;
                body.commission.superAdmin.own = currentAgent.shareSetting.other.m2.own;
                body.commission.superAdmin.remaining = currentAgent.shareSetting.other.m2.remaining;
                body.commission.superAdmin.min = currentAgent.shareSetting.other.m2.min;
                body.commission.superAdmin.shareReceive = Math.abs(currentPercentReceive);
                body.commission.superAdmin.commission = agentCommission;
                body.commission.superAdmin.amount = getMoney;
                break;
            case 'COMPANY':
                body.commission.company = {};
                body.commission.company.parentGroup = currentAgent.parentId;
                body.commission.company.group = currentAgent._id;
                body.commission.company.parent = currentAgentShareSettingParent;
                body.commission.company.own = currentAgentShareSettingOwn;
                body.commission.company.remaining = currentAgentShareSettingRemaining;
                body.commission.company.min = currentAgentShareSettingMin;
                body.commission.company.shareReceive = Math.abs(currentPercentReceive);
                body.commission.company.commission = agentCommission;
                body.commission.company.amount = getMoney;
                break;
            case 'SHARE_HOLDER':
                body.commission.shareHolder = {};
                body.commission.shareHolder.parentGroup = currentAgent.parentId;
                body.commission.shareHolder.group = currentAgent._id;
                body.commission.shareHolder.parent = currentAgentShareSettingParent;
                body.commission.shareHolder.own = currentAgentShareSettingOwn;
                body.commission.shareHolder.remaining = currentAgentShareSettingRemaining;
                body.commission.shareHolder.min = currentAgentShareSettingMin;
                body.commission.shareHolder.shareReceive = currentPercentReceive;
                body.commission.shareHolder.commission = agentCommission;
                body.commission.shareHolder.amount = getMoney;
                break;
            case 'API':
                body.commission.api = {};
                body.commission.api.parentGroup = currentAgent.parentId;
                body.commission.api.group = currentAgent._id;
                body.commission.api.parent = currentAgentShareSettingParent;
                body.commission.api.own = currentAgentShareSettingOwn;
                body.commission.api.remaining = currentAgentShareSettingRemaining;
                body.commission.api.min = currentAgentShareSettingMin;
                body.commission.api.shareReceive = currentPercentReceive;
                body.commission.api.commission = agentCommission;
                body.commission.api.amount = getMoney;
                break;
            case 'SENIOR':
                body.commission.senior = {};
                body.commission.senior.parentGroup = currentAgent.parentId;
                body.commission.senior.group = currentAgent._id;
                body.commission.senior.parent = currentAgentShareSettingParent;
                body.commission.senior.own = currentAgentShareSettingOwn;
                body.commission.senior.remaining = currentAgentShareSettingRemaining;
                body.commission.senior.min = currentAgentShareSettingMin;
                body.commission.senior.shareReceive = currentPercentReceive;
                body.commission.senior.commission = agentCommission;
                body.commission.senior.amount = getMoney;
                break;
            case 'MASTER_AGENT':
                body.commission.masterAgent = {};
                body.commission.masterAgent.parentGroup = currentAgent.parentId;
                body.commission.masterAgent.group = currentAgent._id;
                body.commission.masterAgent.parent = currentAgentShareSettingParent;
                body.commission.masterAgent.own = currentAgentShareSettingOwn;
                body.commission.masterAgent.remaining = currentAgentShareSettingRemaining;
                body.commission.masterAgent.min = currentAgentShareSettingMin;
                body.commission.masterAgent.shareReceive = currentPercentReceive;
                body.commission.masterAgent.commission = agentCommission;
                body.commission.masterAgent.amount = getMoney;
                break;
            case 'AGENT':
                body.commission.agent = {};
                body.commission.agent.parentGroup = currentAgent.parentId;
                body.commission.agent.group = currentAgent._id;
                body.commission.agent.parent = currentAgentShareSettingParent;
                body.commission.agent.own = currentAgentShareSettingOwn;
                body.commission.agent.remaining = currentAgentShareSettingRemaining;
                body.commission.agent.min = currentAgentShareSettingMin;
                body.commission.agent.shareReceive = currentPercentReceive;
                body.commission.agent.commission = agentCommission;
                body.commission.agent.amount = getMoney;
                break;
        }

        prepareCommission(memberInfo, body, currentAgent.parentId, currentAgent, totalRemaining, overAllShare);
    }
}

function convertPercent(percent) {
    return percent / 100
}

function convertOdd(priceType, odd, oddType) {

    if (priceType === 'MY') {
        return odd;
    } else if (priceType === 'HK') {
        if (!odd.includes('-')) {
            return odd;
        }
        odd = odd.replace('-', '');
        odd = parseFloat(odd) * 100;

        let tmp = 100 / odd;
        return tmp.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
    } else if (priceType === 'EU') {
        if (!odd.includes('-')) {
            if (oddType === 'X12' || oddType === 'X121ST') {
                return odd;
            }
            let t = parseFloat(odd) + 1.00;
            if (!t.toString().includes('.')) {
                t = t.toString() + '.00';
                return t;
            } else {

                if (t.toString().length == 3) {
                    t = t.toString() + '0';
                    return t;
                } else {
                    return t.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
                }

            }
        }
        odd = odd.replace('-', '');
        odd = parseFloat(odd) * 100;
        let tmp = (100 / odd) + 1.00;
        return tmp.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];

    } else if (priceType === 'ID') {
        if (oddType === 'X12' || oddType === 'X121ST') {
            return odd;
        }


        let tmp = parseFloat(odd) * -1;
        tmp = 1 / tmp;

        if (!tmp.toString().includes('.')) {
            tmp = tmp.toString() + '.00';
            return tmp;
        } else {

            if (tmp.toString().length == 3) {
                tmp = tmp.toString() + '0';
                return tmp;
            } else {
                return tmp.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
            }

        }
    }
}

const convertMinMaxByCurrency = (value, currency, membercurrency) => {
    // console.log('###########################');
    // console.log('[BF]value      : ', value);
    // console.log('membercurrency : ', membercurrency);

    let minMax = value;
    if (_.isUndefined(membercurrency) || _.isNull(membercurrency)) {
        return minMax;
    } else {
        const predicate = JSON.parse(`{"currencyName":"${membercurrency}"}`);
        const obj = _.chain(currency)
            .findWhere(predicate)
            .pick('currencyTHB')
            .value();
        if (!_.isUndefined(obj.currencyTHB)) {
            // console.log('               : ',obj.currencyTHB);

            if (_.isEqual(membercurrency, "IDR") || _.isEqual(membercurrency, "VND")) {
                minMax = Math.ceil(value * obj.currencyTHB);
            } else {
                minMax = Math.ceil(value / obj.currencyTHB);
            }
            // console.log('[AF]value      : ', minMax);
            // console.log('###########################');
            return minMax;
        } else {
            return minMax;
        }
    }

};


module.exports = router;