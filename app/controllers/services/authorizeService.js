const express = require('express');
const router = express.Router();
const jwt = require('../../common/jwt');
const TokenModel = require("../app/models/token.model");

module.exports = {

    isValidToken: function (token, callback) {

        const decoded = jwt.verify(token);
        let user = decoded.data;
        TokenModel.findOne({username: user.username}, function (err, response) {
            if (err) {
                callback(err, null);
                return;
            }
            if(response){
                if(response.uniqueId !== user.uid){
                    callback(1004,null);
                }else {
                    callback(null, response);
                }
            }else {
                callback(null, response);
            }

        });
    },
}