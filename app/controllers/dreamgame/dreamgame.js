const express = require('express');
const router = express.Router();
const request = require('request');
const moment = require('moment');
const crypto = require('crypto');
const querystring = require('querystring');
const async = require("async");
const config = require('config');
const MemberService = require('../../common/memberService');
const Joi = require('joi');
const DateUtils = require('../../common/dateUtils');
const roundTo = require('round-to');
const mongoose = require('mongoose');
const _ = require('underscore');
const AgentGroupModel = require('../../models/agentGroup.model.js');
const NumberUtils = require('../../common/numberUtils1');
const AgentService = require('../../common/agentService');
const DreamGameService = require('../../common/dreamgameService');

const xml2js = require('xml2js');

const DgTransactionModel = require('../../models/dgTransaction2.model.js');
const BetTransactionModel = require('../../models/betTransaction.model.js');


const DRAW = "DRAW";
const WIN = "WIN";
const HALF_WIN = "HALF_WIN";
const LOSE = "LOSE";
const HALF_LOSE = "HALF_LOSE";


function random(low, high) {
    return Math.floor(Math.random() * (high - low + 1) + low)
}
function generateBetId() {
    return 'BET_DG' + new Date().getTime()+random(1000000,9999999);
}

const CODE_PARAMETER_ERROR = 1;
const CODE_OPERATOR_FAIL = 98;
const CODE_MEMBER_NOT_FOUND = 114;
const CODE_INSUFFICIENT_BALANCE = 120;
const CODE_SYSTEM_ERROR = 501;
const CODE_DATA_OPERATION_ERROR = 503;





router.get('/betreport',(req,res)=>{

    DreamGameService.callReportBet((err, loginResponse) => {

        // let betDetail = loginResponse.betDetail.filter(loginResponse.betDetail.gameId);
        if (err) {
            return res.send({message: 'fail', code: 999});


        }
        else {
            console.log(loginResponse);
            return res.send({message: 'success',
                code: 0,
                result: loginResponse
            });
        }
    });

})
const callbackGetBalanceSchema = Joi.object().keys({
    token: Joi.string().required(),
    member: {
        username: Joi.string().required()
    }
});
//2. get_balance
router.post('/callback/user/getBalance/:agentName', (req, res) => {

    let requestBody = req.body;
    // console.log('requestBody : ', requestBody);
    let validateAgent = Joi.validate(req.body, callbackGetBalanceSchema);
    if (validateAgent.error) {
        return res.send({
            codeId: CODE_PARAMETER_ERROR,
            token: requestBody.token
        });
    }


    MemberService.findByUserNameForPartnerService(requestBody.member.username, (err, memberResponse) => {

        if (err) {
            return res.send({
                codeId: CODE_SYSTEM_ERROR,
                token: requestBody.token
            });

        }

        if (memberResponse) {

            let credit = roundTo((memberResponse.creditLimit + memberResponse.balance), 2);

            return res.send({
                codeId: 0,
                token: requestBody.token,
                member: {
                    username: memberResponse.username.toUpperCase(),
                    balance: credit
                }
            });

        } else {
            return res.send({
                codeId: CODE_MEMBER_NOT_FOUND,
                token: requestBody.token,
                member: {
                    username: "",
                    balance: 0
                }
            });
        }

    });


});

const callbackTransferSchema = Joi.object().keys({
    token: Joi.string().required(),
    ticketId: Joi.alternatives(Joi.string(), Joi.number()).required(),
    data: Joi.string().required(),
    member: {
        username: Joi.string().required(),
        amount: Joi.number().required(),
    }
});

router.post('/callback/account/transfer/:agentName', (req, res) => {
    let requestBody = req.body;
    let validateAgent = Joi.validate(req.body, callbackTransferSchema);
    if (validateAgent.error) {
        return res.send({
            codeId: CODE_PARAMETER_ERROR,
            token: requestBody.token
        });
    }

    saveDgTransactionLog(requestBody, 'TRANSFER', (err, saveDgTransResponse) => {

    });


    if(requestBody.data.startsWith('-') && requestBody.member.amount == 0){

        MemberService.findByUserNameForPartnerService(requestBody.member.username, function (err, memberResponse) {
            if (err) {
                return res.send({
                    codeId: CODE_SYSTEM_ERROR,
                    token: requestBody.token,
                    data: requestBody.data
                });
            }

            if (memberResponse) {

                let credit = roundTo((memberResponse.creditLimit + memberResponse.balance), 2);

                return res.send({
                    codeId: 0,
                    token: requestBody.token,
                    data: requestBody.data,
                    member: {
                        username: memberResponse.username_lower,
                        amount: requestBody.member.amount,
                        balance: credit
                    }
                });
            } else {
                return res.send({
                    codeId: CODE_MEMBER_NOT_FOUND,
                    token: requestBody.token,
                    data: requestBody.data
                });
            }
        });


    } else {


        let ACTION = requestBody.member.amount < 0 ? 'BET' : 'RESULT';

        if (ACTION == 'BET') {

            let betAmount = Math.abs(requestBody.member.amount);

            async.waterfall([callback => {

                MemberService.findByUserNameForPartnerService(requestBody.member.username, function (err, memberResponse) {
                    if (err) {
                        callback(err, null);
                        return;
                    }

                    if (memberResponse) {

                        let credit = roundTo((memberResponse.creditLimit + memberResponse.balance), 2);
                        if (credit < betAmount) {
                            callback(1003, credit);
                        } else {
                            callback(null, memberResponse);
                        }
                    } else {
                        callback(1004, null);
                    }
                });

            },  (memberInfo, callback) => {

                isValidBetStatus(memberInfo,(err, isValidBetStatus) => {
                    if (err) {
                        callback(err, null);
                    } else {

                        if (!isValidBetStatus) {
                            callback(777, null);
                        } else {
                            callback(null,memberInfo, isValidBetStatus);
                        }

                    }
                });

            },(memberInfo, isValidBetStatus,callback) => {

                console.log('isWlEnable : ',memberInfo.limitSetting.casino.isWlEnable);
                if (memberInfo.limitSetting.casino.isWlEnable) {


                    getTodayWinLoss(memberInfo.username, (err, wlAmt) => {
                        if (err) {
                            callback(err, null);
                        } else {


                            if (wlAmt > memberInfo.limitSetting.casino.winPerDay) {
                                callback(666, null);
                            } else {
                                callback(null,memberInfo,isValidBetStatus, wlAmt);
                            }

                        }
                    });

                } else {
                    callback(null, memberInfo,isValidBetStatus,0);
                }

            }], (err, memberInfo,isValidBetStatus, winLossAmt) => {

                if (err) {

                    if (err === 1004) {
                        return res.send({
                            codeId: CODE_MEMBER_NOT_FOUND,
                            token: requestBody.token,
                            data: requestBody.data
                        });
                    }
                    if (err === 1003) {
                        return res.send({
                            codeId: CODE_INSUFFICIENT_BALANCE,
                            token: requestBody.token,
                            data: requestBody.data
                        });
                    }
                    if (err === 666 || err ==777) {
                        return res.send({
                            codeId: CODE_SYSTEM_ERROR,
                            token: requestBody.token,
                            data: requestBody.data
                        });
                    }
                    return res.send({
                        codeId: CODE_SYSTEM_ERROR,
                        token: requestBody.token,
                        data: requestBody.data
                    });
                }

                let beforeCredit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2);

                let condition = {
                    'memberId': mongoose.Types.ObjectId(memberInfo._id),
                    'game': 'CASINO',
                    'source': 'DREAM_GAME',
                    'baccarat.dg.ticketId': requestBody.ticketId
                };


                BetTransactionModel.findOne(condition).lean().exec((err, response) => {

                    if (response) {

                        let sameTxId = _.filter(response.baccarat.dg.txns, item => {
                            return item.data === requestBody.data;
                        });

                        // if (sameTxId.length > 0) {
                        //     return res.send(jsonToSAGameResponseFormat({
                        //         username: requestBody.username,
                        //         currency: requestBody.currency,
                        //         amount: 0,
                        //         error: 1005
                        //     }));
                        // }


                        let update = {
                            $inc: {
                                'amount': betAmount,
                                "memberCredit": (betAmount * -1),
                                "payout": betAmount
                            },
                            $push: {
                                'baccarat.dg.txns': {
                                    data: requestBody.data,
                                    betAmount: betAmount,
                                }
                            }
                        };

                        MemberService.updateBalance2(memberInfo.username_lower, (betAmount * -1), response.betId, 'DREAM_GAME', 'BET|'+requestBody.data, function (err, updateBalanceResponse) {
                            if (err) {

                                if (err == 888) {
                                    return res.send({
                                        codeId: CODE_DATA_OPERATION_ERROR,
                                        token: requestBody.token,
                                        data: requestBody.data
                                    });
                                }

                                return res.send({
                                    codeId: CODE_SYSTEM_ERROR,
                                    token: requestBody.token,
                                    data: requestBody.data
                                });

                            } else {

                                BetTransactionModel.update({_id: response._id}, update, (err, updateTransResponse) => {
                                    if (err) {
                                        return res.send({
                                            codeId: CODE_SYSTEM_ERROR,
                                            token: requestBody.token,
                                            data: requestBody.data
                                        });
                                    }

                                    return res.send({
                                        codeId: 0,
                                        token: requestBody.token,
                                        data: requestBody.data,
                                        member: {
                                            username: memberInfo.username_lower,
                                            amount: requestBody.member.amount,
                                            balance: beforeCredit
                                        }
                                    });
                                });
                            }
                        });

                    } else {


                        let body = {
                            memberId: memberInfo._id,
                            memberParentGroup: memberInfo.group._id,
                            memberUsername: memberInfo.username_lower,
                            betId: generateBetId(),
                            baccarat: {
                                dg: {
                                    ticketId: requestBody.ticketId,
                                    txns: [{
                                        data: requestBody.data,
                                        betAmount: betAmount
                                    }]
                                }
                            },
                            amount: betAmount,
                            memberCredit: betAmount * -1,
                            payout: betAmount,
                            gameType: 'TODAY',
                            acceptHigherPrice: false,
                            priceType: 'TH',
                            game: 'CASINO',
                            source: 'DREAM_GAME',
                            status: 'RUNNING',
                            commission: {},
                            gameDate: DateUtils.getCurrentDate(),
                            createdDate: DateUtils.getCurrentDate(),
                            isEndScore: false,
                            currency: memberInfo.currency,
                            ipAddress: memberInfo.ipAddress,
                            shortBetKey: generateMemberBet(memberInfo.username_lower)
                        };

                        let betTransactionModel = new BetTransactionModel(body);
                        betTransactionModel.save((err, response) => {

                            if (err) {
                                return res.send({
                                    codeId: CODE_SYSTEM_ERROR,
                                    token: requestBody.token,
                                    data: requestBody.data
                                });
                            }

                            MemberService.updateBalance2(memberInfo.username_lower, (betAmount * -1), response.betId, 'DREAM_GAME', 'BET|'+requestBody.data, function (err, updateBalanceResponse) {
                                if (err) {
                                    if (err == 888) {
                                        BetTransactionModel.remove({_id: response._id}, function (err, removeResult) {
                                            console.log(removeResult)
                                        });
                                        return res.send({
                                            codeId: CODE_DATA_OPERATION_ERROR,
                                            token: requestBody.token,
                                            data: requestBody.data
                                        });
                                    }
                                    return res.send({
                                        codeId: CODE_SYSTEM_ERROR,
                                        token: requestBody.token,
                                        data: requestBody.data
                                    });

                                } else {


                                    return res.send({
                                        codeId: 0,
                                        token: requestBody.token,
                                        data: requestBody.data,
                                        member: {
                                            username: memberInfo.username_lower,
                                            amount: requestBody.member.amount,
                                            balance: beforeCredit
                                        }
                                    });
                                }
                            });
                        });
                    }

                });
            });

            //RESULT
        } else {


            async.series([callback => {

                MemberService.findByUserNameForPartnerService(requestBody.member.username, function (err, memberResponse) {
                    if (err) {
                        callback(err, null);
                        return;
                    }
                    if (memberResponse) {
                        callback(null, memberResponse);
                    } else {
                        callback(1004, null);
                    }
                });

            }], (err, asyncResponse) => {

                if (err) {
                    if (err === 1004) {
                        return res.send({
                            codeId: CODE_MEMBER_NOT_FOUND,
                            token: requestBody.token,
                            data: requestBody.data
                        });
                    }

                    return res.send({
                        codeId: CODE_SYSTEM_ERROR,
                        token: requestBody.token,
                        data: requestBody.data
                    });
                }

                let memberInfo = asyncResponse[0];

                let credit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2);

                let condition = {
                    'memberId': mongoose.Types.ObjectId(memberInfo._id),
                    'game': 'CASINO',
                    'source': 'DREAM_GAME',
                    'baccarat.dg.ticketId': requestBody.ticketId,
                    'status': 'RUNNING'
                };


                BetTransactionModel.findOne(condition)
                    .populate({path: 'memberParentGroup', select: 'type'})
                    .lean()
                    .exec(function (err, betObj) {


                        if (!betObj) {
                            return res.send({
                                codeId: CODE_DATA_OPERATION_ERROR,
                                token: requestBody.token,
                                data: requestBody.data
                            });
                        }

                        AgentGroupModel.findById(memberInfo.group).select('_id type parentId shareSetting commissionSetting').populate({
                            path: 'parentId',
                            select: '_id type parentId shareSetting commissionSetting name',
                            populate: {
                                path: 'parentId',
                                select: '_id type parentId shareSetting commissionSetting name',
                                populate: {
                                    path: 'parentId',
                                    select: '_id type parentId shareSetting commissionSetting name',
                                    populate: {
                                        path: 'parentId',
                                        select: '_id type parentId shareSetting commissionSetting name',
                                        populate: {
                                            path: 'parentId',
                                            select: '_id type parentId shareSetting commissionSetting name',
                                            populate: {
                                                path: 'parentId',
                                                select: '_id type parentId shareSetting commissionSetting name',
                                            }
                                        }
                                    }
                                }
                            }

                        }).exec(function (err, agentGroups) {

                            let betAmount = betObj.amount;
                            let resultCash = roundTo(Number.parseFloat(requestBody.member.amount), 2);
                            let winLose = resultCash - betAmount;
                            let betResult = winLose > 0 ? WIN : winLose < 0 ? LOSE : DRAW;

                            let body = betObj;

                            prepareCommission(memberInfo, body, agentGroups, null, 0, 0);

                            //init
                            if (!body.commission.senior) {
                                body.commission.senior = {};
                            }

                            if (!body.commission.masterAgent) {
                                body.commission.masterAgent = {};
                            }

                            if (!body.commission.agent) {
                                body.commission.agent = {};
                            }

                            const shareReceive = AgentService.prepareShareReceive(winLose, betResult, betObj);

                            body.commission.superAdmin.winLoseCom = shareReceive.superAdmin.winLoseCom;
                            body.commission.superAdmin.winLose = shareReceive.superAdmin.winLose;
                            body.commission.superAdmin.totalWinLoseCom = shareReceive.superAdmin.totalWinLoseCom;

                            body.commission.company.winLoseCom = shareReceive.company.winLoseCom;
                            body.commission.company.winLose = shareReceive.company.winLose;
                            body.commission.company.totalWinLoseCom = shareReceive.company.totalWinLoseCom;

                            body.commission.shareHolder.winLoseCom = shareReceive.shareHolder.winLoseCom;
                            body.commission.shareHolder.winLose = shareReceive.shareHolder.winLose;
                            body.commission.shareHolder.totalWinLoseCom = shareReceive.shareHolder.totalWinLoseCom;

                            body.commission.senior.winLoseCom = shareReceive.senior.winLoseCom;
                            body.commission.senior.winLose = shareReceive.senior.winLose;
                            body.commission.senior.totalWinLoseCom = shareReceive.senior.totalWinLoseCom;

                            body.commission.masterAgent.winLoseCom = shareReceive.masterAgent.winLoseCom;
                            body.commission.masterAgent.winLose = shareReceive.masterAgent.winLose;
                            body.commission.masterAgent.totalWinLoseCom = shareReceive.masterAgent.totalWinLoseCom;

                            body.commission.agent.winLoseCom = shareReceive.agent.winLoseCom;
                            body.commission.agent.winLose = shareReceive.agent.winLose;
                            body.commission.agent.totalWinLoseCom = shareReceive.agent.totalWinLoseCom;

                            body.commission.member.winLoseCom = shareReceive.member.winLoseCom;
                            body.commission.member.winLose = shareReceive.member.winLose;
                            body.commission.member.totalWinLoseCom = shareReceive.member.totalWinLoseCom;
                            body.validAmount = betResult === DRAW ? 0 : body.amount;


                            updateAgentMemberBalance(shareReceive, betAmount, requestBody.data, function (err, updateBalanceResponse) {
                                if (err) {

                                    if (err == 888) {
                                        return res.send({
                                            codeId: CODE_SYSTEM_ERROR,
                                            token: requestBody.token,
                                            data: requestBody.data
                                        });
                                    }
                                    return res.send({
                                        codeId: CODE_SYSTEM_ERROR,
                                        token: requestBody.token,
                                        data: requestBody.data
                                    });

                                } else {

                                    let newBalance = updateBalanceResponse.newBalance;

                                    let updateBody = {
                                        $set: {
                                            // 'baccarat.lotus.winLose': winLose,
                                            'commission': body.commission,
                                            'validAmount': body.validAmount,
                                            'betResult': betResult,
                                            'isEndScore': true,
                                            'status': 'DONE',
                                            'remark':body.memberParentGroup.type,
                                            'updatedDate':DateUtils.getCurrentDate(),
                                            'settleDate': DateUtils.getCurrentDate()
                                        }
                                    };


                                    BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                                        if (err) {
                                            return res.send({
                                                codeId: CODE_SYSTEM_ERROR,
                                                token: requestBody.token,
                                                data: requestBody.data
                                            });
                                        } else {

                                                return res.send({
                                                    codeId: 0,
                                                    token: requestBody.token,
                                                    data: requestBody.data,
                                                    member: {
                                                        username: memberInfo.username_lower,
                                                        amount: requestBody.member.amount,
                                                        balance: roundTo((credit + shareReceive.member.winLoseCom), 2)
                                                    }
                                                });
                                        }
                                    });
                                }
                            });
                        });

                    });
            });

        }
    }

});

const callbackCheckTransferSchema = Joi.object().keys({
    token: Joi.string().required(),
    data: Joi.string().required()
});

router.post('/callback/account/checkTransfer/:agentName', (req, res) => {

    let requestBody = req.body;
    let validateAgent = Joi.validate(req.body, callbackCheckTransferSchema);
    if (validateAgent.error) {
        return res.send({
            codeId: CODE_PARAMETER_ERROR,
            token: requestBody.token
        });
    }


    let condition = {
        'game': 'CASINO',
        'source': 'DREAM_GAME',
        'baccarat.dg.txns.data': requestBody.data
    };

    BetTransactionModel.findOne(condition).lean().exec((err, response) => {

        if (response) {
            return res.send({
                codeId: 0,
                token: requestBody.token
            });
        } else {
            return res.send({
                codeId: CODE_OPERATOR_FAIL,
                token: requestBody.token
            });
        }
    });

});

const callbackInformSchema = Joi.object().keys({
    token: Joi.string().required(),
    ticketId: Joi.alternatives(Joi.string(), Joi.number()).required(),
    data: Joi.string().required(),
    member: {
        username: Joi.string().required(),
        amount: Joi.number().required(),
    }
});

router.post('/callback/account/inform/:agentName', (req, res) => {


    let requestBody = req.body;
    let validateAgent = Joi.validate(req.body, callbackInformSchema);
    if (validateAgent.error) {
        return res.send({
            codeId: CODE_PARAMETER_ERROR,
            token: requestBody.token
        });
    }

    saveDgTransactionLog(requestBody, 'INFORM', (err, saveDgTransResponse) => {

    });


    if(requestBody.data.startsWith('-') && requestBody.member.amount == 0){

        MemberService.findByUserNameForPartnerService(requestBody.member.username, function (err, memberResponse) {
            if (err) {
                return res.send({
                    codeId: CODE_SYSTEM_ERROR,
                    token: requestBody.token,
                    data: requestBody.data
                });
            }

            if (memberResponse) {

                let credit = roundTo((memberResponse.creditLimit + memberResponse.balance), 2);

                return res.send({
                    codeId: 0,
                    token: requestBody.token,
                    data: requestBody.data,
                    member: {
                        username: memberResponse.username_lower,
                        amount: requestBody.member.amount,
                        balance: credit
                    }
                });
            } else {
                return res.send({
                    codeId: CODE_MEMBER_NOT_FOUND,
                    token: requestBody.token,
                    data: requestBody.data
                });
            }
        });


    } else {


        let ACTION = requestBody.member.amount < 0 ? 'BET' : 'RESULT';


        if (ACTION == 'BET') {

            async.waterfall([callback => {

                MemberService.findByUserNameForPartnerService(requestBody.member.username, function (err, memberResponse) {
                    if (err) {
                        callback(err, null);
                        return;
                    }
                    if (memberResponse) {
                        callback(null, memberResponse);
                    } else {
                        callback(1004, null);
                    }
                });

            }], (err, memberInfo) => {

                if (err) {
                    if (err === 1004) {
                        return res.send({
                            codeId: CODE_MEMBER_NOT_FOUND,
                            token: requestBody.token,
                            data: requestBody.data
                        });
                    }
                    return res.send({
                        codeId: CODE_SYSTEM_ERROR,
                        token: requestBody.token,
                        data: requestBody.data
                    });
                }

                let beforeCredit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2);

                let betAmount = Math.abs(requestBody.member.amount);

                let condition = {
                    'memberId': mongoose.Types.ObjectId(memberInfo._id),
                    'game': 'CASINO',
                    'source': 'DREAM_GAME',
                    'baccarat.dg.ticketId': requestBody.ticketId,
                    'baccarat.dg.txns.data': requestBody.data
                };

                BetTransactionModel.findOne(condition).lean().exec((err, betObj) => {

                    if (!betObj) {


                        return res.send({
                            codeId: 0,
                            token: requestBody.token,
                            data: requestBody.data,
                            member: {
                                username: memberInfo.username_lower,
                                amount: requestBody.member.amount,
                                balance: beforeCredit
                            }
                        });
                    }

                    //HAVE SAME SERIAL
                    let updateCondition = {
                        $pull: {'baccarat.dg.txns': {"data": requestBody.data}},
                        $inc: {
                            amount: betAmount * -1,
                            memberCredit: betAmount
                        }
                    };

                    BetTransactionModel.update({
                        _id: betObj._id,
                        'baccarat.dg.txns.data': requestBody.data
                    }, updateCondition, {safe: true}, function (err, cancelResponse) {
                        if (err) {
                            return res.send({
                                codeId: CODE_DATA_OPERATION_ERROR,
                                token: requestBody.token,
                                data: requestBody.data
                            });
                        }

                        if (cancelResponse.nModified == 1) {
                            MemberService.updateBalance2(memberInfo.username_lower, betAmount, betObj.betId, 'DREAM_GAME', 'CANCEL_BET|'+requestBody.data, function (err, updateBalanceResponse) {

                                if (err) {
                                    if (err == 888) {
                                        return res.send({
                                            codeId: 0,
                                            token: requestBody.token,
                                            data: requestBody.data
                                        });
                                    }

                                    return res.send({
                                        codeId: CODE_SYSTEM_ERROR,
                                        token: requestBody.token,
                                        data: requestBody.data
                                    });
                                } else {

                                    BetTransactionModel.remove({
                                        _id: betObj._id,
                                        "baccarat.dg.txns": {$size: 0}
                                    }, function (err, removeResponse) {
                                        console.log("remove res : ", removeResponse)
                                        console.log(err)
                                    });

                                    return res.send({
                                        codeId: 0,
                                        token: requestBody.token,
                                        data: requestBody.data,
                                        member: {
                                            username: memberInfo.username_lower,
                                            amount: requestBody.member.amount,
                                            balance: beforeCredit
                                        }
                                    });
                                }
                            });
                        } else {
                            return res.send({
                                codeId: CODE_DATA_OPERATION_ERROR,
                                token: requestBody.token,
                                data: requestBody.data
                            });
                        }
                    });


                });
            });

        } else {

            async.series([callback => {

                MemberService.findByUserNameForPartnerService(requestBody.member.username, function (err, memberResponse) {
                    if (err) {
                        callback(err, null);
                        return;
                    }
                    if (memberResponse) {
                        callback(null, memberResponse);
                    } else {
                        callback(1004, null);
                    }
                });

            }], (err, asyncResponse) => {

                if (err) {
                    if (err === 1004) {
                        return res.send({
                            codeId: CODE_MEMBER_NOT_FOUND,
                            token: requestBody.token,
                            data: requestBody.data
                        });
                    }

                    return res.send({
                        codeId: CODE_SYSTEM_ERROR,
                        token: requestBody.token,
                        data: requestBody.data
                    });
                }

                let memberInfo = asyncResponse[0];

                let beforeCredit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2);

                let condition = {
                    'memberId': mongoose.Types.ObjectId(memberInfo._id),
                    'game': 'CASINO',
                    'source': 'DREAM_GAME',
                    'baccarat.dg.ticketId': requestBody.ticketId
                };

                BetTransactionModel.findOne(condition)
                    .populate({path: 'memberParentGroup', select: 'type'})
                    .lean().exec(function (err, betObj) {


                    if (!betObj) {
                        return res.send({
                            codeId: CODE_DATA_OPERATION_ERROR,
                            token: requestBody.token,
                            data: requestBody.data
                        });
                    }

                    if (betObj.status === 'DONE') {
                        return res.send({
                            codeId: 0,
                            token: requestBody.token,
                            data: requestBody.data,
                            member: {
                                username: memberInfo.username_lower,
                                balance: beforeCredit
                            }
                        });
                    }

                    AgentGroupModel.findById(memberInfo.group).select('_id type parentId shareSetting commissionSetting').populate({
                        path: 'parentId',
                        select: '_id type parentId shareSetting commissionSetting name',
                        populate: {
                            path: 'parentId',
                            select: '_id type parentId shareSetting commissionSetting name',
                            populate: {
                                path: 'parentId',
                                select: '_id type parentId shareSetting commissionSetting name',
                                populate: {
                                    path: 'parentId',
                                    select: '_id type parentId shareSetting commissionSetting name',
                                    populate: {
                                        path: 'parentId',
                                        select: '_id type parentId shareSetting commissionSetting name',
                                        populate: {
                                            path: 'parentId',
                                            select: '_id type parentId shareSetting commissionSetting name',
                                        }
                                    }
                                }
                            }
                        }

                    }).exec(function (err, agentGroups) {

                        let betAmount = betObj.amount;
                        let resultCash = roundTo(Number.parseFloat(requestBody.member.amount), 2);
                        let winLose = resultCash - betAmount;
                        let betResult = winLose > 0 ? WIN : winLose < 0 ? LOSE : DRAW;

                        let body = betObj;

                        prepareCommission(memberInfo, body, agentGroups, null, 0, 0);

                        //init
                        if (!body.commission.senior) {
                            body.commission.senior = {};
                        }

                        if (!body.commission.masterAgent) {
                            body.commission.masterAgent = {};
                        }

                        if (!body.commission.agent) {
                            body.commission.agent = {};
                        }

                        const shareReceive = AgentService.prepareShareReceive(winLose, betResult, betObj);

                        body.commission.superAdmin.winLoseCom = shareReceive.superAdmin.winLoseCom;
                        body.commission.superAdmin.winLose = shareReceive.superAdmin.winLose;
                        body.commission.superAdmin.totalWinLoseCom = shareReceive.superAdmin.totalWinLoseCom;

                        body.commission.company.winLoseCom = shareReceive.company.winLoseCom;
                        body.commission.company.winLose = shareReceive.company.winLose;
                        body.commission.company.totalWinLoseCom = shareReceive.company.totalWinLoseCom;

                        body.commission.shareHolder.winLoseCom = shareReceive.shareHolder.winLoseCom;
                        body.commission.shareHolder.winLose = shareReceive.shareHolder.winLose;
                        body.commission.shareHolder.totalWinLoseCom = shareReceive.shareHolder.totalWinLoseCom;

                        body.commission.senior.winLoseCom = shareReceive.senior.winLoseCom;
                        body.commission.senior.winLose = shareReceive.senior.winLose;
                        body.commission.senior.totalWinLoseCom = shareReceive.senior.totalWinLoseCom;

                        body.commission.masterAgent.winLoseCom = shareReceive.masterAgent.winLoseCom;
                        body.commission.masterAgent.winLose = shareReceive.masterAgent.winLose;
                        body.commission.masterAgent.totalWinLoseCom = shareReceive.masterAgent.totalWinLoseCom;

                        body.commission.agent.winLoseCom = shareReceive.agent.winLoseCom;
                        body.commission.agent.winLose = shareReceive.agent.winLose;
                        body.commission.agent.totalWinLoseCom = shareReceive.agent.totalWinLoseCom;

                        body.commission.member.winLoseCom = shareReceive.member.winLoseCom;
                        body.commission.member.winLose = shareReceive.member.winLose;
                        body.commission.member.totalWinLoseCom = shareReceive.member.totalWinLoseCom;
                        body.validAmount = betResult === DRAW ? 0 : body.amount;

                        updateAgentMemberBalance(shareReceive, betAmount, requestBody.data, function (err, updateBalanceResponse) {
                            if (err) {

                                if (err == 888) {
                                    return res.send({
                                        codeId: 0,
                                        token: requestBody.token,
                                        data: requestBody.data,
                                        member: {
                                            username: memberInfo.username_lower,
                                            balance: beforeCredit
                                        }
                                    });
                                }
                                return res.send({
                                    codeId: CODE_SYSTEM_ERROR,
                                    token: requestBody.token,
                                    data: requestBody.data,
                                    member: {
                                        username: memberInfo.username_lower,
                                        balance: beforeCredit
                                    }
                                });

                            } else {

                                let newBalance = updateBalanceResponse.newBalance;

                                let updateBody = {
                                    $set: {
                                        'commission': body.commission,
                                        'validAmount': body.validAmount,
                                        'betResult': betResult,
                                        'isEndScore': true,
                                        'status': 'DONE',
                                        'updatedDate':DateUtils.getCurrentDate(),
                                        'settleDate': DateUtils.getCurrentDate()
                                    }
                                };


                                BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                                    if (err) {
                                        return res.send({
                                            codeId: CODE_SYSTEM_ERROR,
                                            token: requestBody.token,
                                            data: requestBody.data,
                                            member: {
                                                username: memberInfo.username_lower,
                                                balance: newBalance
                                            }
                                        });
                                    } else {
                                            return res.send({
                                                codeId: 0,
                                                token: requestBody.token,
                                                data: requestBody.data,
                                                member: {
                                                    username: memberInfo.username_lower,
                                                    balance: roundTo((newBalance + shareReceive.member.winLoseCom), 2)
                                                }
                                            });

                                    }
                                });
                            }
                        });
                    });

                });
            });

        }
    }
});

const callbackOrderReconcileSchema = Joi.object().keys({
    token: Joi.string().required(),
    ticketId: Joi.alternatives(Joi.string(), Joi.number()).required()
});

router.post('/callback/account/order/:agentName', (req, res) => {

    let requestBody = req.body;
    let validateAgent = Joi.validate(req.body, callbackOrderReconcileSchema);
    if (validateAgent.error) {
        return res.send({
            codeId: CODE_PARAMETER_ERROR,
            token: requestBody.token
        });
    }


    let condition = {
        'game': 'CASINO',
        'source': 'DREAM_GAME',
        'baccarat.dg.ticketId': requestBody.ticketId
    };

    BetTransactionModel.aggregate([
        {
            $match: condition
        },
        {
            $unwind: "$baccarat.dg.txns"
        },
        {
            $project: {
                username: "$memberUsername",
                ticketId: "$baccarat.dg.ticketId",
                serial: "$baccarat.dg.txns.data",
                betAmount: "$baccarat.dg.txns.betAmount",
            }
        }
    ]).exec((err, response) => {

        if (err) {
            return res.send({
                codeId: CODE_SYSTEM_ERROR,
                token: requestBody.token,
                ticketId: requestBody.ticketId
            });
        }
        if (response) {

            let list = _.map(response, (item) => {
                return {
                    username: item.username,
                    ticketId: item.ticketId,
                    serial: item.serial,
                    amount: item.betAmount
                }
            });

            return res.send({
                codeId: 0,
                token: requestBody.token,
                ticketId: requestBody.ticketId,
                list: list
            });
        } else {
            return res.send({
                codeId: 0,
                token: requestBody.token,
                ticketId: requestBody.ticketId,
                list: []
            });
        }
    });

});

const callbackUnsettleSchema = Joi.object().keys({
    token: Joi.string().required()
});

router.post('/callback/account/unsettle/:agentName', (req, res) => {

    let requestBody = req.body;
    let validateAgent = Joi.validate(req.body, callbackUnsettleSchema);
    if (validateAgent.error) {
        return res.send({
            codeId: CODE_PARAMETER_ERROR,
            token: requestBody.token
        });
    }


    let condition = {
        'game': 'CASINO',
        'source': 'DREAM_GAME',
        'status':'RUNNING',
        'gameDate': {$gte: moment().add(-10, 'minute').utc(true).toDate()},
    };

    BetTransactionModel.aggregate([
        {
            $match: condition
        },
        {
            $unwind: "$baccarat.dg.txns"
        },
        {
            $project: {
                username: "$memberUsername",
                ticketId: "$baccarat.dg.ticketId",
                serial: "$baccarat.dg.txns.data",
                betAmount: "$baccarat.dg.txns.betAmount",
            }
        }
    ]).exec((err, response) => {

        if (err) {
            return res.send({
                codeId: CODE_SYSTEM_ERROR,
                token: requestBody.token,
                ticketId: requestBody.ticketId
            });
        }

        if (response) {
            let list = _.map(response, (item) => {
                return {
                    username: item.username,
                    ticketId: item.ticketId,
                    serial: item.serial,
                    amount: item.betAmount
                }
            });

            return res.send({
                codeId: 0,
                token: requestBody.token,
                list: list
            });
        } else {
            return res.send({
                codeId: 0,
                token: requestBody.token,
                list: []
            });
        }
    });

});

//2.1. token_forward_game
router.get('/login', (req, res) => {


    console.log(req.query);

    let userInfo = req.userInfo;

    async.waterfall([callback => {

        AgentService.getUpLineStatus(userInfo.group._id, function (err, status) {
            if (err) {
                callback(err, null);
            } else {
                if (status.suspend) {
                    callback(5011, null);
                } else {
                    callback(null, status);
                }
            }
        });

    }, (uplineStatus, callback) => {

        MemberService.findById(userInfo._id, 'creditLimit balance limitSetting group suspend username_lower', function (err, memberResponse) {
            if (err) {
                callback(err, null);
            } else {

                if (memberResponse.suspend) {
                    callback(5011, null);
                } else if (memberResponse.limitSetting.casino.dg.isEnable) {
                    AgentService.getUpLineStatus(memberResponse.group._id, (err, response) => {
                        if (!response.casinoDgEnable) {
                            callback(1088, null);
                        } else {
                            callback(null, uplineStatus, memberResponse);
                        }
                    });
                } else {
                    callback(1088, null);
                }
            }
        });

    }], function (err, uplineStatus, memberInfo) {

        if (err) {
            if (err == 1088) {
                return res.send({
                    code: 1088,
                    message: 'Service Locked, Please contact your upline.'
                });
            }
            if (err == 5011) {
                return res.send({
                    message: "Account or Upline was suspend.",
                    result: null,
                    code: 5011
                });
            }
            return res.send({code: 999, message: err});
        }


    let client = req.query.client;

    DreamGameService.callRegisterNewMember(memberInfo.username_lower, (err, registerResponse) => {

        if (err) {
            return res.send({message: 'fail', code: 999});
        } else {
            //bar 601 , sicbo 631 , dragon 640
            if (registerResponse.codeId == 116) {

            }
            let device = 5;
            DreamGameService.callMemberLogin(memberInfo.username_lower, device,memberInfo.limitSetting.casino.dg.limit ,(err, url) => {

                if (err) {
                    return res.send({message: err, code: 999});
                } else {
                    return res.send({message: 'success', code: 0, url: url});
                }
            });


        }
    });

    });


});

function prepareCommission(memberInfo, body, currentAgent, overAgent, remaining, overAllShare) {


    if (currentAgent && (currentAgent.type === 'SUPER_ADMIN' || currentAgent.parentId)) {


        console.log('==============================================');
        console.log('process : ' + currentAgent.type + ' : ' + currentAgent.name);


        let money = body.amount;


        if (!overAgent) {
            overAgent = {};
            overAgent.type = 'member';
            overAgent.shareSetting = {};
            overAgent.shareSetting.casino = {};
            overAgent.shareSetting.casino.dg = {};
            overAgent.shareSetting.casino.dg.parent = memberInfo.shareSetting.casino.dg.parent;
            overAgent.shareSetting.casino.dg.own = 0;
            overAgent.shareSetting.casino.dg.remaining = 0;

            body.commission.member = {};
            body.commission.member.parent = memberInfo.shareSetting.casino.dg.parent;
            body.commission.member.commission = memberInfo.commissionSetting.casino.dg;

        }

        console.log('');
        console.log('---- setting ----');
        console.log('ส่วนแบ่งที่ได้มามีทั้งหมด : ' + currentAgent.shareSetting.casino.dg.own + ' %');
        console.log('ขอส่วนแบ่งจาก : ' + overAgent.type + ' ' + overAgent.shareSetting.casino.dg.parent + ' %');
        console.log('ให้ส่วนแบ่ง : ' + overAgent.type + ' ที่เหลือไป ' + overAgent.shareSetting.casino.dg.own + ' %');
        console.log('remaining จาก : ' + overAgent.type + ' : ' + overAgent.shareSetting.casino.dg.remaining + ' %');
        console.log('โดนบังคับเสียขั้นต่ำ : ' + currentAgent.shareSetting.casino.dg.min + ' %');
        console.log('---- end setting ----');
        console.log('');


        // console.log('และได้รับ remaining ไว้ : '+overAgent.shareSetting.casino.dg.remaining+' %');

        let currentPercentReceive = overAgent.shareSetting.casino.dg.parent;
        console.log('% ที่ได้ : ' + currentPercentReceive);
        console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');
        let totalRemaining = remaining;

        if (overAgent.shareSetting.casino.dg.remaining > 0) {
            // console.log()
            // console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');

            if (overAgent.shareSetting.casino.dg.remaining > totalRemaining) {
                console.log('รับ remaining ทั้งหมดไว้: ' + totalRemaining + ' %');
                currentPercentReceive += totalRemaining;
                totalRemaining = 0;
            } else {
                totalRemaining = remaining - overAgent.shareSetting.casino.dg.remaining;
                console.log('หักค่า remaining ที่ได้รับมากับที่เซตรับไว้ = ' + remaining + ' - ' + overAgent.shareSetting.casino.dg.remaining + ' = ' + totalRemaining);
                currentPercentReceive += overAgent.shareSetting.casino.dg.remaining;
            }

        }

        console.log('รวม % ที่ได้รับ : ' + currentPercentReceive);


        let unUseShare = currentAgent.shareSetting.casino.dg.own -
            (overAgent.shareSetting.casino.dg.parent + overAgent.shareSetting.casino.dg.own);

        console.log('เหลือส่วนแบ่งที่ไม่ได้ใช้ : ' + unUseShare + ' %');
        totalRemaining = (unUseShare + totalRemaining);
        console.log('ส่วนแบ่งที่ไม่ได้ใช้ บวกกับค่า remaining ท่ี่เหลือจะเป็น : ' + totalRemaining + ' %');

        console.log('จากที่โดนบังคับเสียขั้นต่ำ : ' + currentAgent.shareSetting.casino.dg.min + ' %');
        console.log('overAllShare : ', overAllShare);
        if (currentAgent.shareSetting.casino.dg.min > 0 && ((overAllShare + currentPercentReceive) < currentAgent.shareSetting.casino.dg.min)) {

            if (currentPercentReceive < currentAgent.shareSetting.casino.dg.min) {
                console.log('% ที่ได้รับ < ขั้นต่ำที่ถูกตั้ง min ไว้');
                let percent = currentAgent.shareSetting.casino.dg.min - (currentPercentReceive + overAllShare);
                console.log('หักออกไป ' + percent + ' % : แล้วไปเพิ่มในส่วนที่ต้องรับผิดชอบ ');
                totalRemaining = totalRemaining - percent;
                if (totalRemaining < 0) {
                    totalRemaining = 0;
                }
                currentPercentReceive += percent;
            }
        } else {
            // currentPercentReceive += totalRemaining;
        }


        if (currentAgent.type === 'SUPER_ADMIN') {
            currentPercentReceive += totalRemaining;
        }

        let getMoney = (money * NumberUtils.convertPercent(currentPercentReceive)).toFixed(2);
        overAllShare = overAllShare + currentPercentReceive;

        console.log('ยอดแทง ' + money + ' ได้ทั้งหมด : ' + currentPercentReceive + ' %');

        console.log('รวม % + % remaining  จะเป็นสัดส่วนที่ได้ทั้งหมด = ' + getMoney);
        console.log('ค่าที่จะถูกคืนกลับไป (remaining) : ' + totalRemaining + ' %');

        console.log('จำนวน % ที่ถูกแบ่งไปแล้วทั้งหมด : ', overAllShare);

        //set commission
        let agentCommission = currentAgent.commissionSetting.casino.dg;


        switch (currentAgent.type) {
            case 'SUPER_ADMIN':
                body.commission.superAdmin = {};
                body.commission.superAdmin.group = currentAgent._id;
                body.commission.superAdmin.parent = currentAgent.shareSetting.casino.dg.parent;
                body.commission.superAdmin.own = currentAgent.shareSetting.casino.dg.own;
                body.commission.superAdmin.remaining = currentAgent.shareSetting.casino.dg.remaining;
                body.commission.superAdmin.min = currentAgent.shareSetting.casino.dg.min;
                body.commission.superAdmin.shareReceive = Math.abs(currentPercentReceive);
                body.commission.superAdmin.commission = agentCommission;
                body.commission.superAdmin.amount = getMoney;
                break;
            case 'COMPANY':
                body.commission.company = {};
                body.commission.company.parentGroup = currentAgent.parentId;
                body.commission.company.group = currentAgent._id;
                body.commission.company.parent = currentAgent.shareSetting.casino.dg.parent;
                body.commission.company.own = currentAgent.shareSetting.casino.dg.own;
                body.commission.company.remaining = currentAgent.shareSetting.casino.dg.remaining;
                body.commission.company.min = currentAgent.shareSetting.casino.dg.min;
                body.commission.company.shareReceive = Math.abs(currentPercentReceive);
                body.commission.company.commission = agentCommission;
                body.commission.company.amount = getMoney;
                break;
            case 'SHARE_HOLDER':
                body.commission.shareHolder = {};
                body.commission.shareHolder.parentGroup = currentAgent.parentId;
                body.commission.shareHolder.group = currentAgent._id;
                body.commission.shareHolder.parent = currentAgent.shareSetting.casino.dg.parent;
                body.commission.shareHolder.own = currentAgent.shareSetting.casino.dg.own;
                body.commission.shareHolder.remaining = currentAgent.shareSetting.casino.dg.remaining;
                body.commission.shareHolder.min = currentAgent.shareSetting.casino.dg.min;
                body.commission.shareHolder.shareReceive = currentPercentReceive;
                body.commission.shareHolder.commission = agentCommission;
                body.commission.shareHolder.amount = getMoney;
                break;
            case 'SENIOR':
                body.commission.senior = {};
                body.commission.senior.parentGroup = currentAgent.parentId;
                body.commission.senior.group = currentAgent._id;
                body.commission.senior.parent = currentAgent.shareSetting.casino.dg.parent;
                body.commission.senior.own = currentAgent.shareSetting.casino.dg.own;
                body.commission.senior.remaining = currentAgent.shareSetting.casino.dg.remaining;
                body.commission.senior.min = currentAgent.shareSetting.casino.dg.min;
                body.commission.senior.shareReceive = currentPercentReceive;
                body.commission.senior.commission = agentCommission;
                body.commission.senior.amount = getMoney;
                break;
            case 'MASTER_AGENT':
                body.commission.masterAgent = {};
                body.commission.masterAgent.parentGroup = currentAgent.parentId;
                body.commission.masterAgent.group = currentAgent._id;
                body.commission.masterAgent.parent = currentAgent.shareSetting.casino.dg.parent;
                body.commission.masterAgent.own = currentAgent.shareSetting.casino.dg.own;
                body.commission.masterAgent.remaining = currentAgent.shareSetting.casino.dg.remaining;
                body.commission.masterAgent.min = currentAgent.shareSetting.casino.dg.min;
                body.commission.masterAgent.shareReceive = currentPercentReceive;
                body.commission.masterAgent.commission = agentCommission;
                body.commission.masterAgent.amount = getMoney;
                break;
            case 'AGENT':
                body.commission.agent = {};
                body.commission.agent.parentGroup = currentAgent.parentId;
                body.commission.agent.group = currentAgent._id;
                body.commission.agent.parent = currentAgent.shareSetting.casino.dg.parent;
                body.commission.agent.own = currentAgent.shareSetting.casino.dg.own;
                body.commission.agent.remaining = currentAgent.shareSetting.casino.dg.remaining;
                body.commission.agent.min = currentAgent.shareSetting.casino.dg.min;
                body.commission.agent.shareReceive = currentPercentReceive;
                body.commission.agent.commission = agentCommission;
                body.commission.agent.amount = getMoney;
                break;
        }

        prepareCommission(memberInfo, body, currentAgent.parentId, currentAgent, totalRemaining, overAllShare);
    }
}


function updateAgentMemberBalance(shareReceive, betAmount, ref, updateCallback) {

    console.log('updateAgentMemberBalance');
    console.log('shareReceive : ', shareReceive);


    let balance = betAmount + shareReceive.member.totalWinLoseCom;

    try {
        console.log('update member balance : ', balance);

        MemberService.updateBalance2(shareReceive.memberUsername, balance, shareReceive.betId, 'DREAM_GAME','SETTLE|'+ ref, function (err, updateBalanceResponse) {
            if (err) {
                updateCallback(err, null);
            } else {
                updateCallback(null, updateBalanceResponse);
            }
        });
    } catch (err) {
        MemberService.updateBalance2(shareReceive.memberUsername, balance, shareReceive.betId, 'DG_SETTLE_ERROR ' + shareReceive.member.id + ' : ' + balance, ref, function (err, updateBalanceResponse) {
            if (err) {
                updateCallback(err, null);
            } else {
                updateCallback(null, updateBalanceResponse);
            }
        });
    }

}


function getTodayWinLoss(memberName, callback) {

    let today;
    if (moment().hours() >= 11) {
        today = moment().format('DD/MM/YYYY');
    } else {
        today = moment().add(-1, 'd').format('DD/MM/YYYY');
    }

    let condition = {
        'shortBetKey': `${today}|${memberName.toUpperCase()}|CASINO`
    };

    BetTransactionModel.aggregate([
        {
            $match: condition
        },
        {
            "$group": {
                "_id": {
                    member: '$memberId',
                },
                "memberTotalWinLoseCom": {$sum: '$commission.member.totalWinLoseCom'},
            }
        },
        {
            $project: {
                _id: 0,
                memberTotalWinLoseCom: '$memberTotalWinLoseCom'
            }
        }
    ], (err, results) => {
        callback(null, results[0] ? results[0].memberTotalWinLoseCom : 0);
    });
}


function isValidBetStatus(memberInfo, callback) {

    AgentGroupModel.findById(mongoose.Types.ObjectId(memberInfo.group)).select('_id lock suspend active parentId limitSetting name_lower').populate({
        path: 'parentId',
        select: '_id lock suspend active parentId limitSetting name_lower',
        populate: {
            path: 'parentId',
            select: '_id lock suspend active parentId limitSetting name_lower',
            populate: {
                path: 'parentId',
                select: '_id lock suspend active parentId limitSetting name_lower',
                populate: {
                    path: 'parentId',
                    select: '_id lock suspend active parentId limitSetting name_lower',
                    populate: {
                        path: 'parentId',
                        select: '_id lock suspend active parentId limitSetting name_lower',
                    }
                }
            }
        }
    }).exec(function (err, response) {
        if (err) {
            callback(err, null);
        } else {

            if (response) {
                let result = {
                    lock: false,
                    suspend: false,
                    active: true,
                    casinoDgEnable: true,
                    downline: []
                };

                let status  = isLockOrSuspend(result, response);

                if(status.lock == true || status.suspend == true || status.active == false || status.casinoDgEnable == false ){
                    callback(null, false);
                }else if(memberInfo.lock == true || memberInfo.suspend == true || memberInfo.active == false || memberInfo.limitSetting.casino.dg.isEnable == false ){
                    callback(null, false);
                } else {
                    callback(null, true);
                }

            } else {
                callback('not found', null);
            }
        }
    });

    function isLockOrSuspend(result, data) {

        result.downline.push(data.name_lower);

        if (data.lock) {
            result.lock = true;
        }

        if (data.suspend) {
            result.suspend = true;
        }

        if (!data.active) {
            result.active = false;
        }

        if (!data.limitSetting.casino.sa.isEnable) {
            result.casinoSaEnable = false;
        }


        if (data.parentId) {
            return isLockOrSuspend(result, data.parentId);
        } else {
            result.downline.shift();
            result.downline = result.downline.reverse().join('|');
            return result;
        }
    }
}

function generateMemberBet(username) {

    if (moment().hours() >= 11) {
        return moment().format('DD/MM/YYYY')+'|'+username.toUpperCase()+'|CASINO';
    } else {
        return moment().add(-1, 'd').format('DD/MM/YYYY')+'|'+username.toUpperCase()+'|CASINO';
    }
}

function saveDgTransactionLog(request, action, callback) {

    let model = new DgTransactionModel(
        {
            token: request.token,
            ticketId: request.ticketId,
            data: request.data,
            username: request.member.username,
            amount: request.member.amount,
            action: action,
            createdDate: DateUtils.getCurrentDate(),
        }
    );


    model.save((err, response) => {
        callback(null, 'success');
    });
}


module.exports = router;
