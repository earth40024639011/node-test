const express = require('express');
const router = express.Router();
const _         = require('underscore');

const AgentGroupModel   = require('../models/agentGroup.model');
router.get('/:agent', (req, res) => {
    const {
        agent
    } = req.params;
    AgentGroupModel.findOne({
        name_lower: agent
    })
        .lean()
        .select('_id')
        .exec((err, data) => {
            if (!_.isNull(data)) {
                AgentGroupModel.findById(data).select('_id type parentId').populate({
                    path: 'parentId',
                    select: '_id type parentId name',
                    populate: {
                        path: 'parentId',
                        select: '_id type parentId name',
                        populate: {
                            path: 'parentId',
                            select: '_id type parentId name',
                            populate: {
                                path: 'parentId',
                                select: '_id type parentId name',
                                populate: {
                                    path: 'parentId',
                                    select: '_id type parentId name',
                                    populate: {
                                        path: 'parentId',
                                        select: '_id type parentId name',
                                    }
                                }
                            }
                        }
                    }

                }).exec(function (err, agentGroups) {
                    return res.send({
                        code: 0,
                        message: "success",
                        result : agentGroups
                    });
                });
            } else {
                return res.send({
                    code: -1,
                    message: "Fail",
                    result : `Agent ${agent} not found`
                });
            }
        });

});

module.exports = router;