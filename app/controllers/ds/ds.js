const express = require('express');
const router = express.Router();
const request = require('request');
const moment = require('moment');
const crypto = require('crypto');
const querystring = require('querystring');
const async = require("async");
const config = require('config');
const MemberService = require('../../common/memberService');
const Joi = require('joi');
const DateUtils = require('../../common/dateUtils');
const roundTo = require('round-to');
const mongoose = require('mongoose');
const _ = require('underscore');
const BetTransactionModel = require('../../models/betTransaction.model.js');
const AgentGroupModel = require('../../models/agentGroup.model.js');
const NumberUtils = require('../../common/numberUtils1');
const AgentService = require('../../common/agentService');
const DsGameService = require('../../common/dsgameService');
const MemberModel = require("../../models/member.model");
const uuid = require('uuid/v4');

const DRAW = "DRAW";
const WIN = "WIN";
const HALF_WIN = "HALF_WIN";
const LOSE = "LOSE";
const HALF_LOSE = "HALF_LOSE";


const ERROR_MSG_UNKNOWN = 0;
const ERROR_MSG_SUCCEED = 1;
const ERROR_MSG_MEMBER_DOESNOT_EXIST = 2;
const ERROR_MSG_VERIFICATION_FAIL = 3;
const ERROR_MSG_TRNSACTION_DUBPLICATE = 4;
const ERROR_MSG_BALANCE_INSUFFICIENT = 5;
const ERROR_MSG_TRANSACTION_DOESNOT_EXIST = 6;

function random(low, high) {
    return Math.floor(Math.random() * (high - low + 1) + low)
}
function generateBetId() {
    return 'BET' + new Date().getTime() + random(1000000, 9999999);
}


//3.2.2 token_forward_game
router.get('/login', (req, res) => {


    console.log(req.query);

    let userInfo = req.userInfo;


    async.waterfall([callback => {
        try {
            AgentService.getUpLineStatus(userInfo.group._id, function (err, status) {
                if (err) {
                    callback(err, null);
                } else {
                    if (status.suspend) {
                        callback(5011, null);
                    } else {
                        callback(null, status);
                    }
                }
            });
        } catch (err) {
            callback(9929, null);
        }

    }, (uplineStatus, callback) => {

        try {
            MemberService.findById(userInfo._id, 'creditLimit balance limitSetting group suspend username_lower', function (err, memberResponse) {
                if (err) {
                    callback(err, null);
                } else {

                    if (memberResponse.suspend) {
                        callback(5011, null);
                    } else if (memberResponse.limitSetting.game.slotXO.isEnable) {
                        AgentService.getLive22Status(memberResponse.group._id, (err, response) => {
                            if (!response.isEnable) {
                                callback(1088, null);
                            } else {
                                callback(null, uplineStatus, memberResponse);
                            }
                        });
                    } else {
                        callback(1088, null);
                    }
                }
            });
        } catch (err) {
            callback(999, null);
        }

    }], function (err, uplineStatus, memberInfo) {

        if (err) {
            if (err == 1088) {
                return res.send({
                    code: 1088,
                    message: 'Service Locked, Please contact your upline.'
                });
            }
            if (err == 5011) {
                return res.send({
                    message: "Account or Upline was suspend.",
                    result: null,
                    code: 5011
                });
            }
            return res.send({code: 999, message: err});
        }


        let gameId = req.query.GameId;

        MemberService.findById(userInfo._id, 'username_lower', function (err, memberInfo) {

            DsGameService.callRegisterMember(memberInfo.username_lower, (err, RegisterResponse) => {

                // DsGameService.callMemberBetRecords((err,response)=>{
                //
                // });
                DsGameService.callLoginGame(memberInfo.username_lower, uuid(), gameId, (err, loginResponse) => {
                    if (err) {
                        return res.send({message: 'fail', code: 999});
                    } else {
                        return res.send({message: 'success', url: loginResponse.url, code: 0});
                    }
                });
            });

        });
    });

});


router.get('/getGameList', (req, res) => {

    return res.send({data: DsGameService.callGetGameList(), code: 0});

});

router.get('/getLog', function (req, res) {

    let {
        account,
        gameId,
        agent
    } = req.query;


    DsGameService.callGetTransactionHistoryResult(account, gameId, agent, (err, response) => {


        console.log(response)
        return res.send(
            {
                code: 0,
                message: 'success',
                result: response
            }
        );


    });
});


module.exports = router;
