const express = require('express');
const router = express.Router();
const request = require('request');
const _ = require('underscore');
const async = require("async");
const hmacsha1 = require('hmacsha1');
const moment = require('moment');
const querystring = require('querystring');
const Crypto = require("crypto");
const roundTo = require('round-to');
const NumberUtils = require('../../common/numberUtils1');
const AgentService = require('../../common/agentService');
const SlotXOService = require('../../common/slotXOService');
const Joi = require('joi');
const MemberService = require('../../common/memberService');
const DateUtils = require('../../common/dateUtils');
const SlotxoHistoryModel           = require('../../models/slotxoHistory.model');
const ActiveSlotxoTransactionModel           = require('../../models/activeSlotxoTransaction.model');
const AgentGroupModel            = require('../../models/agentGroup.model.js');
const BetTransactionModel = require('../../models/betTransaction.model.js');
const TokenModel = require('../../models/token.model');

// const SLOT_XO_APP_ID = process.env.SLOT_XO_APP_ID || 'TF33';
// const SLOT_XO_HOST = process.env.SLOT_XO_HOST || 'http://api.joker688.net:81';
// const SLOT_XO_SECRET_KEY = process.env.SLOT_XO_SECRET_KEY || 'qc8xtfra7ga56';

const DRAW = "DRAW";
const WIN = "WIN";
const HALF_WIN = "HALF_WIN";
const LOSE = "LOSE";
const HALF_LOSE = "HALF_LOSE";


function random(low, high) {
    return Math.floor(Math.random() * (high - low + 1) + low)
}
function generateBetId() {
    return 'BET' + new Date().getTime() + random(1000000, 9999999);
}

const authenticateSchema = Joi.object().keys({
    appid: Joi.string().required(),
    hash: Joi.string().required(),
    timestamp: Joi.string().required(),
    ip: Joi.string().required(),
    token: Joi.string().required()
});

router.post('/authenticate-token', function (req, res) {
    let validate = Joi.validate(req.body, authenticateSchema);
    if (validate.error) {
        return res.send({
            "Status": 4,
            "Message": "Invalid parameters",
            "Balance": 0,
        })
    }


    let requestBody = req.body;
    console.log('--------------------authenticate-token start request--------------------------')
    let param = `appid=${requestBody.appid}&ip=${requestBody.ip}&timestamp=${requestBody.timestamp}&token=${requestBody.token}`;
    let hash = SlotXOService.buildSignature(param);

    if(!req.body.hash || requestBody.hash != hash){
        return res.send({
            "Status": 5,
            "Message": "Invalid signature",
            "Balance": 0,
        })
    }

    console.log('token == ',requestBody.token);
    TokenModel.findOne({uniqueId: requestBody.token}, function (err, response) {
        console.log('find by token ====== ',response);
        if(response) {

            MemberService.findByUserNameForPartnerService(response.username, (errr, memberResponse) => {
                // console.log('memberResponse ',memberResponse);
                if (memberResponse) {

                    let credit = roundTo((memberResponse.creditLimit + memberResponse.balance), 2);
                    let obj = {
                        Status: 0,
                        Message: 'Success',
                        Balance: credit,
                        Username : response.username
                    };

                    console.log('response == ',obj);
                    console.log('--------------------authenticate-token end request--------------------------', response.username)
                    return res.send(obj);

                } else {

                    return res.send({Status: 1004, Message: "Invalid user id", Balance: 0});
                }

            });
        } else {
            console.log('------error------', err);
            return res.send({
                Status : 3,
                Message : 'Invalid token',
                Balance : 0
            })
        }

    });

});

const getBalanceSchema = Joi.object().keys({
    appid: Joi.string().required(),
    hash: Joi.string().required(),
    timestamp: Joi.string().required(),
    username: Joi.string().required()
});

router.post('/balance', function (req, res) {
    console.log(req.body, '--------------------balance start request--------------------------', req.body.username)
    let validate = Joi.validate(req.body, getBalanceSchema);
    if (validate.error) {
        return res.send({
            "Status": 4,
            "Message": "Invalid parameters",
            "Balance": 0,
        })
    }

    let requestBody = req.body;

    let param = `appid=${requestBody.appid}&timestamp=${requestBody.timestamp}&username=${requestBody.username}`;
    let hash = SlotXOService.buildSignature(param);

    if(!req.body.hash || requestBody.hash != hash){
        return res.send({
            "Status": 5,
            "Message": "Invalid signature",
            "Balance": 0,
        })
    }

    MemberService.findByUserNameForPartnerService(requestBody.username, (err, memberResponse) => {

        if (err) {
            return res.send({Status: 1004, Message: "Invalid user id", Balance: 0});
        }

        if (memberResponse) {

            let credit = roundTo((memberResponse.creditLimit + memberResponse.balance), 2);
            console.log('balance == ', credit)
            console.log('--------------------balance end request--------------------------', requestBody.username)
            return res.send({Status: 0, Message: 'Success', Balance: credit});

        } else {

            return res.send({Status: 1004, Message: "Invalid user id", Balance: 0});
        }

    });
});

const Bet = Joi.object().keys({
    amount: Joi.number().required(),
    appid: Joi.string().required(),
    gamecode: Joi.string().required(),
    hash: Joi.string().required(),
    id: Joi.string().required(),
    timestamp: Joi.string().required(),
    username: Joi.string().required(),
    roundid: Joi.string().required()
});

router.post('/bet', function (req, res) {
    console.log('--------------------bet start request--------------------------', req.body.username)
    let validate = Joi.validate(req.body, Bet);
    if (validate.error) {
        return res.send({
            "Status": 4,
            "Message": "Invalid parameters",
            "Balance": 0,
        })
    }

    let requestBody = req.body;
    let reqAmount = parseFloat(requestBody.amount).toFixed(2);
    let param = `amount=${reqAmount}&appid=${requestBody.appid}&gamecode=${requestBody.gamecode}&id=${requestBody.id}&roundid=${requestBody.roundid}&timestamp=${requestBody.timestamp}&username=${requestBody.username}`;
    let hash = SlotXOService.buildSignature(param);
    if(!req.body.hash || requestBody.hash != hash){
        return res.send({
            "Status": 5,
            "Message": "Invalid signature",
            "Balance": 0,
        })
    }

    let body = {
        amount: requestBody.amount,
        gameCode: requestBody.gamecode,
        roundId: requestBody.roundid,
        hash: requestBody.hash,
        betId: requestBody.id,
        timestamp: requestBody.timestamp,
        username: requestBody.username,
        createdDate: DateUtils.getCurrentDate()
    };
    let model = new SlotxoHistoryModel(body);
    model.save((err, hisRes) => {
        if (err) {
            // console.log('-------save SlotxoHistoryModel fail-------')
        } else {
            // console.log('-------success-------')
            // callback(null, agentResponse);
        }
    });

    let betAmount = roundTo(Number.parseFloat(requestBody.amount), 2);
    if(betAmount < 0){
        return res.send({
            "Status": 4,
            "Message": "Invalid parameters",
            "Balance": 0,
        })
    }
    // console.log('amount = ', betAmount);

    async.series([callback => {

        MemberService.findByUserNameForPartnerService(requestBody.username, function (err, memberResponse) {
            if (err) {
                callback(err, null);
                return;
            }

            if (memberResponse) {
                if (roundTo((memberResponse.creditLimit + memberResponse.balance), 2) < betAmount) {
                    callback(900605, memberResponse);
                } else {
                    callback(null, memberResponse);
                }
            }
        });

    }], (err, asyncResponse) => {

        if (err) {
            if (err == 900404) {
                return res.send({
                    "Status": 1000,
                    "Message": "Member not found",
                    "Balance": 0,
                })
            } else if (err == 900605) {
                let memberInfo = asyncResponse[0];
                let credit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2);

                return res.send({
                    "Status": 100,
                    "Message": "Insufficient fund",
                    "Balance": credit,
                })

            } else {
                return res.send({
                    "Status": 1000,
                    "Message": "Member not found",
                    "Balance": 0,
                })
            }
        }

        let memberInfo = asyncResponse[0];
        // console.log('member info = ',memberInfo);
        let credit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2);

        if(credit < betAmount){
            return res.send({
                "Status": 100,
                "Message": "Insufficient fund",
                "Balance": credit,
            })
        }

        let condition = {
            'memberUsername': requestBody.username.toLowerCase(),
            'slot.xo.roundId': requestBody.roundid
        };

        ActiveSlotxoTransactionModel.findOne(condition, (err, response) => {

            if (response) {

                let txIds = _.map(response.slot.xo.txns, item => {
                    return item.txId;
                });

                const sameTxId = _.intersection(txIds, [requestBody.id]);

                if (sameTxId.length > 0) {
                    return res.send({
                        "Status": 0,
                        "Message": "The Bet already existed",
                        "Balance": credit,
                    })
                }

                let update = {
                    $inc: {
                        'amount': betAmount,
                        "memberCredit": (betAmount * -1),
                        "payout": betAmount
                    },
                    $push: {
                        'slot.xo.txns': {
                            $each: [{
                                txId: requestBody.id,
                                betAmt: requestBody.amount,
                                timestamp: requestBody.timestamp,
                                hash: requestBody.hash,
                            }]
                        }
                    }
                };


                ActiveSlotxoTransactionModel.update({
                    _id: response._id,
                    'slot.xo.txns.txId': {$nin: requestBody.id}
                }, update, (err, updateTranResponse) => {

                    if (updateTranResponse.nModified == 1) {

                        MemberService.updateBalance(memberInfo._id, (betAmount * -1), response.betId, 'SLOT_XO_BET', requestBody.id, function (err, updateBalanceResponse) {
                            if (err) {
                                if (err == 888) {
                                    return res.send({
                                        "Status": 201,
                                        "Message": "Transaction is being processed",
                                        "Balance": credit,
                                    })
                                } else {
                                    return res.send({
                                        "Status": 1000,
                                        "Message": "Internal server error",
                                        "Balance": credit,
                                    })
                                }

                            } else {
                                return res.send({
                                    "Status": 0,
                                    "Message": "Success",
                                    "Balance": updateBalanceResponse.newBalance,
                                })

                            }
                        });
                    } else if (updateTranResponse.ok == 1 && updateTranResponse.nModified == 0) {
                        return res.send({
                            "Status": 201,
                            "Message": "Transaction is being processed",
                            "Balance": credit,
                        })
                    } else {
                        return res.send({
                            "Status": 1000,
                            "Message": "Internal server error",
                            "Balance": credit,
                        })
                    }
                });

            } else {

                AgentGroupModel.findById(memberInfo.group).select('_id type parentId shareSetting commissionSetting').populate({
                    path: 'parentId',
                    select: '_id type parentId shareSetting commissionSetting name',
                    populate: {
                        path: 'parentId',
                        select: '_id type parentId shareSetting commissionSetting name',
                        populate: {
                            path: 'parentId',
                            select: '_id type parentId shareSetting commissionSetting name',
                            populate: {
                                path: 'parentId',
                                select: '_id type parentId shareSetting commissionSetting name',
                                populate: {
                                    path: 'parentId',
                                    select: '_id type parentId shareSetting commissionSetting name',
                                    populate: {
                                        path: 'parentId',
                                        select: '_id type parentId shareSetting commissionSetting name',
                                    }
                                }
                            }
                        }
                    }

                }).exec(function (err, agentGroups) {
                    let body = {
                        memberId: memberInfo._id,
                        memberParentGroup: memberInfo.group,
                        memberUsername: memberInfo.username_lower,
                        betId: SlotXOService.generateBetId(),
                        slot: {
                            xo : {
                                gameCode: requestBody.gamecode,
                                roundId: requestBody.roundid,
                                txns: [
                                    {
                                        txId: requestBody.id,
                                        betAmt: requestBody.amount,
                                        timestamp: requestBody.timestamp,
                                        hash: requestBody.hash,
                                    }
                                ]
                            }
                        },
                        amount: betAmount,
                        memberCredit: betAmount * -1,
                        payout: betAmount,
                        commission: {},
                        status: 'RUNNING',
                        gameDate: DateUtils.getCurrentDate(),
                        createdDate: DateUtils.getCurrentDate(),
                        currency: memberInfo.currency,
                        ipAddress: memberInfo.ipAddress
                    };

                    prepareCommission(memberInfo, body, agentGroups, null, 0, 0);
                    // console.log('body = ', body);
                    let betTransactionModel = new ActiveSlotxoTransactionModel(body);

                    betTransactionModel.save(function (err, response) {

                        if (err) {
                            if (err.code == 11000) {
                                return res.send({
                                    "Status": 201,
                                    "Message": "Transaction is being processed",
                                    "Balance": credit,
                                })
                            }
                            return res.send({
                                "Status": 1000,
                                "Message": "Internal server error",
                                "Balance": credit,
                            })
                        }

                        MemberService.updateBalance(memberInfo._id, (betAmount * -1), response.betId, 'SLOT_XO_BET', requestBody.id, function (err, updateBalanceResponse) {
                            if (err) {
                                return res.send({
                                    "Status": 1000,
                                    "Message": "Internal server error",
                                    "Balance": credit,
                                })
                            } else {
                                console.log('--------------------bet end request--------------------------', memberInfo.username_lower)
                                return res.send({
                                    "Status": 0,
                                    "Message": "Success",
                                    "Balance": updateBalanceResponse.newBalance,
                                })
                            }
                        });
                    });
                });
            }
        });
    });


});

const settle = Joi.object().keys({
    amount: Joi.number().required(),
    appid: Joi.string().required(),
    gamecode: Joi.string().required(),
    hash: Joi.string().required(),
    id: Joi.string().required(),
    roundid: Joi.string().required(),
    timestamp: Joi.string().required(),
    username: Joi.string().required(),
    description: Joi.string().required(),
    type: Joi.string().required()
});

router.post('/settle-bet', function (req, res) {
    let validate = Joi.validate(req.body, settle);
    if (validate.error) {
        return res.send({
            "Status": 4,
            "Message": "Invalid parameters",
            "Balance": 0,
        })
    }

    let requestBody = req.body;

    console.log('--------------------settle start request--------------------------', requestBody.username)
    // console.log('request == ', requestBody);

    let reqAmount = parseFloat(requestBody.amount).toFixed(2);
    let param = `amount=${reqAmount}&appid=${requestBody.appid}&description=${requestBody.description}&gamecode=${requestBody.gamecode}&id=${requestBody.id}&roundid=${requestBody.roundid}&timestamp=${requestBody.timestamp}&type=${requestBody.type}&username=${requestBody.username}`;
    let hash = SlotXOService.buildSignature(param);

    if(!req.body.hash || requestBody.hash != hash){
        return res.send({
            "Status": 5,
            "Message": "Invalid signature",
            "Balance": 0,
        })
    }

    let body = {
        settleId: requestBody.id,
        betId: requestBody.id,
        amount: requestBody.amount,
        gameCode: requestBody.gamecode,
        roundId: requestBody.roundid,
        hash: requestBody.hash,
        username: requestBody.username,
        timestamp: requestBody.timestamp,
        createdDate: DateUtils.getCurrentDate(),
        description: requestBody.description,
        type: requestBody.type
    };

    let model = new SlotxoHistoryModel(body);
    // console.log('save to history = ',model);
    model.save((err, agentResponse) => {
        if (err) {
            // callback(err, null);
        } else {
            // callback(null, agentResponse);
        }
    });

    let amount = roundTo(Number.parseFloat(requestBody.amount), 2);

    // console.log('amount == ', amount);

    if(amount < 0){
        return res.send({
            "Status": 4,
            "Message": "Invalid parameters",
            "Balance": 0,
        })
    }

    async.series([callback => {

        MemberService.findByUserNameForPartnerService(requestBody.username, function (err, memberResponse) {
            // console.log('member info = ', memberResponse);
            if (err) {
                callback(err, null);
                return;
            }
            if (memberResponse) {
                callback(null, memberResponse);
            } else {
                callback(900404, null);
            }
        });

    }], (err, asyncResponse) => {

        if (err) {
            if (err == 900404) {
                return res.send({
                    "Status": 1000,
                    "Message": "Member not found",
                    "Balance": 0,
                })
            } else {
                return res.send({
                    "Status": 1000,
                    "Message": "Internal server error",
                    "Balance": 0,
                })
            }
        }

        let memberInfo = asyncResponse[0];

        let credit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2);
        // console.log('Credit : ', credit);

        async.series([callback => {

            let condition = {
                'memberUsername': requestBody.username.toLowerCase(),
                'slot.xo.roundId': requestBody.roundid
            };

            ActiveSlotxoTransactionModel.findOne(condition).lean().exec(function (err, mainObj) {
                // console.log('active : ',mainObj);
                if (err) {
                    callback(err, null);
                    return
                }

                if (!mainObj) {
                    callback(900415, null);
                } else {

                    if (mainObj.status == 'DONE') {
                        callback(900409)
                    }else if(mainObj.slot.xo.txns.length < 1){
                        callback(900444);
                    } else {

                        // console.log('mainObj ---- ',mainObj);
                        callback(null, mainObj);
                    }
                }

            });

        }], (err, asyncResponse) => {

            let activeBetObj = asyncResponse[0];

            if (err) {
                if (err == 900415) {
                    return res.send({
                        "Status": 1000,
                        "Message": "Transaction can not be found",
                        "Balance": credit,
                    })
                }

                if (err == 900444) {
                    return res.send({
                        "Status": 1000,
                        "Message": "The Bet can not be found - can't settle",
                        "Balance": credit,
                    })
                }

                if (err == 900409) {
                    return res.send({
                        "Status": 0,
                        "Message": "The Bet was settled",
                        "Balance": credit,
                    })
                }
            }

            let validAmount = activeBetObj.amount;
            let winLose = amount - activeBetObj.amount;
            let betResult = winLose > 0 ? 'WIN' : winLose < 0 ? 'LOSE' : 'DRAW';
            // console.log(amount, 'bet result --- ',betResult);
            const shareReceive = AgentService.prepareShareReceive(winLose, betResult, activeBetObj);

            // updateAgentMemberBalance(shareReceive, amount, requestBody.id, function (err, updateBalanceResponse) {
            updateAgentMemberBalance(shareReceive, activeBetObj.amount, requestBody.id, credit, function (err, updateBalanceResponse) {

                if (err) {
                    if (err == 888) {
                        return res.send({
                            "Status": 201,
                            "Message": "Transaction is being processed",
                            "Balance": credit,
                        })
                    }
                    return res.send({
                        "Status": 1000,
                        "Message": "Internal server error",
                        "Balance": 0,
                    })
                } else {

                    let updateBody = {
                        $set: {

                            'slot.xo.settleId': requestBody.id,
                            'slot.xo.winLoss': requestBody.amount,
                            'slot.xo.description': requestBody.description,
                            'slot.xo.type': requestBody.type,
                            'commission.superAdmin.winLoseCom': shareReceive.superAdmin.winLoseCom,
                            'commission.superAdmin.winLose': shareReceive.superAdmin.winLose,
                            'commission.superAdmin.totalWinLoseCom': shareReceive.superAdmin.totalWinLoseCom,

                            'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                            'commission.company.winLose': shareReceive.company.winLose,
                            'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                            'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                            'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                            'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                            'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                            'commission.senior.winLose': shareReceive.senior.winLose,
                            'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                            'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                            'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                            'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                            'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                            'commission.agent.winLose': shareReceive.agent.winLose,
                            'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                            'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                            'commission.member.winLose': shareReceive.member.winLose,
                            'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                            'validAmount': amount,
                            'betResult': betResult,
                            'isEndScore': true,
                            'status': 'DONE'
                        }
                    };

                    // console.log('activexo ======= ',updateBody);
                    ActiveSlotxoTransactionModel.update({_id: activeBetObj._id}, updateBody, function (err, response) {

                        if (response.nModified == 1) {

                            //TODO queue process
                            let body = {
                                memberId: activeBetObj.memberId,
                                memberParentGroup: activeBetObj.memberParentGroup,
                                memberUsername: activeBetObj.memberUsername,
                                betId: activeBetObj.betId,
                                slot: {xo: activeBetObj.slot.xo},
                                amount: activeBetObj.amount,
                                memberCredit: activeBetObj.amount * -1,
                                payout: activeBetObj.amount,
                                gameType: 'TODAY',
                                acceptHigherPrice: false,
                                priceType: 'TH',
                                game: 'GAME',
                                source: 'SLOT_XO',
                                status: 'DONE',
                                commission: activeBetObj.commission,
                                gameDate: DateUtils.getCurrentDate(),
                                createdDate: DateUtils.getCurrentDate(),
                                validAmount: activeBetObj.amount,
                                betResult: betResult,
                                isEndScore: true,
                                currency: memberInfo.currency,
                                ipAddress: memberInfo.ipAddress
                            };

                            body.commission.superAdmin.winLoseCom = shareReceive.superAdmin.winLoseCom;
                            body.commission.superAdmin.winLose = shareReceive.superAdmin.winLose;
                            body.commission.superAdmin.totalWinLoseCom = shareReceive.superAdmin.totalWinLoseCom;

                            body.commission.company.winLoseCom = shareReceive.company.winLoseCom;
                            body.commission.company.winLose = shareReceive.company.winLose;
                            body.commission.company.totalWinLoseCom = shareReceive.company.totalWinLoseCom;

                            body.commission.shareHolder.winLoseCom = shareReceive.shareHolder.winLoseCom;
                            body.commission.shareHolder.winLose = shareReceive.shareHolder.winLose;
                            body.commission.shareHolder.totalWinLoseCom = shareReceive.shareHolder.totalWinLoseCom;

                            body.commission.senior.winLoseCom = shareReceive.senior.winLoseCom;
                            body.commission.senior.winLose = shareReceive.senior.winLose;
                            body.commission.senior.totalWinLoseCom = shareReceive.senior.totalWinLoseCom;

                            body.commission.masterAgent.winLoseCom = shareReceive.masterAgent.winLoseCom;
                            body.commission.masterAgent.winLose = shareReceive.masterAgent.winLose;
                            body.commission.masterAgent.totalWinLoseCom = shareReceive.masterAgent.totalWinLoseCom;

                            body.commission.agent.winLoseCom = shareReceive.agent.winLoseCom;
                            body.commission.agent.winLose = shareReceive.agent.winLose;
                            body.commission.agent.totalWinLoseCom = shareReceive.agent.totalWinLoseCom;

                            body.commission.member.winLoseCom = shareReceive.member.winLoseCom;
                            body.commission.member.winLose = shareReceive.member.winLose;
                            body.commission.member.totalWinLoseCom = shareReceive.member.totalWinLoseCom;

                            // body.slot.xo.winLose = requestBody.amount;
                            body.slot.xo.settleId = requestBody.id;
                            body.slot.xo.description = requestBody.description;
                            body.slot.xo.gameName =  SlotXOService.getGameName(requestBody.gamecode)
                            body.slot.xo.type = requestBody.type;
                            body.settleDate = DateUtils.getCurrentDate();
                            body.updatedDate = DateUtils.getCurrentDate();

                            let betTransactionModel = new BetTransactionModel(body);
                            betTransactionModel.save((err, betObj) => {
                                if (err) {
                                    if (err.code == 11000) {
                                        return res.send({
                                            "Status": 201,
                                            "Message": "Transaction is being processed",
                                            "Balance": credit,
                                        })
                                    }

                                    return res.send({
                                        "Status": 1000,
                                        "Message": "Internal server error",
                                        "Balance": credit,
                                    })
                                } else {
                                    // SlotxoService.sendToQueue(activeBetObj.betId, function (queErr, queRes) {
                                    console.log('--------------------settle end request--------------------------',memberInfo.username_lower)
                                    return res.send({
                                        "Status": 0,
                                        "Message": "Success",
                                        "Balance": updateBalanceResponse.newBalance,
                                    })
                                    // });

                                }
                            });

                        } else {
                            return res.send({
                                "Status": 1000,
                                "Message": "Internal server error",
                                "Balance": credit,
                            })
                        }
                    });
                }
            });

        });
    });
});

const cancelBetSchema = Joi.object().keys({
    appid: Joi.string().required(),
    gamecode: Joi.string().required(),
    hash: Joi.string().required(),
    timestamp: Joi.string().required(),
    id: Joi.string().required(),
    betid: Joi.string().required(),
    roundid: Joi.string().required(),
    username: Joi.string().required()
});

router.post('/cancel-bet', function (req, res) {
    let validate = Joi.validate(req.body, cancelBetSchema);
    if (validate.error) {
        return res.send({
            "Status": 4,
            "Message": "Invalid parameters",
            "Balance": 0,
        })
    }

    let requestBody = req.body;

    let param = `appid=${requestBody.appid}&betid=${requestBody.betid}&gamecode=${requestBody.gamecode}&id=${requestBody.id}&roundid=${requestBody.roundid}&timestamp=${requestBody.timestamp}&username=${requestBody.username}`;
    let hash = SlotXOService.buildSignature(param);

    if(!req.body.hash || requestBody.hash != hash){
        return res.send({
            "Status": 5,
            "Message": "Invalid signature",
            "Balance": 0,
        })
    }

    let body = {
        gameCode: requestBody.gamecode,
        roundId: requestBody.roundid,
        hash: requestBody.hash,
        betId: requestBody.id,
        timestamp: requestBody.timestamp,
        username: requestBody.username,
        createdDate: DateUtils.getCurrentDate(),
        cancelId : requestBody.id
    };

    let model = new SlotxoHistoryModel(body);
    model.save((err, agentResponse) => {

        if (err) {
            // callback(err, null);
        } else {
            // callback(null, agentResponse);
        }
    });

    async.series([callback => {

        MemberService.findByUserNameForPartnerService(requestBody.username, function (err, memberResponse) {
            if (err) {
                callback(err, null);
                return;
            }
            if (memberResponse) {
                callback(null, memberResponse);
            } else {
                callback(900404, null);
            }
        });

    }], (err, asyncResponse) => {

        if (err) {
            if (err == 900404) {
                return res.send({
                    "Status": 1000,
                    "Message": "Member not found",
                    "Balance": 0,
                })
            } else {
                return res.send({
                    "Status": 1000,
                    "Message": "Internal server error",
                    "Balance": 0,
                })
            }
        }


        let memberInfo = asyncResponse[0];
        let credit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2);


        let condition = {
            'memberUsername': requestBody.username.toLowerCase(),
            'slot.xo.gameCode': requestBody.gamecode,
            'slot.xo.roundId': requestBody.roundid,
            'slot.xo.txns.txId': requestBody.betid,
        };

        ActiveSlotxoTransactionModel.findOne(condition).exec(function (err, activeXoResponse) {

            if (err) {
                return res.send({
                    "Status": 1000,
                    "Message": "Internal server error",
                    "Balance": 0,
                })
            } else {

                if (!activeXoResponse) {
                    return res.send({
                        "Status": 0,
                        "Message": "The CancelBet already existed",
                        "Balance": credit,
                    });
                }else{
                    // console.log('--------------',activeXoResponse);
                    if(activeXoResponse.status === 'DONE'){
                        //Bet is settled
                        BetTransactionModel.findOne(condition).exec(function (betErr, betRes) {
                            if(betErr){
                                return res.send({
                                    "Status": 1000,
                                    "Message": "Bet id not found",
                                    "Balance": credit,
                                });
                            }else {
                                if(betRes){
                                    // console.log('--------res------',betRes);
                                    if(betRes.slot.xo.txns.length > 1){
                                        //ยกเลิกได้
                                        let betAmount = 0;
                                        for(let i=0; i< betRes.slot.xo.txns.length; i++){
                                            if(betRes.slot.xo.txns[i].txId === requestBody.betid){
                                                betAmount = roundTo((betRes.slot.xo.txns[i].betAmt), 2);
                                                // console.log('betamount  ',betAmount);
                                            }
                                        }

                                        // console.log('cancel amount ====', betAmount);

                                        let updateCondition = {
                                            $pull: {'slot.xo.txns': {"txId": requestBody.betid}},
                                            $inc: {
                                                amount: betAmount * -1,
                                                memberCredit: betAmount
                                            },
                                            'updatedDate': DateUtils.getCurrentDate()

                                        };

                                        BetTransactionModel.update({
                                            _id: betRes._id,
                                            'slot.xo.txns.txId': requestBody.betid
                                        }, updateCondition, {safe: true}, function (err, cancelResponse) {
                                            // console.log('err ==== ',err);
                                            if (err) {
                                                return res.send({
                                                    "Status": 1000,
                                                    "Message": "Internal server error",
                                                    "Balance": 0,
                                                })
                                            }

                                            if (cancelResponse.nModified == 1) {

                                                MemberService.updateBalance(betRes.memberId, betAmount , betRes.betId, 'SLOT_XO_CANCEL', requestBody.betid, function (err, updateBalanceResponse) {
                                                    if (err) {
                                                        if (err == 888) {
                                                            return res.send({
                                                                "Status": 201,
                                                                "Message": "Transaction is being processed",
                                                                "Balance": credit,
                                                            })
                                                        }

                                                        return res.send({
                                                            "Status": 1000,
                                                            "Message": "Internal server error",
                                                            "Balance": 0,
                                                        })
                                                    } else {
                                                        // SlotxoService.sendToQueue(betRes.betId, function (queErr, queRes) {
                                                        return res.send({
                                                            "Status": 0,
                                                            "Message": "Success",
                                                            "Balance": updateBalanceResponse.newBalance,
                                                        })
                                                        // })
                                                    }
                                                });
                                            }
                                        });
                                    }else {
                                        return res.send({
                                            "Status": 1000,
                                            "Message": "Bet was settled",
                                            "Balance": credit,
                                        });
                                    }
                                }else {
                                    return res.send({
                                        "Status": 1000,
                                        "Message": "Bet id not found",
                                        "Balance": credit,
                                    });
                                }
                            }
                        });
                    }else {
                        //Bet is running
                        // console.log('ddddd====',activeXoResponse.slot.xo.txns);
                        if(activeXoResponse.slot.xo.txns.length > 0){
                            //ยกเลิกได้
                            let betAmount = 0;
                            for(let i=0; i< activeXoResponse.slot.xo.txns.length; i++){
                                // console.log(requestBody.betid, '----------------------',activeXoResponse.slot.xo.txns[i].txId);
                                if(activeXoResponse.slot.xo.txns[i].txId === requestBody.betid){
                                    betAmount = roundTo((activeXoResponse.slot.xo.txns[i].betAmt), 2);
                                    // return;
                                }
                            }

                            // console.log('cancel active amount ====', betAmount);

                            let updateCondition = {
                                $pull: {'slot.xo.txns': {"txId": requestBody.betid}},
                                $inc: {
                                    amount: betAmount * -1,
                                    memberCredit: betAmount
                                }
                            };

                            ActiveSlotxoTransactionModel.update({
                                _id: activeXoResponse._id,
                                'slot.xo.txns.txId': requestBody.betid
                            }, updateCondition, {safe: true}, function (err, cancelResponse) {
                                // console.log('err ==== ',err);
                                if (err) {
                                    return res.send({
                                        "Status": 1000,
                                        "Message": "Internal server error",
                                        "Balance": 0,
                                    })
                                }

                                if (cancelResponse.nModified == 1) {

                                    MemberService.updateBalance(activeXoResponse.memberId, betAmount , activeXoResponse.betId, 'SLOT_XO_CANCEL', requestBody.betid, function (err, updateBalanceResponse) {
                                        if (err) {
                                            if (err == 888) {
                                                return res.send({
                                                    "Status": 201,
                                                    "Message": "Transaction is being processed",
                                                    "Balance": credit,
                                                })
                                            }

                                            return res.send({
                                                "Status": 1000,
                                                "Message": "Internal server error",
                                                "Balance": 0,
                                            })
                                        } else {
                                            return res.send({
                                                "Status": 0,
                                                "Message": "Success",
                                                "Balance": updateBalanceResponse.newBalance,
                                            })

                                        }
                                    });
                                }
                            });
                        }else {
                            return res.send({
                                "Status": 0,
                                "Message": "The CancelBet already existed",
                                "Balance": credit,
                            });
                        }

                    }
                }
            }
        });
    });
});

const forwardGame = Joi.object().keys({
    gameCode: Joi.string().required(),
    language: Joi.string().allow(''),
    isMobile: Joi.string().allow(''),
    redirectUrl: Joi.string().allow('')
});

router.post('/forward-to-game', function (req, res) {
    let validate = Joi.validate(req.body, forwardGame);
    if (validate.error) {
        return res.send({
            code : 999,
            message : 'Invalid parameters',
            result : ''
        })
    }
    const userInfo       = req.userInfo;
    let requestBody = req.body;

    async.waterfall([callback => {
        try {
            AgentService.getUpLineStatus(userInfo.group._id, function (err, status) {
                if (err) {
                    callback(err, null);
                } else {
                    if (status.suspend) {
                        callback(5011, null);
                    } else {
                        callback(null, status);
                    }
                }
            });
        } catch (err) {
            callback(9929, null);
        }

    }, (uplineStatus, callback) => {

        try {
            MemberService.findById(userInfo._id, 'creditLimit balance limitSetting group suspend username_lower', function (err, memberResponse) {
                if (err) {
                    callback(err, null);
                } else {
                    console.log('is enable ================================ ',memberResponse.limitSetting.game.slotXO.isEnable);
                    if (memberResponse.suspend) {
                        callback(5011, null);
                    } else if (memberResponse.limitSetting.game.slotXO.isEnable) {
                        AgentService.getLive22Status(memberResponse.group._id, (err, response) => {
                            if (!response.isEnable) {
                                callback(1088, null);
                            } else {
                                callback(null, uplineStatus, memberResponse);
                            }
                        });
                    } else {
                        callback(1088, null);
                    }
                }
            });
        } catch (err) {
            callback(999, null);
        }

    }], function (err, uplineStatus, memberInfo) {
        console.log('-----err------------',err);
        if (err) {
            if (err == 1088) {
                return res.send({
                    code: 1088,
                    message: 'Service Locked, Please contact your upline.'
                });
            }
            if (err == 5011) {
                return res.send({
                    message: "Account or Upline was suspend.",
                    result: null,
                    code: 5011
                });
            }
            return res.send({code: 999, message: err});
        }

        TokenModel.findOne({username: userInfo.username}, function (err, response) {

            if(response) {
                SlotXOService.forwardToGame(response.uniqueId, requestBody.gameCode, requestBody.language, requestBody.redirectUrl, requestBody.isMobile, (err, result) =>{
                    console.log('---------forwardToGame----------',result);
                    return res.send({
                        code : 0,
                        message : 'success',
                        result : result
                    })
                })
            } else {
                return res.send({
                    code : 1,
                    message : 'Invalid token',
                    result : ''
                })
            }

        });

    });

});

const jackpot = Joi.object().keys({
    amount: Joi.number().required(),
    appid: Joi.string().required(),
    gamecode: Joi.string().required(),
    hash: Joi.string().required(),
    id: Joi.string().required(),
    roundid: Joi.string().required(),
    timestamp: Joi.string().required(),
    username: Joi.string().required(),
    description: Joi.string().required(),
    type: Joi.string().required()
});

router.post('/jackpot-win', function (req, res) {
    let validate = Joi.validate(req.body, jackpot);
    if (validate.error) {
        return res.send({
            "Status": 4,
            "Message": "Invalid parameters",
            "Balance": 0,
        })
    }

    let requestBody = req.body;

    let reqAmount = parseFloat(requestBody.amount).toFixed(2);
    let param = `amount=${reqAmount}&appid=${requestBody.appid}&description=${requestBody.description}&gamecode=${requestBody.gamecode}&id=${requestBody.id}&roundid=${requestBody.roundid}&timestamp=${requestBody.timestamp}&type=${requestBody.type}&username=${requestBody.username}`;
    let hash = SlotXOService.buildSignature(param);

    if(!req.body.hash || requestBody.hash != hash){
        return res.send({
            "Status": 5,
            "Message": "Invalid signature",
            "Balance": 0,
        })
    }

    console.log('bonus-win request == ', requestBody);

    let body = {
        settleId: requestBody.id,
        betId: requestBody.id,
        amount: requestBody.amount,
        gameCode: requestBody.gamecode,
        roundId: requestBody.roundid,
        hash: requestBody.hash,
        username: requestBody.username,
        timestamp: requestBody.timestamp,
        createdDate: DateUtils.getCurrentDate(),
        description: requestBody.description,
        type: requestBody.type
    };

    let model = new SlotxoHistoryModel(body);
    // console.log('save to history = ',model);
    model.save((err, agentResponse) => {
        if (err) {
            // callback(err, null);
        } else {
            // callback(null, agentResponse);
        }
    });


    let betAmount = 0;

    let amount = roundTo(Number.parseFloat(requestBody.amount), 2);

    if(amount < 0){
        return res.send({
            "Status": 4,
            "Message": "Invalid parameters",
            "Balance": 0,
        })
    }

    async.series([callback => {

        MemberService.findByUserNameForPartnerService(requestBody.username, function (err, memberResponse) {
            // console.log('member info = ', memberResponse);
            if (err) {
                callback(err, null);
                return;
            }
            if (memberResponse) {
                callback(null, memberResponse);
            } else {
                callback(900404, null);
            }
        });

    }], (err, asyncResponse) => {

        if (err) {
            if (err == 900404) {
                return res.send({
                    "Status": 1000,
                    "Message": "Member not found",
                    "Balance": 0,
                })
            } else {
                return res.send({
                    "Status": 1000,
                    "Message": "Internal server error",
                    "Balance": 0,
                })
            }
        }

        let memberInfo = asyncResponse[0];

        let credit = roundTo(roundTo(memberInfo.creditLimit, 2) + roundTo(memberInfo.balance, 2), 2);

        AgentGroupModel.findById(memberInfo.group).select('_id type parentId shareSetting commissionSetting').populate({
            path: 'parentId',
            select: '_id type parentId shareSetting commissionSetting name',
            populate: {
                path: 'parentId',
                select: '_id type parentId shareSetting commissionSetting name',
                populate: {
                    path: 'parentId',
                    select: '_id type parentId shareSetting commissionSetting name',
                    populate: {
                        path: 'parentId',
                        select: '_id type parentId shareSetting commissionSetting name',
                        populate: {
                            path: 'parentId',
                            select: '_id type parentId shareSetting commissionSetting name',
                            populate: {
                                path: 'parentId',
                                select: '_id type parentId shareSetting commissionSetting name',
                            }
                        }
                    }
                }
            }

        }).exec(function (err, agentGroups) {

            let condition = {
                memberUsername : requestBody.username.toLowerCase(),
                'slot.xo.settleId' : requestBody.id,
                'slot.xo.gameCode' : requestBody.gamecode,
                'slot.xo.roundId' : requestBody.roundid
            };

            BetTransactionModel.findOne(condition).exec(function (err, responseBonus) {
                if(responseBonus){
                    return res.send({
                        "Balance":credit,
                        "Message":"Success",
                        "Status":0
                    });
                }else {
                    let body = {
                        memberId: memberInfo._id,
                        memberParentGroup: memberInfo.group,
                        memberUsername: memberInfo.username_lower,
                        betId: SlotXOService.generateBetId(),
                        slot: {
                            xo: {
                                gameCode: requestBody.gamecode,
                                gameName: SlotXOService.getGameName(requestBody.gamecode),
                                settleId : requestBody.id,
                                roundId: requestBody.roundid,
                                txns: [{
                                    betAmt: 0,
                                    timestamp: requestBody.timestamp,
                                    hash: requestBody.hash,
                                }],
                                winLoss : requestBody.amount,
                                description : requestBody.description,
                                type : requestBody.type
                            }
                        },
                        amount: betAmount,
                        memberCredit: betAmount * -1,
                        payout: 0,
                        validAmount: 0,
                        gameType: 'TODAY',
                        acceptHigherPrice: false,
                        priceType: 'TH',
                        game: 'GAME',
                        source: 'SLOT_XO',
                        status: 'DONE',
                        commission: {},
                        gameDate: DateUtils.getCurrentDate(),
                        createdDate: DateUtils.getCurrentDate(),
                        isEndScore: false,
                        currency: memberInfo.currency,
                        ipAddress: memberInfo.ipAddress,
                        settleDate :DateUtils.getCurrentDate()
                    };

                    prepareCommission(memberInfo, body, agentGroups, null, 0, 0)

                    let winLose = amount;
                    let betResult = winLose > 0 ? 'WIN' : winLose < 0 ? 'LOSE' : 'DRAW';

                    //init
                    if (!body.commission.senior) {
                        body.commission.senior = {};
                    }

                    if (!body.commission.masterAgent) {
                        body.commission.masterAgent = {};
                    }

                    if (!body.commission.agent) {
                        body.commission.agent = {};
                    }

                    // console.log('body === ',body);
                    const shareReceive = AgentService.prepareShareReceive(winLose, betResult, body);


                    body.commission.superAdmin.winLoseCom = shareReceive.superAdmin.winLoseCom;
                    body.commission.superAdmin.winLose = shareReceive.superAdmin.winLose;
                    body.commission.superAdmin.totalWinLoseCom = shareReceive.superAdmin.totalWinLoseCom;

                    body.commission.company.winLoseCom = shareReceive.company.winLoseCom;
                    body.commission.company.winLose = shareReceive.company.winLose;
                    body.commission.company.totalWinLoseCom = shareReceive.company.totalWinLoseCom;

                    body.commission.shareHolder.winLoseCom = shareReceive.shareHolder.winLoseCom;
                    body.commission.shareHolder.winLose = shareReceive.shareHolder.winLose;
                    body.commission.shareHolder.totalWinLoseCom = shareReceive.shareHolder.totalWinLoseCom;

                    body.commission.senior.winLoseCom = shareReceive.senior.winLoseCom;
                    body.commission.senior.winLose = shareReceive.senior.winLose;
                    body.commission.senior.totalWinLoseCom = shareReceive.senior.totalWinLoseCom;

                    body.commission.masterAgent.winLoseCom = shareReceive.masterAgent.winLoseCom;
                    body.commission.masterAgent.winLose = shareReceive.masterAgent.winLose;
                    body.commission.masterAgent.totalWinLoseCom = shareReceive.masterAgent.totalWinLoseCom;

                    body.commission.agent.winLoseCom = shareReceive.agent.winLoseCom;
                    body.commission.agent.winLose = shareReceive.agent.winLose;
                    body.commission.agent.totalWinLoseCom = shareReceive.agent.totalWinLoseCom;

                    body.commission.member.winLoseCom = shareReceive.member.winLoseCom;
                    body.commission.member.winLose = shareReceive.member.winLose;
                    body.commission.member.totalWinLoseCom = shareReceive.member.totalWinLoseCom;
                    body.betResult = betResult;

                    let betTransactionModel = new BetTransactionModel(body);

                    betTransactionModel.save(function (err, response) {

                        if (err) {
                            if (err.code == 11000) {
                                return res.send({
                                    "Status": 201,
                                    "Message": "Transaction is being processed",
                                    "Balance": credit,
                                })
                            }
                            return res.send({
                                "Status": 1000,
                                "Message": "Internal server error",
                                "Balance": credit,
                            })
                        }

                        MemberService.updateBalance(memberInfo._id, amount, response.betId, 'SLOT_XO_JACKPOT', requestBody.id, function (err, updateBalanceResponse) {
                            if (err) {
                                return res.send({
                                    "Status": 1000,
                                    "Message": "Internal server error",
                                    "Balance": credit,
                                })
                            } else {
                                // SlotxoService.sendToQueue(response.betId, function (queErr, queRes) {
                                return res.send({
                                    "Balance": updateBalanceResponse.newBalance,
                                    "Message": "Success",
                                    "Status": 0
                                });
                                // });
                            }
                        });
                    });

                }

            });



        });

    });
});


//request = { amount: '4.00',
// appid: 'TFB8',
// description: '5m6k9j7rwspjs',
// gamecode: '5m6k9j7rwspjs',
// hash: '8f4e01d8e77188795af5126788ec0826',
// id: 'W-xkbhmp4a788pc',
// roundid: 'xkbhmp4a788pc',
// timestamp: '1572336013758',
// type: '5m6k9j7rwspjs',
// username: 'SHOW5' }

const bonus = Joi.object().keys({
    amount: Joi.number().required(),
    appid: Joi.string().required(),
    gamecode: Joi.string().required(),
    hash: Joi.string().required(),
    id: Joi.string().required(),
    roundid: Joi.string().required(),
    timestamp: Joi.string().required(),
    username: Joi.string().required(),
    description: Joi.string().required(),
    type: Joi.string().required()
});


router.post('/bonus-win', function (req, res) {
    let validate = Joi.validate(req.body, bonus);
    if (validate.error) {
        return res.send({
            "Status": 4,
            "Message": "Invalid parameters",
            "Balance": 0,
        })
    }

    let requestBody = req.body;
    // console.log('bonus-win request == ', requestBody);

    let reqAmount = parseFloat(requestBody.amount).toFixed(2);
    let param = `amount=${reqAmount}&appid=${requestBody.appid}&description=${requestBody.description}&gamecode=${requestBody.gamecode}&id=${requestBody.id}&roundid=${requestBody.roundid}&timestamp=${requestBody.timestamp}&type=${requestBody.type}&username=${requestBody.username}`;
    let hash = SlotXOService.buildSignature(param);
    // console.log(hash);
    if(!req.body.hash || requestBody.hash != hash){
        return res.send({
            "Status": 5,
            "Message": "Invalid signature",
            "Balance": 0,
        })
    }

    console.log('bonus-win request == ', requestBody);

    let body = {
        settleId: requestBody.id,
        betId: requestBody.id,
        amount: requestBody.amount,
        gameCode: requestBody.gamecode,
        roundId: requestBody.roundid,
        hash: requestBody.hash,
        username: requestBody.username,
        timestamp: requestBody.timestamp,
        createdDate: DateUtils.getCurrentDate(),
        description: requestBody.description,
        type: requestBody.type
    };

    let model = new SlotxoHistoryModel(body);
    // console.log('save to history = ',model);
    model.save((err, agentResponse) => {
        if (err) {
            // callback(err, null);
        } else {
            // callback(null, agentResponse);
        }
    });

    let betAmount = 0;
    let amount = roundTo(Number.parseFloat(requestBody.amount), 2);

    if(amount < 0){
        return res.send({
            "Status": 4,
            "Message": "Invalid parameters",
            "Balance": 0,
        })
    }

    async.series([callback => {

        MemberService.findByUserNameForPartnerService(requestBody.username, function (err, memberResponse) {
            // console.log('member info = ', memberResponse);
            if (err) {
                callback(err, null);
                return;
            }
            if (memberResponse) {
                callback(null, memberResponse);
            } else {
                callback(900404, null);
            }
        });

    }], (err, asyncResponse) => {

        if (err) {
            if (err == 900404) {
                return res.send({
                    "Status": 1000,
                    "Message": "Member not found",
                    "Balance": 0,
                })
            } else {
                return res.send({
                    "Status": 1000,
                    "Message": "Internal server error",
                    "Balance": 0,
                })
            }
        }

        let memberInfo = asyncResponse[0];

        let credit = roundTo(roundTo(memberInfo.creditLimit, 2) + roundTo(memberInfo.balance, 2), 2);

        AgentGroupModel.findById(memberInfo.group).select('_id type parentId shareSetting commissionSetting').populate({
            path: 'parentId',
            select: '_id type parentId shareSetting commissionSetting name',
            populate: {
                path: 'parentId',
                select: '_id type parentId shareSetting commissionSetting name',
                populate: {
                    path: 'parentId',
                    select: '_id type parentId shareSetting commissionSetting name',
                    populate: {
                        path: 'parentId',
                        select: '_id type parentId shareSetting commissionSetting name',
                        populate: {
                            path: 'parentId',
                            select: '_id type parentId shareSetting commissionSetting name',
                            populate: {
                                path: 'parentId',
                                select: '_id type parentId shareSetting commissionSetting name',
                            }
                        }
                    }
                }
            }

        }).exec(function (err, agentGroups) {

            let condition = {
                memberUsername : requestBody.username.toLowerCase(),
                'slot.xo.settleId' : requestBody.id,
                'slot.xo.gameCode' : requestBody.gamecode,
                'slot.xo.roundId' : requestBody.roundid
            };

            BetTransactionModel.findOne(condition).exec(function (err, responseBonus) {
                if(responseBonus){
                    return res.send({
                        "Balance":credit,
                        "Message":"Success",
                        "Status":0
                    });
                }else {
                    let body = {
                        memberId: memberInfo._id,
                        memberParentGroup: memberInfo.group,
                        memberUsername: memberInfo.username_lower,
                        betId: SlotXOService.generateBetId(),
                        slot: {
                            xo: {
                                gameCode: requestBody.gamecode,
                                gameName: SlotXOService.getGameName(requestBody.gamecode),
                                settleId : requestBody.id,
                                roundId: requestBody.roundid,
                                txns: [{
                                    betAmt: 0,
                                    timestamp: requestBody.timestamp,
                                    hash: requestBody.hash,
                                }],
                                winLoss : requestBody.amount,
                                description : requestBody.description,
                                type : requestBody.type
                            }
                        },
                        amount: betAmount,
                        memberCredit: betAmount * -1,
                        payout: 0,
                        validAmount: 0,
                        gameType: 'TODAY',
                        acceptHigherPrice: false,
                        priceType: 'TH',
                        game: 'GAME',
                        source: 'SLOT_XO',
                        status: 'DONE',
                        commission: {},
                        gameDate: DateUtils.getCurrentDate(),
                        createdDate: DateUtils.getCurrentDate(),
                        isEndScore: false,
                        currency: memberInfo.currency,
                        ipAddress: memberInfo.ipAddress,
                        settleDate :DateUtils.getCurrentDate()
                    };

                    prepareCommission(memberInfo, body, agentGroups, null, 0, 0)

                    let winLose = amount;
                    let betResult = winLose > 0 ? 'WIN' : winLose < 0 ? 'LOSE' : 'DRAW';

                    //init
                    if (!body.commission.senior) {
                        body.commission.senior = {};
                    }

                    if (!body.commission.masterAgent) {
                        body.commission.masterAgent = {};
                    }

                    if (!body.commission.agent) {
                        body.commission.agent = {};
                    }

                    // console.log('body === ',body);
                    const shareReceive = AgentService.prepareShareReceive(winLose, betResult, body);


                    body.commission.superAdmin.winLoseCom = shareReceive.superAdmin.winLoseCom;
                    body.commission.superAdmin.winLose = shareReceive.superAdmin.winLose;
                    body.commission.superAdmin.totalWinLoseCom = shareReceive.superAdmin.totalWinLoseCom;

                    body.commission.company.winLoseCom = shareReceive.company.winLoseCom;
                    body.commission.company.winLose = shareReceive.company.winLose;
                    body.commission.company.totalWinLoseCom = shareReceive.company.totalWinLoseCom;

                    body.commission.shareHolder.winLoseCom = shareReceive.shareHolder.winLoseCom;
                    body.commission.shareHolder.winLose = shareReceive.shareHolder.winLose;
                    body.commission.shareHolder.totalWinLoseCom = shareReceive.shareHolder.totalWinLoseCom;

                    body.commission.senior.winLoseCom = shareReceive.senior.winLoseCom;
                    body.commission.senior.winLose = shareReceive.senior.winLose;
                    body.commission.senior.totalWinLoseCom = shareReceive.senior.totalWinLoseCom;

                    body.commission.masterAgent.winLoseCom = shareReceive.masterAgent.winLoseCom;
                    body.commission.masterAgent.winLose = shareReceive.masterAgent.winLose;
                    body.commission.masterAgent.totalWinLoseCom = shareReceive.masterAgent.totalWinLoseCom;

                    body.commission.agent.winLoseCom = shareReceive.agent.winLoseCom;
                    body.commission.agent.winLose = shareReceive.agent.winLose;
                    body.commission.agent.totalWinLoseCom = shareReceive.agent.totalWinLoseCom;

                    body.commission.member.winLoseCom = shareReceive.member.winLoseCom;
                    body.commission.member.winLose = shareReceive.member.winLose;
                    body.commission.member.totalWinLoseCom = shareReceive.member.totalWinLoseCom;
                    body.betResult = betResult;

                    let betTransactionModel = new BetTransactionModel(body);

                    betTransactionModel.save(function (err, response) {

                        if (err) {
                            if (err.code == 11000) {
                                return res.send({
                                    "Status": 201,
                                    "Message": "Transaction is being processed",
                                    "Balance": credit,
                                })
                            }
                            return res.send({
                                "Status": 1000,
                                "Message": "Internal server error",
                                "Balance": credit,
                            })
                        }

                        MemberService.updateBalance(memberInfo._id, amount, response.betId, 'SLOT_XO_BONUS', requestBody.id, function (err, updateBalanceResponse) {
                            if (err) {
                                return res.send({
                                    "Status": 1000,
                                    "Message": "Internal server error",
                                    "Balance": credit,
                                })
                            } else {
                                // SlotxoService.sendToQueue(response.betId, function (queErr, queResult) {
                                return res.send({
                                    "Balance":updateBalanceResponse.newBalance,
                                    "Message":"Success",
                                    "Status":0
                                });
                                // })
                            }
                        });
                    });

                }

            });

        });

    });
});

const transaction = Joi.object().keys({
    amount: Joi.number().required(),
    appid: Joi.string().required(),
    gamecode: Joi.string().required(),
    hash: Joi.string().required(),
    id: Joi.string().required(),
    roundid: Joi.string().required(),
    timestamp: Joi.string().required(),
    username: Joi.string().required(),
    description: Joi.string().required(),
    startbalance: Joi.string().required(),
    endbalance: Joi.string().required(),
    result: Joi.string().required(),
    type: Joi.string().required()
});


router.post('/transaction', function (req, res) {
    let validate = Joi.validate(req.body, transaction);
    if (validate.error) {
        return res.send({
            "Status": 4,
            "Message": "Invalid parameters",
            "Balance": 0,
        })
    }

    let requestBody = req.body;
    // console.log('transaction request == ', requestBody);

    let reqAmount = parseFloat(requestBody.amount).toFixed(2);
    let reqResult = parseFloat(requestBody.result).toFixed(2);
    let reqStart = parseFloat(requestBody.startbalance).toFixed(2);
    let reqEnd = parseFloat(requestBody.endbalance).toFixed(2);
    let param = `amount=${reqAmount}&appid=${requestBody.appid}&description=${requestBody.description}&endbalance=${reqEnd}&gamecode=${requestBody.gamecode}&id=${requestBody.id}&result=${reqResult}&roundid=${requestBody.roundid}&startbalance=${reqStart}&timestamp=${requestBody.timestamp}&type=${requestBody.type}&username=${requestBody.username}`;
    let hash = SlotXOService.buildSignature(param);

    if(!req.body.hash || requestBody.hash != hash){
        return res.send({
            "Status": 5,
            "Message": "Invalid signature",
            "Balance": 0,
        })
    }

    console.log('transaction request == ', requestBody);

    let body = {
        settleId: requestBody.id,
        amount: requestBody.amount,
        gameCode: requestBody.gamecode,
        roundId: requestBody.roundid,
        hash: requestBody.hash,
        username: requestBody.username,
        startBalance: requestBody.startbalance,
        endBalance: requestBody.endbalance,
        timestamp: requestBody.timestamp,
        createdDate: DateUtils.getCurrentDate(),
        description: requestBody.description,
        betId: requestBody.id,
        type: requestBody.type
    };

    let model = new SlotxoHistoryModel(body);
    // console.log('save to history = ',model);
    model.save((err, agentResponse) => {
        if (err) {
            // callback(err, null);
        } else {
            // callback(null, agentResponse);
        }
    });


    let betAmount = roundTo(Number.parseFloat(requestBody.amount), 2);
    let amount = roundTo(Number.parseFloat(requestBody.result), 2);
    let netAmount = amount - betAmount;

    // if(amount < 0){
    //     return res.send({
    //         "Status": 4,
    //         "Message": "Invalid parameters",
    //         "Balance": 0,
    //     })
    // }

    async.series([callback => {

        MemberService.findByUserNameForPartnerService(requestBody.username, function (err, memberResponse) {
            // console.log('member info = ', memberResponse);
            if (err) {
                callback(err, null);
                return;
            }
            if (memberResponse) {
                callback(null, memberResponse);
            } else {
                callback(900404, null);
            }
        });

    }], (err, asyncResponse) => {

        if (err) {
            if (err == 900404) {
                return res.send({
                    "Status": 1000,
                    "Message": "Member not found",
                    "Balance": 0,
                })
            } else {
                return res.send({
                    "Status": 1000,
                    "Message": "Internal server error",
                    "Balance": 0,
                })
            }
        }

        let memberInfo = asyncResponse[0];

        let credit = roundTo(roundTo(memberInfo.creditLimit, 2) + roundTo(memberInfo.balance, 2), 2);


        AgentGroupModel.findById(memberInfo.group).select('_id type parentId shareSetting commissionSetting').populate({
            path: 'parentId',
            select: '_id type parentId shareSetting commissionSetting name',
            populate: {
                path: 'parentId',
                select: '_id type parentId shareSetting commissionSetting name',
                populate: {
                    path: 'parentId',
                    select: '_id type parentId shareSetting commissionSetting name',
                    populate: {
                        path: 'parentId',
                        select: '_id type parentId shareSetting commissionSetting name',
                        populate: {
                            path: 'parentId',
                            select: '_id type parentId shareSetting commissionSetting name',
                            populate: {
                                path: 'parentId',
                                select: '_id type parentId shareSetting commissionSetting name',
                            }
                        }
                    }
                }
            }

        }).exec(function (err, agentGroups) {
            let betId = SlotXOService.generateBetId();
            let body = {
                memberId: memberInfo._id,
                memberParentGroup: memberInfo.group,
                memberUsername: memberInfo.username_lower,
                betId: betId,
                slot: {
                    xo: {
                        gameCode: requestBody.gamecode,
                        gameName: SlotXOService.getGameName(requestBody.gamecode),
                        settleId : requestBody.id,
                        roundId: requestBody.roundid,
                        txns: [{
                            betAmt: 0,
                            timestamp: requestBody.timestamp,
                            hash: requestBody.hash,
                        }],
                        winLoss : netAmount,
                        description : requestBody.description,
                        type : requestBody.type
                    }
                },
                amount: betAmount,
                memberCredit: betAmount * -1,
                payout: betAmount,
                validAmount: betAmount,
                gameType: 'TODAY',
                acceptHigherPrice: false,
                priceType: 'TH',
                game: 'GAME',
                source: 'SLOT_XO',
                status: 'DONE',
                commission: {},
                gameDate: DateUtils.getCurrentDate(),
                createdDate: DateUtils.getCurrentDate(),
                isEndScore: false,
                currency: memberInfo.currency,
                ipAddress: memberInfo.ipAddress,
                settleDate :DateUtils.getCurrentDate(),
                updatedDate :DateUtils.getCurrentDate()
            };

            prepareCommission(memberInfo, body, agentGroups, null, 0, 0)

            let winLose = netAmount;
            let betResult = winLose > 0 ? 'WIN' : winLose < 0 ? 'LOSE' : 'DRAW';

            //init
            if (!body.commission.senior) {
                body.commission.senior = {};
            }

            if (!body.commission.masterAgent) {
                body.commission.masterAgent = {};
            }

            if (!body.commission.agent) {
                body.commission.agent = {};
            }

            // console.log('body === ',body);
            const shareReceive = AgentService.prepareShareReceive(winLose, betResult, body);

            body.commission.superAdmin.winLoseCom = shareReceive.superAdmin.winLoseCom;
            body.commission.superAdmin.winLose = shareReceive.superAdmin.winLose;
            body.commission.superAdmin.totalWinLoseCom = shareReceive.superAdmin.totalWinLoseCom;

            body.commission.company.winLoseCom = shareReceive.company.winLoseCom;
            body.commission.company.winLose = shareReceive.company.winLose;
            body.commission.company.totalWinLoseCom = shareReceive.company.totalWinLoseCom;

            body.commission.shareHolder.winLoseCom = shareReceive.shareHolder.winLoseCom;
            body.commission.shareHolder.winLose = shareReceive.shareHolder.winLose;
            body.commission.shareHolder.totalWinLoseCom = shareReceive.shareHolder.totalWinLoseCom;

            body.commission.senior.winLoseCom = shareReceive.senior.winLoseCom;
            body.commission.senior.winLose = shareReceive.senior.winLose;
            body.commission.senior.totalWinLoseCom = shareReceive.senior.totalWinLoseCom;

            body.commission.masterAgent.winLoseCom = shareReceive.masterAgent.winLoseCom;
            body.commission.masterAgent.winLose = shareReceive.masterAgent.winLose;
            body.commission.masterAgent.totalWinLoseCom = shareReceive.masterAgent.totalWinLoseCom;

            body.commission.agent.winLoseCom = shareReceive.agent.winLoseCom;
            body.commission.agent.winLose = shareReceive.agent.winLose;
            body.commission.agent.totalWinLoseCom = shareReceive.agent.totalWinLoseCom;

            body.commission.member.winLoseCom = shareReceive.member.winLoseCom;
            body.commission.member.winLose = shareReceive.member.winLose;
            body.commission.member.totalWinLoseCom = shareReceive.member.totalWinLoseCom;
            body.betResult = betResult;

            // console.log('--------------',body);

            let betTransactionModel = new BetTransactionModel(body);

            betTransactionModel.save(function (err, response) {

                if (err) {
                    if (err.code == 11000) {
                        return res.send({
                            "Status": 201,
                            "Message": "Transaction is being processed",
                            "Balance": credit,
                        })
                    }
                    return res.send({
                        "Status": 1000,
                        "Message": "Internal server error",
                        "Balance": credit,
                    })
                }

                // SlotxoService.sendToQueue(betId, function (queErr, queRes) {
                return res.send({
                    "Balance":credit,
                    "Message":"Success",
                    "Status":0
                });
                // });

            });
        });

    });
});


const withdraw = Joi.object().keys({
    amount: Joi.number().required(),
    appid: Joi.string().required(),
    hash: Joi.string().required(),
    id: Joi.string().required(),
    timestamp: Joi.string().required(),
    username: Joi.string().required()

});
//transfer to xo
router.post('/withdraw', function (req, res) {
    let validate = Joi.validate(req.body, withdraw);
    if (validate.error) {
        return res.send({
            "Status": 4,
            "Message": "Invalid parameters",
            "Balance": 0,
        })
    }

    let requestBody = req.body;
    let betId = SlotXOService.generateBetId();
    console.log('request withdraw============= ', requestBody);

    let reqAmount = parseFloat(requestBody.amount).toFixed(2);
    let param = `amount=${reqAmount}&appid=${requestBody.appid}&id=${requestBody.id}&timestamp=${requestBody.timestamp}&username=${requestBody.username}`;
    let hash = SlotXOService.buildSignature(param);

    // console.log('hash == ',hash);

    if(!req.body.hash || requestBody.hash != hash){
        return res.send({
            "Status": 5,
            "Message": "Invalid signature",
            "Balance": 0,
        })
    }

    console.log('request withdraw============= ', requestBody);

    let body = {
        betId: requestBody.id,
        depositId: requestBody.id,
        amount: requestBody.amount,
        hash: requestBody.hash,
        username: requestBody.username,
        timestamp: requestBody.timestamp,
        createdDate: DateUtils.getCurrentDate(),
        type: 'DEPOSIT'
    };

    let model = new SlotxoHistoryModel(body);
    console.log('save to history = ',model);
    model.save((err, agentResponse) => {
        if (err) {
            // callback(err, null);
        } else {
            // callback(null, agentResponse);
        }
    });

    let amount = roundTo(Number.parseFloat(requestBody.amount), 2);

    if(amount < 0){
        return res.send({
            "Status": 4,
            "Message": "Invalid parameters",
            "Balance": 0,
        })
    }

    console.log('----------amount------------', amount);
    async.series([callback => {

        MemberService.findByUserNameForPartnerService(requestBody.username, function (err, memberResponse) {
            // console.log('member info = ', memberResponse);
            if (err) {
                callback(err, null);
                return;
            }
            if (memberResponse) {
                callback(null, memberResponse);
            } else {
                callback(900404, null);
            }
        });

    }], (err, asyncResponse) => {

        if (err) {
            console.log('----------error------------', err);
            if (err == 900404) {
                return res.send({
                    "Status": 1000,
                    "Message": "Member not found",
                    "Balance": 0,
                })
            } else {
                return res.send({
                    "Status": 1000,
                    "Message": "Internal server error",
                    "Balance": 0,
                })
            }
        }

        let memberInfo = asyncResponse[0];

        let credit = roundTo(roundTo(memberInfo.creditLimit, 2) + roundTo(memberInfo.balance, 2), 2);
        console.log('-----credit-------',credit);
        if(credit < amount){
            return res.send({
                "Status": 100,
                "Message": "Insufficient fund",
                "Balance": credit,
            })
        }

        let condition = {
            memberUsername : requestBody.username.toLowerCase(),
            'slot.xo.settleId' : requestBody.id
        };
        console.log('condition============== ',condition);
        BetTransactionModel.findOne(condition).exec(function (err, responseBonus) {
            console.log(err,'=============res=============== ',responseBonus);
            if (responseBonus) {
                return res.send({
                    "Balance": credit,
                    "Message": "Success",
                    "Status": 0
                });
            } else {
                let body = {
                    memberId: memberInfo._id,
                    memberParentGroup: memberInfo.group,
                    memberUsername: memberInfo.username_lower,
                    betId: SlotXOService.generateBetId(),
                    slot: {
                        xo: {
                            gameName: 'Transfer Credit',
                            settleId : requestBody.id,
                            txns: [{
                                betAmt: amount,
                                timestamp: requestBody.timestamp,
                                hash: requestBody.hash,
                            }],
                            winLoss : amount * -1,
                            description : 'Transfer credit to slotxo',
                            type : "Deposit"
                        }
                    },
                    amount: amount,
                    memberCredit: amount * -1,
                    payout: amount,
                    validAmount : amount,
                    gameType: 'TODAY',
                    acceptHigherPrice: false,
                    priceType: 'TH',
                    game: 'GAME',
                    source: 'SLOT_XO',
                    status: 'RUNNING',
                    commission: {},
                    gameDate: DateUtils.getCurrentDate(),
                    createdDate: DateUtils.getCurrentDate(),
                    isEndScore: false,
                    currency: memberInfo.currency,
                    ref1 : `${memberInfo._id}_SLOTXO_DEPOSIT`
                };

                console.log('body========= ',body);

                let betTransactionModel = new BetTransactionModel(body);

                betTransactionModel.save((err, response) => {

                    if (err) {
                        return res.send({
                            "Status": 1000,
                            "Message": "Internal server error",
                            "Balance": credit,
                        })
                    }else {
                        MemberService.updateBalance(memberInfo._id, amount * -1, betId, 'SLOT_XO_DEPOSIT', requestBody.id, function (err, updateBalanceResponse) {
                            if (err) {
                                return res.send({
                                    "Status": 1000,
                                    "Message": "Internal server error",
                                    "Balance": credit,
                                })
                            } else {
                                console.log('======================', updateBalanceResponse.newBalance)
                                return res.send({
                                    "Balance":updateBalanceResponse.newBalance,
                                    "Message":"Success",
                                    "Status":0
                                });
                            }
                        });
                    }
                });
            }
        });
    });

});

const deposit = Joi.object().keys({
    amount: Joi.number().required(),
    appid: Joi.string().required(),
    hash: Joi.string().required(),
    id: Joi.string().required(),
    timestamp: Joi.string().required(),
    username: Joi.string().required()

});

//transfer to sportbook
router.post('/deposit', function (req, res) {
    console.log('--------------------deposit start request--------------------------',req.body.username)
    let validate = Joi.validate(req.body, deposit);
    if (validate.error) {
        return res.send({
            "Status": 4,
            "Message": "Invalid parameters",
            "Balance": 0,
        })
    }

    let requestBody = req.body;

    let reqAmount = parseFloat(requestBody.amount).toFixed(2);
    let param = `amount=${reqAmount}&appid=${requestBody.appid}&id=${requestBody.id}&timestamp=${requestBody.timestamp}&username=${requestBody.username}`;
    let hash = SlotXOService.buildSignature(param);

    if(!req.body.hash || requestBody.hash != hash){
        return res.send({
            "Status": 5,
            "Message": "Invalid signature",
            "Balance": 0,
        })
    }

    let betId = SlotXOService.generateBetId();

    console.log('request deposit============= ', requestBody);

    let amount = roundTo(Number.parseFloat(requestBody.amount), 2);

    if(amount < 0){
        return res.send({
            "Status": 4,
            "Message": "Invalid parameters",
            "Balance": 0,
        })
    }

    async.waterfall([callback => {

        MemberService.findByUserNameForPartnerService(requestBody.username, function (err, memberResponse) {
            // console.log('member info = ', memberResponse);
            if (err) {
                callback(err, null);
                return;
            }
            if (memberResponse) {
                callback(null, memberResponse);
            } else {
                callback(900404, null);
            }
        });

    }, (memberInfo, callback) =>{
        let credit = roundTo(roundTo(memberInfo.creditLimit, 2) + roundTo(memberInfo.balance, 2), 2);
        let con = {
            betId: requestBody.id,
            username : requestBody.username.toLowerCase()
        };

        // console.log('condition == ', con);

        SlotxoHistoryModel.findOne(con).exec(function (err, responseHis) {
            // console.log('res==', responseHis);
            if(responseHis){
                return res.send({
                    "Balance":credit,
                    "Message":"Success",
                    "Status":0
                });
            }else {
                let body = {
                    betId: requestBody.id,
                    withdrawId: requestBody.id,
                    amount: requestBody.amount,
                    hash: requestBody.hash,
                    username: requestBody.username,
                    timestamp: requestBody.timestamp,
                    createdDate: DateUtils.getCurrentDate(),
                    type: 'WITHDRAW'
                };

                // console.log(body);

                let model = new SlotxoHistoryModel(body);

                // console.log('save to history = ',model);
                model.save((err, agentResponse) => {
                    if (err) {
                        console.log('-------------------dupicate key-------------------',err);
                        return res.send({
                            "Balance":credit,
                            "Message":"Success",
                            "Status":0
                        });
                    } else {
                        // console.log('--------res-------',agentResponse);
                        callback(null, memberInfo);
                    }
                });

            }
        });
    }], (err, asyncResponse) => {

        if (err) {
            if (err == 900404) {
                return res.send({
                    "Status": 1000,
                    "Message": "Member not found",
                    "Balance": 0,
                })
            } else {
                return res.send({
                    "Status": 1000,
                    "Message": "Internal server error",
                    "Balance": 0,
                })
            }
        }

        let memberInfo = asyncResponse;

        let credit = roundTo(roundTo(memberInfo.creditLimit, 2) + roundTo(memberInfo.balance, 2), 2);

        let condition = {
            ref1 : `${memberInfo._id}_SLOTXO_DEPOSIT`
        };

        console.log('condition  ======== ',condition);

        BetTransactionModel.findOne(condition, function (errr, response) {
            if(errr){
                console.log('delete transaction error =============== ',errr);
            }else {
                response.remove(function (removeErr, removeRes) {
                    console.log('================== delete transaction success =============== ');
                    MemberService.updateBalance(memberInfo._id, amount, betId, 'SLOT_XO_WITHDRAW', requestBody.id, function (err, updateBalanceResponse) {
                        if (err) {
                            return res.send({
                                "Status": 1000,
                                "Message": "Internal server error",
                                "Balance": credit,
                            })
                        } else {
                            console.log('--------------------deposit end request--------------------------',memberInfo.username_lower)
                            return res.send({
                                "Balance":updateBalanceResponse.newBalance,
                                "Message":"Success",
                                "Status":0
                            });
                        }
                    });
                })
            }
        })

        // BetTransactionModel.deleteMany(condition, function (err) {
        //     // console.log('delete many error ======= ',err);
        //     if(err){
        //         console.log('delete transaction error =============== ',err);
        //     }else {
        //         console.log('================== delete transaction success =============== ');
        //     }
        //
        //     MemberService.updateBalance(memberInfo._id, amount, betId, 'SLOT_XO_WITHDRAW', requestBody.id, function (err, updateBalanceResponse) {
        //         if (err) {
        //             return res.send({
        //                 "Status": 1000,
        //                 "Message": "Internal server error",
        //                 "Balance": credit,
        //             })
        //         } else {
        //
        //             return res.send({
        //                 "Balance":updateBalanceResponse.newBalance,
        //                 "Message":"Success",
        //                 "Status":0
        //             });
        //         }
        //     });
        // });

    });
});


router.get('/list-games', function (req, res) {
    SlotXOService.gameList((err, result) =>{
        return res.send(result)
    })
});

function updateAgentMemberBalance(shareReceive, betAmount, ref1, credit, updateCallback) {

    console.log('updateAgentMemberBalance');
    console.log('shareReceive : ', shareReceive);

    let balance = betAmount + shareReceive.member.totalWinLoseCom;

    try {
        console.log('update member balance : ', balance);

        MemberService.updateBalance(shareReceive.member.id, balance, shareReceive.betId, 'SLOT_XO_SETTLE', ref1, function (err, updateBalanceResponse) {
            if (err) {
                updateCallback(err, null);
            } else {
                updateCallback(null, updateBalanceResponse);
            }
        });
    } catch (err) {
        MemberService.updateBalance(shareReceive.member.id, balance, shareReceive.betId, 'SLOT_XO_SETTLE_ERROR ' + shareReceive.member.id + ' : ' + balance, '', function (err, updateBalanceResponse) {
            if (err) {
                updateCallback(err, null);
            } else {
                updateCallback(null, updateBalanceResponse);
            }
        });
    }

}

function prepareCommission(memberInfo, body, currentAgent, overAgent, remaining, overAllShare) {

    if (currentAgent && (currentAgent.type === 'SUPER_ADMIN' || currentAgent.parentId)) {

        let money = body.amount;

        if (!overAgent) {
            overAgent = {};
            overAgent.type = 'member';
            overAgent.shareSetting = {};
            overAgent.shareSetting.game = {};
            overAgent.shareSetting.game.slotXO = {};
            overAgent.shareSetting.game.slotXO.parent = memberInfo.shareSetting.game.slotXO.parent;
            overAgent.shareSetting.game.slotXO.own = 0;
            overAgent.shareSetting.game.slotXO.remaining = 0;

            body.commission.member = {};
            body.commission.member.parent = memberInfo.shareSetting.game.slotXO.parent;
            body.commission.member.commission = memberInfo.commissionSetting.game.slotXO;

        }

        let currentPercentReceive = overAgent.shareSetting.game.slotXO.parent;
        let totalRemaining = remaining;

        if (overAgent.shareSetting.game.slotXO.remaining > 0) {

            if (overAgent.shareSetting.game.slotXO.remaining > totalRemaining) {
                currentPercentReceive += totalRemaining;
                totalRemaining = 0;
            } else {
                totalRemaining = remaining - overAgent.shareSetting.game.slotXO.remaining;
                currentPercentReceive += overAgent.shareSetting.game.slotXO.remaining;
            }

        }

        let unUseShare = currentAgent.shareSetting.game.slotXO.own -
            (overAgent.shareSetting.game.slotXO.parent + overAgent.shareSetting.game.slotXO.own);

        totalRemaining = (unUseShare + totalRemaining);

        if (currentAgent.shareSetting.game.slotXO.min > 0 && ((overAllShare + currentPercentReceive) < currentAgent.shareSetting.game.slotXO.min)) {

            if (currentPercentReceive < currentAgent.shareSetting.game.slotXO.min) {
                let percent = currentAgent.shareSetting.game.slotXO.min - (currentPercentReceive + overAllShare);
                totalRemaining = totalRemaining - percent;
                if (totalRemaining < 0) {
                    totalRemaining = 0;
                }
                currentPercentReceive += percent;
            }
        } else {
            // currentPercentReceive += totalRemaining;
        }


        if (currentAgent.type === 'SUPER_ADMIN') {
            currentPercentReceive += totalRemaining;
        }

        let getMoney = (money * NumberUtils.convertPercent(currentPercentReceive)).toFixed(2);
        overAllShare = overAllShare + currentPercentReceive;

        //set commission
        let agentCommission = currentAgent.commissionSetting.game.slotXO;


        switch (currentAgent.type) {
            case 'SUPER_ADMIN':
                body.commission.superAdmin = {};
                body.commission.superAdmin.group = currentAgent._id;
                body.commission.superAdmin.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.superAdmin.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.superAdmin.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.superAdmin.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.superAdmin.shareReceive = Math.abs(currentPercentReceive);
                body.commission.superAdmin.commission = agentCommission;
                body.commission.superAdmin.amount = getMoney;
                break;
            case 'COMPANY':
                body.commission.company = {};
                body.commission.company.parentGroup = currentAgent.parentId;
                body.commission.company.group = currentAgent._id;
                body.commission.company.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.company.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.company.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.company.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.company.shareReceive = Math.abs(currentPercentReceive);
                body.commission.company.commission = agentCommission;
                body.commission.company.amount = getMoney;
                break;
            case 'SHARE_HOLDER':
                body.commission.shareHolder = {};
                body.commission.shareHolder.parentGroup = currentAgent.parentId;
                body.commission.shareHolder.group = currentAgent._id;
                body.commission.shareHolder.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.shareHolder.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.shareHolder.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.shareHolder.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.shareHolder.shareReceive = currentPercentReceive;
                body.commission.shareHolder.commission = agentCommission;
                body.commission.shareHolder.amount = getMoney;
                break;
            case 'SENIOR':
                body.commission.senior = {};
                body.commission.senior.parentGroup = currentAgent.parentId;
                body.commission.senior.group = currentAgent._id;
                body.commission.senior.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.senior.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.senior.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.senior.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.senior.shareReceive = currentPercentReceive;
                body.commission.senior.commission = agentCommission;
                body.commission.senior.amount = getMoney;
                break;
            case 'MASTER_AGENT':
                body.commission.masterAgent = {};
                body.commission.masterAgent.parentGroup = currentAgent.parentId;
                body.commission.masterAgent.group = currentAgent._id;
                body.commission.masterAgent.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.masterAgent.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.masterAgent.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.masterAgent.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.masterAgent.shareReceive = currentPercentReceive;
                body.commission.masterAgent.commission = agentCommission;
                body.commission.masterAgent.amount = getMoney;
                break;
            case 'AGENT':
                body.commission.agent = {};
                body.commission.agent.parentGroup = currentAgent.parentId;
                body.commission.agent.group = currentAgent._id;
                body.commission.agent.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.agent.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.agent.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.agent.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.agent.shareReceive = currentPercentReceive;
                body.commission.agent.commission = agentCommission;
                body.commission.agent.amount = getMoney;
                break;
        }

        prepareCommission(memberInfo, body, currentAgent.parentId, currentAgent, totalRemaining, overAllShare);
    }
}


// router.get('/login', function (req, res) {
//
//     console.log('SLOT_XO_APP_ID : ', SLOT_XO_APP_ID)
//     console.log('SLOT_XO_HOST : ', SLOT_XO_HOST)
//     console.log('SLOT_XO_SECRET_KEY : ', SLOT_XO_SECRET_KEY)
//
//     const userInfo = req.userInfo;
//
//
//     async.waterfall([(callback) => {
//
//         MemberService.findById(userInfo._id, 'username limitSetting group', function (err, memberResponse) {
//             if (err) {
//                 callback(err, null);
//             } else {
//
//                 if (memberResponse.limitSetting.game.slotXO.isEnable) {
//                     AgentService.getLive22Status(memberResponse.group._id, (err, response) => {
//                         if (!response.isEnable) {
//                             callback(1088, null);
//                         } else {
//                             callback(null, memberResponse);
//                         }
//                     });
//                 } else {
//                     callback(1088, null);
//                 }
//             }
//         });
//
//     }, (memberInfo, callback) => {
//         SlotXOService.createUser(memberInfo.username, (err, response) => {
//             console.log('====__==== : ', err);
//             console.log('====__====22 : ', response)
//             if (err) {
//                 callback('Create User Fail.', null);
//             } else {
//                 callback(null, memberInfo, response);
//             }
//
//         })
//     }], (err, memberInfo) => {
//         if (err) {
//             if (err == 1088) {
//                 return res.send({
//                     code: 1088,
//                     message: 'Service Locked, Please contact your upline.'
//                 });
//             }
//             return res.send({code: 999, message: 'Login slot xo fail.', result: err})
//         } else {
//
//
//             let url = `/login-factor/` + memberInfo._id;
//
//
//             return res.send({
//                 code: 0,
//                 url: url
//             });
//         }
//     });
// });
//
// //TODO: refactor getToken
// router.get('/login-factor/:id', function (req, res) {
//
//
//     let code = req.query.code;
//
//     async.series([(callback) => {
//         MemberService.findById(req.params.id, 'username username_lower limitSetting group', function (err, memberResponse) {
//             if (err) {
//                 callback(err, null);
//             } else {
//                 callback(null, memberResponse);
//             }
//         });
//     }], (err, asyncResponse) => {
//
//         let memberInfo = asyncResponse[0];
//
//         if (err) {
//             return res.send({code: 999, message: 'Login slot xo fail.', result: err})
//         }
//
//         let method = 'RT';
//         let timestamp = moment().unix();
//         let parameters = `Method=${method}&Timestamp=${timestamp}&Username=${memberInfo.username_lower}`;
//
//         generateSignature(parameters, (err, data) => {
//
//             const headers = {
//                 'Content-Type': 'application/x-www-form-urlencoded'
//             };
//
//             let form = {
//                 Method: method,
//                 Timestamp: timestamp,
//                 Username: memberInfo.username_lower
//             };
//
//             const option = {
//                 url: `${SLOT_XO_HOST}?AppID=${SLOT_XO_APP_ID}&Signature=${encodeURIComponent(data)}`,
//                 method: 'POST',
//                 headers: headers,
//                 body: querystring.stringify(form)
//             };
//
//             request(option, (err, response, body) => {
//                 if (err) {
//                     return res.send({code: 999, message: 'Login slot xo fail.', result: err})
//                 } else {
//
//                     console.log('Get Token =======> ', JSON.stringify(response));
//
//
//                     try {
//                         if (response.statusCode != 200) {
//                             return res.send({
//                                 code: 999,
//                                 message: 'Login slot xo fail. (Token)',
//                                 result: err
//                             })
//                         }
//                     } catch (e) {
//                         return res.send({code: 999, message: 'Get Token Fail', result: err})
//                     }
//
//                     let tokenBody = JSON.parse(response.body);
//
//
//                     res.writeHeader(200, {"Content-Type": "text/html"});
//                     let template = templateRedirect(tokenBody.Token, code);
//
//                     console.log('Template =====>  ', template);
//                     res.write(template);
//                     res.end();
//                 }
//             });
//
//         });
//     });
//
// });
//
//
// router.post('/create-user', function (req, res) {
//
//     let {
//         username,
//     } = req.body;
//
//     let method = 'CU';
//
//     let timestamp = moment().unix();
//
//     let parameters = `Method=${method}&Timestamp=${timestamp}&Username=${username}`;
//
//     generateSignature(parameters, (err, data) => {
//
//         const headers = {
//             'Content-Type': 'application/x-www-form-urlencoded'
//         };
//
//         let form = {
//             Method: method,
//             Timestamp: timestamp,
//             Username: username
//         }
//
//         const option = {
//             url: `${SLOT_XO_HOST}?AppID=${SLOT_XO_APP_ID}&Signature=${encodeURIComponent(data)}`,
//             method: 'POST',
//             headers: headers,
//             body: querystring.stringify(form)
//         };
//
//         request(option, (err, response, body) => {
//             if (err) {
//                 return res.send({code: 999, result: err});
//             } else {
//
//                 try {
//                     if (response.statusCode === 200) {
//                         return res.send({code: 0, message: JSON.parse(response.body)})
//                     } else {
//                         return res.send({code: 999, message: JSON.parse(response.body)})
//                     }
//                 } catch (e) {
//                     return res.send({code: 1, result: 'Create user fail.'});
//                 }
//             }
//         });
//     })
// });
//
//
// const getBalanceSchema = Joi.object().keys({
//     username: Joi.string().required(),
// });
//
// router.post('/get-credit', function (req, res) {
//
//
//     let validate = Joi.validate(req.body, getBalanceSchema);
//     if (validate.error) {
//         return res.send({
//             message: "validation fail",
//             results: validate.error.details,
//             code: 1001
//         });
//     }
//
//     SlotXOService.getCredit(req.body.username, function (err, credit) {
//         if (err) {
//             return res.send({code: 999, result: err});
//         } else {
//             return res.send({code: 0, result: {credit: credit}})
//         }
//     });
// });
//
// router.post('/transfer-credit', function (req, res) {
//
//     let {
//         username,
//         amount
//     } = req.body;
//
//     let method = 'TC';
//
//     let timestamp = moment().unix();
//
//     let parameters = `Amount=${amount}&Method=${method}&Timestamp=${timestamp}&Username=${username}`;
//
//     generateSignature(parameters, (err, data) => {
//
//         const headers = {
//             'Content-Type': 'application/x-www-form-urlencoded'
//         };
//
//         let form = {
//             Amount: amount,
//             Method: method,
//             Timestamp: timestamp,
//             Username: username
//         }
//
//         const option = {
//             url: `${SLOT_XO_HOST}?AppID=${SLOT_XO_APP_ID}&Signature=${encodeURIComponent(data)}`,
//             method: 'POST',
//             headers: headers,
//             body: querystring.stringify(form)
//         };
//
//         request(option, (err, response, body) => {
//             if (err) {
//                 return res.send({code: 999, result: err});
//             } else {
//
//                 try {
//                     if (response.statusCode === 200) {
//                         return res.send({code: 0, result: JSON.parse(response.body)})
//                     } else {
//                         return res.send({code: 999, message: JSON.parse(response.body)})
//                     }
//                 } catch (e) {
//                     return res.send({code: 1, result: 'Transfer credit fail.'});
//                 }
//             }
//         });
//     })
// });
//
// router.post('/get-token', function (req, res) {
//
//     let {
//         username,
//     } = req.body;
//
//     let method = 'RT';
//     let timestamp = moment().unix();
//     let parameters = `Method=${method}&Timestamp=${timestamp}&Username=${username}`;
//
//     generateSignature(parameters, (err, data) => {
//
//         const headers = {
//             'Content-Type': 'application/x-www-form-urlencoded'
//         };
//
//         let form = {
//             Method: method,
//             Timestamp: timestamp,
//             Username: username
//         }
//
//         const option = {
//             url: `${SLOT_XO_HOST}?AppID=${SLOT_XO_APP_ID}&Signature=${encodeURIComponent(data)}`,
//             method: 'POST',
//             headers: headers,
//             body: querystring.stringify(form)
//         };
//
//         request(option, (err, response, body) => {
//             if (err) {
//                 return res.send({code: 999, result: err});
//             } else {
//
//                 console.log('Get Token =======> ', JSON.stringify(response));
//
//                 try {
//                     if (response.statusCode === 200) {
//                         return res.send({code: 0, result: JSON.parse(response.body)})
//                     } else {
//                         return res.send({code: 999, message: JSON.parse(response.body)})
//                     }
//                 } catch (e) {
//                     return res.send({code: 1, result: 'Get token fail.'});
//                 }
//             }
//         });
//     })
// });
//
// router.post('/suspend-user', function (req, res) {
//
//     let {
//         username,
//     } = req.body;
//
//     let method = 'SU';
//
//     let timestamp = moment().unix();
//
//     let parameters = `Method=${method}&Timestamp=${timestamp}&Username=${username}`;
//
//     generateSignature(parameters, (err, data) => {
//
//         const headers = {
//             'Content-Type': 'application/x-www-form-urlencoded'
//         };
//
//         let form = {
//             Method: method,
//             Timestamp: timestamp,
//             Username: username
//         }
//
//         const option = {
//             url: `${SLOT_XO_HOST}?AppID=${SLOT_XO_APP_ID}&Signature=${encodeURIComponent(data)}`,
//             method: 'POST',
//             headers: headers,
//             body: querystring.stringify(form)
//         };
//
//         request(option, (err, response, body) => {
//             if (err) {
//                 return res.send({code: 999, result: err});
//             } else {
//
//                 console.log('Suspend User ====> ', body, response);
//
//                 try {
//                     if (response.statusCode === 200) {
//                         return res.send({
//                             code: 0,
//                             message: `Suspend user id : ${username} success.`
//                         })
//                     } else {
//                         return res.send({code: 999, message: response.body})
//                     }
//                 } catch (e) {
//                     return res.send({code: 1, result: `Suspend user id : ${username} fail.`});
//                 }
//             }
//         });
//     })
// });
//
// router.post('/set-password', function (req, res) {
//
//     let {
//         username,
//         password
//     } = req.body;
//
//     let method = 'SP';
//
//     let timestamp = moment().unix();
//
//     let parameters = `Method=${method}&Password=${password}&Timestamp=${timestamp}&Username=${username}`;
//
//     generateSignature(parameters, (err, data) => {
//
//         const headers = {
//             'Content-Type': 'application/x-www-form-urlencoded'
//         };
//
//         let form = {
//             Method: method,
//             Password: password,
//             Timestamp: timestamp,
//             Username: username
//         }
//
//         const option = {
//             url: `${SLOT_XO_HOST}?AppID=${SLOT_XO_APP_ID}&Signature=${encodeURIComponent(data)}`,
//             method: 'POST',
//             headers: headers,
//             body: querystring.stringify(form)
//         };
//
//         request(option, (err, response, body) => {
//             if (err) {
//                 return res.send({code: 999, result: err});
//             } else {
//
//                 console.log('Reset Password ====> ', body, response);
//
//                 try {
//                     if (response.statusCode === 200) {
//                         return res.send({
//                             code: 0,
//                             message: `Set password user id : ${username} success.`
//                         })
//                     } else {
//                         return res.send({code: 999, message: response.body})
//                     }
//                 } catch (e) {
//                     return res.send({code: 1, result: `Set password user id : ${username} fail.`});
//                 }
//             }
//         });
//     })
// });
//
// router.post('/retrieve-transaction', function (req, res) {
//
//     let {
//         endDate,
//         startDate,
//         nextId
//     } = req.body;
//
//     let method = 'TS';
//
//     let timestamp = moment().unix();
//
//     let parameters = '';
//     let form = {};
//
//     if (nextId) {
//         parameters = `EndDate=${endDate}&Method=${method}&NextId=${nextId}&StartDate=${startDate}&Timestamp=${timestamp}`;
//
//         form = {
//             Method: method,
//             StartDate: startDate,
//             NextId: nextId,
//             EndDate: endDate,
//             Timestamp: timestamp
//         }
//     } else {
//         parameters = `EndDate=${endDate}&Method=${method}&StartDate=${startDate}&Timestamp=${timestamp}`;
//
//         form = {
//             Method: method,
//             StartDate: startDate,
//             EndDate: endDate,
//             Timestamp: timestamp
//         }
//     }
//
//     generateSignature(parameters, (err, data) => {
//
//         const headers = {
//             'Content-Type': 'application/x-www-form-urlencoded'
//         };
//
//         const option = {
//             url: `${SLOT_XO_HOST}?AppID=${SLOT_XO_APP_ID}&Signature=${encodeURIComponent(data)}`,
//             method: 'POST',
//             headers: headers,
//             body: querystring.stringify(form)
//         };
//
//         request(option, (err, response, body) => {
//             if (err) {
//                 return res.send({code: 999, result: err});
//             } else {
//
//                 try {
//                     if (response.statusCode === 200) {
//                         return res.send({code: 0, result: JSON.parse(response.body)})
//                     } else {
//                         return res.send({code: 999, message: JSON.parse(response.body)})
//                     }
//                 } catch (e) {
//                     return res.send({code: 1, result: 'Get retrieve transaction fail.'});
//                 }
//             }
//         });
//     })
// });
//
// router.post('/transaction-total', function (req, res) {
//
//     let {
//         username,
//         endDate,
//         startDate
//     } = req.body;
//
//     SlotXOService.getTotalTransactions(username, startDate, endDate, function (err, response) {
//
//         if (err) {
//             return res.send({code: 999, result: err});
//         } else {
//             return res.send({code: 0, result: response})
//         }
//     });
// });
// //
// // router.get('/:id', function (req, res) {
// //     res.writeHeader(200, {"Content-Type": "text/html"});
// //     res.write(`
// //                 <!DOCTYPE html>
// //                 <html>
// //                 <head>
// //                 <style>
// //                     .loader {
// //                       border: 16px solid #f3f3f3;
// //                       border-radius: 50%;
// //                       border-top: 16px solid blue;
// //                      border-right: 16px solid green;
// //                      border-bottom: 16px solid red;
// //                      border-left: 16px solid pink;
// //                       width: 120px;
// //                       height: 120px;
// //                       -webkit-animation: spin 2s linear infinite;
// //                       animation: spin 2s linear infinite;
// //                       margin-left: 45%;
// //                       margin-top: 20%;
// //                     }
// //
// //                     @-webkit-keyframes spin {
// //                       0% { -webkit-transform: rotate(0deg); }
// //                       100% { -webkit-transform: rotate(360deg); }
// //                     }
// //
// //                     @keyframes spin {
// //                       0% { transform: rotate(0deg); }
// //                       100% { transform: rotate(360deg); }
// //                     }
// //                 </style>
// //                  <script>
// //                      var auto_refresh = setInterval(function()
// //                      {
// //                         submitform();
// //                      }, 1000);
// //                      function submitform(){
// //                          document.getElementById("myForm").submit();
// //                      }
// //                 </script>
// //                 </head>
// //                     <body>
// //                          <form id="myForm" action="http://www.joker688.net" method="post">
// //                              <input hidden type="text" name="token" value="${req.params.id}">
// //                              <input hidden type="submit" value="Submit">
// //                         </form>
// //
// //                         <div class="loader"></div>
// //
// //                     </body>
// //                 </html>
// //                 `);
// //     res.end();
// // });
// //
// // router.get('/mobile/:id', function (req, res) {
// //     res.writeHeader(200, {"Content-Type": "text/html"});
// //     res.write(`
// //                 <!DOCTYPE html>
// //                 <html>
// //                 <head>
// //                 <style>
// //                     .loader {
// //                       border: 16px solid #f3f3f3;
// //                       border-radius: 50%;
// //                       border-top: 16px solid blue;
// //                      border-right: 16px solid green;
// //                      border-bottom: 16px solid red;
// //                      border-left: 16px solid pink;
// //                       width: 120px;
// //                       height: 120px;
// //                       -webkit-animation: spin 2s linear infinite;
// //                       animation: spin 2s linear infinite;
// //                       margin-left: 45%;
// //                       margin-top: 20%;
// //                     }
// //
// //                     @-webkit-keyframes spin {
// //                       0% { -webkit-transform: rotate(0deg); }
// //                       100% { -webkit-transform: rotate(360deg); }
// //                     }
// //
// //                     @keyframes spin {
// //                       0% { transform: rotate(0deg); }
// //                       100% { transform: rotate(360deg); }
// //                     }
// //                 </style>
// //                  <script>
// //                      var auto_refresh = setInterval(function()
// //                      {
// //                         submitform();
// //                      }, 1000);
// //                      function submitform(){
// //                          document.getElementById("myForm").submit();
// //                      }
// //                 </script>
// //                 </head>
// //                     <body>
// //                          <form id="myForm" action="http://www.joker688.net" method="post">
// //                              <input hidden type="text" name="token" value="${req.params.id}">
// //                              <input hidden type="text" name="togameken" value="AllBet">
// //                              <input hidden type="text" name="mobile" value="true">
// //                              <input hidden type="submit" value="Submit">
// //                         </form>
// //
// //                         <div class="loader"></div>
// //
// //                     </body>
// //                 </html>
// //                 `);
// //     res.end();
// // });
//
// function templateRedirect(token, code) {
//
//     let template = '';
//
//     let game = code ? code : '';
//
//
//     if (token) {
//         template = `
//                 <!DOCTYPE html>
//                 <html>
//                 <head>
//                 <style>
//                     .loader {
//                       border: 16px solid #f3f3f3;
//                       border-radius: 50%;
//                       border-top: 16px solid blue;
//                      border-right: 16px solid green;
//                      border-bottom: 16px solid red;
//                      border-left: 16px solid pink;
//                       width: 120px;
//                       height: 120px;
//                       -webkit-animation: spin 2s linear infinite;
//                       animation: spin 2s linear infinite;
//                       margin-left: 45%;
//                       margin-top: 20%;
//                     }
//
//                     @-webkit-keyframes spin {
//                       0% { -webkit-transform: rotate(0deg); }
//                       100% { -webkit-transform: rotate(360deg); }
//                     }
//
//                     @keyframes spin {
//                       0% { transform: rotate(0deg); }
//                       100% { transform: rotate(360deg); }
//                     }
//                 </style>
//                  <script>
//                      var auto_refresh = setInterval(function()
//                      {
//                         submitform();
//                      }, 2000);
//                      function submitform(){
//                          document.getElementById("myForm").submit();
//                      }
//                 </script>
//                 </head>
//                     <body>
//                          <form id="myForm" action="http://www.joker688.net" method="post">
//                              <input hidden type="text" name="token" value="${token}">
//                              <input hidden type="text" name="game" value="${game}">
//                              <input hidden type="submit" value="Submit">
//                         </form>
//
//                         <div class="loader"></div>
//
//                     </body>
//                 </html>
//                 `
//     }
//     return template;
//
// }
//
// function generateSignature(parameters, callback) {
//     console.log('parameters ==========> ', parameters);
//
//     let key = new Buffer(SLOT_XO_SECRET_KEY, 'utf8');
//     let hmac = Crypto.createHmac("sha1", key);
//     let hash2 = hmac.update(parameters, 'utf8');
//     let digest = hash2.digest("base64");
//
//     callback(null, digest)
// }

module.exports = router;