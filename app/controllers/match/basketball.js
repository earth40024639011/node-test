const BasketballModel      = require('../../models/basketball.model');
const BasketballTrashModel = require('../../models/basketballTrash.model');

const Commons = require('../../common/commons');
const express = require('express');
const router  = express.Router();
const request = require('request');
const _ = require('underscore');
const async = require("async");
const clone = require('clone');
const roundTo = require('round-to');

const MemberService = require('../../common/memberService');
const RedisService = require('../../common/redisService');

const CACHE_IP = process.env.CACHE_IP || 'isn-backend.api-hub.com';
const URL_CACHE_HDP_SINGLE_MATCH = `http://${CACHE_IP}:8001/basketball/cache/hdp_single_match`;
const CLIENT_NAME = process.env.CLIENT_NAME || 'SPORTBOOK88';

const redis     = require("ioredis");
const redisIp   = process.env.REDIS_IP || "redis.api-hub.com";
const redisPort = process.env.REDIS_PORT || "6379";
const client_redis = new redis({
    port: redisPort,          // Redis port
    host: redisIp,   // Redis host
    family: 4,           // 4 (IPv4) or 6 (IPv6)
    db: 1
});
// redis.createClient({host: redisIp, port: redisPort});
// client_redis.select(1);

//[1]Today
router.get('/hdp', (req, res) => {
    const now             = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const today_startDate = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const today_endDate   = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);

    if ((now.getHours() - 7) >= 11 && now.getMinutes() >= 0 && now.getSeconds() >= 0 ) {
        today_startDate.setDate(now.getDate());
        today_endDate.setDate(now.getDate()  + 1);
    } else {
        today_startDate.setDate(now.getDate() - 1);
        today_endDate.setDate(now.getDate());
    }
    today_startDate.setHours(11 + 7);
    today_startDate.setMinutes(0);
    today_startDate.setSeconds(0);
    today_endDate.setHours(10 + 7);
    today_endDate.setMinutes(59);
    today_endDate.setSeconds(59);

    async.parallel([
            (callback) => {
                BasketballModel.find({})
                    .lean()
                    .sort('sk')
                    .exec((error, response) => {
                        if(error){
                            callback(error, null);
                        } else {
                            callback(null, response);
                        }
                    });
            },

            (callback) => {
                RedisService.basketballHDP((error, response) => {
                    if(error){
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            },

            (callback) => {
                RedisService.getCurrency((error, result) => {
                    if (error) {
                        callback(null, false);
                    } else {
                        callback(null, result);
                    }
                })
            },
            (callback) => {
                MemberService.getLimitSetting(req.userInfo._id, (errLimitSetting, dataLimitSetting)=>{
                    if(errLimitSetting){
                        callback(errLimitSetting, null);
                    } else {
                        callback(null, dataLimitSetting.limitSetting);
                    }
                });
            },
        ],
        (error, result) => {
            if (error) {
                return res.send({
                    code    : 9999,
                    message : error
                });
            } else {
                const [
                    basketballDB,
                    basketballRedis,
                    currency,
                    limitSetting
                ] = result;

                const basketballResult = _.chain(basketballRedis)
                    .each(objRedis => {

                        const objDB = _.chain(basketballDB)
                            .findWhere(JSON.parse(`{"k":${objRedis.k}}`))
                            .pick('r')
                            .map(obj => {
                                return obj;
                            })
                            .first()
                            .value();

                        if (!_.isUndefined(objDB)) {
                            // console.log(objRedis.k + ' : ' + objRedis.n);
                            // console.log(objDB);

                            objRedis.m = _.filter(objRedis.m, match => {
                               return new Date(match.d) >= today_startDate && new Date(match.d) <= today_endDate && new Date(match.d) >= now;
                            });

                            _.each(objRedis.m, match => {
                                // console.log('   ',match.n.en.h+' vs '+match.n.en.a);
                                delete match.bpl;
                                _.each(match.bp, (betPrice, index)=> {
                                    // console.log('       '+betPrice.allKey + ' : ' + index);

                                    const r = {};
                                    r.ah = {};
                                    r.ou = {};
                                    r.oe = {};
                                    r.ml = {};
                                    r.t1ou = {};
                                    r.t2ou = {};


                                    r.ah.ma = calculateBetPrice(Math.min(objDB.ah.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), index);
                                    r.ah.mi = objDB.ah.mi;

                                    r.ou.ma = calculateBetPrice(Math.min(objDB.ou.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), index);
                                    r.ou.mi = objDB.ou.mi;

                                    r.oe.ma = calculateBetPrice(Math.min(objDB.oe.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), index);
                                    r.oe.mi = objDB.oe.mi;

                                    r.ml.ma = calculateBetPrice(Math.min(objDB.ml.ma, limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet), index);
                                    r.ml.mi = objDB.ml.mi;

                                    r.t1ou.ma = calculateBetPrice(Math.min(objDB.t1ou.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), index);
                                    r.t1ou.mi = objDB.t1ou.mi;

                                    r.t2ou.ma = calculateBetPrice(Math.min(objDB.t2ou.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), index);
                                    r.t2ou.mi = objDB.t2ou.mi;


                                    r.ah.ma   = calculateBetPriceByTime(r.ah.ma, now, new Date(match.d));
                                    r.ou.ma   = calculateBetPriceByTime(r.ou.ma, now, new Date(match.d));
                                    r.oe.ma   = calculateBetPriceByTime(r.oe.ma, now, new Date(match.d));
                                    r.ml.ma   = calculateBetPriceByTime(r.ml.ma, now, new Date(match.d));
                                    r.t1ou.ma = calculateBetPriceByTime(r.t1ou.ma, now, new Date(match.d));
                                    r.t2ou.ma = calculateBetPriceByTime(r.t2ou.ma, now, new Date(match.d));


                                    betPrice.r = r;
                                });
                            });
                            objRedis.show = true;
                        }
                    })
                    .reject(objRedis => { return _.size(objRedis.m) === 0 })
                    .reject(objRedis => {
                        return _.isUndefined(objRedis.show);
                    })
                    .value();

                return res.send({
                    code: 0,
                    message: "success",
                    result: basketballResult
                });
            }
        });

});

//[2]
router.put('/hdp_match_bet_price', (req, res) => {
    const {
        id,
        prefix,
        key,
        value
    } = req.body;

    const predicate = JSON.parse(`{"${key}":"${value}"}`);

    const league_key = parseInt(_.first(id.split(':')));
    const match_key = parseInt(_.last(id.split(':')));

    const membercurrency = 'THB';//req.userInfo.currency;
    async.series([
        (callback) => {
            BasketballModel.findOne({k: league_key})
                .lean()
                .sort('sk')
                .exec((error, response) => {
                    if(error){
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
        },

        (callback) => {
            const option = {
                url: `${URL_CACHE_HDP_SINGLE_MATCH}/${id}`,
                method: 'GET'
            };

            request(option, (error, response, body) => {
                if(error){
                    callback(error, null);
                } else {
                    const result = JSON.parse(body);

                    if(result){
                        callback(null, result);
                    } else {
                        callback(null, []);
                    }
                }
            });
        },

        (callback) => {
            RedisService.getCurrency((error, result) => {
                if (error) {
                    callback(null, false);
                } else {
                    callback(null, result);
                }
            })
        },
        (callback) => {
            MemberService.getLimitSetting(req.userInfo._id, (errLimitSetting, dataLimitSetting)=>{
                if(errLimitSetting){
                    callback(errLimitSetting, null);
                } else {
                    callback(null, dataLimitSetting.limitSetting);
                }
            });
        },

        (callback) => {
            MemberService.getOddAdjustment( req.userInfo.username, (err, data) => {
                if (err) {
                    // console.log('Get Odd Adjust error');
                    callback(err, null);
                } else {
                    callback(null, data);
                }
            });
        },

    ], (error, result)=> {
        if (error) {
            return res.send({
                message: "error",
                result: error,
                code: 998
            });
        } else {

            const [
                basketballDB,
                match_cache,
                currency,
                limitSetting,
                oddAdjust
            ] = result;

            const type_lower = prefix.toLowerCase();


            let index = -1;
            let point = 0;
            const bp =  _.first(_.filter(match_cache.result, league => {
                index++;
                if (_.contains(league.allKey, value)) {
                    point = index;
                }
                return _.contains(league.allKey, value)
            }));
            let model_result = {};

            let rp_hdp = {
                hdp: {
                    ah:{
                        f: 0,
                        u: 0
                    },
                    ou:{
                        f: 0,
                        u: 0
                    },
                    oe:{
                        f: 0,
                        u: 0
                    },
                    ml:{
                        f: 0,
                        u: 0
                    },
                    t1ou:{
                        f: 0,
                        u: 0
                    },
                    t2ou:{
                        f: 0,
                        u: 0
                    }
                }
            }

            let CONFIG_MTACH = {
                ah: {
                    a: 0,
                    h: 0
                },
                ou: {
                    a: 0,
                    h: 0
                },
                oe: {
                    a: 0,
                    h: 0
                },
                ml: {
                    a: 0,
                    h: 0
                },
                t1ou: {
                    a: 0,
                    h: 0
                },
                t2ou: {
                    a: 0,
                    h: 0
                },
            }

            let odd = _.filter(oddAdjust, (o)=>{
                return o.matchId === id;
            });


            Commons.switch_type_for_basket_ball(type_lower, bp, rp_hdp.hdp, model_result, CONFIG_MTACH, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, odd, (err, result)=>{
                if(err){
                    return res.send({message: err, code: 10102});
                } else {
                    let result_new_array = [result];

                    let betPrice = _.chain(result_new_array)
                        .flatten(true)
                        .map(betPrice => {
                            return _.findWhere(betPrice, predicate);
                        })
                        .reject(betPrice => {
                            return _.isUndefined(betPrice)
                        })
                        .map((betPrice, index) => {
                            // indexRound = index;
                            return Commons.getBetObjectByKeyForBasketball(prefix, key, betPrice);
                        })
                        .first()
                        .value();

                    let min_max = basketballDB.r;

                    Commons.switch_min_max_for_basket_ball(type_lower, min_max, betPrice, (err, dataResult)=>{
                        if(_.isEqual(type_lower, 'ml')){

                            dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet);
                            if(dataResult.a){
                                dataResult.max = dataResult.max / dataResult.a;
                            } else if(dataResult.h){
                                dataResult.max = dataResult.max / dataResult.h;
                            } else if(dataResult.d){
                                dataResult.max = dataResult.max / dataResult.d;
                            }
                        } else {
                            dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.hdpOuOe.maxPerBet);
                        }

                        Commons.calculateBetPrice(dataResult.max, bp[prefix], (err, data)=>{
                            dataResult.max = data;
                        })

                        dataResult.min = convertMinMaxByCurrency(dataResult.min, currency, membercurrency);
                        dataResult.max = convertMinMaxByCurrency(dataResult.max, currency, membercurrency);
                        return res.send({
                            message: "success",
                            result: dataResult,
                            code: 0
                        });
                    })
                }
            })
        }
    });
});

//[2]
router.get('/live', (req, res) =>{
    RedisService.basketballLive((error, response) => {
        return res.send({
            code: 0,
            message: "success",
            result: response
        });
    });
});

//[11]
router.get('/score', (req, res) => {
    const day = req.query.d || 0;
    const month = req.query.m || 0;


    const startDate = new Date(_.now());
    const endDate = new Date(_.now());

    // const startDate = new Date(new Date().getTime());
    // const endDate = new Date(new Date().getTime());

    console.log('====== Start Date ======' + day + ":" + month);
    startDate.setMonth(month - 1);
    startDate.setDate(day);
    startDate.setHours(18 /*- 7*/);
    startDate.setMinutes(0);
    startDate.setSeconds(0);

    console.log('====== End Date ======' + day + ":" + month);
    endDate.setMonth(month - 1);
    endDate.setDate(day);
    endDate.setDate(endDate.getDate() + 1);
    endDate.setHours(17 /*- 7*/);
    endDate.setMinutes(59);
    endDate.setSeconds(59);

    console.log('============== startDate ==============');
    console.log(startDate);
    console.log('============== startDate ==============');
    console.log('=============== endDate ===============');
    console.log(endDate);
    console.log('=============== endDate ===============');

    async.series([
        (callback =>
            {
                BasketballModel.find(
                    {
                        m: {
                            $elemMatch: {
                                // 's.isCr': true,
                                'd': {
                                    $gte: startDate,
                                    $lt: endDate
                                }
                            }
                        }
                    })
                    .lean()
                    .sort('sk')
                    .exec((err, data) => {
                        if (err) {
                            callback(err, null);
                        } else {
                            const footballData = _.chain(data)
                                .each(league => {
                                    league.m = _.filter(league.m, match => {
                                        return /*match.s.isCr && */match.d >= startDate && match.d <= endDate
                                    });

                                    _.each(league.m, match => {
                                        if (match.s.isH) {
                                            match.s.cr.ft.h = '-';
                                            match.s.cr.ft.a = '-';

                                        } else if (match.s.isL) {
                                            match.s.cr.ft.h = '-';
                                            match.s.cr.ft.a = '-';

                                        } else if (match.s.isE) {
                                            match.s.cr.ft.h = '-';
                                            match.s.cr.ft.a = '-';

                                        } else if (match.s.isC.ht) {
                                            match.s.cr.ft.h = 'Refunded';
                                            match.s.cr.ft.a = '';
                                            if (!_.isUndefined(match.s.isC.ft_remark) && !_.isNull(match.s.isC.ft_remark)) {
                                                match.s.cr.ft.h = match.s.isC.ft_remark;
                                            }
                                        } else if (match.s.isC.ft) {
                                            match.s.cr.ft.h = 'Refunded';
                                            match.s.cr.ft.a = '';
                                            if (!_.isUndefined(match.s.isC.ft_remark) && !_.isNull(match.s.isC.ft_remark)) {
                                                match.s.cr.ft.h = match.s.isC.ft_remark;
                                            }
                                        }
                                        const {
                                            cr,
                                            isC
                                        } = match.s;
                                        match.s = {};
                                        match.s.cr = cr;
                                        match.s.isC = isC;

                                        delete match._id;
                                        delete match.bpl;
                                        delete match.bp;
                                        delete match.rm;
                                        delete match.r;
                                        delete match.hasParlay;
                                        delete match.i;
                                    });
                                    delete league._id;
                                    delete league.r;
                                    delete league.rp;
                                    delete league.rm;
                                    delete league.__v;
                                })
                                .reject(league => { return _.size(league.m) === 0 })
                                .value();

                            callback(null, footballData);
                        }
                    });
            }
        ),
        (callback =>
            {
                BasketballTrashModel.find(
                    {
                        m: {
                            $elemMatch: {
                                // 's.isCr': true,
                                'd': {
                                    $gte: startDate,
                                    $lt: endDate
                                }
                            }
                        }
                    })
                    .lean()
                    .sort('sk')
                    .exec((err, data) => {
                        if (err) {
                            callback(err, null);
                        } else {
                            const footballTrashData = _.chain(data)
                                .each(league => {
                                    league.m = _.filter(league.m, match => {
                                        return /*match.s.isCr && */match.d >= startDate && match.d <= endDate
                                    });

                                    _.each(league.m, match => {
                                        if (match.s.isH) {
                                            match.s.cr.ft.h = '-';
                                            match.s.cr.ft.a = '-';

                                        } else if (match.s.isL) {
                                            match.s.cr.ft.h = '-';
                                            match.s.cr.ft.a = '-';

                                        } else if (match.s.isE) {
                                            match.s.cr.ft.h = '-';
                                            match.s.cr.ft.a = '-';

                                        } else if (match.s.isC.ht) {
                                            match.s.cr.ft.h = 'Refunded';
                                            match.s.cr.ft.a = '';
                                            if (!_.isUndefined(match.s.isC.ft_remark) && !_.isNull(match.s.isC.ft_remark)) {
                                                match.s.cr.ft.h = match.s.isC.ft_remark;
                                            }
                                        } else if (match.s.isC.ft) {
                                            match.s.cr.ft.h = 'Refunded';
                                            match.s.cr.ft.a = '';
                                            if (!_.isUndefined(match.s.isC.ft_remark) && !_.isNull(match.s.isC.ft_remark)) {
                                                match.s.cr.ft.h = match.s.isC.ft_remark;
                                            }
                                        }

                                        const {
                                            cr,
                                            isC
                                        } = match.s;
                                        match.s = {};
                                        match.s.cr = cr;
                                        match.s.isC = isC;

                                        delete match._id;
                                        delete match.bpl;
                                        delete match.bp;
                                        delete match.rm;
                                        delete match.r;
                                        delete match.hasParlay;
                                        delete match.i;
                                    });
                                    delete league._id;
                                    delete league.r;
                                    delete league.rp;
                                    delete league.rm;
                                    delete league.__v;
                                })
                                .reject(league => { return _.size(league.m) === 0 })
                                .value();

                            callback(null, footballTrashData);
                        }
                    });
            }
        )], (err, asyncSeriesResponse) => {


        // let a = _.first(asyncSeriesResponse);
        // let b = _.last(asyncSeriesResponse);
        //
        //
        // a.push(...b);


        const today = _.first(asyncSeriesResponse);
        const temp  = _.last(asyncSeriesResponse);

        const todayLeagueKey = filterLeagueKey(today);
        const tempLeagueKey = filterLeagueKey(temp);

        const differenceLeagueKey = _.difference(tempLeagueKey, todayLeagueKey);

        if (!_.isEmpty(differenceLeagueKey)) {
            const leagueEarly =_.filter(temp, league => {
                return  _.contains(differenceLeagueKey, league.k);
            });
            today.push(...leagueEarly);
        }

        // let response = _.first(asyncSeriesResponse);
        // if (_.size(response) === 0) {
        //     response = _.last(asyncSeriesResponse);
        // }

        return res.send(
            {
                code: 0,
                message: "success",
                // result : response,
                result : _.sortBy(today, 'sk')
            }
        );
    });

});

const calculateBetPrice = (price, round) => {
    let percent = 0;
    switch (round) {
        case 0:
            percent = 0;
            break;
        case 1:
            percent = 40;
            break;
        case 2:
            percent = 80;
            break;
        case 3:
            percent = 90;
            break;
        case 4:
            percent = 90;
            break;
        case 5:
            percent = 90;
            break;
    }
    return (price * ( (100 - percent) / 100)).toFixed(2);
};
const calculateBetPriceByTime = (price, current, match) => {
    if (price <= 5000) {
        return price;
    } else {
        let diff =(current.getTime() - match.getTime()) / 1000;
        diff /= (60 * 60);

        const gapHour = Math.abs(Math.round(diff));
        let percent = 0;

        if (12 <= gapHour) {
            percent = 70;
        } else if (11 === gapHour || 10 === gapHour || 9 === gapHour || 8 === gapHour || 7 === gapHour) {
            percent = 65;
        } else if (6 === gapHour || 5 === gapHour || 4 === gapHour ) {
            percent = 50;
        } else if (3 === gapHour || 2 === gapHour ) {
            percent = 35;
        } else {
            percent = 0;
        }

        return roundTo(parseFloat(decrease(price, percent)), 0);
    }
};
const decrease = (price, percent) => {
    const result = (price * ( (100 - percent) / 100)).toFixed(2);
    // console.log(`${price} - ${percent}% = ${result}`);
    return result;
};
const convertMinMaxByCurrency = (value, currency, membercurrency) => {
    // console.log('###########################');
    // console.log('[BF]value      : ', value);
    // console.log('membercurrency : ', membercurrency);

    let minMax = value;
    if (_.isUndefined(membercurrency) || _.isNull(membercurrency)) {
        return minMax;
    } else {
        const predicate = JSON.parse(`{"currencyName":"${membercurrency}"}`);
        const obj = _.chain(currency)
            .findWhere(predicate)
            .pick('currencyTHB')
            .value();
        if (!_.isUndefined(obj.currencyTHB)) {
            // console.log('               : ',obj.currencyTHB);

            if (_.isEqual(membercurrency, "IDR") || _.isEqual(membercurrency, "VND")) {
                minMax = Math.ceil(value * obj.currencyTHB);
            } else {
                minMax = Math.ceil(value / obj.currencyTHB);
            }
            // console.log('[AF]value      : ', minMax);
            // console.log('###########################');
            return minMax;
        } else {
            return minMax;
        }
    }

};
const filterLeagueKey = (league) => {
    return _.chain(league)
        .flatten(true)
        .pluck('k')
        .sort()
        .value();
};
module.exports = router;