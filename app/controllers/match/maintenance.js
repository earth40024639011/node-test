const express = require('express');
const router  = express.Router();
const RedisService = require('../../common/redisService');
const CLIENT_NAME = process.env.CLIENT_NAME || 'SPORTBOOK88';

//[2]
router.put('/turn_off', (req, res) => {
    RedisService.setIsMaintenance(CLIENT_NAME, false, (error, result) => {
        return res.send({
            code: 0,
            message: "success"
        });
    });
});

//[2]
router.put('/turn_on', (req, res) => {
    RedisService.setIsMaintenance(CLIENT_NAME, true, (error, result) => {
        return res.send({
            code: 0,
            message: "success"
        });
    });
});

module.exports = router;