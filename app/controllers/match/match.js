const FootballModel = require('../../models/football.model');
const FootballTrashModel = require('../../models/footballTrash.model');
const Commons = require('../../common/commons');
const express = require('express');
const router  = express.Router();
const request = require('request');
const _ = require('underscore');
const async = require("async");
const clone = require('clone');
const roundTo = require('round-to');

const MemberService = require('../../common/memberService');
const RedisService = require('../../common/redisService');

const NodeCache = require( "node-cache" );
const cache = new NodeCache( { stdTTL: 5, checkperiod: 0 } );
const cache_count = new NodeCache( { stdTTL: 60, checkperiod: 0 } );
const CACHE_IP = process.env.CACHE_IP || 'isn-backend.api-hub.com';
const URL_HDP = `http://${CACHE_IP}:8001/cache/hdp`;
const URL_LIVE = `http://${CACHE_IP}:8001/cache/live`;
const URL_CACHE_HDP_SINGLE_MATCH = `http://${CACHE_IP}:8001/cache/hdp_single_match`;
const URL_CACHE_LIVE_SINGLE_MATCH = `http://${CACHE_IP}:8001/cache/live_single_match`;

const CLIENT_NAME = process.env.CLIENT_NAME || 'SPORTBOOK88';
const redis     = require("ioredis");
const redisIp   = process.env.REDIS_IP || "redis.api-hub.com";
const redisPort = process.env.REDIS_PORT || "6379";
const client_redis = new redis({
    port: redisPort,          // Redis port
    host: redisIp,   // Redis host
    family: 4,           // 4 (IPv4) or 6 (IPv6)
    db: 1
});
// const client_redis = redis.createClient({host: redisIp, port: redisPort});
// client_redis.select(1);

router.get('/hdp', (req, res) => {
    const day = req.query.d || 0;
    const month = req.query.m || 0;
    const startDate = new Date(_.now());
    const endDate = new Date(_.now());

    console.log('====== Start Date ======' + day + ":" + month);
    startDate.setMonth(month - 1);
    startDate.setDate(day);
    startDate.setHours(18);
    startDate.setMinutes(0);
    startDate.setSeconds(0);
    console.log(startDate);

    console.log('====== End Date ======' + day + ":" + month);
    endDate.setMonth(month - 1);
    endDate.setDate(day);
    endDate.setDate(endDate.getDate() + 1);
    endDate.setHours(17);
    endDate.setMinutes(59);
    endDate.setSeconds(59);
    console.log(endDate);

    console.log(startDate);
    console.log(endDate);

    const now_date = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    console.log('============== now_date ==============');
    console.log(now_date);
    console.log('============== now_date ==============');
    const membercurrency = req.userInfo.currency;
    async.parallel([
            (callback) => {
                MemberService.getLimitSetting(req.userInfo._id, (errLimitSetting, dataLimitSetting)=>{
                    if(errLimitSetting){
                        callback(errLimitSetting, null);
                    } else {
                        callback(null, dataLimitSetting.limitSetting);
                    }
                });
            },

            (callback) => {
                MemberService.getOddAdjustment( req.userInfo.username, (err, data) => {
                    if (err) {
                        console.log('Get Odd Adjust error');
                        callback(err, null);
                    } else {
                        callback(null, data);
                    }
                });
            },

            (callback) => {
                RedisService.getCurrency((error, result) => {
                    if (error) {
                        callback(null, false);
                    } else {
                        callback(null, result);
                    }
                })
            }
        ],
        (error, result) => {
            if (error) {
                return res.send({
                    code    : 9999,
                    message : error
                });
            } else {
                const [
                    limitSetting,
                    oddAdjust,
                    currency
                ] = result;

                RedisService.footballHDP((error, response) => {
                    if (error) {
                        console.error('[1.1 ] 999 : ==> \n', error);
                        return res.send(
                            {
                                code: 9999,
                                message: error
                            });
                    } else {
                        FootballModel.find({
                            m: {
                                $elemMatch: {
                                    's.isH': true,
                                    isToday : false,
                                    'd': {
                                        $gte: startDate,
                                        $lt: endDate
                                    }
                                }
                            }
                        })
                            .lean()
                            .sort('sk')
                            .exec((err, data) => {
                                if (err) {
                                    console.error('[1.2 ] 999 : ==> \n', err);
                                    return res.send({
                                        message: "success",
                                        result: [],
                                        code: 0
                                    });
                                }  else if (_.size(data) === 0) {
                                    console.log('[1.3 ] Data not found');
                                    return res.send({
                                        message: "success",
                                        result: [],
                                        code: 0
                                    });
                                } else {
                                    const dataClone = clone(data);
                                    let matchHDPFromDBArray = _.chain(data)
                                        .each(league => {
                                            league.m =
                                                _.filter(league.m, match => {
                                                    return match.s.isH && match.d >= now_date && match.d >= startDate && match.d <= endDate
                                                });
                                        })
                                        .value();

                                    const matchIdHDPFromDBArray = _.chain(matchHDPFromDBArray)
                                        .flatten(true)
                                        .pluck('m')
                                        .flatten(true)
                                        .pluck('id')
                                        .value();

                                    if (_.size(matchIdHDPFromDBArray) !== 0) {
                                        console.log("[1.4] HDP match form database : " + JSON.stringify(matchIdHDPFromDBArray));
                                        const result = response;//JSON.parse(response.body);
                                        const matchHDPAPIArray = _.chain(result)
                                            .flatten(true)
                                            .pluck('m')
                                            .value();

                                        const matchIdAPIArray = _.chain(matchHDPAPIArray)
                                            .flatten(true)
                                            .pluck('id')
                                            .value();
                                        // console.log("[1.5] HDP match form API : " + JSON.stringify(matchIdAPIArray));

                                        let resultMatchedArray = _.intersection(matchIdHDPFromDBArray, matchIdAPIArray);
                                        console.log("[1.6] HDP matched : " + JSON.stringify(resultMatchedArray));

                                        let resultTotalArray = _.chain(result)
                                            .each(league => {
                                                // delete league._id;
                                                league.m = _.filter(league.m, match => {
                                                    // delete match._id;
                                                    return _.contains(resultMatchedArray, match.id)
                                                });
                                            })
                                            .reject(league => { return _.size(league.m) === 0 })
                                            .value();

                                        // console.log("=====DB=====");

                                        matchHDPFromDBArray = _.chain(data)
                                            .each(league => {
                                                _.each(league.m, match => {
                                                    match.bp = _.filter(match.bp, betPrise => { return betPrise.isOn && betPrise.isOnM; });

                                                    let i = 0;
                                                    _.each(match.bp, bp => {

                                                        const resultBP = _.chain(resultTotalArray)
                                                            .filter(leagueAPI => {
                                                                return _.isEqual(leagueAPI.k, league.k)
                                                            })
                                                            .flatten(true)
                                                            .pluck('m')
                                                            .flatten(true)
                                                            .filter(matchAPI => {
                                                                match.i = matchAPI.i;
                                                                return _.isEqual(matchAPI.k, match.k)
                                                            })
                                                            .each(matchAPI => {
                                                                match.i = matchAPI.i;
                                                            })
                                                            .pluck('bp')
                                                            .flatten(true)
                                                            .filter(betPrice => {
                                                                return _.size(_.intersection(bp.allKey, betPrice.allKey)) > 0
                                                            })
                                                            .value();

                                                        if (!_.isUndefined(bp.allKey) &&
                                                            !_.isUndefined(_.first(resultBP)) &&
                                                            !_.isUndefined(_.first(resultBP).allKey) &&
                                                            _.size(_.intersection(bp.allKey, _.first(resultBP).allKey)) > 0) {

                                                            const {
                                                                rem
                                                            } = match.bp[i];
                                                            match.bp[i] = _.first(resultBP);
                                                            match.bp[i].r = rem;
                                                            delete match.bp[i].r.isLock;
                                                        }
                                                        i++;
                                                    });
                                                });
                                                league.m = _.reject(league.m, match => { return _.size(match.bp) === 0 });
                                            })
                                            .reject(league => { return _.size(league.m) === 0 })
                                            .each(league => {
                                                _.each(league.m, match => {
                                                    if(league.rp && league.rp.hdp){
                                                        if(_.size(match.bp) > 0){

                                                            match.bp  = _.uniq(match.bp, (betPrice) => {
                                                                return betPrice.allKey.toString();
                                                            });

                                                            _.each(match.bp, bp =>{

                                                                const resultBP = _.first(_.chain(dataClone)
                                                                    .filter(leagueAPI => {
                                                                        return _.isEqual(leagueAPI.k, league.k)
                                                                    })
                                                                    .flatten(true)
                                                                    .pluck('m')
                                                                    .flatten(true)
                                                                    .filter(matchAPI => {
                                                                        return _.isEqual(matchAPI.k, match.k)
                                                                    })
                                                                    .each(matchAPI => {
                                                                        match.i = matchAPI.i;
                                                                    })
                                                                    .pluck('bp')
                                                                    .flatten(true)
                                                                    .filter(betPrice => {
                                                                        return _.size(_.intersection(bp.allKey, betPrice.allKey)) > 0;
                                                                    })
                                                                    .pluck('rp')
                                                                    .flatten(true)
                                                                    .value());

                                                                let odd = _.filter(oddAdjust, (o)=>{
                                                                    return o.matchId === match.id;
                                                                });

                                                                let ou1st = {o: 0, u: 0};
                                                                let ou = {o: 0, u: 0};
                                                                let oe = {o: 0, e: 0};
                                                                let oe1st = {o: 0, e: 0};
                                                                let ah = {home: 0, away: 0};
                                                                let ah1st = {home: 0, away: 0};

                                                                // console.log('odd ====> ', odd);

                                                                if(_.size(odd) > 0){
                                                                    _.each(odd, (o)=>{
                                                                        if(o.prefix === 'ah' && resultBP.ah){
                                                                            if(o.key === 'hpk' && o.value === resultBP.ah.hpk){
                                                                                ah = {
                                                                                    home: -o.count,
                                                                                    away: o.count
                                                                                }
                                                                            } else if(o.key === 'apk' && o.value === resultBP.ah.apk){
                                                                                ah = {
                                                                                    home: o.count,
                                                                                    away: -o.count
                                                                                }
                                                                            }
                                                                        } else if(o.prefix === 'ah1st' && resultBP.ah1st){

                                                                            if(o.key === 'hpk' && o.value === resultBP.ah1st.hpk){
                                                                                ah1st = {
                                                                                    home: -o.count,
                                                                                    away: o.count
                                                                                }
                                                                            } else if(o.key === 'apk' && o.value === resultBP.ah1st.apk){
                                                                                ah1st = {
                                                                                    home: o.count,
                                                                                    away: -o.count
                                                                                }
                                                                            }
                                                                        } else if(o.prefix === 'ou' && resultBP.ou){
                                                                            // console.log(o.value , resultBP.ou.opk);
                                                                            if(o.key === 'opk' && o.value === resultBP.ou.opk){
                                                                                ou = {
                                                                                    o: -o.count,
                                                                                    u: o.count
                                                                                }
                                                                            } else if(o.key === 'upk' && o.value === resultBP.ou.upk){
                                                                                ou = {
                                                                                    o: o.count,
                                                                                    u: -o.count
                                                                                }
                                                                            }
                                                                        } else if(o.prefix === 'ou1st' && resultBP.ou1st){
                                                                            if(o.key === 'opk' && o.value === resultBP.ou1st.opk){
                                                                                ou1st = {
                                                                                    o: -o.count,
                                                                                    u: o.count
                                                                                }
                                                                            } else if(o.key === 'upk' && o.value === resultBP.ou1st.upk){
                                                                                ou1st = {
                                                                                    o: o.count,
                                                                                    u: -o.count
                                                                                }
                                                                            }
                                                                        } else if(o.prefix === 'oe' && resultBP.oe){
                                                                            if(o.key === 'ok' && o.value === resultBP.oe.ok){
                                                                                oe = {
                                                                                    o: -o.count,
                                                                                    e: o.count
                                                                                }
                                                                            } else if(o.key === 'ek' && o.value === resultBP.oe.ek){
                                                                                oe = {
                                                                                    o: o.count,
                                                                                    e: -o.count
                                                                                }
                                                                            }
                                                                        } else if(o.prefix === 'oe1st' && resultBP.oe1st){
                                                                            if(o.key === 'ok' && o.value === resultBP.oe1st.ok){
                                                                                oe1st = {
                                                                                    o: -o.count,
                                                                                    e: o.count
                                                                                }
                                                                            } else if(o.key === 'ek' && o.value === resultBP.oe1st.ek){
                                                                                oe1st = {
                                                                                    o: o.count,
                                                                                    e: -o.count
                                                                                }
                                                                            }
                                                                        }
                                                                    });
                                                                }

                                                                // console.log('ah : ', ah, ', ah1st : ', ah1st, ', ou : ', ou, ', ou1st : ', ou1st, ', oe : ', oe, ', oe1st : ', oe1st);

                                                                // ---------------------------  ou1st  -----------------------------
                                                                if(bp.ou1st && resultBP && resultBP.ou1st){
                                                                    if(bp.ou1st.op || bp.ou1st.up){
                                                                        Commons.cal_ou(bp.ou1st.op, bp.ou1st.up, league.rp.hdp.ou1st.f, league.rp.hdp.ou1st.u, resultBP.ou1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ou1st, (err, data)=>{
                                                                            if(data){
                                                                                bp.ou1st.op = data.op;
                                                                                bp.ou1st.up = data.up;
                                                                            }
                                                                        })
                                                                    }
                                                                }

                                                                // ---------------------------  ou  ----------------------------------
                                                                if(bp.ou && resultBP && resultBP.ou){
                                                                    if(bp.ou.op || bp.ou.up){
                                                                        Commons.cal_ou(bp.ou.op, bp.ou.up, league.rp.hdp.ou.f, league.rp.hdp.ou.u, resultBP.ou, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ou, (err, data)=>{
                                                                            if(data){
                                                                                bp.ou.op = data.op;
                                                                                bp.ou.up = data.up;
                                                                            }
                                                                        })
                                                                    }
                                                                }

                                                                // ---------------------------  oe1st  ----------------------------------
                                                                if(bp.oe1st && resultBP && resultBP.oe1st){
                                                                    if(bp.oe1st.o || bp.oe1st.e){
                                                                        Commons.cal_oe(bp.oe1st.o, bp.oe1st.e, league.rp.hdp.oe1st.f, league.rp.hdp.oe1st.u, resultBP.oe1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, oe1st, (err, data)=>{
                                                                            if(data){
                                                                                bp.oe1st.o = data.o;
                                                                                bp.oe1st.e = data.e;
                                                                            }
                                                                        })
                                                                    }
                                                                }

                                                                // ---------------------------  oe  ----------------------------------
                                                                if(bp.oe && resultBP && resultBP.oe){
                                                                    if(bp.oe.o || bp.oe.e){
                                                                        Commons.cal_oe(bp.oe.o, bp.oe.e, league.rp.hdp.oe.f, league.rp.hdp.oe.u, resultBP.oe, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, oe, (err, data)=>{
                                                                            if(data){
                                                                                bp.oe.o = data.o;
                                                                                bp.oe.e = data.e;
                                                                            }
                                                                        })
                                                                    }
                                                                }

                                                                // ---------------------------  ah1st  ----------------------------------
                                                                if(bp.ah1st && resultBP && resultBP.ah1st){
                                                                    if(bp.ah1st.h || bp.ah1st.a){
                                                                        Commons.cal_ah(bp.ah1st.h, bp.ah1st.a, bp.ah1st.hp, bp.ah1st.ap, league.rp.hdp.ah1st.f, league.rp.hdp.ah1st.u, resultBP.ah1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ah1st, (err, data)=>{
                                                                            if(data){
                                                                                bp.ah1st.hp = data.hp;
                                                                                bp.ah1st.ap = data.ap;
                                                                            }
                                                                        })
                                                                    }
                                                                }

                                                                // ---------------------------  ah  ----------------------------------
                                                                if(bp.ah && resultBP && resultBP.ah){
                                                                    if(bp.ah.h || bp.ah.a){
                                                                        Commons.cal_ah(bp.ah.h, bp.ah.a, bp.ah.hp, bp.ah.ap, league.rp.hdp.ah.f, league.rp.hdp.ah.u, resultBP.ah, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ah, (err, data)=>{
                                                                            if(data){
                                                                                bp.ah.hp = data.hp;
                                                                                bp.ah.ap = data.ap;
                                                                            }
                                                                        })
                                                                    }
                                                                }

                                                                // ---------------------------  x121st  ----------------------------------
                                                                if(bp.x121st && resultBP && resultBP.x121st){
                                                                    if(bp.x121st.h || bp.x121st.a || bp.x121st.d){
                                                                        Commons.cal_x12(bp.x121st.h, bp.x121st.a, bp.x121st.d, resultBP.x121st, (err, data)=>{
                                                                            if(data){
                                                                                bp.x121st.h = data.h;
                                                                                bp.x121st.a = data.a;
                                                                                bp.x121st.d = data.d;
                                                                            }
                                                                        })
                                                                    }
                                                                }

                                                                // ---------------------------  x12  ----------------------------------
                                                                if(bp.x12 && resultBP && resultBP.x12){
                                                                    if(bp.x12.h || bp.x12.a || bp.x12.d){
                                                                        Commons.cal_x12(bp.x12.h, bp.x12.a, bp.x12.d, resultBP.x12, (err, data)=>{
                                                                            if(data){
                                                                                bp.x12.h = data.h;
                                                                                bp.x12.a = data.a;
                                                                                bp.x12.d = data.d;
                                                                            }
                                                                        })
                                                                    }
                                                                }
                                                                delete bp._id;
                                                                delete bp.isOn;
                                                                delete bp.isOnM;
                                                                delete bp.allKey;
                                                            });
                                                        }
                                                    }
                                                    delete match._id;
                                                    delete match.bpl;
                                                    delete match.rm;
                                                    delete match.rlm;
                                                    delete match.rl;
                                                    delete match.rmem;
                                                    delete match.rem;
                                                    delete match.r;
                                                    delete match.s;
                                                    delete match.isToday;
                                                });
                                                delete league._id;
                                                delete league.rp;
                                                delete league.r;
                                                delete league.rm;
                                                delete league.rl;
                                                delete league.rlm;
                                                delete league.rmem;
                                                delete league.__v;
                                            })
                                            .each(league => {
                                                const rem = league.rem;

                                                _.each(league.m, match => {
                                                    let rule = league.rem;
                                                    match.r = {};
                                                    match.r.pm = rem.pm;
                                                    if (!_.isUndefined(match.srem) && !_.isUndefined(match.srem.pm) && match.srem.isOn) {
                                                        rule = match.srem;
                                                        match.r.pm = match.srem.pm;

                                                    }

                                                    let round = 0;
                                                    console.log(`       ${match.n.en.h} vs ${match.n.en.a}`);
                                                    _.each(match.bp, betPrice => {
                                                        // console.log(`       ============== REM ============== ${match.id} BetPrice = ${round}`);
                                                        betPrice.r = {};
                                                        betPrice.r.ah = {};
                                                        betPrice.r.ou = {};
                                                        betPrice.r.x12 = {};
                                                        betPrice.r.oe = {};
                                                        betPrice.r.ah1st = {};
                                                        betPrice.r.ou1st = {};
                                                        betPrice.r.x121st = {};
                                                        betPrice.r.oe1st = {};
                                                        const {
                                                            ah,
                                                            ou,
                                                            x12,
                                                            oe,
                                                            ah1st,
                                                            ou1st,
                                                            x121st,
                                                            oe1st
                                                        } = rule;

                                                        betPrice.r.ah.ma = calculateBetPrice(Math.min(ah.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                                                        betPrice.r.ah.mi = ah.mi;

                                                        betPrice.r.ou.ma = calculateBetPrice(Math.min(ou.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                                                        betPrice.r.ou.mi = ou.mi;


                                                        betPrice.r.x12.ma = calculateBetPrice(Math.min(x12.ma, limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet), round);
                                                        betPrice.r.x12.mi = x12.mi;

                                                        betPrice.r.oe.ma = calculateBetPrice(Math.min(oe.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                                                        betPrice.r.oe.mi = oe.mi;

                                                        betPrice.r.ah1st.ma = calculateBetPrice(Math.min(ah1st.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                                                        betPrice.r.ah1st.mi = ah1st.mi;

                                                        betPrice.r.ou1st.ma = calculateBetPrice(Math.min(ou1st.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                                                        betPrice.r.ou1st.mi = ou1st.mi;

                                                        betPrice.r.x121st.ma = calculateBetPrice(Math.min(x121st.ma, limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet), round);
                                                        betPrice.r.x121st.mi = x121st.mi;

                                                        betPrice.r.oe1st.ma = calculateBetPrice(Math.min(oe1st.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                                                        betPrice.r.oe1st.mi = oe1st.mi;
                                                        round++;


                                                        // betPrice.nr = {};
                                                        // betPrice.nr.ah = {};
                                                        // betPrice.nr.ou = {};
                                                        // betPrice.nr.x12 = {};
                                                        // betPrice.nr.oe = {};
                                                        // betPrice.nr.ah1st = {};
                                                        // betPrice.nr.ou1st = {};
                                                        // betPrice.nr.x121st = {};
                                                        // betPrice.nr.oe1st = {};

                                                        betPrice.r.ah.ma = convertMinMaxByCurrency(betPrice.r.ah.ma, currency, membercurrency);
                                                        betPrice.r.ah.mi = convertMinMaxByCurrency(betPrice.r.ah.mi, currency, membercurrency);

                                                        betPrice.r.ou.ma = convertMinMaxByCurrency(betPrice.r.ou.ma, currency, membercurrency);
                                                        betPrice.r.ou.mi = convertMinMaxByCurrency(betPrice.r.ou.mi, currency, membercurrency);

                                                        betPrice.r.x12.ma = convertMinMaxByCurrency(betPrice.r.x12.ma, currency, membercurrency);
                                                        betPrice.r.x12.mi = convertMinMaxByCurrency(betPrice.r.x12.mi, currency, membercurrency);

                                                        betPrice.r.oe.ma = convertMinMaxByCurrency(betPrice.r.oe.ma, currency, membercurrency);
                                                        betPrice.r.oe.mi = convertMinMaxByCurrency(betPrice.r.oe.mi, currency, membercurrency);

                                                        betPrice.r.ah1st.ma = convertMinMaxByCurrency(betPrice.r.ah1st.ma, currency, membercurrency);
                                                        betPrice.r.ah1st.mi = convertMinMaxByCurrency(betPrice.r.ah1st.mi, currency, membercurrency);

                                                        betPrice.r.ou1st.ma = convertMinMaxByCurrency(betPrice.r.ou1st.ma, currency, membercurrency);
                                                        betPrice.r.ou1st.mi = convertMinMaxByCurrency(betPrice.r.ou1st.mi, currency, membercurrency);

                                                        betPrice.r.x121st.ma = convertMinMaxByCurrency(betPrice.r.x121st.ma, currency, membercurrency);
                                                        betPrice.r.x121st.mi = convertMinMaxByCurrency(betPrice.r.x121st.mi, currency, membercurrency);

                                                        betPrice.r.oe1st.ma = convertMinMaxByCurrency(betPrice.r.oe1st.ma, currency, membercurrency);
                                                        betPrice.r.oe1st.mi = convertMinMaxByCurrency(betPrice.r.oe1st.mi, currency, membercurrency);
                                                    });
                                                });
                                                delete league.rem;
                                            })
                                            .value();

                                        return res.send({
                                            code: 0,
                                            message: "success",
                                            result: matchHDPFromDBArray
                                        });

                                    } else {
                                        return res.send({
                                            message: "success",
                                            result: [],
                                            code: 0
                                        });
                                    }
                                }
                            });
                    }
                });
            }
        }
    );

});

router.get('/hdp_all', (req, res) => {

    const now_date = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    console.log('============== now_date ==============');
    console.log(now_date);
    console.log('============== now_date ==============');
    const membercurrency = req.userInfo.currency;
    async.series([
        (callback) => {
            MemberService.getLimitSetting(req.userInfo._id, (errLimitSetting, dataLimitSetting)=>{
                if(errLimitSetting){
                    callback(errLimitSetting, null);
                } else {
                    callback(null, dataLimitSetting.limitSetting);
                }
            });
        },

        (callback) => {
            MemberService.getOddAdjustment( req.userInfo.username, (err, data) => {
                if (err) {
                    console.log('Get Odd Adjust error');
                    callback(err, null);
                } else {
                    callback(null, data);
                }
            });
        },

        (callback) => {
            RedisService.getCurrency((error, result) => {
                if (error) {
                    callback(null, false);
                } else {
                    callback(null, result);
                }
            })
        }
    ], (errAsync, result)=>{
        if(errAsync){
            return res.send({
                message: "error",
                result: errAsync,
                code: 998
            });
        } else {

            const [
                limitSetting,
                oddAdjust,
                currency
            ] = result;

            RedisService.footballHDP((error, response) => {
                if (error) {
                    console.error('[1.1 ] 999 : ==> \n', error);
                    return res.send(
                        {
                            code: 9999,
                            message: error
                        });
                } else {
                    FootballModel.find({
                        m: {
                            $elemMatch: {
                                's.isH': true,
                                isToday : false
                            }
                        }
                    })
                        .lean()
                        .sort('sk')
                        .exec((err, data) => {
                            if (err) {
                                console.error('[1.2 ] 999 : ==> \n', err);
                                return res.send({
                                    message: "success",
                                    result: [],
                                    code: 0
                                });
                            }  else if (_.size(data) === 0) {
                                console.log('[1.3 ] Data not found');
                                return res.send({
                                    message: "success",
                                    result: [],
                                    code: 0
                                });
                            } else {
                                const dataClone = clone(data);
                                let matchHDPFromDBArray = _.chain(data)
                                    .each(league => {
                                        league.m =
                                            _.filter(league.m, match => {
                                                return match.s.isH && match.d >= now_date && !match.isToday
                                            });
                                    })
                                    .value();

                                const matchIdHDPFromDBArray = _.chain(matchHDPFromDBArray)
                                    .flatten(true)
                                    .pluck('m')
                                    .flatten(true)
                                    .pluck('id')
                                    .value();

                                if (_.size(matchIdHDPFromDBArray) !== 0) {
                                    console.log("[1.4] HDP match form database : " + JSON.stringify(matchIdHDPFromDBArray));
                                    const result = response;//JSON.parse(response.body);
                                    const matchHDPAPIArray = _.chain(result)
                                        .flatten(true)
                                        .pluck('m')
                                        .value();

                                    const matchIdAPIArray = _.chain(matchHDPAPIArray)
                                        .flatten(true)
                                        .pluck('id')
                                        .value();
                                    // console.log("[1.5] HDP match form API : " + JSON.stringify(matchIdAPIArray));

                                    let resultMatchedArray = _.intersection(matchIdHDPFromDBArray, matchIdAPIArray);
                                    console.log("[1.6] HDP matched : " + JSON.stringify(resultMatchedArray));

                                    let resultTotalArray = _.chain(result)
                                        .each(league => {
                                            // delete league._id;
                                            league.m = _.filter(league.m, match => {
                                                // delete match._id;
                                                return _.contains(resultMatchedArray, match.id)
                                            });
                                        })
                                        .reject(league => { return _.size(league.m) === 0 })
                                        .value();

                                    // console.log("=====DB=====");

                                    matchHDPFromDBArray = _.chain(data)
                                        .each(league => {
                                            _.each(league.m, match => {
                                                match.bp = _.filter(match.bp, betPrise => { return betPrise.isOn && betPrise.isOnM; });

                                                let i = 0;
                                                _.each(match.bp, bp => {

                                                    const resultBP = _.chain(resultTotalArray)
                                                        .filter(leagueAPI => {
                                                            return _.isEqual(leagueAPI.k, league.k)
                                                        })
                                                        .flatten(true)
                                                        .pluck('m')
                                                        .flatten(true)
                                                        .filter(matchAPI => {
                                                            match.i = matchAPI.i;
                                                            return _.isEqual(matchAPI.k, match.k)
                                                        })
                                                        .each(matchAPI => {
                                                            match.i = matchAPI.i;
                                                        })
                                                        .pluck('bp')
                                                        .flatten(true)
                                                        .filter(betPrice => {
                                                            return _.size(_.intersection(bp.allKey, betPrice.allKey)) > 0
                                                        })
                                                        .value();

                                                    if (!_.isUndefined(bp.allKey) &&
                                                        !_.isUndefined(_.first(resultBP)) &&
                                                        !_.isUndefined(_.first(resultBP).allKey) &&
                                                        _.size(_.intersection(bp.allKey, _.first(resultBP).allKey)) > 0) {

                                                        const {
                                                            rem
                                                        } = match.bp[i];
                                                        match.bp[i] = _.first(resultBP);
                                                        match.bp[i].r = rem;
                                                        delete match.bp[i].r.isLock;
                                                    }
                                                    i++;
                                                });
                                            });
                                            league.m = _.reject(league.m, match => { return _.size(match.bp) === 0 });
                                        })
                                        .reject(league => { return _.size(league.m) === 0 })
                                        .each(league => {
                                            _.each(league.m, match => {
                                                if(league.rp && league.rp.hdp){
                                                    if(_.size(match.bp) > 0){

                                                        match.bp  = _.uniq(match.bp, (betPrice) => {
                                                            return betPrice.allKey.toString();
                                                        });

                                                        _.each(match.bp, bp =>{

                                                            const resultBP = _.first(_.chain(dataClone)
                                                                .filter(leagueAPI => {
                                                                    return _.isEqual(leagueAPI.k, league.k)
                                                                })
                                                                .flatten(true)
                                                                .pluck('m')
                                                                .flatten(true)
                                                                .filter(matchAPI => {
                                                                    return _.isEqual(matchAPI.k, match.k)
                                                                })
                                                                .each(matchAPI => {
                                                                    match.i = matchAPI.i;
                                                                })
                                                                .pluck('bp')
                                                                .flatten(true)
                                                                .filter(betPrice => {
                                                                    return _.size(_.intersection(bp.allKey, betPrice.allKey)) > 0;
                                                                })
                                                                .pluck('rp')
                                                                .flatten(true)
                                                                .value());

                                                            let odd = _.filter(oddAdjust, (o)=>{
                                                                return o.matchId === match.id;
                                                            });

                                                            let ou1st = {o: 0, u: 0};
                                                            let ou = {o: 0, u: 0};
                                                            let oe = {o: 0, e: 0};
                                                            let oe1st = {o: 0, e: 0};
                                                            let ah = {home: 0, away: 0};
                                                            let ah1st = {home: 0, away: 0};

                                                            // console.log('odd ====> ', odd);

                                                            if(_.size(odd) > 0){
                                                                _.each(odd, (o)=>{
                                                                    if(o.prefix === 'ah' && resultBP.ah){
                                                                        if(o.key === 'hpk' && o.value === resultBP.ah.hpk){
                                                                            ah = {
                                                                                home: -o.count,
                                                                                away: o.count
                                                                            }
                                                                        } else if(o.key === 'apk' && o.value === resultBP.ah.apk){
                                                                            ah = {
                                                                                home: o.count,
                                                                                away: -o.count
                                                                            }
                                                                        }
                                                                    } else if(o.prefix === 'ah1st' && resultBP.ah1st){

                                                                        if(o.key === 'hpk' && o.value === resultBP.ah1st.hpk){
                                                                            ah1st = {
                                                                                home: -o.count,
                                                                                away: o.count
                                                                            }
                                                                        } else if(o.key === 'apk' && o.value === resultBP.ah1st.apk){
                                                                            ah1st = {
                                                                                home: o.count,
                                                                                away: -o.count
                                                                            }
                                                                        }
                                                                    } else if(o.prefix === 'ou' && resultBP.ou){
                                                                        // console.log(o.value , resultBP.ou.opk);
                                                                        if(o.key === 'opk' && o.value === resultBP.ou.opk){
                                                                            ou = {
                                                                                o: -o.count,
                                                                                u: o.count
                                                                            }
                                                                        } else if(o.key === 'upk' && o.value === resultBP.ou.upk){
                                                                            ou = {
                                                                                o: o.count,
                                                                                u: -o.count
                                                                            }
                                                                        }
                                                                    } else if(o.prefix === 'ou1st' && resultBP.ou1st){
                                                                        if(o.key === 'opk' && o.value === resultBP.ou1st.opk){
                                                                            ou1st = {
                                                                                o: -o.count,
                                                                                u: o.count
                                                                            }
                                                                        } else if(o.key === 'upk' && o.value === resultBP.ou1st.upk){
                                                                            ou1st = {
                                                                                o: o.count,
                                                                                u: -o.count
                                                                            }
                                                                        }
                                                                    } else if(o.prefix === 'oe' && resultBP.oe){
                                                                        if(o.key === 'ok' && o.value === resultBP.oe.ok){
                                                                            oe = {
                                                                                o: -o.count,
                                                                                e: o.count
                                                                            }
                                                                        } else if(o.key === 'ek' && o.value === resultBP.oe.ek){
                                                                            oe = {
                                                                                o: o.count,
                                                                                e: -o.count
                                                                            }
                                                                        }
                                                                    } else if(o.prefix === 'oe1st' && resultBP.oe1st){
                                                                        if(o.key === 'ok' && o.value === resultBP.oe1st.ok){
                                                                            oe1st = {
                                                                                o: -o.count,
                                                                                e: o.count
                                                                            }
                                                                        } else if(o.key === 'ek' && o.value === resultBP.oe1st.ek){
                                                                            oe1st = {
                                                                                o: o.count,
                                                                                e: -o.count
                                                                            }
                                                                        }
                                                                    }
                                                                });
                                                            }

                                                            // console.log('ah : ', ah, ', ah1st : ', ah1st, ', ou : ', ou, ', ou1st : ', ou1st, ', oe : ', oe, ', oe1st : ', oe1st);

                                                            // ---------------------------  ou1st  -----------------------------
                                                            if(bp.ou1st && resultBP && resultBP.ou1st){
                                                                if(bp.ou1st.op || bp.ou1st.up){
                                                                    Commons.cal_ou(bp.ou1st.op, bp.ou1st.up, league.rp.hdp.ou1st.f, league.rp.hdp.ou1st.u, resultBP.ou1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ou1st, (err, data)=>{
                                                                        if(data){
                                                                            bp.ou1st.op = data.op;
                                                                            bp.ou1st.up = data.up;
                                                                        }
                                                                    })
                                                                }
                                                            }

                                                            // ---------------------------  ou  ----------------------------------
                                                            if(bp.ou && resultBP && resultBP.ou){
                                                                if(bp.ou.op || bp.ou.up){
                                                                    Commons.cal_ou(bp.ou.op, bp.ou.up, league.rp.hdp.ou.f, league.rp.hdp.ou.u, resultBP.ou, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ou, (err, data)=>{
                                                                        if(data){
                                                                            bp.ou.op = data.op;
                                                                            bp.ou.up = data.up;
                                                                        }
                                                                    })
                                                                }
                                                            }

                                                            // ---------------------------  oe1st  ----------------------------------
                                                            if(bp.oe1st && resultBP && resultBP.oe1st){
                                                                if(bp.oe1st.o || bp.oe1st.e){
                                                                    Commons.cal_oe(bp.oe1st.o, bp.oe1st.e, league.rp.hdp.oe1st.f, league.rp.hdp.oe1st.u, resultBP.oe1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, oe1st, (err, data)=>{
                                                                        if(data){
                                                                            bp.oe1st.o = data.o;
                                                                            bp.oe1st.e = data.e;
                                                                        }
                                                                    })
                                                                }
                                                            }

                                                            // ---------------------------  oe  ----------------------------------
                                                            if(bp.oe && resultBP && resultBP.oe){
                                                                if(bp.oe.o || bp.oe.e){
                                                                    Commons.cal_oe(bp.oe.o, bp.oe.e, league.rp.hdp.oe.f, league.rp.hdp.oe.u, resultBP.oe, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, oe, (err, data)=>{
                                                                        if(data){
                                                                            bp.oe.o = data.o;
                                                                            bp.oe.e = data.e;
                                                                        }
                                                                    })
                                                                }
                                                            }

                                                            // ---------------------------  ah1st  ----------------------------------
                                                            if(bp.ah1st && resultBP && resultBP.ah1st){
                                                                if(bp.ah1st.h || bp.ah1st.a){
                                                                    Commons.cal_ah(bp.ah1st.h, bp.ah1st.a, bp.ah1st.hp, bp.ah1st.ap, league.rp.hdp.ah1st.f, league.rp.hdp.ah1st.u, resultBP.ah1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ah1st, (err, data)=>{
                                                                        if(data){
                                                                            bp.ah1st.hp = data.hp;
                                                                            bp.ah1st.ap = data.ap;
                                                                        }
                                                                    })
                                                                }
                                                            }

                                                            // ---------------------------  ah  ----------------------------------
                                                            if(bp.ah && resultBP && resultBP.ah){
                                                                if(bp.ah.h || bp.ah.a){
                                                                    Commons.cal_ah(bp.ah.h, bp.ah.a, bp.ah.hp, bp.ah.ap, league.rp.hdp.ah.f, league.rp.hdp.ah.u, resultBP.ah, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ah, (err, data)=>{
                                                                        if(data){
                                                                            bp.ah.hp = data.hp;
                                                                            bp.ah.ap = data.ap;
                                                                        }
                                                                    })
                                                                }
                                                            }

                                                            // ---------------------------  x121st  ----------------------------------
                                                            if(bp.x121st && resultBP && resultBP.x121st){
                                                                if(bp.x121st.h || bp.x121st.a || bp.x121st.d){
                                                                    Commons.cal_x12(bp.x121st.h, bp.x121st.a, bp.x121st.d, resultBP.x121st, (err, data)=>{
                                                                        if(data){
                                                                            bp.x121st.h = data.h;
                                                                            bp.x121st.a = data.a;
                                                                            bp.x121st.d = data.d;
                                                                        }
                                                                    })
                                                                }
                                                            }

                                                            // ---------------------------  x12  ----------------------------------
                                                            if(bp.x12 && resultBP && resultBP.x12){
                                                                if(bp.x12.h || bp.x12.a || bp.x12.d){
                                                                    Commons.cal_x12(bp.x12.h, bp.x12.a, bp.x12.d, resultBP.x12, (err, data)=>{
                                                                        if(data){
                                                                            bp.x12.h = data.h;
                                                                            bp.x12.a = data.a;
                                                                            bp.x12.d = data.d;
                                                                        }
                                                                    })
                                                                }
                                                            }
                                                            delete bp._id;
                                                            delete bp.isOn;
                                                            delete bp.isOnM;
                                                            delete bp.allKey;
                                                        });
                                                    }
                                                }
                                                delete match._id;
                                                delete match.bpl;
                                                delete match.rm;
                                                delete match.rlm;
                                                delete match.rl;
                                                delete match.rmem;
                                                delete match.rem;
                                                delete match.r;
                                                delete match.s;
                                                delete match.isToday;
                                            });
                                            delete league._id;
                                            delete league.rp;
                                            delete league.r;
                                            delete league.rm;
                                            delete league.rl;
                                            delete league.rlm;
                                            delete league.rmem;
                                            delete league.__v;
                                        })
                                        .each(league => {
                                            const rem = league.rem;

                                            _.each(league.m, match => {
                                                let rule = league.rem;
                                                match.r = {};
                                                match.r.pm = rem.pm;
                                                if (!_.isUndefined(match.srem) && !_.isUndefined(match.srem.pm) && match.srem.isOn) {
                                                    rule = match.srem;
                                                    match.r.pm = match.srem.pm;

                                                }

                                                let round = 0;
                                                console.log(`       ${match.n.en.h} vs ${match.n.en.a}`);
                                                _.each(match.bp, betPrice => {
                                                    // console.log(`       ============== REM ============== ${match.id} BetPrice = ${round}`);
                                                    betPrice.r = {};
                                                    betPrice.r.ah = {};
                                                    betPrice.r.ou = {};
                                                    betPrice.r.x12 = {};
                                                    betPrice.r.oe = {};
                                                    betPrice.r.ah1st = {};
                                                    betPrice.r.ou1st = {};
                                                    betPrice.r.x121st = {};
                                                    betPrice.r.oe1st = {};
                                                    const {
                                                        ah,
                                                        ou,
                                                        x12,
                                                        oe,
                                                        ah1st,
                                                        ou1st,
                                                        x121st,
                                                        oe1st
                                                    } = rule;

                                                    betPrice.r.ah.ma = calculateBetPrice(Math.min(ah.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                                                    betPrice.r.ah.mi = ah.mi;

                                                    betPrice.r.ou.ma = calculateBetPrice(Math.min(ou.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                                                    betPrice.r.ou.mi = ou.mi;


                                                    betPrice.r.x12.ma = calculateBetPrice(Math.min(x12.ma, limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet), round);
                                                    betPrice.r.x12.mi = x12.mi;

                                                    betPrice.r.oe.ma = calculateBetPrice(Math.min(oe.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                                                    betPrice.r.oe.mi = oe.mi;

                                                    betPrice.r.ah1st.ma = calculateBetPrice(Math.min(ah1st.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                                                    betPrice.r.ah1st.mi = ah1st.mi;

                                                    betPrice.r.ou1st.ma = calculateBetPrice(Math.min(ou1st.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                                                    betPrice.r.ou1st.mi = ou1st.mi;

                                                    betPrice.r.x121st.ma = calculateBetPrice(Math.min(x121st.ma, limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet), round);
                                                    betPrice.r.x121st.mi = x121st.mi;

                                                    betPrice.r.oe1st.ma = calculateBetPrice(Math.min(oe1st.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                                                    betPrice.r.oe1st.mi = oe1st.mi;

                                                    round++;

                                                    // betPrice.nr = {};
                                                    // betPrice.nr.ah = {};
                                                    // betPrice.nr.ou = {};
                                                    // betPrice.nr.x12 = {};
                                                    // betPrice.nr.oe = {};
                                                    // betPrice.nr.ah1st = {};
                                                    // betPrice.nr.ou1st = {};
                                                    // betPrice.nr.x121st = {};
                                                    // betPrice.nr.oe1st = {};
                                                    //
                                                    // betPrice.nr.ah.ma = convertMinMaxByCurrency(ah.ma, currency, membercurrency);
                                                    // betPrice.nr.ah.mi = convertMinMaxByCurrency(ah.mi, currency, membercurrency);
                                                    //
                                                    // betPrice.nr.ou.ma = convertMinMaxByCurrency(ou.ma, currency, membercurrency);
                                                    // betPrice.nr.ou.mi = convertMinMaxByCurrency(ou.mi, currency, membercurrency);
                                                    //
                                                    //
                                                    // betPrice.nr.x12.ma = convertMinMaxByCurrency(x12.mi, currency, membercurrency);
                                                    // betPrice.nr.x12.mi = convertMinMaxByCurrency(x12.mi, currency, membercurrency);
                                                    //
                                                    // betPrice.nr.oe.ma = convertMinMaxByCurrency(oe.ma, currency, membercurrency);
                                                    // betPrice.nr.oe.mi = convertMinMaxByCurrency(oe.mi, currency, membercurrency);
                                                    //
                                                    // betPrice.nr.ah1st.ma = convertMinMaxByCurrency(ah1st.ma, currency, membercurrency);
                                                    // betPrice.nr.ah1st.mi = convertMinMaxByCurrency(ah1st.mi, currency, membercurrency);
                                                    //
                                                    // betPrice.nr.ou1st.ma = convertMinMaxByCurrency(ou1st.ma, currency, membercurrency);
                                                    // betPrice.nr.ou1st.mi = convertMinMaxByCurrency(ou1st.mi, currency, membercurrency);
                                                    //
                                                    // betPrice.nr.x121st.ma = convertMinMaxByCurrency(x121st.ma, currency, membercurrency);
                                                    // betPrice.nr.x121st.mi = convertMinMaxByCurrency(x121st.mi, currency, membercurrency);
                                                    //
                                                    // betPrice.nr.oe1st.ma = convertMinMaxByCurrency(oe1st.ma, currency, membercurrency);
                                                    // betPrice.nr.oe1st.mi = convertMinMaxByCurrency(oe1st.mi, currency, membercurrency);


                                                    betPrice.r.ah.ma = convertMinMaxByCurrency(betPrice.r.ah.ma, currency, membercurrency);
                                                    betPrice.r.ah.mi = convertMinMaxByCurrency(betPrice.r.ah.mi, currency, membercurrency);

                                                    betPrice.r.ou.ma = convertMinMaxByCurrency(betPrice.r.ou.ma, currency, membercurrency);
                                                    betPrice.r.ou.mi = convertMinMaxByCurrency(betPrice.r.ou.mi, currency, membercurrency);

                                                    betPrice.r.x12.ma = convertMinMaxByCurrency(betPrice.r.x12.ma, currency, membercurrency);
                                                    betPrice.r.x12.mi = convertMinMaxByCurrency(betPrice.r.x12.mi, currency, membercurrency);

                                                    betPrice.r.oe.ma = convertMinMaxByCurrency(betPrice.r.oe.ma, currency, membercurrency);
                                                    betPrice.r.oe.mi = convertMinMaxByCurrency(betPrice.r.oe.mi, currency, membercurrency);

                                                    betPrice.r.ah1st.ma = convertMinMaxByCurrency(betPrice.r.ah1st.ma, currency, membercurrency);
                                                    betPrice.r.ah1st.mi = convertMinMaxByCurrency(betPrice.r.ah1st.mi, currency, membercurrency);

                                                    betPrice.r.ou1st.ma = convertMinMaxByCurrency(betPrice.r.ou1st.ma, currency, membercurrency);
                                                    betPrice.r.ou1st.mi = convertMinMaxByCurrency(betPrice.r.ou1st.mi, currency, membercurrency);

                                                    betPrice.r.x121st.ma = convertMinMaxByCurrency(betPrice.r.x121st.ma, currency, membercurrency);
                                                    betPrice.r.x121st.mi = convertMinMaxByCurrency(betPrice.r.x121st.mi, currency, membercurrency);

                                                    betPrice.r.oe1st.ma = convertMinMaxByCurrency(betPrice.r.oe1st.ma, currency, membercurrency);
                                                    betPrice.r.oe1st.mi = convertMinMaxByCurrency(betPrice.r.oe1st.mi, currency, membercurrency);
                                                });
                                            });
                                            delete league.rem;
                                        })
                                        .value();

                                    return res.send({
                                        code: 0,
                                        message: "success",
                                        result: matchHDPFromDBArray
                                    });

                                } else {
                                    return res.send({
                                        message: "success",
                                        result: [],
                                        code: 0
                                    });
                                }
                            }
                        });
                }
            });

        }
    })
});

//[3]
router.get('/hdp_today', (req, res) => {
    const isHasParlay = req.query.isHasParlay || false;

    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);//new Date(new Date().getTime());
    // console.log('============== now_date ==============');
    // console.log(now);
    // console.log(now.getHours());
    // console.log(now.getMinutes());
    // console.log(now.getSeconds());
    // console.log('============== now_date ==============');
    const startDate = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const endDate = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);

    if ((now.getHours() - 7) >= 11 && now.getMinutes() >= 0 && now.getSeconds() >= 0 ) {
        startDate.setDate(now.getDate());
        endDate.setDate(now.getDate()  + 1);
    } else {
        startDate.setDate(now.getDate() - 1);
        endDate.setDate(now.getDate());
    }
    startDate.setHours(11 + 7);
    startDate.setMinutes(0);
    startDate.setSeconds(0);
    endDate.setHours(10 + 7);
    endDate.setMinutes(59);
    endDate.setSeconds(59);
    // console.log(startDate);
    // console.log(endDate);

    const now_date = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    // console.log('============== now_date ==============');
    // console.log(now_date);
    // console.log('============== now_date ==============');
    const membercurrency = req.userInfo.currency;
    async.series([
        (callback) => {
            MemberService.getLimitSetting(req.userInfo._id, (errLimitSetting, dataLimitSetting)=>{
                if(errLimitSetting){
                    callback(errLimitSetting, null);
                } else {
                    callback(null, dataLimitSetting.limitSetting);
                }
            });
        },

        (callback) => {
            MemberService.getOddAdjustment( req.userInfo.username, (err, data) => {
                if (err) {
                    // console.log('Get Odd Adjust error');
                    callback(err, null);
                } else {
                    callback(null, data);
                }
            });
        },

        (callback) => {
            RedisService.getCurrency((error, result) => {
                if (error) {
                    callback(null, false);
                } else {
                    callback(null, result);
                }
            })
        }
    ], (errAsync, result)=> {
        if (errAsync) {
            return res.send({
                message: "error",
                result: errAsync,
                code: 998
            });
        } else {

            const [
                limitSetting,
                oddAdjust,
                currency
            ] = result;

            RedisService.footballHDP((error, response) => {
                if (error) {
                    console.error('[1.1 ] 999 : ==> \n', error);
                    return res.send(
                        {
                            code: 9999,
                            message: error
                        });
                } else {
                    FootballModel.find({
                        m: {
                            $elemMatch: {
                                's.isH': true,
                                isToday : true,
                                'd': {
                                    $gte: startDate,
                                    $lt: endDate
                                }
                            }
                        }
                    })
                        .lean()
                        .sort('sk')
                        .exec((err, data) => {
                            if (err) {
                                // console.error('[3.2 ] 999 : ==> \n', err);
                                return res.send({
                                    message: "success",
                                    result: [],
                                    code: 0
                                });
                            }  else if (_.size(data) === 0) {
                                // console.log('[3.3 ] Data not found');
                                return res.send({
                                    message: "success",
                                    result: [],
                                    code: 0
                                });
                            } else {
                                const dataClone = clone(data);

                                let matchHDPFromDBArray = _.chain(data)
                                    .each(league => {
                                        if (isHasParlay) {
                                            league.m = _.filter(league.m, match => {
                                                return match.d >= startDate && match.d <= endDate && match.d >= now_date && match.hasParlay && match.s.isH });
                                        } else {
                                            league.m = _.filter(league.m, match => {
                                                return match.d >= startDate && match.d <= endDate && match.d >= now_date && match.s.isH
                                            });
                                        }
                                    })
                                    .value();

                                const matchIdHDPFromDBArray = _.chain(matchHDPFromDBArray)
                                    .flatten(true)
                                    .pluck('m')
                                    .flatten(true)
                                    .pluck('id')
                                    .value();

                                if (_.size(matchIdHDPFromDBArray) !== 0) {
                                    const result = response;//JSON.parse(response.body);
                                    const matchHDPAPIArray = _.chain(result)
                                        .flatten(true)
                                        .pluck('m')
                                        .value();

                                    const matchIdAPIArray = _.chain(matchHDPAPIArray)
                                        .flatten(true)
                                        .pluck('id')
                                        .value();

                                    let resultMatchedArray = _.intersection(matchIdHDPFromDBArray, matchIdAPIArray);
                                    // console.log("[3.6] HDP matched : " + JSON.stringify(resultMatchedArray));

                                    let resultTotalArray = _.chain(result)
                                        .each(league => {
                                            league.m = _.filter(league.m, match => {
                                                return _.contains(resultMatchedArray, match.id)
                                            });
                                        })
                                        .reject(league => { return _.size(league.m) === 0 })
                                        .value();

                                    // console.log("=====DB=====");
                                    matchHDPFromDBArray = _.chain(data)
                                        .each(league => {
                                            _.each(league.m, match => {
                                                match.bp = _.filter(match.bp, betPrise => { return betPrise.isOn && betPrise.isOnM; });
                                                let i = 0;
                                                _.each(match.bp, bp => {

                                                    const resultBP = _.chain(resultTotalArray)
                                                        .filter(leagueAPI => {
                                                            return _.isEqual(leagueAPI.k, league.k)
                                                        })
                                                        .flatten(true)
                                                        .pluck('m')
                                                        .flatten(true)
                                                        .filter(matchAPI => {
                                                            return _.isEqual(matchAPI.k, match.k)
                                                        })
                                                        .each(matchAPI => {
                                                            match.i = matchAPI.i;
                                                        })
                                                        .pluck('bp')
                                                        .flatten(true)
                                                        .filter(betPrice => {
                                                            return _.size(_.intersection(bp.allKey, betPrice.allKey)) > 0
                                                        })
                                                        .value();

                                                    if (!_.isUndefined(bp.allKey) &&
                                                        !_.isUndefined(_.first(resultBP)) &&
                                                        !_.isUndefined(_.first(resultBP).allKey) &&
                                                        _.size(_.intersection(bp.allKey, _.first(resultBP).allKey)) > 0) {
                                                        const {
                                                            r
                                                        } = match.bp[i];
                                                        match.bp[i] = _.first(resultBP);
                                                        match.bp[i].r = r;
                                                        delete match.bp[i].r.isLock;
                                                    }
                                                    i++;
                                                });
                                            });
                                            league.m = _.reject(league.m, match => { return _.size(match.bp) === 0 });
                                        })
                                        .reject(league => { return _.size(league.m) === 0 })
                                        .each(league => {
                                            _.each(league.m, match => {
                                                if(league.rp && league.rp.hdp){
                                                    if(_.size(match.bp) > 0){

                                                        // match.bp  = _.uniq(match.bp, (betPrice) => {
                                                        //     return betPrice.allKey.toString();
                                                        // });

                                                        _.each(match.bp, bp =>{

                                                            const resultBP = _.first(_.chain(dataClone)
                                                                .filter(leagueAPI => {
                                                                    return _.isEqual(leagueAPI.k, league.k)
                                                                })
                                                                .flatten(true)
                                                                .pluck('m')
                                                                .flatten(true)
                                                                .filter(matchAPI => {
                                                                    return _.isEqual(matchAPI.k, match.k)
                                                                })
                                                                .each(matchAPI => {
                                                                    match.i = matchAPI.i;
                                                                })
                                                                .pluck('bp')
                                                                .flatten(true)
                                                                .filter(betPrice => {
                                                                    return _.size(_.intersection(bp.allKey, betPrice.allKey)) > 0
                                                                })
                                                                .value());

                                                            let odd = _.filter(oddAdjust, (o)=>{
                                                                // console.log('================>', o.matchId , match.id);
                                                                return o.matchId === match.id;
                                                            });

                                                            // console.log('odd ======> ', odd);

                                                            let ou1st = {o: 0, u: 0};
                                                            let ou = {o: 0, u: 0};
                                                            let oe = {o: 0, e: 0};
                                                            let oe1st = {o: 0, e: 0};
                                                            let ah = {home: 0, away: 0};
                                                            let ah1st = {home: 0, away: 0};

                                                            // console.log('odd ====> ', odd);

                                                            if(_.size(odd) > 0 && !_.isUndefined(resultBP)){

                                                                _.each(odd, (o)=>{
                                                                    if(o.prefix === 'ah' && !_.isUndefined(resultBP.ah) && resultBP.ah){
                                                                        if(o.key === 'hpk' && o.value === resultBP.ah.hpk){
                                                                            ah.home += -o.count;
                                                                            ah.away += o.count;
                                                                        } else if(o.key === 'apk' && o.value === resultBP.ah.apk){
                                                                            ah.home += o.count;
                                                                            ah.away += -o.count;
                                                                        }
                                                                    } else if(o.prefix === 'ah1st' && !_.isUndefined(resultBP.ah1st) && resultBP.ah1st){

                                                                        if(o.key === 'hpk' && o.value === resultBP.ah1st.hpk){
                                                                            ah1st.home += -o.count;
                                                                            ah1st.away += o.count;
                                                                        } else if(o.key === 'apk' && o.value === resultBP.ah1st.apk){
                                                                            ah1st.home += o.count;
                                                                            ah1st.away += -o.count;
                                                                        }
                                                                    } else if(o.prefix === 'ou' && !_.isUndefined(resultBP.ou) && resultBP.ou){
                                                                        // console.log(o.value , resultBP.ou.opk);
                                                                        if(o.key === 'opk' && o.value === resultBP.ou.opk){
                                                                            ou.o += -o.count;
                                                                            ou.u += o.count;
                                                                        } else if(o.key === 'upk' && o.value === resultBP.ou.upk){
                                                                            ou.o += o.count;
                                                                            ou.u += -o.count;
                                                                        }
                                                                    } else if(o.prefix === 'ou1st' && !_.isUndefined(resultBP.ou1st) && resultBP.ou1st){
                                                                        if(o.key === 'opk' && o.value === resultBP.ou1st.opk){
                                                                            ou1st.o += -o.count;
                                                                            ou1st.u += o.count;
                                                                        } else if(o.key === 'upk' && o.value === resultBP.ou1st.upk){
                                                                            ou1st.o += o.count;
                                                                            ou1st.u += -o.count;
                                                                        }
                                                                    } else if(o.prefix === 'oe' && !_.isUndefined(resultBP.oe) && resultBP.oe){
                                                                        if(o.key === 'ok' && o.value === resultBP.oe.ok){
                                                                            oe.o += -o.count;
                                                                            oe.e += o.count;
                                                                        } else if(o.key === 'ek' && o.value === resultBP.oe.ek){
                                                                            oe.o += o.count;
                                                                            oe.e += -o.count;
                                                                        }
                                                                    } else if(o.prefix === 'oe1st' && !_.isUndefined(resultBP.oe1st) && resultBP.oe1st){
                                                                        if(o.key === 'ok' && o.value === resultBP.oe1st.ok){
                                                                            oe1st.o += -o.count;
                                                                            oe1st.e += o.count;
                                                                        } else if(o.key === 'ek' && o.value === resultBP.oe1st.ek){
                                                                            oe1st.o += o.count;
                                                                            oe1st.e += -o.count;
                                                                        }
                                                                    }
                                                                });
                                                            }

                                                            // ---------------------------  ou1st  -----------------------------
                                                            if(bp.ou1st && resultBP && !_.isUndefined(resultBP.ou1st) && resultBP.ou1st){
                                                                if(bp.ou1st.op || bp.ou1st.up){
                                                                    Commons.cal_ou(bp.ou1st.op, bp.ou1st.up, league.rp.hdp.ou1st.f, league.rp.hdp.ou1st.u, resultBP.rp.ou1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ou1st, (err, data)=>{
                                                                        if(data){
                                                                            bp.ou1st.op = data.op;
                                                                            bp.ou1st.up = data.up;
                                                                        }
                                                                    })
                                                                }
                                                            }

                                                            // ---------------------------  ou  ----------------------------------
                                                            if(bp.ou && resultBP && !_.isUndefined(resultBP.ou) && resultBP.ou){
                                                                if(bp.ou.op || bp.ou.up){
                                                                    Commons.cal_ou(bp.ou.op, bp.ou.up, league.rp.hdp.ou.f, league.rp.hdp.ou.u, resultBP.rp.ou, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ou, (err, data)=>{
                                                                        if(data){
                                                                            bp.ou.op = data.op;
                                                                            bp.ou.up = data.up;
                                                                        }
                                                                    })
                                                                }
                                                            }

                                                            // ---------------------------  oe1st  ----------------------------------
                                                            if(bp.oe1st && resultBP && !_.isUndefined(resultBP.oe1st) && resultBP.oe1st){
                                                                if(bp.oe1st.o || bp.oe1st.e){
                                                                    Commons.cal_oe(bp.oe1st.o, bp.oe1st.e, league.rp.hdp.oe1st.f, league.rp.hdp.oe1st.u, resultBP.rp.oe1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, oe1st, (err, data)=>{
                                                                        if(data){
                                                                            bp.oe1st.o = data.o;
                                                                            bp.oe1st.e = data.e;
                                                                        }
                                                                    })
                                                                }
                                                            }

                                                            // ---------------------------  oe  ----------------------------------
                                                            if(bp.oe && resultBP && !_.isUndefined(resultBP.oe) && resultBP.oe){
                                                                if(bp.oe.o || bp.oe.e){
                                                                    Commons.cal_oe(bp.oe.o, bp.oe.e, league.rp.hdp.oe.f, league.rp.hdp.oe.u, resultBP.rp.oe, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, oe, (err, data)=>{
                                                                        if(data){
                                                                            bp.oe.o = data.o;
                                                                            bp.oe.e = data.e;
                                                                        }
                                                                    })
                                                                }
                                                            }

                                                            // ---------------------------  ah1st  ----------------------------------
                                                            if(bp.ah1st && resultBP && !_.isUndefined(resultBP.ah1st) && resultBP.ah1st){
                                                                if(bp.ah1st.h || bp.ah1st.a){
                                                                    Commons.cal_ah(bp.ah1st.h, bp.ah1st.a, bp.ah1st.hp, bp.ah1st.ap, league.rp.hdp.ah1st.f, league.rp.hdp.ah1st.u, resultBP.rp.ah1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ah1st, (err, data)=>{
                                                                        if(data){
                                                                            bp.ah1st.hp = data.hp;
                                                                            bp.ah1st.ap = data.ap;
                                                                        }
                                                                    })
                                                                }
                                                            }

                                                            // ---------------------------  ah  ----------------------------------
                                                            if(bp.ah && resultBP && !_.isUndefined(resultBP.ah) && resultBP.ah){
                                                                if(bp.ah.h || bp.ah.a){
                                                                    Commons.cal_ah(bp.ah.h, bp.ah.a, bp.ah.hp, bp.ah.ap, league.rp.hdp.ah.f, league.rp.hdp.ah.u, resultBP.rp.ah, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ah, (err, data)=>{
                                                                        if(data){
                                                                            bp.ah.hp = data.hp;
                                                                            bp.ah.ap = data.ap;
                                                                        }
                                                                    })
                                                                }
                                                            }
                                                            // ---------------------------  x121st  ----------------------------------
                                                            if(bp.x121st && resultBP && !_.isUndefined(resultBP.x121st) && resultBP.x121st){
                                                                if(bp.x121st.h || bp.x121st.a || bp.x121st.d){
                                                                    Commons.cal_x12(bp.x121st.h, bp.x121st.a, bp.x121st.d, resultBP.rp.x121st, (err, data)=>{
                                                                        if(data){
                                                                            bp.x121st.h = data.h;
                                                                            bp.x121st.a = data.a;
                                                                            bp.x121st.d = data.d;
                                                                        }
                                                                    })
                                                                }
                                                            }

                                                            // ---------------------------  x12  ----------------------------------
                                                            if(bp.x12 && resultBP && !_.isUndefined(resultBP.x12) && resultBP.x12){
                                                                if(bp.x12.h || bp.x12.a || bp.x12.d){
                                                                    Commons.cal_x12(bp.x12.h, bp.x12.a, bp.x12.d, resultBP.rp.x12, (err, data)=>{
                                                                        if(data){
                                                                            bp.x12.h = data.h;
                                                                            bp.x12.a = data.a;
                                                                            bp.x12.d = data.d;
                                                                        }
                                                                    })
                                                                }
                                                            }
                                                            delete bp._id;
                                                            delete bp.isOn;
                                                            delete bp.isOnM;
                                                            delete bp.allKey;
                                                        });
                                                    }
                                                }
                                                delete match._id;
                                                delete match.bpl;
                                                delete match.rm;
                                                delete match.rlm;
                                                delete match.rl;
                                                delete match.rmem;
                                                delete match.rem;
                                                delete match.r;
                                                delete match.s;
                                                delete match.isToday;
                                            });
                                            delete league._id;
                                            delete league.rp;
                                            delete league.rm;
                                            delete league.rl;
                                            delete league.rlm;
                                            delete league.rmem;
                                            delete league.rem;
                                            delete league.__v;
                                        })
                                        .each(league => {
                                            const r = league.r;
                                            _.each(league.m, match => {
                                                let rule = league.r;
                                                match.r = {};
                                                match.r.pm = r.pm;
                                                if (!_.isUndefined(match.sr) && !_.isUndefined(match.sr.pm) && match.sr.isOn) {
                                                    rule = match.sr;
                                                    match.r.pm = match.sr.pm;
                                                }

                                                let round = 0;
                                                // console.log(`       ${match.n.en.h} vs ${match.n.en.a}`);

                                                _.each(match.bp, betPrice => {
                                                    // console.log(`       ==============  R  ============== ${match.id} BetPrice = ${round}`);
                                                    betPrice.r = {};
                                                    betPrice.r.ah = {};
                                                    betPrice.r.ou = {};
                                                    betPrice.r.x12 = {};
                                                    betPrice.r.oe = {};
                                                    betPrice.r.ah1st = {};
                                                    betPrice.r.ou1st = {};
                                                    betPrice.r.x121st = {};
                                                    betPrice.r.oe1st = {};
                                                    const {
                                                        ah,
                                                        ou,
                                                        x12,
                                                        oe,
                                                        ah1st,
                                                        ou1st,
                                                        x121st,
                                                        oe1st
                                                    } = rule;
                                                    betPrice.r.ah.ma = calculateBetPrice(Math.min(ah.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                                                    betPrice.r.ah.mi = ah.mi;

                                                    betPrice.r.ou.ma = calculateBetPrice(Math.min(ou.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                                                    betPrice.r.ou.mi = ou.mi;


                                                    betPrice.r.x12.ma = calculateBetPrice(Math.min(x12.ma, limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet), round);
                                                    betPrice.r.x12.mi = x12.mi;

                                                    betPrice.r.oe.ma = calculateBetPrice(Math.min(oe.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                                                    betPrice.r.oe.mi = oe.mi;

                                                    betPrice.r.ah1st.ma = calculateBetPrice(Math.min(ah1st.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                                                    betPrice.r.ah1st.mi = ah1st.mi;

                                                    betPrice.r.ou1st.ma = calculateBetPrice(Math.min(ou1st.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                                                    betPrice.r.ou1st.mi = ou1st.mi;

                                                    betPrice.r.x121st.ma = calculateBetPrice(Math.min(x121st.ma, limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet), round);
                                                    betPrice.r.x121st.mi = x121st.mi;

                                                    betPrice.r.oe1st.ma = calculateBetPrice(Math.min(oe1st.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                                                    betPrice.r.oe1st.mi = oe1st.mi;

                                                    round++;
                                                });
                                            });
                                            delete league.r;
                                        })
                                        .value();


                                    _.each(matchHDPFromDBArray, league => {
                                        _.each(league.m, match => {
                                            _.each(match.bp, betPrice => {
                                                const r =betPrice.r;
                                                // console.log(`--------------------------------`);
                                                // console.log(JSON.stringify(r));


                                                betPrice.r.ah.ma = calculateBetPriceByTime(r.ah.ma, now_date, new Date(match.d));
                                                betPrice.r.ou.ma = calculateBetPriceByTime(r.ou.ma, now_date, new Date(match.d));
                                                betPrice.r.x12.ma = calculateBetPriceByTime(r.x12.ma, now_date, new Date(match.d));
                                                betPrice.r.oe.ma = calculateBetPriceByTime(r.oe.ma, now_date, new Date(match.d));
                                                betPrice.r.ah1st.ma = calculateBetPriceByTime(r.ah1st.ma, now_date, new Date(match.d));
                                                betPrice.r.ou1st.ma = calculateBetPriceByTime(r.ou1st.ma, now_date, new Date(match.d));
                                                betPrice.r.x121st.ma = calculateBetPriceByTime(r.x121st.ma, now_date, new Date(match.d));
                                                betPrice.r.oe1st.ma = calculateBetPriceByTime(r.oe1st.ma, now_date, new Date(match.d));


                                                // betPrice.nr = {};
                                                // betPrice.nr.ah = {};
                                                // betPrice.nr.ou = {};
                                                // betPrice.nr.x12 = {};
                                                // betPrice.nr.oe = {};
                                                // betPrice.nr.ah1st = {};
                                                // betPrice.nr.ou1st = {};
                                                // betPrice.nr.x121st = {};
                                                // betPrice.nr.oe1st = {};
                                                //
                                                // const membercurrency = "MYR";
                                                // betPrice.nr.ah.ma     = convertMinMaxByCurrency(betPrice.r.ah.ma, currency, membercurrency);
                                                // betPrice.nr.ah.mi     = convertMinMaxByCurrency(betPrice.r.ah.mi, currency, membercurrency);
                                                // betPrice.nr.ou.ma     = convertMinMaxByCurrency(betPrice.r.ou.ma, currency, membercurrency);
                                                // betPrice.nr.ou.mi     = convertMinMaxByCurrency(betPrice.r.ou.mi, currency, membercurrency);
                                                // betPrice.nr.x12.ma    = convertMinMaxByCurrency(betPrice.r.x12.mi, currency, membercurrency);
                                                // betPrice.nr.x12.mi    = convertMinMaxByCurrency(betPrice.r.x12.mi, currency, membercurrency);
                                                // betPrice.nr.oe.ma     = convertMinMaxByCurrency(betPrice.r.oe.ma, currency, membercurrency);
                                                // betPrice.nr.oe.mi     = convertMinMaxByCurrency(betPrice.r.oe.mi, currency, membercurrency);
                                                // betPrice.nr.ah1st.ma  = convertMinMaxByCurrency(betPrice.r.ah1st.ma, currency, membercurrency);
                                                // betPrice.nr.ah1st.mi  = convertMinMaxByCurrency(betPrice.r.ah1st.mi, currency, membercurrency);
                                                // betPrice.nr.ou1st.ma  = convertMinMaxByCurrency(betPrice.r.ou1st.ma, currency, membercurrency);
                                                // betPrice.nr.ou1st.mi  = convertMinMaxByCurrency(betPrice.r.ou1st.mi, currency, membercurrency);
                                                // betPrice.nr.x121st.ma = convertMinMaxByCurrency(betPrice.r.x121st.ma, currency, membercurrency);
                                                // betPrice.nr.x121st.mi = convertMinMaxByCurrency(betPrice.r.x121st.mi, currency, membercurrency);
                                                // betPrice.nr.oe1st.ma  = convertMinMaxByCurrency(betPrice.r.oe1st.ma, currency, membercurrency);
                                                // betPrice.nr.oe1st.mi  = convertMinMaxByCurrency(betPrice.r.oe1st.mi, currency, membercurrency);



                                                betPrice.r.ah.ma = convertMinMaxByCurrency(betPrice.r.ah.ma, currency, membercurrency);
                                                betPrice.r.ah.mi = convertMinMaxByCurrency(betPrice.r.ah.mi, currency, membercurrency);

                                                betPrice.r.ou.ma = convertMinMaxByCurrency(betPrice.r.ou.ma, currency, membercurrency);
                                                betPrice.r.ou.mi = convertMinMaxByCurrency(betPrice.r.ou.mi, currency, membercurrency);

                                                betPrice.r.x12.ma = convertMinMaxByCurrency(betPrice.r.x12.ma, currency, membercurrency);
                                                betPrice.r.x12.mi = convertMinMaxByCurrency(betPrice.r.x12.mi, currency, membercurrency);

                                                betPrice.r.oe.ma = convertMinMaxByCurrency(betPrice.r.oe.ma, currency, membercurrency);
                                                betPrice.r.oe.mi = convertMinMaxByCurrency(betPrice.r.oe.mi, currency, membercurrency);

                                                betPrice.r.ah1st.ma = convertMinMaxByCurrency(betPrice.r.ah1st.ma, currency, membercurrency);
                                                betPrice.r.ah1st.mi = convertMinMaxByCurrency(betPrice.r.ah1st.mi, currency, membercurrency);

                                                betPrice.r.ou1st.ma = convertMinMaxByCurrency(betPrice.r.ou1st.ma, currency, membercurrency);
                                                betPrice.r.ou1st.mi = convertMinMaxByCurrency(betPrice.r.ou1st.mi, currency, membercurrency);

                                                betPrice.r.x121st.ma = convertMinMaxByCurrency(betPrice.r.x121st.ma, currency, membercurrency);
                                                betPrice.r.x121st.mi = convertMinMaxByCurrency(betPrice.r.x121st.mi, currency, membercurrency);

                                                betPrice.r.oe1st.ma = convertMinMaxByCurrency(betPrice.r.oe1st.ma, currency, membercurrency);
                                                betPrice.r.oe1st.mi = convertMinMaxByCurrency(betPrice.r.oe1st.mi, currency, membercurrency);
                                            });
                                        });
                                    });

                                    return res.send({
                                        code: 0,
                                        message: "success",
                                        result: matchHDPFromDBArray
                                    });

                                } else {
                                    return res.send({
                                        message: "success",
                                        result: [],
                                        code: 0
                                    });
                                }
                            }
                        });
                }
            });
        }
    })
});

//[2]
router.get('/live', (req, res) =>{
    // console.log('URL : ', URL_LIVE);
    const membercurrency = req.userInfo.currency;
    async.series([
            (callback) => {
                MemberService.getLimitSetting(req.userInfo._id, (errLimitSetting, dataLimitSetting)=>{
                    if(errLimitSetting){
                        callback(errLimitSetting, null);
                    } else {
                        callback(null, dataLimitSetting.limitSetting);
                    }
                });
            },

            (callback) => {
                MemberService.getOddAdjustment( req.userInfo.username, (err, data) => {
                    if (err) {
                        // console.log('Get Odd Adjust error');
                        callback(err, null);
                    } else {
                        callback(null, data);
                    }
                });
            },

            (callback) => {
                RedisService.getCurrency((error, result) => {
                    if (error) {
                        callback(null, false);
                    } else {
                        callback(null, result);
                    }
                })
            }
        ],
        (error, resultOddAndLimit) => {
            if (error) {
                return res.send({
                    code    : 9999,
                    message : error
                });
            } else {
                const [
                    limitSetting,
                    oddAdjust,
                    currency
                ] = resultOddAndLimit;

                RedisService.footballLive((error, response) => {
                    if (error) {
                        return res.send(
                            {
                                code: 9999,
                                message: error
                            });
                    } else {
                        FootballModel.find({
                            m: {
                                $elemMatch: {
                                    's.isL': true
                                }
                            }
                        })
                            .lean()
                            .sort('sk')
                            .exec((err, data) => {
                                if (err) {
                                    // console.error('[2.2 ] 999 : ==> \n', err);
                                    return res.send({
                                        message: "success",
                                        result: [],
                                        code: 0
                                    });
                                }  else if (_.size(data) === 0) {
                                    // console.log('[2.3 ] Data not found');
                                    return res.send({
                                        message: "success",
                                        result: [],
                                        code: 0
                                    });
                                } else {
                                    const dataClone = clone(data);
                                    let matchLiveFromDBArray = _.chain(data)
                                        .each(league => {
                                            league.m = _.filter(league.m, match => {
                                                return match.s.isL //&& match.s.isAl
                                            });
                                            league.m = _.reject(league.m, match => {
                                                return match.s.isC.ft || match.s.isC.ht
                                            });
                                        })
                                        .value();

                                    const matchIdLiveFromDBArray = _.chain(matchLiveFromDBArray)
                                        .flatten(true)
                                        .pluck('m')
                                        .flatten(true)
                                        .pluck('id')
                                        .value();

                                    if (_.size(matchIdLiveFromDBArray) !== 0) {
                                        const result = response;//JSON.parse(response.body);
                                        // console.log('result => ', JSON.stringify(result));
                                        const matchLiveAPIArray = _.chain(result)
                                            .flatten(true)
                                            .pluck('m')
                                            .value();

                                        const matchIdAPIArray = _.chain(matchLiveAPIArray)
                                            .flatten(true)
                                            .pluck('id')
                                            .value();

                                        let resultMatchedArray = _.intersection(matchIdLiveFromDBArray, matchIdAPIArray);
                                        // console.log("[2.6] Live matched : " + JSON.stringify(resultMatchedArray));

                                        let resultTotalArray = _.chain(result)
                                            .each(league => {

                                                // league.m = _.filter(league.m, match => {
                                                //     return _.contains(resultMatchedArray, match.id)
                                                // });

                                                // league.m = _.filter(league.m, match => {
                                                //     let live_time;
                                                //     if (_.isEqual(match.i.mt, 'First Half') ||
                                                //         _.isEqual(match.i.mt, 'Second Half')) {
                                                //         // console.log(`[${match.id}][${match.n.en.h} vs ${match.n.en.a}] ==== ${match.i.mt} ====`);
                                                //         live_time = parseInt(_.first(match.i.lt.split(':')));
                                                //         let result = false;
                                                //         if (!_.isNaN(live_time)) {
                                                //             // console.log(`[${match.id}][${match.n.en.h} vs ${match.n.en.a}] 3 <= [${live_time}] >= 92`);
                                                //             result = live_time >= 2; //&& live_time <= 88;
                                                //         }
                                                //         return result;
                                                //     } else if (_.isEqual(match.i.mt, 'Half Time')) {
                                                //         // console.log(`[${match.id}][${match.n.en.h} vs ${match.n.en.a}] ==== ${match.i.mt} ==== Allow to disploy`);
                                                //         return true;
                                                //     } else {
                                                //         // console.log(`[${match.id}][${match.n.en.h} vs ${match.n.en.a}] ==== Not allow to display`);
                                                //         return false;
                                                //     }
                                                // });
                                            })
                                            .reject(league => { return _.size(league.m) === 0 })
                                            .value();

                                        let resultAPIAllKeyBPLArray = _.chain(resultTotalArray)
                                            .flatten(true)
                                            .pluck('m')
                                            .flatten(true)
                                            .pluck('bp')
                                            .flatten(true)
                                            .pluck('allKey')
                                            .flatten(true)
                                            .value();


                                        matchLiveFromDBArray = _.chain(data)
                                            .each(league => {
                                                _.each(league.m, match => {

                                                    _.each(match.bpl, bp => {
                                                        const score = _.chain(resultTotalArray)
                                                            .filter(leagueAPI => {
                                                                return _.isEqual(leagueAPI.k, league.k)
                                                            })
                                                            .flatten(true)
                                                            .pluck('m')
                                                            .flatten(true)
                                                            .filter(matchAPI => {
                                                                return _.isEqual(matchAPI.k, match.k)
                                                            })
                                                            .each(matchAPI => {
                                                                match.i = matchAPI.i;
                                                            })
                                                            .pluck('bp')
                                                            .flatten(true)
                                                            .filter(betPrice => {
                                                                return _.size(_.intersection(bp.allKey, betPrice.allKey)) > 0
                                                            })
                                                            .value();
                                                    });

                                                    match.bpl = _.filter(match.bpl, betPrise => {
                                                        return /*betPrise.isOn && betPrise.isOnM && */_.size(_.intersection(resultAPIAllKeyBPLArray, betPrise.allKey)) > 0
                                                    });

                                                    let i = 0;
                                                    // const r = _.first(match.bpl).r;

                                                    const resultBPL = _.chain(resultTotalArray)
                                                        .filter(leagueAPI => {
                                                            return _.isEqual(leagueAPI.k, league.k)
                                                        })
                                                        .flatten(true)
                                                        .pluck('m')
                                                        .flatten(true)
                                                        .filter(matchAPI => {
                                                            return _.isEqual(matchAPI.k, match.k)
                                                        })
                                                        .each(matchAPI => {
                                                            match.i = matchAPI.i;
                                                        })
                                                        .pluck('bp')
                                                        .flatten(true)
                                                        .value();

                                                    match.bpl = resultBPL;
                                                });
                                            })
                                            .reject(league => { return _.size(league.m) === 0 })
                                            .each(league => {
                                                _.each(league.m, match => {
                                                    if(league.rp && league.rp.live){
                                                        if(_.size(match.bpl) > 0){

                                                            match.bpl  = _.uniq(match.bpl, (betPrice) => {
                                                                return betPrice.allKey.toString();
                                                            });

                                                            _.each(match.bpl, bp =>{

                                                                const resultBPL = _.first(_.chain(dataClone)
                                                                    .filter(leagueAPI => {
                                                                        return _.isEqual(leagueAPI.k, league.k)
                                                                    })
                                                                    .flatten(true)
                                                                    .pluck('m')
                                                                    .flatten(true)
                                                                    .filter(matchAPI => {
                                                                        return _.isEqual(matchAPI.k, match.k)
                                                                    })
                                                                    .pluck('bpl')
                                                                    .flatten(true)
                                                                    .filter(betPrice => {
                                                                        return _.size(_.intersection(bp.allKey, betPrice.allKey)) > 0;
                                                                    })
                                                                    .pluck('rp')
                                                                    .flatten(true)
                                                                    .value());

                                                                let odd = _.filter(oddAdjust, (o)=>{
                                                                    return o.matchId === match.id;
                                                                });

                                                                let ou1st = {o: 0, u: 0};
                                                                let ou = {o: 0, u: 0};
                                                                let oe = {o: 0, e: 0};
                                                                let oe1st = {o: 0, e: 0};
                                                                let ah = {home: 0, away: 0};
                                                                let ah1st = {home: 0, away: 0};

                                                                // console.log('odd ====> ', odd);

                                                                if(_.size(odd) > 0){
                                                                    _.each(odd, (o)=>{
                                                                        if(o.prefix === 'ah' && bp.ah){

                                                                            // console.log(o.value , bp);
                                                                            if(o.key === 'hpk' && o.value === bp.ah.hpk){
                                                                                ah.home += -o.count;
                                                                                ah.away += o.count;
                                                                            } else if(o.key === 'apk' && o.value === bp.ah.apk){
                                                                                ah.home += o.count;
                                                                                ah.away += -o.count;
                                                                            }
                                                                        } else if(o.prefix === 'ah1st' && bp.ah1st){

                                                                            if(o.key === 'hpk' && o.value === bp.ah1st.hpk){
                                                                                ah1st.home += -o.count;
                                                                                ah1st.away += o.count;
                                                                            } else if(o.key === 'apk' && o.value === bp.ah1st.apk){
                                                                                ah1st.home += o.count;
                                                                                ah1st.away += -o.count;
                                                                            }
                                                                        } else if(o.prefix === 'ou' && bp.ou){
                                                                            if(o.key === 'opk' && o.value === bp.ou.opk){
                                                                                ou.o += -o.count;
                                                                                ou.u += o.count;
                                                                            } else if(o.key === 'upk' && o.value === bp.ou.upk){
                                                                                ou.o += o.count;
                                                                                ou.u += -o.count;
                                                                            }
                                                                        } else if(o.prefix === 'ou1st' && bp.ou1st){
                                                                            if(o.key === 'opk' && o.value === bp.ou1st.opk){
                                                                                ou1st.o += -o.count;
                                                                                ou1st.u += o.count;
                                                                            } else if(o.key === 'upk' && o.value === bp.ou1st.upk){
                                                                                ou1st.o += o.count;
                                                                                ou1st.u += -o.count;
                                                                            }
                                                                        } else if(o.prefix === 'oe' && bp.oe){
                                                                            if(o.key === 'ok' && o.value === bp.oe.ok){
                                                                                oe.o += -o.count;
                                                                                oe.e += o.count;
                                                                            } else if(o.key === 'ek' && o.value === bp.oe.ek){
                                                                                oe.o += o.count;
                                                                                oe.e += -o.count;
                                                                            }
                                                                        } else if(o.prefix === 'oe1st' && bp.oe1st){
                                                                            if(o.key === 'ok' && o.value === bp.oe1st.ok){
                                                                                oe1st.o += -o.count;
                                                                                oe1st.e += o.count;
                                                                            } else if(o.key === 'ek' && o.value === bp.oe1st.ek){
                                                                                oe1st.o += o.count;
                                                                                oe1st.e += -o.count;
                                                                            }
                                                                        }
                                                                    });
                                                                }

                                                                // console.log('ah : ', ah, ', ah1st : ', ah1st, ', ou : ', ou, ', ou1st : ', ou1st, ', oe : ', oe, ', oe1st : ', oe1st);

                                                                // console.log('bp => ', JSON.stringify(bp));

                                                                // ---------------------------  ou1st  -----------------------------
                                                                if(bp.ou1st && resultBPL && resultBPL.ou1st){
                                                                    if(bp.ou1st.op || bp.ou1st.up){
                                                                        Commons.cal_ou(bp.ou1st.op, bp.ou1st.up, league.rp.live.ou1st.f, league.rp.live.ou1st.u, resultBPL.ou1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ou1st, (err, data)=>{
                                                                            if(data){
                                                                                bp.ou1st.op = data.op;
                                                                                bp.ou1st.up = data.up;
                                                                            }
                                                                        })
                                                                    }
                                                                }

                                                                // ---------------------------  ou  ----------------------------------
                                                                if(bp.ou && resultBPL && resultBPL.ou){
                                                                    if(bp.ou.op || bp.ou.up){
                                                                        Commons.cal_ou(bp.ou.op, bp.ou.up, league.rp.live.ou.f, league.rp.live.ou.u, resultBPL.ou, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ou, (err, data)=>{
                                                                            if(data){
                                                                                bp.ou.op = data.op;
                                                                                bp.ou.up = data.up;
                                                                            }
                                                                        })
                                                                    }
                                                                }

                                                                // ---------------------------  oe1st  ----------------------------------
                                                                if(bp.oe1st && resultBPL && resultBPL.oe1st){
                                                                    if(bp.oe1st.o || bp.oe1st.e){
                                                                        Commons.cal_oe(bp.oe1st.o, bp.oe1st.e, league.rp.live.oe1st.f, league.rp.live.oe1st.u, resultBPL.oe1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, oe1st, (err, data)=>{
                                                                            if(data){
                                                                                bp.oe1st.o = data.o;
                                                                                bp.oe1st.e = data.e;
                                                                            }
                                                                        })
                                                                    }
                                                                }

                                                                // ---------------------------  oe  ----------------------------------
                                                                if(bp.oe && resultBPL && resultBPL.oe){
                                                                    if(bp.oe.o || bp.oe.e){
                                                                        Commons.cal_oe(bp.oe.o, bp.oe.e, league.rp.live.oe.f, league.rp.live.oe.u, resultBPL.oe, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, oe, (err, data)=>{
                                                                            if(data){
                                                                                bp.oe.o = data.o;
                                                                                bp.oe.e = data.e;
                                                                            }
                                                                        })
                                                                    }
                                                                }

                                                                // ---------------------------  ah1st  ----------------------------------
                                                                if(bp.ah1st && resultBPL && resultBPL.ah1st){
                                                                    if(bp.ah1st.h || bp.ah1st.a){
                                                                        Commons.cal_ah(bp.ah1st.h, bp.ah1st.a, bp.ah1st.hp, bp.ah1st.ap, league.rp.live.ah1st.f, league.rp.live.ah1st.u, resultBPL.ah1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ah1st, (err, data)=>{
                                                                            if(data){
                                                                                bp.ah1st.hp = data.hp;
                                                                                bp.ah1st.ap = data.ap;
                                                                            }
                                                                        })
                                                                    }
                                                                }

                                                                // ---------------------------  ah  ----------------------------------
                                                                if(bp.ah && resultBPL && resultBPL.ah){
                                                                    if(bp.ah.h || bp.ah.a){
                                                                        Commons.cal_ah(bp.ah.h, bp.ah.a, bp.ah.hp, bp.ah.ap, league.rp.live.ah.f, league.rp.live.ah.u, resultBPL.ah, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ah, (err, data)=>{
                                                                            if(data){
                                                                                bp.ah.hp = data.hp;
                                                                                bp.ah.ap = data.ap;
                                                                            }
                                                                        })
                                                                    }
                                                                }
                                                                // ---------------------------  x121st  ----------------------------------
                                                                if(bp.x121st && resultBPL && resultBPL.x121st){
                                                                    if(bp.x121st.h || bp.x121st.a || bp.x121st.d){
                                                                        Commons.cal_x12(bp.x121st.h, bp.x121st.a, bp.x121st.d, resultBPL.x121st, (err, data)=>{
                                                                            if(data){
                                                                                bp.x121st.h = data.h;
                                                                                bp.x121st.a = data.a;
                                                                                bp.x121st.d = data.d;
                                                                            }
                                                                        })
                                                                    }
                                                                }

                                                                // ---------------------------  x12  ----------------------------------
                                                                if(bp.x12 && resultBPL && resultBPL.x12){
                                                                    if(bp.x12.h || bp.x12.a || bp.x12.d){
                                                                        Commons.cal_x12(bp.x12.h, bp.x12.a, bp.x12.d, resultBPL.x12, (err, data)=>{
                                                                            if(data){
                                                                                bp.x12.h = data.h;
                                                                                bp.x12.a = data.a;
                                                                                bp.x12.d = data.d;
                                                                            }
                                                                        })
                                                                    }
                                                                }
                                                                delete bp._id;
                                                                delete bp.isOn;
                                                                delete bp.isOnM;
                                                                delete bp.allKey;
                                                            });
                                                        }
                                                    }
                                                    delete match._id;
                                                    delete match.bp;
                                                    delete match.rm;
                                                    delete match.rlm;
                                                    delete match.rl;
                                                    delete match.rmem;
                                                    delete match.rem;
                                                    delete match.r;
                                                    delete match.s;
                                                    delete match.isToday;
                                                });
                                                delete league._id;
                                                delete league.rp;
                                                delete league.r;
                                                delete league.rm;
                                                delete league.rlm;
                                                delete league.rmem;
                                                delete league.rem;
                                                delete league.__v;
                                            })
                                            .each(league => {
                                                const rl = league.rl;
                                                // console.log(`####################################`);
                                                // console.log(`${league.k} ${league.n}`);
                                                // console.log(`RL  Max pay out ${rl.pm}`);
                                                // console.log(`ah     : ${JSON.stringify(rl.ah.ma)} ${JSON.stringify(rl.ah.mi)}`);
                                                // console.log(`ou     : ${JSON.stringify(rl.ou.ma)} ${JSON.stringify(rl.ou.mi)}`);
                                                // console.log(`x12    : ${JSON.stringify(rl.x12.ma)} ${JSON.stringify(rl.x12.mi)}`);
                                                // console.log(`oe     : ${JSON.stringify(rl.oe.ma)} ${JSON.stringify(rl.oe.mi)}`);
                                                // console.log(`ah1st  : ${JSON.stringify(rl.ah1st.ma)} ${JSON.stringify(rl.ah1st.mi)}`);
                                                // console.log(`ou1st  : ${JSON.stringify(rl.ou1st.ma)} ${JSON.stringify(rl.ou1st.mi)}`);
                                                // console.log(`x121st : ${JSON.stringify(rl.x121st.ma)} ${JSON.stringify(rl.x121st.mi)}`);
                                                // console.log(`oe1st  : ${JSON.stringify(rl.oe1st.ma)} ${JSON.stringify(rl.oe1st.mi)}`);
                                                // console.log(`####################################`);
                                                _.each(league.m, match => {
                                                    let rule = league.rl;
                                                    match.r = {};
                                                    match.r.pm = rl.pm;
                                                    if (!_.isUndefined(match.srl) && !_.isUndefined(match.srl.pm) && match.srl.isOn) {
                                                        rule = match.srl;
                                                        match.r.pm = match.srl.pm;
                                                    }

                                                    let round = 0;
                                                    _.each(match.bpl, betPrice => {
                                                        betPrice.r = {};
                                                        betPrice.r.ah = {};
                                                        betPrice.r.ou = {};
                                                        betPrice.r.x12 = {};
                                                        betPrice.r.oe = {};
                                                        betPrice.r.ah1st = {};
                                                        betPrice.r.ou1st = {};
                                                        betPrice.r.x121st = {};
                                                        betPrice.r.oe1st = {};
                                                        const {
                                                            ah,
                                                            ou,
                                                            x12,
                                                            oe,
                                                            ah1st,
                                                            ou1st,
                                                            x121st,
                                                            oe1st
                                                        } = rule;

                                                        betPrice.r.ah.ma = calculateBetPrice(Math.min(ah.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                                                        betPrice.r.ah.mi = ah.mi;

                                                        betPrice.r.ou.ma = calculateBetPrice(Math.min(ou.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                                                        betPrice.r.ou.mi = ou.mi;


                                                        betPrice.r.x12.ma = calculateBetPrice(Math.min(x12.ma, limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet), round);
                                                        betPrice.r.x12.mi = x12.mi;

                                                        betPrice.r.oe.ma = calculateBetPrice(Math.min(oe.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                                                        betPrice.r.oe.mi = oe.mi;

                                                        betPrice.r.ah1st.ma = calculateBetPrice(Math.min(ah1st.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                                                        betPrice.r.ah1st.mi = ah1st.mi;

                                                        betPrice.r.ou1st.ma = calculateBetPrice(Math.min(ou1st.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                                                        betPrice.r.ou1st.mi = ou1st.mi;

                                                        betPrice.r.x121st.ma = calculateBetPrice(Math.min(x121st.ma, limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet), round);
                                                        betPrice.r.x121st.mi = x121st.mi;

                                                        betPrice.r.oe1st.ma = calculateBetPrice(Math.min(oe1st.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                                                        betPrice.r.oe1st.mi = oe1st.mi;

                                                        round++;

                                                        // betPrice.nr = {};
                                                        // betPrice.nr.ah = {};
                                                        // betPrice.nr.ou = {};
                                                        // betPrice.nr.x12 = {};
                                                        // betPrice.nr.oe = {};
                                                        // betPrice.nr.ah1st = {};
                                                        // betPrice.nr.ou1st = {};
                                                        // betPrice.nr.x121st = {};
                                                        // betPrice.nr.oe1st = {};
                                                        //
                                                        // betPrice.nr.ah.ma = convertMinMaxByCurrency(ah.ma, currency, membercurrency);
                                                        // betPrice.nr.ah.mi = convertMinMaxByCurrency(ah.mi, currency, membercurrency);
                                                        //
                                                        // betPrice.nr.ou.ma = convertMinMaxByCurrency(ou.ma, currency, membercurrency);
                                                        // betPrice.nr.ou.mi = convertMinMaxByCurrency(ou.mi, currency, membercurrency);
                                                        //
                                                        //
                                                        // betPrice.nr.x12.ma = convertMinMaxByCurrency(x12.mi, currency, membercurrency);
                                                        // betPrice.nr.x12.mi = convertMinMaxByCurrency(x12.mi, currency, membercurrency);
                                                        //
                                                        // betPrice.nr.oe.ma = convertMinMaxByCurrency(oe.ma, currency, membercurrency);
                                                        // betPrice.nr.oe.mi = convertMinMaxByCurrency(oe.mi, currency, membercurrency);
                                                        //
                                                        // betPrice.nr.ah1st.ma = convertMinMaxByCurrency(ah1st.ma, currency, membercurrency);
                                                        // betPrice.nr.ah1st.mi = convertMinMaxByCurrency(ah1st.mi, currency, membercurrency);
                                                        //
                                                        // betPrice.nr.ou1st.ma = convertMinMaxByCurrency(ou1st.ma, currency, membercurrency);
                                                        // betPrice.nr.ou1st.mi = convertMinMaxByCurrency(ou1st.mi, currency, membercurrency);
                                                        //
                                                        // betPrice.nr.x121st.ma = convertMinMaxByCurrency(x121st.ma, currency, membercurrency);
                                                        // betPrice.nr.x121st.mi = convertMinMaxByCurrency(x121st.mi, currency, membercurrency);
                                                        //
                                                        // betPrice.nr.oe1st.ma = convertMinMaxByCurrency(oe1st.ma, currency, membercurrency);
                                                        // betPrice.nr.oe1st.mi = convertMinMaxByCurrency(oe1st.mi, currency, membercurrency);



                                                        betPrice.r.ah.ma = convertMinMaxByCurrency(betPrice.r.ah.ma, currency, membercurrency);
                                                        betPrice.r.ah.mi = convertMinMaxByCurrency(betPrice.r.ah.mi, currency, membercurrency);

                                                        betPrice.r.ou.ma = convertMinMaxByCurrency(betPrice.r.ou.ma, currency, membercurrency);
                                                        betPrice.r.ou.mi = convertMinMaxByCurrency(betPrice.r.ou.mi, currency, membercurrency);

                                                        betPrice.r.x12.ma = convertMinMaxByCurrency(betPrice.r.x12.ma, currency, membercurrency);
                                                        betPrice.r.x12.mi = convertMinMaxByCurrency(betPrice.r.x12.mi, currency, membercurrency);

                                                        betPrice.r.oe.ma = convertMinMaxByCurrency(betPrice.r.oe.ma, currency, membercurrency);
                                                        betPrice.r.oe.mi = convertMinMaxByCurrency(betPrice.r.oe.mi, currency, membercurrency);

                                                        betPrice.r.ah1st.ma = convertMinMaxByCurrency(betPrice.r.ah1st.ma, currency, membercurrency);
                                                        betPrice.r.ah1st.mi = convertMinMaxByCurrency(betPrice.r.ah1st.mi, currency, membercurrency);

                                                        betPrice.r.ou1st.ma = convertMinMaxByCurrency(betPrice.r.ou1st.ma, currency, membercurrency);
                                                        betPrice.r.ou1st.mi = convertMinMaxByCurrency(betPrice.r.ou1st.mi, currency, membercurrency);

                                                        betPrice.r.x121st.ma = convertMinMaxByCurrency(betPrice.r.x121st.ma, currency, membercurrency);
                                                        betPrice.r.x121st.mi = convertMinMaxByCurrency(betPrice.r.x121st.mi, currency, membercurrency);

                                                        betPrice.r.oe1st.ma = convertMinMaxByCurrency(betPrice.r.oe1st.ma, currency, membercurrency);
                                                        betPrice.r.oe1st.mi = convertMinMaxByCurrency(betPrice.r.oe1st.mi, currency, membercurrency);
                                                    });
                                                });
                                                delete league.rl;
                                            })
                                            .each(league => {
                                                league.m = _.reject(league.m, match => {
                                                    let live_time;
                                                    live_time = parseInt(_.first(match.i.lt.split(':')));
                                                    let result = false;
                                                    if (!_.isNaN(live_time)) {
                                                        result = (85 < live_time) && _.isEqual(match.i.h, '0') && _.isEqual(match.i.a, '0');
                                                    }

                                                    if (result) {
                                                        // console.log(`[HIDE] [${match.id}][${match.n.en.h} [${match.i.h}]vs[${match.i.a}] ${match.n.en.a}] ==== ${match.i.mt} (${live_time})====`);
                                                    } else {
                                                        // console.log(`[SHOW] [${match.id}][${match.n.en.h} [${match.i.h}]vs[${match.i.a}] ${match.n.en.a}] ==== ${match.i.mt} (${live_time})====`);
                                                    }

                                                    // console.log('   ', JSON.stringify(match.i));

                                                    return result;
                                                });
                                            })
                                            .reject(league => { return _.isEmpty(league.m) })
                                            .value();

                                        return res.send({
                                            message: "success",
                                            result: matchLiveFromDBArray,
                                            code: 0
                                        });

                                    } else {
                                        return res.send({
                                            message: "success",
                                            result: [],
                                            code: 0
                                        });
                                    }
                                }
                            });
                    }
                });

                // request.get(
                //     URL_LIVE,
                //     (err, response) => {
                //         if(err){
                //             // console.error('[2.1 ] 999 : ==> \n', err);
                //             return res.send(
                //                 {
                //                     code: 9999,
                //                     message: err
                //                 });
                //         } else {
                //
                //             FootballModel.find({
                //                 m: {
                //                     $elemMatch: {
                //                         's.isL': true
                //                     }
                //                 }
                //             })
                //                 .lean()
                //                 .sort('sk')
                //                 .exec((err, data) => {
                //                     if (err) {
                //                         // console.error('[2.2 ] 999 : ==> \n', err);
                //                         return res.send({
                //                             message: "success",
                //                             result: [],
                //                             code: 0
                //                         });
                //                     }  else if (_.size(data) === 0) {
                //                         // console.log('[2.3 ] Data not found');
                //                         return res.send({
                //                             message: "success",
                //                             result: [],
                //                             code: 0
                //                         });
                //                     } else {
                //                         const dataClone = clone(data);
                //                         let matchLiveFromDBArray = _.chain(data)
                //                             .each(league => {
                //                                 league.m = _.filter(league.m, match => {
                //                                     return match.s.isL //&& match.s.isAl
                //                                 });
                //                                 league.m = _.reject(league.m, match => {
                //                                     return match.s.isC.ft || match.s.isC.ht
                //                                 });
                //                             })
                //                             .value();
                //
                //                         const matchIdLiveFromDBArray = _.chain(matchLiveFromDBArray)
                //                             .flatten(true)
                //                             .pluck('m')
                //                             .flatten(true)
                //                             .pluck('id')
                //                             .value();
                //
                //                         if (_.size(matchIdLiveFromDBArray) !== 0) {
                //                             const result = JSON.parse(response.body);
                //                             // console.log('result => ', JSON.stringify(result));
                //                             const matchLiveAPIArray = _.chain(result.result)
                //                                 .flatten(true)
                //                                 .pluck('m')
                //                                 .value();
                //
                //                             const matchIdAPIArray = _.chain(matchLiveAPIArray)
                //                                 .flatten(true)
                //                                 .pluck('id')
                //                                 .value();
                //
                //                             let resultMatchedArray = _.intersection(matchIdLiveFromDBArray, matchIdAPIArray);
                //                             // console.log("[2.6] Live matched : " + JSON.stringify(resultMatchedArray));
                //
                //                             let resultTotalArray = _.chain(result.result)
                //                                 .each(league => {
                //
                //                                     // league.m = _.filter(league.m, match => {
                //                                     //     return _.contains(resultMatchedArray, match.id)
                //                                     // });
                //
                //                                     // league.m = _.filter(league.m, match => {
                //                                     //     let live_time;
                //                                     //     if (_.isEqual(match.i.mt, 'First Half') ||
                //                                     //         _.isEqual(match.i.mt, 'Second Half')) {
                //                                     //         // console.log(`[${match.id}][${match.n.en.h} vs ${match.n.en.a}] ==== ${match.i.mt} ====`);
                //                                     //         live_time = parseInt(_.first(match.i.lt.split(':')));
                //                                     //         let result = false;
                //                                     //         if (!_.isNaN(live_time)) {
                //                                     //             // console.log(`[${match.id}][${match.n.en.h} vs ${match.n.en.a}] 3 <= [${live_time}] >= 92`);
                //                                     //             result = live_time >= 2; //&& live_time <= 88;
                //                                     //         }
                //                                     //         return result;
                //                                     //     } else if (_.isEqual(match.i.mt, 'Half Time')) {
                //                                     //         // console.log(`[${match.id}][${match.n.en.h} vs ${match.n.en.a}] ==== ${match.i.mt} ==== Allow to disploy`);
                //                                     //         return true;
                //                                     //     } else {
                //                                     //         // console.log(`[${match.id}][${match.n.en.h} vs ${match.n.en.a}] ==== Not allow to display`);
                //                                     //         return false;
                //                                     //     }
                //                                     // });
                //                                 })
                //                                 .reject(league => { return _.size(league.m) === 0 })
                //                                 .value();
                //
                //                             let resultAPIAllKeyBPLArray = _.chain(resultTotalArray)
                //                                 .flatten(true)
                //                                 .pluck('m')
                //                                 .flatten(true)
                //                                 .pluck('bp')
                //                                 .flatten(true)
                //                                 .pluck('allKey')
                //                                 .flatten(true)
                //                                 .value();
                //
                //
                //                             matchLiveFromDBArray = _.chain(data)
                //                                 .each(league => {
                //                                     _.each(league.m, match => {
                //
                //                                         _.each(match.bpl, bp => {
                //                                             const score = _.chain(resultTotalArray)
                //                                                 .filter(leagueAPI => {
                //                                                     return _.isEqual(leagueAPI.k, league.k)
                //                                                 })
                //                                                 .flatten(true)
                //                                                 .pluck('m')
                //                                                 .flatten(true)
                //                                                 .filter(matchAPI => {
                //                                                     return _.isEqual(matchAPI.k, match.k)
                //                                                 })
                //                                                 .each(matchAPI => {
                //                                                     match.i = matchAPI.i;
                //                                                 })
                //                                                 .pluck('bp')
                //                                                 .flatten(true)
                //                                                 .filter(betPrice => {
                //                                                     return _.size(_.intersection(bp.allKey, betPrice.allKey)) > 0
                //                                                 })
                //                                                 .value();
                //                                         });
                //
                //                                         match.bpl = _.filter(match.bpl, betPrise => {
                //                                             return /*betPrise.isOn && betPrise.isOnM && */_.size(_.intersection(resultAPIAllKeyBPLArray, betPrise.allKey)) > 0
                //                                         });
                //
                //                                         let i = 0;
                //                                         // const r = _.first(match.bpl).r;
                //
                //                                         const resultBPL = _.chain(resultTotalArray)
                //                                             .filter(leagueAPI => {
                //                                                 return _.isEqual(leagueAPI.k, league.k)
                //                                             })
                //                                             .flatten(true)
                //                                             .pluck('m')
                //                                             .flatten(true)
                //                                             .filter(matchAPI => {
                //                                                 return _.isEqual(matchAPI.k, match.k)
                //                                             })
                //                                             .each(matchAPI => {
                //                                                 match.i = matchAPI.i;
                //                                             })
                //                                             .pluck('bp')
                //                                             .flatten(true)
                //                                             .value();
                //
                //                                         match.bpl = resultBPL;
                //                                     });
                //                                 })
                //                                 .reject(league => { return _.size(league.m) === 0 })
                //                                 .each(league => {
                //                                     _.each(league.m, match => {
                //                                         if(league.rp && league.rp.live){
                //                                             if(_.size(match.bpl) > 0){
                //
                //                                                 match.bpl  = _.uniq(match.bpl, (betPrice) => {
                //                                                     return betPrice.allKey.toString();
                //                                                 });
                //
                //                                                 _.each(match.bpl, bp =>{
                //
                //                                                     const resultBPL = _.first(_.chain(dataClone)
                //                                                         .filter(leagueAPI => {
                //                                                             return _.isEqual(leagueAPI.k, league.k)
                //                                                         })
                //                                                         .flatten(true)
                //                                                         .pluck('m')
                //                                                         .flatten(true)
                //                                                         .filter(matchAPI => {
                //                                                             return _.isEqual(matchAPI.k, match.k)
                //                                                         })
                //                                                         .pluck('bpl')
                //                                                         .flatten(true)
                //                                                         .filter(betPrice => {
                //                                                             return _.size(_.intersection(bp.allKey, betPrice.allKey)) > 0;
                //                                                         })
                //                                                         .pluck('rp')
                //                                                         .flatten(true)
                //                                                         .value());
                //
                //                                                     let odd = _.filter(oddAdjust, (o)=>{
                //                                                         return o.matchId === match.id;
                //                                                     });
                //
                //                                                     let ou1st = {o: 0, u: 0};
                //                                                     let ou = {o: 0, u: 0};
                //                                                     let oe = {o: 0, e: 0};
                //                                                     let oe1st = {o: 0, e: 0};
                //                                                     let ah = {home: 0, away: 0};
                //                                                     let ah1st = {home: 0, away: 0};
                //
                //                                                     // console.log('odd ====> ', odd);
                //
                //                                                     if(_.size(odd) > 0){
                //                                                         _.each(odd, (o)=>{
                //                                                             if(o.prefix === 'ah' && bp.ah){
                //
                //                                                                 // console.log(o.value , bp);
                //                                                                 if(o.key === 'hpk' && o.value === bp.ah.hpk){
                //                                                                     ah.home += -o.count;
                //                                                                     ah.away += o.count;
                //                                                                 } else if(o.key === 'apk' && o.value === bp.ah.apk){
                //                                                                     ah.home += o.count;
                //                                                                     ah.away += -o.count;
                //                                                                 }
                //                                                             } else if(o.prefix === 'ah1st' && bp.ah1st){
                //
                //                                                                 if(o.key === 'hpk' && o.value === bp.ah1st.hpk){
                //                                                                     ah1st.home += -o.count;
                //                                                                     ah1st.away += o.count;
                //                                                                 } else if(o.key === 'apk' && o.value === bp.ah1st.apk){
                //                                                                     ah1st.home += o.count;
                //                                                                     ah1st.away += -o.count;
                //                                                                 }
                //                                                             } else if(o.prefix === 'ou' && bp.ou){
                //                                                                 if(o.key === 'opk' && o.value === bp.ou.opk){
                //                                                                     ou.o += -o.count;
                //                                                                     ou.u += o.count;
                //                                                                 } else if(o.key === 'upk' && o.value === bp.ou.upk){
                //                                                                     ou.o += o.count;
                //                                                                     ou.u += -o.count;
                //                                                                 }
                //                                                             } else if(o.prefix === 'ou1st' && bp.ou1st){
                //                                                                 if(o.key === 'opk' && o.value === bp.ou1st.opk){
                //                                                                     ou1st.o += -o.count;
                //                                                                     ou1st.u += o.count;
                //                                                                 } else if(o.key === 'upk' && o.value === bp.ou1st.upk){
                //                                                                     ou1st.o += o.count;
                //                                                                     ou1st.u += -o.count;
                //                                                                 }
                //                                                             } else if(o.prefix === 'oe' && bp.oe){
                //                                                                 if(o.key === 'ok' && o.value === bp.oe.ok){
                //                                                                     oe.o += -o.count;
                //                                                                     oe.e += o.count;
                //                                                                 } else if(o.key === 'ek' && o.value === bp.oe.ek){
                //                                                                     oe.o += o.count;
                //                                                                     oe.e += -o.count;
                //                                                                 }
                //                                                             } else if(o.prefix === 'oe1st' && bp.oe1st){
                //                                                                 if(o.key === 'ok' && o.value === bp.oe1st.ok){
                //                                                                     oe1st.o += -o.count;
                //                                                                     oe1st.e += o.count;
                //                                                                 } else if(o.key === 'ek' && o.value === bp.oe1st.ek){
                //                                                                     oe1st.o += o.count;
                //                                                                     oe1st.e += -o.count;
                //                                                                 }
                //                                                             }
                //                                                         });
                //                                                     }
                //
                //                                                     // console.log('ah : ', ah, ', ah1st : ', ah1st, ', ou : ', ou, ', ou1st : ', ou1st, ', oe : ', oe, ', oe1st : ', oe1st);
                //
                //                                                     // console.log('bp => ', JSON.stringify(bp));
                //
                //                                                     // ---------------------------  ou1st  -----------------------------
                //                                                     if(bp.ou1st && resultBPL && resultBPL.ou1st){
                //                                                         if(bp.ou1st.op || bp.ou1st.up){
                //                                                             Commons.cal_ou(bp.ou1st.op, bp.ou1st.up, league.rp.live.ou1st.f, league.rp.live.ou1st.u, resultBPL.ou1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ou1st, (err, data)=>{
                //                                                                 if(data){
                //                                                                     bp.ou1st.op = data.op;
                //                                                                     bp.ou1st.up = data.up;
                //                                                                 }
                //                                                             })
                //                                                         }
                //                                                     }
                //
                //                                                     // ---------------------------  ou  ----------------------------------
                //                                                     if(bp.ou && resultBPL && resultBPL.ou){
                //                                                         if(bp.ou.op || bp.ou.up){
                //                                                             Commons.cal_ou(bp.ou.op, bp.ou.up, league.rp.live.ou.f, league.rp.live.ou.u, resultBPL.ou, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ou, (err, data)=>{
                //                                                                 if(data){
                //                                                                     bp.ou.op = data.op;
                //                                                                     bp.ou.up = data.up;
                //                                                                 }
                //                                                             })
                //                                                         }
                //                                                     }
                //
                //                                                     // ---------------------------  oe1st  ----------------------------------
                //                                                     if(bp.oe1st && resultBPL && resultBPL.oe1st){
                //                                                         if(bp.oe1st.o || bp.oe1st.e){
                //                                                             Commons.cal_oe(bp.oe1st.o, bp.oe1st.e, league.rp.live.oe1st.f, league.rp.live.oe1st.u, resultBPL.oe1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, oe1st, (err, data)=>{
                //                                                                 if(data){
                //                                                                     bp.oe1st.o = data.o;
                //                                                                     bp.oe1st.e = data.e;
                //                                                                 }
                //                                                             })
                //                                                         }
                //                                                     }
                //
                //                                                     // ---------------------------  oe  ----------------------------------
                //                                                     if(bp.oe && resultBPL && resultBPL.oe){
                //                                                         if(bp.oe.o || bp.oe.e){
                //                                                             Commons.cal_oe(bp.oe.o, bp.oe.e, league.rp.live.oe.f, league.rp.live.oe.u, resultBPL.oe, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, oe, (err, data)=>{
                //                                                                 if(data){
                //                                                                     bp.oe.o = data.o;
                //                                                                     bp.oe.e = data.e;
                //                                                                 }
                //                                                             })
                //                                                         }
                //                                                     }
                //
                //                                                     // ---------------------------  ah1st  ----------------------------------
                //                                                     if(bp.ah1st && resultBPL && resultBPL.ah1st){
                //                                                         if(bp.ah1st.h || bp.ah1st.a){
                //                                                             Commons.cal_ah(bp.ah1st.h, bp.ah1st.a, bp.ah1st.hp, bp.ah1st.ap, league.rp.live.ah1st.f, league.rp.live.ah1st.u, resultBPL.ah1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ah1st, (err, data)=>{
                //                                                                 if(data){
                //                                                                     bp.ah1st.hp = data.hp;
                //                                                                     bp.ah1st.ap = data.ap;
                //                                                                 }
                //                                                             })
                //                                                         }
                //                                                     }
                //
                //                                                     // ---------------------------  ah  ----------------------------------
                //                                                     if(bp.ah && resultBPL && resultBPL.ah){
                //                                                         if(bp.ah.h || bp.ah.a){
                //                                                             Commons.cal_ah(bp.ah.h, bp.ah.a, bp.ah.hp, bp.ah.ap, league.rp.live.ah.f, league.rp.live.ah.u, resultBPL.ah, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ah, (err, data)=>{
                //                                                                 if(data){
                //                                                                     bp.ah.hp = data.hp;
                //                                                                     bp.ah.ap = data.ap;
                //                                                                 }
                //                                                             })
                //                                                         }
                //                                                     }
                //                                                     // ---------------------------  x121st  ----------------------------------
                //                                                     if(bp.x121st && resultBPL && resultBPL.x121st){
                //                                                         if(bp.x121st.h || bp.x121st.a || bp.x121st.d){
                //                                                             Commons.cal_x12(bp.x121st.h, bp.x121st.a, bp.x121st.d, resultBPL.x121st, (err, data)=>{
                //                                                                 if(data){
                //                                                                     bp.x121st.h = data.h;
                //                                                                     bp.x121st.a = data.a;
                //                                                                     bp.x121st.d = data.d;
                //                                                                 }
                //                                                             })
                //                                                         }
                //                                                     }
                //
                //                                                     // ---------------------------  x12  ----------------------------------
                //                                                     if(bp.x12 && resultBPL && resultBPL.x12){
                //                                                         if(bp.x12.h || bp.x12.a || bp.x12.d){
                //                                                             Commons.cal_x12(bp.x12.h, bp.x12.a, bp.x12.d, resultBPL.x12, (err, data)=>{
                //                                                                 if(data){
                //                                                                     bp.x12.h = data.h;
                //                                                                     bp.x12.a = data.a;
                //                                                                     bp.x12.d = data.d;
                //                                                                 }
                //                                                             })
                //                                                         }
                //                                                     }
                //                                                     delete bp._id;
                //                                                     delete bp.isOn;
                //                                                     delete bp.isOnM;
                //                                                     delete bp.allKey;
                //                                                 });
                //                                             }
                //                                         }
                //                                         delete match._id;
                //                                         delete match.bp;
                //                                         delete match.rm;
                //                                         delete match.rlm;
                //                                         delete match.rl;
                //                                         delete match.rmem;
                //                                         delete match.rem;
                //                                         delete match.r;
                //                                         delete match.s;
                //                                         delete match.isToday;
                //                                     });
                //                                     delete league._id;
                //                                     delete league.rp;
                //                                     delete league.r;
                //                                     delete league.rm;
                //                                     delete league.rlm;
                //                                     delete league.rmem;
                //                                     delete league.rem;
                //                                     delete league.__v;
                //                                 })
                //                                 .each(league => {
                //                                     const rl = league.rl;
                //                                     // console.log(`####################################`);
                //                                     // console.log(`${league.k} ${league.n}`);
                //                                     // console.log(`RL  Max pay out ${rl.pm}`);
                //                                     // console.log(`ah     : ${JSON.stringify(rl.ah.ma)} ${JSON.stringify(rl.ah.mi)}`);
                //                                     // console.log(`ou     : ${JSON.stringify(rl.ou.ma)} ${JSON.stringify(rl.ou.mi)}`);
                //                                     // console.log(`x12    : ${JSON.stringify(rl.x12.ma)} ${JSON.stringify(rl.x12.mi)}`);
                //                                     // console.log(`oe     : ${JSON.stringify(rl.oe.ma)} ${JSON.stringify(rl.oe.mi)}`);
                //                                     // console.log(`ah1st  : ${JSON.stringify(rl.ah1st.ma)} ${JSON.stringify(rl.ah1st.mi)}`);
                //                                     // console.log(`ou1st  : ${JSON.stringify(rl.ou1st.ma)} ${JSON.stringify(rl.ou1st.mi)}`);
                //                                     // console.log(`x121st : ${JSON.stringify(rl.x121st.ma)} ${JSON.stringify(rl.x121st.mi)}`);
                //                                     // console.log(`oe1st  : ${JSON.stringify(rl.oe1st.ma)} ${JSON.stringify(rl.oe1st.mi)}`);
                //                                     // console.log(`####################################`);
                //                                     _.each(league.m, match => {
                //                                         let rule = league.rl;
                //                                         match.r = {};
                //                                         match.r.pm = rl.pm;
                //                                         if (!_.isUndefined(match.srl) && !_.isUndefined(match.srl.pm) && match.srl.isOn) {
                //                                             rule = match.srl;
                //                                             match.r.pm = match.srl.pm;
                //                                         }
                //
                //                                         let round = 0;
                //                                         _.each(match.bpl, betPrice => {
                //                                             betPrice.r = {};
                //                                             betPrice.r.ah = {};
                //                                             betPrice.r.ou = {};
                //                                             betPrice.r.x12 = {};
                //                                             betPrice.r.oe = {};
                //                                             betPrice.r.ah1st = {};
                //                                             betPrice.r.ou1st = {};
                //                                             betPrice.r.x121st = {};
                //                                             betPrice.r.oe1st = {};
                //                                             const {
                //                                                 ah,
                //                                                 ou,
                //                                                 x12,
                //                                                 oe,
                //                                                 ah1st,
                //                                                 ou1st,
                //                                                 x121st,
                //                                                 oe1st
                //                                             } = rule;
                //
                //                                             betPrice.r.ah.ma = calculateBetPrice(Math.min(ah.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                //                                             betPrice.r.ah.mi = ah.mi;
                //
                //                                             betPrice.r.ou.ma = calculateBetPrice(Math.min(ou.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                //                                             betPrice.r.ou.mi = ou.mi;
                //
                //
                //                                             betPrice.r.x12.ma = calculateBetPrice(Math.min(x12.ma, limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet), round);
                //                                             betPrice.r.x12.mi = x12.mi;
                //
                //                                             betPrice.r.oe.ma = calculateBetPrice(Math.min(oe.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                //                                             betPrice.r.oe.mi = oe.mi;
                //
                //                                             betPrice.r.ah1st.ma = calculateBetPrice(Math.min(ah1st.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                //                                             betPrice.r.ah1st.mi = ah1st.mi;
                //
                //                                             betPrice.r.ou1st.ma = calculateBetPrice(Math.min(ou1st.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                //                                             betPrice.r.ou1st.mi = ou1st.mi;
                //
                //                                             betPrice.r.x121st.ma = calculateBetPrice(Math.min(x121st.ma, limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet), round);
                //                                             betPrice.r.x121st.mi = x121st.mi;
                //
                //                                             betPrice.r.oe1st.ma = calculateBetPrice(Math.min(oe1st.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                //                                             betPrice.r.oe1st.mi = oe1st.mi;
                //
                //                                             round++;
                //
                //                                             // betPrice.nr = {};
                //                                             // betPrice.nr.ah = {};
                //                                             // betPrice.nr.ou = {};
                //                                             // betPrice.nr.x12 = {};
                //                                             // betPrice.nr.oe = {};
                //                                             // betPrice.nr.ah1st = {};
                //                                             // betPrice.nr.ou1st = {};
                //                                             // betPrice.nr.x121st = {};
                //                                             // betPrice.nr.oe1st = {};
                //                                             //
                //                                             // betPrice.nr.ah.ma = convertMinMaxByCurrency(ah.ma, currency, membercurrency);
                //                                             // betPrice.nr.ah.mi = convertMinMaxByCurrency(ah.mi, currency, membercurrency);
                //                                             //
                //                                             // betPrice.nr.ou.ma = convertMinMaxByCurrency(ou.ma, currency, membercurrency);
                //                                             // betPrice.nr.ou.mi = convertMinMaxByCurrency(ou.mi, currency, membercurrency);
                //                                             //
                //                                             //
                //                                             // betPrice.nr.x12.ma = convertMinMaxByCurrency(x12.mi, currency, membercurrency);
                //                                             // betPrice.nr.x12.mi = convertMinMaxByCurrency(x12.mi, currency, membercurrency);
                //                                             //
                //                                             // betPrice.nr.oe.ma = convertMinMaxByCurrency(oe.ma, currency, membercurrency);
                //                                             // betPrice.nr.oe.mi = convertMinMaxByCurrency(oe.mi, currency, membercurrency);
                //                                             //
                //                                             // betPrice.nr.ah1st.ma = convertMinMaxByCurrency(ah1st.ma, currency, membercurrency);
                //                                             // betPrice.nr.ah1st.mi = convertMinMaxByCurrency(ah1st.mi, currency, membercurrency);
                //                                             //
                //                                             // betPrice.nr.ou1st.ma = convertMinMaxByCurrency(ou1st.ma, currency, membercurrency);
                //                                             // betPrice.nr.ou1st.mi = convertMinMaxByCurrency(ou1st.mi, currency, membercurrency);
                //                                             //
                //                                             // betPrice.nr.x121st.ma = convertMinMaxByCurrency(x121st.ma, currency, membercurrency);
                //                                             // betPrice.nr.x121st.mi = convertMinMaxByCurrency(x121st.mi, currency, membercurrency);
                //                                             //
                //                                             // betPrice.nr.oe1st.ma = convertMinMaxByCurrency(oe1st.ma, currency, membercurrency);
                //                                             // betPrice.nr.oe1st.mi = convertMinMaxByCurrency(oe1st.mi, currency, membercurrency);
                //
                //
                //
                //                                             betPrice.r.ah.ma = convertMinMaxByCurrency(betPrice.r.ah.ma, currency, membercurrency);
                //                                             betPrice.r.ah.mi = convertMinMaxByCurrency(betPrice.r.ah.mi, currency, membercurrency);
                //
                //                                             betPrice.r.ou.ma = convertMinMaxByCurrency(betPrice.r.ou.ma, currency, membercurrency);
                //                                             betPrice.r.ou.mi = convertMinMaxByCurrency(betPrice.r.ou.mi, currency, membercurrency);
                //
                //                                             betPrice.r.x12.ma = convertMinMaxByCurrency(betPrice.r.x12.ma, currency, membercurrency);
                //                                             betPrice.r.x12.mi = convertMinMaxByCurrency(betPrice.r.x12.mi, currency, membercurrency);
                //
                //                                             betPrice.r.oe.ma = convertMinMaxByCurrency(betPrice.r.oe.ma, currency, membercurrency);
                //                                             betPrice.r.oe.mi = convertMinMaxByCurrency(betPrice.r.oe.mi, currency, membercurrency);
                //
                //                                             betPrice.r.ah1st.ma = convertMinMaxByCurrency(betPrice.r.ah1st.ma, currency, membercurrency);
                //                                             betPrice.r.ah1st.mi = convertMinMaxByCurrency(betPrice.r.ah1st.mi, currency, membercurrency);
                //
                //                                             betPrice.r.ou1st.ma = convertMinMaxByCurrency(betPrice.r.ou1st.ma, currency, membercurrency);
                //                                             betPrice.r.ou1st.mi = convertMinMaxByCurrency(betPrice.r.ou1st.mi, currency, membercurrency);
                //
                //                                             betPrice.r.x121st.ma = convertMinMaxByCurrency(betPrice.r.x121st.ma, currency, membercurrency);
                //                                             betPrice.r.x121st.mi = convertMinMaxByCurrency(betPrice.r.x121st.mi, currency, membercurrency);
                //
                //                                             betPrice.r.oe1st.ma = convertMinMaxByCurrency(betPrice.r.oe1st.ma, currency, membercurrency);
                //                                             betPrice.r.oe1st.mi = convertMinMaxByCurrency(betPrice.r.oe1st.mi, currency, membercurrency);
                //                                         });
                //                                     });
                //                                     delete league.rl;
                //                                 })
                //                                 .each(league => {
                //                                     league.m = _.reject(league.m, match => {
                //                                         let live_time;
                //                                         live_time = parseInt(_.first(match.i.lt.split(':')));
                //                                         let result = false;
                //                                         if (!_.isNaN(live_time)) {
                //                                             result = (85 < live_time) && _.isEqual(match.i.h, '0') && _.isEqual(match.i.a, '0');
                //                                         }
                //
                //                                         if (result) {
                //                                             // console.log(`[HIDE] [${match.id}][${match.n.en.h} [${match.i.h}]vs[${match.i.a}] ${match.n.en.a}] ==== ${match.i.mt} (${live_time})====`);
                //                                         } else {
                //                                             // console.log(`[SHOW] [${match.id}][${match.n.en.h} [${match.i.h}]vs[${match.i.a}] ${match.n.en.a}] ==== ${match.i.mt} (${live_time})====`);
                //                                         }
                //
                //                                         // console.log('   ', JSON.stringify(match.i));
                //
                //                                         return result;
                //                                     });
                //                                 })
                //                                 .reject(league => { return _.isEmpty(league.m) })
                //                                 .value();
                //
                //                             return res.send({
                //                                 message: "success",
                //                                 result: matchLiveFromDBArray,
                //                                 code: 0
                //                             });
                //
                //                         } else {
                //                             return res.send({
                //                                 message: "success",
                //                                 result: [],
                //                                 code: 0
                //                             });
                //                         }
                //                     }
                //                 });
                //         }
                //     });
            }
        }
    );
});



//[10]
router.get('/load_live_match', (req, res) =>{
    liveTotalPage((err, totalPage) =>{
        if (err) {
            console.error('[10.1]999 : ==> \n', err);
            return res.send(
                {
                    code: 9999,
                    message: err
                });
        } else {
            let taskList = [];
            const range = _.range(totalPage);
            // console.log('Total Page => ', totalPage);
            range.forEach(page => {
                taskList.push((callback) => {
                    liveAPI(page, (err, result) => {
                        if (err) {
                            callback(err, null)
                        } else {
                            callback(null, result);
                        }
                    });
                });
            });

            async.parallel(taskList,
                (err, result) => {
                    if (err) {
                        return res.send({message: err, code: 10102});
                    } else {
                        // console.log('[10.2] success');
                        return res.send({
                            message: "success",
                            result: _.first(result),
                            code: 0
                        });
                    }
                });
        }
    });
});

//[4]
router.put('/live_match_bet_price', (req, res) => {
    const {
        id,
        prefix,
        key,
        value
    } = req.body;


    const predicate = JSON.parse(`{"${key}":"${value}"}`);
    const k = id.split(':');

    let indexRound;
    const membercurrency = req.userInfo.currency;
    async.series([
        (callback) => {
            MemberService.getLimitSetting(req.userInfo._id, (errLimitSetting, dataLimitSetting)=>{
                if(errLimitSetting){
                    callback(errLimitSetting, null);
                } else {
                    callback(null, dataLimitSetting.limitSetting);
                }
            });
        },

        (callback) => {
            MemberService.getOddAdjustment( req.userInfo.username, (err, data) => {
                if (err) {
                    // console.log('Get Odd Adjust error');
                    callback(err, null);
                } else {
                    callback(null, data);
                }
            });
        },

        (callback) => {
            RedisService.getCurrency((error, result) => {
                if (error) {
                    callback(null, false);
                } else {
                    callback(null, result);
                }
            })
        }
    ], (errAsync, dataAsync)=> {
        if (errAsync) {
            return res.send({
                message: "error",
                result: errAsync,
                code: 998
            });
        } else {

            const [
                limitSetting,
                oddAdjust,
                currency
            ] = dataAsync;

            async.series([(cb)=>{

                FootballModel.findOne({k: k[0]},
                    (err, data)=>{
                        if(err){
                            cb(err, null)
                        } else if(data){
                            cb(null, data)
                        } else {
                            cb('league is null', null)
                        }
                    })
            }, (cb)=>{



                const headers = {
                    'Content-Type' : 'application/json'
                };

                const option = {
                    url: `${URL_CACHE_LIVE_SINGLE_MATCH}/${id}`,
                    method: 'GET',
                    headers: headers
                };

                request(option, (err, response, body) => {
                    if (err) {
                        cb(err, null)
                    } else {
                        try {
                            let result = JSON.parse(body);
                            if(result){
                                cb(null, result);
                            } else {
                                cb(null, []);
                            }
                        } catch (e) {
                            console.error(e);
                            request(option, (err, response, body) => {
                                if (err) {
                                    cb(err, null)
                                } else {
                                    try {
                                        let result = JSON.parse(body);
                                        if(result){
                                            cb(null, result);
                                        } else {
                                            cb(null, []);
                                        }
                                    } catch (e) {
                                        console.error(e);
                                        request(option, (err, response, body) => {
                                            if (err) {
                                                cb(err, null)
                                            } else {
                                                try {
                                                    let result = JSON.parse(body);
                                                    if(result){
                                                        cb(null, result);
                                                    } else {
                                                        cb(null, []);
                                                    }
                                                } catch (e) {
                                                    console.error(e);
                                                    request(option, (err, response, body) => {
                                                        if (err) {
                                                            cb(err, null)
                                                        } else {
                                                            try {
                                                                let result = JSON.parse(body);
                                                                if(result){
                                                                    cb(null, result);
                                                                } else {
                                                                    cb(null, []);
                                                                }
                                                            } catch (e) {
                                                                console.error(e);
                                                                request(option, (err, response, body) => {
                                                                    if (err) {
                                                                        cb(err, null)
                                                                    } else {
                                                                        let result = JSON.parse(body);
                                                                        if(result){
                                                                            cb(null, result);
                                                                        } else {
                                                                            cb(null, []);
                                                                        }
                                                                    }
                                                                });
                                                            }

                                                        }
                                                    });
                                                }

                                            }
                                        });
                                    }

                                }
                            });
                        }

                    }
                });
            }], (err, results)=>{
                if(err){
                    return res.send({message: err, code: 10102});
                } else {
                    const type_lower = prefix.toLowerCase();
                    const rp_live = results[0].rp.live;
                    const rp = results[0].rp;

                    let match_cache = results[1];
                    let bp =  _.first(_.filter(match_cache.result, league => {
                        // console.log('allKey => ', league.allKey, ', value => ', value);
                        return _.contains(league.allKey, value)
                    }));

                    let model_result = {};
                    let srl = {};



                    let resultBPL = _.first(_.chain(results)
                        .filter(league => {
                            return _.isEqual(league.k, parseInt(k[0]))
                        })
                        .flatten(true)
                        .pluck('m')
                        .flatten(true)
                        .filter(matchAPI => {
                            return _.isEqual(matchAPI.id, id)// && matchAPI.s.isAl
                        })
                        .each(match => {
                            // let round = 0;
                            // const r = _.first(match.bpl).r;

                            if(match.srl /*&& match.srl.isOn*/){
                                srl = match.srl;
                            }

                            _.each(match.bpl, (betPrice, index) => {
                                if(betPrice.allKey.indexOf(value) > -1){
                                    // console.log(index);
                                    indexRound = index;
                                }

                            })
                        })
                        .pluck('bpl')
                        .flatten(true)
                        .filter(betPrice => {
                            return _.contains(betPrice.allKey, value);
                        })
                        .map(betPrice => {
                            return _.pick(betPrice, 'rp', 'srl')
                        })
                        .flatten(true)
                        .value());

                    // console.log('==============>', resultBPL);

                    if( _.isUndefined(resultBPL) || _.isUndefined(resultBPL.rp) ){
                        // return res.send({
                        //     message: "resultBP is undefined ",
                        //     code: 1007
                        // });
                        resultBPL = {};

                        resultBPL.rp = {};
                        resultBPL.rp.ah = {};
                        resultBPL.rp.ah.h = "0";
                        resultBPL.rp.ah.a = "0";

                        resultBPL.rp.ou = {};
                        resultBPL.rp.ou.h = "0";
                        resultBPL.rp.ou.a = "0";

                        resultBPL.rp.x12 = {};
                        resultBPL.rp.x12.h = "0";
                        resultBPL.rp.x12.a = "0";

                        resultBPL.rp.oe = {};
                        resultBPL.rp.oe.h = "0";
                        resultBPL.rp.oe.a = "0";

                        resultBPL.rp.ah1st = {};
                        resultBPL.rp.ah1st.h = "0";
                        resultBPL.rp.ah1st.a = "0";

                        resultBPL.rp.ou1st = {};
                        resultBPL.rp.ou1st.h = "0";
                        resultBPL.rp.ou1st.a = "0";

                        resultBPL.rp.x121st = {};
                        resultBPL.rp.x121st.h = "0";
                        resultBPL.rp.x121st.a = "0";

                        resultBPL.rp.oe1st = {};
                        resultBPL.rp.oe1st.h = "0";
                        resultBPL.rp.oe1st.a = "0";
                    }


                    let odd = _.filter(oddAdjust, (o)=>{
                        return o.matchId === id;
                    });

                    // console.log('resultBPL ===> ', JSON.stringify(resultBPL));

                    Commons.switch_type(type_lower, bp, rp_live, model_result, resultBPL.rp, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, odd, (err, result)=>{
                        if(err){
                            return res.send({message: err, code: 10102});
                        } else {
                            let results2 = [result];

                            // console.log('results2 ===> ', JSON.stringify(results2));
                            let betPrice = _.chain(results2)
                                .flatten(true)
                                .map(betPrice => {
                                    return _.findWhere(betPrice, predicate);
                                })
                                .reject(betPrice => {
                                    return _.isUndefined(betPrice)
                                })
                                .map(betPrice => {
                                    return Commons.getBetObjectByKey(prefix, key, betPrice);
                                })
                                .first()
                                .value();

                            let min_max = {};

                            if(srl && !_.isUndefined(srl.pm) && srl.isOn){
                                min_max = srl;
                            } else {
                                min_max = _.first(results).rl;
                            }

                            // console.log('=======', JSON.stringify(resultBPL.srl));
                            // console.log('--------', JSON.stringify(_.first(results).rl));
                            // console.log('+++++++++', min_max);

                            if( _.isUndefined(betPrice) || _.isNull(betPrice)){
                                return res.send({
                                    message: "resultBP is undefined ",
                                    code: 1007
                                });
                            }

                            Commons.switch_min_max(type_lower, min_max, betPrice, (err, dataResult)=>{
                                // console.log('dataResult => ', dataResult);

                                dataResult.score = {
                                    h: match_cache.detail.h,
                                    a: match_cache.detail.a
                                };

                                // console.log('limitSetting ===> ', limitSetting);
                                if(_.isEqual(type_lower, 'x12') || _.isEqual(type_lower, 'x121st')){
                                    // console.log('dataResult ====> ', dataResult)

                                    dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet);
                                    // console.log('2. MAX ====> ', dataResult.max);
                                    if(dataResult.a){
                                        dataResult.max = dataResult.max / dataResult.a;
                                    } else if(dataResult.h){
                                        dataResult.max = dataResult.max / dataResult.h;
                                    } else if(dataResult.d){
                                        dataResult.max = dataResult.max / dataResult.d;
                                    }
                                } else {
                                    // console.log('===========>', dataResult.max, limitSetting.sportsBook.hdpOuOe.maxPerBet);
                                    dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.hdpOuOe.maxPerBet);
                                }

                                // console.log('indexRound ===> ', indexRound);

                                Commons.calculateBetPrice(dataResult.max, indexRound, (err, data)=>{
                                    dataResult.max = data;
                                });

                                // dataResult.nmin = convertMinMaxByCurrency(dataResult.min, currency, membercurrency);
                                // dataResult.nmax = convertMinMaxByCurrency(dataResult.max, currency, membercurrency);
                                dataResult.min = convertMinMaxByCurrency(dataResult.min, currency, membercurrency);
                                dataResult.max = convertMinMaxByCurrency(dataResult.max, currency, membercurrency);
                                return res.send({
                                    message: "success",
                                    result: dataResult,
                                    code: 0
                                });

                            })

                        }
                    })
                }
            });
        }
    })
});

//[5]
router.put('/hdp_match_bet_price', (req, res) => {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    let indexRound;

    const {
        id,
        prefix,
        key,
        value
    } = req.body;

    // console.log(req.body);
    const predicate = JSON.parse(`{"${key}":"${value}"}`);

    // console.log(`${URL_CACHE_LIVE_SINGLE_MATCH}/${id}`);

    // console.log(predicate);

    const k = id.split(':');

    // console.log(req.userInfo.username);
    const membercurrency = req.userInfo.currency;
    async.series([
        (callback) => {
            MemberService.getLimitSetting(req.userInfo._id, (errLimitSetting, dataLimitSetting)=>{
                if(errLimitSetting){
                    callback(errLimitSetting, null);
                } else {
                    callback(null, dataLimitSetting.limitSetting);
                }
            });
        },

        (callback) => {
            MemberService.getOddAdjustment( req.userInfo.username, (err, data) => {
                if (err) {
                    // console.log('Get Odd Adjust error');
                    callback(err, null);
                } else {
                    callback(null, data);
                }
            });
        },

        (callback) => {
            RedisService.getCurrency((error, result) => {
                if (error) {
                    callback(null, false);
                } else {
                    callback(null, result);
                }
            })
        }
    ], (errAsync, dataAsync)=> {
        if (errAsync) {
            return res.send({
                message: "error",
                result: errAsync,
                code: 998
            });
        } else {

            const [
                limitSetting,
                oddAdjust,
                currency
            ] = dataAsync;

            async.series([(cb)=>{
                FootballModel.findOne({k: k[0]},
                    (err, data)=>{
                        if(err){
                            cb(err, null)
                        } else if(data){
                            cb(null, data)
                        } else {
                            cb('league is null', null)
                        }
                    })
            }, (cb)=>{

                const headers = {
                    'Content-Type' : 'application/json'
                };

                const option = {
                    url: `${URL_CACHE_HDP_SINGLE_MATCH}/${id}`,
                    method: 'GET',
                    headers: headers
                };

                request(option, (err, response, body) => {
                    if (err) {
                        cb(err, null)
                    } else {
                        try {
                            let result = JSON.parse(body);

                            if(result){
                                cb(null, result);
                            } else {
                                cb(null, []);
                            }
                        } catch (e) {
                            console.error(e);
                            request(option, (err, response, body) => {
                                if (err) {
                                    cb(err, null)
                                } else {
                                    try {
                                        let result = JSON.parse(body);

                                        if(result){
                                            cb(null, result);
                                        } else {
                                            cb(null, []);
                                        }
                                    } catch (e) {
                                        console.error(e);
                                        request(option, (err, response, body) => {
                                            if (err) {
                                                cb(err, null)
                                            } else {
                                                try {
                                                    let result = JSON.parse(body);

                                                    if(result){
                                                        cb(null, result);
                                                    } else {
                                                        cb(null, []);
                                                    }
                                                } catch (e) {
                                                    console.error(e);
                                                    request(option, (err, response, body) => {
                                                        if (err) {
                                                            cb(err, null)
                                                        } else {
                                                            try {
                                                                let result = JSON.parse(body);

                                                                if(result){
                                                                    cb(null, result);
                                                                } else {
                                                                    cb(null, []);
                                                                }
                                                            } catch (e) {
                                                                console.error(e);
                                                                request(option, (err, response, body) => {
                                                                    if (err) {
                                                                        cb(err, null)
                                                                    } else {
                                                                        let result = JSON.parse(body);

                                                                        if(result){
                                                                            cb(null, result);
                                                                        } else {
                                                                            cb(null, []);
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    });
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }
                });

            }], (err, results)=>{
                if(err){
                    return res.send({message: err, code: 10102});
                } else {
                    const type_lower = prefix.toLowerCase();
                    const rp_hdp = results[0].rp.hdp;
                    let match_cache = results[1];
                    let bp =  _.first(_.filter(match_cache.result, league => {
                        return _.contains(league.allKey, value)
                    }));

                    let model_result = {};
                    let isToday = false;
                    // let min_max_r = _.first(results).r;
                    // let min_max_rem = _.first(results).rem;

                    let sr = {};
                    let srem = {};

                    const resultBP = _.first(_.chain(results)
                        .filter((league, index) => {
                            return _.isEqual(league.k, parseInt(k[0]))
                        })
                        .flatten(true)
                        .pluck('m')
                        .flatten(true)
                        .filter(matchAPI => {
                            return _.isEqual(matchAPI.id, id)
                        })
                        .each(matchAPI => {
                            isToday = matchAPI.isToday;

                            if(matchAPI.sr /*&& matchAPI.sr.isOn*/){
                                sr = matchAPI.sr;
                            }

                            if(matchAPI.srem /*&& matchAPI.srem.isOn*/){
                                srem = matchAPI.srem;
                            }

                            _.each(matchAPI.bp, (betPrice, index) => {
                                if(betPrice.allKey.indexOf(value) > -1){
                                    indexRound = index;
                                }
                            })
                        })
                        .pluck('bp')
                        .flatten(true)
                        .filter(betPrice => {
                            return _.contains(betPrice.allKey, value);
                        })
                        .map(betPrice => {
                            return _.pick(betPrice, 'rp', 'sr', 'srem')
                        })
                        .flatten(true)
                        .value());

                    // console.log('resultBP => ', JSON.stringify(resultBP));

                    if( _.isUndefined(resultBP) || _.isUndefined(resultBP.rp) ){
                        return res.send({
                            message: "resultBP is undefined ",
                            code: 1007
                        });
                    }

                    let odd = _.filter(oddAdjust, (o)=>{
                        // console.log(o.matchId , id);
                        return o.matchId === id;
                    });

                    Commons.switch_type(type_lower, bp, rp_hdp, model_result, resultBP.rp, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, odd, (err, result)=>{
                        if(err){
                            return res.send({message: err, code: 10102});
                        } else {
                            let result_new_array = [result];
                            let betPrice = _.chain(result_new_array)
                                .flatten(true)
                                .map(betPrice => {
                                    return _.findWhere(betPrice, predicate);
                                })
                                .reject(betPrice => {
                                    return _.isUndefined(betPrice)
                                })
                                .map((betPrice, index) => {
                                    // indexRound = index;
                                    return Commons.getBetObjectByKey(prefix, key, betPrice);
                                })
                                .first()
                                .value();

                            // console.log('isToday ====> ', isToday);
                            if(isToday){
                                // console.log('-----------', JSON.stringify(_.first(results).r));
                                // console.log('-----------', JSON.stringify(sr));

                                let min_max = {};
                                // console.log('sr ====> ', sr);
                                if(sr && !_.isUndefined(sr.pm) && sr.isOn){
                                    // console.log('=== 1 ====');
                                    min_max = sr;
                                } else {
                                    // console.log('=== 2 ====');
                                    min_max = _.first(results).r;
                                }

                                // console.log('min_max ===> ', min_max);

                                Commons.switch_min_max(type_lower, min_max, betPrice, (err, dataResult)=>{
                                    if(_.isEqual(type_lower, 'x12') || _.isEqual(type_lower, 'x121st')){

                                        dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet);
                                        if(dataResult.a){
                                            dataResult.max = dataResult.max / dataResult.a;
                                        } else if(dataResult.h){
                                            dataResult.max = dataResult.max / dataResult.h;
                                        } else if(dataResult.d){
                                            dataResult.max = dataResult.max / dataResult.d;
                                        }
                                    } else {
                                        dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.hdpOuOe.maxPerBet);
                                    }

                                    if(isToday){
                                        Commons.calculateBetPriceByTime(dataResult.max, now, new Date(match_cache.date), (err, data)=>{
                                            dataResult.max = data;
                                        })
                                    }

                                    // console.log('indexRound ===> ', indexRound);

                                    Commons.calculateBetPrice(dataResult.max, indexRound, (err, data)=>{
                                        dataResult.max = data;
                                    })

                                    // dataResult.nmin = convertMinMaxByCurrency(dataResult.min, currency, membercurrency);
                                    // dataResult.nmax = convertMinMaxByCurrency(dataResult.max, currency, membercurrency);
                                    // console.log(dataResult.nmin);
                                    // console.log(dataResult.nmax);
                                    dataResult.min = convertMinMaxByCurrency(dataResult.min, currency, membercurrency);
                                    dataResult.max = convertMinMaxByCurrency(dataResult.max, currency, membercurrency);
                                    return res.send({
                                        message: "success",
                                        result: dataResult,
                                        code: 0
                                    });
                                })
                            } else {

                                let min_max = {};

                                if(srem && !_.isUndefined(srem.pm) && srem.isOn){
                                    min_max = srem;
                                } else {
                                    min_max = _.first(results).rem;
                                }

                                // console.log('----- 2 ------');
                                Commons.switch_min_max(type_lower, min_max, betPrice, (err, dataResult)=>{
                                    // console.log('limitSetting ===> ', limitSetting);
                                    if(_.isEqual(type_lower, 'x12') || _.isEqual(type_lower, 'x121st')){
                                        dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet);
                                        if(dataResult.a){
                                            dataResult.max = dataResult.max / dataResult.a;
                                        } else if(dataResult.h){
                                            dataResult.max = dataResult.max / dataResult.h;
                                        } else if(dataResult.d){
                                            dataResult.max = dataResult.max / dataResult.d;
                                        }
                                    } else {
                                        // console.log(dataResult.max, limitSetting.sportsBook.hdpOuOe.maxPerBet);
                                        dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.hdpOuOe.maxPerBet);
                                    }

                                    if(isToday){
                                        Commons.calculateBetPriceByTime(dataResult.max, now, new Date(match_cache.date), (err, data)=>{
                                            dataResult.max = data;
                                        })
                                    } else {
                                        Commons.calculateBetPrice(dataResult.max, indexRound, (err, data)=>{
                                            dataResult.max = data;
                                        });
                                    }

                                    dataResult.min = convertMinMaxByCurrency(dataResult.min, currency, membercurrency);
                                    dataResult.max = convertMinMaxByCurrency(dataResult.max, currency, membercurrency);
                                    // console.log(dataResult.nmin);
                                    // console.log(dataResult.nmax);
                                    return res.send({
                                        message: "success",
                                        result: dataResult,
                                        code: 0
                                    });
                                })
                            }
                        }
                    })
                }
            });
        }
    });
});

//[6]
router.get('/backend/hdp', (req, res) => {
    const day = req.query.d || 0;
    const month = req.query.m || 0;
    const startDate = new Date(_.now());
    const endDate = new Date(_.now());

    console.log('====== Start Date ======' + day + ":" + month);
    startDate.setMonth(month - 1);
    startDate.setDate(day);
    startDate.setHours(11 + 7);
    startDate.setMinutes(0);
    startDate.setSeconds(0);
    console.log(startDate);

    console.log('====== End Date ======' + day + ":" + month);
    endDate.setMonth(month - 1);
    endDate.setDate(day);
    endDate.setDate(endDate.getDate() + 1);
    endDate.setHours(10 + 7);
    endDate.setMinutes(59);
    endDate.setSeconds(59);
    console.log(endDate);

    const now = new Date(new Date().getTime());
    console.log(URL_HDP);
    request.get(
        URL_HDP,
        (err, response) => {
            if(err){
                console.log('[6.1 ] 999 : ==> \n', err);
                return res.send(
                    {
                        code: 9999,
                        message: err
                    });
            } else {
                FootballModel.find({})
                    .lean()
                    .sort('sk')
                    .exec((err, data) => {
                        if (err) {
                            // console.log('[6.2 ] 999 : ==> \n', err);
                            return res.send({
                                message: "success",
                                result: [],
                                code: 0
                            });
                        }  else if (_.size(data) === 0) {
                            // console.log('[6.3 ] Data not found');
                            return res.send({
                                message: "success",
                                result: [],
                                code: 0
                            });
                        } else {
                            const dataClone = clone(data);
                            let matchHDPFromDBArray = _.chain(data)
                                .each(league => {
                                    league.m =
                                        _.filter(league.m, match => {
                                            return match.s.isH && match.d >= now && match.d >= startDate && match.d <= endDate
                                        });
                                })
                                .reject(league => {
                                    return _.size(league.m) === 0
                                })
                                .value();

                            const matchIdHDPFromDBArray = _.chain(matchHDPFromDBArray)
                                .flatten(true)
                                .pluck('m')
                                .flatten(true)
                                .pluck('id')
                                .value();

                            if (_.size(matchIdHDPFromDBArray) !== 0) {
                                // console.log("[6.4] HDP match form database : " + JSON.stringify(matchIdHDPFromDBArray));
                                const result = JSON.parse(response.body);
                                const matchHDPAPIArray = _.chain(result.result)
                                    .flatten(true)
                                    .pluck('m')
                                    .value();

                                const matchIdAPIArray = _.chain(matchHDPAPIArray)
                                    .flatten(true)
                                    .pluck('id')
                                    .value();

                                let resultMatchedArray = _.intersection(matchIdHDPFromDBArray, matchIdAPIArray);
                                console.log("[6.6] HDP matched : " + JSON.stringify(resultMatchedArray));

                                let resultTotalArray = _.chain(result.result)
                                    .each(league => {
                                        delete league._id;
                                        league.m = _.filter(league.m, match => {
                                            delete match._id;
                                            return _.contains(resultMatchedArray, match.id)
                                        });
                                    })
                                    .reject(league => { return _.size(league.m) === 0 })
                                    .value();

                                matchHDPFromDBArray = _.chain(data)
                                    .each(league => {
                                        _.each(league.m, match => {
                                            match.bp = _.filter(match.bp, betPrise => { return betPrise.isOnM; });
                                            const resultBP = _.chain(resultTotalArray)
                                                .filter(leagueAPI => {
                                                    return _.isEqual(leagueAPI.k, league.k)
                                                })
                                                .flatten(true)
                                                .pluck('m')
                                                .flatten(true)
                                                .filter(matchAPI => {
                                                    return _.isEqual(matchAPI.k, match.k)
                                                })
                                                .each(matchAPI => {
                                                    match.i = matchAPI.i;
                                                })
                                                .pluck('bp')
                                                .flatten(true)
                                                .value();
                                            let i = 0;
                                            _.each(match.bp, bp => {
                                                if (!_.isUndefined(bp.allKey) &&
                                                    !_.isUndefined(resultBP[i]) &&
                                                    !_.isUndefined(resultBP[i].allKey) &&
                                                    _.size(_.intersection(bp.allKey, resultBP[i].allKey)) > 0) {
                                                    // delete bp.r;
                                                    match.bp[i] = resultBP[i];
                                                }
                                                i++;
                                            });
                                        });
                                        league.m = _.reject(league.m, match => { return _.size(match.bp) === 0 });
                                    })
                                    .reject(league => { return _.size(league.m) === 0 })
                                    .value();

                                return res.send({
                                    message: "success",
                                    result: _.sortBy(matchHDPFromDBArray, 'sk'),
                                    code: 0
                                });

                            } else {
                                return res.send({
                                    message: "success",
                                    result: [],
                                    code: 0
                                });
                            }
                        }
                    });
            }
        });
});

//[7]
router.get('/backend/hdp_cal', (req, res) => {
    const day = req.query.d || 0;
    const month = req.query.m || 0;
    const startDate = new Date(_.now());
    const endDate = new Date(_.now());

    console.log('====== Start Date ======' + day + ":" + month);
    startDate.setMonth(month - 1);
    startDate.setDate(day);
    startDate.setHours(11 + 7);
    startDate.setMinutes(0);
    startDate.setSeconds(0);
    console.log(startDate);

    console.log('====== End Date ======' + day + ":" + month);
    endDate.setMonth(month - 1);
    endDate.setDate(day);
    endDate.setDate(endDate.getDate() + 1);
    endDate.setHours(10 + 7);
    endDate.setMinutes(59);
    endDate.setSeconds(59);
    console.log(endDate);

    const now = new Date(new Date().getTime());
    console.log(URL_HDP);
    request.get(
        URL_HDP,
        (err, response) => {
            if(err){
                console.log('[7.1 ] 999 : ==> \n', err);
                return res.send(
                    {
                        code: 9999,
                        message: err
                    });
            } else {
                FootballModel.find({})
                    .lean()
                    .sort('sk')
                    .exec((err, data) => {
                        if (err) {
                            console.log('[7.2 ] 999 : ==> \n', err);
                            return res.send({
                                message: "success",
                                result: [],
                                code: 0
                            });
                        }  else if (_.size(data) === 0) {
                            console.log('[7.3 ] Data not found');
                            return res.send({
                                message: "success",
                                result: [],
                                code: 0
                            });
                        } else {
                            const dataClone = clone(data);
                            let matchHDPFromDBArray = _.chain(data)
                                .each(league => {
                                    league.m =
                                        _.filter(league.m, match => {
                                            return match.s.isH && match.d >= now && match.d >= startDate && match.d <= endDate
                                        });
                                })
                                .reject(league => {
                                    return _.size(league.m) === 0
                                })
                                .value();

                            const matchIdHDPFromDBArray = _.chain(matchHDPFromDBArray)
                                .flatten(true)
                                .pluck('m')
                                .flatten(true)
                                .pluck('id')
                                .value();

                            if (_.size(matchIdHDPFromDBArray) !== 0) {
                                console.log("[7.4] HDP match form database : " + JSON.stringify(matchIdHDPFromDBArray));
                                const result = JSON.parse(response.body);
                                const matchHDPAPIArray = _.chain(result.result)
                                    .flatten(true)
                                    .pluck('m')
                                    .value();

                                const matchIdAPIArray = _.chain(matchHDPAPIArray)
                                    .flatten(true)
                                    .pluck('id')
                                    .value();
                                // console.log("[1.5] HDP match form API : " + JSON.stringify(matchIdAPIArray));

                                let resultMatchedArray = _.intersection(matchIdHDPFromDBArray, matchIdAPIArray);
                                console.log("[7.6] HDP matched : " + JSON.stringify(resultMatchedArray));

                                let resultTotalArray = _.chain(result.result)
                                    .each(league => {
                                        // delete league._id;
                                        league.m = _.filter(league.m, match => {
                                            // delete match._id;
                                            return _.contains(resultMatchedArray, match.id)
                                        });
                                    })
                                    .reject(league => { return _.size(league.m) === 0 })
                                    .value();

                                // console.log("=====DB=====");
                                matchHDPFromDBArray = _.chain(data)
                                    .each(league => {
                                        _.each(league.m, match => {
                                            match.bp = _.filter(match.bp, betPrise => { return betPrise.isOnM; });
                                            const resultBP = _.chain(resultTotalArray)
                                                .filter(leagueAPI => {
                                                    return _.isEqual(leagueAPI.k, league.k)
                                                })
                                                .flatten(true)
                                                .pluck('m')
                                                .flatten(true)
                                                .filter(matchAPI => {
                                                    return _.isEqual(matchAPI.k, match.k)
                                                })
                                                .each(matchAPI => {
                                                    match.i = matchAPI.i;
                                                })
                                                .pluck('bp')
                                                .flatten(true)
                                                .value();
                                            let i = 0;
                                            _.each(match.bp, bp => {
                                                if (!_.isUndefined(bp.allKey) &&
                                                    !_.isUndefined(resultBP[i]) &&
                                                    !_.isUndefined(resultBP[i].allKey) &&
                                                    _.size(_.intersection(bp.allKey, resultBP[i].allKey)) > 0) {
                                                    // delete bp.r;
                                                    // console.log(match.id+" OLD : "+JSON.stringify(bp));
                                                    match.bp[i] = resultBP[i];
                                                    // console.log(match.id+" NEW : "+JSON.stringify(match.bp[i]));
                                                }
                                                i++;
                                            });
                                        });
                                        league.m = _.reject(league.m, match => { return _.size(match.bp) === 0 });
                                    })
                                    .reject(league => { return _.size(league.m) === 0 })
                                    .each(league => {
                                        _.each(league.m, match => {
                                            if(league.rp && league.rp.hdp){
                                                if(_.size(match.bp) > 0){
                                                    _.each(match.bp, bp =>{

                                                        const resultBP = _.first(_.chain(dataClone)
                                                            .filter(leagueAPI => {
                                                                return _.isEqual(leagueAPI.k, league.k)
                                                            })
                                                            .flatten(true)
                                                            .pluck('m')
                                                            .flatten(true)
                                                            .filter(matchAPI => {
                                                                return _.isEqual(matchAPI.k, match.k)
                                                            })
                                                            .each(matchAPI => {
                                                                match.i = matchAPI.i;
                                                            })
                                                            .pluck('bp')
                                                            .flatten(true)
                                                            .filter(betPrice => {
                                                                return _.size(_.intersection(bp.allKey, betPrice.allKey)) > 0;
                                                            })
                                                            .map(betPrice => {
                                                                return _.pick(betPrice, 'rp', 'r', 'rm', '_id', 'isOn')
                                                            })
                                                            .flatten(true)
                                                            .value());

                                                        // console.log('resultBP => ', resultBP);

                                                        bp._id = resultBP._id;
                                                        bp.rp = resultBP.rp;
                                                        bp.r = resultBP.r;
                                                        bp.rm = resultBP.rm;
                                                        bp.isOn = resultBP.isOn;

                                                        // console.log('resultBP => ', resultBP);

                                                        // delete bp._id;
                                                        // delete bp.r;
                                                        // delete bp.allKey;
                                                        // delete bp.isOn;

                                                        let ou1st = {o: 0, u: 0};
                                                        let ou = {o: 0, u: 0};
                                                        let oe = {o: 0, e: 0};
                                                        let oe1st = {o: 0, e: 0};
                                                        let ah = {home: 0, away: 0};
                                                        let ah1st = {home: 0, away: 0};

                                                        // console.log('odd ====> ', odd);

                                                        // ---------------------------  ou1st  -----------------------------
                                                        if(bp.ou1st){
                                                            if(bp.ou1st.op || bp.ou1st.up){
                                                                Commons.cal_ou(bp.ou1st.op, bp.ou1st.up, league.rp.hdp.ou1st.f, league.rp.hdp.ou1st.u, resultBP.rp.ou1st, '', ou1st, (err, data)=>{
                                                                    if(data){
                                                                        bp.ou1st.op = data.op;
                                                                        bp.ou1st.up = data.up;
                                                                    }
                                                                })
                                                            }
                                                        }

                                                        // ---------------------------  ou  ----------------------------------
                                                        if(bp.ou){
                                                            if(bp.ou.op || bp.ou.up){
                                                                Commons.cal_ou(bp.ou.op, bp.ou.up, league.rp.hdp.ou.f, league.rp.hdp.ou.u, resultBP.rp.ou, '', ou, (err, data)=>{
                                                                    if(data){
                                                                        bp.ou.op = data.op;
                                                                        bp.ou.up = data.up;
                                                                    }
                                                                })
                                                            }
                                                        }

                                                        // ---------------------------  oe1st  ----------------------------------
                                                        if(bp.oe1st){
                                                            if(bp.oe1st.o || bp.oe1st.e){
                                                                Commons.cal_oe(bp.oe1st.o, bp.oe1st.e, league.rp.hdp.oe1st.f, league.rp.hdp.oe1st.u, resultBP.rp.oe1st, '', oe1st, (err, data)=>{
                                                                    if(data){
                                                                        bp.oe1st.o = data.o;
                                                                        bp.oe1st.e = data.e;
                                                                    }
                                                                })
                                                            }
                                                        }

                                                        // ---------------------------  oe  ----------------------------------
                                                        if(bp.oe){
                                                            if(bp.oe.o || bp.oe.e){
                                                                Commons.cal_oe(bp.oe.o, bp.oe.e, league.rp.hdp.oe.f, league.rp.hdp.oe.u, resultBP.rp.oe, '', oe, (err, data)=>{
                                                                    if(data){
                                                                        bp.oe.o = data.o;
                                                                        bp.oe.e = data.e;
                                                                    }
                                                                })
                                                            }
                                                        }

                                                        // ---------------------------  ah1st  ----------------------------------
                                                        if(bp.ah1st){
                                                            if(bp.ah1st.h || bp.ah1st.a){
                                                                Commons.cal_ah(bp.ah1st.h, bp.ah1st.a, bp.ah1st.hp, bp.ah1st.ap, league.rp.hdp.ah1st.f, league.rp.hdp.ah1st.u, resultBP.rp.ah1st, '', ah1st, (err, data)=>{
                                                                    if(data){
                                                                        bp.ah1st.hp = data.hp;
                                                                        bp.ah1st.ap = data.ap;
                                                                    }
                                                                })
                                                            }
                                                        }

                                                        // ---------------------------  ah  ----------------------------------
                                                        if(bp.ah){
                                                            if(bp.ah.h || bp.ah.a){
                                                                Commons.cal_ah(bp.ah.h, bp.ah.a, bp.ah.hp, bp.ah.ap, league.rp.hdp.ah.f, league.rp.hdp.ah.u, resultBP.rp.ah, '', ah, (err, data)=>{
                                                                    if(data){
                                                                        bp.ah.hp = data.hp;
                                                                        bp.ah.ap = data.ap;
                                                                    }
                                                                })
                                                            }
                                                        }
                                                        // ---------------------------  x121st  ----------------------------------
                                                        if(bp.x121st){
                                                            if(bp.x121st.h || bp.x121st.a || bp.x121st.d){
                                                                Commons.cal_x12(bp.x121st.h, bp.x121st.a, bp.x121st.d, resultBP.rp.x121st, (err, data)=>{
                                                                    if(data){
                                                                        bp.x121st.h = data.h;
                                                                        bp.x121st.a = data.a;
                                                                        bp.x121st.d = data.d;
                                                                    }
                                                                })
                                                            }
                                                        }

                                                        // ---------------------------  x12  ----------------------------------
                                                        if(bp.x12){
                                                            if(bp.x12.h || bp.x12.a || bp.x12.d){
                                                                Commons.cal_x12(bp.x12.h, bp.x12.a, bp.x12.d, resultBP.rp.x12, (err, data)=>{
                                                                    if(data){
                                                                        bp.x12.h = data.h;
                                                                        bp.x12.a = data.a;
                                                                        bp.x12.d = data.d;
                                                                    }
                                                                })
                                                            }
                                                        }
                                                    });
                                                }
                                            }
                                        });
                                    })
                                    .each(league => {
                                        const r = league.r;
                                        console.log(`####################################`);
                                        console.log(`${league.k} ${league.n}`);
                                        console.log(`R   Max pay out ${r.pm}`);
                                        console.log(`ah     : ${JSON.stringify(r.ah.ma)} ${JSON.stringify(r.ah.mi)}`);
                                        console.log(`ou     : ${JSON.stringify(r.ou.ma)} ${JSON.stringify(r.ou.mi)}`);
                                        console.log(`x12    : ${JSON.stringify(r.x12.ma)} ${JSON.stringify(r.x12.mi)}`);
                                        console.log(`oe     : ${JSON.stringify(r.oe.ma)} ${JSON.stringify(r.oe.mi)}`);
                                        console.log(`ah1st  : ${JSON.stringify(r.ah1st.ma)} ${JSON.stringify(r.ah1st.mi)}`);
                                        console.log(`ou1st  : ${JSON.stringify(r.ou1st.ma)} ${JSON.stringify(r.ou1st.mi)}`);
                                        console.log(`x121st : ${JSON.stringify(r.x121st.ma)} ${JSON.stringify(r.x121st.mi)}`);
                                        console.log(`oe1st  : ${JSON.stringify(r.oe1st.ma)} ${JSON.stringify(r.oe1st.mi)}`);
                                        console.log(`####################################`);
                                        const rem = league.rem;
                                        console.log(`####################################`);
                                        console.log(`${league.k} ${league.n}`);
                                        console.log(`REM Max pay out ${rem.pm}`);
                                        console.log(`ah     : ${JSON.stringify(rem.ah.ma)} ${JSON.stringify(rem.ah.mi)}`);
                                        console.log(`ou     : ${JSON.stringify(rem.ou.ma)} ${JSON.stringify(rem.ou.mi)}`);
                                        console.log(`x12    : ${JSON.stringify(rem.x12.ma)} ${JSON.stringify(rem.x12.mi)}`);
                                        console.log(`oe     : ${JSON.stringify(rem.oe.ma)} ${JSON.stringify(rem.oe.mi)}`);
                                        console.log(`ah1st  : ${JSON.stringify(rem.ah1st.ma)} ${JSON.stringify(rem.ah1st.mi)}`);
                                        console.log(`ou1st  : ${JSON.stringify(rem.ou1st.ma)} ${JSON.stringify(rem.ou1st.mi)}`);
                                        console.log(`x121st : ${JSON.stringify(rem.x121st.ma)} ${JSON.stringify(rem.x121st.mi)}`);
                                        console.log(`oe1st  : ${JSON.stringify(rem.oe1st.ma)} ${JSON.stringify(rem.oe1st.mi)}`);
                                        console.log(`####################################`);

                                        _.each(league.m, match => {
                                            match.r = {};
                                            let rule;

                                            if (match.isToday) {
                                                rule = r;
                                                match.r.pm = r.pm;
                                                if (!_.isUndefined(match.sr) && !_.isUndefined(match.sr.pm) && match.sr.isOn) {
                                                    rule = match.sr;
                                                    match.r.pm = match.sr.pm;

                                                    console.log(`$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$`);
                                                    console.log(`R   Max pay out ${rule.pm}`);
                                                    console.log(`ah     : ${JSON.stringify(rule.ah.ma)} ${JSON.stringify(rule.ah.mi)}`);
                                                    console.log(`ou     : ${JSON.stringify(rule.ou.ma)} ${JSON.stringify(rule.ou.mi)}`);
                                                    console.log(`x12    : ${JSON.stringify(rule.x12.ma)} ${JSON.stringify(rule.x12.mi)}`);
                                                    console.log(`oe     : ${JSON.stringify(rule.oe.ma)} ${JSON.stringify(rule.oe.mi)}`);
                                                    console.log(`ah1st  : ${JSON.stringify(rule.ah1st.ma)} ${JSON.stringify(rule.ah1st.mi)}`);
                                                    console.log(`ou1st  : ${JSON.stringify(rule.ou1st.ma)} ${JSON.stringify(rule.ou1st.mi)}`);
                                                    console.log(`x121st : ${JSON.stringify(rule.x121st.ma)} ${JSON.stringify(rule.x121st.mi)}`);
                                                    console.log(`oe1st  : ${JSON.stringify(rule.oe1st.ma)} ${JSON.stringify(rule.oe1st.mi)}`);
                                                    console.log(`$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$`);
                                                }

                                            } else {
                                                rule = rem;
                                                match.r.pm = rem.pm;
                                                if (!_.isUndefined(match.srem) && !_.isUndefined(match.srem.pm) && match.srem.isOn) {
                                                    rule = match.srem;
                                                    match.r.pm = match.srem.pm;

                                                    console.log(`$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$`);
                                                    console.log(`R   Max pay out ${rule.pm}`);
                                                    console.log(`ah     : ${JSON.stringify(rule.ah.ma)} ${JSON.stringify(rule.ah.mi)}`);
                                                    console.log(`ou     : ${JSON.stringify(rule.ou.ma)} ${JSON.stringify(rule.ou.mi)}`);
                                                    console.log(`x12    : ${JSON.stringify(rule.x12.ma)} ${JSON.stringify(rule.x12.mi)}`);
                                                    console.log(`oe     : ${JSON.stringify(rule.oe.ma)} ${JSON.stringify(rule.oe.mi)}`);
                                                    console.log(`ah1st  : ${JSON.stringify(rule.ah1st.ma)} ${JSON.stringify(rule.ah1st.mi)}`);
                                                    console.log(`ou1st  : ${JSON.stringify(rule.ou1st.ma)} ${JSON.stringify(rule.ou1st.mi)}`);
                                                    console.log(`x121st : ${JSON.stringify(rule.x121st.ma)} ${JSON.stringify(rule.x121st.mi)}`);
                                                    console.log(`oe1st  : ${JSON.stringify(rule.oe1st.ma)} ${JSON.stringify(rule.oe1st.mi)}`);
                                                    console.log(`$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$`);
                                                }

                                            }

                                            let round = 0;
                                            console.log(`       ${match.n.en.h} vs ${match.n.en.a}`);
                                            _.each(match.bp, betPrice => {
                                                console.log(`       ============== REM ============== ${match.id} BetPrice = ${round}`);
                                                betPrice.r = {};
                                                betPrice.r.ah = {};
                                                betPrice.r.ou = {};
                                                betPrice.r.x12 = {};
                                                betPrice.r.oe = {};
                                                betPrice.r.ah1st = {};
                                                betPrice.r.ou1st = {};
                                                betPrice.r.x121st = {};
                                                betPrice.r.oe1st = {};
                                                const {
                                                    ah,
                                                    ou,
                                                    x12,
                                                    oe,
                                                    ah1st,
                                                    ou1st,
                                                    x121st,
                                                    oe1st
                                                } = rule;

                                                betPrice.r.ah.ma = calculateBetPrice(ah.ma, round);
                                                betPrice.r.ah.mi = ah.mi;

                                                betPrice.r.ou.ma = calculateBetPrice(ou.ma, round);
                                                betPrice.r.ou.mi = ou.mi;

                                                betPrice.r.x12.ma = calculateBetPrice(x12.ma, round);
                                                betPrice.r.x12.mi = x12.mi;

                                                betPrice.r.oe.ma = calculateBetPrice(oe.ma, round);
                                                betPrice.r.oe.mi = oe.mi;

                                                betPrice.r.ah1st.ma = calculateBetPrice(ah1st.ma, round);
                                                betPrice.r.ah1st.mi = ah1st.mi;

                                                betPrice.r.ou1st.ma = calculateBetPrice(ou1st.ma, round);
                                                betPrice.r.ou1st.mi = ou1st.mi;

                                                betPrice.r.x121st.ma = calculateBetPrice(x121st.ma, round);
                                                betPrice.r.x121st.mi = x121st.mi;

                                                betPrice.r.oe1st.ma = calculateBetPrice(oe1st.ma, round);
                                                betPrice.r.oe1st.mi = oe1st.mi;

                                                console.log(`       ah     : ${JSON.stringify(betPrice.r.ah.ma)} ${JSON.stringify(betPrice.r.ah.mi)}`);
                                                console.log(`       ou     : ${JSON.stringify(betPrice.r.ou.ma)} ${JSON.stringify(betPrice.r.ou.mi)}`);
                                                console.log(`       x12    : ${JSON.stringify(betPrice.r.x12.ma)} ${JSON.stringify(betPrice.r.x12.mi)}`);
                                                console.log(`       oe     : ${JSON.stringify(betPrice.r.oe.ma)} ${JSON.stringify(betPrice.r.oe.mi)}`);
                                                console.log(`       ah1st  : ${JSON.stringify(betPrice.r.ah1st.ma)} ${JSON.stringify(betPrice.r.ah1st.mi)}`);
                                                console.log(`       ou1st  : ${JSON.stringify(betPrice.r.ou1st.ma)} ${JSON.stringify(betPrice.r.ou1st.mi)}`);
                                                console.log(`       x121st : ${JSON.stringify(betPrice.r.x121st.ma)} ${JSON.stringify(betPrice.r.x121st.mi)}`);
                                                console.log(`       oe1st  : ${JSON.stringify(betPrice.r.oe1st.ma)} ${JSON.stringify(betPrice.r.oe1st.mi)}`);
                                                round++;
                                            });
                                        });
                                    })
                                    .value();

                                const now_date = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);

                                _.each(matchHDPFromDBArray, league => {
                                    _.each(league.m, match => {
                                        if (match.isToday) {
                                            _.each(match.bp, betPrice => {
                                                const r =betPrice.r;
                                                betPrice.r.ah.ma = calculateBetPriceByTime(r.ah.ma, now_date, new Date(match.d));
                                                betPrice.r.ou.ma = calculateBetPriceByTime(r.ou.ma, now_date, new Date(match.d));
                                                betPrice.r.x12.ma = calculateBetPriceByTime(r.x12.ma, now_date, new Date(match.d));
                                                betPrice.r.oe.ma = calculateBetPriceByTime(r.oe.ma, now_date, new Date(match.d));
                                                betPrice.r.ah1st.ma = calculateBetPriceByTime(r.ah1st.ma, now_date, new Date(match.d));
                                                betPrice.r.ou1st.ma = calculateBetPriceByTime(r.ou1st.ma, now_date, new Date(match.d));
                                                betPrice.r.x121st.ma = calculateBetPriceByTime(r.x121st.ma, now_date, new Date(match.d));
                                                betPrice.r.oe1st.ma = calculateBetPriceByTime(r.oe1st.ma, now_date, new Date(match.d));
                                            });
                                        }
                                    });
                                });

                                return res.send({
                                    message: "success",
                                    result: _.sortBy(matchHDPFromDBArray, 'sk'),
                                    code: 0
                                });

                            } else {
                                return res.send({
                                    message: "success",
                                    result: [],
                                    code: 0
                                });
                            }
                        }
                    });
            }
        });
});

//[8]
router.get('/backend/live_cal', (req, res) =>{
    console.log(URL_LIVE);
    request.get(
        URL_LIVE,
        (err, response) => {
            if(err){
                console.log('[8.1 ] 999 : ==> \n', err);
                return res.send(
                    {
                        code: 9999,
                        message: err
                    });
            } else {
                FootballModel.find({})
                    .lean()
                    .sort('sk')
                    .exec((err, data) => {
                        if (err) {
                            console.log('[2.2 ] 999 : ==> \n', err);
                            return res.send({
                                message: "success",
                                result: [],
                                code: 0
                            });
                        }  else if (_.size(data) === 0) {
                            console.log('[2.3 ] Data not found');
                            return res.send({
                                message: "success",
                                result: [],
                                code: 0
                            });
                        } else {
                            const dataClone = clone(data);
                            let matchLiveFromDBArray = _.chain(data)
                                .each(league => {
                                    league.m = _.filter(league.m, match => {
                                        return match.s.isL && match.s.isAl
                                    });
                                })
                                .reject(league => {
                                    return _.size(league.m) === 0
                                })
                                .value();

                            const matchIdLiveFromDBArray = _.chain(matchLiveFromDBArray)
                                .flatten(true)
                                .pluck('m')
                                .flatten(true)
                                .pluck('id')
                                .value();

                            if (_.size(matchIdLiveFromDBArray) !== 0) {
                                const result = JSON.parse(response.body);
                                // console.log('result => ', JSON.stringify(result));
                                const matchLiveAPIArray = _.chain(result.result)
                                    .flatten(true)
                                    .pluck('m')
                                    .value();

                                const matchIdAPIArray = _.chain(matchLiveAPIArray)
                                    .flatten(true)
                                    .pluck('id')
                                    .value();

                                let resultMatchedArray = _.intersection(matchIdLiveFromDBArray, matchIdAPIArray);
                                // console.log("[2.6] Live matched : " + JSON.stringify(resultMatchedArray));

                                let resultTotalArray = _.chain(result.result)
                                    .each(league => {
                                        league.m = _.filter(league.m, match => {
                                            return _.contains(resultMatchedArray, match.id)
                                        });
                                    })
                                    .reject(league => { return _.size(league.m) === 0 })
                                    .value();

                                let resultAPIAllKeyBPLArray = _.chain(resultTotalArray)
                                    .flatten(true)
                                    .pluck('m')
                                    .flatten(true)
                                    .pluck('bp')
                                    .flatten(true)
                                    .pluck('allKey')
                                    .flatten(true)
                                    .value();

                                console.log("[2.6.1] AllKey bet price["+_.size(resultAPIAllKeyBPLArray)+"] : " + JSON.stringify(resultAPIAllKeyBPLArray));
                                // console.log("=====DB=====");
                                matchLiveFromDBArray = _.chain(data)
                                    .each(league => {
                                        _.each(league.m, match => {
                                            console.log(`match.bpl before filter => ${_.size(match.bpl)}`);
                                            match.bpl = _.filter(match.bpl, betPrise => {
                                                console.log("all key DB["+_.size(betPrise.allKey)+"] : " + JSON.stringify(betPrise.allKey));
                                                if (_.size(_.intersection(resultAPIAllKeyBPLArray, betPrise.allKey)) === 0) {
                                                    console.log(`########all key was removed######## ${JSON.stringify(betPrise.allKey)}`);
                                                }
                                                return betPrise.isOnM && _.size(_.intersection(resultAPIAllKeyBPLArray, betPrise.allKey)) > 0
                                            });
                                            console.log(`match.bpl after filter => ${_.size(match.bpl)}`);
                                            console.log(`===============[${match.id}]`);

                                            let i = 0;

                                            _.each(match.bpl, bp => {
                                                const resultBPL = _.chain(resultTotalArray)
                                                    .filter(leagueAPI => {
                                                        return _.isEqual(leagueAPI.k, league.k)
                                                    })
                                                    .flatten(true)
                                                    .pluck('m')
                                                    .flatten(true)
                                                    .filter(matchAPI => {
                                                        return _.isEqual(matchAPI.k, match.k)
                                                    })
                                                    .each(matchAPI => {
                                                        match.i = matchAPI.i;
                                                    })
                                                    .pluck('bp')
                                                    .flatten(true)
                                                    .filter(betPrice => {
                                                        return _.size(_.intersection(bp.allKey, betPrice.allKey)) > 0
                                                    })
                                                    .value();
                                                // console.log('resultBPL => ', JSON.stringify(resultBPL));
                                                // console.log('bp.allKey => ', JSON.stringify(bp.allKey));
                                                if (!_.isUndefined(bp.allKey) &&
                                                    !_.isUndefined(_.first(resultBPL)) &&
                                                    !_.isUndefined(_.first(resultBPL).allKey) &&
                                                    _.size(_.intersection(bp.allKey, _.first(resultBPL).allKey)) > 0) {
                                                    // delete bp.r;
                                                    match.bpl[i] = _.first(resultBPL);
                                                }
                                                i++;
                                            });
                                        });
                                        league.m = _.reject(league.m, match => { return _.size(match.bpl) === 0 });
                                    })
                                    .reject(league => { return _.size(league.m) === 0 })
                                    .each(league => {
                                        _.each(league.m, match => {
                                            if(league.rp && league.rp.live){
                                                if(_.size(match.bpl) > 0){
                                                    _.each(match.bpl, bp =>{

                                                        const resultBPL = _.first(_.chain(dataClone)
                                                            .filter(leagueAPI => {
                                                                return _.isEqual(leagueAPI.k, league.k)
                                                            })
                                                            .flatten(true)
                                                            .pluck('m')
                                                            .flatten(true)
                                                            .filter(matchAPI => {
                                                                return _.isEqual(matchAPI.k, match.k)
                                                            })
                                                            .pluck('bpl')
                                                            .flatten(true)
                                                            .filter(betPrice => {
                                                                return _.size(_.intersection(bp.allKey, betPrice.allKey)) > 0;
                                                            })
                                                            .map(betPrice => {
                                                                return _.pick(betPrice, 'rp', 'r', 'rm', '_id', 'isOn')
                                                            })
                                                            .flatten(true)
                                                            .value());

                                                        // console.log('bp => ', JSON.stringify(bp));

                                                        bp._id = resultBPL._id;
                                                        bp.rp = resultBPL.rp;
                                                        bp.r = resultBPL.r;
                                                        bp.rm = resultBPL.rm;
                                                        bp.isOn = resultBPL.isOn;

                                                        console.log('resultBPL => ', JSON.stringify(resultBPL));

                                                        let ou1st = {o: 0, u: 0};
                                                        let ou = {o: 0, u: 0};
                                                        let oe = {o: 0, e: 0};
                                                        let oe1st = {o: 0, e: 0};
                                                        let ah = {home: 0, away: 0};
                                                        let ah1st = {home: 0, away: 0};

                                                        // ---------------------------  ou1st  -----------------------------
                                                        if(bp.ou1st && resultBPL.rp && resultBPL.rp.ou1st){
                                                            if(bp.ou1st.op || bp.ou1st.up){
                                                                Commons.cal_ou(bp.ou1st.op, bp.ou1st.up, league.rp.live.ou1st.f, league.rp.live.ou1st.u, resultBPL.rp.ou1st, '', ou1st, (err, data)=>{
                                                                    if(data){
                                                                        bp.ou1st.op = data.op;
                                                                        bp.ou1st.up = data.up;
                                                                    }
                                                                })
                                                            }
                                                        }

                                                        // ---------------------------  ou  ----------------------------------
                                                        if(bp.ou && resultBPL.rp && resultBPL.rp.ou){
                                                            if(bp.ou.op || bp.ou.up){
                                                                Commons.cal_ou(bp.ou.op, bp.ou.up, league.rp.live.ou.f, league.rp.live.ou.u, resultBPL.rp.ou, '', ou, (err, data)=>{
                                                                    if(data){
                                                                        bp.ou.op = data.op;
                                                                        bp.ou.up = data.up;
                                                                    }
                                                                })
                                                            }
                                                        }

                                                        // ---------------------------  oe1st  ----------------------------------
                                                        if(bp.oe1st && resultBPL.rp && resultBPL.rp.oe1st){
                                                            if(bp.oe1st.o || bp.oe1st.e){
                                                                Commons.cal_oe(bp.oe1st.o, bp.oe1st.e, league.rp.live.oe1st.f, league.rp.live.oe1st.u, resultBPL.rp.oe1st, '', oe1st, (err, data)=>{
                                                                    if(data){
                                                                        bp.oe1st.o = data.o;
                                                                        bp.oe1st.e = data.e;
                                                                    }
                                                                })
                                                            }
                                                        }

                                                        // ---------------------------  oe  ----------------------------------
                                                        if(bp.oe && resultBPL.rp && resultBPL.rp.oe){
                                                            if(bp.oe.o || bp.oe.e){
                                                                Commons.cal_oe(bp.oe.o, bp.oe.e, league.rp.live.oe.f, league.rp.live.oe.u, resultBPL.rp.oe, '', oe, (err, data)=>{
                                                                    if(data){
                                                                        bp.oe.o = data.o;
                                                                        bp.oe.e = data.e;
                                                                    }
                                                                })
                                                            }
                                                        }

                                                        // ---------------------------  ah1st  ----------------------------------
                                                        if(bp.ah1st && resultBPL.rp && resultBPL.rp.ah1st){
                                                            if(bp.ah1st.h || bp.ah1st.a){
                                                                Commons.cal_ah(bp.ah1st.h, bp.ah1st.a, bp.ah1st.hp, bp.ah1st.ap, league.rp.live.ah1st.f, league.rp.live.ah1st.u, resultBPL.rp.ah1st, '', ah1st, (err, data)=>{
                                                                    if(data){
                                                                        bp.ah1st.hp = data.hp;
                                                                        bp.ah1st.ap = data.ap;
                                                                    }
                                                                })
                                                            }
                                                        }

                                                        // ---------------------------  ah  ----------------------------------
                                                        if(bp.ah && resultBPL.rp && resultBPL.rp.ah){
                                                            if(bp.ah.h || bp.ah.a){
                                                                Commons.cal_ah(bp.ah.h, bp.ah.a, bp.ah.hp, bp.ah.ap, league.rp.live.ah.f, league.rp.live.ah.u, resultBPL.rp.ah, '', ah, (err, data)=>{
                                                                    if(data){
                                                                        bp.ah.hp = data.hp;
                                                                        bp.ah.ap = data.ap;
                                                                    }
                                                                })
                                                            }
                                                        }
                                                        // ---------------------------  x121st  ----------------------------------
                                                        if(bp.x121st){
                                                            if(bp.x121st.h || bp.x121st.a || bp.x121st.d){
                                                                Commons.cal_x12(bp.x121st.h, bp.x121st.a, bp.x121st.d, resultBPL.rp.x121st, (err, data)=>{
                                                                    if(data){
                                                                        bp.x121st.h = data.h;
                                                                        bp.x121st.a = data.a;
                                                                        bp.x121st.d = data.d;
                                                                    }
                                                                })
                                                            }
                                                        }

                                                        // ---------------------------  x12  ----------------------------------
                                                        if(bp.x12){
                                                            if(bp.x12.h || bp.x12.a || bp.x12.d){
                                                                Commons.cal_x12(bp.x12.h, bp.x12.a, bp.x12.d, resultBPL.rp.x12, (err, data)=>{
                                                                    if(data){
                                                                        bp.x12.h = data.h;
                                                                        bp.x12.a = data.a;
                                                                        bp.x12.d = data.d;
                                                                    }
                                                                })
                                                            }
                                                        }
                                                    });
                                                }
                                            }
                                        });
                                    })
                                    .each(league => {
                                        const rl = league.rl;
                                        console.log(`####################################`);
                                        console.log(`${league.k} ${league.n}`);
                                        console.log(`R   Max pay out ${rl.pm}`);
                                        console.log(`ah     : ${JSON.stringify(rl.ah.ma)} ${JSON.stringify(rl.ah.mi)}`);
                                        console.log(`ou     : ${JSON.stringify(rl.ou.ma)} ${JSON.stringify(rl.ou.mi)}`);
                                        console.log(`x12    : ${JSON.stringify(rl.x12.ma)} ${JSON.stringify(rl.x12.mi)}`);
                                        console.log(`oe     : ${JSON.stringify(rl.oe.ma)} ${JSON.stringify(rl.oe.mi)}`);
                                        console.log(`ah1st  : ${JSON.stringify(rl.ah1st.ma)} ${JSON.stringify(rl.ah1st.mi)}`);
                                        console.log(`ou1st  : ${JSON.stringify(rl.ou1st.ma)} ${JSON.stringify(rl.ou1st.mi)}`);
                                        console.log(`x121st : ${JSON.stringify(rl.x121st.ma)} ${JSON.stringify(rl.x121st.mi)}`);
                                        console.log(`oe1st  : ${JSON.stringify(rl.oe1st.ma)} ${JSON.stringify(rl.oe1st.mi)}`);
                                        console.log(`####################################`);
                                        _.each(league.m, match => {
                                            let rule = league.rl;
                                            match.r = {};
                                            match.r.pm = rl.pm;
                                            if (!_.isUndefined(match.srl) && !_.isUndefined(match.srl.pm) && match.srl.isOn) {
                                                rule = match.srl;
                                                match.r.pm = match.srl.pm;

                                                console.log(`$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$`);
                                                console.log(`R   Max pay out ${rule.pm}`);
                                                console.log(`ah     : ${JSON.stringify(rule.ah.ma)} ${JSON.stringify(rule.ah.mi)}`);
                                                console.log(`ou     : ${JSON.stringify(rule.ou.ma)} ${JSON.stringify(rule.ou.mi)}`);
                                                console.log(`x12    : ${JSON.stringify(rule.x12.ma)} ${JSON.stringify(rule.x12.mi)}`);
                                                console.log(`oe     : ${JSON.stringify(rule.oe.ma)} ${JSON.stringify(rule.oe.mi)}`);
                                                console.log(`ah1st  : ${JSON.stringify(rule.ah1st.ma)} ${JSON.stringify(rule.ah1st.mi)}`);
                                                console.log(`ou1st  : ${JSON.stringify(rule.ou1st.ma)} ${JSON.stringify(rule.ou1st.mi)}`);
                                                console.log(`x121st : ${JSON.stringify(rule.x121st.ma)} ${JSON.stringify(rule.x121st.mi)}`);
                                                console.log(`oe1st  : ${JSON.stringify(rule.oe1st.ma)} ${JSON.stringify(rule.oe1st.mi)}`);
                                                console.log(`$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$`);
                                            }

                                            let round = 0;
                                            console.log(`       ${match.n.en.h} vs ${match.n.en.a}`);
                                            _.each(match.bpl, betPrice => {
                                                console.log(`       ==============  R  ============== ${match.id} BetPrice = ${round}`);
                                                betPrice.r = {};
                                                betPrice.r.ah = {};
                                                betPrice.r.ou = {};
                                                betPrice.r.x12 = {};
                                                betPrice.r.oe = {};
                                                betPrice.r.ah1st = {};
                                                betPrice.r.ou1st = {};
                                                betPrice.r.x121st = {};
                                                betPrice.r.oe1st = {};
                                                const {
                                                    ah,
                                                    ou,
                                                    x12,
                                                    oe,
                                                    ah1st,
                                                    ou1st,
                                                    x121st,
                                                    oe1st
                                                } = rule;

                                                betPrice.r.ah.ma = calculateBetPrice(ah.ma, round);
                                                betPrice.r.ah.mi = ah.mi;

                                                betPrice.r.ou.ma = calculateBetPrice(ou.ma, round);
                                                betPrice.r.ou.mi = ou.mi;

                                                betPrice.r.x12.ma = calculateBetPrice(x12.ma, round);
                                                betPrice.r.x12.mi = x12.mi;

                                                betPrice.r.oe.ma = calculateBetPrice(oe.ma, round);
                                                betPrice.r.oe.mi = oe.mi;

                                                betPrice.r.ah1st.ma = calculateBetPrice(ah1st.ma, round);
                                                betPrice.r.ah1st.mi = ah1st.mi;

                                                betPrice.r.ou1st.ma = calculateBetPrice(ou1st.ma, round);
                                                betPrice.r.ou1st.mi = ou1st.mi;

                                                betPrice.r.x121st.ma = calculateBetPrice(x121st.ma, round);
                                                betPrice.r.x121st.mi = x121st.mi;

                                                betPrice.r.oe1st.ma = calculateBetPrice(oe1st.ma, round);
                                                betPrice.r.oe1st.mi = oe1st.mi;

                                                console.log(`       ah     : ${JSON.stringify(betPrice.r.ah.ma)} ${JSON.stringify(betPrice.r.ah.mi)}`);
                                                console.log(`       ou     : ${JSON.stringify(betPrice.r.ou.ma)} ${JSON.stringify(betPrice.r.ou.mi)}`);
                                                console.log(`       x12    : ${JSON.stringify(betPrice.r.x12.ma)} ${JSON.stringify(betPrice.r.x12.mi)}`);
                                                console.log(`       oe     : ${JSON.stringify(betPrice.r.oe.ma)} ${JSON.stringify(betPrice.r.oe.mi)}`);
                                                console.log(`       ah1st  : ${JSON.stringify(betPrice.r.ah1st.ma)} ${JSON.stringify(betPrice.r.ah1st.mi)}`);
                                                console.log(`       ou1st  : ${JSON.stringify(betPrice.r.ou1st.ma)} ${JSON.stringify(betPrice.r.ou1st.mi)}`);
                                                console.log(`       x121st : ${JSON.stringify(betPrice.r.x121st.ma)} ${JSON.stringify(betPrice.r.x121st.mi)}`);
                                                console.log(`       oe1st  : ${JSON.stringify(betPrice.r.oe1st.ma)} ${JSON.stringify(betPrice.r.oe1st.mi)}`);
                                                round++;
                                            });
                                        });
                                    })
                                    .value();

                                return res.send({
                                    message: "success",
                                    result: _.sortBy(matchLiveFromDBArray, 'sk'),
                                    code: 0
                                });

                            } else {
                                return res.send({
                                    message: "success",
                                    result: [],
                                    code: 0
                                });
                            }
                        }
                    });
            }
        });
});

//[9]
router.get('/backend/live', (req, res) =>{
    console.log(URL_LIVE);
    request.get(
        URL_LIVE,
        (err, response) => {
            if(err){
                console.log('[9.1 ] 999 : ==> \n', err);
                return res.send(
                    {
                        code: 9999,
                        message: err
                    });
            } else {
                FootballModel.find({
                    m: {
                        $elemMatch: {
                            's.isL': true
                        }
                    }
                })
                    .lean()
                    .sort('sk')
                    .exec((err, data) => {
                        if (err) {
                            console.log('[2.2 ] 999 : ==> \n', err);
                            return res.send({
                                message: "success",
                                result: [],
                                code: 0
                            });
                        }  else if (_.size(data) === 0) {
                            console.log('[2.3 ] Data not found');
                            return res.send({
                                message: "success",
                                result: [],
                                code: 0
                            });
                        } else {
                            const dataClone = clone(data);
                            let matchLiveFromDBArray = _.chain(data)
                                .each(league => {
                                    league.m = _.filter(league.m, match => {
                                        return match.s.isL && match.s.isAl
                                    });
                                })
                                // .reject(league => {
                                //     return _.size(league.m) === 0
                                // })
                                .value();

                            const matchIdLiveFromDBArray = _.chain(matchLiveFromDBArray)
                                .flatten(true)
                                .pluck('m')
                                .flatten(true)
                                .pluck('id')
                                .value();

                            if (_.size(matchIdLiveFromDBArray) !== 0) {
                                const result = JSON.parse(response.body);
                                // console.log('result => ', JSON.stringify(result));
                                const matchLiveAPIArray = _.chain(result.result)
                                    .flatten(true)
                                    .pluck('m')
                                    .value();

                                const matchIdAPIArray = _.chain(matchLiveAPIArray)
                                    .flatten(true)
                                    .pluck('id')
                                    .value();

                                let resultMatchedArray = _.intersection(matchIdLiveFromDBArray, matchIdAPIArray);
                                // console.log("[2.6] Live matched : " + JSON.stringify(resultMatchedArray));

                                let resultTotalArray = _.chain(result.result)
                                    .each(league => {
                                        league.m = _.filter(league.m, match => {
                                            return _.contains(resultMatchedArray, match.id)
                                        });
                                    })
                                    .reject(league => { return _.size(league.m) === 0 })
                                    .value();

                                // console.log("=====DB=====");
                                matchLiveFromDBArray = _.chain(data)
                                    .each(league => {
                                        _.each(league.m, match => {
                                            match.bpl = _.filter(match.bpl, betPrise => { return betPrise.isOn && betPrise.isOnM; });
                                            // console.log('resultTotalArray => ', JSON.stringify(resultTotalArray));

                                            let i = 0;

                                            _.each(match.bpl, bp => {

                                                const resultBPL = _.chain(resultTotalArray)
                                                    .filter(leagueAPI => {
                                                        return _.isEqual(leagueAPI.k, league.k)
                                                    })
                                                    .flatten(true)
                                                    .pluck('m')
                                                    .flatten(true)
                                                    .filter(matchAPI => {
                                                        return _.isEqual(matchAPI.k, match.k)
                                                    })
                                                    .each(matchAPI => {
                                                        match.i = matchAPI.i;
                                                    })
                                                    .pluck('bp')
                                                    .flatten(true)
                                                    .filter(betPrice => {
                                                        return _.size(_.intersection(bp.allKey, betPrice.allKey)) > 0
                                                    })
                                                    .value();
                                                // console.log('resultBPL => ', JSON.stringify(resultBPL));
                                                // console.log('bp.allKey => ', JSON.stringify(bp.allKey));
                                                if (!_.isUndefined(bp.allKey) &&
                                                    !_.isUndefined(_.first(resultBPL)) &&
                                                    !_.isUndefined(_.first(resultBPL).allKey) &&
                                                    _.size(_.intersection(bp.allKey, _.first(resultBPL).allKey)) > 0) {
                                                    // delete bp.r;
                                                    match.bpl[i] = _.first(resultBPL);
                                                }
                                                i++;
                                            });
                                        });
                                        league.m = _.reject(league.m, match => { return _.size(match.bpl) === 0 });
                                    })
                                    .reject(league => { return _.size(league.m) === 0 })
                                    .value();

                                return res.send({
                                    message: "success",
                                    result: matchLiveFromDBArray,
                                    code: 0
                                });

                            } else {
                                return res.send({
                                    message: "success",
                                    result: [],
                                    code: 0
                                });
                            }
                        }
                    });
            }
        });
});

router.get('/count', (req, res) => {
    // console.log(`KEY COUNT => count${CLIENT_NAME}`);
    client_redis.get(`count${CLIENT_NAME}`, (error, result) => {
        if (error || _.isNull(result)) {
            FootballModel.find({})
                .lean()
                .sort('sk')
                .exec((err, data) => {
                    const data1 = clone(data);
                    const data2 = clone(data);
                    const data3 = clone(data);
                    const data4 = clone(data);
                    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);

                    const live = _.chain(data1)
                        .each(league => {
                            league.m = _.filter(league.m, match => {
                                return match.s.isL && match.s.isAl
                            });
                        })
                        .flatten(true)
                        .pluck('m')
                        .flatten(true)
                        .value();
                    const today = _.chain(data2)
                        .each(league => {
                            league.m = _.filter(league.m, match => {
                                return match.s.isH && match.isToday && match.d >= now
                            });
                        })
                        .flatten(true)
                        .pluck('m')
                        .flatten(true)
                        .value();
                    const earlyMarket = _.chain(data3)
                        .each(league => {
                            league.m = _.filter(league.m, match => {
                                return match.s.isH && !match.isToday
                            });
                        })
                        .flatten(true)
                        .pluck('m')
                        .flatten(true)
                        .value();

                    const hasParlay = _.chain(data4)
                        .each(league => {
                            league.m = _.filter(league.m, match => {
                                return match.s.isH && match.isToday && match.hasParlay && match.d >= now
                            });
                        })
                        .flatten(true)
                        .pluck('m')
                        .flatten(true)
                        .value();

                    const obj = {};
                    obj.total= _.size(today)+_.size(earlyMarket)+_.size(live);
                    obj.today= _.size(today);
                    obj.earlyMarket= _.size(earlyMarket);
                    obj.live= _.size(live);
                    obj.hasParlay= _.size(hasParlay);
                    client_redis.set(`count${CLIENT_NAME}`, JSON.stringify(obj));
                    return res.send({
                        code: 0,
                        message: 'success',
                        total: obj.total,
                        today: obj.today,
                        earlyMarket: obj.earlyMarket,
                        live: obj.live,
                        hasParlay: obj.hasParlay
                    });
                });
        } else {
            const obj = JSON.parse(result);
            return res.send({
                code: 0,
                message: 'success',
                total: obj.total,
                today: obj.today,
                earlyMarket: obj.earlyMarket,
                live: obj.live,
                hasParlay: obj.hasParlay
            });
        }
    });
});

router.get('/v2/count', (req, res) => {
    async.series([
        (callback) => {
            client_redis.get(`count${CLIENT_NAME}`, (error, result) => {
                const response = {};
                response.total=0;
                response.today=0;
                response.earlyMarket=0;
                response.live=0;
                response.hasParlay=0;

                if (error || _.isNull(result)) {
                    callback(null, response);
                } else {
                    const obj = JSON.parse(result);
                    response.total=obj.total;
                    response.today=obj.today;
                    response.earlyMarket=obj.earlyMarket;
                    response.live=obj.live;
                    response.hasParlay=obj.hasParlay;
                    callback(null, response);
                }
            });
        },

        (callback) => {
            client_redis.get(`countBasketball${CLIENT_NAME}`, (error, result) => {
                const response = {};
                response.total=0;
                response.today=0;
                response.earlyMarket=0;
                response.live=0;
                response.hasParlay=0;

                if (error || _.isNull(result)) {
                    callback(null, response);
                } else {
                    const obj = JSON.parse(result);
                    response.total=obj.total;
                    response.today=obj.today;
                    response.earlyMarket=obj.earlyMarket;
                    response.live=obj.live;
                    response.hasParlay=obj.hasParlay;
                    callback(null, response);
                }
            });
        },

        (callback) => {
            client_redis.get(`countBasketball${CLIENT_NAME}`, (error, result) => {
                const response = {};
                response.total=0;
                response.today=0;
                response.earlyMarket=0;
                response.live=0;
                response.hasParlay=0;

                if (error || _.isNull(result)) {
                    callback(null, response);
                } else {
                    const obj = JSON.parse(result);
                    response.total=obj.total;
                    response.today=obj.today;
                    response.earlyMarket=obj.earlyMarket;
                    response.live=obj.live;
                    response.hasParlay=obj.hasParlay;
                    callback(null, response);
                }
            });
        }
    ], (errAsync, dataAsync)=> {
        const [
            football,
            basketball,
            tennis
        ] = dataAsync;
        return res.send({
            code: 0,
            message: 'success',
            football: football,
            basketball: basketball,
            tennis: tennis,
        });
    });
});

//[16]
router.get('/maintenance', (req, res) => {
    async.parallel([
            (callback) => {
                RedisService.getIsMaintenance(CLIENT_NAME, (error, result) => {
                    if (error) {
                        callback(null, false);
                    } else {
                        callback(null, result);
                    }
                });
            },

            (callback) => {
                RedisService.getIsSexyBacaratMaintenance(CLIENT_NAME, (error, result) => {
                    if (error) {
                        callback(null, false);
                    } else {
                        callback(null, result);
                    }
                });
            },

            (callback) => {
                RedisService.getIsSlotMaintenance(CLIENT_NAME, (error, result) => {
                    if (error) {
                        callback(null, false);
                    } else {
                        callback(null, result);
                    }
                });
            },

            (callback) => {
                RedisService.getIsSABacaratMaintenance(CLIENT_NAME, (error, result) => {
                    if (error) {
                        callback(null, false);
                    } else {
                        callback(null, result);
                    }
                });
            },

            (callback) => {
                RedisService.getIsLotusBacaratMaintenance(CLIENT_NAME, (error, result) => {
                    if (error) {
                        callback(null, false);
                    } else {
                        callback(null, result);
                    }
                });
            }
        ],
        (error, result) => {
            if (error) {
                return res.send({
                    code    : 9999,
                    message : error
                });
            } else {
                return res.send({
                    code: 0,
                    message: 'success',
                    isMaintenance: result[0],
                    isSexyBacaratMaintenance: result[1],
                    isSlotMaintenance: result[2],
                    isSABacaratMaintenance: result[3],
                    isLotusBacaratMaintenance: result[4]
                });
            }
        }
    );
});

//[11]
router.get('/score', (req, res) => {
    const day = req.query.d || 0;
    const month = req.query.m || 0;


    const startDate = new Date(_.now());
    const endDate = new Date(_.now());

    // const startDate = new Date(new Date().getTime());
    // const endDate = new Date(new Date().getTime());

    console.log('====== Start Date ======' + day + ":" + month);
    startDate.setMonth(month - 1);
    startDate.setDate(day);
    startDate.setHours(18 /*- 7*/);
    startDate.setMinutes(0);
    startDate.setSeconds(0);

    console.log('====== End Date ======' + day + ":" + month);
    endDate.setMonth(month - 1);
    endDate.setDate(day);
    endDate.setDate(endDate.getDate() + 1);
    endDate.setHours(17 /*- 7*/);
    endDate.setMinutes(59);
    endDate.setSeconds(59);

    console.log('============== startDate ==============');
    console.log(startDate);
    console.log('============== startDate ==============');
    console.log('=============== endDate ===============');
    console.log(endDate);
    console.log('=============== endDate ===============');

    async.series([
        (callback =>
            {
                FootballModel.find(
                    {
                        m: {
                            $elemMatch: {
                                // 's.isCr': true,
                                'd': {
                                    $gte: startDate,
                                    $lt: endDate
                                }
                            }
                        }
                    })
                    .lean()
                    .sort('sk')
                    .exec((err, data) => {
                        if (err) {
                            callback(err, null);
                        } else {
                            const footballData = _.chain(data)
                                .each(league => {
                                    league.m = _.filter(league.m, match => {
                                        return /*match.s.isCr && */match.d >= startDate && match.d <= endDate
                                    });

                                    _.each(league.m, match => {
                                        if (match.s.isH) {
                                            match.s.cr.ft.h = '-';
                                            match.s.cr.ft.a = '-';
                                            match.s.cr.ht.h = '-';
                                            match.s.cr.ht.a = '-';

                                        } else if (match.s.isL) {
                                            match.s.cr.ft.h = '-';
                                            match.s.cr.ft.a = '-';

                                        } else if (match.s.isE) {
                                            match.s.cr.ft.h = '-';
                                            match.s.cr.ft.a = '-';

                                        } else if (match.s.isC.ht) {
                                            match.s.cr.ft.h = 'Refunded';
                                            match.s.cr.ft.a = '';
                                            if (!_.isUndefined(match.s.isC.ft_remark) && !_.isNull(match.s.isC.ft_remark)) {
                                                match.s.cr.ft.h = match.s.isC.ft_remark;
                                            }
                                        } else if (match.s.isC.ft) {
                                            match.s.cr.ft.h = 'Refunded';
                                            match.s.cr.ft.a = '';
                                            if (!_.isUndefined(match.s.isC.ft_remark) && !_.isNull(match.s.isC.ft_remark)) {
                                                match.s.cr.ft.h = match.s.isC.ft_remark;
                                            }

                                            match.s.cr.ht.h = 'Refunded';
                                            match.s.cr.ht.a = '';
                                            if (!_.isUndefined(match.s.isC.ht_remark) && !_.isNull(match.s.isC.ht_remark)) {
                                                match.s.cr.ht.h = match.s.isC.ht_remark;
                                            }
                                        }
                                        const {
                                            cr,
                                            isC
                                        } = match.s;
                                        match.s = {};
                                        match.s.cr = cr;
                                        match.s.isC = isC;

                                        delete match._id;
                                        delete match.bpl;
                                        delete match.bp;
                                        delete match.rm;
                                        delete match.r;
                                        delete match.hasParlay;
                                        delete match.i;
                                    });
                                    delete league._id;
                                    delete league.r;
                                    delete league.rp;
                                    delete league.rm;
                                    delete league.__v;
                                })
                                .reject(league => { return _.size(league.m) === 0 })
                                .value();

                            callback(null, footballData);
                        }
                    });
            }
        ),
        (callback =>
            {
                FootballTrashModel.find(
                    {
                        m: {
                            $elemMatch: {
                                // 's.isCr': true,
                                'd': {
                                    $gte: startDate,
                                    $lt: endDate
                                }
                            }
                        }
                    })
                    .lean()
                    .sort('sk')
                    .exec((err, data) => {
                        if (err) {
                            callback(err, null);
                        } else {
                            const footballTrashData = _.chain(data)
                                .each(league => {
                                    league.m = _.filter(league.m, match => {
                                        return /*match.s.isCr && */match.d >= startDate && match.d <= endDate
                                    });

                                    _.each(league.m, match => {
                                        if (match.s.isH) {
                                            match.s.cr.ft.h = '-';
                                            match.s.cr.ft.a = '-';
                                            match.s.cr.ht.h = '-';
                                            match.s.cr.ht.a = '-';

                                        } else if (match.s.isL) {
                                            match.s.cr.ft.h = '-';
                                            match.s.cr.ft.a = '-';

                                        } else if (match.s.isE) {
                                            match.s.cr.ft.h = '-';
                                            match.s.cr.ft.a = '-';

                                        } else if (match.s.isC.ht) {
                                            match.s.cr.ft.h = 'Refunded';
                                            match.s.cr.ft.a = '';
                                            if (!_.isUndefined(match.s.isC.ft_remark) && !_.isNull(match.s.isC.ft_remark)) {
                                                match.s.cr.ft.h = match.s.isC.ft_remark;
                                            }
                                        } else if (match.s.isC.ft) {
                                            match.s.cr.ft.h = 'Refunded';
                                            match.s.cr.ft.a = '';
                                            if (!_.isUndefined(match.s.isC.ft_remark) && !_.isNull(match.s.isC.ft_remark)) {
                                                match.s.cr.ft.h = match.s.isC.ft_remark;
                                            }

                                            match.s.cr.ht.h = 'Refunded';
                                            match.s.cr.ht.a = '';
                                            if (!_.isUndefined(match.s.isC.ht_remark) && !_.isNull(match.s.isC.ht_remark)) {
                                                match.s.cr.ht.h = match.s.isC.ht_remark;
                                            }
                                        }

                                        const {
                                            cr,
                                            isC
                                        } = match.s;
                                        match.s = {};
                                        match.s.cr = cr;
                                        match.s.isC = isC;

                                        delete match._id;
                                        delete match.bpl;
                                        delete match.bp;
                                        delete match.rm;
                                        delete match.r;
                                        delete match.hasParlay;
                                        delete match.i;
                                    });
                                    delete league._id;
                                    delete league.r;
                                    delete league.rp;
                                    delete league.rm;
                                    delete league.__v;
                                })
                                .reject(league => { return _.size(league.m) === 0 })
                                .value();

                            callback(null, footballTrashData);
                        }
                    });
            }
        )], (err, asyncSeriesResponse) => {


        // let a = _.first(asyncSeriesResponse);
        // let b = _.last(asyncSeriesResponse);
        //
        //
        // a.push(...b);


        const today = _.first(asyncSeriesResponse);
        const temp  = _.last(asyncSeriesResponse);

        const todayLeagueKey = filterLeagueKey(today);
        const tempLeagueKey = filterLeagueKey(temp);

        const differenceLeagueKey = _.difference(tempLeagueKey, todayLeagueKey);

        if (!_.isEmpty(differenceLeagueKey)) {
            const leagueEarly =_.filter(temp, league => {
                return  _.contains(differenceLeagueKey, league.k);
            });
            today.push(...leagueEarly);
        }

        // let response = _.first(asyncSeriesResponse);
        // if (_.size(response) === 0) {
        //     response = _.last(asyncSeriesResponse);
        // }

        return res.send(
            {
                code: 0,
                message: "success",
                // result : response,
                result : _.sortBy(today, 'sk')
            }
        );
    });

});

const filterLeagueKey = (league) => {
    return _.chain(league)
        .flatten(true)
        .pluck('k')
        .sort()
        .value();
};

router.post('/create', (req, res) =>{

    const body = req.body;
    //
    // Commons.cal_ah(body, callback())
    // async.waterfall([
    //         (callback) => {
    //             console.log(URL_HDP);
    //             request.get(
    //                 URL_HDP,
    //                 (error, response) => {
    //                     if (error) {
    //                         callback(error, null);
    //                     } else {
    //                         callback(null, response);
    //                     }
    //                 });
    //         }
    //     ],
    //     (error, response) => {
    //         if (error) {
    //             callback(error, null)
    //         } else {
    //             const result = JSON.parse(response.body).result;
    //
    //             const out = [];
    //
    //             _.each(body, obj => {
    //                 const {
    //                     matchId,
    //                     oddType,
    //                     oddKey
    //                 } = obj;
    //
    //                 const [
    //                     league_key,
    //                     match_key ] = matchId.split(':');
    //
    //                 let betPrice = _.chain(result)
    //                     .findWhere(JSON.parse(`{"k":${parseInt(league_key)}}`))
    //                     .pick('m')
    //                     .map(obj => {
    //                         return obj
    //                     })
    //                     .flatten(true)
    //                     .findWhere(JSON.parse(`{"k":${parseInt(match_key)}}`))
    //                     .pick('bp')
    //                     .map(obj => {
    //                         return obj
    //                     })
    //                     .flatten(true)
    //                     .filter(obj => {
    //                         return _.contains(obj.allKey, oddKey)
    //                     })
    //                     .first()
    //                     .pick(oddType.toLowerCase())
    //                     .map(obj => {
    //                         return obj
    //                     })
    //                     .flatten(true)
    //                     .first()
    //                     .value();
    //
    //
    //                 let key;
    //                 _.findKey(betPrice, function (v, k) {
    //                     if (v === oddKey) {
    //                         key = k;
    //                         return true;
    //                     }
    //                 });
    //                 console.log(key);
    //
    //                 console.log(_.pick(betPrice, key, key.replace(/k/, '')));
    //
    //                 console.log(JSON.stringify(betPrice));
    //             });
    //
    //
    //
    //             return res.send(
    //                 {
    //                     code: 0,
    //                     result: []
    //                 });
    //         }
    //     }
    // );

    Commons.call_ods_hdp(body, (error, data)=>{
        return res.send(
            {
                code: 0,
                result: data
            });
    })
});

//[13]Popular
router.get('/popular', (req, res) => {
    return res.send({
        message: "success",
        result: [],
        code: 0
    });
});


router.post('/get', (req, res) =>{
    // const objectModel = {};
    // objectModel.username = '51ov1112';
    // objectModel.matchId = '27487:2197814';
    // objectModel.prefix = 'ah';
    // objectModel.key = 'hpk';
    // objectModel.value = 'o1856895411';
    //
    // const {
    //     id
    // } = req.body;

    const result = MemberService.getOddAdjustment( req.body, (err, data) => {
        if (err) {
            return res.send(
                {
                    code: 9999,
                    message: err
                });
        } else {
            return res.send(
                {
                    code: 0,
                    message: data
                });
        }
    });
});

//[12]
router.post('/check_match_status', (req, res) => {
    const {
        idList
    } = req.body;
    // console.log(JSON.stringify(idList));

    FootballModel.find({
        m: {
            $elemMatch: {
                $or:[
                    {
                        's.isE': true
                    },
                    {
                        's.isCr': true
                    }
                ]
            }
        }
    })
        .lean()
        .sort('sk')
        .exec((err, data) => {
            if (err) {
                console.error('[12.1] 999 : ==> \n', err);
                return res.send({message: "error", result: err, code: 999});
            } else {
                const array = _.chain(data)
                    .each(league => {
                        league.m = _.filter(league.m, match => {
                            return _.contains(idList, match.id) && (match.s.isE || match.s.isCr)
                        });
                    })
                    .flatten(true)
                    .pluck('m')
                    .flatten(true)
                    .pluck('id')
                    .value();


                return res.send({
                    message: "success",
                    result: array,
                    code: 0
                });
            }
        });
});
const calculateBetPrice = (price, round) => {
    let percent = 0;
    switch (round) {
        case 0:
            percent = 0;
            break;
        case 1:
            percent = 40;
            break;
        case 2:
            percent = 80;
            break;
        case 3:
            percent = 90;
            break;
        case 4:
            percent = 90;
            break;
        case 5:
            percent = 90;
            break;
    }
    return decrease(price, percent);
};
const calculateBetPriceByTime = (price, current, match) => {
    if (price <= 20000) {
        return price;
    } else {
        let diff =(current.getTime() - match.getTime()) / 1000;
        diff /= (60 * 60);

        const gapHour = Math.abs(Math.round(diff));
        let percent = 0;

        if (12 <= gapHour) {
            percent = 70;
        } else if (11 === gapHour || 10 === gapHour || 9 === gapHour || 8 === gapHour || 7 === gapHour) {
            percent = 65;
        } else if (6 === gapHour || 5 === gapHour || 4 === gapHour ) {
            percent = 50;
        } else if (3 === gapHour || 2 === gapHour ) {
            percent = 35;
        } else {
            percent = 0;
        }

        return roundTo(parseFloat(decrease(price, percent)), 0);
    }
};

const decrease = (price, percent) => {
    const result = (price * ( (100 - percent) / 100)).toFixed(2);
    // console.log(`${price} - ${percent}% = ${result}`);
    return result;
};

const convertMinMaxByCurrency = (value, currency, membercurrency) => {
    // console.log('###########################');
    // console.log('[BF]value      : ', value);
    // console.log('membercurrency : ', membercurrency);

    let minMax = value;
    if (_.isUndefined(membercurrency) || _.isNull(membercurrency)) {
        return minMax;
    } else {
        const predicate = JSON.parse(`{"currencyName":"${membercurrency}"}`);
        const obj = _.chain(currency)
            .findWhere(predicate)
            .pick('currencyTHB')
            .value();
        if (!_.isUndefined(obj.currencyTHB)) {
            // console.log('               : ',obj.currencyTHB);

            if (_.isEqual(membercurrency, "IDR") || _.isEqual(membercurrency, "VND")) {
                minMax = Math.ceil(value * obj.currencyTHB);
            } else {
                minMax = Math.ceil(value / obj.currencyTHB);
            }
            // console.log('[AF]value      : ', minMax);
            // console.log('###########################');
            return minMax;
        } else {
            return minMax;
        }
    }

};



module.exports = router;