const BetTransactionModel = require('../models/betTransaction.model.js');
const AgentGroupModel = require('../models/agentGroup.model.js');
const UserModel = require('../models/users.model.js');
const MemberModel = require('../models/member.model.js');
const AgentLottoLimitModel = require('../models/agentLottoLimit.model.js');
const AgentLottoLaosLimitModel = require('../models/agentLottoLaosLimit.model.js');
const AgentLottoPPLimitModel = require('../models/agentLottoPPLimit.model.js');
const MemberService = require('../common/memberService');
const express = require('express');
const router = express.Router();
const async = require("async");
const _ = require('underscore');
var moment = require('moment-timezone');
moment().tz("Asia/Bangkok").format();
const roundTo = require('round-to');
const AgentService = require('../common/agentService');
const NumberUtils = require('../common/numberUtils1');
const LottoService = require('../common/lottoService');
const ReportService = require('../common/reportService');
const mongoose = require('mongoose');

const UpdateBalanceHistoryModel2 = require('../models/updateBalanceHistory2.model.js');
const SaTransactionModel2 = require('../models/saTransaction2.model.js');

const DgTransaction = require('../models/dgTransaction2.model.js');
router.get('/sync', function (req, res) {


    const DRAW = "DRAW";
    const WIN = "WIN";
    const HALF_WIN = "HALF_WIN";
    const LOSE = "LOSE";
    const HALF_LOSE = "HALF_LOSE";

    let condition = {
        gameDate: {
            "$gte": moment().millisecond(0).second(33).minute(43).hour(23).date(25).utc(true).toDate(),
            "$lte": moment().millisecond(0).second(56).minute(3).hour(0).date(26).utc(true).toDate()
        },
        game: 'CASINO',
        source: 'DREAM_GAME',
        status: 'DONE'
    }

    console.log(condition)
    BetTransactionModel.find(condition).exec(function (err, list) {

        console.log('list : ',list.length)

        async.each(list, (betObj, callbackWL) => {


            console.log({
                username: betObj.memberUsername.toUpperCase(),
                ticketId: betObj.baccarat.dg.ticketId,
                action:'INFORM'
            })
            DgTransaction.findOne({
                username: betObj.memberUsername.toUpperCase(),
                ticketId: betObj.baccarat.dg.ticketId,
                action:'INFORM'
            }, (err, dgResponse) => {

                if (!dgResponse) {
                    console.log('not found')
                    callbackWL(null, '')
                    return;
                }
                let requestBody = {
                    token: "xxx",
                    ticketId: dgResponse.ticketId,
                    data: dgResponse.data,
                    member: {
                        username: dgResponse.username,
                        amount: dgResponse.amount,
                    }
                }

                async.series([callback => {

                    MemberService.findByUserNameForPartnerService(requestBody.member.username, function (err, memberResponse) {
                        if (err) {
                            callback(err, null);
                            return;
                        }
                        if (memberResponse) {
                            callback(null, memberResponse);
                        } else {
                            callback(1004, null);
                        }
                    });

                }], (err, asyncResponse) => {

                    if (err) {
                        callbackWL(null, '')
                    }

                    let memberInfo = asyncResponse[0];

                    let beforeCredit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2);

                    let condition = {
                        'memberId': mongoose.Types.ObjectId(memberInfo._id),
                        'game': 'CASINO',
                        'source': 'DREAM_GAME',
                        'baccarat.dg.ticketId': requestBody.ticketId
                    };

                    BetTransactionModel.findOne(condition)
                        .populate({path: 'memberParentGroup', select: 'type'})
                        .lean().exec(function (err, betObj) {


                        if (!betObj) {
                            callbackWL(null, '')
                        }

                        // if (betObj.status === 'DONE') {
                        //     return res.send({
                        //         codeId: 0,
                        //         token: requestBody.token,
                        //         data: requestBody.data,
                        //         member: {
                        //             username: memberInfo.username_lower,
                        //             balance: beforeCredit
                        //         }
                        //     });
                        // }

                        AgentGroupModel.findById(memberInfo.group).select('_id type parentId shareSetting commissionSetting').populate({
                            path: 'parentId',
                            select: '_id type parentId shareSetting commissionSetting name',
                            populate: {
                                path: 'parentId',
                                select: '_id type parentId shareSetting commissionSetting name',
                                populate: {
                                    path: 'parentId',
                                    select: '_id type parentId shareSetting commissionSetting name',
                                    populate: {
                                        path: 'parentId',
                                        select: '_id type parentId shareSetting commissionSetting name',
                                        populate: {
                                            path: 'parentId',
                                            select: '_id type parentId shareSetting commissionSetting name',
                                            populate: {
                                                path: 'parentId',
                                                select: '_id type parentId shareSetting commissionSetting name',
                                            }
                                        }
                                    }
                                }
                            }

                        }).exec(function (err, agentGroups) {

                            let betAmount = betObj.amount;
                            let resultCash = roundTo(Number.parseFloat(requestBody.member.amount), 2);
                            let winLose = resultCash - betAmount;
                            let betResult = winLose > 0 ? WIN : winLose < 0 ? LOSE : DRAW;

                            let body = betObj;

                            prepareDGCommission(memberInfo, body, agentGroups, null, 0, 0);

                            //init
                            if (!body.commission.senior) {
                                body.commission.senior = {};
                            }

                            if (!body.commission.masterAgent) {
                                body.commission.masterAgent = {};
                            }

                            if (!body.commission.agent) {
                                body.commission.agent = {};
                            }

                            const shareReceive = AgentService.prepareShareReceive(winLose, betResult, betObj);

                            body.commission.superAdmin.winLoseCom = shareReceive.superAdmin.winLoseCom;
                            body.commission.superAdmin.winLose = shareReceive.superAdmin.winLose;
                            body.commission.superAdmin.totalWinLoseCom = shareReceive.superAdmin.totalWinLoseCom;

                            body.commission.company.winLoseCom = shareReceive.company.winLoseCom;
                            body.commission.company.winLose = shareReceive.company.winLose;
                            body.commission.company.totalWinLoseCom = shareReceive.company.totalWinLoseCom;

                            body.commission.shareHolder.winLoseCom = shareReceive.shareHolder.winLoseCom;
                            body.commission.shareHolder.winLose = shareReceive.shareHolder.winLose;
                            body.commission.shareHolder.totalWinLoseCom = shareReceive.shareHolder.totalWinLoseCom;

                            body.commission.senior.winLoseCom = shareReceive.senior.winLoseCom;
                            body.commission.senior.winLose = shareReceive.senior.winLose;
                            body.commission.senior.totalWinLoseCom = shareReceive.senior.totalWinLoseCom;

                            body.commission.masterAgent.winLoseCom = shareReceive.masterAgent.winLoseCom;
                            body.commission.masterAgent.winLose = shareReceive.masterAgent.winLose;
                            body.commission.masterAgent.totalWinLoseCom = shareReceive.masterAgent.totalWinLoseCom;

                            body.commission.agent.winLoseCom = shareReceive.agent.winLoseCom;
                            body.commission.agent.winLose = shareReceive.agent.winLose;
                            body.commission.agent.totalWinLoseCom = shareReceive.agent.totalWinLoseCom;

                            body.commission.member.winLoseCom = shareReceive.member.winLoseCom;
                            body.commission.member.winLose = shareReceive.member.winLose;
                            body.commission.member.totalWinLoseCom = shareReceive.member.totalWinLoseCom;
                            body.validAmount = betResult === DRAW ? 0 : body.amount;


                            let updateBody = {
                                $set: {
                                    'commission': body.commission,
                                    'validAmount': body.validAmount,
                                    'betResult': betResult,
                                    'isEndScore': true,
                                    'status': 'DONE',
                                    'updatedDate': body.createdDate,
                                    'settleDate': body.createdDate
                                }
                            };


                            BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                                callbackWL(null, '')
                            });

                        });
                    });

                });
            });


        }, (err) => {
            console.log('length : ', list.length)
            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: 'success'
                }
            );
        });


        function prepareDGCommission(memberInfo, body, currentAgent, overAgent, remaining, overAllShare) {


            if (currentAgent && (currentAgent.type === 'SUPER_ADMIN' || currentAgent.parentId)) {


                console.log('==============================================');
                console.log('process : ' + currentAgent.type + ' : ' + currentAgent.name);


                let money = body.amount;


                if (!overAgent) {
                    overAgent = {};
                    overAgent.type = 'member';
                    overAgent.shareSetting = {};
                    overAgent.shareSetting.casino = {};
                    overAgent.shareSetting.casino.dg = {};
                    overAgent.shareSetting.casino.dg.parent = memberInfo.shareSetting.casino.dg.parent;
                    overAgent.shareSetting.casino.dg.own = 0;
                    overAgent.shareSetting.casino.dg.remaining = 0;

                    body.commission.member = {};
                    body.commission.member.parent = memberInfo.shareSetting.casino.dg.parent;
                    body.commission.member.commission = memberInfo.commissionSetting.casino.dg;

                }

                console.log('');
                console.log('---- setting ----');
                console.log('ส่วนแบ่งที่ได้มามีทั้งหมด : ' + currentAgent.shareSetting.casino.dg.own + ' %');
                console.log('ขอส่วนแบ่งจาก : ' + overAgent.type + ' ' + overAgent.shareSetting.casino.dg.parent + ' %');
                console.log('ให้ส่วนแบ่ง : ' + overAgent.type + ' ที่เหลือไป ' + overAgent.shareSetting.casino.dg.own + ' %');
                console.log('remaining จาก : ' + overAgent.type + ' : ' + overAgent.shareSetting.casino.dg.remaining + ' %');
                console.log('โดนบังคับเสียขั้นต่ำ : ' + currentAgent.shareSetting.casino.dg.min + ' %');
                console.log('---- end setting ----');
                console.log('');


                // console.log('และได้รับ remaining ไว้ : '+overAgent.shareSetting.casino.dg.remaining+' %');

                let currentPercentReceive = overAgent.shareSetting.casino.dg.parent;
                console.log('% ที่ได้ : ' + currentPercentReceive);
                console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');
                let totalRemaining = remaining;

                if (overAgent.shareSetting.casino.dg.remaining > 0) {
                    // console.log()
                    // console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');

                    if (overAgent.shareSetting.casino.dg.remaining > totalRemaining) {
                        console.log('รับ remaining ทั้งหมดไว้: ' + totalRemaining + ' %');
                        currentPercentReceive += totalRemaining;
                        totalRemaining = 0;
                    } else {
                        totalRemaining = remaining - overAgent.shareSetting.casino.dg.remaining;
                        console.log('หักค่า remaining ที่ได้รับมากับที่เซตรับไว้ = ' + remaining + ' - ' + overAgent.shareSetting.casino.dg.remaining + ' = ' + totalRemaining);
                        currentPercentReceive += overAgent.shareSetting.casino.dg.remaining;
                    }

                }

                console.log('รวม % ที่ได้รับ : ' + currentPercentReceive);


                let unUseShare = currentAgent.shareSetting.casino.dg.own -
                    (overAgent.shareSetting.casino.dg.parent + overAgent.shareSetting.casino.dg.own);

                console.log('เหลือส่วนแบ่งที่ไม่ได้ใช้ : ' + unUseShare + ' %');
                totalRemaining = (unUseShare + totalRemaining);
                console.log('ส่วนแบ่งที่ไม่ได้ใช้ บวกกับค่า remaining ท่ี่เหลือจะเป็น : ' + totalRemaining + ' %');

                console.log('จากที่โดนบังคับเสียขั้นต่ำ : ' + currentAgent.shareSetting.casino.dg.min + ' %');
                console.log('overAllShare : ', overAllShare);
                if (currentAgent.shareSetting.casino.dg.min > 0 && ((overAllShare + currentPercentReceive) < currentAgent.shareSetting.casino.dg.min)) {

                    if (currentPercentReceive < currentAgent.shareSetting.casino.dg.min) {
                        console.log('% ที่ได้รับ < ขั้นต่ำที่ถูกตั้ง min ไว้');
                        let percent = currentAgent.shareSetting.casino.dg.min - (currentPercentReceive + overAllShare);
                        console.log('หักออกไป ' + percent + ' % : แล้วไปเพิ่มในส่วนที่ต้องรับผิดชอบ ');
                        totalRemaining = totalRemaining - percent;
                        if (totalRemaining < 0) {
                            totalRemaining = 0;
                        }
                        currentPercentReceive += percent;
                    }
                } else {
                    // currentPercentReceive += totalRemaining;
                }


                if (currentAgent.type === 'SUPER_ADMIN') {
                    currentPercentReceive += totalRemaining;
                }

                let getMoney = (money * NumberUtils.convertPercent(currentPercentReceive)).toFixed(2);
                overAllShare = overAllShare + currentPercentReceive;

                console.log('ยอดแทง ' + money + ' ได้ทั้งหมด : ' + currentPercentReceive + ' %');

                console.log('รวม % + % remaining  จะเป็นสัดส่วนที่ได้ทั้งหมด = ' + getMoney);
                console.log('ค่าที่จะถูกคืนกลับไป (remaining) : ' + totalRemaining + ' %');

                console.log('จำนวน % ที่ถูกแบ่งไปแล้วทั้งหมด : ', overAllShare);

                //set commission
                let agentCommission = currentAgent.commissionSetting.casino.dg;


                switch (currentAgent.type) {
                    case 'SUPER_ADMIN':
                        body.commission.superAdmin = {};
                        body.commission.superAdmin.group = currentAgent._id;
                        body.commission.superAdmin.parent = currentAgent.shareSetting.casino.dg.parent;
                        body.commission.superAdmin.own = currentAgent.shareSetting.casino.dg.own;
                        body.commission.superAdmin.remaining = currentAgent.shareSetting.casino.dg.remaining;
                        body.commission.superAdmin.min = currentAgent.shareSetting.casino.dg.min;
                        body.commission.superAdmin.shareReceive = Math.abs(currentPercentReceive);
                        body.commission.superAdmin.commission = agentCommission;
                        body.commission.superAdmin.amount = getMoney;
                        break;
                    case 'COMPANY':
                        body.commission.company = {};
                        body.commission.company.parentGroup = currentAgent.parentId;
                        body.commission.company.group = currentAgent._id;
                        body.commission.company.parent = currentAgent.shareSetting.casino.dg.parent;
                        body.commission.company.own = currentAgent.shareSetting.casino.dg.own;
                        body.commission.company.remaining = currentAgent.shareSetting.casino.dg.remaining;
                        body.commission.company.min = currentAgent.shareSetting.casino.dg.min;
                        body.commission.company.shareReceive = Math.abs(currentPercentReceive);
                        body.commission.company.commission = agentCommission;
                        body.commission.company.amount = getMoney;
                        break;
                    case 'SHARE_HOLDER':
                        body.commission.shareHolder = {};
                        body.commission.shareHolder.parentGroup = currentAgent.parentId;
                        body.commission.shareHolder.group = currentAgent._id;
                        body.commission.shareHolder.parent = currentAgent.shareSetting.casino.dg.parent;
                        body.commission.shareHolder.own = currentAgent.shareSetting.casino.dg.own;
                        body.commission.shareHolder.remaining = currentAgent.shareSetting.casino.dg.remaining;
                        body.commission.shareHolder.min = currentAgent.shareSetting.casino.dg.min;
                        body.commission.shareHolder.shareReceive = currentPercentReceive;
                        body.commission.shareHolder.commission = agentCommission;
                        body.commission.shareHolder.amount = getMoney;
                        break;
                    case 'SENIOR':
                        body.commission.senior = {};
                        body.commission.senior.parentGroup = currentAgent.parentId;
                        body.commission.senior.group = currentAgent._id;
                        body.commission.senior.parent = currentAgent.shareSetting.casino.dg.parent;
                        body.commission.senior.own = currentAgent.shareSetting.casino.dg.own;
                        body.commission.senior.remaining = currentAgent.shareSetting.casino.dg.remaining;
                        body.commission.senior.min = currentAgent.shareSetting.casino.dg.min;
                        body.commission.senior.shareReceive = currentPercentReceive;
                        body.commission.senior.commission = agentCommission;
                        body.commission.senior.amount = getMoney;
                        break;
                    case 'MASTER_AGENT':
                        body.commission.masterAgent = {};
                        body.commission.masterAgent.parentGroup = currentAgent.parentId;
                        body.commission.masterAgent.group = currentAgent._id;
                        body.commission.masterAgent.parent = currentAgent.shareSetting.casino.dg.parent;
                        body.commission.masterAgent.own = currentAgent.shareSetting.casino.dg.own;
                        body.commission.masterAgent.remaining = currentAgent.shareSetting.casino.dg.remaining;
                        body.commission.masterAgent.min = currentAgent.shareSetting.casino.dg.min;
                        body.commission.masterAgent.shareReceive = currentPercentReceive;
                        body.commission.masterAgent.commission = agentCommission;
                        body.commission.masterAgent.amount = getMoney;
                        break;
                    case 'AGENT':
                        body.commission.agent = {};
                        body.commission.agent.parentGroup = currentAgent.parentId;
                        body.commission.agent.group = currentAgent._id;
                        body.commission.agent.parent = currentAgent.shareSetting.casino.dg.parent;
                        body.commission.agent.own = currentAgent.shareSetting.casino.dg.own;
                        body.commission.agent.remaining = currentAgent.shareSetting.casino.dg.remaining;
                        body.commission.agent.min = currentAgent.shareSetting.casino.dg.min;
                        body.commission.agent.shareReceive = currentPercentReceive;
                        body.commission.agent.commission = agentCommission;
                        body.commission.agent.amount = getMoney;
                        break;
                }

                prepareDGCommission(memberInfo, body, currentAgent.parentId, currentAgent, totalRemaining, overAllShare);
            }
        }

    })


});


router.get('/patch-ambgame-agent', function (req, res) {

    AgentGroupModel.find({type: {$in: ['SUPER_ADMIN', 'COMPANY']}}).exec(function (err, response) {


        async.each(response, (transaction, callbackWL) => {

            let body = {};
            if (transaction.type == 'SUPER_ADMIN') {

                body = {

                    $set: {
                        'shareSetting.multi.amb.parent': 0,//transaction.shareSetting.casino.sexy.parent,
                        'shareSetting.multi.amb.own': 100,//transaction.shareSetting.casino.sexy.own,
                        'shareSetting.multi.amb.remaining': 100,//transaction.shareSetting.casino.sexy.remaining,
                        'shareSetting.multi.amb.min': 0,//transaction.shareSetting.casino.sexy.min,

                        'limitSetting.multi.amb.isEnable': true,
                    }
                };
            } else {
                body = {

                    $set: {
                        'shareSetting.multi.amb.parent': 20,//transaction.shareSetting.casino.sexy.parent,
                        'shareSetting.multi.amb.own': 80,//transaction.shareSetting.casino.sexy.own,
                        'shareSetting.multi.amb.remaining': 80,//transaction.shareSetting.casino.sexy.remaining,
                        'shareSetting.multi.amb.min': 0,//transaction.shareSetting.casino.sexy.min,

                        'limitSetting.multi.amb.isEnable': true,


                    }
                };
            }


            AgentGroupModel.findOneAndUpdate({_id: transaction._id}, body)
                .exec(function (err, creditResponse) {
                    if (err) {
                        callbackWL(err);
                    }
                    else {
                        callbackWL();
                    }
                });
        }, (err) => {

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: 'success'
                }
            );
        });

    });

});

router.get('/x', async function (req, res) {

    let condition = {
        "source": "DREAM_GAME", "status": "RUNNING",
        "gameDate": {
            "$gte": moment().millisecond(0).second(0).minute(0).hour(16).date(8).utc(true).toDate(),
            "$lt": moment().millisecond(0).second(0).minute(21).hour(16).date(8).utc(true).toDate()}
    }
    console.log(condition)
    let results = await BetTransactionModel.find(condition).exec();

    console.log(results.length)


    let list = _.map(results,item =>{
        return {username:item.memberUsername.toUpperCase(),ticketId: item.baccarat.dg.ticketId}
    });
    return res.send(
        {
            code: 0,
            message: "success",
            result: list,
            s: list.length
        }
    );
});

router.get('/patch-m2-agent', function (req, res) {

    AgentGroupModel.find({type: {$in: ['SUPER_ADMIN', 'COMPANY']}}).exec(function (err, response) {


        async.each(response, (transaction, callbackWL) => {

            let body = {};
            if (transaction.type == 'SUPER_ADMIN') {

                body = {

                    $set: {
                        'shareSetting.other.m2.parent': 0,//transaction.shareSetting.casino.sexy.parent,
                        'shareSetting.other.m2.own': 100,//transaction.shareSetting.casino.sexy.own,
                        'shareSetting.other.m2.remaining': 100,//transaction.shareSetting.casino.sexy.remaining,
                        'shareSetting.other.m2.min': 0,//transaction.shareSetting.casino.sexy.min,

                        // 'limitSetting.other.m2.maxPerBetHDP': 10000,
                        // 'limitSetting.other.m2.minPerBetHDP': 20,
                        // 'limitSetting.other.m2.maxBetPerMatchHDP': 50000,
                        //
                        // 'limitSetting.other.m2.maxPerBet': 5000,
                        // 'limitSetting.other.m2.minPerBet': 20,
                        // 'limitSetting.other.m2.maxBetPerDay': 100000,
                        // 'limitSetting.other.m2.maxPayPerBill': 100000,
                        // 'limitSetting.other.m2.maxMatchPerBet': 12,
                        // 'limitSetting.other.m2.minMatchPerBet': 2,
                        // 'limitSetting.other.m2.isEnable': true,
                        //
                        //
                        // 'commissionSetting.other.m2': 0,//transaction.commissionSetting.casino.sexy,

                    }
                };
            } else {
                body = {

                    $set: {
                        'shareSetting.other.m2.parent': 15,//transaction.shareSetting.casino.sexy.parent,
                        'shareSetting.other.m2.own': 85,//transaction.shareSetting.casino.sexy.own,
                        'shareSetting.other.m2.remaining': 85,//transaction.shareSetting.casino.sexy.remaining,
                        'shareSetting.other.m2.min': 85,//transaction.shareSetting.casino.sexy.min,

                        // 'limitSetting.other.m2.maxPerBetHDP': 10000,
                        // 'limitSetting.other.m2.minPerBetHDP': 20,
                        // 'limitSetting.other.m2.maxBetPerMatchHDP': 50000,
                        //
                        // 'limitSetting.other.m2.maxPerBet': 5000,
                        // 'limitSetting.other.m2.minPerBet': 20,
                        // 'limitSetting.other.m2.maxBetPerDay': 100000,
                        // 'limitSetting.other.m2.maxPayPerBill': 100000,
                        // 'limitSetting.other.m2.maxMatchPerBet': 12,
                        // 'limitSetting.other.m2.minMatchPerBet': 2,
                        // 'limitSetting.other.m2.isEnable': true,
                        //
                        // 'commissionSetting.other.m2': 0,//transaction.commissionSetting.casino.sexy,

                    }
                };
            }


            AgentGroupModel.findOneAndUpdate({_id: transaction._id}, body)
                .exec(function (err, creditResponse) {
                    if (err) {
                        callbackWL(err);
                    }
                    else {
                        callbackWL();
                    }
                });
        }, (err) => {

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: 'success'
                }
            );
        });

    });

});

router.get('/patch-m2-member', function (req, res) {


    const cursor = MemberModel.find({})
        .batchSize(1000)
        .lean()
        .cursor();

    cursor.on('data', function (betObj) {
        let body = {

            $set: {
                // 'shareSetting.other.m2.parent': 0,

                'limitSetting.other.m2.maxPerBetHDP': 10000,
                'limitSetting.other.m2.minPerBetHDP': 20,
                'limitSetting.other.m2.maxBetPerMatchHDP': 50000,

                'limitSetting.other.m2.maxPerBet': 5000,
                'limitSetting.other.m2.minPerBet': 20,
                'limitSetting.other.m2.maxBetPerDay': 100000,
                'limitSetting.other.m2.maxPayPerBill': 100000,
                'limitSetting.other.m2.maxMatchPerBet': 12,
                'limitSetting.other.m2.minMatchPerBet': 2,
                'limitSetting.other.m2.isEnable': true,

                'commissionSetting.other.m2': 0
            }
        };

        MemberModel.findOneAndUpdate({_id: betObj._id}, body)
            .exec(function (err, creditResponse) {
                if (err) {
                    // callbackWL(err);
                }
                else {
                    // callbackWL();
                    console.log(creditResponse)
                }
            });

    });


    cursor.on('end', function () {
        console.log('Done!');
        return res.send({code: 0, message: "success"});
    });


});

router.get('/patchCasinoLotto', function (req, res) {


    console.log(roundTo.down(6.811992, 3));

    return res.send(
        {
            code: 0,
            message: "success",
            result: 'success'
        }
    );

});

router.get('/patch1234', function (req, res) {


    async.parallel([callback => {

        let condition = {
            'commissionSetting.casino.sexy': 0.8
        };

        let body = {
            $set: {
                'commissionSetting.casino.sexy': 0.7
            }
        };
        AgentGroupModel.update(condition, body, {multi: true})
            .exec(function (err, creditResponse) {
                if (err) {
                    // callbackWL(err);
                }
                else {
                    MemberModel.update(condition, body, {multi: true})
                        .exec(function (err, creditResponse) {
                            if (err) {
                                // callbackWL(err);
                            }
                            else {
                                callback(null, '');
                            }
                        });
                }
            });
    }, callback => {

        let condition = {
            'commissionSetting.casino.ag': 0.8
        }
        let body = {
            $set: {
                'commissionSetting.casino.ag': 0.7
            }
        };
        AgentGroupModel.update(condition, body, {multi: true})
            .exec(function (err, creditResponse) {
                if (err) {
                    // callbackWL(err);
                }
                else {
                    MemberModel.update(condition, body, {multi: true})
                        .exec(function (err, creditResponse) {
                            if (err) {
                                // callbackWL(err);
                            }
                            else {
                                callback(null, '');
                            }
                        });
                }
            });
    }, callback => {

        let condition = {
            'commissionSetting.casino.dg': 0.8
        }
        let body = {
            $set: {
                'commissionSetting.casino.dg': 0.7
            }
        };
        AgentGroupModel.update(condition, body, {multi: true})
            .exec(function (err, creditResponse) {
                if (err) {
                    // callbackWL(err);
                }
                else {
                    MemberModel.update(condition, body, {multi: true})
                        .exec(function (err, creditResponse) {
                            if (err) {
                                // callbackWL(err);
                            }
                            else {
                                callback(null, '');
                            }
                        });
                }
            });
    }, callback => {

        let condition = {
            'commissionSetting.casino.sa': 0.8
        }
        let body = {
            $set: {
                'commissionSetting.casino.sa': 0.7
            }
        };
        AgentGroupModel.update(condition, body, {multi: true})
            .exec(function (err, creditResponse) {
                if (err) {
                    // callbackWL(err);
                }
                else {
                    MemberModel.update(condition, body, {multi: true})
                        .exec(function (err, creditResponse) {
                            if (err) {
                                // callbackWL(err);
                            }
                            else {
                                callback(null, '');
                            }
                        });
                }
            });
    }], (err, response) => {
        return res.send({code: 0, message: "success"});
    });


});

router.get('/patchSA-agent', function (req, res) {

    let body = {

        $set: {
            'limitSetting.casino.sa.limit': 5,
            'limitSetting.casino.dg.limit': 5
        }
    };

    AgentGroupModel.update({}, body, {multi: true})
        .exec(function (err, creditResponse) {
            if (err) {
                // callbackWL(err);
            }
            else {
                return res.send({code: 0, message: "success"});
            }
        });

});

router.get('/patch-pt-agent', function (req, res) {

    AgentGroupModel.find({}).exec(function (err, response) {


        async.each(response, (transaction, callbackWL) => {

            let body = {

                $set: {
                    'shareSetting.casino.pt.parent': transaction.shareSetting.casino.sexy.parent,
                    'shareSetting.casino.pt.own': transaction.shareSetting.casino.sexy.own,
                    'shareSetting.casino.pt.remaining': transaction.shareSetting.casino.sexy.remaining,
                    'shareSetting.casino.pt.min': transaction.shareSetting.casino.sexy.min,

                    'limitSetting.casino.pt.isEnable': transaction.limitSetting.casino.sexy.isEnable,
                    'limitSetting.casino.pt.limit': transaction.limitSetting.casino.sexy.limit,

                    'commissionSetting.casino.pt': transaction.commissionSetting.casino.sexy,

                    // 'shareSetting.multi.amb.parent': 0,
                    // 'shareSetting.multi.amb.own': 0,
                    // 'shareSetting.multi.amb.remaining': 0,
                    // 'shareSetting.multi.amb.min': 0,
                    //
                    // 'limitSetting.multi.amb.isEnable': false,
                    //
                    // 'commissionSetting.multi.amb': 0
                }
            };


            console.log(body)
            AgentGroupModel.findOneAndUpdate({_id: transaction._id}, body)
                .exec(function (err, creditResponse) {
                    if (err) {
                        callbackWL(err);
                    }
                    else {
                        callbackWL();
                    }
                });
        }, (err) => {

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: 'success'
                }
            );
        });

    });

});

router.get('/patch-pt-member', function (req, res) {


    // let body = {
    //     $set: {
    //         'shareSetting.casino.pt.parent': transaction.shareSetting.casino.sexy.parent,
    //
    //         'limitSetting.casino.pt.isEnable': transaction.limitSetting.casino.sexy.isEnable,
    //         'limitSetting.casino.pt.limit': transaction.limitSetting.casino.sexy.limit,
    //
    //         'commissionSetting.casino.pt': 0,
    //
    //
    //         // 'shareSetting.multi.amb.parent': 0,//transaction.shareSetting.casino.sexy.parent,
    //         //
    //         // 'limitSetting.multi.amb.isEnable': false,
    //         //
    //         // 'commissionSetting.multi.amb': 0
    //     }
    // };
    //
    //
    // MemberModel.update({}, body, {multi: true})
    //     .exec(function (err, creditResponse) {
    //         if (err) {
    //             return res.send(
    //                 {
    //                     code: 999,
    //                     message: "error"
    //                 }
    //             );
    //         } else {
    //             console.log(creditResponse)
    //             return res.send(
    //                 {
    //                     code: 0,
    //                     message: "success",
    //                     result: 'success'
    //                 }
    //             );
    //         }
    //     });

    // const cursor = MemberModel.find({'createdDate': {"$gte": new Date(moment("08/02/2020", "DD/MM/YYYY").format('YYYY-MM-DDT20:13:00.000'))}})
    const cursor = MemberModel.find({
        // createdDate: {
        //     "$gte": new Date(moment().utc(true).format('2020-02-01T00:00:00.000'))
        // }
    })
        .batchSize(10000)
        .cursor();

    cursor.on('data', function (transaction) {

        let body = {

            $set: {
                'shareSetting.casino.pt.parent': transaction.shareSetting.casino.sexy.parent,

                'limitSetting.casino.pt.isEnable': transaction.limitSetting.casino.sexy.isEnable,
                'limitSetting.casino.pt.limit': transaction.limitSetting.casino.sexy.limit,

                'commissionSetting.casino.pt': transaction.commissionSetting.casino.sexy,


                // 'shareSetting.multi.amb.parent': 0,//transaction.shareSetting.casino.sexy.parent,
                //
                // 'limitSetting.multi.amb.isEnable': false,
                //
                // 'commissionSetting.multi.amb': 0
            }
        };


        console.log(body)
        transaction.update(body)
            .exec(function (err, creditResponse) {
                if (err) {
                    // callbackWL(err);
                    console.log(err)
                }
                else {
                    // callbackWL();
                    console.log(creditResponse)
                }
            });

    });


    cursor.on('end', function () {
        console.log('Done!');
        return res.send({code: 0, message: "success"});
    });


});

router.get('/patchSA-member', function (req, res) {


    let body = {

        $set: {
            'limitSetting.casino.sa.limit': 5,
            'limitSetting.casino.dg.limit': 5
        }
    };

    MemberModel.update({}, body, {multi: true})
        .exec(function (err, creditResponse) {
            if (err) {
                // callbackWL(err);
            }
            else {
                return res.send({code: 0, message: "success"});
            }
        });


});

router.get('/patch-ag-agent', function (req, res) {

    AgentGroupModel.find({}).exec(function (err, response) {


        async.each(response, (transaction, callbackWL) => {

            let body = {

                $set: {
                    'shareSetting.casino.ag.parent': transaction.shareSetting.casino.sexy.parent,
                    'shareSetting.casino.ag.own': transaction.shareSetting.casino.sexy.own,
                    'shareSetting.casino.ag.remaining': transaction.shareSetting.casino.sexy.remaining,
                    'shareSetting.casino.ag.min': transaction.shareSetting.casino.sexy.min,

                    'limitSetting.casino.ag.isEnable': transaction.limitSetting.casino.sexy.isEnable,
                    'limitSetting.casino.ag.limit': transaction.limitSetting.casino.sexy.limit,

                    'commissionSetting.casino.ag': transaction.commissionSetting.casino.sexy,

                    // 'shareSetting.multi.amb.parent': 0,
                    // 'shareSetting.multi.amb.own': 0,
                    // 'shareSetting.multi.amb.remaining': 0,
                    // 'shareSetting.multi.amb.min': 0,
                    //
                    // 'limitSetting.multi.amb.isEnable': false,
                    //
                    // 'commissionSetting.multi.amb': 0
                }
            };


            console.log(body)
            AgentGroupModel.findOneAndUpdate({_id: transaction._id}, body)
                .exec(function (err, creditResponse) {
                    if (err) {
                        callbackWL(err);
                    }
                    else {
                        callbackWL();
                    }
                });
        }, (err) => {

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: 'success'
                }
            );
        });

    });

});

router.get('/patch-ag-member', function (req, res) {

    //
    // let body = {
    //     $set: {
    //         // 'shareSetting.casino.ag.parent': transaction.shareSetting.casino.sexy.parent,
    //         //
    //         // 'limitSetting.casino.ag.isEnable': transaction.limitSetting.casino.sexy.isEnable,
    //         // 'limitSetting.casino.ag.limit': transaction.limitSetting.casino.sexy.limit,
    //
    //         'commissionSetting.casino.ag': 0,
    //
    //
    //         // 'shareSetting.multi.amb.parent': 0,//transaction.shareSetting.casino.sexy.parent,
    //         //
    //         // 'limitSetting.multi.amb.isEnable': false,
    //         //
    //         // 'commissionSetting.multi.amb': 0
    //     }
    // };
    //
    //
    // MemberModel.update({}, body, {multi: true})
    //     .exec(function (err, creditResponse) {
    //         if (err) {
    //             return res.send(
    //                 {
    //                     code: 999,
    //                     message: "error"
    //                 }
    //             );
    //         } else {
    //             console.log(creditResponse)
    //             return res.send(
    //                 {
    //                     code: 0,
    //                     message: "success",
    //                     result: 'success'
    //                 }
    //             );
    //         }
    //     });

    const cursor = MemberModel.find({})
        .batchSize(10000)
        .cursor();

    cursor.on('data', function (transaction) {

        let body = {

            $set: {
                'shareSetting.casino.ag.parent': transaction.shareSetting.casino.sexy.parent,

                'limitSetting.casino.ag.isEnable': transaction.limitSetting.casino.sexy.isEnable,
                'limitSetting.casino.ag.limit': transaction.limitSetting.casino.sexy.limit,

                'commissionSetting.casino.ag': transaction.commissionSetting.casino.sexy,


                // 'shareSetting.multi.amb.parent': 0,//transaction.shareSetting.casino.sexy.parent,
                //
                // 'limitSetting.multi.amb.isEnable': false,
                //
                // 'commissionSetting.multi.amb': 0
            }
        };


        console.log(body)
        MemberModel.findOneAndUpdate({_id: transaction._id}, body)
            .exec(function (err, creditResponse) {
                if (err) {
                    // callbackWL(err);
                }
                else {
                    // callbackWL();
                    console.log(creditResponse)
                }
            });

    });


    cursor.on('end', function () {
        console.log('Done!');
        return res.send({code: 0, message: "success"});
    });


});

router.get('/patch-dg-member', function (req, res) {

    const cursor = MemberModel.find({})
        .batchSize(1000)
        .lean()
        .cursor();

    cursor.on('data', function (betObj) {
        let body = {

            $set: {
                'shareSetting.casino.dg.parent': betObj.shareSetting.casino.sexy.parent,

                'limitSetting.casino.dg.isEnable': betObj.limitSetting.casino.sexy.isEnable,

                'commissionSetting.casino.dg': betObj.commissionSetting.casino.sexy
            }
        };

        MemberModel.findOneAndUpdate({_id: betObj._id}, body)
            .exec(function (err, creditResponse) {
                if (err) {
                    // callbackWL(err);
                }
                else {
                    // callbackWL();
                    console.log(creditResponse)
                }
            });

    });


    cursor.on('end', function () {
        console.log('Done!');
        return res.send({code: 0, message: "success"});
    });

});

router.get('/patch-payment-agent', function (req, res) {


    let body = {
        $set: {
            paymentSetting: {
                monday: true,
                tuesday: true,
                wednesday: true,
                thursday: true,
                friday: true,
                saturday: true,
                sunday: true
            }
        }
    };


    AgentGroupModel.update({}, body, {multi: true})
        .exec(function (err, creditResponse) {
            if (err) {
                return res.send(
                    {
                        code: 999,
                        message: "error"
                    }
                );
            } else {
                console.log(creditResponse)
                return res.send(
                    {
                        code: 0,
                        message: "success",
                        result: 'success'
                    }
                );
            }
        });

});

router.get('/patch-allbet-member', function (req, res) {

    MemberModel.find({}).exec(function (err, response) {


        async.each(response, (transaction, callbackWL) => {

            let body = {

                $set: {
                    'shareSetting.casino.allbet.parent': transaction.shareSetting.casino.sexy.parent,

                    'limitSetting.casino.allbet.isEnable': transaction.limitSetting.casino.sexy.isEnable,

                    'limitSetting.casino.allbet.limitA': true,
                    'limitSetting.casino.allbet.limitB': true,
                    'limitSetting.casino.allbet.limitC': true,

                    'limitSetting.casino.allbet.limitVip': true,

                    'commissionSetting.casino.allbet': transaction.commissionSetting.casino.sexy
                }
            };


            console.log(body)
            MemberModel.findOneAndUpdate({_id: transaction._id}, body)
                .exec(function (err, creditResponse) {
                    if (err) {
                        callbackWL(err);
                    }
                    else {
                        callbackWL();
                    }
                });
        }, (err) => {

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: 'success'
                }
            );
        });

    });

});

router.get('/patchLotto-member', function (req, res) {

    let body = {

        $set: {
            //         // 'shareSetting.lotto.pp.parent': 0,
            //         // 'limitSetting.lotto.pp.hour': 0,
            //         // 'limitSetting.lotto.pp.minute': 0,
            //         // 'limitSetting.lotto.pp.isEnable': false,
            //         //
            //         // 'shareSetting.lotto.laos.parent': 0,
            //         'limitSetting.lotto.laos.hour': 20,
            //         'limitSetting.lotto.laos.minute': 0,
            //         'limitSetting.lotto.laos.isEnable': true,
            //         //
            //         //
            //         //
            //         'limitSetting.lotto.laos.L_4TOP.payout': 1000,
            //         'limitSetting.lotto.laos.L_4TOP.discount': 10,
            //         'limitSetting.lotto.laos.L_4TOP.max': 3000,
            //         //
            //         'limitSetting.lotto.laos.L_4TOD.payout': 40,
            //         'limitSetting.lotto.laos.L_4TOD.discount': 10,
            //         'limitSetting.lotto.laos.L_4TOD.max': 3000,
            //         //
            //         'limitSetting.lotto.laos.L_3TOP.payout': 350,
            //         'limitSetting.lotto.laos.L_3TOP.discount': 10,
            //         'limitSetting.lotto.laos.L_3TOP.max': 3000,
            //         //
            //         'limitSetting.lotto.laos.L_3TOD.payout': 25,
            //         'limitSetting.lotto.laos.L_3TOD.discount': 10,
            //         'limitSetting.lotto.laos.L_3TOD.max': 3000,
            //         //
            //         'limitSetting.lotto.laos.L_2FB.payout': 15,
            //         'limitSetting.lotto.laos.L_2FB.discount': 10,
            //         'limitSetting.lotto.laos.L_2FB.max': 3000,
            //         //
            //
            //
            //         'limitSetting.lotto.laos.T_4TOP.payout': 5000,
            //         'limitSetting.lotto.laos.T_4TOP.discount': 33,
            //         'limitSetting.lotto.laos.T_4TOP.max': 5000,
            //         //
            //         'limitSetting.lotto.laos.T_4TOD.payout': 225,
            //         'limitSetting.lotto.laos.T_4TOD.discount': 33,
            //         'limitSetting.lotto.laos.T_4TOD.max': 7000,
            //         //
            //         'limitSetting.lotto.laos.T_3TOP.payout': 550,
            //         'limitSetting.lotto.laos.T_3TOP.discount': 33,
            //         'limitSetting.lotto.laos.T_3TOP.max': 5000,
            //         //
            //         'limitSetting.lotto.laos.T_3TOD.payout': 100,
            //         'limitSetting.lotto.laos.T_3TOD.discount': 33,
            //         'limitSetting.lotto.laos.T_3TOD.max': 20000,
            //
            //         'limitSetting.lotto.laos.T_2TOP.payout': 70,
            //         'limitSetting.lotto.laos.T_2TOP.discount': 28,
            //         'limitSetting.lotto.laos.T_2TOP.max': 20000,
            //         //
            //         'limitSetting.lotto.laos.T_2TOD.payout': 12,
            //         'limitSetting.lotto.laos.T_2TOD.discount': 28,
            //         'limitSetting.lotto.laos.T_2TOD.max': 20000,
            //         //
            //         'limitSetting.lotto.laos.T_2BOT.payout': 70,
            //         'limitSetting.lotto.laos.T_2BOT.discount': 28,
            //         'limitSetting.lotto.laos.T_2BOT.max': 20000,
            //
            //         'limitSetting.lotto.laos.T_1TOP.payout': 3,
            //         'limitSetting.lotto.laos.T_1TOP.discount': 12,
            //         'limitSetting.lotto.laos.T_1TOP.max': 200000,
            //         //
            //         'limitSetting.lotto.laos.T_1BOT.payout': 4,
            //         'limitSetting.lotto.laos.T_1BOT.discount': 12,
            //         'limitSetting.lotto.laos.T_1BOT.max': 200000,
            //         //
            //         //
            'limitSetting.lotto.pp._6TOP.payout': 50000,
            'limitSetting.lotto.pp._6TOP.discount': 0,
            'limitSetting.lotto.pp._6TOP.max': 1000,

            'limitSetting.lotto.pp._5TOP.payout': 25000,
            'limitSetting.lotto.pp._5TOP.discount': 0,
            'limitSetting.lotto.pp._5TOP.max': 1000,

            'limitSetting.lotto.pp._4TOP.payout': 5000,
            'limitSetting.lotto.pp._4TOP.discount': 0,
            'limitSetting.lotto.pp._4TOP.max': 1000,

            'limitSetting.lotto.pp._4TOD.payout': 225,
            'limitSetting.lotto.pp._4TOD.discount': 0,
            'limitSetting.lotto.pp._4TOD.max': 10000,

            'limitSetting.lotto.pp._3TOP.payout': 800,
            'limitSetting.lotto.pp._3TOP.discount': 0,
            'limitSetting.lotto.pp._3TOP.max': 2000,

            'limitSetting.lotto.pp._3TOD.payout': 130,
            'limitSetting.lotto.pp._3TOD.discount': 0,
            'limitSetting.lotto.pp._3TOD.max': 10000,

            'limitSetting.lotto.pp._2TOP.payout': 90,
            'limitSetting.lotto.pp._2TOP.discount': 0,
            'limitSetting.lotto.pp._2TOP.max': 10000,

            'limitSetting.lotto.pp._2TOD.payout': 12,
            'limitSetting.lotto.pp._2TOD.discount': 0,
            'limitSetting.lotto.pp._2TOD.max': 100000,

            'limitSetting.lotto.pp._2BOT.payout': 90,
            'limitSetting.lotto.pp._2BOT.discount': 0,
            'limitSetting.lotto.pp._2BOT.max': 10000,

            'limitSetting.lotto.pp._1TOP.payout': 3,
            'limitSetting.lotto.pp._1TOP.discount': 0,
            'limitSetting.lotto.pp._1TOP.max': 100000,

            'limitSetting.lotto.pp._1BOT.payout': 4,
            'limitSetting.lotto.pp._1BOT.discount': 0,
            'limitSetting.lotto.pp._1BOT.max': 100000,

        }
    };

    MemberModel.update({}, body, {multi: true})
        .exec(function (err, creditResponse) {
            if (err) {
                return res.send(
                    {
                        code: 999,
                        message: "error"
                    }
                );
            } else {

                console.log(creditResponse)
                return res.send(
                    {
                        code: 0,
                        message: "success",
                        result: 'success'
                    }
                );
            }
        });

    // MemberModel.find({}).exec(function (err, response) {
    //
    //     async.each(response, (betObj, callbackWL) => {
    //
    //         let body = {
    //
    //             // $set: {
    //             //     'shareSetting.lotto.pp.parent': 0,
    //             //     'limitSetting.lotto.pp.hour': 0,
    //             //     'limitSetting.lotto.pp.minute': 0,
    //             //     'limitSetting.lotto.pp.isEnable': false,
    //             //
    //             //     'shareSetting.lotto.laos.parent': 0,
    //             //     'limitSetting.lotto.laos.hour': 0,
    //             //     'limitSetting.lotto.laos.minute': 0,
    //             //     'limitSetting.lotto.laos.isEnable': false,
    //             //
    //             //
    //             //
    //             //     'limitSetting.lotto.laos.L_4TOP.payout': 0,
    //             //     'limitSetting.lotto.laos.L_4TOP.discount': 0,
    //             //     'limitSetting.lotto.laos.L_4TOP.max': 0,
    //             //     //
    //             //     'limitSetting.lotto.laos.L_4TOD.payout': 0,
    //             //     'limitSetting.lotto.laos.L_4TOD.discount': 0,
    //             //     'limitSetting.lotto.laos.L_4TOD.max': 0,
    //             //     //
    //             //     'limitSetting.lotto.laos.L_3TOP.payout': 0,
    //             //     'limitSetting.lotto.laos.L_3TOP.discount': 0,
    //             //     'limitSetting.lotto.laos.L_3TOP.max': 0,
    //             //     //
    //             //     'limitSetting.lotto.laos.L_3TOD.payout': 0,
    //             //     'limitSetting.lotto.laos.L_3TOD.discount': 0,
    //             //     'limitSetting.lotto.laos.L_3TOD.max': 0,
    //             //     //
    //             //     'limitSetting.lotto.laos.L_2FB.payout': 0,
    //             //     'limitSetting.lotto.laos.L_2FB.discount': 0,
    //             //     'limitSetting.lotto.laos.L_2FB.max': 0,
    //             //     //
    //             //
    //             //
    //             //     'limitSetting.lotto.laos.T_4TOP.payout': 0,
    //             //     'limitSetting.lotto.laos.T_4TOP.discount': 0,
    //             //     'limitSetting.lotto.laos.T_4TOP.max': 0,
    //             //     //
    //             //     'limitSetting.lotto.laos.T_4TOD.payout': 0,
    //             //     'limitSetting.lotto.laos.T_4TOD.discount': 0,
    //             //     'limitSetting.lotto.laos.T_4TOD.max': 0,
    //             //     //
    //             //     'limitSetting.lotto.laos.T_3TOP.payout': 0,
    //             //     'limitSetting.lotto.laos.T_3TOP.discount': 0,
    //             //     'limitSetting.lotto.laos.T_3TOP.max': 0,
    //             //     //
    //             //     'limitSetting.lotto.laos.T_3TOD.payout': 0,
    //             //     'limitSetting.lotto.laos.T_3TOD.discount': 0,
    //             //     'limitSetting.lotto.laos.T_3TOD.max': 0,
    //             //
    //             //     'limitSetting.lotto.laos.T_2TOP.payout': 0,
    //             //     'limitSetting.lotto.laos.T_2TOP.discount': 0,
    //             //     'limitSetting.lotto.laos.T_2TOP.max': 0,
    //             //     //
    //             //     'limitSetting.lotto.laos.T_2TOD.payout': 0,
    //             //     'limitSetting.lotto.laos.T_2TOD.discount': 0,
    //             //     'limitSetting.lotto.laos.T_2TOD.max': 0,
    //             //     //
    //             //     'limitSetting.lotto.laos.T_2BOT.payout': 0,
    //             //     'limitSetting.lotto.laos.T_2BOT.discount': 0,
    //             //     'limitSetting.lotto.laos.T_2BOT.max': 0,
    //             //
    //             //     'limitSetting.lotto.laos.T_1TOP.payout': 0,
    //             //     'limitSetting.lotto.laos.T_1TOP.discount': 0,
    //             //     'limitSetting.lotto.laos.T_1TOP.max': 0,
    //             //     //
    //             //     'limitSetting.lotto.laos.T_1BOT.payout': 0,
    //             //     'limitSetting.lotto.laos.T_1BOT.discount': 0,
    //             //     'limitSetting.lotto.laos.T_1BOT.max': 0,
    //             //
    //             //
    //             //
    //             //     'limitSetting.lotto.pp._6TOP.payout': 0,
    //             //     'limitSetting.lotto.pp._6TOP.discount': 0,
    //             //     'limitSetting.lotto.pp._6TOP.max': 0,
    //             //
    //             //     'limitSetting.lotto.pp._5TOP.payout': 0,
    //             //     'limitSetting.lotto.pp._5TOP.discount': 0,
    //             //     'limitSetting.lotto.pp._5TOP.max': 0,
    //             //
    //             //     'limitSetting.lotto.pp._4TOP.payout': 0,
    //             //     'limitSetting.lotto.pp._4TOP.discount': 0,
    //             //     'limitSetting.lotto.pp._4TOP.max': 0,
    //             //
    //             //     'limitSetting.lotto.pp._4TOD.payout': 0,
    //             //     'limitSetting.lotto.pp._4TOD.discount': 0,
    //             //     'limitSetting.lotto.pp._4TOD.max': 0,
    //             //
    //             //     'limitSetting.lotto.pp._3TOP.payout': 0,
    //             //     'limitSetting.lotto.pp._3TOP.discount': 0,
    //             //     'limitSetting.lotto.pp._3TOP.max': 0,
    //             //
    //             //     'limitSetting.lotto.pp._3TOD.payout': 0,
    //             //     'limitSetting.lotto.pp._3TOD.discount': 0,
    //             //     'limitSetting.lotto.pp._3TOD.max': 0,
    //             //
    //             //     'limitSetting.lotto.pp._3BOT.payout': 0,
    //             //     'limitSetting.lotto.pp._3BOT.discount': 0,
    //             //     'limitSetting.lotto.pp._3BOT.max': 0,
    //             //
    //             //     'limitSetting.lotto.pp._3TOP_OE.payout': 0,
    //             //     'limitSetting.lotto.pp._3TOP_OE.discount': 0,
    //             //     'limitSetting.lotto.pp._3TOP_OE.max': 0,
    //             //
    //             //     'limitSetting.lotto.pp._3TOP_OU.payout': 0,
    //             //     'limitSetting.lotto.pp._3TOP_OU.discount': 0,
    //             //     'limitSetting.lotto.pp._3TOP_OU.max': 0,
    //             //
    //             //     'limitSetting.lotto.pp._2TOP.payout': 0,
    //             //     'limitSetting.lotto.pp._2TOP.discount': 0,
    //             //     'limitSetting.lotto.pp._2TOP.max': 0,
    //             //
    //             //     'limitSetting.lotto.pp._2TOD.payout': 0,
    //             //     'limitSetting.lotto.pp._2TOD.discount': 0,
    //             //     'limitSetting.lotto.pp._2TOD.max': 0,
    //             //
    //             //     'limitSetting.lotto.pp._2BOT.payout': 0,
    //             //     'limitSetting.lotto.pp._2BOT.discount': 0,
    //             //     'limitSetting.lotto.pp._2BOT.max': 0,
    //             //
    //             //     'limitSetting.lotto.pp._2TOP_OE.payout': 0,
    //             //     'limitSetting.lotto.pp._2TOP_OE.discount': 0,
    //             //     'limitSetting.lotto.pp._2TOP_OE.max': 0,
    //             //
    //             //     'limitSetting.lotto.pp._2TOP_OU.payout': 0,
    //             //     'limitSetting.lotto.pp._2TOP_OU.discount': 0,
    //             //     'limitSetting.lotto.pp._2TOP_OU.max': 0,
    //             //
    //             //     'limitSetting.lotto.pp._2BOT_OE.payout': 0,
    //             //     'limitSetting.lotto.pp._2BOT_OE.discount': 0,
    //             //     'limitSetting.lotto.pp._2BOT_OE.max': 0,
    //             //
    //             //     'limitSetting.lotto.pp._2BOT_OU.payout': 0,
    //             //     'limitSetting.lotto.pp._2BOT_OU.discount': 0,
    //             //     'limitSetting.lotto.pp._2BOT_OU.max': 0,
    //             //
    //             //     'limitSetting.lotto.pp._1TOP.payout': 0,
    //             //     'limitSetting.lotto.pp._1TOP.discount': 0,
    //             //     'limitSetting.lotto.pp._1TOP.max': 0,
    //             //
    //             //     'limitSetting.lotto.pp._1BOT.payout': 0,
    //             //     'limitSetting.lotto.pp._1BOT.discount': 0,
    //             //     'limitSetting.lotto.pp._1BOT.max': 0,
    //             //
    //             // }
    //
    //
    //             $set: {
    //
    //                 'shareSetting.lotto.pp.parent': betObj.shareSetting.lotto.amb.parent,
    //                 'limitSetting.lotto.pp.isEnable': true,//betObj.limitSetting.lotto.pp.isEnable,
    //                 'limitSetting.lotto.pp.hour': 0,
    //                 'limitSetting.lotto.pp.minute': 0,
    //
    //                 // 'shareSetting.lotto.laos.parent': betObj.shareSetting.lotto.amb.parent,
    //                 // 'shareSetting.lotto.laos.min': betObj.shareSetting.lotto.amb.min,
    //                 // 'limitSetting.lotto.laos.hour': 20,
    //                 // 'limitSetting.lotto.laos.minute': 0,
    //                 //
    //                 // 'limitSetting.lotto.laos.isEnable': betObj.limitSetting.lotto.amb.isEnable,
    //                 //
    //                 //
    //                 // 'limitSetting.lotto.laos.L_4TOP.payout': 1000,
    //                 // 'limitSetting.lotto.laos.L_4TOP.discount': 0,
    //                 // 'limitSetting.lotto.laos.L_4TOP.max': 3000,
    //                 // //
    //                 // 'limitSetting.lotto.laos.L_4TOD.payout': 40,
    //                 // 'limitSetting.lotto.laos.L_4TOD.discount': 0,
    //                 // 'limitSetting.lotto.laos.L_4TOD.max': 3000,
    //                 // //
    //                 // 'limitSetting.lotto.laos.L_3TOP.payout': 350,
    //                 // 'limitSetting.lotto.laos.L_3TOP.discount': 0,
    //                 // 'limitSetting.lotto.laos.L_3TOP.max': 3000,
    //                 // //
    //                 // 'limitSetting.lotto.laos.L_3TOD.payout': 25,
    //                 // 'limitSetting.lotto.laos.L_3TOD.discount': 0,
    //                 // 'limitSetting.lotto.laos.L_3TOD.max': 3000,
    //                 // //
    //                 // 'limitSetting.lotto.laos.L_2FB.payout': 15,
    //                 // 'limitSetting.lotto.laos.L_2FB.discount': 0,
    //                 // 'limitSetting.lotto.laos.L_2FB.max': 3000,
    //                 //
    //                 //
    //                 // 'limitSetting.lotto.laos.T_4TOP.payout': betObj.limitSetting.lotto.amb._4TOP.payout,
    //                 // 'limitSetting.lotto.laos.T_4TOP.discount': betObj.limitSetting.lotto.amb._4TOP.discount,
    //                 // 'limitSetting.lotto.laos.T_4TOP.max': betObj.limitSetting.lotto.amb._4TOP.max,
    //                 // //
    //                 // 'limitSetting.lotto.laos.T_4TOD.payout': betObj.limitSetting.lotto.amb._4TOD.payout,
    //                 // 'limitSetting.lotto.laos.T_4TOD.discount': betObj.limitSetting.lotto.amb._4TOD.discount,
    //                 // 'limitSetting.lotto.laos.T_4TOD.max': betObj.limitSetting.lotto.amb._4TOD.max,
    //                 // //
    //                 // 'limitSetting.lotto.laos.T_3TOP.payout': betObj.limitSetting.lotto.amb._3TOP.payout,
    //                 // 'limitSetting.lotto.laos.T_3TOP.discount': betObj.limitSetting.lotto.amb._3TOP.discount,
    //                 // 'limitSetting.lotto.laos.T_3TOP.max': betObj.limitSetting.lotto.amb._3TOP.max,
    //                 // //
    //                 // 'limitSetting.lotto.laos.T_3TOD.payout': betObj.limitSetting.lotto.amb._3TOD.payout,
    //                 // 'limitSetting.lotto.laos.T_3TOD.discount': betObj.limitSetting.lotto.amb._3TOD.discount,
    //                 // 'limitSetting.lotto.laos.T_3TOD.max': betObj.limitSetting.lotto.amb._3TOD.max,
    //                 //
    //                 // 'limitSetting.lotto.laos.T_2TOP.payout': betObj.limitSetting.lotto.amb._2TOP.payout,
    //                 // 'limitSetting.lotto.laos.T_2TOP.discount': betObj.limitSetting.lotto.amb._2TOP.discount,
    //                 // 'limitSetting.lotto.laos.T_2TOP.max': betObj.limitSetting.lotto.amb._2TOP.max,
    //                 // //
    //                 // 'limitSetting.lotto.laos.T_2TOD.payout': betObj.limitSetting.lotto.amb._2TOD.payout,
    //                 // 'limitSetting.lotto.laos.T_2TOD.discount': betObj.limitSetting.lotto.amb._2TOD.discount,
    //                 // 'limitSetting.lotto.laos.T_2TOD.max': betObj.limitSetting.lotto.amb._2TOD.max,
    //                 // //
    //                 // 'limitSetting.lotto.laos.T_2BOT.payout': betObj.limitSetting.lotto.amb._2BOT.payout,
    //                 // 'limitSetting.lotto.laos.T_2BOT.discount': betObj.limitSetting.lotto.amb._2BOT.discount,
    //                 // 'limitSetting.lotto.laos.T_2BOT.max': betObj.limitSetting.lotto.amb._2BOT.max,
    //                 //
    //                 // 'limitSetting.lotto.laos.T_1TOP.payout': betObj.limitSetting.lotto.amb._1TOP.payout,
    //                 // 'limitSetting.lotto.laos.T_1TOP.discount': betObj.limitSetting.lotto.amb._1TOP.discount,
    //                 // 'limitSetting.lotto.laos.T_1TOP.max': betObj.limitSetting.lotto.amb._1TOP.max,
    //                 // //
    //                 // 'limitSetting.lotto.laos.T_1BOT.payout': betObj.limitSetting.lotto.amb._1BOT.payout,
    //                 // 'limitSetting.lotto.laos.T_1BOT.discount': betObj.limitSetting.lotto.amb._1BOT.discount,
    //                 // 'limitSetting.lotto.laos.T_1BOT.max': betObj.limitSetting.lotto.amb._1BOT.max,
    //                 //
    //                 //
    //                 //
    //                 'limitSetting.lotto.pp._6TOP.payout': 50000,
    //                 'limitSetting.lotto.pp._6TOP.discount': 5,
    //                 'limitSetting.lotto.pp._6TOP.max': 1000,
    //
    //                 'limitSetting.lotto.pp._5TOP.payout': 25000,
    //                 'limitSetting.lotto.pp._5TOP.discount': 5,
    //                 'limitSetting.lotto.pp._5TOP.max': 1000,
    //
    //                 'limitSetting.lotto.pp._4TOP.payout': 4500,
    //                 'limitSetting.lotto.pp._4TOP.discount': 5,
    //                 'limitSetting.lotto.pp._4TOP.max': 1000,
    //
    //                 'limitSetting.lotto.pp._4TOD.payout': 220,
    //                 'limitSetting.lotto.pp._4TOD.discount': 5,
    //                 'limitSetting.lotto.pp._4TOD.max': 10000,
    //
    //                 'limitSetting.lotto.pp._3TOP.payout': 800,
    //                 'limitSetting.lotto.pp._3TOP.discount': 5,
    //                 'limitSetting.lotto.pp._3TOP.max': 2000,
    //
    //                 'limitSetting.lotto.pp._3TOD.payout': 130,
    //                 'limitSetting.lotto.pp._3TOD.discount': 5,
    //                 'limitSetting.lotto.pp._3TOD.max': 10000,
    //
    //                 'limitSetting.lotto.pp._2TOP.payout': 85,
    //                 'limitSetting.lotto.pp._2TOP.discount': 5,
    //                 'limitSetting.lotto.pp._2TOP.max': 10000,
    //
    //                 'limitSetting.lotto.pp._2TOD.payout': 12,
    //                 'limitSetting.lotto.pp._2TOD.discount': 5,
    //                 'limitSetting.lotto.pp._2TOD.max': 100000,
    //
    //                 'limitSetting.lotto.pp._2BOT.payout': 85,
    //                 'limitSetting.lotto.pp._2BOT.discount': 5,
    //                 'limitSetting.lotto.pp._2BOT.max': 10000,
    //
    //                 'limitSetting.lotto.pp._1TOP.payout': 3,
    //                 'limitSetting.lotto.pp._1TOP.discount': 5,
    //                 'limitSetting.lotto.pp._1TOP.max': 100000,
    //
    //                 'limitSetting.lotto.pp._1BOT.payout': 4,
    //                 'limitSetting.lotto.pp._1BOT.discount': 5,
    //                 'limitSetting.lotto.pp._1BOT.max': 100000,
    //             }
    //         };
    //
    //
    //         console.log(body)
    //         MemberModel.findOneAndUpdate({_id: betObj._id}, body)
    //             .exec(function (err, creditResponse) {
    //                 if (err) {
    //                     callbackWL(err);
    //                 }
    //                 else {
    //                     callbackWL();
    //                 }
    //             });
    //     }, (err) => {
    //
    //         return res.send(
    //             {
    //                 code: 0,
    //                 message: "success",
    //                 result: 'success'
    //             }
    //         );
    //     });
    //
    // });

});

router.get('/patchLotto-member2', function (req, res) {

    const cursor = MemberModel.find()
        .batchSize(1000)
        .lean()
        .cursor();

    cursor.on('data', function (betObj) {

        console.log(betObj.username_lower)

        let body = {

            $set: {


                // 'shareSetting.lotto.laos.parent': betObj.shareSetting.lotto.amb.parent,
                //
                // 'shareSetting.lotto.laos.min': betObj.shareSetting.lotto.amb.min,
                // 'limitSetting.lotto.laos.hour': 20,
                // 'limitSetting.lotto.laos.minute': 0,
                //
                // 'limitSetting.lotto.laos.isEnable': betObj.limitSetting.lotto.amb.isEnable,
                //
                //
                // 'limitSetting.lotto.laos.L_4TOP.payout': 1000,
                // 'limitSetting.lotto.laos.L_4TOP.discount': 10,
                // 'limitSetting.lotto.laos.L_4TOP.max': 3000,
                // //
                // 'limitSetting.lotto.laos.L_4TOD.payout': 40,
                // 'limitSetting.lotto.laos.L_4TOD.discount': 10,
                // 'limitSetting.lotto.laos.L_4TOD.max': 3000,
                // //
                // 'limitSetting.lotto.laos.L_3TOP.payout': 350,
                // 'limitSetting.lotto.laos.L_3TOP.discount': 10,
                // 'limitSetting.lotto.laos.L_3TOP.max': 3000,
                // //
                // 'limitSetting.lotto.laos.L_3TOD.payout': 25,
                // 'limitSetting.lotto.laos.L_3TOD.discount': 10,
                // 'limitSetting.lotto.laos.L_3TOD.max': 3000,
                // //
                // 'limitSetting.lotto.laos.L_2FB.payout': 15,
                // 'limitSetting.lotto.laos.L_2FB.discount': 10,
                // 'limitSetting.lotto.laos.L_2FB.max': 3000,
                //
                //
                // 'limitSetting.lotto.laos.T_4TOP.payout': betObj.limitSetting.lotto.amb._4TOP.payout,
                // 'limitSetting.lotto.laos.T_4TOP.discount': betObj.limitSetting.lotto.amb._4TOP.discount,
                // 'limitSetting.lotto.laos.T_4TOP.max': betObj.limitSetting.lotto.amb._4TOP.max,
                // //
                // 'limitSetting.lotto.laos.T_4TOD.payout': betObj.limitSetting.lotto.amb._4TOD.payout,
                // 'limitSetting.lotto.laos.T_4TOD.discount': betObj.limitSetting.lotto.amb._4TOD.discount,
                // 'limitSetting.lotto.laos.T_4TOD.max': betObj.limitSetting.lotto.amb._4TOD.max,
                // //
                // 'limitSetting.lotto.laos.T_3TOP.payout': betObj.limitSetting.lotto.amb._3TOP.payout,
                // 'limitSetting.lotto.laos.T_3TOP.discount': betObj.limitSetting.lotto.amb._3TOP.discount,
                // 'limitSetting.lotto.laos.T_3TOP.max': betObj.limitSetting.lotto.amb._3TOP.max,
                // //
                // 'limitSetting.lotto.laos.T_3TOD.payout': betObj.limitSetting.lotto.amb._3TOD.payout,
                // 'limitSetting.lotto.laos.T_3TOD.discount': betObj.limitSetting.lotto.amb._3TOD.discount,
                // 'limitSetting.lotto.laos.T_3TOD.max': betObj.limitSetting.lotto.amb._3TOD.max,
                //
                // 'limitSetting.lotto.laos.T_2TOP.payout': betObj.limitSetting.lotto.amb._2TOP.payout,
                // 'limitSetting.lotto.laos.T_2TOP.discount': betObj.limitSetting.lotto.amb._2TOP.discount,
                // 'limitSetting.lotto.laos.T_2TOP.max': betObj.limitSetting.lotto.amb._2TOP.max,
                // //
                // 'limitSetting.lotto.laos.T_2TOD.payout': betObj.limitSetting.lotto.amb._2TOD.payout,
                // 'limitSetting.lotto.laos.T_2TOD.discount': betObj.limitSetting.lotto.amb._2TOD.discount,
                // 'limitSetting.lotto.laos.T_2TOD.max': betObj.limitSetting.lotto.amb._2TOD.max,
                // //
                // 'limitSetting.lotto.laos.T_2BOT.payout': betObj.limitSetting.lotto.amb._2BOT.payout,
                // 'limitSetting.lotto.laos.T_2BOT.discount': betObj.limitSetting.lotto.amb._2BOT.discount,
                // 'limitSetting.lotto.laos.T_2BOT.max': betObj.limitSetting.lotto.amb._2BOT.max,
                //
                // 'limitSetting.lotto.laos.T_1TOP.payout': betObj.limitSetting.lotto.amb._1TOP.payout,
                // 'limitSetting.lotto.laos.T_1TOP.discount': betObj.limitSetting.lotto.amb._1TOP.discount,
                // 'limitSetting.lotto.laos.T_1TOP.max': betObj.limitSetting.lotto.amb._1TOP.max,
                // //
                // 'limitSetting.lotto.laos.T_1BOT.payout': betObj.limitSetting.lotto.amb._1BOT.payout,
                // 'limitSetting.lotto.laos.T_1BOT.discount': betObj.limitSetting.lotto.amb._1BOT.discount,
                // 'limitSetting.lotto.laos.T_1BOT.max': betObj.limitSetting.lotto.amb._1BOT.max


                // 'shareSetting.lotto.pp.parent': betObj.shareSetting.lotto.amb.parent,
                //
                // 'limitSetting.lotto.pp.isEnable': betObj.limitSetting.lotto.amb.isEnable,
                // 'limitSetting.lotto.pp.hour': 0,
                // 'limitSetting.lotto.pp.minute': 0,
                //
                // 'limitSetting.lotto.pp._6TOP.payout': betObj.limitSetting.lotto.amb._6TOP.payout,
                // 'limitSetting.lotto.pp._6TOP.discount': betObj.limitSetting.lotto.amb._6TOP.discount,
                // 'limitSetting.lotto.pp._6TOP.max': betObj.limitSetting.lotto.amb._6TOP.max,
                //
                // 'limitSetting.lotto.pp._5TOP.payout': betObj.limitSetting.lotto.amb._5TOP.payout,
                // 'limitSetting.lotto.pp._5TOP.discount': betObj.limitSetting.lotto.amb._5TOP.discount,
                // 'limitSetting.lotto.pp._5TOP.max': betObj.limitSetting.lotto.amb._5TOP.max,
                //
                // 'limitSetting.lotto.pp._4TOP.payout': betObj.limitSetting.lotto.amb._4TOP.payout,
                // 'limitSetting.lotto.pp._4TOP.discount': betObj.limitSetting.lotto.amb._4TOP.discount,
                // 'limitSetting.lotto.pp._4TOP.max': betObj.limitSetting.lotto.amb._4TOP.max,
                //
                // 'limitSetting.lotto.pp._4TOD.payout': betObj.limitSetting.lotto.amb._4TOD.payout,
                // 'limitSetting.lotto.pp._4TOD.discount': betObj.limitSetting.lotto.amb._4TOD.discount,
                // 'limitSetting.lotto.pp._4TOD.max': betObj.limitSetting.lotto.amb._4TOD.max,
                //
                // 'limitSetting.lotto.pp._3TOP.payout': betObj.limitSetting.lotto.amb._3TOP.payout,
                // 'limitSetting.lotto.pp._3TOP.discount': betObj.limitSetting.lotto.amb._3TOP.discount,
                // 'limitSetting.lotto.pp._3TOP.max': betObj.limitSetting.lotto.amb._3TOP.max,
                //
                // 'limitSetting.lotto.pp._3TOD.payout': betObj.limitSetting.lotto.amb._3TOD.payout,
                // 'limitSetting.lotto.pp._3TOD.discount': betObj.limitSetting.lotto.amb._3TOD.discount,
                // 'limitSetting.lotto.pp._3TOD.max': betObj.limitSetting.lotto.amb._3TOD.max,
                //
                // 'limitSetting.lotto.pp._3BOT.payout': 0,
                // 'limitSetting.lotto.pp._3BOT.discount': 0,
                // 'limitSetting.lotto.pp._3BOT.max': 0,
                //
                // 'limitSetting.lotto.pp._3TOP_OE.payout': 0,
                // 'limitSetting.lotto.pp._3TOP_OE.discount': 0,
                // 'limitSetting.lotto.pp._3TOP_OE.max': 0,
                //
                // 'limitSetting.lotto.pp._3TOP_OU.payout': 0,
                // 'limitSetting.lotto.pp._3TOP_OU.discount': 0,
                // 'limitSetting.lotto.pp._3TOP_OU.max': 0,
                //
                // 'limitSetting.lotto.pp._2TOP.payout': betObj.limitSetting.lotto.amb._2TOP.payout,
                // 'limitSetting.lotto.pp._2TOP.discount': betObj.limitSetting.lotto.amb._2TOP.discount,
                // 'limitSetting.lotto.pp._2TOP.max': betObj.limitSetting.lotto.amb._2TOP.max,
                //
                // 'limitSetting.lotto.pp._2TOD.payout': betObj.limitSetting.lotto.amb._2TOD.payout,
                // 'limitSetting.lotto.pp._2TOD.discount': betObj.limitSetting.lotto.amb._2TOD.discount,
                // 'limitSetting.lotto.pp._2TOD.max': betObj.limitSetting.lotto.amb._2TOD.max,
                //
                // 'limitSetting.lotto.pp._2BOT.payout': betObj.limitSetting.lotto.amb._2BOT.payout,
                // 'limitSetting.lotto.pp._2BOT.discount': betObj.limitSetting.lotto.amb._2BOT.discount,
                // 'limitSetting.lotto.pp._2BOT.max': betObj.limitSetting.lotto.amb._2BOT.max,
                //
                // 'limitSetting.lotto.pp._2TOP_OE.payout': 0,
                // 'limitSetting.lotto.pp._2TOP_OE.discount': 0,
                // 'limitSetting.lotto.pp._2TOP_OE.max': 0,
                //
                // 'limitSetting.lotto.pp._2TOP_OU.payout': 0,
                // 'limitSetting.lotto.pp._2TOP_OU.discount': 0,
                // 'limitSetting.lotto.pp._2TOP_OU.max': 0,
                //
                // 'limitSetting.lotto.pp._2BOT_OE.payout': 0,
                // 'limitSetting.lotto.pp._2BOT_OE.discount': 0,
                // 'limitSetting.lotto.pp._2BOT_OE.max': 0,
                //
                // 'limitSetting.lotto.pp._2BOT_OU.payout': 0,
                // 'limitSetting.lotto.pp._2BOT_OU.discount': 0,
                // 'limitSetting.lotto.pp._2BOT_OU.max': 0,
                //
                // 'limitSetting.lotto.pp._1TOP.payout': betObj.limitSetting.lotto.amb._1TOP.payout,
                // 'limitSetting.lotto.pp._1TOP.discount': betObj.limitSetting.lotto.amb._1TOP.discount,
                // 'limitSetting.lotto.pp._1TOP.max': betObj.limitSetting.lotto.amb._1TOP.max,
                //
                // 'limitSetting.lotto.pp._1BOT.payout': betObj.limitSetting.lotto.amb._1BOT.payout,
                // 'limitSetting.lotto.pp._1BOT.discount': betObj.limitSetting.lotto.amb._1BOT.discount,
                // 'limitSetting.lotto.pp._1BOT.max': betObj.limitSetting.lotto.amb._1BOT.max,


                // 'shareSetting.lotto.pp.parent': betObj.shareSetting.lotto.amb.parent,
                // 'limitSetting.lotto.pp.isEnable': true,//betObj.limitSetting.lotto.pp.isEnable,
                // 'limitSetting.lotto.pp.hour': 0,
                // 'limitSetting.lotto.pp.minute': 0,

                'limitSetting.lotto.pp._6TOP.payout': 50000,
                'limitSetting.lotto.pp._6TOP.discount': 0,
                'limitSetting.lotto.pp._6TOP.max': 1000,

                'limitSetting.lotto.pp._5TOP.payout': 25000,
                'limitSetting.lotto.pp._5TOP.discount': 0,
                'limitSetting.lotto.pp._5TOP.max': 1000,

                'limitSetting.lotto.pp._4TOP.payout': 5000,
                'limitSetting.lotto.pp._4TOP.discount': 0,
                'limitSetting.lotto.pp._4TOP.max': 1000,

                'limitSetting.lotto.pp._4TOD.payout': 225,
                'limitSetting.lotto.pp._4TOD.discount': 0,
                'limitSetting.lotto.pp._4TOD.max': 10000,

                'limitSetting.lotto.pp._3TOP.payout': 800,
                'limitSetting.lotto.pp._3TOP.discount': 0,
                'limitSetting.lotto.pp._3TOP.max': 2000,

                'limitSetting.lotto.pp._3TOD.payout': 130,
                'limitSetting.lotto.pp._3TOD.discount': 0,
                'limitSetting.lotto.pp._3TOD.max': 10000,

                'limitSetting.lotto.pp._2TOP.payout': 90,
                'limitSetting.lotto.pp._2TOP.discount': 0,
                'limitSetting.lotto.pp._2TOP.max': 10000,

                'limitSetting.lotto.pp._2TOD.payout': 12,
                'limitSetting.lotto.pp._2TOD.discount': 0,
                'limitSetting.lotto.pp._2TOD.max': 100000,

                'limitSetting.lotto.pp._2BOT.payout': 90,
                'limitSetting.lotto.pp._2BOT.discount': 0,
                'limitSetting.lotto.pp._2BOT.max': 10000,

                'limitSetting.lotto.pp._1TOP.payout': 3,
                'limitSetting.lotto.pp._1TOP.discount': 0,
                'limitSetting.lotto.pp._1TOP.max': 100000,

                'limitSetting.lotto.pp._1BOT.payout': 4,
                'limitSetting.lotto.pp._1BOT.discount': 0,
                'limitSetting.lotto.pp._1BOT.max': 100000,
            }
        };


        // console.log(body)
        MemberModel.update({_id: betObj._id}, body)
            .exec(function (err, creditResponse) {

                console.log(creditResponse)
                if (err) {
                    // callbackWL(err);
                }
                else {
                    // callbackWL();
                }
            });


    });


    cursor.on('end', function () {
        console.log('Done!');
        return res.send({code: 0, message: "success"});
    });

});

router.get('/patchLotto-agent', function (req, res) {

    let body = {
        $set: {

            // 'shareSetting.lotto.pp.parent': 0,
            // 'shareSetting.lotto.pp.own': 0,
            // 'shareSetting.lotto.pp.remaining': 0,
            // 'shareSetting.lotto.pp.min': 0,
            //
            // 'limitSetting.lotto.pp.isEnable': false,
            // 'limitSetting.lotto.pp.hour': 0,
            // 'limitSetting.lotto.pp.minute': 0,
            //
            // 'shareSetting.lotto.laos.parent': 0,
            // 'shareSetting.lotto.laos.own': 0,
            // 'shareSetting.lotto.laos.remaining': 0,
            // 'shareSetting.lotto.laos.min': 0,
            // 'limitSetting.lotto.laos.hour': 20,
            // 'limitSetting.lotto.laos.minute': 0,
            //
            // 'limitSetting.lotto.laos.isEnable': true,
            //
            //
            //
            // 'limitSetting.lotto.laos.L_4TOP.payout': 1000,
            // 'limitSetting.lotto.laos.L_4TOP.discount': 10,
            // 'limitSetting.lotto.laos.L_4TOP.max': 3000,
            // //
            // 'limitSetting.lotto.laos.L_4TOD.payout': 40,
            // 'limitSetting.lotto.laos.L_4TOD.discount': 10,
            // 'limitSetting.lotto.laos.L_4TOD.max': 3000,
            // //
            // 'limitSetting.lotto.laos.L_3TOP.payout': 350,
            // 'limitSetting.lotto.laos.L_3TOP.discount': 10,
            // 'limitSetting.lotto.laos.L_3TOP.max': 3000,
            // //
            // 'limitSetting.lotto.laos.L_3TOD.payout': 25,
            // 'limitSetting.lotto.laos.L_3TOD.discount': 10,
            // 'limitSetting.lotto.laos.L_3TOD.max': 3000,
            // //
            // 'limitSetting.lotto.laos.L_2FB.payout': 15,
            // 'limitSetting.lotto.laos.L_2FB.discount': 10,
            // 'limitSetting.lotto.laos.L_2FB.max': 3000,
            // //
            //
            //
            // 'limitSetting.lotto.laos.T_4TOP.payout': 5000,
            // 'limitSetting.lotto.laos.T_4TOP.discount': 33,
            // 'limitSetting.lotto.laos.T_4TOP.max': 5000,
            // //
            // 'limitSetting.lotto.laos.T_4TOD.payout': 225,
            // 'limitSetting.lotto.laos.T_4TOD.discount': 33,
            // 'limitSetting.lotto.laos.T_4TOD.max': 7000,
            // //
            // 'limitSetting.lotto.laos.T_3TOP.payout': 550,
            // 'limitSetting.lotto.laos.T_3TOP.discount': 33,
            // 'limitSetting.lotto.laos.T_3TOP.max': 5000,
            // //
            // 'limitSetting.lotto.laos.T_3TOD.payout': 100,
            // 'limitSetting.lotto.laos.T_3TOD.discount': 33,
            // 'limitSetting.lotto.laos.T_3TOD.max': 20000,
            //
            // 'limitSetting.lotto.laos.T_2TOP.payout': 70,
            // 'limitSetting.lotto.laos.T_2TOP.discount': 28,
            // 'limitSetting.lotto.laos.T_2TOP.max': 20000,
            // //
            // 'limitSetting.lotto.laos.T_2TOD.payout': 12,
            // 'limitSetting.lotto.laos.T_2TOD.discount': 28,
            // 'limitSetting.lotto.laos.T_2TOD.max': 20000,
            // //
            // 'limitSetting.lotto.laos.T_2BOT.payout': 70,
            // 'limitSetting.lotto.laos.T_2BOT.discount': 28,
            // 'limitSetting.lotto.laos.T_2BOT.max': 20000,
            //
            // 'limitSetting.lotto.laos.T_1TOP.payout': 3,
            // 'limitSetting.lotto.laos.T_1TOP.discount': 12,
            // 'limitSetting.lotto.laos.T_1TOP.max': 200000,
            // //
            // 'limitSetting.lotto.laos.T_1BOT.payout': 4,
            // 'limitSetting.lotto.laos.T_1BOT.discount': 12,
            // 'limitSetting.lotto.laos.T_1BOT.max': 200000,
            //
            //
            //
            'limitSetting.lotto.pp._6TOP.payout': 50000,
            'limitSetting.lotto.pp._6TOP.discount': 0,
            'limitSetting.lotto.pp._6TOP.max': 1000,

            'limitSetting.lotto.pp._5TOP.payout': 25000,
            'limitSetting.lotto.pp._5TOP.discount': 0,
            'limitSetting.lotto.pp._5TOP.max': 1000,

            'limitSetting.lotto.pp._4TOP.payout': 5000,
            'limitSetting.lotto.pp._4TOP.discount': 0,
            'limitSetting.lotto.pp._4TOP.max': 1000,

            'limitSetting.lotto.pp._4TOD.payout': 225,
            'limitSetting.lotto.pp._4TOD.discount': 0,
            'limitSetting.lotto.pp._4TOD.max': 10000,

            'limitSetting.lotto.pp._3TOP.payout': 800,
            'limitSetting.lotto.pp._3TOP.discount': 0,
            'limitSetting.lotto.pp._3TOP.max': 2000,

            'limitSetting.lotto.pp._3TOD.payout': 130,
            'limitSetting.lotto.pp._3TOD.discount': 0,
            'limitSetting.lotto.pp._3TOD.max': 10000,

            'limitSetting.lotto.pp._2TOP.payout': 90,
            'limitSetting.lotto.pp._2TOP.discount': 0,
            'limitSetting.lotto.pp._2TOP.max': 10000,

            'limitSetting.lotto.pp._2TOD.payout': 12,
            'limitSetting.lotto.pp._2TOD.discount': 0,
            'limitSetting.lotto.pp._2TOD.max': 100000,

            'limitSetting.lotto.pp._2BOT.payout': 90,
            'limitSetting.lotto.pp._2BOT.discount': 0,
            'limitSetting.lotto.pp._2BOT.max': 10000,

            'limitSetting.lotto.pp._1TOP.payout': 3,
            'limitSetting.lotto.pp._1TOP.discount': 0,
            'limitSetting.lotto.pp._1TOP.max': 100000,

            'limitSetting.lotto.pp._1BOT.payout': 4,
            'limitSetting.lotto.pp._1BOT.discount': 0,
            'limitSetting.lotto.pp._1BOT.max': 100000,
        }
    }

    AgentGroupModel.update({}, body, {multi: true})
        .exec(function (err, creditResponse) {
            if (err) {
                return res.send(
                    {
                        code: 999,
                        message: "error"
                    }
                );
            } else {

                console.log(creditResponse)
                return res.send(
                    {
                        code: 0,
                        message: "success",
                        result: 'success'
                    }
                );
            }
        });

    // const cursor = AgentGroupModel.find(/*{type: {$in: ['SUPER_ADMIN', 'COMPANY']}}*/)
    //     .batchSize(1000)
    //     .lean()
    //     .cursor();
    //
    // cursor.on('data', function (betObj) {
    //
    //     console.log(betObj.name_lower)
    //
    //     let body = {
    //
    //         $set: {
    //
    //
    //             // 'shareSetting.lotto.laos.parent': betObj.shareSetting.lotto.amb.parent,
    //             // 'shareSetting.lotto.laos.own': betObj.shareSetting.lotto.amb.own,
    //             // 'shareSetting.lotto.laos.remaining': betObj.shareSetting.lotto.amb.remaining,
    //             // 'shareSetting.lotto.laos.min': betObj.shareSetting.lotto.amb.min,
    //             // 'limitSetting.lotto.laos.hour': 20,
    //             // 'limitSetting.lotto.laos.minute': 0,
    //             //
    //             // 'limitSetting.lotto.laos.isEnable': betObj.limitSetting.lotto.amb.isEnable,
    //             //
    //             //
    //             // 'limitSetting.lotto.laos.L_4TOP.payout': 1000,
    //             // 'limitSetting.lotto.laos.L_4TOP.discount': 10,
    //             // 'limitSetting.lotto.laos.L_4TOP.max': 3000,
    //             // //
    //             // 'limitSetting.lotto.laos.L_4TOD.payout': 40,
    //             // 'limitSetting.lotto.laos.L_4TOD.discount': 10,
    //             // 'limitSetting.lotto.laos.L_4TOD.max': 3000,
    //             // //
    //             // 'limitSetting.lotto.laos.L_3TOP.payout': 350,
    //             // 'limitSetting.lotto.laos.L_3TOP.discount': 10,
    //             // 'limitSetting.lotto.laos.L_3TOP.max': 3000,
    //             // //
    //             // 'limitSetting.lotto.laos.L_3TOD.payout': 25,
    //             // 'limitSetting.lotto.laos.L_3TOD.discount': 10,
    //             // 'limitSetting.lotto.laos.L_3TOD.max': 3000,
    //             // //
    //             // 'limitSetting.lotto.laos.L_2FB.payout': 15,
    //             // 'limitSetting.lotto.laos.L_2FB.discount': 10,
    //             // 'limitSetting.lotto.laos.L_2FB.max': 3000,
    //             //
    //             //
    //             // 'limitSetting.lotto.laos.T_4TOP.payout': betObj.limitSetting.lotto.amb._4TOP.payout,
    //             // 'limitSetting.lotto.laos.T_4TOP.discount': betObj.limitSetting.lotto.amb._4TOP.discount,
    //             // 'limitSetting.lotto.laos.T_4TOP.max': betObj.limitSetting.lotto.amb._4TOP.max,
    //             // //
    //             // 'limitSetting.lotto.laos.T_4TOD.payout': betObj.limitSetting.lotto.amb._4TOD.payout,
    //             // 'limitSetting.lotto.laos.T_4TOD.discount': betObj.limitSetting.lotto.amb._4TOD.discount,
    //             // 'limitSetting.lotto.laos.T_4TOD.max': betObj.limitSetting.lotto.amb._4TOD.max,
    //             // //
    //             // 'limitSetting.lotto.laos.T_3TOP.payout': betObj.limitSetting.lotto.amb._3TOP.payout,
    //             // 'limitSetting.lotto.laos.T_3TOP.discount': betObj.limitSetting.lotto.amb._3TOP.discount,
    //             // 'limitSetting.lotto.laos.T_3TOP.max': betObj.limitSetting.lotto.amb._3TOP.max,
    //             // //
    //             // 'limitSetting.lotto.laos.T_3TOD.payout': betObj.limitSetting.lotto.amb._3TOD.payout,
    //             // 'limitSetting.lotto.laos.T_3TOD.discount': betObj.limitSetting.lotto.amb._3TOD.discount,
    //             // 'limitSetting.lotto.laos.T_3TOD.max': betObj.limitSetting.lotto.amb._3TOD.max,
    //             //
    //             // 'limitSetting.lotto.laos.T_2TOP.payout': betObj.limitSetting.lotto.amb._2TOP.payout,
    //             // 'limitSetting.lotto.laos.T_2TOP.discount': betObj.limitSetting.lotto.amb._2TOP.discount,
    //             // 'limitSetting.lotto.laos.T_2TOP.max': betObj.limitSetting.lotto.amb._2TOP.max,
    //             // //
    //             // 'limitSetting.lotto.laos.T_2TOD.payout': betObj.limitSetting.lotto.amb._2TOD.payout,
    //             // 'limitSetting.lotto.laos.T_2TOD.discount': betObj.limitSetting.lotto.amb._2TOD.discount,
    //             // 'limitSetting.lotto.laos.T_2TOD.max': betObj.limitSetting.lotto.amb._2TOD.max,
    //             // //
    //             // 'limitSetting.lotto.laos.T_2BOT.payout': betObj.limitSetting.lotto.amb._2BOT.payout,
    //             // 'limitSetting.lotto.laos.T_2BOT.discount': betObj.limitSetting.lotto.amb._2BOT.discount,
    //             // 'limitSetting.lotto.laos.T_2BOT.max': betObj.limitSetting.lotto.amb._2BOT.max,
    //             //
    //             // 'limitSetting.lotto.laos.T_1TOP.payout': betObj.limitSetting.lotto.amb._1TOP.payout,
    //             // 'limitSetting.lotto.laos.T_1TOP.discount': betObj.limitSetting.lotto.amb._1TOP.discount,
    //             // 'limitSetting.lotto.laos.T_1TOP.max': betObj.limitSetting.lotto.amb._1TOP.max,
    //             // //
    //             // 'limitSetting.lotto.laos.T_1BOT.payout': betObj.limitSetting.lotto.amb._1BOT.payout,
    //             // 'limitSetting.lotto.laos.T_1BOT.discount': betObj.limitSetting.lotto.amb._1BOT.discount,
    //             // 'limitSetting.lotto.laos.T_1BOT.max': betObj.limitSetting.lotto.amb._1BOT.max,
    //
    //             // 'shareSetting.lotto.pp.parent': betObj.shareSetting.lotto.amb.parent,
    //             // 'shareSetting.lotto.pp.own': betObj.shareSetting.lotto.amb.own,
    //             // 'shareSetting.lotto.pp.remaining': betObj.shareSetting.lotto.amb.remaining,
    //             //
    //             'limitSetting.lotto.pp.isEnable': true,//betObj.limitSetting.lotto.amb.isEnable,
    //             'limitSetting.lotto.pp.hour': 0,
    //             'limitSetting.lotto.pp.minute': 0,
    //
    //             //
    //             // 'limitSetting.lotto.pp._6TOP.payout': betObj.limitSetting.lotto.amb._6TOP.payout,
    //             // 'limitSetting.lotto.pp._6TOP.discount': betObj.limitSetting.lotto.amb._6TOP.discount,
    //             // 'limitSetting.lotto.pp._6TOP.max': betObj.limitSetting.lotto.amb._6TOP.max,
    //             //
    //             // 'limitSetting.lotto.pp._5TOP.payout': betObj.limitSetting.lotto.amb._5TOP.payout,
    //             // 'limitSetting.lotto.pp._5TOP.discount': betObj.limitSetting.lotto.amb._5TOP.discount,
    //             // 'limitSetting.lotto.pp._5TOP.max': betObj.limitSetting.lotto.amb._5TOP.max,
    //             //
    //             // 'limitSetting.lotto.pp._4TOP.payout': betObj.limitSetting.lotto.amb._4TOP.payout,
    //             // 'limitSetting.lotto.pp._4TOP.discount': betObj.limitSetting.lotto.amb._4TOP.discount,
    //             // 'limitSetting.lotto.pp._4TOP.max': betObj.limitSetting.lotto.amb._4TOP.max,
    //             //
    //             // 'limitSetting.lotto.pp._4TOD.payout': betObj.limitSetting.lotto.amb._4TOD.payout,
    //             // 'limitSetting.lotto.pp._4TOD.discount': betObj.limitSetting.lotto.amb._4TOD.discount,
    //             // 'limitSetting.lotto.pp._4TOD.max': betObj.limitSetting.lotto.amb._4TOD.max,
    //             //
    //             // 'limitSetting.lotto.pp._3TOP.payout': betObj.limitSetting.lotto.amb._3TOP.payout,
    //             // 'limitSetting.lotto.pp._3TOP.discount': betObj.limitSetting.lotto.amb._3TOP.discount,
    //             // 'limitSetting.lotto.pp._3TOP.max': betObj.limitSetting.lotto.amb._3TOP.max,
    //             //
    //             // 'limitSetting.lotto.pp._3TOD.payout': betObj.limitSetting.lotto.amb._3TOD.payout,
    //             // 'limitSetting.lotto.pp._3TOD.discount': betObj.limitSetting.lotto.amb._3TOD.discount,
    //             // 'limitSetting.lotto.pp._3TOD.max': betObj.limitSetting.lotto.amb._3TOD.max,
    //             //
    //             // 'limitSetting.lotto.pp._3BOT.payout': 0,
    //             // 'limitSetting.lotto.pp._3BOT.discount': 0,
    //             // 'limitSetting.lotto.pp._3BOT.max': 0,
    //             //
    //             // 'limitSetting.lotto.pp._3TOP_OE.payout': 0,
    //             // 'limitSetting.lotto.pp._3TOP_OE.discount': 0,
    //             // 'limitSetting.lotto.pp._3TOP_OE.max': 0,
    //             //
    //             // 'limitSetting.lotto.pp._3TOP_OU.payout': 0,
    //             // 'limitSetting.lotto.pp._3TOP_OU.discount': 0,
    //             // 'limitSetting.lotto.pp._3TOP_OU.max': 0,
    //             //
    //             // 'limitSetting.lotto.pp._2TOP.payout': betObj.limitSetting.lotto.amb._2TOP.payout,
    //             // 'limitSetting.lotto.pp._2TOP.discount': betObj.limitSetting.lotto.amb._2TOP.discount,
    //             // 'limitSetting.lotto.pp._2TOP.max': betObj.limitSetting.lotto.amb._2TOP.max,
    //             //
    //             // 'limitSetting.lotto.pp._2TOD.payout': betObj.limitSetting.lotto.amb._2TOD.payout,
    //             // 'limitSetting.lotto.pp._2TOD.discount': betObj.limitSetting.lotto.amb._2TOD.discount,
    //             // 'limitSetting.lotto.pp._2TOD.max': betObj.limitSetting.lotto.amb._2TOD.max,
    //             //
    //             // 'limitSetting.lotto.pp._2BOT.payout': betObj.limitSetting.lotto.amb._2BOT.payout,
    //             // 'limitSetting.lotto.pp._2BOT.discount': betObj.limitSetting.lotto.amb._2BOT.discount,
    //             // 'limitSetting.lotto.pp._2BOT.max': betObj.limitSetting.lotto.amb._2BOT.max,
    //             //
    //             // 'limitSetting.lotto.pp._2TOP_OE.payout': 0,
    //             // 'limitSetting.lotto.pp._2TOP_OE.discount': 0,
    //             // 'limitSetting.lotto.pp._2TOP_OE.max': 0,
    //             //
    //             // 'limitSetting.lotto.pp._2TOP_OU.payout': 0,
    //             // 'limitSetting.lotto.pp._2TOP_OU.discount': 0,
    //             // 'limitSetting.lotto.pp._2TOP_OU.max': 0,
    //             //
    //             // 'limitSetting.lotto.pp._2BOT_OE.payout': 0,
    //             // 'limitSetting.lotto.pp._2BOT_OE.discount': 0,
    //             // 'limitSetting.lotto.pp._2BOT_OE.max': 0,
    //             //
    //             // 'limitSetting.lotto.pp._2BOT_OU.payout': 0,
    //             // 'limitSetting.lotto.pp._2BOT_OU.discount': 0,
    //             // 'limitSetting.lotto.pp._2BOT_OU.max': 0,
    //             //
    //             // 'limitSetting.lotto.pp._1TOP.payout': betObj.limitSetting.lotto.amb._1TOP.payout,
    //             // 'limitSetting.lotto.pp._1TOP.discount': betObj.limitSetting.lotto.amb._1TOP.discount,
    //             // 'limitSetting.lotto.pp._1TOP.max': betObj.limitSetting.lotto.amb._1TOP.max,
    //             //
    //             // 'limitSetting.lotto.pp._1BOT.payout': betObj.limitSetting.lotto.amb._1BOT.payout,
    //             // 'limitSetting.lotto.pp._1BOT.discount': betObj.limitSetting.lotto.amb._1BOT.discount,
    //             // 'limitSetting.lotto.pp._1BOT.max': betObj.limitSetting.lotto.amb._1BOT.max,
    //
    //
    //             //PP
    //
    //             'limitSetting.lotto.pp._6TOP.payout': 50000,
    //             'limitSetting.lotto.pp._6TOP.discount': 0,
    //             'limitSetting.lotto.pp._6TOP.max': 1000,
    //
    //             'limitSetting.lotto.pp._5TOP.payout': 25000,
    //             'limitSetting.lotto.pp._5TOP.discount': 0,
    //             'limitSetting.lotto.pp._5TOP.max': 1000,
    //
    //             'limitSetting.lotto.pp._4TOP.payout': 5000,
    //             'limitSetting.lotto.pp._4TOP.discount': 0,
    //             'limitSetting.lotto.pp._4TOP.max': 1000,
    //
    //             'limitSetting.lotto.pp._4TOD.payout': 225,
    //             'limitSetting.lotto.pp._4TOD.discount': 0,
    //             'limitSetting.lotto.pp._4TOD.max': 10000,
    //
    //             'limitSetting.lotto.pp._3TOP.payout': 800,
    //             'limitSetting.lotto.pp._3TOP.discount': 0,
    //             'limitSetting.lotto.pp._3TOP.max': 2000,
    //
    //             'limitSetting.lotto.pp._3TOD.payout': 130,
    //             'limitSetting.lotto.pp._3TOD.discount': 0,
    //             'limitSetting.lotto.pp._3TOD.max': 10000,
    //
    //             'limitSetting.lotto.pp._2TOP.payout': 90,
    //             'limitSetting.lotto.pp._2TOP.discount': 0,
    //             'limitSetting.lotto.pp._2TOP.max': 10000,
    //
    //             'limitSetting.lotto.pp._2TOD.payout': 12,
    //             'limitSetting.lotto.pp._2TOD.discount': 0,
    //             'limitSetting.lotto.pp._2TOD.max': 100000,
    //
    //             'limitSetting.lotto.pp._2BOT.payout': 90,
    //             'limitSetting.lotto.pp._2BOT.discount': 0,
    //             'limitSetting.lotto.pp._2BOT.max': 10000,
    //
    //             'limitSetting.lotto.pp._1TOP.payout': 3,
    //             'limitSetting.lotto.pp._1TOP.discount': 0,
    //             'limitSetting.lotto.pp._1TOP.max': 100000,
    //
    //             'limitSetting.lotto.pp._1BOT.payout': 4,
    //             'limitSetting.lotto.pp._1BOT.discount': 0,
    //             'limitSetting.lotto.pp._1BOT.max': 100000,
    //         }
    //     };
    //
    //
    //     // console.log(body)
    //     AgentGroupModel.update({_id: betObj._id}, body)
    //         .exec(function (err, creditResponse) {
    //
    //             console.log(creditResponse)
    //             if (err) {
    //                 // callbackWL(err);
    //             }
    //             else {
    //                 // callbackWL();
    //             }
    //         });
    //
    //
    // });
    //
    //
    // cursor.on('end', function () {
    //     console.log('Done!');
    //     return res.send({code: 0, message: "success"});
    // });

});

router.get('/patchLotto-agent2', function (req, res) {

    const cursor = MemberModel.find({'limitSetting.lotto.amb._2BOT.discount': 28})
        .batchSize(1000)
        .lean()
        .cursor();

    cursor.on('data', function (betObj) {

        console.log(betObj.username_lower)

        let body = {

            $set: {


                // 'limitSetting.lotto.amb._2TOP.discount': 27,
                // 'limitSetting.lotto.amb._2TOD.discount': 27,
                'limitSetting.lotto.amb._2BOT.discount': 27,
            }
        };

        MemberModel.findOneAndUpdate({_id: betObj._id}, body)
            .exec(function (err, creditResponse) {
                if (err) {
                    // callbackWL(err);
                }
                else {
                    // callbackWL();
                }
            });


    });


    cursor.on('end', function () {
        console.log('Done!');
        return res.send({code: 0, message: "success"});
    });

});

router.get('/patchLotto-agent-laos-limit', function (req, res) {


    let limit = 0;
    AgentGroupModel.find({}).exec(function (err, response) {


        console.log('length : ', response.length)

        async.each(response, (transaction, callbackWL) => {

            let body = {
                agent: transaction._id,
                name: transaction.name_lower,
                hour: 20,
                min: 0,
                isEnableHotNumber: false,
                po: {
                    L_4TOP: {
                        limit: limit
                    },
                    L_4TOD: {
                        limit: limit
                    },
                    L_3TOP: {
                        limit: limit
                    },
                    L_3TOD: {
                        limit: limit
                    },
                    L_2FB: {
                        limit: limit
                    },
                    T_4TOP: {
                        limit: limit
                    },
                    T_4TOD: {
                        limit: limit
                    },
                    T_3TOP: {
                        limit: limit
                    },
                    T_3TOD: {
                        limit: limit
                    },
                    T_2TOP: {
                        limit: limit
                    },
                    T_2TOD: {
                        limit: limit
                    },
                    T_2BOT: {
                        limit: limit
                    },
                    T_1TOP: {
                        limit: limit
                    },
                    T_1BOT: {
                        limit: limit
                    }
                }
            };

            let model = new AgentLottoLaosLimitModel(body);
            model.save(body, function (err, lottoResponse) {
                if (err) {
                    callbackWL(err);
                }
                else {
                    console.log(lottoResponse)
                    AgentGroupModel.update({_id: transaction._id}, {$set: {ambLottoLaos: lottoResponse._id}})
                        .exec(function (err, creditResponse) {
                            if (err) {
                                console.log(err)
                                callbackWL(err);
                            } else {
                                callbackWL();
                            }
                        });
                }
            });
        }, (err) => {

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: 'success'
                }
            );
        });

    });

});

router.get('/patchLotto-agent-pp-limit', function (req, res) {


    let limit = 0;
    AgentGroupModel.find({}).exec(function (err, response) {


        console.log('length : ', response.length)

        async.each(response, (transaction, callbackWL) => {

            let body = {
                agent: transaction._id,
                name: transaction.name_lower,
                hour: 15,
                min: 20,
                isEnableHotNumber: false,
                po: {
                    _1TOP: {
                        limit: limit
                    },
                    _1BOT: {
                        limit: limit
                    },
                    _2TOP: {
                        limit: limit
                    },
                    _2BOT: {
                        limit: limit
                    },
                    _2TOD: {
                        limit: limit
                    },
                    _2TOP_OE: {
                        limit: limit
                    },
                    _2TOP_OU: {
                        limit: limit
                    },
                    _2BOT_OE: {
                        limit: limit
                    },
                    _2BOT_OU: {
                        limit: limit
                    },
                    _3TOP: {
                        limit: limit
                    },
                    _3BOT: {
                        limit: limit
                    },
                    _3TOD: {
                        limit: limit
                    },
                    _3TOP_OE: {
                        limit: limit
                    },
                    _3TOP_OU: {
                        limit: limit
                    },
                    _4TOP: {
                        limit: limit
                    },
                    _4TOD: {
                        limit: limit
                    },
                    _5TOP: {
                        limit: limit
                    },
                    _6TOP: {
                        limit: limit
                    }
                }
            };


            let model = new AgentLottoPPLimitModel(body);
            model.save(body, function (err, lottoResponse) {
                if (err) {
                    callbackWL(err);
                }
                else {
                    console.log(lottoResponse)
                    AgentGroupModel.update({_id: transaction._id}, {$set: {ambLottoPP: lottoResponse._id}})
                        .exec(function (err, creditResponse) {
                            if (err) {
                                console.log(err)
                                callbackWL(err);
                            } else {
                                callbackWL();
                            }
                        });
                }
            });
        }, (err) => {

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: 'success'
                }
            );
        });

    });

});

router.get('/patchLotto-agent-time', function (req, res) {

    AgentGroupModel.find({}).exec(function (err, response) {


        async.each(response, (transaction, callbackWL) => {

            let body = {

                $set: {
                    // 'shareSetting.lotto.amb.parent': 0,
                    // 'shareSetting.lotto.amb.own': 0,
                    // 'shareSetting.lotto.amb.remaining': transaction.shareSetting.sportsBook.hdpOuOe.remaining,
                    // 'shareSetting.lotto.amb.min': transaction.shareSetting.sportsBook.hdpOuOe.min,

                    'limitSetting.lotto.amb.hour': 15,
                    'limitSetting.lotto.amb.minute': 25,
                }
            };


            console.log(body)
            AgentGroupModel.findOneAndUpdate({_id: transaction._id}, body)
                .exec(function (err, creditResponse) {
                    if (err) {
                        callbackWL(err);
                    }
                    else {
                        callbackWL();
                    }
                });
        }, (err) => {

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: 'success'
                }
            );
        });

    });

});

router.get('/patchLotto-member-time', function (req, res) {

    MemberModel.find({}).exec(function (err, response) {


        async.each(response, (transaction, callbackWL) => {

            let body = {

                $set: {
                    // 'shareSetting.lotto.amb.parent': 0,
                    // 'shareSetting.lotto.amb.own': 0,
                    // 'shareSetting.lotto.amb.remaining': transaction.shareSetting.sportsBook.hdpOuOe.remaining,
                    // 'shareSetting.lotto.amb.min': transaction.shareSetting.sportsBook.hdpOuOe.min,

                    'limitSetting.lotto.amb.hour': 15,
                    'limitSetting.lotto.amb.minute': 25,
                }
            };


            console.log(body)
            MemberModel.findOneAndUpdate({_id: transaction._id}, body)
                .exec(function (err, creditResponse) {
                    if (err) {
                        callbackWL(err);
                    }
                    else {
                        callbackWL();
                    }
                });
        }, (err) => {

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: 'success'
                }
            );
        });

    });

});
//
// router.get('/patchLotto-agent-limit-clear', function (req, res) {
//
//
//     let limit = 0;
//     AgentLottoLimitModel.find({}).exec(function (err, response) {
//
//
//         console.log('length : ', response.length)
//
//         async.each(response, (transaction, callbackWL) => {
//
//
//             let body = {
//
//                 $set: {
//                     'po._1TOP.bets': [],
//                     'po._1BOT.bets': [],
//                     'po._2TOP.bets': [],
//                     'po._2BOT.bets': [],
//                     'po._2TOD.bets': [],
//                     'po._2TOP_OE.bets': [],
//                     'po._2TOP_OU.bets': [],
//                     'po._2BOT_OE.bets': [],
//                     'po._2BOT_OU.bets': [],
//                     'po._3TOP.bets': [],
//                     'po._3BOT.bets': [],
//                     'po._3TOD.bets': [],
//                     'po._3TOP_OE.bets': [],
//                     'po._3TOP_OU.bets': [],
//                     'po._4TOP.bets': [],
//                     'po._4TOD.bets': [],
//                     'po._5TOP.bets': [],
//                     'po._6TOP.bets': []
//                 }
//             };
//
//
//             AgentLottoLimitModel.update({_id: transaction._id}, body, function (err, lottoResponse) {
//                 if (err) {
//                     console.log(err)
//                     callbackWL(err);
//                 }
//                 else {
//                     console.log(lottoResponse)
//                     callbackWL();
//                 }
//             });
//         }, (err) => {
//
//             return res.send(
//                 {
//                     code: 0,
//                     message: "success",
//                     result: 'success'
//                 }
//             );
//         });
//
//     });
//
// });

router.get('/patch-newSetting', function (req, res) {


    AgentGroupModel.find({}).exec(function (err, response) {


        async.each(response, (transaction, callbackWL) => {

            console.log('===== : ', transaction.name)
            let body = {

                $set: {
                    'shareSetting.other.m2.parent': transaction.shareSetting.casino.sexy.parent,
                    'shareSetting.other.m2.own': transaction.shareSetting.casino.sexy.own,
                    'shareSetting.other.m2.remaining': transaction.shareSetting.casino.sexy.remaining,
                    'shareSetting.other.m2.min': transaction.shareSetting.casino.sexy.min,

                    // 'limitSetting.other.m2.maxPerBetHDP': 0,
                    // 'limitSetting.other.m2.minPerBetHDP': 10,
                    // 'limitSetting.other.m2.maxBetPerMatchHDP': 0,
                    //
                    // 'limitSetting.other.m2.maxPerBet': 0,
                    // 'limitSetting.other.m2.minPerBet': 10,
                    // 'limitSetting.other.m2.maxBetPerDay': 0,
                    // 'limitSetting.other.m2.maxPayPerBill': 0,
                    // 'limitSetting.other.m2.maxMatchPerBet': 12,
                    // 'limitSetting.other.m2.minMatchPerBet': 2,
                    // 'limitSetting.other.m2.isEnable': true,
                    //
                    // 'commissionSetting.other.m2': 0,
                }
            };


            console.log(body)
            AgentGroupModel.findOneAndUpdate({_id: transaction._id}, body)
                .exec(function (err, creditResponse) {
                    if (err) {
                        callbackWL(err);
                    }
                    else {
                        callbackWL();
                    }
                });
        }, (err) => {

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: 'success'
                }
            );
        });

    });

});

router.get('/patch-newSetting1', function (req, res) {


    const cursor = MemberModel.find({})
        .batchSize(1000)
        .lean()
        .cursor();

    cursor.on('data', function (betObj) {
        let body = {

            $set: {
                'shareSetting.other.m2.parent': betObj.shareSetting.casino.sexy.parent,

                // 'limitSetting.other.m2.maxPerBetHDP': 0,
                // 'limitSetting.other.m2.minPerBetHDP': 10,
                // 'limitSetting.other.m2.maxBetPerMatchHDP': 0,
                //
                // 'limitSetting.other.m2.maxPerBet': 0,
                // 'limitSetting.other.m2.minPerBet': 10,
                // 'limitSetting.other.m2.maxBetPerDay': 0,
                // 'limitSetting.other.m2.maxPayPerBill': 0,
                // 'limitSetting.other.m2.maxMatchPerBet': 12,
                // 'limitSetting.other.m2.minMatchPerBet': 2,
                // 'limitSetting.other.m2.isEnable': true,
                //
                // 'commissionSetting.other.m2': 0
            }
        };

        console.log(body)
        MemberModel.findOneAndUpdate({_id: betObj._id}, body)
            .exec(function (err, creditResponse) {
                if (err) {
                    // callbackWL(err);
                }
                else {
                    // callbackWL();
                    console.log(creditResponse)
                }
            });

    });


    cursor.on('end', function () {
        console.log('Done!');
        return res.send({code: 0, message: "success"});
    });


});

router.get('/patch234', function (req, res) {


    AgentGroupModel.find({}).exec(function (err, response) {

        async.each(response, (transaction, callbackWL) => {

            console.log('===== : ', transaction.name)
            let body = {

                $set: {
                    'maxCreditLimit': transaction.creditLimit
                }
            };


            console.log(body)
            AgentGroupModel.findOneAndUpdate({_id: transaction._id}, body)
                .exec(function (err, creditResponse) {
                    if (err) {
                        callbackWL(err);
                    }
                    else {
                        callbackWL();
                    }
                });
        }, (err) => {

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: 'success'
                }
            );
        });
    });


});


const Joi = require('joi');

const addBalanceSchema = Joi.object().keys({

    amount: Joi.number().required(),
    betId: Joi.string().required()
});

router.post('/addBalance', function (req, res) {
    let validate = Joi.validate(req.body, addBalanceSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            result: validate.error.details,
            code: 999
        });
    }

    BetTransactionModel.findOne({betId: req.body.betId}, (err, response) => {

        if (response) {
            // let bb = Math.abs(response.memberCredit);//req.body.amount;
            let bb = req.body.amount;

            MemberService.updateBalance(response.memberId, bb, req.body.betId, 'ROLLBACK_BALANCE' + new Date(), '', function (err, updateMemberResponse) {

                // BetTransactionModel.deleteOne({ _id: response._id }, function (err,r) {
                //     console.log(r)
                    return res.send({
                        code: 0,
                        message: "success",
                        result: 'success'
                    });
                // })
            });

        } else {
            return res.send({
                code: 0,
                message: "fail",
                result: 'success'
            });
        }


    });


});


const addBalanceSchema2 = Joi.object().keys({

    betId: Joi.string().required()
});

router.post('/addBalance2', function (req, res) {
    let validate = Joi.validate(req.body, addBalanceSchema2);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            result: validate.error.details,
            code: 999
        });
    }

    BetTransactionModel.findOne({betId: req.body.betId}, (err, response) => {

        if (response) {
            let bb = Math.abs(response.memberCredit);//req.body.amount;

            MemberService.updateBalance(response.memberId, bb, req.body.betId, 'ROLLBACK_BALANCE' + new Date(), '', function (err, updateMemberResponse) {

                BetTransactionModel.deleteOne({_id: response._id}, function (err, r) {
                    //     console.log(r)
                    return res.send({
                        code: 0,
                        message: "success",
                        result: 'success'
                    });
                })
            });

        } else {
            return res.send({
                code: 0,
                message: "fail",
                result: 'success'
            });
        }


    });


});


router.get('/patch-agent-cash', function (req, res) {


    let body = {
        $set: {
            cash: {
                isMemberDeposit: false,
                depositMin: 0,
                depositMax: 0,
                isMemberWithdraw: false,
                withdrawCountDay: 0,
                withdrawMin: 0,
                withdrawMax: 0,
                isActive: false
            }
        }
    };


    AgentGroupModel.update({}, body, {multi: true})
        .exec(function (err, creditResponse) {
            if (err) {
                return res.send(
                    {
                        code: 999,
                        message: "error"
                    }
                );
            } else {
                console.log(creditResponse)
                return res.send(
                    {
                        code: 0,
                        message: "success",
                        result: 'success'
                    }
                );
            }
        });

});

router.get('/user-newsign', function (req, res) {


    MemberModel.aggregate([
        {
            $match: {'createdDate': {"$gte": new Date(moment("03/01/2019", "DD/MM/YYYY").format('YYYY-MM-DDT00:00:00.000'))}}
        },
        {
            $group: {
                _id: {
                    date: {$dateToString: {format: "%d/%m/%Y", date: "$createdDate"}},
                    day: {$dateToString: {format: "%d", date: "$createdDate"}},
                },
                count: {$sum: 1},
            },
        },
        {$sort: {'_id.day': -1}},
        {
            $project: {
                _id: 0,
                date: '$_id.date',
                count: '$count',
            }
        }
    ], (err, results) => {

        return res.send(
            {
                code: 0,
                message: "success",
                result: results
            }
        );
    });


});

const UpdateBalanceHistoryModel = require('../models/updateBalanceHistory.model.js');

router.get('/12345', function (req, res) {


    BetTransactionModel.find({
        '_id': {$gte: mongoose.Types.ObjectId('5e4c99c87f06ce00079ac4ad')},
        game: 'CASINO', source: 'SEXY_BACCARAT', 'baccarat.sexy.roundId': {
            $in: ['Mexico-01-GA253350057', 'Mexico-01-GA253360043', 'Mexico-01-GA253370030', 'Mexico-01-GA253370044', 'Mexico-02-GA252950023'
                , 'Mexico-02-GA252950063', 'Mexico-02-GA252960021', 'Mexico-02-GA252960053', 'Mexico-02-GA252960059', 'Mexico-03-GA252100021', 'Mexico-03-GA252100036'
                , 'Mexico-03-GA252100037', 'Mexico-03-GA252110059', 'Mexico-03-GA252120002', 'Mexico-03-GA252120030', 'Mexico-03-GA252130009', 'Mexico-03-GA252120050'
                , 'Mexico-04-GA253900042', 'Mexico-04-GA253920003', 'Mexico-04-GA253920015', 'Mexico-04-GA253920027', 'Mexico-04-GA253930006', 'Mexico-05-GA250870021'
                , 'Mexico-05-GA250870045', 'Mexico-05-GA250870053', 'Mexico-05-GA250880048', 'Mexico-06-GA63810025', 'Mexico-06-GA63820008', 'Mexico-06-GA63820015', 'Mexico-06-GA63820018', 'Mexico-06-GA63830023'
                , 'Mexico-07-GA56740058', 'Mexico-07-GA56750011', 'Mexico-07-GA56750053', 'Mexico-07-GA56760011', 'Mexico-07-GA56760024', 'Mexico-08-GA56110034', 'Mexico-08-GA56120051', 'Mexico-08-GA56130044'
                , 'Mexico-09-GA33340052', 'Mexico-09-GA33360017', 'Mexico-09-GA33360059', 'Mexico-10-GA23060046', 'Mexico-10-GA23070021', 'Mexico-10-GA23070031', 'Mexico-10-GA23070056', 'Mexico-10-GA23080032',
                'Mexico-10-GA23080033', 'Mexico-10-GA23080054', 'Mexico-10-GA23080056', 'Mexico-21-GA83890037']
        }
        // ,betId:'BET15820906530637738817'
    }, function (err, betList) {
        if (err) {
            return res.send({code: 0, message: "fail1"});
        }
        async.each(betList, (betObj, betListCallback) => {


            async.waterfall([callback => {

                console.log('22222222')
                UpdateBalanceHistoryModel.find({
                    'createdDate': {$gte: new Date(moment().add(-1, 'd').format('YYYY-MM-DDTHH:mm:ss.000'))},
                    betId: betObj.betId
                })
                    .sort({createdDate: 1})
                    .exec(function (err, memberResponse) {
                        if (err) {
                            callback(err, null);
                            return;
                        }
                        if (memberResponse) {
                            callback(null, memberResponse);
                        } else {
                            callback(1004, null);
                        }
                    });

            }], (err, cancelHis) => {

                if (err) {
                    console.log(err)
                    return res.send({code: 1004, message: "Invalid User Id"});
                }


                let lastObj = cancelHis[cancelHis.length - 1];


                if (lastObj.description == 'Roll Back') {
                    MemberService.updateBalance(betObj.memberId, lastObj.amount * -1, betObj.betId, 'PATCH_WIN', 'xxx', function (err, updateBalanceResponse) {
                        if (err) {
                            if (err.code == 888) {
                                betListCallback(null, {});
                            } else {
                                betListCallback(err, null);
                            }
                        } else {
                            console.log('do : ', betObj.betId)
                            betListCallback(null, {});

                        }
                    });

                } else {
                    console.log('not do : ', betObj.betId)
                    betListCallback(null, {});
                }


            });

        }, (err, result) => {

            if (err) {
                return res.send({code: 0, message: "fail"});
            }

            return res.send({code: 1, message: "success"});
        });
    });

});

router.get('/roll-tran2', function (req, res) {

    let condition = {
        gameDate: {
            "$gte": new Date(moment().utc(true).format('2020-04-10T00:00:00.000')),
            "$lte": new Date(moment().utc(true).format('2020-04-10T05:00:00.000'))
        },
        game: 'CASINO',
        source: 'PRETTY_GAME',
        'baccarat.pretty.gameType': 'Dragon',
        memberUsername: '08bo68206426'
    };

    console.log(condition)

    BetTransactionModel.find(condition)
        .exec(function (err, list) {


            async.each(list, (betObj, callbackWL) => {


                let balance = betObj.status == 'RUNNING' ? Math.abs(betObj.memberCredit) : (betObj.commission.member.totalWinLoseCom * -1);
                //refundBalance
                MemberService.updateBalance(betObj.memberId, balance, betObj.betId, 'CANCELLED', '', function (err, updateBalanceResponse) {
                    if (err) {
                        return res.send({message: "data not found", results: err, code: 999});
                    } else {


                        let updateBody = {
                            $set: {
                                status: 'CANCELLED',
                                'commission.superAdmin.winLoseCom': 0,
                                'commission.superAdmin.winLose': 0,
                                'commission.superAdmin.totalWinLoseCom': 0,

                                'commission.company.winLoseCom': 0,
                                'commission.company.winLose': 0,
                                'commission.company.totalWinLoseCom': 0,

                                'commission.shareHolder.winLoseCom': 0,
                                'commission.shareHolder.winLose': 0,
                                'commission.shareHolder.totalWinLoseCom': 0,

                                'commission.senior.winLoseCom': 0,
                                'commission.senior.winLose': 0,
                                'commission.senior.totalWinLoseCom': 0,

                                'commission.masterAgent.winLoseCom': 0,
                                'commission.masterAgent.winLose': 0,
                                'commission.masterAgent.totalWinLoseCom': 0,

                                'commission.agent.winLoseCom': 0,
                                'commission.agent.winLose': 0,
                                'commission.agent.totalWinLoseCom': 0,

                                'commission.member.winLoseCom': 0,
                                'commission.member.winLose': 0,
                                'commission.member.totalWinLoseCom': 0,
                            }
                        };
                        BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                            if (err) {
                                callbackWL(err, '');
                            } else {
                                callbackWL(null, '');
                            }
                        });

                    }
                });
            }, (err) => {

                return res.send(
                    {
                        code: 0,
                        message: "success",
                        result: 'success'
                    }
                );
            });
        });
});


router.get('/patchLotto-clone-agent', function (req, res) {


    AgentGroupModel.findOne({name_lower: '68wzl0'}).exec((err, memberObj) => {


        if (!memberObj) {
            return res.send({code: 999, message: "member not found"});
        }

        const cursor = AgentGroupModel.find({parentId: mongoose.Types.ObjectId(memberObj.group)})
            .batchSize(1000)
            .lean()
            .cursor();

        cursor.on('data', function (currentObj) {

            console.log(currentObj.name_lower)

            let body = {

                $set: {


                    'shareSetting.sportsBook.today.parent': memberObj.shareSetting.sportsBook.today.parent,
                    'shareSetting.sportsBook.today.own': memberObj.shareSetting.sportsBook.today.own,
                    'shareSetting.sportsBook.today.remaining': memberObj.shareSetting.sportsBook.today.remaining,
                    'shareSetting.sportsBook.today.min': memberObj.shareSetting.sportsBook.today.min,


                    'shareSetting.sportsBook.live.parent': memberObj.shareSetting.sportsBook.live.parent,
                    'shareSetting.sportsBook.live.own': memberObj.shareSetting.sportsBook.live.own,
                    'shareSetting.sportsBook.live.remaining': memberObj.shareSetting.sportsBook.live.remaining,
                    'shareSetting.sportsBook.live.min': memberObj.shareSetting.sportsBook.live.min,

                    'shareSetting.step.parlay.parent': memberObj.shareSetting.step.parlay.parent,
                    'shareSetting.step.parlay.own': memberObj.shareSetting.step.parlay.own,
                    'shareSetting.step.parlay.remaining': memberObj.shareSetting.step.parlay.remaining,
                    'shareSetting.step.parlay.min': memberObj.shareSetting.step.parlay.min,

                    'shareSetting.step.step.parent': memberObj.shareSetting.step.step.parent,
                    'shareSetting.step.step.own': memberObj.shareSetting.step.step.own,
                    'shareSetting.step.step.remaining': memberObj.shareSetting.step.step.remaining,
                    'shareSetting.step.step.min': memberObj.shareSetting.step.step.min,

                    'shareSetting.casino.sexy.parent': memberObj.shareSetting.casino.sexy.parent,
                    'shareSetting.casino.sexy.own': memberObj.shareSetting.casino.sexy.own,
                    'shareSetting.casino.sexy.remaining': memberObj.shareSetting.casino.sexy.remaining,
                    'shareSetting.casino.sexy.min': memberObj.shareSetting.casino.sexy.min,

                    'shareSetting.casino.ag.parent': memberObj.shareSetting.casino.ag.parent,
                    'shareSetting.casino.ag.own': memberObj.shareSetting.casino.ag.own,
                    'shareSetting.casino.ag.remaining': memberObj.shareSetting.casino.ag.remaining,
                    'shareSetting.casino.ag.min': memberObj.shareSetting.casino.ag.min,

                    'shareSetting.casino.sa.parent': memberObj.shareSetting.casino.sa.parent,
                    'shareSetting.casino.sa.own': memberObj.shareSetting.casino.sa.own,
                    'shareSetting.casino.sa.remaining': memberObj.shareSetting.casino.sa.remaining,
                    'shareSetting.casino.sa.min': memberObj.shareSetting.casino.sa.min,

                    'shareSetting.casino.dg.parent': memberObj.shareSetting.casino.dg.parent,
                    'shareSetting.casino.dg.own': memberObj.shareSetting.casino.dg.own,
                    'shareSetting.casino.dg.remaining': memberObj.shareSetting.casino.dg.remaining,
                    'shareSetting.casino.dg.min': memberObj.shareSetting.casino.dg.min,

                    'shareSetting.game.slotXO.parent': memberObj.shareSetting.game.slotXO.parent,
                    'shareSetting.game.slotXO.own': memberObj.shareSetting.game.slotXO.own,
                    'shareSetting.game.slotXO.remaining': memberObj.shareSetting.game.slotXO.remaining,
                    'shareSetting.game.slotXO.min': memberObj.shareSetting.game.slotXO.min,

                    'shareSetting.multi.amb.parent': memberObj.shareSetting.multi.amb.parent,
                    'shareSetting.multi.amb.own': memberObj.shareSetting.multi.amb.own,
                    'shareSetting.multi.amb.remaining': memberObj.shareSetting.multi.amb.remaining,
                    'shareSetting.multi.amb.min': memberObj.shareSetting.multi.amb.min,

                    'shareSetting.other.m2.parent': memberObj.shareSetting.other.m2.parent,
                    'shareSetting.other.m2.own': memberObj.shareSetting.other.m2.own,
                    'shareSetting.other.m2.remaining': memberObj.shareSetting.other.m2.remaining,
                    'shareSetting.other.m2.min': memberObj.shareSetting.other.m2.min,

                    'shareSetting.lotto.amb.parent': memberObj.shareSetting.lotto.amb.parent,
                    'shareSetting.lotto.amb.own': memberObj.shareSetting.lotto.amb.own,
                    'shareSetting.lotto.amb.remaining': memberObj.shareSetting.lotto.amb.remaining,

                    'shareSetting.lotto.pp.parent': memberObj.shareSetting.lotto.pp.parent,
                    'shareSetting.lotto.pp.own': memberObj.shareSetting.lotto.pp.own,
                    'shareSetting.lotto.pp.remaining': memberObj.shareSetting.lotto.pp.remaining,

                    'shareSetting.lotto.laos.parent': memberObj.shareSetting.lotto.laos.parent,
                    'shareSetting.lotto.laos.own': memberObj.shareSetting.lotto.laos.own,
                    'shareSetting.lotto.laos.remaining': memberObj.shareSetting.lotto.laos.remaining,


                    'limitSetting.sportsBook.hdpOuOe.maxPerBet': memberObj.limitSetting.sportsBook.hdpOuOe.maxPerBet,
                    'limitSetting.sportsBook.hdpOuOe.maxPerMatch': memberObj.limitSetting.sportsBook.hdpOuOe.maxPerMatch,
                    'limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet': memberObj.limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet,
                    'limitSetting.sportsBook.oneTwoDoubleChance.maxPerMatch': memberObj.limitSetting.sportsBook.oneTwoDoubleChance.maxPerMatch,
                    'limitSetting.sportsBook.others.maxPerBet': memberObj.limitSetting.sportsBook.others.maxPerBet,
                    'limitSetting.sportsBook.others.maxPerMatch': memberObj.limitSetting.sportsBook.others.maxPerMatch,
                    'limitSetting.sportsBook.outright.maxPerBet': memberObj.limitSetting.sportsBook.outright.maxPerBet,
                    'limitSetting.sportsBook.outright.maxPerMatch': memberObj.limitSetting.sportsBook.outright.maxPerMatch,
                    'limitSetting.sportsBook.isEnable': memberObj.limitSetting.sportsBook.isEnable,


                    'limitSetting.step.parlay.maxPerBet': memberObj.limitSetting.step.parlay.maxPerBet,
                    'limitSetting.step.parlay.minPerBet': memberObj.limitSetting.step.parlay.minPerBet,
                    'limitSetting.step.parlay.maxBetPerDay': memberObj.limitSetting.step.parlay.maxBetPerDay,
                    'limitSetting.step.parlay.maxPayPerBill': memberObj.limitSetting.step.parlay.maxPayPerBill,
                    'limitSetting.step.parlay.maxMatchPerBet': memberObj.limitSetting.step.parlay.maxMatchPerBet,
                    'limitSetting.step.parlay.minMatchPerBet': memberObj.limitSetting.step.parlay.minMatchPerBet,
                    'limitSetting.step.parlay.isEnable': memberObj.limitSetting.step.parlay.isEnable,

                    'limitSetting.step.step.maxPerBet': memberObj.limitSetting.step.step.maxPerBet,
                    'limitSetting.step.step.minPerBet': memberObj.limitSetting.step.step.minPerBet,
                    'limitSetting.step.step.maxBetPerDay': memberObj.limitSetting.step.step.maxBetPerDay,
                    'limitSetting.step.step.maxPayPerBill': memberObj.limitSetting.step.step.maxPayPerBill,
                    'limitSetting.step.step.maxMatchPerBet': memberObj.limitSetting.step.step.maxMatchPerBet,
                    'limitSetting.step.step.minMatchPerBet': memberObj.limitSetting.step.step.minMatchPerBet,
                    'limitSetting.step.step.isEnable': memberObj.limitSetting.step.step.isEnable,

                    'limitSetting.casino.sexy.isEnable': memberObj.limitSetting.casino.sexy.isEnable,
                    'limitSetting.casino.sexy.limit': memberObj.limitSetting.casino.sexy.limit,
                    'limitSetting.casino.ag.isEnable': memberObj.limitSetting.casino.ag.isEnable,
                    'limitSetting.casino.ag.limit': memberObj.limitSetting.casino.ag.limit,
                    'limitSetting.casino.sa.isEnable': memberObj.limitSetting.casino.sa.isEnable,
                    'limitSetting.casino.sa.limit': memberObj.limitSetting.casino.sa.limit,
                    'limitSetting.casino.dg.isEnable': memberObj.limitSetting.casino.dg.isEnable,
                    'limitSetting.casino.dg.limit': memberObj.limitSetting.casino.dg.limit,

                    'limitSetting.multi.amb.isEnable': memberObj.limitSetting.multi.amb.isEnable,

                    'limitSetting.game.slotXO.isEnable': memberObj.limitSetting.game.slotXO.isEnable,


                    'limitSetting.other.m2.maxPerBetHDP': memberObj.limitSetting.other.m2.maxPerBetHDP,
                    'limitSetting.other.m2.minPerBetHDP': memberObj.limitSetting.other.m2.minPerBetHDP,
                    'limitSetting.other.m2.maxBetPerMatchHDP': memberObj.limitSetting.other.m2.maxBetPerMatchHDP,
                    'limitSetting.other.m2.maxPerBet': memberObj.limitSetting.other.m2.maxPerBet,
                    'limitSetting.other.m2.minPerBet': memberObj.limitSetting.other.m2.minPerBet,
                    'limitSetting.other.m2.maxBetPerDay': memberObj.limitSetting.other.m2.maxBetPerDay,
                    'limitSetting.other.m2.maxPayPerBill': memberObj.limitSetting.other.m2.maxPayPerBill,
                    'limitSetting.other.m2.maxMatchPerBet': memberObj.limitSetting.other.m2.maxMatchPerBet,
                    'limitSetting.other.m2.minMatchPerBet': memberObj.limitSetting.other.m2.minMatchPerBet,
                    'limitSetting.other.m2.isEnable': memberObj.limitSetting.other.m2.isEnable,


                    'limitSetting.lotto.laos.hour': memberObj.limitSetting.lotto.laos.hour,
                    'limitSetting.lotto.laos.minute': memberObj.limitSetting.lotto.laos.minute,
                    'limitSetting.lotto.laos.L_4TOP.payout': memberObj.limitSetting.lotto.laos.L_4TOP.payout,
                    'limitSetting.lotto.laos.L_4TOP.discount': memberObj.limitSetting.lotto.laos.L_4TOP.discount,
                    'limitSetting.lotto.laos.L_4TOP.max': memberObj.limitSetting.lotto.laos.L_4TOP.max,
                    //
                    'limitSetting.lotto.laos.L_4TOD.payout': memberObj.limitSetting.lotto.laos.L_4TOD.payout,
                    'limitSetting.lotto.laos.L_4TOD.discount': memberObj.limitSetting.lotto.laos.L_4TOD.discount,
                    'limitSetting.lotto.laos.L_4TOD.max': memberObj.limitSetting.lotto.laos.L_4TOD.max,
                    //
                    'limitSetting.lotto.laos.L_3TOP.payout': memberObj.limitSetting.lotto.laos.L_3TOP.payout,
                    'limitSetting.lotto.laos.L_3TOP.discount': memberObj.limitSetting.lotto.laos.L_3TOP.discount,
                    'limitSetting.lotto.laos.L_3TOP.max': memberObj.limitSetting.lotto.laos.L_3TOP.max,
                    //
                    'limitSetting.lotto.laos.L_3TOD.payout': memberObj.limitSetting.lotto.laos.L_3TOD.payout,
                    'limitSetting.lotto.laos.L_3TOD.discount': memberObj.limitSetting.lotto.laos.L_3TOD.discount,
                    'limitSetting.lotto.laos.L_3TOD.max': memberObj.limitSetting.lotto.laos.L_3TOD.max,
                    //
                    'limitSetting.lotto.laos.L_2FB.payout': memberObj.limitSetting.lotto.laos.L_2FB.payout,
                    'limitSetting.lotto.laos.L_2FB.discount': memberObj.limitSetting.lotto.laos.L_2FB.discount,
                    'limitSetting.lotto.laos.L_2FB.max': memberObj.limitSetting.lotto.laos.L_2FB.max,


                    'limitSetting.lotto.laos.T_4TOP.payout': memberObj.limitSetting.lotto.laos.T_4TOP.payout,
                    'limitSetting.lotto.laos.T_4TOP.discount': memberObj.limitSetting.lotto.laos.T_4TOP.discount,
                    'limitSetting.lotto.laos.T_4TOP.max': memberObj.limitSetting.lotto.laos.T_4TOP.max,
                    //
                    'limitSetting.lotto.laos.T_4TOD.payout': memberObj.limitSetting.lotto.laos.T_4TOD.payout,
                    'limitSetting.lotto.laos.T_4TOD.discount': memberObj.limitSetting.lotto.laos.T_4TOD.discount,
                    'limitSetting.lotto.laos.T_4TOD.max': memberObj.limitSetting.lotto.laos.T_4TOD.max,
                    //
                    'limitSetting.lotto.laos.T_3TOP.payout': memberObj.limitSetting.lotto.laos.T_3TOP.payout,
                    'limitSetting.lotto.laos.T_3TOP.discount': memberObj.limitSetting.lotto.laos.T_3TOP.discount,
                    'limitSetting.lotto.laos.T_3TOP.max': memberObj.limitSetting.lotto.laos.T_3TOP.max,
                    //
                    'limitSetting.lotto.laos.T_3TOD.payout': memberObj.limitSetting.lotto.laos.T_3TOD.payout,
                    'limitSetting.lotto.laos.T_3TOD.discount': memberObj.limitSetting.lotto.laos.T_3TOD.discount,
                    'limitSetting.lotto.laos.T_3TOD.max': memberObj.limitSetting.lotto.laos.T_3TOD.max,

                    'limitSetting.lotto.laos.T_2TOP.payout': memberObj.limitSetting.lotto.laos.T_2TOP.payout,
                    'limitSetting.lotto.laos.T_2TOP.discount': memberObj.limitSetting.lotto.laos.T_2TOP.discount,
                    'limitSetting.lotto.laos.T_2TOP.max': memberObj.limitSetting.lotto.laos.T_2TOP.max,
                    //
                    'limitSetting.lotto.laos.T_2TOD.payout': memberObj.limitSetting.lotto.laos.T_2TOD.payout,
                    'limitSetting.lotto.laos.T_2TOD.discount': memberObj.limitSetting.lotto.laos.T_2TOD.discount,
                    'limitSetting.lotto.laos.T_2TOD.max': memberObj.limitSetting.lotto.laos.T_2TOD.max,
                    //
                    'limitSetting.lotto.laos.T_2BOT.payout': memberObj.limitSetting.lotto.laos.T_2BOT.payout,
                    'limitSetting.lotto.laos.T_2BOT.discount': memberObj.limitSetting.lotto.laos.T_2BOT.discount,
                    'limitSetting.lotto.laos.T_2BOT.max': memberObj.limitSetting.lotto.laos.T_2BOT.max,

                    'limitSetting.lotto.laos.T_1TOP.payout': memberObj.limitSetting.lotto.laos.T_1TOP.payout,
                    'limitSetting.lotto.laos.T_1TOP.discount': memberObj.limitSetting.lotto.laos.T_1TOP.discount,
                    'limitSetting.lotto.laos.T_1TOP.max': memberObj.limitSetting.lotto.laos.T_1TOP.max,
                    //
                    'limitSetting.lotto.laos.T_1BOT.payout': memberObj.limitSetting.lotto.laos.T_1BOT.payout,
                    'limitSetting.lotto.laos.T_1BOT.discount': memberObj.limitSetting.lotto.laos.T_1BOT.discount,
                    'limitSetting.lotto.laos.T_1BOT.max': memberObj.limitSetting.lotto.laos.T_1BOT.max,


                    'limitSetting.lotto.amb.isEnable': memberObj.limitSetting.lotto.amb.isEnable,
                    'limitSetting.lotto.amb.hour': memberObj.limitSetting.lotto.amb.hour,
                    'limitSetting.lotto.amb.minute': memberObj.limitSetting.lotto.amb.minute,

                    'limitSetting.lotto.amb._6TOP.payout': memberObj.limitSetting.lotto.amb._6TOP.payout,
                    'limitSetting.lotto.amb._6TOP.discount': memberObj.limitSetting.lotto.amb._6TOP.discount,
                    'limitSetting.lotto.amb._6TOP.max': memberObj.limitSetting.lotto.amb._6TOP.max,

                    'limitSetting.lotto.amb._5TOP.payout': memberObj.limitSetting.lotto.amb._5TOP.payout,
                    'limitSetting.lotto.amb._5TOP.discount': memberObj.limitSetting.lotto.amb._5TOP.discount,
                    'limitSetting.lotto.amb._5TOP.max': memberObj.limitSetting.lotto.amb._5TOP.max,

                    'limitSetting.lotto.amb._4TOP.payout': memberObj.limitSetting.lotto.amb._4TOP.payout,
                    'limitSetting.lotto.amb._4TOP.discount': memberObj.limitSetting.lotto.amb._4TOP.discount,
                    'limitSetting.lotto.amb._4TOP.max': memberObj.limitSetting.lotto.amb._4TOP.max,

                    'limitSetting.lotto.amb._4TOD.payout': memberObj.limitSetting.lotto.amb._4TOD.payout,
                    'limitSetting.lotto.amb._4TOD.discount': memberObj.limitSetting.lotto.amb._4TOD.discount,
                    'limitSetting.lotto.amb._4TOD.max': memberObj.limitSetting.lotto.amb._4TOD.max,

                    'limitSetting.lotto.amb._3TOP.payout': memberObj.limitSetting.lotto.amb._3TOP.payout,
                    'limitSetting.lotto.amb._3TOP.discount': memberObj.limitSetting.lotto.amb._3TOP.discount,
                    'limitSetting.lotto.amb._3TOP.max': memberObj.limitSetting.lotto.amb._3TOP.max,

                    'limitSetting.lotto.amb._3TOD.payout': memberObj.limitSetting.lotto.amb._3TOD.payout,
                    'limitSetting.lotto.amb._3TOD.discount': memberObj.limitSetting.lotto.amb._3TOD.discount,
                    'limitSetting.lotto.amb._3TOD.max': memberObj.limitSetting.lotto.amb._3TOD.max,

                    'limitSetting.lotto.amb._3BOT.payout': memberObj.limitSetting.lotto.amb._3BOT.payout,
                    'limitSetting.lotto.amb._3BOT.discount': memberObj.limitSetting.lotto.amb._3BOT.discount,
                    'limitSetting.lotto.amb._3BOT.max': memberObj.limitSetting.lotto.amb._3BOT.max,

                    'limitSetting.lotto.amb._3TOP_OE.payout': memberObj.limitSetting.lotto.amb._3TOP_OE.payout,
                    'limitSetting.lotto.amb._3TOP_OE.discount': memberObj.limitSetting.lotto.amb._3TOP_OE.discount,
                    'limitSetting.lotto.amb._3TOP_OE.max': memberObj.limitSetting.lotto.amb._3TOP_OE.max,

                    'limitSetting.lotto.amb._3TOP_OU.payout': memberObj.limitSetting.lotto.amb._3TOP_OU.payout,
                    'limitSetting.lotto.amb._3TOP_OU.discount': memberObj.limitSetting.lotto.amb._3TOP_OU.discount,
                    'limitSetting.lotto.amb._3TOP_OU.max': memberObj.limitSetting.lotto.amb._3TOP_OU.max,

                    'limitSetting.lotto.amb._2TOP.payout': memberObj.limitSetting.lotto.amb._2TOP.payout,
                    'limitSetting.lotto.amb._2TOP.discount': memberObj.limitSetting.lotto.amb._2TOP.discount,
                    'limitSetting.lotto.amb._2TOP.max': memberObj.limitSetting.lotto.amb._2TOP.max,

                    'limitSetting.lotto.amb._2TOD.payout': memberObj.limitSetting.lotto.amb._2TOD.payout,
                    'limitSetting.lotto.amb._2TOD.discount': memberObj.limitSetting.lotto.amb._2TOD.discount,
                    'limitSetting.lotto.amb._2TOD.max': memberObj.limitSetting.lotto.amb._2TOD.max,

                    'limitSetting.lotto.amb._2BOT.payout': memberObj.limitSetting.lotto.amb._2BOT.payout,
                    'limitSetting.lotto.amb._2BOT.discount': memberObj.limitSetting.lotto.amb._2BOT.discount,
                    'limitSetting.lotto.amb._2BOT.max': memberObj.limitSetting.lotto.amb._2BOT.max,

                    'limitSetting.lotto.amb._2TOP_OE.payout': memberObj.limitSetting.lotto.amb._2TOP_OE.payout,
                    'limitSetting.lotto.amb._2TOP_OE.discount': memberObj.limitSetting.lotto.amb._2TOP_OE.discount,
                    'limitSetting.lotto.amb._2TOP_OE.max': memberObj.limitSetting.lotto.amb._2TOP_OE.max,

                    'limitSetting.lotto.amb._2TOP_OU.payout': memberObj.limitSetting.lotto.amb._2TOP_OU.payout,
                    'limitSetting.lotto.amb._2TOP_OU.discount': memberObj.limitSetting.lotto.amb._2TOP_OU.discount,
                    'limitSetting.lotto.amb._2TOP_OU.max': memberObj.limitSetting.lotto.amb._2TOP_OU.max,

                    'limitSetting.lotto.amb._2BOT_OE.payout': memberObj.limitSetting.lotto.amb._2BOT_OE.payout,
                    'limitSetting.lotto.amb._2BOT_OE.discount': memberObj.limitSetting.lotto.amb._2BOT_OE.discount,
                    'limitSetting.lotto.amb._2BOT_OE.max': memberObj.limitSetting.lotto.amb._2BOT_OE.max,

                    'limitSetting.lotto.amb._2BOT_OU.payout': memberObj.limitSetting.lotto.amb._2BOT_OU.payout,
                    'limitSetting.lotto.amb._2BOT_OU.discount': memberObj.limitSetting.lotto.amb._2BOT_OU.discount,
                    'limitSetting.lotto.amb._2BOT_OU.max': memberObj.limitSetting.lotto.amb._2BOT_OU.max,

                    'limitSetting.lotto.amb._1TOP.payout': memberObj.limitSetting.lotto.amb._1TOP.payout,
                    'limitSetting.lotto.amb._1TOP.discount': memberObj.limitSetting.lotto.amb._1TOP.discount,
                    'limitSetting.lotto.amb._1TOP.max': memberObj.limitSetting.lotto.amb._1TOP.max,

                    'limitSetting.lotto.amb._1BOT.payout': memberObj.limitSetting.lotto.amb._1BOT.payout,
                    'limitSetting.lotto.amb._1BOT.discount': memberObj.limitSetting.lotto.amb._1BOT.discount,
                    'limitSetting.lotto.amb._1BOT.max': memberObj.limitSetting.lotto.amb._1BOT.max,


                    'limitSetting.lotto.pp.isEnable': memberObj.limitSetting.lotto.pp.isEnable,
                    'limitSetting.lotto.pp.hour': memberObj.limitSetting.lotto.pp.hour,
                    'limitSetting.lotto.pp.minute': memberObj.limitSetting.lotto.pp.minute,

                    'limitSetting.lotto.pp._6TOP.payout': memberObj.limitSetting.lotto.pp._6TOP.payout,
                    'limitSetting.lotto.pp._6TOP.discount': memberObj.limitSetting.lotto.pp._6TOP.discount,
                    'limitSetting.lotto.pp._6TOP.max': memberObj.limitSetting.lotto.pp._6TOP.max,

                    'limitSetting.lotto.pp._5TOP.payout': memberObj.limitSetting.lotto.pp._5TOP.payout,
                    'limitSetting.lotto.pp._5TOP.discount': memberObj.limitSetting.lotto.pp._5TOP.discount,
                    'limitSetting.lotto.pp._5TOP.max': memberObj.limitSetting.lotto.pp._5TOP.max,

                    'limitSetting.lotto.pp._4TOP.payout': memberObj.limitSetting.lotto.pp._4TOP.payout,
                    'limitSetting.lotto.pp._4TOP.discount': memberObj.limitSetting.lotto.pp._4TOP.discount,
                    'limitSetting.lotto.pp._4TOP.max': memberObj.limitSetting.lotto.pp._4TOP.max,

                    'limitSetting.lotto.pp._4TOD.payout': memberObj.limitSetting.lotto.pp._4TOD.payout,
                    'limitSetting.lotto.pp._4TOD.discount': memberObj.limitSetting.lotto.pp._4TOD.discount,
                    'limitSetting.lotto.pp._4TOD.max': memberObj.limitSetting.lotto.pp._4TOD.max,

                    'limitSetting.lotto.pp._3TOP.payout': memberObj.limitSetting.lotto.pp._3TOP.payout,
                    'limitSetting.lotto.pp._3TOP.discount': memberObj.limitSetting.lotto.pp._3TOP.discount,
                    'limitSetting.lotto.pp._3TOP.max': memberObj.limitSetting.lotto.pp._3TOP.max,

                    'limitSetting.lotto.pp._3TOD.payout': memberObj.limitSetting.lotto.pp._3TOD.payout,
                    'limitSetting.lotto.pp._3TOD.discount': memberObj.limitSetting.lotto.pp._3TOD.discount,
                    'limitSetting.lotto.pp._3TOD.max': memberObj.limitSetting.lotto.pp._3TOD.max,

                    'limitSetting.lotto.pp._3BOT.payout': memberObj.limitSetting.lotto.pp._3BOT.payout,
                    'limitSetting.lotto.pp._3BOT.discount': memberObj.limitSetting.lotto.pp._3BOT.discount,
                    'limitSetting.lotto.pp._3BOT.max': memberObj.limitSetting.lotto.pp._3BOT.max,

                    'limitSetting.lotto.pp._3TOP_OE.payout': memberObj.limitSetting.lotto.pp._3TOP_OE.payout,
                    'limitSetting.lotto.pp._3TOP_OE.discount': memberObj.limitSetting.lotto.pp._3TOP_OE.discount,
                    'limitSetting.lotto.pp._3TOP_OE.max': memberObj.limitSetting.lotto.pp._3TOP_OE.max,

                    'limitSetting.lotto.pp._3TOP_OU.payout': memberObj.limitSetting.lotto.pp._3TOP_OU.payout,
                    'limitSetting.lotto.pp._3TOP_OU.discount': memberObj.limitSetting.lotto.pp._3TOP_OU.discount,
                    'limitSetting.lotto.pp._3TOP_OU.max': memberObj.limitSetting.lotto.pp._3TOP_OU.max,

                    'limitSetting.lotto.pp._2TOP.payout': memberObj.limitSetting.lotto.pp._2TOP.payout,
                    'limitSetting.lotto.pp._2TOP.discount': memberObj.limitSetting.lotto.pp._2TOP.discount,
                    'limitSetting.lotto.pp._2TOP.max': memberObj.limitSetting.lotto.pp._2TOP.max,

                    'limitSetting.lotto.pp._2TOD.payout': memberObj.limitSetting.lotto.pp._2TOD.payout,
                    'limitSetting.lotto.pp._2TOD.discount': memberObj.limitSetting.lotto.pp._2TOD.discount,
                    'limitSetting.lotto.pp._2TOD.max': memberObj.limitSetting.lotto.pp._2TOD.max,

                    'limitSetting.lotto.pp._2BOT.payout': memberObj.limitSetting.lotto.pp._2BOT.payout,
                    'limitSetting.lotto.pp._2BOT.discount': memberObj.limitSetting.lotto.pp._2BOT.discount,
                    'limitSetting.lotto.pp._2BOT.max': memberObj.limitSetting.lotto.pp._2BOT.max,

                    'limitSetting.lotto.pp._2TOP_OE.payout': memberObj.limitSetting.lotto.pp._2TOP_OE.payout,
                    'limitSetting.lotto.pp._2TOP_OE.discount': memberObj.limitSetting.lotto.pp._2TOP_OE.discount,
                    'limitSetting.lotto.pp._2TOP_OE.max': memberObj.limitSetting.lotto.pp._2TOP_OE.max,

                    'limitSetting.lotto.pp._2TOP_OU.payout': memberObj.limitSetting.lotto.pp._2TOP_OU.payout,
                    'limitSetting.lotto.pp._2TOP_OU.discount': memberObj.limitSetting.lotto.pp._2TOP_OU.discount,
                    'limitSetting.lotto.pp._2TOP_OU.max': memberObj.limitSetting.lotto.pp._2TOP_OU.max,

                    'limitSetting.lotto.pp._2BOT_OE.payout': memberObj.limitSetting.lotto.pp._2BOT_OE.payout,
                    'limitSetting.lotto.pp._2BOT_OE.discount': memberObj.limitSetting.lotto.pp._2BOT_OE.discount,
                    'limitSetting.lotto.pp._2BOT_OE.max': memberObj.limitSetting.lotto.pp._2BOT_OE.max,

                    'limitSetting.lotto.pp._2BOT_OU.payout': memberObj.limitSetting.lotto.pp._2BOT_OU.payout,
                    'limitSetting.lotto.pp._2BOT_OU.discount': memberObj.limitSetting.lotto.pp._2BOT_OU.discount,
                    'limitSetting.lotto.pp._2BOT_OU.max': memberObj.limitSetting.lotto.pp._2BOT_OU.max,

                    'limitSetting.lotto.pp._1TOP.payout': memberObj.limitSetting.lotto.pp._1TOP.payout,
                    'limitSetting.lotto.pp._1TOP.discount': memberObj.limitSetting.lotto.pp._1TOP.discount,
                    'limitSetting.lotto.pp._1TOP.max': memberObj.limitSetting.lotto.pp._1TOP.max,

                    'limitSetting.lotto.pp._1BOT.payout': memberObj.limitSetting.lotto.pp._1BOT.payout,
                    'limitSetting.lotto.pp._1BOT.discount': memberObj.limitSetting.lotto.pp._1BOT.discount,
                    'limitSetting.lotto.pp._1BOT.max': memberObj.limitSetting.lotto.pp._1BOT.max,

                    'commissionSetting.sportsBook.typeHdpOuOe': memberObj.commissionSetting.sportsBook.typeHdpOuOe,
                    'commissionSetting.sportsBook.hdpOuOe': memberObj.commissionSetting.sportsBook.hdpOuOe,
                    'commissionSetting.sportsBook.oneTwoDoubleChance': memberObj.commissionSetting.sportsBook.oneTwoDoubleChance,
                    'commissionSetting.sportsBook.others': memberObj.commissionSetting.sportsBook.others,

                    'commissionSetting.parlay.com': memberObj.commissionSetting.parlay.com,

                    'commissionSetting.step.com2': memberObj.commissionSetting.step.com2,
                    'commissionSetting.step.com3': memberObj.commissionSetting.step.com3,
                    'commissionSetting.step.com4': memberObj.commissionSetting.step.com4,
                    'commissionSetting.step.com5': memberObj.commissionSetting.step.com5,
                    'commissionSetting.step.com6': memberObj.commissionSetting.step.com6,
                    'commissionSetting.step.com7': memberObj.commissionSetting.step.com7,
                    'commissionSetting.step.com8': memberObj.commissionSetting.step.com8,
                    'commissionSetting.step.com9': memberObj.commissionSetting.step.com9,
                    'commissionSetting.step.com10': memberObj.commissionSetting.step.com10,
                    'commissionSetting.step.com11': memberObj.commissionSetting.step.com11,
                    'commissionSetting.step.com12': memberObj.commissionSetting.step.com12,

                    'commissionSetting.casino.sexy': memberObj.commissionSetting.casino.sexy,
                    'commissionSetting.casino.ag': memberObj.commissionSetting.casino.ag,
                    'commissionSetting.casino.sa': memberObj.commissionSetting.casino.sa,
                    'commissionSetting.casino.dg': memberObj.commissionSetting.casino.dg,

                    'commissionSetting.game.slotXO': memberObj.commissionSetting.game.slotXO,

                    'commissionSetting.multi.amb': memberObj.commissionSetting.multi.amb,

                    'commissionSetting.other.m2': memberObj.commissionSetting.other.m2
                }
            };


            // console.log(body)
            AgentGroupModel.update({_id: currentObj._id}, body)
                .exec(function (err, creditResponse) {

                    console.log(creditResponse)
                    if (err) {
                        // callbackWL(err);
                    }
                    else {
                        // callbackWL();
                    }
                });


        });


        cursor.on('end', function () {
            console.log('Done!');
            return res.send({code: 0, message: "success"});
        });

    });


});


router.get('/patchLotto-clone-member', function (req, res) {


    MemberModel.findOne({username_lower: '37pml001'}).exec((err, memberObj) => {


        if (!memberObj) {
            return res.send({code: 999, message: "member not found"});
        }

        const cursor = MemberModel.find({group: mongoose.Types.ObjectId(memberObj.group)})
            .batchSize(1000)
            .lean()
            .cursor();

        cursor.on('data', function (currentObj) {

            console.log(currentObj.username_lower)

            let body = {

                $set: {

                    // 'shareSetting.sportsBook.today.parent': memberObj.shareSetting.sportsBook.today.parent,
                    // 'shareSetting.sportsBook.live.parent': memberObj.shareSetting.sportsBook.live.parent,
                    //
                    // 'shareSetting.step.parlay.parent': memberObj.shareSetting.step.parlay.parent,
                    // 'shareSetting.step.step.parent': memberObj.shareSetting.step.step.parent,
                    //
                    // 'shareSetting.casino.sexy.parent': memberObj.shareSetting.casino.sexy.parent,
                    // 'shareSetting.casino.ag.parent': memberObj.shareSetting.casino.ag.parent,
                    // 'shareSetting.casino.sa.parent': memberObj.shareSetting.casino.sa.parent,
                    // 'shareSetting.casino.dg.parent': memberObj.shareSetting.casino.dg.parent,
                    //
                    // 'shareSetting.game.slotXO.parent': memberObj.shareSetting.game.slotXO.parent,
                    // 'shareSetting.multi.amb.parent': memberObj.shareSetting.multi.amb.parent,
                    // 'shareSetting.other.m2.parent': memberObj.shareSetting.other.m2.parent,


                    'shareSetting.lotto.amb.parent': memberObj.shareSetting.lotto.amb.parent,
                    // 'shareSetting.lotto.pp.parent': memberObj.shareSetting.lotto.pp.parent,
                    'shareSetting.lotto.laos.parent': memberObj.shareSetting.lotto.laos.parent,


                    // 'limitSetting.sportsBook.hdpOuOe.maxPerBet': memberObj.limitSetting.sportsBook.hdpOuOe.maxPerBet,
                    // 'limitSetting.sportsBook.hdpOuOe.maxPerMatch': memberObj.limitSetting.sportsBook.hdpOuOe.maxPerMatch,
                    // 'limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet': memberObj.limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet,
                    // 'limitSetting.sportsBook.oneTwoDoubleChance.maxPerMatch': memberObj.limitSetting.sportsBook.oneTwoDoubleChance.maxPerMatch,
                    // 'limitSetting.sportsBook.others.maxPerBet': memberObj.limitSetting.sportsBook.others.maxPerBet,
                    // 'limitSetting.sportsBook.others.maxPerMatch': memberObj.limitSetting.sportsBook.others.maxPerMatch,
                    // 'limitSetting.sportsBook.outright.maxPerBet': memberObj.limitSetting.sportsBook.outright.maxPerBet,
                    // 'limitSetting.sportsBook.outright.maxPerMatch': memberObj.limitSetting.sportsBook.outright.maxPerMatch,
                    // 'limitSetting.sportsBook.isEnable': memberObj.limitSetting.sportsBook.isEnable,
                    //
                    //
                    // 'limitSetting.step.parlay.maxPerBet': memberObj.limitSetting.step.parlay.maxPerBet,
                    // 'limitSetting.step.parlay.minPerBet': memberObj.limitSetting.step.parlay.minPerBet,
                    // 'limitSetting.step.parlay.maxBetPerDay': memberObj.limitSetting.step.parlay.maxBetPerDay,
                    // 'limitSetting.step.parlay.maxPayPerBill': memberObj.limitSetting.step.parlay.maxPayPerBill,
                    // 'limitSetting.step.parlay.maxMatchPerBet': memberObj.limitSetting.step.parlay.maxMatchPerBet,
                    // 'limitSetting.step.parlay.minMatchPerBet': memberObj.limitSetting.step.parlay.minMatchPerBet,
                    // 'limitSetting.step.parlay.isEnable': memberObj.limitSetting.step.parlay.isEnable,
                    //
                    // 'limitSetting.step.step.maxPerBet': memberObj.limitSetting.step.step.maxPerBet,
                    // 'limitSetting.step.step.minPerBet': memberObj.limitSetting.step.step.minPerBet,
                    // 'limitSetting.step.step.maxBetPerDay': memberObj.limitSetting.step.step.maxBetPerDay,
                    // 'limitSetting.step.step.maxPayPerBill': memberObj.limitSetting.step.step.maxPayPerBill,
                    // 'limitSetting.step.step.maxMatchPerBet': memberObj.limitSetting.step.step.maxMatchPerBet,
                    // 'limitSetting.step.step.minMatchPerBet': memberObj.limitSetting.step.step.minMatchPerBet,
                    // 'limitSetting.step.step.isEnable': memberObj.limitSetting.step.step.isEnable,
                    //
                    // 'limitSetting.casino.isWlEnable': memberObj.limitSetting.casino.isWlEnable,
                    // 'limitSetting.casino.winPerDay': memberObj.limitSetting.casino.winPerDay,
                    // 'limitSetting.casino.sexy.isEnable': memberObj.limitSetting.casino.sexy.isEnable,
                    // 'limitSetting.casino.sexy.limit': memberObj.limitSetting.casino.sexy.limit,
                    // 'limitSetting.casino.ag.isEnable': memberObj.limitSetting.casino.ag.isEnable,
                    // 'limitSetting.casino.ag.limit': memberObj.limitSetting.casino.ag.limit,
                    // 'limitSetting.casino.sa.isEnable': memberObj.limitSetting.casino.sa.isEnable,
                    // 'limitSetting.casino.sa.limit': memberObj.limitSetting.casino.sa.limit,
                    // 'limitSetting.casino.dg.isEnable': memberObj.limitSetting.casino.dg.isEnable,
                    // 'limitSetting.casino.dg.limit': memberObj.limitSetting.casino.dg.limit,
                    //
                    // 'limitSetting.multi.amb.isEnable': memberObj.limitSetting.multi.amb.isEnable,
                    //
                    // 'limitSetting.game.slotXO.isEnable': memberObj.limitSetting.game.slotXO.isEnable,


                    // 'limitSetting.other.m2.maxPerBetHDP': memberObj.limitSetting.other.m2.maxPerBetHDP,
                    // 'limitSetting.other.m2.minPerBetHDP': memberObj.limitSetting.other.m2.minPerBetHDP,
                    // 'limitSetting.other.m2.maxBetPerMatchHDP': memberObj.limitSetting.other.m2.maxBetPerMatchHDP,
                    // 'limitSetting.other.m2.maxPerBet': memberObj.limitSetting.other.m2.maxPerBet,
                    // 'limitSetting.other.m2.minPerBet': memberObj.limitSetting.other.m2.minPerBet,
                    // 'limitSetting.other.m2.maxBetPerDay': memberObj.limitSetting.other.m2.maxBetPerDay,
                    // 'limitSetting.other.m2.maxPayPerBill': memberObj.limitSetting.other.m2.maxPayPerBill,
                    // 'limitSetting.other.m2.maxMatchPerBet': memberObj.limitSetting.other.m2.maxMatchPerBet,
                    // 'limitSetting.other.m2.minMatchPerBet': memberObj.limitSetting.other.m2.minMatchPerBet,
                    // 'limitSetting.other.m2.isEnable': memberObj.limitSetting.other.m2.isEnable,


                    'limitSetting.lotto.laos.hour': memberObj.limitSetting.lotto.laos.hour,
                    'limitSetting.lotto.laos.minute': memberObj.limitSetting.lotto.laos.minute,
                    'limitSetting.lotto.laos.L_4TOP.payout': memberObj.limitSetting.lotto.laos.L_4TOP.payout,
                    'limitSetting.lotto.laos.L_4TOP.discount': memberObj.limitSetting.lotto.laos.L_4TOP.discount,
                    'limitSetting.lotto.laos.L_4TOP.max': memberObj.limitSetting.lotto.laos.L_4TOP.max,
                    //
                    'limitSetting.lotto.laos.L_4TOD.payout': memberObj.limitSetting.lotto.laos.L_4TOD.payout,
                    'limitSetting.lotto.laos.L_4TOD.discount': memberObj.limitSetting.lotto.laos.L_4TOD.discount,
                    'limitSetting.lotto.laos.L_4TOD.max': memberObj.limitSetting.lotto.laos.L_4TOD.max,
                    //
                    'limitSetting.lotto.laos.L_3TOP.payout': memberObj.limitSetting.lotto.laos.L_3TOP.payout,
                    'limitSetting.lotto.laos.L_3TOP.discount': memberObj.limitSetting.lotto.laos.L_3TOP.discount,
                    'limitSetting.lotto.laos.L_3TOP.max': memberObj.limitSetting.lotto.laos.L_3TOP.max,
                    //
                    'limitSetting.lotto.laos.L_3TOD.payout': memberObj.limitSetting.lotto.laos.L_3TOD.payout,
                    'limitSetting.lotto.laos.L_3TOD.discount': memberObj.limitSetting.lotto.laos.L_3TOD.discount,
                    'limitSetting.lotto.laos.L_3TOD.max': memberObj.limitSetting.lotto.laos.L_3TOD.max,
                    //
                    'limitSetting.lotto.laos.L_2FB.payout': memberObj.limitSetting.lotto.laos.L_2FB.payout,
                    'limitSetting.lotto.laos.L_2FB.discount': memberObj.limitSetting.lotto.laos.L_2FB.discount,
                    'limitSetting.lotto.laos.L_2FB.max': memberObj.limitSetting.lotto.laos.L_2FB.max,


                    'limitSetting.lotto.laos.T_4TOP.payout': memberObj.limitSetting.lotto.laos.T_4TOP.payout,
                    'limitSetting.lotto.laos.T_4TOP.discount': memberObj.limitSetting.lotto.laos.T_4TOP.discount,
                    'limitSetting.lotto.laos.T_4TOP.max': memberObj.limitSetting.lotto.laos.T_4TOP.max,
                    //
                    'limitSetting.lotto.laos.T_4TOD.payout': memberObj.limitSetting.lotto.laos.T_4TOD.payout,
                    'limitSetting.lotto.laos.T_4TOD.discount': memberObj.limitSetting.lotto.laos.T_4TOD.discount,
                    'limitSetting.lotto.laos.T_4TOD.max': memberObj.limitSetting.lotto.laos.T_4TOD.max,
                    //
                    'limitSetting.lotto.laos.T_3TOP.payout': memberObj.limitSetting.lotto.laos.T_3TOP.payout,
                    'limitSetting.lotto.laos.T_3TOP.discount': memberObj.limitSetting.lotto.laos.T_3TOP.discount,
                    'limitSetting.lotto.laos.T_3TOP.max': memberObj.limitSetting.lotto.laos.T_3TOP.max,
                    //
                    'limitSetting.lotto.laos.T_3TOD.payout': memberObj.limitSetting.lotto.laos.T_3TOD.payout,
                    'limitSetting.lotto.laos.T_3TOD.discount': memberObj.limitSetting.lotto.laos.T_3TOD.discount,
                    'limitSetting.lotto.laos.T_3TOD.max': memberObj.limitSetting.lotto.laos.T_3TOD.max,

                    'limitSetting.lotto.laos.T_2TOP.payout': memberObj.limitSetting.lotto.laos.T_2TOP.payout,
                    'limitSetting.lotto.laos.T_2TOP.discount': memberObj.limitSetting.lotto.laos.T_2TOP.discount,
                    'limitSetting.lotto.laos.T_2TOP.max': memberObj.limitSetting.lotto.laos.T_2TOP.max,
                    //
                    'limitSetting.lotto.laos.T_2TOD.payout': memberObj.limitSetting.lotto.laos.T_2TOD.payout,
                    'limitSetting.lotto.laos.T_2TOD.discount': memberObj.limitSetting.lotto.laos.T_2TOD.discount,
                    'limitSetting.lotto.laos.T_2TOD.max': memberObj.limitSetting.lotto.laos.T_2TOD.max,
                    //
                    'limitSetting.lotto.laos.T_2BOT.payout': memberObj.limitSetting.lotto.laos.T_2BOT.payout,
                    'limitSetting.lotto.laos.T_2BOT.discount': memberObj.limitSetting.lotto.laos.T_2BOT.discount,
                    'limitSetting.lotto.laos.T_2BOT.max': memberObj.limitSetting.lotto.laos.T_2BOT.max,

                    'limitSetting.lotto.laos.T_1TOP.payout': memberObj.limitSetting.lotto.laos.T_1TOP.payout,
                    'limitSetting.lotto.laos.T_1TOP.discount': memberObj.limitSetting.lotto.laos.T_1TOP.discount,
                    'limitSetting.lotto.laos.T_1TOP.max': memberObj.limitSetting.lotto.laos.T_1TOP.max,
                    //
                    'limitSetting.lotto.laos.T_1BOT.payout': memberObj.limitSetting.lotto.laos.T_1BOT.payout,
                    'limitSetting.lotto.laos.T_1BOT.discount': memberObj.limitSetting.lotto.laos.T_1BOT.discount,
                    'limitSetting.lotto.laos.T_1BOT.max': memberObj.limitSetting.lotto.laos.T_1BOT.max,


                    'limitSetting.lotto.amb.isEnable': memberObj.limitSetting.lotto.amb.isEnable,
                    'limitSetting.lotto.amb.hour': memberObj.limitSetting.lotto.amb.hour,
                    'limitSetting.lotto.amb.minute': memberObj.limitSetting.lotto.amb.minute,

                    'limitSetting.lotto.amb._6TOP.payout': memberObj.limitSetting.lotto.amb._6TOP.payout,
                    'limitSetting.lotto.amb._6TOP.discount': memberObj.limitSetting.lotto.amb._6TOP.discount,
                    'limitSetting.lotto.amb._6TOP.max': memberObj.limitSetting.lotto.amb._6TOP.max,

                    'limitSetting.lotto.amb._5TOP.payout': memberObj.limitSetting.lotto.amb._5TOP.payout,
                    'limitSetting.lotto.amb._5TOP.discount': memberObj.limitSetting.lotto.amb._5TOP.discount,
                    'limitSetting.lotto.amb._5TOP.max': memberObj.limitSetting.lotto.amb._5TOP.max,

                    'limitSetting.lotto.amb._4TOP.payout': memberObj.limitSetting.lotto.amb._4TOP.payout,
                    'limitSetting.lotto.amb._4TOP.discount': memberObj.limitSetting.lotto.amb._4TOP.discount,
                    'limitSetting.lotto.amb._4TOP.max': memberObj.limitSetting.lotto.amb._4TOP.max,

                    'limitSetting.lotto.amb._4TOD.payout': memberObj.limitSetting.lotto.amb._4TOD.payout,
                    'limitSetting.lotto.amb._4TOD.discount': memberObj.limitSetting.lotto.amb._4TOD.discount,
                    'limitSetting.lotto.amb._4TOD.max': memberObj.limitSetting.lotto.amb._4TOD.max,

                    'limitSetting.lotto.amb._3TOP.payout': memberObj.limitSetting.lotto.amb._3TOP.payout,
                    'limitSetting.lotto.amb._3TOP.discount': memberObj.limitSetting.lotto.amb._3TOP.discount,
                    'limitSetting.lotto.amb._3TOP.max': memberObj.limitSetting.lotto.amb._3TOP.max,

                    'limitSetting.lotto.amb._3TOD.payout': memberObj.limitSetting.lotto.amb._3TOD.payout,
                    'limitSetting.lotto.amb._3TOD.discount': memberObj.limitSetting.lotto.amb._3TOD.discount,
                    'limitSetting.lotto.amb._3TOD.max': memberObj.limitSetting.lotto.amb._3TOD.max,

                    'limitSetting.lotto.amb._3BOT.payout': memberObj.limitSetting.lotto.amb._3BOT.payout,
                    'limitSetting.lotto.amb._3BOT.discount': memberObj.limitSetting.lotto.amb._3BOT.discount,
                    'limitSetting.lotto.amb._3BOT.max': memberObj.limitSetting.lotto.amb._3BOT.max,

                    'limitSetting.lotto.amb._3TOP_OE.payout': memberObj.limitSetting.lotto.amb._3TOP_OE.payout,
                    'limitSetting.lotto.amb._3TOP_OE.discount': memberObj.limitSetting.lotto.amb._3TOP_OE.discount,
                    'limitSetting.lotto.amb._3TOP_OE.max': memberObj.limitSetting.lotto.amb._3TOP_OE.max,

                    'limitSetting.lotto.amb._3TOP_OU.payout': memberObj.limitSetting.lotto.amb._3TOP_OU.payout,
                    'limitSetting.lotto.amb._3TOP_OU.discount': memberObj.limitSetting.lotto.amb._3TOP_OU.discount,
                    'limitSetting.lotto.amb._3TOP_OU.max': memberObj.limitSetting.lotto.amb._3TOP_OU.max,

                    'limitSetting.lotto.amb._2TOP.payout': memberObj.limitSetting.lotto.amb._2TOP.payout,
                    'limitSetting.lotto.amb._2TOP.discount': memberObj.limitSetting.lotto.amb._2TOP.discount,
                    'limitSetting.lotto.amb._2TOP.max': memberObj.limitSetting.lotto.amb._2TOP.max,

                    'limitSetting.lotto.amb._2TOD.payout': memberObj.limitSetting.lotto.amb._2TOD.payout,
                    'limitSetting.lotto.amb._2TOD.discount': memberObj.limitSetting.lotto.amb._2TOD.discount,
                    'limitSetting.lotto.amb._2TOD.max': memberObj.limitSetting.lotto.amb._2TOD.max,

                    'limitSetting.lotto.amb._2BOT.payout': memberObj.limitSetting.lotto.amb._2BOT.payout,
                    'limitSetting.lotto.amb._2BOT.discount': memberObj.limitSetting.lotto.amb._2BOT.discount,
                    'limitSetting.lotto.amb._2BOT.max': memberObj.limitSetting.lotto.amb._2BOT.max,

                    'limitSetting.lotto.amb._2TOP_OE.payout': memberObj.limitSetting.lotto.amb._2TOP_OE.payout,
                    'limitSetting.lotto.amb._2TOP_OE.discount': memberObj.limitSetting.lotto.amb._2TOP_OE.discount,
                    'limitSetting.lotto.amb._2TOP_OE.max': memberObj.limitSetting.lotto.amb._2TOP_OE.max,

                    'limitSetting.lotto.amb._2TOP_OU.payout': memberObj.limitSetting.lotto.amb._2TOP_OU.payout,
                    'limitSetting.lotto.amb._2TOP_OU.discount': memberObj.limitSetting.lotto.amb._2TOP_OU.discount,
                    'limitSetting.lotto.amb._2TOP_OU.max': memberObj.limitSetting.lotto.amb._2TOP_OU.max,

                    'limitSetting.lotto.amb._2BOT_OE.payout': memberObj.limitSetting.lotto.amb._2BOT_OE.payout,
                    'limitSetting.lotto.amb._2BOT_OE.discount': memberObj.limitSetting.lotto.amb._2BOT_OE.discount,
                    'limitSetting.lotto.amb._2BOT_OE.max': memberObj.limitSetting.lotto.amb._2BOT_OE.max,

                    'limitSetting.lotto.amb._2BOT_OU.payout': memberObj.limitSetting.lotto.amb._2BOT_OU.payout,
                    'limitSetting.lotto.amb._2BOT_OU.discount': memberObj.limitSetting.lotto.amb._2BOT_OU.discount,
                    'limitSetting.lotto.amb._2BOT_OU.max': memberObj.limitSetting.lotto.amb._2BOT_OU.max,

                    'limitSetting.lotto.amb._1TOP.payout': memberObj.limitSetting.lotto.amb._1TOP.payout,
                    'limitSetting.lotto.amb._1TOP.discount': memberObj.limitSetting.lotto.amb._1TOP.discount,
                    'limitSetting.lotto.amb._1TOP.max': memberObj.limitSetting.lotto.amb._1TOP.max,

                    'limitSetting.lotto.amb._1BOT.payout': memberObj.limitSetting.lotto.amb._1BOT.payout,
                    'limitSetting.lotto.amb._1BOT.discount': memberObj.limitSetting.lotto.amb._1BOT.discount,
                    'limitSetting.lotto.amb._1BOT.max': memberObj.limitSetting.lotto.amb._1BOT.max,


                    // 'limitSetting.lotto.pp.isEnable': memberObj.limitSetting.lotto.pp.isEnable,
                    // 'limitSetting.lotto.pp.hour': memberObj.limitSetting.lotto.pp.hour,
                    // 'limitSetting.lotto.pp.minute': memberObj.limitSetting.lotto.pp.minute,
                    //
                    // 'limitSetting.lotto.pp._6TOP.payout': memberObj.limitSetting.lotto.pp._6TOP.payout,
                    // 'limitSetting.lotto.pp._6TOP.discount': memberObj.limitSetting.lotto.pp._6TOP.discount,
                    // 'limitSetting.lotto.pp._6TOP.max': memberObj.limitSetting.lotto.pp._6TOP.max,
                    //
                    // 'limitSetting.lotto.pp._5TOP.payout': memberObj.limitSetting.lotto.pp._5TOP.payout,
                    // 'limitSetting.lotto.pp._5TOP.discount': memberObj.limitSetting.lotto.pp._5TOP.discount,
                    // 'limitSetting.lotto.pp._5TOP.max': memberObj.limitSetting.lotto.pp._5TOP.max,
                    //
                    // 'limitSetting.lotto.pp._4TOP.payout': memberObj.limitSetting.lotto.pp._4TOP.payout,
                    // 'limitSetting.lotto.pp._4TOP.discount': memberObj.limitSetting.lotto.pp._4TOP.discount,
                    // 'limitSetting.lotto.pp._4TOP.max': memberObj.limitSetting.lotto.pp._4TOP.max,
                    //
                    // 'limitSetting.lotto.pp._4TOD.payout': memberObj.limitSetting.lotto.pp._4TOD.payout,
                    // 'limitSetting.lotto.pp._4TOD.discount': memberObj.limitSetting.lotto.pp._4TOD.discount,
                    // 'limitSetting.lotto.pp._4TOD.max': memberObj.limitSetting.lotto.pp._4TOD.max,
                    //
                    // 'limitSetting.lotto.pp._3TOP.payout': memberObj.limitSetting.lotto.pp._3TOP.payout,
                    // 'limitSetting.lotto.pp._3TOP.discount': memberObj.limitSetting.lotto.pp._3TOP.discount,
                    // 'limitSetting.lotto.pp._3TOP.max': memberObj.limitSetting.lotto.pp._3TOP.max,
                    //
                    // 'limitSetting.lotto.pp._3TOD.payout': memberObj.limitSetting.lotto.pp._3TOD.payout,
                    // 'limitSetting.lotto.pp._3TOD.discount': memberObj.limitSetting.lotto.pp._3TOD.discount,
                    // 'limitSetting.lotto.pp._3TOD.max': memberObj.limitSetting.lotto.pp._3TOD.max,
                    //
                    // 'limitSetting.lotto.pp._3BOT.payout': memberObj.limitSetting.lotto.pp._3BOT.payout,
                    // 'limitSetting.lotto.pp._3BOT.discount': memberObj.limitSetting.lotto.pp._3BOT.discount,
                    // 'limitSetting.lotto.pp._3BOT.max': memberObj.limitSetting.lotto.pp._3BOT.max,
                    //
                    // 'limitSetting.lotto.pp._3TOP_OE.payout': memberObj.limitSetting.lotto.pp._3TOP_OE.payout,
                    // 'limitSetting.lotto.pp._3TOP_OE.discount': memberObj.limitSetting.lotto.pp._3TOP_OE.discount,
                    // 'limitSetting.lotto.pp._3TOP_OE.max': memberObj.limitSetting.lotto.pp._3TOP_OE.max,
                    //
                    // 'limitSetting.lotto.pp._3TOP_OU.payout': memberObj.limitSetting.lotto.pp._3TOP_OU.payout,
                    // 'limitSetting.lotto.pp._3TOP_OU.discount': memberObj.limitSetting.lotto.pp._3TOP_OU.discount,
                    // 'limitSetting.lotto.pp._3TOP_OU.max': memberObj.limitSetting.lotto.pp._3TOP_OU.max,
                    //
                    // 'limitSetting.lotto.pp._2TOP.payout': memberObj.limitSetting.lotto.pp._2TOP.payout,
                    // 'limitSetting.lotto.pp._2TOP.discount': memberObj.limitSetting.lotto.pp._2TOP.discount,
                    // 'limitSetting.lotto.pp._2TOP.max': memberObj.limitSetting.lotto.pp._2TOP.max,
                    //
                    // 'limitSetting.lotto.pp._2TOD.payout': memberObj.limitSetting.lotto.pp._2TOD.payout,
                    // 'limitSetting.lotto.pp._2TOD.discount': memberObj.limitSetting.lotto.pp._2TOD.discount,
                    // 'limitSetting.lotto.pp._2TOD.max': memberObj.limitSetting.lotto.pp._2TOD.max,
                    //
                    // 'limitSetting.lotto.pp._2BOT.payout': memberObj.limitSetting.lotto.pp._2BOT.payout,
                    // 'limitSetting.lotto.pp._2BOT.discount': memberObj.limitSetting.lotto.pp._2BOT.discount,
                    // 'limitSetting.lotto.pp._2BOT.max': memberObj.limitSetting.lotto.pp._2BOT.max,
                    //
                    // 'limitSetting.lotto.pp._2TOP_OE.payout': memberObj.limitSetting.lotto.pp._2TOP_OE.payout,
                    // 'limitSetting.lotto.pp._2TOP_OE.discount': memberObj.limitSetting.lotto.pp._2TOP_OE.discount,
                    // 'limitSetting.lotto.pp._2TOP_OE.max': memberObj.limitSetting.lotto.pp._2TOP_OE.max,
                    //
                    // 'limitSetting.lotto.pp._2TOP_OU.payout': memberObj.limitSetting.lotto.pp._2TOP_OU.payout,
                    // 'limitSetting.lotto.pp._2TOP_OU.discount': memberObj.limitSetting.lotto.pp._2TOP_OU.discount,
                    // 'limitSetting.lotto.pp._2TOP_OU.max': memberObj.limitSetting.lotto.pp._2TOP_OU.max,
                    //
                    // 'limitSetting.lotto.pp._2BOT_OE.payout': memberObj.limitSetting.lotto.pp._2BOT_OE.payout,
                    // 'limitSetting.lotto.pp._2BOT_OE.discount': memberObj.limitSetting.lotto.pp._2BOT_OE.discount,
                    // 'limitSetting.lotto.pp._2BOT_OE.max': memberObj.limitSetting.lotto.pp._2BOT_OE.max,
                    //
                    // 'limitSetting.lotto.pp._2BOT_OU.payout': memberObj.limitSetting.lotto.pp._2BOT_OU.payout,
                    // 'limitSetting.lotto.pp._2BOT_OU.discount': memberObj.limitSetting.lotto.pp._2BOT_OU.discount,
                    // 'limitSetting.lotto.pp._2BOT_OU.max': memberObj.limitSetting.lotto.pp._2BOT_OU.max,
                    //
                    // 'limitSetting.lotto.pp._1TOP.payout': memberObj.limitSetting.lotto.pp._1TOP.payout,
                    // 'limitSetting.lotto.pp._1TOP.discount': memberObj.limitSetting.lotto.pp._1TOP.discount,
                    // 'limitSetting.lotto.pp._1TOP.max': memberObj.limitSetting.lotto.pp._1TOP.max,
                    //
                    // 'limitSetting.lotto.pp._1BOT.payout': memberObj.limitSetting.lotto.pp._1BOT.payout,
                    // 'limitSetting.lotto.pp._1BOT.discount': memberObj.limitSetting.lotto.pp._1BOT.discount,
                    // 'limitSetting.lotto.pp._1BOT.max': memberObj.limitSetting.lotto.pp._1BOT.max,
                    //
                    // 'commissionSetting.sportsBook.typeHdpOuOe': memberObj.commissionSetting.sportsBook.typeHdpOuOe,
                    // 'commissionSetting.sportsBook.hdpOuOe': memberObj.commissionSetting.sportsBook.hdpOuOe,
                    // 'commissionSetting.sportsBook.oneTwoDoubleChance': memberObj.commissionSetting.sportsBook.oneTwoDoubleChance,
                    // 'commissionSetting.sportsBook.others': memberObj.commissionSetting.sportsBook.others,
                    //
                    // 'commissionSetting.parlay.com': memberObj.commissionSetting.parlay.com,
                    //
                    // 'commissionSetting.step.com2': memberObj.commissionSetting.step.com2,
                    // 'commissionSetting.step.com3': memberObj.commissionSetting.step.com3,
                    // 'commissionSetting.step.com4': memberObj.commissionSetting.step.com4,
                    // 'commissionSetting.step.com5': memberObj.commissionSetting.step.com5,
                    // 'commissionSetting.step.com6': memberObj.commissionSetting.step.com6,
                    // 'commissionSetting.step.com7': memberObj.commissionSetting.step.com7,
                    // 'commissionSetting.step.com8': memberObj.commissionSetting.step.com8,
                    // 'commissionSetting.step.com9': memberObj.commissionSetting.step.com9,
                    // 'commissionSetting.step.com10': memberObj.commissionSetting.step.com10,
                    // 'commissionSetting.step.com11': memberObj.commissionSetting.step.com11,
                    // 'commissionSetting.step.com12': memberObj.commissionSetting.step.com12,
                    //
                    // 'commissionSetting.casino.sexy': memberObj.commissionSetting.casino.sexy,
                    // 'commissionSetting.casino.ag': memberObj.commissionSetting.casino.ag,
                    // 'commissionSetting.casino.sa': memberObj.commissionSetting.casino.sa,
                    // 'commissionSetting.casino.dg': memberObj.commissionSetting.casino.dg,
                    //
                    // 'commissionSetting.game.slotXO': memberObj.commissionSetting.game.slotXO,
                    //
                    // 'commissionSetting.multi.amb': memberObj.commissionSetting.multi.amb,
                    //
                    // 'commissionSetting.other.m2': memberObj.commissionSetting.other.m2
                }
            };


            // console.log(body)
            MemberModel.update({_id: currentObj._id}, body)
                .exec(function (err, creditResponse) {

                    console.log(creditResponse)
                    if (err) {
                        // callbackWL(err);
                    }
                    else {
                        // callbackWL();
                    }
                });


        });


        cursor.on('end', function () {
            console.log('Done!');
            return res.send({code: 0, message: "success"});
        });

    });


});


// router.get('/190', function (req, res) {
//
//
//     let condition = {
//         'createdDate': {
//             "$gte": new Date(moment().utc(true).format('2019-10-01T00:30:00.000'))
//         },
//         game: 'LOTTO',
//         status: 'RUNNING',
//         'lotto.amb.gameId': '5d9aefbad75c587c0a674964',
//         // 'betId':{$in:["BET15709583307717755046","BET15709583308429799327"]}
//     };
//
//
//     BetTransactionModel.find(condition).sort({_id:1}).exec(function (err, response) {
//         console.log(response.length)
//
//
//         let tasks = [];
//
//         _.forEach(response, (obj) => {
//             tasks.push(xxx(obj));
//         });
//
//         console.log('task : ',tasks.length);
//
//         function xxx(currentObj) {
//
//             return function (callbackTask) {
//                 MemberService.findById(currentObj.memberId, '', function (err, memberInfo) {
//
//
//                     // console.log('before : ', currentObj.commission)
//
//                     let body = currentObj;
//
//
//                     AgentGroupModel.findById(memberInfo.group).select('_id type parentId shareSetting limitSetting ambLotto')
//                         .populate({
//                             path: 'parentId',
//                             select: '_id type parentId shareSetting limitSetting name ambLotto',
//                             options: {lean: true},
//                             populate: {
//                                 path: 'parentId',
//                                 select: '_id type parentId shareSetting limitSetting name ambLotto',
//                                 options: {lean: true},
//                                 populate: {
//                                     path: 'parentId',
//                                     select: '_id type parentId shareSetting limitSetting name ambLotto',
//                                     options: {lean: true},
//                                     populate: {
//                                         path: 'parentId',
//                                         select: '_id type parentId shareSetting limitSetting name ambLotto',
//                                         options: {lean: true},
//                                         populate: {
//                                             path: 'parentId',
//                                             select: '_id type parentId shareSetting limitSetting name ambLotto',
//                                             options: {lean: true},
//                                             populate: {
//                                                 path: 'parentId',
//                                                 select: '_id type parentId shareSetting limitSetting name ambLotto',
//                                                 options: {lean: true},
//                                             },
//                                         },
//                                     },
//                                 },
//                             },
//                         }).lean().exec((err, agentGroups) => {
//                         if (err) {
//                             return res.send({code: 999, message: "err"});
//                         } else {
//
//                             prepareCommission(currentObj._id,memberInfo, body, agentGroups, null, 0, 0);
//
//                             function prepareCommission(_id,memberInfo, body, currentAgent, overAgent, remainingBalance, overAllShare) {
//
//
//                                 if (currentAgent && (currentAgent.type === 'SUPER_ADMIN' || currentAgent.parentId)) {
//
//
//                                     // console.log('==============================================');
//                                     // console.log('process : ' + currentAgent.type + ' : ' + currentAgent.name);
//                                     // // console.log('currentAgent : ',currentAgent)
//                                     // // console.log('overAgent : ',overAgent)
//
//                                     const betType = body.lotto.amb.betType;
//                                     let betNumber = body.lotto.amb.betNumber;
//                                     const betAmount = body.lotto.amb.betAmount;
//
//
//                                     if (betType == '_2TOD' || betType == '_3TOD' || betType == '_4TOD') {
//                                         betNumber = LottoService.sortTodNumber(betNumber);
//                                     }
//
//                                     let conditions = {};
//                                     conditions['agent'] = mongoose.Types.ObjectId(currentAgent._id);
//                                     // conditions['po.'+body.lotto.amb.betType+'.bets.betNumber'] = body.lotto.amb.betNumber;
//
//                                     AgentLottoLimitModel.findOne(conditions, 'hour min po.' + betType, function (err, agentLimit) {
//
//
//                                         if (!overAgent) {
//                                             overAgent = {};
//                                             overAgent.type = 'member';
//                                             overAgent.shareSetting = {};
//                                             overAgent.shareSetting.lotto = {};
//                                             overAgent.shareSetting.lotto.amb = {};
//                                             overAgent.shareSetting.lotto.amb.parent = memberInfo.shareSetting.lotto.amb.parent;
//                                             overAgent.shareSetting.lotto.amb.own = 0;
//                                             overAgent.shareSetting.lotto.amb.remaining = 0;
//
//                                             body.commission.member = {};
//                                             body.commission.member.parent = memberInfo.shareSetting.lotto.amb.parent;
//                                             body.commission.member.commission = LottoService.getLottoCommission(memberInfo, body.lotto.amb.betType);
//                                             body.commission.member.payout = LottoService.getLottoPayout(memberInfo, body.lotto.amb.betType);
//
//                                         }
//
//
//                                         // console.log('');
//                                         // console.log('---- setting ----');
//                                         // console.log('ส่วนแบ่งที่ได้มามีทั้งหมด : ' + currentAgent.shareSetting.lotto.amb.own + ' %');
//                                         // console.log('ขอส่วนแบ่งจาก : ' + overAgent.type + ' ' + overAgent.shareSetting.lotto.amb.parent + ' %');
//                                         // console.log('ให้ส่วนแบ่ง : ' + overAgent.type + ' ที่เหลือไป ' + overAgent.shareSetting.lotto.amb.own + ' %');
//                                         // console.log('มีการตั้งค่าremaining จาก : ' + overAgent.type + ' : ' + overAgent.shareSetting.lotto.amb.remaining + ' %');
//                                         // console.log('ได้remaining จาก : ' + overAgent.type + ' : ' + remainingBalance + ' บาท');
//                                         // console.log('ได้remaining จาก : ' + overAgent.type + ' : ' + remainingBalancePercent + ' %');
//                                         // console.log('---- end setting ----');
//                                         // console.log('');
//
//
//                                         // console.log('และได้รับ remaining ไว้ : '+overAgent.shareSetting.lotto.amb.remaining+' %');
//
//                                         let currentPercentReceive = overAgent.shareSetting.lotto.amb.parent;
//                                         let currentReceiveAmount = (betAmount * NumberUtils.convertPercent(currentPercentReceive));
//                                         let remainingBalancePercent = NumberUtils.findBetAmountPercentage(remainingBalance, betAmount);
//
//                                         // let totalRemaining = remaining;
//
//
//                                         // console.log('มีค่า remainingBalance จาก : ' + overAgent.type + ' : ' + remainingBalance);
//                                         // console.log('ค่า remainingBalance คิดเป็น % =  ' + NumberUtils.findBetAmountPercentage(remainingBalance, betAmount) + ' %');
//
//
//                                         if (overAgent.shareSetting.lotto.amb.remaining > 0) {
//                                             // console.log()
//                                             // console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');
//                                             // console.log('overAgent.shareSetting.lotto.amb.remaining : ', overAgent.shareSetting.lotto.amb.remaining);
//                                             // console.log('remainingBalancePercent : ', remainingBalancePercent);
//
//                                             if (overAgent.shareSetting.lotto.amb.remaining >= remainingBalancePercent) {
//                                                 // console.log('รับ remaining ทั้งหมดไว้: ' + remainingBalancePercent + ' %');
//                                                 currentPercentReceive += remainingBalancePercent;
//                                                 currentReceiveAmount += remainingBalance;
//                                                 remainingBalance = 0;
//                                             } else {
//
//                                                 // console.log('หักค่า remaining ที่ได้รับมากับที่เซตรับไว้ = ' + remainingBalancePercent + ' - ' + overAgent.shareSetting.lotto.amb.remaining + ' = ' + (remainingBalancePercent - overAgent.shareSetting.lotto.amb.remaining));
//                                                 remainingBalancePercent = remainingBalancePercent - overAgent.shareSetting.lotto.amb.remaining;
//                                                 currentPercentReceive += overAgent.shareSetting.lotto.amb.remaining;
//                                                 currentReceiveAmount = (betAmount * NumberUtils.convertPercent(currentPercentReceive));
//                                                 remainingBalance = (betAmount * NumberUtils.convertPercent(remainingBalancePercent));
//
//                                             }
//
//                                         }
//                                         // console.log('remainingBalancePercent : ', remainingBalancePercent)
//                                         // console.log('remaining balance ที่ถูกยกมาคงเหลือ : ', remainingBalance);
//
//                                         // else {
//                                         //     currentPercentReceive += NumberUtils.findBetAmountPercentage(remainingBalance, betAmount);
//                                         //     currentReceiveAmount += remainingBalance;
//                                         // }
//
//                                         // console.log('% ที่ได้ : ' + currentPercentReceive);
//                                         // console.log('เป็นจำนวนเงิน : ' + currentReceiveAmount);
//                                         //
//                                         // console.log('สรุปรับ % ทั้งหมด รวมเงิน remaining : ' + currentPercentReceive + ' %');
//                                         // console.log('สรุปรับ % ทั้งหมด รวมเงิน remaining เป็นเงิน  : ' + currentReceiveAmount);
//
//
//                                         let totalRemainingAmount = currentReceiveAmount;
//
//                                         let limitObj = agentLimit.po[betType];
//
//                                         let betNumberObj = _.filter(limitObj.bets, filterItem => {
//                                             return filterItem.betNumber === betNumber;
//                                         })[0];
//
//                                         // console.log('------- :: limit : ' + limitObj.limit + ' ' + betNumberObj);
//
//                                         let finalReceiveAmount = 0;
//
//
//                                         // console.log(currentAgent.limitSetting.lotto.amb)
//                                         // console.log('agent Time : ', agentReceiveTime);
//                                         if ((currentAgent.type !== 'SUPER_ADMIN')) {
//                                             //set limit
//                                             // let inTime = true;
//                                             // if(currentAgent.type === 'SHARE_HOLDER'){
//                                             //     inTime = true;
//                                             // }
//                                             // console.log('limit : ', limitObj.limit);
//                                             // console.log('currentPercentReceive : ', currentPercentReceive);
//                                             // console.log('inTime : ', inTime);
//                                             if (limitObj.limit > 0 && currentPercentReceive > 0) {
//
//
//                                                 let increaseAmountValue = 0;
//
//                                                 if (betNumberObj && betNumberObj.betAmount >= limitObj.limit) {
//                                                     //limit is full
//                                                     // console.log('case 1');
//                                                     finalReceiveAmount = 0;
//                                                     totalRemainingAmount = currentReceiveAmount;
//                                                     currentPercentReceive = 0;
//                                                     currentReceiveAmount = 0;
//                                                 } else {
//
//                                                     if (!betNumberObj) {
//                                                         betNumberObj = {
//                                                             betAmount: 0,
//                                                             betNumber: betNumber,
//                                                         }
//                                                     }
//
//                                                     // console.log('limit : ' + limitObj.limit + ' , totalBetNumberAmount : ' + betNumberObj.betAmount + ' = remain ' + (limitObj.limit - (betNumberObj.betAmount) ))
//
//
//                                                     let currentTotalBetNumberAmount = betNumberObj.betAmount + currentReceiveAmount;
//
//                                                     // console.log('currentReceiveAmount : ', currentReceiveAmount);
//                                                     // console.log('currentTotalBetNumberAmount : ', currentTotalBetNumberAmount)
//                                                     if (currentTotalBetNumberAmount > limitObj.limit) {
//                                                         // console.log('case 2');
//
//                                                         increaseAmountValue = limitObj.limit - betNumberObj.betAmount;
//                                                         finalReceiveAmount = increaseAmountValue;
//
//                                                         totalRemainingAmount = currentReceiveAmount - increaseAmountValue;
//                                                         // console.log('totalReceiveAmount : ', finalReceiveAmount);
//
//                                                         currentPercentReceive = NumberUtils.findBetAmountPercentage(finalReceiveAmount, betAmount);
//
//                                                     } else {
//                                                         // console.log('case 3');
//                                                         finalReceiveAmount = currentReceiveAmount;
//                                                         increaseAmountValue = currentReceiveAmount;
//                                                         totalRemainingAmount = 0;
//                                                     }
//
//                                                     // console.log('currentReceiveAmount : ', currentReceiveAmount);
//                                                     // console.log('increaseAmountValue : ', increaseAmountValue);
//                                                     // console.log('totalReceiveAmount : ', finalReceiveAmount);
//
//                                                     updateBetLimit(currentAgent._id, betType, betNumber, increaseAmountValue, (err, response) => {
//
//                                                     });
//                                                 }
//
//                                                 // totalReceiveAmount -
//
//
//                                             } else {
//                                                 // console.log('nooooooooooo')
//                                                 //unset limit limit = 0 shareReceive = 0 outOfTime
//                                                 totalRemainingAmount = currentReceiveAmount;
//                                                 finalReceiveAmount = 0;
//                                                 currentPercentReceive = 0;
//                                             }
//                                         } else {
//                                             // superadmin
//                                             finalReceiveAmount = currentReceiveAmount + remainingBalance;
//                                             currentPercentReceive = NumberUtils.findBetAmountPercentage(finalReceiveAmount, betAmount)
//                                             totalRemainingAmount = 0;
//                                         }
//
//                                         overAllShare = overAllShare + currentPercentReceive;
//
//                                         let unUseShare = currentAgent.shareSetting.lotto.amb.own -
//                                             (overAgent.shareSetting.lotto.amb.parent + overAgent.shareSetting.lotto.amb.own);
//
//                                         let unUseShareBalance = betAmount * NumberUtils.convertPercent(unUseShare);
//
//
//                                         totalRemainingAmount = (unUseShareBalance + totalRemainingAmount) + remainingBalance;
//
//
//                                         // console.log('ยอดแทง ' + body.amount + ' ได้ทั้งหมด : ' + currentPercentReceive + ' %');
//                                         //
//                                         // console.log('รวม % + % remaining  จะเป็นสัดส่วนที่ได้ทั้งหมด = ' + finalReceiveAmount);
//                                         // console.log('ค่าที่จะถูกคืนกลับไป (remainingBalance) : ' + totalRemainingAmount);
//                                         // console.log('เหลือส่วนแบ่งที่ไม่ได้ใช้ : ' + unUseShare + ' %');
//                                         //
//                                         // console.log('จำนวน % ที่ถูกแบ่งไปแล้วทั้งหมด : ', overAllShare);
//
//                                         //set commission
//                                         let agentCommission = LottoService.getLottoCommission(currentAgent, body.lotto.amb.betType);
//
//
//                                         switch (currentAgent.type) {
//                                             case 'SUPER_ADMIN':
//                                                 body.commission.superAdmin = {};
//                                                 body.commission.superAdmin.group = currentAgent._id;
//                                                 body.commission.superAdmin.parent = currentAgent.shareSetting.lotto.amb.parent;
//                                                 body.commission.superAdmin.own = currentAgent.shareSetting.lotto.amb.own;
//                                                 body.commission.superAdmin.remaining = currentAgent.shareSetting.lotto.amb.remaining;
//                                                 body.commission.superAdmin.min = currentAgent.shareSetting.lotto.amb.min;
//                                                 body.commission.superAdmin.shareReceive = Math.abs(currentPercentReceive);
//                                                 body.commission.superAdmin.commission = agentCommission;
//                                                 body.commission.superAdmin.amount = finalReceiveAmount;
//                                                 body.commission.superAdmin.payout = LottoService.getLottoPayout(currentAgent, body.lotto.amb.betType);
//                                                 break;
//                                             case 'COMPANY':
//                                                 body.commission.company = {};
//                                                 body.commission.company.parentGroup = currentAgent.parentId;
//                                                 body.commission.company.group = currentAgent._id;
//                                                 body.commission.company.parent = currentAgent.shareSetting.lotto.amb.parent;
//                                                 body.commission.company.own = currentAgent.shareSetting.lotto.amb.own;
//                                                 body.commission.company.remaining = currentAgent.shareSetting.lotto.amb.remaining;
//                                                 body.commission.company.min = currentAgent.shareSetting.lotto.amb.min;
//                                                 body.commission.company.shareReceive = Math.abs(currentPercentReceive);
//                                                 body.commission.company.commission = agentCommission;
//                                                 body.commission.company.amount = finalReceiveAmount;
//                                                 body.commission.company.payout = LottoService.getLottoPayout(currentAgent, body.lotto.amb.betType);
//                                                 break;
//                                             case 'SHARE_HOLDER':
//                                                 body.commission.shareHolder = {};
//                                                 body.commission.shareHolder.parentGroup = currentAgent.parentId;
//                                                 body.commission.shareHolder.group = currentAgent._id;
//                                                 body.commission.shareHolder.parent = currentAgent.shareSetting.lotto.amb.parent;
//                                                 body.commission.shareHolder.own = currentAgent.shareSetting.lotto.amb.own;
//                                                 body.commission.shareHolder.remaining = currentAgent.shareSetting.lotto.amb.remaining;
//                                                 body.commission.shareHolder.min = currentAgent.shareSetting.lotto.amb.min;
//                                                 body.commission.shareHolder.shareReceive = currentPercentReceive;
//                                                 body.commission.shareHolder.commission = agentCommission;
//                                                 body.commission.shareHolder.amount = finalReceiveAmount;
//                                                 body.commission.shareHolder.payout = LottoService.getLottoPayout(currentAgent, body.lotto.amb.betType);
//                                                 break;
//                                             case 'SENIOR':
//                                                 body.commission.senior = {};
//                                                 body.commission.senior.parentGroup = currentAgent.parentId;
//                                                 body.commission.senior.group = currentAgent._id;
//                                                 body.commission.senior.parent = currentAgent.shareSetting.lotto.amb.parent;
//                                                 body.commission.senior.own = currentAgent.shareSetting.lotto.amb.own;
//                                                 body.commission.senior.remaining = currentAgent.shareSetting.lotto.amb.remaining;
//                                                 body.commission.senior.min = currentAgent.shareSetting.lotto.amb.min;
//                                                 body.commission.senior.shareReceive = currentPercentReceive;
//                                                 body.commission.senior.commission = agentCommission;
//                                                 body.commission.senior.amount = finalReceiveAmount;
//                                                 body.commission.senior.payout = LottoService.getLottoPayout(currentAgent, body.lotto.amb.betType);
//                                                 break;
//                                             case 'MASTER_AGENT':
//                                                 body.commission.masterAgent = {};
//                                                 body.commission.masterAgent.parentGroup = currentAgent.parentId;
//                                                 body.commission.masterAgent.group = currentAgent._id;
//                                                 body.commission.masterAgent.parent = currentAgent.shareSetting.lotto.amb.parent;
//                                                 body.commission.masterAgent.own = currentAgent.shareSetting.lotto.amb.own;
//                                                 body.commission.masterAgent.remaining = currentAgent.shareSetting.lotto.amb.remaining;
//                                                 body.commission.masterAgent.min = currentAgent.shareSetting.lotto.amb.min;
//                                                 body.commission.masterAgent.shareReceive = currentPercentReceive;
//                                                 body.commission.masterAgent.commission = agentCommission;
//                                                 body.commission.masterAgent.amount = finalReceiveAmount;
//                                                 body.commission.masterAgent.payout = LottoService.getLottoPayout(currentAgent, body.lotto.amb.betType);
//                                                 break;
//                                             case 'AGENT':
//                                                 body.commission.agent = {};
//                                                 body.commission.agent.parentGroup = currentAgent.parentId;
//                                                 body.commission.agent.group = currentAgent._id;
//                                                 body.commission.agent.parent = currentAgent.shareSetting.lotto.amb.parent;
//                                                 body.commission.agent.own = currentAgent.shareSetting.lotto.amb.own;
//                                                 body.commission.agent.remaining = currentAgent.shareSetting.lotto.amb.remaining;
//                                                 body.commission.agent.min = currentAgent.shareSetting.lotto.amb.min;
//                                                 body.commission.agent.shareReceive = currentPercentReceive;
//                                                 body.commission.agent.commission = agentCommission;
//                                                 body.commission.agent.amount = finalReceiveAmount;
//                                                 body.commission.agent.payout = LottoService.getLottoPayout(currentAgent, body.lotto.amb.betType);
//                                                 break;
//                                         }
//
//                                         prepareCommission(_id,memberInfo, body, currentAgent.parentId, currentAgent, totalRemainingAmount, overAllShare);
//                                     });
//
//
//                                 } else {
//                                     // console.log('after : ', body.commission)
//
//                                     let updateBody = {
//
//                                         $set: {
//                                             "commission":body.commission
//                                             // "body.commission.agent.parentGroup": body.commission.agent.parentGroup,
//                                             // "body.commission.agent.group": body.commission.agent.group,
//                                             // "body.commission.agent.parent": body.commission.agent.parent,
//                                             // "body.commission.agent.own": body.commission.agent.own,
//                                             // "body.commission.agent.remaining": body.commission.agent.remaining,
//                                             // "body.commission.agent.min": body.commission.agent.min,
//                                             // "body.commission.agent.shareReceive": body.commission.agent.shareReceive,
//                                             // "body.commission.agent.commission": body.commission.agent.commission,
//                                             // "body.commission.agent.amount": body.commission.agent.amount,
//                                             // "body.commission.agent.payout": body.commission.agent.payout
//
//                                         }
//                                     };
//
//
//                                     // console.log('updateBody : ',updateBody)
//
//
//
//                                     BetTransactionModel.update({_id: mongoose.Types.ObjectId(_id)}, updateBody).exec(function (err, creditResponse) {
//
//                                     console.log(creditResponse)
//                                     callbackTask();
//                                     });
//                                 }
//
//                             }
//
//                         }
//                     });
//
//                 });
//             }
//         }
//
//         // console.log(tasks[0])
//         // return res.send(
//         //     {
//         //         code: 0,
//         //         message: "success",
//         //         result: 'success'
//         //     }
//         // );
//
//         async.waterfall(tasks, function (err, result) {
//             // result now equals 'done'
//             console.log(result)
//
//             return res.send(
//                 {
//                     code: 0,
//                     message: "success",
//                     result: 'success'
//                 }
//             );
//         });
//
//     });
//
//
//
//
//     function updateBetLimit(agentId, betType, betNumber, betAmount, callback) {
//         // console.log('::::: updateBetLimit ::::: ' + betType + ' : betNumber = ' + betNumber + ' : amount = ' + betAmount)
//         let increaseAmountConditions = {};
//         increaseAmountConditions['agent'] = mongoose.Types.ObjectId(agentId);
//         increaseAmountConditions['po.' + betType + '.bets.betNumber'] = betNumber;
//
//         let updateIncreaseAmountStatement = {};
//         updateIncreaseAmountStatement['po.' + betType + '.bets.$.betAmount'] = betAmount;
//
//         AgentLottoLimitModel.update(increaseAmountConditions, {$inc: updateIncreaseAmountStatement}, function (err, updateLimitResponse) {
//
//             if (updateLimitResponse.nModified == 0) {
//
//                 // console.log('set new betnumber')
//                 let conditions = {};
//                 conditions['agent'] = mongoose.Types.ObjectId(agentId);
//                 conditions['po.' + betType + '.bets.betNumber'] = {$nin: [betNumber]};
//                 let updateStatement = {};
//                 updateStatement['po.' + betType + '.bets'] = {
//                     betNumber: betNumber,
//                     betAmount: betAmount
//                 };
//
//                 AgentLottoLimitModel.update(conditions, {$push: updateStatement}, function (err, updateLimitResponse) {
//                     if (err) {
//                         console.log('err : ', err.code)
//                     }
//                     if (updateLimitResponse.nModified == 0) {
//                         AgentLottoLimitModel.update(increaseAmountConditions, {$inc: updateIncreaseAmountStatement}, function (err, updateLimitResponse) {
//                             // console.log('xc', err)
//                             // console.log('xc', updateLimitResponse)
//                             callback(null, {})
//                         });
//                     } else {
//                         callback(null, {})
//                     }
//                 });
//
//             } else if (updateLimitResponse.nModified == 1) {
//                 //update success
//                 callback(null, {})
//             }
//         });
//         // callback(null, {})
//     }
//
//
// });


router.post('/qqq', function (req, res) {

    let condition = {"betId": {$in: ['BET1573044858475', 'BET1573046642295', 'BET1573044863732', 'BET1573044882781', 'BET1573047163949', 'BET1573056300123', 'BET1573047210868', 'BET1573056369809', 'BET1573056492057', 'BET1573057926231', 'BET1573063202531', 'BET1573060633896', 'BET1573063209060', 'BET1573301830240', 'BET1573293783740', 'BET1573303630657', 'BET1573303649558', 'BET1573303584030', 'BET1573303601030', 'BET1573303615054', 'BET1573306636050', 'BET1573306796332', 'BET1573306653165', 'BET1572974643914', 'BET1572974646802', 'BET1572949677110', 'BET1572970450873', 'BET1572975485259', 'BET1572972885404', 'BET1573060794618', 'BET1573043439671', 'BET1573046829350', 'BET1573060759491', 'BET1573051598497', 'BET1573299246928', 'BET1573299272743', 'BET1573299221717', 'BET1573299245961', 'BET1573299263248', 'BET1573300347837', 'BET1573300347679', 'BET1573300363455', 'BET1573308409397', 'BET1573296976713', 'BET1573308366050', 'BET1572971862255', 'BET1572972015662', 'BET1572972067662', 'BET1573317035207', 'BET1573317025074', 'BET1573296331906', 'BET1573308105915', 'BET1573308268185', 'BET1573308556862', 'BET1573309932912', 'BET1573304000647', 'BET1573309895352', 'BET1573315194534', 'BET1573315121105']}}

    console.log(condition)

    BetTransactionModel.find(condition)
        .exec(function (err, transaction) {

            async.each(transaction, (betObj, callbackWL) => {

                console.log(betObj.memberCredit)
                //refundBalance
                MemberService.updateBalance(betObj.memberId, betObj.memberCredit, betObj.betId, 'BET2', '', function (err, updateBalanceResponse) {
                    if (err) {
                        return res.send({message: "data not found", results: err, code: 999});
                    } else {


                        let updateBody = {
                            $set: {
                                status: 'RUNNING',
                                'commission.superAdmin.winLoseCom': 0,
                                'commission.superAdmin.winLose': 0,
                                'commission.superAdmin.totalWinLoseCom': 0,

                                'commission.company.winLoseCom': 0,
                                'commission.company.winLose': 0,
                                'commission.company.totalWinLoseCom': 0,

                                'commission.shareHolder.winLoseCom': 0,
                                'commission.shareHolder.winLose': 0,
                                'commission.shareHolder.totalWinLoseCom': 0,

                                'commission.senior.winLoseCom': 0,
                                'commission.senior.winLose': 0,
                                'commission.senior.totalWinLoseCom': 0,

                                'commission.masterAgent.winLoseCom': 0,
                                'commission.masterAgent.winLose': 0,
                                'commission.masterAgent.totalWinLoseCom': 0,

                                'commission.agent.winLoseCom': 0,
                                'commission.agent.winLose': 0,
                                'commission.agent.totalWinLoseCom': 0,

                                'commission.member.winLoseCom': 0,
                                'commission.member.winLose': 0,
                                'commission.member.totalWinLoseCom': 0,
                            }
                        };
                        BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {

                            console.log(response)
                            if (err) {
                                callbackWL(err);
                            }
                            else {
                                callbackWL();
                            }
                        });

                    }
                });
            }, (err, response) => {

                return res.send(
                    {
                        code: 0,
                        message: "success",
                        result: 'success'
                    }
                );

            });
        });
});


router.get('/test', function (req, res) {

    BetTransactionModel.aggregate([
        {
            $match: {status: 'DONE'}
        },
        {
            "$group": {
                "_id": {
                    commission: '$memberParentGroup',
                    member: '$memberId'
                },
                "stackCount": {$sum: 1},

                "amount": {$sum: '$amount'},

                "validAmount": {$sum: '$validAmount'},

                "agentWinLose": {$sum: {$toDecimal: '$commission.agent.winLose'}},

                "agentWinLoseCom": {$sum: {$toDecimal: '$commission.agent.winLoseCom'}},

                "agentTotalWinLoseCom": {$sum: {$toDecimal: '$commission.agent.totalWinLoseCom'}},

                "masterAgentWinLose": {$sum: {$toDecimal: '$commission.masterAgent.winLose'}},

                "masterAgentWinLoseCom": {$sum: {$toDecimal: '$commission.masterAgent.winLoseCom'}},

                "masterAgentTotalWinLoseCom": {$sum: {$toDecimal: '$commission.masterAgent.totalWinLoseCom'}},

                "seniorWinLose": {$sum: {$toDecimal: '$commission.senior.winLose'}},

                "seniorWinLoseCom": {$sum: {$toDecimal: '$commission.senior.winLoseCom'}},

                "seniorTotalWinLoseCom": {$sum: {$toDecimal: '$commission.senior.totalWinLoseCom'}},

                "shareHolderWinLose": {$sum: {$toDecimal: '$commission.shareHolder.winLose'}},

                "shareHolderWinLoseCom": {$sum: {$toDecimal: '$commission.shareHolder.winLoseCom'}},

                "shareHolderTotalWinLoseCom": {$sum: {$toDecimal: '$commission.shareHolder.totalWinLoseCom'}},

                "companyWinLose": {$sum: {$toDecimal: '$commission.company.winLose'}},

                "companyWinLoseCom": {$sum: {$toDecimal: '$commission.company.winLoseCom'}},

                "companyTotalWinLoseCom": {$sum: {$toDecimal: '$commission.company.totalWinLoseCom'}},

                "superAdminWinLose": {$sum: {$toDecimal: '$commission.superAdmin.winLose'}},

                "superAdminWinLoseCom": {$sum: {$toDecimal: '$commission.superAdmin.winLoseCom'}},

                "superAdminTotalWinLoseCom": {$sum: {$toDecimal: '$commission.superAdmin.totalWinLoseCom'}}
            }
        }
    ]).option({maxTimeMS: 30000}).exec((err, results) => {
        if (err) {
            console.log(err)
            return res.send({message: "error", results: err, code: 999});
        } else {
            return res.send({message: "success", code: 0});
        }
    });
});

const clearTicketSchema = Joi.object().keys({

    amount: Joi.number().required(),
    betId: Joi.string().required()
});

router.post('/clear-ticket-running', function (req, res) {
    let validate = Joi.validate(req.body, clearTicketSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            result: validate.error.details,
            code: 999
        });
    }

    BetTransactionModel.findOne({betId: req.body.betId}, (err, response) => {

        let updateBody = {
            $set: {
                'status': 'GG'
            }
        };
        if (response) {
            // let bb = Math.abs(response.memberCredit);//req.body.amount;
            let bb = req.body.amount;
            console.log(response,'qqqw');

            MemberService.updateBalance2(response.memberUsername, bb, req.body.betId, 'ROLLBACK_BALANCE' + new Date(),  req.body.betId+new Date(), function (err, updateMemberResponse) {

                BetTransactionModel.updateOne({_id:response._id}, updateBody, function (err, response) {
                    if (err) {
                        // callbackWL(err, '');
                    } else {
                        // callbackWL(null, '');
                        console.log(response)
                    }
                });

                return res.send({
                    code: 0,
                    message: "success",
                    result: 'success'
                });
                // })
            });

        } else {
            return res.send({
                code: 0,
                message: "fail",
                result: 'success'
            });
        }


    });


});


router.get('/clear-sa', function (req, res) {

    let condition = {
        gameDate: {
            "$gte": moment().millisecond(0).second(0).minute(0).hour(13).date(28).utc(true).toDate(),
            "$lt": moment().millisecond(0).second(0).minute(30).hour(15).date(28).utc(true).toDate(),
        },
        // status: 'RUNNING', game: 'CASINO',source:{$in:['SA_GAME','SEXY_BACCARAT','AG_GAME','DREAM_GAME']}
        // status: 'RUNNING', game: 'CASINO', source: {$ne: 'AG_GAME'}
        status: 'RUNNING', game: 'GAME',source:'PG_SLOT'
    }
    // status: 'RUNNING', game: 'CASINO',source:{$ne:'SEXY_BACCARAT'}}
    // status: 'RUNNING', game: 'CASINO',source:'SEXY_BACCARAT'}
    // status: 'RUNNING', game: 'GAME',source:'DS_GAME'}


    console.log(condition)

    const cursor = BetTransactionModel.find(condition)
        .batchSize(1000)
        .lean()
        .cursor();

    cursor.on('data', function (betObj) {

        let updateBody = {
            $set: {
                'memberCredit': 0,
                'status': 'GG'
            }
        };

        MemberService.updateBalance2(betObj.memberUsername, Math.abs(betObj.memberCredit), betObj.betId, 'CANCELLED_MANUAL', betObj.betId, function (err, updateBalanceResponse) {
            if (err) {

                if (err == 888) {
                    BetTransactionModel.updateOne({_id: betObj._id}, updateBody, function (err, response) {
                        if (err) {
                            // callbackWL(err, '');
                        } else {
                            // callbackWL(null, '');
                            console.log(response)
                        }
                    });
                }
                // return res.send({message: "data not found", results: err, code: 999});
            } else {


                BetTransactionModel.updateOne({_id: betObj._id}, updateBody, function (err, response) {
                    if (err) {
                        // callbackWL(err, '');
                    } else {
                        // callbackWL(null, '');
                        console.log(response)
                    }
                });

            }
        });


    });


    cursor.on('end', function () {
        console.log('Done!');
        return res.send({code: 0, message: "success"});
    });


});



router.get('/clear-sa55', function (req, res) {

    let condition = {
        gameDate: {
            "$gte": moment().millisecond(0).second(0).minute(0).hour(13).date(28).utc(true).toDate(),
            "$lt": moment().millisecond(0).second(0).minute(30).hour(15).date(28).utc(true).toDate(),
        },
        // status: 'RUNNING', game: 'CASINO',source:{$in:['SA_GAME','SEXY_BACCARAT','DREAM_GAME']},
        // status: 'RUNNING', game: 'CASINO', source: {$ne: 'AG_GAME'}
        status: 'RUNNING', game: 'CASINO',source:{$in:['SA_GAME','SEXY_BACCARAT']}
        // betId:"BET15902375823442243600"
    }
    // status: 'RUNNING', game: 'CASINO',source:{$ne:'SEXY_BACCARAT'}}
    // status: 'RUNNING', game: 'CASINO',source:'SEXY_BACCARAT'}
    // status: 'RUNNING', game: 'GAME',source:'DS_GAME'}


    console.log(condition)

    const cursor = BetTransactionModel.find(condition)
        .batchSize(1000)
        .lean()
        .cursor();

    cursor.on('data', function (betObj) {


        UpdateBalanceHistoryModel2.findOne({betId:betObj.betId,},(err,updateBalanceResponse)=>{
            if(!updateBalanceResponse){

                let updateBody = {
                    $set: {
                        // 'memberCredit': 0,
                        'status': 'GG2'
                    }
                };


                BetTransactionModel.updateOne({_id: betObj._id}, updateBody, function (err, response) {
                                    if (err) {
                                        // callbackWL(err, '');
                                    } else {
                                        // callbackWL(null, '');
                                        console.log('betId : '+betObj.betId+ ' ::: mai me = update GG2')
                                        // console.log(response)
                                    }
                                });
            }
            else {
                console.log('betId : '+betObj.betId+ ' ::: me')
            }
        });

        // MemberService.updateBalance2(betObj.memberUsername, Math.abs(betObj.memberCredit), betObj.betId, 'CANCELLED_MANUAL', betObj.betId, function (err, updateBalanceResponse) {
        //     if (err) {
        //
        //         if (err == 888) {
        //             BetTransactionModel.updateOne({_id: betObj._id}, updateBody, function (err, response) {
        //                 if (err) {
        //                     // callbackWL(err, '');
        //                 } else {
        //                     // callbackWL(null, '');
        //                     console.log(response)
        //                 }
        //             });
        //         }
        //         // return res.send({message: "data not found", results: err, code: 999});
        //     } else {
        //
        //
        //         BetTransactionModel.updateOne({_id: betObj._id}, updateBody, function (err, response) {
        //             if (err) {
        //                 // callbackWL(err, '');
        //             } else {
        //                 // callbackWL(null, '');
        //                 console.log(response)
        //             }
        //         });
        //
        //     }
        // });


    });


    cursor.on('end', function () {
        console.log('Done!');
        return res.send({code: 0, message: "success"});
    });


});



router.get('/clear-sa66', function (req, res) {


    const DRAW = "DRAW";
    const WIN = "WIN";
    const HALF_WIN = "HALF_WIN";
    const LOSE = "LOSE";
    const HALF_LOSE = "HALF_LOSE";

    let condition = {
        gameDate: {
            "$gte": moment().millisecond(0).second(0).minute(0).hour(13).date(28).utc(true).toDate(),
            "$lt": moment().millisecond(0).second(0).minute(30).hour(15).date(28).utc(true).toDate(),
        },
        status: 'RUNNING', game: 'CASINO',source:'SA_GAME'
        // status: 'RUNNING', game: 'CASINO', source: {$ne: 'AG_GAME'}
        // status: 'RUNNING', game: 'CASINO',source:'SEXY_BACCARAT',

    };
    // status: 'RUNNING', game: 'CASINO',source:{$ne:'SEXY_BACCARAT'}}
    // status: 'RUNNING', game: 'CASINO',source:'SEXY_BACCARAT'}
    // status: 'RUNNING', game: 'GAME',source:'DS_GAME'}


    console.log(condition);

    const cursor = BetTransactionModel.find(condition)
        .batchSize(1000)
        .lean()
        .cursor();

    cursor.on('data', function (betObj) {


        SaTransactionModel2.findOne({username:betObj.memberUsername,gameId:betObj.baccarat.sa.gameId},(err,saResponse)=>{

            console.log('SaResponse : ',saResponse)

            if(saResponse){

                let requestBody =  saResponse;

                if(saResponse.action == 'PLAYER_WIN'){

                    async.series([callback => {

                        MemberService.findByUserNameForPartnerService(requestBody.username, function (err, memberResponse) {
                            if (err) {
                                callback(err, null);
                                return;
                            }
                            if (memberResponse) {
                                callback(null, memberResponse);
                            } else {
                                callback(1004, null);
                            }
                        });

                    }], (err, asyncResponse) => {

                        if (err) {
                            if (err === 1004) {

                            }

                            console.log(err)
                        }

                        let memberInfo = asyncResponse[0];


                        let condition = {
                            'betId': betObj.betId
                        };


                        BetTransactionModel.findOne(condition)
                            .populate({path: 'memberParentGroup', select: 'type'})
                            .exec(function (err, betObj) {


                                if (!betObj) {

                                }

                                if (betObj.status === 'DONE') {

                                    MemberService.getCredit(memberInfo._id, (err, credit) => {


                                    });

                                } else {

                                    AgentGroupModel.findById(memberInfo.group).select('_id type parentId shareSetting commissionSetting').populate({
                                        path: 'parentId',
                                        select: '_id type parentId shareSetting commissionSetting name',
                                        populate: {
                                            path: 'parentId',
                                            select: '_id type parentId shareSetting commissionSetting name',
                                            populate: {
                                                path: 'parentId',
                                                select: '_id type parentId shareSetting commissionSetting name',
                                                populate: {
                                                    path: 'parentId',
                                                    select: '_id type parentId shareSetting commissionSetting name',
                                                    populate: {
                                                        path: 'parentId',
                                                        select: '_id type parentId shareSetting commissionSetting name',
                                                        populate: {
                                                            path: 'parentId',
                                                            select: '_id type parentId shareSetting commissionSetting name',
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                    }).exec(function (err, agentGroups) {

                                        let betAmount = betObj.amount;
                                        let resultCash = roundTo(Number.parseFloat(requestBody.amount), 2);
                                        let winLose = resultCash - betAmount;
                                        let betResult = winLose > 0 ? WIN : winLose < 0 ? LOSE : DRAW;

                                        let body = betObj;

                                        prepareCommission(memberInfo, body, agentGroups, null, 0, 0);

                                        //init
                                        if (!body.commission.senior) {
                                            body.commission.senior = {};
                                        }

                                        if (!body.commission.masterAgent) {
                                            body.commission.masterAgent = {};
                                        }

                                        if (!body.commission.agent) {
                                            body.commission.agent = {};
                                        }

                                        const shareReceive = AgentService.prepareShareReceive(winLose, betResult, betObj);

                                        body.commission.superAdmin.winLoseCom = shareReceive.superAdmin.winLoseCom;
                                        body.commission.superAdmin.winLose = shareReceive.superAdmin.winLose;
                                        body.commission.superAdmin.totalWinLoseCom = shareReceive.superAdmin.totalWinLoseCom;

                                        body.commission.company.winLoseCom = shareReceive.company.winLoseCom;
                                        body.commission.company.winLose = shareReceive.company.winLose;
                                        body.commission.company.totalWinLoseCom = shareReceive.company.totalWinLoseCom;

                                        body.commission.shareHolder.winLoseCom = shareReceive.shareHolder.winLoseCom;
                                        body.commission.shareHolder.winLose = shareReceive.shareHolder.winLose;
                                        body.commission.shareHolder.totalWinLoseCom = shareReceive.shareHolder.totalWinLoseCom;

                                        body.commission.senior.winLoseCom = shareReceive.senior.winLoseCom;
                                        body.commission.senior.winLose = shareReceive.senior.winLose;
                                        body.commission.senior.totalWinLoseCom = shareReceive.senior.totalWinLoseCom;

                                        body.commission.masterAgent.winLoseCom = shareReceive.masterAgent.winLoseCom;
                                        body.commission.masterAgent.winLose = shareReceive.masterAgent.winLose;
                                        body.commission.masterAgent.totalWinLoseCom = shareReceive.masterAgent.totalWinLoseCom;

                                        body.commission.agent.winLoseCom = shareReceive.agent.winLoseCom;
                                        body.commission.agent.winLose = shareReceive.agent.winLose;
                                        body.commission.agent.totalWinLoseCom = shareReceive.agent.totalWinLoseCom;

                                        body.commission.member.winLoseCom = shareReceive.member.winLoseCom;
                                        body.commission.member.winLose = shareReceive.member.winLose;
                                        body.commission.member.totalWinLoseCom = shareReceive.member.totalWinLoseCom;
                                        body.validAmount = betResult === DRAW ? 0 : body.amount;

                                        // updateAgentMemberBalance(shareReceive, betAmount, requestBody.txnid, function (err, updateBalanceResponse) {

                                                let updateBody = {
                                                    $set: {
                                                        'baccarat.sa.winLose': winLose,
                                                        'commission': body.commission,
                                                        'validAmount': body.validAmount,
                                                        'betResult': betResult,
                                                        'isEndScore': true,
                                                        'status': 'DONE',
                                                        'settleDate': betObj.gameDate
                                                    }
                                                };


                                                BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                                                    if (err) {

                                                    } else {
                                                        console.log('ok')
                                                    }
                                                });

                                        // });
                                    });
                                }

                            });
                    });


                }
                else  if(saResponse.action == 'PLAYER_LOST'){

                    async.series([callback => {

                        MemberService.findByUserNameForPartnerService(requestBody.username, function (err, memberResponse) {
                            if (err) {
                                callback(err, null);
                                return;
                            }
                            if (memberResponse) {
                                callback(null, memberResponse);
                            } else {
                                callback(1004, null);
                            }
                        });

                    }], (err, asyncResponse) => {


                        if (err) {
                           console.log(err)
                        }

                        let memberInfo = asyncResponse[0];


                        let condition = {
                            'betId': betObj.betId
                        };

                        BetTransactionModel.findOne(condition)
                            .populate({path: 'memberParentGroup', select: 'type'})
                            .exec(function (err, betObj) {


                                if (betObj.status === 'DONE') {

                                    MemberService.getCredit(memberInfo._id, (err, credit) => {

                                    })

                                } else {

                                    AgentGroupModel.findById(memberInfo.group).select('_id type parentId shareSetting commissionSetting').populate({
                                        path: 'parentId',
                                        select: '_id type parentId shareSetting commissionSetting name',
                                        populate: {
                                            path: 'parentId',
                                            select: '_id type parentId shareSetting commissionSetting name',
                                            populate: {
                                                path: 'parentId',
                                                select: '_id type parentId shareSetting commissionSetting name',
                                                populate: {
                                                    path: 'parentId',
                                                    select: '_id type parentId shareSetting commissionSetting name',
                                                    populate: {
                                                        path: 'parentId',
                                                        select: '_id type parentId shareSetting commissionSetting name',
                                                        populate: {
                                                            path: 'parentId',
                                                            select: '_id type parentId shareSetting commissionSetting name',
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                    }).exec(function (err, agentGroups) {


                                        let betAmount = betObj.amount;
                                        let winLose = roundTo(Number.parseFloat(betObj.memberCredit), 2);
                                        let betResult = LOSE;

                                        let body = betObj;

                                        prepareCommission(memberInfo, body, agentGroups, null, 0, 0);

                                        //init
                                        if (!body.commission.senior) {
                                            body.commission.senior = {};
                                        }

                                        if (!body.commission.masterAgent) {
                                            body.commission.masterAgent = {};
                                        }

                                        if (!body.commission.agent) {
                                            body.commission.agent = {};
                                        }

                                        const shareReceive = AgentService.prepareShareReceive(winLose, betResult, betObj);

                                        body.commission.superAdmin.winLoseCom = shareReceive.superAdmin.winLoseCom;
                                        body.commission.superAdmin.winLose = shareReceive.superAdmin.winLose;
                                        body.commission.superAdmin.totalWinLoseCom = shareReceive.superAdmin.totalWinLoseCom;

                                        body.commission.company.winLoseCom = shareReceive.company.winLoseCom;
                                        body.commission.company.winLose = shareReceive.company.winLose;
                                        body.commission.company.totalWinLoseCom = shareReceive.company.totalWinLoseCom;

                                        body.commission.shareHolder.winLoseCom = shareReceive.shareHolder.winLoseCom;
                                        body.commission.shareHolder.winLose = shareReceive.shareHolder.winLose;
                                        body.commission.shareHolder.totalWinLoseCom = shareReceive.shareHolder.totalWinLoseCom;

                                        body.commission.senior.winLoseCom = shareReceive.senior.winLoseCom;
                                        body.commission.senior.winLose = shareReceive.senior.winLose;
                                        body.commission.senior.totalWinLoseCom = shareReceive.senior.totalWinLoseCom;

                                        body.commission.masterAgent.winLoseCom = shareReceive.masterAgent.winLoseCom;
                                        body.commission.masterAgent.winLose = shareReceive.masterAgent.winLose;
                                        body.commission.masterAgent.totalWinLoseCom = shareReceive.masterAgent.totalWinLoseCom;

                                        body.commission.agent.winLoseCom = shareReceive.agent.winLoseCom;
                                        body.commission.agent.winLose = shareReceive.agent.winLose;
                                        body.commission.agent.totalWinLoseCom = shareReceive.agent.totalWinLoseCom;

                                        body.commission.member.winLoseCom = shareReceive.member.winLoseCom;
                                        body.commission.member.winLose = shareReceive.member.winLose;
                                        body.commission.member.totalWinLoseCom = shareReceive.member.totalWinLoseCom;


                                                let updateBody = {
                                                    $set: {
                                                        // 'baccarat.sa.payoutTime': requestBody.PayoutTime,
                                                        'commission': body.commission,
                                                        'betResult': betResult,
                                                        'isEndScore': true,
                                                        'status': 'DONE',
                                                        'validAmount': betAmount,
                                                        'settleDate': betObj.gameDate
                                                    }
                                                };


                                                BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                                                  console.log('ok')
                                                });

                                    });
                                }

                            });

                    });

                }else {
                    console.log('action not match')
                }

            }





        }).sort({createdDate: -1});


    });


    cursor.on('end', function () {
        console.log('Done!');
        return res.send({code: 0, message: "success"});
    });


    function prepareCommission(memberInfo, body, currentAgent, overAgent, remaining, overAllShare) {


        if (currentAgent && (currentAgent.type === 'SUPER_ADMIN' || currentAgent.parentId)) {


            console.log('==============================================');
            console.log('process : ' + currentAgent.type + ' : ' + currentAgent.name);


            let money = body.amount;


            if (!overAgent) {
                overAgent = {};
                overAgent.type = 'member';
                overAgent.shareSetting = {};
                overAgent.shareSetting.casino = {};
                overAgent.shareSetting.casino.sa = {};
                overAgent.shareSetting.casino.sa.parent = memberInfo.shareSetting.casino.sa.parent;
                overAgent.shareSetting.casino.sa.own = 0;
                overAgent.shareSetting.casino.sa.remaining = 0;

                body.commission.member = {};
                body.commission.member.parent = memberInfo.shareSetting.casino.sa.parent;
                body.commission.member.commission = memberInfo.commissionSetting.casino.sa;

            }

            console.log('');
            console.log('---- setting ----');
            console.log('ส่วนแบ่งที่ได้มามีทั้งหมด : ' + currentAgent.shareSetting.casino.sa.own + ' %');
            console.log('ขอส่วนแบ่งจาก : ' + overAgent.type + ' ' + overAgent.shareSetting.casino.sa.parent + ' %');
            console.log('ให้ส่วนแบ่ง : ' + overAgent.type + ' ที่เหลือไป ' + overAgent.shareSetting.casino.sa.own + ' %');
            console.log('remaining จาก : ' + overAgent.type + ' : ' + overAgent.shareSetting.casino.sa.remaining + ' %');
            console.log('โดนบังคับเสียขั้นต่ำ : ' + currentAgent.shareSetting.casino.sa.min + ' %');
            console.log('---- end setting ----');
            console.log('');


            // console.log('และได้รับ remaining ไว้ : '+overAgent.shareSetting.casino.sa.remaining+' %');

            let currentPercentReceive = overAgent.shareSetting.casino.sa.parent;
            console.log('% ที่ได้ : ' + currentPercentReceive);
            console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');
            let totalRemaining = remaining;

            if (overAgent.shareSetting.casino.sa.remaining > 0) {
                // console.log()
                // console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');

                if (overAgent.shareSetting.casino.sa.remaining > totalRemaining) {
                    console.log('รับ remaining ทั้งหมดไว้: ' + totalRemaining + ' %');
                    currentPercentReceive += totalRemaining;
                    totalRemaining = 0;
                } else {
                    totalRemaining = remaining - overAgent.shareSetting.casino.sa.remaining;
                    console.log('หักค่า remaining ที่ได้รับมากับที่เซตรับไว้ = ' + remaining + ' - ' + overAgent.shareSetting.casino.sa.remaining + ' = ' + totalRemaining);
                    currentPercentReceive += overAgent.shareSetting.casino.sa.remaining;
                }

            }

            console.log('รวม % ที่ได้รับ : ' + currentPercentReceive);


            let unUseShare = currentAgent.shareSetting.casino.sa.own -
                (overAgent.shareSetting.casino.sa.parent + overAgent.shareSetting.casino.sa.own);

            console.log('เหลือส่วนแบ่งที่ไม่ได้ใช้ : ' + unUseShare + ' %');
            totalRemaining = (unUseShare + totalRemaining);
            console.log('ส่วนแบ่งที่ไม่ได้ใช้ บวกกับค่า remaining ท่ี่เหลือจะเป็น : ' + totalRemaining + ' %');

            console.log('จากที่โดนบังคับเสียขั้นต่ำ : ' + currentAgent.shareSetting.casino.sa.min + ' %');
            console.log('overAllShare : ', overAllShare);
            if (currentAgent.shareSetting.casino.sa.min > 0 && ((overAllShare + currentPercentReceive) < currentAgent.shareSetting.casino.sa.min)) {

                if (currentPercentReceive < currentAgent.shareSetting.casino.sa.min) {
                    console.log('% ที่ได้รับ < ขั้นต่ำที่ถูกตั้ง min ไว้');
                    let percent = currentAgent.shareSetting.casino.sa.min - (currentPercentReceive + overAllShare);
                    console.log('หักออกไป ' + percent + ' % : แล้วไปเพิ่มในส่วนที่ต้องรับผิดชอบ ');
                    totalRemaining = totalRemaining - percent;
                    if (totalRemaining < 0) {
                        totalRemaining = 0;
                    }
                    currentPercentReceive += percent;
                }
            } else {
                // currentPercentReceive += totalRemaining;
            }


            if (currentAgent.type === 'SUPER_ADMIN') {
                currentPercentReceive += totalRemaining;
            }

            let getMoney = (money * NumberUtils.convertPercent(currentPercentReceive)).toFixed(2);
            overAllShare = overAllShare + currentPercentReceive;

            console.log('ยอดแทง ' + money + ' ได้ทั้งหมด : ' + currentPercentReceive + ' %');

            console.log('รวม % + % remaining  จะเป็นสัดส่วนที่ได้ทั้งหมด = ' + getMoney);
            console.log('ค่าที่จะถูกคืนกลับไป (remaining) : ' + totalRemaining + ' %');

            console.log('จำนวน % ที่ถูกแบ่งไปแล้วทั้งหมด : ', overAllShare);

            //set commission
            let agentCommission = currentAgent.commissionSetting.casino.sa;


            switch (currentAgent.type) {
                case 'SUPER_ADMIN':
                    body.commission.superAdmin = {};
                    body.commission.superAdmin.group = currentAgent._id;
                    body.commission.superAdmin.parent = currentAgent.shareSetting.casino.sa.parent;
                    body.commission.superAdmin.own = currentAgent.shareSetting.casino.sa.own;
                    body.commission.superAdmin.remaining = currentAgent.shareSetting.casino.sa.remaining;
                    body.commission.superAdmin.min = currentAgent.shareSetting.casino.sa.min;
                    body.commission.superAdmin.shareReceive = Math.abs(currentPercentReceive);
                    body.commission.superAdmin.commission = agentCommission;
                    body.commission.superAdmin.amount = getMoney;
                    break;
                case 'COMPANY':
                    body.commission.company = {};
                    body.commission.company.parentGroup = currentAgent.parentId;
                    body.commission.company.group = currentAgent._id;
                    body.commission.company.parent = currentAgent.shareSetting.casino.sa.parent;
                    body.commission.company.own = currentAgent.shareSetting.casino.sa.own;
                    body.commission.company.remaining = currentAgent.shareSetting.casino.sa.remaining;
                    body.commission.company.min = currentAgent.shareSetting.casino.sa.min;
                    body.commission.company.shareReceive = Math.abs(currentPercentReceive);
                    body.commission.company.commission = agentCommission;
                    body.commission.company.amount = getMoney;
                    break;
                case 'SHARE_HOLDER':
                    body.commission.shareHolder = {};
                    body.commission.shareHolder.parentGroup = currentAgent.parentId;
                    body.commission.shareHolder.group = currentAgent._id;
                    body.commission.shareHolder.parent = currentAgent.shareSetting.casino.sa.parent;
                    body.commission.shareHolder.own = currentAgent.shareSetting.casino.sa.own;
                    body.commission.shareHolder.remaining = currentAgent.shareSetting.casino.sa.remaining;
                    body.commission.shareHolder.min = currentAgent.shareSetting.casino.sa.min;
                    body.commission.shareHolder.shareReceive = currentPercentReceive;
                    body.commission.shareHolder.commission = agentCommission;
                    body.commission.shareHolder.amount = getMoney;
                    break;
                case 'SENIOR':
                    body.commission.senior = {};
                    body.commission.senior.parentGroup = currentAgent.parentId;
                    body.commission.senior.group = currentAgent._id;
                    body.commission.senior.parent = currentAgent.shareSetting.casino.sa.parent;
                    body.commission.senior.own = currentAgent.shareSetting.casino.sa.own;
                    body.commission.senior.remaining = currentAgent.shareSetting.casino.sa.remaining;
                    body.commission.senior.min = currentAgent.shareSetting.casino.sa.min;
                    body.commission.senior.shareReceive = currentPercentReceive;
                    body.commission.senior.commission = agentCommission;
                    body.commission.senior.amount = getMoney;
                    break;
                case 'MASTER_AGENT':
                    body.commission.masterAgent = {};
                    body.commission.masterAgent.parentGroup = currentAgent.parentId;
                    body.commission.masterAgent.group = currentAgent._id;
                    body.commission.masterAgent.parent = currentAgent.shareSetting.casino.sa.parent;
                    body.commission.masterAgent.own = currentAgent.shareSetting.casino.sa.own;
                    body.commission.masterAgent.remaining = currentAgent.shareSetting.casino.sa.remaining;
                    body.commission.masterAgent.min = currentAgent.shareSetting.casino.sa.min;
                    body.commission.masterAgent.shareReceive = currentPercentReceive;
                    body.commission.masterAgent.commission = agentCommission;
                    body.commission.masterAgent.amount = getMoney;
                    break;
                case 'AGENT':
                    body.commission.agent = {};
                    body.commission.agent.parentGroup = currentAgent.parentId;
                    body.commission.agent.group = currentAgent._id;
                    body.commission.agent.parent = currentAgent.shareSetting.casino.sa.parent;
                    body.commission.agent.own = currentAgent.shareSetting.casino.sa.own;
                    body.commission.agent.remaining = currentAgent.shareSetting.casino.sa.remaining;
                    body.commission.agent.min = currentAgent.shareSetting.casino.sa.min;
                    body.commission.agent.shareReceive = currentPercentReceive;
                    body.commission.agent.commission = agentCommission;
                    body.commission.agent.amount = getMoney;
                    break;
            }

            prepareCommission(memberInfo, body, currentAgent.parentId, currentAgent, totalRemaining, overAllShare);
        }
    }

});

router.get('/clear-sa77', function (req, res) {


    const DRAW = "DRAW";
    const WIN = "WIN";
    const HALF_WIN = "HALF_WIN";
    const LOSE = "LOSE";
    const HALF_LOSE = "HALF_LOSE";

    let condition = {
        // gameDate: {
        //     "$gte": moment().millisecond(0).second(0).minute(0).hour(19).date(23).utc(true).toDate(),
        //     "$lt": moment().millisecond(0).second(0).minute(50).hour(19).date(23).utc(true).toDate(),
        // },
        status: 'RUNNING', game: 'CASINO',source:'SA_GAME',betId:'BET15901422783072519355'
        // status: 'RUNNING', game: 'CASINO', source: {$ne: 'AG_GAME'}
        // status: 'RUNNING', game: 'CASINO',source:'SEXY_BACCARAT',

    }
    // status: 'RUNNING', game: 'CASINO',source:{$ne:'SEXY_BACCARAT'}}
    // status: 'RUNNING', game: 'CASINO',source:'SEXY_BACCARAT'}
    // status: 'RUNNING', game: 'GAME',source:'DS_GAME'}


    console.log(condition);

    const cursor = BetTransactionModel.find(condition)
        .batchSize(1000)
        .lean()
        .cursor();

    cursor.on('data', function (betObj) {


        SaTransactionModel2.findOne({username:betObj.memberUsername,gameId:betObj.baccarat.sa.gameId},(err,saResponse)=>{

            console.log('SaResponse : ',saResponse)

            if(saResponse){

                let requestBody =  saResponse;

                if(saResponse.action == 'PLAYER_WIN'){

                    async.series([callback => {

                        MemberService.findByUserNameForPartnerService(requestBody.username, function (err, memberResponse) {
                            if (err) {
                                callback(err, null);
                                return;
                            }
                            if (memberResponse) {
                                callback(null, memberResponse);
                            } else {
                                callback(1004, null);
                            }
                        });

                    }], (err, asyncResponse) => {

                        if (err) {
                            if (err === 1004) {

                            }

                            console.log(err)
                        }

                        let memberInfo = asyncResponse[0];


                        let condition = {
                            'betId': betObj.betId
                        };


                        BetTransactionModel.findOne(condition)
                            .populate({path: 'memberParentGroup', select: 'type'})
                            .exec(function (err, betObj) {


                                if (!betObj) {

                                }

                                if (betObj.status === 'DONE') {

                                    MemberService.getCredit(memberInfo._id, (err, credit) => {


                                    });

                                } else {

                                    AgentGroupModel.findById(memberInfo.group).select('_id type parentId shareSetting commissionSetting').populate({
                                        path: 'parentId',
                                        select: '_id type parentId shareSetting commissionSetting name',
                                        populate: {
                                            path: 'parentId',
                                            select: '_id type parentId shareSetting commissionSetting name',
                                            populate: {
                                                path: 'parentId',
                                                select: '_id type parentId shareSetting commissionSetting name',
                                                populate: {
                                                    path: 'parentId',
                                                    select: '_id type parentId shareSetting commissionSetting name',
                                                    populate: {
                                                        path: 'parentId',
                                                        select: '_id type parentId shareSetting commissionSetting name',
                                                        populate: {
                                                            path: 'parentId',
                                                            select: '_id type parentId shareSetting commissionSetting name',
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                    }).exec(function (err, agentGroups) {

                                        let betAmount = betObj.amount;
                                        let resultCash = roundTo(Number.parseFloat(requestBody.amount), 2);
                                        let winLose = resultCash - betAmount;
                                        let betResult = winLose > 0 ? WIN : winLose < 0 ? LOSE : DRAW;

                                        let body = betObj;

                                        prepareCommission(memberInfo, body, agentGroups, null, 0, 0);

                                        //init
                                        if (!body.commission.senior) {
                                            body.commission.senior = {};
                                        }

                                        if (!body.commission.masterAgent) {
                                            body.commission.masterAgent = {};
                                        }

                                        if (!body.commission.agent) {
                                            body.commission.agent = {};
                                        }

                                        const shareReceive = AgentService.prepareShareReceive(winLose, betResult, betObj);

                                        body.commission.superAdmin.winLoseCom = shareReceive.superAdmin.winLoseCom;
                                        body.commission.superAdmin.winLose = shareReceive.superAdmin.winLose;
                                        body.commission.superAdmin.totalWinLoseCom = shareReceive.superAdmin.totalWinLoseCom;

                                        body.commission.company.winLoseCom = shareReceive.company.winLoseCom;
                                        body.commission.company.winLose = shareReceive.company.winLose;
                                        body.commission.company.totalWinLoseCom = shareReceive.company.totalWinLoseCom;

                                        body.commission.shareHolder.winLoseCom = shareReceive.shareHolder.winLoseCom;
                                        body.commission.shareHolder.winLose = shareReceive.shareHolder.winLose;
                                        body.commission.shareHolder.totalWinLoseCom = shareReceive.shareHolder.totalWinLoseCom;

                                        body.commission.senior.winLoseCom = shareReceive.senior.winLoseCom;
                                        body.commission.senior.winLose = shareReceive.senior.winLose;
                                        body.commission.senior.totalWinLoseCom = shareReceive.senior.totalWinLoseCom;

                                        body.commission.masterAgent.winLoseCom = shareReceive.masterAgent.winLoseCom;
                                        body.commission.masterAgent.winLose = shareReceive.masterAgent.winLose;
                                        body.commission.masterAgent.totalWinLoseCom = shareReceive.masterAgent.totalWinLoseCom;

                                        body.commission.agent.winLoseCom = shareReceive.agent.winLoseCom;
                                        body.commission.agent.winLose = shareReceive.agent.winLose;
                                        body.commission.agent.totalWinLoseCom = shareReceive.agent.totalWinLoseCom;

                                        body.commission.member.winLoseCom = shareReceive.member.winLoseCom;
                                        body.commission.member.winLose = shareReceive.member.winLose;
                                        body.commission.member.totalWinLoseCom = shareReceive.member.totalWinLoseCom;
                                        body.validAmount = betResult === DRAW ? 0 : body.amount;

                                        // updateAgentMemberBalance(shareReceive, betAmount, requestBody.txnid, function (err, updateBalanceResponse) {

                                        let updateBody = {
                                            $set: {
                                                'baccarat.sa.winLose': winLose,
                                                'commission': body.commission,
                                                'validAmount': body.validAmount,
                                                'betResult': betResult,
                                                'isEndScore': true,
                                                'status': 'DONE',
                                                'settleDate': betObj.gameDate
                                            }
                                        };


                                        BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                                            if (err) {

                                            } else {
                                                console.log('ok')
                                            }
                                        });

                                        // });
                                    });
                                }

                            });
                    });


                }
                else  if(saResponse.action == 'PLAYER_LOST'){

                    async.series([callback => {

                        MemberService.findByUserNameForPartnerService(requestBody.username, function (err, memberResponse) {
                            if (err) {
                                callback(err, null);
                                return;
                            }
                            if (memberResponse) {
                                callback(null, memberResponse);
                            } else {
                                callback(1004, null);
                            }
                        });

                    }], (err, asyncResponse) => {


                        if (err) {
                            console.log(err)
                        }

                        let memberInfo = asyncResponse[0];


                        let condition = {
                            'betId': betObj.betId
                        };

                        BetTransactionModel.findOne(condition)
                            .populate({path: 'memberParentGroup', select: 'type'})
                            .exec(function (err, betObj) {


                                if (betObj.status === 'DONE') {

                                    MemberService.getCredit(memberInfo._id, (err, credit) => {

                                    })

                                } else {

                                    AgentGroupModel.findById(memberInfo.group).select('_id type parentId shareSetting commissionSetting').populate({
                                        path: 'parentId',
                                        select: '_id type parentId shareSetting commissionSetting name',
                                        populate: {
                                            path: 'parentId',
                                            select: '_id type parentId shareSetting commissionSetting name',
                                            populate: {
                                                path: 'parentId',
                                                select: '_id type parentId shareSetting commissionSetting name',
                                                populate: {
                                                    path: 'parentId',
                                                    select: '_id type parentId shareSetting commissionSetting name',
                                                    populate: {
                                                        path: 'parentId',
                                                        select: '_id type parentId shareSetting commissionSetting name',
                                                        populate: {
                                                            path: 'parentId',
                                                            select: '_id type parentId shareSetting commissionSetting name',
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                    }).exec(function (err, agentGroups) {


                                        let betAmount = betObj.amount;
                                        let winLose = roundTo(Number.parseFloat(betObj.memberCredit), 2);
                                        let betResult = LOSE;

                                        let body = betObj;

                                        prepareCommission(memberInfo, body, agentGroups, null, 0, 0);

                                        //init
                                        if (!body.commission.senior) {
                                            body.commission.senior = {};
                                        }

                                        if (!body.commission.masterAgent) {
                                            body.commission.masterAgent = {};
                                        }

                                        if (!body.commission.agent) {
                                            body.commission.agent = {};
                                        }

                                        const shareReceive = AgentService.prepareShareReceive(winLose, betResult, betObj);

                                        body.commission.superAdmin.winLoseCom = shareReceive.superAdmin.winLoseCom;
                                        body.commission.superAdmin.winLose = shareReceive.superAdmin.winLose;
                                        body.commission.superAdmin.totalWinLoseCom = shareReceive.superAdmin.totalWinLoseCom;

                                        body.commission.company.winLoseCom = shareReceive.company.winLoseCom;
                                        body.commission.company.winLose = shareReceive.company.winLose;
                                        body.commission.company.totalWinLoseCom = shareReceive.company.totalWinLoseCom;

                                        body.commission.shareHolder.winLoseCom = shareReceive.shareHolder.winLoseCom;
                                        body.commission.shareHolder.winLose = shareReceive.shareHolder.winLose;
                                        body.commission.shareHolder.totalWinLoseCom = shareReceive.shareHolder.totalWinLoseCom;

                                        body.commission.senior.winLoseCom = shareReceive.senior.winLoseCom;
                                        body.commission.senior.winLose = shareReceive.senior.winLose;
                                        body.commission.senior.totalWinLoseCom = shareReceive.senior.totalWinLoseCom;

                                        body.commission.masterAgent.winLoseCom = shareReceive.masterAgent.winLoseCom;
                                        body.commission.masterAgent.winLose = shareReceive.masterAgent.winLose;
                                        body.commission.masterAgent.totalWinLoseCom = shareReceive.masterAgent.totalWinLoseCom;

                                        body.commission.agent.winLoseCom = shareReceive.agent.winLoseCom;
                                        body.commission.agent.winLose = shareReceive.agent.winLose;
                                        body.commission.agent.totalWinLoseCom = shareReceive.agent.totalWinLoseCom;

                                        body.commission.member.winLoseCom = shareReceive.member.winLoseCom;
                                        body.commission.member.winLose = shareReceive.member.winLose;
                                        body.commission.member.totalWinLoseCom = shareReceive.member.totalWinLoseCom;


                                        let updateBody = {
                                            $set: {
                                                // 'baccarat.sa.payoutTime': requestBody.PayoutTime,
                                                'commission': body.commission,
                                                'betResult': betResult,
                                                'isEndScore': true,
                                                'status': 'DONE',
                                                'validAmount': betAmount,
                                                'settleDate': betObj.gameDate
                                            }
                                        };


                                        BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                                            console.log('ok')
                                        });

                                    });
                                }

                            });

                    });

                }else {
                    console.log('action not match')
                }

            }

        }).sort({createdDate: -1});


    });


    cursor.on('end', function () {
        console.log('Done!');
        return res.send({code: 0, message: "success"});
    });


    function prepareCommission(memberInfo, body, currentAgent, overAgent, remaining, overAllShare) {


        if (currentAgent && (currentAgent.type === 'SUPER_ADMIN' || currentAgent.parentId)) {


            console.log('==============================================');
            console.log('process : ' + currentAgent.type + ' : ' + currentAgent.name);


            let money = body.amount;


            if (!overAgent) {
                overAgent = {};
                overAgent.type = 'member';
                overAgent.shareSetting = {};
                overAgent.shareSetting.casino = {};
                overAgent.shareSetting.casino.sa = {};
                overAgent.shareSetting.casino.sa.parent = memberInfo.shareSetting.casino.sa.parent;
                overAgent.shareSetting.casino.sa.own = 0;
                overAgent.shareSetting.casino.sa.remaining = 0;

                body.commission.member = {};
                body.commission.member.parent = memberInfo.shareSetting.casino.sa.parent;
                body.commission.member.commission = memberInfo.commissionSetting.casino.sa;

            }

            console.log('');
            console.log('---- setting ----');
            console.log('ส่วนแบ่งที่ได้มามีทั้งหมด : ' + currentAgent.shareSetting.casino.sa.own + ' %');
            console.log('ขอส่วนแบ่งจาก : ' + overAgent.type + ' ' + overAgent.shareSetting.casino.sa.parent + ' %');
            console.log('ให้ส่วนแบ่ง : ' + overAgent.type + ' ที่เหลือไป ' + overAgent.shareSetting.casino.sa.own + ' %');
            console.log('remaining จาก : ' + overAgent.type + ' : ' + overAgent.shareSetting.casino.sa.remaining + ' %');
            console.log('โดนบังคับเสียขั้นต่ำ : ' + currentAgent.shareSetting.casino.sa.min + ' %');
            console.log('---- end setting ----');
            console.log('');


            // console.log('และได้รับ remaining ไว้ : '+overAgent.shareSetting.casino.sa.remaining+' %');

            let currentPercentReceive = overAgent.shareSetting.casino.sa.parent;
            console.log('% ที่ได้ : ' + currentPercentReceive);
            console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');
            let totalRemaining = remaining;

            if (overAgent.shareSetting.casino.sa.remaining > 0) {
                // console.log()
                // console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');

                if (overAgent.shareSetting.casino.sa.remaining > totalRemaining) {
                    console.log('รับ remaining ทั้งหมดไว้: ' + totalRemaining + ' %');
                    currentPercentReceive += totalRemaining;
                    totalRemaining = 0;
                } else {
                    totalRemaining = remaining - overAgent.shareSetting.casino.sa.remaining;
                    console.log('หักค่า remaining ที่ได้รับมากับที่เซตรับไว้ = ' + remaining + ' - ' + overAgent.shareSetting.casino.sa.remaining + ' = ' + totalRemaining);
                    currentPercentReceive += overAgent.shareSetting.casino.sa.remaining;
                }

            }

            console.log('รวม % ที่ได้รับ : ' + currentPercentReceive);


            let unUseShare = currentAgent.shareSetting.casino.sa.own -
                (overAgent.shareSetting.casino.sa.parent + overAgent.shareSetting.casino.sa.own);

            console.log('เหลือส่วนแบ่งที่ไม่ได้ใช้ : ' + unUseShare + ' %');
            totalRemaining = (unUseShare + totalRemaining);
            console.log('ส่วนแบ่งที่ไม่ได้ใช้ บวกกับค่า remaining ท่ี่เหลือจะเป็น : ' + totalRemaining + ' %');

            console.log('จากที่โดนบังคับเสียขั้นต่ำ : ' + currentAgent.shareSetting.casino.sa.min + ' %');
            console.log('overAllShare : ', overAllShare);
            if (currentAgent.shareSetting.casino.sa.min > 0 && ((overAllShare + currentPercentReceive) < currentAgent.shareSetting.casino.sa.min)) {

                if (currentPercentReceive < currentAgent.shareSetting.casino.sa.min) {
                    console.log('% ที่ได้รับ < ขั้นต่ำที่ถูกตั้ง min ไว้');
                    let percent = currentAgent.shareSetting.casino.sa.min - (currentPercentReceive + overAllShare);
                    console.log('หักออกไป ' + percent + ' % : แล้วไปเพิ่มในส่วนที่ต้องรับผิดชอบ ');
                    totalRemaining = totalRemaining - percent;
                    if (totalRemaining < 0) {
                        totalRemaining = 0;
                    }
                    currentPercentReceive += percent;
                }
            } else {
                // currentPercentReceive += totalRemaining;
            }


            if (currentAgent.type === 'SUPER_ADMIN') {
                currentPercentReceive += totalRemaining;
            }

            let getMoney = (money * NumberUtils.convertPercent(currentPercentReceive)).toFixed(2);
            overAllShare = overAllShare + currentPercentReceive;

            console.log('ยอดแทง ' + money + ' ได้ทั้งหมด : ' + currentPercentReceive + ' %');

            console.log('รวม % + % remaining  จะเป็นสัดส่วนที่ได้ทั้งหมด = ' + getMoney);
            console.log('ค่าที่จะถูกคืนกลับไป (remaining) : ' + totalRemaining + ' %');

            console.log('จำนวน % ที่ถูกแบ่งไปแล้วทั้งหมด : ', overAllShare);

            //set commission
            let agentCommission = currentAgent.commissionSetting.casino.sa;


            switch (currentAgent.type) {
                case 'SUPER_ADMIN':
                    body.commission.superAdmin = {};
                    body.commission.superAdmin.group = currentAgent._id;
                    body.commission.superAdmin.parent = currentAgent.shareSetting.casino.sa.parent;
                    body.commission.superAdmin.own = currentAgent.shareSetting.casino.sa.own;
                    body.commission.superAdmin.remaining = currentAgent.shareSetting.casino.sa.remaining;
                    body.commission.superAdmin.min = currentAgent.shareSetting.casino.sa.min;
                    body.commission.superAdmin.shareReceive = Math.abs(currentPercentReceive);
                    body.commission.superAdmin.commission = agentCommission;
                    body.commission.superAdmin.amount = getMoney;
                    break;
                case 'COMPANY':
                    body.commission.company = {};
                    body.commission.company.parentGroup = currentAgent.parentId;
                    body.commission.company.group = currentAgent._id;
                    body.commission.company.parent = currentAgent.shareSetting.casino.sa.parent;
                    body.commission.company.own = currentAgent.shareSetting.casino.sa.own;
                    body.commission.company.remaining = currentAgent.shareSetting.casino.sa.remaining;
                    body.commission.company.min = currentAgent.shareSetting.casino.sa.min;
                    body.commission.company.shareReceive = Math.abs(currentPercentReceive);
                    body.commission.company.commission = agentCommission;
                    body.commission.company.amount = getMoney;
                    break;
                case 'SHARE_HOLDER':
                    body.commission.shareHolder = {};
                    body.commission.shareHolder.parentGroup = currentAgent.parentId;
                    body.commission.shareHolder.group = currentAgent._id;
                    body.commission.shareHolder.parent = currentAgent.shareSetting.casino.sa.parent;
                    body.commission.shareHolder.own = currentAgent.shareSetting.casino.sa.own;
                    body.commission.shareHolder.remaining = currentAgent.shareSetting.casino.sa.remaining;
                    body.commission.shareHolder.min = currentAgent.shareSetting.casino.sa.min;
                    body.commission.shareHolder.shareReceive = currentPercentReceive;
                    body.commission.shareHolder.commission = agentCommission;
                    body.commission.shareHolder.amount = getMoney;
                    break;
                case 'SENIOR':
                    body.commission.senior = {};
                    body.commission.senior.parentGroup = currentAgent.parentId;
                    body.commission.senior.group = currentAgent._id;
                    body.commission.senior.parent = currentAgent.shareSetting.casino.sa.parent;
                    body.commission.senior.own = currentAgent.shareSetting.casino.sa.own;
                    body.commission.senior.remaining = currentAgent.shareSetting.casino.sa.remaining;
                    body.commission.senior.min = currentAgent.shareSetting.casino.sa.min;
                    body.commission.senior.shareReceive = currentPercentReceive;
                    body.commission.senior.commission = agentCommission;
                    body.commission.senior.amount = getMoney;
                    break;
                case 'MASTER_AGENT':
                    body.commission.masterAgent = {};
                    body.commission.masterAgent.parentGroup = currentAgent.parentId;
                    body.commission.masterAgent.group = currentAgent._id;
                    body.commission.masterAgent.parent = currentAgent.shareSetting.casino.sa.parent;
                    body.commission.masterAgent.own = currentAgent.shareSetting.casino.sa.own;
                    body.commission.masterAgent.remaining = currentAgent.shareSetting.casino.sa.remaining;
                    body.commission.masterAgent.min = currentAgent.shareSetting.casino.sa.min;
                    body.commission.masterAgent.shareReceive = currentPercentReceive;
                    body.commission.masterAgent.commission = agentCommission;
                    body.commission.masterAgent.amount = getMoney;
                    break;
                case 'AGENT':
                    body.commission.agent = {};
                    body.commission.agent.parentGroup = currentAgent.parentId;
                    body.commission.agent.group = currentAgent._id;
                    body.commission.agent.parent = currentAgent.shareSetting.casino.sa.parent;
                    body.commission.agent.own = currentAgent.shareSetting.casino.sa.own;
                    body.commission.agent.remaining = currentAgent.shareSetting.casino.sa.remaining;
                    body.commission.agent.min = currentAgent.shareSetting.casino.sa.min;
                    body.commission.agent.shareReceive = currentPercentReceive;
                    body.commission.agent.commission = agentCommission;
                    body.commission.agent.amount = getMoney;
                    break;
            }

            prepareCommission(memberInfo, body, currentAgent.parentId, currentAgent, totalRemaining, overAllShare);
        }
    }

});



router.get('/clear-sa2', function (req, res) {

    let condition = {
        gameDate: {
            "$gte": moment().millisecond(0).second(0).minute(0).hour(15).date(20).utc(true).toDate(),
            "$lt": moment().millisecond(0).second(0).minute(35).hour(16).date(20).utc(true).toDate(),
        },
        // status: 'RUNNING', game: 'CASINO'
        // status: 'RUNNING', game: 'CASINO',source:{$ne:'AG_GAME'}
        // "baccarat.sexy.txns.1.winLose": { $exists: false },
        status: 'DONE', game: 'CASINO', source: 'SEXY_BACCARAT'
    }
    // status: 'RUNNING', game: 'CASINO',source:{$ne:'SEXY_BACCARAT'}}
    // status: 'RUNNING', game: 'CASINO',source:'SEXY_BACCARAT'}
    // status: 'RUNNING', game: 'GAME',source:'DS_GAME'}


    console.log(condition)
    // return res.send({code: 0, message: "success"});

    const cursor = BetTransactionModel.find(condition)
        .batchSize(10000)
        .lean()
        .cursor();

    cursor.on('data', function (betObj) {


        let cancelItems = _.filter(betObj.baccarat.sexy.txns, (item) => {
            return item.winLose == null;
        });

        let activeItems = _.filter(betObj.baccarat.sexy.txns, (item) => {
            return item.winLose != null;
        });

        let activeAmount = _.reduce(activeItems, function (total, x) {
            return total + x.betAmount;
        }, 0);


        if (cancelItems.length > 0) {

            let totalAmount = _.reduce(cancelItems, function (total, x) {
                return total + x.betAmount;
            }, 0);


            let cancelTxIds = _.map(cancelItems, (item) => {
                return item.txId;
            });
            console.log('BET_ID : ' + betObj.betId + ' === before amount : ' + betObj.amount + ' , deduct amount : ' + totalAmount + ' , activeAmount : ', activeAmount)

            let updateBody = {
                amount: activeAmount,
                $inc: {
                    'memberCredit': (totalAmount)
                },
                $pull: {'baccarat.sexy.txns': {txId: {$in: cancelTxIds}}}
            };


            console.log('updateBody : ', updateBody)

            if (cancelTxIds && cancelTxIds.length > 5) {
                cancelTxIds = cancelTxIds.slice(0, 5);
            }

            MemberService.updateBalance(betObj.memberId, Math.abs(totalAmount), betObj.betId, 'RREE', cancelTxIds.join(','), function (err, updateBalanceResponse) {
                if (err) {

                    console.log('err : ', err)
                    if (err == 888) {
                        BetTransactionModel.updateOne({_id: betObj._id}, updateBody, function (err, response) {
                            if (err) {
                                // callbackWL(err, '');
                            } else {
                                // callbackWL(null, '');
                                console.log(response)
                            }
                        });
                    }
                    // return res.send({message: "data not found", results: err, code: 999});
                } else {


                    BetTransactionModel.updateOne({_id: betObj._id}, updateBody, function (err, response) {
                        if (err) {
                            // callbackWL(err, '');
                        } else {
                            // callbackWL(null, '');
                            console.log('YES betId : ' + betObj.betId)
                        }
                    });

                }
            });
        } else {
            // console.log('NO betId : '+betObj.betId)
        }


    });


    cursor.on('end', function () {
        console.log('Done!');
        return res.send({code: 0, message: "success"});
    });


});


router.get('/cancel-pt', function (req, res) {

    let condition = {
        "createdDate": {"$gte": new Date(moment('15/02/2020', "DD/MM/YYYY").format('YYYY-MM-DDT00:00:00.000'))},
        game: 'CASINO',
        source: 'PRETTY_GAME',
        status: 'DONE',
        'baccarat.pretty.txns.txnId': {
            $in: ['c1a31d4d0f464e1abc5357afbe47dac6',
                '4559c0698ad647d7b7c8fecb885df5e9',
                '11b0468e904542b3adfd709cbd9e0dfd',
                '7eefc798b7b042d5aa6d7f294b67ccfa',
                'efc48996d7014dcd8556641437d3e562',
                '98081b5f96d24bcc95634552a7ca8bfc',
                'b3fdd0364cf2486bb8e7ffd3d5b09ffc',
                '9ce579f2a875439c983837c87b1cda12',
                '0e2f4d377aff48089d03d2658be2e602',
                '239c1e41b9444f8cb3f222544ce49513',
                'bc40bffee907489abc295bd0be79d7e1',
                'd93cde8d19d6443cacf22c2e1a2358f6',
                'cac770c873804ca88bbe089de4f73ab6',
                'd4faafb3a11d4012bdb3cdaaf0d065da',
                '32eed2494d454f96acbe76a7e5145d37',
                'e141c85a709e4b109368a1b0e63d8b69',
                '64dbe1182ced4f6eb6ac931c27273387',
                '070c91be053f46b0ad096f7774783ec3',
                '102c4cb7a9994dee9d6eae443677daec',
                '4c26b6823aa34f9c88600307ea859ddc',
                'e0114d93630e41528ba27f8f64b1e9da',
                'cc75457f5fd740bf8c5012a1866aa5fc',
                '4e238c3db08848b5b0e4629a2a850677',
                'cb0705bc035d42a896b3daf9c720f982',
                'd2777963edcb4b18ad3f54308cdb5b26',
                '0720c7c5a94c44b6b48271370deb89a1',
                '2238ffa3428549d7bc26060197595265',
                '79f5cfbaf6ae490fbbc8e5cc1d04dc8e',
                '0df8fd78c6e34753b82a3c48f4fd3362',
                '9c10ef43e6ac41eb9d22c82806f22dcf',
                '6da123f372434c67ba4bf4130aadbd7e',
                '15767ad5b40245cd88887d66db15ac08',
                '5f5b3fe9d87b4b19851d1567182e6190',
                'cdf83f772c624b0db1c34d3478a9478b',
                '79cc38e589894e46926e7e92fcd90b62',
                'ad82a50449e74866997365ad5f05ad5f',
                'f3c3bb0818da4880a972549d64634f31',
                '7be191251de54febb8ce0526b2111901',
                '4ed05468d4b34a6db066e7ddde88ebcd',
                'cdf7c394486345d3bf9eff981ef84da2',
                'ddd31412c21d48c388de78cd2c86de06',
                '1f6a963d434c4b1c9cd0b043f51caeae',
                '7b14f939761d48478820accb9d1ab720',
                'd5c4954a8b8d48d58185d0e23bbff249',
                '8f9490de8259407c928e4ad4dcb24982',
                'ff1119197cf54c01a17f15c2515d3505',
                '7a8f97b6073043198c76795270223060',
                'a6c33202a73d4b5d8249be52a53648e9',
                '5c849bd547594581bbd761ad6610901f',
                '14fa6fd4a92e4955a974e5516e90e3ad',
                '5e4515877e964e43bbed6e4202404318',
                '3e7aa57e947b45e2ba291f45e3b86088',
                '0baa1ea63a77412b9607dbcbe25ff183',
                'e99aeb310bfc4c9382e59c334683a174',
                '8505f743dfc74f6e886b97d4a999a4fb',
                'e83048c1c3124b10acc027ceeec7ef2e',
                'ceb104a554fc4a86b2392f1abe5bd08e',
                'a14d9798047a4e5890de98874cf8d13c',
                'b0325efab0a24b3580f156df74f26abd',
                '5ad464bde2564e919cf5439677e4f089',
                '5cbbde8ad8e74156bb78238d5abcf302',
                'c7ec9c7c20374bd7b3b4d4ce1efb9d29',
                'b446a1f0699444a78d34e64b93a436b4',
                'e090e454c0cf4445bd41fc137e2be889',
                '492e288ed9434dacb81d97b400517fb2',
                '65cf0b0815944f4fae6c50311e4b664c',
                '1842acb8be4b441f828a3f18ff8d8fe0',
                '2e320b2aad0a46aab706b797ff1cfd41',
                '9425e374511640d8bdb7e7fe90c02f57',
                '9ea70d59be024137a79743beec03c400',
                '4d5a643df2ae48efb427fe06a7cd518f',
                '97c24060b4604967a491776ddce9b82a',
                'd08264310b6b4bf1991a939619f2efe2',
                'b38e1457a9724c82a0a4125bb6cebd2c',
                'c819cc070efe430b9dcc34ba6df0b279',
                'c9762a0467264cadb1745666938e915d',
                '243df885f74f4515a6d8a77a74a1662a',
                '7f7129355f7c4e34b44e9d50e595f711',
                '0dcfba6e96f44b22a04a09ec65974d90',
                '5344828b34194e789d689faa102016b8',
                '121e91a64e324762a58e7935fb2c1553',
                'f7a0ff9ea0bc4eb7bef62295afa715f1',
                'c71f6a2e47574715b41245fcfc012d78',
                '5e3b158c28dd4fbba331f33525fab1d0',
                '6bd4de50daad46ea8c5b030bb3dd8c32',
                '92520c2c47ac4680856ace652b3e66f0',
                'b94a41b70f9b4a65a39e8d8d566b1c11',
                '861b09291d2141b2b37b9d3e0d7e71c4',
                '525734d5caad49538b861daab5aad23b',
                'ac17293fca35493a90df75708044297d',
                '50852399f7da4e71afa469c05760f901',
                'fa6cfb82570546429349b3b5a5b6a719',
                '9a21b830090b43e4ba3d17cdc163a384',
                'b96c9bf6c2444835a64e8d9b804e3714',
                '07abc50556e24a878da8d69459c41021',
                '60c74106bc184ffc80104f9604e6a461',
                '182f322e02fe443d9bd5cffcbc623035',
                '9cc9fafbb817424c853ae34e5d1e1322',
                'a1a3dfe0a1dc4bc59117f913b2e5b1cf',
                '9a4e3a379460408aa3c98f37eca46959',
                '51d55ea8b9de48d1b204419a98ccfd51',
                '2e0980b1648c4b5192763b7d77918518',
                '94ed772d1c804d4fba41f5946b96bab8',
                'f56e518c40ae4d5cb9ec6ceb98bed4ba',
                '73a8473cba5d4a4dbe6e7fb6260451e5',
                'e9e38601b68942a0a373a2a024ad5c84',
                '03270622f61e4e808e6b528b585ee8ad',
                '810753c7553747aca37451cfcc4b6bed',
                'e136ac4266ed439b8b8336fb91173962',
                '8f7a982b16534912a50c5ace6aaa9515',
                '427916eba60e4e298370101fd5a7c7b0',
                '2be84a202b2744a1b91e0a2a33694155',
                'cb267580d7d144cd90f405cb2697d0ed',
                'b4bf49874bd74e23851297342e809837',
                '79fb300921aa4b4298912d17be115e00',
                'b7942f36bd3f4f6695ca2fb481c1f067',
                'b4eac3681b3b4e389dbf72feac755783',
                'e97e332516ba452ab308e53e60309721',
                '34a00d45a7844105bd08acb21502e66f',
                'e4d58e47561d4598bb0356263a87a394',
                '74cb74879e774d92b68cb8f668946a3f',
                '575094e2499b4992857b354587105b93',
                'd1b5ae775b66412e99141bc0bcfc7afb',
                '4aa34589405e4b3a92a2003e9ba4378b',
                '374b5b59be044183a28f1b295e43bab8',
                'f343b0945a464a0184ab8b430b9598a2',
                '18026e365ad542d3b187bc7a02950375',
                '5b1a5d0332c24387a65d5730d3bed6fa',
                '11fe93e4722a4546b943644c2e6a942b',
                '8c3a6f57992d441c90ebbdda307310a5',
                '118aab0b81434c9b8c332cda10807956',
                '89d74647ecd84a2ebe819c4eef63003b',
                '9f98a32293954bc38be212340b06f390',
                '574915caaea84d639baa181a584f9aa7',
                'e0414aeb427c4de59dee64b6e0b066af',
                'd3d7e3ac260541fc987f77e35be19d94',
                '10c5547b7df54b0e894e40ec125804b8',
                'e95a2f4ea87d4d6baf1223c9700f2c8d',
                '2747a7e711d548d098ff7099de0ef55b',
                'ac7e5deaf1804062a6fd5f4960943d3b',
                '60d73dfff21e4dccb5756e71d36666f8',
                '15b36c5310544718a9102134ab3f0f14',
                '2e13a0c7b8c6466da226939cf4f8a625',
                '333c04d3c7624422b6b8cd570b770a1e',
                '436dbb9b4b48460984676f49695013dc',
                '842ae803636b4cf198fb13cef7a390d9',
                'fe82b40ac16d4cb4afe8c9be290681c8',
                '47ed774274cc40a6bd765f76ecf990f9',
                'b64cdf5bcb3b4b07aa9fe5755c3011c9',
                '6bc269b0407a49cd82e07e47de1092d9',
                '28c8d393949b4738b1ca396187851e58',
                'ecfe52441cf248f38c4a66ee953612a3',
                '27af4964ac6440deab29b5005b46f934',
                'd16a94c934a74564b2ec90e33f3822e9',
                '9e02a894dd59463b85872d4455a39400',
                'a042fc06d4664ade91302c32e12a4553',
                '523221eeae404ba6b9b83fe0b7147369',
                'dff5708571d444d9999c751cc7e509b9',
                '07da047bfdf44406aab28bb096a34e70',
                '9dd63c8d6e814aa7951359db4c7751b3',
                'b222e9bbfe714e268a94faaf52b23daf',
                '2c7ccb7d11ae4161b59d11119c272768',
                'e3fb1e99a21a42faa95865cea9af3c03',
                '73bf4cf3bade41739bd4d87ffc446033',
                'd9eb5a3f3bc04622ba4179d9855abece',
                '94ef84a38e7044ada9f2d834fa55499f',
                '2509e186f7da48c4bb70520ead0588b2',
                '6875dc0ac3024c0bad899fc35a8a97fe',
                '0322985d47354418bcc271c405714741',
                'd0c6a04181214dbe942a35224deae5cf',
                '59a589b753554d079594c67d20fd78d0',
                '64fdb2033e3544d791694dc2d0e715c7',
                'dc49b22f34d7499e9d210b1d5e6d2f0a',
                '49fd90b643de46b5ab488854dcb5081d',
                'a0f912cfa9024911a759d75eeb8f7702',
                '62ee51d2bd1c4b409e0aa8306d376d1e',
                'adb24e2584614807813a7bbef5bdda33',
                'a2e386d114244d8a828651261dad93b9',
                '5d12c5f1146143458e09f358c9a24191',
                '553ae11955da46faad7a8197be20ecbe',
                '3687deb8f82541359aba4f3c64c492f3',
                '41bd9abf4d97442ca8adb2deab04a883',
                'b2aef4c0e02a4879b6574ff05773c495',
                '78d32d5392374871a136ed34ad4bd29e',
                '035c2d0191364ac083527eedb28dd8f7',
                '39840dd0e05f4cca91c78790c97c5b49',
                '3a5a2e0400104974990536b10e8150c6',
                '778af1d9de3c4aaca6c67808b403b6ea',
                '1363e7b7e1b54dd4ac2b86c4f9cb22c5',
                '78668910af0d46c0bf482a270fc1abe3',
                '57ef3b602e7c4f528bc1ea70163e318e',
                'e36d771a83ec451fa3a4ba5ee4a9e9a1',
                '63458a0c976a4c9cb92270b7905f4004',
                '22abf3fdd01a46458c0b9ae9b5d8fcfe',
                '274b0307bbda4453b654d3382615acd5',
                '6f8415724355435b84b014a02ae0f231',
                'af380120c2654edea399f19d62cce9aa',
                '4a49869187fc4f49ada2e13862dc9b8a',
                'cf67dacc9bed46e5aa6d842dd2cfc35e',
                '7e93c0755f1d4adcb970a73b62bf7b04',
                '73115fa9a38c4fb0a0bf51a7c668df31',
                '687398d85751410ea3193edf6cc4ad9f',
                '20cbc196dbc74528bb763815bbe20b69',
                'fe1451f4fe944db5a8d5fa202abd5203',
                'dfb0934c2c534a67a9a0592c762c664e',
                '7cb703935d1443fab1a844cd3c742406',
                '65adff78507b41fa95f952053083e72c',
                'eb01d08a817b4e8196c50ed55ea53055',
                '5126103e588a49eaa5be1b655e03c17a',
                '33ff3c3189d84f05938d38c7a006a914',
                '0f68d3e2908b4bd28ae8bc604cab49a0',
                'e69df382ce66490a8eb360362d59e6c6',
                '078f57c346d44af3a988de9ed37c270b',
                '8217810c3f7643dc90e92015fa4664fe',
                'ffb6671a913f44e98e7b9cd4392c1bdb',
                '79f038be4a2f435fa4ef5a4de99c8d58',
                'a3e84010c6e146f48cc2b8aa7db7e5ea',
                '79183b234ef04d0da213d62bb3a97518',
                '7df2b599e85b4756959279a2e93afea5',
                '4932e6d7e6ee4781a53bfe7c0fc4a8c6',
                '17d2d8dcd38544868166189d82ce7a56',
                '933b54db339f4a8680a29d514ec1a9a8',
                '7836455b1c724a6d94677e9e65a51daf',
                'b98f7c96cc3747feb46c81c990d3bacf',
                '43979c42bb8f4def858cff2d52fe404b',
                '804728254dcd4086b382805569007bf1',
                '24ed66ce83704e32ac2d42d5ccaf9d5a',
                '3139dea9d3744698a340dc0d71ae5c46',
                '8e8f20c6de714ec4adf3ebddf27738f2',
                '5ffb106d17324f728edc7fdcd2dd37be',
                'e4e0e1915da14e10b0b75fdee6b9afdf',
                '523c52a586664a6181899043e33e8079',
                '88d52ea8eaa54c55bcaceb8eef818f57',
                '4c03849dd2d843178aacfd09e4f72045',
                'a20cab33a8ce43309ca5df01af6a1136',
                'ac1cfe6968884ccc9deac57b80def965',
                'dbc9f4cd598d4e7fb40591b7f8b7297f',
                '1afbb568273941919f6de87f8c44b581',
                '1faff3ac56254e9a9f474f15a433968e',
                'f1f156f40ef84ef3a84df6f103a6df42',
                '350b67c178bd42d7b425155bbf5243fd',
                '4b41ad4dd6ad4307b19a06fd20812488',
                '5d0a8c51737541f3a0ec6812f2e77a6b',
                '55209865951b465086ca44f44ab724e9',
                '43d71607f88e449bac7481c7091e0c44',
                '8f929792f13d476ab8ea6d4eccc8ee89',
                'b4ac7c1f57684ff4bcbbff9255a09775',
                '9f0b4af413114829887bbb8e0b1856fc',
                'd06f4ad3463349f39ce87148829464e7',
                '0a00a367751741439fb7fbac13043e4b',
                '80b02a744830435e9c284005445c7e32',
                'a23f8a4667b240c29586bf1ca9ddb1ef',
                'fe3c6e690dc44cfd96eb907904ef15b9',
                'b08548f1d8e64ef5aa86d9434ae68b9b',
                'cdbdd46e87cf406eb755e9d053231c46',
                'df5a91d7a31f4ed5ad114880e8e48e54',
                '0b73a59394404d958ab0cccdce1c0e7f',
                'a0cd327cd64648f8a479fd8244e2c21c',
                '1d4c7fb0e7184c64a40949e71943ad83',
                '21a7bc2c5644495a9e55226c6f1bb935',
                'fd1c2a5a221c46af814aa51c27f10d37',
                'd47ef50bfc15444784688ba6b8c7ce5c',
                '4f670a288e9b40198fecb65efa955c1c',
                'cfe45149e6a942de909204318615c331',
                '31fe1875916b4be09a0934d8f9dc7988',
                'aca90fb7b165487c8f1e650a76ed3dda',
                '8fb81fddf10344bc880497bcc6c98054',
                '23bba1a5d9be4fdbb514368b5dc73d1b',
                'a9b65cfae29947d5b6ed0b5dd8ac45c6',
                'd53e2f8e41974932ae21d007c6be24d8',
                'a1228a0f1ef1402ca469134fa44aacff',
                'c48b3f882d374db4b984e076a3933c10',
                '7f65c2da864f4a00977116dc7c7bd980',
                'f17a9e3d6b604a4fb17b53a4015b0615',
                '9ecde1b2e0f64d358fb8858223103580',
                'b490126a2583443aa5aa9a67aa2f41bc',
                'ae382c12fa7045f88270e7435865bf33',
                '9d8f2879482746c58c84a8c155cbc103',
                'becc19937491437b8e3fffba050b3207',
                '82062ff9e645441cbaa70c9aa1941d0f',
                'cb1686e71f6d4fe0bebff833476ebdca',
                '5b6568c4f9c64578bc4adb8dbdaaf3b3',
                '9c60c81f7504431dae636d2da7453800',
                '33344008f3b1402dbbb763a4d4433e94',
                '26d706c8b8184513883f19e633ba523a',
                '23be07040cb84bdab1283b5613969232',
                'da467a9d43ad41f1b26c2a39a55bcd72',
                '60289dcdba0948a3aef87d1e590a9d9e',
                '5890dc145c894b68b8ca3290ea3c99aa',
                '92acef7de03d41729b42dece2b1208a2',
                '8fb429ed7726402bb94fb9f043d90980',
                'fc0b29f20ec74c6ca95d76a22c74a23b',
                '420ad453e6724ecc95de81097ffaf637',
                'aaa649abc3224ee4bd9548e3f61126d4',
                '26fc63c49e6b4c00bcfb76914d458996',
                '008da615e4cc4d1c9e46a36bd395e8af',
                '04484d518a214535af3611660522764b',
                '942359985abe40eb836b8393de1002b6',
                '3f531ab8158147be8ce2812cb726ff03',
                '28b80404a5164efaa00c6cbcf17f05de',
                'f26d8f00f9724ab18d648298454627e0',
                '181bd5ffd311473db1dd2e2baf011943',
                '0ceb124f28d340138e3bc2e885c7967a',
                'c02a5c8f8e0847538eefef75aa7090b7',
                'edbaf72a631347789d32159d4d6f66db',
                'cc41f0fdea9e43009188d59258a5fbcd',
                '00017c1c0c9b458abda98c6786f9f5d4',
                '6787152931d146649f796b481bf9b065',
                'ac167751ebee48f1b636df688c88877f',
                '875d793ede2142dc8652ef1d2a3d0d3b',
                '69b51d9f9e7e4934af9ef5c09cfc210d',
                'e1493fb0106e4b15b59d2a2a4ee6aebf',
                'a6e2fad7d6be4038b55100d04c037dfa',
                '20653a7e91bb439ebd74a141b379dbee',
                '4a8f4fadf60e45718397b77b9ef9d191',
                '26f9633691cb444b99d440d25f3c55d5',
                '3224f2cdfafc428c979434e5b42ac45d',
                '6e243250827343c6818ef58a987e7b74',
                '05d85ef844a943db99411781f0f28d52',
                '56fcb3afb340456389bee0c5f78dac3a',
                '81fb58e427764a7eba40d279b1505788',
                '807402c172144d078906a2e7ee5ea5e8',
                '6aee067a948747a9ad6364ccad9d0662',
                'edbe2774596345debcf85a0677f1af96',
                'f3508ebbb0454b0da9ed09a7e70ef7d3',
                'c82fd0cb9e964b9cab70fed5d074dd9b',
                '7691c9575e924a559e90e7eb718005ac',
                'c198137573314be18492662873a73d56',
                'cb2bcdf0226f4469be98d37dff18f2c0',
                'a7076a8fda4247e58572932330211a08',
                'c87f59cdb2b24d59a6b503c40377d683',
                '51af818751fb4581a114ea8e580f0cc1',
                '983095951c4e4b1c8f21928887ef721c',
                '2fc86e6190ea42698c6a25fa3cf8093f',
                'aaff0e5b37794ef08f9f0a62dcda291d',
                'a51b1108d3cf4df8a66eb02ca95cd317',
                '33c9b188da4343fda92bc09d4eaaa0d8',
                '81090a07f1414f2a81d2880215815f90',
                '7abdd2cc07c8448d9af6a1365e08e7f6',
                '7f170534028a46088ba4f458fc7a522a',
                '1ac596ac25f74ae4b1888f0c2ed86116',
                '6f4f283435fc423a8eb82e0a9b099244',
                'a7257853ade145cd9582b24a56913abf',
                '4d8d18dc1eb74c509746ae3bf22d8420',
                '1e7edfef75e74a779b3edc9102f11cd4',
                'e1a9a739359a4bf7a50b254e328bdef0',
                '76f41df7285e4684bc5889910776398b',
                '3040cd3f5f844bc48ec238aa11b86efe',
                '6301403d4c254a8eb71eaadaa48bb18c',
                'ea7a74ca8fa844af9398c5aec9d10360',
                '88ef18101bea4f3d91b53f49340944bf',
                'f9c79e329cad48ee8ecaca3448078d64',
                'fce985ad41fc429689d8e31970d304a8',
                'af41a1e0cf6a4f03800d8f957d712727',
                'e155262f5b2046dbaca558419dbedec5',
                '4e5d1e4ea88d498aad81d65d7c82df7d',
                'ae4d8eba408a40a19a2743ef747ced5e',
                '190d84fbe9c3436e90f5b5336c4d48bc',
                '2db04e47d5794aa4b95e2009fd706558',
                '847c53c67024484aa3dfaf558f958bf0',
                'c3a970a80ef14b4eb771981d7e87b88c',
                '5e0d3d683d7c4bb88d2020bc5889f082',
                '2dc5d4ca655a4c4a8255388bb4b63c7f',
                'e2ce7072c93a4f0c826f27fe48b46cd8',
                '278e1b8904c34074809972668cc74fe5',
                '2471ea10761f4b8abe7542b63f6c98da',
                '957839bbc5534a9094d0b07acb792e6a',
                '9b0aa8f42210455ead6b0aaec4cb9276',
                '2969b3b143b14733a604ba1a13e23cd9',
                '4229416e959241d79b876ab37b5b5c3b',
                'dae6143b08c64b8faa0434418271eadb',
                '1fd880a64fd54d2197d8bae76e376f1c',
                '094ed6481b3d463ca68b2dbb1b2f2c16',
                '81cb23a3d25b48cca21618a60793bc34',
                '332c6b43c2614b98bfe7b36b53c71452',
                'a75cf0fd5f2c48a5a49403f89ae02143',
                '6a6e4069e60d4d8897a0ee4aa1fd3b85',
                'c85b3d6e8390440d8e841c9ce2cdcb90',
                '2866cc457d7d4c4e9ac7aa8f484c46ba',
                '26e48244826d44a38aa8e28cd32b608c',
                '3e100c3e91df44d6a07f204ddfa6ebcd',
                'b5d173e271fc4f748d8b7a38bfacce8d',
                '57e3506f8de646c1bcabbda202a50a6d',
                'ef80b18987f042f0865a0c4a9d9f0b63',
                '83ad734e68594abe8feaae7cf61d2363',
                '031ebed61c304a89bfdfef26fcbe7b30',
                '3bfaa2e2921f4591b106b3e7285d08d4',
                '970354a701ae4e3db81dc2c4af95cda5',
                'b852f7d7654b4640b35513d1f75a9ef2',
                'f8d629ea780c4c7e947fd4d942641b2c',
                'a75836f2cf694b8bba4a4faa65de2637',
                '27dea07ac80e438587835bdea1b84683',
                '6923170abf914adbab49bf015de23fd6',
                'a8c31635a31d493f9bfa6c4e45bebb05',
                '42e05a998bfd43ba8c8d441d02529593',
                'e0061c4bf5b34ac290176ea7b77660ad',
                'c63241984a394e18b05c1584cf6cdf96',
                '0caccb33a7174c9fbc7ceb5c73973451',
                'a054c086ae3e4315b138216df064629a',
                '12e1a7eb17634c5b937d6690b66477b5',
                'ae9e734a2c95491485f4dfc2861c87c2',
                'f2f10f9ae6834b0da9ef468ddc83cbfe',
                '4d097c2c98014cc0bcfcb8d1d567d1d2',
                'f407f21d72794a7193b2d9751503390a',
                'b7779b2e44bd47f8a8c48eabad64fd8d',
                '545c7ba080354924960ab27b2b4810d4',
                '234972a2640b44dd8a206154d006a9a5',
                'c95d8aa400624c7c9dbdc960b4eeb0f7',
                'b9b324b1ba634a67a314bd41fcabe26c',
                '2aa8b15e03fd4a258506ab6afec9c952',
                'a983828d9e8c4595863fccf1010f3d20',
                '63d115f2164f44fa8788427f66bcf777',
                'fc40c5abc97e4e94b69f197951e5b7f3',
                '26391c1f7a234d37a904e419082b6c1a',
                'aa49b06c0154447db95984740a585618',
                '7615dfb4f35c4934a437789e0f858585',
                '67144a92acd54a609996828f8d5c0166',
                '9416e252ea504ec4aaf57fb08970b675',
                'fedcc2137f5a4c278dfc6e8802d56565',
                '122f14faa7c74f498f62f20380047902',
                'a75ff795b1a7438eb55c62d735897790',
                'a00116c94d934fbd99aaabe2a9c214d9',
                '67847b36e4f645819022197e06444501',
                '6a2d4b8acdce46a7b7e07cf047b172f7',
                'cca1a5ebf42e4f0ca69ec3ddbaa6d470',
                '2696229245044513a5ccb89cc8431104',
                '7cb11e2d3d6045f29cbc534ff1f612d6',
                'bc6548dcbc7744af88879492bb8d46bc',
                '73e1f3d17db24db09752ff4d0c785795',
                '93fb0df5140a496cba750fa8e6bf31c5',
                '069240125e614774a82f5eae7a9828d0',
                'b0ece922e9c946d287463b9e625a1c5a',
                '4024ad55011e41f3b98ef2e42073aa27',
                'f57333bfe8e7493d8d1d1528429a9b9c',
                'b6d352bb870643a3a977bde7eba7e812',
                '1609321328114b9195cb4ccb95c5adfc',
                '1fecb1e82d5a405f91027967f22e2378',
                'f11fe992c7c14d4b8ac35e1e6e480af7',
                '1eaad842174d433facf36d88450e6262',
                'ad60766aaa2444a1adce7112c638c0ad',
                '3e5fcdf800cb4795982032fbda0a7c05',
                'daf72f2c4390426faf86febf2627f520',
                'a3e26374cabf400394d6d2e12456101d',
                '149b8cbb12c540e695e75cc6c30f72b8',
                'fe6956ae14854b42a4c764a928f7fca7',
                '166f5ac7611748af92366298e4705a68',
                '55517a3bf6e9421a89e18d939d54de2f',
                'd51e1694203f4c7e9d946b9ac032f8ef',
                '37b6ff294a7f4e8a9254aa77b97c7183',
                'fe853dc8fdd94dbd84c21332057d4f60',
                '04bd0785c5af491fac0cf6cf41ca9a4e',
                '97516e92120e4db2be0765e07712a892',
                '00c3f79d00774352a4e94032135bc2a8',
                '2ccea60eff3c493a93b26e498ddbc157',
                'ad9914c263394f77b0e6da920cabc10d',
                '2a4364dc720447d6b40c3812dbf756c5',
                'ffb6da95d5664aa987564f708082a1ea',
                '7680c861d74344fe8ebb15e29da833d1',
                'b6b0111786a54dd482ecee3f1260623f',
                '30be2cf10b7c4263a7910a5129abc2af',
                'f42f953610fd4b09b1206a3f2f40f1c3',
                'f05e158f9c444335a1ed19e6e9b987d9',
                'c2ab81adc6e543448b66cf1d0fd01f21',
                '3b9e78a8c01f442b88aa9c402305b0e2',
                '3d685b8abdff47a1ba24fafc93c35d6c',
                '99cca212f3914c69b18f0f851dd73e5b',
                'ede80efdb8c240fca57a0e81b28d47e0',
                '27e3624af7db4ad0ae58c76f377615d0',
                'c99c98eb043e4b41a051c36a51c572e5',
                'e6846d2938104d339a454dccc07fd1af',
                '53a28b33441d498b8492ef17213a5dd8',
                'c709a14f12154261baf253a5f81025f2',
                '19e0715947a74a32bfd5be5e4bb4d829',
                'adee54b0edbf4d5684a91078cd7b17c5',
                'e98772272af8479b972b02384e7307d9',
                '77298417caa94e38817d06ae2436dd4f',
                '4c58963c66074774b434921a84c4f18b',
                '34cba390c46348af9c1a804a08fc38ab',
                'cde1a2b6159841ed9f64eaf49ad62ed3',
                'bad4a902e7dc498abb6dead20d039304',
                '516f48d5a1f94d938873b520c8b26e17',
                'b271ecd3402545e29f986c7f05958951',
                '8b3f4c2408384c20be6d8deca55bb717',
                'b667bf20d33c4358a1387c616895061c',
                '17ca763b2beb453696edeb7070a80095',
                '435da460b4a0439a898d9b2e28670623',
                '845f4030908443c5b5951768aaf1eb2e',
                '4733ca0f1664415a9540a124fd926881',
                'b28b73832fb64e46912122fb834cf9b1',
                '6891582cc5af486aa8bd31ed0fee90ea',
                'c5aabd70af0841b9b3500397a4785488',
                '56274da7b31e405bb723ef7c3c00926f',
                '9fab5d0a2cd94f799d20de60e3aafaa1',
                'f79d2b7ac9bc476c93b69915817fd71b',
                '0b2f599fe2d742358454771cd0fd7171',
                'c1a2b6be762544c489e9ba535633cd83',
                'ad75343da75c403a883048483dae5ef1',
                '374094d4a9614166b085cf60ff59bdaa',
                'ce8d2c3aa41341cd9b2b309dff56bae1',
                '07f8d42bf9cc4b69b8d53a4c0fee26f6',
                'dee37e8bd99041a384c5f308e9f82bdc',
                'ae3f3cd4415f41fb97670b72b8a9383a',
                'ee86649afc254ad3a891bb623e9f0352',
                '72cf1875b57d4445862bf865b9e2234f',
                'c9eb2b96190e41d7a9ff1bac133d621c',
                'c3f86358402a4b1b8b904f5b0ad71834',
                '2fa0e284a0f74acdbf850a2f3164dc4f',
                'd65ffb5f2cd643ebaa14db39f8c9fd59',
                'a1a08e1facdd434191b12287de5f0c49',
                '14cc3f48f6bc45da8398c41c7e40ec1d',
                '4560e805bf6e40fc828297955c4a7235',
                '0df178b37aca4b0ab0b281a834be7050',
                'a79c4b0558084383b0db22dad78075ab',
                'c9ef02ed135742948d133a8169990ebc',
                'cd9ee892b6a44f078e0b9101a4b9076f',
                '4baecaaa75474178b0292d25200fde22',
                '595974cbfefb4f7eb1cf05d0d447575f',
                '5d9965d9c6404af8a0fcacc64c4bd4a0',
                '8bd0d6b1fbef4d859e604eaf21c8c946',
                '1af81c8083e44d9ebadd70b92835ded6',
                '2504a6e3a8c84c56a3fbc8abaefd4c11',
                'f944d3c41647430bbed3157acef7cc92',
                'ca83d3b2c71a4630bbcf33420f397096',
                '7ef589d82f244726819760d3628a10f0',
                '7c48aa8f266244b38d5e33e60ffa5460',
                'c7988aa1d4d1425485e73b59e3d89808',
                '72d51138136a4867a5892517a20a5cac',
                '4fb683c506824ca89cee35dae7ca73e0',
                '74040c27cb6f428f960bf39fd4735a57',
                'aaeb3780aef947c89fc80f6afb0ba515',
                '499b4ba4b9fd470d9ce7de035fdc24e1',
                'cd3a6128656b40ba98695bdf43890e43',
                'c2a99f8fee1b4b0eba5d388269913a7c',
                'b1ea344704424f6cb3f22cada5e29281',
                '199e14ce900a4986992e0c50c0c5ef52',
                '5b5795d23fd74bed8c72c34dcf9e1525',
                '7857f1c3134c43a7ab0c5a8f7ffb1615',
                '9897d2d5c8db4cc9b52e9927409b0384',
                '1f22328c10dc4a59a2f862474ae3de69',
                'af8f7d3a5399419e83b8edd541d3654a',
                'b9f5157ef30f477f8e81dc595d45941d',
                'ef5d7c28912f410b8c23fe152e9ea4cd',
                'e4f5f5f2e60a4c7fbcee5e42e97bee9a',
                'cd812d4847464358afab6edc55c16b83',
                '4d10d1d8d2074b3192194e999a302576',
                '97cf492cb5ba4915ad0e817056badd13',
                'b3c353cd32844df197490c7a896612f3',
                '08cab65592df4ba3ad15f16968a78d9e',
                'df1ad3963af441199e5fc819ea3d5296',
                '60907f63a8fa4128a0d125d344f25911',
                'dafc9ae951b340e2940901eaf72af677',
                '34c1ae21e47d42e297685b8714a83861',
                '20ce621d13664da7a7854bd0103bd188',
                'a1bfed1a07694025a33ad5609711539a',
                '9aa375591365414994dc7fd51014a4bf',
                '5718343b223e40009c27142a0e31eeb4',
                '27bb817395744517bb368cb15de38321',
                '204db1f40fdc4a579d001df799373b76',
                '3ba530abbbeb4e5aac30975a411f63f5',
                '7adcdb6d7a78442eacc202929099f2ed',
                '7f6dbfff14b94d5ab1800acde7ba673e',
                '5bafba591a3444858b82a0446cd18c23',
                'a22cda78c46341fda1a85b4a5abb5e78',
                '569349fa4b0143fda8fd361dc67ccb08',
                '37ada9b992b34ea5baa5603fd10e3daf',
                '8d833afccbb642629411d626b38e1ab5',
                '659a13aefd24410681a4190406da6857',
                '38c73a49faf841128f15d27f9f4387e3',
                '19d34d82cdac4e21ba1f17944de78a57',
                'abb591a70a0e47858ea5993938993367',
                'e0188a5f04eb4c609df4f32e68a77e9f',
                '6aa4bb0a0f84455a97e6bc52e54b9e31',
                'ffdfff21306048529515178f60598b3a',
                'ceef235b5b994556a4cbc63ace566ade',
                '803206532a484a2c8a8d0f844e111782',
                '946e0d96edb44fc49eb1733ad896b922',
                '3ab62a84266c4144a7e4ed5e426e844c',
                '3a70f85e8b2f4570a6aaef8d67e77e7f',
                '4ec96a7be109402a89f7d5e4b544bb3e',
                '81bfab030384492b87ad858b86a5e939',
                '283482f60e584300bf120b40626eb615',
                '7f6ac4ee635043caa16529d337ca86c6',
                'd8e615f31f254d69b782658732aef0db',
                '4b162d425eb44829a546006b1423df50',
                '42fda60c5a664015ad771248eb67b589',
                'b429493cd611470491fffa4d2d91fca4',
                'ee05a6f8c8f441668e35da44353fff1d',
                '26db616623e4464c8792864aa882ee4b',
                'dc2c06f1b65f42e9b310f47da888dbfe',
                '75a49f4a8d764e10a188928cc032d39f',
                'fd279a3a4856441e905e2c25737e39da',
                '15641e33c23f4f7fa23126a7dba81d05',
                'e5416354e77f43f1a0f21b0a8867a70c',
                'dcbcfdf8be8a4b08a7cfcb1d6e2d5d92',
                'f85e47b7995c49a582b19ed99533d526',
                '211c4fc8bed0427281d77fd99ff2eefe',
                'bbc085011c1b4b5ebe3a60dbf4cc4d3e',
                '5d0410400a9743839e613005cd590aba',
                'f00b985bbc59402f8a52cec9aae8ce7e',
                '069faf2c3de24da69257c20e7728ab0e',
                '4fa8ed7372c942049031e565437da00f',
                '4022e4eeae6544e4a51b89b7269fc78a',
                '436a00a4b4bc4b1584dba9563d6d8efb',
                'c270c70c44e44abd995287462b322089',
                'dee35a4f108849f5b491a41970d43cc0',
                '538e0d12ad28441788420218b5acc874',
                'daf6d66c3f6a46489639c265410a7e66',
                '138d0c0f397641a5a347c57196325539',
                'e4049285afae493b8737805288f54a3b',
                'd44c48b11de64bc3b1d2ec847b892dba',
                'ee83bd02fb914006815ee21d383913c0',
                '256fdd6cda2e40459c999355f6e30e22',
                '10056f0cff654d49b4ec201c5e6f8ba1',
                'd1dae2a7ec764f27866b71c48e5a9a9b',
                'df9a593c0b594bd0b4dde38277f0ee6d',
                '38d9621de2574a0eb85fd3ec8873da3b',
                'a4f8c8d46a784c6b88ddba55f000bb05',
                '68bcaed6b22346daa7fa6f82d4239716',
                '1b1970317bd6493db9252117ad773a70',
                '4730a0a9606c4235ba2d090cf5f2741f',
                '8ec70899834f459b9977dd516c68d610',
                '45c520a7909f4aba88147e3c9acb72d0',
                '938ac6f76a7548bbbc2d0c7e90691f42',
                '5b3672095098425da0c7cefbf6ab400f',
                '507c2418ebcb4540833650625d78ff1e',
                '3b46e4a8a50c4dd3816a7d8cb27a5081',
                'e89c6e82698d4934b5576e7cb12e155f',
                '1e4c9e930a6a45d9a90f97009dd8d362',
                'efa5ef2d27f84d26838bf4313eaf45f4',
                'bf93d25797af4950af5ea7b6fe05f136',
                'a62cd5990b0d4e7a8ef31c3ed3784b46',
                '5a2efdfe6ed74a8a8ce20942631ecc88',
                '02090f1a28b34fdb86493fbaf30766a6',
                '019e7b07b675469492b5d9edcdefac47',
                'f317fffb0d564d109fd66fd2f687c547',
                'd032e3d3562547e4b2cdaf3fe51a6cf8',
                '32a0695b7c1a4e068142a780d09cf762',
                'fa4fd9de4ff74bc3a3d298967091a892',
                '82137f6bbef44d71856a9692d29de333',
                'cb51fc60bfc344cd89c7bb39e1e6d47d',
                '8d13e1abffd14e2d985d1ba44e438144',
                '9b617e52d697424d9ac73f0d8990b1de',
                'fd325994ef564620b4ba76f3356c46e2',
                'ff3576a189d54319a21f3b0f81ccfd70',
                '37ea85301062407f98c731744b0349ac',
                'f1394fe341ac4954866bea9ca47c8ebc',
                'cf07ded7f68c4710a6de0b61b16a6821',
                '2e20739a85b34c17b1166d80ae870c5e',
                '78b3117307934062b35456f36c5a03f7',
                '9590c6fe035a41699be06efbae11ed5f',
                'de06da6c3b314e52b4d1fb5b7e920b59',
                'f7acf2359e95408581b62ea95511465a',
                '72073cf49bc44bf59313e6bac2144baa',
                '10a7e6afcfe04a44bfaa7aa137729531',
                '60911d2cb1614c14be9d61a11ee048ba',
                'dbcc6307654b4a9eb15334a6bf6046c8',
                'b3885abf2b724113bb3b6cd1b48002ad',
                'f93fd14612d4439c9211ef9482ffe838',
                '0da1d04ca2b445888be12d96c5fac6dd',
                '857d624f7f234b65b8a7cce8e59d9caa',
                'b36cbefa1e9f48f1ba805c42375fbbeb',
                'f65ad571e662467ca4e973bc5fbb3dcf',
                '80ebf61bc96c4410a496e42ceac4fe8f',
                '58b44774f11640d2863453169f2fb187',
                'ac8e1e64585d4bc69dcf86780477b741',
                'c8325db51be249a4b4008b12b0062b21',
                'fff645fe61114c9e94b5b52d7a319514',
                'dadfa8b66a824254a46a8b293a081d76',
                '321cf375bb4b428c9031afb4dac31dc4',
                '48481ccac2d54ddfb5c50e5a9872f5f2',
                '3ed5901fe2484ec98e44693d800673be',
                '9c357444ac84404db881a42fa61a6e58',
                '2f92b8fcaa2940f182abed9b7e690780',
                '9c7e2487449c4248a68aa15759d24be0',
                '7f0fffc5f86b4fcca3c7e9ea9f15f8fe',
                '0784e0203f1044979c91aaa1c51f0a0c',
                'af1647a2675d454aaacaea93a74f2972',
                '603de0526c634ce29e836f83021e132d',
                '54270dd056e140a7a3d560e8c4b87aa7',
                '2b9580e672604b6580b40e1c57ffd6c7',
                'bc402e6917254f48856f76d712fb0227',
                '33c0fb045e3445d2a3ed7c03207d6c4e',
                '7662828d36d54a5a99c15e3a8ade15ea',
                'd27ddc5241a24eb6a312b84e84ca63da',
                '5ff510be834646829793049db508a13d',
                'b5e1b804566b45ed90018ce5c86aa0aa',
                '4b15b5096b40474c9a050002150ef0bd',
                'd282c47fdb93497093c330cb10b9e705',
                '61921ec7b8d14d0cbd22a177dcb0e069',
                '1567c952832544aba46290a03c42a0b3',
                '90c2802c8e094ede8da4e33f49acf9f8',
                '4488adf690c240579c03e7e118c860d2',
                'cfabe8669a684df29fcb92c2839b3060',
                'bde028415af64ca0b530bc896113ec60',
                '67e289f229b84d848fa7ee308e53e9d6',
                '7d018eb203cb4e05872be19c477caf96',
                '86b4fff8df3d4ea9b46d3237ab7bb9b8',
                'e3e1f8cf8ba94f3ca3ac08d0952e0c3a',
                'f9fbb1b3b5d94f6dac06b9f50be88e66',
                'a830d32bf17f4ec8904699c5f3390a23',
                '5346cef7a1254ae08d4d8d32582d339c',
                'd24060e8e8254e8387ec6fe8a42b4063',
                '274371da958f4ebfaf060c554bd5919e',
                '5904fd913f484646ac44278321eeb5f1',
                'a135e3b211c54ede9fa4c2dc9f2125d6',
                'a4ff05119b1240d08956e47a00ea7df8',
                'a7c8249da4754efb9901de947a7cd5cf',
                '5b0c8790ebad49ce89aa85e7a71e6136',
                'a310f66cd1be42fd9e03d5f97e45f023',
                '71d43970e1874421819836197402ba70',
                'd96831c5ea58423ead4a46915d899ec1',
                'a4e7e43a9fd6427c99581220e781846b',
                'e33be4776efc479e959b35512cf4d5f4',
                'a6dc10b699134edea49f2931de49a474',
                'e723c78fad05431ba9fbca5c768c0a7d',
                '60a4536f3d294e188c531e03e89c4c21',
                'f931f662dc4541d6be373e9dd335d2b7',
                '15f0c77b8ab142ada52f30b83cd7e674',
                '3448a36c635f40d5ac262aaa5b6f8847',
                'c5441c061a4e42e095c3d9512f900c27',
                '05b68b2f2db144d4a6a9b5f3dec4c489',
                '133792f2cf4749458448af637968b308',
                'baec20159ab64aae8b69beaf2e51085c',
                '2b0b07ebe5654e7caae23c67637bca0d',
                '0c67031f7cb24f728d503fd4704185cd',
                'f02287abaf7f4f468871839581791837',
                'f2fd148159b64a8697e232e568d90660',
                'b9ff3b554e7f43b2bb0074f08109d291',
                '83f9574cb9b04bd9b2df55df023dc947',
                '12b7221d129d4264a3f89469762cd5e5',
                '5bc736ab06124069a0c8f568e31a9067',
                '5d48c4be6fd84efbae7ca30b16646f49',
                'f64d1e365f864990be6ffb3ef3ffff1d',
                'a8b3a27cf16f4499b54b88e41dd61e3b',
                '467cbcef7eb240239c4be2c115143310',
                'c8e3b694d3cb4066bfce281cbce0dfe5',
                '310d035233994e55818c27dbc7fac574',
                '18684e358a26460d9766470dffe6044a',
                '654d9664d5bd441eaaab352c94e6eede',
                'c1665ae49d674062977682689ae8a102',
                '06724306cc7b45848fa5caa5ad87463d',
                '1fc079447d824268a7baaeed0e9784d1',
                '6cc716fafb804fcb8e7663f4c7bd7b4a',
                'd6d1fdb5d3cf4f50b1abce95b8f3f1a6',
                '3b47b7a322ef4c69a33a33214748f927',
                'd158d3f2364744d9984f9caa886c29b0',
                '7a56394dd8554bc18538cfd5c3ff462a',
                'fe12d71e09a14befb966ce7b1f6c56ec',
                'c572c610bd0f42ce8b8b618e0fa909be',
                '1db8b7397c2a4af9a739289ec09016ff',
                'd6cbe6b0bfd845989bfb1a235c326b6e',
                'c714f4cee89d423783ebc1f07088f767',
                '50a0414e602b43a2a3b92de8fc74c87c',
                'f67b94d683994068869cadbb29747e35',
                '7c952afd68374326b13e4295810242f5',
                '3e2f2da1fa1747c2965c095fffa6ca09',
                '179c9dd4925543989f4a857b08fbf075',
                '7c0a05a7d2374c15834395b63a7f8fcf',
                'beb6dd1d9cf54b5680164e89e73d33c4',
                'a5be002332f841d7a5da7dfd146e199d',
                '0583734d7d1b469c9d88b84738d1901a',
                '0726ac36ef864295b8bf006ece920ae5',
                '101c6f041b5a41689c4856aee826782d',
                'de91ad6a138a4755934e225c3c0db152',
                '5711c6e471734ca78d4a2945b2df95b6',
                'bb8cf0976f044af3bb315b77490489b5',
                'fbf6075a896246438bb2fc2f8b5082ab',
                'a47bb859c58a44b4bb70789b54f118de',
                'da094e9280ce4f12b12ec5d146956d6e',
                '760457e748a0434e8e18fae1056a1779',
                '53620989af7d4b2e8ab4392f5fdfaf97',
                '10d455625b61451bb396b8318a55e196',
                '1185f69557f546fabe5ab907c106ceca',
                '373b789d841348c8af76769c9797bcd3',
                '602590a060b8420e83af5f45d856e4f3',
                '0eccaae9edcd46dd97e39fbe384a8f5d',
                'ce09d93f99094e5580d297d228918a17',
                '82da52edb2be4d51b573990bc09b2e13',
                '79fdd6d7f1274157b0782c2515dc819b',
                '3a8d659ef46d48699310f3c6f3df757f',
                'd86a52a5722b4bc5a9f9c135700dbdd3',
                '2b72a97ad83d4edb97c545a82b1820b6',
                'e2b2bbaa08d74050b12ea4235a3b3939',
                'a353337bb0fa49559f21e3eed00f3352',
                '6cd17637ed8148e4b5ee19ea89cedb89',
                'a87f5e7cba1e4c6d95293edbb6f81a5f',
                'd94f83e9ecf94ed29b3556b81193c68a',
                '6d78f0c2769349e68e1def359ae3a79b',
                '4c50046ceec74ba18d3514953febf008',
                '613ac9d7cc95455eb09ca90171c70f51',
                '1e2375e01b514215b9d5abea11dfd058',
                '725ef63435b04d8d834dfaa053269bdd',
                'bcb06ce69b014d9b8de19cd35d9488e4',
                'c30f1a936dd841628d8169f172961999',
                'b726fba005744185847f691a38f945c6',
                '137512c3753545ceab8284bb7c9e5d68',
                'b17c99520c394242941da5148de67065',
                '919b9f7657c84a7aa83d585fa8bb6b42',
                '540c0f3595e941659b27504b81c9cca9',
                'b1e9a1902a794f81a93ca07f41227627',
                'f81cc928b1664a44b961c2c167d9da4c',
                '5a1a2f39e4ba4d7094d6c566aae44f4a',
                '5fd686935672492eb35ece03f5152114',
                'b09c9ee44e864875b751e25a043e117c',
                '53fc5974d2b5470dbb78c4e4b5d22420',
                '79bd38dc0ef64025880c6bcae7242907',
                'a3b7d878160b4422bf5f76b0b77bdd59',
                'a01b04fe4f1e4ff6999cdb22b2b95363',
                '07c9e775461e49dbbe673f265bf6b7fa',
                'eccb50bfe30f48f9920bc53942b07a88',
                '661b9e060f2a473db91ddb5baefc47a0',
                '148fa148f92144ef9b8b15965d9583d3',
                '1135fc2f160646babc4e0ca737fb70c6',
                'c0ae8915117943f0bcb8108904a7e791',
                '3f717148714a4a2fbeabdbbedd190534',
                '185b17e3400b4b918f4d33dd314d41a1',
                'c8f4188b4d2b4b9c9493e5053158386e',
                '2d9c9a5f142549e9822e6a52b3a825d3',
                'fa07ec2c54d5425f838728bb5fc3ae46',
                '13c3e986c6014f8993398a6aca37cd7f',
                '35edfdf2d2844a1c97e0ffe25c555e04',
                '66eeedb0c57a4e0e8507be480e891a44',
                'eedd0f79533545ccaf6d05c4dd0669d1',
                '01ddae52fcd946cf98cff99954c6e10e',
                '9b3528612a344c0eb4b1253b77e4ff8b',
                '8e6ab4efa7ed448eac074b8cf027c960',
                '453f93661a2943c58e6076aa225b9578',
                'b275786b926c488d8b78c7c306bff6eb',
                'b734b631fa34460c99d4b4c037640414',
                '70e464d89e69412d91395c7da1b773ae',
                '44b50b65db0543e78edaeb2dd6f125fe',
                'b4d44a9d025046959a74a03fe542bc63',
                'aa269c105a4643fb8b080ce78c668fc3',
                'eff1350886894edfa844f1bb3ddb8083',
                'da7b5d1a901b46a6b1d6b44a1fa1f4a9',
                '64ca500ce3ae4ea49b58f2bbeb89ea1d',
                '9c8f4cf115864c049ab9e9ca91009f1e',
                'b7437c97d8da4224854630ccb2070a18',
                '7b077f3028974909a246ab52e1a11701',
                '0e92e3e7e0c94253be78b439c377a8c3',
                'a1016adcc10443b8b1537e2b6ab806eb',
                '5bce610423374ee18fd513306f504028',
                '823ecda8216f45949cc8f325b23ae65c',
                '584567079b984162b6c69ca7691c665b',
                'ae5433a4261147e3abf8a5765270ca59',
                '1a82dc87b8e7441ebb449341879bd948',
                '9069be2dff6f4ae1b17eb7aecf4e7d9c',
                '034d3a8f33ca4f5cab148b95737353e4',
                '4cfdda9db62e4826af602c6d62fc6a0b',
                '75a527014df34013adbf3020e7217a78',
                'ecbbe185e19c4a3ba13d5b11a70026d2',
                '163f39c078264c6b9359b4b07213c0ec',
                '36d885120e4647ac86fa1a9045c326be',
                'e622a8ea7c344b318421b94ae342eedb',
                '2170f5a29fa94c1d8543dcb3115524e1',
                '9cb230cd668d4f208dd331768f720a40',
                '4410ffa4f3c140408091ee5051564af4',
                '8389eef6514445a2907252a8afc54f3c',
                'a3e5001141f5499e8867042526d808be',
                'f72aac395c9e4befa272842e1ea2d2d3',
                '4052f90368d64154ac966f360fe64eb5',
                '4c215f3a901f4256a11b54af025cdbd1',
                '57544356e015470d9ebbf0069457a11b',
                '2d43bad6ff4045d2ab88214a354e9898',
                '151bdd83770f43a3962d2c2985dd0fe7',
                '55c88adf4b0d4eb89964aac636f1b6a8',
                '2de743aa5c814e53ad549e2137eb4336',
                '6d812b94b098454284048b3b0ebb0667',
                '60b2ff6aca5c41ec97ca9636fecb3441',
                '2f6c4cab50c2457f9bb47ae0f4629da7',
                '02b7edf708da4ba4a0291c23587c26cb',
                '9ab5af204b35451bba425b77bdd7426f',
                'e5ad331f7f2a43cbbb6371a6914a157b',
                'fbfed8347a1246e7916a1479db1278c2',
                '7a3feba20b2544d08205f81c55889975',
                '7e37e3083eb04a019bc5fe8a041f8ad8',
                '10df6b141575497b86695e697c719466',
                '0a5d4857c6114fceb3a914651f1900ac',
                'b14785fdbb614e0eb43f247660cb04e0',
                '6e40d0fbe45c4ccd9b621526af8b09d7',
                '7233fd85a0554454af7a568a46edc79e',
                '570844b752ef4e33954889d3ab987b02',
                '51e2859d5b664c37adc804be32d625cd',
                '6351a21becf3462fa6fa53207b344b98',
                '1b839b72705a4810a14220f3b1c2b516',
                '24939107d022483c91b81ec25df4ca63',
                'd6bed07592c44e2da7b47e9d1fc5798e',
                '8cae36c0a2584531a5b4f35482c18b31',
                '5f2f895564554b1e9e105b6083f0cd65',
                '0c8c07e4d29c4cf6bfd1f9a79cb1316b',
                '989ec072482f45da8543c7ba4a31eaa0',
                '1a1369a375824b05a4cf677be1af0684',
                '1a5155f189de40c8bd4a4e7554f4ae24',
                'fb3fa470051c49c39930aad7df600f2c',
                'fe6f94ee55c6466f9ed28f63a0661b90',
                '9adf6c8e497c445b9f0e2819a97e61f5',
                'd066f642b0484afe97f662729e56567c',
                'a62714821e0044b8b520748a72fc1ce8',
                '591d83274db7408a97fd3227a9e041ed',
                'b6072802b42845a1bcc60215d7c42e30',
                '6387a2affff84f5e8065c57d59fd4439',
                'c14009ca4ea449d7bff2fc96767fd157',
                'da3a1b8e8f4048f285b539b93f5a546c',
                '3f0f01e4cc2f4eb6b4dd0a8036ef99a8',
                '4b3767a580a44cc591776726f330daf9',
                'ae5964bc53764b84a8c5c5bdd57dc7d2',
                'dac0bd7a387044cdaf9b2cda65ef9874',
                '0ee2228ae50a45e49439496ee7fc7b47',
                '2c9f35ed77474dea9d5e67af7cb28e9b',
                '08876a9fd1884ae793a8079beec4af97',
                '6d0d7531e9ed4b78bcbfc00b58b7bd4a',
                '17f22683942d4ecea2b7bcdfec9edfe8',
                '96e727b0bc3f4a52bcd5f4212cd0ab36',
                'b9ce2dccfdfa409fa1241f5dd7f108b0',
                '5c4335b78c8e4a219db7d0cfca9b1ea3',
                'd40a8fda14714ac390ff36530d2b51fc',
                '5b190b1ed07c4031aab37f352454e2ae',
                '676946f7a1d940f19efd8bc4cb0a72fa',
                '722d194f520b45e69a3539e0036cccba',
                'a1e271bd098d4e1397230cbac7372889',
                '73df1d89126c4b5eb67954b0ba1bc7b5',
                'ee6f8da47f6b4b84b82e0eff0689eb04',
                '8ff0b230da4d4a539fbdecb252297fc0',
                '4f7b3a2728744d858485eca3254abca0',
                '35bbdcf8e24a4bf0b9bc881588378392',
                'b4a7e6a988304af3bb0e5eb6b49603ad',
                'c533b2328d464e489a6af0964d408d27',
                'c394de9b067a4391a2de593daf798c1f',
                '9d05b1bc39b2460a8c89483c6133f8c5',
                '8ef5851ac85d49e8b1d5fc3ec732f701',
                '0dc2b13d528644a2b324e8ea46c8dfe0',
                '7edad31cbfe347c88bf491f4b050e1c0',
                'd947c8088e0f47a2b5a6219bd5138b02',
                '8e49abbb38c74de2965ef8e01b48daa0',
                '9267ffb9c58f4073bf6db8753852dda7',
                'b7139043a41b4f639171e417719ae73d',
                'f98e474a180f41aebe85d9e061ffd88e',
                '6c672a4350e6450fae68d9243eabe5b7',
                'e4ade28a87154aac90aebea5c6424263',
                '53f0ce3047ec4059a97564cade02d731',
                '243d308437144dc0b2302ab7dfc6d854',
                '7194e8c287e24c52b7974677fe547739',
                'dd8c1f3dada74ee6a01561809b525dd9',
                '3e4964c9bebc43dcac1b6ef180f7e230',
                'c5a9cf94adb44becbca04c7e4a1935b6',
                '3e08642a3811411ebc1decfe334d6f4b',
                '062450e077134730ac7c1c02dda56e93',
                'e5005739bb904240ad6f41a732353f63',
                '63f2b7057dd1460ea6859790272e3951',
                '1914698a818f4ae79ebc5014ae666c30',
                '486c42d4d51b4cad95df3fb956212db1',
                '8a77a19a06e0462785d4c8bf1cd0a9c9',
                '9ea1bd0b497440e9b52d25a3b4837337',
                '22e06c0bd9b04f0bab3bf2011095a3bd',
                '0193c6a1b4754dd0bf06cb0cd638c1bd',
                'c60c035034414548b5219a3c616a8dc8',
                '2147f89551ab4638b8569def05e4f9a0',
                'd82212529c1547aab841a9a211f8002d',
                '52936198980e4f3bb7eb933cc6f21820',
                '2eb1fe64b6c0441586d3341360bc5eb6',
                '716b6b9bde1d4de8be385804aa7f7a46',
                'c1deac749f444e9791ab8fc3a678a9ed',
                '04f3062db051431d9795e12496c10a6d',
                'b7d46dcf817944d48831e08197118388',
                'e9a3145a12684ff5b5edf6e5c2603d99',
                '6ea1884206754a999662c79f8b7e1160',
                'b408733553f9477a84bf77f1f4cf50f8',
                '740cf0843186404696d36b4cecb262bb',
                '9b4b9c3fe60849d295bfef52ec6c8b3f',
                '985121ce482246ee9bca0ad91023a089',
                '333c81e1f2dc47afbfca0050657ba6e2',
                '3228cd594d6d424ab972b1462e28e348',
                '9c9ce5018b91430d85002dda2759cdd2',
                '61b8f4cd15a341e285c668c87dfb5c7f',
                'c805b94fbfe14d2a98d5fb306bfa61fc',
                'd7988dc768d74541ad4467d8396e1c66',
                '488f9f25cbe440eca120aee042fe9e47',
                '4a2bef5896e84bc99331a9cc3801c2cc',
                '325daf2ee82b4ded801927298eb95387',
                '9eaa0419b74c4c96b75467ea5b0695aa',
                'c811e93d323e43b3a6b58c7d79dbe7b4',
                'ca7fdbc725c147a0a5c0f9e81ff71abd',
                '68406a00df284e5a8d28d74e1acbc99c',
                '639273a639414a04ac3edacd6aa7f95b',
                'c1438cb07851485f9bd324a411e77600',
                '81f22c822a884edcb5da0c50e2e3bf45',
                '1f9c9ff1127c4584b72ca0b51004863f',
                '5911fadb048e4bbf9f90d3d1e9ff2a2c',
                '06f8f19756c74220825327e4cd29e056',
                '2070e26d6fca4d4d8b80497f2229b410',
                'f52c19351fd147fdb605ca6a40c11587',
                'a87e7bef4c574c369e8563b6a28db799',
                'a7e3e3f0f1f94c4f83f839f680ecb67d',
                'ea4cf5bcdada4195982670f58ff31f5e',
                'c1dbd7838e5c4f1586d46438e39d6b94',
                '4a4486f545d14b3dad81ea243ff428b4',
                'fd462a1e12414438a1701c5e529fbdf5',
                'ea338b14a4a14c0083fddfddc20df9b8',
                'f09c6e823a424c93854be268f3fdfc4c',
                'f197572d204341a1b329b767766c78e0',
                'f23b57f360d4419fbe15f040d3d20abd',
                '24f574dc93cf4ba9a676f89e0f3a9e53',
                '7509296aaa2f46bfb066f4d5753916df',
                '8b70e8c23bbc4e6aad738b7c98bc19db',
                'aefe4bbb205e451b82485cab717d85c4',
                '07186d1f8695452283136f414c44af55',
                'cccb5117f445464393beec6386a5e6ad',
                'b892b8155811448bacde57d506d3e4d5',
                'bbdc7e2bdad84cf3b13cdb6d20c81908',
                '7385f9d3ea3f4eba887208773316ef1b',
                'ad03a694ac4049ecb941673d0eaf7475',
                'f7643b9053ad43a09d3fa682c711d91e',
                '0fd0490df055415b9633b3b52a725415',
                'b7e7c88e363942ab8dda316df5336a7f',
                '65884330f7ce40239497f4287ecf62fd',
                'a079cb2bec6f4f37981265af3ba30822',
                '953231e6b5b948b78ba561bf1c9e3b53',
                '9064c87e82d14972895ec955dfeeac4c',
                'a220448f27b74e59a00d3b547346cdfa',
                '19043738748047a59eab7ce72c6345ef',
                'ef86bff528f04adb9b3e2a11530e749e',
                '03e345819b7140899988ec60992f532f',
                '942d4b2565234fe6993ce2778c029f07',
                '83304d0681484d99aca0e830bf9c365b',
                'c1fce3511e2e4de389a32b7b8194f54b',
                'b7edeb8d6b58477ebed1d636458c1a82',
                'f0b19d6891294f96bcfe11ed3ae265fb',
                'c7ce2e02385247d3a81db9b002e29766',
                '66ae75222dae49efbb139c2b067b8a1b',
                'f5d570765c084629a1dc70e188b87836',
                '663d7f409d43480f9703e585d0ed74b0',
                'd160687d6dfe4459bf84004168ffff13',
                'cc31cbf31b054f31b1970a0498320dec',
                'a043edc78a924e64998d678ffc842fe0',
                'b85de68f924b412ca6b9cb74eaaacc42',
                '12d0ae6e0e1b43ee994ecb0e9e8d11fe',
                '01c7f494843d469c9243e1767060869d',
                '537ca6abc1ad4666a5f1c1e709feb445',
                'd8a67f27aee742bc95722543d6cd8e0b',
                '41c0e666ba014487af8d3a357d1d3131',
                'dca22644df284105bf14f69247bc250d',
                'aaca5eb095c74f8d9017537b2229b068',
                '1fe902886b2a46b0b48155d916a057ce',
                '855f3680608f4b6fb880beca29c71e51',
                '75032f74f603424c901f9062cb41e3b4',
                '4b814276d2de4a7bbd6dd7d83fba75af',
                'd0f49908cf7245339a07913968cb0576',
                '1f665ef2a6d646deb6537cbf3d94bcdd',
                '66c219c3f745407d936b738aa6b1bbe8',
                'c5f0da073fbc4fac8377270d5628b731',
                '80686f67837144099709e58d3a63897c',
                'd1cba8b5562a4cceac0a9ca65cd3ff72',
                '53f3367d3cdd44d0902fbc9b9ae39f14',
                '23649f1e55794f7c8e94051dd0193375',
                '2b0808c146c84effbd86b392ac53a94a',
                '9b33598ae1b94eb0b9177c9bdca54453',
                '53d888c1c22c4c1b9f18777430162738',
                '58fc26b1bd1d4a69be80f6f48ccce4b7',
                'b04c116c29764036b396c8686fef047c',
                '25a822de81b14dfc8bba93727490b595',
                '688678a7aea14175839e1e79b189aa2d',
                '383d87c78b444aab8594e993a56e7b5f',
                '94b99517bc334d7db93e84d04ef0a388',
                '29d7a7c7ddb54987b16f7e070136061f',
                '27d9b5b7ac034d18be6406b5eac3ca1e',
                '99a8526e8011458dab0e39b19218f287',
                '50578b136f23497085fab4a454112f11',
                '8c4a0b4d5fdb40f587a454fc5db17db5',
                'c81b5cddf1504a7ab44a9b4634d8e152',
                '6e3dc5ff51024f06932bb2b905a12001',
                'e9e8bbe031014078847d23f4e48a4724',
                '08416c53f2bd47178ac005590c009bbf',
                'a865cbf1ce7d4442b45f474d2f58cca8',
                'af109d33c0a142f380539505f52587a3',
                'fc120644f5194e1490cbc6823fd59562',
                'eb632fdc29f64913a3a40ba72fddc3c8',
                'aa92bcbf62e04685a7c8094d8fab425e',
                '4390b9ef84b0455ab51a60ad1d6c8f54',
                '6abc7bc48b9049268970dc20edf9b519',
                '6826683670fe464fb21d80cbbc30a5ad',
                '5aa382f3c5044622af84c827d021200c',
                '7d848bd6cb2d491789a4b8ca17d77905',
                '32ba6d336ae04bb4a97763b107427e20',
                'bf34942ef961425cb999efda292cf4d4',
                'e070562af8a34595bceea4eb96361d84',
                '16fdd402cd9f41a18866e788560aefe4',
                '636724817e8441ecb79db36b278175cc',
                'ea9a3479815047d8b6642a13ac7f7fc7',
                '1a7a45ea8d6146b3b9d1144172c0c3ed',
                '64db85ed307a478b86f112675942de41',
                '2130431ad10e4bbcb12408ae490ab4e1',
                '7f772ed72d2347feb0e0e2c78aa9ff52',
                '09439743de42418e955aca4b1af65284',
                'f0076e0da9bb45a481173e48c4cb5f9b',
                'a4f486b6fcec4b548113da9a5accd52d',
                '4da1d2ff1ba34fa4a620ff1814477835',
                '699c640666964539946ca84e541230d8',
                '247bb76c047e4fbfa51ebfdcabe4a812',
                '71c813a9ea404af98bc4a275f605189c',
                'd61199ae28834b659c5d44f6454d569d',
                '51269b3a01174d9ba2a288d724277af2',
                'af127e1e9c0f4fc0b60f2a70b96458a7',
                '2b0846db9c1940329f45592d2f053b13',
                '4401fa438285432dbb7adf118b7a8db5',
                'ce221941945248bcb8f99e1a89af7de1',
                '54f8065de9a740f280eedb0174a6c865',
                '8a2b6c06e0ac42ae82ba63b05710d17e',
                '2c096a4017a9433aa9d03320ea857cbd',
                '96abc7189b504e49acb4d8c1bcf2bbf6',
                'ed2d1acae9bb4896b812798380ab07de',
                'f6948d0b56d140749ccca1d70adc75a7',
                'dbe3523f2dbf4069b40af9c9aaf48f81',
                'cc8a8e1d42ec40d99c39cd79ffe82292',
                'b23b7d6fdfbe40678b0ee4ff239d3472',
                'cffed387c2ac4a9b9dc6fcebcac4bb88',
                'c4e2295a7b4c4450a83567577af4460f',
                'c1a566435d63402d8899a02d585bf14e',
                'cb0c858a83d84622bd3a0a21037639a4',
                'de557cb508544f23b1b140135cd8475a',
                '6a76c9b5e1c24e61aa3c295d15d25df9',
                '0efcdee9b092489eacf9d92c09af3e6b',
                '7a724fa9860c4ed0a3e68d955c7ead12',
                'bc8fc1ae2fc14c348e081513306eb69c',
                'a8e26ff286c64727a379cb1b5b5e9029'
            ]
        }
    };

    const cursor = BetTransactionModel.find(condition)
        .batchSize(5000)
        .lean()
        .cursor();

    cursor.on('data', function (betObj) {

        const memberWinLose = betObj.commission.member.totalWinLoseCom * -1;

        let body = {
            'commission.superAdmin.winLoseCom': 0,
            'commission.superAdmin.winLose': 0,
            'commission.superAdmin.totalWinLoseCom': 0,

            'commission.company.winLoseCom': 0,
            'commission.company.winLose': 0,
            'commission.company.totalWinLoseCom': 0,

            'commission.shareHolder.winLoseCom': 0,
            'commission.shareHolder.winLose': 0,
            'commission.shareHolder.totalWinLoseCom': 0,

            'commission.senior.winLoseCom': 0,
            'commission.senior.winLose': 0,
            'commission.senior.totalWinLoseCom': 0,

            'commissionf.masterAgent.winLoseCom': 0,
            'commission.masterAgent.winLose': 0,
            'commission.masterAgent.totalWinLoseCom': 0,

            'commission.agent.winLoseCom': 0,
            'commission.agent.winLose': 0,
            'commission.agent.totalWinLoseCom': 0,

            'commission.member.winLoseCom': 0,
            'commission.member.winLose': 0,
            'commission.member.totalWinLoseCom': 0,
            'status': 'CANCELLED'
        };


        BetTransactionModel.update({_id: betObj._id}, body, function (err, updateResponse) {

            MemberService.updateBalance(betObj.memberId, memberWinLose, betObj.betId, 'CANCELLED', '', (err, updateMemberResponse) => {
                if (err) {

                } else {
                    console.log(updateMemberResponse)
                }
            });
        });


        // });
    });


    cursor.on('end', function () {

        return res.send(
            {
                code: 0,
                message: "success",
                result: 'success'
            }
        );
    });

});


router.get('/repair-sexy', function (req, res) {


    const DRAW = "DRAW";
    const WIN = "WIN";
    const HALF_WIN = "HALF_WIN";
    const LOSE = "LOSE";
    const HALF_LOSE = "HALF_LOSE";

    // let condition = {memberId: mongoose.Types.ObjectId(req.body.memberId), status: 'DONE',betResult:'WIN','gameType': 'PARLAY'};
    //
    // let condition = {betId:"BET15776195914776329843"};

    let dateFrom = req.query.dateFrom;
    let dateTo = req.query.dateTo;

    let condition = {
        // "createdDate": {
        //     "$gte": new Date(moment(dateFrom, "DD/MM/YYYY").format('YYYY-MM-DDT00:00:00.000')),
        //     "$lt": new Date(moment(dateTo, "DD/MM/YYYY").format('YYYY-MM-DDT23:59:59.000'))
        // },
        game: 'CASINO',
        source: 'SEXY_BACCARAT',
        status: 'DONE',
        betId:'BET15887506978755886854'
    };

    console.log()

    const cursor = BetTransactionModel.find(condition)
        .batchSize(5000)
        .populate({path: 'memberParentGroup', select: 'type _id'})
        .lean()
        .cursor();

    cursor.on('data', function (betObj) {


        const memberWinLose = betObj.commission.member.winLose;

        let body = betObj;

        let winLose = memberWinLose;

        let betResult = winLose > 0 ? WIN : winLose < 0 ? LOSE : DRAW;

        let shareReceive = AgentService.prepareShareReceive(winLose, betResult, betObj);


        body.commission.superAdmin.winLoseCom = shareReceive.superAdmin.winLoseCom;
        body.commission.superAdmin.winLose = shareReceive.superAdmin.winLose;
        body.commission.superAdmin.totalWinLoseCom = shareReceive.superAdmin.totalWinLoseCom;

        body.commission.company.winLoseCom = shareReceive.company.winLoseCom;
        body.commission.company.winLose = shareReceive.company.winLose;
        body.commission.company.totalWinLoseCom = shareReceive.company.totalWinLoseCom;

        body.commission.shareHolder.winLoseCom = shareReceive.shareHolder.winLoseCom;
        body.commission.shareHolder.winLose = shareReceive.shareHolder.winLose;
        body.commission.shareHolder.totalWinLoseCom = shareReceive.shareHolder.totalWinLoseCom;

        body.commission.senior.winLoseCom = shareReceive.senior.winLoseCom;
        body.commission.senior.winLose = shareReceive.senior.winLose;
        body.commission.senior.totalWinLoseCom = shareReceive.senior.totalWinLoseCom;

        body.commission.masterAgent.winLoseCom = shareReceive.masterAgent.winLoseCom;
        body.commission.masterAgent.winLose = shareReceive.masterAgent.winLose;
        body.commission.masterAgent.totalWinLoseCom = shareReceive.masterAgent.totalWinLoseCom;

        body.commission.agent.winLoseCom = shareReceive.agent.winLoseCom;
        body.commission.agent.winLose = shareReceive.agent.winLose;
        body.commission.agent.totalWinLoseCom = shareReceive.agent.totalWinLoseCom;

        body.commission.member.winLoseCom = shareReceive.member.winLoseCom;
        body.commission.member.winLose = shareReceive.member.winLose;
        body.commission.member.totalWinLoseCom = shareReceive.member.totalWinLoseCom;


        let updateBody = {
            $set: {
                'commission': body.commission
            }
        };

        BetTransactionModel.update({
            _id: betObj._id
        }, updateBody, function (err, response) {
            if (err) {
                // betListCallback(err, null);
            } else {
                // betListCallback(null, response);
                console.log(response)
            }
        });


        // });
    });


    cursor.on('end', function () {

        return res.send(
            {
                code: 0,
                message: "success",
                result: 'success'
            }
        );
    });

});

module.exports = router;