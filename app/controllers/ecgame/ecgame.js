const express = require('express');
const router = express.Router();
const request = require('request');
const moment = require('moment');
const crypto = require('crypto');
const querystring = require('querystring');
const async = require("async");
const config = require('config');
const MemberService = require('../../common/memberService');
const Joi = require('joi');
const DateUtils = require('../../common/dateUtils');
const roundTo = require('round-to');
const mongoose = require('mongoose');
const _ = require('underscore');
const AgentGroupModel = require('../../models/agentGroup.model.js');
const NumberUtils = require('../../common/numberUtils1');
const AgentService = require('../../common/agentService');
const EcGameService = require('../../common/ecService');
const MemberModel = require("../../models/member.model");
const TokenModel = require('../../models/token.model');

const BetTransactionModel = require('../../models/betTransaction.model.js');


const DRAW = "DRAW";
const WIN = "WIN";
const HALF_WIN = "HALF_WIN";
const LOSE = "LOSE";
const HALF_LOSE = "HALF_LOSE";


function generateBetId() {
    return 'BET' + new Date().getTime();
}


//PPP AMB


router.get('/forward-to-game', function (req, res) {

    const userInfo       = req.userInfo;
    // let requestBody = req.body;
console.log('------------------')
    async.waterfall([callback => {
        try {
            AgentService.getUpLineStatus(userInfo.group._id, function (err, status) {
                if (err) {
                    callback(err, null);
                } else {
                    if (status.suspend) {
                        callback(5011, null);
                    } else {
                        callback(null, status);
                    }
                }
            });
        } catch (err) {
            callback(9929, null);
        }

    }, (uplineStatus, callback) => {

        try {
            MemberService.findById(userInfo._id, 'creditLimit balance limitSetting group suspend username_lower username', function (err, memberResponse) {
                if (err) {
                    callback(err, null);
                } else {

                    if (memberResponse.suspend) {
                        callback(5011, null);
                    } else if (memberResponse.limitSetting.game.slotXO.isEnable) {
                        AgentService.getLive22Status(memberResponse.group._id, (err, response) => {
                            if (!response.isEnable) {
                                callback(1088, null);
                            } else {
                                callback(null, uplineStatus, memberResponse);
                            }
                        });
                    } else {
                        callback(1088, null);
                    }
                }
            });
        } catch (err) {
            callback(999, null);
        }

    }], function (err, uplineStatus, memberInfo) {

        if (err) {
            if (err == 1088) {
                return res.send({
                    code: 1088,
                    message: 'Service Locked, Please contact your upline.'
                });
            }
            if (err == 5011) {
                return res.send({
                    message: "Account or Upline was suspend.",
                    result: null,
                    code: 5011
                });
            }
            return res.send({code: 999, message: err});
        }
        console.log(memberInfo.username);
        TokenModel.findOne({username: memberInfo.username}, function (err, response) {
            console.log('find by token ',response);
            let balance = roundTo((memberInfo.creditLimit + memberInfo.balance), 2);
            if(response) {
                EcGameService.forwardToGame(memberInfo.username_lower, balance, (err, result) =>{
                    console.log('---------forwardToGame----------',result);
                    return res.send({
                        code : 0,
                        message : 'success',
                        result : result
                    })
                })
            } else {
                return res.send({
                    code : 1,
                    message : 'Token not found.',
                    result : ''
                })
            }

        });
    });

});

router.post('/updateUserBalance', (req, res) => {
    console.log('registerOrLogin =========================>',req.body)

    res.send(
        {
            code: 200,
            status : true,
            message: "User balance update successfully."
        }
    )


});



module.exports = router;
