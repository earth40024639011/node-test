const redis       = require("ioredis");
const _           = require('underscore');
const CLIENT_NAME = process.env.CLIENT_NAME || 'SPORTBOOK88';
let redisIp       = process.env.REDIS_IP || "redis.api-hub.com";

const IP_REDIS_GROUP_1 = ['BET123', 'FOXZ'];
if (_.contains(IP_REDIS_GROUP_1, CLIENT_NAME)) {
    redisIp = '10.130.250.246';
}
const IP_REDIS_GROUP_2 = ['MGM', 'CSR'];
if (_.contains(IP_REDIS_GROUP_2, CLIENT_NAME)) {
    redisIp = '10.130.236.176';
}
const IP_REDIS_GROUP_3 = ['IFM789', 'IRON', 'GOAL168', 'DD88BET'];
if (_.contains(IP_REDIS_GROUP_3, CLIENT_NAME)) {
    redisIp = '10.130.236.160';
}

if (_.isEqual('AMB_SPORTBOOK', CLIENT_NAME)) {
    redisIp = '10.130.164.240';
}

if (_.isEqual('SB234', CLIENT_NAME)) {
    redisIp = '10.130.157.149';
}

const redisPort = process.env.REDIS_PORT || "6379";

let client_redis = new redis({
    port: redisPort,          // Redis port
    host: redisIp,   // Redis host
    family: 4,           // 4 (IPv4) or 6 (IPv6)
    db: 8
});

if (_.isEqual(CLIENT_NAME, 'SPORTBOOK88')) {
    redisIp = "redis-19323.c1.ap-southeast-1-1.ec2.cloud.redislabs.com";
     client_redis = new redis({
        port: 19323,
        host: redisIp,
        family: 4,
        db: 0,
        password: "tm5ooPZeVqFrxbWYQy3DZEauC2hRt2pq"
    });
}

// redis.createClient({host: redisIp, port: redisPort});
// client_redis.select(8);
console.log('Redis IP #############');
console.log(redisIp);
console.log('Redis IP #############');
module.exports = {
    setSportMatchCount : (obj, callback) => {
        client_redis.set(`sportMatchCount`, JSON.stringify(obj));
        callback(null, 'success');
    },

    getByKey : (key, callback) => {
        client_redis.get(key, (error, result) => {
            callback(null, JSON.parse(result));
        });
        // client_redis.quit();
    },

    setByKey : (key, obj, callback) => {
        client_redis.set(key, JSON.stringify(obj));
        callback(null, 'success');
    },

    deleteByKey : (key, callback) => {
        client_redis.del(key);
        callback(null, 'success');
    },
};

