const express = require('express');
const router  = express.Router();
const _       = require('underscore');

const BoxingSBOModel = require('../../../../models/boxingSBO.model');
const Redis          = require('../../1_common/1_redis/1_1_redis');
const CLIENT_NAME    = process.env.CLIENT_NAME          || 'SPORTBOOK88';
const LOG = 'SETTING_ODDS';
router.put('/:leagueId', (req, res) => {
    const {
        leagueId
    } = req.params;
    const {
        rp
    } = req.body;
    Redis.deleteByKey(`BoxingSBOModel${CLIENT_NAME}`, (error, result) => {

    });
    BoxingSBOModel.update({
            k: parseInt(leagueId)
        },
        {
            $set: {
                'rp': rp
            }
        },
        {
            multi: false
        },
        (err, data) => {
            if (err) {
                console.log(`[${LOG}] Error update league odds : ${leagueId} ${rp}`);
                console.log(err);
                return res.send({
                    message: "error",
                    code: 999,
                    result : err
                });
            } else if (data.nModified === 0) {
                console.log(`[${LOG}] Failed update league odds : ${leagueId} ${rp}`);
                return res.send({
                    message: "failed",
                    code: 4,
                    result : data
                });
            } else {
                console.log(`[${LOG}] Success update league odds : ${leagueId} ${rp}`);
                return res.send({
                    message: "success",
                    code: 0,
                    result : _.first(data)
                });
            }
        });
});
module.exports = router;