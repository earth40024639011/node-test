const express = require('express');
const router  = express.Router();
const _       = require('underscore');

const IceHockeySBOModel = require('../../../../models/iceHockeySBO.model');
const Redis             = require('../../1_common/1_redis/1_1_redis');
const CLIENT_NAME       = process.env.CLIENT_NAME          || 'SPORTBOOK88';
const LOG = 'SETTING_SBO_LEAGUE';
router.post('/', (req, res) => {
    console.log(`[${LOG}] ${JSON.stringify(req.body)}`);

    const {
        k,
        n
    } = req.body;
    Redis.deleteByKey(`IceHockeySBOModel${CLIENT_NAME}`, (error, result) => {

    });
    const iceHockeySBOModel = new IceHockeySBOModel();
    iceHockeySBOModel.k = k;
    iceHockeySBOModel.n = n;

    iceHockeySBOModel.save((err, data) => {
        if (err) {
            return res.send({
                code: 0,
                message: "error",
                data: err
            });
        } else {
            return res.send({
                code: 0,
                message: "success"
            });
        }
    });
});


//[3]
router.get('/', (req, res) => {
    IceHockeySBOModel.find({})
        .lean()
        .sort('sk')
        .select('sk k n r')
        .exec((error, response) => {
            if (error) {
                return res.send({
                    code: 0,
                    message: "error",
                    result: error
                });
            } else {
                return res.send({
                    code: 0,
                    message: `success`,
                    result: _.sortBy(response, 'sk')
                });
            }
        });
});
module.exports = router;