const express   = require('express');
const router    = express.Router();
const async     = require("async");
const Joi       = require('joi');
const mongoose  = require('mongoose');
const _         = require('underscore');
const roundTo   = require('round-to');

const MemberModel                = require('../../../../../models/member.model');
const BetTransactionModel        = require('../../../../../models/betTransaction.model.js');
const AgentGroupModel            = require('../../../../../models/agentGroup.model.js');
const Commons                    = require('../../../../../common/commons');
const _Utils                     = require('../../../../../common/underscoreUtils');
const FormulaUtils               = require('../../../../../common/formula');
const MemberService              = require('../../../../../common/memberService');
const AgentService               = require("../../../../../common/agentService");
const DateUtils                  = require('../../../../../common/dateUtils');
const ApiService                 = require('../../../../../common/apiService');
const RedisService               = require('../../../../../common/redisService');
const Utility                    = require('../../../../../common/utlity');

const CLIENT_NAME                = process.env.CLIENT_NAME || 'SPORTBOOK88';
function generateBetId() {
    return 'BET' + new Date().getTime();
}

function calculateParlayPayout(amount, odd, callback) {
    let maxPayout = 2000000.1;
    let max = roundTo(maxPayout / odd, 2);
    if (amount > max) {
        callback(5012, null);
    } else {
        callback(null, '');
        // callback((amount * odd).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]);
    }
}

const addBetSchema = Joi.object().keys({
    memberId: Joi.string().required(),
    matchId: Joi.string().required(),
    odd: Joi.number().required(),
    oddKey: Joi.string().required(),
    oddType: Joi.string().required(),
    handicap: Joi.string().allow(''),
    bet: Joi.string().allow(''),
    gameType: Joi.string().allow(''),
    acceptAnyOdd: Joi.boolean().required(),
    priceType: Joi.string().required(),
    amount: Joi.number().required(),
    payout: Joi.number().required(),
    allKey: Joi.array(),
    ip: Joi.string().required(),
    key: Joi.string().required()
});


router.post('/hdp', function (req, res) {

    let validate = Joi.validate(req.body, addBetSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }
    console.log(JSON.stringify(req.body));
    const source         = 'ESPORT';
    const userInfo       = req.userInfo;
    const membercurrency = userInfo.currency;
    const matchType = 'NONE_LIVE';
    let isOfflineCash = false;
    async.series([
            (callback) => {
                MemberService.getOddAdjustment(userInfo.username, (err, result) => {
                    if (err) {
                        callback(error, null);
                    } else {
                        callback(null, result);
                    }
                });
            },

            (callback) => {
                MemberService.getLimitSetting(userInfo._id, (err, result) => {
                    if (err) {
                        callback(error, null);
                    } else {
                        callback(null, result.limitSetting);
                    }
                });
            },

            (callback) => {
                RedisService.getCurrency((error, result) => {
                    if (error) {
                        callback(null, false);
                    } else {
                        callback(null, result);
                    }
                })
            },
            (callback) => {
                if (_.isEqual(CLIENT_NAME, 'AMB_SPORTBOOK') ||
                    _.isEqual(CLIENT_NAME, 'IRON') ||
                    _.isEqual(CLIENT_NAME, 'FASTBET98') ||
                    _.isEqual(CLIENT_NAME, 'FAST98')||
                    _.isEqual(CLIENT_NAME, 'AFC1688') ||
                    _.isEqual(CLIENT_NAME, 'SPORTBOOK88') ||
                    _.isEqual(CLIENT_NAME, 'CSR') ||
                    _.isEqual(CLIENT_NAME, 'MGM')) {
                    AgentService.getOfflineCashStatus(userInfo.username, (error, isOfflineCash) => {
                        if (error) {
                            callback(error, false);
                        } else {
                            callback(null, isOfflineCash);
                        }
                    });
                } else {
                    callback(null, false);
                }
            }
        ],
        (error, resultOddAndLimit) => {
            if (error) {
                return res.send({
                    code: 9999,
                    message: error
                });
            } else {

                const [
                    oddAdjust,
                    limitSetting,
                    currency,
                    isOfflineCashResult
                ] = resultOddAndLimit;
                isOfflineCash = isOfflineCashResult;
                async.series([callback => {

                    //TODO: duplicate find member
                    MemberModel.findById(req.body.memberId, (err, response) => {
                        if (err)
                            callback(err, null);

                        if (response.suspend) {
                            callback(5011, null);
                        } else {
                            AgentService.getUpLineStatus(userInfo.group._id, (err, status) => {
                                if (err) {
                                    callback(err, null);
                                } else {
                                    if (status.suspend) {
                                        callback(5011, null);
                                    } else {
                                        callback(null, status);
                                    }
                                }
                            });
                        }
                    });


                }, callback => {

                    if (userInfo.group.type === 'API') {
                        callback(null, 0);
                    } else {
                        MemberService.getCredit(userInfo._id, (err, credit) => {
                            if (err) {
                                callback(err, null);
                            } else {
                                if (credit < req.body.amount) {
                                    callback(5005, null);
                                } else {
                                    callback(null, credit);
                                }
                            }
                        });
                    }

                }, callback => {

                    FormulaUtils.calculatePayout(req.body.amount, req.body.odd, req.body.oddType, req.body.priceType.toUpperCase(), (payout) => {
                        // if (req.body.payout != payout) {
                        //     callback(5004, null);
                        // } else {
                        callback(null, payout);
                        // }
                    });

                }], (err, results) => {

                    if (err) {
                        if (err === 5004) {
                            return res.send({
                                message: "payout not matched.",
                                result: null,
                                code: 5004
                            });
                        }
                        if (err === 5005) {
                            return res.send({
                                message: "credit exceed.",
                                result: null,
                                code: 5005
                            });
                        }
                        if (err === 5011) {
                            return res.send({
                                message: "Account or Upline was suspend.",
                                result: null,
                                code: 5011
                            });
                        }
                        return res.send({message: "error", result: err, code: 999});
                    }

                    async.waterfall([callback => {

                        MemberModel.findById(req.body.memberId, (err, response) => {
                            if (err)
                                callback(err, null);

                            if (response) {

                                let maxBet = findMaxBet(response, req.body.oddType);

                                if (req.body.amount > maxBet) {
                                    callback(5001, null);
                                } else {
                                    callback(null, response);

                                }
                            }
                        });

                    }, (memberInfo, callback) => {


                        const oddType = req.body.oddType.toUpperCase();

                        if (oddType === 'AH' ||
                            oddType === 'ML' ) {

                            let condition = {
                                memberId: mongoose.Types.ObjectId(req.body.memberId),
                                'hdp.matchId': req.body.matchId
                            };
                            condition['$or'] = [
                                {
                                    "status": "RUNNING"
                                },
                                {
                                    "status": "WAITING"
                                },
                                {
                                    "status": "DONE"
                                }
                            ];
                            BetTransactionModel.aggregate([
                                {
                                    $match: condition
                                },
                                {
                                    $project: {_id: 0}
                                },
                                {
                                    "$group": {
                                        "_id": {
                                            id: '$_id'
                                        },
                                        "amount": {$sum: '$amount'}
                                    }
                                }
                            ], function (err, results) {

                                if (err) {
                                    callback(err, '');
                                    return;
                                }

                                let totalBet = results[0] ? results[0].amount : 0;

                                if ((totalBet + Number.parseFloat(req.body.amount)) > convertMinMaxByCurrency(memberInfo.limitSetting.sportsBook.hdpOuOe.maxPerMatch, currency, membercurrency)) {
                                    callback(5002, null);
                                } else {
                                    callback(null, memberInfo, totalBet);
                                }
                            });
                        } else {
                            callback(null, memberInfo, 0);
                        }

                    }, (memberInfo, totalBet, callback) => {

                        AgentGroupModel.findById(userInfo.group._id).select('_id type parentId shareSetting commissionSetting').populate({
                            path: 'parentId',
                            select: '_id type parentId shareSetting commissionSetting name',
                            populate: {
                                path: 'parentId',
                                select: '_id type parentId shareSetting commissionSetting name',
                                populate: {
                                    path: 'parentId',
                                    select: '_id type parentId shareSetting commissionSetting name',
                                    populate: {
                                        path: 'parentId',
                                        select: '_id type parentId shareSetting commissionSetting name',
                                        populate: {
                                            path: 'parentId',
                                            select: '_id type parentId shareSetting commissionSetting name',
                                            populate: {
                                                path: 'parentId',
                                                select: '_id type parentId shareSetting commissionSetting name',
                                            }
                                        }
                                    }
                                }
                            }

                        }).exec(function (err, response) {
                            if (err)
                                callback(err, null);
                            else
                                callback(null, memberInfo, totalBet, response);
                        });

                    }, (memberInfo, totalBet, commission, callback) => {

                        const {
                            matchId,
                            oddType,
                            oddKey,
                            priceType,
                            odd,
                            handicap,
                            acceptAnyOdd,
                            key
                        } = req.body;

                        const {
                            _id
                        } = req.userInfo;

                        Commons.cal_bp_hdp_single_match_eSport_sbo(
                            matchId,
                            oddType,
                            oddKey.toLowerCase(),
                            memberInfo.commissionSetting.sportsBook.typeHdpOuOe,
                            oddAdjust,
                            _id,
                            matchType,
                            key,
                            (err, response) => {
                                if (error) {
                                    callback(error, null);
                                } else {
                                    console.log('response ----- :: ', response);
                                    checkOddProcess(
                                        response,
                                        priceType,
                                        oddType,
                                        oddKey,
                                        odd,
                                        handicap,
                                        (isOddNotMatch, key, oddValue, handicapValue, bet) => {
                                            let odd = {
                                                leagueName: response.league_name,
                                                leagueNameN: response.league_name_n,
                                                matchId: response.matchId,
                                                matchName: response.name,
                                                matchDate: response.date,
                                                oddType: oddType,
                                                odd: oddValue,
                                                key: key,
                                                oddKey: oddKey,
                                                handicap: handicapValue,
                                                satang: response.s,
                                                max: response.max,
                                                bet: bet,
                                                maxPerMatch: response.maxPerMatch
                                            };

                                            if (isOddNotMatch) {
                                                if (acceptAnyOdd) {
                                                    callback(null, memberInfo, totalBet, commission, odd);
                                                } else {
                                                    callback(5003, memberInfo, totalBet, commission, odd);
                                                }
                                            } else {
                                                callback(null, memberInfo, totalBet, commission, odd);
                                            }
                                        });
                                }
                            });

                    }], (err, memberInfo, totalBet, agentGroups, hdpInfo) => {


                        if (err) {
                            console.log(err);
                            if (err === 5001) {
                                return res.send({
                                    message: "amount exceeded (per bet)",
                                    result: null,
                                    code: 5001
                                });
                            }
                            else if (err === 5002) {
                                return res.send({
                                    message: "Your total bet has exceeded the maximum bet per match.",
                                    result: null,
                                    code: 5002
                                });
                            }
                            else if (err === 5003) {
                                return res.send({
                                    message: "odd was already changed.",
                                    result: hdpInfo,
                                    code: 5003
                                });
                            }
                            else if (err === 5006) {
                                return res.send({
                                    message: "score was already changed.",
                                    result: hdpInfo,
                                    code: 5006
                                });
                            }
                            return res.send({message: "error", result: err, code: 999});
                        }


                        if ((totalBet + Number.parseFloat(req.body.amount)) > convertMinMaxByCurrency(hdpInfo.maxPerMatch, currency, membercurrency)) {
                            return res.send({
                                message: "Your total bet has exceeded the maximum bet per match.",
                                result: null,
                                code: 5002
                            });
                        }

                        let memberCredit = roundTo(hdpInfo.odd < 0 ? req.body.amount * hdpInfo.odd : (req.body.amount * -1), 2);
                        let processDate = DateUtils.getCurrentDate();
                        console.log('hdpInfo', JSON.stringify(hdpInfo));
                        let body = {
                            betId: generateBetId(),
                            memberId: req.body.memberId,
                            memberParentGroup: memberInfo.group,
                            memberUsername: memberInfo.username_lower,
                            hdp: {
                                leagueId: req.body.matchId.split(':')[0],
                                matchId: req.body.matchId,
                                leagueName: hdpInfo.leagueName,
                                leagueNameN: hdpInfo.leagueNameN,
                                matchName: hdpInfo.matchName,
                                matchDate: hdpInfo.matchDate,
                                oddKey: hdpInfo.oddKey,
                                odd: hdpInfo.odd,
                                oddType: req.body.oddType,
                                handicap: hdpInfo.handicap,
                                bet: hdpInfo.bet,
                                score: '0:0',
                                matchType: matchType,
                                rowID: ""//hdpInfo.detail.rowID
                            },
                            amount: req.body.amount,
                            memberCredit: memberCredit,
                            payout: req.body.payout,
                            gameType: 'TODAY',
                            acceptHigherPrice: req.body.acceptHigherPrice,
                            priceType: req.body.priceType.toUpperCase(),
                            game: 'FOOTBALL',
                            source: source,
                            status: 'RUNNING',
                            commission: {},
                            gameDate: hdpInfo.matchDate,
                            createdDate: processDate,
                            ipAddress: Utility.getIP(req),
                            currency: memberInfo.currency,
                            isOfflineCash: isOfflineCash,
                            active: !isOfflineCash
                        };

                        console.log('BODY : ', JSON.stringify(body));

                        prepareCommission(memberInfo, body, agentGroups, null, 0, 0);

                        async.waterfall([callback => {
                            console.log('============== 1 : SAVE TRANSACTION ============');
                            if (userInfo.group.type === 'API') {

                                body.username = memberInfo.username;
                                body.gameType = 'TODAY';
                                body.sportType = source;

                                AgentGroupModel.findById(memberInfo.group, 'endpoint', function (err, agentResponse) {

                                    ApiService.placeBet(agentResponse.endpoint, body, (err, apiResponse) => {

                                        if (err) {
                                            if (err.code) {
                                                callback(err.code, null);
                                            } else {
                                                callback(999, null);
                                            }
                                        } else {
                                            let model = new BetTransactionModel(body);
                                            model.save(function (err, betResponse) {

                                                if (err) {
                                                    callback(999, null);
                                                } else {
                                                    let betResponse1 = Object.assign({}, betResponse)._doc;

                                                    betResponse1.playerBalance = apiResponse.balance;
                                                    callback(null, betResponse1);
                                                }
                                            });
                                        }

                                    });
                                });
                            } else {

                                // console.log('body : ', JSON.stringify(body));
                                let model = new BetTransactionModel(body);
                                model.save((err, betResponse) => {
                                    if (err) {
                                        console.error(err);
                                        callback(999, null);
                                    } else {

                                        callback(null, betResponse);
                                    }
                                });
                            }


                        }, (betResponse, callback) => {

                            console.log('============== 2 : ODD ADJUST ============');

                            let maxBetPrice = 5000;//_.min([findMaxBet(memberInfo, req.body.oddType), hdpInfo.max]);

                            let oddAdjBody = {
                                username: memberInfo.username,
                                matchId: req.body.matchId,
                                prefix: req.body.oddType.toLowerCase(),
                                key: hdpInfo.key,
                                value: hdpInfo.oddKey,
                                maxBetPrice: maxBetPrice,
                                amount: req.body.amount
                            };

                            MemberService.createOddAdjustment(oddAdjBody, (err, response) => {
                                if (err)
                                    callback(5007, null);
                                else
                                    callback(null, betResponse, response);
                            });

                        }, (betResponse, addJust, callback) => {

                            if (userInfo.group.type === 'API') {
                                callback(null, betResponse, addJust, {});
                            } else {
                                MemberService.updateBalance2(memberInfo.username_lower, memberCredit, betResponse.betId, 'SPORTS_BOOK_BET', betResponse.betId, function (err, response) {
                                    if (err) {
                                        callback(5008, null);
                                    } else {
                                        callback(null, betResponse, addJust, response);
                                    }
                                });
                            }

                        }, (betResponse, addJust, balance, callback) => {

                            callback(null, betResponse, addJust, balance, 'robotResponse');

                        }], function (err, betResponse, addJust, balance, robotResponse) {

                            if (err) {

                                if (err == 5007) {
                                    return res.send({
                                        message: "createOddAdjustment failed.",
                                        result: hdpInfo,
                                        code: 5007
                                    });
                                } else if (err == 5008) {
                                    return res.send({
                                        message: "update credit failed",
                                        result: hdpInfo,
                                        code: 5008
                                    });
                                } else if (err == 1003) {
                                    return res.send({
                                        message: "Insufficient Balance",
                                        code: 1003
                                    });
                                } else {
                                    return res.send({
                                        message: "Internal Server Error",
                                        result: err,
                                        code: 999
                                    });
                                }
                            } else {

                                MemberService.getCredit(userInfo._id, function (err, credit) {
                                    if (err) {
                                        return res.send({
                                            message: "get credit fail",
                                            result: err,
                                            code: 999
                                        });
                                    } else {

                                        return res.send({
                                            code: 0,
                                            message: "success",
                                            result: {
                                                betId: betResponse.betId,
                                                hdpInfo: hdpInfo,
                                                creditLimit: userInfo.group.type === 'API' ? betResponse.playerBalance : credit
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    });
                });
            }
        }
    );
});

function findMaxBet(memberInfo, oddType) {

    switch (oddType.toUpperCase()) {
        case 'AH':
        case 'AH1ST':
        case 'OU':
        case 'OU1ST':
        case 'OE':
        case 'OE1ST':
        case 'T1OU':
        case 'T2OU':
            return memberInfo.limitSetting.sportsBook.hdpOuOe.maxPerBet;
        case 'X12':
        case 'X121ST':
            return memberInfo.limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet;
    }
}

function checkOddProcess(bpResponse, priceType, oddType, oddKey, odd, handicap, callback) {


    console.log('----- :::::: ', oddType);
    console.log('----- :::::: ', oddKey);

    let handicapPrefix, oddPrefix;
    let type = oddType.toLowerCase();
    let isOddNotMatch = false;
    let oddValue, handicapValue;
    let key = '';
    let bet = '';

    if (['TG', 'DC', 'CS', 'FHCS', 'FTHT', 'FGLG', 'TTKO', 'TC'].includes(oddType.toUpperCase())) {
        key = oddKey;
    }

    if (oddType === 'AH' || oddType === 'AH1ST') {

        _Utils.findKeyByValue(bpResponse[type], oddKey, function (keyResponse) {
            key = keyResponse;
        });

        if (key === 'hpk') {
            handicapPrefix = 'h';
            oddPrefix = 'hp';
            bet = 'HOME';
        } else {
            handicapPrefix = 'a';
            oddPrefix = 'ap';
            bet = 'AWAY';
        }

        if (!_.isUndefined(bpResponse[type])) {
            console.log(bpResponse[type][oddPrefix] + '  1:  ' + odd);

            oddValue = bpResponse[type][oddPrefix];
            oddValue = convertOdd(priceType, oddValue, oddType);
            handicapValue = bpResponse[type][handicapPrefix];
            if (oddValue !== odd) {
                isOddNotMatch = true;
            }
            if (handicapValue !== handicap) {
                isOddNotMatch = true;
            }
        } else {
            isOddNotMatch = true;
        }

    } else if (oddType === 'OU' || oddType === 'OU1ST' || oddType === 'T1OU' || oddType === 'T2OU') {

        _Utils.findKeyByValue(bpResponse[type], oddKey, function (keyResponse) {
            key = keyResponse;
        });


        if (key === 'opk') {
            handicapPrefix = 'o';
            oddPrefix = 'op';
            bet = 'OVER';
        } else {
            handicapPrefix = 'u';
            oddPrefix = 'up';
            bet = 'UNDER';
        }

        if (!_.isUndefined(bpResponse[type])) {
            oddValue = bpResponse[type][oddPrefix];
            oddValue = convertOdd(priceType, oddValue, oddType);
            handicapValue = bpResponse[type][handicapPrefix];
            if (oddValue !== odd) {
                isOddNotMatch = true;
            }
            if (handicapValue !== handicap) {
                isOddNotMatch = true;
            }
        } else {
            isOddNotMatch = true;
        }

    } else if (oddType === 'X12' || oddType === 'X121ST' || oddType === 'ML') {

        _Utils.findKeyByValue(bpResponse[type], oddKey, function (keyResponse) {
            key = keyResponse;
        });

        if (key === 'hk') {
            oddPrefix = 'h';
            bet = 'HOME';
        } else if (key === 'ak') {
            oddPrefix = 'a';
            bet = 'AWAY';
        } else if (key === 'dk') {
            oddPrefix = 'd';
            bet = 'DRAW';
        }

        if (!_.isUndefined(bpResponse[type])) {
            oddValue = bpResponse[type][oddPrefix];
            oddValue = convertOdd(priceType, oddValue, oddType);
            if (oddValue !== odd) {
                isOddNotMatch = true;
            }
        } else {
            isOddNotMatch = true;
        }

    } else if (oddType === 'OE' || oddType === 'OE1ST') {

        _Utils.findKeyByValue(bpResponse[type], oddKey, function (keyResponse) {
            key = keyResponse;
        });

        if (key === 'ok') {
            oddPrefix = 'o';
            bet = 'ODD';
        } else if (key === 'ek') {
            oddPrefix = 'e';
            bet = 'EVEN';
        }

        if (!_.isUndefined(bpResponse[type])) {
            oddValue = bpResponse[type][oddPrefix];
            oddValue = convertOdd(priceType, oddValue, oddType);
            if (oddValue !== odd) {
                isOddNotMatch = true;
            }
        } else {
            isOddNotMatch = true;
        }
    } else if (oddType === 'OE' || oddType === 'OE1ST') {

        _Utils.findKeyByValue(bpResponse[type], oddKey, function (keyResponse) {
            key = keyResponse;
        });

        if (key === 'ok') {
            oddPrefix = 'o';
            bet = 'ODD';
        } else if (key === 'ek') {
            oddPrefix = 'e';
            bet = 'EVEN';
        }

        if (!_.isUndefined(bpResponse[type])) {
            oddValue = bpResponse[type][oddPrefix];
            oddValue = convertOdd(priceType, oddValue, oddType);
            if (oddValue !== odd) {
                isOddNotMatch = true;
            }
        } else {
            isOddNotMatch = true;
        }
    } else if (oddType === 'TG') {
        if (key === 'tg01k') {
            bet = 'TG01';
        } else if (key === 'tg02k') {
            bet = 'TG23';
        } else if (key === 'tg03k') {
            bet = 'TG46';
        } else {
            bet = 'TG7OVER';
        }
        oddValue = odd;
        handicapValue = handicap;
        isOddNotMatch = false;

    } else if (oddType === 'DC') {
        if (key === 'hdk') {
            bet = 'DC1X';
        } else if (key === 'hak') {
            bet = 'DC12';
        } else {
            bet = 'DC2X';
        }
        oddValue = odd;
        handicapValue = handicap;
        isOddNotMatch = false;

    } else if (oddType === 'CS' || oddType === 'FHCS') {
        if (key === 'd00k') {
            bet = 'CS00';
        } else if (key === 'a01k') {
            bet = 'CS01';
        } else if (key === 'a02k') {
            bet = 'CS02';
        } else if (key === 'a03k') {
            bet = 'CS03';
        } else if (key === 'a04k') {
            bet = 'CS04';
        } else if (key === 'h10k') {
            bet = 'CS10';
        } else if (key === 'd11k') {
            bet = 'CS11';
        } else if (key === 'a12k') {
            bet = 'CS12';
        } else if (key === 'a13k') {
            bet = 'CS13';
        } else if (key === 'a14k') {
            bet = 'CS14';
        } else if (key === 'h20k') {
            bet = 'CS20';
        } else if (key === 'h21k') {
            bet = 'CS21';
        } else if (key === 'd22k') {
            bet = 'CS22';
        } else if (key === 'a23k') {
            bet = 'CS23';
        } else if (key === 'a24k') {
            bet = 'CS24';
        } else if (key === 'h30k') {
            bet = 'CS30';
        } else if (key === 'h31k') {
            bet = 'CS31';
        } else if (key === 'h32k') {
            bet = 'CS32';
        } else if (key === 'd33k') {
            bet = 'CS33';
        } else if (key === 'a34k') {
            bet = 'CS34';
        } else if (key === 'h40k') {
            bet = 'CS40';
        } else if (key === 'h41k') {
            bet = 'CS41';
        } else if (key === 'h42k') {
            bet = 'CS42';
        } else if (key === 'h3k') {
            bet = 'CS43';
        } else if (key === 'd44k') {
            bet = 'CS44';
        } else {
            bet = 'CSAOS';
        }
        oddValue = odd;
        handicapValue = handicap;
        isOddNotMatch = false;

    } else if (oddType === 'FTHT') {
        if (key === 'hhk') {
            bet = 'FTHT_HH';
        } else if (key === 'hdk') {
            bet = 'FTHT_HD';
        } else if (key === 'hak') {
            bet = 'FTHT_HA';
        } else if (key === 'dhk') {
            bet = 'FTHT_DH';
        } else if (key === 'ddk') {
            bet = 'FTHT_DD';
        } else if (key === 'dak') {
            bet = 'FTHT_DA';
        } else if (key === 'ahk') {
            bet = 'FTHT_AH';
        } else if (key === 'adk') {
            bet = 'FTHT_AD';
        } else {
            bet = 'FTHT_AA';
        }
        oddValue = odd;
        handicapValue = handicap;
        isOddNotMatch = false;

    } else if (oddType === 'FGLG') {
        if (key === 'hfk') {
            bet = 'FGLGFH';
        } else if (key === 'hlk') {
            bet = 'FGLGHL';
        } else if (key === 'afk') {
            bet = 'FGLGAF';
        } else if (key === 'alk') {
            bet = 'FGLGAL';
        } else {
            bet = 'FGLGNG';
        }
        oddValue = odd;
        handicapValue = handicap;
        isOddNotMatch = false;

    } else if (oddType === 'TTKO') {
        if (key === 'hk') {
            bet = 'HOME';
        } else {
            bet = 'AWAY';
        }
        oddValue = odd;
        handicapValue = handicap;
        isOddNotMatch = false;

    } else {
        console.log('oddType not match')
    }
    console.log('input_odd : ', odd + '  , oddValue : ' + oddValue);
    console.log('input_handicap : ', handicap + '  , handicapValue : ' + handicapValue);
    console.log('isOddNotMatch : ', isOddNotMatch);
    callback(isOddNotMatch, key, oddValue, handicapValue, bet);

}


function prepareCommission(memberInfo, body, currentAgent, overAgent, remaining, overAllShare) {


    if (currentAgent && (currentAgent.type === 'SUPER_ADMIN' || currentAgent.parentId)) {


        console.log('==============================================');
        console.log('process : ' + currentAgent.type + ' : ' + currentAgent.name);
        console.log('currenctAgent : ',currentAgent)


        let currentAgentShareSettingParent;
        let currentAgentShareSettingOwn;
        let currentAgentShareSettingRemaining;
        let currentAgentShareSettingMin;

        let overAgentShareSettingParent;
        let overAgentShareSettingOwn;
        let overAgentShareSettingRemaining;
        let overAgentShareSettingMin;

        let agentCommission;


        if (body.gameType === 'PARLAY') {
            currentAgentShareSettingParent = currentAgent.shareSetting.step.parlay.parent;
            currentAgentShareSettingOwn = currentAgent.shareSetting.step.parlay.own;
            currentAgentShareSettingRemaining = currentAgent.shareSetting.step.parlay.remaining;
            currentAgentShareSettingMin = currentAgent.shareSetting.step.parlay.min;

            let parlayMatchCount = body.parlay.matches.length;

            if (overAgent) {
                overAgentShareSettingParent = overAgent.shareSetting.step.parlay.parent;
                overAgentShareSettingOwn = overAgent.shareSetting.step.parlay.own;
                overAgentShareSettingRemaining = overAgent.shareSetting.step.parlay.remaining;
                overAgentShareSettingMin = overAgent.shareSetting.step.parlay.min;
            } else {
                overAgent = {};
                overAgent.type = 'member';
                overAgentShareSettingParent = memberInfo.shareSetting.step.parlay.parent;
                overAgentShareSettingOwn = 0;
                overAgentShareSettingRemaining = 0;

                body.commission.member = {};
                body.commission.member.parent = memberInfo.shareSetting.step.parlay.parent;


                // if (parlayMatchCount == 2) {
                    body.commission.member.commission = memberInfo.commissionSetting.step.com2;
                // } else if (parlayMatchCount >= 3 && parlayMatchCount <= 4) {
                //     body.commission.member.commission = memberInfo.commissionSetting.step.com34;
                // } else if (parlayMatchCount >= 5 && parlayMatchCount <= 6) {
                //     body.commission.member.commission = memberInfo.commissionSetting.step.com56;
                // } else if (parlayMatchCount >= 7 && parlayMatchCount <= 8) {
                //     body.commission.member.commission = memberInfo.commissionSetting.step.com78;
                // } else if (parlayMatchCount >= 9 && parlayMatchCount <= 10) {
                //     body.commission.member.commission = memberInfo.commissionSetting.step.com910;
                // } else if (parlayMatchCount >= 11 && parlayMatchCount <= 12) {
                //     body.commission.member.commission = memberInfo.commissionSetting.step.com1112;
                // }else {
                //     body.commission.member.commission = 0;
                // }
            }

            // if (parlayMatchCount == 2) {
                agentCommission = currentAgent.commissionSetting.step.com2;
            // } else if (parlayMatchCount >= 3 && parlayMatchCount <= 4) {
            //     agentCommission = currentAgent.commissionSetting.step.com34;
            // } else if (parlayMatchCount >= 5 && parlayMatchCount <= 6) {
            //     agentCommission = currentAgent.commissionSetting.step.com56;
            // } else if (parlayMatchCount >= 7 && parlayMatchCount <= 8) {
            //     agentCommission = currentAgent.commissionSetting.step.com78;
            // } else if (parlayMatchCount >= 9 && parlayMatchCount <= 10) {
            //     agentCommission = currentAgent.commissionSetting.step.com910;
            // } else if (parlayMatchCount >= 11 && parlayMatchCount <= 12) {
            //     agentCommission = currentAgent.commissionSetting.step.com1112;
            // }


        } else {


            if (body.hdp.matchType === 'LIVE') {
                currentAgentShareSettingParent = currentAgent.shareSetting.sportsBook.live.parent;
                currentAgentShareSettingOwn = currentAgent.shareSetting.sportsBook.live.own;
                currentAgentShareSettingRemaining = currentAgent.shareSetting.sportsBook.live.remaining;
                currentAgentShareSettingMin = currentAgent.shareSetting.sportsBook.live.min;

                if (overAgent) {
                    overAgentShareSettingParent = overAgent.shareSetting.sportsBook.live.parent;
                    overAgentShareSettingOwn = overAgent.shareSetting.sportsBook.live.own;
                    overAgentShareSettingRemaining = overAgent.shareSetting.sportsBook.live.remaining;
                    overAgentShareSettingMin = overAgent.shareSetting.sportsBook.live.min;
                } else {
                    overAgent = {};
                    overAgent.type = 'member';
                    overAgentShareSettingParent = memberInfo.shareSetting.sportsBook.live.parent;
                    overAgentShareSettingOwn = 0;
                    overAgentShareSettingRemaining = 0;

                    body.commission.member = {};
                    body.commission.member.parent = memberInfo.shareSetting.sportsBook.live.parent;
                    body.commission.member.commission = memberInfo.commissionSetting.sportsBook.hdpOuOe;
                }
            } else if (body.hdp.matchType === 'NONE_LIVE') {
                currentAgentShareSettingParent = currentAgent.shareSetting.sportsBook.today.parent;
                currentAgentShareSettingOwn = currentAgent.shareSetting.sportsBook.today.own;
                currentAgentShareSettingRemaining = currentAgent.shareSetting.sportsBook.today.remaining;
                currentAgentShareSettingMin = currentAgent.shareSetting.sportsBook.today.min;

                if (overAgent) {
                    overAgentShareSettingParent = overAgent.shareSetting.sportsBook.today.parent;
                    overAgentShareSettingOwn = overAgent.shareSetting.sportsBook.today.own;
                    overAgentShareSettingRemaining = overAgent.shareSetting.sportsBook.today.remaining;
                    overAgentShareSettingMin = overAgent.shareSetting.sportsBook.today.min;
                } else {
                    overAgent = {};
                    overAgent.type = 'member';
                    overAgentShareSettingParent = memberInfo.shareSetting.sportsBook.today.parent;
                    overAgentShareSettingOwn = 0;
                    overAgentShareSettingRemaining = 0;

                    body.commission.member = {};
                    body.commission.member.parent = memberInfo.shareSetting.sportsBook.today.parent;
                    body.commission.member.commission = memberInfo.commissionSetting.sportsBook.hdpOuOe;
                }
            }

            if (['AH', 'OU', 'OE', 'AH1ST', 'OU1ST', 'OE1ST'].includes(body.hdp.oddType.toUpperCase())) {

                if (memberInfo.commissionSetting.sportsBook.typeHdpOuOe === 'A') {
                    agentCommission = currentAgent.commissionSetting.sportsBook.hdpOuOeA;
                }
                if (memberInfo.commissionSetting.sportsBook.typeHdpOuOe === 'B') {
                    agentCommission = currentAgent.commissionSetting.sportsBook.hdpOuOeB;
                }
                if (memberInfo.commissionSetting.sportsBook.typeHdpOuOe === 'C') {
                    agentCommission = currentAgent.commissionSetting.sportsBook.hdpOuOeC;
                }
                if (memberInfo.commissionSetting.sportsBook.typeHdpOuOe === 'D') {
                    agentCommission = currentAgent.commissionSetting.sportsBook.hdpOuOeD;
                }
                if (memberInfo.commissionSetting.sportsBook.typeHdpOuOe === 'E') {
                    agentCommission = currentAgent.commissionSetting.sportsBook.hdpOuOeE;
                }
                if (memberInfo.commissionSetting.sportsBook.typeHdpOuOe === 'F') {
                    agentCommission = currentAgent.commissionSetting.sportsBook.hdpOuOeF;
                }

            } else if (['X12', 'X121ST', 'DC'].includes(body.hdp.oddType.toUpperCase())) {
                body.commission.member.commission = memberInfo.commissionSetting.sportsBook.oneTwoDoubleChance;
                agentCommission = currentAgent.commissionSetting.sportsBook.oneTwoDoubleChance;

            } else {
                body.commission.member.commission = memberInfo.commissionSetting.sportsBook.others;
                agentCommission = currentAgent.commissionSetting.sportsBook.others;
            }
        }

        let money = body.amount;

        console.log('');
        console.log('---- setting ----');
        console.log('ส่วนแบ่งที่ได้มามีทั้งหมด : ' + currentAgentShareSettingOwn + ' %');
        console.log('ขอส่วนแบ่งจาก : ' + overAgent.type + ' ' + overAgentShareSettingParent + ' %');
        console.log('ให้ส่วนแบ่ง : ' + overAgent.type + ' ที่เหลือไป ' + overAgentShareSettingOwn + ' %');
        console.log('remaining จาก : ' + overAgent.type + ' : ' + overAgentShareSettingRemaining + ' %');
        console.log('โดนบังคับเสียขั้นต่ำ : ' + currentAgentShareSettingMin + ' %');
        console.log('---- end setting ----');
        console.log('');


        // console.log('และได้รับ remaining ไว้ : '+overAgent.shareSetting.sportsBook.hdpOuOe.remaining+' %');

        let currentPercentReceive = overAgentShareSettingParent;
        console.log('% ที่ได้ : ' + currentPercentReceive);
        console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');
        let totalRemaining = remaining;

        if (overAgentShareSettingRemaining > 0) {
            // console.log()
            // console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');

            if (overAgentShareSettingRemaining > totalRemaining) {
                console.log('รับ remaining ทั้งหมดไว้: ' + totalRemaining + ' %');
                currentPercentReceive += totalRemaining;
                totalRemaining = 0;
            } else {
                totalRemaining = remaining - overAgentShareSettingRemaining;
                console.log('หักค่า remaining ที่ได้รับมากับที่เซตรับไว้ = ' + remaining + ' - ' + overAgentShareSettingRemaining + ' = ' + totalRemaining);
                currentPercentReceive += overAgentShareSettingRemaining;
            }

        }

        console.log('รวม % ที่ได้รับ : ' + currentPercentReceive);


        let unUseShare = currentAgentShareSettingOwn - (overAgentShareSettingParent + overAgentShareSettingOwn);

        console.log('เหลือส่วนแบ่งที่ไม่ได้ใช้ : ' + unUseShare + ' %');
        totalRemaining = (unUseShare + totalRemaining);
        console.log('ส่วนแบ่งที่ไม่ได้ใช้ บวกกับค่า remaining ท่ี่เหลือจะเป็น : ' + totalRemaining + ' %');

        console.log('จากที่โดนบังคับเสียขั้นต่ำ : ' + currentAgentShareSettingMin + ' %');
        console.log('overAllShare : ', overAllShare)
        if (currentAgentShareSettingMin > 0 && ((overAllShare + currentPercentReceive) < currentAgentShareSettingMin)) {

            if (currentPercentReceive < currentAgentShareSettingMin) {
                console.log('% ที่ได้รับ < ขั้นต่ำที่ถูกตั้ง min ไว้');
                let percent = currentAgentShareSettingMin - (currentPercentReceive + overAllShare);
                console.log('หักออกไป ' + percent + ' % : แล้วไปเพิ่มในส่วนที่ต้องรับผิดชอบ ');
                totalRemaining = totalRemaining - percent;
                if (totalRemaining < 0) {
                    totalRemaining = 0;
                }
                currentPercentReceive += percent;
            }
        } else {
            // currentPercentReceive += totalRemaining;
        }


        if (currentAgent.type === 'SUPER_ADMIN') {
            currentPercentReceive += totalRemaining;
        }
        let getMoney = (money * convertPercent(currentPercentReceive)).toFixed(2);
        overAllShare = overAllShare + currentPercentReceive;

        console.log('ยอดแทง ' + money + ' ได้ทั้งหมด : ' + currentPercentReceive + ' %');

        console.log('รวม % + % remaining  จะเป็นสัดส่วนที่ได้ทั้งหมด = ' + getMoney);
        console.log('ค่าที่จะถูกคืนกลับไป (remaining) : ' + totalRemaining + ' %');
        console.log('จำนวน % ที่ถูกแบ่งไปแล้วทั้งหมด : ', overAllShare);
        console.log('agentCommission : ', agentCommission)

        switch (currentAgent.type) {
            case 'SUPER_ADMIN':
                body.commission.superAdmin = {};
                body.commission.superAdmin.group = currentAgent._id;
                body.commission.superAdmin.parent = currentAgent.shareSetting.casino.sexy.parent;
                body.commission.superAdmin.own = currentAgent.shareSetting.casino.sexy.own;
                body.commission.superAdmin.remaining = currentAgent.shareSetting.casino.sexy.remaining;
                body.commission.superAdmin.min = currentAgent.shareSetting.casino.sexy.min;
                body.commission.superAdmin.shareReceive = Math.abs(currentPercentReceive);
                body.commission.superAdmin.commission = agentCommission;
                body.commission.superAdmin.amount = getMoney;
                break;
            case 'COMPANY':
                body.commission.company = {};
                body.commission.company.parentGroup = currentAgent.parentId;
                body.commission.company.group = currentAgent._id;
                body.commission.company.parent = currentAgentShareSettingParent;
                body.commission.company.own = currentAgentShareSettingOwn;
                body.commission.company.remaining = currentAgentShareSettingRemaining;
                body.commission.company.min = currentAgentShareSettingMin;
                body.commission.company.shareReceive = Math.abs(currentPercentReceive);
                body.commission.company.commission = agentCommission;
                body.commission.company.amount = getMoney;
                break;
            case 'SHARE_HOLDER':
                body.commission.shareHolder = {};
                body.commission.shareHolder.parentGroup = currentAgent.parentId;
                body.commission.shareHolder.group = currentAgent._id;
                body.commission.shareHolder.parent = currentAgentShareSettingParent;
                body.commission.shareHolder.own = currentAgentShareSettingOwn;
                body.commission.shareHolder.remaining = currentAgentShareSettingRemaining;
                body.commission.shareHolder.min = currentAgentShareSettingMin;
                body.commission.shareHolder.shareReceive = currentPercentReceive;
                body.commission.shareHolder.commission = agentCommission;
                body.commission.shareHolder.amount = getMoney;
                break;
            case 'API':
                body.commission.api = {};
                body.commission.api.parentGroup = currentAgent.parentId;
                body.commission.api.group = currentAgent._id;
                body.commission.api.parent = currentAgentShareSettingParent;
                body.commission.api.own = currentAgentShareSettingOwn;
                body.commission.api.remaining = currentAgentShareSettingRemaining;
                body.commission.api.min = currentAgentShareSettingMin;
                body.commission.api.shareReceive = currentPercentReceive;
                body.commission.api.commission = agentCommission;
                body.commission.api.amount = getMoney;
                break;
            case 'SENIOR':
                body.commission.senior = {};
                body.commission.senior.parentGroup = currentAgent.parentId;
                body.commission.senior.group = currentAgent._id;
                body.commission.senior.parent = currentAgentShareSettingParent;
                body.commission.senior.own = currentAgentShareSettingOwn;
                body.commission.senior.remaining = currentAgentShareSettingRemaining;
                body.commission.senior.min = currentAgentShareSettingMin;
                body.commission.senior.shareReceive = currentPercentReceive;
                body.commission.senior.commission = agentCommission;
                body.commission.senior.amount = getMoney;
                break;
            case 'MASTER_AGENT':
                body.commission.masterAgent = {};
                body.commission.masterAgent.parentGroup = currentAgent.parentId;
                body.commission.masterAgent.group = currentAgent._id;
                body.commission.masterAgent.parent = currentAgentShareSettingParent;
                body.commission.masterAgent.own = currentAgentShareSettingOwn;
                body.commission.masterAgent.remaining = currentAgentShareSettingRemaining;
                body.commission.masterAgent.min = currentAgentShareSettingMin;
                body.commission.masterAgent.shareReceive = currentPercentReceive;
                body.commission.masterAgent.commission = agentCommission;
                body.commission.masterAgent.amount = getMoney;
                break;
            case 'AGENT':
                body.commission.agent = {};
                body.commission.agent.parentGroup = currentAgent.parentId;
                body.commission.agent.group = currentAgent._id;
                body.commission.agent.parent = currentAgentShareSettingParent;
                body.commission.agent.own = currentAgentShareSettingOwn;
                body.commission.agent.remaining = currentAgentShareSettingRemaining;
                body.commission.agent.min = currentAgentShareSettingMin;
                body.commission.agent.shareReceive = currentPercentReceive;
                body.commission.agent.commission = agentCommission;
                body.commission.agent.amount = getMoney;
                break;
        }

        prepareCommission(memberInfo, body, currentAgent.parentId, currentAgent, totalRemaining, overAllShare);
    }
}

function convertPercent(percent) {
    return percent / 100
}

function convertOdd(priceType, odd, oddType) {

    if (priceType === 'MY') {
        return odd;
    } else if (priceType === 'HK') {
        if (!odd.includes('-')) {
            return odd;
        }
        odd = odd.replace('-', '');
        odd = parseFloat(odd) * 100;

        let tmp = 100 / odd;
        return tmp.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
    } else if (priceType === 'EU') {
        if (!odd.includes('-')) {
            if (oddType === 'X12' || oddType === 'X121ST') {
                return odd;
            }
            let t = parseFloat(odd) + 1.00;
            if (!t.toString().includes('.')) {
                t = t.toString() + '.00';
                return t;
            } else {

                if (t.toString().length == 3) {
                    t = t.toString() + '0';
                    return t;
                } else {
                    return t.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
                }

            }
        }
        odd = odd.replace('-', '');
        odd = parseFloat(odd) * 100;
        let tmp = (100 / odd) + 1.00;
        return tmp.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];

    } else if (priceType === 'ID') {
        if (oddType === 'X12' || oddType === 'X121ST') {
            return odd;
        }


        let tmp = parseFloat(odd) * -1;
        tmp = 1 / tmp;

        if (!tmp.toString().includes('.')) {
            tmp = tmp.toString() + '.00';
            return tmp;
        } else {

            if (tmp.toString().length == 3) {
                tmp = tmp.toString() + '0';
                return tmp;
            } else {
                return tmp.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
            }

        }
    }
}

const convertMinMaxByCurrency = (value, currency, membercurrency) => {
    // console.log('###########################');
    // console.log('[BF]value      : ', value);
    // console.log('membercurrency : ', membercurrency);

    let minMax = value;
    if (_.isUndefined(membercurrency) || _.isNull(membercurrency)) {
        return minMax;
    } else {
        const predicate = JSON.parse(`{"currencyName":"${membercurrency}"}`);
        const obj = _.chain(currency)
            .findWhere(predicate)
            .pick('currencyTHB')
            .value();
        if (!_.isUndefined(obj.currencyTHB)) {
            // console.log('               : ',obj.currencyTHB);

            if (_.isEqual(membercurrency, "IDR") || _.isEqual(membercurrency, "VND")) {
                minMax = Math.ceil(value * obj.currencyTHB);
            } else {
                minMax = Math.ceil(value / obj.currencyTHB);
            }
            // console.log('[AF]value      : ', minMax);
            // console.log('###########################');
            return minMax;
        } else {
            return minMax;
        }
    }

};


module.exports = router;