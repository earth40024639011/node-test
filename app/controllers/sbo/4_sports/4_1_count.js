const express   = require('express');
const router    = express.Router();
const Redis     = require('../1_common/1_redis/1_1_redis');
const _         = require('underscore');
const BetTransactionModel        = require('../../../models/betTransaction.model.js');
const httpRequest = require('request');
const LOG       = 'SPORT_MATCH_COUNT';
const NodeCache = require( "node-cache" );
const CLIENT_NAME = process.env.CLIENT_NAME || 'SPORTBOOK88';
const cache = new NodeCache( { stdTTL: 30, checkperiod: 31 } );

router.get('/', (req, res) => {
    Redis.getByKey('sportMatchCount', (error, response) => {
        if (error) {
            return res.send(
                {
                    code: 0,
                    message: "Error",
                    result: error
                });
        } else {
            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: response
                });
        }
    });
});

router.get('/2', (req, res) => {
    try{
        const contents = cache.get( "c2", true );
        return res.send(
            {
                code: 0,
                message: "success",
                result: contents
            });
    } catch( err ){
        Redis.getByKey('_sportMatchCount2', (error, response) => {
            if (error) {
                return res.send(
                    {
                        code: 0,
                        message: "Error",
                        result: error
                    });
            } else {
                response.spacialOdds = {
                    id: 999,
                    all: 0
                }
                Redis.getByKey(`spacialOdds${CLIENT_NAME}`, (error, responseSpacialOdds) => {
                    if (error) {
                        cache.set( "c2", response);
                        return res.send(
                            {
                                code: 0,
                                message: "success",
                                result: response
                            });
                    } else {
                        const responseSpacialOddsList = _.chain(responseSpacialOdds)
                            .pluck('m')
                            .flatten(true)
                            .filter(match => {
                                return match.isOn
                            })
                            .value();

                        response.spacialOdds.all = _.size(responseSpacialOddsList);
                        cache.set( "c2", response);
                        return res.send(
                            {
                                code: 0,
                                message: "success",
                                result: response
                            });
                    }
                });
            }
        });
    }
});

const parallel          = require("async/parallel");
const stringSimilarity  = require('string-similarity');
const URL_CHECK_OUT     = process.env.URL_CHECK_OUT     || 'http://dd88bet.api-hub.com:8001/spa/end';
const URL_CANCEL_MATCH  = process.env.URL_CANCEL_MATCH  || 'http://dd88bet.api-hub.com:8001/spa/stock/cancel-ticket-match';

router.get('/endScore', (req, res) => {
    parallel([
            (callback) => {
                BetTransactionModel.find({
                    game: "FOOTBALL",
                    status: 'RUNNING'
                })
                    .lean()
                    .sort('betId')
                    .select('hdp parlay')
                    .exec((error, response) => {
                        if (error) {
                            callback(error, null);
                        } else {
                            callback(null, response);
                        }
                    });
            },

            (callback) => {
                Redis.getByKey('resultToday', (error, response) => {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            },

            (callback) => {
                Redis.getByKey('resultPrevious', (error, response) => {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            }
        ],
        (error, parallelResult) => {
            if (error) {
                return res.send(
                    {
                        code: 0,
                        message: "error",
                        result: error
                    });
            } else {
                const [
                    betTransaction,
                    resultToday,
                    resultPrevious
                ] = parallelResult;

                const sboObjResultTodayList = _.chain(resultToday.today)
                    .pluck('match')
                    .flatten(true)
                    .value();

                const sboObjResultPreviousList = _.chain(resultPrevious.last1)
                    .pluck('match')
                    .flatten(true)
                    .value();

                sboObjResultTodayList.push(...sboObjResultPreviousList);

                const groupsHdp = _.chain(betTransaction)
                    .filter(obj => {
                        return _.isEmpty(obj.parlay.matches);
                    })
                    .groupBy(obj =>{
                        let mt = 'FT';
                        if (obj.hdp.oddType.includes("1ST")) {
                            mt = 'HT';
                        }
                        return `${obj.hdp.matchId}:${mt}:${obj.hdp.matchName.en.h}:${obj.hdp.matchName.en.a}`
                    })
                    .map((value, key) => {
                        const [
                            league_key,
                            match_key,
                            mt,
                            home,
                            away
                        ] = key.split(':');
                        return {
                            league_key: league_key,
                            match_key: match_key,
                            mt: mt,
                            home: home,
                            away: away
                        }
                    })
                    .value();

                const groupsParlay = _.chain(betTransaction)
                    .filter(obj => {
                        return !_.isEmpty(obj.parlay.matches);
                    })
                    .map(obj => {
                        return obj.parlay.matches;
                    })
                    .flatten(true)
                    .filter(obj => {
                        return !obj.isCancel
                    })
                    .groupBy(obj =>{

                        const {
                            oddType,
                            matchId,
                            matchName
                        } = obj;

                        let mt = 'FT';
                        if (oddType.includes("1ST")) {
                            mt = 'HT';
                        }
                        return `${matchId}:${mt}:${matchName.en.h}:${matchName.en.a}`
                    })
                    .map((value, key) => {
                        const [
                            league_key,
                            match_key,
                            mt,
                            home,
                            away
                        ] = key.split(':');
                        return {
                            league_key: league_key,
                            match_key: match_key,
                            mt: mt,
                            home: home,
                            away: away
                        }
                    })
                    .value();

                groupsHdp.push(...groupsParlay);

                // {
                //     "key": 2444984,
                //     "home": "Fagiano Okayama",
                //     "away": "Tokushima Vortis",
                //     "date": "07/29/2018 17:00",
                //     "ht": "Refunded",
                //     "ft": "Refunded",
                //     "moreResult": null
                // }

                let groupsMatch = _.chain(groupsHdp)
                    .each(obj => {
                        const {
                            league_key,
                            match_key,
                            mt,
                            home,
                            away
                        } = obj;
                        let result = _.findWhere(sboObjResultTodayList, JSON.parse(`{"key":${match_key}}`));

                        if (_.isUndefined(result)) {
                            result = _.first(_.chain(sboObjResultTodayList)
                                .filter(objResult => {
                                    return stringSimilarity.compareTwoStrings(objResult.home, home) > 0.8 || stringSimilarity.compareTwoStrings(objResult.away, away) > 0.8
                                })
                                .value());
                            console.log(result);
                        }
                    // , "home":"${home}","away":"${away}"}`));
                    //     console.log(sboObjResultTodayList);
                        if (!_.isUndefined(result)) {
                            console.log('result : ', JSON.stringify(obj));
                            const {
                                ht,
                                ft,
                            } = result;

                            let score = ft;
                            if (_.isEqual(mt, 'HT')) {
                                score = ht;
                            }

                            const [
                                h,
                                a,
                            ] = score.split(':');

                            if (ht.includes("Refunded") && ft.includes("Refunded")) {
                                httpRequest.post(
                                    URL_CANCEL_MATCH,
                                    {
                                        json: {
                                            matchId: `${league_key}:${match_key}`,
                                            status: 'CANCELLED',
                                            remark: 'Refunded',
                                            type: 'FT'
                                        }
                                    },
                                    (error, response) => {
                                        if(error){
                                            console.error(error);
                                        } else {
                                            console.log(`${URL_CANCEL_MATCH} [${response.body.code}]`);
                                        }
                                    });
                            } else if (!h.includes("-") && !a.includes("-")) {

                                const json = {
                                    matchId: `${league_key}:${match_key}`,
                                    type: mt
                                };

                                if (_.isEqual(mt, 'HT')) {
                                    json.homeScoreHT= h;
                                    json.awayScoreHT= a;
                                    json.homeScoreFT= 0;
                                    json.awayScoreFT= 0;
                                } else {
                                    const [
                                        htH,
                                        htA,
                                    ] = ht.split(':');
                                    json.homeScoreHT= htH;
                                    json.awayScoreHT= htA;
                                    json.homeScoreFT= h;
                                    json.awayScoreFT= a;
                                }

                                console.log('END SCORE');
                                console.log(JSON.stringify(json));
                                httpRequest.post(
                                    URL_CHECK_OUT,
                                    {
                                        json: json
                                    },
                                    (error, response) => {
                                        if(error){
                                            console.error(error);
                                        } else {
                                            console.log(`${URL_CHECK_OUT} [${response.body.code}]`);
                                        }
                                    });
                            }

                        }
                    })
                    .value();


                return res.send(
                    {
                        code: 0,
                        message: "success",
                        result: groupsMatch
                    });

            }
    });
});
module.exports = router;
