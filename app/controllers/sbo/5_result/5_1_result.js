const express   = require('express');
const router    = express.Router();
const Redis     = require('../1_common/1_redis/1_1_redis');
const LOG       = 'SPORT_FOOTBALL_RESULT';

router.get('/next', (req, res) => {
    Redis.getByKey('resultNextAMBFM', (error, response) => {
        if (error) {
            return res.send(
                {
                    code: 0,
                    message: "Error",
                    result: error
                });
        } else {
            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: response
                });
        }
    });
});
router.get('/previous', (req, res) => {
    Redis.getByKey('resultPreviousAMBFM', (error, response) => {
        if (error) {
            return res.send(
                {
                    code: 0,
                    message: "Error",
                    result: error
                });
        } else {
            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: response
                });
        }
    });
});
router.get('/today', (req, res) => {
    Redis.getByKey('resultTodayAMBFM', (error, response) => {
        if (error) {
            return res.send(
                {
                    code: 0,
                    message: "Error",
                    result: error
                });
        } else {
            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: response
                });
        }
    });
});

module.exports = router;
