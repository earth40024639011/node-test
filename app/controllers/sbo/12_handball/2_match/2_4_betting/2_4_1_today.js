const express     = require('express');
const router      = express.Router();
const _           = require('underscore');
const parallel    = require("async/parallel");
const Joi         = require('joi');
const redis       = require('../../../../sbo/1_common/1_redis/1_1_redis');
const httpRequest = require('request');

const ESportSBOModel = require('../../../../../models/eSportSBO.model');
const MemberService  = require('../../../../../common/memberService');
const Commons        = require('../../../../../common/commons');
const LOG            = 'BETTING_BASKETBALL_TODAY';

const bettingSchema = Joi.object().keys({
    id: Joi.string().required(),
    prefix: Joi.string().required(),
    key: Joi.string().required(),
    value:Joi.string().required()
});

router.put('/', (req, res) => {
    console.log(`[${LOG}] ${JSON.stringify(req.body)}`);
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const request = req.body;
    let validate = Joi.validate(request, bettingSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    const {
        _id,
        username,
        currency,
        commissionSetting
    } = req.userInfo;

    const {
        id,
        prefix,
        key,
        value
    } = request;

    const league_key = parseInt(_.first(id.split(':')));
    const match_key = parseInt(_.last(id.split(':')));
    const prefixLowerCase = prefix.toLowerCase();
    const predicate = JSON.parse(`{"${key}":"${value}"}`);

    parallel([
            (callback) => {
                MemberService.getLimitSetting(_id, (errLimitSetting, dataLimitSetting)=>{
                    if(errLimitSetting){
                        callback(errLimitSetting, null);
                    } else {
                        callback(null, dataLimitSetting.limitSetting);
                    }
                });
            },

            (callback) => {
                MemberService.getOddAdjustment(username, (err, data) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, data);
                    }
                });
            },

            (callback) => {
                ESportSBOModel.find({})
                    .lean()
                    .sort('sk')
                    .select('k n r')
                    .exec((error, response) => {
                        if (error) {
                            callback(error, null);
                        } else {
                            callback(null, response);
                        }
                    });
            },

            (callback) => {
                redis.getByKey('sboTodayESport', (error, response) => {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            }
        ],
        (error, parallelResult) => {
            if (error) {
                return res.send(
                    {
                        code: 0,
                        message: "error",
                        result: error
                    });
            } else {
                const [
                    limitSetting,
                    oddAdjust,
                    eSportSBOModel,
                    sboTodayESport
                ] = parallelResult;

                const sboObjLeague = _.chain(eSportSBOModel)
                    .filter(sboLeague => {
                        return _.isEqual(parseInt(sboLeague.k), league_key)
                    })
                    .first()
                    .value();

                if (!_.isUndefined(sboObjLeague)) {

                    let matchDate = new Date();
                    let indexRound;

                    const bp = _.first(_.chain(sboTodayESport)
                        .flatten(true)
                        .findWhere(JSON.parse(`{"k":${league_key}}`))
                        .pick('m')
                        .map(obj => {
                            return obj;
                        })
                        .flatten(true)
                        .findWhere(JSON.parse(`{"id":"${league_key}:${match_key}"}`))
                        .tap(obj => {
                            try {
                                matchDate = obj.d;
                            } catch (e) {
                                console.error(e);
                            }
                        })
                        .pick('bp')
                        .map(obj => {
                            return obj;
                        })
                        .flatten(true)
                        .map(obj => {
                            return obj[prefix]
                        })
                        .filter((obj, index) => {
                            try {
                                const result = _.isEqual(obj[key], value);
                                if (result) {
                                    indexRound = index;
                                }
                                return result;
                            } catch (e) {
                                return false;
                            }
                        })
                        .map(obj => {
                            const bpObj = {};
                            bpObj[prefix] = obj;
                            return bpObj
                        })
                        .value());

                    if( _.isUndefined(bp)){
                        return res.send({
                            message: "resultBP is undefined ",
                            code: 1007
                        });
                    }

                    let odd = _.filter(oddAdjust, obj => {
                        return _.isEqual(obj.matchId, id);
                    });

                    Commons.switch_type(
                        prefixLowerCase,
                        bp,
                        Commons.rulePriceHDP(),
                        {},
                        Commons.configMatch(),
                        commissionSetting.sportsBook.typeHdpOuOe,
                        odd,
                        (err, result)=>{
                            if(err){
                                return res.send({message: err, code: 10102});
                            } else {
                                let result_new_array = [result];
                                let betPrice = _.chain(result_new_array)
                                    .flatten(true)
                                    .map(betPrice => {
                                        return _.findWhere(betPrice, predicate);
                                    })
                                    .reject(betPrice => {
                                        return _.isUndefined(betPrice)
                                    })
                                    .map(betPrice => {
                                        return Commons.getBetObjectByKey(prefix, key, betPrice);
                                    })
                                    .first()
                                    .value();

                                Commons.switch_min_max(
                                    prefixLowerCase,
                                    sboObjLeague.r,
                                    betPrice,
                                    (err, dataResult)=>{

                                        dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.hdpOuOe.maxPerBet);

                                        Commons.calculateBetPriceByTime(dataResult.max, now, new Date(matchDate), (err, data)=>{
                                            dataResult.max = data;
                                        });

                                        Commons.calculateBetPrice(dataResult.max, indexRound, (err, data)=>{
                                            dataResult.max = data;
                                        });

                                        return res.send({
                                            message: "success",
                                            result: dataResult,
                                            code: 0
                                        });
                                    })
                            }
                        });

                } else {
                    httpRequest.get(
                        `http://lb-sbo-master.api-hub.com/muaythai/external/sboLeague/${league_key}`,
                        (error, result) => {
                            if (error) {
                                console.error(error);
                            } else {
                                const model = JSON.parse(result.body).result;
                                ESportSBOModel.findOne({
                                    'n.en': model.n.en
                                })
                                    .lean()
                                    .exec((err, data) => {
                                        if (err) {
                                            console.log(err);
                                        }  else if (!data) {
                                            const eSportSBOModel = new ESportSBOModel(model);
                                            eSportSBOModel.save((error, data) => {
                                                if (error) {
                                                    console.error(error);
                                                } else {
                                                    console.log(data);
                                                }
                                            });
                                        } else {
                                            data.k = model.k;
                                            delete data._id;
                                            const eSportSBOModel = new ESportSBOModel(data);
                                            eSportSBOModel.save((error, data) => {
                                                if (error) {
                                                    console.error(error);
                                                } else {
                                                    console.log(data);
                                                }
                                            });
                                        }
                                    });
                            }
                        });
                    return res.send({
                        message: `sboObjLeague not found ${league_key}`,
                        code: 1007
                    });
                }
            }
    });
});

const _currentDate = () => {
    return new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
};
module.exports = router;
