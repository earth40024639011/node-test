const express = require('express');
const router  = express.Router();
const _       = require('underscore');

const VolleyballSBOModel = require('../../../../models/volleyballSBO.model');
const Redis               = require('../../1_common/1_redis/1_1_redis');
const CLIENT_NAME         = process.env.CLIENT_NAME          || 'SPORTBOOK88';
const LOG = 'SETTING_MIN-MAX';
router.put('/:leagueId/:sk/today', (req, res) => {
    const {
        leagueId,
        sk
    } = req.params;
    const {
        r
    } = req.body;
    Redis.deleteByKey(`VolleyballSBOModel${CLIENT_NAME}`, (error, result) => {

    });
    VolleyballSBOModel.update({
            k: parseInt(leagueId)
        },
        {
            $set: {
                'r': r,
                'sk': sk
            }
        },
        {
            multi: false
        },
        (err, data) => {
            if (err) {
                console.log(`[${LOG}] Error update minx-max league today : ${leagueId} ${r}`);
                console.log(err);
                return res.send({
                    message: "error",
                    code: 999,
                    result : err
                });
            } else if (data.nModified === 0) {
                console.log(`[${LOG}] Failed update minx-max league today : ${leagueId} ${r}`);
                return res.send({
                    message: "failed",
                    code: 4,
                    result : data
                });
            } else {
                console.log(`[${LOG}] Success update minx-max league today : ${leagueId} ${r}`);
                return res.send({
                    message: "success",
                    code: 0,
                    result : _.first(data)
                });
            }
        });
});
module.exports = router;