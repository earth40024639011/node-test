const express = require('express');
const router  = express.Router();
const _       = require('underscore');

const SpacialOddsModel   = require('../../../../models/spacialOdds.model');
const Redis              = require('../../1_common/1_redis/1_1_redis');
const CLIENT_NAME        = process.env.CLIENT_NAME          || 'SPORTBOOK88';
const LOG = 'SETTING_SBO_LEAGUE';
router.post('/', (req, res) => {
    console.log(`[${LOG}] ${JSON.stringify(req.body)}`);
    let {
        k,
        n
    } = req.body;
    k = 999999999;
    Redis.deleteByKey(`SpacialOddsModel${CLIENT_NAME}`, (error, result) => {

    });
    const result =[
        {
            "k": 999999999,
            "nn": n,
            "m": [
            ],
            "ms": 1
        }
    ]
    Redis.setByKey(`spacialOdds${CLIENT_NAME}`, result, (error, result) => {

    });

    const model = new SpacialOddsModel();
    model.k = k;
    model.n = n;

    model.save((err, data) => {
        if (err) {
            return res.send({
                code: 0,
                message: "error",
                data: err
            });
        } else {
            return res.send({
                code: 0,
                message: "success"
            });
        }
    });
});

router.put('/', (req, res) => {
    console.log(`[${LOG}] ${JSON.stringify(req.body)}`);
    let {
        k,
        n
    } = req.body;
    k = 999999999;
    Redis.getByKey(`spacialOdds${CLIENT_NAME}`, (error, result) => {
        if (error) {
            console.error(`spacialOdds${CLIENT_NAME}`);
            console.error(error);
            return res.send({
                code: 0,
                message: "error",
                data: error
            });
        } else {
            _.chain(result)
                .each(league => {
                    console.log(league);
                    if (_.isEqual(league.k, k)) {
                        league.nn = n;
                    }
                })
                .value();
            console.log(result);
            Redis.setByKey(`spacialOdds${CLIENT_NAME}`, result, (error, result) => {

            });
            return res.send({
                code: 0,
                message: "success"
            });
        }
    });
});

//[3]
router.get('/', (req, res) => {
    SpacialOddsModel.find({})
        .lean()
        .sort('sk')
        .select('sk k n r')
        .exec((error, response) => {
            if (error) {
                return res.send({
                    code: 0,
                    message: "error",
                    result: error
                });
            } else {
                return res.send({
                    code: 0,
                    message: `success`,
                    result: _.sortBy(response, 'sk')
                });
            }
        });
});
module.exports = router;