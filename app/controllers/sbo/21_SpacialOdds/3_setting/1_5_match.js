const express = require('express');
const router  = express.Router();
const _       = require('underscore');
const md5     = require('md5');

const SpacialOddsModel   = require('../../../../models/spacialOdds.model');
const SpacialOddsActionModel = require('../../../../models/spacialOddsActionLog.model');
const Redis              = require('../../1_common/1_redis/1_1_redis');
const CLIENT_NAME        = process.env.CLIENT_NAME          || 'SPORTBOOK88';
const LOG = 'SETTING_SBO_MATCH';
router.get('/', (req, res) => {
    Redis.getByKey(`spacialOdds${CLIENT_NAME}`, (error, result) => {
        if (error) {
            console.error(`spacialOdds${CLIENT_NAME}`);
            console.error(error);
            return res.send({
                code: 0,
                message: "error",
                data: error
            });
        } else {
            return res.send({
                code: 0,
                message: "success",
                result
            });
        }
    });
});

router.post('/', (req, res) => {
    console.log(`[${LOG}] ${JSON.stringify(req.body)}`);
    const {
        d,
        n,
        bp
    } = req.body;

    let h= "0";
    let a= "0";
    let hpk = "h0";
    let apk = "a0";
    const oddsBase = bp.odds.oddsBase;
    if (!_.isEqual(oddsBase, "0")) {
        if (_.isEqual(bp.odds.give, "h")) {
            hpk = `h-${oddsBase}`;
            h = `-${oddsBase}`;

            apk = `a${oddsBase}`;
            a = `${oddsBase}`;
        } else {
            apk = `a-${oddsBase}`;
            a = `-${oddsBase}`;
            hpk = `h${oddsBase}`;
            h = `${oddsBase}`;
        }
    }
    Redis.getByKey(`spacialOdds${CLIENT_NAME}`, (error, result) => {
        if (error) {
            console.error(`spacialOdds${CLIENT_NAME}`);
            console.error(error);
            return res.send({
                code: 0,
                message: "error",
                data: error
            });
        } else {
            _.chain(result)
                .each(league => {
                    console.log(league);
                    if (_.isEqual(league.k, 999999999)) {
                        const k = _.random(10000000, 99999999);
                        const m = {
                            id: `999999999:${k}`,
                            isOn: true,
                            k: k,
                            hasParlay: false,
                            d: d,
                            n: n,
                            md5: md5(`${league.nn.en}${n.en.h.toUpperCase()}${n.en.a.toUpperCase()}`),
                            md5d: md5(`${league.nn.en}${n.en.h.toUpperCase()}${n.en.a.toUpperCase()}`),
                            st: '',
                            i: {
                                "lt": "Live",
                                "mt": "",
                                "h": "0",
                                "a": "0",
                                "s": "",
                                "hrc": "",
                                "arc": ""
                            },
                            moreSize: 0,
                            bp: [
                                {
                                    "ah": {
                                        h,
                                        "hk": "hk",
                                        hpk,
                                        "hp": `${bp.hp}`,
                                        a,
                                        "ak": "ak",
                                        apk,
                                        "ap": `${bp.ap}`
                                    }
                                }
                            ]
                        };
                        league.m.push(m);
                    }
                })
                .value();
            // console.log(result);
            Redis.setByKey(`spacialOdds${CLIENT_NAME}`, result, (error, result) => {

            });
            return res.send({
                code: 0,
                message: "success",
                result
            });
        }
    });
});

router.put('/', (req, res) => {
    console.log(`[${LOG}] ${JSON.stringify(req.body)}`);
    const {
        id,
        isOn,
        d,
        n,
        bp
    } = req.body;
    let h= "0";
    let a= "0";
    let hpk = "h0";
    let apk = "a0";
    const oddsBase = bp.odds.oddsBase;
    if (!_.isEqual(oddsBase, "0")) {
        if (_.isEqual(bp.odds.give, "h")) {
            hpk = `h-${oddsBase}`;
            h = `-${oddsBase}`;
            apk = `a${oddsBase}`;
            a = `${oddsBase}`;
        } else {
            apk = `a-${oddsBase}`;
            a = `-${oddsBase}`;
            hpk = `h${oddsBase}`;
            h = `${oddsBase}`;
        }
    }
    Redis.getByKey(`spacialOdds${CLIENT_NAME}`, (error, result) => {
        if (error) {
            console.error(`spacialOdds${CLIENT_NAME}`);
            console.error(error);
            return res.send({
                code: 0,
                message: "error",
                data: error
            });
        } else {
            _.chain(result)
                .each(league => {
                    console.log(league);
                    if (_.isEqual(league.k, 999999999)) {
                        _.each(league.m, match => {
                            if (_.isEqual(id, match.id)) {
                                match.isOn = isOn;
                                match.d = d;
                                match.n = n;
                                _.each(match.bp, betPrice => {
                                    betPrice.ah.h = `${h}`;
                                    betPrice.ah.hp = `${bp.hp}`;
                                    betPrice.ah.hpk = `${hpk}`;

                                    betPrice.ah.a = `${a}`;
                                    betPrice.ah.ap = `${bp.ap}`;
                                    betPrice.ah.apk = `${apk}`;
                                });
                            }
                        });
                    }
                })
                .value();
            Redis.setByKey(`spacialOdds${CLIENT_NAME}`, result, (error, result) => {

            });
            return res.send({
                code: 0,
                message: "success",
                result
            });
        }
    });
});

router.delete('/', (req, res) => {
    console.log(`[${LOG}] ${JSON.stringify(req.body)}`);
    const {
        id
    } = req.body;
    Redis.getByKey(`spacialOdds${CLIENT_NAME}`, (error, result) => {
        if (error) {
            console.error(`spacialOdds${CLIENT_NAME}`);
            console.error(error);
            return res.send({
                code: 0,
                message: "error",
                data: error
            });
        } else {
            let index = -1;
            _.chain(result)
                .each(league => {
                    if (_.isEqual(league.k, 999999999)) {
                        _.each(league.m, (match, round) => {
                            if (_.isEqual(id, match.id)) {
                                index = round;
                            }
                        });
                        if (!_.isEqual(index, -1)) {
                            console.log(index);
                            league.m.pop(index);
                        }
                    }
                })
                .value();
            if (!_.isEqual(index, -1)) {
                Redis.setByKey(`spacialOdds${CLIENT_NAME}`, result, (error, result) => {

                });
                return res.send({
                    code: 0,
                    message: "success",
                    result
                });
            } else {
                return res.send({
                    code: 9001,
                    message: `${id} not found.`
                });
            }
        }
    });
});


function actionLog(actionBy, action, reqBodyObj, resBodyObj, previousObj, currentObj, callback) {
    const model = new SpacialOddsActionModel();
    model.actionBy = actionBy;
    model.action = action;
    model.reqBodyObj = reqBodyObj;
    model.resBodyObj = resBodyObj;
    model.previousObj = previousObj;
    model.currentObj = currentObj;
    model.save((err, data) => {
        console.error(err);
        console.log(data);
    });
}
module.exports = router;