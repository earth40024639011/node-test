const express       = require('express');
const router        = express.Router();
const Redis         = require('../1_common/1_redis/1_1_redis');
const LOG           = 'SPORT_DARTS_RESULT';
const _             = require('underscore');
const CLIENT_NAME   = process.env.CLIENT_NAME || 'SPORTBOOK88';
const SpacialOddsModel    = require('../../../models/spacialOddsResult.model');

router.get('/today', (req, res) => {
    Redis.getByKey(`spacialOdds${CLIENT_NAME}`, (error, todaySpacialOdds) => {
        if (error) {
            return res.send(
                {
                    code: 0,
                    message: "Error",
                    result: error
                });
        } else {
            SpacialOddsModel.find({})
                .lean()
                .exec((error, response) => {
                    if (error) {
                        return res.send(
                            {
                                code: 0,
                                message: "Error",
                                result: error
                            });
                    } else {
                        _.chain(todaySpacialOdds)
                            .each(league => {
                                league.m = response;
                            })
                            .value();
                        return res.send(
                            {
                                code: 0,
                                message: "success",
                                result: todaySpacialOdds
                            });
                    }
                });
        }
    });
});
module.exports = router;
