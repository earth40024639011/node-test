const express     = require('express');
const router      = express.Router();
const _           = require('underscore');
const parallel    = require("async/parallel");
const Redis       = require('../../1_common/1_redis/1_1_redis');

const SpacialOddsModel = require('../../../../models/spacialOdds.model');
const MemberService    = require('../../../../common/memberService');
const Commons          = require('../../../../common/commons');
const RoundTo          = require('round-to');
const LOG              = 'MATCH_SPACIAL_ODDS_TODAY';
const CLIENT_NAME      = process.env.CLIENT_NAME || 'SPORTBOOK88';

router.get('/', (req, res) => {
    const now_date = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const {
        _id,
        username,
        currency
    } = req.userInfo;

    parallel([
            (callback) => {
                MemberService.getLimitSetting(_id, (errLimitSetting, dataLimitSetting)=>{
                    if(errLimitSetting){
                        callback(errLimitSetting, null);
                    } else {
                        callback(null, dataLimitSetting.limitSetting);
                    }
                });
            },

            (callback) => {
                MemberService.getOddAdjustment(username, (err, data) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, data);
                    }
                });
            },

            (callback) => {
                Redis.getByKey(`spacialOdds${CLIENT_NAME}`, (error, result) => {
                    if (error) {
                        callback(null, false);
                    } else {
                        callback(null, result);
                    }
                })
            },

            (callback) => {
                Redis.getByKey(`SpacialOddsModel${CLIENT_NAME}`, (error, result) => {
                    if (error || _.isNull(result) || _.isUndefined(result)) {
                        SpacialOddsModel.find({})
                            .lean()
                            .sort('sk')
                            .select('k n r')
                            .exec((error, response) => {
                                if (error) {
                                    callback(error, null);
                                } else {
                                    Redis.setByKey(`SpacialOddsModel${CLIENT_NAME}`, response, (error, result) => {

                                    });
                                    callback(null, response);
                                }
                            });
                    } else {
                        callback(null, result);
                    }
                });
            }
        ],
        (error, parallelResult) => {
            if (error) {
                return res.send({
                    code    : 9999,
                    message : error
                });
            } else {
                let [
                    limitSetting,
                    oddAdjust,
                    todaySpacialOdds,
                    spacialOddsModel
                ] = parallelResult;
                console.log(parallelResult);
                const configMatch = Commons.configMatch();
                if (!_.isEmpty(todaySpacialOdds)) {
                    _.chain(todaySpacialOdds)
                        .each(league => {
                            const sboObjLeague = _.chain(spacialOddsModel)
                                .filter(sboLeague => {
                                    return _.isEqual(parseInt(sboLeague.k), league.k)
                                })
                                .first()
                                .value();

                            if (!_.isUndefined(sboObjLeague)) {
                                league.m = _.filter(league.m, match => {
                                    return match.isOn;
                                });
                                // league.m = _.filter(league.m, match => {
                                //     return new Date(match.d) >= now_date;
                                // });

                                sboObjLeague.rp = {};
                                sboObjLeague.rp.hdp = {};
                                sboObjLeague.rp.hdp = Commons.rulePriceHDP();

                                _.each(league.m, match => {
                                    const matchDate = new Date(match.d);

                                    _.each(match.bp, betPrice => {
                                        const odd = _.filter(oddAdjust, obj =>{
                                            return _.isEqual(obj.matchId, match.id);
                                        });

                                        let ah = {home: 0, away: 0};

                                        if(!_.isEmpty(odd)){
                                            _.each(odd, o => {
                                                const {
                                                    prefix,
                                                    key,
                                                    value,
                                                    count
                                                } = o;
                                                if(_.isEqual(prefix, 'ah') && !_.isUndefined(betPrice.ah)) {
                                                    // console.log(betPrice.ah.hpk);
                                                    if(_.isEqual(key, 'hpk') && _.isEqual(value, betPrice.ah.hpk)){
                                                        ah = {
                                                            home: -count,
                                                            away: count
                                                        };
                                                        console.log(`       hpk ah => ${JSON.stringify(ah)}`);
                                                    } else if(_.isEqual(key, 'apk') && _.isEqual(value, betPrice.ah.apk)){
                                                        ah = {
                                                            home: count,
                                                            away: -count
                                                        };
                                                        // console.log(`       apk ah => ${JSON.stringify(ah)}`);
                                                    }

                                                }
                                            });
                                        }

                                        // ---------------------------  ah  ----------------------------------
                                        if(betPrice.ah){
                                            if(betPrice.ah.h || betPrice.ah.a){
                                                Commons.cal_ah(betPrice.ah.h, betPrice.ah.a, betPrice.ah.hp, betPrice.ah.ap, sboObjLeague.rp.hdp.ah.f, sboObjLeague.rp.hdp.ah.u, configMatch.ah, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ah, (err, data)=>{
                                                    if(data){
                                                        betPrice.ah.hp = data.hp;
                                                        betPrice.ah.ap = data.ap;
                                                    }
                                                });
                                            }
                                        }
                                    });

                                    const {
                                        ah
                                    } = sboObjLeague.r;
                                    _.each(match.bp, (betPrice, round) => {
                                        betPrice.r = {};
                                        betPrice.r.ah = {};
                                        betPrice.r.ah.ma = calculateBetPriceByTime(calculateBetPrice(ah.ma, round), now_date, matchDate);
                                        betPrice.r.ah.mi = ah.mi;
                                    });
                                    delete match.isOn;
                                });
                            } else {
                                Redis.deleteByKey(`SpacialOddsModel${CLIENT_NAME}`, (error, result) => {

                                });
                                const model = {};
                                model.k = 999999999;
                                model.n = {
                                    en : "Spacial Odds",
                                    th : "ราคาพิเศษ",
                                    cn : "Spacial Odds",
                                    tw : "Spacial Odds",
                                };
                                const spacialOddsModel = new SpacialOddsModel(model);
                                spacialOddsModel.save((error, data) => {
                                    if (error) {
                                        console.error(error);
                                    } else {
                                        console.log(data);
                                    }
                                });
                                console.error(`sboObjLeague not found ${league.k}`);
                            }
                        })
                        .value();

                    todaySpacialOdds = _.reject(todaySpacialOdds, league => {
                        return _.size(league.m) === 0;
                    });

                    // console.log(todaySpacialOdds);
                    return res.send({
                        message: "success",
                        result: todaySpacialOdds,
                        code: 0
                    });

                } else {
                    return res.send({
                        message: "success",
                        result: [],
                        code: 0
                    });
                }
            }
    });
});

const calculateBetPrice = (price, round) => {
    let percent = 0;
    switch (round) {
        case 0:
            percent = 0;
            break;
        case 1:
            percent = 40;
            break;
        case 2:
            percent = 80;
            break;
        case 3:
            percent = 90;
            break;
        case 4:
            percent = 90;
            break;
        case 5:
            percent = 90;
            break;
    }
    return decrease(price, percent);
};

const calculateBetPriceByTime = (price, current, match) => {
    if (price <= 20000) {
        return price;
    } else {
        let diff =(current.getTime() - match.getTime()) / 1000;
        diff /= (60 * 60);

        const gapHour = Math.abs(Math.round(diff));
        let percent = 0;

        if (12 <= gapHour) {
            percent = 70;
        } else if (11 === gapHour || 10 === gapHour || 9 === gapHour || 8 === gapHour || 7 === gapHour) {
            percent = 65;
        } else if (6 === gapHour || 5 === gapHour || 4 === gapHour ) {
            percent = 50;
        } else if (3 === gapHour || 2 === gapHour ) {
            percent = 35;
        } else {
            percent = 0;
        }

        return RoundTo(parseFloat(decrease(price, percent)), 0);
    }
};

const decrease = (price, percent) => {
    return (price * ( (100 - percent) / 100)).toFixed(2);
};

module.exports = router;
