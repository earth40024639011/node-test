const express = require('express');
const router  = express.Router();
const _       = require('underscore');

const MuaythaiSBOModel = require('../../../../models/muaythaiSBO.model');
const CLIENT_NAME      = process.env.CLIENT_NAME          || 'SPORTBOOK88';
const Redis            = require('../../1_common/1_redis/1_1_redis');
const LOG = 'SETTING_SBO_LEAGUE';
router.post('/', (req, res) => {
    console.log(`[${LOG}] ${JSON.stringify(req.body)}`);

    const {
        k,
        n
    } = req.body;

    const muaythaiSBOModel = new MuaythaiSBOModel();
    muaythaiSBOModel.k = k;
    muaythaiSBOModel.n = n;

    muaythaiSBOModel.save((err, data) => {
        if (err) {
            return res.send({
                code: 0,
                message: "error",
                data: err
            });
        } else {
            Redis.deleteByKey(`MuaythaiSBOModel${CLIENT_NAME}`, (error, result) => {

            });
            return res.send({
                code: 0,
                message: "success"
            });
        }
    });
});


//[3]
router.get('/', (req, res) => {
    MuaythaiSBOModel.find({})
        .lean()
        .sort('sk')
        .select('sk k n r')
        .exec((error, response) => {
            if (error) {
                return res.send({
                    code: 0,
                    message: "error",
                    result: error
                });
            } else {
                Redis.deleteByKey(`MuaythaiSBOModel${CLIENT_NAME}`, (error, result) => {

                });
                return res.send({
                    code: 0,
                    message: `success`,
                    result: _.sortBy(response, 'sk')
                });
            }
        });
});
module.exports = router;