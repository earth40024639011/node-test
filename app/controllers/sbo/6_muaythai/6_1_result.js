const express   = require('express');
const router    = express.Router();
const Redis     = require('../1_common/1_redis/1_1_redis');
const LOG       = 'SPORT_MUAYTHAI_RESULT';

router.get('/previous', (req, res) => {
    Redis.getByKey('resultMuayThaiPreviousAMBFM', (error, response) => {
        if (error) {
            return res.send(
                {
                    code: 0,
                    message: "Error",
                    result: error
                });
        } else {
            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: response
                });
        }
    });
});
router.get('/today', (req, res) => {
    Redis.getByKey('resultMuayThaiTodayAMBFM', (error, response) => {
        if (error) {
            return res.send(
                {
                    code: 0,
                    message: "Error",
                    result: error
                });
        } else {
            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: response
                });
        }
    });
});
module.exports = router;
