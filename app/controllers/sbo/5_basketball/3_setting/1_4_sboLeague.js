const express = require('express');
const router  = express.Router();
const _       = require('underscore');

const BasketballSBOModel = require('../../../../models/basketballSBO.model');
const CLIENT_NAME      = process.env.CLIENT_NAME          || 'SPORTBOOK88';
const Redis            = require('../../1_common/1_redis/1_1_redis');
const LOG = 'SETTING_SBO_LEAGUE';
router.post('/', (req, res) => {
    console.log(`[${LOG}] ${JSON.stringify(req.body)}`);

    const {
        k,
        n
    } = req.body;

    const basketballSBOModel = new BasketballSBOModel();
    basketballSBOModel.k = k;
    basketballSBOModel.n = n;

    basketballSBOModel.save((err, data) => {
        if (err) {
            return res.send({
                code: 0,
                message: "error",
                data: err
            });
        } else {
            Redis.deleteByKey(`BasketballSBOModel${CLIENT_NAME}`, (error, result) => {

            });
            return res.send({
                code: 0,
                message: "success"
            });
        }
    });
});


//[3]
router.get('/', (req, res) => {
    BasketballSBOModel.find({})
        .lean()
        .sort('sk')
        .select('sk k n r')
        .exec((error, response) => {
            if (error) {
                return res.send({
                    code: 0,
                    message: "error",
                    result: error
                });
            } else {
                Redis.deleteByKey(`BasketballSBOModel${CLIENT_NAME}`, (error, result) => {

                });
                return res.send({
                    code: 0,
                    message: `success`,
                    result: _.sortBy(response, 'sk')
                });
            }
        });
});
module.exports = router;