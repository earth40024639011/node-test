const express   = require('express');
const router    = express.Router();
const async     = require("async");
const Joi       = require('joi');
const mongoose  = require('mongoose');
const _         = require('underscore');
const roundTo   = require('round-to');
const md5       = require('md5');

const MemberModel                   = require('../../../../models/member.model');
const BetTransactionModel           = require('../../../../models/betTransaction.model.js');
const BetLiveTransactionModel       = require('../../../../models/betLiveTransaction.model.js');
const RobotTransactionModel         = require('../../../../models/robotTransaction.model.js');
const AgentGroupModel               = require('../../../../models/agentGroup.model.js');
const SeqBettingTransactionModel    = require('../../../../models/seqBettingTransaction.model.js');
const Commons                       = require('../../../../common/commons');
const _Utils                        = require('../../../../common/underscoreUtils');
const FormulaUtils                  = require('../../../../common/formula');
const MemberService                 = require('../../../../common/memberService');
const AgentService                  = require("../../../../common/agentService");
const DateUtils                     = require('../../../../common/dateUtils');
const naturalSort                   = require("javascript-natural-sort");
const ApiService                    = require('../../../../common/apiService');
const RedisService                  = require('../../../../common/redisService');
const Utility                       = require('../../../../common/utlity');
const moment                        = require('moment');

const clientWhiteList   = ['AMB_SPORTBOOK'];
const CLIENT_NAME       = process.env.CLIENT_NAME || 'SPORTBOOK88';
const parallel          = require("async/parallel");

function generateBetId() {
    return 'BET' + new Date().getTime();
}

function calculateParlayPayout(amount, odd, callback) {
    let maxPayout = 2000000.1;
    let max = roundTo(maxPayout / odd, 2);
    if (amount > max) {
        callback(5012, null);
    } else {
        callback(null, '');
        // callback((amount * odd).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]);
    }
}

const addBetSchema = Joi.object().keys({
    memberId: Joi.string().required(),
    matchId: Joi.string().required(),
    odd: Joi.number().required(),
    oddKey: Joi.string().required(),
    oddType: Joi.string().required(),
    handicap: Joi.string().allow(''),
    bet: Joi.string().allow(''),
    gameType: Joi.string().allow(''),
    matchType: Joi.string().required(),
    acceptAnyOdd: Joi.boolean().required(),
    homeScore: Joi.number().required(),
    awayScore: Joi.number().required(),
    priceType: Joi.string().required(),
    amount: Joi.number().required(),
    payout: Joi.number().required(),
    allKey: Joi.array(),
    ip: Joi.string().allow(''),
    source: Joi.string().required(),
    key: Joi.string().required(),
    type: Joi.string().allow(''),
    isSpecial: Joi.boolean(),
    specialType:Joi.string(),
    requestId:Joi.string().allow('')
});
router.post('/hdp', function (req, res) {

    let validate = Joi.validate(req.body, addBetSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    const source = req.body.source;
    const userInfo = req.userInfo;
    const membercurrency = userInfo.currency;
    let isOfflineCash = false;
    async.waterfall([
            (callback) => {
                parallel([
                        (callback) => {
                            if (_.contains(clientWhiteList, CLIENT_NAME)) {
                                MemberService.getIsUserBlackList(userInfo.username, (error, isUserBlackList) => {
                                    if (error) {
                                        callback(error, null);
                                    } else {
                                        callback(null, isUserBlackList);
                                    }
                                });
                            } else {
                                callback(null, false);
                            }
                        },

                        (callback) => {
                            if (_.contains(clientWhiteList, CLIENT_NAME)) {
                                MemberService.getIsUserBlackList2(userInfo.username, (error, isUserBlackList) => {
                                    if (error) {
                                        callback(error, null);
                                    } else {
                                        callback(null, isUserBlackList);
                                    }
                                });
                            } else {
                                callback(null, false);
                            }
                        },

                        (callback) => {
                            if (_.isEqual(CLIENT_NAME, 'AMB_SPORTBOOK') ||
                                _.isEqual(CLIENT_NAME, 'IRON') ||
                                _.isEqual(CLIENT_NAME, 'FASTBET98') ||
                                _.isEqual(CLIENT_NAME, 'FAST98')||
                                _.isEqual(CLIENT_NAME, 'AFC1688') ||
                                _.isEqual(CLIENT_NAME, 'SPORTBOOK88') ||
                                _.isEqual(CLIENT_NAME, 'CSR') ||
                                _.isEqual(CLIENT_NAME, 'MGM')) {
                                AgentService.getOfflineCashStatus(userInfo.username, (error, isOfflineCash) => {
                                    if (error) {
                                        callback(error, false);
                                    } else {
                                        callback(null, isOfflineCash);
                                    }
                                });
                            } else {
                                callback(null, false);
                            }
                        }
                    ],
                    (error, parallelResult) => {
                        if (error) {
                            callback(null, 0);
                        } else {
                            const [
                                isUserBlackList,
                                isUserBlackList2,
                                isOfflineCashResult
                            ] = parallelResult;
                            isOfflineCash = isOfflineCashResult;
                            if (_.contains(clientWhiteList, CLIENT_NAME) && (isUserBlackList || isUserBlackList2) && req.body.matchType !== 'LIVE') {
                                const time_out = [0, 350, 650, 950, 1250, 1550, 1850, 2150, 2450, 2750];
                                const seq = SeqBettingTransactionModel();
                                seq.username = req.userInfo.username;

                                seq.save((error, data) => {
                                    if (error) {
                                        console.log(error);
                                        const random = _.random(0, 7);
                                        const sec = req.body.matchType === 'LIVE' ? 0 : time_out[random];
                                        setTimeout(() => {
                                            callback(null, sec);
                                        }, sec);
                                    } else {
                                        let sec = 0;
                                        console.log(' ####============> ', req.body.matchType);
                                        // if (req.body.matchType !== 'LIVE') {
                                        const transactionLast = _.chain(`${data.transactionId}`)
                                            .flatten()
                                            .last()
                                            .value();
                                        console.log(' transactionLast[' + transactionLast + '] ####============> ', (transactionLast % 3));

                                        if (isUserBlackList) {
                                            sec = (parseInt(transactionLast) % 3) * 5000;
                                            console.log(' isUserBlackList sec ####============> ', sec);
                                            setTimeout(() => {
                                                callback(null, sec);
                                            }, sec);
                                        } else if (isUserBlackList2) {
                                            sec = (parseInt(transactionLast) % 5) * 3000;
                                            console.log(' isUserBlackList2 sec ####============> ', sec);
                                            setTimeout(() => {
                                                callback(null, sec);
                                            }, sec);
                                        } else {
                                            callback(null, 0);
                                        }
                                        // } else {
                                        //     console.log(' ELSE ####============> ', sec);
                                        //     callback(null, sec);
                                        // }
                                    }
                                });
                            } else {
                                callback(null, 0);
                            }
                        }
                    });
            }
        ],
        (error, response) => {
            async.series([
                    (callback) => {
                        MemberService.getOddAdjustment(userInfo.username, (err, result) => {
                            if (err) {
                                callback(error, null);
                            } else {
                                callback(null, result);
                            }
                        });
                    },

                    (callback) => {
                        MemberService.getLimitSetting(userInfo._id, (err, result) => {
                            if (err) {
                                callback(error, null);
                            } else {
                                callback(null, result.limitSetting);
                            }
                        });
                    },

                    (callback) => {
                        RedisService.getCurrency((error, result) => {
                            if (error) {
                                callback(null, false);
                            } else {
                                callback(null, result);
                            }
                        })
                    }
                ],
                (error, resultOddAndLimit) => {
                    if (error) {
                        return res.send({
                            code: 9999,
                            message: error
                        });
                    } else {

                        const [
                            oddAdjust,
                            limitSetting,
                            currency
                        ] = resultOddAndLimit;

                        async.series([callback => {

                            //TODO: duplicate find member
                            MemberModel.findById(req.body.memberId, (err, response) => {
                                if (err) {
                                    callback(err, null);
                                    return;
                                }

                                if (response.suspend) {
                                    callback(5011, null);
                                } else if(!response.limitSetting.sportsBook.isEnable){
                                    callback(5013,null);
                                } else {
                                    AgentService.getUpLineStatus(userInfo.group._id, (err, status) => {
                                        if (err) {
                                            callback(err, null);
                                        } else {
                                            if (status.suspend) {
                                                callback(5011, null);
                                            } else if(!status.sportBookEnable){
                                                callback(5013,null);
                                            }   else {
                                                callback(null, status);
                                            }
                                        }
                                    });
                                }
                            });


                        }, callback => {

                            // if (userInfo.group.type === 'API') {
                            //     callback(null, 0);
                            // } else {



                            MemberService.getCredit(userInfo._id, (err, credit) => {
                                if (err) {
                                    callback(err, null);
                                } else {

                                    if (typeof credit !== 'number' || _.isUndefined(credit) || _.isNaN(credit)) {
                                        callback(5005, null);
                                    }else {

                                        if (credit < roundTo(req.body.amount,2)) {
                                            callback(5005, null);
                                        } else {
                                            callback(null, credit);
                                        }
                                    }
                                }
                            });
                            // }

                        }, callback => {

                            FormulaUtils.calculatePayout(req.body.amount, req.body.odd, req.body.oddType, req.body.priceType.toUpperCase(), (payout) => {
                                // if (req.body.payout != payout) {
                                //     callback(5004, null);
                                // } else {
                                callback(null, payout);
                                // }
                            });

                        }], (err, results) => {

                            if (err) {
                                if (err === 5004) {
                                    return res.send({
                                        message: "payout not matched.",
                                        result: null,
                                        code: 5004
                                    });
                                }
                                if (err === 5005) {
                                    return res.send({
                                        message: "credit exceed.",
                                        result: null,
                                        code: 5005
                                    });
                                }
                                if (err === 5011) {
                                    return res.send({
                                        message: "Account or Upline was suspend.",
                                        result: null,
                                        code: 5011
                                    });
                                }
                                if (err === 5013) {
                                    return res.send({message: "Service Lock.", result: null, code: 5013});
                                }
                                return res.send({message: "error", result: err, code: 999});
                            }

                            async.waterfall([callback => {

                                MemberModel.findById(req.body.memberId, (err, response) => {
                                    if (err)
                                        callback(err, null);

                                    if (response) {

                                        let maxBet = findMaxBet(response, req.body.oddType);

                                        if (req.body.amount > maxBet) {
                                            callback(5001, null);
                                        } else {
                                            callback(null, response);

                                        }
                                    }
                                });

                            }, (memberInfo, callback) => {


                                const oddType = req.body.oddType.toUpperCase();

                                if (oddType === 'AH' || oddType === 'AH1ST' || oddType === 'OU' || oddType === 'OU1ST' || oddType === 'OE' || oddType === 'OE1ST') {


                                    let condition = {
                                        memberId: mongoose.Types.ObjectId(req.body.memberId),
                                        'game':'FOOTBALL',
                                        'hdp.matchId': req.body.matchId
                                    };
                                    condition['$or'] = [{"status": 'RUNNING'}, {"status": 'WAITING'}, {"status": 'DONE'}];
                                    BetTransactionModel.aggregate([
                                        {
                                            $match: condition
                                        },
                                        {
                                            $project: {_id: 0}
                                        },
                                        {
                                            "$group": {
                                                "_id": {
                                                    id: '$_id'
                                                },
                                                "amount": {$sum: '$amount'}
                                            }
                                        }
                                    ], function (err, results) {

                                        if (err) {
                                            callback(err, '');
                                            return;
                                        }

                                        let totalBet = results[0] ? results[0].amount : 0;

                                        if ((totalBet + Number.parseFloat(req.body.amount)) > convertMinMaxByCurrency(memberInfo.limitSetting.sportsBook.hdpOuOe.maxPerMatch, currency, membercurrency)) {
                                            callback(5002, null);
                                        } else {
                                            callback(null, memberInfo, totalBet);
                                        }
                                    });
                                } else {
                                    callback(null, memberInfo, 0);
                                }

                            }, (memberInfo, totalBet, callback) => {

                                AgentGroupModel.findById(userInfo.group._id).select('_id type parentId shareSetting commissionSetting').populate({
                                    path: 'parentId',
                                    select: '_id type parentId shareSetting commissionSetting name',
                                    populate: {
                                        path: 'parentId',
                                        select: '_id type parentId shareSetting commissionSetting name',
                                        populate: {
                                            path: 'parentId',
                                            select: '_id type parentId shareSetting commissionSetting name',
                                            populate: {
                                                path: 'parentId',
                                                select: '_id type parentId shareSetting commissionSetting name',
                                                populate: {
                                                    path: 'parentId',
                                                    select: '_id type parentId shareSetting commissionSetting name',
                                                    populate: {
                                                        path: 'parentId',
                                                        select: '_id type parentId shareSetting commissionSetting name',
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }).exec(function (err, response) {
                                    if (err)
                                        callback(err, null);
                                    else
                                        callback(null, memberInfo, totalBet, response);
                                });

                            }, (memberInfo, totalBet, commission, callback) => {

                                let {
                                    matchId,
                                    oddType,
                                    oddKey,
                                    priceType,
                                    odd,
                                    handicap,
                                    acceptAnyOdd,
                                    matchType,
                                    key,
                                    isSpecial,
                                    specialType
                                } = req.body;

                                isSpecial = _.isUndefined(isSpecial) ? false : isSpecial;

                                const {
                                    _id
                                } = req.userInfo;

                                if (source.toUpperCase() === 'FOOTBALL') {

                                    if (req.body.matchType === 'LIVE') {
                                        if (!isSpecial) {//normal
                                            Commons.cal_bp_live_single_match_sbo(
                                                matchId,
                                                oddType,
                                                oddKey.toLowerCase(),
                                                memberInfo.commissionSetting.sportsBook.typeHdpOuOe,
                                                oddAdjust,
                                                limitSetting,
                                                key,
                                                (err, response) => {

                                                    if (err) {
                                                        callback(err, null);
                                                    } else {
                                                        if (_.isUndefined(response) ||
                                                            _.isUndefined(response.min) ||
                                                            _.isNull(response.min) || req.body.amount < convertMinMaxByCurrency(response.min, currency, membercurrency)) {
                                                            callback(5009);
                                                            return;
                                                        }

                                                        if (_.isUndefined(response) ||
                                                            _.isUndefined(response.max) ||
                                                            _.isNull(response.max) || req.body.amount > convertMinMaxByCurrency(response.max, currency, membercurrency)) {
                                                            callback(5010);
                                                            return;
                                                        }

                                                        if (response.score.h !== req.body.homeScore || response.score.a !== req.body.awayScore) {
                                                            callback(5006);
                                                            return;
                                                        }

                                                        checkOddProcess(
                                                            response,
                                                            priceType,
                                                            oddType,
                                                            oddKey,
                                                            odd,
                                                            handicap,
                                                            (isOddNotMatch, key, oddValue, handicapValue, bet) => {
                                                                let odd = {
                                                                    leagueName: response.league_name,
                                                                    leagueNameN: response.league_name_n,
                                                                    matchId: response.matchId,
                                                                    matchName: response.name,
                                                                    matchDate: response.date,
                                                                    oddType: oddType,
                                                                    odd: oddValue,
                                                                    key: key,
                                                                    oddKey: oddKey,
                                                                    handicap: handicapValue,
                                                                    satang: response.s,
                                                                    max: response.max,
                                                                    isHalfTime: response.isHalfTime,
                                                                    bet: bet,
                                                                    maxPerMatch: response.maxPerMatch,
                                                                    detail: response.detail
                                                                };

                                                                if (isOddNotMatch) {
                                                                    if (req.body.acceptAnyOdd) {
                                                                        callback(null, memberInfo, totalBet, commission, odd);
                                                                    } else {
                                                                        callback(5003, memberInfo, totalBet, commission, odd);
                                                                    }
                                                                } else {
                                                                    callback(null, memberInfo, totalBet, commission, odd);
                                                                }
                                                            });
                                                    }
                                                });
                                        } else {
                                            Commons.cal_bp_live_single_match_sbo_special(
                                                matchId,
                                                oddType,
                                                oddKey.toLowerCase(),
                                                memberInfo.commissionSetting.sportsBook.typeHdpOuOe,
                                                oddAdjust,
                                                limitSetting,
                                                key,
                                                specialType,
                                                (err, response) => {

                                                    if (err) {
                                                        callback(err, null);
                                                    } else {
                                                        if (_.isUndefined(response) ||
                                                            _.isUndefined(response.min) ||
                                                            _.isNull(response.min) || req.body.amount < convertMinMaxByCurrency(response.min, currency, membercurrency)) {
                                                            callback(5009);
                                                            return;
                                                        }

                                                        if (_.isUndefined(response) ||
                                                            _.isUndefined(response.max) ||
                                                            _.isNull(response.max) || req.body.amount > convertMinMaxByCurrency(response.max, currency, membercurrency)) {
                                                            callback(5010);
                                                            return;
                                                        }

                                                        if (response.score.h !== req.body.homeScore || response.score.a !== req.body.awayScore) {
                                                            callback(5006);
                                                            return;
                                                        }

                                                        checkOddProcess(
                                                            response,
                                                            priceType,
                                                            oddType,
                                                            oddKey,
                                                            odd,
                                                            handicap,
                                                            (isOddNotMatch, key, oddValue, handicapValue, bet) => {
                                                                let odd = {
                                                                    leagueName: response.league_name,
                                                                    leagueNameN: response.league_name_n,
                                                                    matchId: response.matchId,
                                                                    matchName: response.name,
                                                                    matchDate: response.date,
                                                                    oddType: oddType,
                                                                    odd: oddValue,
                                                                    key: key,
                                                                    oddKey: oddKey,
                                                                    handicap: handicapValue,
                                                                    satang: response.s,
                                                                    max: response.max,
                                                                    isHalfTime: response.isHalfTime,
                                                                    bet: bet,
                                                                    maxPerMatch: response.maxPerMatch,
                                                                    detail: response.detail
                                                                };

                                                                if (isOddNotMatch) {
                                                                    if (req.body.acceptAnyOdd) {
                                                                        callback(null, memberInfo, totalBet, commission, odd);
                                                                    } else {
                                                                        callback(5003, memberInfo, totalBet, commission, odd);
                                                                    }
                                                                } else {
                                                                    callback(null, memberInfo, totalBet, commission, odd);
                                                                }
                                                            });
                                                    }
                                                });
                                        }
                                    } else {

                                        Commons.cal_bp_hdp_single_match_sbo(
                                            matchId,
                                            oddType,
                                            oddKey.toLowerCase(),
                                            memberInfo.commissionSetting.sportsBook.typeHdpOuOe,
                                            oddAdjust,
                                            _id,
                                            matchType,
                                            key,
                                            (err, response) => {
                                                if (error) {
                                                    callback(error, null);
                                                } else {
                                                    console.log('response ----- :: ', response);
                                                    checkOddProcess(
                                                        response,
                                                        priceType,
                                                        oddType,
                                                        oddKey,
                                                        odd,
                                                        handicap,
                                                        (isOddNotMatch, key, oddValue, handicapValue, bet) => {
                                                            let odd = {
                                                                leagueName: response.league_name,
                                                                leagueNameN: response.league_name_n,
                                                                matchId: response.matchId,
                                                                matchName: response.name,
                                                                matchDate: response.date,
                                                                oddType: oddType,
                                                                odd: oddValue,
                                                                key: key,
                                                                oddKey: oddKey,
                                                                handicap: handicapValue,
                                                                satang: response.s,
                                                                max: response.max,
                                                                bet: bet,
                                                                maxPerMatch: response.maxPerMatch
                                                            };


                                                            if (isOddNotMatch) {
                                                                if (acceptAnyOdd) {
                                                                    callback(null, memberInfo, totalBet, commission, odd);
                                                                } else {
                                                                    callback(5003, memberInfo, totalBet, commission, odd);
                                                                }
                                                            } else {
                                                                callback(null, memberInfo, totalBet, commission, odd);
                                                            }
                                                        });
                                                }
                                            });
                                    }
                                }

                            }], (err, memberInfo, totalBet, agentGroups, hdpInfo) => {


                                if (err) {

                                    if (err === 5001) {
                                        return res.send({
                                            message: "amount exceeded (per bet)",
                                            result: null,
                                            code: 5001
                                        });
                                    }
                                    else if (err === 5002) {
                                        return res.send({
                                            message: "Your total bet has exceeded the maximum bet per match.",
                                            result: null,
                                            code: 5002
                                        });
                                    }
                                    else if (err === 5003) {
                                        return res.send({
                                            message: "odd was already changed.",
                                            result: hdpInfo,
                                            code: 5003
                                        });
                                    }
                                    else if (err === 5006) {
                                        return res.send({
                                            message: "score was already changed.",
                                            result: hdpInfo,
                                            code: 5006
                                        });
                                    }
                                    return res.send({message: "error", result: err, code: 999});
                                }


                                if ((totalBet + Number.parseFloat(req.body.amount)) > convertMinMaxByCurrency(hdpInfo.maxPerMatch, currency, membercurrency)) {
                                    return res.send({
                                        message: "Your total bet has exceeded the maximum bet per match.",
                                        result: null,
                                        code: 5002
                                    });
                                }

                                let memberCredit = roundTo(hdpInfo.odd < 0 ? req.body.amount * hdpInfo.odd : (req.body.amount * -1), 2);
                                let processDate = DateUtils.getCurrentDate();
                                console.log('hdpInfo', JSON.stringify(hdpInfo));

                                let liveTimeBet = '';
                                try {
                                    liveTimeBet = hdpInfo.detail.lt;
                                } catch (e) {

                                }

                                // //TODO
                                // if(req.body.oddType == 'X121ST' || req.body.oddType == 'AH1ST' || req.body.oddType == 'OE1ST' || req.body.oddType == 'OU1ST' ){
                                //
                                //     console.log("------ hdpInfo.mt : ",hdpInfo.detail.mt)
                                //     if(hdpInfo.detail.mt == 'Second Half' || hdpInfo.isHalfTime){
                                //         return res.send({
                                //             message: "odd was already changed.",
                                //             result: hdpInfo,
                                //             code: 5003
                                //         });
                                //     }
                                //
                                // }
                                let random = _.random(1000,9999);

                                let body = {
                                    betId: generateBetId(),
                                    memberId: req.body.memberId,
                                    memberParentGroup: memberInfo.group,
                                    memberUsername: memberInfo.username_lower,
                                    hdp: {
                                        leagueId: req.body.matchId.split(':')[0],
                                        matchId: req.body.matchId,
                                        leagueName: hdpInfo.leagueName,
                                        leagueNameN: hdpInfo.leagueNameN,
                                        matchName: hdpInfo.matchName,
                                        matchDate: hdpInfo.matchDate,
                                        oddKey: hdpInfo.oddKey,
                                        odd: hdpInfo.odd,
                                        oddType: req.body.oddType,
                                        handicap: hdpInfo.handicap,
                                        bet: hdpInfo.bet,
                                        score: req.body.homeScore + ':' + req.body.awayScore,
                                        matchType: _.isEqual(req.body.matchType, 'EARLY') ? 'NONE_LIVE' : req.body.matchType,
                                        rowID: "",//hdpInfo.detail.rowID,
                                        liveTimeBet: liveTimeBet,
                                        md5: md5(`${hdpInfo.leagueNameN.en.trim().toUpperCase()}${hdpInfo.matchName.en.h.trim().toUpperCase()}${hdpInfo.matchName.en.a.trim().toUpperCase()}`),
                                        md5d: md5(`${hdpInfo.leagueNameN.en.trim().toUpperCase()}${hdpInfo.matchName.en.h.trim().toUpperCase()}${hdpInfo.matchName.en.a.trim().toUpperCase()}${new Date(hdpInfo.matchDate).getTime()}`)
                                    } ,
                                    amount: req.body.amount,
                                    memberCredit: memberCredit,
                                    payout: req.body.payout,
                                    gameType: 'TODAY',
                                    acceptHigherPrice: req.body.acceptHigherPrice,
                                    priceType: req.body.priceType.toUpperCase(),
                                    game: 'FOOTBALL',
                                    source: source.toUpperCase(),
                                    status: (req.body.matchType === 'LIVE' && !hdpInfo.isHalfTime && !_.isEqual(hdpInfo.detail.lt, 'Live')) ? 'WAITING' : 'RUNNING',
                                    commission: {},
                                    gameDate: hdpInfo.matchDate,
                                    createdDate: processDate,
                                    ipAddress: Utility.getIP(req),
                                    currency: memberInfo.currency,
                                    isOfflineCash: isOfflineCash,
                                    // codeOfflineTicket: isOfflineCash? random : '',
                                    codeOfflineTicket:random,
                                    active: !isOfflineCash
                                };

                                console.log('$$$$$md5dKey');
                                console.log('$$$$$md5dKey   ', body.hdp.matchDate);
                                console.log('$$$$$md5dKey   ', body.hdp.leagueNameN.en.trim().toUpperCase());
                                console.log('$$$$$md5dKey   ', body.hdp.matchName.en.h.trim().toUpperCase());
                                console.log('$$$$$md5dKey   ', body.hdp.matchName.en.a.trim().toUpperCase());
                                console.log('$$$$$md5dKey   ', new Date(body.hdp.matchDate).getTime());

                                body.hdp.md5d = `${body.hdp.md5d}${new Date(body.hdp.matchDate).getTime()}`;
                                let {
                                    oddType,
                                    isSpecial,
                                    specialType
                                } = req.body;

                                isSpecial = _.isUndefined(isSpecial) ? false : isSpecial;
                                if (isSpecial) {
                                    // body.status = 'RUNNING';
                                    if (_.isEqual(specialType, '1ST_GOAL')) {
                                        body.hdp.oddType = '1STG';
                                    } else if (_.isEqual(specialType, '2ND_GOAL')) {
                                        body.hdp.oddType = '2NDG';
                                    } else if (_.isEqual(specialType, '3RD_GOAL')) {
                                        body.hdp.oddType = '3RDG';
                                    } else if (_.isEqual(specialType, '4TH_GOAL')) {
                                        body.hdp.oddType = '4THG';
                                    } else if (_.isEqual(specialType, '5TH_GOAL')) {
                                        body.hdp.oddType = '5THG';
                                    } else if (_.isEqual(specialType, '6TH_GOAL')) {
                                        body.hdp.oddType = '6THG';
                                    } else if (_.isEqual(specialType, '7TH_GOAL')) {
                                        body.hdp.oddType = '7THG';
                                    } else if (_.isEqual(specialType, '8TH_GOAL')) {
                                        body.hdp.oddType = '8THG';
                                    } else if (_.isEqual(specialType, '9TH_GOAL')) {
                                        body.hdp.oddType = '9THG';
                                    } else if (_.isEqual(specialType, 'TOTAL_CORNER')) {
                                        if (_.isEqual(oddType, 'OU')) {
                                            body.hdp.oddType = 'TTC';
                                        } else {
                                            body.hdp.oddType = 'TTC1ST';
                                        }
                                    }
                                }

                                console.log('BODY : ', JSON.stringify(body));


                                prepareCommission(memberInfo, body, agentGroups, null, 0, 0);


                                if (req.body.matchType === 'LIVE' && !hdpInfo.isHalfTime && !_.isEqual(hdpInfo.detail.lt, 'Live')) {
                                    Commons.call_master_on_ticket_sbo2(body.betId, body.hdp, userInfo.username, (err, data) => {
                                        if (err) {
                                            // callback(err, null);
                                        } else {
                                            // callback(null, data);
                                        }
                                    });

                                    // if (!isSpecial) {
                                    //     // Commons.call_master_on_ticket_sbo(body.betId, body.hdp, (err, data) => {
                                    //     //     if (err) {
                                    //     //         // callback(err, null);
                                    //     //     } else {
                                    //     //         // callback(null, data);
                                    //     //     }
                                    //     // });
                                    //     Commons.call_master_on_ticket_sbo2(body.betId, body.hdp, userInfo.username, (err, data) => {
                                    //         if (err) {
                                    //             // callback(err, null);
                                    //         } else {
                                    //             // callback(null, data);
                                    //         }
                                    //     });
                                    // } else {
                                    //     Commons.call_master_on_ticket_sbo_special(body.betId, body.hdp, (err, data) => {
                                    //         if (err) {
                                    //             // callback(err, null);
                                    //         } else {
                                    //             // callback(null, data);
                                    //         }
                                    //     });
                                    // }
                                }

                                async.waterfall([callback => {
                                    console.log('============== 1 : SAVE TRANSACTION ============');
                                    if (userInfo.group.type === 'API') {

                                        body.username = memberInfo.username;
                                        body.gameType = 'TODAY';
                                        body.sportType = source.toUpperCase() === 'FOOTBALL' ? 'SOCCER' : source.toUpperCase() === 'BASKETBALL' ? 'BASKETBALL' : '';

                                        AgentGroupModel.findById(memberInfo.group, 'endpoint', function (err, agentResponse) {

                                            ApiService.placeBet(agentResponse.endpoint, body, (err, apiResponse) => {

                                                if (err) {
                                                    if (err.code) {
                                                        callback(err.code, null);
                                                    } else {
                                                        callback(999, null);
                                                    }
                                                } else {
                                                    let model = new BetTransactionModel(body);
                                                    model.save(function (err, betResponse) {

                                                        if (err) {
                                                            callback(999, null);
                                                        } else {
                                                            let betResponse1 = Object.assign({}, betResponse)._doc;

                                                            betResponse1.playerBalance = apiResponse.balance;
                                                            callback(null, betResponse1);
                                                        }
                                                    });
                                                }

                                            });
                                        });
                                    } else {

                                        // console.log('body : ', JSON.stringify(body));
                                        let model = new BetTransactionModel(body);
                                        model.save(function (err, betResponse) {
                                            if (err) {
                                                callback(999, null);
                                            } else {

                                                callback(null, betResponse);
                                            }
                                        });

                                        try {
                                            const oddType = req.body.oddType.toUpperCase();
                                            if (_.isEqual(req.body.matchType, 'LIVE') &&
                                                !isSpecial &&
                                                (oddType === 'AH' ||
                                                    oddType === 'AH1ST' ||
                                                    oddType === 'OU' ||
                                                    oddType === 'OU1ST' ||
                                                    oddType === 'OE' ||
                                                    oddType === 'OE1ST')) {
                                                const {
                                                    betId,
                                                    memberId,
                                                    hdp
                                                } = body;
                                                const liveModel = {};
                                                liveModel.betIdKey = `${betId}:${hdp.matchId}:${hdp.score}`;
                                                liveModel.betId = betId;
                                                liveModel.memberId = memberId;
                                                liveModel.memberUsername = memberInfo.username;
                                                liveModel.leagueId = hdp.leagueId;
                                                liveModel.matchId = hdp.matchId;
                                                liveModel.leagueName = hdp.leagueName;
                                                liveModel.leagueNameN = hdp.leagueNameN;
                                                liveModel.matchName = hdp.matchName;
                                                liveModel.score = hdp.score;
                                                liveModel.hScore = parseInt(_.first(hdp.score.split(':')));
                                                liveModel.aScore = parseInt(_.last(hdp.score.split(':')));
                                                liveModel.liveTimeBet = hdp.liveTimeBet;
                                                liveModel.oddType = oddType;
                                                const model = new BetLiveTransactionModel(liveModel);
                                                model.save((err, betLiveResponse) => {});
                                            }
                                        } catch (e) {
                                            console.error('ERROR INDEX 40025');
                                            console.error(e);
                                        }
                                    }


                                }, (betResponse, callback) => {

                                    console.log('============== 2 : ODD ADJUST ============');

                                    let maxBetPrice = _.min([findMaxBet(memberInfo, req.body.oddType), hdpInfo.max]);

                                    let oddAdjBody = {
                                        username: memberInfo.username,
                                        matchId: req.body.matchId,
                                        prefix: req.body.oddType.toLowerCase(),
                                        key: hdpInfo.key,
                                        value: hdpInfo.oddKey,
                                        maxBetPrice: maxBetPrice,
                                        amount: req.body.amount
                                    };

                                    MemberService.createOddAdjustment(oddAdjBody, (err, response) => {
                                        if (err)
                                            callback(5007, null);
                                        else
                                            callback(null, betResponse, response);
                                    });

                                }, (betResponse, addJust, callback) => {

                                    if (userInfo.group.type === 'API') {
                                        callback(null, betResponse, addJust, {});
                                    } else {
                                        MemberService.updateBalance2(memberInfo.username_lower, memberCredit, betResponse.betId, 'SPORTS_BOOK_BET', req.body.requestId ? req.body.requestId : betResponse.betId, function (err, response) {
                                            if (err) {

                                                BetTransactionModel.remove({
                                                    betId: betResponse.betId
                                                }, function (err, removeResponse) {
                                                    callback(999, null);
                                                });

                                            } else {
                                                callback(null, betResponse, addJust, response);
                                            }
                                        });
                                    }

                                }, (betResponse, addJust, balance, callback) => {

                                    console.log('============== 3 : ROBOT ============');
                                    let body = {
                                        betId: betResponse.betId,
                                        memberId: betResponse.memberId,
                                        username: memberInfo.username,
                                        amount:betResponse.amount,
                                        memberCredit:betResponse.memberCredit,
                                        leagueId: betResponse.hdp.leagueId,
                                        matchId: betResponse.hdp.matchId,
                                        odd: betResponse.hdp.odd,
                                        oddKey: betResponse.hdp.oddKey,
                                        oddType: betResponse.hdp.oddType,
                                        handicap: betResponse.hdp.handicap,
                                        matchType: betResponse.hdp.matchType,
                                        priceType: betResponse.priceType,
                                        typeHdpOuOe: memberInfo.commissionSetting.sportsBook.typeHdpOuOe,
                                        isAutoUpdateOdd: memberInfo.isAutoChangeOdd,
                                        key: req.body.key,
                                        remark: '',
                                        createdDate: processDate
                                    };

                                    let model = new RobotTransactionModel(body);
                                    model.save(function (err, robotResponse) {
                                        callback(null, betResponse, addJust, balance, robotResponse);
                                    });
                                }], function (err, betResponse, addJust, balance, robotResponse) {

                                    if (err) {

                                        if (err == 5007) {
                                            return res.send({
                                                message: "createOddAdjustment failed.",
                                                result: hdpInfo,
                                                code: 5007
                                            });
                                        } else if (err == 5008) {
                                            return res.send({
                                                message: "update credit failed",
                                                result: hdpInfo,
                                                code: 5008
                                            });
                                        } else if (err == 1003) {
                                            return res.send({
                                                message: "Insufficient Balance",
                                                code: 1003
                                            });
                                        } else {
                                            return res.send({
                                                message: "Internal Server Error",
                                                result: err,
                                                code: 999
                                            });
                                        }
                                    } else {


                                        MemberService.getCredit(userInfo._id, function (err, credit) {
                                            if (err) {
                                                return res.send({
                                                    message: "get credit fail",
                                                    result: err,
                                                    code: 999
                                                });
                                            } else {

                                                return res.send({
                                                    code: 0,
                                                    message: "success",
                                                    result: {
                                                        betId: betResponse.betId,
                                                        hdpInfo: hdpInfo,
                                                        creditLimit: userInfo.group.type === 'API' ? betResponse.playerBalance : credit
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            });
                        });
                    }
                }
            );
        });
});
function findMaxBet(memberInfo, oddType) {

    switch (oddType.toUpperCase()) {
        case 'AH':
        case 'AH1ST':
        case 'OU':
        case 'OU1ST':
        case 'OE':
        case 'OE1ST':
        case 'T1OU':
        case 'T2OU':
            return memberInfo.limitSetting.sportsBook.hdpOuOe.maxPerBet;
        case 'X12':
        case 'X121ST':
            return memberInfo.limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet;
    }
}

const addMixParlaySchema = Joi.object().keys({
    memberId: Joi.string().required(),
    matches: Joi.array().items({
        matchId: Joi.string().required(),
        odd: Joi.number().required(),
        oddKey: Joi.string().required(),
        oddType: Joi.string().required(),
        bet: Joi.string().allow(''),
        handicap: Joi.string().allow(''),
        homeScore: Joi.number().required(),
        awayScore: Joi.number().required(),
        matchType: Joi.string().required(),
        key: Joi.string().required(),
        time: Joi.string().allow('')
    }),
    odds: Joi.number().required(),
    amount: Joi.number().required(),
    payout: Joi.number().required(),
    source: Joi.string().required(),
    ip: Joi.string().allow(''),
    requestId: Joi.string().allow('')
});
router.post('/mixParlay', function (req, res) {
    // console.log('================');
    // console.log(req.body);
    // console.log('================');
    let validate = Joi.validate(req.body, addMixParlaySchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }


    const userInfo = req.userInfo;
    const source = req.body.source;
    let isOfflineCash = false;
    let oddAdjust;
    let matchStatus = 'RUNNING';

    MemberService.getOddAdjustment(req.userInfo.username, (err, data) => {
        if (err) {
            console.log('Get Odd Adjest error');
        } else {
            oddAdjust = data;
        }
    });


    async.series([callback => {

        MemberModel.findById(req.body.memberId, function (err, response) {
            if (err) {
                callback(err, null);
                return;
            }

            if (response.suspend) {
                callback(5011, null);
            } else if(!response.limitSetting.step.parlay.isEnable){
                callback(5013,null);
            } else {
                AgentService.getUpLineStatus(userInfo.group._id, function (err, status) {

                    if (err) {
                        callback(err, null);
                    } else {
                        if (status.suspend) {
                            callback(5011, null);
                        } else if(!status.parlayEnable){
                            callback(5013,null);
                        }  else {
                            callback(null, status);
                        }
                    }
                });
            }
        });

    }, callback => {

        if (userInfo.group.type === 'API') {
            callback(null, 0);
        } else {
            MemberService.getCredit(userInfo._id, function (err, credit) {
                if (err) {
                    callback(err, null);
                } else {

                    if (typeof credit !== 'number' || _.isUndefined(credit) || _.isNaN(credit)) {
                        callback(5005, null);
                    }else {
                        if (credit < req.body.amount) {
                            callback(5005, null);
                        } else {
                            callback(null, credit);
                        }
                    }
                }
            });
        }

    }, callback => {
        callback(null, 0);

    },

        (callback) => {
            if (_.isEqual(CLIENT_NAME, 'AMB_SPORTBOOK') ||
                _.isEqual(CLIENT_NAME, 'IRON') ||
                _.isEqual(CLIENT_NAME, 'FASTBET98') ||
                _.isEqual(CLIENT_NAME, 'FAST98')||
                _.isEqual(CLIENT_NAME, 'AFC1688') ||
                _.isEqual(CLIENT_NAME, 'SPORTBOOK88') ||
                _.isEqual(CLIENT_NAME, 'CSR') ||
                _.isEqual(CLIENT_NAME, 'MGM')) {
                AgentService.getOfflineCashStatus(userInfo.username, (error, isOfflineCashResult) => {
                    if (error) {
                        callback(error, false);
                    } else {
                        callback(null, isOfflineCashResult);
                        isOfflineCash = isOfflineCashResult;
                    }
                });
            } else {
                callback(null, false);
            }
        }], (err, results) =>{

        if (err) {
            if (err === 5004) {
                return res.send({message: "Payout not matched.", result: null, code: 5004});

            } else if (err === 5005) {
                return res.send({message: "Credit exceed.", result: null, code: 5005});

            } else if (err === 5011) {
                return res.send({
                    message: "Account or Upline was suspend.",
                    result: null,
                    code: 5011
                });
            }
            else if (err === 5013) {
                return res.send({message: "Service Lock.", result: null, code: 5013});
            }
            return res.send({message: "error", result: err, code: 999});
        }

        async.waterfall([callback => {

            MemberModel.findById(req.body.memberId, function (err, response) {
                if (err) {
                    callback(err, null);
                } else {

                    let matchCount = req.body.matches.length;
                    let payout = roundTo(req.body.amount / req.body.odds, 2);

                    console.log('payout : ', payout);

                    let maxBet = response.limitSetting.step.parlay.maxPerBet;
                    let minBet = response.limitSetting.step.parlay.minPerBet;
                    let maxPayout = response.limitSetting.step.parlay.maxPayPerBill;
                    let maxMatchPerBet = response.limitSetting.step.parlay.maxMatchPerBet;
                    let minMatchPerBet = response.limitSetting.step.parlay.minMatchPerBet;

                    if (req.body.amount > maxBet) {
                        callback(5001, null);
                    } else if (req.body.amount < minBet) {
                        callback(5001, null);
                    } else if (matchCount < minMatchPerBet) {
                        callback(5020, null);
                    } else if (matchCount > maxMatchPerBet) {
                        callback(5021, null);
                    } else if (payout > maxPayout) {
                        callback(5012, null);
                    } else {
                        callback(null, response);
                    }
                }
            });

        }, (memberInfo, callback) => {


            let todayDate;
            if (moment().hours() >= 11) {
                todayDate = moment().utc(true);
            } else {
                todayDate = moment().add(-1, 'days').utc(true);
            }

            let condition = {};
            condition['createdDate'] = {
                "$gte": new Date(todayDate.add(0, 'days').utc(true).format('YYYY-MM-DDT11:00:00.000')),
                "$lt": new Date(todayDate.add(1, 'd').utc(true).format('YYYY-MM-DDT11:00:00.000'))
            };
            condition['memberId'] = mongoose.Types.ObjectId(req.body.memberId);
            condition['game'] = 'FOOTBALL';
            condition['gameType'] =  'PARLAY';
            condition['$or'] = [{'status': 'DONE'}, {'status': 'RUNNING'}];
            BetTransactionModel.aggregate([
                {
                    $match: condition
                },
                {
                    $project: {_id: 0}
                },
                {
                    "$group": {
                        "_id": {
                            id: '$_id'
                        },
                        "amount": {$sum: '$amount'}
                    }
                }
            ], function (err, results) {

                if (err) {
                    callback(err, '');
                } else {
                    callback(null, memberInfo, results[0] ? results[0].amount : 0);
                }
            });

        }, (memberInfo, totalBet, callback) => {

            let oddMatch = [];

            async.each(req.body.matches, (match, callback) => {

                let odd = _.filter(oddAdjust, (o) => {
                    return o.matchId === match.id;
                });

                const {
                    matchId,
                    oddType,
                    oddKey,
                    handicap,
                    matchType,
                    homeScore,
                    awayScore,
                    time
                } = match;

                if (_.isEqual(matchStatus, 'RUNNING') && (_.isEqual(matchType, 'LIVE') && !_.isEqual(time, 'HT'))) {
                    matchStatus = 'WAITING';
                }


                if (_.isEqual(matchType, 'NONE_LIVE')) {
                    Commons.cal_bp_mix_parlay_sbo(
                        matchId,
                        oddType,
                        oddKey.toLowerCase(),
                        memberInfo.commissionSetting.sportsBook.typeHdpOuOe,
                        odd,
                        match.key,
                        (err, response) => {
                            if (err) {
                                callback(err, null);
                            } else {

                                checkOddProcess(
                                    response,
                                    'MY',
                                    oddType.toUpperCase(),
                                    oddKey,
                                    match.odd,
                                    handicap,
                                    (isOddNotMatch, key, oddValue, handicapValue, bet) => {

                                        let odd = {
                                            leagueName: response.league_name,
                                            leagueNameN: response.league_name_n,
                                            matchId: response.matchId,
                                            matchName: response.name,
                                            matchDate: response.date,
                                            oddType: oddType,
                                            odd: oddValue,
                                            key: key,
                                            oddKey: oddKey,
                                            handicap: handicapValue,
                                            score: '0:0',
                                            matchType: matchType,
                                            isOddNotMatch: isOddNotMatch,
                                            bet: bet
                                        };
                                        oddMatch.push(odd);
                                        callback();
                                    });
                            }
                        });
                } else {
                    Commons.cal_bp_live_mix_parlay_sbo(
                        matchId,
                        oddType,
                        oddKey.toLowerCase(),
                        memberInfo.commissionSetting.sportsBook.typeHdpOuOe,
                        odd,
                        match.key,
                        (err, response) => {
                            if (err) {
                                callback(err, null);
                            } else {

                                let liveTimeBet = '';
                                try {
                                    liveTimeBet = response.detail.lt;
                                } catch (e) {
                                    console.error('liveTimeBet ', e);
                                }
                                checkOddProcess(
                                    response,
                                    'MY',
                                    oddType.toUpperCase(),
                                    oddKey,
                                    match.odd,
                                    handicap,
                                    (isOddNotMatch, key, oddValue, handicapValue, bet) => {
                                        let odd = {
                                            leagueName: response.league_name,
                                            leagueNameN: response.league_name_n,
                                            matchId: response.matchId,
                                            matchName: response.name,
                                            matchDate: response.date,
                                            oddType: oddType,
                                            odd: Math.min(match.odd, oddValue),
                                            key: key,
                                            oddKey: oddKey,
                                            handicap: handicapValue,
                                            score: `${homeScore}:${awayScore}`,
                                            matchType: matchType,
                                            isOddNotMatch: false,// isOddNotMatch,
                                            bet: bet,
                                            liveTimeBet: liveTimeBet,
                                            detail:response.detail
                                        };
                                        oddMatch.push(odd);
                                        callback();
                                    });
                            }
                        });
                }

            }, (err) => {

                if (oddMatch.length !== req.body.matches.length) {
                    callback(9999, memberInfo, totalBet, oddMatch);
                    return;
                }
                let oddNotMatchLength = _.filter(oddMatch, function (item) {
                    return item.isOddNotMatch;
                });

                if (oddNotMatchLength.length > 0) {
                    callback(5003, memberInfo, totalBet, oddMatch);
                } else {
                    callback(null, memberInfo, totalBet, oddMatch);
                }
            });//end forEach Match


        }, (memberInfo, totalBet, matches, callback) => {

            AgentGroupModel.findById(userInfo.group._id).select('_id type parentId shareSetting commissionSetting').populate({
                path: 'parentId',
                select: '_id type parentId shareSetting commissionSetting name',
                populate: {
                    path: 'parentId',
                    select: '_id type parentId shareSetting commissionSetting name',
                    populate: {
                        path: 'parentId',
                        select: '_id type parentId shareSetting commissionSetting name',
                        populate: {
                            path: 'parentId',
                            select: '_id type parentId shareSetting commissionSetting name',
                            populate: {
                                path: 'parentId',
                                select: '_id type parentId shareSetting commissionSetting name',
                                populate: {
                                    path: 'parentId',
                                    select: '_id type parentId shareSetting commissionSetting name',
                                }
                            }
                        }
                    }
                }
            }).exec(function (err, response) {
                if (err)
                    callback(err, null);
                else
                    callback(null, memberInfo, totalBet, matches, response);
            });


        }], (err, memberInfo, totalBet, matches, agentGroups) => {

            if (err) {
                if (err === 5001) {
                    return res.send({
                        message: "amount exceeded (per bet)",
                        result: null,
                        code: 5001
                    });
                }
                else if (err === 5002) {
                    return res.send({
                        message: "amount exceeded (per match)",
                        result: null,
                        code: 5001
                    });
                }
                else if (err === 5003) {
                    return res.send({
                        message: "odd was already changed.",
                        result: matches,
                        code: 5003
                    });
                } else if (err === 5012) {
                    return res.send({
                        message: "Over maximum bet amount.",
                        result: matches,
                        code: 5012
                    });
                } else if (err === 5020) {
                    return res.send({
                        message: "Match Count < min setting.",
                        result: matches,
                        code: 5020
                    });
                } else if (err === 5021) {
                    return res.send({
                        message: "Match Count > max setting.",
                        result: matches,
                        code: 5021
                    });
                }
                else if (err === 9999) {
                    return res.send({
                        message: "err oddMatch.length != req.body.matches.length",
                        result: matches,
                        code: 9999
                    });
                }
                return res.send({message: "error", result: err, code: 999});
            }

            console.log('totalBetParlay ::: ',totalBet);
            console.log((totalBet + req.body.amount) > memberInfo.limitSetting.step.parlay.maxBetPerDay)

            if((totalBet + req.body.amount) > memberInfo.limitSetting.step.parlay.maxBetPerDay){
                return res.send({
                    message: "amount exceeded (per day)",
                    result: matches,
                    code: 5022
                });
            }

            // //TODO
            // if(req.body.oddType == 'X121ST' || req.body.oddType == 'AH1ST' || req.body.oddType == 'OE1ST' || req.body.oddType == 'OU1ST' ){
            //
            //     console.log("------ hdpInfo.mt : ",hdpInfo.detail.mt)
            //     if(hdpInfo.detail.mt == 'Second Half' || hdpInfo.isHalfTime){
            //         return res.send({
            //             message: "odd was already changed.",
            //             result: hdpInfo,
            //             code: 5003
            //         });
            //     }
            //
            // }

            // try {
            //     let isErr = _.filter(matches, matchInfo => {
            //         console.log('111  :: ',matchInfo.oddType.toUpperCase())
            //         console.log('222  matchInfo.detail.mt :: ',matchInfo.detail.mt)
            //         console.log('333  matchInfo.detail.isHT :: ',matchInfo.detail.isHalfTime)
            //         return (matchInfo.oddType.toUpperCase() == 'X121ST' || matchInfo.oddType.toUpperCase() == 'AH1ST'
            //             || matchInfo.oddType.toUpperCase() == 'OE1ST' || matchInfo.oddType.toUpperCase() == 'OU1ST' )  && (matchInfo.detail.mt == 'Second Half')
            //     });
            //
            //     // //TODO
            //     if(isErr.length > 0){
            //         return res.send({
            //             message: "odd was already changed.",
            //             result: '',
            //             code: 5003
            //         });
            //     }
            // } catch (e) {
            //     console.error(e);
            // }


            let matchTransform = [];
            _.each(matches, matchInfo => {
                matchTransform.push({
                    leagueId: matchInfo.matchId.split(':')[0],
                    matchId: matchInfo.matchId,
                    leagueName: matchInfo.leagueName,
                    leagueNameN: matchInfo.leagueNameN,
                    matchName: matchInfo.matchName,
                    matchDate: matchInfo.matchDate,
                    oddKey: matchInfo.oddKey,
                    odd: matchInfo.odd,
                    oddType: matchInfo.oddType.toUpperCase(),
                    handicap: matchInfo.handicap,
                    bet: matchInfo.bet,
                    score: matchInfo.score,
                    matchType: matchInfo.matchType,
                    liveTimeBet: matchInfo.liveTimeBet,
                    md5: md5(`${matchInfo.leagueNameN.en.trim().toUpperCase()}${matchInfo.matchName.en.h.trim().toUpperCase()}${matchInfo.matchName.en.a.trim().toUpperCase()}`),
                    md5d: md5(`${matchInfo.leagueNameN.en.trim().toUpperCase()}${matchInfo.matchName.en.h.trim().toUpperCase()}${matchInfo.matchName.en.a.trim().toUpperCase()}${new Date(matchInfo.matchDate).getTime()}`)
                });
            });

            let oddCount = 1;
            _.each(matchTransform, obj => {
                oddCount *= parseFloat(obj.odd);
                // console.log(oddCount);
                console.log('######### => ', obj.odd);
                obj.md5d = `${obj.md5}${new Date(obj.matchDate).getTime()}`;
            });
            // console.log(req.body.amount);
            oddCount = roundTo.down(oddCount, 3);
            // console.log(oddCount);
            let payoutNew = roundTo(req.body.amount * oddCount, 2);
            // console.log(payoutNew);
            let random = _.random(1000,9999);

            let body = {
                betId: generateBetId(),
                memberId: req.body.memberId,
                memberParentGroup: memberInfo.group,
                memberUsername: memberInfo.username_lower,
                parlay: {
                    matches: matchTransform,
                    odds: oddCount
                },
                amount: req.body.amount,
                memberCredit: (req.body.amount * -1),
                payout: payoutNew,
                gameType: 'PARLAY',
                acceptHigherPrice: false,
                priceType: 'EU',
                game: 'FOOTBALL',
                source: source.toUpperCase(),
                status: matchStatus,
                commission: {},
                gameDate: matchTransform[0].matchDate,
                createdDate: DateUtils.getCurrentDate(),
                ipAddress: Utility.getIP(req),
                currency: memberInfo.currency,
                isOfflineCash: isOfflineCash,
                // codeOfflineTicket: isOfflineCash? random : 'none',
                codeOfflineTicket:random,
                active: !isOfflineCash
            };


            if (_.isEqual(matchStatus, 'WAITING')) {
                Commons.call_master_on_ticket_mix_parlay_sbo(body.betId, matchTransform, (err, data) => {
                    if (err) {
                        // callback(err, null);
                    } else {
                        // callback(null, data);
                    }
                });
            }

            prepareCommission(memberInfo, body, agentGroups, null, 0, 0);

            let model = new BetTransactionModel(body);
            model.save(function (err, betResponse) {

                if (err) {
                    return res.send({message: "error", result: err, code: 999});
                }

                if (userInfo.group.type === 'API') {

                    let body = Object.assign({}, betResponse)._doc;
                    body.username = memberInfo.username;
                    body.gameType = 'PARLAY';
                    body.sportType = 'SOCCER';

                    AgentGroupModel.findById(memberInfo.group, 'endpoint', function (err, agentResponse) {

                        ApiService.placeBet(agentResponse.endpoint, body, (err, response) => {
                            if (err) {
                                return res.send({
                                    code: 999,
                                    message: "error",
                                    result: err
                                });
                            } else {

                                return res.send({
                                    code: 0,
                                    message: "success",
                                    result: {
                                        betId: betResponse.betId,
                                        parlayInfo: betResponse.parlay
                                    }
                                });
                            }

                        });
                    });
                } else {
                    async.waterfall([callback => {

                        MemberService.updateBalance2(memberInfo.username_lower, (req.body.amount * -1), betResponse.betId, 'SPORTS_BOOK_BET', req.body.requestId ? req.body.requestId : betResponse.betId, function (err, response) {
                            if (err) {
                                BetTransactionModel.remove({
                                    betId: betResponse.betId
                                }, function (err, removeResponse) {
                                    callback(999, null);
                                });
                            } else {
                                callback(null, response);
                            }
                        });

                    }, (updateBalanceResponse, callback) => {

                        MemberService.getCredit(userInfo._id, function (err, credit) {
                            if (err) {
                                callback(999, null);
                            } else {
                                callback(null, updateBalanceResponse, credit);
                            }
                        });

                    }], function (err, balanceResponse, credit) {

                        if (err) {
                            return res.send({message: "error", result: err, code: 999});
                        }
                        return res.send(
                            {
                                code: 0,
                                message: "success",
                                result: {
                                    betId: betResponse.betId,
                                    parlayInfo: betResponse.parlay,
                                    creditLimit: credit
                                }
                            }
                        );

                    });
                }
            });

            try {
                _.each(matchTransform, obj => {
                    const {
                        matchType,
                        leagueId,
                        matchId,
                        leagueName,
                        leagueNameN,
                        matchName,
                        score,
                        liveTimeBet
                    } = obj;
                    if (_.isEqual(matchType, 'LIVE')) {
                        const {
                            betId,
                            memberId
                        } = body;
                        const liveModel = {};
                        liveModel.betIdKey = `${betId}:${matchId}:${score}`;
                        liveModel.betId = betId;
                        liveModel.memberId = memberId;
                        liveModel.memberUsername = memberInfo.username;
                        liveModel.leagueId = leagueId;
                        liveModel.matchId = matchId;
                        liveModel.leagueName = leagueName;
                        liveModel.leagueNameN = leagueNameN;
                        liveModel.matchName = matchName;
                        liveModel.score = score;
                        liveModel.hScore = parseInt(_.first(score.split(':')));
                        liveModel.aScore = parseInt(_.last(score.split(':')));
                        liveModel.liveTimeBet = liveTimeBet;
                        const model = new BetLiveTransactionModel(liveModel);
                        model.save((err, betLiveResponse) => {});
                    }
                });
            } catch (e) {
                console.error('ERROR INDEX 40025');
                console.error(e);
            }
        });

    });


});

const addMixStepSchema = Joi.object().keys({
    memberId: Joi.string().required(),
    matches: Joi.array().items({
        matchId: Joi.string().required(),
        odd: Joi.number().required(),
        oddKey: Joi.string().required(),
        oddType: Joi.string().required(),
        bet: Joi.string().allow(''),
        handicap: Joi.string().allow(''),
        homeScore: Joi.number().required(),
        awayScore: Joi.number().required(),
        matchType: Joi.string().required(),
        key: Joi.string().required(),
        time: Joi.string().allow(''),
        sportType: Joi.string().allow('')
    }),
    odds: Joi.number().required(),
    amount: Joi.number().required(),
    payout: Joi.number().required(),
    ip: Joi.string().allow(''),
    requestId: Joi.string().allow('')
});
router.post('/mixStep', function (req, res) {
    // console.log('================');
    // console.log(req.body);
    // console.log('================');
    let validate = Joi.validate(req.body, addMixStepSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }


    const userInfo = req.userInfo;
    const source = req.body.source;
    let isOfflineCash = false;
    let oddAdjust;
    let matchStatus = 'RUNNING';

    MemberService.getOddAdjustment(req.userInfo.username, (err, data) => {
        if (err) {
            console.log('Get Odd Adjest error');
        } else {
            oddAdjust = data;
        }
    });


    async.series([callback => {

        MemberModel.findById(req.body.memberId, function (err, response) {
            if (err) {
                callback(err, null);
                return;
            }

            if (response.suspend) {
                callback(5011, null);
            } else if(!response.limitSetting.step.step.isEnable){
                callback(5013,null);
            }else {
                AgentService.getUpLineStatus(userInfo.group._id, function (err, status) {

                    if (err) {
                        callback(err, null);
                    } else {
                        if (status.suspend) {
                            callback(5011, null);
                        } else if(!status.stepEnable){
                            callback(5013,null);
                        } else {
                            callback(null, status);
                        }
                    }
                });
            }
        });

    }, callback => {

        if (userInfo.group.type === 'API') {
            callback(null, 0);
        } else {
            MemberService.getCredit(userInfo._id, function (err, credit) {
                if (err) {
                    callback(err, null);
                } else {

                    if (typeof credit !== 'number' || _.isUndefined(credit) || _.isNaN(credit)) {
                        callback(5005, null);
                    }else {
                        if (credit < req.body.amount) {
                            callback(5005, null);
                        } else {
                            callback(null, credit);
                        }
                    }
                }
            });
        }

    }, callback => {
        callback(null, 0);

    },

        (callback) => {
            if (_.isEqual(CLIENT_NAME, 'AMB_SPORTBOOK') ||
                _.isEqual(CLIENT_NAME, 'IRON') ||
                _.isEqual(CLIENT_NAME, 'FASTBET98') ||
                _.isEqual(CLIENT_NAME, 'FAST98')||
                _.isEqual(CLIENT_NAME, 'AFC1688') ||
                _.isEqual(CLIENT_NAME, 'SPORTBOOK88') ||
                _.isEqual(CLIENT_NAME, 'CSR') ||
                _.isEqual(CLIENT_NAME, 'MGM')) {
                AgentService.getOfflineCashStatus(userInfo.username, (error, isOfflineCashResult) => {
                    if (error) {
                        callback(error, false);
                    } else {
                        callback(null, isOfflineCashResult);
                        isOfflineCash = isOfflineCashResult;
                    }
                });
            } else {
                callback(null, false);
            }
        }], (err, results) =>{

        if (err) {
            if (err === 5004) {
                return res.send({message: "Payout not matched.", result: null, code: 5004});

            } else if (err === 5005) {
                return res.send({message: "Credit exceed.", result: null, code: 5005});

            } else if (err === 5011) {
                return res.send({
                    message: "Account or Upline was suspend.",
                    result: null,
                    code: 5011
                });
            } else if (err === 5013) {
                return res.send({message: "Service Lock.", result: null, code: 5013});
            }
            return res.send({message: "error", result: err, code: 999});
        }

        async.waterfall([callback => {

            MemberModel.findById(req.body.memberId, function (err, response) {
                if (err) {
                    callback(err, null);
                } else {

                    let matchCount = req.body.matches.length;
                    let payout = roundTo(req.body.amount / req.body.odds, 2);

                    console.log('payout : ', payout);

                    let maxBet = response.limitSetting.step.parlay.maxPerBet;
                    let minBet = response.limitSetting.step.parlay.minPerBet;
                    let maxPayout = response.limitSetting.step.parlay.maxPayPerBill;
                    let maxMatchPerBet = response.limitSetting.step.parlay.maxMatchPerBet;
                    let minMatchPerBet = response.limitSetting.step.parlay.minMatchPerBet;

                    if (req.body.amount > maxBet) {
                        callback(5001, null);
                    } else if (req.body.amount < minBet) {
                        callback(5001, null);
                    } else if (matchCount < minMatchPerBet) {
                        callback(5020, null);
                    } else if (matchCount > maxMatchPerBet) {
                        callback(5021, null);
                    } else if (payout > maxPayout) {
                        callback(5012, null);
                    } else {
                        callback(null, response);
                    }
                }
            });

        }, (memberInfo, callback) => {


            let todayDate;
            if (moment().hours() >= 11) {
                todayDate = moment().utc(true);
            } else {
                todayDate = moment().add(-1, 'days').utc(true);
            }

            let condition = {};
            condition['createdDate'] = {
                "$gte": new Date(todayDate.add(0, 'days').utc(true).format('YYYY-MM-DDT11:00:00.000')),
                "$lt": new Date(todayDate.add(1, 'd').utc(true).format('YYYY-MM-DDT11:00:00.000'))
            };
            condition['memberId'] = mongoose.Types.ObjectId(req.body.memberId);
            condition['game'] = 'FOOTBALL';
            condition['gameType'] =  'MIX_STEP';
            condition['$or'] = [
                {
                    'status': 'DONE'
                },
                {
                    'status': 'RUNNING'
                }];
            BetTransactionModel.aggregate([
                {
                    $match: condition
                },
                {
                    $project: {_id: 0}
                },
                {
                    "$group": {
                        "_id": {
                            id: '$_id'
                        },
                        "amount": {$sum: '$amount'}
                    }
                }
            ], function (err, results) {

                if (err) {
                    callback(err, '');
                } else {
                    callback(null, memberInfo, results[0] ? results[0].amount : 0);
                }
            });

        }, (memberInfo, totalBet, callback) => {

            let oddMatch = [];

            async.each(req.body.matches, (match, callback) => {

                let odd = _.filter(oddAdjust, (o) => {
                    return o.matchId === match.id;
                });

                const {
                    matchId,
                    oddType,
                    oddKey,
                    handicap,
                    matchType,
                    homeScore,
                    awayScore,
                    time,
                    sportType,
                    key
                } = match;

                const {
                    _id
                } = req.userInfo;

                const priceType = 'EU';//'MY';
                const acceptAnyOdd = true;
                if (_.isEqual(matchStatus, 'RUNNING') && (_.isEqual(matchType, 'LIVE') && !_.isEqual(time, 'HT'))) {
                    matchStatus = 'WAITING';
                }

                if (_.isEqual(sportType, 'BASKETBALL')) {
                    Commons.cal_bp_hdp_single_match_basketball_sbo_mix_step(
                        matchId,
                        oddType,
                        oddKey.toLowerCase(),
                        memberInfo.commissionSetting.sportsBook.typeHdpOuOe,
                        oddAdjust,
                        _id,
                        matchType,
                        key,
                        (error, response) => {
                            if (error) {
                                callback(error, null);
                            } else {
                                console.log('response ----- :: ', response);
                                checkOddProcessMixStep(
                                    response,
                                    priceType,
                                    oddType.toUpperCase(),
                                    oddKey,
                                    match.odd,
                                    handicap,
                                    (isOddNotMatch, key, oddValue, handicapValue, bet) => {
                                        let odd = {
                                            leagueName: response.league_name,
                                            leagueNameN: response.league_name_n,
                                            matchId: response.matchId,
                                            matchName: response.name,
                                            matchDate: response.date,
                                            oddType: oddType,
                                            odd: Math.min(match.odd, oddValue),
                                            key: key,
                                            oddKey: oddKey,
                                            handicap: handicapValue,
                                            matchType: matchType,
                                            score: '0:0',
                                            satang: response.s,
                                            max: response.max,
                                            bet: bet,
                                            maxPerMatch: response.maxPerMatch,
                                            sportType: sportType
                                        };

                                        oddMatch.push(odd);
                                        callback();
                                    });
                            }
                        });
                } else if (_.isEqual(sportType, 'MUAY_THAI')) {
                    Commons.cal_bp_hdp_single_match_muaythai_sbo_mix_step(
                        matchId,
                        oddType,
                        oddKey.toLowerCase(),
                        memberInfo.commissionSetting.sportsBook.typeHdpOuOe,
                        oddAdjust,
                        _id,
                        key,
                        (error, response) => {
                            if (error) {
                                callback(error, null);
                            } else {
                                console.log('response ----- :: ', response);
                                checkOddProcessMixStep(
                                    response,
                                    priceType,
                                    oddType.toUpperCase(),
                                    oddKey,
                                    match.odd,
                                    handicap,
                                    (isOddNotMatch, key, oddValue, handicapValue, bet) => {
                                        let odd = {
                                            leagueName: response.league_name,
                                            leagueNameN: response.league_name_n,
                                            matchId: response.matchId,
                                            matchName: response.name,
                                            matchDate: response.date,
                                            oddType: oddType,
                                            odd: Math.min(match.odd, oddValue),
                                            key: key,
                                            oddKey: oddKey,
                                            handicap: handicapValue,
                                            matchType: matchType,
                                            score: '0:0',
                                            satang: response.s,
                                            max: response.max,
                                            bet: bet,
                                            maxPerMatch: response.maxPerMatch,
                                            sportType: sportType
                                        };
                                        oddMatch.push(odd);
                                        callback();
                                    });
                            }
                        });

                } else {
                    if (_.isEqual(matchType, 'NONE_LIVE')) {
                        Commons.cal_bp_hdp_single_match_sbo_mix_step(
                            matchId,
                            oddType,
                            oddKey.toLowerCase(),
                            memberInfo.commissionSetting.sportsBook.typeHdpOuOe,
                            odd,
                            _id,
                            key,
                            (error, response) => {
                                if (error) {
                                    callback(error, null);
                                } else {
                                    checkOddProcessMixStep(
                                        response,
                                        priceType,
                                        oddType.toUpperCase(),
                                        oddKey,
                                        match.odd,
                                        handicap,
                                        (isOddNotMatch, key, oddValue, handicapValue, bet) => {

                                            let odd = {
                                                leagueName: response.league_name,
                                                leagueNameN: response.league_name_n,
                                                matchId: response.matchId,
                                                matchName: response.name,
                                                matchDate: response.date,
                                                oddType: oddType,
                                                odd: Math.min(match.odd, oddValue),
                                                key: key,
                                                oddKey: oddKey,
                                                handicap: handicapValue,
                                                score: '0:0',
                                                matchType: matchType,
                                                isOddNotMatch: false,// isOddNotMatch,
                                                bet: bet,
                                                sportType: sportType
                                            };
                                            oddMatch.push(odd);
                                            callback();
                                        });
                                }
                            });
                    } else {
                        Commons.cal_bp_live_single_match_sbo_mix_step(
                            matchId,
                            oddType,
                            oddKey.toLowerCase(),
                            memberInfo.commissionSetting.sportsBook.typeHdpOuOe,
                            odd,
                            key,
                            (error, response) => {
                                if (error) {
                                    callback(error, null);
                                } else {

                                    checkOddProcess(
                                        response,
                                        priceType,
                                        oddType.toUpperCase(),
                                        oddKey,
                                        match.odd,
                                        handicap,
                                        (isOddNotMatch, key, oddValue, handicapValue, bet) => {
                                            let odd = {
                                                leagueName: response.league_name,
                                                leagueNameN: response.league_name_n,
                                                matchId: response.matchId,
                                                matchName: response.name,
                                                matchDate: response.date,
                                                oddType: oddType,
                                                odd: Math.min(match.odd, oddValue),
                                                key: key,
                                                oddKey: oddKey,
                                                handicap: handicapValue,
                                                score: `${homeScore}:${awayScore}`,
                                                matchType: matchType,
                                                isOddNotMatch: false,// isOddNotMatch,
                                                bet: bet,
                                                sportType: sportType,
                                                liveTimeBet: response.detail.lt,
                                                detail:response.detail
                                            };
                                            oddMatch.push(odd);
                                            callback();
                                        });
                                }
                            });
                    }
                }
            }, (err) => {

                if (oddMatch.length !== req.body.matches.length) {
                    callback(9999, memberInfo, totalBet, oddMatch);
                    return;
                }
                let oddNotMatchLength = _.filter(oddMatch, function (item) {
                    return item.isOddNotMatch;
                });

                if (oddNotMatchLength.length > 0) {
                    callback(5003, memberInfo, totalBet, oddMatch);
                } else {
                    callback(null, memberInfo, totalBet, oddMatch);
                }
            });//end forEach Match


        }, (memberInfo, totalBet, matches, callback) => {

            AgentGroupModel.findById(userInfo.group._id).select('_id type parentId shareSetting commissionSetting').populate({
                path: 'parentId',
                select: '_id type parentId shareSetting commissionSetting name',
                populate: {
                    path: 'parentId',
                    select: '_id type parentId shareSetting commissionSetting name',
                    populate: {
                        path: 'parentId',
                        select: '_id type parentId shareSetting commissionSetting name',
                        populate: {
                            path: 'parentId',
                            select: '_id type parentId shareSetting commissionSetting name',
                            populate: {
                                path: 'parentId',
                                select: '_id type parentId shareSetting commissionSetting name',
                                populate: {
                                    path: 'parentId',
                                    select: '_id type parentId shareSetting commissionSetting name',
                                }
                            }
                        }
                    }
                }
            }).exec(function (err, response) {
                if (err)
                    callback(err, null);
                else
                    callback(null, memberInfo, totalBet, matches, response);
            });


        }], (err, memberInfo, totalBet, matches, agentGroups) => {

            if (err) {
                if (err === 5001) {
                    return res.send({
                        message: "amount exceeded (per bet)",
                        result: null,
                        code: 5001
                    });
                }
                else if (err === 5002) {
                    return res.send({
                        message: "amount exceeded (per match)",
                        result: null,
                        code: 5001
                    });
                }
                else if (err === 5003) {
                    return res.send({
                        message: "odd was already changed.",
                        result: matches,
                        code: 5003
                    });
                } else if (err === 5012) {
                    return res.send({
                        message: "Over maximum bet amount.",
                        result: matches,
                        code: 5012
                    });
                } else if (err === 5020) {
                    return res.send({
                        message: "Match Count < min setting.",
                        result: matches,
                        code: 5020
                    });
                } else if (err === 5021) {
                    return res.send({
                        message: "Match Count > max setting.",
                        result: matches,
                        code: 5021
                    });
                } else if (err === 9999) {
                    return res.send({
                        message: "err oddMatch.length != req.body.matches.length",
                        result: matches,
                        code: 9999
                    });
                }
                return res.send({message: "error", result: err, code: 999});
            }

            console.log('totalBetParlay ::: ',totalBet);
            console.log((totalBet + req.body.amount) > memberInfo.limitSetting.step.parlay.maxBetPerDay)

            if((totalBet + req.body.amount) > memberInfo.limitSetting.step.parlay.maxBetPerDay){
                return res.send({
                    message: "amount exceeded (per day)",
                    result: matches,
                    code: 5022
                });
            }


            // try {
            //     let isErr = _.filter(matches, matchInfo => {
            //         console.log('111  :: ',matchInfo.oddType.toUpperCase())
            //         console.log('222  matchInfo.detail.mt :: ',matchInfo.detail.mt)
            //         console.log('333  matchInfo.detail.isHT :: ',matchInfo.detail.isHalfTime)
            //         return (matchInfo.oddType.toUpperCase() == 'X121ST' || matchInfo.oddType.toUpperCase() == 'AH1ST'
            //             || matchInfo.oddType.toUpperCase() == 'OE1ST' || matchInfo.oddType.toUpperCase() == 'OU1ST' )  && (matchInfo.detail.mt == 'Second Half')
            //     });
            //
            //     // //TODO
            //     if(isErr.length > 0){
            //         return res.send({
            //             message: "odd was already changed.",
            //             result: '',
            //             code: 5003
            //         });
            //     }
            // } catch (e) {
            //     console.error(e);
            // }

            let matchTransform = [];
            _.each(matches, matchInfo => {

                console.log('matchInfo : ',matchInfo)
                matchTransform.push({
                    leagueId:    matchInfo.matchId.split(':')[0],
                    matchId:     matchInfo.matchId,
                    leagueName:  matchInfo.leagueName,
                    leagueNameN: matchInfo.leagueNameN,
                    matchName:   matchInfo.matchName,
                    matchDate:   matchInfo.matchDate,
                    oddKey:      matchInfo.oddKey,
                    odd:         matchInfo.odd,
                    oddType:     matchInfo.oddType.toUpperCase(),
                    handicap:    matchInfo.handicap,
                    bet:         matchInfo.bet,
                    score:       matchInfo.score,
                    matchType:   matchInfo.matchType,
                    sportType:   matchInfo.sportType,
                    liveTimeBet: matchInfo.liveTimeBet,
                    md5: md5(`${matchInfo.leagueNameN.en.trim().toUpperCase()}${matchInfo.matchName.en.h.trim().toUpperCase()}${matchInfo.matchName.en.a.trim().toUpperCase()}`),
                    md5d: md5(`${matchInfo.leagueNameN.en.trim().toUpperCase()}${matchInfo.matchName.en.h.trim().toUpperCase()}${matchInfo.matchName.en.a.trim().toUpperCase()}${new Date(matchInfo.matchDate).getTime()}`)
                });
            });




            let oddCount = 1;
            _.each(matchTransform, obj => {
                oddCount *= parseFloat(obj.odd);
                obj.md5d = `${obj.md5}${new Date(obj.matchDate).getTime()}`;
                console.log('######### => ', obj.odd);
            });
            // console.log(req.body.amount);
            oddCount = roundTo.down(oddCount, 3);
            // console.log(oddCount);
            let payoutNew = roundTo(req.body.amount * oddCount, 2);
            // console.log(payoutNew);
            let random = _.random(1000,9999);

            let body = {
                betId: generateBetId(),
                memberId: req.body.memberId,
                memberParentGroup: memberInfo.group,
                memberUsername: memberInfo.username_lower,
                parlay: {
                    matches: matchTransform,
                    odds: oddCount
                },
                amount: req.body.amount,
                memberCredit: (req.body.amount * -1),
                payout: payoutNew,
                gameType: 'MIX_STEP',
                acceptHigherPrice: false,
                priceType: 'EU',
                game: 'FOOTBALL',
                source: 'FOOTBALL',
                status: matchStatus,
                commission: {},
                gameDate: matchTransform[0].matchDate,
                createdDate: DateUtils.getCurrentDate(),
                ipAddress: Utility.getIP(req),
                currency: memberInfo.currency,
                codeOfflineTicket:random,
                isOfflineCash: isOfflineCash,
                active: !isOfflineCash
            };


            if (_.isEqual(matchStatus, 'WAITING')) {
                Commons.call_master_on_ticket_mix_step_sbo(body.betId, matchTransform, (err, data) => {
                    if (err) {
                        // callback(err, null);
                    } else {
                        // callback(null, data);
                    }
                });
            }

            prepareCommission(memberInfo, body, agentGroups, null, 0, 0);

            let model = new BetTransactionModel(body);
            model.save(function (err, betResponse) {

                if (err) {
                    return res.send({message: "error", result: err, code: 999});
                }

                if (userInfo.group.type === 'API') {

                    let body = Object.assign({}, betResponse)._doc;
                    body.username = memberInfo.username;
                    body.gameType = 'MIX_STEP';
                    body.sportType = 'SOCCER';

                    AgentGroupModel.findById(memberInfo.group, 'endpoint', function (err, agentResponse) {

                        ApiService.placeBet(agentResponse.endpoint, body, (err, response) => {
                            if (err) {
                                return res.send({
                                    code: 999,
                                    message: "error",
                                    result: err
                                });
                            } else {

                                return res.send({
                                    code: 0,
                                    message: "success",
                                    result: {
                                        betId: betResponse.betId,
                                        parlayInfo: betResponse.parlay
                                    }
                                });
                            }

                        });
                    });
                } else {
                    async.waterfall([callback => {

                        MemberService.updateBalance2(memberInfo.username_lower, (req.body.amount * -1), betResponse.betId, 'SPORTS_BOOK_BET', req.body.requestId ? req.body.requestId : betResponse.betId, function (err, response) {
                            if (err) {
                                BetTransactionModel.remove({
                                    betId: betResponse.betId
                                }, function (err, removeResponse) {
                                    callback(999, null);
                                });
                            } else {
                                callback(null, response);
                            }
                        });

                    }, (updateBalanceResponse, callback) => {

                        MemberService.getCredit(userInfo._id, function (err, credit) {
                            if (err) {
                                callback(999, null);
                            } else {
                                callback(null, updateBalanceResponse, credit);
                            }
                        });

                    }], function (err, balanceResponse, credit) {

                        if (err) {
                            return res.send({message: "error", result: err, code: 999});
                        }
                        return res.send(
                            {
                                code: 0,
                                message: "success",
                                result: {
                                    betId: betResponse.betId,
                                    parlayInfo: betResponse.parlay,
                                    creditLimit: credit
                                }
                            }
                        );

                    });
                }
            });

            try {
                _.each(matchTransform, obj => {
                    const {
                        matchType,
                        leagueId,
                        matchId,
                        leagueName,
                        leagueNameN,
                        matchName,
                        score,
                        liveTimeBet,
                        sportType
                    } = obj;
                    if (_.isEqual(matchType, 'LIVE') && _.isEqual(sportType, 'FOOTBALL')) {
                        const {
                            betId,
                            memberId
                        } = body;
                        const liveModel = {};
                        liveModel.betIdKey = `${betId}:${matchId}:${score}`;
                        liveModel.betId = betId;
                        liveModel.memberId = memberId;
                        liveModel.memberUsername = memberInfo.username;
                        liveModel.leagueId = leagueId;
                        liveModel.matchId = matchId;
                        liveModel.leagueName = leagueName;
                        liveModel.leagueNameN = leagueNameN;
                        liveModel.matchName = matchName;
                        liveModel.score = score;
                        liveModel.hScore = parseInt(_.first(score.split(':')));
                        liveModel.aScore = parseInt(_.last(score.split(':')));
                        liveModel.liveTimeBet = liveTimeBet;
                        const model = new BetLiveTransactionModel(liveModel);
                        model.save((err, betLiveResponse) => {});
                    }
                });
            } catch (e) {
                console.error('ERROR INDEX 40025');
                console.error(e);
            }
        });

    });


});

const addMixParlayTableSchema = Joi.object().keys({
    memberId: Joi.string().required(),
    matches: Joi.array().items({
        matchId: Joi.string().required(),
        odd: Joi.number().required(),
        oddKey: Joi.string().required(),
        oddType: Joi.string().required(),
        bet: Joi.string().allow(''),
        handicap: Joi.string().allow(''),
        homeScore: Joi.number().required(),
        awayScore: Joi.number().required(),
        matchType: Joi.string().required(),
        key: Joi.string().required(),
        time: Joi.string().allow(''),
        keyIn: Joi.string().required()
    }),
    odds: Joi.number().required(),
    amount: Joi.number().required(),
    payout: Joi.number().required(),
    source: Joi.string().required(),
    ip: Joi.string().allow(''),
    keyInAll: Joi.string().required()
});
router.post('/mixParlayTable', function (req, res) {
    // console.log('================');
    // console.log(req.body);
    // console.log('================');
    let validate = Joi.validate(req.body, addMixParlayTableSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }


    const userInfo = req.userInfo;
    const source = req.body.source;
    let isOfflineCash = false;
    let oddAdjust;
    let matchStatus = 'RUNNING';

    MemberService.getOddAdjustment(req.userInfo.username, (err, data) => {
        if (err) {
            console.log('Get Odd Adjest error');
        } else {
            oddAdjust = data;
        }
    });


    async.series([callback => {

        MemberModel.findById(req.body.memberId, function (err, response) {
            if (err) {
                callback(err, null);
                return;
            }

            if (response.suspend) {
                callback(5011, null);
            } else if(!response.limitSetting.step.parlay.isEnable){
                callback(5013,null);
            } else {
                AgentService.getUpLineStatus(userInfo.group._id, function (err, status) {

                    if (err) {
                        callback(err, null);
                    } else {
                        if (status.suspend) {
                            callback(5011, null);
                        } else if(!status.parlayEnable){
                            callback(5013,null);
                        }  else {
                            callback(null, status);
                        }
                    }
                });
            }
        });

    }, callback => {

        if (userInfo.group.type === 'API') {
            callback(null, 0);
        } else {
            MemberService.getCredit(userInfo._id, function (err, credit) {
                if (err) {
                    callback(err, null);
                } else {

                    if (typeof credit !== 'number' || _.isUndefined(credit) || _.isNaN(credit)) {
                        callback(5005, null);
                    }else {
                        if (credit < req.body.amount) {
                            callback(5005, null);
                        } else {
                            callback(null, credit);
                        }
                    }
                }
            });
        }

    }, callback => {
        callback(null, 0);

    },

        (callback) => {
            if (_.isEqual(CLIENT_NAME, 'AMB_SPORTBOOK') ||
                _.isEqual(CLIENT_NAME, 'IRON') ||
                _.isEqual(CLIENT_NAME, 'FASTBET98') ||
                _.isEqual(CLIENT_NAME, 'FAST98')||
                _.isEqual(CLIENT_NAME, 'AFC1688') ||
                _.isEqual(CLIENT_NAME, 'SPORTBOOK88') ||
                _.isEqual(CLIENT_NAME, 'CSR') ||
                _.isEqual(CLIENT_NAME, 'MGM')) {
                AgentService.getOfflineCashStatus(userInfo.username, (error, isOfflineCashResult) => {
                    if (error) {
                        callback(error, false);
                    } else {
                        callback(null, isOfflineCashResult);
                        isOfflineCash = isOfflineCashResult;
                    }
                });
            } else {
                callback(null, false);
            }
        }], (err, results) =>{

        if (err) {
            if (err === 5004) {
                return res.send({message: "Payout not matched.", result: null, code: 5004});

            } else if (err === 5005) {
                return res.send({message: "Credit exceed.", result: null, code: 5005});

            } else if (err === 5011) {
                return res.send({
                    message: "Account or Upline was suspend.",
                    result: null,
                    code: 5011
                });
            }
            else if (err === 5013) {
                return res.send({message: "Service Lock.", result: null, code: 5013});
            }
            return res.send({message: "error", result: err, code: 999});
        }

        async.waterfall([callback => {

            MemberModel.findById(req.body.memberId, function (err, response) {
                if (err) {
                    callback(err, null);
                } else {

                    let matchCount = req.body.matches.length;
                    let payout = roundTo(req.body.amount / req.body.odds, 2);

                    console.log('payout : ', payout);

                    let maxBet = response.limitSetting.step.parlay.maxPerBet;
                    let minBet = response.limitSetting.step.parlay.minPerBet;
                    let maxPayout = response.limitSetting.step.parlay.maxPayPerBill;
                    let maxMatchPerBet = response.limitSetting.step.parlay.maxMatchPerBet;
                    let minMatchPerBet = response.limitSetting.step.parlay.minMatchPerBet;

                    if (req.body.amount > maxBet) {
                        callback(5001, null);
                    } else if (req.body.amount < minBet) {
                        callback(5001, null);
                    } else if (matchCount < minMatchPerBet) {
                        callback(5020, null);
                    } else if (matchCount > maxMatchPerBet) {
                        callback(5021, null);
                    } else if (payout > maxPayout) {
                        callback(5012, null);
                    } else {
                        callback(null, response);
                    }
                }
            });

        }, (memberInfo, callback) => {


            let todayDate;
            if (moment().hours() >= 11) {
                todayDate = moment().utc(true);
            } else {
                todayDate = moment().add(-1, 'days').utc(true);
            }

            let condition = {};
            condition['createdDate'] = {
                "$gte": new Date(todayDate.add(0, 'days').utc(true).format('YYYY-MM-DDT11:00:00.000')),
                "$lt": new Date(todayDate.add(1, 'd').utc(true).format('YYYY-MM-DDT11:00:00.000'))
            };
            condition['memberId'] = mongoose.Types.ObjectId(req.body.memberId);
            condition['game'] = 'FOOTBALL';
            condition['gameType'] =  'PARLAY';
            condition['$or'] = [{'status': 'DONE'}, {'status': 'RUNNING'}];
            BetTransactionModel.aggregate([
                {
                    $match: condition
                },
                {
                    $project: {_id: 0}
                },
                {
                    "$group": {
                        "_id": {
                            id: '$_id'
                        },
                        "amount": {$sum: '$amount'}
                    }
                }
            ], function (err, results) {

                if (err) {
                    callback(err, '');
                } else {
                    callback(null, memberInfo, results[0] ? results[0].amount : 0);
                }
            });

        }, (memberInfo, totalBet, callback) => {

            let oddMatch = [];

            async.each(req.body.matches, (match, callback) => {

                let odd = _.filter(oddAdjust, (o) => {
                    return o.matchId === match.id;
                });

                const {
                    matchId,
                    oddType,
                    oddKey,
                    handicap,
                    matchType,
                    keyIn
                } = match;

                Commons.cal_bp_mix_parlay_sbo(
                    matchId,
                    oddType,
                    oddKey.toLowerCase(),
                    memberInfo.commissionSetting.sportsBook.typeHdpOuOe,
                    odd,
                    match.key,
                    (err, response) => {
                        if (err) {
                            callback(err, null);
                        } else {

                            checkOddProcess(
                                response,
                                'MY',
                                oddType.toUpperCase(),
                                oddKey,
                                match.odd,
                                handicap,
                                (isOddNotMatch, key, oddValue, handicapValue, bet) => {

                                    let odd = {
                                        leagueName: response.league_name,
                                        leagueNameN: response.league_name_n,
                                        matchId: response.matchId,
                                        matchName: response.name,
                                        matchDate: response.date,
                                        oddType: oddType,
                                        odd: oddValue,
                                        key: key,
                                        oddKey: oddKey,
                                        handicap: handicapValue,
                                        score: '0:0',
                                        matchType: matchType,
                                        isOddNotMatch: isOddNotMatch,
                                        bet: bet,
                                        keyIn
                                    };
                                    oddMatch.push(odd);
                                    callback();
                                });
                        }
                    });

            }, (err) => {

                if (oddMatch.length !== req.body.matches.length) {
                    callback(9999, memberInfo, totalBet, oddMatch);
                    return;
                }
                let oddNotMatchLength = _.filter(oddMatch, function (item) {
                    return item.isOddNotMatch;
                });

                if (oddNotMatchLength.length > 0) {
                    callback(5003, memberInfo, totalBet, oddMatch);
                } else {
                    callback(null, memberInfo, totalBet, oddMatch);
                }
            });//end forEach Match


        }, (memberInfo, totalBet, matches, callback) => {

            AgentGroupModel.findById(userInfo.group._id).select('_id type parentId shareSetting commissionSetting').populate({
                path: 'parentId',
                select: '_id type parentId shareSetting commissionSetting name',
                populate: {
                    path: 'parentId',
                    select: '_id type parentId shareSetting commissionSetting name',
                    populate: {
                        path: 'parentId',
                        select: '_id type parentId shareSetting commissionSetting name',
                        populate: {
                            path: 'parentId',
                            select: '_id type parentId shareSetting commissionSetting name',
                            populate: {
                                path: 'parentId',
                                select: '_id type parentId shareSetting commissionSetting name',
                                populate: {
                                    path: 'parentId',
                                    select: '_id type parentId shareSetting commissionSetting name',
                                }
                            }
                        }
                    }
                }
            }).exec(function (err, response) {
                if (err)
                    callback(err, null);
                else
                    callback(null, memberInfo, totalBet, matches, response);
            });


        }], (err, memberInfo, totalBet, matches, agentGroups) => {

            if (err) {
                if (err === 5001) {
                    return res.send({
                        message: "amount exceeded (per bet)",
                        result: null,
                        code: 5001
                    });
                }
                else if (err === 5002) {
                    return res.send({
                        message: "amount exceeded (per match)",
                        result: null,
                        code: 5001
                    });
                }
                else if (err === 5003) {
                    return res.send({
                        message: "odd was already changed.",
                        result: matches,
                        code: 5003
                    });
                } else if (err === 5012) {
                    return res.send({
                        message: "Over maximum bet amount.",
                        result: matches,
                        code: 5012
                    });
                } else if (err === 5020) {
                    return res.send({
                        message: "Match Count < min setting.",
                        result: matches,
                        code: 5020
                    });
                } else if (err === 5021) {
                    return res.send({
                        message: "Match Count > max setting.",
                        result: matches,
                        code: 5021
                    });
                }
                else if (err === 9999) {
                    return res.send({
                        message: "err oddMatch.length != req.body.matches.length",
                        result: matches,
                        code: 9999
                    });
                }
                return res.send({message: "error", result: err, code: 999});
            }

            console.log('totalBetParlay ::: ',totalBet);
            console.log((totalBet + req.body.amount) > memberInfo.limitSetting.step.parlay.maxBetPerDay);

            if((totalBet + req.body.amount) > memberInfo.limitSetting.step.parlay.maxBetPerDay){
                return res.send({
                    message: "amount exceeded (per day)",
                    result: matches,
                    code: 5022
                });
            }

            let matchTransform = [];
            _.each(matches, matchInfo => {
                matchTransform.push({
                    leagueId: matchInfo.matchId.split(':')[0],
                    matchId: matchInfo.matchId,
                    leagueName: matchInfo.leagueName,
                    leagueNameN: matchInfo.leagueNameN,
                    matchName: matchInfo.matchName,
                    matchDate: matchInfo.matchDate,
                    oddKey: matchInfo.oddKey,
                    odd: matchInfo.odd,
                    oddType: matchInfo.oddType.toUpperCase(),
                    handicap: matchInfo.handicap,
                    bet: matchInfo.bet,
                    score: matchInfo.score,
                    matchType: matchInfo.matchType,
                    liveTimeBet: matchInfo.liveTimeBet,
                    md5: md5(`${matchInfo.leagueNameN.en.trim().toUpperCase()}${matchInfo.matchName.en.h.trim().toUpperCase()}${matchInfo.matchName.en.a.trim().toUpperCase()}`),
                    md5d: md5(`${matchInfo.leagueNameN.en.trim().toUpperCase()}${matchInfo.matchName.en.h.trim().toUpperCase()}${matchInfo.matchName.en.a.trim().toUpperCase()}${new Date(matchInfo.matchDate).getTime()}`),
                    keyIn: matchInfo.keyIn
                });
            });

            let oddCount = 1;
            _.each(matchTransform, obj => {
                oddCount *= parseFloat(obj.odd);
                // console.log(oddCount);
                obj.md5d = `${obj.md5}${new Date(obj.matchDate).getTime()}`;
                console.log('######### => ', obj.odd);
            });
            // console.log(req.body.amount);
            oddCount = roundTo.down(oddCount, 3);
            // console.log(oddCount);
            let payoutNew = roundTo(req.body.amount * oddCount, 2);
            // console.log(payoutNew);
            let random = _.random(1000,9999);

            let body = {
                betId: generateBetId(),
                memberId: req.body.memberId,
                memberParentGroup: memberInfo.group,
                parlay: {
                    matches: matchTransform,
                    odds: oddCount,
                    keyInAll: req.body.keyInAll
                },
                amount: req.body.amount,
                memberCredit: (req.body.amount * -1),
                payout: payoutNew,
                gameType: 'PARLAY',
                acceptHigherPrice: false,
                priceType: 'EU',
                game: 'FOOTBALL',
                source: source.toUpperCase(),
                status: matchStatus,
                commission: {},
                gameDate: matchTransform[0].matchDate,
                createdDate: DateUtils.getCurrentDate(),
                ipAddress: Utility.getIP(req),
                currency: memberInfo.currency,
                isOfflineCash: isOfflineCash,
                // codeOfflineTicket: isOfflineCash? random : 'none',
                codeOfflineTicket:random,
                active: !isOfflineCash
            };


            if (_.isEqual(matchStatus, 'WAITING')) {
                Commons.call_master_on_ticket_mix_parlay_sbo(body.betId, matchTransform, (err, data) => {
                    if (err) {
                        // callback(err, null);
                    } else {
                        // callback(null, data);
                    }
                });
            }

            prepareCommission(memberInfo, body, agentGroups, null, 0, 0);

            let model = new BetTransactionModel(body);
            model.save(function (err, betResponse) {

                if (err) {
                    return res.send({message: "error", result: err, code: 999});
                }

                if (userInfo.group.type === 'API') {

                    let body = Object.assign({}, betResponse)._doc;
                    body.username = memberInfo.username;
                    body.gameType = 'PARLAY';
                    body.sportType = 'SOCCER';

                    AgentGroupModel.findById(memberInfo.group, 'endpoint', function (err, agentResponse) {

                        ApiService.placeBet(agentResponse.endpoint, body, (err, response) => {
                            if (err) {
                                return res.send({
                                    code: 999,
                                    message: "error",
                                    result: err
                                });
                            } else {

                                return res.send({
                                    code: 0,
                                    message: "success",
                                    result: {
                                        betId: betResponse.betId,
                                        parlayInfo: betResponse.parlay
                                    }
                                });
                            }

                        });
                    });
                } else {
                    async.waterfall([callback => {

                        MemberService.updateBalance2(memberInfo.username_lower, (req.body.amount * -1), betResponse.betId, 'SPORTS_BOOK_BET', betResponse.betId, function (err, response) {
                            if (err) {
                                callback(5008, null);
                            } else {
                                callback(null, response);
                            }
                        });

                    }, (updateBalanceResponse, callback) => {

                        MemberService.getCredit(userInfo._id, function (err, credit) {
                            if (err) {
                                callback(999, null);
                            } else {
                                callback(null, updateBalanceResponse, credit);
                            }
                        });

                    }], function (err, balanceResponse, credit) {

                        if (err) {
                            return res.send({message: "error", result: err, code: 999});
                        }
                        return res.send(
                            {
                                code: 0,
                                message: "success",
                                result: {
                                    betId: betResponse.betId,
                                    parlayInfo: betResponse.parlay,
                                    creditLimit: credit
                                }
                            }
                        );

                    });
                }
            });

            try {
                _.each(matchTransform, obj => {
                    const {
                        matchType,
                        leagueId,
                        matchId,
                        leagueName,
                        leagueNameN,
                        matchName,
                        score,
                        liveTimeBet
                    } = obj;
                    if (_.isEqual(matchType, 'LIVE')) {
                        const {
                            betId,
                            memberId
                        } = body;
                        const liveModel = {};
                        liveModel.betIdKey = `${betId}:${matchId}:${score}`;
                        liveModel.betId = betId;
                        liveModel.memberId = memberId;
                        liveModel.memberUsername = memberInfo.username;
                        liveModel.leagueId = leagueId;
                        liveModel.matchId = matchId;
                        liveModel.leagueName = leagueName;
                        liveModel.leagueNameN = leagueNameN;
                        liveModel.matchName = matchName;
                        liveModel.score = score;
                        liveModel.hScore = parseInt(_.first(score.split(':')));
                        liveModel.aScore = parseInt(_.last(score.split(':')));
                        liveModel.liveTimeBet = liveTimeBet;
                        const model = new BetLiveTransactionModel(liveModel);
                        model.save((err, betLiveResponse) => {});
                    }
                });
            } catch (e) {
                console.error('ERROR INDEX 40025');
                console.error(e);
            }
        });

    });


});

function checkOddProcessMixStep(bpResponse, priceType, oddType, oddKey, odd, handicap, callback) {


    console.log('----- :::::: ', oddType);
    console.log('----- :::::: ', oddKey);

    let handicapPrefix, oddPrefix;
    let type = oddType.toLowerCase();
    let isOddNotMatch = false;
    let oddValue, handicapValue;
    let key = '';
    let bet = '';

    if (oddType === 'AH' || oddType === 'AH1ST') {

        _Utils.findKeyByValue(bpResponse[type], oddKey, function (keyResponse) {
            key = keyResponse;
        });

        if (key === 'hpk') {
            handicapPrefix = 'h';
            oddPrefix = 'hp';
            bet = 'HOME';
        } else {
            handicapPrefix = 'a';
            oddPrefix = 'ap';
            bet = 'AWAY';
        }

        if (!_.isUndefined(bpResponse[type])) {
            console.log(bpResponse[type][oddPrefix] + '  AH 1:  ' + odd);

            oddValue = bpResponse[type][oddPrefix];
            oddValue = convertOdd(priceType, oddValue, oddType);

            console.log(bpResponse[type][oddPrefix] + '  EU:  ' + oddValue);

            handicapValue = bpResponse[type][handicapPrefix];
            if (oddValue !== odd) {
                isOddNotMatch = true;
            }
            if (handicapValue !== handicap) {
                isOddNotMatch = true;
            }
        } else {
            isOddNotMatch = true;
        }

    } else if (oddType === 'OU' || oddType === 'OU1ST' || oddType === 'T1OU' || oddType === 'T2OU') {

        _Utils.findKeyByValue(bpResponse[type], oddKey, function (keyResponse) {
            key = keyResponse;
        });


        if (key === 'opk') {
            handicapPrefix = 'o';
            oddPrefix = 'op';
            bet = 'OVER';
        } else {
            handicapPrefix = 'u';
            oddPrefix = 'up';
            bet = 'UNDER';
        }

        if (!_.isUndefined(bpResponse[type])) {
            oddValue = bpResponse[type][oddPrefix];
            oddValue = convertOdd(priceType, oddValue, oddType);
            handicapValue = bpResponse[type][handicapPrefix];
            if (oddValue !== odd) {
                isOddNotMatch = true;
            }
            if (handicapValue !== handicap) {
                isOddNotMatch = true;
            }
        } else {
            isOddNotMatch = true;
        }

    } else if (oddType === 'X12' || oddType === 'X121ST' || oddType === 'ML') {

        _Utils.findKeyByValue(bpResponse[type], oddKey, function (keyResponse) {
            key = keyResponse;
        });

        if (key === 'hk') {
            oddPrefix = 'h';
            bet = 'HOME';
        } else if (key === 'ak') {
            oddPrefix = 'a';
            bet = 'AWAY';
        } else if (key === 'dk') {
            oddPrefix = 'd';
            bet = 'DRAW';
        }

        if (!_.isUndefined(bpResponse[type])) {
            oddValue = bpResponse[type][oddPrefix];
            oddValue = convertOdd(priceType, oddValue, oddType);
            if (oddValue !== odd) {
                isOddNotMatch = true;
            }
        } else {
            isOddNotMatch = true;
        }

    } else if (oddType === 'OE' || oddType === 'OE1ST') {

        _Utils.findKeyByValue(bpResponse[type], oddKey, function (keyResponse) {
            key = keyResponse;
        });

        if (key === 'ok') {
            oddPrefix = 'o';
            bet = 'ODD';
        } else if (key === 'ek') {
            oddPrefix = 'e';
            bet = 'EVEN';
        }

        if (!_.isUndefined(bpResponse[type])) {
            oddValue = bpResponse[type][oddPrefix];
            oddValue = convertOdd(priceType, oddValue, oddType);
            if (oddValue !== odd) {
                isOddNotMatch = true;
            }
        } else {
            isOddNotMatch = true;
        }
    } else if (oddType === 'OE' || oddType === 'OE1ST') {

        _Utils.findKeyByValue(bpResponse[type], oddKey, function (keyResponse) {
            key = keyResponse;
        });

        if (key === 'ok') {
            oddPrefix = 'o';
            bet = 'ODD';
        } else if (key === 'ek') {
            oddPrefix = 'e';
            bet = 'EVEN';
        }

        if (!_.isUndefined(bpResponse[type])) {
            oddValue = bpResponse[type][oddPrefix];
            oddValue = convertOdd(priceType, oddValue, oddType);
            if (oddValue !== odd) {
                isOddNotMatch = true;
            }
        } else {
            isOddNotMatch = true;
        }
    } else {
        console.log('oddType not match')
    }
    console.log('input_odd      : ', odd + '  , oddValue : ' + oddValue);
    console.log('input_handicap : ', handicap + '  , handicapValue : ' + handicapValue);
    console.log('isOddNotMatch  : ', isOddNotMatch);
    callback(isOddNotMatch, key, oddValue, handicapValue, bet);

}
function checkOddProcess(bpResponse, priceType, oddType, oddKey, odd, handicap, callback) {


    console.log('----- :::::: ', oddType);
    console.log('----- :::::: ', oddKey);

    let handicapPrefix, oddPrefix;
    let type = oddType.toLowerCase();
    let isOddNotMatch = false;
    let oddValue, handicapValue;
    let key = '';
    let bet = '';

    if (['TG', 'DC', 'CS', 'FHCS', 'FTHT', 'FGLG', 'TTKO', 'TC'].includes(oddType.toUpperCase())) {
        key = oddKey;
    }

    if (oddType === 'AH' || oddType === 'AH1ST') {

        _Utils.findKeyByValue(bpResponse[type], oddKey, function (keyResponse) {
            key = keyResponse;
        });

        if (key === 'hpk') {
            handicapPrefix = 'h';
            oddPrefix = 'hp';
            bet = 'HOME';
        } else {
            handicapPrefix = 'a';
            oddPrefix = 'ap';
            bet = 'AWAY';
        }

        if (!_.isUndefined(bpResponse[type])) {
            console.log(bpResponse[type][oddPrefix] + '  1:  ' + odd);

            oddValue = bpResponse[type][oddPrefix];
            oddValue = convertOdd(priceType, oddValue, oddType);
            handicapValue = bpResponse[type][handicapPrefix];
            if (oddValue !== odd) {
                isOddNotMatch = true;
            }
            if (handicapValue !== handicap) {
                isOddNotMatch = true;
            }
        } else {
            isOddNotMatch = true;
        }

    } else if (oddType === 'OU' || oddType === 'OU1ST' || oddType === 'T1OU' || oddType === 'T2OU') {

        _Utils.findKeyByValue(bpResponse[type], oddKey, function (keyResponse) {
            key = keyResponse;
        });


        if (key === 'opk') {
            handicapPrefix = 'o';
            oddPrefix = 'op';
            bet = 'OVER';
        } else {
            handicapPrefix = 'u';
            oddPrefix = 'up';
            bet = 'UNDER';
        }

        if (!_.isUndefined(bpResponse[type])) {
            oddValue = bpResponse[type][oddPrefix];
            oddValue = convertOdd(priceType, oddValue, oddType);
            handicapValue = bpResponse[type][handicapPrefix];
            if (oddValue !== odd) {
                isOddNotMatch = true;
            }
            if (handicapValue !== handicap) {
                isOddNotMatch = true;
            }
        } else {
            isOddNotMatch = true;
        }

    } else if (oddType === 'X12' || oddType === 'X121ST' || oddType === 'ML') {

        _Utils.findKeyByValue(bpResponse[type], oddKey, function (keyResponse) {
            key = keyResponse;
        });

        if (key === 'hk') {
            oddPrefix = 'h';
            bet = 'HOME';
        } else if (key === 'ak') {
            oddPrefix = 'a';
            bet = 'AWAY';
        } else if (key === 'dk') {
            oddPrefix = 'd';
            bet = 'DRAW';
        }

        if (!_.isUndefined(bpResponse[type])) {
            oddValue = bpResponse[type][oddPrefix];
            oddValue = convertOdd(priceType, oddValue, oddType);
            if (oddValue !== odd) {
                isOddNotMatch = true;
            }
        } else {
            isOddNotMatch = true;
        }

    } else if (oddType === 'OE' || oddType === 'OE1ST') {

        _Utils.findKeyByValue(bpResponse[type], oddKey, function (keyResponse) {
            key = keyResponse;
        });

        if (key === 'ok') {
            oddPrefix = 'o';
            bet = 'ODD';
        } else if (key === 'ek') {
            oddPrefix = 'e';
            bet = 'EVEN';
        }

        if (!_.isUndefined(bpResponse[type])) {
            oddValue = bpResponse[type][oddPrefix];
            oddValue = convertOdd(priceType, oddValue, oddType);
            if (oddValue !== odd) {
                isOddNotMatch = true;
            }
        } else {
            isOddNotMatch = true;
        }
    } else if (oddType === 'OE' || oddType === 'OE1ST') {

        _Utils.findKeyByValue(bpResponse[type], oddKey, function (keyResponse) {
            key = keyResponse;
        });

        if (key === 'ok') {
            oddPrefix = 'o';
            bet = 'ODD';
        } else if (key === 'ek') {
            oddPrefix = 'e';
            bet = 'EVEN';
        }

        if (!_.isUndefined(bpResponse[type])) {
            oddValue = bpResponse[type][oddPrefix];
            oddValue = convertOdd(priceType, oddValue, oddType);
            if (oddValue !== odd) {
                isOddNotMatch = true;
            }
        } else {
            isOddNotMatch = true;
        }
    } else if (oddType === 'TG') {
        if (key === 'tg01k') {
            bet = 'TG01';
        } else if (key === 'tg02k') {
            bet = 'TG23';
        } else if (key === 'tg03k') {
            bet = 'TG46';
        } else {
            bet = 'TG7OVER';
        }
        oddValue = odd;
        handicapValue = handicap;
        isOddNotMatch = false;

    } else if (oddType === 'DC') {
        if (key === 'hdk') {
            bet = 'DC1X';
        } else if (key === 'hak') {
            bet = 'DC12';
        } else {
            bet = 'DC2X';
        }
        oddValue = odd;
        handicapValue = handicap;
        isOddNotMatch = false;

    } else if (oddType === 'CS' || oddType === 'FHCS') {
        if (key === 'd00k') {
            bet = 'CS00';
        } else if (key === 'a01k') {
            bet = 'CS01';
        } else if (key === 'a02k') {
            bet = 'CS02';
        } else if (key === 'a03k') {
            bet = 'CS03';
        } else if (key === 'a04k') {
            bet = 'CS04';
        } else if (key === 'h10k') {
            bet = 'CS10';
        } else if (key === 'd11k') {
            bet = 'CS11';
        } else if (key === 'a12k') {
            bet = 'CS12';
        } else if (key === 'a13k') {
            bet = 'CS13';
        } else if (key === 'a14k') {
            bet = 'CS14';
        } else if (key === 'h20k') {
            bet = 'CS20';
        } else if (key === 'h21k') {
            bet = 'CS21';
        } else if (key === 'd22k') {
            bet = 'CS22';
        } else if (key === 'a23k') {
            bet = 'CS23';
        } else if (key === 'a24k') {
            bet = 'CS24';
        } else if (key === 'h30k') {
            bet = 'CS30';
        } else if (key === 'h31k') {
            bet = 'CS31';
        } else if (key === 'h32k') {
            bet = 'CS32';
        } else if (key === 'd33k') {
            bet = 'CS33';
        } else if (key === 'a34k') {
            bet = 'CS34';
        } else if (key === 'h40k') {
            bet = 'CS40';
        } else if (key === 'h41k') {
            bet = 'CS41';
        } else if (key === 'h42k') {
            bet = 'CS42';
        } else if (key === 'h43k') {
            bet = 'CS43';
        } else if (key === 'd44k') {
            bet = 'CS44';
        } else {
            bet = 'CSAOS';
        }
        oddValue = odd;
        handicapValue = handicap;
        isOddNotMatch = false;

    } else if (oddType === 'FTHT') {
        if (key === 'hhk') {
            bet = 'FTHT_HH';
        } else if (key === 'hdk') {
            bet = 'FTHT_HD';
        } else if (key === 'hak') {
            bet = 'FTHT_HA';
        } else if (key === 'dhk') {
            bet = 'FTHT_DH';
        } else if (key === 'ddk') {
            bet = 'FTHT_DD';
        } else if (key === 'dak') {
            bet = 'FTHT_DA';
        } else if (key === 'ahk') {
            bet = 'FTHT_AH';
        } else if (key === 'adk') {
            bet = 'FTHT_AD';
        } else {
            bet = 'FTHT_AA';
        }
        oddValue = odd;
        handicapValue = handicap;
        isOddNotMatch = false;

    } else if (oddType === 'FGLG') {
        if (key === 'hfk') {
            bet = 'FGLGFH';
        } else if (key === 'hlk') {
            bet = 'FGLGHL';
        } else if (key === 'afk') {
            bet = 'FGLGAF';
        } else if (key === 'alk') {
            bet = 'FGLGAL';
        } else {
            bet = 'FGLGNG';
        }
        oddValue = odd;
        handicapValue = handicap;
        isOddNotMatch = false;

    } else if (oddType === 'TTKO') {
        if (key === 'hk') {
            bet = 'HOME';
        } else {
            bet = 'AWAY';
        }
        oddValue = odd;
        handicapValue = handicap;
        isOddNotMatch = false;

    } else {
        console.log('oddType not match')
    }
    console.log('input_odd      : ', odd + '  , oddValue : ' + oddValue);
    console.log('input_handicap : ', handicap + '  , handicapValue : ' + handicapValue);
    console.log('isOddNotMatch  : ', isOddNotMatch);
    callback(isOddNotMatch, key, oddValue, handicapValue, bet);

}
function prepareCommission(memberInfo, body, currentAgent, overAgent, remaining, overAllShare) {


    if (currentAgent && (currentAgent.type === 'SUPER_ADMIN' || currentAgent.parentId)) {


        console.log('==============================================');
        console.log('process : ' + currentAgent.type + ' : ' + currentAgent.name);
        console.log('currenctAgent : ',currentAgent)


        let currentAgentShareSettingParent;
        let currentAgentShareSettingOwn;
        let currentAgentShareSettingRemaining;
        let currentAgentShareSettingMin;

        let overAgentShareSettingParent;
        let overAgentShareSettingOwn;
        let overAgentShareSettingRemaining;
        let overAgentShareSettingMin;

        let agentCommission;


        if (body.gameType === 'PARLAY') {
            currentAgentShareSettingParent = currentAgent.shareSetting.step.parlay.parent;
            currentAgentShareSettingOwn = currentAgent.shareSetting.step.parlay.own;
            currentAgentShareSettingRemaining = currentAgent.shareSetting.step.parlay.remaining;
            currentAgentShareSettingMin = currentAgent.shareSetting.step.parlay.min;

            if (overAgent) {
                overAgentShareSettingParent = overAgent.shareSetting.step.parlay.parent;
                overAgentShareSettingOwn = overAgent.shareSetting.step.parlay.own;
                overAgentShareSettingRemaining = overAgent.shareSetting.step.parlay.remaining;
                overAgentShareSettingMin = overAgent.shareSetting.step.parlay.min;
            } else {
                overAgent = {};
                overAgent.type = 'member';
                overAgentShareSettingParent = memberInfo.shareSetting.step.parlay.parent;
                overAgentShareSettingOwn = 0;
                overAgentShareSettingRemaining = 0;

                body.commission.member = {};
                body.commission.member.parent = memberInfo.shareSetting.step.parlay.parent;
                body.commission.member.commission = memberInfo.commissionSetting.parlay.com;
            }

            agentCommission = currentAgent.commissionSetting.parlay.com;


        } else if (body.gameType === 'MIX_STEP') {
            currentAgentShareSettingParent = currentAgent.shareSetting.step.step.parent;
            currentAgentShareSettingOwn = currentAgent.shareSetting.step.step.own;
            currentAgentShareSettingRemaining = currentAgent.shareSetting.step.step.remaining;
            currentAgentShareSettingMin = currentAgent.shareSetting.step.step.min;

            let parlayMatchCount = body.parlay.matches.length;

            if (overAgent) {
                overAgentShareSettingParent = overAgent.shareSetting.step.step.parent;
                overAgentShareSettingOwn = overAgent.shareSetting.step.step.own;
                overAgentShareSettingRemaining = overAgent.shareSetting.step.step.remaining;
                overAgentShareSettingMin = overAgent.shareSetting.step.step.min;
            } else {
                overAgent = {};
                overAgent.type = 'member';
                overAgentShareSettingParent = memberInfo.shareSetting.step.step.parent;
                overAgentShareSettingOwn = 0;
                overAgentShareSettingRemaining = 0;

                body.commission.member = {};
                body.commission.member.parent = memberInfo.shareSetting.step.step.parent;


                // if (parlayMatchCount == 2) {
                //     body.commission.member.commission = memberInfo.commissionSetting.step.com2;
                // } else if (parlayMatchCount >= 3 && parlayMatchCount <= 4) {
                //     body.commission.member.commission = memberInfo.commissionSetting.step.com34;
                // } else if (parlayMatchCount >= 5 && parlayMatchCount <= 6) {
                //     body.commission.member.commission = memberInfo.commissionSetting.step.com56;
                // } else if (parlayMatchCount >= 7 && parlayMatchCount <= 8) {
                //     body.commission.member.commission = memberInfo.commissionSetting.step.com78;
                // } else if (parlayMatchCount >= 9 && parlayMatchCount <= 10) {
                //     body.commission.member.commission = memberInfo.commissionSetting.step.com910;
                // } else if (parlayMatchCount >= 11 && parlayMatchCount <= 12) {
                //     body.commission.member.commission = memberInfo.commissionSetting.step.com1112;
                // }else {
                //     body.commission.member.commission = 0;
                // }

                if (parlayMatchCount == 2) {
                    body.commission.member.commission = memberInfo.commissionSetting.step.com2;
                } else if (parlayMatchCount == 3) {
                    body.commission.member.commission = memberInfo.commissionSetting.step.com3;
                } else if (parlayMatchCount == 4) {
                    body.commission.member.commission = memberInfo.commissionSetting.step.com4;
                } else if (parlayMatchCount == 5) {
                    body.commission.member.commission = memberInfo.commissionSetting.step.com5;
                } else if (parlayMatchCount == 6) {
                    body.commission.member.commission = memberInfo.commissionSetting.step.com6;
                } else if (parlayMatchCount == 7) {
                    body.commission.member.commission = memberInfo.commissionSetting.step.com7;
                } else if (parlayMatchCount == 8) {
                    body.commission.member.commission = memberInfo.commissionSetting.step.com8;
                } else if (parlayMatchCount == 9) {
                    body.commission.member.commission = memberInfo.commissionSetting.step.com9;
                } else if (parlayMatchCount == 10) {
                    body.commission.member.commission = memberInfo.commissionSetting.step.com10;
                } else if (parlayMatchCount == 11) {
                    body.commission.member.commission = memberInfo.commissionSetting.step.com11;
                } else if (parlayMatchCount == 12) {
                    body.commission.member.commission = memberInfo.commissionSetting.step.com12;
                } else {
                    body.commission.member.commission = 0;
                }
            }

            // if (parlayMatchCount == 2) {
            //     agentCommission = currentAgent.commissionSetting.step.com2;
            // } else if (parlayMatchCount >= 3 && parlayMatchCount <= 4) {
            //     agentCommission = currentAgent.commissionSetting.step.com34;
            // } else if (parlayMatchCount >= 5 && parlayMatchCount <= 6) {
            //     agentCommission = currentAgent.commissionSetting.step.com56;
            // } else if (parlayMatchCount >= 7 && parlayMatchCount <= 8) {
            //     agentCommission = currentAgent.commissionSetting.step.com78;
            // } else if (parlayMatchCount >= 9 && parlayMatchCount <= 10) {
            //     agentCommission = currentAgent.commissionSetting.step.com910;
            // } else if (parlayMatchCount >= 11 && parlayMatchCount <= 12) {
            //     agentCommission = currentAgent.commissionSetting.step.com1112;
            // }

            if (parlayMatchCount == 2) {
                agentCommission = currentAgent.commissionSetting.step.com2;
            } else if (parlayMatchCount == 3) {
                agentCommission = currentAgent.commissionSetting.step.com3;
            } else if (parlayMatchCount == 4) {
                agentCommission = currentAgent.commissionSetting.step.com4;
            } else if (parlayMatchCount == 5) {
                agentCommission = currentAgent.commissionSetting.step.com5;
            } else if (parlayMatchCount == 6) {
                agentCommission = currentAgent.commissionSetting.step.com6;
            } else if (parlayMatchCount == 7) {
                agentCommission = currentAgent.commissionSetting.step.com7;
            } else if (parlayMatchCount == 8) {
                agentCommission = currentAgent.commissionSetting.step.com8;
            } else if (parlayMatchCount == 9) {
                agentCommission = currentAgent.commissionSetting.step.com9;
            } else if (parlayMatchCount == 10) {
                agentCommission = currentAgent.commissionSetting.step.com10;
            } else if (parlayMatchCount == 11) {
                agentCommission = currentAgent.commissionSetting.step.com11;
            } else if (parlayMatchCount == 12) {
                agentCommission = currentAgent.commissionSetting.step.com12;
            }

        } else {


            if (body.hdp.matchType === 'LIVE') {
                currentAgentShareSettingParent = currentAgent.shareSetting.sportsBook.live.parent;
                currentAgentShareSettingOwn = currentAgent.shareSetting.sportsBook.live.own;
                currentAgentShareSettingRemaining = currentAgent.shareSetting.sportsBook.live.remaining;
                currentAgentShareSettingMin = currentAgent.shareSetting.sportsBook.live.min;

                if (overAgent) {
                    overAgentShareSettingParent = overAgent.shareSetting.sportsBook.live.parent;
                    overAgentShareSettingOwn = overAgent.shareSetting.sportsBook.live.own;
                    overAgentShareSettingRemaining = overAgent.shareSetting.sportsBook.live.remaining;
                    overAgentShareSettingMin = overAgent.shareSetting.sportsBook.live.min;
                } else {
                    overAgent = {};
                    overAgent.type = 'member';
                    overAgentShareSettingParent = memberInfo.shareSetting.sportsBook.live.parent;
                    overAgentShareSettingOwn = 0;
                    overAgentShareSettingRemaining = 0;

                    body.commission.member = {};
                    body.commission.member.parent = memberInfo.shareSetting.sportsBook.live.parent;
                    body.commission.member.commission = memberInfo.commissionSetting.sportsBook.hdpOuOe;
                }
            } else if (body.hdp.matchType === 'NONE_LIVE') {
                currentAgentShareSettingParent = currentAgent.shareSetting.sportsBook.today.parent;
                currentAgentShareSettingOwn = currentAgent.shareSetting.sportsBook.today.own;
                currentAgentShareSettingRemaining = currentAgent.shareSetting.sportsBook.today.remaining;
                currentAgentShareSettingMin = currentAgent.shareSetting.sportsBook.today.min;

                if (overAgent) {
                    overAgentShareSettingParent = overAgent.shareSetting.sportsBook.today.parent;
                    overAgentShareSettingOwn = overAgent.shareSetting.sportsBook.today.own;
                    overAgentShareSettingRemaining = overAgent.shareSetting.sportsBook.today.remaining;
                    overAgentShareSettingMin = overAgent.shareSetting.sportsBook.today.min;
                } else {
                    overAgent = {};
                    overAgent.type = 'member';
                    overAgentShareSettingParent = memberInfo.shareSetting.sportsBook.today.parent;
                    overAgentShareSettingOwn = 0;
                    overAgentShareSettingRemaining = 0;

                    body.commission.member = {};
                    body.commission.member.parent = memberInfo.shareSetting.sportsBook.today.parent;
                    body.commission.member.commission = memberInfo.commissionSetting.sportsBook.hdpOuOe;
                }
            }

            if (['AH', 'OU', 'OE', 'AH1ST', 'OU1ST', 'OE1ST'].includes(body.hdp.oddType.toUpperCase())) {

                if (memberInfo.commissionSetting.sportsBook.typeHdpOuOe === 'A') {
                    agentCommission = currentAgent.commissionSetting.sportsBook.hdpOuOeA;
                }
                if (memberInfo.commissionSetting.sportsBook.typeHdpOuOe === 'B') {
                    agentCommission = currentAgent.commissionSetting.sportsBook.hdpOuOeB;
                }
                if (memberInfo.commissionSetting.sportsBook.typeHdpOuOe === 'C') {
                    agentCommission = currentAgent.commissionSetting.sportsBook.hdpOuOeC;
                }
                if (memberInfo.commissionSetting.sportsBook.typeHdpOuOe === 'D') {
                    agentCommission = currentAgent.commissionSetting.sportsBook.hdpOuOeD;
                }
                if (memberInfo.commissionSetting.sportsBook.typeHdpOuOe === 'E') {
                    agentCommission = currentAgent.commissionSetting.sportsBook.hdpOuOeE;
                }
                if (memberInfo.commissionSetting.sportsBook.typeHdpOuOe === 'F') {
                    agentCommission = currentAgent.commissionSetting.sportsBook.hdpOuOeF;
                }

            } else if (['X12', 'X121ST', 'DC'].includes(body.hdp.oddType.toUpperCase())) {
                body.commission.member.commission = memberInfo.commissionSetting.sportsBook.oneTwoDoubleChance;
                agentCommission = currentAgent.commissionSetting.sportsBook.oneTwoDoubleChance;

            } else {
                body.commission.member.commission = memberInfo.commissionSetting.sportsBook.others;
                agentCommission = currentAgent.commissionSetting.sportsBook.others;
            }
        }

        let money = body.amount;

        console.log('');
        console.log('---- setting ----');
        console.log('ส่วนแบ่งที่ได้มามีทั้งหมด : ' + currentAgentShareSettingOwn + ' %');
        console.log('ขอส่วนแบ่งจาก : ' + overAgent.type + ' ' + overAgentShareSettingParent + ' %');
        console.log('ให้ส่วนแบ่ง : ' + overAgent.type + ' ที่เหลือไป ' + overAgentShareSettingOwn + ' %');
        console.log('remaining จาก : ' + overAgent.type + ' : ' + overAgentShareSettingRemaining + ' %');
        console.log('โดนบังคับเสียขั้นต่ำ : ' + currentAgentShareSettingMin + ' %');
        console.log('---- end setting ----');
        console.log('');


        // console.log('และได้รับ remaining ไว้ : '+overAgent.shareSetting.sportsBook.hdpOuOe.remaining+' %');

        let currentPercentReceive = overAgentShareSettingParent;
        console.log('% ที่ได้ : ' + currentPercentReceive);
        console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');
        let totalRemaining = remaining;

        if (overAgentShareSettingRemaining > 0) {
            // console.log()
            // console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');

            if (overAgentShareSettingRemaining > totalRemaining) {
                console.log('รับ remaining ทั้งหมดไว้: ' + totalRemaining + ' %');
                currentPercentReceive += totalRemaining;
                totalRemaining = 0;
            } else {
                totalRemaining = remaining - overAgentShareSettingRemaining;
                console.log('หักค่า remaining ที่ได้รับมากับที่เซตรับไว้ = ' + remaining + ' - ' + overAgentShareSettingRemaining + ' = ' + totalRemaining);
                currentPercentReceive += overAgentShareSettingRemaining;
            }

        }

        console.log('รวม % ที่ได้รับ : ' + currentPercentReceive);


        let unUseShare = currentAgentShareSettingOwn - (overAgentShareSettingParent + overAgentShareSettingOwn);

        console.log('เหลือส่วนแบ่งที่ไม่ได้ใช้ : ' + unUseShare + ' %');
        totalRemaining = (unUseShare + totalRemaining);
        console.log('ส่วนแบ่งที่ไม่ได้ใช้ บวกกับค่า remaining ท่ี่เหลือจะเป็น : ' + totalRemaining + ' %');

        console.log('จากที่โดนบังคับเสียขั้นต่ำ : ' + currentAgentShareSettingMin + ' %');
        console.log('overAllShare : ', overAllShare)
        if (currentAgentShareSettingMin > 0 && ((overAllShare + currentPercentReceive) < currentAgentShareSettingMin)) {

            if (currentPercentReceive < currentAgentShareSettingMin) {
                console.log('% ที่ได้รับ < ขั้นต่ำที่ถูกตั้ง min ไว้');
                let percent = currentAgentShareSettingMin - (currentPercentReceive + overAllShare);
                console.log('หักออกไป ' + percent + ' % : แล้วไปเพิ่มในส่วนที่ต้องรับผิดชอบ ');
                totalRemaining = totalRemaining - percent;
                if (totalRemaining < 0) {
                    totalRemaining = 0;
                }
                currentPercentReceive += percent;
            }
        } else {
            // currentPercentReceive += totalRemaining;
        }


        if (currentAgent.type === 'SUPER_ADMIN') {
            currentPercentReceive += totalRemaining;
        }
        let getMoney = (money * convertPercent(currentPercentReceive)).toFixed(2);
        overAllShare = overAllShare + currentPercentReceive;

        console.log('ยอดแทง ' + money + ' ได้ทั้งหมด : ' + currentPercentReceive + ' %');

        console.log('รวม % + % remaining  จะเป็นสัดส่วนที่ได้ทั้งหมด = ' + getMoney);
        console.log('ค่าที่จะถูกคืนกลับไป (remaining) : ' + totalRemaining + ' %');
        console.log('จำนวน % ที่ถูกแบ่งไปแล้วทั้งหมด : ', overAllShare);
        console.log('agentCommission : ', agentCommission)

        switch (currentAgent.type) {
            case 'SUPER_ADMIN':
                body.commission.superAdmin = {};
                body.commission.superAdmin.group = currentAgent._id;
                body.commission.superAdmin.parent = currentAgent.shareSetting.casino.sexy.parent;
                body.commission.superAdmin.own = currentAgent.shareSetting.casino.sexy.own;
                body.commission.superAdmin.remaining = currentAgent.shareSetting.casino.sexy.remaining;
                body.commission.superAdmin.min = currentAgent.shareSetting.casino.sexy.min;
                body.commission.superAdmin.shareReceive = Math.abs(currentPercentReceive);
                body.commission.superAdmin.commission = agentCommission;
                body.commission.superAdmin.amount = getMoney;
                break;
            case 'COMPANY':
                body.commission.company = {};
                body.commission.company.parentGroup = currentAgent.parentId;
                body.commission.company.group = currentAgent._id;
                body.commission.company.parent = currentAgentShareSettingParent;
                body.commission.company.own = currentAgentShareSettingOwn;
                body.commission.company.remaining = currentAgentShareSettingRemaining;
                body.commission.company.min = currentAgentShareSettingMin;
                body.commission.company.shareReceive = Math.abs(currentPercentReceive);
                body.commission.company.commission = agentCommission;
                body.commission.company.amount = getMoney;
                break;
            case 'SHARE_HOLDER':
                body.commission.shareHolder = {};
                body.commission.shareHolder.parentGroup = currentAgent.parentId;
                body.commission.shareHolder.group = currentAgent._id;
                body.commission.shareHolder.parent = currentAgentShareSettingParent;
                body.commission.shareHolder.own = currentAgentShareSettingOwn;
                body.commission.shareHolder.remaining = currentAgentShareSettingRemaining;
                body.commission.shareHolder.min = currentAgentShareSettingMin;
                body.commission.shareHolder.shareReceive = currentPercentReceive;
                body.commission.shareHolder.commission = agentCommission;
                body.commission.shareHolder.amount = getMoney;
                break;
            case 'API':
                body.commission.api = {};
                body.commission.api.parentGroup = currentAgent.parentId;
                body.commission.api.group = currentAgent._id;
                body.commission.api.parent = currentAgentShareSettingParent;
                body.commission.api.own = currentAgentShareSettingOwn;
                body.commission.api.remaining = currentAgentShareSettingRemaining;
                body.commission.api.min = currentAgentShareSettingMin;
                body.commission.api.shareReceive = currentPercentReceive;
                body.commission.api.commission = agentCommission;
                body.commission.api.amount = getMoney;
                break;
            case 'SENIOR':
                body.commission.senior = {};
                body.commission.senior.parentGroup = currentAgent.parentId;
                body.commission.senior.group = currentAgent._id;
                body.commission.senior.parent = currentAgentShareSettingParent;
                body.commission.senior.own = currentAgentShareSettingOwn;
                body.commission.senior.remaining = currentAgentShareSettingRemaining;
                body.commission.senior.min = currentAgentShareSettingMin;
                body.commission.senior.shareReceive = currentPercentReceive;
                body.commission.senior.commission = agentCommission;
                body.commission.senior.amount = getMoney;
                break;
            case 'MASTER_AGENT':
                body.commission.masterAgent = {};
                body.commission.masterAgent.parentGroup = currentAgent.parentId;
                body.commission.masterAgent.group = currentAgent._id;
                body.commission.masterAgent.parent = currentAgentShareSettingParent;
                body.commission.masterAgent.own = currentAgentShareSettingOwn;
                body.commission.masterAgent.remaining = currentAgentShareSettingRemaining;
                body.commission.masterAgent.min = currentAgentShareSettingMin;
                body.commission.masterAgent.shareReceive = currentPercentReceive;
                body.commission.masterAgent.commission = agentCommission;
                body.commission.masterAgent.amount = getMoney;
                break;
            case 'AGENT':
                body.commission.agent = {};
                body.commission.agent.parentGroup = currentAgent.parentId;
                body.commission.agent.group = currentAgent._id;
                body.commission.agent.parent = currentAgentShareSettingParent;
                body.commission.agent.own = currentAgentShareSettingOwn;
                body.commission.agent.remaining = currentAgentShareSettingRemaining;
                body.commission.agent.min = currentAgentShareSettingMin;
                body.commission.agent.shareReceive = currentPercentReceive;
                body.commission.agent.commission = agentCommission;
                body.commission.agent.amount = getMoney;
                break;
        }

        prepareCommission(memberInfo, body, currentAgent.parentId, currentAgent, totalRemaining, overAllShare);
    }
}
function convertPercent(percent) {
    return percent / 100
}
function convertOdd(priceType, odd, oddType) {

    if (priceType === 'MY') {
        return odd;
    } else if (priceType === 'HK') {
        if (!odd.includes('-')) {
            return odd;
        }
        odd = odd.replace('-', '');
        odd = parseFloat(odd) * 100;

        let tmp = 100 / odd;
        return tmp.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
    } else if (priceType === 'EU') {
        if (!odd.includes('-')) {
            if (oddType === 'X12' || oddType === 'X121ST') {
                return odd;
            }
            let t = parseFloat(odd) + 1.00;
            if (!t.toString().includes('.')) {
                t = t.toString() + '.00';
                return t;
            } else {

                if (t.toString().length == 3) {
                    t = t.toString() + '0';
                    return t;
                } else {
                    return t.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
                }

            }
        }
        odd = odd.replace('-', '');
        odd = parseFloat(odd) * 100;
        let tmp = (100 / odd) + 1.00;
        return tmp.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];

    } else if (priceType === 'ID') {
        if (oddType === 'X12' || oddType === 'X121ST') {
            return odd;
        }


        let tmp = parseFloat(odd) * -1;
        tmp = 1 / tmp;

        if (!tmp.toString().includes('.')) {
            tmp = tmp.toString() + '.00';
            return tmp;
        } else {

            if (tmp.toString().length == 3) {
                tmp = tmp.toString() + '0';
                return tmp;
            } else {
                return tmp.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
            }

        }
    }
}
const convertMinMaxByCurrency = (value, currency, membercurrency) => {
    // console.log('###########################');
    // console.log('[BF]value      : ', value);
    // console.log('membercurrency : ', membercurrency);

    let minMax = value;
    if (_.isUndefined(membercurrency) || _.isNull(membercurrency)) {
        return minMax;
    } else {
        const predicate = JSON.parse(`{"currencyName":"${membercurrency}"}`);
        const obj = _.chain(currency)
            .findWhere(predicate)
            .pick('currencyTHB')
            .value();
        if (!_.isUndefined(obj.currencyTHB)) {
            // console.log('               : ',obj.currencyTHB);

            if (_.isEqual(membercurrency, "IDR") || _.isEqual(membercurrency, "VND")) {
                minMax = Math.ceil(value * obj.currencyTHB);
            } else {
                minMax = Math.ceil(value / obj.currencyTHB);
            }
            // console.log('[AF]value      : ', minMax);
            // console.log('###########################');
            return minMax;
        } else {
            return minMax;
        }
    }

};

module.exports = router;