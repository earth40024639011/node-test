const express     = require('express');
const router      = express.Router();
const _           = require('underscore');
const parallel    = require("async/parallel");
const waterfall   = require("async/waterfall");
const Joi         = require('joi');
const redis       = require('../../../sbo/1_common/1_redis/1_1_redis');
const httpRequest = require('request');

const FootballSBOModel  = require('../../../../models/footballSBO.model');
const AdjustmentByMatch = require('../../../../models/oddAdjustmentByMatch.model');
const MemberService     = require('../../../../common/memberService');
const Commons           = require('../../../../common/commons');
const LOG       = 'BETTING_EARLY_MARKET';
const CLIENT_NAME      = process.env.CLIENT_NAME || 'SPORTBOOK88';
const bettingSchema = Joi.object().keys({
    id: Joi.string().required(),
    prefix: Joi.string().required(),
    key: Joi.string().required(),
    value:Joi.string().required()
});

router.put('/', (req, res) => {
    console.log(`[${LOG}] ${JSON.stringify(req.body)}`);
    const request = req.body;
    let validate = Joi.validate(request, bettingSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    const {
        _id,
        username,
        currency,
        commissionSetting
    } = req.userInfo;

    const {
        id,
        prefix,
        key,
        value,
    } = request;

    const league_key = parseInt(_.first(id.split(':')));
    const match_key = parseInt(_.last(id.split(':')));
    const prefixLowerCase = prefix.toLowerCase();
    const predicate = JSON.parse(`{"${key}":"${value}"}`);

    parallel([
            (callback) => {
                MemberService.getLimitSetting(_id, (errLimitSetting, dataLimitSetting)=>{
                    if(errLimitSetting){
                        callback(errLimitSetting, null);
                    } else {
                        callback(null, dataLimitSetting.limitSetting);
                    }
                });
            },

            (callback) => {
                MemberService.getOddAdjustment(username, (err, data) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, data);
                    }
                });
            },

            (callback) => {
                waterfall([
                        (callback) => {
                            redis.getByKey('sboEarlyMarket', (error, response) => {
                                if (error) {
                                    callback(error, null);
                                } else {
                                    callback(null, response);
                                }
                            });
                        }
                    ],
                    (error, sboEarlyMarket) => {
                        if (error) {
                            redis.getByKey(`FootballSBOModelEarlyMarket${CLIENT_NAME}`, (error, result) => {
                                if (error || _.isNull(result) || _.isUndefined(result)) {
                                    FootballSBOModel.find({})
                                        .lean()
                                        .select('k n r rl rem rp')
                                        .exec((error, response) => {
                                            if (error) {
                                                callback(error, null);
                                            } else {
                                                redis.setByKey(`FootballSBOModelEarlyMarket${CLIENT_NAME}`, response, (error, result) => {

                                                });
                                                callback(null, response);
                                            }
                                        });
                                } else {
                                    callback(null, result);
                                }
                            });
                        } else {
                            const sboEarlyMarketK = _.chain(sboEarlyMarket)
                                .flatten(true)
                                .pluck('k')
                                .value();

                            redis.getByKey(`FootballSBOModelEarlyMarket${CLIENT_NAME}`, (error, result) => {
                                if (error || _.isNull(result) || _.isUndefined(result)) {
                                    FootballSBOModel.find({
                                        k: {
                                            $in: sboEarlyMarketK
                                        }
                                    })
                                        .lean()
                                        .select('k n r rl rem rp')
                                        .exec((error, response) => {
                                            if (error) {
                                                callback(error, null);
                                            } else {
                                                redis.setByKey(`FootballSBOModelEarlyMarket${CLIENT_NAME}`, response, (error, result) => {

                                                });
                                                callback(null, response);
                                            }
                                        });
                                } else {
                                    callback(null, result);
                                }
                            });
                        }
                    });
            },

            (callback) => {
                redis.getByKey('sboEarlyMarket', (error, response) => {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            },

            (callback) => {
                AdjustmentByMatch.find({
                })
                    .lean()
                    .exec((error, response) => {
                        if (error) {
                            callback(error, null);
                        } else {
                            callback(null, response);
                        }
                    });
            }
        ],
        (error, parallelResult) => {
            if (error) {
                return res.send(
                    {
                        code: 0,
                        message: "error",
                        result: error
                    });
            } else {
                const [
                    limitSetting,
                    oddAdjust,
                    footballSBOModel,
                    sboEarlyMarket,
                    adjustmentByMatchList
                ] = parallelResult;

                const sboObjLeague = _.chain(footballSBOModel)
                    .filter(sboLeague => {
                        return _.isEqual(parseInt(sboLeague.k), league_key)
                    })
                    .first()
                    .value();

                if (_.isUndefined(sboObjLeague)) {
                    redis.deleteByKey(`FootballSBOModel${CLIENT_NAME}`, (error, result) => {

                    });
                    redis.deleteByKey(`FootballSBOModelEarlyMarket${CLIENT_NAME}`, (error, result) => {

                    });
                    httpRequest.get(
                        `http://lb-sbo-master.api-hub.com/external/sboLeague/${league_key}`,
                        (error, result) => {
                            if (error) {
                                console.error(error);
                            } else {
                                const model = JSON.parse(result.body).result;
                                FootballSBOModel.findOne({
                                    'n.en': model.n.en
                                })
                                    .lean()
                                    .exec((err, data) => {
                                        if (err) {
                                            console.log(err);
                                        }  else if (!data) {
                                            const footballSBOModel = new FootballSBOModel(model);
                                            footballSBOModel.save((error, data) => {
                                                if (error) {
                                                    console.error(error);
                                                } else {
                                                    console.log(data);
                                                }
                                            });
                                        } else {
                                            data.k = model.k;
                                            delete data._id;
                                            const footballSBOModel = new FootballSBOModel(data);
                                            footballSBOModel.save((error, data) => {
                                                if (error) {
                                                    console.error(error);
                                                } else {
                                                    console.log(data);
                                                }
                                            });
                                        }
                                    });
                            }
                        });

                    return res.send({
                        message: `sboObjLeague not found ${league_key}`,
                        code: 1007
                    });
                } else {
                    let indexRound;

                    const bp = _.first(_.chain(sboEarlyMarket)
                        .flatten(true)
                        .findWhere(JSON.parse(`{"k":${league_key}}`))
                        .pick('m')
                        .map(obj => {
                            return obj;
                        })
                        .flatten(true)
                        .findWhere(JSON.parse(`{"id":"${league_key}:${match_key}"}`))
                        .pick('bp')
                        .map(obj => {
                            return obj;
                        })
                        .flatten(true)
                        .map(obj => {
                            return obj[prefix]
                        })
                        .filter((obj, index) => {
                            try {
                                const result = _.isEqual(obj[key], value);
                                if (result) {
                                    indexRound = index;
                                }
                                return result;
                            } catch (e) {
                                return false;
                            }
                        })
                        .map(obj => {
                            const bpObj = {};
                            bpObj[prefix] = obj;
                            return bpObj
                        })
                        .value());

                    if( _.isUndefined(bp)){
                        return res.send({
                            message: "resultBP is undefined ",
                            code: 1007
                        });
                    }

                    let odd = _.filter(oddAdjust, obj => {
                        return _.isEqual(obj.matchId, id);
                    });

                    let adjustmentByMatch = _.chain(adjustmentByMatchList)
                        .filter(obj => {
                            // console.log(`match.id ${match.id} obj.id ${obj.id}`);
                            return _.isEqual(id, obj.id)
                        })
                        .first()
                        .value();

                    let ahAdjustmentByMatch = {home: 0, away: 0};
                    let ouAdjustmentByMatch = {o: 0, u: 0};
                    let ah1stAdjustmentByMatch = {home: 0, away: 0};
                    let ou1stAdjustmentByMatch = {o: 0, u: 0};

                    if (!_.isUndefined(adjustmentByMatch)) {
                        const {
                            ah,
                            ah1st,
                            ou,
                            ou1st
                        } = adjustmentByMatch;

                        adjustmentByMatch = {};
                        adjustmentByMatch.ahAdjustmentByMatch = {};
                        adjustmentByMatch.ahAdjustmentByMatch.home = ah.h;
                        adjustmentByMatch.ahAdjustmentByMatch.away = ah.a;

                        adjustmentByMatch.ouAdjustmentByMatch = {};
                        adjustmentByMatch.ouAdjustmentByMatch.o = ou.o;
                        adjustmentByMatch.ouAdjustmentByMatch.u = ou.u;

                        adjustmentByMatch.ah1stAdjustmentByMatch = {};
                        adjustmentByMatch.ah1stAdjustmentByMatch.home = ah1st.h;
                        adjustmentByMatch.ah1stAdjustmentByMatch.away = ah1st.a;

                        adjustmentByMatch.ou1stAdjustmentByMatch = {};
                        adjustmentByMatch.ou1stAdjustmentByMatch.o = ou1st.o;
                        adjustmentByMatch.ou1stAdjustmentByMatch.u = ou1st.u;
                    } else {
                        adjustmentByMatch = {};
                        adjustmentByMatch.ahAdjustmentByMatch = {home: 0, away: 0};
                        adjustmentByMatch.ouAdjustmentByMatch = {o: 0, u: 0};
                        adjustmentByMatch.ah1stAdjustmentByMatch = {home: 0, away: 0};
                        adjustmentByMatch.ou1stAdjustmentByMatch = {o: 0, u: 0};
                    }

                    Commons.switch_type_adjustmentByMatchList(
                        prefixLowerCase,
                        bp,
                        sboObjLeague.rp.hdp,
                        {},
                        Commons.configMatch(),
                        commissionSetting.sportsBook.typeHdpOuOe,
                        odd,
                        adjustmentByMatch,
                        (err, result)=>{
                            if(err){
                                return res.send({message: err, code: 10102});
                            } else {
                                let result_new_array = [result];
                                let betPrice = _.chain(result_new_array)
                                    .flatten(true)
                                    .map(betPrice => {
                                        return _.findWhere(betPrice, predicate);
                                    })
                                    .reject(betPrice => {
                                        return _.isUndefined(betPrice)
                                    })
                                    .map(betPrice => {
                                        return Commons.getBetObjectByKey(prefix, key, betPrice);
                                    })
                                    .first()
                                    .value();

                                Commons.switch_min_max(
                                    prefixLowerCase,
                                    sboObjLeague.rem,
                                    betPrice,
                                    (err, dataResult)=>{

                                        if(_.isEqual(prefixLowerCase, 'x12') || _.isEqual(prefixLowerCase, 'x121st')) {

                                            dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet);
                                            if(dataResult.a){
                                                dataResult.max = dataResult.max / dataResult.a;
                                            } else if(dataResult.h){
                                                dataResult.max = dataResult.max / dataResult.h;
                                            } else if(dataResult.d){
                                                dataResult.max = dataResult.max / dataResult.d;
                                            }
                                        } else {
                                            dataResult.max = Math.min(dataResult.max, limitSetting.sportsBook.hdpOuOe.maxPerBet);
                                        }

                                        Commons.calculateBetPrice(dataResult.max, indexRound, (err, data)=>{
                                            dataResult.max = data;
                                        });

                                        return res.send({
                                            message: "success",
                                            result: dataResult,
                                            code: 0
                                        });
                                    })
                            }
                        });
                }
            }
        });
});

const _currentDate = () => {
    return new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
};
module.exports = router;
