const express     = require('express');
const router      = express.Router();
const _           = require('underscore');
const parallel    = require("async/parallel");
const waterfall   = require("async/waterfall");
const Joi         = require('joi');
const Redis       = require('../1_common/1_redis/1_1_redis');
const httpRequest = require('request');

const FootballSBOModel = require('../../../models/footballSBO.model');
const MemberService    = require('../../../common/memberService');
const Commons          = require('../../../common/commons');
const RoundTo          = require('round-to');

const LOG       = 'MATCH_TODAY';

router.get('/', (req, res) => {
    const now_date = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const {
        _id,
        username,
        currency
    } = req.userInfo;

    parallel([
            (callback) => {
                MemberService.getLimitSetting(_id, (errLimitSetting, dataLimitSetting)=>{
                    if(errLimitSetting){
                        callback(errLimitSetting, null);
                    } else {
                        callback(null, dataLimitSetting.limitSetting);
                    }
                });
            },

            (callback) => {
                MemberService.getOddAdjustment(username, (err, data) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, data);
                    }
                });
            },

            (callback) => {
                Redis.getByKey('sboParlay', (error, result) => {
                    if (error) {
                        callback(null, false);
                    } else {
                        callback(null, result);
                    }
                })
            },

            (callback) => {
                FootballSBOModel.find({})
                    .lean()
                    .sort('sk')
                    .select('k n rp')
                    .exec((error, response) => {
                        if (error) {
                            callback(error, null);
                        } else {
                            callback(null, response);
                        }
                    });
            }
        ],
        (error, parallelResult) => {
            if (error) {
                return res.send({
                    code    : 9999,
                    message : error
                });
            } else {
                const [
                    limitSetting,
                    oddAdjust,
                    sboParlay,
                    footballSBOModel
                ] = parallelResult;

                const configMatch = Commons.configMatch();

                if (!_.isEmpty(sboParlay)) {
                    _.chain(sboParlay)
                        .each(league => {
                            const sboObjLeague = _.chain(footballSBOModel)
                                .filter(sboLeague => {
                                    return _.isEqual(parseInt(sboLeague.k), league.k)
                                })
                                .first()
                                .value();

                            if (!_.isUndefined(sboObjLeague)) {
                                _.each(league.m, match => {

                                    _.each(match.bp, betPrice => {
                                        const odd = _.filter(oddAdjust, obj =>{
                                            return _.isEqual(obj.matchId, match.id);
                                        });

                                        let ou1st = {o: 0, u: 0};
                                        let ou = {o: 0, u: 0};
                                        let oe = {o: 0, e: 0};
                                        let oe1st = {o: 0, e: 0};
                                        let ah = {home: 0, away: 0};
                                        let ah1st = {home: 0, away: 0};

                                        if(!_.isEmpty(odd)){
                                            _.each(odd, o => {
                                                const {
                                                    prefix,
                                                    key,
                                                    value,
                                                    count
                                                } = o;

                                                if(_.isEqual(prefix, 'ah') && !_.isUndefined(betPrice.ah)) {
                                                    // console.log(betPrice.ah.hpk);
                                                    if(_.isEqual(key, 'hpk') && _.isEqual(value, betPrice.ah.hpk)){
                                                        ah = {
                                                            home: -count,
                                                            away: count
                                                        };
                                                        console.log(`       hpk ah => ${JSON.stringify(ah)}`);
                                                    } else if(_.isEqual(key, 'apk') && _.isEqual(value, betPrice.ah.apk)){
                                                        ah = {
                                                            home: count,
                                                            away: -count
                                                        };
                                                        // console.log(`       apk ah => ${JSON.stringify(ah)}`);
                                                    }

                                                } else if(_.isEqual(prefix, 'ah1st') && !_.isUndefined(betPrice.ah1st)) {

                                                    if(_.isEqual(key, 'hpk') && _.isEqual(value, betPrice.ah1st.hpk)){
                                                        ah1st = {
                                                            home: -count,
                                                            away: count
                                                        }
                                                    } else if(_.isEqual(key, 'apk') && _.isEqual(value, betPrice.ah1st.apk)){
                                                        ah1st = {
                                                            home: count,
                                                            away: -count
                                                        }
                                                    }

                                                } else if(_.isEqual(prefix, 'ou') && !_.isUndefined(betPrice.ou)) {

                                                    if(_.isEqual(key, 'opk') && _.isEqual(value, betPrice.ou.opk)){
                                                        ou = {
                                                            o: -count,
                                                            u: count
                                                        }
                                                    } else if(_.isEqual(key, 'upk') && _.isEqual(value, betPrice.ou.upk)){
                                                        ou = {
                                                            o: count,
                                                            u: -count
                                                        }
                                                    }

                                                } else if(_.isEqual(prefix, 'ou1st') && !_.isUndefined(betPrice.ou1st)) {

                                                    if(_.isEqual(key, 'opk') && _.isEqual(value, betPrice.ou1st.opk)){
                                                        ou1st = {
                                                            o: -count,
                                                            u: count
                                                        }
                                                    } else if(_.isEqual(key, 'upk') && _.isEqual(value, betPrice.ou1st.upk)){
                                                        ou1st = {
                                                            o: count,
                                                            u: -count
                                                        }
                                                    }

                                                } else if(_.isEqual(prefix, 'oe') && !_.isUndefined(betPrice.oe)) {

                                                    if(_.isEqual(key, 'ok') && _.isEqual(value, betPrice.oe.ok)){
                                                        oe = {
                                                            o: -count,
                                                            e: count
                                                        }
                                                    } else if(_.isEqual(key, 'ek') && _.isEqual(value, betPrice.oe.ek)){
                                                        oe = {
                                                            o: count,
                                                            e: -count
                                                        }
                                                    }

                                                } else if(_.isEqual(prefix, 'oe1st') && !_.isUndefined(betPrice.oe1st)) {

                                                    if(_.isEqual(key, 'ok') && _.isEqual(value, betPrice.oe1st.ok)){
                                                        oe1st = {
                                                            o: -count,
                                                            e: count
                                                        }
                                                    } else if(_.isEqual(key, 'ek') && _.isEqual(value, betPrice.oe1st.ek)){
                                                        oe1st = {
                                                            o: count,
                                                            e: -count
                                                        }
                                                    }

                                                }
                                            });
                                        }

                                        if(betPrice.ou1st){
                                            if(betPrice.ou1st.op || betPrice.ou1st.up){

                                                Commons.cal_eu_ou_sbo(betPrice.ou1st.op, betPrice.ou1st.up, sboObjLeague.rp.hdp.ou1st.f, sboObjLeague.rp.hdp.ou1st.u, configMatch.ou1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ou1st, (err, data)=>{
                                                    if(data){
                                                        betPrice.ou1st.op = data.op;
                                                        betPrice.ou1st.up = data.up;
                                                    }
                                                })
                                            }
                                        }

                                        // ---------------------------  ou  ----------------------------------
                                        if(betPrice.ou){
                                            if(betPrice.ou.op || betPrice.ou.up){
                                                Commons.cal_eu_ou_sbo(betPrice.ou.op, betPrice.ou.up, sboObjLeague.rp.hdp.ou.f, sboObjLeague.rp.hdp.ou.u, configMatch.ou, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ou, (err, data)=>{
                                                    if(data){
                                                        betPrice.ou.op = data.op;
                                                        betPrice.ou.up = data.up;
                                                    }
                                                })
                                            }
                                        }

                                        // ---------------------------  oe1st  ----------------------------------
                                        if(betPrice.oe1st){
                                            if(betPrice.oe1st.o || betPrice.oe1st.e){
                                                Commons.cal_eu_oe_sbo(betPrice.oe1st.o, betPrice.oe1st.e, sboObjLeague.rp.hdp.oe1st.f, sboObjLeague.rp.hdp.oe1st.u, configMatch.oe1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, oe1st, (err, data)=>{
                                                    if(data){
                                                        betPrice.oe1st.o = data.o;
                                                        betPrice.oe1st.e = data.e;
                                                    }
                                                })
                                            }
                                        }

                                        // ---------------------------  oe  ----------------------------------
                                        if(betPrice.oe){
                                            if(betPrice.oe.o || betPrice.oe.e){
                                                Commons.cal_eu_oe_sbo(betPrice.oe.o, betPrice.oe.e, sboObjLeague.rp.hdp.oe.f, sboObjLeague.rp.hdp.oe.u, configMatch.oe, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, oe, (err, data)=>{
                                                    if(data){
                                                        betPrice.oe.o = data.o;
                                                        betPrice.oe.e = data.e;
                                                    }
                                                })
                                            }
                                        }

                                        // ---------------------------  ah1st  ----------------------------------
                                        if(betPrice.ah1st){
                                            if(betPrice.ah1st.h || betPrice.ah1st.a){
                                                Commons.cal_eu_ah_sbo(betPrice.ah1st.h, betPrice.ah1st.a, betPrice.ah1st.hp, betPrice.ah1st.ap, sboObjLeague.rp.hdp.ah1st.f, sboObjLeague.rp.hdp.ah1st.u, configMatch.ah1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ah1st, (err, data)=>{
                                                    if(data){
                                                        betPrice.ah1st.hp = data.hp;
                                                        betPrice.ah1st.ap = data.ap;
                                                    }
                                                })
                                            }
                                        }

                                        // ---------------------------  ah  ----------------------------------
                                        if(betPrice.ah){
                                            console.log(betPrice.ah);
                                            if(betPrice.ah.h || betPrice.ah.a){
                                                Commons.cal_eu_ah_sbo(betPrice.ah.h, betPrice.ah.a, betPrice.ah.hp, betPrice.ah.ap, sboObjLeague.rp.hdp.ah.f, sboObjLeague.rp.hdp.ah.u, configMatch.ah, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ah, (err, data)=>{
                                                    if(data){
                                                        betPrice.ah.hp = data.hp;
                                                        betPrice.ah.ap = data.ap;
                                                        // console.log('xxxx => ', JSON.stringify(betPrice.ah));
                                                    }
                                                })
                                            }

                                        }

                                        // ---------------------------  x121st  ----------------------------------
                                        if(betPrice.x121st){
                                            if(betPrice.x121st.h || betPrice.x121st.a || betPrice.x121st.d){
                                                Commons.cal_x12(betPrice.x121st.h, betPrice.x121st.a, betPrice.x121st.d, configMatch.x121st, (err, data)=>{
                                                    if(data){
                                                        betPrice.x121st.h = data.h;
                                                        betPrice.x121st.a = data.a;
                                                        betPrice.x121st.d = data.d;
                                                    }
                                                })
                                            }
                                        }

                                        // ---------------------------  x12  ----------------------------------
                                        if(betPrice.x12){
                                            if(betPrice.x12.h || betPrice.x12.a || betPrice.x12.d){
                                                Commons.cal_x12(betPrice.x12.h, betPrice.x12.a, betPrice.x12.d, configMatch.x12, (err, data)=>{
                                                    if(data){
                                                        betPrice.x12.h = data.h;
                                                        betPrice.x12.a = data.a;
                                                        betPrice.x12.d = data.d;
                                                    }
                                                })
                                            }
                                        }
                                    });
                                });
                            } else {
                                httpRequest.get(
                                    `http://lb-sbo-master.api-hub.com/external/sboLeague/${league.k}`,
                                    (error, result) => {
                                        if (error) {
                                            console.error(error);
                                        } else {
                                            const model = JSON.parse(result.body).result;
                                            const footballSBOModel = new FootballSBOModel(model);
                                            footballSBOModel.save((error, data) => {
                                                if (error) {
                                                    console.error(error);
                                                } else {
                                                    console.log(data);
                                                }
                                            });
                                        }
                                    });
                                console.error(`sboObjLeague not found ${league.k}`);
                            }
                        })
                        .value();

                    return res.send({
                        message: "success",
                        result: sboParlay,
                        code: 0
                    });

                } else {
                    return res.send({
                        message: "success",
                        result: [],
                        code: 0
                    });
                }
            }
    });
});

router.get('/more/:id', (req, res) => {
    const id = req.params.id;
    waterfall([
            (callback) => {
                Redis.getByKey('sboMoreToday', (error, response) => {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            }
        ],
        (error, result) => {
            if (error) {
                return res.send({
                    code    : 9999,
                    message : error
                });
            } else {
                const moreToday = _.findWhere(result, JSON.parse(`{"id":"${id}"}`));
                return res.send(
                    {
                        code: 0,
                        message: "success",
                        result: moreToday
                    });
            }
        });
});
const calculateBetPrice = (price, round) => {
    let percent = 0;
    switch (round) {
        case 0:
            percent = 0;
            break;
        case 1:
            percent = 40;
            break;
        case 2:
            percent = 80;
            break;
        case 3:
            percent = 90;
            break;
        case 4:
            percent = 90;
            break;
        case 5:
            percent = 90;
            break;
    }
    return decrease(price, percent);
};

const calculateBetPriceByTime = (price, current, match) => {
    if (price <= 20000) {
        return price;
    } else {
        let diff =(current.getTime() - match.getTime()) / 1000;
        diff /= (60 * 60);

        const gapHour = Math.abs(Math.round(diff));
        let percent = 0;

        if (12 <= gapHour) {
            percent = 70;
        } else if (11 === gapHour || 10 === gapHour || 9 === gapHour || 8 === gapHour || 7 === gapHour) {
            percent = 65;
        } else if (6 === gapHour || 5 === gapHour || 4 === gapHour ) {
            percent = 50;
        } else if (3 === gapHour || 2 === gapHour ) {
            percent = 35;
        } else {
            percent = 0;
        }

        return RoundTo(parseFloat(decrease(price, percent)), 0);
    }
};

const decrease = (price, percent) => {
    return (price * ( (100 - percent) / 100)).toFixed(2);
};

module.exports = router;
