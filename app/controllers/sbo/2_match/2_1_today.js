const express     = require('express');
const router      = express.Router();
const _           = require('underscore');
const parallel    = require("async/parallel");
const waterfall   = require("async/waterfall");
const Joi         = require('joi');
const Redis       = require('../1_common/1_redis/1_1_redis');
const httpRequest = require('request');
const moment      = require('moment');

const FootballSBOModel  = require('../../../models/footballSBO.model');
const MemberService     = require('../../../common/memberService');
const Commons           = require('../../../common/commons');
const RoundTo           = require('round-to');
const AdjustmentByMatch = require('../../../models/oddAdjustmentByMatch.model');
const CloseMatch        = require('../../../models/closeMatch.model');
const LOG       = 'MATCH_TODAY';

const CLIENT_NAME             = process.env.CLIENT_NAME          || 'SPORTBOOK88';
const MORE_ODDS_WHITE_LIST    = process.env.MORE_ODDS_WHITE_LIST || 'tg:dc';
const MORE_SPACIAL_WHITE_LIST = ['AMB_SPORTBOOK', 'SPORTBOOK88', 'DD88BET', 'SB234', 'CSR', 'IFM789'];
router.get('/', (req, res) => {
    const now_date = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const {
        _id,
        username,
        currency
    } = req.userInfo;

    parallel([
            (callback) => {
                MemberService.getLimitSetting(_id, (errLimitSetting, dataLimitSetting)=>{
                    if(errLimitSetting){
                        callback(errLimitSetting, null);
                    } else {
                        callback(null, dataLimitSetting.limitSetting);
                    }
                });
            },

            (callback) => {
                MemberService.getOddAdjustment(username, (err, data) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, data);
                    }
                });
            },

            (callback) => {
                Redis.getByKey('sboToday', (error, result) => {
                    if (error) {
                        callback(null, false);
                    } else {
                        callback(null, result);
                    }
                })
            },

            (callback) => {
                waterfall([
                        (callback) => {
                            Redis.getByKey('sboToday', (error, response) => {
                                if (error) {
                                    callback(error, null);
                                } else {
                                    callback(null, response);
                                }
                            });
                        }
                    ],
                    (error, sboToday) => {
                        if (error) {
                            Redis.getByKey(`FootballSBOModelToday${CLIENT_NAME}`, (error, result) => {
                                if (error || _.isNull(result) || _.isUndefined(result)) {
                                    FootballSBOModel.find({})
                                        .lean()
                                        .sort('sk')
                                        .select('k n r rl rem rp')
                                        .exec((error, response) => {
                                            if (error) {
                                                callback(error, null);
                                            } else {
                                                Redis.setByKey(`FootballSBOModelToday${CLIENT_NAME}`, response, (error, result) => {

                                                });
                                                callback(null, response);
                                            }
                                        });
                                } else {
                                    callback(null, result);
                                }
                            });
                        } else {
                            const sboTodayK = _.chain(sboToday)
                                .flatten(true)
                                .pluck('k')
                                .value();

                            Redis.getByKey(`FootballSBOModelToday${CLIENT_NAME}`, (error, result) => {
                                if (error || _.isNull(result) || _.isUndefined(result)) {
                                    FootballSBOModel.find({
                                        k: {
                                            $in: sboTodayK
                                        }
                                    })
                                        .lean()
                                        .sort('sk')
                                        .select('k n r rl rem rp')
                                        .exec((error, response) => {
                                            if (error) {
                                                callback(error, null);
                                            } else {
                                                Redis.setByKey(`FootballSBOModelToday${CLIENT_NAME}`, response, (error, result) => {

                                                });
                                                callback(null, response);
                                            }
                                        });
                                } else {
                                    callback(null, result);
                                }
                            });
                        }
                    });
            },

            (callback) => {
                Redis.getByKey('sboTodayInfo', (error, result) => {
                    if (error) {
                        callback(null, false);
                    } else {
                        callback(null, result);
                    }
                })
            },

            (callback) => {
                if (_.isEqual(CLIENT_NAME, 'SPORTBOOK88')) {
                    MemberService.getAdjustmentByMatch(username, (err, data) => {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, data);
                            console.log('***** ', data);
                        }
                    });
                } else {

                    AdjustmentByMatch.find({
                    })
                        .lean()
                        .exec((error, response) => {
                            if (error) {
                                callback(error, null);
                            } else {
                                callback(null, response);
                                console.log('***** ', response);
                            }
                        });
                }
            },

            (callback) => {
                CloseMatch.find({
                })
                    .lean()
                    .exec((error, response) => {
                        if (error) {
                            callback(error, null);
                        } else {
                            if (_.isEmpty(response)) {
                                callback(null, []);
                            } else {
                                callback(null, _.chain(response)
                                    .pluck('id')
                                    .flatten(true)
                                    .value());
                            }
                        }
                    });
            }
        ],
        (error, parallelResult) => {
            if (error) {
                return res.send({
                    code    : 9999,
                    message : error
                });
            } else {
                let [
                    limitSetting,
                    oddAdjust,
                    sboToday,
                    footballSBOModel,
                    sboTodayInfo,
                    adjustmentByMatchList,
                    closeMatchList
                ] = parallelResult;
                const now_date = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
                const secondsDiff = moment(now_date).diff(sboTodayInfo.updatedDate, 'seconds');
                const configMatch = Commons.configMatch();

                if (secondsDiff > 90) {
                    return res.send({
                        message: "success",
                        result: [],
                        code: 0
                    });
                } else {
                    if (!_.isEmpty(sboToday)) {
                        _.chain(sboToday)
                            .each(league => {
                                const sboObjLeague = _.chain(footballSBOModel)
                                    .filter(sboLeague => {
                                        return _.isEqual(parseInt(sboLeague.k), league.k)
                                    })
                                    .first()
                                    .value();

                                if (!_.isUndefined(sboObjLeague)) {
                                    league.m = _.filter(league.m, match => {
                                        return new Date(match.d) >= now_date && !_.contains(closeMatchList, match.id);
                                    });

                                    _.each(league.m, match => {
                                        const matchDate = new Date(match.d);

                                        // console.log(adjustmentByMatchList);
                                        const adjustmentByMatch = _.chain(adjustmentByMatchList)
                                            .filter(obj => {
                                                // console.log(`match.id ${match.id} obj.id ${obj.id}`);
                                                return _.isEqual(match.id, obj.id)
                                            })
                                            .first()
                                            .value();

                                        let ahAdjustmentByMatch = {home: 0, away: 0};
                                        let ouAdjustmentByMatch = {o: 0, u: 0};
                                        let ah1stAdjustmentByMatch = {home: 0, away: 0};
                                        let ou1stAdjustmentByMatch = {o: 0, u: 0};

                                        if (!_.isUndefined(adjustmentByMatch)) {
                                            // console.log('#################');
                                            // console.log(adjustmentByMatch);
                                            ahAdjustmentByMatch.home = adjustmentByMatch.ah.h;
                                            ahAdjustmentByMatch.away = adjustmentByMatch.ah.a;
                                            ouAdjustmentByMatch.o = adjustmentByMatch.ou.o;
                                            ouAdjustmentByMatch.u = adjustmentByMatch.ou.u;

                                            ah1stAdjustmentByMatch.home = adjustmentByMatch.ah1st.h;
                                            ah1stAdjustmentByMatch.away = adjustmentByMatch.ah1st.a;
                                            ou1stAdjustmentByMatch.o = adjustmentByMatch.ou1st.o;
                                            ou1stAdjustmentByMatch.u = adjustmentByMatch.ou1st.u;
                                            // console.log(adjustmentByMatch);
                                            // console.log(ou1stAdjustmentByMatch);
                                            // console.log('#################');
                                        }

                                        if (!_.contains(MORE_SPACIAL_WHITE_LIST, CLIENT_NAME)) {
                                            match.moreSize = 0;
                                        }

                                        _.each(match.bp, betPrice => {
                                            const odd = _.filter(oddAdjust, obj =>{
                                                return _.isEqual(obj.matchId, match.id);
                                            });
                                            // console.log(odd);
                                            let ou1st = {o: 0, u: 0};
                                            let ou = {o: 0, u: 0};
                                            let oe = {o: 0, e: 0};
                                            let oe1st = {o: 0, e: 0};
                                            let ah = {home: 0, away: 0};
                                            let ah1st = {home: 0, away: 0};

                                            // console.log(`============== ${match.id} =============`);
                                            // console.log(`ah => ${JSON.stringify(ah)}`);
                                            // console.log(`ah1st => ${JSON.stringify(ah1st)}`);
                                            // console.log(`ou => ${JSON.stringify(ou)}`);
                                            // console.log(`ou1st => ${JSON.stringify(ou1st)}`);
                                            // console.log(`oe => ${JSON.stringify(oe)}`);
                                            // console.log(`oe1st => ${JSON.stringify(oe1st)}`);

                                            if(!_.isEmpty(odd)){
                                                _.each(odd, o => {
                                                    const {
                                                        prefix,
                                                        key,
                                                        value,
                                                        count
                                                    } = o;

                                                    // console.log(`   prefix => ${JSON.stringify(prefix)}`);
                                                    // console.log(`   key => ${JSON.stringify(key)}`);
                                                    // console.log(`   value => ${JSON.stringify(value)}`);
                                                    // console.log(`   count => ${JSON.stringify(count)}`);
                                                    if(_.isEqual(prefix, 'ah') && !_.isUndefined(betPrice.ah)) {
                                                        // console.log(betPrice.ah.hpk);
                                                        if(_.isEqual(key, 'hpk') && _.isEqual(value, betPrice.ah.hpk)){
                                                            ah = {
                                                                home: -count,
                                                                away: count
                                                            };
                                                            // console.log(`       hpk ah => ${JSON.stringify(ah)}`);
                                                        } else if(_.isEqual(key, 'apk') && _.isEqual(value, betPrice.ah.apk)){
                                                            ah = {
                                                                home: count,
                                                                away: -count
                                                            };
                                                            // console.log(`       apk ah => ${JSON.stringify(ah)}`);
                                                        }

                                                    } else if(_.isEqual(prefix, 'ah1st') && !_.isUndefined(betPrice.ah1st)) {

                                                        if(_.isEqual(key, 'hpk') && _.isEqual(value, betPrice.ah1st.hpk)){
                                                            ah1st = {
                                                                home: -count,
                                                                away: count
                                                            }
                                                        } else if(_.isEqual(key, 'apk') && _.isEqual(value, betPrice.ah1st.apk)){
                                                            ah1st = {
                                                                home: count,
                                                                away: -count
                                                            }
                                                        }

                                                    } else if(_.isEqual(prefix, 'ou') && !_.isUndefined(betPrice.ou)) {

                                                        if(_.isEqual(key, 'opk') && _.isEqual(value, betPrice.ou.opk)){
                                                            ou = {
                                                                o: -count,
                                                                u: count
                                                            }
                                                        } else if(_.isEqual(key, 'upk') && _.isEqual(value, betPrice.ou.upk)){
                                                            ou = {
                                                                o: count,
                                                                u: -count
                                                            }
                                                        }

                                                    } else if(_.isEqual(prefix, 'ou1st') && !_.isUndefined(betPrice.ou1st)) {

                                                        if(_.isEqual(key, 'opk') && _.isEqual(value, betPrice.ou1st.opk)){
                                                            ou1st = {
                                                                o: -count,
                                                                u: count
                                                            }
                                                        } else if(_.isEqual(key, 'upk') && _.isEqual(value, betPrice.ou1st.upk)){
                                                            ou1st = {
                                                                o: count,
                                                                u: -count
                                                            }
                                                        }

                                                    } else if(_.isEqual(prefix, 'oe') && !_.isUndefined(betPrice.oe)) {

                                                        if(_.isEqual(key, 'ok') && _.isEqual(value, betPrice.oe.ok)){
                                                            oe = {
                                                                o: -count,
                                                                e: count
                                                            }
                                                        } else if(_.isEqual(key, 'ek') && _.isEqual(value, betPrice.oe.ek)){
                                                            oe = {
                                                                o: count,
                                                                e: -count
                                                            }
                                                        }

                                                    } else if(_.isEqual(prefix, 'oe1st') && !_.isUndefined(betPrice.oe1st)) {

                                                        if(_.isEqual(key, 'ok') && _.isEqual(value, betPrice.oe1st.ok)){
                                                            oe1st = {
                                                                o: -count,
                                                                e: count
                                                            }
                                                        } else if(_.isEqual(key, 'ek') && _.isEqual(value, betPrice.oe1st.ek)){
                                                            oe1st = {
                                                                o: count,
                                                                e: -count
                                                            }
                                                        }

                                                    }
                                                });
                                            }

                                            // console.log(`ah => ${JSON.stringify(ah)}`);
                                            // console.log(`ah1st => ${JSON.stringify(ah1st)}`);
                                            // console.log(`ou => ${JSON.stringify(ou)}`);
                                            // console.log(`ou1st => ${JSON.stringify(ou1st)}`);
                                            // console.log(`oe => ${JSON.stringify(oe)}`);
                                            // console.log(`oe1st => ${JSON.stringify(oe1st)}`);
                                            // console.log(`================`);

                                            // ---------------------------  ah  ----------------------------------
                                            if(betPrice.ah){
                                                if(betPrice.ah.h || betPrice.ah.a){
                                                    ah = {
                                                        home: ah.home + ahAdjustmentByMatch.home,
                                                        away: ah.away + ahAdjustmentByMatch.away
                                                    };
                                                    Commons.cal_ah(betPrice.ah.h, betPrice.ah.a, betPrice.ah.hp, betPrice.ah.ap, sboObjLeague.rp.hdp.ah.f, sboObjLeague.rp.hdp.ah.u, configMatch.ah, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ah, (err, data)=>{
                                                        if(data){
                                                            betPrice.ah.hp = data.hp;
                                                            betPrice.ah.ap = data.ap;
                                                        }
                                                    })
                                                }
                                            }
                                            // ---------------------------  ah1st  ----------------------------------
                                            if(betPrice.ah1st){
                                                if(betPrice.ah1st.h || betPrice.ah1st.a){
                                                    ah1st = {
                                                        home: ah1st.home + ah1stAdjustmentByMatch.home,
                                                        away: ah1st.away + ah1stAdjustmentByMatch.away
                                                    };
                                                    Commons.cal_ah(betPrice.ah1st.h, betPrice.ah1st.a, betPrice.ah1st.hp, betPrice.ah1st.ap, sboObjLeague.rp.hdp.ah1st.f, sboObjLeague.rp.hdp.ah1st.u, configMatch.ah1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ah1st, (err, data)=>{
                                                        if(data){
                                                            betPrice.ah1st.hp = data.hp;
                                                            betPrice.ah1st.ap = data.ap;
                                                        }
                                                    })
                                                }
                                            }

                                            // ---------------------------  ou  ----------------------------------
                                            if(betPrice.ou){
                                                if(betPrice.ou.op || betPrice.ou.up){
                                                    ou = {
                                                        o: ou.o + ouAdjustmentByMatch.o,
                                                        u: ou.u + ouAdjustmentByMatch.u
                                                    };
                                                    Commons.cal_ou(betPrice.ou.op, betPrice.ou.up, sboObjLeague.rp.hdp.ou.f, sboObjLeague.rp.hdp.ou.u, configMatch.ou, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ou, (err, data)=>{
                                                        if(data){
                                                            betPrice.ou.op = data.op;
                                                            betPrice.ou.up = data.up;
                                                        }
                                                    })
                                                }
                                            }
                                            if(betPrice.ou1st){
                                                if(betPrice.ou1st.op || betPrice.ou1st.up){
                                                    ou1st = {
                                                        o: ou1st.o + ou1stAdjustmentByMatch.o,
                                                        u: ou1st.u + ou1stAdjustmentByMatch.u
                                                    };
                                                    Commons.cal_ou(betPrice.ou1st.op, betPrice.ou1st.up, sboObjLeague.rp.hdp.ou1st.f, sboObjLeague.rp.hdp.ou1st.u, configMatch.ou1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ou1st, (err, data)=>{
                                                        if(data){
                                                            betPrice.ou1st.op = data.op;
                                                            betPrice.ou1st.up = data.up;
                                                        }
                                                    })
                                                }
                                            }



                                            // ---------------------------  oe1st  ----------------------------------
                                            if(betPrice.oe1st){
                                                if(betPrice.oe1st.o || betPrice.oe1st.e){
                                                    Commons.cal_oe(betPrice.oe1st.o, betPrice.oe1st.e, sboObjLeague.rp.hdp.oe1st.f, sboObjLeague.rp.hdp.oe1st.u, configMatch.oe1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, oe1st, (err, data)=>{
                                                        if(data){
                                                            betPrice.oe1st.o = data.o;
                                                            betPrice.oe1st.e = data.e;
                                                        }
                                                    })
                                                }
                                            }

                                            // ---------------------------  oe  ----------------------------------
                                            if(betPrice.oe){
                                                if(betPrice.oe.o || betPrice.oe.e){
                                                    Commons.cal_oe(betPrice.oe.o, betPrice.oe.e, sboObjLeague.rp.hdp.oe.f, sboObjLeague.rp.hdp.oe.u, configMatch.oe, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, oe, (err, data)=>{
                                                        if(data){
                                                            betPrice.oe.o = data.o;
                                                            betPrice.oe.e = data.e;
                                                        }
                                                    })
                                                }
                                            }

                                            // ---------------------------  x121st  ----------------------------------
                                            if(betPrice.x121st){
                                                if(betPrice.x121st.h || betPrice.x121st.a || betPrice.x121st.d){
                                                    Commons.cal_x12(betPrice.x121st.h, betPrice.x121st.a, betPrice.x121st.d, configMatch.x121st, (err, data)=>{
                                                        if(data){
                                                            betPrice.x121st.h = data.h;
                                                            betPrice.x121st.a = data.a;
                                                            betPrice.x121st.d = data.d;
                                                        }
                                                    })
                                                }
                                            }

                                            // ---------------------------  x12  ----------------------------------
                                            if(betPrice.x12){
                                                if(betPrice.x12.h || betPrice.x12.a || betPrice.x12.d){
                                                    Commons.cal_x12(betPrice.x12.h, betPrice.x12.a, betPrice.x12.d, configMatch.x12, (err, data)=>{
                                                        if(data){
                                                            betPrice.x12.h = data.h;
                                                            betPrice.x12.a = data.a;
                                                            betPrice.x12.d = data.d;
                                                        }
                                                    })
                                                }
                                            }
                                        });

                                        const {
                                            ah,
                                            ou,
                                            x12,
                                            oe,
                                            ah1st,
                                            ou1st,
                                            x121st,
                                            oe1st
                                        } = sboObjLeague.r;
                                        _.each(match.bp, (betPrice, round) => {
                                            betPrice.r = Commons.rule();
                                            betPrice.r.ah.ma = calculateBetPriceByTime(calculateBetPrice(Math.min(ah.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round), now_date, matchDate);
                                            betPrice.r.ah.mi = ah.mi;

                                            betPrice.r.ou.ma = calculateBetPriceByTime(calculateBetPrice(Math.min(ou.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round), now_date, matchDate);
                                            betPrice.r.ou.mi = ou.mi;

                                            betPrice.r.x12.ma = calculateBetPriceByTime(calculateBetPrice(Math.min(x12.ma, limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet), round), now_date, matchDate);
                                            betPrice.r.x12.mi = x12.mi;

                                            betPrice.r.oe.ma = calculateBetPriceByTime(calculateBetPrice(Math.min(oe.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round), now_date, matchDate);
                                            betPrice.r.oe.mi = oe.mi;

                                            betPrice.r.ah1st.ma = calculateBetPriceByTime(calculateBetPrice(Math.min(ah1st.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round), now_date, matchDate);
                                            betPrice.r.ah1st.mi = ah1st.mi;

                                            betPrice.r.ou1st.ma = calculateBetPriceByTime(calculateBetPrice(Math.min(ou1st.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round), now_date, matchDate);
                                            betPrice.r.ou1st.mi = ou1st.mi;

                                            betPrice.r.x121st.ma = calculateBetPriceByTime(calculateBetPrice(Math.min(x121st.ma, limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet), round), now_date, matchDate);
                                            betPrice.r.x121st.mi = x121st.mi;

                                            betPrice.r.oe1st.ma = calculateBetPriceByTime(calculateBetPrice(Math.min(oe1st.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round), now_date, matchDate);
                                            betPrice.r.oe1st.mi = oe1st.mi;
                                        });
                                    });
                                } else {
                                    Redis.deleteByKey(`FootballSBOModel${CLIENT_NAME}`, (error, result) => {

                                    });
                                    Redis.deleteByKey(`FootballSBOModelToday${CLIENT_NAME}`, (error, result) => {

                                    });

                                    httpRequest.get(
                                        `http://lb-sbo-master.api-hub.com/external/sboLeague/${league.k}`,
                                        (error, result) => {
                                            if (error) {
                                                console.error(error);
                                            } else {
                                                const model = JSON.parse(result.body).result;
                                                FootballSBOModel.findOne({
                                                    'n.en': model.n.en
                                                })
                                                    .lean()
                                                    .exec((err, data) => {
                                                        if (err) {
                                                            console.log(err);
                                                        }  else if (!data) {
                                                            const footballSBOModel = new FootballSBOModel(model);
                                                            footballSBOModel.save((error, data) => {
                                                                if (error) {
                                                                    console.error(error);
                                                                } else {
                                                                    console.log(data);
                                                                }
                                                            });
                                                        } else {
                                                            data.k = model.k;
                                                            delete data._id;
                                                            const footballSBOModel = new FootballSBOModel(data);
                                                            footballSBOModel.save((error, data) => {
                                                                if (error) {
                                                                    console.error(error);
                                                                } else {
                                                                    console.log(data);
                                                                }
                                                            });
                                                        }
                                                    });
                                            }
                                        });
                                    console.error(`sboObjLeague not found ${league.k}`);
                                }
                            })
                            .value();

                        sboToday = _.reject(sboToday, league => {
                            return _.size(league.m) === 0;
                        });
                        return res.send({
                            message: "success",
                            result: sboToday,
                            code: 0
                        });

                    } else {
                        return res.send({
                            message: "success",
                            result: [],
                            code: 0
                        });
                    }
                }
            }
    });
});

router.get('/q', (req, res) => {
    const now_date = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const {
        _id,
        username,
        currency
    } = req.userInfo;

    parallel([
            (callback) => {
                MemberService.getLimitSetting(_id, (errLimitSetting, dataLimitSetting)=>{
                    if(errLimitSetting){
                        callback(errLimitSetting, null);
                    } else {
                        callback(null, dataLimitSetting.limitSetting);
                    }
                });
            },

            (callback) => {
                MemberService.getOddAdjustment(username, (err, data) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, data);
                    }
                });
            },

            (callback) => {
                Redis.getByKey('sboToday', (error, result) => {
                    if (error) {
                        callback(null, false);
                    } else {
                        callback(null, result);
                    }
                })
            },

            (callback) => {
                waterfall([
                        (callback) => {
                            Redis.getByKey('sboToday', (error, response) => {
                                if (error) {
                                    callback(error, null);
                                } else {
                                    callback(null, response);
                                }
                            });
                        }
                    ],
                    (error, sboToday) => {
                        if (error) {
                            Redis.getByKey(`FootballSBOModelToday${CLIENT_NAME}`, (error, result) => {
                                if (error || _.isNull(result) || _.isUndefined(result)) {
                                    FootballSBOModel.find({})
                                        .lean()
                                        .select('k n r rl rem rp')
                                        .exec((error, response) => {
                                            if (error) {
                                                callback(error, null);
                                            } else {
                                                Redis.setByKey(`FootballSBOModelToday${CLIENT_NAME}`, response, (error, result) => {

                                                });
                                                callback(null, response);
                                            }
                                        });
                                } else {
                                    callback(null, result);
                                }
                            });
                        } else {
                            const sboTodayK = _.chain(sboToday)
                                .flatten(true)
                                .pluck('k')
                                .value();

                            Redis.getByKey(`FootballSBOModelToday${CLIENT_NAME}`, (error, result) => {
                                if (error || _.isNull(result) || _.isUndefined(result)) {
                                    FootballSBOModel.find({
                                        k: {
                                            $in: sboTodayK
                                        }
                                    })
                                        .lean()
                                        .select('k n r rl rem rp')
                                        .exec((error, response) => {
                                            if (error) {
                                                callback(error, null);
                                            } else {
                                                Redis.setByKey(`FootballSBOModelToday${CLIENT_NAME}`, response, (error, result) => {

                                                });
                                                callback(null, response);
                                            }
                                        });
                                } else {
                                    callback(null, result);
                                }
                            });
                        }
                    });
            },

            (callback) => {
                Redis.getByKey('sboTodayInfo', (error, result) => {
                    if (error) {
                        callback(null, false);
                    } else {
                        callback(null, result);
                    }
                })
            },

            (callback) => {
                AdjustmentByMatch.find({
                })
                    .lean()
                    .exec((error, response) => {
                        if (error) {
                            callback(error, null);
                        } else {
                            callback(null, response);
                        }
                    });
            },

            (callback) => {
                CloseMatch.find({
                })
                    .lean()
                    .exec((error, response) => {
                        if (error) {
                            callback(error, null);
                        } else {
                            if (_.isEmpty(response)) {
                                callback(null, []);
                            } else {
                                callback(null, _.chain(response)
                                    .pluck('id')
                                    .flatten(true)
                                    .value());
                            }
                        }
                    });
            }
        ],
        (error, parallelResult) => {
            if (error) {
                return res.send({
                    code    : 9999,
                    message : error
                });
            } else {
                let [
                    limitSetting,
                    oddAdjust,
                    sboToday,
                    footballSBOModel,
                    sboTodayInfo,
                    adjustmentByMatchList,
                    closeMatchList
                ] = parallelResult;
                const now_date = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
                const secondsDiff = moment(now_date).diff(sboTodayInfo.updatedDate, 'seconds');
                const configMatch = Commons.configMatch();

                if (secondsDiff > 90) {
                    return res.send({
                        message: "success",
                        result: [],
                        code: 0
                    });
                } else {
                    if (!_.isEmpty(sboToday)) {
                        _.chain(sboToday)
                            .each(league => {
                                const sboObjLeague = _.chain(footballSBOModel)
                                    .filter(sboLeague => {
                                        return _.isEqual(parseInt(sboLeague.k), league.k)
                                    })
                                    .first()
                                    .value();

                                if (!_.isUndefined(sboObjLeague)) {
                                    league.m = _.filter(league.m, match => {
                                        return new Date(match.d) >= now_date && !_.contains(closeMatchList, match.id);
                                    });

                                    _.each(league.m, match => {
                                        const matchDate = new Date(match.d);

                                        // console.log(adjustmentByMatchList);
                                        const adjustmentByMatch = _.chain(adjustmentByMatchList)
                                            .filter(obj => {
                                                // console.log(`match.id ${match.id} obj.id ${obj.id}`);
                                                return _.isEqual(match.id, obj.id)
                                            })
                                            .first()
                                            .value();

                                        let ahAdjustmentByMatch = {home: 0, away: 0};
                                        let ouAdjustmentByMatch = {o: 0, u: 0};
                                        let ah1stAdjustmentByMatch = {home: 0, away: 0};
                                        let ou1stAdjustmentByMatch = {o: 0, u: 0};

                                        if (!_.isUndefined(adjustmentByMatch)) {
                                            // console.log('#################');
                                            // console.log(adjustmentByMatch);
                                            ahAdjustmentByMatch.home = adjustmentByMatch.ah.h;
                                            ahAdjustmentByMatch.away = adjustmentByMatch.ah.a;
                                            ouAdjustmentByMatch.o = adjustmentByMatch.ou.o;
                                            ouAdjustmentByMatch.u = adjustmentByMatch.ou.u;

                                            ah1stAdjustmentByMatch.home = adjustmentByMatch.ah1st.h;
                                            ah1stAdjustmentByMatch.away = adjustmentByMatch.ah1st.a;
                                            ou1stAdjustmentByMatch.o = adjustmentByMatch.ou1st.o;
                                            ou1stAdjustmentByMatch.u = adjustmentByMatch.ou1st.u;
                                            // console.log(adjustmentByMatch);
                                            // console.log(ou1stAdjustmentByMatch);
                                            // console.log('#################');
                                        }

                                        if (!_.contains(MORE_SPACIAL_WHITE_LIST, CLIENT_NAME)) {
                                            match.moreSize = 0;
                                        }

                                        _.each(match.bp, betPrice => {
                                            const odd = _.filter(oddAdjust, obj =>{
                                                return _.isEqual(obj.matchId, match.id);
                                            });
                                            // console.log(odd);
                                            let ou1st = {o: 0, u: 0};
                                            let ou = {o: 0, u: 0};
                                            let oe = {o: 0, e: 0};
                                            let oe1st = {o: 0, e: 0};
                                            let ah = {home: 0, away: 0};
                                            let ah1st = {home: 0, away: 0};

                                            // console.log(`============== ${match.id} =============`);
                                            // console.log(`ah => ${JSON.stringify(ah)}`);
                                            // console.log(`ah1st => ${JSON.stringify(ah1st)}`);
                                            // console.log(`ou => ${JSON.stringify(ou)}`);
                                            // console.log(`ou1st => ${JSON.stringify(ou1st)}`);
                                            // console.log(`oe => ${JSON.stringify(oe)}`);
                                            // console.log(`oe1st => ${JSON.stringify(oe1st)}`);

                                            if(!_.isEmpty(odd)){
                                                _.each(odd, o => {
                                                    const {
                                                        prefix,
                                                        key,
                                                        value,
                                                        count
                                                    } = o;

                                                    // console.log(`   prefix => ${JSON.stringify(prefix)}`);
                                                    // console.log(`   key => ${JSON.stringify(key)}`);
                                                    // console.log(`   value => ${JSON.stringify(value)}`);
                                                    // console.log(`   count => ${JSON.stringify(count)}`);
                                                    if(_.isEqual(prefix, 'ah') && !_.isUndefined(betPrice.ah)) {
                                                        // console.log(betPrice.ah.hpk);
                                                        if(_.isEqual(key, 'hpk') && _.isEqual(value, betPrice.ah.hpk)){
                                                            ah = {
                                                                home: -count,
                                                                away: count
                                                            };
                                                            // console.log(`       hpk ah => ${JSON.stringify(ah)}`);
                                                        } else if(_.isEqual(key, 'apk') && _.isEqual(value, betPrice.ah.apk)){
                                                            ah = {
                                                                home: count,
                                                                away: -count
                                                            };
                                                            // console.log(`       apk ah => ${JSON.stringify(ah)}`);
                                                        }

                                                    } else if(_.isEqual(prefix, 'ah1st') && !_.isUndefined(betPrice.ah1st)) {

                                                        if(_.isEqual(key, 'hpk') && _.isEqual(value, betPrice.ah1st.hpk)){
                                                            ah1st = {
                                                                home: -count,
                                                                away: count
                                                            }
                                                        } else if(_.isEqual(key, 'apk') && _.isEqual(value, betPrice.ah1st.apk)){
                                                            ah1st = {
                                                                home: count,
                                                                away: -count
                                                            }
                                                        }

                                                    } else if(_.isEqual(prefix, 'ou') && !_.isUndefined(betPrice.ou)) {

                                                        if(_.isEqual(key, 'opk') && _.isEqual(value, betPrice.ou.opk)){
                                                            ou = {
                                                                o: -count,
                                                                u: count
                                                            }
                                                        } else if(_.isEqual(key, 'upk') && _.isEqual(value, betPrice.ou.upk)){
                                                            ou = {
                                                                o: count,
                                                                u: -count
                                                            }
                                                        }

                                                    } else if(_.isEqual(prefix, 'ou1st') && !_.isUndefined(betPrice.ou1st)) {

                                                        if(_.isEqual(key, 'opk') && _.isEqual(value, betPrice.ou1st.opk)){
                                                            ou1st = {
                                                                o: -count,
                                                                u: count
                                                            }
                                                        } else if(_.isEqual(key, 'upk') && _.isEqual(value, betPrice.ou1st.upk)){
                                                            ou1st = {
                                                                o: count,
                                                                u: -count
                                                            }
                                                        }

                                                    } else if(_.isEqual(prefix, 'oe') && !_.isUndefined(betPrice.oe)) {

                                                        if(_.isEqual(key, 'ok') && _.isEqual(value, betPrice.oe.ok)){
                                                            oe = {
                                                                o: -count,
                                                                e: count
                                                            }
                                                        } else if(_.isEqual(key, 'ek') && _.isEqual(value, betPrice.oe.ek)){
                                                            oe = {
                                                                o: count,
                                                                e: -count
                                                            }
                                                        }

                                                    } else if(_.isEqual(prefix, 'oe1st') && !_.isUndefined(betPrice.oe1st)) {

                                                        if(_.isEqual(key, 'ok') && _.isEqual(value, betPrice.oe1st.ok)){
                                                            oe1st = {
                                                                o: -count,
                                                                e: count
                                                            }
                                                        } else if(_.isEqual(key, 'ek') && _.isEqual(value, betPrice.oe1st.ek)){
                                                            oe1st = {
                                                                o: count,
                                                                e: -count
                                                            }
                                                        }

                                                    }
                                                });
                                            }

                                            // console.log(`ah => ${JSON.stringify(ah)}`);
                                            // console.log(`ah1st => ${JSON.stringify(ah1st)}`);
                                            // console.log(`ou => ${JSON.stringify(ou)}`);
                                            // console.log(`ou1st => ${JSON.stringify(ou1st)}`);
                                            // console.log(`oe => ${JSON.stringify(oe)}`);
                                            // console.log(`oe1st => ${JSON.stringify(oe1st)}`);
                                            // console.log(`================`);

                                            // ---------------------------  ah  ----------------------------------
                                            if(betPrice.ah){
                                                if(betPrice.ah.h || betPrice.ah.a){
                                                    ah = {
                                                        home: ah.home + ahAdjustmentByMatch.home,
                                                        away: ah.away + ahAdjustmentByMatch.away
                                                    };
                                                    Commons.cal_ah(betPrice.ah.h, betPrice.ah.a, betPrice.ah.hp, betPrice.ah.ap, sboObjLeague.rp.hdp.ah.f, sboObjLeague.rp.hdp.ah.u, configMatch.ah, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ah, (err, data)=>{
                                                        if(data){
                                                            betPrice.ah.hp = data.hp;
                                                            betPrice.ah.ap = data.ap;
                                                        }
                                                    })
                                                }
                                            }
                                            // ---------------------------  ah1st  ----------------------------------
                                            if(betPrice.ah1st){
                                                if(betPrice.ah1st.h || betPrice.ah1st.a){
                                                    ah1st = {
                                                        home: ah1st.home + ah1stAdjustmentByMatch.home,
                                                        away: ah1st.away + ah1stAdjustmentByMatch.away
                                                    };
                                                    Commons.cal_ah(betPrice.ah1st.h, betPrice.ah1st.a, betPrice.ah1st.hp, betPrice.ah1st.ap, sboObjLeague.rp.hdp.ah1st.f, sboObjLeague.rp.hdp.ah1st.u, configMatch.ah1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ah1st, (err, data)=>{
                                                        if(data){
                                                            betPrice.ah1st.hp = data.hp;
                                                            betPrice.ah1st.ap = data.ap;
                                                        }
                                                    })
                                                }
                                            }

                                            // ---------------------------  ou  ----------------------------------
                                            if(betPrice.ou){
                                                if(betPrice.ou.op || betPrice.ou.up){
                                                    ou = {
                                                        o: ou.o + ouAdjustmentByMatch.o,
                                                        u: ou.u + ouAdjustmentByMatch.u
                                                    };
                                                    Commons.cal_ou(betPrice.ou.op, betPrice.ou.up, sboObjLeague.rp.hdp.ou.f, sboObjLeague.rp.hdp.ou.u, configMatch.ou, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ou, (err, data)=>{
                                                        if(data){
                                                            betPrice.ou.op = data.op;
                                                            betPrice.ou.up = data.up;
                                                        }
                                                    })
                                                }
                                            }
                                            if(betPrice.ou1st){
                                                if(betPrice.ou1st.op || betPrice.ou1st.up){
                                                    ou1st = {
                                                        o: ou1st.o + ou1stAdjustmentByMatch.o,
                                                        u: ou1st.u + ou1stAdjustmentByMatch.u
                                                    };
                                                    Commons.cal_ou(betPrice.ou1st.op, betPrice.ou1st.up, sboObjLeague.rp.hdp.ou1st.f, sboObjLeague.rp.hdp.ou1st.u, configMatch.ou1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ou1st, (err, data)=>{
                                                        if(data){
                                                            betPrice.ou1st.op = data.op;
                                                            betPrice.ou1st.up = data.up;
                                                        }
                                                    })
                                                }
                                            }



                                            // ---------------------------  oe1st  ----------------------------------
                                            if(betPrice.oe1st){
                                                if(betPrice.oe1st.o || betPrice.oe1st.e){
                                                    Commons.cal_oe(betPrice.oe1st.o, betPrice.oe1st.e, sboObjLeague.rp.hdp.oe1st.f, sboObjLeague.rp.hdp.oe1st.u, configMatch.oe1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, oe1st, (err, data)=>{
                                                        if(data){
                                                            betPrice.oe1st.o = data.o;
                                                            betPrice.oe1st.e = data.e;
                                                        }
                                                    })
                                                }
                                            }

                                            // ---------------------------  oe  ----------------------------------
                                            if(betPrice.oe){
                                                if(betPrice.oe.o || betPrice.oe.e){
                                                    Commons.cal_oe(betPrice.oe.o, betPrice.oe.e, sboObjLeague.rp.hdp.oe.f, sboObjLeague.rp.hdp.oe.u, configMatch.oe, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, oe, (err, data)=>{
                                                        if(data){
                                                            betPrice.oe.o = data.o;
                                                            betPrice.oe.e = data.e;
                                                        }
                                                    })
                                                }
                                            }

                                            // ---------------------------  x121st  ----------------------------------
                                            // if(betPrice.x121st){
                                            //     if(betPrice.x121st.h || betPrice.x121st.a || betPrice.x121st.d){
                                            //         Commons.cal_x12(betPrice.x121st.h, betPrice.x121st.a, betPrice.x121st.d, configMatch.x121st, (err, data)=>{
                                            //             if(data){
                                            //                 betPrice.x121st.h = data.h;
                                            //                 betPrice.x121st.a = data.a;
                                            //                 betPrice.x121st.d = data.d;
                                            //             }
                                            //         })
                                            //     }
                                            // }

                                            // ---------------------------  x12  ----------------------------------
                                            // if(betPrice.x12){
                                            //     if(betPrice.x12.h || betPrice.x12.a || betPrice.x12.d){
                                            //         Commons.cal_x12(betPrice.x12.h, betPrice.x12.a, betPrice.x12.d, configMatch.x12, (err, data)=>{
                                            //             if(data){
                                            //                 betPrice.x12.h = data.h;
                                            //                 betPrice.x12.a = data.a;
                                            //                 betPrice.x12.d = data.d;
                                            //             }
                                            //         })
                                            //     }
                                            // }
                                        });

                                        const {
                                            ah,
                                            ou,
                                            x12,
                                            oe,
                                            ah1st,
                                            ou1st,
                                            x121st,
                                            oe1st
                                        } = sboObjLeague.r;
                                        _.each(match.bp, (betPrice, round) => {
                                            betPrice.r = Commons.rule();
                                            betPrice.r.ah.ma = calculateBetPriceByTime(calculateBetPrice(Math.min(ah.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round), now_date, matchDate);
                                            betPrice.r.ah.mi = ah.mi;

                                            betPrice.r.ou.ma = calculateBetPriceByTime(calculateBetPrice(Math.min(ou.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round), now_date, matchDate);
                                            betPrice.r.ou.mi = ou.mi;

                                            // betPrice.r.x12.ma = calculateBetPriceByTime(calculateBetPrice(Math.min(x12.ma, limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet), round), now_date, matchDate);
                                            // betPrice.r.x12.mi = x12.mi;

                                            betPrice.r.oe.ma = calculateBetPriceByTime(calculateBetPrice(Math.min(oe.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round), now_date, matchDate);
                                            betPrice.r.oe.mi = oe.mi;

                                            betPrice.r.ah1st.ma = calculateBetPriceByTime(calculateBetPrice(Math.min(ah1st.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round), now_date, matchDate);
                                            betPrice.r.ah1st.mi = ah1st.mi;

                                            betPrice.r.ou1st.ma = calculateBetPriceByTime(calculateBetPrice(Math.min(ou1st.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round), now_date, matchDate);
                                            betPrice.r.ou1st.mi = ou1st.mi;

                                            // betPrice.r.x121st.ma = calculateBetPriceByTime(calculateBetPrice(Math.min(x121st.ma, limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet), round), now_date, matchDate);
                                            // betPrice.r.x121st.mi = x121st.mi;
                                            //
                                            betPrice.r.oe1st.ma = calculateBetPriceByTime(calculateBetPrice(Math.min(oe1st.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round), now_date, matchDate);
                                            betPrice.r.oe1st.mi = oe1st.mi;

                                            delete betPrice.x121st;
                                            delete betPrice.x12;
                                        });
                                    });
                                } else {
                                    Redis.deleteByKey(`FootballSBOModel${CLIENT_NAME}`, (error, result) => {

                                    });
                                    Redis.deleteByKey(`FootballSBOModelToday${CLIENT_NAME}`, (error, result) => {

                                    });

                                    httpRequest.get(
                                        `http://lb-sbo-master.api-hub.com/external/sboLeague/${league.k}`,
                                        (error, result) => {
                                            if (error) {
                                                console.error(error);
                                            } else {
                                                const model = JSON.parse(result.body).result;
                                                FootballSBOModel.findOne({
                                                    'n.en': model.n.en
                                                })
                                                    .lean()
                                                    .exec((err, data) => {
                                                        if (err) {
                                                            console.log(err);
                                                        }  else if (!data) {
                                                            const footballSBOModel = new FootballSBOModel(model);
                                                            footballSBOModel.save((error, data) => {
                                                                if (error) {
                                                                    console.error(error);
                                                                } else {
                                                                    console.log(data);
                                                                }
                                                            });
                                                        } else {
                                                            data.k = model.k;
                                                            delete data._id;
                                                            const footballSBOModel = new FootballSBOModel(data);
                                                            footballSBOModel.save((error, data) => {
                                                                if (error) {
                                                                    console.error(error);
                                                                } else {
                                                                    console.log(data);
                                                                }
                                                            });
                                                        }
                                                    });
                                            }
                                        });
                                    console.error(`sboObjLeague not found ${league.k}`);
                                }
                            })
                            .value();

                        sboToday = _.reject(sboToday, league => {
                            return _.size(league.m) === 0;
                        });
                        return res.send({
                            message: "success",
                            result: sboToday,
                            code: 0
                        });

                    } else {
                        return res.send({
                            message: "success",
                            result: [],
                            code: 0
                        });
                    }
                }
            }
        });
});

router.get('/more/:id', (req, res) => {
    const id = req.params.id;
    const league_key = parseInt(_.first(id.split(':')));
    const match_key = parseInt(_.last(id.split(':')));

    parallel([
            (callback) => {
                FootballSBOModel.find({})
                    .lean()
                    .sort('sk')
                    .select('k n r')
                    .exec((error, response) => {
                        if (error) {
                            callback(error, null);
                        } else {
                            callback(null, response);
                        }
                    });
            },

            (callback) => {
                Redis.getByKey('sboMoreToday', (error, response) => {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            }
        ],
        (error, parallelResult) => {
            if (error) {
                return res.send(
                    {
                        code: 0,
                        message: "error",
                        result: error
                    });
            } else {
                const [
                    footballSBOModel,
                    sboMoreToday
                ] = parallelResult;

                const sboObjLeague = _.chain(footballSBOModel)
                    .filter(sboLeague => {
                        return _.isEqual(parseInt(sboLeague.k), league_key)
                    })
                    .first()
                    .value();
                if (!_.isUndefined(sboObjLeague) && !_.isUndefined(sboObjLeague.r)) {
                    const moreToday = _.findWhere(sboMoreToday, JSON.parse(`{"id":"${id}"}`));
                    if (!_.isUndefined(moreToday)) {
                        moreToday.r = sboObjLeague.r;

                        const more = _.first(moreToday.more);
                        const whiteList = MORE_ODDS_WHITE_LIST.split(':');

                        if (!_.isEmpty(moreToday.more)) {
                            _.each(Object.keys(_.first(moreToday.more)), key => {
                                if (!_.contains(whiteList, key)) {
                                    console.log(key, ' deleted');
                                    delete more[key];
                                }
                            });
                        }

                        moreToday.more = [more];

                        return res.send(
                            {
                                code: 0,
                                message: "success",
                                result: moreToday
                            });
                    } else {
                        return res.send({
                            code: 999,
                            message: "Data not found",
                            result: []
                        });
                    }

                } else {
                    return res.send({
                        code: 999,
                        message: "Data not found",
                        result: []
                    });
                }
            }
        });
});
const calculateBetPrice = (price, round) => {
    let percent = 0;
    switch (round) {
        case 0:
            percent = 0;
            break;
        case 1:
            percent = 40;
            break;
        case 2:
            percent = 80;
            break;
        case 3:
            percent = 90;
            break;
        case 4:
            percent = 90;
            break;
        case 5:
            percent = 90;
            break;
    }
    return decrease(price, percent);
};

const calculateBetPriceByTime = (price, current, match) => {
    if (price <= 20000) {
        return price;
    } else {
        let diff =(current.getTime() - match.getTime()) / 1000;
        diff /= (60 * 60);

        const gapHour = Math.abs(Math.round(diff));
        let percent = 0;

        if (12 <= gapHour) {
            percent = 70;
        } else if (11 === gapHour || 10 === gapHour || 9 === gapHour || 8 === gapHour || 7 === gapHour) {
            percent = 65;
        } else if (6 === gapHour || 5 === gapHour || 4 === gapHour ) {
            percent = 50;
        } else if (3 === gapHour || 2 === gapHour ) {
            percent = 35;
        } else {
            percent = 0;
        }

        return RoundTo(parseFloat(decrease(price, percent)), 0);
    }
};

const decrease = (price, percent) => {
    return (price * ( (100 - percent) / 100)).toFixed(2);
};

module.exports = router;
