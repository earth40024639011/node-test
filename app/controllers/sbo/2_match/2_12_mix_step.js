const express     = require('express');
const router      = express.Router();
const _           = require('underscore');
const parallel    = require("async/parallel");
const waterfall   = require("async/waterfall");
const Redis       = require('../1_common/1_redis/1_1_redis');
const httpRequest = require('request');
const moment      = require('moment');

const FootballSBOModel   = require('../../../models/footballSBO.model');
const MemberService      = require('../../../common/memberService');
const Commons            = require('../../../common/commons');
const RoundTo            = require('round-to');
const BasketballSBOModel = require('../../../models/basketballSBO.model');
const MuaythaiSBOModel   = require('../../../models/muaythaiSBO.model');
const TennisSBOModel     = require('../../../models/tennisSBO.model');
const ESportSBOModel     = require('../../../models/eSportSBO.model');
const CLIENT_NAME        = process.env.CLIENT_NAME || 'SPORTBOOK88';
const LOG       = 'MIX_STEP';

router.get('/football/live', (req, res) => {
    const {
        _id,
        username,
        currency
    } = req.userInfo;

    if (_.isEqual(CLIENT_NAME, 'CSR') ||
        _.isEqual(CLIENT_NAME, 'IFM789')) {
        return res.send({
            message: "success",
            result: [],
            code: 0
        });
    } else {
        parallel([
                (callback) => {
                    MemberService.getLimitSetting(_id, (errLimitSetting, dataLimitSetting)=>{
                        if(errLimitSetting){
                            callback(errLimitSetting, null);
                        } else {
                            callback(null, dataLimitSetting.limitSetting);
                        }
                    });
                },

                (callback) => {
                    MemberService.getOddAdjustment(username, (err, data) => {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, data);
                        }
                    });
                },

                (callback) => {
                    Redis.getByKey('sboLiveStep', (error, result) => {
                        if (error) {
                            callback(null, false);
                        } else {
                            callback(null, result);
                        }
                    })
                },

                // (callback) => {
                //     Redis.getByKey(`FootballSBOModel${CLIENT_NAME}`, (error, result) => {
                //         if (error || _.isNull(result) || _.isUndefined(result)) {
                //             FootballSBOModel.find({})
                //                 .lean()
                //                 .sort('sk')
                //                 .select('k n r rl rem rp')
                //                 .exec((error, response) => {
                //                     if (error) {
                //                         callback(error, null);
                //                     } else {
                //                         Redis.setByKey(`FootballSBOModel${CLIENT_NAME}`, response, (error, result) => {
                //
                //                         });
                //                         callback(null, response);
                //                     }
                //                 });
                //         } else {
                //             callback(null, result);
                //         }
                //     });
                // },

                (callback) => {
                    waterfall([
                            (callback) => {
                                Redis.getByKey('sboLive', (error, response) => {
                                    if (error) {
                                        callback(error, null);
                                    } else {
                                        callback(null, response);
                                    }
                                });
                            }
                        ],
                        (error, sboLive) => {
                            if (error) {
                                Redis.getByKey(`FootballSBOModelLive${CLIENT_NAME}`, (error, result) => {
                                    if (error || _.isNull(result) || _.isUndefined(result)) {
                                        FootballSBOModel.find({})
                                            .lean()
                                            .select('k n r rl rem rp')
                                            .exec((error, response) => {
                                                if (error) {
                                                    callback(error, null);
                                                } else {
                                                    Redis.setByKey(`FootballSBOModelLive${CLIENT_NAME}`, response, (error, result) => {

                                                    });
                                                    callback(null, response);
                                                }
                                            });
                                    } else {
                                        callback(null, result);
                                    }
                                });
                            } else {
                                const sboLiveK = _.chain(sboLive)
                                    .flatten(true)
                                    .pluck('k')
                                    .value();

                                Redis.getByKey(`FootballSBOModelLive${CLIENT_NAME}`, (error, result) => {
                                    if (error || _.isNull(result) || _.isUndefined(result)) {
                                        FootballSBOModel.find({
                                            k: {
                                                $in: sboLiveK
                                            }
                                        })
                                            .lean()
                                            .select('k n r rl rem rp')
                                            .exec((error, response) => {
                                                if (error) {
                                                    callback(error, null);
                                                } else {
                                                    Redis.setByKey(`FootballSBOModelLive${CLIENT_NAME}`, response, (error, result) => {

                                                    });
                                                    callback(null, response);
                                                }
                                            });
                                    } else {
                                        callback(null, result);
                                    }
                                });
                            }
                        });
                },

                (callback) => {
                    Redis.getByKey('sboLiveInfo', (error, result) => {
                        if (error) {
                            callback(null, false);
                        } else {
                            callback(null, result);
                        }
                    })
                }
            ],
            (error, parallelResult) => {
                if (error) {
                    return res.send({
                        code    : 9999,
                        message : error
                    });
                } else {
                    const [
                        limitSetting,
                        oddAdjust,
                        sboLive,
                        footballSBOModel,
                        sboLiveInfo
                    ] = parallelResult;

                    const now_date = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
                    const secondsDiff = moment(now_date).diff(sboLiveInfo.updatedDate, 'seconds');

                    if (secondsDiff > 9) {
                        return res.send({
                            message: "success",
                            result: [],
                            code: 0
                        });
                    } else {
                        if (!_.isEmpty(sboLive)) {
                            _.chain(sboLive)
                                .each(league => {
                                    const sboObjLeague = _.chain(footballSBOModel)
                                        .filter(sboLeague => {
                                            return _.isEqual(parseInt(sboLeague.k), league.k)
                                        })
                                        .first()
                                        .value();

                                    if (!_.isUndefined(sboObjLeague)) {

                                        const configMatch = Commons.configMatchMixStep("8");

                                        league.m = _.reject(league.m, match => {
                                            if (match.i.mt.includes("Second Half")) {
                                                return match.i.lt.includes("41") ||
                                                    match.i.lt.includes("42") ||
                                                    match.i.lt.includes("43") ||
                                                    match.i.lt.includes("44") ||
                                                    match.i.lt.includes("45") ||
                                                    match.i.lt.includes("+")
                                            } else {
                                                return false;
                                            }
                                        });

                                        league.m = _.reject(league.m, match => {
                                            return match.n.en.h.toUpperCase().includes("(PEN)") ||
                                                match.n.en.a.toUpperCase().includes("(PEN)") ||
                                                match.n.en.h.toUpperCase().includes("(ET)") ||
                                                match.n.en.a.toUpperCase().includes("(ET)") ||
                                                match.n.en.a.toUpperCase().includes("WINNER")
                                        });

                                        _.each(league.m, match => {
                                            let isOUDisable = false;
                                            // console.log(match.i.mt);
                                            // console.log(match.i.lt);
                                            if (match.i.mt.includes("First Half") && (
                                                match.i.lt.includes("35") ||
                                                match.i.lt.includes("36") ||
                                                match.i.lt.includes("37") ||
                                                match.i.lt.includes("38") ||
                                                match.i.lt.includes("39") ||
                                                match.i.lt.includes("40") ||
                                                match.i.lt.includes("41") ||
                                                match.i.lt.includes("42") ||
                                                match.i.lt.includes("43") ||
                                                match.i.lt.includes("44") ||
                                                match.i.lt.includes("45") ||
                                                match.i.lt.includes("+"))) {
                                                isOUDisable = true;
                                            }
                                            _.each(match.bpl, betPrice => {
                                                const odd = _.filter(oddAdjust, obj =>{
                                                    return _.isEqual(obj.matchId, match.id);
                                                });

                                                let ou1st = {o: 0, u: 0};
                                                let ou = {o: 0, u: 0};
                                                let oe = {o: 0, e: 0};
                                                let oe1st = {o: 0, e: 0};
                                                let ah = {home: 0, away: 0};
                                                let ah1st = {home: 0, away: 0};

                                                if(!_.isEmpty(odd)){
                                                    _.each(odd, o => {
                                                        const {
                                                            prefix,
                                                            key,
                                                            value,
                                                            count
                                                        } = o;

                                                        if(_.isEqual(prefix, 'ah') && !_.isUndefined(betPrice.ah)) {
                                                            // console.log(betPrice.ah.hpk);
                                                            if(_.isEqual(key, 'hpk') && _.isEqual(value, betPrice.ah.hpk)){
                                                                ah = {
                                                                    home: -count,
                                                                    away: count
                                                                };
                                                                // console.log(`       hpk ah => ${JSON.stringify(ah)}`);
                                                            } else if(_.isEqual(key, 'apk') && _.isEqual(value, betPrice.ah.apk)){
                                                                ah = {
                                                                    home: count,
                                                                    away: -count
                                                                };
                                                                // console.log(`       apk ah => ${JSON.stringify(ah)}`);
                                                            }

                                                        } else if(_.isEqual(prefix, 'ah1st') && !_.isUndefined(betPrice.ah1st)) {

                                                            if(_.isEqual(key, 'hpk') && _.isEqual(value, betPrice.ah1st.hpk)){
                                                                ah1st = {
                                                                    home: -count,
                                                                    away: count
                                                                }
                                                            } else if(_.isEqual(key, 'apk') && _.isEqual(value, betPrice.ah1st.apk)){
                                                                ah1st = {
                                                                    home: count,
                                                                    away: -count
                                                                }
                                                            }

                                                        } else if(_.isEqual(prefix, 'ou') && !_.isUndefined(betPrice.ou)) {

                                                            if(_.isEqual(key, 'opk') && _.isEqual(value, betPrice.ou.opk)){
                                                                ou = {
                                                                    o: -count,
                                                                    u: count
                                                                }
                                                            } else if(_.isEqual(key, 'upk') && _.isEqual(value, betPrice.ou.upk)){
                                                                ou = {
                                                                    o: count,
                                                                    u: -count
                                                                }
                                                            }

                                                        } else if(_.isEqual(prefix, 'ou1st') && !_.isUndefined(betPrice.ou1st)) {

                                                            if(_.isEqual(key, 'opk') && _.isEqual(value, betPrice.ou1st.opk)){
                                                                ou1st = {
                                                                    o: -count,
                                                                    u: count
                                                                }
                                                            } else if(_.isEqual(key, 'upk') && _.isEqual(value, betPrice.ou1st.upk)){
                                                                ou1st = {
                                                                    o: count,
                                                                    u: -count
                                                                }
                                                            }

                                                        } else if(_.isEqual(prefix, 'oe') && !_.isUndefined(betPrice.oe)) {

                                                            if(_.isEqual(key, 'ok') && _.isEqual(value, betPrice.oe.ok)){
                                                                oe = {
                                                                    o: -count,
                                                                    e: count
                                                                }
                                                            } else if(_.isEqual(key, 'ek') && _.isEqual(value, betPrice.oe.ek)){
                                                                oe = {
                                                                    o: count,
                                                                    e: -count
                                                                }
                                                            }

                                                        } else if(_.isEqual(prefix, 'oe1st') && !_.isUndefined(betPrice.oe1st)) {

                                                            if(_.isEqual(key, 'ok') && _.isEqual(value, betPrice.oe1st.ok)){
                                                                oe1st = {
                                                                    o: -count,
                                                                    e: count
                                                                }
                                                            } else if(_.isEqual(key, 'ek') && _.isEqual(value, betPrice.oe1st.ek)){
                                                                oe1st = {
                                                                    o: count,
                                                                    e: -count
                                                                }
                                                            }

                                                        }
                                                    });
                                                }

                                                if(betPrice.ou1st){
                                                    if(betPrice.ou1st.op || betPrice.ou1st.up){
                                                        Commons.cal_ou(betPrice.ou1st.op, betPrice.ou1st.up, sboObjLeague.rp.live.ou1st.f, sboObjLeague.rp.live.ou1st.u, configMatch.ou1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ou1st, (err, data)=>{
                                                            if(data){
                                                                betPrice.ou1st.op = data.op;
                                                                betPrice.ou1st.up = data.up;

                                                                if (parseFloat(betPrice.ou1st.op) > -0.19 && parseFloat(betPrice.ou1st.op) < 0) {
                                                                    // console.log('^^^^^^^^^^  ou1st op ^^^^^^^^^^^^^^^^');
                                                                    // console.log(betPrice.ou1st.op);
                                                                    // console.log('^^^^^^^^^^  ou1st op ^^^^^^^^^^^^^^^^');
                                                                    betPrice.ou1st.op = '-0.20';
                                                                }

                                                                if (parseFloat(betPrice.ou1st.up) > -0.19 && parseFloat(betPrice.ou1st.up) < 0) {
                                                                    // console.log('^^^^^^^^^^  ou1st up ^^^^^^^^^^^^^^^^');
                                                                    // console.log(betPrice.ou1st.up);
                                                                    // console.log('^^^^^^^^^^  ou1st up ^^^^^^^^^^^^^^^^');
                                                                    betPrice.ou1st.up = '0.20';
                                                                }
                                                            }
                                                        })
                                                    }
                                                }

                                                // ---------------------------  ou  ----------------------------------
                                                if(betPrice.ou){
                                                    if(betPrice.ou.op || betPrice.ou.up){
                                                        Commons.cal_ou(betPrice.ou.op, betPrice.ou.up, sboObjLeague.rp.live.ou.f, sboObjLeague.rp.live.ou.u, configMatch.ou, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ou, (err, data)=>{
                                                            if(data){
                                                                betPrice.ou.op = data.op;
                                                                betPrice.ou.up = data.up;
                                                            }

                                                            if (parseFloat(betPrice.ou.op) > -0.19 && parseFloat(betPrice.ou.op) < 0) {
                                                                // console.log('^^^^^^^^^^  ou op ^^^^^^^^^^^^^^^^');
                                                                // console.log(betPrice.ou.op);
                                                                // console.log('^^^^^^^^^^  ou op ^^^^^^^^^^^^^^^^');
                                                                betPrice.ou.op = '-0.20';
                                                            }

                                                            if (parseFloat(betPrice.ou.up) > -0.19 && parseFloat(betPrice.ou.up) < 0) {
                                                                // console.log('^^^^^^^^^^  ou up ^^^^^^^^^^^^^^^^');
                                                                // console.log(betPrice.ou.up);
                                                                // console.log('^^^^^^^^^^  ou up ^^^^^^^^^^^^^^^^');
                                                                betPrice.ou.up = '0.20';
                                                            }
                                                        })
                                                    }
                                                }

                                                // ---------------------------  oe1st  ----------------------------------
                                                if(betPrice.oe1st){
                                                    if(betPrice.oe1st.o || betPrice.oe1st.e){
                                                        Commons.cal_oe(betPrice.oe1st.o, betPrice.oe1st.e, sboObjLeague.rp.live.oe1st.f, sboObjLeague.rp.live.oe1st.u, configMatch.oe1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, oe1st, (err, data)=>{
                                                            if(data){
                                                                betPrice.oe1st.o = data.o;
                                                                betPrice.oe1st.e = data.e;
                                                            }
                                                        })
                                                    }
                                                }

                                                // ---------------------------  oe  ----------------------------------
                                                if(betPrice.oe){
                                                    if(betPrice.oe.o || betPrice.oe.e){
                                                        Commons.cal_oe(betPrice.oe.o, betPrice.oe.e, sboObjLeague.rp.live.oe.f, sboObjLeague.rp.live.oe.u, configMatch.oe, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, oe, (err, data)=>{
                                                            if(data){
                                                                betPrice.oe.o = data.o;
                                                                betPrice.oe.e = data.e;
                                                            }
                                                        })
                                                    }
                                                }

                                                // ---------------------------  ah1st  ----------------------------------
                                                if(betPrice.ah1st){
                                                    if(betPrice.ah1st.h || betPrice.ah1st.a){
                                                        Commons.cal_ah(betPrice.ah1st.h, betPrice.ah1st.a, betPrice.ah1st.hp, betPrice.ah1st.ap, sboObjLeague.rp.live.ah1st.f, sboObjLeague.rp.live.ah1st.u, configMatch.ah1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ah1st, (err, data)=>{
                                                            if(data){
                                                                betPrice.ah1st.hp = data.hp;
                                                                betPrice.ah1st.ap = data.ap;

                                                                if (parseFloat(betPrice.ah1st.hp) > -0.19 && parseFloat(betPrice.ah1st.hp) < 0) {
                                                                    // console.log('^^^^^^^^^^  ah1st hp ^^^^^^^^^^^^^^^^');
                                                                    // console.log(betPrice.ah1st.hp);
                                                                    // console.log('^^^^^^^^^^  ah1st hp ^^^^^^^^^^^^^^^^');
                                                                    betPrice.ah1st.hp = '-0.20';
                                                                }

                                                                if (parseFloat(betPrice.ah1st.ap) > -0.19 && parseFloat(betPrice.ah1st.ap) < 0) {
                                                                    // console.log('^^^^^^^^^^  ah1st ap ^^^^^^^^^^^^^^^^');
                                                                    // console.log(betPrice.ah1st.ap);
                                                                    // console.log('^^^^^^^^^^  ah1st ap ^^^^^^^^^^^^^^^^');
                                                                    betPrice.ah1st.ap = '-0.20';
                                                                }
                                                            }
                                                        })
                                                    }
                                                }

                                                // ---------------------------  ah  ----------------------------------
                                                if(betPrice.ah){
                                                    if(betPrice.ah.h || betPrice.ah.a){
                                                        Commons.cal_ah(betPrice.ah.h, betPrice.ah.a, betPrice.ah.hp, betPrice.ah.ap, sboObjLeague.rp.live.ah.f, sboObjLeague.rp.live.ah.u, configMatch.ah, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ah, (err, data)=>{
                                                            if(data){

                                                                betPrice.ah.hp = data.hp;
                                                                betPrice.ah.ap = data.ap;

                                                                // console.log(betPrice.ah.hp);
                                                                // console.log(betPrice.ah.ap);

                                                                if (parseFloat(betPrice.ah.hp) > -0.19 && parseFloat(betPrice.ah.hp) < 0) {
                                                                    // console.log('^^^^^^^^^^  ah hp ^^^^^^^^^^^^^^^^');
                                                                    // console.log(betPrice.ah.hp);
                                                                    // console.log('^^^^^^^^^^  ah hp ^^^^^^^^^^^^^^^^');
                                                                    betPrice.ah.hp = '-0.20';
                                                                }

                                                                if (parseFloat(betPrice.ah.ap) > -0.19 && parseFloat(betPrice.ah.ap) < 0) {
                                                                    // console.log('^^^^^^^^^^  ah ap ^^^^^^^^^^^^^^^^');
                                                                    // console.log(betPrice.ah.ap);
                                                                    // console.log('^^^^^^^^^^  ah ap ^^^^^^^^^^^^^^^^');
                                                                    betPrice.ah.ap = '-0.20';
                                                                }

                                                            }
                                                        })
                                                    }
                                                }

                                                // ---------------------------  x121st  ----------------------------------
                                                if(betPrice.x121st){
                                                    if(betPrice.x121st.h || betPrice.x121st.a || betPrice.x121st.d){
                                                        Commons.cal_x12(betPrice.x121st.h, betPrice.x121st.a, betPrice.x121st.d, configMatch.x121st, (err, data)=>{
                                                            if(data){
                                                                // betPrice.x121st.h = data.h;
                                                                // betPrice.x121st.a = data.a;
                                                                // betPrice.x121st.d = data.d;
                                                                betPrice.x121st.h = Math.min(parseFloat(data.h), 9.23);
                                                                betPrice.x121st.a = Math.min(parseFloat(data.a), 9.23);
                                                                betPrice.x121st.d = Math.min(parseFloat(data.d), 9.23);
                                                            }
                                                        })
                                                    }
                                                }

                                                // ---------------------------  x12  ----------------------------------
                                                if(betPrice.x12){
                                                    if(betPrice.x12.h || betPrice.x12.a || betPrice.x12.d){
                                                        Commons.cal_x12(betPrice.x12.h, betPrice.x12.a, betPrice.x12.d, configMatch.x12, (err, data)=>{
                                                            if(data){
                                                                // betPrice.x12.h = data.h;
                                                                // betPrice.x12.a = data.a;
                                                                // betPrice.x12.d = data.d;
                                                                betPrice.x12.h = Math.min(parseFloat(data.h), 9.23);
                                                                betPrice.x12.a = Math.min(parseFloat(data.a), 9.23);
                                                                betPrice.x12.d = Math.min(parseFloat(data.d), 9.23);
                                                            }
                                                        })
                                                    }
                                                }
                                            });

                                            const {
                                                ah,
                                                ou,
                                                x12,
                                                oe,
                                                ah1st,
                                                ou1st,
                                                x121st,
                                                oe1st
                                            } = sboObjLeague.rl;
                                            _.each(match.bpl, (betPrice, round) => {
                                                betPrice.r = Commons.rule();
                                                betPrice.r.ah.ma = calculateBetPrice(Math.min(ah.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                                                betPrice.r.ah.mi = ah.mi;

                                                betPrice.r.ou.ma = calculateBetPrice(Math.min(ou.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                                                betPrice.r.ou.mi = ou.mi;

                                                betPrice.r.x12.ma = calculateBetPrice(Math.min(x12.ma, limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet), round);
                                                betPrice.r.x12.mi = x12.mi;

                                                betPrice.r.oe.ma = calculateBetPrice(Math.min(oe.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                                                betPrice.r.oe.mi = oe.mi;


                                                if (isOUDisable) {
                                                    // console.log(match.n.en.h + ' VS ' + match.n.en.a +'       Deleted');
                                                    delete betPrice.r.ou1st;
                                                    delete betPrice.ou1st;

                                                    delete betPrice.r.x121st;
                                                    delete betPrice.x121st;

                                                    delete betPrice.r.ah1st;
                                                    delete betPrice.ah1st;

                                                    delete betPrice.r.oe1st;
                                                    delete betPrice.oe1st;
                                                } else {
                                                    betPrice.r.ou1st.ma = calculateBetPrice(Math.min(ou1st.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                                                    betPrice.r.ou1st.mi = ou1st.mi;

                                                    betPrice.r.x121st.ma = calculateBetPrice(Math.min(x121st.ma, limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet), round);
                                                    betPrice.r.x121st.mi = x121st.mi;

                                                    betPrice.r.ah1st.ma = calculateBetPrice(Math.min(ah1st.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                                                    betPrice.r.ah1st.mi = ah1st.mi;

                                                    betPrice.r.oe1st.ma = calculateBetPrice(Math.min(oe1st.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round);
                                                    betPrice.r.oe1st.mi = oe1st.mi;
                                                }
                                            });
                                        });
                                    } else {
                                        httpRequest.get(
                                            `http://lb-sbo-master.api-hub.com/external/sboLeague/${league.k}`,
                                            (error, result) => {
                                                if (error) {
                                                    console.error(error);
                                                } else {
                                                    const model = JSON.parse(result.body).result;
                                                    const footballSBOModel = new FootballSBOModel(model);
                                                    footballSBOModel.save((error, data) => {
                                                        if (error) {
                                                            console.error(error);
                                                        } else {
                                                            console.log(data);
                                                        }
                                                    });
                                                }
                                            });
                                        console.error(`sboObjLeague not found ${league.k}`);
                                    }
                                })
                                .value();

                            return res.send({
                                message: "success",
                                result: sboLive,
                                code: 0
                            });
                        } else {
                            return res.send({
                                message: "success",
                                result: [],
                                code: 0
                            });
                        }
                    }
                }
            });
    }
});

router.get('/football/hdp', (req, res) => {
    const now_date = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const {
        _id,
        username,
        currency
    } = req.userInfo;
    if (_.isEqual(CLIENT_NAME, 'IFM789')) {
        return res.send({
            message: "success",
            result: [],
            code: 0
        });
    } else {
        parallel([
                (callback) => {
                    MemberService.getLimitSetting(_id, (errLimitSetting, dataLimitSetting)=>{
                        if(errLimitSetting){
                            callback(errLimitSetting, null);
                        } else {
                            callback(null, dataLimitSetting.limitSetting);
                        }
                    });
                },

                (callback) => {
                    MemberService.getOddAdjustment(username, (err, data) => {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, data);
                        }
                    });
                },

                (callback) => {
                    Redis.getByKey('sboToday', (error, result) => {
                        if (error) {
                            callback(null, false);
                        } else {
                            callback(null, result);
                        }
                    })
                },

                // (callback) => {
                //     Redis.getByKey(`FootballSBOModel${CLIENT_NAME}`, (error, result) => {
                //         if (error || _.isNull(result) || _.isUndefined(result)) {
                //             FootballSBOModel.find({})
                //                 .lean()
                //                 .sort('sk')
                //                 .select('k n r rl rem rp')
                //                 .exec((error, response) => {
                //                     if (error) {
                //                         callback(error, null);
                //                     } else {
                //                         Redis.setByKey(`FootballSBOModel${CLIENT_NAME}`, response, (error, result) => {
                //
                //                         });
                //                         callback(null, response);
                //                     }
                //                 });
                //         } else {
                //             callback(null, result);
                //         }
                //     });
                // },

                (callback) => {
                    waterfall([
                            (callback) => {
                                Redis.getByKey('sboToday', (error, response) => {
                                    if (error) {
                                        callback(error, null);
                                    } else {
                                        callback(null, response);
                                    }
                                });
                            }
                        ],
                        (error, sboToday) => {
                            if (error) {
                                Redis.getByKey(`FootballSBOModelToday${CLIENT_NAME}`, (error, result) => {
                                    if (error || _.isNull(result) || _.isUndefined(result)) {
                                        FootballSBOModel.find({})
                                            .lean()
                                            .select('k n r rl rem rp')
                                            .exec((error, response) => {
                                                if (error) {
                                                    callback(error, null);
                                                } else {
                                                    Redis.setByKey(`FootballSBOModelToday${CLIENT_NAME}`, response, (error, result) => {

                                                    });
                                                    callback(null, response);
                                                }
                                            });
                                    } else {
                                        callback(null, result);
                                    }
                                });
                            } else {
                                const sboTodayK = _.chain(sboToday)
                                    .flatten(true)
                                    .pluck('k')
                                    .value();

                                Redis.getByKey(`FootballSBOModelToday${CLIENT_NAME}`, (error, result) => {
                                    if (error || _.isNull(result) || _.isUndefined(result)) {
                                        FootballSBOModel.find({
                                            k: {
                                                $in: sboTodayK
                                            }
                                        })
                                            .lean()
                                            .select('k n r rl rem rp')
                                            .exec((error, response) => {
                                                if (error) {
                                                    callback(error, null);
                                                } else {
                                                    Redis.setByKey(`FootballSBOModelToday${CLIENT_NAME}`, response, (error, result) => {

                                                    });
                                                    callback(null, response);
                                                }
                                            });
                                    } else {
                                        callback(null, result);
                                    }
                                });
                            }
                        });
                },

                (callback) => {
                    Redis.getByKey('sboTodayInfo', (error, result) => {
                        if (error) {
                            callback(null, false);
                        } else {
                            callback(null, result);
                        }
                    })
                }
            ],
            (error, parallelResult) => {
                if (error) {
                    return res.send({
                        code    : 9999,
                        message : error
                    });
                } else {
                    const [
                        limitSetting,
                        oddAdjust,
                        sboToday,
                        footballSBOModel,
                        sboTodayInfo
                    ] = parallelResult;

                    const now_date = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
                    const secondsDiff = moment(now_date).diff(sboTodayInfo.updatedDate, 'seconds');

                    if (secondsDiff > 90) {
                        return res.send({
                            message: "success",
                            result: [],
                            code: 0
                        });
                    } else {
                        if (!_.isEmpty(sboToday)) {
                            _.chain(sboToday)
                                .each(league => {
                                    const sboObjLeague = _.chain(footballSBOModel)
                                        .filter(sboLeague => {
                                            return _.isEqual(parseInt(sboLeague.k), league.k)
                                        })
                                        .first()
                                        .value();

                                    if (!_.isUndefined(sboObjLeague)) {

                                        const configMatch = Commons.configMatchMixStep("8");

                                        // league.m = _.filter(league.m, match => {
                                        //     return new Date(match.d) >= now_date;
                                        // });

                                        _.each(league.m, match => {

                                            _.each(match.bp, betPrice => {
                                                const odd = _.filter(oddAdjust, obj =>{
                                                    return _.isEqual(obj.matchId, match.id);
                                                });

                                                let ou1st = {o: 0, u: 0};
                                                let ou = {o: 0, u: 0};
                                                let oe = {o: 0, e: 0};
                                                let oe1st = {o: 0, e: 0};
                                                let ah = {home: 0, away: 0};
                                                let ah1st = {home: 0, away: 0};

                                                if(!_.isEmpty(odd)){
                                                    _.each(odd, o => {
                                                        const {
                                                            prefix,
                                                            key,
                                                            value,
                                                            count
                                                        } = o;

                                                        if(_.isEqual(prefix, 'ah') && !_.isUndefined(betPrice.ah)) {
                                                            // console.log(betPrice.ah.hpk);
                                                            if(_.isEqual(key, 'hpk') && _.isEqual(value, betPrice.ah.hpk)){
                                                                ah = {
                                                                    home: -count,
                                                                    away: count
                                                                };
                                                                // console.log(`       hpk ah => ${JSON.stringify(ah)}`);
                                                            } else if(_.isEqual(key, 'apk') && _.isEqual(value, betPrice.ah.apk)){
                                                                ah = {
                                                                    home: count,
                                                                    away: -count
                                                                };
                                                                // console.log(`       apk ah => ${JSON.stringify(ah)}`);
                                                            }

                                                        } else if(_.isEqual(prefix, 'ah1st') && !_.isUndefined(betPrice.ah1st)) {

                                                            if(_.isEqual(key, 'hpk') && _.isEqual(value, betPrice.ah1st.hpk)){
                                                                ah1st = {
                                                                    home: -count,
                                                                    away: count
                                                                }
                                                            } else if(_.isEqual(key, 'apk') && _.isEqual(value, betPrice.ah1st.apk)){
                                                                ah1st = {
                                                                    home: count,
                                                                    away: -count
                                                                }
                                                            }

                                                        } else if(_.isEqual(prefix, 'ou') && !_.isUndefined(betPrice.ou)) {

                                                            if(_.isEqual(key, 'opk') && _.isEqual(value, betPrice.ou.opk)){
                                                                ou = {
                                                                    o: -count,
                                                                    u: count
                                                                }
                                                            } else if(_.isEqual(key, 'upk') && _.isEqual(value, betPrice.ou.upk)){
                                                                ou = {
                                                                    o: count,
                                                                    u: -count
                                                                }
                                                            }

                                                        } else if(_.isEqual(prefix, 'ou1st') && !_.isUndefined(betPrice.ou1st)) {

                                                            if(_.isEqual(key, 'opk') && _.isEqual(value, betPrice.ou1st.opk)){
                                                                ou1st = {
                                                                    o: -count,
                                                                    u: count
                                                                }
                                                            } else if(_.isEqual(key, 'upk') && _.isEqual(value, betPrice.ou1st.upk)){
                                                                ou1st = {
                                                                    o: count,
                                                                    u: -count
                                                                }
                                                            }

                                                        } else if(_.isEqual(prefix, 'oe') && !_.isUndefined(betPrice.oe)) {

                                                            if(_.isEqual(key, 'ok') && _.isEqual(value, betPrice.oe.ok)){
                                                                oe = {
                                                                    o: -count,
                                                                    e: count
                                                                }
                                                            } else if(_.isEqual(key, 'ek') && _.isEqual(value, betPrice.oe.ek)){
                                                                oe = {
                                                                    o: count,
                                                                    e: -count
                                                                }
                                                            }

                                                        } else if(_.isEqual(prefix, 'oe1st') && !_.isUndefined(betPrice.oe1st)) {

                                                            if(_.isEqual(key, 'ok') && _.isEqual(value, betPrice.oe1st.ok)){
                                                                oe1st = {
                                                                    o: -count,
                                                                    e: count
                                                                }
                                                            } else if(_.isEqual(key, 'ek') && _.isEqual(value, betPrice.oe1st.ek)){
                                                                oe1st = {
                                                                    o: count,
                                                                    e: -count
                                                                }
                                                            }

                                                        }
                                                    });
                                                }

                                                if(betPrice.ou1st){
                                                    if(betPrice.ou1st.op || betPrice.ou1st.up){

                                                        Commons.cal_eu_ou(betPrice.ou1st.op, betPrice.ou1st.up, sboObjLeague.rp.hdp.ou1st.f, sboObjLeague.rp.hdp.ou1st.u, configMatch.ou1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ou1st, (err, data)=>{
                                                            if(data){
                                                                betPrice.ou1st.op = data.op;
                                                                betPrice.ou1st.up = data.up;
                                                            }
                                                        })
                                                    }
                                                }

                                                // ---------------------------  ou  ----------------------------------
                                                if(betPrice.ou){
                                                    if(betPrice.ou.op || betPrice.ou.up){
                                                        Commons.cal_eu_ou(betPrice.ou.op, betPrice.ou.up, sboObjLeague.rp.hdp.ou.f, sboObjLeague.rp.hdp.ou.u, configMatch.ou, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ou, (err, data)=>{
                                                            if(data){
                                                                betPrice.ou.op = data.op;
                                                                betPrice.ou.up = data.up;
                                                            }
                                                        })
                                                    }
                                                }

                                                // ---------------------------  oe1st  ----------------------------------
                                                if(betPrice.oe1st){
                                                    if(betPrice.oe1st.o || betPrice.oe1st.e){
                                                        Commons.cal_eu_oe(betPrice.oe1st.o, betPrice.oe1st.e, sboObjLeague.rp.hdp.oe1st.f, sboObjLeague.rp.hdp.oe1st.u, configMatch.oe1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, oe1st, (err, data)=>{
                                                            if(data){
                                                                betPrice.oe1st.o = data.o;
                                                                betPrice.oe1st.e = data.e;
                                                            }
                                                        })
                                                    }
                                                }

                                                // ---------------------------  oe  ----------------------------------
                                                if(betPrice.oe){
                                                    if(betPrice.oe.o || betPrice.oe.e){
                                                        Commons.cal_eu_oe(betPrice.oe.o, betPrice.oe.e, sboObjLeague.rp.hdp.oe.f, sboObjLeague.rp.hdp.oe.u, configMatch.oe, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, oe, (err, data)=>{
                                                            if(data){
                                                                betPrice.oe.o = data.o;
                                                                betPrice.oe.e = data.e;
                                                            }
                                                        })
                                                    }
                                                }

                                                // ---------------------------  ah1st  ----------------------------------
                                                if(betPrice.ah1st){
                                                    if(betPrice.ah1st.h || betPrice.ah1st.a){
                                                        Commons.cal_eu_ah(betPrice.ah1st.h, betPrice.ah1st.a, betPrice.ah1st.hp, betPrice.ah1st.ap, sboObjLeague.rp.hdp.ah1st.f, sboObjLeague.rp.hdp.ah1st.u, configMatch.ah1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ah1st, (err, data)=>{
                                                            if(data){
                                                                betPrice.ah1st.hp = data.hp;
                                                                betPrice.ah1st.ap = data.ap;
                                                            }
                                                        })
                                                    }
                                                }

                                                // ---------------------------  ah  ----------------------------------
                                                if(betPrice.ah){
                                                    // console.log(betPrice.ah);
                                                    if(betPrice.ah.h || betPrice.ah.a){
                                                        Commons.cal_eu_ah(betPrice.ah.h, betPrice.ah.a, betPrice.ah.hp, betPrice.ah.ap, sboObjLeague.rp.hdp.ah.f, sboObjLeague.rp.hdp.ah.u, configMatch.ah, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ah, (err, data)=>{
                                                            if(data){
                                                                betPrice.ah.hp = data.hp;
                                                                betPrice.ah.ap = data.ap;
                                                                // console.log('xxxx => ', JSON.stringify(betPrice.ah));
                                                            }
                                                        })
                                                    }

                                                }

                                                // ---------------------------  x121st  ----------------------------------
                                                if(betPrice.x121st){
                                                    if(betPrice.x121st.h || betPrice.x121st.a || betPrice.x121st.d){
                                                        Commons.cal_x12(betPrice.x121st.h, betPrice.x121st.a, betPrice.x121st.d, configMatch.x121st, (err, data)=>{
                                                            if(data){
                                                                betPrice.x121st.h = data.h;
                                                                betPrice.x121st.a = data.a;
                                                                betPrice.x121st.d = data.d;
                                                            }
                                                        })
                                                    }
                                                }

                                                // ---------------------------  x12  ----------------------------------
                                                if(betPrice.x12){
                                                    if(betPrice.x12.h || betPrice.x12.a || betPrice.x12.d){
                                                        Commons.cal_x12(betPrice.x12.h, betPrice.x12.a, betPrice.x12.d, configMatch.x12, (err, data)=>{
                                                            if(data){
                                                                betPrice.x12.h = data.h;
                                                                betPrice.x12.a = data.a;
                                                                betPrice.x12.d = data.d;
                                                            }
                                                        })
                                                    }
                                                }
                                            });
                                        });
                                    } else {
                                        httpRequest.get(
                                            `http://lb-sbo-master.api-hub.com/external/sboLeague/${league.k}`,
                                            (error, result) => {
                                                if (error) {
                                                    console.error(error);
                                                } else {
                                                    const model = JSON.parse(result.body).result;
                                                    const footballSBOModel = new FootballSBOModel(model);
                                                    footballSBOModel.save((error, data) => {
                                                        if (error) {
                                                            console.error(error);
                                                        } else {
                                                            console.log(data);
                                                        }
                                                    });
                                                }
                                            });
                                        console.error(`sboObjLeague not found ${league.k}`);
                                    }
                                })
                                .value();

                            return res.send({
                                message: "success",
                                result: sboToday,
                                code: 0
                            });

                        } else {
                            return res.send({
                                message: "success",
                                result: [],
                                code: 0
                            });
                        }
                    }
                }
            });
    }
});

router.get('/basketball/hdp', (req, res) => {
    const now_date = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const {
        _id,
        username,
        currency
    } = req.userInfo;
    if (_.isEqual(CLIENT_NAME, 'IFM789')) {
        return res.send({
            message: "success",
            result: [],
            code: 0
        });
    } else {
        parallel([
                (callback) => {
                    MemberService.getLimitSetting(_id, (errLimitSetting, dataLimitSetting)=>{
                        if(errLimitSetting){
                            callback(errLimitSetting, null);
                        } else {
                            callback(null, dataLimitSetting.limitSetting);
                        }
                    });
                },

                (callback) => {
                    Redis.getByKey('sboTodayBasketball', (error, result) => {
                        if (error) {
                            callback(null, false);
                        } else {
                            callback(null, result);
                        }
                    })
                },

                (callback) => {
                    Redis.getByKey(`BasketballSBOModel${CLIENT_NAME}`, (error, result) => {
                        if (error || _.isNull(result) || _.isUndefined(result)) {
                            BasketballSBOModel.find({})
                                .lean()
                                .sort('sk')
                                .select('k n r')
                                .exec((error, response) => {
                                    if (error) {
                                        callback(error, null);
                                    } else {
                                        Redis.setByKey(`BasketballSBOModel${CLIENT_NAME}`, response, (error, result) => {

                                        });
                                        callback(null, response);
                                    }
                                });
                        } else {
                            callback(null, result);
                        }
                    });
                }
            ],
            (error, parallelResult) => {
                if (error) {
                    return res.send({
                        code    : 9999,
                        message : error
                    });
                } else {
                    let [
                        limitSetting,
                        sboTodayBasketball,
                        basketballSBOModel
                    ] = parallelResult;

                    if (!_.isEmpty(sboTodayBasketball)) {
                        _.chain(sboTodayBasketball)
                            .each(league => {
                                const sboObjLeague = _.chain(basketballSBOModel)
                                    .filter(sboLeague => {
                                        return _.isEqual(parseInt(sboLeague.k), league.k)
                                    })
                                    .first()
                                    .value();

                                if (!_.isUndefined(sboObjLeague)) {
                                    // league.m = _.filter(league.m, match => {
                                    //     return new Date(match.d) >= now_date;
                                    // });

                                    const configMatch = Commons.configMatchMixStep("8");
                                    sboObjLeague.rp = {};
                                    sboObjLeague.rp.hdp = Commons.rulePriceHDP();

                                    let ahData = {home: 0, away: 0};
                                    let ouData = {o: 0, u: 0};
                                    let oeData = {o: 0, e: 0};
                                    let ah1stData = {home: 0, away: 0};
                                    let ou1stData = {o: 0, u: 0};
                                    let oe1stData = {o: 0, e: 0};

                                    const {
                                        ah,
                                        ou,
                                        oe,
                                        ah1st,
                                        ou1st,
                                        oe1st
                                    } = sboObjLeague.r;

                                    _.each(league.m, match => {

                                        const matchDate = new Date(match.d);

                                        _.each(match.bp, (betPrice, round) => {
                                            // AH
                                            if(betPrice.ah){
                                                if(betPrice.ah.h || betPrice.ah.a){
                                                    Commons.cal_ah(betPrice.ah.h, betPrice.ah.a, betPrice.ah.hp, betPrice.ah.ap, sboObjLeague.rp.hdp.ah.f, sboObjLeague.rp.hdp.ah.u, configMatch.ah, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ahData, (err, data)=>{
                                                        if(data){
                                                            betPrice.ah.hp = data.hp;
                                                            betPrice.ah.ap = data.ap;
                                                        }
                                                    })
                                                }
                                            }
                                            // OU
                                            if(betPrice.ou){
                                                if(betPrice.ou.op || betPrice.ou.up){
                                                    Commons.cal_ou(betPrice.ou.op, betPrice.ou.up, sboObjLeague.rp.hdp.ou.f, sboObjLeague.rp.hdp.ou.u, configMatch.ou, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ouData, (err, data)=>{
                                                        if(data){
                                                            betPrice.ou.op = data.op;
                                                            betPrice.ou.up = data.up;
                                                        }
                                                    })
                                                }
                                            }
                                            // OE
                                            if(betPrice.oe){
                                                if(betPrice.oe.o || betPrice.oe.e){
                                                    Commons.cal_oe(betPrice.oe.o, betPrice.oe.e, sboObjLeague.rp.hdp.oe.f, sboObjLeague.rp.hdp.oe.u, configMatch.oe, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, oeData, (err, data)=>{
                                                        if(data){
                                                            betPrice.oe.o = data.o;
                                                            betPrice.oe.e = data.e;
                                                        }
                                                    })
                                                }
                                            }

                                            //// ============================ 1st ============================
                                            // AH
                                            if(betPrice.ah1st){
                                                if(betPrice.ah1st.h || betPrice.ah1st.a){
                                                    Commons.cal_ah(betPrice.ah1st.h, betPrice.ah1st.a, betPrice.ah1st.hp, betPrice.ah1st.ap, sboObjLeague.rp.hdp.ah1st.f, sboObjLeague.rp.hdp.ah1st.u, configMatch.ah1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ah1stData, (err, data)=>{
                                                        if(data){
                                                            betPrice.ah1st.hp = data.hp;
                                                            betPrice.ah1st.ap = data.ap;
                                                        }
                                                    })
                                                }
                                            }
                                            // OU
                                            if(betPrice.ou1st){
                                                if(betPrice.ou1st.op || betPrice.ou1st.up){

                                                    Commons.cal_ou(betPrice.ou1st.op, betPrice.ou1st.up, sboObjLeague.rp.hdp.ou1st.f, sboObjLeague.rp.hdp.ou1st.u, configMatch.ou1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ou1stData, (err, data)=>{
                                                        if(data){
                                                            betPrice.ou1st.op = data.op;
                                                            betPrice.ou1st.up = data.up;
                                                        }
                                                    })
                                                }
                                            }
                                            // OE
                                            if(betPrice.oe1st){
                                                if(betPrice.oe1st.o || betPrice.oe1st.e){
                                                    Commons.cal_oe(betPrice.oe1st.o, betPrice.oe1st.e, sboObjLeague.rp.hdp.oe1st.f, sboObjLeague.rp.hdp.oe1st.u, configMatch.oe1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, oe1stData, (err, data)=>{
                                                        if(data){
                                                            betPrice.oe1st.o = data.o;
                                                            betPrice.oe1st.e = data.e;
                                                        }
                                                    })
                                                }
                                            }

                                            betPrice.r = Commons.rule();
                                            betPrice.r.ah.ma = calculateBetPriceByTime(calculateBetPrice(Math.min(ah.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round), now_date, matchDate);
                                            betPrice.r.ah.mi = ah.mi;

                                            betPrice.r.ou.ma = calculateBetPriceByTime(calculateBetPrice(Math.min(ou.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round), now_date, matchDate);
                                            betPrice.r.ou.mi = ou.mi;

                                            betPrice.r.oe.ma = calculateBetPriceByTime(calculateBetPrice(Math.min(oe.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round), now_date, matchDate);
                                            betPrice.r.oe.mi = oe.mi;

                                            betPrice.r.ah1st.ma = calculateBetPriceByTime(calculateBetPrice(Math.min(ah1st.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round), now_date, matchDate);
                                            betPrice.r.ah1st.mi = ah1st.mi;

                                            betPrice.r.ou1st.ma = calculateBetPriceByTime(calculateBetPrice(Math.min(ou1st.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round), now_date, matchDate);
                                            betPrice.r.ou1st.mi = ou1st.mi;

                                            betPrice.r.oe1st.ma = calculateBetPriceByTime(calculateBetPrice(Math.min(oe1st.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round), now_date, matchDate);
                                            betPrice.r.oe1st.mi = oe1st.mi;

                                            delete betPrice.r.x12;
                                            delete betPrice.r.x121st;
                                        });
                                    });
                                } else {
                                    httpRequest.get(
                                        `http://lb-sbo-master.api-hub.com/basketball/external/sboLeague/${league.k}`,
                                        (error, result) => {
                                            if (error) {
                                                console.error(error);
                                            } else {
                                                const model = JSON.parse(result.body).result;
                                                BasketballSBOModel.findOne({
                                                    'n.en': model.n.en
                                                })
                                                    .lean()
                                                    .exec((err, data) => {
                                                        if (err) {
                                                            console.log(err);
                                                        }  else if (!data) {
                                                            const basketballSBOModel = new BasketballSBOModel(model);
                                                            basketballSBOModel.save((error, data) => {
                                                                if (error) {
                                                                    console.error(error);
                                                                } else {
                                                                    console.log(data);
                                                                }
                                                            });
                                                        } else {
                                                            data.k = model.k;
                                                            delete data._id;
                                                            const basketballSBOModel = new BasketballSBOModel(data);
                                                            basketballSBOModel.save((error, data) => {
                                                                if (error) {
                                                                    console.error(error);
                                                                } else {
                                                                    console.log(data);
                                                                }
                                                            });
                                                        }
                                                    });
                                            }
                                        });
                                    console.error(`sboObjLeague not found ${league.k}`);
                                }
                            })
                            .value();

                        sboTodayBasketball = _.reject(sboTodayBasketball, league => {
                            return _.size(league.m) === 0;
                        });

                        return res.send({
                            message: "success",
                            result: sboTodayBasketball,
                            code: 0
                        });

                    } else {
                        return res.send({
                            message: "success",
                            result: [],
                            code: 0
                        });
                    }
                }
            });
    }
});

router.get('/muaythai/hdp', (req, res) => {
    const now_date = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const {
        _id,
        username,
        currency
    } = req.userInfo;

    if (_.isEqual(CLIENT_NAME, 'IFM789')) {
        return res.send({
            message: "success",
            result: [],
            code: 0
        });
    } else {
        parallel([
                (callback) => {
                    MemberService.getLimitSetting(_id, (errLimitSetting, dataLimitSetting)=>{
                        if(errLimitSetting){
                            callback(errLimitSetting, null);
                        } else {
                            callback(null, dataLimitSetting.limitSetting);
                        }
                    });
                },

                (callback) => {
                    Redis.getByKey('sboTodayMuayThai', (error, result) => {
                        if (error) {
                            callback(null, false);
                        } else {
                            callback(null, result);
                        }
                    })
                },

                (callback) => {
                    Redis.getByKey(`MuaythaiSBOModel${CLIENT_NAME}`, (error, result) => {
                        if (error || _.isNull(result) || _.isUndefined(result)) {
                            MuaythaiSBOModel.find({})
                                .lean()
                                .sort('sk')
                                .select('k n r')
                                .exec((error, response) => {
                                    if (error) {
                                        callback(error, null);
                                    } else {
                                        Redis.setByKey(`MuaythaiSBOModel${CLIENT_NAME}`, response, (error, result) => {

                                        });
                                        callback(null, response);
                                    }
                                });
                        } else {
                            callback(null, result);
                        }
                    });
                }
            ],
            (error, parallelResult) => {
                if (error) {
                    return res.send({
                        code    : 9999,
                        message : error
                    });
                } else {
                    let [
                        limitSetting,
                        sboTodayMuayThai,
                        muaythaiSBOModel
                    ] = parallelResult;

                    if (!_.isEmpty(sboTodayMuayThai)) {
                        _.chain(sboTodayMuayThai)
                            .each(league => {
                                const sboObjLeague = _.chain(muaythaiSBOModel)
                                    .filter(sboLeague => {
                                        return _.isEqual(parseInt(sboLeague.k), league.k)
                                    })
                                    .first()
                                    .value();

                                if (!_.isUndefined(sboObjLeague)) {
                                    // league.m = _.filter(league.m, match => {
                                    //     return new Date(match.d) >= now_date;
                                    // });

                                    const configMatch = Commons.configMatchMixStep("3");
                                    sboObjLeague.rp = {};
                                    sboObjLeague.rp.hdp = Commons.rulePriceHDP();

                                    let ahData = {home: 0, away: 0};
                                    let ouData = {o: 0, u: 0};

                                    const {
                                        ah,
                                        ou
                                    } = sboObjLeague.r;

                                    _.each(league.m, match => {

                                        const matchDate = new Date(match.d);

                                        _.each(match.bp, (betPrice, round) => {
                                            if(betPrice.ah){
                                                // console.log(betPrice.ah);
                                                if(betPrice.ah.h || betPrice.ah.a){
                                                    Commons.cal_ah(betPrice.ah.h, betPrice.ah.a, betPrice.ah.hp, betPrice.ah.ap, sboObjLeague.rp.hdp.ah.f, sboObjLeague.rp.hdp.ah.u, configMatch.ah, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ahData, (err, data)=>{
                                                        if(data){
                                                            betPrice.ah.hp = data.hp;
                                                            betPrice.ah.ap = data.ap;
                                                        }
                                                    })
                                                }
                                            }

                                            if(betPrice.ou){
                                                if(betPrice.ou.op || betPrice.ou.up){
                                                    Commons.cal_ou(betPrice.ou.op, betPrice.ou.up, sboObjLeague.rp.hdp.ou.f, sboObjLeague.rp.hdp.ou.u, configMatch.ou, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ouData, (err, data)=>{
                                                        if(data){
                                                            betPrice.ou.op = data.op;
                                                            betPrice.ou.up = data.up;
                                                        }
                                                    })
                                                }
                                            }

                                            betPrice.r = {};
                                            betPrice.r.ah = {};
                                            betPrice.r.ah.ma = calculateBetPriceByTime(calculateBetPrice(ah.ma, round), now_date, matchDate);
                                            betPrice.r.ah.mi = ah.mi;

                                            betPrice.r.ou = {};
                                            betPrice.r.ou.ma = calculateBetPriceByTime(calculateBetPrice(ou.ma, round), now_date, matchDate);
                                            betPrice.r.ou.mi = ou.mi;
                                        });
                                    });
                                } else {
                                    httpRequest.get(
                                        `http://lb-sbo-master.api-hub.com/muaythai/external/sboLeague/${league.k}`,
                                        (error, result) => {
                                            if (error) {
                                                console.error(error);
                                            } else {
                                                const model = JSON.parse(result.body).result;
                                                MuaythaiSBOModel.findOne({
                                                    'n.en': model.n.en
                                                })
                                                    .lean()
                                                    .exec((err, data) => {
                                                        if (err) {
                                                            console.log(err);
                                                        }  else if (!data) {
                                                            const muaythaiSBOModel = new MuaythaiSBOModel(model);
                                                            muaythaiSBOModel.save((error, data) => {
                                                                if (error) {
                                                                    console.error(error);
                                                                } else {
                                                                    console.log(data);
                                                                }
                                                            });
                                                        } else {
                                                            data.k = model.k;
                                                            delete data._id;
                                                            const muaythaiSBOModel = new MuaythaiSBOModel(data);
                                                            muaythaiSBOModel.save((error, data) => {
                                                                if (error) {
                                                                    console.error(error);
                                                                } else {
                                                                    console.log(data);
                                                                }
                                                            });
                                                        }
                                                    });
                                            }
                                        });
                                    console.error(`sboObjLeague not found ${league.k}`);
                                }
                            })
                            .value();

                        sboTodayMuayThai = _.reject(sboTodayMuayThai, league => {
                            return _.size(league.m) === 0;
                        });

                        // console.log(JSON.stringify(sboTodayMuayThai));
                        return res.send({
                            message: "success",
                            result: sboTodayMuayThai,
                            code: 0
                        });

                    } else {
                        return res.send({
                            message: "success",
                            result: [],
                            code: 0
                        });
                    }
                }
            });
    }
});

router.get('/tennis/hdp', (req, res) => {
    const now_date = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const {
        _id,
        username,
        currency
    } = req.userInfo;
    if (_.isEqual(CLIENT_NAME, 'IFM789')) {
        return res.send({
            message: "success",
            result: [],
            code: 0
        });
    } else {
        parallel([
                (callback) => {
                    MemberService.getLimitSetting(_id, (errLimitSetting, dataLimitSetting)=>{
                        if(errLimitSetting){
                            callback(errLimitSetting, null);
                        } else {
                            callback(null, dataLimitSetting.limitSetting);
                        }
                    });
                },

                (callback) => {
                    MemberService.getOddAdjustment(username, (err, data) => {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, data);
                        }
                    });
                },

                (callback) => {
                    Redis.getByKey('sboTodayTennis', (error, result) => {
                        if (error) {
                            callback(null, false);
                        } else {
                            callback(null, result);
                        }
                    })
                },

                (callback) => {
                    TennisSBOModel.find({})
                        .lean()
                        .sort('sk')
                        .select('k n r')
                        .exec((error, response) => {
                            if (error) {
                                callback(error, null);
                            } else {
                                callback(null, response);
                            }
                        });
                }
            ],
            (error, parallelResult) => {
                if (error) {
                    return res.send({
                        code    : 9999,
                        message : error
                    });
                } else {
                    let [
                        limitSetting,
                        oddAdjust,
                        sboTodayTennis,
                        tennisSBOModel
                    ] = parallelResult;

                    const configMatch = Commons.configMatch();
                    if (!_.isEmpty(sboTodayTennis)) {
                        _.chain(sboTodayTennis)
                            .each(league => {
                                const sboObjLeague = _.chain(tennisSBOModel)
                                    .filter(sboLeague => {
                                        return _.isEqual(parseInt(sboLeague.k), league.k)
                                    })
                                    .first()
                                    .value();

                                if (!_.isUndefined(sboObjLeague)) {
                                    league.m = _.filter(league.m, match => {
                                        return new Date(match.d) >= now_date;
                                    });

                                    sboObjLeague.rp = {};
                                    sboObjLeague.rp.hdp = {};
                                    sboObjLeague.rp.hdp = Commons.rulePriceHDP();

                                    _.each(league.m, match => {
                                        const matchDate = new Date(match.d);

                                        _.each(match.bp, betPrice => {
                                            const odd = _.filter(oddAdjust, obj =>{
                                                return _.isEqual(obj.matchId, match.id);
                                            });

                                            let ah = {home: 0, away: 0};

                                            if(!_.isEmpty(odd)){
                                                _.each(odd, o => {
                                                    const {
                                                        prefix,
                                                        key,
                                                        value,
                                                        count
                                                    } = o;
                                                    if(_.isEqual(prefix, 'ah') && !_.isUndefined(betPrice.ah)) {
                                                        // console.log(betPrice.ah.hpk);
                                                        if(_.isEqual(key, 'hpk') && _.isEqual(value, betPrice.ah.hpk)){
                                                            ah = {
                                                                home: -count,
                                                                away: count
                                                            };
                                                            // console.log(`       hpk ah => ${JSON.stringify(ah)}`);
                                                        } else if(_.isEqual(key, 'apk') && _.isEqual(value, betPrice.ah.apk)){
                                                            ah = {
                                                                home: count,
                                                                away: -count
                                                            };
                                                            // console.log(`       apk ah => ${JSON.stringify(ah)}`);
                                                        }

                                                    }
                                                });
                                            }

                                            // ---------------------------  ah  ----------------------------------
                                            if(betPrice.ah){
                                                if(betPrice.ah.h || betPrice.ah.a){
                                                    Commons.cal_ah(betPrice.ah.h, betPrice.ah.a, betPrice.ah.hp, betPrice.ah.ap, sboObjLeague.rp.hdp.ah.f, sboObjLeague.rp.hdp.ah.u, configMatch.ah, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ah, (err, data)=>{
                                                        if(data){
                                                            betPrice.ah.hp = data.hp;
                                                            betPrice.ah.ap = data.ap;
                                                        }
                                                    });
                                                }
                                            }
                                            // ---------------------------  ml  ----------------------------------
                                            if(betPrice.ml){
                                                if(betPrice.ml.h || betPrice.ml.a){
                                                    Commons.cal_ml(
                                                        betPrice.ml.h,
                                                        betPrice.ml.a,
                                                        configMatch.ml, (err, data)=>{
                                                            if(data){
                                                                betPrice.ml.h = data.h;
                                                                betPrice.ml.a = data.a;
                                                            }
                                                        });
                                                }
                                            }
                                        });

                                        const {
                                            ah,
                                            ml
                                        } = sboObjLeague.r;
                                        _.each(match.bp, (betPrice, round) => {
                                            betPrice.r = {};
                                            betPrice.r.ah = {};
                                            betPrice.r.ah.ma = calculateBetPriceByTime(calculateBetPrice(ah.ma, round), now_date, matchDate);
                                            betPrice.r.ah.mi = ah.mi;

                                            betPrice.r.ml = {};
                                            betPrice.r.ml.ma = calculateBetPriceByTime(calculateBetPrice(ml.ma, round), now_date, matchDate);
                                            betPrice.r.ml.mi = ml.mi;
                                        });
                                    });
                                } else {
                                    httpRequest.get(
                                        `http://lb-sbo-master.api-hub.com/tennis/external/sboLeague/${league.k}`,
                                        (error, result) => {
                                            if (error) {
                                                console.error(error);
                                            } else {
                                                const model = JSON.parse(result.body).result;
                                                TennisSBOModel.findOne({
                                                    'n.en': model.n.en
                                                })
                                                    .lean()
                                                    .exec((err, data) => {
                                                        if (err) {
                                                            console.log(err);
                                                        }  else if (!data) {
                                                            const tennisSBOModel = new TennisSBOModel(model);
                                                            tennisSBOModel.save((error, data) => {
                                                                if (error) {
                                                                    console.error(error);
                                                                } else {
                                                                    console.log(data);
                                                                }
                                                            });
                                                        } else {
                                                            data.k = model.k;
                                                            delete data._id;
                                                            const tennisSBOModel = new TennisSBOModel(data);
                                                            tennisSBOModel.save((error, data) => {
                                                                if (error) {
                                                                    console.error(error);
                                                                } else {
                                                                    console.log(data);
                                                                }
                                                            });
                                                        }
                                                    });
                                            }
                                        });
                                    console.error(`sboObjLeague not found ${league.k}`);
                                }
                            })
                            .value();

                        sboTodayTennis = _.reject(sboTodayTennis, league => {
                            return _.size(league.m) === 0;
                        });

                        // console.log(JSON.stringify(sboTodayTennis));
                        return res.send({
                            message: "success",
                            result: sboTodayTennis,
                            code: 0
                        });

                    } else {
                        return res.send({
                            message: "success",
                            result: [],
                            code: 0
                        });
                    }
                }
            });
    }
});

router.get('/eSport/hdp', (req, res) => {
    const now_date = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const {
        _id,
        username,
        currency
    } = req.userInfo;
    if (_.isEqual(CLIENT_NAME, 'IFM789')) {
        return res.send({
            message: "success",
            result: [],
            code: 0
        });
    } else {
        parallel([
                (callback) => {
                    MemberService.getLimitSetting(_id, (errLimitSetting, dataLimitSetting)=>{
                        if(errLimitSetting){
                            callback(errLimitSetting, null);
                        } else {
                            callback(null, dataLimitSetting.limitSetting);
                        }
                    });
                },

                (callback) => {
                    MemberService.getOddAdjustment(username, (err, data) => {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, data);
                        }
                    });
                },

                (callback) => {
                    Redis.getByKey('sboTodayESport', (error, result) => {
                        if (error) {
                            callback(null, false);
                        } else {
                            callback(null, result);
                        }
                    })
                },

                (callback) => {
                    ESportSBOModel.find({})
                        .lean()
                        .sort('sk')
                        .select('k n r')
                        .exec((error, response) => {
                            if (error) {
                                callback(error, null);
                            } else {
                                callback(null, response);
                            }
                        });
                }
            ],
            (error, parallelResult) => {
                if (error) {
                    return res.send({
                        code    : 9999,
                        message : error
                    });
                } else {
                    let [
                        limitSetting,
                        oddAdjust,
                        sboTodayESport,
                        eSportSBOModel
                    ] = parallelResult;

                    const configMatch = Commons.configMatch();
                    if (!_.isEmpty(sboTodayESport)) {
                        _.chain(sboTodayESport)
                            .each(league => {
                                const sboObjLeague = _.chain(eSportSBOModel)
                                    .filter(sboLeague => {
                                        return _.isEqual(parseInt(sboLeague.k), league.k)
                                    })
                                    .first()
                                    .value();

                                if (!_.isUndefined(sboObjLeague)) {
                                    league.m = _.filter(league.m, match => {
                                        return new Date(match.d) >= now_date;
                                    });

                                    sboObjLeague.rp = {};
                                    sboObjLeague.rp.hdp = {};
                                    sboObjLeague.rp.hdp = Commons.rulePriceHDP();

                                    _.each(league.m, match => {
                                        const matchDate = new Date(match.d);

                                        _.each(match.bp, betPrice => {
                                            const odd = _.filter(oddAdjust, obj =>{
                                                return _.isEqual(obj.matchId, match.id);
                                            });

                                            let ah = {home: 0, away: 0};

                                            if(!_.isEmpty(odd)){
                                                _.each(odd, o => {
                                                    const {
                                                        prefix,
                                                        key,
                                                        value,
                                                        count
                                                    } = o;
                                                    if(_.isEqual(prefix, 'ah') && !_.isUndefined(betPrice.ah)) {
                                                        // console.log(betPrice.ah.hpk);
                                                        if(_.isEqual(key, 'hpk') && _.isEqual(value, betPrice.ah.hpk)){
                                                            ah = {
                                                                home: -count,
                                                                away: count
                                                            };
                                                            // console.log(`       hpk ah => ${JSON.stringify(ah)}`);
                                                        } else if(_.isEqual(key, 'apk') && _.isEqual(value, betPrice.ah.apk)){
                                                            ah = {
                                                                home: count,
                                                                away: -count
                                                            };
                                                            // console.log(`       apk ah => ${JSON.stringify(ah)}`);
                                                        }

                                                    }
                                                });
                                            }

                                            // ---------------------------  ah  ----------------------------------
                                            if(betPrice.ah){
                                                if(betPrice.ah.h || betPrice.ah.a){
                                                    Commons.cal_ah(betPrice.ah.h, betPrice.ah.a, betPrice.ah.hp, betPrice.ah.ap, sboObjLeague.rp.hdp.ah.f, sboObjLeague.rp.hdp.ah.u, configMatch.ah, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ah, (err, data)=>{
                                                        if(data){
                                                            betPrice.ah.hp = data.hp;
                                                            betPrice.ah.ap = data.ap;
                                                        }
                                                    });
                                                }
                                            }
                                            // ---------------------------  ml  ----------------------------------
                                            if(betPrice.ml){
                                                if(betPrice.ml.h || betPrice.ml.a){
                                                    Commons.cal_ml(
                                                        betPrice.ml.h,
                                                        betPrice.ml.a,
                                                        configMatch.ml, (err, data)=>{
                                                            if(data){
                                                                betPrice.ml.h = data.h;
                                                                betPrice.ml.a = data.a;
                                                            }
                                                        });
                                                }
                                            }
                                        });

                                        const {
                                            ah,
                                            ml
                                        } = sboObjLeague.r;
                                        _.each(match.bp, (betPrice, round) => {
                                            betPrice.r = {};
                                            betPrice.r.ah = {};
                                            betPrice.r.ah.ma = calculateBetPriceByTime(calculateBetPrice(ah.ma, round), now_date, matchDate);
                                            betPrice.r.ah.mi = ah.mi;

                                            betPrice.r.ml = {};
                                            betPrice.r.ml.ma = calculateBetPriceByTime(calculateBetPrice(ml.ma, round), now_date, matchDate);
                                            betPrice.r.ml.mi = ml.mi;
                                        });
                                    });
                                } else {
                                    httpRequest.get(
                                        `http://lb-sbo-master.api-hub.com/eSport/external/sboLeague/${league.k}`,
                                        (error, result) => {
                                            if (error) {
                                                console.error(error);
                                            } else {
                                                const model = JSON.parse(result.body).result;
                                                ESportSBOModel.findOne({
                                                    'n.en': model.n.en
                                                })
                                                    .lean()
                                                    .exec((err, data) => {
                                                        if (err) {
                                                            console.log(err);
                                                        }  else if (!data) {
                                                            const eSportSBOModel = new ESportSBOModel(model);
                                                            eSportSBOModel.save((error, data) => {
                                                                if (error) {
                                                                    console.error(error);
                                                                } else {
                                                                    console.log(data);
                                                                }
                                                            });
                                                        } else {
                                                            data.k = model.k;
                                                            delete data._id;
                                                            const eSportSBOModel = new ESportSBOModel(data);
                                                            eSportSBOModel.save((error, data) => {
                                                                if (error) {
                                                                    console.error(error);
                                                                } else {
                                                                    console.log(data);
                                                                }
                                                            });
                                                        }
                                                    });
                                            }
                                        });
                                    console.error(`sboObjLeague not found ${league.k}`);
                                }
                            })
                            .value();

                        sboTodayESport = _.reject(sboTodayESport, league => {
                            return _.size(league.m) === 0;
                        });

                        // console.log(JSON.stringify(sboTodayESport));
                        return res.send({
                            message: "success",
                            result: sboTodayESport,
                            code: 0
                        });

                    } else {
                        return res.send({
                            message: "success",
                            result: [],
                            code: 0
                        });
                    }
                }
            });
    }
});

const calculateBetPrice = (price, round) => {
    let percent = 0;
    switch (round) {
        case 0:
            percent = 0;
            break;
        case 1:
            percent = 40;
            break;
        case 2:
            percent = 80;
            break;
        case 3:
            percent = 90;
            break;
        case 4:
            percent = 90;
            break;
        case 5:
            percent = 90;
            break;
    }
    return decrease(price, percent);
};

const calculateBetPriceByTime = (price, current, match) => {
    if (price <= 20000) {
        return price;
    } else {
        let diff =(current.getTime() - match.getTime()) / 1000;
        diff /= (60 * 60);

        const gapHour = Math.abs(Math.round(diff));
        let percent = 0;

        if (12 <= gapHour) {
            percent = 70;
        } else if (11 === gapHour || 10 === gapHour || 9 === gapHour || 8 === gapHour || 7 === gapHour) {
            percent = 65;
        } else if (6 === gapHour || 5 === gapHour || 4 === gapHour ) {
            percent = 50;
        } else if (3 === gapHour || 2 === gapHour ) {
            percent = 35;
        } else {
            percent = 0;
        }

        return RoundTo(parseFloat(decrease(price, percent)), 0);
    }
};

const decrease = (price, percent) => {
    return (price * ( (100 - percent) / 100)).toFixed(2);
};

module.exports = router;
