const express     = require('express');
const router      = express.Router();
const _           = require('underscore');
const parallel    = require("async/parallel");
const Redis       = require('../1_common/1_redis/1_1_redis');
const httpRequest = require('request');

const FootballSBOModel = require('../../../models/footballSBO.model');
const MemberService    = require('../../../common/memberService');
const Commons          = require('../../../common/commons');

const LOG       = 'MATCH_FG&LG';

router.get('/', (req, res) => {
    const now_date = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const {
        _id,
        username,
        currency
    } = req.userInfo;

    parallel([
            (callback) => {
                MemberService.getLimitSetting(_id, (errLimitSetting, dataLimitSetting)=>{
                    if(errLimitSetting){
                        callback(errLimitSetting, null);
                    } else {
                        callback(null, dataLimitSetting.limitSetting);
                    }
                });
            },

            (callback) => {
                Redis.getByKey('sboToday', (error, result) => {
                    if (error) {
                        callback(null, false);
                    } else {
                        callback(null, result);
                    }
                })
            },

            (callback) => {
                FootballSBOModel.find({})
                    .lean()
                    .sort('sk')
                    .select('k n r rp')
                    .exec((error, response) => {
                        if (error) {
                            callback(error, null);
                        } else {
                            callback(null, response);
                        }
                    });
            },

            (callback) => {
                Redis.getByKey('sboMoreToday', (error, result) => {
                    if (error) {
                        callback(null, false);
                    } else {
                        callback(null, result);
                    }
                })
            }
        ],
        (error, parallelResult) => {
            if (error) {
                return res.send({
                    code    : 9999,
                    message : error
                });
            } else {
                let [
                    limitSetting,
                    sboToday,
                    footballSBOModel,
                    sboMoreToday
                ] = parallelResult;

                const configMatch = Commons.configMatch();
                // console.log('oddAdjust => ',JSON.stringify(oddAdjust));
                // console.log(sboToday);
                if (!_.isEmpty(sboToday)) {
                    _.chain(sboToday)
                        .each(league => {
                            const sboObjLeague = _.chain(footballSBOModel)
                                .filter(sboLeague => {
                                    return _.isEqual(parseInt(sboLeague.k), league.k)
                                })
                                .first()
                                .value();

                            if (!_.isUndefined(sboObjLeague)) {
                                league.m = _.filter(league.m, match => {
                                    return new Date(match.d) >= now_date;
                                });

                                _.each(league.m, match => {
                                    match.bp.length = 1;
                                    _.each(match.bp, betPrice => {
                                        delete betPrice.ah;
                                        delete betPrice.ah1st;
                                        delete betPrice.ou;
                                        delete betPrice.ou1st;
                                        delete betPrice.oe;
                                        delete betPrice.oe1st;
                                        delete betPrice.x12;
                                        delete betPrice.x121st;

                                        const moreToday = _.findWhere(sboMoreToday, JSON.parse(`{"id":"${match.id}"}`));
                                        if (!_.isUndefined(moreToday) && !_.isEmpty(moreToday.more) && !_.isUndefined(moreToday.more)) {
                                            const more = _.first(moreToday.more);
                                            const objFGLG = more['fglg'];
                                            if (!_.isUndefined(objFGLG)) {
                                                betPrice.fglg = objFGLG;
                                            }
                                        }

                                        betPrice.r = Commons.rule();
                                        betPrice.r.fglg = {};
                                        betPrice.r.fglg.ma = 0;
                                        betPrice.r.fglg.mi = 0;

                                        delete betPrice.r.ah;
                                        delete betPrice.r.ah1st;
                                        delete betPrice.r.ou;
                                        delete betPrice.r.ou1st;
                                        delete betPrice.r.oe;
                                        delete betPrice.r.oe1st;
                                        delete betPrice.r.x12;
                                        delete betPrice.r.x121st;
                                    });
                                });
                            } else {
                                httpRequest.get(
                                    `http://lb-sbo-master.api-hub.com/external/sboLeague/${league.k}`,
                                    (error, result) => {
                                        if (error) {
                                            console.error(error);
                                        } else {
                                            const model = JSON.parse(result.body).result;
                                            const footballSBOModel = new FootballSBOModel(model);
                                            footballSBOModel.save((error, data) => {
                                                if (error) {
                                                    console.error(error);
                                                } else {
                                                    console.log(data);
                                                }
                                            });
                                        }
                                    });
                                console.error(`sboObjLeague not found ${league.k}`);
                            }
                        })
                        .value();

                    sboToday = _.reject(sboToday, league => {
                        return _.size(league.m) === 0;
                    });

                    // console.log(JSON.stringify(sboToday));
                    return res.send({
                        message: "success",
                        result: sboToday,
                        code: 0
                    });

                } else {
                    return res.send({
                        message: "success",
                        result: [],
                        code: 0
                    });
                }
            }
    });
});

module.exports = router;
