const express     = require('express');
const router      = express.Router();
const _           = require('underscore');
const parallel    = require("async/parallel");
const waterfall   = require("async/waterfall");
const Redis       = require('../1_common/1_redis/1_1_redis');
const httpRequest = require('request');
const moment      = require('moment');
const Excel       = require('exceljs');

const FootballSBOModel = require('../../../models/footballSBO.model');
const MemberService    = require('../../../common/memberService');
const Commons          = require('../../../common/commons');
const RoundTo          = require('round-to');
const CLIENT_NAME      = process.env.CLIENT_NAME || 'SPORTBOOK88';
const LOG       = 'MATCH_TODAY';

router.get('/hdp', (req, res) => {
    const now_date = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const {
        _id,
        username,
        currency
    } = req.userInfo;

    parallel([
            (callback) => {
                MemberService.getLimitSetting(_id, (errLimitSetting, dataLimitSetting)=>{
                    if(errLimitSetting){
                        callback(errLimitSetting, null);
                    } else {
                        callback(null, dataLimitSetting.limitSetting);
                    }
                });
            },

            (callback) => {
                MemberService.getOddAdjustment(username, (err, data) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, data);
                    }
                });
            },

            (callback) => {
                Redis.getByKey('sboParlayTable', (error, result) => {
                    if (error) {
                        callback(null, false);
                    } else {
                        callback(null, result);
                    }
                })
            },

            (callback) => {
                Redis.getByKey(`FootballSBOModel${CLIENT_NAME}`, (error, result) => {
                    if (error || _.isNull(result) || _.isUndefined(result)) {
                        FootballSBOModel.find({})
                            .lean()
                            .select('k n r rl rem rp')
                            .exec((error, response) => {
                                if (error) {
                                    callback(error, null);
                                } else {
                                    Redis.setByKey(`FootballSBOModel${CLIENT_NAME}`, response, (error, result) => {

                                    });
                                    callback(null, response);
                                }
                            });
                    } else {
                        callback(null, result);
                    }
                });
            },

            (callback) => {
                Redis.getByKey('sboParlayInfo', (error, result) => {
                    if (error) {
                        callback(null, false);
                    } else {
                        callback(null, result);
                    }
                })
            }
        ],
        (error, parallelResult) => {
            if (error) {
                return res.send({
                    code    : 9999,
                    message : error
                });
            } else {
                const [
                    limitSetting,
                    oddAdjust,
                    sboParlayTable,
                    footballSBOModel,
                    sboParlayInfo
                ] = parallelResult;

                const now_date = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
                const secondsDiff = moment(now_date).diff(sboParlayInfo.updatedDate, 'seconds');
                const configMatch = Commons.configMatch();

                if (secondsDiff > 90) {
                    return res.send({
                        message: "success",
                        result: [],
                        code: 0
                    });
                } else {
                    if (!_.isEmpty(sboParlayTable)) {
                        _.chain(sboParlayTable)
                            .each(league => {
                                const sboObjLeague = _.chain(footballSBOModel)
                                    .filter(sboLeague => {
                                        return _.isEqual(parseInt(sboLeague.k), league.k)
                                    })
                                    .first()
                                    .value();

                                if (!_.isUndefined(sboObjLeague)) {
                                    league.m = _.filter(league.m, match => {
                                        return new Date(match.d) >= now_date;
                                    });

                                    _.each(league.m, match => {

                                        _.each(match.bp, betPrice => {
                                            const odd = _.filter(oddAdjust, obj =>{
                                                return _.isEqual(obj.matchId, match.id);
                                            });

                                            let ou1st = {o: 0, u: 0};
                                            let ou = {o: 0, u: 0};
                                            let oe = {o: 0, e: 0};
                                            let oe1st = {o: 0, e: 0};
                                            let ah = {home: 0, away: 0};
                                            let ah1st = {home: 0, away: 0};

                                            if(!_.isEmpty(odd)){
                                                _.each(odd, o => {
                                                    const {
                                                        prefix,
                                                        key,
                                                        value,
                                                        count
                                                    } = o;

                                                    if(_.isEqual(prefix, 'ah') && !_.isUndefined(betPrice.ah)) {
                                                        // console.log(betPrice.ah.hpk);
                                                        if(_.isEqual(key, 'hpk') && _.isEqual(value, betPrice.ah.hpk)){
                                                            ah = {
                                                                home: -count,
                                                                away: count
                                                            };
                                                            // console.log(`       hpk ah => ${JSON.stringify(ah)}`);
                                                        } else if(_.isEqual(key, 'apk') && _.isEqual(value, betPrice.ah.apk)){
                                                            ah = {
                                                                home: count,
                                                                away: -count
                                                            };
                                                            // console.log(`       apk ah => ${JSON.stringify(ah)}`);
                                                        }

                                                    } else if(_.isEqual(prefix, 'ah1st') && !_.isUndefined(betPrice.ah1st)) {

                                                        if(_.isEqual(key, 'hpk') && _.isEqual(value, betPrice.ah1st.hpk)){
                                                            ah1st = {
                                                                home: -count,
                                                                away: count
                                                            }
                                                        } else if(_.isEqual(key, 'apk') && _.isEqual(value, betPrice.ah1st.apk)){
                                                            ah1st = {
                                                                home: count,
                                                                away: -count
                                                            }
                                                        }

                                                    } else if(_.isEqual(prefix, 'ou') && !_.isUndefined(betPrice.ou)) {

                                                        if(_.isEqual(key, 'opk') && _.isEqual(value, betPrice.ou.opk)){
                                                            ou = {
                                                                o: -count,
                                                                u: count
                                                            }
                                                        } else if(_.isEqual(key, 'upk') && _.isEqual(value, betPrice.ou.upk)){
                                                            ou = {
                                                                o: count,
                                                                u: -count
                                                            }
                                                        }

                                                    } else if(_.isEqual(prefix, 'ou1st') && !_.isUndefined(betPrice.ou1st)) {

                                                        if(_.isEqual(key, 'opk') && _.isEqual(value, betPrice.ou1st.opk)){
                                                            ou1st = {
                                                                o: -count,
                                                                u: count
                                                            }
                                                        } else if(_.isEqual(key, 'upk') && _.isEqual(value, betPrice.ou1st.upk)){
                                                            ou1st = {
                                                                o: count,
                                                                u: -count
                                                            }
                                                        }

                                                    } else if(_.isEqual(prefix, 'oe') && !_.isUndefined(betPrice.oe)) {

                                                        if(_.isEqual(key, 'ok') && _.isEqual(value, betPrice.oe.ok)){
                                                            oe = {
                                                                o: -count,
                                                                e: count
                                                            }
                                                        } else if(_.isEqual(key, 'ek') && _.isEqual(value, betPrice.oe.ek)){
                                                            oe = {
                                                                o: count,
                                                                e: -count
                                                            }
                                                        }

                                                    } else if(_.isEqual(prefix, 'oe1st') && !_.isUndefined(betPrice.oe1st)) {

                                                        if(_.isEqual(key, 'ok') && _.isEqual(value, betPrice.oe1st.ok)){
                                                            oe1st = {
                                                                o: -count,
                                                                e: count
                                                            }
                                                        } else if(_.isEqual(key, 'ek') && _.isEqual(value, betPrice.oe1st.ek)){
                                                            oe1st = {
                                                                o: count,
                                                                e: -count
                                                            }
                                                        }

                                                    }
                                                });
                                            }

                                            if(betPrice.ou1st){
                                                if(betPrice.ou1st.op || betPrice.ou1st.up){

                                                    Commons.cal_eu_ou_sbo(betPrice.ou1st.op, betPrice.ou1st.up, sboObjLeague.rp.hdp.ou1st.f, sboObjLeague.rp.hdp.ou1st.u, configMatch.ou1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ou1st, (err, data)=>{
                                                        if(data){
                                                            betPrice.ou1st.op = data.op;
                                                            betPrice.ou1st.up = data.up;
                                                        }
                                                    })
                                                }
                                                delete betPrice.ou1st.oId;
                                                delete betPrice.ou1st.uId;
                                            }

                                            // ---------------------------  ou  ----------------------------------
                                            if(betPrice.ou){
                                                if(betPrice.ou.op || betPrice.ou.up){
                                                    Commons.cal_eu_ou_sbo(betPrice.ou.op, betPrice.ou.up, sboObjLeague.rp.hdp.ou.f, sboObjLeague.rp.hdp.ou.u, configMatch.ou, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ou, (err, data)=>{
                                                        if(data){
                                                            betPrice.ou.op = data.op;
                                                            betPrice.ou.up = data.up;
                                                        }
                                                    })
                                                }
                                                delete betPrice.ou.oId;
                                                delete betPrice.ou.uId;
                                            }

                                            delete betPrice.oe1st;
                                            delete betPrice.oe;

                                            // ---------------------------  ah1st  ----------------------------------
                                            if(betPrice.ah1st){
                                                if(betPrice.ah1st.h || betPrice.ah1st.a){
                                                    Commons.cal_eu_ah_sbo(betPrice.ah1st.h, betPrice.ah1st.a, betPrice.ah1st.hp, betPrice.ah1st.ap, sboObjLeague.rp.hdp.ah1st.f, sboObjLeague.rp.hdp.ah1st.u, configMatch.ah1st, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ah1st, (err, data)=>{
                                                        if(data){
                                                            betPrice.ah1st.hp = data.hp;
                                                            betPrice.ah1st.ap = data.ap;
                                                        }
                                                    })
                                                }
                                            }

                                            // ---------------------------  ah  ----------------------------------
                                            if(betPrice.ah){
                                                // console.log(betPrice.ah);
                                                if(betPrice.ah.h || betPrice.ah.a){
                                                    Commons.cal_eu_ah_sbo(betPrice.ah.h, betPrice.ah.a, betPrice.ah.hp, betPrice.ah.ap, sboObjLeague.rp.hdp.ah.f, sboObjLeague.rp.hdp.ah.u, configMatch.ah, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, ah, (err, data)=>{
                                                        if(data){
                                                            betPrice.ah.hp = data.hp;
                                                            betPrice.ah.ap = data.ap;
                                                            // console.log('xxxx => ', JSON.stringify(betPrice.ah));
                                                        }
                                                    })
                                                }

                                            }

                                            delete betPrice.x12;
                                            delete betPrice.x121st;
                                        });
                                    });
                                } else {
                                    httpRequest.get(
                                        `http://lb-sbo-master.api-hub.com/external/sboLeague/${league.k}`,
                                        (error, result) => {
                                            if (error) {
                                                console.error(error);
                                            } else {
                                                const model = JSON.parse(result.body).result;
                                                const footballSBOModel = new FootballSBOModel(model);
                                                footballSBOModel.save((error, data) => {
                                                    if (error) {
                                                        console.error(error);
                                                    } else {
                                                        console.log(data);
                                                    }
                                                });
                                            }
                                        });
                                    console.error(`sboObjLeague not found ${league.k}`);
                                }
                            })
                            .value();
                        console.log(JSON.stringify(sboParlayTable));
                        return res.send({
                            message: "success",
                            result: sboParlayTable,
                            code: 0
                        });

                    } else {
                        return res.send({
                            message: "success",
                            result: [],
                            code: 0
                        });
                    }
                }
            }
    });
});

router.post('/excel',function (req,res) {

    let data = req.body.data;
    moment.locale('th');
    var workbook = new Excel.Workbook();
    workbook.creator = 'amb';
    var sheet = workbook.addWorksheet('My Sheet1', {properties: {tabColor: {argb: 'FFC0000'}}});
    var worksheet = workbook.getWorksheet(1);

    worksheet.addRow([moment.utc().format('dddd ที่ D MMMM YYYY')]);
    worksheet.mergeCells('A1','M1')
    worksheet.getCell('A1','M1').alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.getCell('A1','M1').fill = {
        type: 'pattern',
        pattern:'solid',
        fgColor:{argb:'FFFFFF00'},
        bgColor:{argb:'FF0000FF'}
    }
    worksheet.getCell('A1','M1').border = {
        top: {style:'thin'},
        left: {style:'thin'},
        bottom: {style:'thin'},
        right: {style:'thin'}
    }

    data.forEach(function (league,i) {

        let row = [];
        row[1] = "เวลา";
        row[3] = league.nn.th;
        row[10] = 'สูง';
        row[11] = 'โกล';
        row[12] = 'ต่ำ';
        row[13] = '';

        worksheet.addRow(row)
        worksheet.getColumn(1).width = 6;
        worksheet.getColumn(2).width = 7;
        worksheet.getColumn(3).width = 30;
        worksheet.getColumn(4).width = 7;
        worksheet.getColumn(5).width = 7;
        worksheet.getColumn(6).width = 7;
        worksheet.getColumn(7).width = 30;
        worksheet.getColumn(8).width = 7;
        worksheet.getColumn(9).width = 7;
        worksheet.getColumn(10).width = 7;
        worksheet.getColumn(11).width = 7;
        worksheet.getColumn(12).width = 7;
        worksheet.getColumn(13).width = 7;

        let rowNum = worksheet.lastRow;
        rowNum.eachCell(function (cell) {
            cell.alignment = { vertical: 'middle', horizontal: 'center' };
            cell.border = {
                top: {style:'thin'},
                left: {style:'thin'},
                bottom: {style:'thin'},
                right: {style:'thin'}
            }
        })
        worksheet.mergeCells(rowNum.getCell(3).address,rowNum.getCell(9).address)
        league.m.forEach(function (match) {
            var row = [];
            row[1] = moment.utc(match.d).format('HH:mm')
            row[3] = match.n.th.h;
            row[7] = match.n.th.a;
            match.bp.forEach(function (bp,i) {
                let tmpRow = row;
                tmpRow[2] = bp.ah.hId;
                tmpRow[4] = bp.ah.hp;
                tmpRow[5] = bp.ah.h.replace('-','').replace('/','-');
                tmpRow[6] = bp.ah.ap;
                tmpRow[8] = bp.ah.aId;
                tmpRow[9] = bp.ou.hId;
                tmpRow[10] = bp.ou.op;
                tmpRow[11] = bp.ou.o;
                tmpRow[12] = bp.ou.up;
                tmpRow[13] = bp.ou.aId;
                worksheet.addRow(tmpRow)
                let rowNum = worksheet.lastRow;

                worksheet.getCell(rowNum.getCell(1).address).font = {bold:true};
                worksheet.getCell(rowNum.getCell(2).address).font = {bold:true};
                worksheet.getCell(rowNum.getCell(4).address).font = {bold:true};
                worksheet.getCell(rowNum.getCell(5).address).font = {bold:true};
                worksheet.getCell(rowNum.getCell(6).address).font = {bold:true};
                worksheet.getCell(rowNum.getCell(8).address).font = {bold:true};
                worksheet.getCell(rowNum.getCell(9).address).font = {bold:true};
                worksheet.getCell(rowNum.getCell(10).address).font = {bold:true};
                worksheet.getCell(rowNum.getCell(11).address).font = {bold:true};
                worksheet.getCell(rowNum.getCell(12).address).font = {bold:true};
                worksheet.getCell(rowNum.getCell(13).address).font = {bold:true};
                if(bp.ah.h.includes('-')){
                    worksheet.getCell(rowNum.getCell(3).address).font = {
                        underline:true,
                        bold:true
                    };
                }

                if(bp.ah.a.includes('-')){
                    worksheet.getCell(rowNum.getCell(7).address).font = {
                        underline:true,
                        bold:true
                    };
                }
                rowNum.eachCell(function (cell) {
                    cell.alignment = { vertical: 'middle', horizontal: 'center' };
                    cell.border = {
                        top: {style:'thin'},
                        left: {style:'thin'},
                        bottom: {style:'thin'},
                        right: {style:'thin'}
                    }
                })
            })

        })
    })

    res.setHeader('Content-Type', 'application/vnd.ms-excel');
    res.setHeader("Content-Disposition", `attachment; filename=Product_2323.xlsx`);
    workbook.xlsx.write(res).then(function () {
        res.end();
    });
})

const calculateBetPrice = (price, round) => {
    let percent = 0;
    switch (round) {
        case 0:
            percent = 0;
            break;
        case 1:
            percent = 40;
            break;
        case 2:
            percent = 80;
            break;
        case 3:
            percent = 90;
            break;
        case 4:
            percent = 90;
            break;
        case 5:
            percent = 90;
            break;
    }
    return decrease(price, percent);
};

const calculateBetPriceByTime = (price, current, match) => {
    if (price <= 20000) {
        return price;
    } else {
        let diff =(current.getTime() - match.getTime()) / 1000;
        diff /= (60 * 60);

        const gapHour = Math.abs(Math.round(diff));
        let percent = 0;

        if (12 <= gapHour) {
            percent = 70;
        } else if (11 === gapHour || 10 === gapHour || 9 === gapHour || 8 === gapHour || 7 === gapHour) {
            percent = 65;
        } else if (6 === gapHour || 5 === gapHour || 4 === gapHour ) {
            percent = 50;
        } else if (3 === gapHour || 2 === gapHour ) {
            percent = 35;
        } else {
            percent = 0;
        }

        return RoundTo(parseFloat(decrease(price, percent)), 0);
    }
};

const decrease = (price, percent) => {
    return (price * ( (100 - percent) / 100)).toFixed(2);
};

module.exports = router;
