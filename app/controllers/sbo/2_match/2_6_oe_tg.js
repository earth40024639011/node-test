const express     = require('express');
const router      = express.Router();
const _           = require('underscore');
const parallel    = require("async/parallel");
const Redis       = require('../1_common/1_redis/1_1_redis');
const httpRequest = require('request');

const FootballSBOModel = require('../../../models/footballSBO.model');
const MemberService    = require('../../../common/memberService');
const Commons          = require('../../../common/commons');
const RoundTo          = require('round-to');

const LOG       = 'MATCH_OE_&_TG';//v1

router.get('/', (req, res) => {
    const now_date = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const {
        _id,
        username,
        currency
    } = req.userInfo;
    parallel([
            (callback) => {
                MemberService.getLimitSetting(_id, (errLimitSetting, dataLimitSetting)=>{
                    if(errLimitSetting){
                        callback(errLimitSetting, null);
                    } else {
                        callback(null, dataLimitSetting.limitSetting);
                    }
                });
            },

            (callback) => {
                MemberService.getOddAdjustment(username, (err, data) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, data);
                    }
                });
            },

            (callback) => {
                Redis.getByKey('sboToday', (error, result) => {
                    if (error) {
                        callback(null, false);
                    } else {
                        callback(null, result);
                    }
                })
            },

            (callback) => {
                FootballSBOModel.find({})
                    .lean()
                    .sort('sk')
                    .select('k n r rp')
                    .exec((error, response) => {
                        if (error) {
                            callback(error, null);
                        } else {
                            callback(null, response);
                        }
                    });
            },

            (callback) => {
                Redis.getByKey('sboMoreToday', (error, result) => {
                    if (error) {
                        callback(null, false);
                    } else {
                        callback(null, result);
                    }
                })
            }
        ],
        (error, parallelResult) => {
            if (error) {
                return res.send({
                    code    : 9999,
                    message : error
                });
            } else {
                let [
                    limitSetting,
                    oddAdjust,
                    sboToday,
                    footballSBOModel,
                    sboMoreToday
                ] = parallelResult;

                const configMatch = Commons.configMatch();
                // console.log('oddAdjust => ',JSON.stringify(oddAdjust));
                // console.log(sboToday);
                if (!_.isEmpty(sboToday)) {
                    _.chain(sboToday)
                        .each(league => {
                            const sboObjLeague = _.chain(footballSBOModel)
                                .filter(sboLeague => {
                                    return _.isEqual(parseInt(sboLeague.k), league.k)
                                })
                                .first()
                                .value();

                            if (!_.isUndefined(sboObjLeague)) {
                                league.m = _.filter(league.m, match => {
                                    return new Date(match.d) >= now_date;
                                });

                                _.each(league.m, match => {
                                    const matchDate = new Date(match.d);

                                    match.bp = _.filter(match.bp, betPrice => {
                                        return  !_.isUndefined(betPrice.oe);
                                    });

                                    _.each(match.bp, betPrice => {
                                        delete betPrice.ah;
                                        delete betPrice.ah1st;
                                        delete betPrice.ou;
                                        delete betPrice.ou1st;
                                        delete betPrice.x12;
                                        delete betPrice.x121st;
                                        delete betPrice.oe1st;

                                        const odd = _.filter(oddAdjust, obj =>{
                                            return _.isEqual(obj.matchId, match.id);
                                        });
                                        // console.log(odd);
                                        let oe = {o: 0, e: 0};

                                        // console.log(`============== ${match.id} =============`);
                                        // console.log(`oe => ${JSON.stringify(oe)}`);

                                        if(!_.isEmpty(odd)){
                                            _.each(odd, o => {
                                                const {
                                                    prefix,
                                                    key,
                                                    value,
                                                    count
                                                } = o;

                                                if(_.isEqual(prefix, 'oe') && !_.isUndefined(betPrice.oe)) {

                                                    if(_.isEqual(key, 'ok') && _.isEqual(value, betPrice.oe.ok)){
                                                        oe = {
                                                            o: -count,
                                                            e: count
                                                        }
                                                    } else if(_.isEqual(key, 'ek') && _.isEqual(value, betPrice.oe.ek)){
                                                        oe = {
                                                            o: count,
                                                            e: -count
                                                        }
                                                    }

                                                }
                                            });
                                        }

                                        // ---------------------------  oe  ----------------------------------
                                        if(betPrice.oe){
                                            if(betPrice.oe.o || betPrice.oe.e){
                                                Commons.cal_oe(betPrice.oe.o, betPrice.oe.e, sboObjLeague.rp.hdp.oe.f, sboObjLeague.rp.hdp.oe.u, configMatch.oe, req.userInfo.commissionSetting.sportsBook.typeHdpOuOe, oe, (err, data)=>{
                                                    if(data){
                                                        betPrice.oe.o = data.o;
                                                        betPrice.oe.e = data.e;
                                                    }
                                                })
                                            }
                                        }

                                        const moreToday = _.findWhere(sboMoreToday, JSON.parse(`{"id":"${match.id}"}`));
                                        // console.log(JSON.stringify(moreToday));
                                        if (!_.isUndefined(moreToday) && !_.isEmpty(moreToday.more) && !_.isUndefined(moreToday.more)) {
                                            const more = _.first(moreToday.more);
                                            const objTG = more['tg'];
                                            if (!_.isUndefined(objTG)) {
                                                betPrice.tg = objTG;
                                            }
                                        }

                                    });

                                    const {
                                        oe
                                    } = sboObjLeague.r;
                                    _.each(match.bp, (betPrice, round) => {
                                        betPrice.r = Commons.rule();

                                        betPrice.r.oe.ma = calculateBetPriceByTime(calculateBetPrice(Math.min(oe.ma, limitSetting.sportsBook.hdpOuOe.maxPerBet), round), now_date, matchDate);
                                        betPrice.r.oe.mi = oe.mi;

                                        betPrice.r.tg = {};
                                        betPrice.r.tg.ma = 0;
                                        betPrice.r.tg.mi = 0;
                                        delete betPrice.r.ah;
                                        delete betPrice.r.ah1st;
                                        delete betPrice.r.ou;
                                        delete betPrice.r.ou1st;
                                        delete betPrice.r.x12;
                                        delete betPrice.r.x121st;
                                        delete betPrice.r.oe1st;
                                    });
                                });

                                league.m = _.reject(league.m, match => {
                                    return _.size(match.bp) === 0;
                                });
                            } else {
                                httpRequest.get(
                                    `http://lb-sbo-master.api-hub.com/external/sboLeague/${league.k}`,
                                    (error, result) => {
                                        if (error) {
                                            console.error(error);
                                        } else {
                                            const model = JSON.parse(result.body).result;
                                            const footballSBOModel = new FootballSBOModel(model);
                                            footballSBOModel.save((error, data) => {
                                                if (error) {
                                                    console.error(error);
                                                } else {
                                                    console.log(data);
                                                }
                                            });
                                        }
                                    });
                                console.error(`sboObjLeague not found ${league.k}`);
                            }
                        })
                        .value();

                    sboToday = _.reject(sboToday, league => {
                        return _.size(league.m) === 0;
                    });

                    console.log(JSON.stringify(sboToday));
                    return res.send({
                        message: "success",
                        result: sboToday,
                        code: 0
                    });

                } else {
                    return res.send({
                        message: "success",
                        result: [],
                        code: 0
                    });
                }
            }
    });
});

const calculateBetPrice = (price, round) => {
    let percent = 0;
    switch (round) {
        case 0:
            percent = 0;
            break;
        case 1:
            percent = 40;
            break;
        case 2:
            percent = 80;
            break;
        case 3:
            percent = 90;
            break;
        case 4:
            percent = 90;
            break;
        case 5:
            percent = 90;
            break;
    }
    return decrease(price, percent);
};
const calculateBetPriceByTime = (price, current, match) => {
    if (price <= 20000) {
        return price;
    } else {
        let diff =(current.getTime() - match.getTime()) / 1000;
        diff /= (60 * 60);

        const gapHour = Math.abs(Math.round(diff));
        let percent = 0;

        if (12 <= gapHour) {
            percent = 70;
        } else if (11 === gapHour || 10 === gapHour || 9 === gapHour || 8 === gapHour || 7 === gapHour) {
            percent = 65;
        } else if (6 === gapHour || 5 === gapHour || 4 === gapHour ) {
            percent = 50;
        } else if (3 === gapHour || 2 === gapHour ) {
            percent = 35;
        } else {
            percent = 0;
        }

        return RoundTo(parseFloat(decrease(price, percent)), 0);
    }
};
const decrease = (price, percent) => {
    return (price * ( (100 - percent) / 100)).toFixed(2);
};

module.exports = router;
