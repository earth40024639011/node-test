const express   = require('express');
const router    = express.Router();
const Redis     = require('../1_common/1_redis/1_1_redis');
const LOG       = 'SPORT_VOLLEYBALL_RESULT';

router.get('/previous', (req, res) => {
    Redis.getByKey('resultAmericanFootballPreviousAMBFM', (error, response) => {
        if (error) {
            return res.send(
                {
                    code: 0,
                    message: "Error",
                    result: error
                });
        } else {
            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: response
                });
        }
    });
});
router.get('/today', (req, res) => {
    Redis.getByKey('resultAmericanFootballTodayAMBFM', (error, response) => {
        if (error) {
            return res.send(
                {
                    code: 0,
                    message: "Error",
                    result: error
                });
        } else {
            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: response
                });
        }
    });
});
module.exports = router;
