const express = require('express');
const router  = express.Router();
const request = require('request');
const _       = require('underscore');
const parallel         = require("async/parallel");
const FootballSBOModel = require('../../../models/footballSBO.model');
const Redis            = require('../1_common/1_redis/1_1_redis');
const CLIENT_NAME      = process.env.CLIENT_NAME          || 'SPORTBOOK88';
const LOG = 'SETTING_SBO_LEAGUE';
router.post('/', (req, res) => {
    console.log(`[${LOG}] ${JSON.stringify(req.body)}`);

    const {
        k,
        n
    } = req.body;

    const footballSBOModel = new FootballSBOModel();
    footballSBOModel.k = k;
    footballSBOModel.n = n;

    footballSBOModel.save((err, data) => {
        if (err) {
            return res.send({
                code: 0,
                message: "error",
                data: err
            });
        } else {
            Redis.deleteByKey(`FootballSBOModel${CLIENT_NAME}`, (error, result) => {

            });
            Redis.deleteByKey(`FootballSBOModelToday${CLIENT_NAME}`, (error, result) => {

            });
            Redis.deleteByKey(`FootballSBOModelLive${CLIENT_NAME}`, (error, result) => {

            });
            Redis.deleteByKey(`FootballSBOModelEarlyMarket${CLIENT_NAME}`, (error, result) => {

            });
            return res.send({
                code: 0,
                message: "success"
            });
        }
    });
});


//[3]
router.get('/', (req, res) => {
    parallel([
            (callback) => {
                Redis.getByKey('sboLive', (error, response) => {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            },

            (callback) => {
                Redis.getByKey('sboEarlyMarket', (error, response) => {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            },

            (callback) => {
                Redis.getByKey('sboToday', (error, response) => {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            }
        ],
        (error, parallelResult) => {
            if (error) {
                return res.send(
                    {
                        code: 0,
                        message: "error",
                        result: error
                    });
            } else {
                const [
                    sboLive,
                    sboEarlyMarket,
                    sboToday
                ] = parallelResult;

                let sboTodayK = _.chain(sboToday)
                    .flatten(true)
                    .pluck('k')
                    .value();

                let sboLiveK = _.chain(sboLive)
                    .flatten(true)
                    .pluck('k')
                    .value();

                let sboEarlyMarketK = _.chain(sboEarlyMarket)
                    .flatten(true)
                    .pluck('k')
                    .value();
                sboTodayK.push(...sboLiveK);
                sboTodayK.push(...sboEarlyMarketK);
                FootballSBOModel.find({
                    k: {
                        $in: sboTodayK
                    }
                })
                    .lean()
                    .sort('sk')
                    .select('sk k n r rl rem rp')
                    .exec((error, response) => {
                        if (error) {
                            return res.send({
                                code: 0,
                                message: "error",
                                result: error
                            });
                        } else {
                            Redis.deleteByKey(`FootballSBOModel${CLIENT_NAME}`, (error, result) => {

                            });
                            Redis.deleteByKey(`FootballSBOModelToday${CLIENT_NAME}`, (error, result) => {

                            });
                            Redis.deleteByKey(`FootballSBOModelLive${CLIENT_NAME}`, (error, result) => {

                            });
                            Redis.deleteByKey(`FootballSBOModelEarlyMarket${CLIENT_NAME}`, (error, result) => {

                            });

                            _.each(response, league => {
                                if (!_.isEqual(league.n.en, league.n.th)) {
                                    league.n.en = league.n.en + ` (${league.n.th})`
                                }
                            });

                            return res.send({
                                code: 0,
                                message: `success`,
                                result: _.sortBy(response, 'sk')
                            });
                        }
                    });

            }
        });
});
module.exports = router;