const express = require('express');
const router  = express.Router();
const request = require('request');
const _       = require('underscore');

const FootballSBOModel = require('../../../models/footballSBO.model');
const Redis            = require('../1_common/1_redis/1_1_redis');
const CLIENT_NAME      = process.env.CLIENT_NAME          || 'SPORTBOOK88';

const LOG = 'SETTING_MIN-MAX';
router.put('/:leagueId/:sk/today', (req, res) => {
    const {
        leagueId,
        sk
    } = req.params;
    const {
        r
    } = req.body;

    FootballSBOModel.update({
            k: parseInt(leagueId)
        },
        {
            $set: {
                'r': r,
                'sk': sk
            }
        },
        {
            multi: false
        },
        (err, data) => {
            if (err) {
                console.log(`[${LOG}] Error update minx-max league today : ${leagueId} ${r}`);
                console.log(err);
                return res.send({
                    message: "error",
                    code: 999,
                    result : err
                });
            } else if (data.nModified === 0) {
                console.log(`[${LOG}] Failed update minx-max league today : ${leagueId} ${r}`);
                Redis.deleteByKey(`FootballSBOModel${CLIENT_NAME}`, (error, result) => {

                });
                Redis.deleteByKey(`FootballSBOModelToday${CLIENT_NAME}`, (error, result) => {

                });
                Redis.deleteByKey(`FootballSBOModelLive${CLIENT_NAME}`, (error, result) => {

                });
                Redis.deleteByKey(`FootballSBOModelEarlyMarket${CLIENT_NAME}`, (error, result) => {

                });
                return res.send({
                    message: "failed",
                    code: 4,
                    result : data
                });
            } else {
                console.log(`[${LOG}] Success update minx-max league today : ${leagueId} ${r}`);
                Redis.deleteByKey(`FootballSBOModel${CLIENT_NAME}`, (error, result) => {

                });
                Redis.deleteByKey(`FootballSBOModelToday${CLIENT_NAME}`, (error, result) => {

                });
                Redis.deleteByKey(`FootballSBOModelLive${CLIENT_NAME}`, (error, result) => {

                });
                Redis.deleteByKey(`FootballSBOModelEarlyMarket${CLIENT_NAME}`, (error, result) => {

                });
                return res.send({
                    message: "success",
                    code: 0,
                    result : _.first(data)
                });
            }
        });
});
router.put('/:leagueId/:sk/earlyMarket', (req, res) => {
    const {
        leagueId,
        sk
    } = req.params;
    const {
        rem
    } = req.body;

    FootballSBOModel.update({
            k: parseInt(leagueId)
        },
        {
            $set: {
                'rem': rem,
                'sk': sk
            }
        },
        {
            multi: false
        },
        (err, data) => {
            if (err) {
                console.log(`[${LOG}] Error update league minx-max early market : ${leagueId} ${rem}`);
                console.log(err);
                return res.send({
                    message: "error",
                    code: 999,
                    result : err
                });
            } else if (data.nModified === 0) {
                console.log(`[${LOG}] Failed update league minx-max early market : ${leagueId} ${rem}`);
                Redis.deleteByKey(`FootballSBOModel${CLIENT_NAME}`, (error, result) => {

                });
                Redis.deleteByKey(`FootballSBOModelToday${CLIENT_NAME}`, (error, result) => {

                });
                Redis.deleteByKey(`FootballSBOModelLive${CLIENT_NAME}`, (error, result) => {

                });
                Redis.deleteByKey(`FootballSBOModelEarlyMarket${CLIENT_NAME}`, (error, result) => {

                });
                return res.send({
                    message: "failed",
                    code: 4,
                    result : data
                });
            } else {
                console.log(`[${LOG}] Success update league minx-max early market : ${leagueId} ${rem}`);
                Redis.deleteByKey(`FootballSBOModel${CLIENT_NAME}`, (error, result) => {

                });
                Redis.deleteByKey(`FootballSBOModelToday${CLIENT_NAME}`, (error, result) => {

                });
                Redis.deleteByKey(`FootballSBOModelLive${CLIENT_NAME}`, (error, result) => {

                });
                Redis.deleteByKey(`FootballSBOModelEarlyMarket${CLIENT_NAME}`, (error, result) => {

                });
                return res.send({
                    message: "success",
                    code: 0,
                    result : _.first(data)
                });
            }
        });
});
router.put('/:leagueId/:sk/live', (req, res) => {
    const {
        leagueId,
        sk
    } = req.params;
    const {
        rl
    } = req.body;

    FootballSBOModel.update({
            k: parseInt(leagueId)
        },
        {
            $set: {
                'rl': rl,
                'sk': sk
            }
        },
        {
            multi: false
        },
        (err, data) => {
            if (err) {
                console.log(`[${LOG}] Error update league minx-max live : ${leagueId} ${rl}`);
                console.log(err);
                return res.send({
                    message: "error",
                    code: 999,
                    result : err
                });
            } else if (data.nModified === 0) {
                console.log(`[${LOG}] Failed update league minx-max live : ${leagueId} ${rl}`);
                Redis.deleteByKey(`FootballSBOModel${CLIENT_NAME}`, (error, result) => {

                });
                Redis.deleteByKey(`FootballSBOModelToday${CLIENT_NAME}`, (error, result) => {

                });
                Redis.deleteByKey(`FootballSBOModelLive${CLIENT_NAME}`, (error, result) => {

                });
                Redis.deleteByKey(`FootballSBOModelEarlyMarket${CLIENT_NAME}`, (error, result) => {

                });
                return res.send({
                    message: "failed",
                    code: 4,
                    result : data
                });
            } else {
                console.log(`[${LOG}] Success update league minx-max live : ${leagueId} ${rl}`);
                Redis.deleteByKey(`FootballSBOModel${CLIENT_NAME}`, (error, result) => {

                });
                Redis.deleteByKey(`FootballSBOModelToday${CLIENT_NAME}`, (error, result) => {

                });
                Redis.deleteByKey(`FootballSBOModelLive${CLIENT_NAME}`, (error, result) => {

                });
                Redis.deleteByKey(`FootballSBOModelEarlyMarket${CLIENT_NAME}`, (error, result) => {

                });
                return res.send({
                    message: "success",
                    code: 0,
                    result : _.first(data)
                });
            }
        });
});
module.exports = router;