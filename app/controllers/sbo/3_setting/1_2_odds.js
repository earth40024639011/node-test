const express = require('express');
const router  = express.Router();
const request = require('request');
const _       = require('underscore');

const FootballSBOModel  = require('../../../models/footballSBO.model');
const AdjustmentByMatch = require('../../../models/oddAdjustmentByMatch.model');

const Redis            = require('../1_common/1_redis/1_1_redis');
const CLIENT_NAME      = process.env.CLIENT_NAME          || 'SPORTBOOK88';

const LOG = 'SETTING_ODDS';
router.put('/:leagueId', (req, res) => {
    const {
        leagueId
    } = req.params;
    const {
        rp
    } = req.body;

    FootballSBOModel.update({
            k: parseInt(leagueId)
        },
        {
            $set: {
                'rp': rp
            }
        },
        {
            multi: false
        },
        (err, data) => {
            if (err) {
                console.log(`[${LOG}] Error update league odds : ${leagueId} ${rp}`);
                console.log(err);
                return res.send({
                    message: "error",
                    code: 999,
                    result : err
                });
            } else if (data.nModified === 0) {
                console.log(`[${LOG}] Failed update league odds : ${leagueId} ${rp}`);
                Redis.deleteByKey(`FootballSBOModel${CLIENT_NAME}`, (error, result) => {

                });
                Redis.deleteByKey(`FootballSBOModelToday${CLIENT_NAME}`, (error, result) => {

                });
                Redis.deleteByKey(`FootballSBOModelLive${CLIENT_NAME}`, (error, result) => {

                });
                Redis.deleteByKey(`FootballSBOModelEarlyMarket${CLIENT_NAME}`, (error, result) => {

                });
                return res.send({
                    message: "failed",
                    code: 4,
                    result : data
                });
            } else {
                console.log(`[${LOG}] Success update league odds : ${leagueId} ${rp}`);
                Redis.deleteByKey(`FootballSBOModel${CLIENT_NAME}`, (error, result) => {

                });
                Redis.deleteByKey(`FootballSBOModelToday${CLIENT_NAME}`, (error, result) => {

                });
                Redis.deleteByKey(`FootballSBOModelLive${CLIENT_NAME}`, (error, result) => {

                });
                Redis.deleteByKey(`FootballSBOModelEarlyMarket${CLIENT_NAME}`, (error, result) => {

                });
                return res.send({
                    message: "success",
                    code: 0,
                    result : _.first(data)
                });
            }
        });
});

router.put('/match/:matchId', (req, res) => {
    const {
        matchId
    } = req.params;
    const userInfo       = req.userInfo;
    if (userInfo.group.type === 'COMPANY' || userInfo.group.type === 'SUPER_ADMIN') {
        const {
            ah,
            ou,
            ah1st,
            ou1st
        } = req.body;
        AdjustmentByMatch.update({
                id: matchId
            },
            {
                $set: {
                    ah,
                    ou,
                    ah1st,
                    ou1st
                }
            },
            {
                multi: false
            },
            (err, data) => {
                if (err) {
                    console.log(`[${LOG}] Error update league odds : ${matchId}`);
                    console.log(err);
                    return res.send({
                        message: "error",
                        code: 999,
                        result : err
                    });
                } else if (data.nModified === 0) {
                    console.log(`[${LOG}] Failed update league odds : ${matchId} ${JSON.stringify(req.body)}`);
                    const model = new AdjustmentByMatch();
                    model.id = matchId;
                    model.ah = ah;
                    model.ou = ou;
                    model.ah1st = ah1st;
                    model.ou1st = ou1st;
                    model.save((err) => {});
                    return res.send({
                        message: "success",
                        code: 0
                    });
                } else {
                    console.log(`[${LOG}] Success update league odds : ${matchId} ${JSON.stringify(req.body)}`);
                    return res.send({
                        message: "success",
                        code: 0,
                        result : _.first(data)
                    });
                }
            });
    } else {
        return res.send({
            message: "Your user are not allow.",
            code: 90001,
        });
    }
});
router.get('/match', (req, res) => {
    AdjustmentByMatch.find({})
        .lean()
        .exec((error, response) => {
            if (error) {
                return res.send({
                    message: "error",
                    code: 999,
                    result : error
                });
            } else {
                return res.send({
                    message: "success",
                    code: 0,
                    result : response
                });
            }
        });
});
module.exports = router;