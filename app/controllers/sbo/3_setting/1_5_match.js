const express = require('express');
const router  = express.Router();
const _       = require('underscore');
const parallel= require("async/parallel");
const CloseMatch = require('../../../models/closeMatch.model');

const Redis            = require('../1_common/1_redis/1_1_redis');
const CLIENT_NAME      = process.env.CLIENT_NAME          || 'SPORTBOOK88';

const LOG = 'SETTING_MATCH';
router.get('/on/:matchId', (req, res) => {
    const {
        matchId
    } = req.params;
    CloseMatch.remove({id: matchId}, (err) =>{ });
    return res.send({
        message: "success",
        code: 0
    });
});
router.get('/off/:matchId', (req, res) => {
    const {
        matchId
    } = req.params;
    const model = new CloseMatch();
    model.id = matchId;
    model.save((err) => {});
    return res.send({
        message: "success",
        code: 0
    });
});

router.get('/today', (req, res) => {
    parallel([
            (callback) => {
                Redis.getByKey('sboToday', (error, result) => {
                    if (error) {
                        callback(null, false);
                    } else {
                        callback(null, result);
                    }
                })
            },

            (callback) => {
                CloseMatch.find({
                })
                    .lean()
                    .exec((error, response) => {
                        if (error) {
                            callback(error, null);
                        } else {
                            if (_.isEmpty(response)) {
                                callback(null, []);
                            } else {
                                callback(null, _.chain(response)
                                    .pluck('id')
                                    .flatten(true)
                                    .value());
                            }
                        }
                    });
            }
        ],
        (error, parallelResult) => {
            if (error) {
                return res.send({
                    message: "success",
                    code: 0,
                    result : []
                });
            } else {
                const [
                    sboToday,
                    closeMatchList
                ] = parallelResult;
                _.chain(sboToday)
                    .each(league => {
                        _.each(league.m, match => {
                            delete match.bp;
                            delete match.hasParlay;
                            delete match.liveTv;
                            delete match.i;
                            delete match.md5;
                            delete match.moreSize;
                            match.isOff = _.contains(closeMatchList, match.id);
                        });
                        delete league.ms;
                    })
                    .value();

                return res.send({
                    message: "success",
                    code: 0,
                    result : sboToday
                });
            }
    });
});
router.get('/early', (req, res) => {
    parallel([
            (callback) => {
                Redis.getByKey('sboEarlyMarket', (error, result) => {
                    if (error) {
                        callback(null, false);
                    } else {
                        callback(null, result);
                    }
                })
            },

            (callback) => {
                CloseMatch.find({
                })
                    .lean()
                    .exec((error, response) => {
                        if (error) {
                            callback(error, null);
                        } else {
                            if (_.isEmpty(response)) {
                                callback(null, []);
                            } else {
                                callback(null, _.chain(response)
                                    .pluck('id')
                                    .flatten(true)
                                    .value());
                            }
                        }
                    });
            }
        ],
        (error, parallelResult) => {
            if (error) {
                return res.send({
                    message: "success",
                    code: 0,
                    result : []
                });
            } else {
                const [
                    sboToday,
                    closeMatchList
                ] = parallelResult;
                _.chain(sboToday)
                    .each(league => {
                        _.each(league.m, match => {
                            delete match.bp;
                            delete match.hasParlay;
                            delete match.liveTv;
                            delete match.i;
                            delete match.md5;
                            delete match.moreSize;
                            match.isOff = _.contains(closeMatchList, match.id);
                        });
                        delete league.ms;
                    })
                    .value();

                return res.send({
                    message: "success",
                    code: 0,
                    result : sboToday
                });
            }
        });
});
router.get('/live', (req, res) => {
    parallel([
            (callback) => {
                Redis.getByKey('sboLive', (error, result) => {
                    if (error) {
                        callback(null, false);
                    } else {
                        callback(null, result);
                    }
                })
            },

            (callback) => {
                CloseMatch.find({
                })
                    .lean()
                    .exec((error, response) => {
                        if (error) {
                            callback(error, null);
                        } else {
                            if (_.isEmpty(response)) {
                                callback(null, []);
                            } else {
                                callback(null, _.chain(response)
                                    .pluck('id')
                                    .flatten(true)
                                    .value());
                            }
                        }
                    });
            }
        ],
        (error, parallelResult) => {
            if (error) {
                return res.send({
                    message: "success",
                    code: 0,
                    result : []
                });
            } else {
                const [
                    sboToday,
                    closeMatchList
                ] = parallelResult;
                _.chain(sboToday)
                    .each(league => {
                        _.each(league.m, match => {
                            delete match.bpl;
                            delete match.hasParlay;
                            delete match.liveTv;
                            delete match.md5;
                            delete match.moreSize;
                            match.isOff = _.contains(closeMatchList, match.id);
                        });
                        delete league.ms;
                    })
                    .value();

                return res.send({
                    message: "success",
                    code: 0,
                    result : sboToday
                });
            }
        });
});
module.exports = router;