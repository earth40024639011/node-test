const express = require('express');
const router  = express.Router();
const _       = require('underscore');

const RugbySBOModel = require('../../../../models/rugbySBO.model');
const Redis               = require('../../1_common/1_redis/1_1_redis');
const CLIENT_NAME         = process.env.CLIENT_NAME          || 'SPORTBOOK88';
const LOG = 'SETTING_SK';
router.put('/:leagueId/:sk', (req, res) => {
    const {
        leagueId,
        sk
    } = req.params;
    Redis.deleteByKey(`RugbySBOModel${CLIENT_NAME}`, (error, result) => {

    });
    RugbySBOModel.update({
            k: parseInt(leagueId)
        },
        {
            $set: {
                'sk': sk
            }
        },
        {
            multi: false
        },
        (err, data) => {
            if (err) {
                console.log(`[${LOG}] Error update league sk : ${leagueId} ${sk}`);
                console.log(err);
            } else if (data.nModified === 0) {
                console.log(`[${LOG}] Failed update league sk : ${leagueId} ${sk}`);
            } else {
                console.log(`[${LOG}] Success update league sk : ${leagueId} ${sk}`);
                return res.send({
                    message: "success",
                    code: 0,
                    result : _.first(data)
                });
            }
        });
});
module.exports = router;