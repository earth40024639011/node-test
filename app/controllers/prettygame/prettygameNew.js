const express = require('express');
const router = express.Router();
const request = require('request');
const moment = require('moment');
const crypto = require('crypto');
const querystring = require('querystring');
const async = require("async");
const config = require('config');
const MemberService = require('../../common/memberService');
const Joi = require('joi');
const DateUtils = require('../../common/dateUtils');
const roundTo = require('round-to');
const mongoose = require('mongoose');
const _ = require('underscore');
const AgentGroupModel = require('../../models/agentGroup.model.js');
const NumberUtils = require('../../common/numberUtils1');
const AgentService = require('../../common/agentService');
const PrettyGamingService = require('../../common/prettygameServiceNew');
const MemberModel = require("../../models/member.model");

const xml2js = require('xml2js');

const SaTransactionModel = require('../../models/saTransaction.model.js');
const BetTransactionModel = require('../../models/betTransaction.model.js');


const DRAW = "DRAW";
const WIN = "WIN";
const HALF_WIN = "HALF_WIN";
const LOSE = "LOSE";
const HALF_LOSE = "HALF_LOSE";


function random(low, high) {
    return Math.floor(Math.random() * (high - low + 1) + low)
}
function generateBetId() {
    return 'BET' + new Date().getTime() + random(1000000, 9999999);
}


const CLIENT_NAME = process.env.CLIENT_NAME;

//2.1. token_forward_game
router.get('/login', (req, res) => {


    console.log('------------------------------------------',req.query);

    let userInfo = req.userInfo;
    let client = req.query.client;

    console.log('CLIENT_NAME : ',CLIENT_NAME)


        // if (CLIENT_NAME == 'SPORTBOOK88' || CLIENT_NAME == 'AMB_SPORTBOOK') {

            async.waterfall([callback => {
                try {
                    AgentService.getUpLineStatus(userInfo.group._id, function (err, status) {
                        if (err) {
                            callback(err, null);
                        } else {
                            if (status.suspend) {
                                callback(5011, null);
                            } else {
                                callback(null, status);
                            }
                        }
                    });
                } catch (err) {
                    callback(9929, null);
                }

            }, (uplineStatus, callback) => {

                try {
                    MemberService.findById(userInfo._id, 'creditLimit balance limitSetting group suspend username_lower ipAddress', function (err, memberResponse) {
                        if (err) {
                            callback(err, null);
                        } else {

                            if (memberResponse.suspend) {
                                callback(5011, null);
                            } else if (memberResponse.limitSetting.casino.pt.isEnable) {
                                AgentService.getUpLineStatus(memberResponse.group._id, (err, response) => {
                                    if (!response.casinoPtEnable) {
                                        callback(1088, null);
                                    } else {
                                        callback(null, uplineStatus, memberResponse);
                                    }
                                });
                            } else {
                                callback(1088, null);
                            }
                        }
                    });
                } catch (err) {
                    callback(999, null);
                }

            }], function (err, uplineStatus, memberInfo) {

                if (err) {
                    if (err == 1088) {
                        return res.send({
                            code: 1088,
                            message: 'Service Locked, Please contact your upline.'
                        });
                    }
                    if (err == 5011) {
                        return res.send({
                            message: "Account or Upline was suspend.",
                            result: null,
                            code: 5011
                        });
                    }
                    return res.send({code: 999, message: err});
                }


                PrettyGamingService.callLoginRequest(memberInfo.username_lower, memberInfo.limitSetting.casino.pt.limit, (err, loginResponse) => {
                    if (err) {

                        return res.send({message: 'fail', code: 999});

                    } else {
                        if(client === "MB"){
                            return res.send({message: loginResponse.msg, code: loginResponse.code, url: loginResponse.data.uriMobile+'&game=BAC'});
                        }else {
                            return res.send({message: loginResponse.msg, code: loginResponse.code, url: loginResponse.data.uriDesktop+'&game=BAC'});
                        }



                        // PrettyGamingService.callEnterGameInterfaceRequest(memberInfo.username_lower, client, memberInfo.ipAddress, (err, loginResponse) => {
                        //
                        //
                        //     let outMsg = JSON.parse(loginResponse.OutMsg);
                        //     let url = outMsg.href;
                        //     let token = outMsg.param.replace('Token=', '');
                        //
                        //     let queryString = querystring.stringify({
                        //         token: token,
                        //         GameType: '0',
                        //         url: 'www.google.com'
                        //     });
                        //
                        //
                        //     let gameUrl = url; //+ '?' + queryString;
                        //
                        //     console.log('gameUrl : ',gameUrl);
                        //
                        //     return res.send({message: 'success', code: 0, url: gameUrl});
                        //
                        // });

                    }
                });

            });


        // }else {
        //
        //     return res.send({
        //         message: "Coming Soon.",
        //         result: null,
        //         code: 5011
        //     });
        //
        // }



});

function prepareCommission(memberInfo, body, currentAgent, overAgent, remaining, overAllShare) {


    if (currentAgent && (currentAgent.type === 'SUPER_ADMIN' || currentAgent.parentId)) {


        console.log('==============================================');
        console.log('process : ' + currentAgent.type + ' : ' + currentAgent.name);


        let money = body.amount;


        if (!overAgent) {
            overAgent = {};
            overAgent.type = 'member';
            overAgent.shareSetting = {};
            overAgent.shareSetting.casino = {};
            overAgent.shareSetting.casino.sa = {};
            overAgent.shareSetting.casino.sa.parent = memberInfo.shareSetting.casino.sa.parent;
            overAgent.shareSetting.casino.sa.own = 0;
            overAgent.shareSetting.casino.sa.remaining = 0;

            body.commission.member = {};
            body.commission.member.parent = memberInfo.shareSetting.casino.sa.parent;
            body.commission.member.commission = memberInfo.commissionSetting.casino.sa;

        }

        console.log('');
        console.log('---- setting ----');
        console.log('ส่วนแบ่งที่ได้มามีทั้งหมด : ' + currentAgent.shareSetting.casino.sa.own + ' %');
        console.log('ขอส่วนแบ่งจาก : ' + overAgent.type + ' ' + overAgent.shareSetting.casino.sa.parent + ' %');
        console.log('ให้ส่วนแบ่ง : ' + overAgent.type + ' ที่เหลือไป ' + overAgent.shareSetting.casino.sa.own + ' %');
        console.log('remaining จาก : ' + overAgent.type + ' : ' + overAgent.shareSetting.casino.sa.remaining + ' %');
        console.log('โดนบังคับเสียขั้นต่ำ : ' + currentAgent.shareSetting.casino.sa.min + ' %');
        console.log('---- end setting ----');
        console.log('');


        // console.log('และได้รับ remaining ไว้ : '+overAgent.shareSetting.casino.sa.remaining+' %');

        let currentPercentReceive = overAgent.shareSetting.casino.sa.parent;
        console.log('% ที่ได้ : ' + currentPercentReceive);
        console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');
        let totalRemaining = remaining;

        if (overAgent.shareSetting.casino.sa.remaining > 0) {
            // console.log()
            // console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');

            if (overAgent.shareSetting.casino.sa.remaining > totalRemaining) {
                console.log('รับ remaining ทั้งหมดไว้: ' + totalRemaining + ' %');
                currentPercentReceive += totalRemaining;
                totalRemaining = 0;
            } else {
                totalRemaining = remaining - overAgent.shareSetting.casino.sa.remaining;
                console.log('หักค่า remaining ที่ได้รับมากับที่เซตรับไว้ = ' + remaining + ' - ' + overAgent.shareSetting.casino.sa.remaining + ' = ' + totalRemaining);
                currentPercentReceive += overAgent.shareSetting.casino.sa.remaining;
            }

        }

        console.log('รวม % ที่ได้รับ : ' + currentPercentReceive);


        let unUseShare = currentAgent.shareSetting.casino.sa.own -
            (overAgent.shareSetting.casino.sa.parent + overAgent.shareSetting.casino.sa.own);

        console.log('เหลือส่วนแบ่งที่ไม่ได้ใช้ : ' + unUseShare + ' %');
        totalRemaining = (unUseShare + totalRemaining);
        console.log('ส่วนแบ่งที่ไม่ได้ใช้ บวกกับค่า remaining ท่ี่เหลือจะเป็น : ' + totalRemaining + ' %');

        console.log('จากที่โดนบังคับเสียขั้นต่ำ : ' + currentAgent.shareSetting.casino.sa.min + ' %');
        console.log('overAllShare : ', overAllShare);
        if (currentAgent.shareSetting.casino.sa.min > 0 && ((overAllShare + currentPercentReceive) < currentAgent.shareSetting.casino.sa.min)) {

            if (currentPercentReceive < currentAgent.shareSetting.casino.sa.min) {
                console.log('% ที่ได้รับ < ขั้นต่ำที่ถูกตั้ง min ไว้');
                let percent = currentAgent.shareSetting.casino.sa.min - (currentPercentReceive + overAllShare);
                console.log('หักออกไป ' + percent + ' % : แล้วไปเพิ่มในส่วนที่ต้องรับผิดชอบ ');
                totalRemaining = totalRemaining - percent;
                if (totalRemaining < 0) {
                    totalRemaining = 0;
                }
                currentPercentReceive += percent;
            }
        } else {
            // currentPercentReceive += totalRemaining;
        }


        if (currentAgent.type === 'SUPER_ADMIN') {
            currentPercentReceive += totalRemaining;
        }

        let getMoney = (money * NumberUtils.convertPercent(currentPercentReceive)).toFixed(2);
        overAllShare = overAllShare + currentPercentReceive;

        console.log('ยอดแทง ' + money + ' ได้ทั้งหมด : ' + currentPercentReceive + ' %');

        console.log('รวม % + % remaining  จะเป็นสัดส่วนที่ได้ทั้งหมด = ' + getMoney);
        console.log('ค่าที่จะถูกคืนกลับไป (remaining) : ' + totalRemaining + ' %');

        console.log('จำนวน % ที่ถูกแบ่งไปแล้วทั้งหมด : ', overAllShare);

        //set commission
        let agentCommission = currentAgent.commissionSetting.casino.sa;


        switch (currentAgent.type) {
            case 'SUPER_ADMIN':
                body.commission.superAdmin = {};
                body.commission.superAdmin.group = currentAgent._id;
                body.commission.superAdmin.parent = currentAgent.shareSetting.casino.sa.parent;
                body.commission.superAdmin.own = currentAgent.shareSetting.casino.sa.own;
                body.commission.superAdmin.remaining = currentAgent.shareSetting.casino.sa.remaining;
                body.commission.superAdmin.min = currentAgent.shareSetting.casino.sa.min;
                body.commission.superAdmin.shareReceive = Math.abs(currentPercentReceive);
                body.commission.superAdmin.commission = agentCommission;
                body.commission.superAdmin.amount = getMoney;
                break;
            case 'COMPANY':
                body.commission.company = {};
                body.commission.company.parentGroup = currentAgent.parentId;
                body.commission.company.group = currentAgent._id;
                body.commission.company.parent = currentAgent.shareSetting.casino.sa.parent;
                body.commission.company.own = currentAgent.shareSetting.casino.sa.own;
                body.commission.company.remaining = currentAgent.shareSetting.casino.sa.remaining;
                body.commission.company.min = currentAgent.shareSetting.casino.sa.min;
                body.commission.company.shareReceive = Math.abs(currentPercentReceive);
                body.commission.company.commission = agentCommission;
                body.commission.company.amount = getMoney;
                break;
            case 'SHARE_HOLDER':
                body.commission.shareHolder = {};
                body.commission.shareHolder.parentGroup = currentAgent.parentId;
                body.commission.shareHolder.group = currentAgent._id;
                body.commission.shareHolder.parent = currentAgent.shareSetting.casino.sa.parent;
                body.commission.shareHolder.own = currentAgent.shareSetting.casino.sa.own;
                body.commission.shareHolder.remaining = currentAgent.shareSetting.casino.sa.remaining;
                body.commission.shareHolder.min = currentAgent.shareSetting.casino.sa.min;
                body.commission.shareHolder.shareReceive = currentPercentReceive;
                body.commission.shareHolder.commission = agentCommission;
                body.commission.shareHolder.amount = getMoney;
                break;
            case 'SENIOR':
                body.commission.senior = {};
                body.commission.senior.parentGroup = currentAgent.parentId;
                body.commission.senior.group = currentAgent._id;
                body.commission.senior.parent = currentAgent.shareSetting.casino.sa.parent;
                body.commission.senior.own = currentAgent.shareSetting.casino.sa.own;
                body.commission.senior.remaining = currentAgent.shareSetting.casino.sa.remaining;
                body.commission.senior.min = currentAgent.shareSetting.casino.sa.min;
                body.commission.senior.shareReceive = currentPercentReceive;
                body.commission.senior.commission = agentCommission;
                body.commission.senior.amount = getMoney;
                break;
            case 'MASTER_AGENT':
                body.commission.masterAgent = {};
                body.commission.masterAgent.parentGroup = currentAgent.parentId;
                body.commission.masterAgent.group = currentAgent._id;
                body.commission.masterAgent.parent = currentAgent.shareSetting.casino.sa.parent;
                body.commission.masterAgent.own = currentAgent.shareSetting.casino.sa.own;
                body.commission.masterAgent.remaining = currentAgent.shareSetting.casino.sa.remaining;
                body.commission.masterAgent.min = currentAgent.shareSetting.casino.sa.min;
                body.commission.masterAgent.shareReceive = currentPercentReceive;
                body.commission.masterAgent.commission = agentCommission;
                body.commission.masterAgent.amount = getMoney;
                break;
            case 'AGENT':
                body.commission.agent = {};
                body.commission.agent.parentGroup = currentAgent.parentId;
                body.commission.agent.group = currentAgent._id;
                body.commission.agent.parent = currentAgent.shareSetting.casino.sa.parent;
                body.commission.agent.own = currentAgent.shareSetting.casino.sa.own;
                body.commission.agent.remaining = currentAgent.shareSetting.casino.sa.remaining;
                body.commission.agent.min = currentAgent.shareSetting.casino.sa.min;
                body.commission.agent.shareReceive = currentPercentReceive;
                body.commission.agent.commission = agentCommission;
                body.commission.agent.amount = getMoney;
                break;
        }

        prepareCommission(memberInfo, body, currentAgent.parentId, currentAgent, totalRemaining, overAllShare);
    }
}

function updateAgentMemberBalance(shareReceive, betAmount, ref, updateCallback) {

    console.log('updateAgentMemberBalance');
    console.log('shareReceive : ', shareReceive);


    let balance = betAmount + shareReceive.member.totalWinLoseCom;

    try {
        console.log('update member balance : ', balance);

        MemberService.updateBalance(shareReceive.member.id, balance, shareReceive.betId, 'SA_SETTLE', ref, function (err, updateBalanceResponse) {
            if (err) {
                updateCallback(err, null);
            } else {
                updateCallback(null, updateBalanceResponse);
            }
        });
    } catch (err) {
        MemberService.updateBalance(shareReceive.member.id, balance, shareReceive.betId, 'SA_SETTLE_ERROR ' + shareReceive.member.id + ' : ' + balance, ref, function (err, updateBalanceResponse) {
            if (err) {
                updateCallback(err, null);
            } else {
                updateCallback(null, updateBalanceResponse);
            }
        });
    }

}

function saveSaTransactionLog(request, action, callback) {

    let model = new SaTransactionModel(
        {
            username: request.username,
            currency: request.currency,
            amount: request.amount,
            txnId: request.txnid,
            gameType: request.gametype,
            platform: request.platform,
            gameCode: request.gamecode,
            hostId: request.hostid,
            gameId: request.gameid,
            txnReverseId: request.txn_reverse_id,
            payoutTime: request.PayoutTime,
            action: action,
            createdDate: DateUtils.getCurrentDate(),
        }
    );


    model.save((err, response) => {
        // console.log(err)
        // console.log('save allbet settle transaction')
        callback(null, 'success');
    });
}



module.exports = router;
