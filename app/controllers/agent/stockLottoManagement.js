const MemberModel = require('../../models/member.model');
const BetTransactionModel = require('../../models/betTransaction.model.js');
const AgentGroupModel = require('../../models/agentGroup.model.js');
const AgentLottoLimitModel = require('../../models/agentLottoLimit.model.js');
const AgentLottoLimitHistoryModel = require('../../models/agentLottoLimitHistory.model.js');
const MemberService = require('../../common/memberService');
const LottoService = require('../../common/lottoService');
const express = require('express');
const UserModel = require("../../models/users.model");
const router = express.Router();
const async = require("async");
const Joi = require('joi');
const mongoose = require('mongoose');
const config = require('config');
const _ = require('underscore');
const moment = require('moment');
const naturalSort = require("javascript-natural-sort");
const roundTo = require('round-to');
const ApiService = require('../../common/apiService');
const Hashing = require('../../common/hashing');
const DateUtils = require('../../common/dateUtils');
const nodeExcel = require('excel-export');
const Excel = require('exceljs');


const cancelBet = Joi.object().keys({
    password: Joi.string().required(),
    betId: Joi.string().required(),
    remark: Joi.string().allow(''),
});


router.post('/cancel-bet', (req, res) => {

    let validate = Joi.validate(req.body, cancelBet);
    if (validate.error) {
        console.log('Request Incomplete')
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 1001
        });
    }


    let requestBody = req.body;
    let userInfo = req.userInfo;


    let condition = {
        'betId': requestBody.betId,
        'game': 'LOTTO',
        'source': 'AMB_LOTTO'
    };


    async.series([callback => {

        UserModel.findById(userInfo._id, (err, agentResponse) => {

            if (agentResponse && (Hashing.encrypt256_1(requestBody.password) === agentResponse.password)) {
                callback(null, agentResponse)
            } else {
                callback(1001, null);
            }
        });

    }], (err, asyncResponse) => {

        if (err) {
            if (err === 1001) {
                return res.send({
                    code: 1001,
                    message: "Invalid Password"
                });
            }
            return res.send({
                code: 999,
                message: "error"
            });
        }

        BetTransactionModel.findOne(condition)
            .populate({path: 'memberParentGroup', select: 'type'})
            .populate({path: 'memberId', select: 'username'})
            .exec(function (err, betObj) {


                if (!betObj) {
                    return res.send({
                        code: 999,
                        message: "transaction not found"
                    });
                }

                if (betObj.status === 'DONE' || betObj.status === 'CANCELLED') {

                    return res.send({
                        code: 999,
                        message: "already cancel"
                    });
                }

                let betAmount = betObj.memberCredit * -1;


                LottoService.cancelById(betObj, (err, cancelResponse) => {

                    if (err) {
                        return res.send({
                            code: 999,
                            message: err.message
                        });
                    }


                    MemberService.updateBalance2(betObj.memberUsername, betAmount, betObj.betId, "LOTTO_CANCEL", betObj.betId, (err, updateBalanceResponse) => {

                        let updateBody = {
                            $set: {
                                'status': 'CANCELLED',
                                'cancelByType': 'AGENT',
                                'cancelByAgent': userInfo._id,
                                'cancelDate': DateUtils.getCurrentDate()
                            }
                        };

                        BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                            if (err) {
                                return res.send({
                                    code: 0,
                                    message: "success"
                                });

                            } else {

                                decreaseAgentLimit(betObj, (err, limitResponse) => {

                                    return res.send({
                                        code: 0,
                                        message: "success"
                                    });
                                });
                            }
                        });

                    });

                });

            });
    });

});


function decreaseAgentLimit(betTransaction, callback) {

    const winLose = betTransaction.commission;
    const betType = betTransaction.lotto.amb.betType;
    const betNumber = betTransaction.lotto.amb.betNumber;

    let updateAgentLimitList = [];

    if (winLose.company.group) {
        updateAgentLimitList.push({
            group: winLose.company.group,
            amount: winLose.company.amount
        });
    }
    if (winLose.shareHolder.group) {
        updateAgentLimitList.push({
            group: winLose.shareHolder.group,
            amount: winLose.shareHolder.amount
        });
    }
    if (winLose.senior.group) {
        updateAgentLimitList.push({
            group: winLose.senior.group,
            amount: winLose.senior.amount
        });
    }
    if (winLose.masterAgent.group) {
        updateAgentLimitList.push({
            group: winLose.masterAgent.group,
            amount: winLose.masterAgent.amount
        });
    }
    if (winLose.agent.group) {
        updateAgentLimitList.push({
            group: winLose.agent.group,
            amount: winLose.agent.amount
        });
    }


    console.log('xxxxxx ::: ', updateAgentLimitList);
    updateAgentLimit(updateAgentLimitList);

    function updateAgentLimit(lists) {

        if (lists.length == 0) {
            callback(null, 'success')
        } else {

            let item = lists.pop();

            if (item.group && item.amount !== 0) {

                let increaseAmountConditions = {};
                increaseAmountConditions['agent'] = mongoose.Types.ObjectId(item.group);
                increaseAmountConditions['po.' + betType + '.bets.betNumber'] = betNumber;

                let updateIncreaseAmountStatement = {};
                updateIncreaseAmountStatement['po.' + betType + '.bets.$.betAmount'] = item.amount * -1;

                AgentLottoLimitModel.update(increaseAmountConditions, {$inc: updateIncreaseAmountStatement}, function (err, updateLimitResponse) {
                    console.log('group : ' + item.group + ' , balance : ' + item.amount);
                    updateAgentLimit(lists);

                });
            } else {
                updateAgentLimit(lists);
            }
        }
    }

}


const voidTransactionSchema = Joi.object().keys({
    password: Joi.string().required(),
    refId: Joi.string().required(),
    gameId: Joi.string().required(),
    remark: Joi.string().allow(''),
});


router.post('/void-transaction', (req, res) => {

    let validate = Joi.validate(req.body, voidTransactionSchema);
    if (validate.error) {
        console.log('Request Incomplete');
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 1001
        });
    }


    let requestBody = req.body;
    let userInfo = req.userInfo;


    async.series([callback => {

        UserModel.findById(userInfo._id, (err, agentResponse) => {

            if (agentResponse && (Hashing.encrypt256_1(requestBody.password) === agentResponse.password)) {
                callback(null, agentResponse)
            } else {
                callback(1001, null);
            }
        });

    }], (err, asyncResponse) => {

        if (err) {
            if (err === 1001) {
                return res.send({
                    code: 1001,
                    message: "Invalid Password"
                });
            }
            return res.send({
                code: 999,
                message: "error"
            });
        }

        let condition = {
            'lotto.amb.betId': requestBody.refId,
            'lotto.amb.gameId': requestBody.gameId,
            'game': 'LOTTO',
            'source': 'AMB_LOTTO',
            'status': 'RUNNING'
        };


        BetTransactionModel.find(condition)
            .populate({path: 'memberParentGroup', select: 'type'})
            .populate({path: 'memberId', select: 'username'})
            .exec(function (err, transaction) {

                LottoService.cancelAll(transaction, (err, cancelResponse) => {

                    if (err) {
                        return res.send({
                            code: 999,
                            message: err.message
                        });
                    }

                    cancelBetTask(transaction, requestBody.remark);
                });

            });

        function cancelBetTask(transactions, remark) {
            if (transactions.length == 0) {
                return res.send({
                    code: 0,
                    message: "success"
                });
            } else {

                let item = transactions.pop();


                let betAmount = item.memberCredit * -1;


                MemberService.updateBalance2(item.memberUsername, betAmount, item.betId, "LOTTO_CANCEL", item.betId, (err, updateBalanceResponse) => {

                    let updateBody = {
                        $set: {
                            'status': 'CANCELLED',
                            'remark': remark,
                            'cancelByType': 'AGENT',
                            'cancelByAgent': userInfo._id,
                            'cancelDate': DateUtils.getCurrentDate()
                        }
                    };

                    BetTransactionModel.update({_id: item._id}, updateBody, function (err, response) {

                        decreaseAgentLimit(item, (err, limitResponse) => {
                            cancelBetTask(transactions);
                        });

                    });

                });

            }
        }
    });

});

router.get('/print-bet-list', function (req, res) {

    let condition = {game: 'LOTTO',source: 'AMB_LOTTO'};

    let userInfo = req.userInfo;

    // if (req.query.dateFrom && req.query.dateTo) {
    //     condition['createdDate'] = {
    //         "$gte": new Date(moment(req.query.dateFrom, "DD/MM/YYYY").format('YYYY-MM-DDT00:00:00.000')),
    //         "$lt": new Date(moment(req.query.dateTo, "DD/MM/YYYY").add(1, 'd').format('YYYY-MM-DDT00:00:00.000'))
    //     }
    // }

    if (req.query.gameId) {
        condition['lotto.amb.gameId'] = req.query.gameId;
    }


    let group = '';
    let child = '';
    if (userInfo.group.type === 'SUPER_ADMIN') {
        group = 'superAdmin';
        child = 'company';
    } else if (userInfo.group.type === 'COMPANY') {
        group = 'company';
        child = 'shareHolder';
    } else if (userInfo.group.type === 'SHARE_HOLDER') {
        group = 'shareHolder';
        child = 'superSenior';
    } else if (userInfo.group.type === 'SUPER_SENIOR') {
        group = 'superSenior';
        child = 'senior';
    } else if (userInfo.group.type === 'SENIOR') {
        group = 'senior';
        child = 'masterAgent';
    } else if (userInfo.group.type === 'MASTER_AGENT') {
        group = 'masterAgent';
        child = 'agent';
    } else if (userInfo.group.type === 'AGENT') {
        group = 'agent';
        child = 'member';
    }

    condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(userInfo.group._id);


    let page = req.query.page ? Number.parseInt(req.query.page) : 10;
    let limit = req.query.limit ? Number.parseInt(req.query.limit) : 1;

    async.waterfall([(callback => {

        LottoService.getGameList((err, gameList) => {
            callback(null, gameList[0]);
        });

    }), (game, callback) => {


        condition['lotto.amb.gameId'] = game.id;

        async.parallel([callback => {

            var aggregation = BetTransactionModel.aggregate([
                {
                    $match: condition
                },
                {
                    $project: {
                        _id: 0,
                        memberId: '$memberId',
                        lotto: '$lotto',
                        child: '$commission.' + child,
                        amount: '$amount',
                        payout: '$payout',
                        status: '$status',
                        share: '$commission.' + group + '.shareReceive',
                        receive: '$commission.' + group + '.amount',
                        stake_in: '$amount',
                        betId: '$betId',
                        createdDate: '$createdDate',
                        priceType: '$priceType',
                        gameType: '$gameType',
                        ipAddress: '$ipAddress',
                        isPrinted: '$isPrinted',
                        cancelByType: '$cancelByType',
                        cancelByAgent: '$cancelByAgent',
                        cancelDate: '$cancelDate'
                    }
                },
                {$sort: {'lotto.amb.betId': -1}},
                {
                    $skip: (page - 1) * limit
                }, {
                    $limit: limit
                }
            ]);

            aggregation.options = {allowDiskUse: true};
            aggregation.exec(function (err, results) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, results);
                }
            });
        }, callback => {

            let condition = {game: 'LOTTO'};

            condition['lotto.amb.gameId'] = game.id;
            condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(userInfo.group._id);
            condition['status'] = 'RUNNING';

            BetTransactionModel.aggregate([
                {
                    $match: condition
                },
                {
                    $group: {
                        _id: null,
                        amount: {$sum: '$amount'},
                        receive: {$sum: '$commission.' + group + '.amount'},
                        count: {$sum: 1}
                    }
                },
                {
                    $project: {
                        amount: '$amount',
                        receive: '$receive',
                        count: '$count'
                    }
                }
            ], (err, results) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, results[0] ? results[0] : {amount: 0, receive: 0, count: 0});
                }
            });
        }], (err, asyncResponse) => {

            if (err) {
                callback(err, null);
            } else {

                let info = {
                    list: asyncResponse[0],
                    info: asyncResponse[1]
                };

                callback(null, game, info);
            }
        })

    }, (game, betInfo, callback) => {

        let condition = {game: 'LOTTO',source:'AMB_LOTTO'};

        condition['lotto.amb.gameId'] = game.id;
        condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(userInfo.group._id);
        BetTransactionModel.count(condition, function (err, count) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, game, betInfo, count);
            }

        }).lean();
    }], (err, game, betInfo, dataCount) => {

        if (err)
            return res.send({message: "error", results: err, code: 999});
        let results = betInfo.list;
        let count = dataCount;


        async.series([(callback => {

            MemberModel.populate(results, {
                path: 'memberId',
                select: '_id username'
            }, function (err, memberResult) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, memberResult);
                }
            });


        })], (err, populateResults) => {

            if (err)
                return res.send({message: "error", results: err, code: 999});


            results = _.sortBy(results, function (o) {
                return o.status === "RUNNING" ? -1 : 1;
            });

            let summary = {
                totalAmount: betInfo.info.amount,
                totalReceive: betInfo.info.receive,
                successCount: betInfo.info.count
            }


            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: {
                        betList: results,
                        total: count,
                        limit: limit,
                        page: page,
                        pages: Math.ceil(count / limit) || 1,
                        summary: summary,
                        game: game

                    }
                }
            );
        });
    });
});


router.get('/gameList', function (req, res) {

    LottoService.getGameList((err, gameList) => {
        if (err)
            return res.send({message: "error", result: err, code: 999});

        return res.send({message: "success", result: gameList, code: 0});
    });
});


router.get('/list', function (req, res) {


    let condition = {game: 'LOTTO',source: 'AMB_LOTTO'};

    let userInfo = req.userInfo;

    if (req.query.dateFrom && req.query.dateTo) {
        condition['createdDate'] = {
            "$gte": moment(req.query.dateFrom, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).utc(true).toDate(),
            "$lt": moment(req.query.dateTo, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).add(1,'d').utc(true).toDate()
        }
    }

    if (req.query.memberId) {
        condition['memberId'] = mongoose.Types.ObjectId(req.query.memberId);
    }

    if (req.query.gameId) {
        condition['lotto.amb.gameId'] = req.query.gameId;
    }


    let orCondition = [];

    if (req.query.status) {
        orCondition.push({
            $or: req.query.status.split(',').map(function (status) {
                return {'status': status}
            })
        });
    }

    if (req.query.refId) {
        condition['lotto.amb.betId'] = req.query.refId;
    }

    if (req.query.betType && req.query.betType !== '_ALL') {
        condition['lotto.amb.betType'] = req.query.betType;
    }

    if (req.query.betNumber) {
        if (req.query.betType == '_2TOD' || req.query.betType == '_3TOD' || req.query.betType == '_4TOD') {
            condition['lotto.amb.betNumber'] = {$in: LottoService.generateTodList(req.query.betNumber)};
        } else {
            condition['lotto.amb.betNumber'] = req.query.betNumber;
        }
    }

    if (orCondition.length > 0) {
        condition['$and'] = orCondition;
    }

    let group = '';
    let child = '';
    if (userInfo.group.type === 'SUPER_ADMIN') {
        group = 'superAdmin';
        child = 'company';
    } else if (userInfo.group.type === 'COMPANY') {
        group = 'company';
        child = 'shareHolder';
    } else if (userInfo.group.type === 'SHARE_HOLDER') {
        group = 'shareHolder';
        child = 'superSenior';
    } else if (userInfo.group.type === 'SUPER_SENIOR') {
        group = 'superSenior';
        child = 'senior';
    } else if (userInfo.group.type === 'SENIOR') {
        group = 'senior';
        child = 'masterAgent';
    } else if (userInfo.group.type === 'MASTER_AGENT') {
        group = 'masterAgent';
        child = 'agent';
    } else if (userInfo.group.type === 'AGENT') {
        group = 'agent';
        child = 'member';
    }

    condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(userInfo.group._id);

    console.log(condition);

    let page = req.query.page ? Number.parseInt(req.query.page) : 10;
    let limit = req.query.limit ? Number.parseInt(req.query.limit) : 1;

    async.series([(callback => {
        BetTransactionModel.aggregate([
            {
                $match: condition
            },
            {
                $project: {
                    _id: 0,
                    memberId: '$memberId',
                    lotto: '$lotto',
                    commission: '$commission.' + group,
                    allCommission: '$commission',
                    child: '$commission.' + child,
                    amount: '$amount',
                    usageCredit: {$abs: '$memberCredit'},
                    payout: '$payout',
                    status: '$status',
                    receive: {$multiply: ['$amount', {$divide: ['$commission.' + group + '.shareReceive', 100]}]},
                    stake_in: '$amount',
                    stake_out: {
                        $cond: [{$eq: ['$gameType', 'PARLAY']}, {$multiply: ['$amount', '$parlay.odds']}, {$multiply: ['$amount', '$hdp.odd']}]
                    },
                    betId: '$betId',
                    createdDate: '$createdDate',
                    priceType: '$priceType',
                    gameType: '$gameType',
                    ipAddress: '$ipAddress',
                    isPrinted: '$isPrinted',
                    cancelByType: '$cancelByType',
                    cancelByAgent: '$cancelByAgent',
                    cancelDate: '$cancelDate'
                }
            },
            {$sort: {'createdDate': 1, 'status': -1}},
            {
                $skip: (page - 1) * limit
            }, {
                $limit: limit
            }
        ], (err, results) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, results);
            }
        });

    }), (callback) => {

        BetTransactionModel.count(condition, function (err, data) {
            if (err) {
                callback(err, null);
                return;
            }
            callback(null, data);

        }).lean();
    }], (err, asyncResponse) => {

        if (err)
            return res.send({message: "error", results: err, code: 999});

        let results = asyncResponse[0];
        let count = asyncResponse[1];


        async.series([(callback => {

            MemberModel.populate(results, {
                path: 'memberId',
                select: '_id username'
            }, function (err, memberResult) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, memberResult);
                }
            });


        }), (callback => {

            AgentGroupModel.populate(results, {
                path: 'commission.group',
                select: '_id name'
            }, function (err, memberResult) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, memberResult);
                }
            });

        }), (callback => {

            AgentGroupModel.populate(results, {
                path: 'child.group',
                select: '_id name'
            }, function (err, memberResult) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, memberResult);
                }
            });

        }), (callback => {

            UserModel.populate(results, {
                path: 'cancelByAgent',
                select: '_id username group',
                populate: {
                    path: 'group',
                    select: 'type name_lower'
                }
            }, function (err, memberResult) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, memberResult);
                }
            });

        })], (err, populateResults) => {

            if (err)
                return res.send({message: "error", results: err, code: 999});


            let finalResult = transformData(userInfo.group.type, results);


            function transformData(type, dataList) {
                if (type === 'SHARE_HOLDER') {
                    return _.map(dataList, function (item) {
                        let newItem = item;

                        newItem.agentWinLose = item.allCommission.agent.amount;

                        newItem.allCommission.company.amount = item.allCommission.superAdmin.amount + item.allCommission.company.amount;
                        newItem.allCommission.company.shareReceive = roundTo(item.allCommission.superAdmin.shareReceive + item.allCommission.company.shareReceive, 2);
                        return newItem;
                    });
                } else if (type === 'SENIOR') {
                    return _.map(dataList, function (item) {
                        let newItem = item;
                        newItem.allCommission.company.amount = item.allCommission.superAdmin.amount + item.allCommission.company.amount + item.allCommission.shareHolder.amount;
                        newItem.allCommission.company.shareReceive = roundTo(item.allCommission.superAdmin.shareReceive + item.allCommission.company.shareReceive + item.allCommission.shareHolder.shareReceive, 2);
                        return newItem;
                    });
                } else if (type === 'MASTER_AGENT') {
                    return _.map(dataList, function (item) {
                        let newItem = item;

                        newItem.allCommission.company.amount = item.allCommission.superAdmin.amount + item.allCommission.company.amount + item.allCommission.shareHolder.amount
                            + item.allCommission.senior.amount;
                        newItem.allCommission.company.shareReceive = roundTo(item.allCommission.superAdmin.shareReceive + item.allCommission.company.shareReceive + item.allCommission.shareHolder.shareReceive
                            + item.allCommission.senior.shareReceive, 2);
                        return newItem;
                    });
                } else if (type === 'AGENT') {
                    return _.map(dataList, function (item) {
                        let newItem = item;
                        newItem.allCommission.company.amount = item.allCommission.superAdmin.amount + item.allCommission.company.amount + item.allCommission.shareHolder.amount
                            + item.allCommission.senior.amount + item.allCommission.masterAgent.amount;
                        newItem.allCommission.company.shareReceive = roundTo(item.allCommission.superAdmin.shareReceive + item.allCommission.company.shareReceive + item.allCommission.shareHolder.shareReceive
                            + item.allCommission.senior.shareReceive + item.allCommission.masterAgent.shareReceive, 2);
                        return newItem;
                    });
                } else {
                    return dataList;
                }
            }

            let summary = _.reduce(results, function (memo, num) {
                return {
                    totalAmount: memo.totalAmount + num.amount,
                    totalRunningAmount: memo.totalRunningAmount + (num.status === 'RUNNING' ? num.amount : 0),
                    totalRunningUsageCredit: memo.totalRunningUsageCredit + (num.status === 'RUNNING' ? num.usageCredit : 0),
                    totalReceive: memo.totalReceive + num.receive,
                    totalAgentAmount: memo.totalAgentAmount + (num.allCommission.agent.amount || 0),
                    totalMasterAgentAmount: memo.totalMasterAgentAmount + (num.allCommission.masterAgent.amount || 0),
                    totalSeniorAmount: memo.totalSeniorAmount + (num.allCommission.senior.amount || 0),
                    totalShareHolderAmount: memo.totalShareHolderAmount + (num.allCommission.shareHolder.amount || 0),
                    totalCompanyAmount: memo.totalCompanyAmount + (num.allCommission.company.amount || 0),
                    totalSuperAdminAmount: memo.totalSuperAdminAmount + (num.allCommission.superAdmin.amount || 0),
                }
            }, {
                totalAmount: 0,
                totalReceive: 0,
                totalRunningAmount: 0,
                totalRunningUsageCredit: 0,
                totalAgentAmount: 0,
                totalMasterAgentAmount: 0,
                totalSeniorAmount: 0,
                totalShareHolderAmount: 0,
                totalCompanyAmount: 0,
                totalSuperAdminAmount: 0
            });


            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: {
                        betList: results,
                        betList2: finalResult,
                        total: count,
                        limit: limit,
                        page: page,
                        pages: Math.ceil(count / limit) || 1,
                        summary: summary,
                    }
                }
            );
        });
    });
});


router.get('/bet-list', function (req, res) {


    let condition = {game: 'LOTTO',source: 'AMB_LOTTO'};

    let userInfo = req.userInfo;


    if (req.query.memberId) {
        condition['memberId'] = mongoose.Types.ObjectId(req.query.memberId);
    }

    // if(req.query.username){
    //     condition['memberId'] = mongoose.Types.ObjectId(req.query.memberId);
    // }

    if (req.query.ref) {
        condition['lotto.amb.betId'] = req.query.ref;
    }


    let orCondition = [];

    orCondition.push({
        $or: 'RUNNING,CANCELLED'.split(',').map(function (status) {
            return {'status': status}
        })
    });


    if (req.query.betType && req.query.betType !== '_ALL') {
        condition['lotto.amb.betType'] = req.query.betType;
    }

    if (req.query.betNumber) {
        condition['lotto.amb.betNumber'] = Number.parseInt(req.query.betNumber);
    }

    if (orCondition.length > 0) {
        condition['$and'] = orCondition;
    }

    let group = '';
    let child = '';
    if (userInfo.group.type === 'SUPER_ADMIN') {
        group = 'superAdmin';
        child = 'company';
    } else if (userInfo.group.type === 'COMPANY') {
        group = 'company';
        child = 'shareHolder';
    } else if (userInfo.group.type === 'SHARE_HOLDER') {
        group = 'shareHolder';
        child = 'superSenior';
    } else if (userInfo.group.type === 'SUPER_SENIOR') {
        group = 'superSenior';
        child = 'senior';
    } else if (userInfo.group.type === 'SENIOR') {
        group = 'senior';
        child = 'masterAgent';
    } else if (userInfo.group.type === 'MASTER_AGENT') {
        group = 'masterAgent';
        child = 'agent';
    } else if (userInfo.group.type === 'AGENT') {
        group = 'agent';
        child = 'member';
    }

    condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(userInfo.group._id);

    let page = req.query.page ? Number.parseInt(req.query.page) : 10;
    let limit = req.query.limit ? Number.parseInt(req.query.limit) : 1;

    async.parallel([(callback => {

        BetTransactionModel.aggregate([
            {
                $match: condition
            },
            {
                $group: {
                    _id: {
                        refId: "$lotto.amb.betId",
                        gameId: "$lotto.amb.gameId",
                    },
                    amount: {$sum: '$amount'},
                    receive: {$sum: '$commission.' + group + '.amount'},
                    betCount: {$sum: {$cond: [{$eq: ['$status', 'RUNNING']}, 1, 0]}},
                    voidCount: {$sum: {$cond: [{$eq: ['$status', 'CANCELLED']}, 1, 0]}},
                    memberId: {$first: '$memberId'},
                    betTime: {$first: '$createdDate'},
                    ipAddress: {$first: '$ipAddress'}
                }
            },
            {
                $match: {'betCount': {$ne: 0}}
            },
            {$sort: {betTime: -1}},
            {
                $project: {
                    _id: 0,
                    memberId: '$memberId',
                    refId: '$_id.refId',
                    gameId: '$_id.gameId',
                    amount: '$amount',
                    receive: '$receive',
                    betTime: '$betTime',
                    betCount: '$betCount',
                    voidCount: '$voidCount',
                    ipAddress: '$ipAddress'
                }
            },
            {
                $skip: (page - 1) * limit
            }, {
                $limit: limit
            },
        ], (err, results) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, results);
            }
        });

    }), (callback) => {

        BetTransactionModel.aggregate([
            {
                $match: condition
            },
            {
                $group: {
                    _id: {
                        refId: "$lotto.amb.betId",
                        gameId: "$lotto.amb.gameId",
                    },
                    amount: {$sum: '$amount'},
                    receive: {$sum: '$commission.' + group + '.amount'},
                    betCount: {$sum: {$cond: [{$eq: ['$status', 'RUNNING']}, 1, 0]}},
                    voidCount: {$sum: {$cond: [{$eq: ['$status', 'CANCELLED']}, 1, 0]}},
                    memberId: {$first: '$memberId'},
                    betTime: {$first: '$lotto.amb.betTime'},
                    ipAddress: {$first: '$ipAddress'}
                }
            },
            {
                $match: {'betCount': {$ne: 0}}
            }
        ], (err, results) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, results.length);
            }
        });

        // BetTransactionModel.find(condition).distinct('lotto.amb.betId').exec(function (err, count) {
        //     if (err) {
        //         callback(err, null);
        //     }else {
        //         callback(null, count.length);
        //     }
        // })
    }], (err, asyncResponse) => {

        if (err)
            return res.send({message: "error", results: err, code: 999});

        let results = asyncResponse[0];
        let count = asyncResponse[1];


        async.series([(callback => {

            MemberModel.populate(results, {
                path: 'memberId',
                select: '_id username'
            }, function (err, memberResult) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, memberResult);
                }
            });


        }), (callback => {

            AgentGroupModel.populate(results, {
                path: 'child.group',
                select: '_id name'
            }, function (err, memberResult) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, memberResult);
                }
            });

        })], (err, populateResults) => {

            if (err)
                return res.send({message: "error", results: err, code: 999});


            prepareCancelTimeOut(results);

            let summary = _.reduce(results, function (memo, num) {
                return {
                    totalAmount: memo.totalAmount + num.amount,
                    totalReceive: memo.totalReceive + num.receive
                }
            }, {
                totalAmount: 0,
                totalReceive: 0
            });


            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: {
                        betList: results,
                        total: count,
                        limit: limit,
                        page: page,
                        pages: Math.ceil(count / limit) || 1,
                        summary: summary,
                    }
                }
            );
        });
    });
});

function prepareCancelTimeOut(list) {

    function diff_seconds(dt2, dt1) {

        let diff = (dt2.getTime() - dt1.getTime()) / 1000;
        return Math.abs(Math.round(diff));

    }

    return _.map(list, (item) => {
        let xx = item;
        let diffSeconds = diff_seconds(item.betTime, DateUtils.getCurrentDate());
        xx.cancelTimeout = diffSeconds < 1800 ? (1800 - diffSeconds) : 0;
        return xx;
    });
}


router.get('/void-list', function (req, res) {


    let condition = {game: 'LOTTO',source: 'AMB_LOTTO'};

    let userInfo = req.userInfo;


    if (req.query.memberId) {
        condition['memberId'] = mongoose.Types.ObjectId(req.query.memberId);
    }

    if (req.query.ref) {
        condition['lotto.amb.betId'] = req.query.ref;
    }

    let orCondition = [];

    orCondition.push({
        $or: 'RUNNING,CANCELLED'.split(',').map(function (status) {
            return {'status': status}
        })
    });


    if (req.query.betType && req.query.betType !== '_ALL') {
        condition['lotto.amb.betType'] = req.query.betType;
    }

    if (req.query.betNumber) {
        condition['lotto.amb.betNumber'] = Number.parseInt(req.query.betNumber);
    }

    if (orCondition.length > 0) {
        condition['$and'] = orCondition;
    }

    let group = '';
    let child = '';
    if (userInfo.group.type === 'SUPER_ADMIN') {
        group = 'superAdmin';
        child = 'company';
    } else if (userInfo.group.type === 'COMPANY') {
        group = 'company';
        child = 'shareHolder';
    } else if (userInfo.group.type === 'SHARE_HOLDER') {
        group = 'shareHolder';
        child = 'superSenior';
    } else if (userInfo.group.type === 'SUPER_SENIOR') {
        group = 'superSenior';
        child = 'senior';
    } else if (userInfo.group.type === 'SENIOR') {
        group = 'senior';
        child = 'masterAgent';
    } else if (userInfo.group.type === 'MASTER_AGENT') {
        group = 'masterAgent';
        child = 'agent';
    } else if (userInfo.group.type === 'AGENT') {
        group = 'agent';
        child = 'member';
    }

    condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(userInfo.group._id);

    console.log(condition);

    let page = req.query.page ? Number.parseInt(req.query.page) : 10;
    let limit = req.query.limit ? Number.parseInt(req.query.limit) : 1;

    async.waterfall([callback => {

        LottoService.getGameList((err, gameList) => {
            callback(null, gameList[0].id);
        });

    }, (gameId, callback) => {

        condition['lotto.amb.gameId'] = gameId;

        BetTransactionModel.aggregate([
            {
                $match: condition
            },
            {
                $group: {
                    _id: {
                        refId: "$lotto.amb.betId",
                        gameId: "$lotto.amb.gameId",
                    },
                    amount: {$sum: '$amount'},
                    receive: {$sum: '$commission.' + group + '.amount'},
                    betCount: {$sum: {$cond: [{$eq: ['$status', 'RUNNING']}, 1, 0]}},
                    voidCount: {$sum: {$cond: [{$eq: ['$status', 'CANCELLED']}, 1, 0]}},
                    memberId: {$first: '$memberId'},
                    betTime: {$first: '$createdDate'},
                    ipAddress: {$first: '$ipAddress'}
                }
            },
            {
                $match: {'betCount': {$eq: 0}}
            },
            {$sort: {betTime: -1}},
            {
                $project: {
                    _id: 0,
                    memberId: '$memberId',
                    refId: '$_id.refId',
                    gameId: '$_id.gameId',
                    amount: '$amount',
                    receive: '$receive',
                    betTime: '$betTime',
                    betCount: '$betCount',
                    voidCount: '$voidCount',
                    ipAddress: '$ipAddress'
                }
            },
            {
                $skip: (page - 1) * limit
            }, {
                $limit: limit
            },
        ], (err, results) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, gameId, results);
            }
        });

    }, (gameId, transaction, callback) => {

        condition['lotto.amb.gameId'] = gameId;

        BetTransactionModel.aggregate([
            {
                $match: condition
            },
            {
                $group: {
                    _id: {
                        refId: "$lotto.amb.betId",
                        gameId: "$lotto.amb.gameId",
                    },
                    amount: {$sum: '$amount'},
                    receive: {$sum: '$commission.' + group + '.amount'},
                    betCount: {$sum: {$cond: [{$eq: ['$status', 'RUNNING']}, 1, 0]}},
                    voidCount: {$sum: {$cond: [{$eq: ['$status', 'CANCELLED']}, 1, 0]}},
                    memberId: {$first: '$memberId'},
                    betTime: {$first: '$lotto.amb.betTime'},
                    ipAddress: {$first: '$ipAddress'}
                }
            },
            {
                $match: {'betCount': {$eq: 0}}
            },
        ], (err, results) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, gameId, transaction, results.length);
            }
        })
    }], (err, gameId, transaction, count) => {

        if (err)
            return res.send({message: "error", results: err, code: 999});

        let results = transaction;


        async.series([(callback => {

            MemberModel.populate(results, {
                path: 'memberId',
                select: '_id username'
            }, function (err, memberResult) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, memberResult);
                }
            });


        }), (callback => {

            AgentGroupModel.populate(results, {
                path: 'child.group',
                select: '_id name'
            }, function (err, memberResult) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, memberResult);
                }
            });

        })], (err, populateResults) => {

            if (err)
                return res.send({message: "error", results: err, code: 999});


            let summary = _.reduce(results, function (memo, num) {
                return {
                    totalAmount: memo.totalAmount + num.amount,
                    totalReceive: memo.totalReceive + num.receive
                }
            }, {
                totalAmount: 0,
                totalReceive: 0
            });


            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: {
                        betList: results,
                        total: count,
                        limit: limit,
                        page: page,
                        pages: Math.ceil(count / limit) || 1,
                        summary: summary,
                    }
                }
            );
        });
    });
});

router.get('/summary', function (req, res) {

    let condition = {game: 'LOTTO',source: 'AMB_LOTTO'};

    if (req.query.gameId) {
        condition['lotto.amb.gameId'] = req.query.gameId;
    }

    let userInfo = req.userInfo;

    // if (req.query.dateFrom && req.query.dateTo) {
    //     condition['createdDate'] = {
    //         "$gte": new Date(moment(req.query.dateFrom, "DD/MM/YYYY").format('YYYY-MM-DDT00:00:00.000')),
    //         "$lt": new Date(moment(req.query.dateTo, "DD/MM/YYYY").add(1, 'd').format('YYYY-MM-DDT00:00:00.000'))
    //     }
    // }


    let orCondition = [];

    if (req.query.status) {
        orCondition.push({
            $or: req.query.status.split(',').map(function (status) {
                return {'status': status}
            })
        });
    }

    if (req.query.betType && req.query.betType !== '_ALL') {
        condition['lotto.amb.betType'] = req.query.betType;
    }

    if (orCondition.length > 0) {
        condition['$and'] = orCondition;
    }

    let group = '';
    let child = '';
    if (userInfo.group.type === 'SUPER_ADMIN') {
        group = 'superAdmin';
        child = 'company';
    } else if (userInfo.group.type === 'COMPANY') {
        group = 'company';
        child = 'shareHolder';
    } else if (userInfo.group.type === 'SHARE_HOLDER') {
        group = 'shareHolder';
        child = 'superSenior';
    } else if (userInfo.group.type === 'SUPER_SENIOR') {
        group = 'superSenior';
        child = 'senior';
    } else if (userInfo.group.type === 'SENIOR') {
        group = 'senior';
        child = 'masterAgent';
    } else if (userInfo.group.type === 'MASTER_AGENT') {
        group = 'masterAgent';
        child = 'agent';
    } else if (userInfo.group.type === 'AGENT') {
        group = 'agent';
        child = 'member';
    }

    condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(userInfo.group._id);

    console.log(condition);
    BetTransactionModel.aggregate([
        {
            $match: condition
        },
        {
            $group: {
                _id: {
                    betType: "$lotto.amb.betType",
                    betNumber: "$lotto.amb.betNumber"
                },
                amount: {$sum: '$commission.' + group + '.amount'},
                count: {$sum: 1},
            }
        },
        {
            $project: {
                _id: 0,
                betType: "$_id.betType",
                betNumber: "$_id.betNumber",
                amount: '$amount',
                stakeCount: '$count'
            }
        },
    ], (err, results) => {
        if (err)
            return res.send({message: "error", results: err, code: 999});


        results = _.chain(results).groupBy(key => {
            if (key.betType == '_2TOD' || key.betType == '_3TOD' || key.betType == '_4TOD') {
                return key.betType + '|' + LottoService.sortTodNumber(key.betNumber);
            } else {
                return key.betType + '|' + key.betNumber;
            }
        }).map((value, key) => {
            return {
                betType: key.split('|')[0],
                betNumber: key.split('|')[1],
                amount: _.reduce(value, function (memo, obj) {
                    return memo + obj.amount;
                }, 0),
                stackCount: _.reduce(value, function (memo, obj) {
                    return memo + obj.stakeCount;
                }, 0)
            }
        }).value();

        results = _.sortBy(results, (o) => {
            return o.amount * -1;
        });

        let summary = _.reduce(results, function (memo, num) {
            return {
                totalAmount: memo.totalAmount + num.amount,
                totalStakeCount: memo.totalStakeCount + num.stakeCount
            }
        }, {totalAmount: 0, totalStakeCount: 0});

        return res.send(
            {
                code: 0,
                message: "success",
                result: {
                    betList: results,
                    summary: summary,
                }
            }
        );
    });

});

router.get('/bet-summary', function (req, res) {

    let condition = {game: 'LOTTO',source: 'AMB_LOTTO'};

    let userInfo = req.userInfo;

    if (req.query.dateFrom && req.query.dateTo) {
        condition['createdDate'] = {
            "$gte": moment(req.query.dateFrom, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).utc(true).toDate(),
            "$lt": moment(req.query.dateTo, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).add(1,'d').utc(true).toDate()
        }
    }

    let orCondition = [];

    if (req.query.status) {
        orCondition.push({
            $or: req.query.status.split(',').map(function (status) {
                return {'status': status}
            })
        });
    }
    if (req.query.gameId) {
        condition['lotto.amb.gameId'] = req.query.gameId;
    }

    orCondition.push({
        $or: '_3TOP,_3BOT,_3TOD,_2TOP,_2BOT,_2TOD,_1TOP,_1BOT'.split(',').map(function (status) {
            return {'lotto.amb.betType': status}
        })
    });


    if (orCondition.length > 0) {
        condition['$and'] = orCondition;
    }

    let group = '';
    let child = '';
    if (userInfo.group.type === 'SUPER_ADMIN') {
        group = 'superAdmin';
        child = 'company';
    } else if (userInfo.group.type === 'COMPANY') {
        group = 'company';
        child = 'shareHolder';
    } else if (userInfo.group.type === 'SHARE_HOLDER') {
        group = 'shareHolder';
        child = 'superSenior';
    } else if (userInfo.group.type === 'SUPER_SENIOR') {
        group = 'superSenior';
        child = 'senior';
    } else if (userInfo.group.type === 'SENIOR') {
        group = 'senior';
        child = 'masterAgent';
    } else if (userInfo.group.type === 'MASTER_AGENT') {
        group = 'masterAgent';
        child = 'agent';
    } else if (userInfo.group.type === 'AGENT') {
        group = 'agent';
        child = 'member';
    }

    condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(userInfo.group._id);

    console.log(condition);
    BetTransactionModel.aggregate([
        {
            $match: condition
        },
        {
            $group: {
                _id: {
                    betType: "$lotto.amb.betType",
                    betNumber: "$lotto.amb.betNumber"
                },
                amount: {$sum: '$commission.' + group + '.amount'},
                fullAmount: {$sum: '$amount'}
            }
        },
        {
            $project: {
                _id: 0,
                betType: "$_id.betType",
                betNumber: "$_id.betNumber",
                amount: '$amount',
                fullAmount: '$fullAmount'
            }
        },
    ], (err, results) => {
        if (err)
            return res.send({message: "error", results: err, code: 999});


        results = _.chain(results).groupBy(key => {
            if (key.betType == '_2TOD' || key.betType == '_3TOD' || key.betType == '_4TOD') {
                return key.betType + '|' + LottoService.sortTodNumber(key.betNumber);
            } else {
                return key.betType + '|' + key.betNumber;
            }
        }).map((value, key) => {
            return {
                betType: key.split('|')[0],
                betNumber: key.split('|')[1],
                amount: _.reduce(value, function (memo, obj) {
                    return memo + obj.amount;
                }, 0),
                fullAmount: _.reduce(value, function (memo, obj) {
                    return memo + obj.fullAmount;
                }, 0),

            }
        }).groupBy(key => {
            return key.betType;
        }).map((value, key) => {
            let values = _.sortBy(value, (o) => {
                return o.amount * -1;
            }).splice(0, 25);

            let count = values.length;
            for (let i = count; i < 25; i++) {
                values.push([{betNumber: ''}]);
            }

            return {
                betType: key,
                value: values
            };

        }).value();


        // console.log(results)

        let prepareResult = [];

        _.forEach('_3TOP,_3BOT,_3TOD,_2TOP,_2BOT,_2TOD,_1TOP,_1BOT'.split(','), itemBetType => {

            let filItem = _.filter(results, (filterItem) => {
                return filterItem.betType === itemBetType;
            })[0];

            if (!filItem) {
                let values = [];
                let count = values.length;
                for (let i = count; i < 25; i++) {
                    values.push([{betNumber: ''}]);
                }
                prepareResult.push({
                    betType: itemBetType,
                    value: values
                });

            } else {
                prepareResult.push(filItem);
            }
        });


        return res.send(
            {
                code: 0,
                message: "success",
                result: {
                    betItem: _.object(_.map(prepareResult, _.values))
                }
            }
        );
    });

});

router.post('/export-betout', function (req, res) {

    let condition = {game: 'LOTTO', source: 'AMB_LOTTO',status: 'RUNNING'};

    condition['lotto.amb.gameId'] = req.body.gameId;

    let userInfo = req.userInfo;

    const requestBody = req.body;

    if (req.query.dateFrom && req.query.dateTo) {
        condition['createdDate'] = {
            "$gte": moment(req.query.dateFrom, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).utc(true).toDate(),
            "$lt": moment(req.query.dateTo, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).add(1,'d').utc(true).toDate()
        }
    }

    let orCondition = [];


    let filterBetType = _.filter('_3TOP,_3BOT,_3TOD,_2TOP,_2BOT,_2TOD,_1TOP,_1BOT'.split(','), type => {
        if ('_3TOP' === type) {
            return requestBody._3TOP != null;
        } else if ('_3BOT' === type) {
            return requestBody._3BOT != null;
        } else if ('_3TOD' === type) {
            return requestBody._3TOD != null;
        } else if ('_2TOP' === type) {
            return requestBody._2TOP != null;
        } else if ('_2BOT' === type) {
            return requestBody._2BOT != null;
        } else if ('_2TOD' === type) {
            return requestBody._2TOD != null;
        } else if ('_1TOP' === type) {
            return requestBody._1TOP != null;
        } else if ('_1BOT' === type) {
            return requestBody._1BOT != null;
        }
    });

    orCondition.push({
        $or: filterBetType.map(function (status) {
            return {'lotto.amb.betType': status}
        })
    });


    if (orCondition.length > 0) {
        condition['$and'] = orCondition;
    }

    let group = '';
    let child = '';
    if (userInfo.group.type === 'SUPER_ADMIN') {
        group = 'superAdmin';
        child = 'company';
    } else if (userInfo.group.type === 'COMPANY') {
        group = 'company';
        child = 'shareHolder';
    } else if (userInfo.group.type === 'SHARE_HOLDER') {
        group = 'shareHolder';
        child = 'superSenior';
    } else if (userInfo.group.type === 'SUPER_SENIOR') {
        group = 'superSenior';
        child = 'senior';
    } else if (userInfo.group.type === 'SENIOR') {
        group = 'senior';
        child = 'masterAgent';
    } else if (userInfo.group.type === 'MASTER_AGENT') {
        group = 'masterAgent';
        child = 'agent';
    } else if (userInfo.group.type === 'AGENT') {
        group = 'agent';
        child = 'member';
    }

    condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(userInfo.group._id);

    BetTransactionModel.aggregate([
        {
            $match: condition
        },
        {
            $group: {
                _id: {
                    betType: "$lotto.amb.betType",
                    betNumber: "$lotto.amb.betNumber"
                },
                amount: {$sum: '$commission.' + group + '.amount'},
            }
        },
        {$sort: {amount: -1}},
        {
            $project: {
                _id: 0,
                betType: "$_id.betType",
                betNumber: "$_id.betNumber",
                amount: '$amount'
            }
        },
    ], (err, results) => {
        if (err)
            return res.send({message: "error", results: err, code: 999});

        results = _.chain(results).groupBy(key => {
            return key.betType;
        }).map((value, key) => {

            let values = value.splice(0, 25);

            let filterBetOut = _.filter(values, item => {
                // _3TOP,_3BOT,_3TOD,_2TOP,_2BOT,_2TOD,_1TOP,_1BOT
                if ('_3TOP' === key) {
                    return item.amount > requestBody._3TOP;
                } else if ('_3BOT' === key) {
                    return item.amount > requestBody._3BOT;
                } else if ('_3TOD' === key) {
                    return item.amount > requestBody._3TOD;
                } else if ('_2TOP' === key) {
                    return item.amount > requestBody._2TOP;
                } else if ('_2BOT' === key) {
                    return item.amount > requestBody._2BOT;
                } else if ('_2TOD' === key) {
                    return item.amount > requestBody._2TOD;
                } else if ('_1TOP' === key) {
                    return item.amount > requestBody._1TOP;
                } else if ('_1BOT' === key) {
                    return item.amount > requestBody._1BOT;
                }
            });

            filterBetOut = _.map(filterBetOut, item => {
                let newItem = item;
                if ('_3TOP' === key) {
                    newItem.amount -= requestBody._3TOP;
                } else if ('_3BOT' === key) {
                    newItem.amount -= requestBody._3BOT;
                } else if ('_3TOD' === key) {
                    newItem.amount -= requestBody._3TOD;
                } else if ('_2TOP' === key) {
                    newItem.amount -= requestBody._2TOP;
                } else if ('_2BOT' === key) {
                    newItem.amount -= requestBody._2BOT;
                } else if ('_2TOD' === key) {
                    newItem.amount -= requestBody._2TOD;
                } else if ('_1TOP' === key) {
                    newItem.amount -= requestBody._1TOP;
                } else if ('_1BOT' === key) {
                    newItem.amount -= requestBody._1BOT;
                }
                return newItem
            });

            let count = filterBetOut.length;

            for (let i = count; i < 25; i++) {
                filterBetOut.push(null);
            }


            return {
                betType: key,
                value: filterBetOut
            };
        }).value();

        let prepareResult = [];
        _.forEach('_3TOP,_3BOT,_3TOD,_2TOP,_2BOT,_2TOD,_1TOP,_1BOT'.split(','), itemBetType => {

            let filItem = _.filter(results, (filterItem) => {

                return filterItem.betType === itemBetType;
            })[0];

            if (!filItem) {
                let values = [];
                for (let i = 0; i < 25; i++) {
                    values.push(null);
                }
                prepareResult.push({
                    betType: itemBetType,
                    value: values
                });

            } else {
                prepareResult.push(filItem);
            }
        });


        let conf = {};
        conf.cols = [{
            caption: '3 Top',
            type: 'string'
        }, {
            caption: 'Total',
            type: 'string'
        }, {
            caption: '3 Bottom',
            type: 'string'
        }, {
            caption: 'Total',
            type: 'string'
        }, {
            caption: '3 Roll',
            type: 'string'
        }, {
            caption: 'Total',
            type: 'string'
        }, {
            caption: '2 Top',
            type: 'string'
        }, {
            caption: 'Total',
            type: 'string'
        }, {
            caption: '2 Bottom',
            type: 'string'
        }, {
            caption: 'Total',
            type: 'string'
        }, {
            caption: '2 Roll',
            type: 'string'
        }, {
            caption: 'Total',
            type: 'string'
        }, {
            caption: '1 Top',
            type: 'string'
        }, {
            caption: 'Total',
            type: 'string'
        }, {
            caption: '1 Bottom',
            type: 'string'
        }, {
            caption: 'Total',
            type: 'string'
        }];


        let betOutObj = _.object(_.map(prepareResult, _.values));

        conf.rows = [];
        for (let i = 0; i < 25; i++) {
            conf.rows.push([
                getBetNumber(betOutObj._3TOP[i]), getAmount(betOutObj._3TOP[i]),
                getBetNumber(betOutObj._3BOT[i]), getAmount(betOutObj._3BOT[i]),
                getBetNumber(betOutObj._3TOD[i]), getAmount(betOutObj._3TOD[i]),
                getBetNumber(betOutObj._2TOP[i]), getAmount(betOutObj._2TOP[i]),
                getBetNumber(betOutObj._2BOT[i]), getAmount(betOutObj._2BOT[i]),
                getBetNumber(betOutObj._2TOD[i]), getAmount(betOutObj._2TOD[i]),
                getBetNumber(betOutObj._1TOP[i]), getAmount(betOutObj._1TOP[i]),
                getBetNumber(betOutObj._1BOT[i]), getAmount(betOutObj._1BOT[i])]);
        }

        function getBetNumber(obj) {
            return obj ? obj.betNumber.toString() : '';
        }

        function getAmount(obj) {
            return obj ? obj.amount.toString() : '';
        }

        let result = nodeExcel.execute(conf);
        let fileName = 'fileName';
        res.setHeader('Content-Type', 'application/vnd.ms-excel');
        res.setHeader("Content-Disposition", `attachment; filename=Product_2323.xlsx`);
        res.end(result, 'binary');
    });

});


router.post('/export-bet-list1', function (req, res) {


    let userInfo = req.userInfo;


    // if (req.query.gameId) {
    //     condition['lotto.amb.gameId'] = req.query.gameId;
    // }


    let group = '';
    let child = '';
    if (userInfo.group.type === 'SUPER_ADMIN') {
        group = 'superAdmin';
        child = 'company';
    } else if (userInfo.group.type === 'COMPANY') {
        group = 'company';
        child = 'shareHolder';
    } else if (userInfo.group.type === 'SHARE_HOLDER') {
        group = 'shareHolder';
        child = 'superSenior';
    } else if (userInfo.group.type === 'SUPER_SENIOR') {
        group = 'superSenior';
        child = 'senior';
    } else if (userInfo.group.type === 'SENIOR') {
        group = 'senior';
        child = 'masterAgent';
    } else if (userInfo.group.type === 'MASTER_AGENT') {
        group = 'masterAgent';
        child = 'agent';
    } else if (userInfo.group.type === 'AGENT') {
        group = 'agent';
        child = 'member';
    }

    let condition = {
        gameDate: {$gte: new Date(moment().add(-20, 'd').format('YYYY-MM-DDT11:00:00.000'))},
        game: 'LOTTO',
        source: 'AMB_LOTTO'
    };
    condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(userInfo.group._id);

    async.waterfall([(callback => {

        LottoService.getGameList((err, gameList) => {
            callback(null, gameList[0]);
        });

    }), (game, callback) => {

        condition['lotto.amb.gameId'] = game.id;

        let aggregation = BetTransactionModel.aggregate([
            {
                $match: condition
            },
            {
                $project: {
                    _id: 0,
                    memberId: '$memberId',
                    lotto: '$lotto',
                    child: '$commission.' + child,
                    amount: '$amount',
                    payout: '$payout',
                    status: '$status',
                    share: '$commission.' + group + '.shareReceive',
                    receive: '$commission.' + group + '.amount',
                    stake_in: '$amount',
                    betId: '$betId',
                    createdDate: '$createdDate',
                    priceType: '$priceType',
                    gameType: '$gameType',
                    ipAddress: '$ipAddress',
                    isPrinted: '$isPrinted',
                    cancelByType: '$cancelByType',
                    cancelByAgent: '$cancelByAgent',
                    cancelDate: '$cancelDate'
                }
            },
            {$sort: {'lotto.amb.betId': -1}}
        ]);

        aggregation.options = {allowDiskUse: true};
        aggregation.exec(function (err, results) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, game, results);
            }
        });

    }], (err, game, dataList) => {

        if (err)
            return res.send({message: "error", results: err, code: 999});

        let results = dataList;

        console.log(results)


        async.series([(callback => {

            MemberModel.populate(results, {
                path: 'memberId',
                select: '_id username'
            }, function (err, memberResult) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, memberResult);
                }
            });


        })], (err, populateResults) => {

            if (err)
                return res.send({message: "error", results: err, code: 999});

            // results = _.sortBy(results, function (o) {
            //     return o.lotto.amb.betId * -1   ;
            // });
            results = _.sortBy(results, function (o) {
                return o.status === "RUNNING" ? -1 : 1;
            });

            let info = _.reduce(results, function (memo, num) {
                return {
                    totalAmount: memo.totalAmount + (num.status == "RUNNING" ? num.amount : 0 ),
                    totalReceive: memo.totalReceive + (num.status == "RUNNING" ? num.receive : 0),
                    successCount: memo.successCount + (num.status == "RUNNING" ? 1 : 0),
                }
            }, {
                totalAmount: 0,
                totalReceive: 0,
                successCount: 0
            });

            info.game = game;
            info.name = userInfo.group.name;

            generateExcel(res, info, results)

        });
    });


    function generateExcel(res, info, betList) {

        var options = {
            filename: './streamed-workbook.xlsx',
            useStyles: true,
            useSharedStrings: true
        };
        // var workbook = new Excel.stream.xlsx.WorkbookWriter(options);
        var workbook = new Excel.Workbook();
        workbook.creator = 'System';
        // workbook.lastModifiedBy = 'Her';
        workbook.created = DateUtils.getCurrentDate();
        workbook.modified = DateUtils.getCurrentDate();
        // workbook.lastPrinted = new Date(2016, 9, 27);

        var sheet = workbook.addWorksheet('My Sheet1', {properties: {tabColor: {argb: 'FFC0000'}}});


        var worksheet = workbook.getWorksheet(1);
        // worksheet.state = 'show';


        // worksheet.properties.outlineLevelCol = 2;
        worksheet.properties.defaultRowHeight = 20;

        var headerStyle = {

            font: {
                name: 'Arial Black',
                color: {argb: '00000000'},
                family: 2,
                size: 12,
                italic: true
            },
            alignment: {
                vertical: 'middle',
                horizontal: 'center'
            },
            fill: {
                type: 'pattern',
                pattern: 'solid',
                fgColor: {argb: 'eeeeee'}
            },
            border: {
                top: {style: 'thin'},
                left: {style: 'thin'},
                bottom: {style: 'thin'},
                right: {style: 'thin'}
            }
        };

        worksheet.getRow(2).values = ['วันที่'];
        worksheet.getRow(3).values = ['สมาชิก'];

        worksheet.mergeCells('A2:B2');
        worksheet.mergeCells('A3:B3');

        worksheet.getCell('A2').style = headerStyle;
        worksheet.getCell('A3').style = headerStyle;
        worksheet.getCell('B2').style = headerStyle;
        worksheet.getCell('B3').style = headerStyle;

        worksheet.getCell('C2').value = moment().utc(true).format('DD-MM-YYYY HH:mm');
        worksheet.getCell('C3').value = info.name;
        worksheet.getCell('C2').alignment = {vertical: 'middle', horizontal: 'center'}
        worksheet.getCell('C3').alignment = {vertical: 'middle', horizontal: 'center'}

        worksheet.getRow(6).values = ['งวดที่', '', 'ช่องทาง', 'จำนวนครั้งที่เล่น', 'สรุปการเล่น', '', 'ผลรวมการเล่นของเรา'];
        worksheet.columns = [
            {key: 'งวดที่', width: 20},
            {},
            {key: 'ช่องทาง', width: 20},
            {key: 'จำนวนครั้งที่เล่น', width: 20},
            {key: 'สรุปการเล่น', width: 20},
            {},
            {key: 'ผลรวมการเล่นของเรา', width: 15}
        ];


        worksheet.addRow({
            งวดที่: info.game.date,
            ช่องทาง: 'เว็บ',
            จำนวนครั้งที่เล่น: info.successCount,
            สรุปการเล่น: info.totalAmount,
            ผลรวมการเล่นของเรา: info.totalReceive
        });

        worksheet.mergeCells('A6:B6');
        worksheet.mergeCells('E6:F6');
        worksheet.mergeCells('A7:B7');
        worksheet.mergeCells('E7:F7');
        worksheet.mergeCells('G6:H6');
        worksheet.mergeCells('G7:H7');

        worksheet.getCell('A6').style = headerStyle;
        worksheet.getCell('B6').style = headerStyle;
        worksheet.getCell('C6').style = headerStyle;
        worksheet.getCell('D6').style = headerStyle;
        worksheet.getCell('E6').style = headerStyle;
        worksheet.getCell('F6').style = headerStyle;
        worksheet.getCell('G6').style = headerStyle;
        worksheet.getCell('H6').style = headerStyle;

        worksheet.getCell('A7').alignment = {vertical: 'middle', horizontal: 'center'};
        worksheet.getCell('C7').alignment = {vertical: 'middle', horizontal: 'center'};
        worksheet.getCell('D7').alignment = {vertical: 'middle', horizontal: 'center'};
        worksheet.getCell('E7').alignment = {vertical: 'middle', horizontal: 'center'};
        worksheet.getCell('G7').alignment = {vertical: 'middle', horizontal: 'center'};

        worksheet.getCell('D7').numFmt = '#,##0';
        worksheet.getCell('E7').numFmt = '#,##0.00';
        worksheet.getCell('G7').numFmt = '#,##0.00';


        worksheet.getRow(9).values = ['ลำดับ', 'อ้างอิิง', 'เลขที่ตั๋ว', 'วันที่และเวลาแทง', 'ช่องทาง', 'เลขที่เล่น',
            'ประเภทการเล่น', 'จำนวนเงิน', 'สมาชิก', 'อัตราจ่าย', 'เปอร์เซ็นต์การถือหุ้น', 'ยอดจ่ายให้กับคอมพานี', 'สถานะ'];

        worksheet.getCell('A9').style = headerStyle;
        worksheet.getCell('B9').style = headerStyle;
        worksheet.getCell('C9').style = headerStyle;
        worksheet.getCell('D9').style = headerStyle;
        worksheet.getCell('E9').style = headerStyle;
        worksheet.getCell('F9').style = headerStyle;
        worksheet.getCell('G9').style = headerStyle;
        worksheet.getCell('H9').style = headerStyle;
        worksheet.getCell('I9').style = headerStyle;
        worksheet.getCell('J9').style = headerStyle;
        worksheet.getCell('K9').style = headerStyle;
        worksheet.getCell('L9').style = headerStyle;
        worksheet.getCell('M9').style = headerStyle;

        // var dataStyle = {
        //     border: {
        //         top: {style: 'thin'},
        //         left: {style: 'thin'},
        //         bottom: {style: 'thin'},
        //         right: {style: 'thin'}
        //     }
        // };

        worksheet.columns = [{key: 'ลำดับ', width: 8}, {
            key: 'อ้างอิง',
            width: 10
        }, {key: 'เลขที่ตั๋ว', width: 18}, {key: 'วันที่และเวลาแทง', width: 15}, {
            key: 'ช่องทาง',
            width: 10
        },
            {key: 'เลขที่เล่น', width: 8},
            {key: 'ประเภทการเล่น', width: 15}, {
                key: 'จำนวนเงิน',
                width: 10,
                style: {numFmt: '#,##0.00'}
            }, {key: 'สมาชิก', width: 10}, {
                key: 'อัตราจ่าย',
                width: 10,
                style: {numFmt: '#,##0.00'}
            },
            {
                key: 'เปอร์เซ็นต์การถือหุ้น',
                width: 20,
                style: {alignment: {vertical: 'middle', horizontal: 'right'}}
            }, {key: 'ยอดจ่ายให้กับคอมพานี', width: 20, style: {numFmt: '#,##0.00'}}, {
                key: 'สถานะ',
                width: 10
            }];


        betList.forEach((obj, index) => {


            worksheet.addRow({
                ลำดับ: index + 1,
                อ้างอิง: obj.lotto.amb.betId.toString(),
                เลขที่ตั๋ว: obj.betId.toString(),
                วันที่และเวลาแทง: LottoService.convertDateStr(obj.createdDate),
                ช่องทาง: 'WEB',
                เลขที่เล่น: obj.lotto.amb.betNumber.toString(),
                ประเภทการเล่น: LottoService.prepareBetType(obj.lotto.amb.betType),
                จำนวนเงิน: obj.amount,
                สมาชิก: obj.memberId.username,
                อัตราจ่าย: obj.lotto.amb.payout,
                เปอร์เซ็นต์การถือหุ้น: obj.share + "%",
                ยอดจ่ายให้กับคอมพานี: obj.receive,
                สถานะ: (obj.status === 'RUNNING' ? 'เล่นสำเร็จ' : obj.status === 'CANCELLED' ? 'ยกเลิก' : obj.status)
            });
        });


        res.setHeader('Content-Type', 'application/vnd.ms-excel');
        res.setHeader("Content-Disposition", `attachment; filename=Product_2323.xlsx`);
        workbook.xlsx.write(res).then(function () {
            res.end();
        });

        // let result = nodeExcel.execute(conf);
        // let fileName = 'fileName';
        // res.setHeader('Content-Type', 'application/vnd.ms-excel');
        // res.setHeader("Content-Disposition", `attachment; filename=Product_2323.xlsx`);
        // res.end(result, 'binary');


    }


});


router.get('/betInfo', function (req, res) {

    let condition = {game: 'LOTTO',source: 'AMB_LOTTO'};

    let userInfo = req.userInfo;

    if (req.query.dateFrom && req.query.dateTo) {
        condition['createdDate'] = {
            "$gte": moment(req.query.dateFrom, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).utc(true).toDate(),
            "$lt": moment(req.query.dateTo, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).add(1,'d').utc(true).toDate()
        }
    }
    condition['lotto.amb.gameId'] = req.query.gameId;

    condition['status'] = 'RUNNING';

    let group = '';
    let child = '';
    if (userInfo.group.type === 'SUPER_ADMIN') {
        group = 'superAdmin';
        child = 'company';
    } else if (userInfo.group.type === 'COMPANY') {
        group = 'company';
        child = 'shareHolder';
    } else if (userInfo.group.type === 'SHARE_HOLDER') {
        group = 'shareHolder';
        child = 'superSenior';
    } else if (userInfo.group.type === 'SUPER_SENIOR') {
        group = 'superSenior';
        child = 'senior';
    } else if (userInfo.group.type === 'SENIOR') {
        group = 'senior';
        child = 'masterAgent';
    } else if (userInfo.group.type === 'MASTER_AGENT') {
        group = 'masterAgent';
        child = 'agent';
    } else if (userInfo.group.type === 'AGENT') {
        group = 'agent';
        child = 'member';
    }

    condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(userInfo.group._id);

    BetTransactionModel.aggregate([
        {
            $match: condition
        },
        {
            $group: {
                _id: {
                    betType: "$lotto.amb.betType",
                },
                amount: {$sum: '$commission.' + group + '.amount'}
            }
        },
        {
            $project: {
                _id: 0,
                betType: "$_id.betType",
                amount: '$amount'
            }
        }
    ], (err, results) => {
        if (err)
            return res.send({message: "error", results: err, code: 999});


        if (err)
            return res.send({message: "error", results: err, code: 999});

        // '_1TOP','_1BOT','_2TOP','_2BOT','_2TOD','_2TOP_OE','_2TOP_OU',
        //     '_3TOP','_3BOT','_3TOD','_3TOP_OE','_3TOP_OU',
        //     '_4TOP','_4TOD','_5TOP','_5TOD','_6TOP','_6TOD'-->

        let info = _.object(_.map(results, _.values));

        let totalBetAmount = _.reduce(results, function (total, x) {
            return total + x.amount;
        }, 0);

        let result = {
            _ALL: totalBetAmount,
            _1TOP: _.defaults(info, {_1TOP: 0})._1TOP,
            _1BOT: _.defaults(info, {_1BOT: 0})._1BOT,
            _2TOP: _.defaults(info, {_2TOP: 0})._2TOP,
            _2BOT: _.defaults(info, {_2BOT: 0})._2BOT,
            _2TOD: _.defaults(info, {_2TOD: 0})._2TOD,
            _2TOP_OE: _.defaults(info, {_2TOP_OE: 0})._2TOP_OE,
            _2TOP_OU: _.defaults(info, {_2TOP_OU: 0})._2TOP_OU,
            _2BOT_OE: _.defaults(info, {_2BOT_OE: 0})._2BOT_OE,
            _2BOT_OU: _.defaults(info, {_2BOT_OU: 0})._2BOT_OU,
            _3TOP: _.defaults(info, {_3TOP: 0})._3TOP,
            _3BOT: _.defaults(info, {_3BOT: 0})._3BOT,
            _3TOD: _.defaults(info, {_3TOD: 0})._3TOD,
            _3TOP_OE: _.defaults(info, {_3TOP_OE: 0})._3TOP_OE,
            _3TOP_OU: _.defaults(info, {_3TOP_OU: 0})._3TOP_OU,
            _4TOP: _.defaults(info, {_4TOP: 0})._4TOP,
            _4TOD: _.defaults(info, {_4TOD: 0})._4TOD,
            _5TOP: _.defaults(info, {_5TOP: 0})._5TOP,
            _6TOP: _.defaults(info, {_6TOP: 0})._6TOP,
        };

        console.log(results)
        return res.send(
            {
                code: 0,
                message: "success",
                result: {
                    info: result
                }
            }
        );
    });

});

router.get('/stake-statistic', function (req, res) {

    let condition = {game: 'LOTTO',source: 'AMB_LOTTO'};

    let userInfo = req.userInfo;

    // if (req.query.dateFrom && req.query.dateTo) {
    //     condition['createdDate'] = {
    //         "$gte": new Date(moment(req.query.dateFrom, "DD/MM/YYYY").format('YYYY-MM-DDT00:00:00.000')),
    //         "$lt": new Date(moment(req.query.dateTo, "DD/MM/YYYY").add(1, 'd').format('YYYY-MM-DDT00:00:00.000'))
    //     }
    // }

    condition['lotto.amb.gameId'] = req.query.gameId;

    condition['$or'] = [{'status': 'RUNNING'}, {'status': 'DONE'}];

    let group = '';
    let child = '';
    if (userInfo.group.type === 'SUPER_ADMIN') {
        group = 'superAdmin';
        child = 'company';
    } else if (userInfo.group.type === 'COMPANY') {
        group = 'company';
        child = 'shareHolder';
    } else if (userInfo.group.type === 'SHARE_HOLDER') {
        group = 'shareHolder';
        child = 'superSenior';
    } else if (userInfo.group.type === 'SUPER_SENIOR') {
        group = 'superSenior';
        child = 'senior';
    } else if (userInfo.group.type === 'SENIOR') {
        group = 'senior';
        child = 'masterAgent';
    } else if (userInfo.group.type === 'MASTER_AGENT') {
        group = 'masterAgent';
        child = 'agent';
    } else if (userInfo.group.type === 'AGENT') {
        group = 'agent';
        child = 'member';
    }

    condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(userInfo.group._id);

    console.log(condition);
    BetTransactionModel.aggregate([
        {
            $match: condition
        },
        {
            $group: {
                _id: {
                    betType: "$lotto.amb.betType",
                },
                amount: {$sum: '$commission.' + group + '.amount'},
                count: {$sum: 1}
            }
        },
        {
            $project: {
                _id: 0,
                betType: "$_id.betType",
                amount: '$amount',
                count: '$count'
            }
        }
    ], (err, results) => {
        if (err)
            return res.send({message: "error", results: err, code: 999});


        if (err)
            return res.send({message: "error", results: err, code: 999});

        const types = ['_1TOP', '_1BOT', '_2TOP', '_2TOD', '_2BOT', '_2TOP_OE', '_2TOP_OU',
            '_2BOT_OE', '_2BOT_OU',
            '_3TOP', '_3BOT', '_3TOD', '_3TOP_OE', '_3TOP_OU',
            '_4TOP', '_4TOD', '_5TOP', '_6TOP'];


        let result = [];
        _.forEach(types, type => {

            let tran = _.filter(results, item => {
                return item.betType == type;
            })[0];

            if (tran) {

                result.push({betType: type, totalBet: tran.amount, count: tran.count});
            } else {
                result.push({betType: type, totalBet: 0, count: 0})
            }
        });

        const totalBetAmount = _.reduce(result, function (total, obj) {
            return total + obj.totalBet;
        }, 0);

        _.each(result, (item) => {
            _.extend(item, {betPercent: findBetAmountPercentage(totalBetAmount, item)});
        });

        const summary = _.reduce(result, function (memo, obj) {
            return {
                totalBetAmount: memo.totalBetAmount + obj.totalBet,
                totalBetPercent: memo.totalBetPercent + obj.betPercent,
                totalBetCount: memo.totalBetCount + obj.count
            }
        }, {
            totalBetAmount: 0,
            totalBetPercent: 0,
            totalBetCount: 0,
        });


        function findBetAmountPercentage(totalBet, item) {
            return roundTo((item.totalBet / totalBet ) * 100, 3);
        }


        return res.send(
            {
                code: 0,
                message: "success",
                result: {
                    list: result.reverse(),
                    summary: summary
                }
            }
        );
    });

});

const DRAW = "DRAW";
const WIN = "WIN";
const HALF_WIN = "HALF_WIN";
const LOSE = "LOSE";
const HALF_LOSE = "HALF_LOSE";


function subPlyAmount(amount, count) {
    if (count > 0) {
        return subPlyAmount(amount / 2, --count);
    } else {
        return amount;
    }

}

router.get('/agent-member', function (req, res) {


    let groupId = req.query.groupId;
    // let userInfo = req.userInfo.type;

    async.series([(callback => {
        AgentGroupModel.findById(groupId, (err, response) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, response);
            }
        }).select('type parentId')

    }), (callback => {


        let condition = {};
        condition['gameDate'] = {$gte: new Date(moment().add(-20, 'd').format('YYYY-MM-DDT00:00:00.000'))};
        condition['memberParentGroup'] = mongoose.Types.ObjectId(groupId);
        condition['status'] = 'RUNNING';
        condition['game'] = 'LOTTO';
        condition['source'] = 'AMB_LOTTO';

        console.log(condition)
        BetTransactionModel.aggregate([
            {
                $match: condition
            },
            {
                "$group": {
                    "_id": {
                        commission: '$memberParentGroup',
                        member: '$memberId'
                        // m:'$memberParentGroup'
                    },
                    "amount": {$sum: '$amount'},

                    "memberWinLose": {$sum: '$commission.member.amount'},

                    "agentWinLose": {$sum: '$commission.agent.amount'},

                    "masterAgentWinLose": {$sum: '$commission.masterAgent.amount'},

                    "seniorWinLose": {$sum: '$commission.senior.amount'},

                    "shareHolderWinLose": {$sum: '$commission.shareHolder.amount'},

                    "companyWinLose": {$sum: '$commission.company.amount'},

                    "superAdminWinLose": {$sum: '$commission.superAdmin.amount'},

                    // company_stake: {
                    //     $sum: {
                    //         $cond: [{$eq: ['$gameType', 'PARLAY']}, {$multiply: ['$commission.company.amount', {$abs: '$parlay.odds'}]}, {$multiply: ['$commission.company.amount', {$abs: '$hdp.odd'}]}]
                    //     }
                    // },
                }
            },
            {
                $project: {
                    _id: 0,
                    type: 'M_GROUP',
                    member: '$_id.member',
                    group: '$_id.commission',
                    amount: '$amount',
                    memberWinLose: '$memberWinLose',
                    agentWinLose: '$agentWinLose',
                    masterAgentWinLose: '$masterAgentWinLose',
                    seniorWinLose: '$seniorWinLose',
                    shareHolderWinLose: '$shareHolderWinLose',
                    companyWinLose: '$companyWinLose',
                    superAdminWinLose: '$superAdminWinLose',
                    // company_stake: '$company_stake'
                }
            }
        ], (err, results) => {
            if (err) {
                callback(err, null);
            } else {
                MemberModel.populate(results, {
                    path: 'member',
                    select: '_id username username_lower contact'
                }, function (err, memberResult) {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, memberResult);
                    }
                });
            }
        });

    })], function (err, asyncResponse) {

        if (err) {
            return res.send({message: "error", result: err, code: 999});
        }
        let agentGroup = asyncResponse[0];

        let group = '';
        let childGroup;
        if (agentGroup.type === 'SUPER_ADMIN') {
            group = 'superAdmin';
            childGroup = 'company';
        } else if (agentGroup.type === 'COMPANY') {
            group = 'company';
            childGroup = 'shareHolder';
        } else if (agentGroup.type === 'SHARE_HOLDER') {
            group = 'shareHolder';
            childGroup = 'senior';
        } else if (agentGroup.type === 'SENIOR') {
            group = "senior";
            childGroup = 'masterAgent,agent';
        } else if (agentGroup.type === 'MASTER_AGENT') {
            group = "masterAgent";
            childGroup = 'agent';
        } else if (agentGroup.type === 'AGENT') {
            group = "agent";
            childGroup = '';
        }


        let transaction = [];

        async.map(childGroup.split(','), (child, callback) => {

            let condition = {};

            condition.gameDate = {$gte: new Date(moment().add(-30, 'd').format('YYYY-MM-DDT11:00:00.000'))};
            // if (req.query.dateFrom && req.query.dateTo) {
            //     condition['createdDate'] = {
            //         "$gte": new Date(moment(req.query.dateFrom, "DD/MM/YYYY").format('YYYY-MM-DDT00:00:00.000')),
            //         "$lt": new Date(moment(req.query.dateTo, "DD/MM/YYYY").add(1, 'd').format('YYYY-MM-DDT00:00:00.000'))
            //     }
            // }
            condition['memberParentGroup'] = {$ne: mongoose.Types.ObjectId(groupId)};
            condition['status'] = 'RUNNING';
            condition['game'] = 'LOTTO';
            condition['source'] = 'AMB_LOTTO';
            condition['$and'] = [
                {['commission.' + child + '.parentGroup']: mongoose.Types.ObjectId(groupId)},
                {['commission.' + group + '.group']: mongoose.Types.ObjectId(groupId)}
            ];

            BetTransactionModel.aggregate([
                {
                    $match: condition
                },
                {
                    "$group": {
                        "_id": {
                            commission: '$commission.' + child + '.group',
                        },
                        "amount": {$sum: '$amount'},

                        "memberWinLose": {$sum: '$commission.member.amount'},

                        "agentWinLose": {$sum: '$commission.agent.amount'},

                        "masterAgentWinLose": {$sum: '$commission.masterAgent.amount'},

                        "seniorWinLose": {$sum: '$commission.senior.amount'},

                        "shareHolderWinLose": {$sum: '$commission.shareHolder.amount'},

                        "companyWinLose": {$sum: '$commission.company.amount'},

                        "superAdminWinLose": {$sum: '$commission.superAdmin.amount'},
                    }
                },
                {
                    $project: {
                        _id: 0,
                        type: 'A_GROUP',
                        group: '$_id.commission',
                        amount: '$amount',
                        memberWinLose: '$memberWinLose',
                        agentWinLose: '$agentWinLose',
                        masterAgentWinLose: '$masterAgentWinLose',
                        seniorWinLose: '$seniorWinLose',
                        shareHolderWinLose: '$shareHolderWinLose',
                        companyWinLose: '$companyWinLose',
                        superAdminWinLose: '$superAdminWinLose',

                    }
                }
            ], (err, results) => {
                _.each(results, item => {
                    transaction.push(item);
                });
                callback();
            });
        }, (err) => {

            if (err)
                return res.send({message: "error", results: err, code: 999});

            let memberGroupList = asyncResponse[1];
            let mergeDataList = transaction.concat(memberGroupList);

            async.series([(callback => {

                AgentGroupModel.populate(mergeDataList, {
                    path: 'group',
                    select: '_id name name_lower contact'
                }, function (err, memberResult) {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, memberResult);
                    }
                });


            })], (err, populateResults) => {

                if (err)
                    return res.send({message: "error", results: err, code: 999});


                let finalResult = transformDataByAgentType(agentGroup.type, mergeDataList);


                finalResult = finalResult.sort(function (a, b) {
                    let aKey = a.type === 'M_GROUP' ? a.member.username : a.group.name;
                    let bKey = b.type === 'M_GROUP' ? b.member.username : b.group.name;
                    return naturalSort(aKey, bKey);
                });

                let summary = _.reduce(finalResult, function (memo, obj) {
                    return {
                        amount: memo.amount + obj.amount,
                        memberWinLose: memo.memberWinLose + obj.memberWinLose,
                        agentWinLose: memo.agentWinLose + obj.agentWinLose,
                        companyWinLose: memo.companyWinLose + obj.companyWinLose
                    }
                }, {
                    amount: 0,
                    memberWinLose: 0,
                    agentWinLose: 0,
                    companyWinLose: 0,
                });


                return res.send(
                    {
                        code: 0,
                        message: "success",
                        result: {
                            group: agentGroup,
                            dataList: finalResult,
                            summary: summary,
                        }
                    }
                );

            });
        });//end forEach Match
    });


});

router.get('/limitSetting', function (req, res) {

    let userInfo = req.userInfo;

    let selected = 'min hour isEnableHotNumber po._1TOP.limit po._1BOT.limit po._2TOP.limit po._2BOT.limit po._2TOD.limit' +
        ' po._2TOP_OE.limit po._2TOP_OU.limit po._2BOT_OE.limit po._2BOT_OU.limit' +
        ' po._3TOP.limit po._3BOT.limit po._3TOD.limit po._3TOP_OE.limit po._3TOP_OU.limit ' +
        ' po._4TOP.limit po._4TOD.limit po._5TOP.limit po._6TOP.limit';
    AgentLottoLimitModel.findOne({agent: mongoose.Types.ObjectId(userInfo.group._id)}, selected, (err, response) => {
        if (err)
            return res.send({message: "error", result: err, code: 999});

        return res.send({message: "success", result: response, code: 0});
    }).lean();

});


router.get('/limitSetting/log', function (req, res) {

    let userInfo = req.userInfo;

    let selected = 'min hour isEnableHotNumber po._1TOP.limit po._1BOT.limit po._2TOP.limit po._2BOT.limit po._2TOD.limit' +
        ' po._2TOP_OE.limit po._2TOP_OU.limit po._2BOT_OE.limit po._2BOT_OU.limit' +
        ' po._3TOP.limit po._3BOT.limit po._3TOD.limit po._3TOP_OE.limit po._3TOP_OU.limit ' +
        ' po._4TOP.limit po._4TOD.limit po._5TOP.limit po._6TOP.limit';

    let condition = {
        agent: mongoose.Types.ObjectId(userInfo.group._id)
    };

    AgentLottoLimitHistoryModel.find(condition).sort({'createdDate': -1}).limit(10).exec((err, response) => {
        if (err)
            return res.send({message: "error", result: err, code: 999});

        return res.send({
            message: "success", result: {
                list: response
            }, code: 0
        });
    });
});

const limitSettingSchema = Joi.object().keys({
    hour: Joi.number().required(),
    min: Joi.number().required(),
    isEnableHotNumber: Joi.boolean().required(),
    po: Joi.object().required()
});

router.post('/limitSetting', (req, res) => {

    let validate = Joi.validate(req.body, limitSettingSchema);
    if (validate.error) {
        console.log('Request Incomplete')
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 1001
        });
    }

    let userInfo = req.userInfo;


    async.parallel([callback => {

        let updateBody = {
            $set: {
                'hour': req.body.hour,
                'min': req.body.min,
                'isEnableHotNumber': false,
                'po._1TOP.limit': req.body.po._1TOP.limit,
                'po._1BOT.limit': req.body.po._1BOT.limit,
                'po._2TOP.limit': req.body.po._2TOP.limit,
                'po._2BOT.limit': req.body.po._2BOT.limit,
                'po._2TOD.limit': req.body.po._2TOD.limit,
                'po._2TOP_OE.limit': req.body.po._2TOP_OE.limit,
                'po._2TOP_OU.limit': req.body.po._2TOP_OU.limit,
                'po._2BOT_OE.limit': req.body.po._2BOT_OE.limit,
                'po._2BOT_OU.limit': req.body.po._2BOT_OU.limit,
                'po._3TOP.limit': req.body.po._3TOP.limit,
                'po._3BOT.limit': req.body.po._3BOT.limit,
                'po._3TOD.limit': req.body.po._3TOD.limit,
                'po._3TOP_OE.limit': req.body.po._3TOP_OE.limit,
                'po._3TOP_OU.limit': req.body.po._3TOP_OU.limit,
                'po._4TOP.limit': req.body.po._4TOP.limit,
                'po._4TOD.limit': req.body.po._4TOD.limit,
                'po._5TOP.limit': req.body.po._5TOP.limit,
                'po._6TOP.limit': req.body.po._6TOP.limit
            }

        };


        AgentLottoLimitModel.update({agent: mongoose.Types.ObjectId(userInfo.group._id)}, updateBody, (err, response) => {
            if (err) {
                callback(err, null);
                return;
            }

            let historyBody = req.body;
            historyBody.agent = userInfo.group._id;
            historyBody.updateBy = userInfo._id;
            historyBody.createdDate = DateUtils.getCurrentDate();

            let modelHistory = new AgentLottoLimitHistoryModel(historyBody);

            modelHistory.save((err, limitHistoryResponse) => {

                if (err) {
                    callback(err, null);
                    return;
                } else {
                    callback(null, limitHistoryResponse);
                }

            });

        });


    }, callback => {
        callback(null, '');
    }, callback => {
        callback(null, '');
    }], function (err, asyncResponse) {

        return res.send({
            code: 0,
            message: "success",
            result: 0
        });
    });


});


function transformDataByAgentType(type, dataList) {
    if (type === 'SUPER_ADMIN') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = 0;

            newItem.agentWinLose = item.superAdminWinLose;

            newItem.memberWinLose = (item.masterAgentWinLose + item.agentWinLose + item.seniorWinLose + item.shareHolderWinLose + item.companyWinLose);

            return newItem;
        });
    } else if (type === 'COMPANY') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = item.superAdminWinLose;

            newItem.agentWinLose = item.companyWinLose;

            newItem.memberWinLose = (item.masterAgentWinLose + item.agentWinLose + item.seniorWinLose + item.shareHolderWinLose);

            return newItem;
        });
    } else if (type === 'SHARE_HOLDER') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose;

            newItem.agentWinLose = item.shareHolderWinLose;

            newItem.memberWinLose = (item.masterAgentWinLose + item.agentWinLose + item.seniorWinLose);
            return newItem;
        });
    } else if (type === 'SENIOR') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose + item.shareHolderWinLose;

            newItem.agentWinLose = item.seniorWinLose;

            newItem.memberWinLose = (item.masterAgentWinLose + item.agentWinLose);
            return newItem;
        });
    } else if (type === 'MASTER_AGENT') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose + item.shareHolderWinLose + item.seniorWinLose;

            newItem.agentWinLose = item.masterAgentWinLose;

            newItem.memberWinLose = (item.agentWinLose);

            return newItem;
        });
    } else if (type === 'AGENT') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);
            newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose + item.shareHolderWinLose + item.seniorWinLose + item.masterAgentWinLose;

            newItem.agentWinLose = item.agentWinLose;

            newItem.memberWinLose = item.memberWinLose;
            return newItem;
        });
    } else {
        return dataList;
    }
}

module.exports = router;