const MemberModel = require('../../models/member.model');
const BetTransactionModel = require('../../models/betTransaction.model.js');
const AgentGroupModel = require('../../models/agentGroup.model.js');
const UserModel = require('../../models/users.model.js');
const PaymentHistoryModel = require('../../models/paymentHistory.model.js');
const Commons = require('../../common/commons');
const ReportService = require('../../common/reportService');
const express = require('express')
const router = express.Router()
const async = require("async");
const Joi = require('joi');
const mongoose = require('mongoose');
const Hashing = require('../../common/hashing');
const config = require('config');
const _ = require('underscore');
const DateUtils = require('../../common/dateUtils');
const moment = require('moment');
const naturalSort = require("javascript-natural-sort");
const {getIndexName} = require("../../common/elasticsearch")


router.get('/commission', function (req, res) {


    const CLIENT_NAME = process.env.CLIENT_NAME || 'SPORTBOOK88';
    let taskHour;
    if (_.isEqual(CLIENT_NAME, 'CSR')) {
        taskHour = 6;
    } else if (_.isEqual(CLIENT_NAME, 'BET123')) {
        taskHour = 6;
    } else if (_.isEqual(CLIENT_NAME, 'AMBBET')) {
        taskHour = 6;
    } else {
        taskHour = 12;
    }

    AgentGroupModel.findOne({type: 'COMPANY'}, '_id commissionSetting limitSetting shareSetting', function (err, agentResponse) {
        if (err) {
            return res.send({message: "error", result: err, code: 999});
        } else {
            let dateList = DateUtils.enumerateReportBetweenDatesTask(moment(req.query.dateFrom, "DD/MM/YYYY"), moment(req.query.dateTo, "DD/MM/YYYY").add(1, 'd'), '11:00:00', '11:00:00', taskHour);

            // let dateList =DateUtils.enumerateNextDaysBetweenDates(moment(req.query.dateFrom, "DD/MM/YYYY"), moment(req.query.dateTo, "DD/MM/YYYY"), 'DD/MM/YYYY');
            let tasks = [];

            _.forEach(dateList, dateStr => {
                tasks.push(getWinLoseAccountPerDay(dateStr, agentResponse._id));
            });


            async.parallel(tasks, (err, results) => {
                if (err) {
                    console.log('err : ', err)
                }

                let summaryDataPerDay = summaryFunction(results);

                let summary = _.reduce(summaryDataPerDay, function (memo, obj) {
                    return {
                        amount: memo.amount + obj.amount,
                        validAmount: memo.validAmount + obj.validAmount,
                        stackCount: memo.stackCount + obj.stackCount,
                        grossComm: memo.grossComm + obj.grossComm,
                        memberWinLose: memo.memberWinLose + obj.memberWinLose,
                        memberWinLoseCom: memo.memberWinLoseCom + obj.memberWinLoseCom,
                        memberTotalWinLoseCom: memo.memberTotalWinLoseCom + obj.memberTotalWinLoseCom,

                        superAdminWinLose: memo.superAdminWinLose + obj.superAdminWinLose,
                        superAdminWinLoseCom: memo.superAdminWinLoseCom + obj.superAdminWinLoseCom,
                        superAdminTotalWinLoseCom: memo.superAdminTotalWinLoseCom + obj.superAdminTotalWinLoseCom
                    }
                }, {
                    amount: 0,
                    validAmount: 0,
                    stackCount: 0,
                    grossComm: 0,
                    memberWinLose: 0,
                    memberWinLoseCom: 0,
                    memberTotalWinLoseCom: 0,
                    superAdminWinLose: 0,
                    superAdminWinLoseCom: 0,
                    superAdminTotalWinLoseCom: 0
                });

                return res.send(
                    {
                        code: 0,
                        message: "success",
                        result: {
                            info: agentResponse,
                            dataList: summaryDataPerDay,
                            summary: summary
                        }
                    }
                );
            })

        }
    });


});


router.get('/commission2', function (req, res) {


    AgentGroupModel.findOne({type: 'COMPANY'}, 'commissionSetting limitSetting shareSetting', function (err, agentResponse) {
        if (err) {
            return res.send({message: "error", result: err, code: 999});
        } else {

            ReportService.getAmbReport(req.query.dateFrom, req.query.dateTo, agentResponse._id,function (err, wlResponse) {


                let summary = _.reduce(wlResponse, function (memo, obj) {
                    return {
                        amount: memo.amount + obj.amount,
                        validAmount: memo.validAmount + obj.validAmount,
                        stackCount: memo.stackCount + obj.stackCount,
                        grossComm: memo.grossComm + obj.grossComm,
                        memberWinLose: memo.memberWinLose + obj.memberWinLose,
                        memberWinLoseCom: memo.memberWinLoseCom + obj.memberWinLoseCom,
                        memberTotalWinLoseCom: memo.memberTotalWinLoseCom + obj.memberTotalWinLoseCom,

                        superAdminWinLose: memo.superAdminWinLose + obj.superAdminWinLose,
                        superAdminWinLoseCom: memo.superAdminWinLoseCom + obj.superAdminWinLoseCom,
                        superAdminTotalWinLoseCom: memo.superAdminTotalWinLoseCom + obj.superAdminTotalWinLoseCom
                    }
                }, {
                    amount: 0,
                    validAmount: 0,
                    stackCount: 0,
                    grossComm: 0,
                    memberWinLose: 0,
                    memberWinLoseCom: 0,
                    memberTotalWinLoseCom: 0,
                    superAdminWinLose: 0,
                    superAdminWinLoseCom: 0,
                    superAdminTotalWinLoseCom: 0
                });

                return res.send(
                    {
                        code: 0,
                        message: "success",
                        result: {
                            info: agentResponse,
                            dataList: wlResponse,
                            summary: summary
                        }
                    }
                );
            })
        }
    });

});


function getWinLoseAccountPerDay(dateRange,groupId) {
    console.log('dateStr : ' + dateRange);

    return function (callbackTask) {

        let condition = {};

        condition['$and'] = [
            {
                'gameDate': {
                    "$gte": moment(dateRange[0], 'YYYY-MM-DDTHH:mm:ss').utc(true).toDate(),
                    "$lt": moment(dateRange[1], 'YYYY-MM-DDTHH:mm:ss').utc(true).toDate()
                }
            },
            {'commission.company.group': mongoose.Types.ObjectId(groupId)},
            {'status': {$in: ['DONE', 'REJECTED', 'CANCELLED']}}
        ];


        BetTransactionModel.aggregate([
            {
                $match: condition
            },
            {
                "$group": {
                    "_id": {
                        game: '$game',
                        source: '$source'
                    },
                    "stackCount": {$sum: 1},
                    "amount": {$sum: '$amount'},
                    "validAmount": {$sum: '$validAmount'},
                    "grossComm": {$sum: {$multiply: ['$validAmount', {$divide: ['$commission.superAdmin.commission', 100]}]}},
                    "memberWinLose": {$sum: '$commission.member.winLose'},
                    "memberWinLoseCom": {$sum: '$commission.member.winLoseCom'},
                    "memberTotalWinLoseCom": {$sum: '$commission.member.totalWinLoseCom'},

                    "superAdminWinLose": {$sum: '$commission.superAdmin.winLose'},
                    "superAdminWinLoseCom": {$sum: '$commission.superAdmin.winLoseCom'},
                    "superAdminTotalWinLoseCom": {$sum: '$commission.superAdmin.totalWinLoseCom'}
                }
            },
            {
                $project: {
                    _id: 0,
                    game: '$_id.game',
                    source: '$_id.source',
                    stackCount: '$stackCount',
                    amount: '$amount',
                    validAmount: '$validAmount',
                    grossComm: '$grossComm',
                    memberWinLose: '$memberWinLose',
                    memberWinLoseCom: '$memberWinLoseCom',
                    memberTotalWinLoseCom: '$memberTotalWinLoseCom',
                    superAdminWinLose: '$superAdminWinLose',
                    superAdminWinLoseCom: '$superAdminWinLoseCom',
                    superAdminTotalWinLoseCom: '$superAdminTotalWinLoseCom',

                }
            }
        ]).read('secondaryPreferred').exec((err, results) => {
            // separateAggregate(condition,group,child,(err, results) => {
            if (err) {
                callbackTask(err, null);
            } else {
                callbackTask(null, results);
            }
        });

    }

}


function summaryFunction(transactions) {


    return _.reduce(transactions, function (preList, nextList) {

        let uniqueData = _.uniq(_.union(preList, nextList), false, function (item, key, a) {
            return item.game.toString() + item.source.toString();
        });


        return _.map(uniqueData, (uniqueObj) => {

            let preData = _.filter(preList, (preObj) => {
                return (uniqueObj.game.toString() + uniqueObj.source.toString()) === (preObj.game.toString() + preObj.source.toString());
            })[0];

            let next = _.filter(nextList, (nextObj) => {
                return (uniqueObj.game.toString() + uniqueObj.source.toString()) === (nextObj.game.toString() + nextObj.source.toString());
            })[0];


            if (preData && next) {
                return {
                    game: preData.game,
                    source: preData.source,

                    amount: preData.amount + next.amount,
                    validAmount: preData.validAmount + next.validAmount,
                    stackCount: preData.stackCount + next.stackCount,
                    grossComm: preData.grossComm + next.grossComm,
                    memberWinLose: preData.memberWinLose + next.memberWinLose,
                    memberWinLoseCom: preData.memberWinLoseCom + next.memberWinLoseCom,
                    memberTotalWinLoseCom: preData.memberTotalWinLoseCom + next.memberTotalWinLoseCom,


                    superAdminWinLose: preData.superAdminWinLose + next.superAdminWinLose,
                    superAdminWinLoseCom: preData.superAdminWinLoseCom + next.superAdminWinLoseCom,
                    superAdminTotalWinLoseCom: preData.superAdminTotalWinLoseCom + next.superAdminTotalWinLoseCom
                }
            } else if (preData) {
                return preData;
            } else if (next) {
                return next;
            }

        });

    });


}

module.exports = router;