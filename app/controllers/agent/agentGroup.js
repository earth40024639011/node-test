const AgentGroupModel = require('../../models/agentGroup.model');
const AgentGroupHistoryModel = require('../../models/agentGroupHistory.model');
const MemberModel = require('../../models/member.model');
const MemberHistoryModel = require('../../models/memberHistory.model');
const BetTransactionModel = require('../../models/betTransaction.model.js');
const AnnouncementModel = require('../../models/announcement.model.js');
const AnnouncementFrontEndModel = require('../../models/announcementFrontEnd.model.js');
const UserModel = require('../../models/users.model');
const AgentService = require('../../common/agentService');
const express = require('express')
const router = express.Router()
const async = require("async");
const Joi = require('joi');
const mongoose = require('mongoose');
const config = require('config');
const Hashing = require('../../common/hashing');
const DateUtils = require('../../common/dateUtils');
const logger = require('../../../utils/logger');
const _ = require('underscore');
const zpad = require('zpad');
const naturalSort = require("javascript-natural-sort");
const shortid = require('shortid');
const AgentLottoLimitModel = require('../../models/agentLottoLimit.model.js');
const AgentLottoPPLimitModel = require('../../models/agentLottoPPLimit.model.js');
const AgentLottoLaosLimitModel = require('../../models/agentLottoLaosLimit.model.js');

const UpdateStatusHistoryModel = require('../../models/updateStatusHistory.model.js');

const moment = require('moment');


router.get('/uplineStatus', function (req, res) {

    console.log(req.query.id)
    AgentService.getUpLineStatus(req.query.id, (err, response) => {
        if (err) {
            return res.send({message: "error", results: err, code: 999});
        } else {
            return res.send({message: "success", results: response, code: 0});
        }
    });

});

router.get('/info', function (req, res) {

    let userInfo = req.userInfo;

    async.parallel([callback => {
        AgentService.getCreditLimitUsage(userInfo.group._id, function (err, response) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, response);
            }
        });
    }, (callback) => {

        AgentGroupModel.findById(userInfo.group._id, function (err, response) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, response);
            }
        }).select('childGroups childMembers currency creditLimit maxCreditLimit');

    }, (callback) => {

        AnnouncementFrontEndModel.findOne({key: 'HOMEPAGE'}, function (err, response) {
            if (err) {
                callback(err, null);
            } else {
                if (response.active) {
                    callback(null, response);
                } else {
                    AnnouncementModel.findOne({
                        '$or': [{type: 'AGENT'}, {type: 'ALL'}],
                        active: true
                    }, function (err, response) {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, response);
                        }
                    }).sort({_id: -1})
                        .select('message')
                }
            }
        });


    }], (err, asyncResponse) => {

        if (err) {
            return res.send({message: "error", result: err, code: 999});
        }

        let result = {
            childCount: asyncResponse[1].childGroups.length,
            creditUsage: asyncResponse[0],
            currency: asyncResponse[1].currency,
            remainingCredit: asyncResponse[1].creditLimit - asyncResponse[0],
            maxCreditLimit: asyncResponse[1].maxCreditLimit,
            message: asyncResponse[2] ? asyncResponse[2].message : ''
        };

        // console.log(result)

        return res.send({message: "success", result: result, code: 0});
    });

});

router.get('/init', function (req, res) {

    let userInfo = req.userInfo;
    let initGroupId = req.query.initGroupId;


    let activeId = userInfo.group._id;

    let mType = req.query.mType;


    let type = '', prefix = '';
    if (userInfo.group.type === 'COMPANY') {

        if (req.query.group === 'API') {
            type = 'API';
            prefix = 'api';
        } else {
            type = 'SHARE_HOLDER';
            prefix = 'sh';
        }
    }

    async.parallel([(callback => {

        if (mType) {

            if (req.query.type == 'view' || req.query.type == 'edit') {
                activeId = req.query.initGroupId;
            }

            if (mType === 'AGENT') {
                AgentGroupModel.findById(activeId, (err, response) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        if (req.query.type == 'view' || req.query.type == 'edit') {
                            callback(null, response.parentId);
                        } else {
                            callback(null, response);
                        }
                    }
                }).populate('parentId');

            } else if (mType === 'MEMBER') {

                if (req.query.type == 'view' || req.query.type == 'edit') {
                    MemberModel.findById(activeId, (err, response) => {
                        if (err) {
                            callback(err, null);
                        } else {
                            if (req.query.type == 'view' || req.query.type == 'edit') {
                                callback(null, response.group);
                            } else {
                                callback(null, response);
                            }
                        }
                    }).populate('group');
                } else {
                    AgentGroupModel.findById(activeId, (err, response) => {
                        if (err) {
                            callback(err, null);
                        } else {
                            if (req.query.type == 'view' || req.query.type == 'edit') {
                                callback(null, response.parentId);
                            } else {
                                callback(null, response);
                            }
                        }
                    });
                }
            }
        } else {
            AgentGroupModel.findById(activeId, (err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            });
        }


    }), (callback => {

        if (initGroupId) {
            AgentGroupModel.findById(initGroupId, (err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            });
        } else {
            callback(null, null);
        }


    }), (callback => {

        if (userInfo.group.type === 'SHARE_HOLDER') {
            callback(null, '');
        } else if (userInfo.group.type === 'SENIOR') {
            callback(null, userInfo.group.name);
        } else if (userInfo.group.type === 'MASTER_AGENT') {
            callback(null, userInfo.group.name);
        } else if (userInfo.group.type === 'AGENT') {
            callback(null, userInfo.group.name);
        } else {

            AgentGroupModel.countDocuments({type: type}, (err, count) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, (count + 1) + prefix);
                }
            });
        }

    }), (callback => {

        AgentService.getCreditLimitUsage(userInfo.group._id, function (err, response) {
            if (err) {
                callback(err, '');
            } else {
                callback(null, response);
            }
        });

    })], (err, results) => {

        if (err) {
            return res.send({message: "error", result: err, code: 999});
        }

        const currentAgentInfo = results[0];
        const currentGroup = results[1];
        const prefixName = results[2];
        const creditLimitUsage = results[3];

        async.series([callback => {

            AgentService.getUpLineStatus(currentAgentInfo._id, (err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            });

        }], (err, uplineAsyncResponse) => {

            const updateLineStatus = uplineAsyncResponse[0];

            let initConfig = {
                creditLimit: (currentAgentInfo.creditLimit - creditLimitUsage),//creditLimit ,
                maxCreditLimit: currentAgentInfo.maxCreditLimit,
                // maxMemberCreditLimit: currentAgentInfo.maxMemberCreditLimit,
                shareSetting: {
                    sportsBook: {
                        maxToday: currentAgentInfo.shareSetting.sportsBook.today.own,
                        minToday: currentAgentInfo.shareSetting.sportsBook.today.min,
                        maxLive: currentAgentInfo.shareSetting.sportsBook.live.own,
                        minLive: currentAgentInfo.shareSetting.sportsBook.live.min,
                    },
                    step: {
                        maxParlay: currentAgentInfo.shareSetting.step.parlay.own,
                        minParlay: currentAgentInfo.shareSetting.step.parlay.min,
                        maxStep: currentAgentInfo.shareSetting.step.step.own,
                        minStep: currentAgentInfo.shareSetting.step.step.min,
                    },
                    casino: {
                        maxSexy: currentAgentInfo.shareSetting.casino.sexy.own,
                        minSexy: currentAgentInfo.shareSetting.casino.sexy.min,
                        maxAG: currentAgentInfo.shareSetting.casino.ag.own,
                        minAG: currentAgentInfo.shareSetting.casino.ag.min,
                        // maxAllBet: currentAgentInfo.shareSetting.casino.allbet.own,
                        // minAllBet: currentAgentInfo.shareSetting.casino.allbet.min,
                        maxSa: currentAgentInfo.shareSetting.casino.sa.own,
                        minSa: currentAgentInfo.shareSetting.casino.sa.min,
                        maxDg: currentAgentInfo.shareSetting.casino.dg.own,
                        minDg: currentAgentInfo.shareSetting.casino.dg.min,
                        maxPt: currentAgentInfo.shareSetting.casino.pt.own,
                        minPt: currentAgentInfo.shareSetting.casino.pt.min,
                    },
                    game: {
                        maxSlotXO: currentAgentInfo.shareSetting.game.slotXO.own,
                        minSlotXO: currentAgentInfo.shareSetting.game.slotXO.min
                    },
                    multi: {
                        maxAmb: currentAgentInfo.shareSetting.multi.amb.own,
                        minAmb: currentAgentInfo.shareSetting.multi.amb.min
                    },
                    lotto: {
                        maxAmbLotto: currentAgentInfo.shareSetting.lotto.amb.own,
                        minAmbLotto: 0,//currentAgentInfo.shareSetting.lotto.amb.min,
                        maxAmbLottoPP: currentAgentInfo.shareSetting.lotto.pp.own,
                        minAmbLottoPP: 0,//currentAgentInfo.shareSetting.lotto.amb.min
                        maxAmbLottoLaos: currentAgentInfo.shareSetting.lotto.laos.own,
                        minAmbLottoLaos: 0//currentAgentInfo.shareSetting.lotto.amb.min
                    },
                    other: {
                        maxM2: currentAgentInfo.shareSetting.other.m2.own,
                        minM2: currentAgentInfo.shareSetting.other.m2.min
                    }
                }, limitSetting: {
                    sportsBook: {
                        hdpOuOe: {
                            maxPerBet: currentAgentInfo.limitSetting.sportsBook.hdpOuOe.maxPerBet,
                            maxPerMatch: currentAgentInfo.limitSetting.sportsBook.hdpOuOe.maxPerMatch,
                        },
                        oneTwoDoubleChance: {
                            maxPerBet: currentAgentInfo.limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet,
                            // maxPerMatch: req.body.limitSetting.sportsBook.oneTwoDoubleChance.maxPerMatch
                        },
                        others: {
                            maxPerBet: currentAgentInfo.limitSetting.sportsBook.others.maxPerBet,
                            // maxPerMatch: req.body.limitSetting.sportsBook.others.maxPerMatch
                        },
                        // mixParlay: {
                        //     maxPerBet: currentAgentInfo.limitSetting.sportsBook.mixParlay.maxPerBet,
                        //     maxPerMatch: currentAgentInfo.limitSetting.sportsBook.mixParlay.maxPerMatch,
                        // },
                        outright: {
                            // maxPerBet: req.body.limitSetting.sportsBook.outright.maxPerBet,
                            maxPerMatch: currentAgentInfo.limitSetting.sportsBook.outright.maxPerMatch,
                        },
                        isEnable: true,
                    },
                    step: {
                        parlay: {
                            maxPerBet: currentAgentInfo.limitSetting.step.parlay.maxPerBet,
                            minPerBet: currentAgentInfo.limitSetting.step.parlay.minPerBet,
                            maxBetPerDay: currentAgentInfo.limitSetting.step.parlay.maxBetPerDay,
                            maxPayPerBill: currentAgentInfo.limitSetting.step.parlay.maxPayPerBill,
                            maxMatchPerBet: currentAgentInfo.limitSetting.step.parlay.maxMatchPerBet,
                            minMatchPerBet: currentAgentInfo.limitSetting.step.parlay.minMatchPerBet,
                            isEnable: true,
                        },
                        step: {
                            maxPerBet: currentAgentInfo.limitSetting.step.step.maxPerBet,
                            minPerBet: currentAgentInfo.limitSetting.step.step.minPerBet,
                            maxBetPerDay: currentAgentInfo.limitSetting.step.step.maxBetPerDay,
                            maxPayPerBill: currentAgentInfo.limitSetting.step.step.maxPayPerBill,
                            maxMatchPerBet: currentAgentInfo.limitSetting.step.step.maxMatchPerBet,
                            minMatchPerBet: currentAgentInfo.limitSetting.step.step.minMatchPerBet,
                            isEnable: true,
                        }
                    },
                    casino: {
                        sexy: {
                            isEnable: true,
                            limit: currentAgentInfo.limitSetting.casino.sexy.limit,
                        },
                        ag: {
                            isEnable: true,
                            limit: currentAgentInfo.limitSetting.casino.ag.limit,
                        },
                        // allbet: {
                        //     isEnable: true,
                        //     // limitA: currentAgentInfo.limitSetting.casino.allbet.limitA,
                        //     // limitB: currentAgentInfo.limitSetting.casino.allbet.limitB,
                        //     // limitC: currentAgentInfo.limitSetting.casino.allbet.limitC,
                        //     // limitVip: currentAgentInfo.limitSetting.casino.allbet.limitVip
                        // },
                        sa: {
                            isEnable: true,
                            limit: currentAgentInfo.limitSetting.casino.sa.limit
                        },
                        dg: {
                            isEnable: true,
                            limit: currentAgentInfo.limitSetting.casino.dg.limit
                        },
                        pt: {
                            isEnable: true,
                            limit: currentAgentInfo.limitSetting.casino.pt.limit
                        },
                    },
                    game: {
                        slotXO: {
                            isEnable: true
                        }
                    },
                    multi: {
                        amb: {
                            isEnable: true
                        }
                    },
                    lotto: {
                        amb: currentAgentInfo.limitSetting.lotto.amb,
                        pp: currentAgentInfo.limitSetting.lotto.pp,
                        laos: currentAgentInfo.limitSetting.lotto.laos
                    },
                    other: {
                        m2: {
                            maxPerBetHDP: currentAgentInfo.limitSetting.other.m2.maxPerBetHDP,
                            minPerBetHDP: currentAgentInfo.limitSetting.other.m2.minPerBetHDP,
                            maxBetPerMatchHDP: currentAgentInfo.limitSetting.other.m2.maxBetPerMatchHDP,

                            maxPerBet: currentAgentInfo.limitSetting.other.m2.maxPerBet,
                            minPerBet: currentAgentInfo.limitSetting.other.m2.minPerBet,
                            maxBetPerDay: currentAgentInfo.limitSetting.other.m2.maxBetPerDay,
                            maxPayPerBill: currentAgentInfo.limitSetting.other.m2.maxPayPerBill,
                            maxMatchPerBet: currentAgentInfo.limitSetting.other.m2.maxMatchPerBet,
                            minMatchPerBet: currentAgentInfo.limitSetting.other.m2.minMatchPerBet,
                            isEnable: true,
                        }
                    },
                }
            };

            if (currentAgentInfo.type === 'COMPANY') {


                let currency = initGroupId ? currentGroup.currency : 'THB';


                let exchangeRate;
                if (currency === 'THB') {
                    exchangeRate = 1;
                } else if (currency === 'MYR') {
                    exchangeRate = 8;
                } else if (currency === 'HDK') {
                    exchangeRate = 4;
                } else if (currency === 'CNY') {
                    exchangeRate = 5;
                } else if (currency === 'IDR') {
                    exchangeRate = 425;
                } else if (currency === 'USD') {
                    exchangeRate = 33;
                }


                if (currency === 'IDR') {
                    initConfig.limitSetting.sportsBook = {
                        hdpOuOe: {
                            maxPerBet: 1000000 * exchangeRate,
                            maxPerMatch: 2000000 * exchangeRate,
                        },
                        oneTwoDoubleChance: {
                            maxPerBet: 1000000 * exchangeRate,
                        },
                        others: {
                            maxPerBet: 1000000 * exchangeRate,
                        },
                        // mixParlay: {
                        //     maxPerBet: 100000 * exchangeRate,
                        //     maxPerMatch: 100000 * exchangeRate,
                        // },
                        outright: {
                            maxPerMatch: 2000000 * exchangeRate,
                        }
                    }
                } else {
                    initConfig.limitSetting.sportsBook = {
                        hdpOuOe: {
                            maxPerBet: 1000000 / exchangeRate,
                            maxPerMatch: 2000000 / exchangeRate,
                        },
                        oneTwoDoubleChance: {
                            maxPerBet: 1000000 / exchangeRate,
                        },
                        others: {
                            maxPerBet: 1000000 / exchangeRate,
                        },
                        // mixParlay: {
                        //     maxPerBet: 100000 / exchangeRate,
                        //     maxPerMatch: 100000 / exchangeRate,
                        // },
                        outright: {
                            maxPerMatch: 2000000 / exchangeRate,
                        }
                    }
                }
            }

            let commission = {
                sportsBook: {
                    hdpOuOeA: currentAgentInfo.commissionSetting.sportsBook.hdpOuOeA,
                    hdpOuOeB: currentAgentInfo.commissionSetting.sportsBook.hdpOuOeB,
                    hdpOuOeC: currentAgentInfo.commissionSetting.sportsBook.hdpOuOeC,
                    hdpOuOeD: currentAgentInfo.commissionSetting.sportsBook.hdpOuOeD,
                    hdpOuOeE: currentAgentInfo.commissionSetting.sportsBook.hdpOuOeE,
                    hdpOuOeF: currentAgentInfo.commissionSetting.sportsBook.hdpOuOeF,
                    oneTwoDoubleChance: currentAgentInfo.commissionSetting.sportsBook.oneTwoDoubleChance,
                    others: currentAgentInfo.commissionSetting.sportsBook.others
                },
                parlay: {
                    com: currentAgentInfo.commissionSetting.parlay.com,
                },
                step: {
                    com2: currentAgentInfo.commissionSetting.step.com2,
                    com3: currentAgentInfo.commissionSetting.step.com3,
                    com4: currentAgentInfo.commissionSetting.step.com4,
                    com5: currentAgentInfo.commissionSetting.step.com5,
                    com6: currentAgentInfo.commissionSetting.step.com6,
                    com7: currentAgentInfo.commissionSetting.step.com7,
                    com8: currentAgentInfo.commissionSetting.step.com8,
                    com9: currentAgentInfo.commissionSetting.step.com9,
                    com10: currentAgentInfo.commissionSetting.step.com10,
                    com11: currentAgentInfo.commissionSetting.step.com11,
                    com12: currentAgentInfo.commissionSetting.step.com12
                },
                casino: {
                    sexy: currentAgentInfo.commissionSetting.casino.sexy,
                    ag: currentAgentInfo.commissionSetting.casino.ag,
                    // allbet: currentAgentInfo.commissionSetting.casino.allbet,
                    sa: currentAgentInfo.commissionSetting.casino.sa,
                    dg: currentAgentInfo.commissionSetting.casino.dg,
                    pt: currentAgentInfo.commissionSetting.casino.pt
                },
                game: {
                    slotXO: currentAgentInfo.commissionSetting.game.slotXO
                },
                multi: {
                    amb: currentAgentInfo.commissionSetting.multi.amb
                },
                other: {
                    com: currentAgentInfo.commissionSetting.other.m2,
                },
                // lotto: {
                //     lotto: currentAgentInfo.commissionSetting.lotto.lotto
                // }
            };

            let rs = {
                prefix: prefixName,
                maxSetting: initConfig,
                commission: commission,
                updateLineStatus: updateLineStatus,
                currency: currentAgentInfo.currency
            };
            return res.send({message: "success", result: rs, code: 0});
        });

    });


});

router.get('/switch_limit', function (req, res) {

    let currency = req.query.currency;

    let limitSetting;

    // 'THB','MYR','HDK','CNY','IDR','USD'

    let exchangeRate;
    if (currency === 'THB') {
        exchangeRate = 1;
    } else if (currency === 'MYR') {
        exchangeRate = 8;
    } else if (currency === 'HDK') {
        exchangeRate = 4;
    } else if (currency === 'CNY') {
        exchangeRate = 5;
    } else if (currency === 'IDR') {
        exchangeRate = 425;
    } else if (currency === 'USD') {
        exchangeRate = 33;
    }
    if (currency === 'IDR') {
        limitSetting = {
            sportsBook: {
                hdpOuOe: {
                    maxPerBet: 1000000 * exchangeRate,
                    maxPerMatch: 2000000 * exchangeRate,
                },
                oneTwoDoubleChance: {
                    maxPerBet: 1000000 * exchangeRate,
                },
                others: {
                    maxPerBet: 1000000 * exchangeRate,
                },
                mixParlay: {
                    maxPerBet: 100000 * exchangeRate,
                    maxPerMatch: 100000 * exchangeRate,
                },
                outright: {
                    maxPerMatch: 2000000 * exchangeRate,
                }
            }
        }
    } else {
        limitSetting = {
            sportsBook: {
                hdpOuOe: {
                    maxPerBet: 1000000 / exchangeRate,
                    maxPerMatch: 2000000 / exchangeRate,
                },
                oneTwoDoubleChance: {
                    maxPerBet: 1000000 / exchangeRate,
                },
                others: {
                    maxPerBet: 1000000 / exchangeRate,
                },
                mixParlay: {
                    maxPerBet: 100000 / exchangeRate,
                    maxPerMatch: 100000 / exchangeRate,
                },
                outright: {
                    maxPerMatch: 2000000 / exchangeRate,
                }
            }
        }
    }

    return res.send({message: "success", result: limitSetting, code: 0});

});

router.get('/generateGroupSenior', function (req, res) {

    let groupSenior = `${randomNumber(2)}${randomString(2)}`;

    checkGroup(groupSenior);

    function checkGroup(name) {
        AgentGroupModel.findOne({
            name: name
        }, (err, result) => {
            if (err) {
                return res.send({message: "error", result: err, code: 999});
            } else if (result) {
                groupSenior = `${randomNumber(2)}${randomString(2)}`;

                checkGroup(groupSenior);
            } else {
                return res.send({message: "success", result: groupSenior, code: 0});
            }
        })
    }

});

function randomString(index) {
    let text = "";
    const possible = "abcdefghijklmnopqrstuvwxyz";

    for (let i = 0; i < index; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function randomNumber(index) {

    let number = Math.floor(Math.random() * `${index === 1 ? 9 : 99}`);

    if (number < 10) {
        return `0${number}`;
    } else {
        return `${number}`;
    }

}

router.get('/list', function (req, res) {

    let name = req.query.name;

    let condition = {};

    let userInfo = req.userInfo;


    if (req.query.groupId) {
        condition.parentId = req.query.groupId;
    } else {
        if (userInfo.group._id) {
            condition.parentId = userInfo.group._id;
        }
    }

    if (req.query.active) {
        condition.active = req.query.active;
    }


    if (req.query.username) {
        condition.name_lower = req.query.username.toLowerCase();
    }

    if (req.query.type) {
        condition.type = req.query.type;
    }
    console.log(condition)

    AgentGroupModel.find(condition)
        .select('name name_lower contact phoneNo currency creditLimit createdDate active balance lock suspend active user')
        .populate('parentId')
        .populate({path: 'user', select: 'lastLogin ipAddress'})
        .lean()
        .sort({'active': -1})
        .exec(function (err, response) {
            if (err)
                return res.send({message: "error", results: err, code: 999});

            let summary = _.reduce(response, function (memo, num) {
                return {
                    balance: memo.balance + num.balance,
                    creditLimit: memo.creditLimit + num.creditLimit
                }
            }, {balance: 0, creditLimit: 0});

            //transform
            response = _.map(response, function (item) {
                let newItem = _.clone(item);
                newItem.lastLogin = item.user ? item.user.lastLogin : '';
                newItem.ipAddress = item.user ? item.user.ipAddress : '';
                delete newItem.user;
                return newItem;
            });

            let sortingResult = response.sort(function (a, b) {
                return naturalSort(a.name, b.name);
            });


            sortingResult = _.sortBy(sortingResult, function (o) {
                return o.suspend ? 1 : -1;
            });

            sortingResult = _.sortBy(sortingResult, function (o) {
                return o.lock ? 1 : -1;
            });

            sortingResult = _.sortBy(sortingResult, function (o) {
                return o.active ? -1 : 1;
            });


            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: {
                        list: sortingResult,
                        summary: summary
                    }
                }
            );
        });

});

router.get('/v2/list', function (req, res) {

    let page = req.query.page ? Number.parseInt(req.query.page) : 10;
    let limit = req.query.limit ? Number.parseInt(req.query.limit) : 1;

    let name = req.query.name;

    let condition = {'type': {$ne: 'API'}};

    let userInfo = req.userInfo;

    if (req.query.active) {
        condition.active = req.query.active;
    }
    if (req.query.groupId) {
        condition.parentId = req.query.groupId;
    } else {
        if (userInfo.group._id) {
            condition.parentId = userInfo.group._id;
        }
    }


    if (req.query.username) {
        condition.name_lower = req.query.username.toLowerCase();
    }

    if (req.query.type) {
        condition.type = req.query.type;
    }
    console.log(condition)

    AgentGroupModel.paginate(condition, {
        select: 'name name_lower contact phoneNo currency creditLimit createdDate active balance lock suspend active user',
        populate: [{path: 'parentId'}, {path: 'user', select: 'lastLogin ipAddress'}],
        lean: true,
        page: page,
        limit: limit,
        sort: {name: -1}
    }, (err, data) => {
        if (err)
            return res.send({message: "error", results: err, code: 999});

        //transform
        let resultList = _.map(data.docs, function (item) {
            let newItem = _.clone(item);
            newItem.lastLogin = item.user ? item.user.lastLogin : '';
            newItem.ipAddress = item.user ? item.user.ipAddress : '';
            delete newItem.user;
            return newItem;
        });

        // let sortingResult = resultList.sort(function (a, b) {
        //     return naturalSort(a.name, b.name);
        // });


        let sortingResult = _.sortBy(resultList, function (o) {
            return o.suspend ? 1 : -1;
        });

        sortingResult = _.sortBy(sortingResult, function (o) {
            return o.lock ? 1 : -1;
        });

        sortingResult = _.sortBy(sortingResult, function (o) {
            return o.active ? -1 : 1;
        });


        let summary = _.reduce(sortingResult, function (memo, num) {
            return {
                balance: memo.balance + num.balance,
                creditLimit: memo.creditLimit + num.creditLimit
            }
        }, {balance: 0, creditLimit: 0});

        return res.send(
            {
                code: 0,
                message: "success",
                result: {
                    list: sortingResult,
                    summary: summary,
                    total: data.total,
                    limit: data.limit,
                    page: data.page,
                    pages: data.pages,
                }
            }
        );
    });

});

router.get('/pt/list', function (req, res) {


    let userInfo = req.userInfo;
    let requestQuery = req.query;


    AgentService.findById(requestQuery.groupId, (err, agentInfo) => {

        if (err) {
            return res.send({message: "error", results: err, code: 999});
        }


        async.parallel([callback => {


            if (agentInfo.childGroups.length == 0) {
                callback(null, []);
                return;
            }

            let condition = {};


            condition.parentId = agentInfo._id;//{$in: agentInfo.childGroups};

            if (req.query.active) {
                condition.active = req.query.active;
            }
            // if (req.query.username) {
            //     condition.name_lower = req.query.username.toLowerCase();
            // }

            // if (req.query.type) {
            //     condition.type = req.query.type;
            // }

            console.log(condition)

            AgentGroupModel.find(condition)
                .select('name name_lower contact type shareSetting active suspend lock')
                .populate({path: 'user', select: 'lastLogin ipAddress'})
                .lean()
                .exec(function (err, response) {

                    if (err)
                        return res.send({message: "error", results: err, code: 999});


                    let sortingResult = _.map(response, (value) => {
                        let newItem = Object.assign({}, value);
                        newItem.m_type = 'AGENT';
                        return newItem;
                    });

                    callback(null, sortingResult);
                });
        }, callback => {


            if (agentInfo.childMembers.length == 0) {
                callback(null, []);
                return;
            }

            let condition = {};
            condition.group = agentInfo._id;//{$in: agentInfo.childMembers};


            if (requestQuery.active) {
                condition.active = req.query.active;
            }


            // if (requestQuery.username) {
            //     condition.name_lower = req.query.username.toLowerCase();
            // }

            if (requestQuery.type) {
                condition.type = req.query.type;
            }

            console.log(condition)

            MemberModel.find(condition)
                .select('username username_lower contact type shareSetting active suspend lock')
                .populate({path: 'user', select: 'lastLogin ipAddress'})
                .lean()
                .exec(function (err, response) {

                    if (err) {
                        callback(err, null);
                    } else {


                        let sortingResult = response.sort(function (a, b) {
                            return naturalSort(a.name, b.name);
                        });

                        sortingResult = _.map(sortingResult, (value) => {
                            let newItem = Object.assign({}, value);
                            newItem.m_type = 'MEMBER';
                            return newItem;
                        });

                        callback(null, sortingResult);
                    }
                });
        }, (callback) => {


            AgentService.getMaxShareUsageByIds(agentInfo.childGroups, (err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            });

        }], (err, asyncResponse) => {

            let agentList = asyncResponse[0];
            let memberList = asyncResponse[1];
            let maximumList = asyncResponse[2];


            //
            // let sortingAgentResult = agentList.sort(function (a, b) {
            //     return naturalSort(b.type, a.type);
            // });

            let sortingAgentResult = agentList.sort(function (a, b) {
                return naturalSort(b.type + a.name, a.type + b.name);
            });


            sortingAgentResult = _.sortBy(sortingAgentResult, function (o) {
                return o.suspend ? 1 : -1;
            });

            sortingAgentResult = _.sortBy(sortingAgentResult, function (o) {
                return o.lock ? 1 : -1;
            });

            sortingAgentResult = _.sortBy(sortingAgentResult, function (o) {
                return o.active ? -1 : 1;
            });


            let userList = sortingAgentResult.concat(memberList);

            let prepareResult = _.map(userList, item => {

                let max = _.filter(maximumList, maxItem => {
                    return item._id.toString() === maxItem.groupId.toString()
                })[0];

                if (max) {
                    item.max = max;
                    return item;
                } else {
                    item.max = {
                        today: 0,
                        live: 0,
                        parlay: 0,
                        step: 0,
                        sexy: 0,
                        lotus: 0,
                        sa: 0,
                        dg: 0,
                        ag: 0,
                        pt:0,
                        slotXO: 0,
                        ambGame: 0,
                        ambLotto: 0,
                        m2: 0
                    }
                }
                return item;
            });

            // let mode = agentInfo.

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: {
                        list: prepareResult,
                        agentInfo: agentInfo,
                        mode: ''
                    }
                }
            );
        });

    });


});

router.get('/pt/log', function (req, res) {

    let userInfo = req.userInfo;
    let requestQuery = req.query;

    let condition = {id: requestQuery.id};

    if (requestQuery.type === 'AGENT') {

        AgentGroupHistoryModel.find(condition).populate({
            'path': 'createdBy',
            'select': 'username_lower',
            populate: {
                'path': 'group',
                'select': 'name_lower type',
            }
        }).select('shareSetting createdDate')
            .sort({'createdDate': -1}).limit(10).exec((err, response) => {

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: {
                        list: response
                    }
                }
            );
        });

    } else {

        MemberHistoryModel.find(condition).populate({
            'path': 'createdBy',
            'select': 'username_lower',
            populate: {
                'path': 'group',
                'select': 'name_lower type',
            }
        }).select('shareSetting createdDate')
            .sort({'createdDate': -1})
            .limit(10).exec((err, response) => {

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: {
                        list: response
                    }
                }
            );
        });
    }


});

const updateAgentPTSchema = Joi.object().keys({
    id: Joi.string().required(),
    type: Joi.string().required(),
    source: Joi.string().required(),
    parent: Joi.number().required(),
    own: Joi.number().required(),
    min: Joi.number().required(),
    remaining: Joi.number().required(),
});

const updateMemberPTSchema = Joi.object().keys({
    id: Joi.string().required(),
    type: Joi.string().required(),
    source: Joi.string().required(),
    parent: Joi.number().required(),
});

router.put('/pt/update', function (req, res) {

    let requestBody = req.body;
    let userInfo = req.userInfo;

    if (requestBody.type === 'AGENT') {
        let validateAgent = Joi.validate(req.body, updateAgentPTSchema);
        if (validateAgent.error) {
            return res.send({
                message: "validation fail",
                results: validateAgent.error.details,
                code: 999
            });
        }
    }

    if (requestBody.type === 'MEMBER') {
        let validateMember = Joi.validate(req.body, updateMemberPTSchema);
        if (validateMember.error) {
            return res.send({
                message: "validation fail",
                results: validateMember.error.details,
                code: 999
            });
        }
    }

    if (requestBody.type === 'AGENT') {


        async.parallel([callback => {

            AgentGroupModel.findById(requestBody.id, (err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            }).populate('parentId');

        }, callback => {

            AgentService.getMaxShareUsage(requestBody.id, (err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response)
                }
            });
        }], (err, asyncResponse) => {

            let currentAgentInfo = asyncResponse[0].parentId;

            let modifyAgentInfo = asyncResponse[0];

            let maxShareParent = asyncResponse[1];

            let updateBody = {};

            let isModify = true;

            if (requestBody.source === 'SPORT') {

                if ((requestBody.own + requestBody.parent ) < currentAgentInfo.shareSetting.sportsBook.today.min) {
                    return res.send({
                        message: "Agent Share (Sport Today) + Agent Max Share (Sport Today) should be greater than Agent Min Shares (Sport Today)",
                        result: err,
                        code: 4003
                    });
                }

                if (requestBody.own < maxShareParent.today) {
                    return res.send({
                        message: "Agent Max Share(Sport Today) should not be less than " + maxShareParent.today + "%.",
                        result: {max: maxShareParent.today},
                        code: 4020
                    });
                }

                if ((modifyAgentInfo.shareSetting.sportsBook.today.parent == requestBody.parent) && (modifyAgentInfo.shareSetting.sportsBook.today.own == requestBody.own)
                    && (modifyAgentInfo.shareSetting.sportsBook.today.remaining == requestBody.remaining) && (modifyAgentInfo.shareSetting.sportsBook.today.min == requestBody.min)) {

                    isModify = false;
                }

                updateBody = {
                    $set: {
                        'shareSetting.sportsBook.today.parent': requestBody.parent,
                        'shareSetting.sportsBook.today.own': requestBody.own,
                        'shareSetting.sportsBook.today.remaining': requestBody.remaining,
                        'shareSetting.sportsBook.today.min': requestBody.min
                    }
                };

            } else if (requestBody.source === 'SPORT_LIVE') {

                if ((requestBody.own + requestBody.parent ) < currentAgentInfo.shareSetting.sportsBook.live.min) {
                    return res.send({
                        message: "Agent Share (Sport Live) + Agent Max Share (Sport Live) should be greater than Agent Min Shares (Sport Live)",
                        result: err,
                        code: 4004
                    });
                }

                if (requestBody.own < maxShareParent.live) {
                    return res.send({
                        message: "Agent Max Share(Sport Live) should not be less than " + maxShareParent.live + "%.",
                        result: {max: maxShareParent.live},
                        code: 4021
                    });
                }

                if ((modifyAgentInfo.shareSetting.sportsBook.live.parent == requestBody.parent) && (modifyAgentInfo.shareSetting.sportsBook.live.own == requestBody.own)
                    && (modifyAgentInfo.shareSetting.sportsBook.live.remaining == requestBody.remaining) && (modifyAgentInfo.shareSetting.sportsBook.live.min == requestBody.min)) {

                    isModify = false;
                }

                updateBody = {
                    $set: {
                        'shareSetting.sportsBook.live.parent': requestBody.parent,
                        'shareSetting.sportsBook.live.own': requestBody.own,
                        'shareSetting.sportsBook.live.remaining': requestBody.remaining,
                        'shareSetting.sportsBook.live.min': requestBody.min
                    }
                };

            } else if (requestBody.source === 'STEP_PARLAY') {

                if ((requestBody.own + requestBody.parent ) < currentAgentInfo.shareSetting.step.parlay.min) {
                    return res.send({
                        message: "Agent Share (Step Parlay) + Agent Max Share (Step Parlay) should be greater than Agent Min Shares (Step Parlay)",
                        result: err,
                        code: 4005
                    });
                }

                if (requestBody.own < maxShareParent.parlay) {
                    return res.send({
                        message: "Agent Max Share(Step Parlay) should not be less than " + maxShareParent.parlay + "%.",
                        result: {max: maxShareParent.parlay},
                        code: 4022
                    });
                }

                if ((modifyAgentInfo.shareSetting.step.parlay.parent == requestBody.parent) && (modifyAgentInfo.shareSetting.step.parlay.own == requestBody.own)
                    && (modifyAgentInfo.shareSetting.step.parlay.remaining == requestBody.remaining) && (modifyAgentInfo.shareSetting.step.parlay.min == requestBody.min)) {

                    isModify = false;
                }

                updateBody = {
                    $set: {
                        'shareSetting.step.parlay.parent': requestBody.parent,
                        'shareSetting.step.parlay.own': requestBody.own,
                        'shareSetting.step.parlay.remaining': requestBody.remaining,
                        'shareSetting.step.parlay.min': requestBody.min
                    }
                };

            } else if (requestBody.source === 'STEP_STEP') {

                if ((requestBody.own + requestBody.parent ) < currentAgentInfo.shareSetting.step.step.min) {
                    return res.send({
                        message: "Agent Share (Mix Step) + Agent Max Share (Mix Step) should be greater than Agent Min Shares (Mix Step)",
                        result: err,
                        code: 4008
                    });
                }

                if (requestBody.own < maxShareParent.step) {
                    return res.send({
                        message: "Agent Max Share(Mix Step) should not be less than " + maxShareParent.step + "%.",
                        result: {max: maxShareParent.step},
                        code: 4025
                    });
                }

                if ((modifyAgentInfo.shareSetting.step.step.parent == requestBody.parent) && (modifyAgentInfo.shareSetting.step.step.own == requestBody.own)
                    && (modifyAgentInfo.shareSetting.step.step.remaining == requestBody.remaining) && (modifyAgentInfo.shareSetting.step.step.min == requestBody.min)) {

                    isModify = false;
                }

                updateBody = {
                    $set: {
                        'shareSetting.step.step.parent': requestBody.parent,
                        'shareSetting.step.step.own': requestBody.own,
                        'shareSetting.step.step.remaining': requestBody.remaining,
                        'shareSetting.step.step.min': requestBody.min
                    }
                };

            } else if (requestBody.source === 'SEXY_BACCARAT') {

                if ((requestBody.own + requestBody.parent ) < currentAgentInfo.shareSetting.casino.sexy.min) {
                    return res.send({
                        message: "Agent Share (Sexy Baccarat) + Agent Max Share (Sexy Baccarat) should be greater than Agent Min Shares (Sexy Baccarat)",
                        result: err,
                        code: 4006
                    });
                }

                if (requestBody.own < maxShareParent.sexy) {
                    return res.send({
                        message: "Agent Max Share(Sexy Baccarat) should not be less than " + maxShareParent.sexy + "%.",
                        result: {max: maxShareParent.sexy},
                        code: 4023
                    });
                }

                if ((modifyAgentInfo.shareSetting.casino.sexy.parent == requestBody.parent) && (modifyAgentInfo.shareSetting.casino.sexy.own == requestBody.own)
                    && (modifyAgentInfo.shareSetting.casino.sexy.remaining == requestBody.remaining) && (modifyAgentInfo.shareSetting.casino.sexy.min == requestBody.min)) {

                    isModify = false;
                }

                updateBody = {
                    $set: {
                        'shareSetting.casino.sexy.parent': requestBody.parent,
                        'shareSetting.casino.sexy.own': requestBody.own,
                        'shareSetting.casino.sexy.remaining': requestBody.remaining,
                        'shareSetting.casino.sexy.min': requestBody.min
                    }
                };

            } else if (requestBody.source === 'AG_GAME') {

                if ((requestBody.own + requestBody.parent ) < currentAgentInfo.shareSetting.casino.ag.min) {
                    return res.send({
                        message: "Agent Share (AG Gaming) + Agent Max Share (AG Gaming) should be greater than Agent Min Shares (AG Gaming)",
                        result: err,
                        code: 4013
                    });
                }

                if (requestBody.own < maxShareParent.ag) {
                    return res.send({
                        message: "Agent Max Share(AG Gaming) should not be less than " + maxShareParent.ag + "%.",
                        result: {max: maxShareParent.ag},
                        code: 4043
                    });
                }

                if ((modifyAgentInfo.shareSetting.casino.ag.parent == requestBody.parent) && (modifyAgentInfo.shareSetting.casino.ag.own == requestBody.own)
                    && (modifyAgentInfo.shareSetting.casino.ag.remaining == requestBody.remaining) && (modifyAgentInfo.shareSetting.casino.ag.min == requestBody.min)) {

                    isModify = false;
                }

                updateBody = {
                    $set: {
                        'shareSetting.casino.ag.parent': requestBody.parent,
                        'shareSetting.casino.ag.own': requestBody.own,
                        'shareSetting.casino.ag.remaining': requestBody.remaining,
                        'shareSetting.casino.ag.min': requestBody.min
                    }
                };

            } else if (requestBody.source === 'SA_GAME') {

                if ((requestBody.own + requestBody.parent ) < currentAgentInfo.shareSetting.casino.sa.min) {
                    return res.send({
                        message: "Agent Share (SA Gaming) + Agent Max Share (SA Gaming) should be greater than Agent Min Shares (SA Gaming)",
                        result: err,
                        code: 4011
                    });
                }

                if (requestBody.own < maxShareParent.sa) {
                    return res.send({
                        message: "Agent Max Share(SA Gaming) should not be less than " + maxShareParent.sa + "%.",
                        result: {max: maxShareParent.sa},
                        code: 4041
                    });
                }

                if ((modifyAgentInfo.shareSetting.casino.sa.parent == requestBody.parent) && (modifyAgentInfo.shareSetting.casino.sa.own == requestBody.own)
                    && (modifyAgentInfo.shareSetting.casino.sa.remaining == requestBody.remaining) && (modifyAgentInfo.shareSetting.casino.sa.min == requestBody.min)) {

                    isModify = false;
                }

                updateBody = {
                    $set: {
                        'shareSetting.casino.sa.parent': requestBody.parent,
                        'shareSetting.casino.sa.own': requestBody.own,
                        'shareSetting.casino.sa.remaining': requestBody.remaining,
                        'shareSetting.casino.sa.min': requestBody.min
                    }
                };

            } else if (requestBody.source === 'DREAM_GAMING') {

                if ((requestBody.own + requestBody.parent ) < currentAgentInfo.shareSetting.casino.dg.min) {
                    return res.send({
                        message: "Agent Share (Dream Gaming) + Agent Max Share (Dream Gaming) should be greater than Agent Min Shares (Dream Gaming)",
                        result: err,
                        code: 4012
                    });
                }

                if (requestBody.own < maxShareParent.dg) {
                    return res.send({
                        message: "Agent Max Share(Dream Gaming) should not be less than " + maxShareParent.dg + "%.",
                        result: {max: maxShareParent.dg},
                        code: 4042
                    });
                }

                if ((modifyAgentInfo.shareSetting.casino.dg.parent == requestBody.parent) && (modifyAgentInfo.shareSetting.casino.dg.own == requestBody.own)
                    && (modifyAgentInfo.shareSetting.casino.dg.remaining == requestBody.remaining) && (modifyAgentInfo.shareSetting.casino.dg.min == requestBody.min)) {

                    isModify = false;
                }

                updateBody = {
                    $set: {
                        'shareSetting.casino.dg.parent': requestBody.parent,
                        'shareSetting.casino.dg.own': requestBody.own,
                        'shareSetting.casino.dg.remaining': requestBody.remaining,
                        'shareSetting.casino.dg.min': requestBody.min
                    }
                };

            } else if (requestBody.source === 'PRETTY_GAME') {

                if ((requestBody.own + requestBody.parent ) < currentAgentInfo.shareSetting.casino.pt.min) {
                    return res.send({
                        message: "Agent Share (Pretty Gaming) + Agent Max Share (Pretty Gaming) should be greater than Agent Min Shares (Pretty Gaming)",
                        result: err,
                        code: 4015
                    });
                }

                if (requestBody.own < maxShareParent.pt) {
                    return res.send({
                        message: "Agent Max Share(Pretty Gaming) should not be less than " + maxShareParent.pt + "%.",
                        result: {max: maxShareParent.pt},
                        code: 4045
                    });
                }

                if ((modifyAgentInfo.shareSetting.casino.pt.parent == requestBody.parent) && (modifyAgentInfo.shareSetting.casino.pt.own == requestBody.own)
                    && (modifyAgentInfo.shareSetting.casino.pt.remaining == requestBody.remaining) && (modifyAgentInfo.shareSetting.casino.pt.min == requestBody.min)) {

                    isModify = false;
                }

                updateBody = {
                    $set: {
                        'shareSetting.casino.pt.parent': requestBody.parent,
                        'shareSetting.casino.pt.own': requestBody.own,
                        'shareSetting.casino.pt.remaining': requestBody.remaining,
                        'shareSetting.casino.pt.min': requestBody.min
                    }
                };

            } else if (requestBody.source === 'LIVE22') {

                if ((requestBody.own + requestBody.parent ) < currentAgentInfo.shareSetting.game.slotXO.min) {
                    return res.send({
                        message: "Agent Share (Slot XO) + Agent Max Share (Slot XO) should be greater than Agent Min Shares (Slot XO)",
                        result: err,
                        code: 4009
                    });
                }

                if (requestBody.own < maxShareParent.slotXO) {
                    return res.send({
                        message: "Agent Max Share(Slot XO) should not be less than " + maxShareParent.slotXO + "%.",
                        result: {max: maxShareParent.slotXO},
                        code: 4026
                    });
                }

                if ((modifyAgentInfo.shareSetting.game.slotXO.parent == requestBody.parent) && (modifyAgentInfo.shareSetting.game.slotXO.own == requestBody.own)
                    && (modifyAgentInfo.shareSetting.game.slotXO.remaining == requestBody.remaining) && (modifyAgentInfo.shareSetting.game.slotXO.min == requestBody.min)) {

                    isModify = false;
                }

                updateBody = {
                    $set: {
                        'shareSetting.game.slotXO.parent': requestBody.parent,
                        'shareSetting.game.slotXO.own': requestBody.own,
                        'shareSetting.game.slotXO.remaining': requestBody.remaining,
                        'shareSetting.game.slotXO.min': requestBody.min
                    }
                };
            } else if (requestBody.source === 'AMB_GAME') {

                if ((requestBody.own + requestBody.parent ) < currentAgentInfo.shareSetting.multi.amb.min) {
                    return res.send({
                        message: "Agent Share (AMB Game) + Agent Max Share (AMB Game) should be greater than Agent Min Shares (AMB Game)",
                        result: err,
                        code: 4014
                    });
                }

                if (requestBody.own < maxShareParent.ambGame) {
                    return res.send({
                        message: "Agent Max Share(AMB Game) should not be less than " + maxShareParent.ambGame + "%.",
                        result: {max: maxShareParent.ambGame},
                        code: 4044
                    });
                }

                if ((modifyAgentInfo.shareSetting.multi.amb.parent == requestBody.parent) && (modifyAgentInfo.shareSetting.multi.amb.own == requestBody.own)
                    && (modifyAgentInfo.shareSetting.multi.amb.remaining == requestBody.remaining) && (modifyAgentInfo.shareSetting.multi.amb.min == requestBody.min)) {

                    isModify = false;
                }

                updateBody = {
                    $set: {
                        'shareSetting.multi.amb.parent': requestBody.parent,
                        'shareSetting.multi.amb.own': requestBody.own,
                        'shareSetting.multi.amb.remaining': requestBody.remaining,
                        'shareSetting.multi.amb.min': requestBody.min
                    }
                };

            } else if (requestBody.source === 'AMB_LOTTO') {

                if ((requestBody.own + requestBody.parent ) < currentAgentInfo.shareSetting.lotto.amb.min) {
                    return res.send({
                        message: "Agent Share (Amb Lotto) + Agent Max Share (Amb Lotto) should be greater than Agent Min Shares (Amb Lotto)",
                        result: err,
                        code: 4009
                    });
                }

                if (requestBody.own < maxShareParent.ambLotto) {
                    return res.send({
                        message: "Agent Max Share(Amb Lotto) should not be less than " + maxShareParent.ambLotto + "%.",
                        result: {max: maxShareParent.ambLotto},
                        code: 4026
                    });
                }

                if ((modifyAgentInfo.shareSetting.lotto.amb.parent == requestBody.parent) && (modifyAgentInfo.shareSetting.lotto.amb.own == requestBody.own)
                    && (modifyAgentInfo.shareSetting.lotto.amb.remaining == requestBody.remaining)) {

                    isModify = false;
                }

                updateBody = {
                    $set: {
                        'shareSetting.lotto.amb.parent': requestBody.parent,
                        'shareSetting.lotto.amb.own': requestBody.own,
                        'shareSetting.lotto.amb.remaining': requestBody.remaining
                    }
                };
            } else if (requestBody.source === 'M2') {

                if ((requestBody.own + requestBody.parent ) < currentAgentInfo.shareSetting.other.m2.min) {
                    return res.send({
                        message: "Agent Share (M2 Sports) + Agent Max Share (M2 Sports) should be greater than Agent Min Shares (M2 Sports)",
                        result: err,
                        code: 4007
                    });
                }

                if (requestBody.own < maxShareParent.m2) {
                    return res.send({
                        message: "Agent Max Share(M2 Sports) should not be less than " + maxShareParent.m2 + "%.",
                        result: {max: maxShareParent.m2},
                        code: 4024
                    });
                }

                if ((modifyAgentInfo.shareSetting.other.m2.parent == requestBody.parent) && (modifyAgentInfo.shareSetting.other.m2.own == requestBody.own)
                    && (modifyAgentInfo.shareSetting.other.m2.remaining == requestBody.remaining) && (modifyAgentInfo.shareSetting.other.m2.min == requestBody.min)) {

                    isModify = false;
                }

                updateBody = {
                    $set: {
                        'shareSetting.other.m2.parent': requestBody.parent,
                        'shareSetting.other.m2.own': requestBody.own,
                        'shareSetting.other.m2.remaining': requestBody.remaining,
                        'shareSetting.other.m2.min': requestBody.min
                    }
                };

            }


            // if ((req.body.shareSetting.casino.allbet.own + req.body.shareSetting.casino.allbet.parent ) < currentAgentInfo.shareSetting.casino.allbet.min) {
            //     return res.send({
            //         message: "Agent Share (All Bet) + Agent Max Share (All Bet) should be greater than Agent Min Shares (All Bet)",
            //         result: err,
            //         code: 4008
            //     });
            // }


            // if ((req.body.shareSetting.lotto.amb.own + req.body.shareSetting.lotto.amb.parent ) < currentAgentInfo.shareSetting.lotto.amb.own) {
            //     return res.send({
            //         message: "Agent Share (Amb Lotto) + Agent Max Share (Amb Lotto) should be greater than Agent Min Shares (Amb Lotto)",
            //         result: err,
            //         code: 4010
            //     });
            // }

            // if (req.body.shareSetting.lotto.amb.own < maxShareParent.ambLotto) {
            //     return res.send({
            //         message: "Agent Max Share(Amb Lotto) should not be less than " + maxShareParent.ambLotto + "%.",
            //         result: {max: maxShareParent.ambLotto},
            //         code: 4027
            //     });
            // }


            AgentGroupModel.findOneAndUpdate({_id: mongoose.Types.ObjectId(req.body.id)}, updateBody, {new: true}, (err, response) => {

                if (err) {
                    return res.send({message: "error", result: err, code: 999});
                }


                let logBody = Object.assign(response, {})._doc;
                logBody.id = response._id;
                delete logBody._id;


                if (isModify) {
                    let historyModel = AgentGroupHistoryModel(logBody);
                    historyModel.createdBy = userInfo._id;
                    historyModel.createdDate = DateUtils.getCurrentDate();
                    historyModel.remark = 'PT';
                    historyModel.save((err, historyResponse) => {
                        console.log('log err : ', err);
                    });
                }

                return res.send({
                    code: 0,
                    message: "success",
                    result: 'ok'
                });

            });

        });


    } else if (requestBody.type === 'MEMBER') {

        async.parallel([callback => {

            MemberModel.findById(requestBody.id, (err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            }).populate('group');
        }], (err, asyncResponse) => {

            let currentAgentInfo = asyncResponse[0].group;
            let modifyAgentInfo = asyncResponse[0];

            let updateBody = {};

            let isModify = true;

            if (requestBody.source === 'SPORT') {

                if (requestBody.parent < currentAgentInfo.shareSetting.sportsBook.today.min) {
                    return res.send({
                        message: "Agent Share (Sport Today) + Agent Max Share (Sport Today) should be greater than Agent Min Shares (Sport Today)",
                        result: err,
                        code: 4003
                    });
                }

                if (modifyAgentInfo.shareSetting.sportsBook.today.parent == requestBody.parent) {
                    isModify = false;
                }

                updateBody = {
                    $set: {
                        'shareSetting.sportsBook.today.parent': requestBody.parent,
                    }
                };
            } else if (requestBody.source === 'SPORT_LIVE') {

                if (requestBody.parent < currentAgentInfo.shareSetting.sportsBook.live.min) {
                    return res.send({
                        message: "Agent Share (Sport Live) + Agent Max Share (Sport Live) should be greater than Agent Min Shares (Sport Live)",
                        result: err,
                        code: 4004
                    });
                }

                if (modifyAgentInfo.shareSetting.sportsBook.live.parent == requestBody.parent) {
                    isModify = false;
                }

                updateBody = {
                    $set: {
                        'shareSetting.sportsBook.live.parent': requestBody.parent
                    }
                };
            } else if (requestBody.source === 'STEP_PARLAY') {

                if (requestBody.parent < currentAgentInfo.shareSetting.step.parlay.min) {
                    return res.send({
                        message: "Agent Share (Step Parlay) + Agent Max Share (Step Parlay) should be greater than Agent Min Shares (Step Parlay)",
                        result: err,
                        code: 4005
                    });
                }

                if (modifyAgentInfo.shareSetting.step.parlay.parent == requestBody.parent) {
                    isModify = false;
                }

                updateBody = {
                    $set: {
                        'shareSetting.step.parlay.parent': requestBody.parent
                    }
                };
            } else if (requestBody.source === 'STEP_STEP') {

                if (requestBody.parent < currentAgentInfo.shareSetting.step.step.min) {
                    return res.send({
                        message: "Agent Share (Mix Step) + Agent Max Share (Mix Step) should be greater than Agent Min Shares (Mix Step)",
                        result: err,
                        code: 4008
                    });
                }

                if (modifyAgentInfo.shareSetting.step.step.parent == requestBody.parent) {
                    isModify = false;
                }

                updateBody = {
                    $set: {
                        'shareSetting.step.step.parent': requestBody.parent
                    }
                };
            } else if (requestBody.source === 'SEXY_BACCARAT') {

                if (requestBody.parent < currentAgentInfo.shareSetting.casino.sexy.min) {
                    return res.send({
                        message: "Agent Share (Sexy Baccarat) + Agent Max Share (Sexy Baccarat) should be greater than Agent Min Shares (Sexy Baccarat)",
                        result: err,
                        code: 4006
                    });
                }

                if (modifyAgentInfo.shareSetting.casino.sexy.parent == requestBody.parent) {
                    isModify = false;
                }

                updateBody = {
                    $set: {
                        'shareSetting.casino.sexy.parent': requestBody.parent
                    }
                };
            } else if (requestBody.source === 'AG_GAME') {

                if (requestBody.parent < currentAgentInfo.shareSetting.casino.ag.min) {
                    return res.send({
                        message: "Agent Share (AG Gaming) + Agent Max Share (AG Gaming) should be greater than Agent Min Shares (AG Gaming)",
                        result: err,
                        code: 4013
                    });
                }

                if (modifyAgentInfo.shareSetting.casino.ag.parent == requestBody.parent) {
                    isModify = false;
                }

                updateBody = {
                    $set: {
                        'shareSetting.casino.ag.parent': requestBody.parent
                    }
                };
            } else if (requestBody.source === 'SA_GAME') {

                if (requestBody.parent < currentAgentInfo.shareSetting.casino.sa.min) {
                    return res.send({
                        message: "Agent Share (SA Gaming) + Agent Max Share (SA Gaming) should be greater than Agent Min Shares (SA Gaming)",
                        result: err,
                        code: 4011
                    });
                }

                if (modifyAgentInfo.shareSetting.casino.sa.parent == requestBody.parent) {
                    isModify = false;
                }

                updateBody = {
                    $set: {
                        'shareSetting.casino.sa.parent': requestBody.parent
                    }
                };
            } else if (requestBody.source === 'DREAM_GAMING') {

                if (requestBody.parent < currentAgentInfo.shareSetting.casino.dg.min) {
                    return res.send({
                        message: "Agent Share (Dream Gaming) + Agent Max Share (Dream Gaming) should be greater than Agent Min Shares (Dream Gaming)",
                        result: err,
                        code: 4012
                    });
                }

                if (modifyAgentInfo.shareSetting.casino.dg.parent == requestBody.parent) {
                    isModify = false;
                }

                updateBody = {
                    $set: {
                        'shareSetting.casino.dg.parent': requestBody.parent
                    }
                };
            } else if (requestBody.source === 'PRETTY_GAME') {

                if (requestBody.parent < currentAgentInfo.shareSetting.casino.pt.min) {
                    return res.send({
                        message: "Agent Share (Pretty Gaming) + Agent Max Share (Pretty Gaming) should be greater than Agent Min Shares (Pretty Gaming)",
                        result: err,
                        code: 4015
                    });
                }

                if (modifyAgentInfo.shareSetting.casino.pt.parent == requestBody.parent) {
                    isModify = false;
                }

                updateBody = {
                    $set: {
                        'shareSetting.casino.pt.parent': requestBody.parent
                    }
                };
            } else if (requestBody.source === 'LIVE22') {

                if (requestBody.parent < currentAgentInfo.shareSetting.game.slotXO.min) {
                    return res.send({
                        message: "Agent Share (Slot XO) + Agent Max Share (Slot XO) should be greater than Agent Min Shares (Slot XO)",
                        result: err,
                        code: 4009
                    });
                }

                if (modifyAgentInfo.shareSetting.game.slotXO.parent == requestBody.parent) {
                    isModify = false;
                }

                updateBody = {
                    $set: {
                        'shareSetting.game.slotXO.parent': requestBody.parent
                    }
                };
            } else if (requestBody.source === 'AMB_GAME') {

                if (requestBody.parent < currentAgentInfo.shareSetting.multi.amb.min) {
                    return res.send({
                        message: "Agent Share (AMB Game) + Agent Max Share (AMB Game) should be greater than Agent Min Shares (AMB Game)",
                        result: err,
                        code: 4014
                    });
                }

                if (modifyAgentInfo.shareSetting.multi.amb.parent == requestBody.parent) {
                    isModify = false;
                }

                updateBody = {
                    $set: {
                        'shareSetting.multi.amb.parent': requestBody.parent
                    }
                };
            } else if (requestBody.source === 'AMB_LOTTO') {

                if (requestBody.parent < currentAgentInfo.shareSetting.lotto.amb.min) {
                    return res.send({
                        message: "Agent Share (Amb Lotto) + Agent Max Share (Amb Lotto) should be greater than Agent Min Shares (Amb Lotto)",
                        result: err,
                        code: 4009
                    });
                }

                if (modifyAgentInfo.shareSetting.lotto.amb.parent == requestBody.parent) {
                    isModify = false;
                }

                updateBody = {
                    $set: {
                        'shareSetting.lotto.amb.parent': requestBody.parent
                    }
                };
            } else if (requestBody.source === 'M2') {

                if (requestBody.parent < currentAgentInfo.shareSetting.other.m2.min) {
                    return res.send({
                        message: "Agent Share (M2 Sports) + Agent Max Share (M2 Sports) should be greater than Agent Min Shares (M2 Sports)",
                        result: err,
                        code: 4007
                    });
                }

                if (modifyAgentInfo.shareSetting.other.m2.parent == requestBody.parent) {
                    isModify = false;
                }

                updateBody = {
                    $set: {
                        'shareSetting.other.m2.parent': requestBody.parent
                    }
                };
            }


            // if ((req.body.shareSetting.casino.allbet.own + req.body.shareSetting.casino.allbet.parent ) < currentAgentInfo.shareSetting.casino.allbet.min) {
            //     return res.send({
            //         message: "Agent Share (All Bet) + Agent Max Share (All Bet) should be greater than Agent Min Shares (All Bet)",
            //         result: err,
            //         code: 4008
            //     });
            // }


            // if (requestBody.parent < currentAgentInfo.shareSetting.lotto.amb.own) {
            //     return res.send({
            //         message: "Agent Share (Amb Lotto) + Agent Max Share (Amb Lotto) should be greater than Agent Min Shares (Amb Lotto)",
            //         result: err,
            //         code: 4010
            //     });
            // }

            MemberModel.findOneAndUpdate({_id: mongoose.Types.ObjectId(req.body.id)}, updateBody, {new: true}, (err, response) => {

                if (err) {
                    return res.send({message: "error", result: err, code: 999});
                }

                let logBody = Object.assign(response, {})._doc;
                delete logBody._id;


                if (isModify) {
                    let historyModel = MemberHistoryModel(logBody);
                    historyModel.id = req.body.id;
                    historyModel.createdBy = userInfo._id;
                    historyModel.createdDate = DateUtils.getCurrentDate();
                    historyModel.remark = 'PT';
                    historyModel.save((err, historyResponse) => {
                        console.log('log err : ', err)
                    });
                }

                return res.send({
                    code: 0,
                    message: "success",
                    result: 'ok'
                });

            });
        });
    } else {
        return res.send({message: "invalid type", result: err, code: 999});
    }


});


//TODO: refactor using childMember and Agent
router.get('/downline', function (req, res) {

    let name = req.query.name;

    let condition = {};

    let userInfo = req.userInfo;

    if (req.query.active) {
        condition.active = req.query.active;
    }

    let groupId = req.query.groupId;
    if (groupId) {
        condition.parentId = groupId;
    } else {
        if (userInfo.group._id) {
            groupId = userInfo.group._id;
            condition.parentId = groupId;
        }
    }

    async.series([(callback => {


        AgentGroupModel.findOne({_id: mongoose.Types.ObjectId(groupId)}, (err, response) => {
            if (err) {
                callback(err, null);
            } else {
                if (response) {
                    callback(null, response);
                } else {
                    MemberModel.findOne({_id: mongoose.Types.ObjectId(groupId)}, (err, response) => {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, response);
                        }
                    }).select('group');
                }
            }
        }).select('type parentId');


    }), (callback => {

        let memberCondition = {group: mongoose.Types.ObjectId(groupId)};

        if (req.query.active) {
            memberCondition.active = req.query.active;
        }

        MemberModel.find(memberCondition, (err, response) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, response);
            }
        }).select('username username_lower loginName contact phoneNo currency creditLimit balance balance_tp createdDate active group lock suspend active lastLogin ipAddress');


    })], (err, results) => {


        AgentGroupModel.find(condition)
            .populate({
                path: 'parentId',
                select: 'name name_lower contact phoneNo currency creditLimit balance createdDate lock suspend active'
            })
            .select('name name_lower contact phoneNo currency creditLimit balance createdDate lock suspend active parentId user')
            .populate({path: 'user', select: 'lastLogin ipAddress'})
            .lean()
            .exec(function (err, agentList) {
                if (err)
                    return res.send({message: "error", result: err, code: 999});

                let agent = _.map(agentList, (value) => {
                    let newItem = _.clone(value);
                    newItem.type = 'AGENT';
                    newItem.lastLogin = value.user ? value.user.lastLogin : '';
                    newItem.ipAddress = value.user ? value.user.ipAddress : '';
                    delete newItem.user;
                    return newItem;
                });


                let member = _.map(results[1], (value) => {
                    let newItem = Object.assign({}, value)._doc;
                    newItem.type = 'MEMBER';
                    return newItem;
                });

                let mergeDataList = member.concat(agent);

                mergeDataList = mergeDataList.sort(function (a, b) {
                    let aKey = a.name || a.username;
                    let bKey = b.name || b.username;
                    return naturalSort(aKey, bKey);
                });

                mergeDataList = _.sortBy(mergeDataList, (o) => {
                    return o.type;
                });


                mergeDataList = _.sortBy(mergeDataList, function (o) {
                    return o.suspend ? 1 : -1;
                });

                mergeDataList = _.sortBy(mergeDataList, function (o) {
                    return o.lock ? 1 : -1;
                });


                if (req.query.sortActive) {
                    mergeDataList = _.sortBy(mergeDataList, (o) => {
                        return o.active ? -1 : 1;
                    });
                }

                let summary = _.reduce(mergeDataList, function (memo, num) {
                    return {
                        balance: memo.balance + num.balance,
                        creditLimit: memo.creditLimit + num.creditLimit
                    }
                }, {balance: 0, creditLimit: 0});

                return res.send(
                    {
                        code: 0,
                        message: "success",
                        result: {
                            list: mergeDataList,
                            summary: summary,
                            parentId: results[0].parentId ? results[0].parentId : results[0].group
                        }
                    }
                );
            });
    });

});


router.get('/profile/:groupId', function (req, res) {

    const userInfo = req.userInfo;

    logger.info('get profile');

    async.parallel([callback => {

        AgentGroupModel.findById(req.params.groupId, function (err, response) {
            if (err) {
                callback(err, '');
            } else {
                callback(null, response);
            }
        }).select('creditLimit parentId currency shareSetting limitSetting commissionSetting type').populate({
            path: 'parentId',
            select: 'type'
        }).lean();

    }, callback => {
        let condition = {};
        let group = '';
        if (userInfo.group.type === 'SUPER_ADMIN') {
            group = 'superAdmin';
        } else if (userInfo.group.type === 'COMPANY') {
            group = 'company';
        } else if (userInfo.group.type === 'SHARE_HOLDER') {
            group = 'shareHolder';
        } else if (userInfo.group.type === 'SUPER_SENIOR') {
            group = 'superSenior';
        } else if (userInfo.group.type === 'SENIOR') {
            group = 'senior';
        } else if (userInfo.group.type === 'MASTER_AGENT') {
            group = 'masterAgent';
        } else if (userInfo.group.type === 'AGENT') {
            group = 'agent';
        }

        condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(req.params.groupId);
        condition.status = 'RUNNING';
        condition.game = 'FOOTBALL';
        BetTransactionModel.aggregate([
            {
                $match: condition
            },
            {
                "$group": {
                    "_id": {
                        id: 'key'
                    },
                    "amount": {$sum: '$commission.' + group + '.amount'},
                    "count": {$sum: 1},
                }
            }
        ], function (err, results) {

            if (err) {
                callback(err, '');
                return;
            }

            if (_.isUndefined(results) || !results[0]) {
                callback(null, {amount: 0, count: 0});
            } else {
                callback(null, results[0]);
            }
        });

    }, callback => {


        // AgentGroupModel.findById(req.params.groupId, function (err, agentResponse) {
        //     if (err) {
        //         callback(err, '');
        //     } else {
        //
        //         console.log(agentResponse)
        //
        //         let group = '';
        //         let child = '';
        //         getGroupAndChild(userInfo.group.type, (groupRes, childRes) => {
        //             group = groupRes;
        //             child = childRes;
        //         });
        //
        //         let condition = {};
        //
        //         let startYear = Number.parseInt(agentResponse.lastPaymentDate.getUTCFullYear());
        //         let startMonth = Number.parseInt(agentResponse.lastPaymentDate.getUTCMonth());
        //         let startDay = Number.parseInt(agentResponse.lastPaymentDate.getUTCDate());
        //
        //
        //         if (moment().hours() >= 11) {
        //             condition['gameDate'] = {
        //                 "$gte": new Date(moment([startYear, startMonth, startDay]).add(1, 'd').utc(true).format('YYYY-MM-DDT11:00:00.000')),
        //                 // "$lt": new Date(moment().add(0, 'd').utc(true).format('YYYY-MM-DDT11:00:00.000'))
        //             };
        //         } else {
        //             condition['gameDate'] = {
        //                 "$gte": new Date(moment([startYear, startMonth, startDay]).add(1, 'd').utc(true).format('YYYY-MM-DDT11:00:00.000')),
        //                 // "$lt": new Date(moment().add(0, 'd').utc(true).format('YYYY-MM-DDT11:00:00.000'))
        //             };
        //         }
        //
        //
        //         condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(agentResponse._id);
        //
        //         condition.status = 'DONE';
        //
        //
        //         BetTransactionModel.aggregate([
        //             {
        //                 $match: condition
        //             },
        //             {
        //                 "$group": {
        //                     "_id": {
        //                         commission: '$commission.' + group + '.group',
        //                     },
        //                     "amount": {$sum: '$amount'},
        //                     "validAmount": {$sum: '$validAmount'},
        //                     "totalWinLoseCom": {$sum: '$commission.' + group + '.totalWinLoseCom'},
        //
        //                 }
        //             },
        //             {
        //                 $project: {
        //                     _id: 0,
        //                     group: '$_id.commission',
        //                     type: 'A_GROUP',
        //                     amount: '$amount',
        //                     validAmount: '$validAmount',
        //                     totalWinLoseCom: '$totalWinLoseCom',
        //
        //                 }
        //             }
        //         ], (err, results) => {
        //             if (_.isUndefined(results) || !results[0]) {
        //                 callback(null, 0);
        //             } else {
        //                 callback(null, results[0].totalWinLoseCom);
        //             }
        //         });
        //     }
        //
        // });
        callback(null, 0);

    }], function (err, asyncResponse) {

        if (err) {
            return res.send({message: "error", result: err, code: 999});
        }

        let agentInfo = asyncResponse[0];
        // let childAgentInfo = asyncResponse[1];
        let betInfo = asyncResponse[1];
        let currentWinLose = 0;//asyncResponse[3];

        let result = agentInfo;

        // result.remainingCredit = agentInfo.creditLimit - childAgentInfo.creditLimit;
        // result.memberBalance = childAgentInfo.balance;
        // result.cashBalance = currentWinLose - childAgentInfo.balance;
        result.balance = currentWinLose;
        result.totalOutstanding = betInfo.amount;
        result.totalOutstandingBetCount = betInfo.count;
        return res.send({message: "success", result: result, code: 0});

    });

});


router.get('/m_balance/:groupId', function (req, res) {

    const userInfo = req.userInfo;

    logger.info('get profile');


    async.parallel([callback => {

        AgentGroupModel.findById(req.params.groupId, function (err, response) {
            if (err) {
                callback(err, '');
            } else {
                callback(null, response);
            }
        }).select('creditLimit parentId currency shareSetting limitSetting commissionSetting type').populate({
            path: 'parentId',
            select: 'type'
        }).lean();


    }, callback => {

        if (userInfo.group.type === 'SENIOR' || userInfo.group.type === 'MASTER_AGENT' || userInfo.group.type === 'AGENT') {

            MemberModel.find({group: mongoose.Types.ObjectId(req.params.groupId)})
                .select('name contact currency creditLimit balance createdDate active type lastPaymentDate')
                .lean()
                .exec(function (err, agentList) {

                    if (agentList) {

                        let tasks = [];

                        // _.map(agentList, (item) => {
                        //     tasks.push(getYesterdayMemberPaymentBalanceTask(item._id.toString(), item.lastPaymentDate));
                        // });

                        let creditLimit = _.reduce(agentList, function (memo, num) {
                            return {
                                creditLimit: memo.creditLimit + (num.creditLimit),
                            }
                        }, {creditLimit: 0}).creditLimit;

                        async.parallel(tasks, (err, results) => {
                            if (err) {
                                callback(err, '');
                            } else {

                                let balance = _.reduce(results, function (memo, num) {
                                    return {
                                        balance: memo.balance + (num[0] && num[0].balance || 0),
                                    }
                                }, {balance: 0}).balance;

                                callback(null, {
                                    creditLimit: creditLimit,
                                    balance: balance
                                });
                            }
                        });
                    } else {
                        callback(null, {creditLimit: 0, balance: 0});
                    }
                });
        } else {
            callback(null, {creditLimit: 0, balance: 0});
        }


    }, callback => {

        let condition = {};
        condition.parentId = mongoose.Types.ObjectId(userInfo.group._id);

        AgentGroupModel.find(condition)
            .select('name contact currency creditLimit balance createdDate active type lastPaymentDate')
            .lean()
            .exec(function (err, agentList) {

                let tasks = [];

                _.map(agentList, (item) => {
                    // tasks.push(getYesterdayPaymentBalanceTask(item._id.toString(), item.type, item.lastPaymentDate, userInfo.group.type));
                });

                let creditLimit = _.reduce(agentList, function (memo, num) {
                    return {
                        creditLimit: memo.creditLimit + (num.creditLimit),
                    }
                }, {creditLimit: 0}).creditLimit;

                async.parallel(tasks, (err, results) => {
                    if (err) {
                        callback(err, '');
                    } else {

                        let balance = _.reduce(results, function (memo, num) {
                            return {
                                balance: memo.balance + (num[0] && num[0].balance || 0),
                            }
                        }, {balance: 0}).balance;

                        callback(null, {creditLimit: creditLimit, balance: balance});
                    }
                });
            });


    }], function (err, subChildResponse) {
        let agentInfo = subChildResponse[0];
        let memberAsync = subChildResponse[1];
        let agentAsync = subChildResponse[2];
        //
        // console.log('memberAsync : ', memberAsync)
        // console.log('agentAsync : ', agentAsync)
        let sumInfo = {
            creditLimit: memberAsync.creditLimit + agentAsync.creditLimit,
            balance: memberAsync.balance + agentAsync.balance
        };

        let result = agentInfo;

        result.remainingCredit = agentInfo.creditLimit - sumInfo.creditLimit;
        result.memberBalance = sumInfo.balance;
        result.cashBalance = 0 - sumInfo.balance;

        if (err) {
            return res.send({message: "error", result: err, code: 999});
        }

        return res.send({message: "success", result: result, code: 0});

    });
});


router.get('/setting/:groupId', function (req, res) {

    const userInfo = req.userInfo;

    logger.info('get agent setting');

    async.parallel([callback => {

        AgentGroupModel.findById(req.params.groupId, function (err, response) {
            if (err) {
                callback(err, '');
            } else {
                callback(null, response);
            }
        }).populate({path: 'parentId', select: 'type'}).lean();

    }], function (err, asyncResponse) {

        if (err) {
            return res.send({message: "error", result: err, code: 999});
        }
        let agentInfo = asyncResponse[0];


        return res.send({message: "success", result: agentInfo, code: 0});


    });

});

function generateCertApi() {
    return shortid.generate();
}

const addGroupSchema = Joi.object().keys({
    name: Joi.string().required(),
    contact: Joi.string().allow(null, ''),
    phoneNo: Joi.string().allow(null, ''),
    currency: Joi.string().allow(null, ''),
    password: Joi.string().required(),
    type: Joi.string().required(),
    paymentSetting: Joi.object().allow(),
    parentId: Joi.string().allow(null, ''),
    creditLimit: Joi.number().required(),
    maxCreditLimit: Joi.number().required(),
    // maxMemberCreditLimit: Joi.number().required(),
    active: Joi.boolean().required(),
    shareSetting: Joi.object().allow(),
    limitSetting: Joi.object().allow(),
    commissionSetting: Joi.object().allow(),
    settingEnable: Joi.object().allow(),
});


router.post('/', function (req, res) {
    let validate = Joi.validate(req.body, addGroupSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    const userInfo = req.userInfo;


    UserModel.findOne({_id:userInfo._id},(err,checkAgentResponse)=> {


        if (!checkAgentResponse) {

            return res.send({
                message: "UnAuthorization",
                result: err,
                code: 401
            });


        } else {
            const {name, contact, currency, phoneNo, type, active} = req.body;

            if (type === 'SHARE_HOLDER') {
                if (userInfo.group.type !== 'COMPANY') {
                    return res.send({message: "error", result: 'permission denied.', code: 999});
                }
            } else if (type === 'SENIOR') {
                if (userInfo.group.type !== 'SHARE_HOLDER') {
                    return res.send({message: "error", result: 'permission denied.', code: 999});
                }
            } else if (type === 'API') {
                if (userInfo.group.type !== 'COMPANY') {
                    return res.send({message: "error", result: 'permission denied.', code: 999});
                }
            }


            let body = {
                name: name,
                name_lower: name.toLowerCase(),
                contact: contact,
                phoneNo: phoneNo,
                currency: currency,
                type: type,
                parentId: userInfo.group._id ? userInfo.group._id : null,
                creditLimit: req.body.creditLimit,
                maxCreditLimit: req.body.maxCreditLimit,
                // maxMemberCreditLimit: req.body.maxMemberCreditLimit,
                balance: 0,
                active: active,
                paymentSetting: req.body.paymentSetting,
                lastPaymentDate: moment().add(-1, 'days').utc(true),
                shareSetting: {
                    sportsBook: {
                        today: {
                            parent: req.body.shareSetting.sportsBook.today.parent,
                            own: req.body.shareSetting.sportsBook.today.own,
                            remaining: req.body.shareSetting.sportsBook.today.remaining,
                            min: req.body.shareSetting.sportsBook.today.min
                        },
                        live: {
                            parent: req.body.shareSetting.sportsBook.live.parent,
                            own: req.body.shareSetting.sportsBook.live.own,
                            remaining: req.body.shareSetting.sportsBook.live.remaining,
                            min: req.body.shareSetting.sportsBook.live.min
                        }
                    },
                    step: {
                        parlay: {
                            parent: req.body.shareSetting.step.parlay.parent,
                            own: req.body.shareSetting.step.parlay.own,
                            remaining: req.body.shareSetting.step.parlay.remaining,
                            min: req.body.shareSetting.step.parlay.min
                        },
                        step: {
                            parent: req.body.shareSetting.step.step.parent,
                            own: req.body.shareSetting.step.step.own,
                            remaining: req.body.shareSetting.step.step.remaining,
                            min: req.body.shareSetting.step.step.min
                        }
                    },
                    casino: {
                        sexy: {
                            parent: req.body.shareSetting.casino.sexy.parent,
                            own: req.body.shareSetting.casino.sexy.own,
                            remaining: req.body.shareSetting.casino.sexy.remaining,
                            min: req.body.shareSetting.casino.sexy.min
                        },
                        ag: {
                            parent: req.body.shareSetting.casino.ag.parent,
                            own: req.body.shareSetting.casino.ag.own,
                            remaining: req.body.shareSetting.casino.ag.remaining,
                            min: req.body.shareSetting.casino.ag.min
                        },
                        // allbet: {
                        //     parent: req.body.shareSetting.casino.allbet.parent,
                        //     own: req.body.shareSetting.casino.allbet.own,
                        //     remaining: req.body.shareSetting.casino.allbet.remaining,
                        //     min: req.body.shareSetting.casino.allbet.min
                        // }
                        sa: {
                            parent: req.body.shareSetting.casino.sa.parent,
                            own: req.body.shareSetting.casino.sa.own,
                            remaining: req.body.shareSetting.casino.sa.remaining,
                            min: req.body.shareSetting.casino.sa.min
                        },
                        dg: {
                            parent: req.body.shareSetting.casino.dg.parent,
                            own: req.body.shareSetting.casino.dg.own,
                            remaining: req.body.shareSetting.casino.dg.remaining,
                            min: req.body.shareSetting.casino.dg.min
                        },
                        pt: {
                            parent: req.body.shareSetting.casino.pt.parent,
                            own: req.body.shareSetting.casino.pt.own,
                            remaining: req.body.shareSetting.casino.pt.remaining,
                            min: req.body.shareSetting.casino.pt.min
                        },
                    },
                    game: {
                        slotXO: {
                            parent: req.body.shareSetting.game.slotXO.parent,
                            own: req.body.shareSetting.game.slotXO.own,
                            remaining: req.body.shareSetting.game.slotXO.remaining,
                            min: req.body.shareSetting.game.slotXO.min
                        }
                    },
                    multi: {
                        amb: {
                            parent: req.body.shareSetting.multi.amb.parent,
                            own: req.body.shareSetting.multi.amb.own,
                            remaining: req.body.shareSetting.multi.amb.remaining,
                            min: req.body.shareSetting.multi.amb.min
                        }
                    },
                    lotto: {
                        amb: {
                            parent: req.body.shareSetting.lotto.amb.parent,
                            own: req.body.shareSetting.lotto.amb.own,
                            remaining: req.body.shareSetting.lotto.amb.remaining
                        },
                        pp: {
                            parent: req.body.shareSetting.lotto.pp.parent,
                            own: req.body.shareSetting.lotto.pp.own,
                            remaining: req.body.shareSetting.lotto.pp.remaining
                        },
                        laos: {
                            parent: req.body.shareSetting.lotto.laos.parent,
                            own: req.body.shareSetting.lotto.laos.own,
                            remaining: req.body.shareSetting.lotto.laos.remaining
                        },
                    },
                    other: {
                        m2: {
                            parent: req.body.shareSetting.other.m2.parent,
                            own: req.body.shareSetting.other.m2.own,
                            remaining: req.body.shareSetting.other.m2.remaining,
                            min: req.body.shareSetting.other.m2.min
                        }
                    },
                },
                limitSetting: {
                    sportsBook: {
                        hdpOuOe: {
                            maxPerBet: req.body.limitSetting.sportsBook.hdpOuOe.maxPerBet,
                            maxPerMatch: req.body.limitSetting.sportsBook.hdpOuOe.maxPerMatch
                        },
                        oneTwoDoubleChance: {
                            maxPerBet: req.body.limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet,
                            // maxPerMatch: req.body.limitSetting.sportsBook.oneTwoDoubleChance.maxPerMatch
                        },
                        others: {
                            maxPerBet: req.body.limitSetting.sportsBook.others.maxPerBet,
                            // maxPerMatch: req.body.limitSetting.sportsBook.others.maxPerMatch
                        },
                        // mixParlay: {
                        //     maxPerBet: req.body.limitSetting.sportsBook.mixParlay.maxPerBet,
                        //     maxPerMatch: req.body.limitSetting.sportsBook.mixParlay.maxPerMatch
                        // },
                        outright: {
                            // maxPerBet: req.body.limitSetting.sportsBook.outright.maxPerBet,
                            maxPerMatch: req.body.limitSetting.sportsBook.outright.maxPerMatch
                        },
                        isEnable: req.body.limitSetting.sportsBook.isEnable
                    },
                    step: {
                        parlay: {
                            maxPerBet: req.body.limitSetting.step.parlay.maxPerBet,
                            minPerBet: req.body.limitSetting.step.parlay.minPerBet,
                            maxBetPerDay: req.body.limitSetting.step.parlay.maxBetPerDay,
                            maxPayPerBill: req.body.limitSetting.step.parlay.maxPayPerBill,
                            maxMatchPerBet: req.body.limitSetting.step.parlay.maxMatchPerBet,
                            minMatchPerBet: req.body.limitSetting.step.parlay.minMatchPerBet,
                            isEnable: req.body.limitSetting.step.parlay.isEnable
                        },
                        step: {
                            maxPerBet: req.body.limitSetting.step.step.maxPerBet,
                            minPerBet: req.body.limitSetting.step.step.minPerBet,
                            maxBetPerDay: req.body.limitSetting.step.step.maxBetPerDay,
                            maxPayPerBill: req.body.limitSetting.step.step.maxPayPerBill,
                            maxMatchPerBet: req.body.limitSetting.step.step.maxMatchPerBet,
                            minMatchPerBet: req.body.limitSetting.step.step.minMatchPerBet,
                            isEnable: req.body.limitSetting.step.step.isEnable
                        }
                    },
                    casino: {
                        sexy: {
                            isEnable: req.body.limitSetting.casino.sexy.isEnable,
                            limit: req.body.limitSetting.casino.sexy.limit
                        },
                        ag: {
                            isEnable: req.body.limitSetting.casino.ag.isEnable,
                            limit: req.body.limitSetting.casino.ag.limit
                        },
                        // allbet: {
                        //     isEnable: req.body.limitSetting.casino.allbet.isEnable,
                        //     // limitA: req.body.limitSetting.casino.allbet.limitA,
                        //     // limitB: req.body.limitSetting.casino.allbet.limitB,
                        //     // limitC: req.body.limitSetting.casino.allbet.limitC,
                        //     // limitVip: req.body.limitSetting.casino.allbet.limitVip
                        // },
                        sa: {
                            isEnable: req.body.limitSetting.casino.sa.isEnable,
                            limit: req.body.limitSetting.casino.sa.limit
                        },
                        dg: {
                            isEnable: req.body.limitSetting.casino.dg.isEnable,
                            limit: req.body.limitSetting.casino.dg.limit
                        },
                        pt: {
                            isEnable: req.body.limitSetting.casino.pt.isEnable,
                            limit: req.body.limitSetting.casino.pt.limit
                        },
                    },
                    game: {
                        slotXO: {
                            isEnable: req.body.limitSetting.game.slotXO.isEnable
                        }
                    },
                    multi: {
                        amb: {
                            isEnable: req.body.limitSetting.multi.amb.isEnable
                        }
                    },
                    lotto: {
                        amb: {
                            isEnable: req.body.limitSetting.lotto.amb.isEnable,
                            hour: req.body.limitSetting.lotto.amb.hour,
                            minute: req.body.limitSetting.lotto.amb.minute,
                            _6TOP: {
                                payout: req.body.limitSetting.lotto.amb._6TOP.payout,
                                discount: req.body.limitSetting.lotto.amb._6TOP.discount,
                                max: req.body.limitSetting.lotto.amb._6TOP.max
                            },
                            _5TOP: {
                                payout: req.body.limitSetting.lotto.amb._5TOP.payout,
                                discount: req.body.limitSetting.lotto.amb._5TOP.discount,
                                max: req.body.limitSetting.lotto.amb._5TOP.max
                            },
                            _4TOP: {
                                payout: req.body.limitSetting.lotto.amb._4TOP.payout,
                                discount: req.body.limitSetting.lotto.amb._4TOP.discount,
                                max: req.body.limitSetting.lotto.amb._4TOP.max
                            },
                            _4TOD: {
                                payout: req.body.limitSetting.lotto.amb._4TOD.payout,
                                discount: req.body.limitSetting.lotto.amb._4TOD.discount,
                                max: req.body.limitSetting.lotto.amb._4TOD.max
                            },
                            _3TOP: {
                                payout: req.body.limitSetting.lotto.amb._3TOP.payout,
                                discount: req.body.limitSetting.lotto.amb._3TOP.discount,
                                max: req.body.limitSetting.lotto.amb._3TOP.max
                            },
                            _3TOD: {
                                payout: req.body.limitSetting.lotto.amb._3TOD.payout,
                                discount: req.body.limitSetting.lotto.amb._3TOD.discount,
                                max: req.body.limitSetting.lotto.amb._3TOD.max
                            },
                            _3BOT: {
                                payout: req.body.limitSetting.lotto.amb._3BOT.payout,
                                discount: req.body.limitSetting.lotto.amb._3BOT.discount,
                                max: req.body.limitSetting.lotto.amb._3BOT.max
                            },
                            _3TOP_OE: {
                                payout: req.body.limitSetting.lotto.amb._3TOP_OE.payout,
                                discount: req.body.limitSetting.lotto.amb._3TOP_OE.discount,
                                max: req.body.limitSetting.lotto.amb._3TOP_OE.max
                            },
                            _3TOP_OU: {
                                payout: req.body.limitSetting.lotto.amb._3TOP_OU.payout,
                                discount: req.body.limitSetting.lotto.amb._3TOP_OU.discount,
                                max: req.body.limitSetting.lotto.amb._3TOP_OU.max
                            },
                            _2TOP: {
                                payout: req.body.limitSetting.lotto.amb._2TOP.payout,
                                discount: req.body.limitSetting.lotto.amb._2TOP.discount,
                                max: req.body.limitSetting.lotto.amb._2TOP.max
                            },
                            _2TOD: {
                                payout: req.body.limitSetting.lotto.amb._2TOD.payout,
                                discount: req.body.limitSetting.lotto.amb._2TOD.discount,
                                max: req.body.limitSetting.lotto.amb._2TOD.max
                            },
                            _2BOT: {
                                payout: req.body.limitSetting.lotto.amb._2BOT.payout,
                                discount: req.body.limitSetting.lotto.amb._2BOT.discount,
                                max: req.body.limitSetting.lotto.amb._2BOT.max
                            },
                            _2TOP_OE: {
                                payout: req.body.limitSetting.lotto.amb._2TOP_OE.payout,
                                discount: req.body.limitSetting.lotto.amb._2TOP_OE.discount,
                                max: req.body.limitSetting.lotto.amb._2TOP_OE.max
                            },
                            _2TOP_OU: {
                                payout: req.body.limitSetting.lotto.amb._2TOP_OU.payout,
                                discount: req.body.limitSetting.lotto.amb._2TOP_OU.discount,
                                max: req.body.limitSetting.lotto.amb._2TOP_OU.max
                            },
                            _2BOT_OE: {
                                payout: req.body.limitSetting.lotto.amb._2BOT_OE.payout,
                                discount: req.body.limitSetting.lotto.amb._2BOT_OE.discount,
                                max: req.body.limitSetting.lotto.amb._2BOT_OE.max
                            },
                            _2BOT_OU: {
                                payout: req.body.limitSetting.lotto.amb._2BOT_OU.payout,
                                discount: req.body.limitSetting.lotto.amb._2BOT_OU.discount,
                                max: req.body.limitSetting.lotto.amb._2BOT_OU.max
                            },
                            _1TOP: {
                                payout: req.body.limitSetting.lotto.amb._1TOP.payout,
                                discount: req.body.limitSetting.lotto.amb._1TOP.discount,
                                max: req.body.limitSetting.lotto.amb._1TOP.max
                            },
                            _1BOT: {
                                payout: req.body.limitSetting.lotto.amb._1BOT.payout,
                                discount: req.body.limitSetting.lotto.amb._1BOT.discount,
                                max: req.body.limitSetting.lotto.amb._1BOT.max
                            }
                        },
                        pp: {
                            isEnable: req.body.limitSetting.lotto.pp.isEnable,
                            hour: req.body.limitSetting.lotto.pp.hour,
                            minute: req.body.limitSetting.lotto.pp.minute,
                            _6TOP: {
                                payout: req.body.limitSetting.lotto.pp._6TOP.payout,
                                discount: req.body.limitSetting.lotto.pp._6TOP.discount,
                                max: req.body.limitSetting.lotto.pp._6TOP.max
                            },
                            _5TOP: {
                                payout: req.body.limitSetting.lotto.pp._5TOP.payout,
                                discount: req.body.limitSetting.lotto.pp._5TOP.discount,
                                max: req.body.limitSetting.lotto.pp._5TOP.max
                            },
                            _4TOP: {
                                payout: req.body.limitSetting.lotto.pp._4TOP.payout,
                                discount: req.body.limitSetting.lotto.pp._4TOP.discount,
                                max: req.body.limitSetting.lotto.pp._4TOP.max
                            },
                            _4TOD: {
                                payout: req.body.limitSetting.lotto.pp._4TOD.payout,
                                discount: req.body.limitSetting.lotto.pp._4TOD.discount,
                                max: req.body.limitSetting.lotto.pp._4TOD.max
                            },
                            _3TOP: {
                                payout: req.body.limitSetting.lotto.pp._3TOP.payout,
                                discount: req.body.limitSetting.lotto.pp._3TOP.discount,
                                max: req.body.limitSetting.lotto.pp._3TOP.max
                            },
                            _3TOD: {
                                payout: req.body.limitSetting.lotto.pp._3TOD.payout,
                                discount: req.body.limitSetting.lotto.pp._3TOD.discount,
                                max: req.body.limitSetting.lotto.pp._3TOD.max
                            },
                            _3BOT: {
                                payout: 0,
                                discount: 0,
                                max: 0
                            },
                            _3TOP_OE: {
                                payout: 0,
                                discount: 0,
                                max: 0
                            },
                            _3TOP_OU: {
                                payout: 0,
                                discount: 0,
                                max: 0
                            },
                            _2TOP: {
                                payout: req.body.limitSetting.lotto.pp._2TOP.payout,
                                discount: req.body.limitSetting.lotto.pp._2TOP.discount,
                                max: req.body.limitSetting.lotto.pp._2TOP.max
                            },
                            _2TOD: {
                                payout: req.body.limitSetting.lotto.pp._2TOD.payout,
                                discount: req.body.limitSetting.lotto.pp._2TOD.discount,
                                max: req.body.limitSetting.lotto.pp._2TOD.max
                            },
                            _2BOT: {
                                payout: req.body.limitSetting.lotto.pp._2BOT.payout,
                                discount: req.body.limitSetting.lotto.pp._2BOT.discount,
                                max: req.body.limitSetting.lotto.pp._2BOT.max
                            },
                            _2TOP_OE: {
                                payout: 0,
                                discount: 0,
                                max: 0
                            },
                            _2TOP_OU: {
                                payout: 0,
                                discount: 0,
                                max: 0
                            },
                            _2BOT_OE: {
                                payout: 0,
                                discount: 0,
                                max: 0
                            },
                            _2BOT_OU: {
                                payout: 0,
                                discount: 0,
                                max: 0
                            },
                            _1TOP: {
                                payout: req.body.limitSetting.lotto.pp._1TOP.payout,
                                discount: req.body.limitSetting.lotto.pp._1TOP.discount,
                                max: req.body.limitSetting.lotto.pp._1TOP.max
                            },
                            _1BOT: {
                                payout: req.body.limitSetting.lotto.pp._1BOT.payout,
                                discount: req.body.limitSetting.lotto.pp._1BOT.discount,
                                max: req.body.limitSetting.lotto.pp._1BOT.max
                            }
                        },
                        laos: {
                            isEnable: req.body.limitSetting.lotto.laos.isEnable,
                            hour: req.body.limitSetting.lotto.laos.hour,
                            minute: req.body.limitSetting.lotto.laos.minute,
                            L_4TOP: {
                                payout: req.body.limitSetting.lotto.laos.L_4TOP.payout,
                                discount: req.body.limitSetting.lotto.laos.L_4TOP.discount,
                                max: req.body.limitSetting.lotto.laos.L_4TOP.max
                            },
                            L_4TOD: {
                                payout: req.body.limitSetting.lotto.laos.L_4TOD.payout,
                                discount: req.body.limitSetting.lotto.laos.L_4TOP.discount,
                                max: req.body.limitSetting.lotto.laos.L_4TOP.max
                            },
                            L_3TOP: {
                                payout: req.body.limitSetting.lotto.laos.L_3TOP.payout,
                                discount: req.body.limitSetting.lotto.laos.L_4TOP.discount,
                                max: req.body.limitSetting.lotto.laos.L_4TOP.max
                            },
                            L_3TOD: {
                                payout: req.body.limitSetting.lotto.laos.L_3TOD.payout,
                                discount: req.body.limitSetting.lotto.laos.L_4TOP.discount,
                                max: req.body.limitSetting.lotto.laos.L_4TOP.max
                            },
                            L_2FB: {
                                payout: req.body.limitSetting.lotto.laos.L_2FB.payout,
                                discount: req.body.limitSetting.lotto.laos.L_4TOP.discount,
                                max: req.body.limitSetting.lotto.laos.L_4TOP.max
                            },
                            T_4TOP: {
                                payout: req.body.limitSetting.lotto.laos.T_4TOP.payout,
                                discount: req.body.limitSetting.lotto.laos.T_4TOP.discount,
                                max: req.body.limitSetting.lotto.laos.T_4TOP.max
                            },
                            T_4TOD: {
                                payout: req.body.limitSetting.lotto.laos.T_4TOD.payout,
                                discount: req.body.limitSetting.lotto.laos.T_4TOD.discount,
                                max: req.body.limitSetting.lotto.laos.T_4TOD.max
                            },
                            T_3TOP: {
                                payout: req.body.limitSetting.lotto.laos.T_3TOP.payout,
                                discount: req.body.limitSetting.lotto.laos.T_3TOP.discount,
                                max: req.body.limitSetting.lotto.laos.T_3TOP.max
                            },
                            T_3TOD: {
                                payout: req.body.limitSetting.lotto.laos.T_3TOD.payout,
                                discount: req.body.limitSetting.lotto.laos.T_3TOD.discount,
                                max: req.body.limitSetting.lotto.laos.T_3TOD.max
                            },
                            T_2TOP: {
                                payout: req.body.limitSetting.lotto.laos.T_2TOP.payout,
                                discount: req.body.limitSetting.lotto.laos.T_2TOP.discount,
                                max: req.body.limitSetting.lotto.laos.T_2TOP.max
                            },
                            T_2TOD: {
                                payout: req.body.limitSetting.lotto.laos.T_2TOD.payout,
                                discount: req.body.limitSetting.lotto.laos.T_2TOD.discount,
                                max: req.body.limitSetting.lotto.laos.T_2TOD.max
                            },
                            T_2BOT: {
                                payout: req.body.limitSetting.lotto.laos.T_2BOT.payout,
                                discount: req.body.limitSetting.lotto.laos.T_2BOT.discount,
                                max: req.body.limitSetting.lotto.laos.T_2BOT.max
                            },
                            T_1TOP: {
                                payout: req.body.limitSetting.lotto.laos.T_1TOP.payout,
                                discount: req.body.limitSetting.lotto.laos.T_1TOP.discount,
                                max: req.body.limitSetting.lotto.laos.T_1TOP.max
                            },
                            T_1BOT: {
                                payout: req.body.limitSetting.lotto.laos.T_1BOT.payout,
                                discount: req.body.limitSetting.lotto.laos.T_1BOT.discount,
                                max: req.body.limitSetting.lotto.laos.T_1BOT.max
                            }
                        }
                    },
                    other: {
                        m2: {
                            maxPerBetHDP: req.body.limitSetting.other.m2.maxPerBetHDP,
                            minPerBetHDP: req.body.limitSetting.other.m2.minPerBetHDP,
                            maxBetPerMatchHDP: req.body.limitSetting.other.m2.maxBetPerMatchHDP,

                            maxPerBet: req.body.limitSetting.other.m2.maxPerBet,
                            minPerBet: req.body.limitSetting.other.m2.minPerBet,
                            maxBetPerDay: req.body.limitSetting.other.m2.maxBetPerDay,
                            maxPayPerBill: req.body.limitSetting.other.m2.maxPayPerBill,
                            maxMatchPerBet: req.body.limitSetting.other.m2.maxMatchPerBet,
                            minMatchPerBet: req.body.limitSetting.other.m2.minMatchPerBet,
                            isEnable: req.body.limitSetting.other.m2.isEnable
                        }
                    }
                },
                commissionSetting: {
                    sportsBook: {
                        hdpOuOeA: req.body.commissionSetting.sportsBook.hdpOuOeA,
                        hdpOuOeB: req.body.commissionSetting.sportsBook.hdpOuOeB,
                        hdpOuOeC: req.body.commissionSetting.sportsBook.hdpOuOeC,
                        hdpOuOeD: req.body.commissionSetting.sportsBook.hdpOuOeD,
                        hdpOuOeE: req.body.commissionSetting.sportsBook.hdpOuOeE,
                        hdpOuOeF: req.body.commissionSetting.sportsBook.hdpOuOeF,
                        oneTwoDoubleChance: req.body.commissionSetting.sportsBook.oneTwoDoubleChance,
                        others: req.body.commissionSetting.sportsBook.others
                    },
                    parlay: {
                        com: req.body.commissionSetting.parlay.com
                    },
                    step: {
                        com2: req.body.commissionSetting.step.com2,
                        com3: req.body.commissionSetting.step.com3,
                        com4: req.body.commissionSetting.step.com4,
                        com5: req.body.commissionSetting.step.com5,
                        com6: req.body.commissionSetting.step.com6,
                        com7: req.body.commissionSetting.step.com7,
                        com8: req.body.commissionSetting.step.com8,
                        com9: req.body.commissionSetting.step.com9,
                        com10: req.body.commissionSetting.step.com10,
                        com11: req.body.commissionSetting.step.com11,
                        com12: req.body.commissionSetting.step.com12,
                    },
                    casino: {
                        sexy: req.body.commissionSetting.casino.sexy,
                        ag: req.body.commissionSetting.casino.ag,
                        // allbet: req.body.commissionSetting.casino.allbet,
                        sa: req.body.commissionSetting.casino.sa,
                        dg: req.body.commissionSetting.casino.dg,
                        pt: req.body.commissionSetting.casino.pt
                    },
                    multi: {
                        amb: req.body.commissionSetting.multi.amb,
                    },
                    game: {
                        slotXO: req.body.commissionSetting.game.slotXO,
                    },
                    other: {
                        m2: req.body.commissionSetting.other.m2,
                    }
                }
            };

            if (type === 'API') {
                body.cert = generateCertApi();
            }


            async.series([(callback => {
                AgentGroupModel.findOne({name_lower: name.toLowerCase()}, function (err, response) {
                    if (err)
                        callback(err, null);
                    else
                        callback(null, response);
                });
            }), (callback => {

                AgentGroupModel.findById(userInfo.group._id, function (err, response) {
                    if (err)
                        callback(err, null);
                    else
                        callback(null, response);
                });
            }), (callback => {

                AgentService.getCreditLimitUsage(userInfo.group._id, function (err, response) {
                    if (err) {
                        callback(err, '');
                    } else {
                        callback(null, response);
                    }
                });

            })], (err, results) => {
                if (err) {
                    return res.send({message: "error", result: err, code: 999});
                }

                if (results[0]) {
                    return res.send({
                        message: "group name was already used",
                        result: err,
                        code: 4001
                    });
                }

                let currentAgentInfo = results[1];
                const creditLimitUsage = results[2];

                if (currentAgentInfo.type !== 'COMPANY') {

                    if (req.body.creditLimit > (currentAgentInfo.creditLimit - creditLimitUsage)) {
                        return res.send({message: "creditLimit exceeded", result: err, code: 4002});
                    }

                    if (req.body.creditLimit > currentAgentInfo.maxCreditLimit) {
                        return res.send({
                            message: "max creditLimit limit",
                            result: err,
                            code: 4030
                        });
                    }
                }

                // if (currentAgentInfo.type !== 'COMPANY') {

                if (body.maxCreditLimit > currentAgentInfo.maxCreditLimit) {
                    body.maxCreditLimit = currentAgentInfo.maxCreditLimit
                }
                //
                // if(body.maxMemberCreditLimit > currentAgentInfo.maxMemberCreditLimit){
                //     body.maxMemberCreditLimit = currentAgentInfo.maxMemberCreditLimit
                // }

                //SPORT
                if (body.limitSetting.sportsBook.hdpOuOe.maxPerBet > currentAgentInfo.limitSetting.sportsBook.hdpOuOe.maxPerBet) {
                    body.limitSetting.sportsBook.hdpOuOe.maxPerBet = currentAgentInfo.limitSetting.sportsBook.hdpOuOe.maxPerBet;
                }

                if (body.limitSetting.sportsBook.hdpOuOe.maxPerMatch > currentAgentInfo.limitSetting.sportsBook.hdpOuOe.maxPerMatch) {
                    body.limitSetting.sportsBook.hdpOuOe.maxPerMatch = currentAgentInfo.limitSetting.sportsBook.hdpOuOe.maxPerMatch
                }
                if (body.limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet > currentAgentInfo.limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet) {
                    body.limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet = currentAgentInfo.limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet
                }
                if (body.limitSetting.sportsBook.others.maxPerBet > currentAgentInfo.limitSetting.sportsBook.others.maxPerBet) {
                    body.limitSetting.sportsBook.others.maxPerBet = currentAgentInfo.limitSetting.sportsBook.others.maxPerBet
                }
                // if (body.limitSetting.sportsBook.mixParlay.maxPerBet > currentAgentInfo.limitSetting.sportsBook.mixParlay.maxPerBet) {
                //     body.limitSetting.sportsBook.mixParlay.maxPerBet = currentAgentInfo.limitSetting.sportsBook.mixParlay.maxPerBet
                // }
                // if (body.limitSetting.sportsBook.mixParlay.maxPerMatch > currentAgentInfo.limitSetting.sportsBook.mixParlay.maxPerMatch) {
                //     body.limitSetting.sportsBook.mixParlay.maxPerMatch = currentAgentInfo.limitSetting.sportsBook.mixParlay.maxPerMatch
                // }

                if (body.limitSetting.sportsBook.outright.maxPerMatch > currentAgentInfo.limitSetting.sportsBook.outright.maxPerMatch) {
                    body.limitSetting.sportsBook.outright.maxPerMatch = currentAgentInfo.limitSetting.sportsBook.outright.maxPerMatch
                }

                //PARLAY
                if (body.limitSetting.step.parlay.maxPerBet > currentAgentInfo.limitSetting.step.parlay.maxPerBet) {
                    body.limitSetting.step.parlay.maxPerBet = currentAgentInfo.limitSetting.step.parlay.maxPerBet
                }
                if (body.limitSetting.step.parlay.minPerBet < currentAgentInfo.limitSetting.step.parlay.minPerBet) {
                    body.limitSetting.step.parlay.minPerBet = currentAgentInfo.limitSetting.step.parlay.minPerBet
                }
                if (body.limitSetting.step.parlay.maxBetPerDay > currentAgentInfo.limitSetting.step.parlay.maxBetPerDay) {
                    body.limitSetting.step.parlay.maxBetPerDay = currentAgentInfo.limitSetting.step.parlay.maxBetPerDay
                }
                if (body.limitSetting.step.parlay.maxPayPerBill > currentAgentInfo.limitSetting.step.parlay.maxPayPerBill) {
                    body.limitSetting.step.parlay.maxPayPerBill = currentAgentInfo.limitSetting.step.parlay.maxPayPerBill
                }

                //STEP
                if (body.limitSetting.step.step.maxPerBet > currentAgentInfo.limitSetting.step.step.maxPerBet) {
                    body.limitSetting.step.step.maxPerBet = currentAgentInfo.limitSetting.step.step.maxPerBet
                }
                if (body.limitSetting.step.step.minPerBet < currentAgentInfo.limitSetting.step.step.minPerBet) {
                    body.limitSetting.step.step.minPerBet = currentAgentInfo.limitSetting.step.step.minPerBet
                }
                if (body.limitSetting.step.step.maxBetPerDay > currentAgentInfo.limitSetting.step.step.maxBetPerDay) {
                    body.limitSetting.step.step.maxBetPerDay = currentAgentInfo.limitSetting.step.step.maxBetPerDay
                }
                if (body.limitSetting.step.step.maxPayPerBill > currentAgentInfo.limitSetting.step.step.maxPayPerBill) {
                    body.limitSetting.step.step.maxPayPerBill = currentAgentInfo.limitSetting.step.step.maxPayPerBill
                }


                //TODO
                if (body.limitSetting.lotto.amb._6TOP.max > currentAgentInfo.limitSetting.lotto.amb._6TOP.max) {
                    body.limitSetting.lotto.amb._6TOP.max = currentAgentInfo.limitSetting.lotto.amb._6TOP.max
                }

                if (body.limitSetting.lotto.amb._5TOP.max > currentAgentInfo.limitSetting.lotto.amb._5TOP.max) {
                    body.limitSetting.lotto.amb._5TOP.max = currentAgentInfo.limitSetting.lotto.amb._5TOP.max
                }

                if (body.limitSetting.lotto.amb._4TOP.max > currentAgentInfo.limitSetting.lotto.amb._4TOP.max) {
                    body.limitSetting.lotto.amb._4TOP.max = currentAgentInfo.limitSetting.lotto.amb._4TOP.max
                }

                if (body.limitSetting.lotto.amb._4TOD.max > currentAgentInfo.limitSetting.lotto.amb._4TOD.max) {
                    body.limitSetting.lotto.amb._4TOD.max = currentAgentInfo.limitSetting.lotto.amb._4TOD.max
                }

                if (body.limitSetting.lotto.amb._3TOP.max > currentAgentInfo.limitSetting.lotto.amb._3TOP.max) {
                    body.limitSetting.lotto.amb._3TOP.max = currentAgentInfo.limitSetting.lotto.amb._3TOP.max
                }

                if (body.limitSetting.lotto.amb._3TOD.max > currentAgentInfo.limitSetting.lotto.amb._3TOD.max) {
                    body.limitSetting.lotto.amb._3TOD.max = currentAgentInfo.limitSetting.lotto.amb._3TOD.max
                }

                if (body.limitSetting.lotto.amb._3BOT.max > currentAgentInfo.limitSetting.lotto.amb._3BOT.max) {
                    body.limitSetting.lotto.amb._3BOT.max = currentAgentInfo.limitSetting.lotto.amb._3BOT.max
                }

                if (body.limitSetting.lotto.amb._3TOP_OE.max > currentAgentInfo.limitSetting.lotto.amb._3TOP_OE.max) {
                    body.limitSetting.lotto.amb._3TOP_OE.max = currentAgentInfo.limitSetting.lotto.amb._3TOP_OE.max
                }

                if (body.limitSetting.lotto.amb._3TOP_OU.max > currentAgentInfo.limitSetting.lotto.amb._3TOP_OU.max) {
                    body.limitSetting.lotto.amb._3TOP_OU.max = currentAgentInfo.limitSetting.lotto.amb._3TOP_OU.max
                }

                if (body.limitSetting.lotto.amb._2TOP.max > currentAgentInfo.limitSetting.lotto.amb._2TOP.max) {
                    body.limitSetting.lotto.amb._2TOP.max = currentAgentInfo.limitSetting.lotto.amb._2TOP.max
                }

                if (body.limitSetting.lotto.amb._2TOD.max > currentAgentInfo.limitSetting.lotto.amb._2TOD.max) {
                    body.limitSetting.lotto.amb._2TOD.max = currentAgentInfo.limitSetting.lotto.amb._2TOD.max
                }

                if (body.limitSetting.lotto.amb._2BOT.max > currentAgentInfo.limitSetting.lotto.amb._2BOT.max) {
                    body.limitSetting.lotto.amb._2BOT.max = currentAgentInfo.limitSetting.lotto.amb._2BOT.max
                }

                if (body.limitSetting.lotto.amb._2TOP_OE.max > currentAgentInfo.limitSetting.lotto.amb._2TOP_OE.max) {
                    body.limitSetting.lotto.amb._2TOP_OE.max = currentAgentInfo.limitSetting.lotto.amb._2TOP_OE.max
                }

                if (body.limitSetting.lotto.amb._2TOP_OU.max > currentAgentInfo.limitSetting.lotto.amb._2TOP_OU.max) {
                    body.limitSetting.lotto.amb._2TOP_OU.max = currentAgentInfo.limitSetting.lotto.amb._2TOP_OU.max
                }


                if (body.limitSetting.lotto.amb._2BOT_OE.max > currentAgentInfo.limitSetting.lotto.amb._2BOT_OE.max) {
                    body.limitSetting.lotto.amb._2BOT_OE.max = currentAgentInfo.limitSetting.lotto.amb._2BOT_OE.max
                }

                if (body.limitSetting.lotto.amb._2BOT_OU.max > currentAgentInfo.limitSetting.lotto.amb._2BOT_OU.max) {
                    body.limitSetting.lotto.amb._2BOT_OU.max = currentAgentInfo.limitSetting.lotto.amb._2BOT_OU.max
                }


                if (body.limitSetting.lotto.amb._1TOP.max > currentAgentInfo.limitSetting.lotto.amb._1TOP.max) {
                    body.limitSetting.lotto.amb._1TOP.max = currentAgentInfo.limitSetting.lotto.amb._1TOP.max
                }

                if (body.limitSetting.lotto.amb._1BOT.max > currentAgentInfo.limitSetting.lotto.amb._1BOT.max) {
                    body.limitSetting.lotto.amb._1BOT.max = currentAgentInfo.limitSetting.lotto.amb._1BOT.max
                }


                //PP
                if (body.limitSetting.lotto.pp._6TOP.max > currentAgentInfo.limitSetting.lotto.pp._6TOP.max) {
                    body.limitSetting.lotto.pp._6TOP.max = currentAgentInfo.limitSetting.lotto.pp._6TOP.max
                }

                if (body.limitSetting.lotto.pp._5TOP.max > currentAgentInfo.limitSetting.lotto.pp._5TOP.max) {
                    body.limitSetting.lotto.pp._5TOP.max = currentAgentInfo.limitSetting.lotto.pp._5TOP.max
                }

                if (body.limitSetting.lotto.pp._4TOP.max > currentAgentInfo.limitSetting.lotto.pp._4TOP.max) {
                    body.limitSetting.lotto.pp._4TOP.max = currentAgentInfo.limitSetting.lotto.pp._4TOP.max
                }

                if (body.limitSetting.lotto.pp._4TOD.max > currentAgentInfo.limitSetting.lotto.pp._4TOD.max) {
                    body.limitSetting.lotto.pp._4TOD.max = currentAgentInfo.limitSetting.lotto.pp._4TOD.max
                }

                if (body.limitSetting.lotto.pp._3TOP.max > currentAgentInfo.limitSetting.lotto.pp._3TOP.max) {
                    body.limitSetting.lotto.pp._3TOP.max = currentAgentInfo.limitSetting.lotto.pp._3TOP.max
                }

                if (body.limitSetting.lotto.pp._3TOD.max > currentAgentInfo.limitSetting.lotto.pp._3TOD.max) {
                    body.limitSetting.lotto.pp._3TOD.max = currentAgentInfo.limitSetting.lotto.pp._3TOD.max
                }

                if (body.limitSetting.lotto.pp._2TOP.max > currentAgentInfo.limitSetting.lotto.pp._2TOP.max) {
                    body.limitSetting.lotto.pp._2TOP.max = currentAgentInfo.limitSetting.lotto.pp._2TOP.max
                }

                if (body.limitSetting.lotto.pp._2TOD.max > currentAgentInfo.limitSetting.lotto.pp._2TOD.max) {
                    body.limitSetting.lotto.pp._2TOD.max = currentAgentInfo.limitSetting.lotto.pp._2TOD.max
                }

                if (body.limitSetting.lotto.pp._2BOT.max > currentAgentInfo.limitSetting.lotto.pp._2BOT.max) {
                    body.limitSetting.lotto.pp._2BOT.max = currentAgentInfo.limitSetting.lotto.pp._2BOT.max
                }

                if (body.limitSetting.lotto.pp._1TOP.max > currentAgentInfo.limitSetting.lotto.pp._1TOP.max) {
                    body.limitSetting.lotto.pp._1TOP.max = currentAgentInfo.limitSetting.lotto.pp._1TOP.max
                }

                if (body.limitSetting.lotto.pp._1BOT.max > currentAgentInfo.limitSetting.lotto.pp._1BOT.max) {
                    body.limitSetting.lotto.pp._1BOT.max = currentAgentInfo.limitSetting.lotto.pp._1BOT.max
                }


                //OTHER

                if (body.limitSetting.other.m2.maxPerBetHDP > currentAgentInfo.limitSetting.other.m2.maxPerBetHDP) {
                    body.limitSetting.other.m2.maxPerBetHDP = currentAgentInfo.limitSetting.other.m2.maxPerBetHDP
                }
                if (body.limitSetting.other.m2.minPerBetHDP < currentAgentInfo.limitSetting.other.m2.minPerBetHDP) {
                    body.limitSetting.other.m2.minPerBetHDP = currentAgentInfo.limitSetting.other.m2.minPerBetHDP
                }
                if (body.limitSetting.other.m2.maxBetPerMatchHDP > currentAgentInfo.limitSetting.other.m2.maxBetPerMatchHDP) {
                    body.limitSetting.other.m2.maxBetPerMatchHDP = currentAgentInfo.limitSetting.other.m2.maxBetPerMatchHDP
                }


                if (body.limitSetting.other.m2.maxPerBet > currentAgentInfo.limitSetting.other.m2.maxPerBet) {
                    body.limitSetting.other.m2.maxPerBet = currentAgentInfo.limitSetting.other.m2.maxPerBet
                }
                if (body.limitSetting.other.m2.minPerBet < currentAgentInfo.limitSetting.other.m2.minPerBet) {
                    body.limitSetting.other.m2.minPerBet = currentAgentInfo.limitSetting.other.m2.minPerBet
                }
                if (body.limitSetting.other.m2.maxBetPerDay > currentAgentInfo.limitSetting.other.m2.maxBetPerDay) {
                    body.limitSetting.other.m2.maxBetPerDay = currentAgentInfo.limitSetting.other.m2.maxBetPerDay
                }
                if (body.limitSetting.other.m2.maxPayPerBill > currentAgentInfo.limitSetting.other.m2.maxPayPerBill) {
                    body.limitSetting.other.m2.maxPayPerBill = currentAgentInfo.limitSetting.other.m2.maxPayPerBill
                }


                if (req.body.settingEnable.sportBookEnable) {

                    if ((req.body.shareSetting.sportsBook.today.own + req.body.shareSetting.sportsBook.today.parent ) < currentAgentInfo.shareSetting.sportsBook.today.min) {
                        return res.send({
                            message: "Agent Share (Today) + Agent Max Share (Today) should be greater than Agent Min Shares (Today)",
                            result: err,
                            code: 4003
                        });
                    }

                    if ((req.body.shareSetting.sportsBook.live.own + req.body.shareSetting.sportsBook.live.parent ) < currentAgentInfo.shareSetting.sportsBook.live.min) {
                        return res.send({
                            message: "Agent Share (Live) + Agent Max Share (Live) should be greater than Agent Min Shares (Live)",
                            result: err,
                            code: 4004
                        });
                    }
                }

                if (req.body.settingEnable.stepEnable) {
                    if ((req.body.shareSetting.step.step.own + req.body.shareSetting.step.step.parent ) < currentAgentInfo.shareSetting.step.step.min) {
                        return res.send({
                            message: "Agent Share (Mix Step) + Agent Max Share (Mix Step) should be greater than Agent Min Shares (Mix Step)",
                            result: err,
                            code: 4008
                        });
                    }
                }

                if (req.body.settingEnable.parlayEnable) {
                    if ((req.body.shareSetting.step.parlay.own + req.body.shareSetting.step.parlay.parent ) < currentAgentInfo.shareSetting.step.parlay.min) {
                        return res.send({
                            message: "Agent Share (Step) + Agent Max Share (Step) should be greater than Agent Min Shares (Step)",
                            result: err,
                            code: 4005
                        });
                    }
                }

                if (req.body.settingEnable.casinoSexyEnable) {
                    if ((req.body.shareSetting.casino.sexy.own + req.body.shareSetting.casino.sexy.parent ) < currentAgentInfo.shareSetting.casino.sexy.min) {
                        return res.send({
                            message: "Agent Share (Sexy Baccarat) + Agent Max Share (Sexy Baccarat) should be greater than Agent Min Shares (Sexy Baccarat)",
                            result: err,
                            code: 4006
                        });
                    }
                }


                if (req.body.settingEnable.otherM2Enable) {
                    if ((req.body.shareSetting.other.m2.own + req.body.shareSetting.other.m2.parent ) < currentAgentInfo.shareSetting.other.m2.min) {
                        return res.send({
                            message: "Agent Share (M2 Sport) + Agent Max Share (M2 Sport) should be greater than Agent Min Shares (M2 Sport)",
                            result: err,
                            code: 4007
                        });
                    }
                }


                // if ((req.body.shareSetting.casino.allbet.own + req.body.shareSetting.casino.allbet.parent ) < currentAgentInfo.shareSetting.casino.allbet.min) {
                //     return res.send({
                //         message: "Agent Share (All Bet) + Agent Max Share (All Bet) should be greater than Agent Min Shares (All Bet)",
                //         result: err,
                //         code: 4008
                //     });
                // }

                if (req.body.settingEnable.gameSlotEnable) {
                    if ((req.body.shareSetting.game.slotXO.own + req.body.shareSetting.game.slotXO.parent ) < currentAgentInfo.shareSetting.game.slotXO.min) {
                        return res.send({
                            message: "Agent Share (Slot XO) + Agent Max Share (Slot XO) should be greater than Agent Min Shares (Slot XO)",
                            result: err,
                            code: 4009
                        });
                    }
                }

                if (req.body.settingEnable.casinoSaEnable) {
                    if ((req.body.shareSetting.casino.sa.own + req.body.shareSetting.casino.sa.parent ) < currentAgentInfo.shareSetting.casino.sa.min) {
                        return res.send({
                            message: "Agent Share (SA Gaming) + Agent Max Share (SA Gaming) should be greater than Agent Min Shares (SA Gaming)",
                            result: err,
                            code: 4011
                        });
                    }
                }

                if (req.body.settingEnable.casinoDgEnable) {
                    if ((req.body.shareSetting.casino.dg.own + req.body.shareSetting.casino.dg.parent ) < currentAgentInfo.shareSetting.casino.dg.min) {
                        return res.send({
                            message: "Agent Share (DG Gaming) + Agent Max Share (DG Gaming) should be greater than Agent Min Shares (DG Gaming)",
                            result: err,
                            code: 4012
                        });
                    }
                }

                if (req.body.settingEnable.casinoAgEnable) {
                    if ((req.body.shareSetting.casino.ag.own + req.body.shareSetting.casino.ag.parent ) < currentAgentInfo.shareSetting.casino.ag.min) {
                        return res.send({
                            message: "Agent Share (AG Gaming) + Agent Max Share (AG Gaming) should be greater than Agent Min Shares (AG Gaming)",
                            result: err,
                            code: 4013
                        });
                    }
                }


                if (req.body.settingEnable.gameCardEnable) {
                    if ((req.body.shareSetting.multi.amb.own + req.body.shareSetting.multi.amb.parent ) < currentAgentInfo.shareSetting.multi.amb.min) {
                        return res.send({
                            message: "Agent Share (AMB Game) + Agent Max Share (AMB Game) should be greater than Agent Min Shares (AMB Game)",
                            result: err,
                            code: 4014
                        });
                    }
                }

                if (req.body.settingEnable.casinoPtEnable) {
                    if ((req.body.shareSetting.casino.pt.own + req.body.shareSetting.casino.pt.parent ) < currentAgentInfo.shareSetting.casino.pt.min) {
                        return res.send({
                            message: "Agent Share (PT Gaming) + Agent Max Share (PT Gaming) should be greater than Agent Min Shares (PT Gaming)",
                            result: err,
                            code: 4015
                        });
                    }
                }

                // if ((req.body.shareSetting.lotto.amb.own + req.body.shareSetting.lotto.amb.parent ) < currentAgentInfo.shareSetting.lotto.amb.own) {
                //     return res.send({
                //         message: "Agent Share (AmbLotto) + Agent Max Share (AmbLotto) should be greater than Agent Min Shares (AmbLotto)",
                //         result: err,
                //         code: 4010
                //     });
                // }

                let agentGroup = new AgentGroupModel(body);

                agentGroup.save(function (err, agentResponse) {

                    if (err) {
                        return res.send({message: "error", result: err, code: 999});
                    }

                    async.series([callback => {
                        AgentGroupModel.update(
                            {_id: userInfo.group._id},
                            {$push: {childGroups: agentResponse._id}}, function (err, data) {
                                if (err) {
                                    callback(err, false);
                                } else {
                                    // console.log('update parent child success');
                                    callback(null, true);
                                }
                            });

                    }], function (err, asyncResponse) {

                        if (err) {
                            return res.send({message: "error", results: err, code: 999});
                        }

                        let user = new UserModel({
                            username: agentResponse.name,
                            username_lower: agentResponse.name_lower,
                            contact: contact,
                            currency: 'THB',
                            group: agentResponse._id,
                            permission: 'ALL',
                            active: true,
                        });

                        Hashing.encrypt256(req.body.password, function (err, hashingPass) {
                            user.password = hashingPass;
                        });


                        user.save(function (err, userResponse) {

                            if (err) {
                                return res.send({message: "error", results: err, code: 999});
                            }
                            if (userResponse) {


                                async.parallel([callback => {
                                    newAgentLottoLimit(agentResponse, function (err, lottoLimitId) {
                                        if (err) {
                                            callback(err, null);
                                        } else {
                                            callback(null, lottoLimitId);
                                        }
                                    });
                                }, (callback) => {
                                    newAgentLottoPPLimit(agentResponse, function (err, lottoLimitId) {
                                        if (err) {
                                            callback(err, null);
                                        } else {
                                            callback(null, lottoLimitId);
                                        }
                                    });
                                }, (callback) => {
                                    newAgentLottoLaosLimit(agentResponse, function (err, lottoLimitId) {
                                        if (err) {
                                            callback(err, null);
                                        } else {
                                            callback(null, lottoLimitId);
                                        }
                                    });
                                }], (err, lottoResults) => {

                                    let lottoLimitId = lottoResults[0];
                                    let lottoPPLimitId = lottoResults[1];
                                    let lottoLaosLimitId = lottoResults[2];


                                    AgentGroupModel.update({_id: agentResponse._id},
                                        {
                                            $set: {
                                                user: userResponse._id,
                                                ambLotto: lottoLimitId,
                                                ambLottoPP: lottoPPLimitId,
                                                ambLottoLaos: lottoLaosLimitId,

                                            }
                                        }, function (err, data) {
                                            if (err) {
                                                return res.send({
                                                    message: "error",
                                                    results: err,
                                                    code: 999
                                                });
                                            } else {
                                                return res.send({
                                                    message: "success",
                                                    results: data,
                                                    code: 0
                                                });
                                            }
                                        });
                                });

                            }

                        });

                    });

                });
            });
        }
    });


    function newAgentLottoLimit(agentObj, callback) {
        let body = {
            agent: agentObj._id,
            name: agentObj.name_lower,
            hour: 15,
            min: 20,
            isEnableHotNumber: false,
            po: {
                _1TOP: {
                    limit: 0
                },
                _1BOT: {
                    limit: 0
                },
                _2TOP: {
                    limit: 0
                },
                _2BOT: {
                    limit: 0
                },
                _2TOD: {
                    limit: 0
                },
                _2TOP_OE: {
                    limit: 0
                },
                _2TOP_OU: {
                    limit: 0
                },
                _2BOT_OE: {
                    limit: 0
                },
                _2BOT_OU: {
                    limit: 0
                },
                _3TOP: {
                    limit: 0
                },
                _3BOT: {
                    limit: 0
                },
                _3TOD: {
                    limit: 0
                },
                _3TOP_OE: {
                    limit: 0
                },
                _3TOP_OU: {
                    limit: 0
                },
                _4TOP: {
                    limit: 0
                },
                _4TOD: {
                    limit: 0
                },
                _5TOP: {
                    limit: 0
                },
                _6TOP: {
                    limit: 0
                }
            }
        };


        let model = new AgentLottoLimitModel(body);
        model.save(body, function (err, lottoResponse) {
            if (err) {
                callback(err, null);
            }
            else {
                callback(null, lottoResponse._id)
            }
        });

    }

    function newAgentLottoPPLimit(agentObj, callback) {
        let body = {
            agent: agentObj._id,
            name: agentObj.name_lower,
            hour: 15,
            min: 20,
            isEnableHotNumber: false,
            po: {
                _1TOP: {
                    limit: 0
                },
                _1BOT: {
                    limit: 0
                },
                _2TOP: {
                    limit: 0
                },
                _2BOT: {
                    limit: 0
                },
                _2TOD: {
                    limit: 0
                },
                _2TOP_OE: {
                    limit: 0
                },
                _2TOP_OU: {
                    limit: 0
                },
                _2BOT_OE: {
                    limit: 0
                },
                _2BOT_OU: {
                    limit: 0
                },
                _3TOP: {
                    limit: 0
                },
                _3BOT: {
                    limit: 0
                },
                _3TOD: {
                    limit: 0
                },
                _3TOP_OE: {
                    limit: 0
                },
                _3TOP_OU: {
                    limit: 0
                },
                _4TOP: {
                    limit: 0
                },
                _4TOD: {
                    limit: 0
                },
                _5TOP: {
                    limit: 0
                },
                _6TOP: {
                    limit: 0
                }
            }
        };


        let model = new AgentLottoPPLimitModel(body);
        model.save(body, function (err, lottoResponse) {
            if (err) {
                callback(err, null);
            }
            else {
                callback(null, lottoResponse._id)
            }
        });

    }

    function newAgentLottoLaosLimit(agentObj, callback) {
        let body = {
            agent: agentObj._id,
            name: agentObj.name_lower,
            hour: 15,
            min: 20,
            isEnableHotNumber: false,
            po: {
                L_4TOP: {
                    limit: 0
                },
                L_4TOD: {
                    limit: 0
                },
                L_3TOP: {
                    limit: 0
                },
                L_3TOD: {
                    limit: 0
                },
                L_2FB: {
                    limit: 0
                },
                T_4TOP: {
                    limit: 0
                },
                T_4TOD: {
                    limit: 0
                },
                T_3TOP: {
                    limit: 0
                },
                T_3TOD: {
                    limit: 0
                },
                T_2TOP: {
                    limit: 0
                },
                T_2TOD: {
                    limit: 0
                },
                T_2BOT: {
                    limit: 0
                },
                T_1TOP: {
                    limit: 0
                },
                T_1BOT: {
                    limit: 0
                }
            }
        };


        let model = new AgentLottoLaosLimitModel(body);
        model.save(body, function (err, lottoResponse) {
            if (err) {
                callback(err, null);
            }
            else {
                callback(null, lottoResponse._id)
            }
        });

    }

});


const updateGroupSchema = Joi.object().keys({
    contact: Joi.string().allow(null, ''),
    phoneNo: Joi.string().allow(null, ''),
    password: Joi.string().allow(null, ''),
    creditLimit: Joi.number().required(),
    maxCreditLimit: Joi.number().required(),
    paymentSetting: Joi.object().allow(),
    // maxMemberCreditLimit: Joi.number().required(),
    active: Joi.boolean().required(),
    shareSetting: Joi.object().allow(),
    limitSetting: Joi.object().allow(),
    commissionSetting: Joi.object().allow(),
    settingEnable: Joi.object().allow(),
});


router.put('/:groupId', function (req, res) {

    let validate = Joi.validate(req.body, updateGroupSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    const userInfo = req.userInfo;


    UserModel.findOne({_id:userInfo._id},(err,checkAgentResponse)=> {


        if (!checkAgentResponse) {

            return res.send({
                message: "UnAuthorization",
                result: err,
                code: 401
            });


        } else {

            const {name, contact, phoneNo, type, active} = req.body;


            if (type === 'SHARE_HOLDER') {
                if (userInfo.group.type !== 'COMPANY') {
                    return res.send({message: "error", result: 'permission denied.', code: 999});
                }
            }

            async.series([((callback) => {
                AgentService.getCreditLimitUsage(userInfo.group._id, function (err, response) {
                    if (err) {
                        callback(err, '');
                    } else {
                        callback(null, response ? response : 0);
                    }
                });
            }), (callback => {

                AgentGroupModel.findById(userInfo.group._id, function (err, response) {
                    if (err)
                        callback(err, null);
                    else
                        callback(null, response);
                });
            }), (callback => {

                AgentGroupModel.findById(req.params.groupId, function (err, response) {
                    if (err)
                        callback(err, null);
                    else
                        callback(null, response);
                }).select('_id creditLimit name balance');

            }), (callback => {

                async.series([callback => {

                    AgentGroupModel.aggregate([
                        {
                            $match: {
                                parentId: mongoose.Types.ObjectId(req.params.groupId)
                            }
                        },
                        {
                            "$group": {
                                "_id": {
                                    id: '$parentId'
                                },
                                "today": {$max: {$add: ['$shareSetting.sportsBook.today.parent', '$shareSetting.sportsBook.today.own']}},
                                "live": {$max: {$add: ['$shareSetting.sportsBook.live.parent', '$shareSetting.sportsBook.live.own']}},
                                "parlay": {$max: {$add: ['$shareSetting.step.parlay.parent', '$shareSetting.step.parlay.own']}},
                                "step": {$max: {$add: ['$shareSetting.step.step.parent', '$shareSetting.step.step.own']}},
                                "sexy": {$max: {$add: ['$shareSetting.casino.sexy.parent', '$shareSetting.casino.sexy.own']}},
                                // "lotus": {$max: {$add: ['$shareSetting.casino.lotus.parent', '$shareSetting.casino.lotus.own']}},
                                "sa": {$max: {$add: ['$shareSetting.casino.sa.parent', '$shareSetting.casino.sa.own']}},
                                "dg": {$max: {$add: ['$shareSetting.casino.dg.parent', '$shareSetting.casino.dg.own']}},
                                "ag": {$max: {$add: ['$shareSetting.casino.ag.parent', '$shareSetting.casino.ag.own']}},
                                "pt": {$max: {$add: ['$shareSetting.casino.pt.parent', '$shareSetting.casino.pt.own']}},
                                "ambGame": {$max: {$add: ['$shareSetting.multi.amb.parent', '$shareSetting.multi.amb.own']}},
                                // "allbet": {$max: {$add: ['$shareSetting.casino.allbet.parent', '$shareSetting.casino.allbet.own']}},
                                "slotXO": {$max: {$add: ['$shareSetting.game.slotXO.parent', '$shareSetting.game.slotXO.own']}},
                                "ambLotto": {$max: {$add: ['$shareSetting.lotto.amb.parent', '$shareSetting.lotto.amb.own']}},
                                "ambLottoPP": {$max: {$add: ['$shareSetting.lotto.pp.parent', '$shareSetting.lotto.pp.own']}},
                                "ambLottoLaos": {$max: {$add: ['$shareSetting.lotto.laos.parent', '$shareSetting.lotto.laos.own']}},
                                "m2": {$max: {$add: ['$shareSetting.other.m2.parent', '$shareSetting.other.m2.own']}},
                                "creditLimit": {$sum: '$creditLimit'},
                                "creditLimitMax": {$max: "$creditLimit"}
                            }
                        },
                        {
                            $project: {
                                _id: 0,
                                today: '$today',
                                live: '$live',
                                parlay: '$parlay',
                                step: '$step',
                                sexy: '$sexy',
                                // lotus: '$lotus',
                                sa: '$sa',
                                dg: '$dg',
                                ag: '$ag',
                                pt: '$pt',
                                ambGame: '$ambGame',
                                // allbet: '$allbet',
                                m2: '$m2',
                                slotXO: '$slotXO',
                                ambLotto: '$ambLotto',
                                ambLottoPP: '$ambLottoPP',
                                ambLottoLaos: '$ambLottoLaos',
                                creditLimit: '$creditLimit',
                                creditLimitMax: "$creditLimitMax"
                            }
                        }
                    ], function (err, results) {

                        if (err) {
                            callback(err, '');
                        }
                        if (results[0]) {
                            callback(null, results[0]);
                        } else {
                            callback(null, {
                                today: 0,
                                live: 0,
                                parlay: 0,
                                step: 0,
                                sexy: 0,
                                // lotus: 0,
                                sa: 0,
                                dg: 0,
                                ag: 0,
                                pt: 0,
                                ambGame: 0,
                                m2: 0,
                                // allbet: 0,
                                slotXO: 0,
                                creditLimit: 0,
                                creditLimitMax: 0,
                                ambLotto: 0,
                                ambLottoPP: 0,
                                ambLottoLaos: 0
                            })
                        }
                    });
                }, callback => {


                    MemberModel.aggregate([
                        {
                            $match: {
                                group: mongoose.Types.ObjectId(req.params.groupId)
                            }
                        },
                        {
                            "$group": {
                                "_id": {
                                    id: '$group'
                                },
                                "today": {$max: '$shareSetting.sportsBook.today.parent'},
                                "live": {$max: '$shareSetting.sportsBook.live.parent'},
                                "parlay": {$max: '$shareSetting.step.parlay.parent'},
                                "step": {$max: '$shareSetting.step.step.parent'},
                                "sexy": {$max: '$shareSetting.casino.sexy.parent'},
                                // "lotus": {$max: '$shareSetting.casino.lotus.parent'},
                                "sa": {$max: '$shareSetting.casino.sa.parent'},
                                "dg": {$max: '$shareSetting.casino.dg.parent'},
                                "ag": {$max: '$shareSetting.casino.ag.parent'},
                                "pt": {$max: '$shareSetting.casino.pt.parent'},
                                "ambGame": {$max: '$shareSetting.multi.amb.parent'},
                                // "allbet": {$max: '$shareSetting.casino.allbet.parent'},
                                "slotXO": {$max: '$shareSetting.game.slotXO.parent'},
                                "ambLotto": {$max: '$shareSetting.lotto.amb.parent'},
                                "ambLottoPP": {$max: '$shareSetting.lotto.pp.parent'},
                                "ambLottoLaos": {$max: '$shareSetting.lotto.laos.parent'},
                                "m2": {$max: '$shareSetting.other.m2.parent'},
                                "creditLimit": {$sum: '$creditLimit'},
                                "creditLimitMax": {$max: "$creditLimit"}
                            }
                        },
                        {
                            $project: {
                                _id: 0,
                                // hdpOuOe: '$hdpOuOe',
                                // oneTwoDoubleChance: '$oneTwoDoubleChance',
                                // others: '$others',
                                today: '$today',
                                live: '$live',
                                parlay: '$parlay',
                                step: '$step',
                                sexy: '$sexy',
                                // lotus: '$lotus',
                                sa: '$sa',
                                dg: '$dg',
                                ag: '$ag',
                                pt: '$pt',
                                ambGame: '$ambGame',
                                m2: '$m2',
                                // allbet: '$allbet',
                                slotXO: '$slotXO',
                                ambLotto: '$ambLotto',
                                ambLottoPP: '$ambLottoPP',
                                ambLottoLaos: '$ambLottoLaos',
                                creditLimit: '$creditLimit',
                                creditLimitMax: "$creditLimitMax"
                            }
                        }
                    ], function (err, results) {

                        if (err) {
                            callback(err, '');
                        }
                        if (results[0]) {
                            callback(null, results[0]);
                        } else {
                            callback(null, {
                                // hdpOuOe: 0,
                                // oneTwoDoubleChance: 0,
                                // others: 0,
                                today: 0,
                                live: 0,
                                parlay: 0,
                                step: 0,
                                sexy: 0,
                                // lotus: 0,
                                sa: 0,
                                dg: 0,
                                ag: 0,
                                pt: 0,
                                ambGame: 0,
                                m2: 0,
                                // allbet: 0,
                                slotXO: 0,
                                ambLotto: 0,
                                ambLottoPP: 0,
                                ambLottoLaos: 0,
                                creditLimit: 0,
                                creditLimitMax: 0
                            })
                        }
                    });
                }], function (err, asyncResponse) {

                    if (err) {
                        callback(err, null);
                    } else {

                        if (asyncResponse[1]) {
                            let o = {
                                // hdpOuOe: _.max([asyncResponse[0].hdpOuOe, asyncResponse[1].hdpOuOe]),
                                // oneTwoDoubleChance: _.max([asyncResponse[0].oneTwoDoubleChance, asyncResponse[1].oneTwoDoubleChance]),
                                // others: _.max([asyncResponse[0].others, asyncResponse[1].others]),
                                today: _.max([asyncResponse[0].today, asyncResponse[1].today]),
                                live: _.max([asyncResponse[0].live, asyncResponse[1].live]),
                                parlay: _.max([asyncResponse[0].parlay, asyncResponse[1].parlay]),
                                step: _.max([asyncResponse[0].step, asyncResponse[1].step]),
                                sexy: _.max([asyncResponse[0].sexy, asyncResponse[1].sexy]),
                                // lotus: _.max([asyncResponse[0].lotus, asyncResponse[1].lotus]),
                                sa: _.max([asyncResponse[0].sa, asyncResponse[1].sa]),
                                dg: _.max([asyncResponse[0].dg, asyncResponse[1].dg]),
                                ag: _.max([asyncResponse[0].ag, asyncResponse[1].ag]),
                                pt: _.max([asyncResponse[0].pt, asyncResponse[1].pt]),
                                ambGame: _.max([asyncResponse[0].ambGame, asyncResponse[1].ambGame]),
                                // allbet: _.max([asyncResponse[0].allbet, asyncResponse[1].allbet]),
                                slotXO: _.max([asyncResponse[0].slotXO, asyncResponse[1].slotXO]),
                                ambLotto: _.max([asyncResponse[0].ambLotto, asyncResponse[1].ambLotto]),
                                ambLottoPP: _.max([asyncResponse[0].ambLottoPP, asyncResponse[1].ambLottoPP]),
                                ambLottoLaos: _.max([asyncResponse[0].ambLottoLaos, asyncResponse[1].ambLottoLaos]),
                                m2: _.max([asyncResponse[0].m2, asyncResponse[1].m2]),
                                creditLimit: asyncResponse[0].creditLimit + asyncResponse[1].creditLimit,
                                creditLimitMax: _.max([asyncResponse[0].creditLimitMax, asyncResponse[1].creditLimitMax])
                            };
                            callback(null, o);
                        } else {
                            callback(null, asyncResponse[0]);
                        }
                    }

                })

            })], (err, results) => {

                const creditLimitUsage = results[0];
                const currentAgentInfo = results[1];
                const agentUpdateInfo = results[2];
                const maxShareParent = results[3];


                console.log(req.body.settingEnable)


                if (currentAgentInfo.type !== 'COMPANY') {


                    if (req.body.creditLimit > (currentAgentInfo.creditLimit - (creditLimitUsage - agentUpdateInfo.creditLimit))) {
                        return res.send({message: "creditLimit exceeded", result: err, code: 4002});
                    }

                    if (req.body.creditLimit > currentAgentInfo.maxCreditLimit) {
                        return res.send({
                            message: "max creditLimit limit",
                            result: err,
                            code: 4030
                        });
                    }
                }

                if (req.body.maxCreditLimit < maxShareParent.creditLimitMax) {
                    return res.send({
                        message: "Max CreditLimit should not be less than " + maxShareParent.creditLimitMax,
                        result: {max: maxShareParent.creditLimitMax},
                        code: 4031
                    });
                }


                if (req.body.creditLimit < maxShareParent.creditLimit) {
                    return res.send({
                        message: "Match limit(Credit Limit) has less than its minimum value.",
                        result: err,
                        code: 4028
                    });
                }

                console.log('req.body.creditLimit : ' + req.body.creditLimit + ' : ' + agentUpdateInfo.balance)
                if ((req.body.creditLimit + agentUpdateInfo.balance) < 0) {
                    return res.send({
                        message: "Match limit(Credit Limit) has less than its balance.",
                        result: err,
                        code: 4029
                    });
                }


                let body = {
                    contact: contact,
                    phoneNo: phoneNo,
                    creditLimit: req.body.creditLimit,
                    maxCreditLimit: req.body.maxCreditLimit,
                    // maxMemberCreditLimit: req.body.maxMemberCreditLimit,
                    paymentSetting: req.body.paymentSetting,
                    active: active,
                    shareSetting: {
                        sportsBook: {
                            today: {
                                parent: req.body.shareSetting.sportsBook.today.parent,
                                own: req.body.shareSetting.sportsBook.today.own,
                                remaining: req.body.shareSetting.sportsBook.today.remaining,
                                min: req.body.shareSetting.sportsBook.today.min
                            },
                            live: {
                                parent: req.body.shareSetting.sportsBook.live.parent,
                                own: req.body.shareSetting.sportsBook.live.own,
                                remaining: req.body.shareSetting.sportsBook.live.remaining,
                                min: req.body.shareSetting.sportsBook.live.min
                            }
                        },
                        step: {
                            parlay: {
                                parent: req.body.shareSetting.step.parlay.parent,
                                own: req.body.shareSetting.step.parlay.own,
                                remaining: req.body.shareSetting.step.parlay.remaining,
                                min: req.body.shareSetting.step.parlay.min
                            },
                            step: {
                                parent: req.body.shareSetting.step.step.parent,
                                own: req.body.shareSetting.step.step.own,
                                remaining: req.body.shareSetting.step.step.remaining,
                                min: req.body.shareSetting.step.step.min
                            }
                        },
                        casino: {
                            sexy: {
                                parent: req.body.shareSetting.casino.sexy.parent,
                                own: req.body.shareSetting.casino.sexy.own,
                                remaining: req.body.shareSetting.casino.sexy.remaining,
                                min: req.body.shareSetting.casino.sexy.min
                            },
                            ag: {
                                parent: req.body.shareSetting.casino.ag.parent,
                                own: req.body.shareSetting.casino.ag.own,
                                remaining: req.body.shareSetting.casino.ag.remaining,
                                min: req.body.shareSetting.casino.ag.min
                            },
                            // allbet: {
                            //     parent: req.body.shareSetting.casino.allbet.parent,
                            //     own: req.body.shareSetting.casino.allbet.own,
                            //     remaining: req.body.shareSetting.casino.allbet.remaining,
                            //     min: req.body.shareSetting.casino.allbet.min
                            // }
                            sa: {
                                parent: req.body.shareSetting.casino.sa.parent,
                                own: req.body.shareSetting.casino.sa.own,
                                remaining: req.body.shareSetting.casino.sa.remaining,
                                min: req.body.shareSetting.casino.sa.min
                            },
                            dg: {
                                parent: req.body.shareSetting.casino.dg.parent,
                                own: req.body.shareSetting.casino.dg.own,
                                remaining: req.body.shareSetting.casino.dg.remaining,
                                min: req.body.shareSetting.casino.dg.min
                            },
                            pt: {
                                parent: req.body.shareSetting.casino.pt.parent,
                                own: req.body.shareSetting.casino.pt.own,
                                remaining: req.body.shareSetting.casino.pt.remaining,
                                min: req.body.shareSetting.casino.pt.min
                            },
                        },
                        game: {
                            slotXO: {
                                parent: req.body.shareSetting.game.slotXO.parent,
                                own: req.body.shareSetting.game.slotXO.own,
                                remaining: req.body.shareSetting.game.slotXO.remaining,
                                min: req.body.shareSetting.game.slotXO.min
                            }
                        },
                        multi: {
                            amb: {
                                parent: req.body.shareSetting.multi.amb.parent,
                                own: req.body.shareSetting.multi.amb.own,
                                remaining: req.body.shareSetting.multi.amb.remaining,
                                min: req.body.shareSetting.multi.amb.min
                            }
                        },
                        lotto: {
                            amb: {
                                parent: req.body.shareSetting.lotto.amb.parent,
                                own: req.body.shareSetting.lotto.amb.own,
                                remaining: req.body.shareSetting.lotto.amb.remaining
                            },
                            pp: {
                                parent: req.body.shareSetting.lotto.pp.parent,
                                own: req.body.shareSetting.lotto.pp.own,
                                remaining: req.body.shareSetting.lotto.pp.remaining
                            },
                            laos: {
                                parent: req.body.shareSetting.lotto.laos.parent,
                                own: req.body.shareSetting.lotto.laos.own,
                                remaining: req.body.shareSetting.lotto.laos.remaining
                            },
                        },
                        other: {
                            m2: {
                                parent: req.body.shareSetting.other.m2.parent,
                                own: req.body.shareSetting.other.m2.own,
                                remaining: req.body.shareSetting.other.m2.remaining,
                                min: req.body.shareSetting.other.m2.min
                            },
                        }
                    },
                    limitSetting: {
                        sportsBook: {
                            hdpOuOe: {
                                maxPerBet: req.body.limitSetting.sportsBook.hdpOuOe.maxPerBet,
                                maxPerMatch: req.body.limitSetting.sportsBook.hdpOuOe.maxPerMatch
                            },
                            oneTwoDoubleChance: {
                                maxPerBet: req.body.limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet,
                                // maxPerMatch: req.body.limitSetting.sportsBook.oneTwoDoubleChance.maxPerMatch
                            },
                            others: {
                                maxPerBet: req.body.limitSetting.sportsBook.others.maxPerBet,
                                // maxPerMatch: req.body.limitSetting.sportsBook.others.maxPerMatch
                            },
                            // mixParlay: {
                            //     maxPerBet: req.body.limitSetting.sportsBook.mixParlay.maxPerBet,
                            //     maxPerMatch: req.body.limitSetting.sportsBook.mixParlay.maxPerMatch
                            // },
                            outright: {
                                // maxPerBet: req.body.limitSetting.sportsBook.outright.maxPerBet,
                                maxPerMatch: req.body.limitSetting.sportsBook.outright.maxPerMatch
                            },
                            isEnable: req.body.limitSetting.sportsBook.isEnable
                        },
                        step: {
                            parlay: {
                                maxPerBet: req.body.limitSetting.step.parlay.maxPerBet,
                                minPerBet: req.body.limitSetting.step.parlay.minPerBet,
                                maxBetPerDay: req.body.limitSetting.step.parlay.maxBetPerDay,
                                maxPayPerBill: req.body.limitSetting.step.parlay.maxPayPerBill,
                                maxMatchPerBet: req.body.limitSetting.step.parlay.maxMatchPerBet,
                                minMatchPerBet: req.body.limitSetting.step.parlay.minMatchPerBet,
                                isEnable: req.body.limitSetting.step.parlay.isEnable
                            },
                            step: {
                                maxPerBet: req.body.limitSetting.step.step.maxPerBet,
                                minPerBet: req.body.limitSetting.step.step.minPerBet,
                                maxBetPerDay: req.body.limitSetting.step.step.maxBetPerDay,
                                maxPayPerBill: req.body.limitSetting.step.step.maxPayPerBill,
                                maxMatchPerBet: req.body.limitSetting.step.step.maxMatchPerBet,
                                minMatchPerBet: req.body.limitSetting.step.step.minMatchPerBet,
                                isEnable: req.body.limitSetting.step.step.isEnable
                            }
                        },
                        casino: {
                            sexy: {
                                isEnable: req.body.limitSetting.casino.sexy.isEnable,
                                limit: req.body.limitSetting.casino.sexy.limit
                            },
                            ag: {
                                isEnable: req.body.limitSetting.casino.ag.isEnable,
                                limit: req.body.limitSetting.casino.ag.limit
                            },
                            // allbet: {
                            //     isEnable: req.body.limitSetting.casino.allbet.isEnable,
                            //     // limitA: req.body.limitSetting.casino.allbet.limitA,
                            //     // limitB: req.body.limitSetting.casino.allbet.limitB,
                            //     // limitC: req.body.limitSetting.casino.allbet.limitC,
                            //     // limitVip: req.body.limitSetting.casino.allbet.limitVip
                            // },
                            sa: {
                                isEnable: req.body.limitSetting.casino.sa.isEnable,
                                limit: req.body.limitSetting.casino.sa.limit
                            },
                            dg: {
                                isEnable: req.body.limitSetting.casino.dg.isEnable,
                                limit: req.body.limitSetting.casino.dg.limit
                            },
                            pt: {
                                isEnable: req.body.limitSetting.casino.pt.isEnable,
                                limit: req.body.limitSetting.casino.pt.limit
                            },
                        },
                        game: {
                            slotXO: {
                                isEnable: req.body.limitSetting.game.slotXO.isEnable
                            }
                        },
                        multi: {
                            amb: {
                                isEnable: req.body.limitSetting.multi.amb.isEnable
                            }
                        },
                        lotto: {
                            amb: {
                                isEnable: req.body.limitSetting.lotto.amb.isEnable,
                                hour: req.body.limitSetting.lotto.amb.hour,
                                minute: req.body.limitSetting.lotto.amb.minute,
                                _6TOP: {
                                    payout: req.body.limitSetting.lotto.amb._6TOP.payout,
                                    discount: req.body.limitSetting.lotto.amb._6TOP.discount,
                                    max: req.body.limitSetting.lotto.amb._6TOP.max
                                },
                                _5TOP: {
                                    payout: req.body.limitSetting.lotto.amb._5TOP.payout,
                                    discount: req.body.limitSetting.lotto.amb._5TOP.discount,
                                    max: req.body.limitSetting.lotto.amb._5TOP.max
                                },
                                _4TOP: {
                                    payout: req.body.limitSetting.lotto.amb._4TOP.payout,
                                    discount: req.body.limitSetting.lotto.amb._4TOP.discount,
                                    max: req.body.limitSetting.lotto.amb._4TOP.max
                                },
                                _4TOD: {
                                    payout: req.body.limitSetting.lotto.amb._4TOD.payout,
                                    discount: req.body.limitSetting.lotto.amb._4TOD.discount,
                                    max: req.body.limitSetting.lotto.amb._4TOD.max
                                },
                                _3TOP: {
                                    payout: req.body.limitSetting.lotto.amb._3TOP.payout,
                                    discount: req.body.limitSetting.lotto.amb._3TOP.discount,
                                    max: req.body.limitSetting.lotto.amb._3TOP.max
                                },
                                _3TOD: {
                                    payout: req.body.limitSetting.lotto.amb._3TOD.payout,
                                    discount: req.body.limitSetting.lotto.amb._3TOD.discount,
                                    max: req.body.limitSetting.lotto.amb._3TOD.max
                                },
                                _3BOT: {
                                    payout: req.body.limitSetting.lotto.amb._3BOT.payout,
                                    discount: req.body.limitSetting.lotto.amb._3BOT.discount,
                                    max: req.body.limitSetting.lotto.amb._3BOT.max
                                },
                                _3TOP_OE: {
                                    payout: req.body.limitSetting.lotto.amb._3TOP_OE.payout,
                                    discount: req.body.limitSetting.lotto.amb._3TOP_OE.discount,
                                    max: req.body.limitSetting.lotto.amb._3TOP_OE.max
                                },
                                _3TOP_OU: {
                                    payout: req.body.limitSetting.lotto.amb._3TOP_OU.payout,
                                    discount: req.body.limitSetting.lotto.amb._3TOP_OU.discount,
                                    max: req.body.limitSetting.lotto.amb._3TOP_OU.max
                                },
                                _2TOP: {
                                    payout: req.body.limitSetting.lotto.amb._2TOP.payout,
                                    discount: req.body.limitSetting.lotto.amb._2TOP.discount,
                                    max: req.body.limitSetting.lotto.amb._2TOP.max
                                },
                                _2TOD: {
                                    payout: req.body.limitSetting.lotto.amb._2TOD.payout,
                                    discount: req.body.limitSetting.lotto.amb._2TOD.discount,
                                    max: req.body.limitSetting.lotto.amb._2TOD.max
                                },
                                _2BOT: {
                                    payout: req.body.limitSetting.lotto.amb._2BOT.payout,
                                    discount: req.body.limitSetting.lotto.amb._2BOT.discount,
                                    max: req.body.limitSetting.lotto.amb._2BOT.max
                                },
                                _2TOP_OE: {
                                    payout: req.body.limitSetting.lotto.amb._2TOP_OE.payout,
                                    discount: req.body.limitSetting.lotto.amb._2TOP_OE.discount,
                                    max: req.body.limitSetting.lotto.amb._2TOP_OE.max
                                },
                                _2TOP_OU: {
                                    payout: req.body.limitSetting.lotto.amb._2TOP_OU.payout,
                                    discount: req.body.limitSetting.lotto.amb._2TOP_OU.discount,
                                    max: req.body.limitSetting.lotto.amb._2TOP_OU.max
                                },
                                _2BOT_OE: {
                                    payout: req.body.limitSetting.lotto.amb._2BOT_OE.payout,
                                    discount: req.body.limitSetting.lotto.amb._2BOT_OE.discount,
                                    max: req.body.limitSetting.lotto.amb._2BOT_OE.max
                                },
                                _2BOT_OU: {
                                    payout: req.body.limitSetting.lotto.amb._2BOT_OU.payout,
                                    discount: req.body.limitSetting.lotto.amb._2BOT_OU.discount,
                                    max: req.body.limitSetting.lotto.amb._2BOT_OU.max
                                },
                                _1TOP: {
                                    payout: req.body.limitSetting.lotto.amb._1TOP.payout,
                                    discount: req.body.limitSetting.lotto.amb._1TOP.discount,
                                    max: req.body.limitSetting.lotto.amb._1TOP.max
                                },
                                _1BOT: {
                                    payout: req.body.limitSetting.lotto.amb._1BOT.payout,
                                    discount: req.body.limitSetting.lotto.amb._1BOT.discount,
                                    max: req.body.limitSetting.lotto.amb._1BOT.max
                                }
                            },
                            pp: {
                                isEnable: req.body.limitSetting.lotto.pp.isEnable,
                                hour: req.body.limitSetting.lotto.pp.hour,
                                minute: req.body.limitSetting.lotto.pp.minute,
                                _6TOP: {
                                    payout: req.body.limitSetting.lotto.pp._6TOP.payout,
                                    discount: req.body.limitSetting.lotto.pp._6TOP.discount,
                                    max: req.body.limitSetting.lotto.pp._6TOP.max
                                },
                                _5TOP: {
                                    payout: req.body.limitSetting.lotto.pp._5TOP.payout,
                                    discount: req.body.limitSetting.lotto.pp._5TOP.discount,
                                    max: req.body.limitSetting.lotto.pp._5TOP.max
                                },
                                _4TOP: {
                                    payout: req.body.limitSetting.lotto.pp._4TOP.payout,
                                    discount: req.body.limitSetting.lotto.pp._4TOP.discount,
                                    max: req.body.limitSetting.lotto.pp._4TOP.max
                                },
                                _4TOD: {
                                    payout: req.body.limitSetting.lotto.pp._4TOD.payout,
                                    discount: req.body.limitSetting.lotto.pp._4TOD.discount,
                                    max: req.body.limitSetting.lotto.pp._4TOD.max
                                },
                                _3TOP: {
                                    payout: req.body.limitSetting.lotto.pp._3TOP.payout,
                                    discount: req.body.limitSetting.lotto.pp._3TOP.discount,
                                    max: req.body.limitSetting.lotto.pp._3TOP.max
                                },
                                _3TOD: {
                                    payout: req.body.limitSetting.lotto.pp._3TOD.payout,
                                    discount: req.body.limitSetting.lotto.pp._3TOD.discount,
                                    max: req.body.limitSetting.lotto.pp._3TOD.max
                                },
                                _3BOT: {
                                    payout: 0,
                                    discount: 0,
                                    max: 0
                                },
                                _3TOP_OE: {
                                    payout: 0,
                                    discount: 0,
                                    max: 0
                                },
                                _3TOP_OU: {
                                    payout: 0,
                                    discount: 0,
                                    max: 0
                                },
                                _2TOP: {
                                    payout: req.body.limitSetting.lotto.pp._2TOP.payout,
                                    discount: req.body.limitSetting.lotto.pp._2TOP.discount,
                                    max: req.body.limitSetting.lotto.pp._2TOP.max
                                },
                                _2TOD: {
                                    payout: req.body.limitSetting.lotto.pp._2TOD.payout,
                                    discount: req.body.limitSetting.lotto.pp._2TOD.discount,
                                    max: req.body.limitSetting.lotto.pp._2TOD.max
                                },
                                _2BOT: {
                                    payout: req.body.limitSetting.lotto.pp._2BOT.payout,
                                    discount: req.body.limitSetting.lotto.pp._2BOT.discount,
                                    max: req.body.limitSetting.lotto.pp._2BOT.max
                                },
                                _2TOP_OE: {
                                    payout: 0,
                                    discount: 0,
                                    max: 0
                                },
                                _2TOP_OU: {
                                    payout: 0,
                                    discount: 0,
                                    max: 0
                                },
                                _2BOT_OE: {
                                    payout: 0,
                                    discount: 0,
                                    max: 0
                                },
                                _2BOT_OU: {
                                    payout: 0,
                                    discount: 0,
                                    max: 0
                                },
                                _1TOP: {
                                    payout: req.body.limitSetting.lotto.pp._1TOP.payout,
                                    discount: req.body.limitSetting.lotto.pp._1TOP.discount,
                                    max: req.body.limitSetting.lotto.pp._1TOP.max
                                },
                                _1BOT: {
                                    payout: req.body.limitSetting.lotto.pp._1BOT.payout,
                                    discount: req.body.limitSetting.lotto.pp._1BOT.discount,
                                    max: req.body.limitSetting.lotto.pp._1BOT.max
                                }
                            },
                            laos: {
                                isEnable: req.body.limitSetting.lotto.laos.isEnable,
                                hour: req.body.limitSetting.lotto.laos.hour,
                                minute: req.body.limitSetting.lotto.laos.minute,
                                L_4TOP: {
                                    payout: req.body.limitSetting.lotto.laos.L_4TOP.payout,
                                    discount: req.body.limitSetting.lotto.laos.L_4TOP.discount,
                                    max: req.body.limitSetting.lotto.laos.L_4TOP.max
                                },
                                L_4TOD: {
                                    payout: req.body.limitSetting.lotto.laos.L_4TOD.payout,
                                    discount: req.body.limitSetting.lotto.laos.L_4TOP.discount,
                                    max: req.body.limitSetting.lotto.laos.L_4TOP.max
                                },
                                L_3TOP: {
                                    payout: req.body.limitSetting.lotto.laos.L_3TOP.payout,
                                    discount: req.body.limitSetting.lotto.laos.L_4TOP.discount,
                                    max: req.body.limitSetting.lotto.laos.L_4TOP.max
                                },
                                L_3TOD: {
                                    payout: req.body.limitSetting.lotto.laos.L_3TOD.payout,
                                    discount: req.body.limitSetting.lotto.laos.L_4TOP.discount,
                                    max: req.body.limitSetting.lotto.laos.L_4TOP.max
                                },
                                L_2FB: {
                                    payout: req.body.limitSetting.lotto.laos.L_2FB.payout,
                                    discount: req.body.limitSetting.lotto.laos.L_4TOP.discount,
                                    max: req.body.limitSetting.lotto.laos.L_4TOP.max
                                },
                                T_4TOP: {
                                    payout: req.body.limitSetting.lotto.laos.T_4TOP.payout,
                                    discount: req.body.limitSetting.lotto.laos.T_4TOP.discount,
                                    max: req.body.limitSetting.lotto.laos.T_4TOP.max
                                },
                                T_4TOD: {
                                    payout: req.body.limitSetting.lotto.laos.T_4TOD.payout,
                                    discount: req.body.limitSetting.lotto.laos.T_4TOD.discount,
                                    max: req.body.limitSetting.lotto.laos.T_4TOD.max
                                },
                                T_3TOP: {
                                    payout: req.body.limitSetting.lotto.laos.T_3TOP.payout,
                                    discount: req.body.limitSetting.lotto.laos.T_3TOP.discount,
                                    max: req.body.limitSetting.lotto.laos.T_3TOP.max
                                },
                                T_3TOD: {
                                    payout: req.body.limitSetting.lotto.laos.T_3TOD.payout,
                                    discount: req.body.limitSetting.lotto.laos.T_3TOD.discount,
                                    max: req.body.limitSetting.lotto.laos.T_3TOD.max
                                },
                                T_2TOP: {
                                    payout: req.body.limitSetting.lotto.laos.T_2TOP.payout,
                                    discount: req.body.limitSetting.lotto.laos.T_2TOP.discount,
                                    max: req.body.limitSetting.lotto.laos.T_2TOP.max
                                },
                                T_2TOD: {
                                    payout: req.body.limitSetting.lotto.laos.T_2TOD.payout,
                                    discount: req.body.limitSetting.lotto.laos.T_2TOD.discount,
                                    max: req.body.limitSetting.lotto.laos.T_2TOD.max
                                },
                                T_2BOT: {
                                    payout: req.body.limitSetting.lotto.laos.T_2BOT.payout,
                                    discount: req.body.limitSetting.lotto.laos.T_2BOT.discount,
                                    max: req.body.limitSetting.lotto.laos.T_2BOT.max
                                },
                                T_1TOP: {
                                    payout: req.body.limitSetting.lotto.laos.T_1TOP.payout,
                                    discount: req.body.limitSetting.lotto.laos.T_1TOP.discount,
                                    max: req.body.limitSetting.lotto.laos.T_1TOP.max
                                },
                                T_1BOT: {
                                    payout: req.body.limitSetting.lotto.laos.T_1BOT.payout,
                                    discount: req.body.limitSetting.lotto.laos.T_1BOT.discount,
                                    max: req.body.limitSetting.lotto.laos.T_1BOT.max
                                }
                            }
                        },
                        other: {
                            m2: {
                                maxPerBetHDP: req.body.limitSetting.other.m2.maxPerBetHDP,
                                minPerBetHDP: req.body.limitSetting.other.m2.minPerBetHDP,
                                maxBetPerMatchHDP: req.body.limitSetting.other.m2.maxBetPerMatchHDP,

                                maxPerBet: req.body.limitSetting.other.m2.maxPerBet,
                                minPerBet: req.body.limitSetting.other.m2.minPerBet,
                                maxBetPerDay: req.body.limitSetting.other.m2.maxBetPerDay,
                                maxPayPerBill: req.body.limitSetting.other.m2.maxPayPerBill,
                                maxMatchPerBet: req.body.limitSetting.other.m2.maxMatchPerBet,
                                minMatchPerBet: req.body.limitSetting.other.m2.minMatchPerBet,
                                isEnable: req.body.limitSetting.other.m2.isEnable
                            }
                        }
                    },
                    commissionSetting: {
                        sportsBook: {
                            hdpOuOeA: req.body.commissionSetting.sportsBook.hdpOuOeA,
                            hdpOuOeB: req.body.commissionSetting.sportsBook.hdpOuOeB,
                            hdpOuOeC: req.body.commissionSetting.sportsBook.hdpOuOeC,
                            hdpOuOeD: req.body.commissionSetting.sportsBook.hdpOuOeD,
                            hdpOuOeE: req.body.commissionSetting.sportsBook.hdpOuOeE,
                            hdpOuOeF: req.body.commissionSetting.sportsBook.hdpOuOeF,
                            oneTwoDoubleChance: req.body.commissionSetting.sportsBook.oneTwoDoubleChance,
                            others: req.body.commissionSetting.sportsBook.others
                        },
                        parlay: {
                            com: req.body.commissionSetting.parlay.com,
                        },
                        step: {
                            com2: req.body.commissionSetting.step.com2,
                            com3: req.body.commissionSetting.step.com3,
                            com4: req.body.commissionSetting.step.com4,
                            com5: req.body.commissionSetting.step.com5,
                            com6: req.body.commissionSetting.step.com6,
                            com7: req.body.commissionSetting.step.com7,
                            com8: req.body.commissionSetting.step.com8,
                            com9: req.body.commissionSetting.step.com9,
                            com10: req.body.commissionSetting.step.com10,
                            com11: req.body.commissionSetting.step.com11,
                            com12: req.body.commissionSetting.step.com12,
                        },
                        casino: {
                            sexy: req.body.commissionSetting.casino.sexy,
                            ag: req.body.commissionSetting.casino.ag,
                            sa: req.body.commissionSetting.casino.sa,
                            dg: req.body.commissionSetting.casino.dg,
                            pt: req.body.commissionSetting.casino.pt,
                            // allbet: req.body.commissionSetting.casino.allbet,
                        },
                        game: {
                            slotXO: req.body.commissionSetting.game.slotXO,
                        },
                        multi: {
                            amb: req.body.commissionSetting.multi.amb,
                        },
                        other: {
                            m2: req.body.commissionSetting.other.m2,
                        }
                        // lotto: {
                        //     lotto: req.body.commissionSetting.lotto.lotto
                        // }
                    },
                };

                if (body.maxCreditLimit > currentAgentInfo.maxCreditLimit) {
                    body.maxCreditLimit = currentAgentInfo.maxCreditLimit
                }

                // if(body.maxMemberCreditLimit > currentAgentInfo.maxMemberCreditLimit){
                //     body.maxMemberCreditLimit = currentAgentInfo.maxMemberCreditLimit
                // }

                // if (currentAgentInfo.type !== 'COMPANY') {

                if (body.limitSetting.sportsBook.hdpOuOe.maxPerBet > currentAgentInfo.limitSetting.sportsBook.hdpOuOe.maxPerBet) {
                    body.limitSetting.sportsBook.hdpOuOe.maxPerBet = currentAgentInfo.limitSetting.sportsBook.hdpOuOe.maxPerBet;
                }

                if (body.limitSetting.sportsBook.hdpOuOe.maxPerMatch > currentAgentInfo.limitSetting.sportsBook.hdpOuOe.maxPerMatch) {
                    body.limitSetting.sportsBook.hdpOuOe.maxPerMatch = currentAgentInfo.limitSetting.sportsBook.hdpOuOe.maxPerMatch
                }
                if (body.limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet > currentAgentInfo.limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet) {
                    body.limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet = currentAgentInfo.limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet
                }
                if (body.limitSetting.sportsBook.others.maxPerBet > currentAgentInfo.limitSetting.sportsBook.others.maxPerBet) {
                    body.limitSetting.sportsBook.others.maxPerBet = currentAgentInfo.limitSetting.sportsBook.others.maxPerBet
                }
                // if (body.limitSetting.sportsBook.mixParlay.maxPerBet > currentAgentInfo.limitSetting.sportsBook.mixParlay.maxPerBet) {
                //     body.limitSetting.sportsBook.mixParlay.maxPerBet = currentAgentInfo.limitSetting.sportsBook.mixParlay.maxPerBet
                // }
                // if (body.limitSetting.sportsBook.mixParlay.maxPerMatch > currentAgentInfo.limitSetting.sportsBook.mixParlay.maxPerMatch) {
                //     body.limitSetting.sportsBook.mixParlay.maxPerMatch = currentAgentInfo.limitSetting.sportsBook.mixParlay.maxPerMatch
                // }
                if (body.limitSetting.sportsBook.outright.maxPerMatch > currentAgentInfo.limitSetting.sportsBook.outright.maxPerMatch) {
                    body.limitSetting.sportsBook.outright.maxPerMatch = currentAgentInfo.limitSetting.sportsBook.outright.maxPerMatch
                }

                //STEP
                if (body.limitSetting.step.parlay.maxPerBet > currentAgentInfo.limitSetting.step.parlay.maxPerBet) {
                    body.limitSetting.step.parlay.maxPerBet = currentAgentInfo.limitSetting.step.parlay.maxPerBet
                }
                if (body.limitSetting.step.parlay.minPerBet < currentAgentInfo.limitSetting.step.parlay.minPerBet) {
                    body.limitSetting.step.parlay.minPerBet = currentAgentInfo.limitSetting.step.parlay.minPerBet
                }
                if (body.limitSetting.step.parlay.maxBetPerDay > currentAgentInfo.limitSetting.step.parlay.maxBetPerDay) {
                    body.limitSetting.step.parlay.maxBetPerDay = currentAgentInfo.limitSetting.step.parlay.maxBetPerDay
                }
                if (body.limitSetting.step.parlay.maxPayPerBill > currentAgentInfo.limitSetting.step.parlay.maxPayPerBill) {
                    body.limitSetting.step.parlay.maxPayPerBill = currentAgentInfo.limitSetting.step.parlay.maxPayPerBill
                }

                //STEP
                if (body.limitSetting.step.step.maxPerBet > currentAgentInfo.limitSetting.step.step.maxPerBet) {
                    body.limitSetting.step.step.maxPerBet = currentAgentInfo.limitSetting.step.step.maxPerBet
                }
                if (body.limitSetting.step.step.minPerBet < currentAgentInfo.limitSetting.step.step.minPerBet) {
                    body.limitSetting.step.step.minPerBet = currentAgentInfo.limitSetting.step.step.minPerBet
                }
                if (body.limitSetting.step.step.maxBetPerDay > currentAgentInfo.limitSetting.step.step.maxBetPerDay) {
                    body.limitSetting.step.step.maxBetPerDay = currentAgentInfo.limitSetting.step.step.maxBetPerDay
                }
                if (body.limitSetting.step.step.maxPayPerBill > currentAgentInfo.limitSetting.step.step.maxPayPerBill) {
                    body.limitSetting.step.step.maxPayPerBill = currentAgentInfo.limitSetting.step.step.maxPayPerBill
                }

                //OTHER

                if (body.limitSetting.other.m2.maxPerBetHDP > currentAgentInfo.limitSetting.other.m2.maxPerBetHDP) {
                    body.limitSetting.other.m2.maxPerBetHDP = currentAgentInfo.limitSetting.other.m2.maxPerBetHDP
                }
                if (body.limitSetting.other.m2.minPerBetHDP < currentAgentInfo.limitSetting.other.m2.minPerBetHDP) {
                    body.limitSetting.other.m2.minPerBetHDP = currentAgentInfo.limitSetting.other.m2.minPerBetHDP
                }
                if (body.limitSetting.other.m2.maxBetPerMatchHDP > currentAgentInfo.limitSetting.other.m2.maxBetPerMatchHDP) {
                    body.limitSetting.other.m2.maxBetPerMatchHDP = currentAgentInfo.limitSetting.other.m2.maxBetPerMatchHDP
                }


                if (body.limitSetting.other.m2.maxPerBet > currentAgentInfo.limitSetting.other.m2.maxPerBet) {
                    body.limitSetting.other.m2.maxPerBet = currentAgentInfo.limitSetting.other.m2.maxPerBet
                }
                if (body.limitSetting.other.m2.minPerBet < currentAgentInfo.limitSetting.other.m2.minPerBet) {
                    body.limitSetting.other.m2.minPerBet = currentAgentInfo.limitSetting.other.m2.minPerBet
                }
                if (body.limitSetting.other.m2.maxBetPerDay > currentAgentInfo.limitSetting.other.m2.maxBetPerDay) {
                    body.limitSetting.other.m2.maxBetPerDay = currentAgentInfo.limitSetting.other.m2.maxBetPerDay
                }
                if (body.limitSetting.other.m2.maxPayPerBill > currentAgentInfo.limitSetting.other.m2.maxPayPerBill) {
                    body.limitSetting.other.m2.maxPayPerBill = currentAgentInfo.limitSetting.other.m2.maxPayPerBill
                }


                //TODO
                if (body.limitSetting.lotto.amb._6TOP.max > currentAgentInfo.limitSetting.lotto.amb._6TOP.max) {
                    body.limitSetting.lotto.amb._6TOP.max = currentAgentInfo.limitSetting.lotto.amb._6TOP.max
                }

                if (body.limitSetting.lotto.amb._5TOP.max > currentAgentInfo.limitSetting.lotto.amb._5TOP.max) {
                    body.limitSetting.lotto.amb._5TOP.max = currentAgentInfo.limitSetting.lotto.amb._5TOP.max
                }

                if (body.limitSetting.lotto.amb._4TOP.max > currentAgentInfo.limitSetting.lotto.amb._4TOP.max) {
                    body.limitSetting.lotto.amb._4TOP.max = currentAgentInfo.limitSetting.lotto.amb._4TOP.max
                }

                if (body.limitSetting.lotto.amb._4TOD.max > currentAgentInfo.limitSetting.lotto.amb._4TOD.max) {
                    body.limitSetting.lotto.amb._4TOD.max = currentAgentInfo.limitSetting.lotto.amb._4TOD.max
                }

                if (body.limitSetting.lotto.amb._3TOP.max > currentAgentInfo.limitSetting.lotto.amb._3TOP.max) {
                    body.limitSetting.lotto.amb._3TOP.max = currentAgentInfo.limitSetting.lotto.amb._3TOP.max
                }

                if (body.limitSetting.lotto.amb._3TOD.max > currentAgentInfo.limitSetting.lotto.amb._3TOD.max) {
                    body.limitSetting.lotto.amb._3TOD.max = currentAgentInfo.limitSetting.lotto.amb._3TOD.max
                }

                if (body.limitSetting.lotto.amb._3BOT.max > currentAgentInfo.limitSetting.lotto.amb._3BOT.max) {
                    body.limitSetting.lotto.amb._3BOT.max = currentAgentInfo.limitSetting.lotto.amb._3BOT.max
                }

                if (body.limitSetting.lotto.amb._3TOP_OE.max > currentAgentInfo.limitSetting.lotto.amb._3TOP_OE.max) {
                    body.limitSetting.lotto.amb._3TOP_OE.max = currentAgentInfo.limitSetting.lotto.amb._3TOP_OE.max
                }

                if (body.limitSetting.lotto.amb._3TOP_OU.max > currentAgentInfo.limitSetting.lotto.amb._3TOP_OU.max) {
                    body.limitSetting.lotto.amb._3TOP_OU.max = currentAgentInfo.limitSetting.lotto.amb._3TOP_OU.max
                }

                if (body.limitSetting.lotto.amb._2TOP.max > currentAgentInfo.limitSetting.lotto.amb._2TOP.max) {
                    body.limitSetting.lotto.amb._2TOP.max = currentAgentInfo.limitSetting.lotto.amb._2TOP.max
                }

                if (body.limitSetting.lotto.amb._2TOD.max > currentAgentInfo.limitSetting.lotto.amb._2TOD.max) {
                    body.limitSetting.lotto.amb._2TOD.max = currentAgentInfo.limitSetting.lotto.amb._2TOD.max
                }

                if (body.limitSetting.lotto.amb._2BOT.max > currentAgentInfo.limitSetting.lotto.amb._2BOT.max) {
                    body.limitSetting.lotto.amb._2BOT.max = currentAgentInfo.limitSetting.lotto.amb._2BOT.max
                }

                if (body.limitSetting.lotto.amb._2TOP_OE.max > currentAgentInfo.limitSetting.lotto.amb._2TOP_OE.max) {
                    body.limitSetting.lotto.amb._2TOP_OE.max = currentAgentInfo.limitSetting.lotto.amb._2TOP_OE.max
                }

                if (body.limitSetting.lotto.amb._2TOP_OU.max > currentAgentInfo.limitSetting.lotto.amb._2TOP_OU.max) {
                    body.limitSetting.lotto.amb._2TOP_OU.max = currentAgentInfo.limitSetting.lotto.amb._2TOP_OU.max
                }


                if (body.limitSetting.lotto.amb._2BOT_OE.max > currentAgentInfo.limitSetting.lotto.amb._2BOT_OE.max) {
                    body.limitSetting.lotto.amb._2BOT_OE.max = currentAgentInfo.limitSetting.lotto.amb._2BOT_OE.max
                }

                if (body.limitSetting.lotto.amb._2BOT_OU.max > currentAgentInfo.limitSetting.lotto.amb._2BOT_OU.max) {
                    body.limitSetting.lotto.amb._2BOT_OU.max = currentAgentInfo.limitSetting.lotto.amb._2BOT_OU.max
                }


                if (body.limitSetting.lotto.amb._1TOP.max > currentAgentInfo.limitSetting.lotto.amb._1TOP.max) {
                    body.limitSetting.lotto.amb._1TOP.max = currentAgentInfo.limitSetting.lotto.amb._1TOP.max
                }

                if (body.limitSetting.lotto.amb._1BOT.max > currentAgentInfo.limitSetting.lotto.amb._1BOT.max) {
                    body.limitSetting.lotto.amb._1BOT.max = currentAgentInfo.limitSetting.lotto.amb._1BOT.max
                }


                //PP
                if (body.limitSetting.lotto.pp._6TOP.max > currentAgentInfo.limitSetting.lotto.pp._6TOP.max) {
                    body.limitSetting.lotto.pp._6TOP.max = currentAgentInfo.limitSetting.lotto.pp._6TOP.max
                }

                if (body.limitSetting.lotto.pp._5TOP.max > currentAgentInfo.limitSetting.lotto.pp._5TOP.max) {
                    body.limitSetting.lotto.pp._5TOP.max = currentAgentInfo.limitSetting.lotto.pp._5TOP.max
                }

                if (body.limitSetting.lotto.pp._4TOP.max > currentAgentInfo.limitSetting.lotto.pp._4TOP.max) {
                    body.limitSetting.lotto.pp._4TOP.max = currentAgentInfo.limitSetting.lotto.pp._4TOP.max
                }

                if (body.limitSetting.lotto.pp._4TOD.max > currentAgentInfo.limitSetting.lotto.pp._4TOD.max) {
                    body.limitSetting.lotto.pp._4TOD.max = currentAgentInfo.limitSetting.lotto.pp._4TOD.max
                }

                if (body.limitSetting.lotto.pp._3TOP.max > currentAgentInfo.limitSetting.lotto.pp._3TOP.max) {
                    body.limitSetting.lotto.pp._3TOP.max = currentAgentInfo.limitSetting.lotto.pp._3TOP.max
                }

                if (body.limitSetting.lotto.pp._3TOD.max > currentAgentInfo.limitSetting.lotto.pp._3TOD.max) {
                    body.limitSetting.lotto.pp._3TOD.max = currentAgentInfo.limitSetting.lotto.pp._3TOD.max
                }

                if (body.limitSetting.lotto.pp._2TOP.max > currentAgentInfo.limitSetting.lotto.pp._2TOP.max) {
                    body.limitSetting.lotto.pp._2TOP.max = currentAgentInfo.limitSetting.lotto.pp._2TOP.max
                }

                if (body.limitSetting.lotto.pp._2TOD.max > currentAgentInfo.limitSetting.lotto.pp._2TOD.max) {
                    body.limitSetting.lotto.pp._2TOD.max = currentAgentInfo.limitSetting.lotto.pp._2TOD.max
                }

                if (body.limitSetting.lotto.pp._2BOT.max > currentAgentInfo.limitSetting.lotto.pp._2BOT.max) {
                    body.limitSetting.lotto.pp._2BOT.max = currentAgentInfo.limitSetting.lotto.pp._2BOT.max
                }

                if (body.limitSetting.lotto.pp._1TOP.max > currentAgentInfo.limitSetting.lotto.pp._1TOP.max) {
                    body.limitSetting.lotto.pp._1TOP.max = currentAgentInfo.limitSetting.lotto.pp._1TOP.max
                }

                if (body.limitSetting.lotto.pp._1BOT.max > currentAgentInfo.limitSetting.lotto.pp._1BOT.max) {
                    body.limitSetting.lotto.pp._1BOT.max = currentAgentInfo.limitSetting.lotto.pp._1BOT.max
                }


                // if ((req.body.shareSetting.casino.allbet.own + req.body.shareSetting.casino.allbet.parent ) < currentAgentInfo.shareSetting.casino.allbet.min) {
                //     return res.send({
                //         message: "Agent Share (All Bet) + Agent Max Share (All Bet) should be greater than Agent Min Shares (All Bet)",
                //         result: err,
                //         code: 4008
                //     });
                // }


                // if ((req.body.shareSetting.lotto.amb.own + req.body.shareSetting.lotto.amb.parent ) < currentAgentInfo.shareSetting.lotto.amb.own) {
                //     return res.send({
                //         message: "Agent Share (Amb Lotto) + Agent Max Share (Amb Lotto) should be greater than Agent Min Shares (Amb Lotto)",
                //         result: err,
                //         code: 4010
                //     });
                // }

                if (req.body.settingEnable.sportBookEnable) {

                    if (req.body.shareSetting.sportsBook.today.own < maxShareParent.today) {
                        return res.send({
                            message: "Agent Max Share(Sport Today) should not be less than " + maxShareParent.today + "%.",
                            result: {max: maxShareParent.today},
                            code: 4020
                        });
                    }

                    if (req.body.shareSetting.sportsBook.live.own < maxShareParent.live) {
                        return res.send({
                            message: "Agent Max Share(Sport Live) should not be less than " + maxShareParent.live + "%.",
                            result: {max: maxShareParent.live},
                            code: 4021
                        });
                    }


                    if ((req.body.shareSetting.sportsBook.today.own + req.body.shareSetting.sportsBook.today.parent ) < currentAgentInfo.shareSetting.sportsBook.today.min) {
                        return res.send({
                            message: "Agent Share (Sport Today) + Agent Max Share (Sport Today) should be greater than Agent Min Shares (Sport Today)",
                            result: err,
                            code: 4003
                        });
                    }

                    if ((req.body.shareSetting.sportsBook.live.own + req.body.shareSetting.sportsBook.live.parent ) < currentAgentInfo.shareSetting.sportsBook.live.min) {
                        return res.send({
                            message: "Agent Share (Sport Live) + Agent Max Share (Sport Live) should be greater than Agent Min Shares (Sport Live)",
                            result: err,
                            code: 4004
                        });
                    }

                }


                if (req.body.settingEnable.stepEnable) {

                    if (req.body.shareSetting.step.step.own < maxShareParent.step) {
                        return res.send({
                            message: "Agent Max Share(Mix Step) should not be less than " + maxShareParent.step + "%.",
                            result: {max: maxShareParent.step},
                            code: 4025
                        });
                    }

                    if ((req.body.shareSetting.step.step.own + req.body.shareSetting.step.step.parent ) < currentAgentInfo.shareSetting.step.step.min) {
                        return res.send({
                            message: "Agent Share (Mix Step) + Agent Max Share (Mix Step) should be greater than Agent Min Shares (Mix Step)",
                            result: err,
                            code: 4008
                        });
                    }
                }

                if (req.body.settingEnable.parlayEnable) {

                    if (req.body.shareSetting.step.parlay.own < maxShareParent.parlay) {
                        return res.send({
                            message: "Agent Max Share(Mix Parlay) should not be less than " + maxShareParent.parlay + "%.",
                            result: {max: maxShareParent.parlay},
                            code: 4022
                        });
                    }


                    if ((req.body.shareSetting.step.parlay.own + req.body.shareSetting.step.parlay.parent ) < currentAgentInfo.shareSetting.step.parlay.min) {
                        return res.send({
                            message: "Agent Share (Step Parlay) + Agent Max Share (Step Parlay) should be greater than Agent Min Shares (Step Parlay)",
                            result: err,
                            code: 4005
                        });
                    }

                }

                if (req.body.settingEnable.casinoSaEnable) {

                    if (req.body.shareSetting.casino.sa.own < maxShareParent.sa) {
                        return res.send({
                            message: "Agent Max Share(SA Gaming) should not be less than " + maxShareParent.sa + "%.",
                            result: {max: maxShareParent.sa},
                            code: 4041
                        });
                    }


                    if ((req.body.shareSetting.casino.sa.own + req.body.shareSetting.casino.sa.parent ) < currentAgentInfo.shareSetting.casino.sa.min) {
                        return res.send({
                            message: "Agent Share (SA Gaming) + Agent Max Share (SA Gaming) should be greater than Agent Min Shares (SA Gaming)",
                            result: err,
                            code: 4011
                        });
                    }

                }

                if (req.body.settingEnable.casinoSexyEnable) {

                    if (req.body.shareSetting.casino.sexy.own < maxShareParent.sexy) {
                        return res.send({
                            message: "Agent Max Share(Sexy Baccarat) should not be less than " + maxShareParent.sexy + "%.",
                            result: {max: maxShareParent.sexy},
                            code: 4023
                        });
                    }

                    if ((req.body.shareSetting.casino.sexy.own + req.body.shareSetting.casino.sexy.parent ) < currentAgentInfo.shareSetting.casino.sexy.min) {
                        return res.send({
                            message: "Agent Share (Sexy Baccarat) + Agent Max Share (Sexy Baccarat) should be greater than Agent Min Shares (Sexy Baccarat)",
                            result: err,
                            code: 4006
                        });
                    }
                }

                if (req.body.settingEnable.casinoAgEnable) {

                    if (req.body.shareSetting.casino.ag.own < maxShareParent.ag) {
                        return res.send({
                            message: "Agent Max Share(AG Gaming) should not be less than " + maxShareParent.ag + "%.",
                            result: {max: maxShareParent.ag},
                            code: 4043
                        });
                    }

                    if ((req.body.shareSetting.casino.ag.own + req.body.shareSetting.casino.ag.parent ) < currentAgentInfo.shareSetting.casino.ag.min) {
                        return res.send({
                            message: "Agent Share (AG Gaming) + Agent Max Share (AG Gaming) should be greater than Agent Min Shares (AG Gaming)",
                            result: err,
                            code: 4013
                        });
                    }
                }

                if (req.body.settingEnable.casinoDgEnable) {

                    if (req.body.shareSetting.casino.dg.own < maxShareParent.dg) {
                        return res.send({
                            message: "Agent Max Share(Dream Gaming) should not be less than " + maxShareParent.dg + "%.",
                            result: {max: maxShareParent.dg},
                            code: 4042
                        });
                    }


                    if ((req.body.shareSetting.casino.dg.own + req.body.shareSetting.casino.dg.parent ) < currentAgentInfo.shareSetting.casino.dg.min) {
                        return res.send({
                            message: "Agent Share (Dream Gaming) + Agent Max Share (Dream Gaming) should be greater than Agent Min Shares (Dream Gaming)",
                            result: err,
                            code: 4012
                        });
                    }

                }


                if (req.body.settingEnable.casinoPtEnable) {

                    if (req.body.shareSetting.casino.pt.own < maxShareParent.pt) {
                        return res.send({
                            message: "Agent Max Share(Pretty Gaming) should not be less than " + maxShareParent.pt + "%.",
                            result: {max: maxShareParent.pt},
                            code: 4045
                        });
                    }


                    if ((req.body.shareSetting.casino.pt.own + req.body.shareSetting.casino.pt.parent ) < currentAgentInfo.shareSetting.casino.pt.min) {
                        return res.send({
                            message: "Agent Share (Pretty Gaming) + Agent Max Share (Pretty Gaming) should be greater than Agent Min Shares (Pretty Gaming)",
                            result: err,
                            code: 4015
                        });
                    }

                }

                //     sportBookEnable: true,
                //     stepEnable: true,
                //     parlayEnable: true,
                //     casinoSaEnable: true,
                //     casinoAgEnable: true,
                //     casinoSexyEnable: true,
                //     casinoDgEnable: true,
                //     gameSlotEnable: true,
                //     gameCardEnable: true,
                //     ambLottoEnable: true,
                //     ambLottoPPEnable: true,
                //     ambLottoLaosEnable: true,
                //     otherM2Enable: true,


                if (req.body.settingEnable.gameSlotEnable) {

                    if (req.body.shareSetting.game.slotXO.own < maxShareParent.slotXO) {
                        return res.send({
                            message: "Agent Max Share(Slot XO) should not be less than " + maxShareParent.slotXO + "%.",
                            result: {max: maxShareParent.slotXO},
                            code: 4026
                        });
                    }

                    if ((req.body.shareSetting.game.slotXO.own + req.body.shareSetting.game.slotXO.parent ) < currentAgentInfo.shareSetting.game.slotXO.min) {
                        return res.send({
                            message: "Agent Share (Slot XO) + Agent Max Share (Slot XO) should be greater than Agent Min Shares (Slot XO)",
                            result: err,
                            code: 4009
                        });
                    }
                }

                if (req.body.settingEnable.gameCardEnable) {

                    if (req.body.shareSetting.multi.amb.own < maxShareParent.ambGame) {
                        return res.send({
                            message: "Agent Max Share(AMB Game) should not be less than " + maxShareParent.ambGame + "%.",
                            result: {max: maxShareParent.ambGame},
                            code: 4044
                        });

                    }

                    if ((req.body.shareSetting.multi.amb.own + req.body.shareSetting.multi.amb.parent ) < currentAgentInfo.shareSetting.multi.amb.min) {
                        return res.send({
                            message: "Agent Share (AMB Game) + Agent Max Share (AMB Game) should be greater than Agent Min Shares (AMB Game)",
                            result: err,
                            code: 4014
                        });
                    }

                }

                if (req.body.settingEnable.ambLottoEnable) {
                    if (req.body.shareSetting.lotto.amb.own < maxShareParent.ambLotto) {
                        return res.send({
                            message: "Agent Max Share(Amb Lotto) should not be less than " + maxShareParent.ambLotto + "%.",
                            result: {max: maxShareParent.ambLotto},
                            code: 4027
                        });
                    }
                }

                if (req.body.settingEnable.ambLottoPPEnable) {
                    if (req.body.shareSetting.lotto.pp.own < maxShareParent.ambLottoPP) {
                        return res.send({
                            message: "Agent Max Share(Amb Lotto PP) should not be less than " + maxShareParent.ambLottoPP + "%.",
                            result: {max: maxShareParent.ambLottoPP},
                            code: 4046
                        });
                    }
                }

                if (req.body.settingEnable.ambLottoLaosEnable) {
                    if (req.body.shareSetting.lotto.laos.own < maxShareParent.ambLottoLaos) {
                        return res.send({
                            message: "Agent Max Share(Amb Lotto Laos) should not be less than " + maxShareParent.ambLottoLaos + "%.",
                            result: {max: maxShareParent.ambLottoLaos},
                            code: 4047
                        });
                    }
                }

                if (req.body.settingEnable.otherM2Enable) {

                    if (req.body.shareSetting.other.m2.own < maxShareParent.m2) {
                        return res.send({
                            message: "Agent Max Share(M2 Sport) should not be less than " + maxShareParent.m2 + "%.",
                            result: {max: maxShareParent.m2},
                            code: 4024
                        });
                    }

                    if ((req.body.shareSetting.other.m2.own + req.body.shareSetting.other.m2.parent ) < currentAgentInfo.shareSetting.other.m2.min) {
                        return res.send({
                            message: "Agent Share (M2 Sport) + Agent Max Share (M2 Sport) should be greater than Agent Min Shares (M2 Sport)",
                            result: err,
                            code: 4007
                        });
                    }

                }

                console.log(body);

                AgentGroupModel.update({_id: mongoose.Types.ObjectId(req.params.groupId)}, body, function (err, data) {

                    if (err) {
                        return res.send({message: "error", result: err, code: 999});
                    }

                    if (req.body.password !== '' && req.body.password != null) {
                        let bodyUser = {};
                        Hashing.encrypt256(req.body.password, function (err, hashingPass) {
                            bodyUser.password = hashingPass;
                        });


                        console.log('agentUpdateInfo.name : ', agentUpdateInfo.name)
                        UserModel.update({username_lower: agentUpdateInfo.name.toLowerCase()}, bodyUser, function (err, response) {

                            if (err) {
                                return res.send({message: "error", result: err, code: 999});
                            }

                            let historyModel = AgentGroupHistoryModel(body);
                            historyModel.id = req.params.groupId;
                            historyModel.createdBy = userInfo._id;
                            historyModel.createdDate = DateUtils.getCurrentDate();
                            historyModel.save((err, historyResponse) => {
                                console.log(err)
                            });


                            checkAndUpdateParentSetting(req.params.groupId, req.body, function (err, response) {

                            });

                            return res.send(
                                {
                                    code: 0,
                                    message: "success",
                                    result: data
                                }
                            );
                        });

                    } else {
                        console.log(22)
                        let historyModel = AgentGroupHistoryModel(body);
                        historyModel.id = req.params.groupId;
                        historyModel.createdBy = userInfo._id;
                        historyModel.createdDate = DateUtils.getCurrentDate();
                        historyModel.save((err, historyResponse) => {
                            console.log(err)
                        });
                        checkAndUpdateParentSetting(req.params.groupId, req.body, function (err, response) {
                            // console.log('xx : ', response)
                        });


                        return res.send(
                            {
                                code: 0,
                                message: "success",
                                result: data
                            }
                        );
                    }

                });
            });
        }
    });

});


const updateStatusGroupSchema = Joi.object().keys({
    suspend: Joi.boolean().required(),
    lock: Joi.boolean().required(),
    active: Joi.boolean().required(),
});

router.put('/status/:groupId', function (req, res) {

    let validate = Joi.validate(req.body, updateStatusGroupSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    const userInfo = req.userInfo;


    UserModel.findOne({_id:userInfo._id},(err,checkAgentResponse)=> {


        if (!checkAgentResponse) {

            return res.send({
                message: "UnAuthorization",
                result: err,
                code: 401
            });


        } else {

            let groupId = req.params.groupId;

            let requestBody = req.body;

            async.series([mainCallback => {

                if (!req.body.active) {

                    async.waterfall([callback => {
                        findAllSubGroup(groupId, [], (err, response) => {
                            if (err) {
                                callback(err, null);
                            } else {
                                callback(null, response)
                            }
                        });

                    }, (groupList, callback) => {

                        let memberList = [];
                        _.each(groupList, function (item) {
                            memberList = memberList.concat(item.members);
                        });
                        callback(null, groupList, memberList);

                    }], (err, groupList, memberList) => {


                        console.log('groupList : ', groupList);
                        console.log('memberList : ', memberList);

                        let body = {
                            creditLimit: 0,
                            balance: 0,
                            active: false
                        };

                        async.series([callback => {
                            let groupIds = _.map(groupList, function (item) {
                                return mongoose.Types.ObjectId(item.id);
                            });

                            AgentGroupModel.update({_id: {$in: groupIds}}, body, {multi: true}, function (err, data) {
                                if (err) {
                                    callback(err, null);
                                } else {
                                    callback(null, data)
                                }
                            });
                        }, callback => {
                            MemberModel.update({_id: {$in: memberList}}, body, {multi: true}, function (err, data) {
                                if (err) {
                                    callback(err, null);
                                } else {
                                    callback(null, data)
                                }
                            });
                        }], (err, response) => {

                            if (err) {
                                mainCallback(err, null);
                            } else {
                                mainCallback(null, response)
                            }
                        });
                    });
                } else {
                    mainCallback(null, []);
                }

            }], (err, mainAsyncResponse) => {

                if (err) {
                    return res.send({message: "error", result: err, code: 999});
                }

                let body = {
                    suspend: req.body.suspend,
                    lock: req.body.lock,
                    active: req.body.active,
                };

                if (!req.body.active) {
                    body.creditLimit = 0;
                    body.balance = 0;
                }


                async.series([callback => {
                    AgentGroupModel.update({_id: mongoose.Types.ObjectId(req.params.groupId)}, body, function (err, data) {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, data)
                        }
                    });
                }, callback => {
                    MemberModel.update({group: mongoose.Types.ObjectId(req.params.groupId)}, body, function (err, data) {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, data)
                        }
                    });
                }], (err, asyncResponse) => {

                    console.log(asyncResponse)

                    if (err) {
                        return res.send({message: "error", result: err, code: 999});
                    }


                    let historyModel = new UpdateStatusHistoryModel(body);
                    historyModel.id = groupId;
                    historyModel.name = '';
                    historyModel.type = 'AGENT';
                    historyModel.suspend = req.body.suspend;
                    historyModel.lock = req.body.lock;
                    historyModel.active = req.body.active;
                    historyModel.createdBy = userInfo._id;
                    historyModel.createdDate = DateUtils.getCurrentDate();
                    historyModel.save((err, historyResponse) => {
                        console.log(err)
                        return res.send(
                            {
                                code: 0,
                                message: "success",
                                result: true
                            }
                        );
                    });

                    // if(requestBody.suspend || requestBody.lock || !requestBody.active) {
                    //     AgentService.logoutDownlineMemberPartnerGame(req.params.groupId, (err, response) => {
                    //
                    //     });
                    // }

                });

            });
        }

    });

});

function checkAndUpdateParentSetting(groupId, body, callback) {


    async.waterfall([callback => {

        findAllSubGroupAndOwn(groupId, [], (err, response) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, response);
            }
        });

    }, (groupList, callback) => {

        let memberList = [];
        _.each(groupList, function (item) {
            memberList = memberList.concat(item.members);
        });
        callback(null, groupList, memberList);

    }], (err, groupList, memberList) => {

        //
        // console.log('groupList : ', groupList);
        // console.log('memberList : ', memberList.length);
        let saLimit = body.limitSetting.casino.sa.limit;
        let updateSALimitBody = {
            $set: {
                // 'limitSetting.casino.sexy.isEnable': true,
                'limitSetting.casino.sa.limit': saLimit
            }
        };

        let dgLimit = body.limitSetting.casino.dg.limit;
        let updateDGLimitBody = {
            $set: {
                // 'limitSetting.casino.sexy.isEnable': true,
                'limitSetting.casino.dg.limit': dgLimit
            }
        };

        let sexyLimit = body.limitSetting.casino.sexy.limit;
        let updateSexyLimitBody = {
            $set: {
                // 'limitSetting.casino.sexy.isEnable': true,
                'limitSetting.casino.sexy.limit': sexyLimit
            }
        };

        let agLimit = body.limitSetting.casino.ag.limit;
        let updateAGLimitBody = {
            $set: {
                'limitSetting.casino.ag.limit': agLimit
            }
        };

        let ptLimit = body.limitSetting.casino.pt.limit;
        let updatePTLimitBody = {
            $set: {
                'limitSetting.casino.pt.limit': ptLimit
            }
        };

        let stepMaxMatchPerBet = body.limitSetting.step.step.maxMatchPerBet;
        let parlayMaxMatchPerBet = body.limitSetting.step.parlay.maxMatchPerBet;

        let updateParlayBody = {
            $set: {
                // 'limitSetting.casino.sexy.isEnable': true,
                'limitSetting.step.parlay.maxMatchPerBet': parlayMaxMatchPerBet
            }
        };

        let updateStepBody = {
            $set: {
                // 'limitSetting.casino.sexy.isEnable': true,
                'limitSetting.step.step.maxMatchPerBet': stepMaxMatchPerBet
            }
        };

        async.parallel([callback => {
            let groupIds = _.map(groupList, function (item) {
                return mongoose.Types.ObjectId(item.id);
            });

            AgentGroupModel.update({
                _id: {$in: groupIds},
                'limitSetting.casino.sa.limit': {$gt: saLimit}
            }, updateSALimitBody, {multi: true}, function (err, data) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, data)
                }
            });
        }, callback => {
            MemberModel.update({
                _id: {$in: memberList},
                'limitSetting.casino.sa.limit': {$gt: saLimit}
            }, updateSALimitBody, {multi: true}, function (err, data) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, data)
                }
            });
        }, callback => {
            let groupIds = _.map(groupList, function (item) {
                return mongoose.Types.ObjectId(item.id);
            });

            AgentGroupModel.update({
                _id: {$in: groupIds},
                'limitSetting.casino.dg.limit': {$gt: dgLimit}
            }, updateDGLimitBody, {multi: true}, function (err, data) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, data)
                }
            });
        }, callback => {
            MemberModel.update({
                _id: {$in: memberList},
                'limitSetting.casino.dg.limit': {$gt: dgLimit}
            }, updateDGLimitBody, {multi: true}, function (err, data) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, data)
                }
            });
        }, callback => {
            let groupIds = _.map(groupList, function (item) {
                return mongoose.Types.ObjectId(item.id);
            });

            AgentGroupModel.update({
                _id: {$in: groupIds},
                'limitSetting.casino.sexy.limit': {$gt: sexyLimit}
            }, updateSexyLimitBody, {multi: true}, function (err, data) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, data)
                }
            });
        }, callback => {
            MemberModel.update({
                _id: {$in: memberList},
                'limitSetting.casino.sexy.limit': {$gt: sexyLimit}
            }, updateSexyLimitBody, {multi: true}, function (err, data) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, data)
                }
            });
        }, callback => {
            let groupIds = _.map(groupList, function (item) {
                return mongoose.Types.ObjectId(item.id);
            });

            AgentGroupModel.update({
                _id: {$in: groupIds},
                'limitSetting.casino.ag.limit': {$gt: agLimit}
            }, updateAGLimitBody, {multi: true}, function (err, data) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, data)
                }
            });
        }, callback => {
            MemberModel.update({
                _id: {$in: memberList},
                'limitSetting.casino.ag.limit': {$gt: agLimit}
            }, updateAGLimitBody, {multi: true}, function (err, data) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, data)
                }
            });
        }, callback => {
            let groupIds = _.map(groupList, function (item) {
                return mongoose.Types.ObjectId(item.id);
            });

            AgentGroupModel.update({
                _id: {$in: groupIds},
                'limitSetting.casino.pt.limit': {$gt: ptLimit}
            }, updatePTLimitBody, {multi: true}, function (err, data) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, data)
                }
            });
        }, callback => {
            MemberModel.update({
                _id: {$in: memberList},
                'limitSetting.casino.pt.limit': {$gt: ptLimit}
            }, updatePTLimitBody, {multi: true}, function (err, data) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, data)
                }
            });
        },callback => {

            let groupIds = _.map(groupList, function (item) {
                return mongoose.Types.ObjectId(item.id);
            });

            AgentGroupModel.update({
                _id: {$in: groupIds},
                'limitSetting.step.parlay.maxMatchPerBet': {$gt: parlayMaxMatchPerBet}
            }, updateParlayBody, {multi: true}, function (err, data) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, data)
                }
            });
        }, callback => {
            MemberModel.update({
                _id: {$in: memberList},
                'limitSetting.step.parlay.maxMatchPerBet': {$gt: parlayMaxMatchPerBet}
            }, updateParlayBody, {multi: true}, function (err, data) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, data)
                }
            });
        }, callback => {

            let groupIds = _.map(groupList, function (item) {
                return mongoose.Types.ObjectId(item.id);
            });

            AgentGroupModel.update({
                _id: {$in: groupIds},
                'limitSetting.step.step.maxMatchPerBet': {$gt: stepMaxMatchPerBet}
            }, updateStepBody, {multi: true}, function (err, data) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, data)
                }
            });
        }, callback => {
            MemberModel.update({
                _id: {$in: memberList},
                'limitSetting.step.step.maxMatchPerBet': {$gt: stepMaxMatchPerBet}
            }, updateStepBody, {multi: true}, function (err, data) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, data)
                }
            });
        }, callback => {

            let groupIds = _.map(groupList, function (item) {
                return mongoose.Types.ObjectId(item.id);
            });

            let updateMaxCreditLimitBody = {
                $set: {
                    // 'limitSetting.casino.sexy.isEnable': true,
                    'maxCreditLimit': body.maxCreditLimit
                }
            };

            AgentGroupModel.update({
                _id: {$in: groupIds},
                'maxCreditLimit': {$gt: body.maxCreditLimit}
            }, updateMaxCreditLimitBody, {multi: true}, function (err, data) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, data);
                }
            });

        }], (err, response) => {

            if (err) {
                callback(err, null);
            } else {
                callback(null, response)
            }
        });
    });
}

function findAllSubGroup(groupId, listOfAgent, callback) {
    console.log('find : ', groupId);

    AgentGroupModel.findOne({_id: mongoose.Types.ObjectId(groupId)})
        .populate({
            path: 'childGroups',
            select: 'childGroups childMembers',
            populate: {
                path: 'childGroups',
                model: 'AgentGroup',
                select: 'childGroups childMembers'
            }
        })
        .select('_id childGroups childMembers')
        .exec((err, response) => {

            async.each(response.childGroups, (child, callback1) => {

                listOfAgent.push({id: child._id, members: child.childMembers});

                async.each(child.childGroups, (child2, callback2) => {

                    listOfAgent.push({id: child2._id, members: child2.childMembers});

                    findAllSubGroup(child2._id, listOfAgent, (err, response) => {
                        callback2(null, true);
                    });

                }, (err) => {
                    callback1(null, true);
                });
            }, (err) => {
                callback(null, listOfAgent);
            });
        });
}

function findAllSubGroupAndOwn(groupId, listOfAgent, callback) {
    console.log('find : ', groupId);

    AgentGroupModel.findOne({_id: mongoose.Types.ObjectId(groupId)})
        .populate({
            path: 'childGroups',
            select: 'childGroups childMembers',
            populate: {
                path: 'childGroups',
                model: 'AgentGroup',
                select: 'childGroups childMembers'
            }
        })
        .select('_id childGroups childMembers')
        .exec((err, response) => {

            listOfAgent.push({id: response._id, members: response.childMembers});

            async.each(response.childGroups, (child, callback1) => {

                listOfAgent.push({id: child._id, members: child.childMembers});

                async.each(child.childGroups, (child2, callback2) => {

                    listOfAgent.push({id: child2._id, members: child2.childMembers});

                    findAllSubGroupAndOwn(child2._id, listOfAgent, (err, response) => {
                        callback2(null, true);
                    });

                }, (err) => {
                    callback1(null, true);
                });
            }, (err) => {
                callback(null, listOfAgent);
            });
        });

}


function findAllSubGroupAndOwnName(groupId, listOfAgent, callback) {
    console.log('find11 : ', groupId);

    AgentGroupModel.findOne({_id: mongoose.Types.ObjectId(groupId)})
        .populate([{
            path: 'childGroups',
            select: 'childGroups childMembers',
            populate: [{
                path: 'childGroups',
                model: 'AgentGroup',
                select: 'childGroups childMembers'
            }, {
                path: 'childMembers',
                model: 'Member',
                select: 'username_lower'
            }]
        }, {
            path: 'childMembers',
            model: 'Member',
            select: 'username_lower'
        }])
        .select('_id childGroups childMembers')
        .exec((err, response) => {

            listOfAgent.push({
                id: response._id, members: response.childMembers.map(item => {
                    return item.username_lower
                })
            });

            async.each(response.childGroups, (child, callback1) => {

                listOfAgent.push({
                    id: child._id, members: child.childMembers.map(item => {
                        return item.username_lower
                    })
                });

                async.each(child.childGroups, (child2, callback2) => {

                    listOfAgent.push({
                        id: child2._id, members: child2.childMembers.map(item => {
                            return item.username_lower
                        })
                    });

                    findAllSubGroupAndOwn(child2._id, listOfAgent, (err, response) => {
                        callback2(null, true);
                    });

                }, (err) => {
                    callback1(null, true);
                });
            }, (err) => {

                let memberNameList = [];
                _.each(listOfAgent, function (item) {
                    memberNameList = memberNameList.concat(item.members);
                });
                callback(null, memberNameList);
            });
        });
}

function getYesterdayPaymentBalanceTask(agentId, agentType, lastPaymentDate, currentAgentType) {

    return function (callbackTask) {

        let group = '';
        let child = '';

        getGroupAndChild(agentType, (groupRes, childRes) => {
            group = groupRes;
            child = childRes;
        });

        let condition = {};

        let startYear = Number.parseInt(lastPaymentDate.getUTCFullYear());
        let startMonth = Number.parseInt(lastPaymentDate.getUTCMonth());
        let startDay = Number.parseInt(lastPaymentDate.getUTCDate());


        if (moment().hours() >= 11) {
            condition['gameDate'] = {
                "$gte": moment().millisecond(0).second(0).minute(0).hour(11).date(startDay).month(startMonth).year(startYear).add(1, 'd').utc(true).toDate()
                // "$lt": new Date(moment().add(0, 'd').utc(true).format('YYYY-MM-DDT11:00:00.000'))
            };
        } else {
            condition['gameDate'] = {
                "$gte": moment().millisecond(0).second(0).minute(0).hour(11).date(startDay).month(startMonth).year(startYear).add(1, 'd').utc(true).toDate()
                // "$lt": new Date(moment().add(0, 'd').utc(true).format('YYYY-MM-DDT11:00:00.000'))
            };
        }


        condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(agentId);

        condition.status = 'DONE';


        BetTransactionModel.aggregate([
            {
                $match: condition
            },
            {
                "$group": {
                    "_id": {
                        commission: '$commission.' + group + '.group',
                    },
                    "amount": {$sum: '$amount'},
                    "validAmount": {$sum: '$validAmount'},
                    "memberWinLose": {$sum: '$commission.member.winLose'},
                    "memberWinLoseCom": {$sum: '$commission.member.winLoseCom'},
                    "memberTotalWinLoseCom": {$sum: '$commission.member.totalWinLoseCom'},

                    "agentWinLose": {$sum: '$commission.agent.winLose'},
                    "agentWinLoseCom": {$sum: '$commission.agent.winLoseCom'},
                    "agentTotalWinLoseCom": {$sum: '$commission.agent.totalWinLoseCom'},

                    "masterAgentWinLose": {$sum: '$commission.masterAgent.winLose'},
                    "masterAgentWinLoseCom": {$sum: '$commission.masterAgent.winLoseCom'},
                    "masterAgentTotalWinLoseCom": {$sum: '$commission.masterAgent.totalWinLoseCom'},

                    "seniorWinLose": {$sum: '$commission.senior.winLose'},
                    "seniorWinLoseCom": {$sum: '$commission.senior.winLoseCom'},
                    "seniorTotalWinLoseCom": {$sum: '$commission.senior.totalWinLoseCom'},

                    "shareHolderWinLose": {$sum: '$commission.shareHolder.winLose'},
                    "shareHolderWinLoseCom": {$sum: '$commission.shareHolder.winLoseCom'},
                    "shareHolderTotalWinLoseCom": {$sum: '$commission.shareHolder.totalWinLoseCom'},

                    "companyWinLose": {$sum: '$commission.company.winLose'},
                    "companyWinLoseCom": {$sum: '$commission.company.winLoseCom'},
                    "companyTotalWinLoseCom": {$sum: '$commission.company.totalWinLoseCom'},

                    "superAdminWinLose": {$sum: '$commission.superAdmin.winLose'},
                    "superAdminWinLoseCom": {$sum: '$commission.superAdmin.winLoseCom'},
                    "superAdminTotalWinLoseCom": {$sum: '$commission.superAdmin.totalWinLoseCom'}
                }
            },
            {
                $project: {
                    _id: 0,
                    group: '$_id.commission',
                    type: 'A_GROUP',
                    amount: '$amount',
                    validAmount: '$validAmount',
                    memberWinLose: '$memberWinLose',
                    memberWinLoseCom: '$memberWinLoseCom',
                    memberTotalWinLoseCom: '$memberTotalWinLoseCom',
                    agentWinLose: '$agentWinLose',
                    agentWinLoseCom: '$agentWinLoseCom',
                    agentTotalWinLoseCom: '$agentTotalWinLoseCom',
                    masterAgentWinLose: '$masterAgentWinLose',
                    masterAgentWinLoseCom: '$masterAgentWinLoseCom',
                    masterAgentTotalWinLoseCom: '$masterAgentTotalWinLoseCom',
                    seniorWinLose: '$seniorWinLose',
                    seniorWinLoseCom: '$seniorWinLoseCom',
                    seniorTotalWinLoseCom: '$seniorTotalWinLoseCom',
                    shareHolderWinLose: '$shareHolderWinLose',
                    shareHolderWinLoseCom: '$shareHolderWinLoseCom',
                    shareHolderTotalWinLoseCom: '$shareHolderTotalWinLoseCom',
                    companyWinLose: '$companyWinLose',
                    companyWinLoseCom: '$companyWinLoseCom',
                    companyTotalWinLoseCom: '$companyTotalWinLoseCom',
                    superAdminWinLose: '$superAdminWinLose',
                    superAdminWinLoseCom: '$superAdminWinLoseCom',
                    superAdminTotalWinLoseCom: '$superAdminTotalWinLoseCom',

                }
            }
        ]).read('secondaryPreferred').exec((err, results) => {
            let result = transformDataByAgentTypeReport(currentAgentType, results);

            let finalResult = [];

            if (result.length > 0) {
                finalResult.push({group: result[0].group, balance: result[0].memberTotalWinLoseCom})
            }
            callbackTask(null, finalResult);
        });
    }
}

function getYesterdayMemberPaymentBalanceTask(memberId, lastPaymentDate) {

    return function (callbackTask) {


        let condition = {};

        let startYear = Number.parseInt(lastPaymentDate.getUTCFullYear());
        let startMonth = Number.parseInt(lastPaymentDate.getUTCMonth());
        let startDay = Number.parseInt(lastPaymentDate.getUTCDate());

        if (moment().hours() >= 11) {
            condition['gameDate'] = {
                "$gte": moment().millisecond(0).second(0).minute(0).hour(11).date(startDay).month(startMonth).year(startYear).add(1, 'd').utc(true).toDate()
                // "$lt": new Date(moment().add(0, 'd').utc(true).format('YYYY-MM-DDT11:00:00.000'))
            };
        } else {
            condition['gameDate'] = {
                "$gte": moment().millisecond(0).second(0).minute(0).hour(11).date(startDay).month(startMonth).year(startYear).add(1, 'd').utc(true).toDate()
                // "$lt": new Date(moment().add(0, 'd').utc(true).format('YYYY-MM-DDT11:00:00.000'))
            };
        }

        condition['memberId'] = mongoose.Types.ObjectId(memberId);


        condition.status = 'DONE';


        BetTransactionModel.aggregate([
            {
                $match: condition
            },
            {
                "$group": {
                    "_id": {
                        commission: '$memberId',
                    },
                    "amount": {$sum: '$amount'},
                    "validAmount": {$sum: '$validAmount'},
                    "memberWinLose": {$sum: '$commission.member.winLose'},
                    "memberWinLoseCom": {$sum: '$commission.member.winLoseCom'},
                    "memberTotalWinLoseCom": {$sum: '$commission.member.totalWinLoseCom'},

                }
            },
            {
                $project: {
                    _id: 0,
                    group: '$_id.commission',
                    type: 'A_GROUP',
                    amount: '$amount',
                    validAmount: '$validAmount',
                    memberWinLose: '$memberWinLose',
                    memberWinLoseCom: '$memberWinLoseCom',
                    memberTotalWinLoseCom: '$memberTotalWinLoseCom',
                }
            }
        ]).read('secondaryPreferred').exec((err, results) => {

            // let result = transformDataByAgentTypeReport(currentAgentType, results);

            let finalResult = [];

            if (results && results.length > 0) {
                finalResult.push({
                    group: results[0].group,
                    balance: results[0].memberTotalWinLoseCom
                })
            }
            callbackTask(null, finalResult);
        });
    }
}

function getGroupAndChild(groupType, callback) {
    let group, child;
    if (groupType === 'SUPER_ADMIN') {
        group = 'superAdmin';
        child = 'company';
    } else if (groupType === 'COMPANY') {
        group = 'company';
        child = 'shareHolder';
    } else if (groupType === 'SHARE_HOLDER') {
        group = 'shareHolder';
        child = 'senior';
    } else if (groupType === 'SENIOR') {
        group = 'senior';
        child = 'masterAgent';
    } else if (groupType === 'MASTER_AGENT') {
        group = 'masterAgent';
        child = 'agent';
    } else if (groupType === 'AGENT') {
        group = 'agent';
    }
    callback(group, child);
}

function transformDataByAgentTypeReport(type, dataList) {
    if (type === 'COMPANY') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = item.superAdminWinLose;
            newItem.companyWinLoseCom = item.superAdminWinLoseCom;
            newItem.companyTotalWinLoseCom = item.superAdminTotalWinLoseCom;

            newItem.agentWinLose = item.companyWinLose;
            newItem.agentWinLoseCom = item.companyWinLoseCom;
            newItem.agentTotalWinLoseCom = item.companyTotalWinLoseCom;


            newItem.memberWinLose = (item.memberWinLose + item.masterAgentWinLose + item.agentWinLose + item.seniorWinLose + item.shareHolderWinLose);
            newItem.memberWinLoseCom = (item.memberWinLoseCom + item.masterAgentWinLoseCom + item.agentWinLoseCom + item.seniorWinLoseCom + item.shareHolderWinLoseCom);
            newItem.memberTotalWinLoseCom = (item.memberTotalWinLoseCom + item.masterAgentTotalWinLoseCom + item.agentTotalWinLoseCom + item.seniorTotalWinLoseCom + item.shareHolderTotalWinLoseCom);

            return newItem;
        });
    } else if (type === 'SHARE_HOLDER') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose;
            newItem.companyWinLoseCom = item.superAdminWinLoseCom + item.companyWinLoseCom;
            newItem.companyTotalWinLoseCom = item.superAdminTotalWinLoseCom + item.companyTotalWinLoseCom;

            newItem.agentWinLose = item.shareHolderWinLose;
            newItem.agentWinLoseCom = item.shareHolderWinLoseCom;
            newItem.agentTotalWinLoseCom = item.shareHolderTotalWinLoseCom;

            newItem.memberWinLose = (item.memberWinLose + item.masterAgentWinLose + item.agentWinLose + item.seniorWinLose);
            newItem.memberWinLoseCom = (item.memberWinLoseCom + item.masterAgentWinLoseCom + item.agentWinLoseCom + item.seniorWinLoseCom);
            newItem.memberTotalWinLoseCom = (item.memberTotalWinLoseCom + item.masterAgentTotalWinLoseCom + item.agentTotalWinLoseCom + item.seniorTotalWinLoseCom);
            return newItem;
        });
    } else if (type === 'SENIOR') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose + item.shareHolderWinLose;
            newItem.companyWinLoseCom = item.superAdminWinLoseCom + item.companyWinLoseCom + item.shareHolderWinLoseCom;
            newItem.companyTotalWinLoseCom = item.superAdminTotalWinLoseCom + item.companyTotalWinLoseCom + item.shareHolderTotalWinLoseCom;

            newItem.agentWinLose = item.seniorWinLose;
            newItem.agentWinLoseCom = item.seniorWinLoseCom;
            newItem.agentTotalWinLoseCom = item.seniorTotalWinLoseCom;


            newItem.memberWinLose = (item.memberWinLose + item.masterAgentWinLose + item.agentWinLose);
            newItem.memberWinLoseCom = (item.memberWinLoseCom + item.masterAgentWinLoseCom + item.agentWinLoseCom);
            newItem.memberTotalWinLoseCom = (item.memberTotalWinLoseCom + item.masterAgentTotalWinLoseCom + item.agentTotalWinLoseCom);

            return newItem;
        });
    } else if (type === 'MASTER_AGENT') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose + item.shareHolderWinLose + item.seniorWinLose;
            newItem.companyWinLoseCom = item.superAdminWinLoseCom + item.companyWinLoseCom + item.shareHolderWinLoseCom + item.seniorWinLoseCom;
            newItem.companyTotalWinLoseCom = item.superAdminTotalWinLoseCom + item.companyTotalWinLoseCom + item.shareHolderTotalWinLoseCom + item.seniorTotalWinLoseCom;

            newItem.agentWinLose = item.masterAgentWinLose;
            newItem.agentWinLoseCom = item.masterAgentWinLoseCom;
            newItem.agentTotalWinLoseCom = item.masterAgentTotalWinLoseCom;

            newItem.memberWinLose = (item.memberWinLose + item.agentWinLose);
            newItem.memberWinLoseCom = (item.memberWinLoseCom + item.agentWinLoseCom);
            newItem.memberTotalWinLoseCom = (item.memberTotalWinLoseCom + item.agentTotalWinLoseCom);

            return newItem;
        });
    } else if (type === 'AGENT') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);
            newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose + item.shareHolderWinLose + item.seniorWinLose + item.masterAgentWinLose;
            newItem.companyWinLoseCom = item.superAdminWinLoseCom + item.companyWinLoseCom + item.shareHolderWinLoseCom + item.seniorWinLoseCom + item.masterAgentWinLoseCom;
            newItem.companyTotalWinLoseCom = item.superAdminTotalWinLoseCom + item.companyTotalWinLoseCom + item.shareHolderTotalWinLoseCom + item.seniorTotalWinLoseCom + item.masterAgentTotalWinLoseCom;

            newItem.agentWinLose = item.agentWinLose;
            newItem.agentWinLoseCom = item.agentWinLoseCom;
            newItem.agentTotalWinLoseCom = item.agentTotalWinLoseCom;

            newItem.memberWinLose = item.memberWinLose;
            newItem.memberWinLoseCom = item.memberWinLoseCom;
            newItem.memberTotalWinLoseCom = item.memberTotalWinLoseCom;
            return newItem;
        });
    } else {
        return dataList;
    }
}

module.exports = router;

