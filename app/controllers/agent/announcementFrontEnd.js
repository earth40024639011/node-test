const AnnouncementFrontEndModel = require('../../models/announcementFrontEnd.model.js');
const UserModel = require('../../models/users.model');
const express = require('express');
const router = express.Router();
const async = require("async");
const Joi = require('joi');
const mongoose = require('mongoose');
const config = require('config');
const _ = require('underscore');
const zpad = require('zpad');


router.get('/list', function (req, res) {

    let condition = {};

    let userInfo = req.userInfo;


    // if (userInfo.group._id) {
    //     condition.updateBy = userInfo.group._id;
    // }

    AnnouncementFrontEndModel.paginate(condition, {
            page: Number.parseInt(req.query.page),
            limit: Number.parseInt(req.query.limit),
            populate: 'createdBy'
        },
        function (err, data) {
            if (err)
                return res.send({message: "error", results: err, code: 999});

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: data
                }
            );
        });

});



router.get('/home/:key', function (req, res) {


    let condition = {key:req.params.key};

    AnnouncementFrontEndModel.findOne(condition, function (err, response) {
        if (err) {
            return res.send({message: "error", result: err, code: 999});
        }
        return res.send(
            {
                code: 0,
                message: "success",
                result: response
            }
        );
    });

});


const addHomeMsgSchema = Joi.object().keys({
    message: Joi.string().required(),
    active:Joi.boolean().required(),
});

router.put('/update-homepage', function (req, res) {

    let validate = Joi.validate(req.body, addHomeMsgSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    const userInfo = req.userInfo;


    let condition = {key:'HOMEPAGE'};
    let body = {
        $set: {

            message: req.body.message,
            active:req.body.active,
            createdBy: userInfo._id
        }
    };

    AnnouncementFrontEndModel.findOneAndUpdate(condition, body, {upsert: true, new: true,setDefaultsOnInsert: true }, function (err, response) {
        if (err) {
            return res.send({message: "error", result: err, code: 999});
        }
        return res.send(
            {
                code: 0,
                message: "success",
                result: response
            }
        );
    });

});


module.exports = router;

