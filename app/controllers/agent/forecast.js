const MemberModel = require('../../models/member.model');
const BetTransactionModel = require('../../models/betTransaction.model.js');
const AgentGroupModel = require('../../models/agentGroup.model.js');
const MemberService = require('../../common/memberService');
const express = require('express');
const router = express.Router();
const async = require("async");
const Joi = require('joi');
const mongoose = require('mongoose');
const config = require('config');
const _ = require('underscore');
const moment = require('moment');
const naturalSort = require("javascript-natural-sort");
const roundTo = require('round-to');
const ApiService = require('../../common/apiService');
const FormulaUtils = require('../../common/formula');
const Commons = require('../../common/commons');
const StockHdpService = require('../../common/stockHdpService');

router.get('/list', function (req, res) {

    let condition = {game: 'FOOTBALL'};

    let userInfo = req.userInfo;

    if (req.query.dateFrom && req.query.dateTo) {
        condition['createdDate'] = {
            "$gte": moment(req.query.dateFrom, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).utc(true).toDate(),
            "$lt":moment(req.query.dateTo, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).add(1,'d').utc(true).toDate()
        }
    }


    let orCondition = [];

    if (req.query.status) {
        orCondition.push({
            $or: req.query.status.split(',').map(function (status) {
                return {'status': status}
            })
        });
    }

    if (req.query.oddType && req.query.oddType !== 'ALL') {
        if (req.query.gameType === 'PARLAY') {
            condition['gameType'] = 'PARLAY';
        } else if (req.query.gameType === 'TODAY') {
            condition['gameType'] = 'TODAY';
            if (req.query.oddType === 'HDP') {
                orCondition.push({$or: [{'hdp.oddType': 'AH'}, {'hdp.oddType': 'AH1ST'}]})
            } else if (req.query.oddType === 'OU') {
                orCondition.push({$or: [{'hdp.oddType': 'OU'}, {'hdp.oddType': 'OU1ST'}]})
            } else if (req.query.oddType === 'OE') {
                orCondition.push({$or: [{'hdp.oddType': 'OE'}, {'hdp.oddType': 'OE1ST'}]})
            } else if (req.query.oddType === 'X12') {
                orCondition.push({$or: [{'hdp.oddType': 'X12'}, {'hdp.oddType': 'X121ST'}]})
            } else {
                condition['hdp.oddType'] = req.query.oddType;
            }
        }
    }

    if (orCondition.length > 0) {
        condition['$and'] = orCondition;
    }

    let group = '';
    let child = '';
    if (userInfo.group.type === 'SUPER_ADMIN') {
        group = 'superAdmin';
        child = 'company';
    } else if (userInfo.group.type === 'COMPANY') {
        group = 'company';
        child = 'shareHolder';
    } else if (userInfo.group.type === 'SHARE_HOLDER') {
        group = 'shareHolder';
        child = 'superSenior';
    } else if (userInfo.group.type === 'SUPER_SENIOR') {
        group = 'superSenior';
        child = 'senior';
    } else if (userInfo.group.type === 'SENIOR') {
        group = 'senior';
        child = 'masterAgent';
    } else if (userInfo.group.type === 'MASTER_AGENT') {
        group = 'masterAgent';
        child = 'agent';
    } else if (userInfo.group.type === 'AGENT') {
        group = 'agent';
        child = 'member';
    }

    condition['game'] = 'FOOTBALL';
    condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(userInfo.group._id);

    console.log(condition);
    BetTransactionModel.aggregate([
        {
            $match: condition
        },
        {
            $project: {
                _id: 0,
                memberId: '$memberId',
                hdp: '$hdp',
                parlay: '$parlay',
                commission: '$commission.' + group,
                child: '$commission.' + child,
                amount: '$amount',
                payout: '$payout',
                status: '$status',
                totalReceive: {$multiply: ['$amount', {$divide: ['$commission.' + group + '.shareReceive', 100]}]},
                stake_in: '$amount',
                stake_out: {
                    $cond: [{$eq: ['$gameType', 'PARLAY']}, {$multiply: ['$amount', '$parlay.odds']}, {$multiply: ['$amount', '$hdp.odd']}]
                },
                betId: '$betId',
                createdDate: '$createdDate',
                priceType: '$priceType',
                gameType: '$gameType',
                ipAddress: '$ipAddress',
                isPrinted: '$isPrinted',
            }
        }
    ], (err, results) => {
        if (err)
            return res.send({message: "error", results: err, code: 999});


        async.series([(callback => {

            MemberModel.populate(results, {
                path: 'memberId',
                select: '_id username'
            }, function (err, memberResult) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, memberResult);
                }
            });


        }), (callback => {

            AgentGroupModel.populate(results, {
                path: 'commission.group',
                select: '_id name'
            }, function (err, memberResult) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, memberResult);
                }
            });

        }), (callback => {

            AgentGroupModel.populate(results, {
                path: 'child.group',
                select: '_id name'
            }, function (err, memberResult) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, memberResult);
                }
            });

        })], (err, populateResults) => {

            if (err)
                return res.send({message: "error", results: err, code: 999});

            let summary = _.reduce(results, function (memo, num) {
                return {
                    totalAmount: memo.totalAmount + num.amount,
                    totalReceive: memo.totalReceive + num.totalReceive
                }
            }, {totalAmount: 0, totalReceive: 0});

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: {
                        betList: results,
                        summary: summary,
                    }
                }
            );
        });
    });

});


const DRAW = "DRAW";
const WIN = "WIN";
const HALF_WIN = "HALF_WIN";
const LOSE = "LOSE";
const HALF_LOSE = "HALF_LOSE";


function subPlyAmount(amount, count) {
    if (count > 0) {
        return subPlyAmount(amount / 2, --count);
    } else {
        return amount;
    }
}
router.get('/test_stock',function (req,res) {

    let userInfo = req.query.Username;
    StockHdpService.getHdpList(userInfo,(err, loginResponse) => {

        // let betDetail = loginResponse.betDetail.filter(loginResponse.betDetail.gameId);
        if (err) {
            return res.send({message: 'fail', code: 999});

        } else {
            console.log(loginResponse,'sucess');
            return res.send({message: 'success',
                code: 0,
                result: loginResponse
            });
        }
    });
})



router.get('/stock_oe_x12', function (req, res) {

    let name = req.query.name;

    let condition = {game: 'FOOTBALL'};

    let userInfo = req.userInfo;

    if (req.query.active) {
        condition.active = req.query.active;
    }

    let group = '';
    if (userInfo.group.type === 'SUPER_ADMIN') {
        group = 'superAdmin';
    } else if (userInfo.group.type === 'COMPANY') {
        group = 'company';
    } else if (userInfo.group.type === 'SHARE_HOLDER') {
        group = 'shareHolder';
    } else if (userInfo.group.type === 'SUPER_SENIOR') {
        group = 'superSenior';
    } else if (userInfo.group.type === 'SENIOR') {
        group = 'senior';
    } else if (userInfo.group.type === 'MASTER_AGENT') {
        group = 'masterAgent';
    } else if (userInfo.group.type === 'AGENT') {
        group = 'agent';
    }
    condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(userInfo.group._id);

    condition['$and'] = [
        {$or: [{'hdp.oddType': 'OE'}, {'hdp.oddType': 'OE1ST'}, {'hdp.oddType': 'X12'}, {'hdp.oddType': 'X121ST'}]},
        {$or: [{"status": 'RUNNING'}]}
    ];

    console.log(condition)
    BetTransactionModel.aggregate([
        {
            $match: condition
        },
        {
            $project: {
                leagueId: '$hdp.leagueId',
                leagueName: '$hdp.leagueName',
                match: {
                    matchId: '$hdp.matchId',
                    matchName: '$hdp.matchName',
                    matchDate: '$hdp.matchDate',
                    bet: '$hdp.bet'
                }
            }
        }, {
            "$group": {
                "_id": {
                    leagueId: '$leagueId',
                },
                "leagueName": {$first: "$leagueName"},
                "matches": {$push: '$match'},
            }
        },
        {
            "$project": {
                _id: 0,
                leagueId: '$_id.leagueId',
                leagueName: '$leagueName',
                matches: '$matches'
            }
        }
    ], (err, results) => {

        if (err)
            return res.send({message: "error", results: err, code: 999});

        let resultList = _.chain(results)
            .each(league => {

                let groupsMatch = _.groupBy(league.matches, function (value) {
                    return value.matchId + '|' + value.matchName['en'].h + ' vs ' + value.matchName['en'].a + '|' + value.matchDate
                });

                league.matches = _.map(groupsMatch, (value, key) => {
                    return {
                        key: key,
                        matchId: key.split('|')[0],
                        name: key.split('|')[1],
                        date: value[0] ? value[0].matchDate : null,
                        matches: value
                    }
                });

                league.matches = _.sortBy(league.matches, function (o) {
                    return o.name;
                });

                _.each(league.matches, (subMatch) => {
                    let betCount = _.countBy(subMatch.matches, function (match) {
                        return match.bet;
                    });

                    delete subMatch.matches;
                    subMatch.bet = betCount;

                });

            }).value();

        resultList = _.sortBy(resultList, function (o) {
            return o.leagueName;
        });

        return res.send(
            {
                code: 0,
                message: "success",
                results: resultList

            }
        );
    });


});

router.get('/stock_oe_x12/:matchId/:bet', function (req, res) {

    let name = req.query.name;

    let condition = {game: 'FOOTBALL'};

    let userInfo = req.userInfo;


    if (req.query.active) {
        condition.active = req.query.active;
    }

    let group = '';
    if (userInfo.group.type === 'SUPER_ADMIN') {
        group = 'superAdmin';
    } else if (userInfo.group.type === 'COMPANY') {
        group = 'company';
    } else if (userInfo.group.type === 'SHARE_HOLDER') {
        group = 'shareHolder';
    } else if (userInfo.group.type === 'SUPER_SENIOR') {
        group = 'superSenior';
    } else if (userInfo.group.type === 'SENIOR') {
        group = 'senior';
    } else if (userInfo.group.type === 'MASTER_AGENT') {
        group = 'masterAgent';
    } else if (userInfo.group.type === 'AGENT') {
        group = 'agent';
    }
    condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(userInfo.group._id);

    condition['hdp.matchId'] = req.params.matchId;
    condition['$and'] = [
        {$or: [{'hdp.oddType': 'OE'}, {'hdp.oddType': 'OE1ST'}, {'hdp.oddType': 'X12'}, {'hdp.oddType': 'X121ST'}]},
        {$or: [{"status": 'RUNNING'}]}
    ];
    condition['hdp.bet'] = req.params.bet;
    console.log(condition)

    BetTransactionModel.aggregate([
        {
            $match: condition
        },
        {
            $project: {
                _id: 0,
                memberId: '$memberId',
                hdp: '$hdp',
                commission: '$commission.' + group,
                amount: '$amount',
                stake_in: '$amount',
                stake_out: {$multiply: ['$amount', '$hdp.odd']},
                totalReceive: {$multiply: ['$amount', {$divide: ['$commission.' + group + '.shareReceive', 100]}]},
                betId: '$betId',
                createdDate: '$createdDate',
                priceType: '$priceType',
                ipAddress: '$ipAddress'
            }
        }
    ], (err, results) => {
        if (err)
            return res.send({message: "error", results: err, code: 999});

        let x = {};

        MemberModel.populate(results, {
            path: 'memberId',
            select: '_id username'
        }, function (e, memberResult) {
            if (e) return;
            results.memberId = memberResult;

            if (err)
                return res.send({message: "error", results: err, code: 999});


            let summary = _.reduce(results, function (memo, num) {
                return {
                    totalAmount: memo.totalAmount + num.amount,
                    totalReceive: memo.totalReceive + num.totalReceive
                }
            }, {totalAmount: 0, totalReceive: 0});

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: {
                        dataList: results,
                        summary: summary,
                    }
                }
            );
        });
    });

});

router.get('/stock_hdp_ou', function (req, res) {

    let condition = {game: 'FOOTBALL'};

    condition.gameDate = {$gte: new Date(moment().add(-7, 'd').format('YYYY-MM-DDT11:00:00.000'))};

    let userInfo = req.userInfo;

    if (req.query.active) {
        condition.active = req.query.active;
    }
    if (req.query.matchType) {
        condition['hdp.matchType'] = req.query.matchType;
    }

    let group = '';
    if (userInfo.group.type === 'SUPER_ADMIN') {
        group = 'superAdmin';
    } else if (userInfo.group.type === 'COMPANY') {
        group = 'company';
    } else if (userInfo.group.type === 'SHARE_HOLDER') {
        group = 'shareHolder';
    } else if (userInfo.group.type === 'SENIOR') {
        group = 'senior';
    } else if (userInfo.group.type === 'MASTER_AGENT') {
        group = 'masterAgent';
    } else if (userInfo.group.type === 'AGENT') {
        group = 'agent';
    }
    condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(userInfo.group._id);

    if(req.query.type === 'HDP') {
        condition['$and'] = [
            {$or: [{'hdp.oddType': 'AH'}, {'hdp.oddType': 'AH1ST'}]},
            {$or: [{"status": 'RUNNING'}]}
        ];
    }else if(req.query.type === 'OU'){
        condition['$and'] = [
            {$or: [{'hdp.oddType': 'OU'}, {'hdp.oddType': 'OU1ST'}]},
            {$or: [{"status": 'RUNNING'}]}
            ];
    }else {
        return res.send({message: "error", results: err, code: 999});
    }




    // condition['hdp.matchId']= '49027:2483158';

    console.log(condition)

    async.parallel([(callback) => {
            BetTransactionModel.aggregate([
                {
                    $match: condition
                },
                {
                    "$group": {
                        "_id": {
                            leagueId: '$hdp.leagueId',
                            matchId: '$hdp.matchId',
                            score: '$hdp.score',
                            oddType: '$hdp.oddType',
                            handicap: '$hdp.handicap',
                            bet:'$hdp.bet'
                        },
                        matchDate: {$first: "$hdp.matchDate"},
                        leagueName: {$first: "$hdp.leagueName"},
                        matchName: {$first: "$hdp.matchName"},
                        homeReceive: {$sum: {$cond: [{$in: ['$hdp.bet', ['HOME', 'OVER']]}, '$commission.' + group + '.amount', 0]}},
                        awayReceive: {$sum: {$cond: [{$in: ['$hdp.bet', ['AWAY', 'UNDER']]}, '$commission.' + group + '.amount', 0]}},
                        totalAmount: {$sum: '$amount'},
                        totalHomeReceive: {$sum: {$cond: [{$in: ['$hdp.bet', ['HOME', 'OVER']]}, '$amount', 0]}},
                        totalAwayReceive: {$sum: {$cond: [{$in: ['$hdp.bet', ['AWAY', 'UNDER']]}, '$amount', 0]}},
                        totalReceive: {$sum: '$commission.' + group + '.amount'},
                        dummyBet: {$first: '$hdp.bet'},
                        dummyHandicap: {$first: '$hdp.handicap'},
                        payout: {$sum: {$multiply: ['$commission.' + group + '.amount', {$cond:[{ $gte: [ "$hdp.odd", 0 ]}, "$hdp.odd",1]}]}},
                        stakeCount: {$sum: 1}
                    }
                },
                {
                    $project: {
                        _id: 0,
                        leagueId: '$_id.leagueId',
                        matchId: '$_id.matchId',
                        leagueName: '$leagueName',
                        score: '$_id.score',
                        bet:'$_id.bet',
                        handicap: '$_id.handicap',
                        matchName: '$matchName',
                        matchDate: '$matchDate',
                        oddType: '$_id.oddType',
                        totalAmount: '$totalAmount',
                        totalReceive: '$totalReceive',
                        homeReceive: '$homeReceive',
                        awayReceive: '$awayReceive',
                        totalHomeReceive: '$totalHomeReceive',
                        totalAwayReceive: '$totalAwayReceive',
                        payout:'$payout',
                        dummyBet: '$dummyBet',
                        dummyHandicap: '$dummyHandicap',
                        stakeCount:'$stakeCount',
                    }
                },
                {$sort: {'matchId': 1}},


            ], (err, results) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, results);
                }
            });
        }],
        function (err, results) {

            if (err)
                return res.send({message: "error", results: err, code: 999});

            // console.log(results)
            let noneLiveResult = results[0];

            noneLiveResult = _.chain(noneLiveResult)
                .groupBy((value) => {
                    return value.leagueId + '|' + value.leagueName
                })
                .map((value, key) => {
                    return {
                        league: key.split('|')[0],
                        name: key.split('|')[1],
                        matches: _.map(value, (value, key) => {
                            return {
                                matchId: value.matchId,
                                matchName: value.matchName,
                                matchDate: value.matchDate,
                                handicap: value.handicap,
                                score: value.score,
                                oddType: value.oddType,
                                totalAmount: value.totalAmount,
                                totalReceive: value.totalReceive,
                                homeReceive: value.homeReceive,
                                awayReceive: value.awayReceive,
                                totalHomeReceive: value.totalHomeReceive,
                                totalAwayReceive: value.totalAwayReceive,
                                dummyBet: value.dummyBet,
                                dummyHandicap: value.dummyHandicap,
                                stakeCount:value.stakeCount,
                                payout: Math.abs(value.payout),
                                bet:value.bet

                            };
                        })
                    }
                })
                .each(subMatch => {
                    let groupsMatch = _.groupBy(subMatch.matches, function (value) {
                        return value.matchId + '|' + value.matchName['en'].h + ' vs ' + value.matchName['en'].a + '|' + value.matchDate
                    });

                    subMatch.matches = _.map(groupsMatch, (value, key) => {

                        let groupsMatch = _.groupBy(value, function (value) {
                            if(req.query.matchType === 'NONE_LIVE'){
                                return value.handicap.replace('-', '')
                            }else {
                                return value.handicap.replace('-', '') + '|'+value.score
                            }
                        });


                        let list = _.map(groupsMatch, (groupHdpValue, groupHdpKey) => {


                            let summary = _.reduce(groupHdpValue, function (memo, obj) {
                                return {
                                    homeTotal: memo.homeTotal + obj.homeReceive,
                                    awayTotal: memo.awayTotal + obj.awayReceive

                                }
                            }, {
                                homeTotal: 0,
                                awayTotal: 0,
                            });


                            let handicap = groupHdpKey;

                            let hscore1 = 0;
                            let hscore2 = 0;
                            let hscore3 = 0;

                            let xhan = handicap.split('/');

                            let matchHandicap;

                            if (xhan.length == 2) {
                                let fh = Math.abs(Number.parseFloat(xhan[0]));
                                let sh = Number.parseFloat(xhan[1]);
                                matchHandicap = fh + ((sh - fh) / 2);
                            } else {
                                matchHandicap = Math.abs(Number.parseFloat(handicap));
                            }


                            if (groupHdpValue[0].dummyBet === 'HOME') {
                                if (!groupHdpValue[0].dummyHandicap.startsWith('-')) {
                                    matchHandicap = matchHandicap * -1;
                                }
                            } else {
                                if (groupHdpValue[0].dummyHandicap.startsWith('-')) {
                                    matchHandicap = matchHandicap * -1;
                                }
                            }

                            let scoreList = [];

                            if(req.query.type === 'HDP') {
                                if (Math.abs(matchHandicap) <= 0.50) {
                                    hscore1 = 1;
                                    hscore2 = 0;
                                    hscore3 = -1;
                                } else {

                                    let fMatch = roundTo(Math.abs(matchHandicap), 0);
                                    if (matchHandicap > 0) {
                                        hscore1 = fMatch + 1;
                                        hscore2 = fMatch;
                                        hscore3 = fMatch - 1;
                                    } else {
                                        hscore1 = (fMatch - 1) * -1;
                                        hscore2 = (fMatch) * -1;
                                        hscore3 = (fMatch + 1) * -1;
                                    }

                                }
                            }else {
                                let fMatch = roundTo(Math.abs(matchHandicap), 0);
                                hscore1 = fMatch + 1;
                                hscore2 = fMatch;
                                hscore3 = fMatch - 1;
                            }
                            scoreList = [hscore1,hscore2,hscore3];



                            let prepareResult = _.map(groupHdpValue, (subMatchValue, subMatchKey) => {

                                let results = [];
                                _.forEach(scoreList,(score)=>{
                                    let betResult;

                                    if(req.query.type === 'HDP') {
                                        if (Math.abs(matchHandicap) <= 0.5) {
                                            if (score == 1) {
                                                betResult = calculateHandicap(1, 0, subMatchValue.handicap, subMatchValue.bet);
                                            } else if (score == 0) {
                                                betResult = calculateHandicap(0, 0, subMatchValue.handicap, subMatchValue.bet);
                                            } else if (score == -1) {
                                                betResult = calculateHandicap(0, 1, subMatchValue.handicap, subMatchValue.bet);
                                            }
                                        } else {

                                                if (subMatchValue.bet === 'HOME') {

                                                    if (subMatchValue.handicap.startsWith('-')) {
                                                        console.log('111111')
                                                        betResult = calculateHandicap(Math.abs(score), 0, subMatchValue.handicap, 'HOME');
                                                    }else {
                                                        console.log('222222')
                                                        console.log('score : ',score)
                                                        betResult = calculateHandicap(0, Math.abs(score), subMatchValue.handicap, 'HOME');
                                                    }
                                                } else {
                                                    if (subMatchValue.handicap.startsWith('-')) {
                                                        console.log('333333')
                                                        betResult = calculateHandicap(0, Math.abs(score), subMatchValue.handicap, 'AWAY');
                                                    }else {
                                                        console.log('444444')
                                                        betResult = calculateHandicap(Math.abs(score), 0, subMatchValue.handicap, 'AWAY');
                                                    }
                                                }
                                        }
                                    }else {
                                        // let totalScore = Number.parseInt(subMatchValue.score.split(':')[0]) + Number.parseInt(subMatchValue.score.split(':')[1])
                                        betResult = calculateOuScore(0,Math.abs(score), subMatchValue.handicap, subMatchValue.bet);
                                    }

                                    let winLose = 0;
                                    if (betResult === DRAW) {
                                        winLose = 0;
                                    } else if (betResult === WIN) {
                                        winLose = subMatchValue.payout;
                                    } else if (betResult === HALF_WIN) {
                                        winLose =  subMatchValue.payout / 2;
                                    } else if (betResult === LOSE) {
                                        if(req.query.type ==='HDP'){
                                            winLose = 0 - (subMatchValue.bet === 'HOME' ? subMatchValue.homeReceive : subMatchValue.awayReceive);
                                        }else {
                                            if(subMatchValue.bet === 'OVER') {
                                                winLose = 0 - subMatchValue.homeReceive;
                                            }else {
                                                winLose = 0 - subMatchValue.awayReceive;
                                            }
                                        }
                                    } else if (betResult === HALF_LOSE) {
                                        if(req.query.type ==='HDP') {
                                            winLose = 0 - (subMatchValue.bet === 'HOME' ? (subMatchValue.homeReceive / 2) : (subMatchValue.awayReceive / 2));
                                        }else {
                                            if(subMatchValue.bet === 'OVER') {
                                                winLose = 0 - roundTo(subMatchValue.homeReceive / 2 ,2);
                                            }else {
                                                winLose = 0 - roundTo(subMatchValue.awayReceive / 2 ,2);
                                            }
                                        }
                                    }
                                    results.push({score:score,winLose: (winLose * -1),betResult:betResult});
                                });


                                subMatchValue.result = results;
                                console.log(subMatchValue)
                                _.forEach(results,(o)=>{
                                    console.log('-- : ',o)
                                });
                                return subMatchValue;
                            });

                            // console.log('list : ',prepareResult)

                            let summaryWinLose = _.reduce(prepareResult, function (memo, obj) {
                                return {
                                    score1: memo.score1 + obj.result[0].winLose,
                                    score2: memo.score2 + obj.result[1].winLose,
                                    score3: memo.score3 + obj.result[2].winLose,

                                }
                            }, {
                                score1: 0,
                                score2: 0,
                                score3: 0
                            });


                            return {
                                handicap: matchHandicap,
                                score:groupHdpKey.split('|')[1],
                                homeTotal: summary.homeTotal,
                                awayTotal: summary.awayTotal,
                                score1: hscore1,
                                score2: hscore2,
                                score3: hscore3,
                                score1Val: summaryWinLose.score1,
                                score2Val: summaryWinLose.score2,
                                score3Val: summaryWinLose.score3
                            }
                        });


                        return {
                            matchId: key.split('|')[0],
                            name: key.split('|')[1],
                            list: _.sortBy(list, function (i) {
                                return i.handicap && i.score;
                            })
                        }
                    });


                    subMatch.matches = subMatch.matches.sort(function (a, b) {
                        return naturalSort(a.score, b.score);
                    });

                    subMatch.matches = _.sortBy(subMatch.matches, function (o) {
                        return o.matchType;
                    });

                    subMatch.matches = _.sortBy(subMatch.matches, function (o) {
                        return o.name;
                    });

                    subMatch.matches = _.sortBy(subMatch.matches, function (o) {
                        return o.date;
                    });

                })

                .sortBy(function (o) {
                    return o.name;
                })
                .value();

            return res.send(
                {
                    code: 0,
                    message: "success",
                    results: noneLiveResult
                }
            );
        });


});


function prepareHdpScore(home, away) {
    let x = [[home, away]];
    for (let i = 1; i <= 5; i++) {
        x.unshift([home + i, away]);
        x.push([home, away + i]);
    }
    return x;
}

function prepareOuScore() {
    let x = [];
    for (let i = 1; i <= 10; i++) {
        x.push([i, 0]);
    }
    return x;
}

function transformAmountReceive(memberGroupType, dataList) {

    console.log('transformAmountReceive : ', memberGroupType)

    if (memberGroupType === 'SENIOR') {
        return _.map(dataList, function (item) {

            let newItem = Object.assign({}, item)._doc;

            newItem.companyAmount = _.defaults(item.commission.company, {amount: 0}).amount + _.defaults(item.commission.shareHolder, {amount: 0}).amount;

            newItem.agentAmount = _.defaults(item.commission.senior, {amount: 0}).amount;

            newItem.memberAmount = newItem.companyAmount + newItem.agentAmount;

            return newItem;
        });
    } else if (memberGroupType === 'MASTER_AGENT') {
        return _.map(dataList, function (item) {

            let newItem = Object.assign({}, item)._doc;

            newItem.companyAmount = _.defaults(item.commission.company, {amount: 0}).amount + _.defaults(item.commission.shareHolder, {amount: 0}).amount + _.defaults(item.commission.senior, {amount: 0}).amount;

            newItem.agentAmount = _.defaults(item.commission.masterAgent, {amount: 0}).amount;

            newItem.memberAmount = newItem.companyAmount + newItem.agentAmount;

            return newItem;
        });
    } else if (memberGroupType === 'AGENT') {
        return _.map(dataList, function (item) {

            let newItem = Object.assign({}, item)._doc;

            newItem.companyAmount = _.defaults(item.commission.company, {amount: 0}).amount + _.defaults(item.commission.shareHolder, {amount: 0}).amount + _.defaults(item.commission.senior, {amount: 0}).amount
                + _.defaults(item.commission.masterAgent, {amount: 0}).amount;

            newItem.agentAmount = _.defaults(item.commission.agent, {amount: 0}).amount;

            newItem.memberAmount = newItem.companyAmount + newItem.agentAmount;

            return newItem;
        });
    } else {
        return dataList;
    }
}

function transformDataByAgentType(type, dataList) {
    if (type === 'SUPER_ADMIN') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = 0;

            newItem.agentWinLose = item.superAdminWinLose;

            newItem.memberWinLose = (item.masterAgentWinLose + item.agentWinLose + item.seniorWinLose + item.shareHolderWinLose + item.companyWinLose);

            return newItem;
        });
    } else if (type === 'COMPANY') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = item.superAdminWinLose;

            newItem.agentWinLose = item.companyWinLose;

            newItem.memberWinLose = (item.masterAgentWinLose + item.agentWinLose + item.seniorWinLose + item.shareHolderWinLose);

            return newItem;
        });
    } else if (type === 'SHARE_HOLDER') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose;

            newItem.agentWinLose = item.shareHolderWinLose;

            newItem.memberWinLose = (item.masterAgentWinLose + item.agentWinLose + item.seniorWinLose);
            return newItem;
        });
    } else if (type === 'SENIOR') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose + item.shareHolderWinLose;

            newItem.agentWinLose = item.seniorWinLose;

            newItem.memberWinLose = (item.masterAgentWinLose + item.agentWinLose);
            return newItem;
        });
    } else if (type === 'MASTER_AGENT') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose + item.shareHolderWinLose + item.seniorWinLose;

            newItem.agentWinLose = item.masterAgentWinLose;

            newItem.memberWinLose = (item.agentWinLose);

            return newItem;
        });
    } else if (type === 'AGENT') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);
            newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose + item.shareHolderWinLose + item.seniorWinLose + item.masterAgentWinLose;

            newItem.agentWinLose = item.agentWinLose;

            newItem.memberWinLose = item.memberWinLose;
            return newItem;
        });
    } else {
        return dataList;
    }
}

//DUPLICATE ENDSCORE

function calculateOneTwo(home, away, bet) {

    if (bet === 'DRAW' && home == away) {
        return "WIN"
    } else if (bet === 'HOME' && home > away) {
        return "WIN"
    } else if (bet === 'AWAY' && away > home) {
        return "WIN"
    }
    return "LOSE";
}

function calculateOeScore(home, away, bet) {
    let oe = (home + away) % 2;
    if ((bet === 'EVEN' && oe == 0) || (bet === 'ODD' && oe != 0)) {
        return "WIN"
    }
    return "LOSE";
}

function calculateOuScore(home, away, handicap, bet) {

    let totalScore = (home + away);
    let matchHandicap = prePareHandicap(handicap);

    console.log('calculateOuScore');
    console.log('totalScore : ', totalScore);
    console.log('matchHandicap : ', matchHandicap);
    let finalRate;
    if (bet === 'OVER') {
        finalRate = totalScore - matchHandicap;
    } else {
        finalRate = matchHandicap - totalScore;
    }

    let result;
    console.log('finalRate : ', finalRate);
    if (finalRate == 0) {
        result = 'DRAW';
    } else if (finalRate == 0.25) {
        result = 'HALF_WIN';
    } else if (finalRate == -0.25) {
        result = 'HALF_LOSE';
    } else if (finalRate >= 0.5) {
        result = 'WIN';
    } else if (finalRate <= -0.5) {
        result = 'LOSE';
    }
    return result;
}

function prePareHandicap(handicap) {
    let xhan = handicap.split('/');

    let matchHandicap;

    if (xhan.length == 2) {
        let fh = Math.abs(Number.parseFloat(xhan[0]));
        let sh = Number.parseFloat(xhan[1]);
        matchHandicap = fh + ((sh - fh) / 2);
    } else {
        matchHandicap = Math.abs(Number.parseFloat(handicap));
    }
    return matchHandicap;
}

function calculateHandicap(homeScore, awayScore, handicap, bet) {

    let home = Number.parseFloat(homeScore);
    let away = Number.parseFloat(awayScore);

    let matchHandicap = prePareHandicap(handicap);

    let isTor = handicap.startsWith('-');

    let torScore;
    let longScore;

    if (bet === 'HOME') {
        if (isTor) {
            torScore = home;
            longScore = away;
        } else {
            torScore = away;
            longScore = home;
        }
    } else if (bet === 'AWAY') {
        if (isTor) {
            torScore = away;
            longScore = home;
        } else {
            torScore = home;
            longScore = away;
        }
    }


    let finalRate;


    if (isTor) {
        finalRate = torScore - (longScore + matchHandicap);
    } else {
        finalRate = (longScore + matchHandicap) - torScore;
    }


    let result;
    if (finalRate == 0) {
        result = 'DRAW';
    } else if (finalRate == 0.25) {
        result = 'HALF_WIN';
    } else if (finalRate == -0.25) {
        result = 'HALF_LOSE';
    } else if (finalRate >= 0.5) {
        result = 'WIN';
    } else if (finalRate <= -0.5) {
        result = 'LOSE';
    }
    return result;
}

module.exports = router;