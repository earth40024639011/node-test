const EndScoreHistoryModel = require('../../models/endScoreHistory.model');
const BetTransactionModel = require('../../models/betTransaction.model.js');
const UserModel = require("../../models/users.model");
const MemberService = require('../../common/memberService');
const AgentService = require('../../common/agentService');
const express = require('express')
const router = express.Router()
const async = require("async");
const Joi = require('joi');
const mongoose = require('mongoose');
const FormulaUtils = require('../../common/formula');
const config = require('config');
const _ = require('underscore');
const Hashing = require('../../common/hashing');
const roundTo = require('round-to');
const DateUtils = require('../../common/dateUtils');

const MemberModel = require('../../models/member.model.js');
const Path1Model = require('../../models/path1.model.js');
const ReportService = require('../../common/reportService');

const naturalSort = require("javascript-natural-sort");

const waterfall = require("async/waterfall");

const parallel = require("async/parallel");
const Redis = require('../../controllers/sbo/1_common/1_redis/1_1_redis');

router.post('/z', function (req, res) {

    let requestBody = {
        gameName: 'TK',
        bet: {
            card: 22
        },
        results: [22, 42, 21]

    };

    let betList = [], resultList = [];

    if (requestBody.gameName == 'TEEKAI' || requestBody.gameName == 'POKDENG') {
        betList = [{'key': requestBody.bet.card.join('|'), 'amount': requestBody.betAmount}];

        resultList = _.map(requestBody.results, function (item) {
            return item.join('|')
        });

    } else {
        betList = _.map(_.keys(requestBody.bet), function (key) {
            return {'key': key, 'amount': requestBody.bet[key]}
        });

        resultList = requestBody.results
    }


    return res.send(
        {
            code: 0,
            message: "success",
            betList: betList,
            resultList: resultList
        }
    );


});



const DRAW = "DRAW";
const WIN = "WIN";
const HALF_WIN = "HALF_WIN";
const LOSE = "LOSE";
const HALF_LOSE = "HALF_LOSE";


function convertPercent(percent) {
    return (percent || 0) / 100
}


function subPlyAmount(amount, count) {
    if (count > 0) {
        return subPlyAmount(amount / 2, --count);
    } else {
        return amount;
    }

}

const reScoreSchema = Joi.object().keys({
    key: Joi.string().required(),
    matchId: Joi.string().required(),
    homeScoreHT: Joi.number().allow(''),
    awayScoreHT: Joi.number().allow(''),
    homeScoreFT: Joi.number().allow(''),
    awayScoreFT: Joi.number().allow(''),
    type: Joi.string().required()
});
router.post('/re-calculate', function (req, res) {

    let validate = Joi.validate(req.body, reScoreSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    let matchId = req.body.matchId;
    let homeScoreHalf = Number.parseInt(req.body.homeScoreHT);
    let awayScoreHalf = Number.parseInt(req.body.awayScoreHT);
    let homeScoreFull = Number.parseInt(req.body.homeScoreFT);
    let awayScoreFull = Number.parseInt(req.body.awayScoreFT);


    async.parallel([(callback => {

        let condition = {
            game: 'FOOTBALL',
            'hdp.matchId': matchId,
            status: {$in: ['DONE', 'REJECTED']},
            gameType: 'TODAY',
            active: true,
            gameDate: {$gte: new Date(moment().add(-7, 'd').format('YYYY-MM-DDT11:00:00.000'))},
        };

        let orCondition = [];


        // orCondition.push({$or: [{'hdp.oddType': 'TTC1ST'}]});
        if (req.body.type === 'HT') {
            orCondition.push({$or: [{'hdp.oddType': 'AH1ST'}, {'hdp.oddType': 'OU1ST'}, {'hdp.oddType': 'OE1ST'}, {'hdp.oddType': 'X121ST'}, {'hdp.oddType': 'FHCS'}]});
        } else {
            orCondition.push({$and: [{'hdp.oddType': {$ne: 'TTC'}}, {'hdp.oddType': {$ne: 'TTC1ST'}}]});
        }

        if (orCondition.length > 0) {
            condition['$and'] = orCondition;
        }

        console.log(condition)

        BetTransactionModel.find(condition)
            .select('memberId memberUsername hdp commission amount memberCredit payout memberParentGroup betId status priceType betResult game gameType source')
            .populate({path: 'memberParentGroup', select: 'type'})
            .exec(function (err, response) {


                if (!response) {
                    callback(null, 'success');
                    return;
                }

                if (response.length === 0) {
                    callback(null, '');
                    return;
                }

                async.each(response, (betObj, asyncCallback) => {


                    if (betObj.status === 'REJECTED') {
                        let updateBody = {
                            $set: {
                                'isEndScore': true
                            }
                        };

                        BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                            if (err) {
                                asyncCallback(err, null);
                            } else {
                                asyncCallback(null, response);
                            }
                        });

                    } else {
                        //NOT EQUAL REJECTED

                        console.log("========================= : ", betObj.hdp.handicap);
                        let betResult;

                        let _homeScoreHT, _awayScoreHT, _homeScoreFT, _awayScoreFT;

                        if (betObj.hdp.matchType === 'LIVE' && ['AH1ST', 'AH'].includes(betObj.hdp.oddType)) {
                            _homeScoreHT = homeScoreHalf - Number.parseInt(betObj.hdp.score.split(':')[0]);
                            _awayScoreHT = awayScoreHalf - Number.parseInt(betObj.hdp.score.split(':')[1]);
                            _homeScoreFT = homeScoreFull - Number.parseInt(betObj.hdp.score.split(':')[0]);
                            _awayScoreFT = awayScoreFull - Number.parseInt(betObj.hdp.score.split(':')[1]);

                        } else {
                            _homeScoreHT = homeScoreHalf;
                            _awayScoreHT = awayScoreHalf;
                            _homeScoreFT = homeScoreFull;
                            _awayScoreFT = awayScoreFull;
                        }


                        if (['AH1ST', 'OU1ST', 'OE1ST', 'X121ST', 'FHCS', 'TTC1ST'].includes(betObj.hdp.oddType)) {
                            if (betObj.hdp.oddType === 'AH1ST') {
                                betResult = calculateHandicap(_homeScoreHT, _awayScoreHT, betObj.hdp.handicap, betObj.hdp.bet);
                            } else if (betObj.hdp.oddType === 'OU1ST' || betObj.hdp.oddType === 'TTC1ST') {
                                betResult = calculateOuScore(_homeScoreHT, _awayScoreHT, betObj.hdp.handicap, betObj.hdp.bet);
                            } else if (betObj.hdp.oddType === 'OE1ST') {
                                betResult = calculateOeScore(_homeScoreHT, _awayScoreHT, betObj.hdp.bet);
                            } else if (betObj.hdp.oddType === 'X121ST') {
                                betResult = calculateOneTwo(_homeScoreHT, _awayScoreHT, betObj.hdp.bet);
                            } else if (betObj.hdp.oddType === 'FHCS') {
                                betResult = calculateCorrectScore(_homeScoreHT, _awayScoreHT, betObj.hdp.bet);
                            }
                        } else {
                            if (betObj.hdp.oddType === 'AH') {
                                betResult = calculateHandicap(_homeScoreFT, _awayScoreFT, betObj.hdp.handicap, betObj.hdp.bet);
                            } else if (betObj.hdp.oddType === 'OU' || betObj.hdp.oddType === 'TTC') {
                                betResult = calculateOuScore(_homeScoreFT, _awayScoreFT, betObj.hdp.handicap, betObj.hdp.bet);
                            } else if (betObj.hdp.oddType === 'OE') {
                                betResult = calculateOeScore(_homeScoreFT, _awayScoreFT, betObj.hdp.bet);
                            } else if (betObj.hdp.oddType === 'X12' || betObj.hdp.oddType === 'ML') {
                                betResult = calculateOneTwo(_homeScoreFT, _awayScoreFT, betObj.hdp.bet);
                            } else if (betObj.hdp.oddType === 'T1OU') {
                                betResult = calculateOuScore(_homeScoreFT, 0, betObj.hdp.handicap, betObj.hdp.bet);
                            } else if (betObj.hdp.oddType === 'T2OU') {
                                betResult = calculateOuScore(0, _awayScoreFT, betObj.hdp.handicap, betObj.hdp.bet);
                            } else if (betObj.hdp.oddType === 'CS') {
                                betResult = calculateCorrectScore(_homeScoreFT, _awayScoreFT, betObj.hdp.bet);
                            } else if (betObj.hdp.oddType === 'TG') {
                                betResult = calculateTotalGoal(_homeScoreFT, _awayScoreFT, betObj.hdp.bet);
                            } else if (betObj.hdp.oddType === 'DC') {
                                betResult = calculateDoubleChance(_homeScoreFT, _awayScoreFT, betObj.hdp.bet);
                            } else if (betObj.hdp.oddType === 'FTHT') {
                                betResult = calculateFTHT(_homeScoreHT, _awayScoreHT, _homeScoreFT, _awayScoreFT, betObj.hdp.bet);
                            }


                        }

                        console.log('old betResult : ', betObj.betResult);
                        console.log("new betResult : ", betResult);

                        if (false && betResult === betObj.betResult) {
                            console.log('same result : update score only')
                            let updateBody = {
                                $set: {
                                    'hdp.fullScore': homeScoreFull + ':' + awayScoreFull,
                                    'hdp.halfScore': homeScoreHalf + ':' + awayScoreHalf,
                                }
                            };

                            BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                                if (err) {
                                    asyncCallback(err, null);
                                } else {
                                    asyncCallback(null, response);
                                }
                            });

                        } else {

                            let amount = Math.abs(betObj.memberCredit);
                            let validAmount;
                            let winLose = 0;
                            if (betResult === DRAW) {
                                winLose = 0;
                                validAmount = 0;
                            } else if (betResult === WIN) {
                                FormulaUtils.calculatePayout(betObj.amount, betObj.hdp.odd, betObj.hdp.oddType, betObj.priceType, (payout) => {
                                    winLose = Number.parseFloat(payout) - betObj.amount;
                                });
                                validAmount = betObj.amount;
                            } else if (betResult === HALF_WIN) {
                                FormulaUtils.calculatePayout(betObj.amount, betObj.hdp.odd, betObj.hdp.oddType, betObj.priceType, (payout) => {
                                    winLose = (Number.parseFloat(payout) - betObj.amount) / 2;
                                });
                                validAmount = betObj.amount / 2;
                            } else if (betResult === LOSE) {
                                winLose = 0 - amount;
                                validAmount = betObj.amount;
                            } else if (betResult === HALF_LOSE) {
                                winLose = 0 - (amount / 2);
                                validAmount = betObj.amount / 2;
                            }

                            const shareReceive = prepareShareReceive(winLose, betResult, betObj);

                            async.series([subCallback => {
                                //refundBalance
                                AgentService.rollbackCalculateAgentMemberBalance(betObj, req.body.key, (err, response) => {
                                    if (err) {
                                        subCallback(err, null);
                                    } else {
                                        subCallback(null, response);
                                    }
                                });

                            }, subCallback => {

                                let updateBody = {
                                    $set: {
                                        'hdp.fullScore': homeScoreFull + ':' + awayScoreFull,
                                        'hdp.halfScore': homeScoreHalf + ':' + awayScoreHalf,

                                        'commission.superAdmin.winLoseCom': shareReceive.superAdmin.winLoseCom,
                                        'commission.superAdmin.winLose': shareReceive.superAdmin.winLose,
                                        'commission.superAdmin.totalWinLoseCom': shareReceive.superAdmin.totalWinLoseCom,

                                        'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                                        'commission.company.winLose': shareReceive.company.winLose,
                                        'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                                        'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                                        'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                                        'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                                        'commission.api.winLoseCom': shareReceive.api.winLoseCom,
                                        'commission.api.winLose': shareReceive.api.winLose,
                                        'commission.api.totalWinLoseCom': shareReceive.api.totalWinLoseCom,

                                        'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                                        'commission.senior.winLose': shareReceive.senior.winLose,
                                        'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                                        'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                                        'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                                        'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                                        'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                                        'commission.agent.winLose': shareReceive.agent.winLose,
                                        'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                                        'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                                        'commission.member.winLose': shareReceive.member.winLose,
                                        'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                                        'validAmount': validAmount,
                                        'betResult': betResult,
                                        'isEndScore': true,
                                        'updatedDate': DateUtils.getCurrentDate(),
                                        'status': 'DONE'
                                    }
                                };

                                BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                                    if (err) {
                                        subCallback(err, null);
                                    } else {
                                            subCallback(null, response);

                                    }
                                });

                            }, subCallback => {
                                updateAgentMemberBalance(shareReceive, 'TODAY', req.body.key, (err, response) => {
                                    if (err) {
                                        subCallback(err, null);
                                    } else {
                                        subCallback(null, response);
                                    }
                                });
                            }], function (err, response) {
                                if (err) {
                                    asyncCallback(err, null);
                                } else {
                                    asyncCallback(null, response);
                                }
                            });
                        }
                    }

                }, (err) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, 'success');
                    }
                });//end forEach Match
            });


    }), (callback => {

        let condition = {
            game: 'FOOTBALL',
            gameType: {$ne: 'TODAY'},
            'parlay.matches.matchId': matchId,
            status: 'DONE',
            active: true,
            gameDate: {$gte: new Date(moment().add(-7, 'd').format('YYYY-MM-DDT11:00:00.000'))}
        };

        // homeScore
        // if (req.body.type === 'HT') {
        //     condition['$or'] = [{'parlay.matches.oddType': 'AH1ST'}, {'parlay.matches.oddType': 'OU1ST'}, {'parlay.matches.oddType': 'OE1ST'}, {'parlay.matches.oddType': 'X121ST'}]
        // }

        // condition.$or = [{$ne:{'hdp.oddType': 'TTC'}},{$ne:{'hdp.oddType': 'TTC1ST'}}];

        // condition['betId'] = 'BET1574946701143';

        BetTransactionModel.find(condition)
            .select('memberId memberUsername parlay commission amount memberCredit payout memberParentGroup status betId betResult game gameType source')
            .populate({path: 'memberParentGroup', select: 'type'})
            .exec(function (err, response) {

                if (!response) {
                    callback(null, 'success');
                    return;
                }

                async.each(response, (betObj, betListCallback) => {

                    const allMatch = betObj.parlay.matches;

                    let filterList = _.filter(allMatch, function (response) {
                        if (req.body.type === 'HT') {
                            return response.matchId === matchId && ['AH1ST', 'OU1ST', 'OE1ST', 'X121ST'].includes(response.oddType);
                        } else {
                            return response.matchId === matchId;
                        }
                    });


                    if (filterList.length === 0) {
                        betListCallback(null, 'not data found');
                        return;
                    }

                    let unCheckMatch = _.filter(allMatch, function (response) {
                        return response.matchId !== matchId && !response.betResult;
                    });


                    _.each(filterList, (match) => {

                        console.log("========================= : ", match.matchId);

                        let betResult;

                        let _homeScoreHT, _awayScoreHT, _homeScoreFT, _awayScoreFT;

                        if (match.matchType === 'LIVE' && ['AH1ST', 'AH'].includes(match.oddType)) {
                            _homeScoreHT = homeScoreHalf - Number.parseInt(match.score.split(':')[0]);
                            _awayScoreHT = awayScoreHalf - Number.parseInt(match.score.split(':')[1]);
                            _homeScoreFT = homeScoreFull - Number.parseInt(match.score.split(':')[0]);
                            _awayScoreFT = awayScoreFull - Number.parseInt(match.score.split(':')[1]);

                        } else {
                            _homeScoreHT = homeScoreHalf;
                            _awayScoreHT = awayScoreHalf;
                            _homeScoreFT = homeScoreFull;
                            _awayScoreFT = awayScoreFull;
                        }

                        if (['AH1ST', 'OU1ST', 'OE1ST', 'X121ST', 'FHCS', 'TTC1ST'].includes(match.oddType)) {
                            if (match.oddType === 'AH1ST') {
                                betResult = calculateHandicap(_homeScoreHT, _awayScoreHT, match.handicap, match.bet);
                            } else if (match.oddType === 'OU1ST' || match.oddType === 'TTC1ST') {
                                betResult = calculateOuScore(_homeScoreHT, _awayScoreHT, match.handicap, match.bet);
                            } else if (match.oddType === 'OE1ST') {
                                betResult = calculateOeScore(_homeScoreHT, _awayScoreHT, match.bet);
                            } else if (match.oddType === 'X121ST') {
                                betResult = calculateOneTwo(_homeScoreHT, _awayScoreHT, match.bet);
                            } else if (betObj.hdp.oddType === 'FHCS') {
                                betResult = calculateCorrectScore(_homeScoreHT, _awayScoreHT, match.bet);
                            }
                        } else {
                            if (match.oddType === 'AH') {
                                betResult = calculateHandicap(_homeScoreFT, _awayScoreFT, match.handicap, match.bet);
                            } else if (match.oddType === 'OU' || match.oddType === 'TTC') {
                                betResult = calculateOuScore(_homeScoreFT, _awayScoreFT, match.handicap, match.bet);
                            } else if (match.oddType === 'OE') {
                                betResult = calculateOeScore(_homeScoreFT, _awayScoreFT, match.bet);
                            } else if (match.oddType === 'X12' || match.oddType === 'ML') {
                                betResult = calculateOneTwo(_homeScoreFT, _awayScoreFT, match.bet);
                            } else if (match.oddType === 'T1OU') {
                                betResult = calculateOuScore(_homeScoreFT, 0, match.handicap, match.bet);
                            } else if (match.oddType === 'T2OU') {
                                betResult = calculateOuScore(0, _awayScoreFT, match.handicap, match.bet);
                            }
                        }

                        let updateBody;
                        let isCalculateReceive = false;
                        let isRefundBalance = false;
                        let winLoseAmount = 0;
                        let shareReceive;

                        if (false /*match.betResult === betResult*/) {
                            console.log('same result : update score only');
                            updateBody = {
                                $set: {
                                    'parlay.matches.$.fullScore': homeScoreFull + ':' + awayScoreFull,
                                    'parlay.matches.$.halfScore': homeScoreHalf + ':' + awayScoreHalf,
                                    'parlay.matches.$.betResult': betResult,
                                    // 'betResult': betResult,
                                }
                            };

                        } else {

                            let isUnCheckMatchLose = _.filter(allMatch, function (response) {
                                    return response.matchId !== matchId && response.betResult === LOSE;
                                }).length > 0;

                            if (isUnCheckMatchLose || betResult === LOSE) {
                                isRefundBalance = true;
                                isCalculateReceive = true;
                                winLoseAmount = 0 - betObj.amount;
                                shareReceive = prepareShareReceive(winLoseAmount, LOSE, betObj);
                                updateBody = {
                                    $set: {
                                        'parlay.matches.$.fullScore': homeScoreFull + ':' + awayScoreFull,
                                        'parlay.matches.$.halfScore': homeScoreHalf + ':' + awayScoreHalf,
                                        'parlay.matches.$.betResult': betResult,

                                        'commission.superAdmin.winLoseCom': shareReceive.superAdmin.winLoseCom,
                                        'commission.superAdmin.winLose': shareReceive.superAdmin.winLose,
                                        'commission.superAdmin.totalWinLoseCom': shareReceive.superAdmin.totalWinLoseCom,

                                        'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                                        'commission.company.winLose': shareReceive.company.winLose,
                                        'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                                        'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                                        'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                                        'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                                        'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                                        'commission.senior.winLose': shareReceive.senior.winLose,
                                        'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                                        'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                                        'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                                        'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                                        'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                                        'commission.agent.winLose': shareReceive.agent.winLose,
                                        'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                                        'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                                        'commission.member.winLose': shareReceive.member.winLose,
                                        'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                                        'validAmount': betObj.amount,
                                        'betResult': betResult,
                                        'isEndScore': true,
                                        'status': 'DONE',
                                        'settleDate': DateUtils.getCurrentDate()
                                    }
                                };
                            } else {
                                // NOT EQUALS LOSE

                                if (unCheckMatch.length == 0) {
                                    console.log('no unCheck match');
                                    isRefundBalance = true;
                                    isCalculateReceive = true;

                                    let totalUnCheckOdds = _.filter(allMatch, function (response) {
                                        return response.matchId !== matchId && response.betResult;
                                    }).map((item) => {
                                        return calculateParlayOdds(item.betResult, item.odd);
                                    });

                                    console.log(' success match');
                                    console.log(totalUnCheckOdds);

                                    let sumTotalUnCheckOdds = _.reduce(totalUnCheckOdds, (memo, item) => {
                                        return {sum: memo.sum * item};
                                    }, {sum: 1}).sum;


                                    let halfLoseCount = _.filter(allMatch, function (response) {
                                        return response.betResult === HALF_LOSE && response.matchId !== matchId;
                                    }).length;

                                    //prepare with last match result
                                    sumTotalUnCheckOdds = roundTo.down(sumTotalUnCheckOdds * calculateParlayOdds(betResult, match.odd), 3);

                                    if (betResult === HALF_LOSE) {
                                        halfLoseCount += 1;
                                    }

                                    console.log('sumTotalUnCheckOdds : ', sumTotalUnCheckOdds);
                                    console.log('halfLoseCount : ', halfLoseCount);

                                    winLoseAmount = (subPlyAmount(betObj.amount, halfLoseCount) * sumTotalUnCheckOdds);
                                    let validAmount = subPlyAmount(betObj.amount, halfLoseCount);

                                    winLoseAmount = winLoseAmount - betObj.amount;

                                    shareReceive = prepareShareReceive(winLoseAmount, WIN, betObj);
                                    updateBody = {
                                        $set: {
                                            'parlay.matches.$.fullScore': homeScoreFull + ':' + awayScoreFull,
                                            'parlay.matches.$.halfScore': homeScoreHalf + ':' + awayScoreHalf,
                                            'parlay.matches.$.betResult': betResult,
                                            'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                                            'commission.company.winLose': shareReceive.company.winLose,
                                            'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                                            'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                                            'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                                            'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                                            'commission.api.winLoseCom': shareReceive.api.winLoseCom,
                                            'commission.api.winLose': shareReceive.api.winLose,
                                            'commission.api.totalWinLoseCom': shareReceive.api.totalWinLoseCom,

                                            'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                                            'commission.senior.winLose': shareReceive.senior.winLose,
                                            'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                                            'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                                            'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                                            'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                                            'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                                            'commission.agent.winLose': shareReceive.agent.winLose,
                                            'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                                            'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                                            'commission.member.winLose': shareReceive.member.winLose,
                                            'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                                            'validAmount': validAmount,
                                            'betResult': 'WIN',
                                            'isEndScore': true,
                                            'status': 'DONE'
                                        }
                                    };
                                    updateBody.$set.status = 'DONE';

                                } else {
                                    updateBody = {
                                        $set: {
                                            'parlay.matches.$.fullScore': homeScoreFull + ':' + awayScoreFull,
                                            'parlay.matches.$.halfScore': homeScoreHalf + ':' + awayScoreHalf,
                                            'parlay.matches.$.betResult': betResult,
                                            // 'betResult': betResult,
                                        }
                                    };

                                    let unCheckMatch = _.filter(allMatch, function (response) {
                                        return response.matchId !== matchId && !response.betResult;
                                    });

                                    let unCheckMatchLose = _.filter(allMatch, function (response) {
                                        return response.matchId !== matchId && response.betResult === LOSE;
                                    });


                                    if (unCheckMatch.length > 0 && unCheckMatchLose.length == 0) {
                                        isRefundBalance = true;
                                        updateBody = {
                                            $set: {
                                                'status': 'RUNNING',
                                                'parlay.matches.$.fullScore': homeScoreFull + ':' + awayScoreFull,
                                                'parlay.matches.$.halfScore': homeScoreHalf + ':' + awayScoreHalf,
                                                'parlay.matches.$.betResult': betResult,
                                                // 'betResult': betResult,
                                            }
                                        };
                                    }


                                }
                            }
                        }


                        console.log(updateBody);

                        async.series([callback => {
                            //refundBalance
                            if (isRefundBalance) {
                                AgentService.rollbackCalculateAgentMemberBalance(betObj, req.body.key, (err, response) => {
                                    if (err) {
                                        callback(err, null);
                                    } else {
                                        callback(null, response);
                                    }
                                });
                            } else {
                                callback(null, '');
                            }

                        }, callback => {

                            BetTransactionModel.update({
                                _id: betObj._id,
                                'parlay.matches._id': match._id
                            }, updateBody, function (err, response) {
                                if (err) {
                                    callback(err, null);
                                } else {

                                    if(isCalculateReceive){
                                            callback(null, response);
                                    }else {
                                        callback(null, response);
                                    }
                                }
                            });

                        }, callback => {
                            if (isCalculateReceive) {
                                updateAgentMemberBalance(shareReceive, 'PARLAY', req.body.key, (err, response) => {
                                    if (err) {
                                        callback(err, null);
                                    } else {
                                        callback(null, response);
                                    }
                                });
                            } else {
                                callback(null, '');
                            }
                        }], function (err, response) {
                            if (err) {
                                betListCallback(err, null);
                            } else {
                                betListCallback(null, response);
                            }
                        });

                    });//end loop match in parlay

                }, (err) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, 'success');
                    }
                });//end forEach Match
            });


    })], (err, asyncResponse) => {

        if (err)
            return res.send({message: "error", results: err, code: 999});

        return res.send(
            {
                code: 0,
                message: "success",
                result: 'success'
            }
        );

    });

});

const endScoreBasketBallSchema = Joi.object().keys({
    key: Joi.string().required(),
    matchId: Joi.string().required(),
    homeScoreHT: Joi.number().allow(''),
    awayScoreHT: Joi.number().allow(''),
    homeScoreFT: Joi.number().allow(''),
    awayScoreFT: Joi.number().allow(''),
    type: Joi.string().required()
});
router.post('/basketball', function (req, res) {


    let validate = Joi.validate(req.body, endScoreBasketBallSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 1004
        });
    }

    endScoreFunction(req.body, 'BASKETBALL', false, (err, response) => {
        if (err)
            return res.send({message: "error", results: err, code: 999});

        return res.send(
            {
                code: 0,
                message: "success",
                result: 'success'
            }
        );
    });
});
const reCalculateEndScoreBasketBallSchema = Joi.object().keys({
    key: Joi.string().required(),
    matchId: Joi.string().required(),
    homeScoreHT: Joi.number().allow(''),
    awayScoreHT: Joi.number().allow(''),
    homeScoreFT: Joi.number().allow(''),
    awayScoreFT: Joi.number().allow(''),
    type: Joi.string().required()
});
router.post('/re-calculate/basketball', function (req, res) {


    let validate = Joi.validate(req.body, reCalculateEndScoreBasketBallSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 1004
        });
    }


    reEndScoreFunction(req.body, 'BASKETBALL', (err, response) => {
        if (err)
            return res.send({message: "error", results: err, code: 999});

        return res.send(
            {
                code: 0,
                message: "success",
                result: 'success'
            }
        );
    });


});

const endScoreTennisSchema = Joi.object().keys({
    key: Joi.string().required(),
    matchId: Joi.string().required(),
    homeScoreHT: Joi.number().allow(''),
    awayScoreHT: Joi.number().allow(''),
    homeScoreFT: Joi.number().allow(''),
    awayScoreFT: Joi.number().allow(''),
    type: Joi.string().required()
});
router.post('/tennis', function (req, res) {
    let validate = Joi.validate(req.body, endScoreTennisSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 1004
        });
    }

    endScoreFunction(req.body, 'TENNIS', false, (err, response) => {
        if (err) {
            return res.send({message: "error", results: err, code: 999});
        } else {
            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: 'success'
                }
            );
        }
    });
});
const reCalculateEndScoreTennisSchema = Joi.object().keys({
    key: Joi.string().required(),
    matchId: Joi.string().required(),
    homeScoreHT: Joi.number().allow(''),
    awayScoreHT: Joi.number().allow(''),
    homeScoreFT: Joi.number().allow(''),
    awayScoreFT: Joi.number().allow(''),
    type: Joi.string().required()
});
router.post('/re-calculate/tennis', function (req, res) {


    let validate = Joi.validate(req.body, reCalculateEndScoreTennisSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 1004
        });
    }


    reEndScoreFunction(req.body, 'TENNIS', (err, response) => {
        if (err)
            return res.send({message: "error", results: err, code: 999});

        return res.send(
            {
                code: 0,
                message: "success",
                result: 'success'
            }
        );
    });


});

const endScoreESportSchema = Joi.object().keys({
    key: Joi.string().required(),
    matchId: Joi.string().required(),
    homeScoreHT: Joi.number().allow(''),
    awayScoreHT: Joi.number().allow(''),
    homeScoreFT: Joi.number().allow(''),
    awayScoreFT: Joi.number().allow(''),
    type: Joi.string().required()
});
router.post('/esport', function (req, res) {
    let validate = Joi.validate(req.body, endScoreESportSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 1004
        });
    }

    endScoreFunction(req.body, 'ESPORT', false, (err, response) => {
        if (err) {
            return res.send({message: "error", results: err, code: 999});
        } else {
            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: 'success'
                }
            );
        }
    });
});
const reCalculateEndScoreESportSchema = Joi.object().keys({
    key: Joi.string().required(),
    matchId: Joi.string().required(),
    homeScoreHT: Joi.number().allow(''),
    awayScoreHT: Joi.number().allow(''),
    homeScoreFT: Joi.number().allow(''),
    awayScoreFT: Joi.number().allow(''),
    type: Joi.string().required()
});
router.post('/re-calculate/esport', function (req, res) {


    let validate = Joi.validate(req.body, reCalculateEndScoreESportSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 1004
        });
    }


    reEndScoreFunction(req.body, 'ESPORT', (err, response) => {
        if (err)
            return res.send({message: "error", results: err, code: 999});

        return res.send(
            {
                code: 0,
                message: "success",
                result: 'success'
            }
        );
    });


});

const endScoreTableTennisSchema = Joi.object().keys({
    key: Joi.string().required(),
    matchId: Joi.string().required(),
    homeScoreHT: Joi.number().allow(''),
    awayScoreHT: Joi.number().allow(''),
    homeScoreFT: Joi.number().allow(''),
    awayScoreFT: Joi.number().allow(''),
    type: Joi.string().required()
});
router.post('/tableTennis', function (req, res) {
    let validate = Joi.validate(req.body, endScoreTableTennisSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 1004
        });
    }

    endScoreFunction(req.body, 'TABLE_TENNIS', false, (err, response) => {
        if (err) {
            return res.send({message: "error", results: err, code: 999});
        } else {
            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: 'success'
                }
            );
        }
    });
});
const reCalculateEndScoreTableTennisSchema = Joi.object().keys({
    key: Joi.string().required(),
    matchId: Joi.string().required(),
    homeScoreHT: Joi.number().allow(''),
    awayScoreHT: Joi.number().allow(''),
    homeScoreFT: Joi.number().allow(''),
    awayScoreFT: Joi.number().allow(''),
    type: Joi.string().required()
});
router.post('/re-calculate/tableTennis', function (req, res) {


    let validate = Joi.validate(req.body, reCalculateEndScoreTableTennisSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 1004
        });
    }


    reEndScoreFunction(req.body, 'TABLE_TENNIS', (err, response) => {
        if (err)
            return res.send({message: "error", results: err, code: 999});

        return res.send(
            {
                code: 0,
                message: "success",
                result: 'success'
            }
        );
    });


});

const endScoreVolleyballSchema = Joi.object().keys({
    key: Joi.string().required(),
    matchId: Joi.string().required(),
    homeScoreHT: Joi.number().allow(''),
    awayScoreHT: Joi.number().allow(''),
    homeScoreFT: Joi.number().allow(''),
    awayScoreFT: Joi.number().allow(''),
    type: Joi.string().required()
});
router.post('/volleyball', function (req, res) {
    let validate = Joi.validate(req.body, endScoreVolleyballSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 1004
        });
    }

    endScoreFunction(req.body, 'VOLLEYBALL', false, (err, response) => {
        if (err) {
            return res.send({message: "error", results: err, code: 999});
        } else {
            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: 'success'
                }
            );
        }
    });
});
const reCalculateEndScoreVolleyballSchema = Joi.object().keys({
    key: Joi.string().required(),
    matchId: Joi.string().required(),
    homeScoreHT: Joi.number().allow(''),
    awayScoreHT: Joi.number().allow(''),
    homeScoreFT: Joi.number().allow(''),
    awayScoreFT: Joi.number().allow(''),
    type: Joi.string().required()
});
router.post('/re-calculate/volleyball', function (req, res) {


    let validate = Joi.validate(req.body, reCalculateEndScoreVolleyballSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 1004
        });
    }


    reEndScoreFunction(req.body, 'VOLLEYBALL', (err, response) => {
        if (err)
            return res.send({message: "error", results: err, code: 999});

        return res.send(
            {
                code: 0,
                message: "success",
                result: 'success'
            }
        );
    });


});

const endScoreRugbySchema = Joi.object().keys({
    key: Joi.string().required(),
    matchId: Joi.string().required(),
    homeScoreHT: Joi.number().allow(''),
    awayScoreHT: Joi.number().allow(''),
    homeScoreFT: Joi.number().allow(''),
    awayScoreFT: Joi.number().allow(''),
    type: Joi.string().required()
});
router.post('/rugby', function (req, res) {
    let validate = Joi.validate(req.body, endScoreRugbySchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 1004
        });
    }

    endScoreFunction(req.body, 'RUGBY', false, (err, response) => {
        if (err) {
            return res.send({message: "error", results: err, code: 999});
        } else {
            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: 'success'
                }
            );
        }
    });
});
const reCalculateEndScoreRugbySchema = Joi.object().keys({
    key: Joi.string().required(),
    matchId: Joi.string().required(),
    homeScoreHT: Joi.number().allow(''),
    awayScoreHT: Joi.number().allow(''),
    homeScoreFT: Joi.number().allow(''),
    awayScoreFT: Joi.number().allow(''),
    type: Joi.string().required()
});
router.post('/re-calculate/rugby', function (req, res) {


    let validate = Joi.validate(req.body, reCalculateEndScoreRugbySchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 1004
        });
    }


    reEndScoreFunction(req.body, 'RUGBY', (err, response) => {
        if (err)
            return res.send({message: "error", results: err, code: 999});

        return res.send(
            {
                code: 0,
                message: "success",
                result: 'success'
            }
        );
    });


});

const endScoreMuayThaiSchema = Joi.object().keys({
    key: Joi.string().required(),
    matchId: Joi.string().required(),
    result: Joi.string().required(),
    round: Joi.number().required()
});
router.post('/muaythai', function (req, res) {

    let validate = Joi.validate(req.body, endScoreMuayThaiSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    // let model = EndScoreHistoryModel(req.body);
    // model.createdDate = moment().utc(true).toDate();
    // model.save(function (err, response) {
    //     console.log('save endscore history')
    // });


    endScoreMuayThaiFunction(req.body, (err, response) => {
        if (err)
            return res.send({message: "error", results: err, code: 999});

        return res.send({
            code: 0,
            message: "success",
            result: 'success'
        });
    });

});
const reCalculateEndScoreMuayThaiSchema = Joi.object().keys({
    key: Joi.string().required(),
    matchId: Joi.string().required(),
    result: Joi.string().required(),
    round: Joi.number().required()
});
router.post('/re-calculate/muaythai', function (req, res) {


    let validate = Joi.validate(req.body, reCalculateEndScoreMuayThaiSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 1004
        });
    }

    reEndScoreMuayThaiFunction(req.body, (err, response) => {
        if (err)
            return res.send({message: "error", results: err, code: 999});

        return res.send(
            {
                code: 0,
                message: "success",
                result: 'success'
            }
        );
    });


});


const endScoreSchema = Joi.object().keys({
    key: Joi.string().required(),
    matchId: Joi.string().required(),
    homeScoreHT: Joi.number().allow(''),
    awayScoreHT: Joi.number().allow(''),
    homeScoreFT: Joi.number().allow(''),
    awayScoreFT: Joi.number().allow(''),
    type: Joi.string().required(),
    md5: Joi.string()
});

router.post('/', function (req, res) {

    let validate = Joi.validate(req.body, endScoreSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    const {
        type,
        matchId
    } = req.body;

    parallel([
            (callback) => {
                Redis.getByKey('sboLive', (error, response) => {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            },

            (callback) => {
                Redis.getByKey('sboToday', (error, response) => {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            },
        ],
        (error, parallelResult) => {
            if (error) {
                return res.send({
                    code: 9999,
                    message: error
                });
            } else {
                let [
                    sboLive,
                    sboToday,
                ] = parallelResult;
                if (_.isEqual(type.toUpperCase(), 'HT')) {
                    const league_key = parseInt(_.first(matchId.split(':')));
                    const match_key = parseInt(_.last(matchId.split(':')));
                    // console.log(league_key);
                    // console.log(match_key);
                    _.chain(sboLive)
                        .flatten(true)
                        .findWhere(JSON.parse(`{"k":${league_key}}`))
                        .pick('m')
                        .map(obj => {
                            return obj;
                        })
                        .flatten(true)
                        .findWhere(JSON.parse(`{"id":"${league_key}:${match_key}"}`))
                        .tap(obj => {
                            try {
                                if (!obj.i.lt.includes("1H")) {
                                    let model = EndScoreHistoryModel(req.body);
                                    model.createdDate = moment().utc(true).toDate();
                                    model.remark = "100000006";
                                    model.save(function (err, response) {
                                        console.log('save endscore history')
                                    });

                                    endScoreFunction(req.body, 'FOOTBALL', false, (err, response) => {
                                        if (err)
                                            return res.send({
                                                message: "error",
                                                results: err,
                                                code: 999
                                            });

                                        return res.send(
                                            {
                                                code: 0,
                                                message: "success",
                                                result: 'success'
                                            }
                                        );
                                    });
                                    // endScoreFunctionByMD5(req.body, 'FOOTBALL', false, (err, response) => {
                                    //     if (err)
                                    //         return res.send({
                                    //             message: "error",
                                    //             results: err,
                                    //             code: 999
                                    //         });
                                    //
                                    //     return res.send(
                                    //         {
                                    //             code: 0,
                                    //             message: "success",
                                    //             result: 'success'
                                    //         }
                                    //     );
                                    // });
                                }
                            } catch (e) {
                                console.log(obj);
                                const now_date = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
                                const ht_date = moment(new Date(obj.d)).add(+45, 'minute').toDate();
                                console.log('^^^^^^^^^^^^^^^^^^^^^now_date');
                                console.log(now_date);
                                console.log(ht_date);
                                console.log('^^^^^^^^^^^^^^^^^^^^^ht_date');
                                if (now_date >= ht_date) {
                                    let model = EndScoreHistoryModel(req.body);
                                    model.createdDate = moment().utc(true).toDate();
                                    model.remark = "100000009";
                                    model.save(function (err, response) {
                                        console.log('save endscore history')
                                    });

                                    endScoreFunction(req.body, 'FOOTBALL', false, (err, response) => {
                                        if (err)
                                            return res.send({
                                                message: "error",
                                                results: err,
                                                code: 999
                                            });

                                        return res.send(
                                            {
                                                code: 0,
                                                message: "success",
                                                result: 'success'
                                            }
                                        );
                                    });
                                    // endScoreFunctionByMD5(req.body, 'FOOTBALL', false, (err, response) => {
                                    //     if (err)
                                    //         return res.send({
                                    //             message: "error",
                                    //             results: err,
                                    //             code: 999
                                    //         });
                                    //
                                    //     return res.send(
                                    //         {
                                    //             code: 0,
                                    //             message: "success",
                                    //             result: 'success'
                                    //         }
                                    //     );
                                    // });
                                } else {
                                    console.error(e);
                                }
                            }
                        });
                } else {
                    const sboTodayK = _.chain(sboToday)
                        .flatten(true)
                        .pluck('m')
                        .flatten(true)
                        .pluck('id')
                        .value();

                    const sboLiveK = _.chain(sboLive)
                        .flatten(true)
                        .pluck('m')
                        .flatten(true)
                        .pluck('id')
                        .value();

                    sboTodayK.push(...sboLiveK);
                    if (!_.contains(sboTodayK, matchId)) {
                        let model = EndScoreHistoryModel(req.body);
                        model.remark = "100000007";
                        model.createdDate = moment().utc(true).toDate();
                        model.save(function (err, response) {
                            console.log('save endscore history')
                        });

                        endScoreFunction(req.body, 'FOOTBALL', false, (err, response) => {
                            if (err)
                                return res.send({message: "error", results: err, code: 999});

                            return res.send(
                                {
                                    code: 0,
                                    message: "success",
                                    result: 'success'
                                }
                            );
                        });
                    }
                }
            }
        });
});

const reScoreM2Schema = Joi.object().keys({
    key: Joi.string().required(),
    matchId: Joi.string().required(),
    result: Joi.string().required(),
    round: Joi.number().required()
});

router.post('/re-calculate-m2', function (req, res) {

    let validate = Joi.validate(req.body, reScoreM2Schema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    let matchId = req.body.matchId;

    async.parallel([(callback => {

        let condition = {game:'M2',
            'hdp.matchId': matchId,
            'status':{$in:['DONE','REJECTED']},
            'gameType': 'TODAY',
            active: true};

        let orCondition = [];

        orCondition.push({$or: [{'status': 'DONE'}, {'status': 'REJECTED'}]});

        console.log(condition)

        BetTransactionModel.find(condition)
            .select('memberId memberUsername hdp commission amount memberCredit payout memberParentGroup betId status priceType betResult game gameType source')
            .populate({path: 'memberParentGroup', select: 'type'})
            .exec(function (err, response) {


                if (!response) {
                    callback(null, 'success');
                    return;
                }

                if (response.length === 0) {
                    callback(null, '');
                    return;
                }

                async.each(response, (betObj, asyncCallback) => {


                    if (betObj.status === 'REJECTED') {
                        let updateBody = {
                            $set: {
                                'isEndScore': true
                            }
                        };

                        BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                            if (err) {
                                asyncCallback(err, null);
                            } else {
                                asyncCallback(null, response);
                            }
                        });

                    } else {
                        //NOT EQUAL REJECTED

                        console.log("========================= : ", betObj.hdp.handicap);
                        let betResult;

                        // let _homeScoreHT, _awayScoreHT, _homeScoreFT, _awayScoreFT;
                        //
                        // if (betObj.hdp.matchType === 'LIVE' && ['AH1ST', 'AH'].includes(betObj.hdp.oddType)) {
                        //     _homeScoreHT = homeScoreHalf - Number.parseInt(betObj.hdp.score.split(':')[0]);
                        //     _awayScoreHT = awayScoreHalf - Number.parseInt(betObj.hdp.score.split(':')[1]);
                        //     _homeScoreFT = homeScoreFull - Number.parseInt(betObj.hdp.score.split(':')[0]);
                        //     _awayScoreFT = awayScoreFull - Number.parseInt(betObj.hdp.score.split(':')[1]);
                        //
                        // } else {
                        //     _homeScoreHT = homeScoreHalf;
                        //     _awayScoreHT = awayScoreHalf;
                        //     _homeScoreFT = homeScoreFull;
                        //     _awayScoreFT = awayScoreFull;
                        // }


                        // if (['AH1ST', 'OU1ST', 'OE1ST', 'X121ST', 'FHCS', 'TTC1ST'].includes(betObj.hdp.oddType)) {
                        //     if (betObj.hdp.oddType === 'AH1ST') {
                        //         betResult = calculateHandicap(_homeScoreHT, _awayScoreHT, betObj.hdp.handicap, betObj.hdp.bet);
                        //     } else if (betObj.hdp.oddType === 'OU1ST' || betObj.hdp.oddType === 'TTC1ST') {
                        //         betResult = calculateOuScore(_homeScoreHT, _awayScoreHT, betObj.hdp.handicap, betObj.hdp.bet);
                        //     } else if (betObj.hdp.oddType === 'OE1ST') {
                        //         betResult = calculateOeScore(_homeScoreHT, _awayScoreHT, betObj.hdp.bet);
                        //     } else if (betObj.hdp.oddType === 'X121ST') {
                        //         betResult = calculateOneTwo(_homeScoreHT, _awayScoreHT, betObj.hdp.bet);
                        //     } else if (betObj.hdp.oddType === 'FHCS') {
                        //         betResult = calculateCorrectScore(_homeScoreHT, _awayScoreHT, betObj.hdp.bet);
                        //     }
                        // } else {
                        //     if (betObj.hdp.oddType === 'AH') {
                        //         betResult = calculateHandicap(_homeScoreFT, _awayScoreFT, betObj.hdp.handicap, betObj.hdp.bet);
                        //     } else if (betObj.hdp.oddType === 'OU' || betObj.hdp.oddType === 'TTC') {
                        //         betResult = calculateOuScore(_homeScoreFT, _awayScoreFT, betObj.hdp.handicap, betObj.hdp.bet);
                        //     } else if (betObj.hdp.oddType === 'OE') {
                        //         betResult = calculateOeScore(_homeScoreFT, _awayScoreFT, betObj.hdp.bet);
                        //     } else if (betObj.hdp.oddType === 'X12' || betObj.hdp.oddType === 'ML') {
                        //         betResult = calculateOneTwo(_homeScoreFT, _awayScoreFT, betObj.hdp.bet);
                        //     } else if (betObj.hdp.oddType === 'T1OU') {
                        //         betResult = calculateOuScore(_homeScoreFT, 0, betObj.hdp.handicap, betObj.hdp.bet);
                        //     } else if (betObj.hdp.oddType === 'T2OU') {
                        //         betResult = calculateOuScore(0, _awayScoreFT, betObj.hdp.handicap, betObj.hdp.bet);
                        //     } else if (betObj.hdp.oddType === 'CS') {
                        //         betResult = calculateCorrectScore(_homeScoreFT, _awayScoreFT, betObj.hdp.bet);
                        //     } else if (betObj.hdp.oddType === 'TG') {
                        //         betResult = calculateTotalGoal(_homeScoreFT, _awayScoreFT, betObj.hdp.bet);
                        //     } else if (betObj.hdp.oddType === 'DC') {
                        //         betResult = calculateDoubleChance(_homeScoreFT, _awayScoreFT, betObj.hdp.bet);
                        //     } else if (betObj.hdp.oddType === 'FTHT') {
                        //         betResult = calculateFTHT(_homeScoreHT, _awayScoreHT, _homeScoreFT, _awayScoreFT, betObj.hdp.bet);
                        //     }
                        //
                        //
                        // }

                        if (req.body.result === betObj.hdp.bet) {
                            betResult = 'WIN'
                        } else if (req.body.result === 'DRAW') {
                            betResult = 'DRAW';
                        } else {
                            betResult = 'LOSE'
                        }

                        console.log('old betResult : ', betObj.betResult);
                        console.log("new betResult : ", betResult);

                        if (false && betResult === betObj.betResult) {
                            console.log('same result : update score only')
                            // let updateBody = {
                            //     $set: {
                            //         'hdp.fullScore': homeScoreFull + ':' + awayScoreFull,
                            //         'hdp.halfScore': homeScoreHalf + ':' + awayScoreHalf,
                            //     }
                            // };
                            //
                            // BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                            //     if (err) {
                            //         asyncCallback(err, null);
                            //     } else {
                            //         asyncCallback(null, response);
                            //     }
                            // });

                        } else {

                            let amount = Math.abs(betObj.memberCredit);
                            let validAmount;
                            let winLose = 0;
                            if (betResult === 'DRAW') {
                                winLose = 0;
                                validAmount = 0;
                            } else if (betResult === 'WIN') {
                                FormulaUtils.calculatePayout(betObj.amount, betObj.hdp.odd, betObj.hdp.oddType, betObj.priceType, (payout) => {
                                    winLose = Number.parseFloat(payout) - betObj.amount;
                                });
                                validAmount = betObj.amount;
                            } else {
                                winLose = 0 - amount;
                                validAmount = betObj.amount;
                            }

                            const shareReceive = prepareShareReceive(winLose, betResult, betObj);

                            async.series([subCallback => {
                                //refundBalance
                                AgentService.rollbackCalculateAgentMemberBalance(betObj, req.body.key, (err, response) => {
                                    if (err) {
                                        subCallback(err, null);
                                    } else {
                                        subCallback(null, response);
                                    }
                                });

                            }, subCallback => {

                                let updateBody = {
                                    $set: {
                                        // 'hdp.fullScore': homeScoreFull + ':' + awayScoreFull,
                                        'hdp.score': req.body.score + ':' + req.body.score,

                                        'commission.superAdmin.winLoseCom': shareReceive.superAdmin.winLoseCom,
                                        'commission.superAdmin.winLose': shareReceive.superAdmin.winLose,
                                        'commission.superAdmin.totalWinLoseCom': shareReceive.superAdmin.totalWinLoseCom,

                                        'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                                        'commission.company.winLose': shareReceive.company.winLose,
                                        'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                                        'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                                        'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                                        'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                                        'commission.api.winLoseCom': shareReceive.api.winLoseCom,
                                        'commission.api.winLose': shareReceive.api.winLose,
                                        'commission.api.totalWinLoseCom': shareReceive.api.totalWinLoseCom,

                                        'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                                        'commission.senior.winLose': shareReceive.senior.winLose,
                                        'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                                        'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                                        'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                                        'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                                        'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                                        'commission.agent.winLose': shareReceive.agent.winLose,
                                        'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                                        'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                                        'commission.member.winLose': shareReceive.member.winLose,
                                        'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                                        'validAmount': validAmount,
                                        'betResult': betResult,
                                        'isEndScore': true,
                                        'status': 'DONE'
                                    }
                                };

                                BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                                    if (err) {
                                        subCallback(err, null);
                                    } else {
                                        subCallback(null, response);
                                    }
                                });

                            }, subCallback => {
                                updateAgentMemberBalance(shareReceive, 'TODAY', req.body.key, (err, response) => {
                                    if (err) {
                                        subCallback(err, null);
                                    } else {
                                        subCallback(null, response);
                                    }
                                });
                            }], function (err, response) {
                                if (err) {
                                    asyncCallback(err, null);
                                } else {
                                    asyncCallback(null, response);
                                }
                            });
                        }
                    }

                }, (err) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, 'success');
                    }
                });//end forEach Match
            });


    }), (callback => {

        let condition = {
            game:'M2',
            'gameType': {$ne: 'TODAY'},
            'parlay.matches.matchId':matchId,
            status:'DONE',
            active: true
        };

        // homeScore
        // if (req.body.type === 'HT') {
        //     condition['$or'] = [{'parlay.matches.oddType': 'AH1ST'}, {'parlay.matches.oddType': 'OU1ST'}, {'parlay.matches.oddType': 'OE1ST'}, {'parlay.matches.oddType': 'X121ST'}]
        // }
        // condition.$or = [{$ne:{'hdp.oddType': 'TTC'}},{$ne:{'hdp.oddType': 'TTC1ST'}}];

        BetTransactionModel.find(condition)
            .select('memberId memberUsername parlay commission amount memberCredit payout memberParentGroup status betId betResult game gameType source')
            .populate({path: 'memberParentGroup', select: 'type'})
            .exec(function (err, response) {

                if (!response) {
                    callback(null, 'success');
                    return;
                }

                async.each(response, (betObj, betListCallback) => {

                    const allMatch = betObj.parlay.matches;

                    let filterList = _.filter(allMatch, function (response) {
                        return response.matchId === matchId;
                    });


                    if (filterList.length === 0) {
                        betListCallback(null, 'not data found');
                        return;
                    }

                    let unCheckMatch = _.filter(allMatch, function (response) {
                        return response.matchId !== matchId && !response.betResult;
                    });


                    _.each(filterList, (match) => {

                        console.log("========================= : ", match.matchId);

                        let betResult;

                        // let _homeScoreHT, _awayScoreHT, _homeScoreFT, _awayScoreFT;
                        //
                        // if (match.matchType === 'LIVE' && ['AH1ST', 'AH'].includes(match.oddType)) {
                        //     _homeScoreHT = homeScoreHalf - Number.parseInt(match.score.split(':')[0]);
                        //     _awayScoreHT = awayScoreHalf - Number.parseInt(match.score.split(':')[1]);
                        //     _homeScoreFT = homeScoreFull - Number.parseInt(match.score.split(':')[0]);
                        //     _awayScoreFT = awayScoreFull - Number.parseInt(match.score.split(':')[1]);
                        //
                        // } else {
                        //     _homeScoreHT = homeScoreHalf;
                        //     _awayScoreHT = awayScoreHalf;
                        //     _homeScoreFT = homeScoreFull;
                        //     _awayScoreFT = awayScoreFull;
                        // }

                        // if (['AH1ST', 'OU1ST', 'OE1ST', 'X121ST', 'FHCS', 'TTC1ST'].includes(match.oddType)) {
                        //     if (match.oddType === 'AH1ST') {
                        //         betResult = calculateHandicap(_homeScoreHT, _awayScoreHT, match.handicap, match.bet);
                        //     } else if (match.oddType === 'OU1ST' || match.oddType === 'TTC1ST') {
                        //         betResult = calculateOuScore(_homeScoreHT, _awayScoreHT, match.handicap, match.bet);
                        //     } else if (match.oddType === 'OE1ST') {
                        //         betResult = calculateOeScore(_homeScoreHT, _awayScoreHT, match.bet);
                        //     } else if (match.oddType === 'X121ST') {
                        //         betResult = calculateOneTwo(_homeScoreHT, _awayScoreHT, match.bet);
                        //     } else if (betObj.hdp.oddType === 'FHCS') {
                        //         betResult = calculateCorrectScore(_homeScoreHT, _awayScoreHT, match.bet);
                        //     }
                        // } else {
                        //     if (match.oddType === 'AH') {
                        //         betResult = calculateHandicap(_homeScoreFT, _awayScoreFT, match.handicap, match.bet);
                        //     } else if (match.oddType === 'OU' || match.oddType === 'TTC') {
                        //         betResult = calculateOuScore(_homeScoreFT, _awayScoreFT, match.handicap, match.bet);
                        //     } else if (match.oddType === 'OE') {
                        //         betResult = calculateOeScore(_homeScoreFT, _awayScoreFT, match.bet);
                        //     } else if (match.oddType === 'X12' || match.oddType === 'ML') {
                        //         betResult = calculateOneTwo(_homeScoreFT, _awayScoreFT, match.bet);
                        //     } else if (match.oddType === 'T1OU') {
                        //         betResult = calculateOuScore(_homeScoreFT, 0, match.handicap, match.bet);
                        //     } else if (match.oddType === 'T2OU') {
                        //         betResult = calculateOuScore(0, _awayScoreFT, match.handicap, match.bet);
                        //     }
                        // }

                        if (req.body.result === betObj.hdp.bet) {
                            betResult = 'WIN'
                        } else if (req.body.result === 'DRAW') {
                            betResult = 'DRAW';
                        } else {
                            betResult = 'LOSE'
                        }

                        let updateBody;
                        let isCalculateReceive = false;
                        let isRefundBalance = false;
                        let winLoseAmount = 0;
                        let shareReceive;

                        if (match.betResult === betResult) {
                            console.log('same result : update score only');
                            updateBody = {
                                $set: {
                                    // 'parlay.matches.$.fullScore': homeScoreFull + ':' + awayScoreFull,
                                    'parlay.matches.$.score': req.body.score + ':' + req.body.score,
                                    'parlay.matches.$.betResult': betResult,
                                    // 'betResult': betResult,
                                }
                            };

                        } else {

                            let isUnCheckMatchLose = _.filter(allMatch, function (response) {
                                    return response.matchId !== matchId && response.betResult === LOSE;
                                }).length > 0;

                            if (isUnCheckMatchLose || betResult === LOSE) {
                                isRefundBalance = true;
                                isCalculateReceive = true;
                                winLoseAmount = 0 - betObj.amount;
                                shareReceive = prepareShareReceive(winLoseAmount, LOSE, betObj);
                                updateBody = {
                                    $set: {
                                        // 'parlay.matches.$.fullScore': homeScoreFull + ':' + awayScoreFull,
                                        'parlay.matches.$.score': req.body.score + ':' + req.body.score,
                                        'parlay.matches.$.betResult': betResult,

                                        'commission.superAdmin.winLoseCom': shareReceive.superAdmin.winLoseCom,
                                        'commission.superAdmin.winLose': shareReceive.superAdmin.winLose,
                                        'commission.superAdmin.totalWinLoseCom': shareReceive.superAdmin.totalWinLoseCom,

                                        'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                                        'commission.company.winLose': shareReceive.company.winLose,
                                        'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                                        'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                                        'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                                        'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                                        'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                                        'commission.senior.winLose': shareReceive.senior.winLose,
                                        'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                                        'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                                        'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                                        'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                                        'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                                        'commission.agent.winLose': shareReceive.agent.winLose,
                                        'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                                        'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                                        'commission.member.winLose': shareReceive.member.winLose,
                                        'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                                        'validAmount': betObj.amount,
                                        'betResult': betResult,
                                        'isEndScore': true,
                                        'status': 'DONE'
                                    }
                                };
                            } else {
                                // NOT EQUALS LOSE

                                if (unCheckMatch.length == 0) {
                                    console.log('no unCheck match');
                                    isRefundBalance = true;
                                    isCalculateReceive = true;

                                    let totalUnCheckOdds = _.filter(allMatch, function (response) {
                                        return response.matchId !== matchId && response.betResult;
                                    }).map((item) => {
                                        return calculateParlayOdds(item.betResult, item.odd);
                                    });

                                    console.log(' success match');
                                    console.log(totalUnCheckOdds);

                                    let sumTotalUnCheckOdds = _.reduce(totalUnCheckOdds, (memo, item) => {
                                        return {sum: memo.sum * item};
                                    }, {sum: 1}).sum;


                                    let halfLoseCount = 0;

                                    //prepare with last match result
                                    sumTotalUnCheckOdds = roundTo.down(sumTotalUnCheckOdds * calculateParlayOdds(betResult, match.odd), 3);

                                    // if (betResult === HALF_LOSE) {
                                    //     halfLoseCount += 1;
                                    // }

                                    console.log('sumTotalUnCheckOdds : ', sumTotalUnCheckOdds);
                                    console.log('halfLoseCount : ', halfLoseCount);

                                    winLoseAmount = (subPlyAmount(betObj.amount, halfLoseCount) * sumTotalUnCheckOdds);
                                    let validAmount = subPlyAmount(betObj.amount, halfLoseCount);

                                    winLoseAmount = winLoseAmount - betObj.amount;

                                    shareReceive = prepareShareReceive(winLoseAmount, WIN, betObj);
                                    updateBody = {
                                        $set: {
                                            // 'parlay.matches.$.fullScore': homeScoreFull + ':' + awayScoreFull,
                                            'parlay.matches.$.score': req.body.score + ':' + req.body.score,
                                            'parlay.matches.$.betResult': betResult,
                                            'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                                            'commission.company.winLose': shareReceive.company.winLose,
                                            'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                                            'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                                            'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                                            'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                                            'commission.api.winLoseCom': shareReceive.api.winLoseCom,
                                            'commission.api.winLose': shareReceive.api.winLose,
                                            'commission.api.totalWinLoseCom': shareReceive.api.totalWinLoseCom,

                                            'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                                            'commission.senior.winLose': shareReceive.senior.winLose,
                                            'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                                            'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                                            'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                                            'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                                            'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                                            'commission.agent.winLose': shareReceive.agent.winLose,
                                            'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                                            'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                                            'commission.member.winLose': shareReceive.member.winLose,
                                            'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                                            'validAmount': validAmount,
                                            'betResult': 'WIN',
                                            'isEndScore': true,
                                            'status': 'DONE'
                                        }
                                    };
                                    updateBody.$set.status = 'DONE';

                                } else {
                                    updateBody = {
                                        $set: {
                                            // 'parlay.matches.$.fullScore': homeScoreFull + ':' + awayScoreFull,
                                            // 'parlay.matches.$.halfScore': homeScoreHalf + ':' + awayScoreHalf,
                                            'parlay.matches.$.betResult': betResult,
                                            // 'betResult': betResult,
                                        }
                                    };
                                    console.log('have unCheck match : ', unCheckMatch);
                                    console.log(unCheckMatch);

                                }
                            }
                        }


                        console.log(updateBody);

                        async.series([callback => {
                            //refundBalance
                            if (isRefundBalance) {
                                AgentService.rollbackCalculateAgentMemberBalance(betObj, req.body.key, (err, response) => {
                                    if (err) {
                                        callback(err, null);
                                    } else {
                                        callback(null, response);
                                    }
                                });
                            } else {
                                callback(null, '');
                            }

                        }, callback => {

                            BetTransactionModel.update({
                                _id: betObj._id,
                                'parlay.matches._id': match._id
                            }, updateBody, function (err, response) {
                                if (err) {
                                    callback(err, null);
                                } else {
                                    callback(null, response);
                                }
                            });

                        }, callback => {
                            if (isCalculateReceive) {
                                updateAgentMemberBalance(shareReceive, 'PARLAY', req.body.key, (err, response) => {
                                    if (err) {
                                        callback(err, null);
                                    } else {
                                        callback(null, response);
                                    }
                                });
                            } else {
                                callback(null, '');
                            }
                        }], function (err, response) {
                            if (err) {
                                betListCallback(err, null);
                            } else {
                                betListCallback(null, response);
                            }
                        });

                    });//end loop match in parlay

                }, (err) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, 'success');
                    }
                });//end forEach Match
            });


    })], (err, asyncResponse) => {

        if (err)
            return res.send({message: "error", results: err, code: 999});

        return res.send(
            {
                code: 0,
                message: "success",
                result: 'success'
            }
        );

    });

});


const endScoreSportTTCSchema = Joi.object().keys({
    key: Joi.string().required(),
    matchId: Joi.string().required(),
    homeScoreHT: Joi.number().allow(''),
    awayScoreHT: Joi.number().allow(''),
    homeScoreFT: Joi.number().allow(''),
    awayScoreFT: Joi.number().allow(''),
    type: Joi.string().required()
});

router.post('/special/ttc', function (req, res) {


    let validate = Joi.validate(req.body, endScoreSportTTCSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 1004
        });
    }

    let model = EndScoreHistoryModel(req.body);
    model.createdDate = moment().utc(true).toDate();
    model.save(function (err, response) {
        console.log('save endscore history')
    });

    let isSpecial = true;
    endScoreFunction(req.body, 'FOOTBALL', isSpecial, (err, response) => {
        if (err)
            return res.send({message: "error", results: err, code: 999});

        return res.send(
            {
                code: 0,
                message: "success",
                result: 'success'
            }
        );
    });
});


const endScoreSportFGSchema = Joi.object().keys({
    key: Joi.string().required(),
    matchId: Joi.string().required(),
    homeScore: Joi.string().required(),
    awayScore: Joi.string().required(),
    round: Joi.string().required()
});

router.post('/special/fg', function (req, res) {


    let validate = Joi.validate(req.body, endScoreSportFGSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 1004
        });
    }

    endScoreSportFirstGoalFunction(req.body, (err, response) => {
        if (err)
            return res.send({message: "error", results: err, code: 999});

        return res.send(
            {
                code: 0,
                message: "success",
                result: 'success'
            }
        );
    });
});


const endScoreCancelSportFGSchema = Joi.object().keys({
    key: Joi.string().required(),
    matchId: Joi.string().required(),
    round: Joi.string().required()
});

router.post('/special/fg/cancel', function (req, res) {

    let validate = Joi.validate(req.body, endScoreCancelSportFGSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 1004
        });
    }

    let {key, matchId, round} = req.body;

    let condition = {game: 'FOOTBALL','hdp.matchId': matchId, status: 'RUNNING'};

    if (round == 1) {
        condition['hdp.oddType'] = '1STG';
    } else if (round == 2) {
        condition['hdp.oddType'] = '2NDG';
    } else if (round == 3) {
        condition['hdp.oddType'] = '3RDG';
    } else if (round == 4) {
        condition['hdp.oddType'] = '4THG';
    } else if (round == 5) {
        condition['hdp.oddType'] = '5THG';
    } else if (round == 6) {
        condition['hdp.oddType'] = '6THG';
    } else if (round == 7) {
        condition['hdp.oddType'] = '7THG';
    } else if (round == 8) {
        condition['hdp.oddType'] = '8THG';
    } else if (round == 9) {
        condition['hdp.oddType'] = '9THG';
    } else {
        return res.send({message: "not round match", result: err, code: 999});
    }

    return res.send(
        {
            code: 0,
            message: "success",
            result: 'success'
        }
    );


    BetTransactionModel.find(condition, function (err, betResponse) {

        if (err) {
            return res.send({message: "error", result: err, code: 999});
        }

        async.each(betResponse, (betObj, asyncCallback) => {

            if (err) {
                return res.send({message: "error", results: err, code: 999});
            }

            MemberService.updateBalance2(betObj.memberUsername, Math.abs(betObj.memberCredit), betObj.betId, 'CANCEL_MATCH', key, function (err, creditResponse) {

                if (err) {
                    asyncCallback('fail', null);
                } else {

                    let body = {
                        status: 'CANCELLED',
                        cancelDate: DateUtils.getCurrentDate(),
                        cancelByType: 'API'
                    };

                    BetTransactionModel.update({_id: betObj._id}, body, (err, data) => {
                        if (err) {
                            asyncCallback('fail', null);
                        } else {
                            asyncCallback(null, 'success');
                        }
                    });

                }
            });

        }, (err) => {

            if (err) {
                return res.send({message: "error", results: err, code: 999});
            }

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: 'success'
                }
            );
        });

    });

});


function endScoreFunction(requestBody, sportType, isSpecial, endScoreCallback) {
    let matchId = requestBody.matchId;
    let homeScoreHalf = Number.parseInt(requestBody.homeScoreHT);
    let awayScoreHalf = Number.parseInt(requestBody.awayScoreHT);
    let homeScoreFull = Number.parseInt(requestBody.homeScoreFT);
    let awayScoreFull = Number.parseInt(requestBody.awayScoreFT);

    console.log('-- endScoreFunction --');
    console.log('isSpecial : ', isSpecial);

    // async.parallel([(callback => {
    //
    //     let condition = {
    //         'game': 'FOOTBALL',
    //         'hdp.matchId': matchId,
    //         'status': {$in: ['RUNNING', 'REJECTED']},
    //         'gameType': 'TODAY',
    //         active: true,
    //         createdDate: {$gte: new Date(moment().add(-10, 'd').format('YYYY-MM-DDT11:00:00.000'))},
    //     };
    //
    //     if (sportType === 'FOOTBALL') {
    //         condition['source'] = 'FOOTBALL';
    //
    //     } else if (sportType === 'BASKETBALL') {
    //         condition['source'] = 'BASKETBALL';
    //
    //     } else if (sportType === 'TENNIS') {
    //         condition['source'] = 'TENNIS';
    //
    //     } else if (sportType === 'TABLE_TENNIS') {
    //         condition['source'] = 'TABLE_TENNIS';
    //
    //     } else if (sportType === 'VOLLEYBALL') {
    //         condition['source'] = 'VOLLEYBALL';
    //
    //     } else if (sportType === 'ESPORT') {
    //         condition['source'] = 'ESPORT';
    //
    //     } else if (sportType === 'RUGBY') {
    //         condition['source'] = 'RUGBY';
    //     } else {
    //         condition['source'] = 'BASKETBALL';
    //     }
    //
    //     let orCondition = [];
    //
    //
    //     if (isSpecial) {
    //         if (requestBody.type === 'HT') {
    //             orCondition.push({$or: [{'hdp.oddType': 'TTC1ST'}]});
    //         } else {
    //             orCondition.push({$or: [{'hdp.oddType': 'TTC1ST'}, {'hdp.oddType': 'TTC'}]});
    //         }
    //     } else {
    //         if (requestBody.type === 'HT') {
    //             orCondition.push({$or: [{'hdp.oddType': 'AH1ST'}, {'hdp.oddType': 'OU1ST'}, {'hdp.oddType': 'OE1ST'}, {'hdp.oddType': 'X121ST'}, {'hdp.oddType': 'FHCS'}]});
    //         } else {
    //             orCondition.push({$and: [{'hdp.oddType': {$ne: 'TTC'}}, {'hdp.oddType': {$ne: 'TTC1ST'}}]});
    //         }
    //     }
    //
    //     if (orCondition.length > 0) {
    //         condition['$and'] = orCondition;
    //     }
    //
    //
    //     BetTransactionModel.find(condition)
    //         .select('memberId memberUsername hdp commission amount memberCredit payout memberParentGroup betId status priceType betResult game gameType source')
    //         .populate({path: 'memberParentGroup', select: 'type endpoint secretKey'})
    //         .populate({path: 'memberId', select: 'username_lower'})
    //         .exec(function (err, response) {
    //
    //             if (!response) {
    //                 callback(null, []);
    //                 return;
    //             }
    //
    //             if (response.length === 0) {
    //                 callback(null, []);
    //                 return;
    //             }
    //
    //             console.log("match size : ", response.length);
    //
    //             let keepTransaction = [];
    //             async.each(response, (betObj, asyncCallback) => {
    //
    //                 if (betObj.status === 'REJECTED') {
    //                     let updateBody = {
    //                         $set: {
    //                             'isEndScore': true
    //                         }
    //                     };
    //
    //                     BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
    //                         if (err) {
    //                             asyncCallback(err, null);
    //                         } else {
    //                             asyncCallback(null, response);
    //                         }
    //                     });
    //
    //                 } else {
    //                     //NOT EQUAL REJECTED
    //
    //                     console.log("========================= : ", betObj.hdp.handicap);
    //                     let betResult;
    //
    //                     //checkScore >
    //
    //                     // if (betObj.hdp.matchType === 'LIVE') {
    //                     //
    //                     //     if (homeScoreHalf < Number.parseInt(betObj.hdp.score.split(':')[0]) || awayScoreHalf < Number.parseInt(betObj.hdp.score.split(':')[1])) {
    //                     //         asyncCallback(null, response);
    //                     //         return;
    //                     //     }
    //                     //
    //                     //     if (requestBody.type === 'FT') {
    //                     //
    //                     //         if (homeScoreFull < Number.parseInt(betObj.hdp.score.split(':')[0]) || awayScoreFull < Number.parseInt(betObj.hdp.score.split(':')[1])) {
    //                     //             asyncCallback(null, response);
    //                     //             return;
    //                     //         }
    //                     //     }
    //                     // }
    //
    //
    //                     let _homeScoreHT, _awayScoreHT, _homeScoreFT, _awayScoreFT;
    //
    //                     if (betObj.hdp.matchType === 'LIVE' && ['AH1ST', 'AH'].includes(betObj.hdp.oddType)) {
    //                         _homeScoreHT = homeScoreHalf - Number.parseInt(betObj.hdp.score.split(':')[0]);
    //                         _awayScoreHT = awayScoreHalf - Number.parseInt(betObj.hdp.score.split(':')[1]);
    //                         _homeScoreFT = homeScoreFull - Number.parseInt(betObj.hdp.score.split(':')[0]);
    //                         _awayScoreFT = awayScoreFull - Number.parseInt(betObj.hdp.score.split(':')[1]);
    //
    //                     } else {
    //                         _homeScoreHT = homeScoreHalf;
    //                         _awayScoreHT = awayScoreHalf;
    //                         _homeScoreFT = homeScoreFull;
    //                         _awayScoreFT = awayScoreFull;
    //                     }
    //
    //
    //                     if (['AH1ST', 'OU1ST', 'OE1ST', 'X121ST', 'FHCS', 'TTC1ST'].includes(betObj.hdp.oddType)) {
    //                         if (betObj.hdp.oddType === 'AH1ST') {
    //                             betResult = calculateHandicap(_homeScoreHT, _awayScoreHT, betObj.hdp.handicap, betObj.hdp.bet);
    //                         } else if (betObj.hdp.oddType === 'OU1ST' || betObj.hdp.oddType === 'TTC1ST') {
    //                             betResult = calculateOuScore(_homeScoreHT, _awayScoreHT, betObj.hdp.handicap, betObj.hdp.bet);
    //                         } else if (betObj.hdp.oddType === 'OE1ST') {
    //                             betResult = calculateOeScore(_homeScoreHT, _awayScoreHT, betObj.hdp.bet);
    //                         } else if (betObj.hdp.oddType === 'X121ST') {
    //                             betResult = calculateOneTwo(_homeScoreHT, _awayScoreHT, betObj.hdp.bet);
    //                         } else if (betObj.hdp.oddType === 'FHCS') {
    //                             betResult = calculateCorrectScore(_homeScoreHT, _awayScoreHT, betObj.hdp.bet);
    //                         }
    //                     } else {
    //                         if (betObj.hdp.oddType === 'AH') {
    //                             betResult = calculateHandicap(_homeScoreFT, _awayScoreFT, betObj.hdp.handicap, betObj.hdp.bet);
    //                         } else if (betObj.hdp.oddType === 'OU' || betObj.hdp.oddType === 'TTC') {
    //                             betResult = calculateOuScore(_homeScoreFT, _awayScoreFT, betObj.hdp.handicap, betObj.hdp.bet);
    //                         } else if (betObj.hdp.oddType === 'OE') {
    //                             betResult = calculateOeScore(_homeScoreFT, _awayScoreFT, betObj.hdp.bet);
    //                         } else if (betObj.hdp.oddType === 'X12' || betObj.hdp.oddType === 'ML') {
    //                             betResult = calculateOneTwo(_homeScoreFT, _awayScoreFT, betObj.hdp.bet);
    //                         } else if (betObj.hdp.oddType === 'T1OU') {
    //                             betResult = calculateOuScore(_homeScoreFT, 0, betObj.hdp.handicap, betObj.hdp.bet);
    //                         } else if (betObj.hdp.oddType === 'T2OU') {
    //                             betResult = calculateOuScore(0, _awayScoreFT, betObj.hdp.handicap, betObj.hdp.bet);
    //                         } else if (betObj.hdp.oddType === 'CS') {
    //                             betResult = calculateCorrectScore(_homeScoreFT, _awayScoreFT, betObj.hdp.bet);
    //                         } else if (betObj.hdp.oddType === 'TG') {
    //                             betResult = calculateTotalGoal(_homeScoreFT, _awayScoreFT, betObj.hdp.bet);
    //                         } else if (betObj.hdp.oddType === 'DC') {
    //                             betResult = calculateDoubleChance(_homeScoreFT, _awayScoreFT, betObj.hdp.bet);
    //                         } else if (betObj.hdp.oddType === 'FTHT') {
    //                             betResult = calculateFTHT(_homeScoreHT, _awayScoreHT, _homeScoreFT, _awayScoreFT, betObj.hdp.bet);
    //                         }
    //                     }
    //
    //
    //                     console.log("betResult : ", betResult);
    //
    //                     let amount = Math.abs(betObj.memberCredit);
    //                     let validAmount;
    //                     let winLose = 0;
    //                     if (betResult === DRAW) {
    //                         winLose = 0;
    //                         validAmount = 0;
    //                     } else if (betResult === WIN) {
    //                         FormulaUtils.calculatePayout(betObj.amount, betObj.hdp.odd, betObj.hdp.oddType, betObj.priceType, (payout) => {
    //                             winLose = Number.parseFloat(payout) - betObj.amount;
    //                         });
    //                         validAmount = betObj.amount;
    //                     } else if (betResult === HALF_WIN) {
    //                         FormulaUtils.calculatePayout(betObj.amount, betObj.hdp.odd, betObj.hdp.oddType, betObj.priceType, (payout) => {
    //                             winLose = (Number.parseFloat(payout) - betObj.amount) / 2;
    //                         });
    //                         validAmount = betObj.amount / 2;
    //                     } else if (betResult === LOSE) {
    //                         winLose = 0 - amount;
    //                         validAmount = betObj.amount;
    //                     } else if (betResult === HALF_LOSE) {
    //                         winLose = 0 - (amount / 2);
    //                         validAmount = betObj.amount / 2;
    //                     }
    //
    //                     const shareReceive = prepareShareReceive(winLose, betResult, betObj);
    //
    //                     let transaction = {
    //                         _id: betObj._id,
    //                         memberId: betObj.memberId,
    //                         matchId: matchId,
    //                         type: requestBody.type,
    //                         handicap: betObj.hdp.handicap,
    //                         oddType: betObj.hdp.oddType,
    //                         bet: betObj.hdp.bet,
    //                         betAmount: betObj.amount,
    //                         homeScoreHT: homeScoreHalf,
    //                         awayScoreHT: awayScoreHalf,
    //                         homeScoreFT: homeScoreFull,
    //                         awayScoreFT: awayScoreFull,
    //                         result: betResult,
    //                         transStatus: 'DONE',
    //                         validAmount: validAmount,
    //                         winLose: shareReceive
    //                     };
    //
    //                     updateAgentMemberBalance(shareReceive, 'TODAY', 'END'/*requestBody.key*/, (err, response) => {
    //                         if (err) {
    //                             if (err == 888) {
    //                                 asyncCallback(null, {});
    //                             } else {
    //                                 asyncCallback(err, null);
    //                             }
    //                         } else {
    //                             async.series([subCallback => {
    //
    //                                 let updateBody = {
    //                                     $set: {
    //                                         'hdp.fullScore': transaction.homeScoreFT + ':' + transaction.awayScoreFT,
    //                                         'hdp.halfScore': transaction.homeScoreHT + ':' + transaction.awayScoreHT,
    //
    //                                         'commission.superAdmin.winLoseCom': transaction.winLose.superAdmin.winLoseCom,
    //                                         'commission.superAdmin.winLose': transaction.winLose.superAdmin.winLose,
    //                                         'commission.superAdmin.totalWinLoseCom': transaction.winLose.superAdmin.totalWinLoseCom,
    //
    //                                         'commission.company.winLoseCom': transaction.winLose.company.winLoseCom,
    //                                         'commission.company.winLose': transaction.winLose.company.winLose,
    //                                         'commission.company.totalWinLoseCom': transaction.winLose.company.totalWinLoseCom,
    //
    //                                         'commission.shareHolder.winLoseCom': transaction.winLose.shareHolder.winLoseCom,
    //                                         'commission.shareHolder.winLose': transaction.winLose.shareHolder.winLose,
    //                                         'commission.shareHolder.totalWinLoseCom': transaction.winLose.shareHolder.totalWinLoseCom,
    //
    //                                         'commission.api.winLoseCom': transaction.winLose.api.winLoseCom,
    //                                         'commission.api.winLose': transaction.winLose.api.winLose,
    //                                         'commission.api.totalWinLoseCom': transaction.winLose.api.totalWinLoseCom,
    //
    //                                         'commission.senior.winLoseCom': transaction.winLose.senior.winLoseCom,
    //                                         'commission.senior.winLose': transaction.winLose.senior.winLose,
    //                                         'commission.senior.totalWinLoseCom': transaction.winLose.senior.totalWinLoseCom,
    //
    //                                         'commission.masterAgent.winLoseCom': transaction.winLose.masterAgent.winLoseCom,
    //                                         'commission.masterAgent.winLose': transaction.winLose.masterAgent.winLose,
    //                                         'commission.masterAgent.totalWinLoseCom': transaction.winLose.masterAgent.totalWinLoseCom,
    //
    //                                         'commission.agent.winLoseCom': transaction.winLose.agent.winLoseCom,
    //                                         'commission.agent.winLose': transaction.winLose.agent.winLose,
    //                                         'commission.agent.totalWinLoseCom': transaction.winLose.agent.totalWinLoseCom,
    //
    //                                         'commission.member.winLoseCom': transaction.winLose.member.winLoseCom,
    //                                         'commission.member.winLose': transaction.winLose.member.winLose,
    //                                         'commission.member.totalWinLoseCom': transaction.winLose.member.totalWinLoseCom,
    //                                         'validAmount': transaction.validAmount,
    //                                         'betResult': transaction.result,
    //                                         'isEndScore': true,
    //                                         'status': 'DONE',
    //                                         'settleDate': DateUtils.getCurrentDate()
    //                                     }
    //                                 };
    //
    //                                 BetTransactionModel.findOneAndUpdate({_id: transaction._id}, updateBody, {new: true}, function (err, response) {
    //                                     if (err) {
    //                                         subCallback(err, null);
    //                                     } else {
    //
    //                                             let object = Object.assign({}, response)._doc;
    //                                             object.memberParentGroup = betObj.memberParentGroup;
    //                                             object.memberId = betObj.memberId;
    //                                             subCallback(null, response);
    //
    //                                     }
    //                                 });
    //
    //                             }], function (err, asyncResponse) {
    //                                 if (err) {
    //                                     asyncCallback(err, null);
    //                                 } else {
    //                                     keepTransaction.push(asyncResponse[0]);
    //                                     asyncCallback(null, 'success');
    //                                 }
    //                             });
    //                         }
    //
    //                     });
    //                 }
    //
    //             }, (err) => {
    //                 if (err) {
    //                     callback(err, null);
    //                 } else {
    //                     callback(null, keepTransaction);
    //                 }
    //             });//end forEach Match
    //         });
    //
    //
    // }), (callback => {
    //
    //
    //     if (isSpecial) {
    //         callback(null, []);
    //     } else {
    //         let condition = {
    //             'game': 'FOOTBALL',
    //             'gameType': {$ne: 'TODAY'},
    //             'parlay.matches.matchId':matchId,
    //             active: true,
    //             createdDate: {$gte: new Date(moment().add(-10, 'd').format('YYYY-MM-DDT11:00:00.000'))},
    //         };
    //
    //
    //         BetTransactionModel.find(condition)
    //             .select('memberId memberUsername parlay commission amount memberCredit payout memberParentGroup status betId betResult game gameType source')
    //             .populate({path: 'memberParentGroup', select: 'type endpoint'})
    //             .populate({path: 'memberId', select: 'username_lower'})
    //             .exec(function (err, response) {
    //
    //                 if (!response) {
    //                     callback(null, []);
    //                     return;
    //                 }
    //
    //                 let keepTransaction = [];
    //                 async.each(response, (betObj, betListCallback) => {
    //
    //                     const allMatch = betObj.parlay.matches;
    //
    //                     let filterList = _.filter(allMatch, function (response) {
    //                         if (requestBody.type === 'HT') {
    //                             return response.matchId === matchId && ['AH1ST', 'OU1ST', 'OE1ST', 'X121ST', 'FHCS', 'TTC1ST'].includes(response.oddType);
    //                         } else {
    //                             return response.matchId === matchId;
    //                         }
    //                     });
    //
    //
    //                     if (filterList.length === 0) {
    //                         betListCallback(null, 'not data found');
    //                         return;
    //                     }
    //
    //                     let unCheckMatch = _.filter(allMatch, function (response) {
    //                         return response.matchId !== matchId && !response.betResult;
    //                     });
    //
    //
    //                     const match = filterList[0];
    //
    //                     let betResult;
    //
    //                     let _homeScoreHT, _awayScoreHT, _homeScoreFT, _awayScoreFT;
    //
    //                     if (match.matchType === 'LIVE' && ['AH1ST', 'AH'].includes(match.oddType)) {
    //                         _homeScoreHT = homeScoreHalf - Number.parseInt(match.score.split(':')[0]);
    //                         _awayScoreHT = awayScoreHalf - Number.parseInt(match.score.split(':')[1]);
    //                         _homeScoreFT = homeScoreFull - Number.parseInt(match.score.split(':')[0]);
    //                         _awayScoreFT = awayScoreFull - Number.parseInt(match.score.split(':')[1]);
    //
    //                     } else {
    //                         _homeScoreHT = homeScoreHalf;
    //                         _awayScoreHT = awayScoreHalf;
    //                         _homeScoreFT = homeScoreFull;
    //                         _awayScoreFT = awayScoreFull;
    //                     }
    //
    //
    //                     if (['AH1ST', 'OU1ST', 'OE1ST', 'X121ST', 'FHCS', 'TTC1ST'].includes(match.oddType)) {
    //                         if (match.oddType === 'AH1ST') {
    //                             betResult = calculateHandicap(_homeScoreHT, _awayScoreHT, match.handicap, match.bet);
    //                         } else if (match.oddType === 'OU1ST' || match.oddType === 'TTC1ST') {
    //                             betResult = calculateOuScore(_homeScoreHT, _awayScoreHT, match.handicap, match.bet);
    //                         } else if (match.oddType === 'OE1ST') {
    //                             betResult = calculateOeScore(_homeScoreHT, _awayScoreHT, match.bet);
    //                         } else if (match.oddType === 'X121ST') {
    //                             betResult = calculateOneTwo(_homeScoreHT, _awayScoreHT, match.bet);
    //                         }
    //                     } else {
    //                         if (match.oddType === 'AH') {
    //                             betResult = calculateHandicap(_homeScoreFT, _awayScoreFT, match.handicap, match.bet);
    //                         } else if (match.oddType === 'OU' || match.oddType === 'TTC') {
    //                             betResult = calculateOuScore(_homeScoreFT, _awayScoreFT, match.handicap, match.bet);
    //                         } else if (match.oddType === 'OE') {
    //                             betResult = calculateOeScore(_homeScoreFT, _awayScoreFT, match.bet);
    //                         } else if (match.oddType === 'X12' || match.oddType === 'ML') {
    //                             betResult = calculateOneTwo(_homeScoreFT, _awayScoreFT, match.bet);
    //                         } else if (betObj.hdp.oddType === 'T1OU') {
    //                             betResult = calculateOuScore(_homeScoreFT, 0, match.handicap, match.bet);
    //                         } else if (betObj.hdp.oddType === 'T2OU') {
    //                             betResult = calculateOuScore(0, _awayScoreFT, match.handicap, match.bet);
    //                         }
    //                     }
    //
    //
    //                     if (betObj.status === 'DONE' || betObj.status === 'REJECTED' || betObj.status === 'CANCELLED') {
    //                         let updateBody = {
    //                             $set: {
    //                                 'parlay.matches.$.fullScore': homeScoreFull + ':' + awayScoreFull,
    //                                 'parlay.matches.$.halfScore': homeScoreHalf + ':' + awayScoreHalf,
    //                                 'parlay.matches.$.betResult': betResult,
    //                                 'parlay.matches.$.settleDate': DateUtils.getCurrentDate(),
    //                                 'parlay.matches.$.remark': 'update score only',
    //                             }
    //                         };
    //
    //                         BetTransactionModel.update({
    //                             _id: betObj._id,
    //                             'parlay.matches._id': match._id
    //                         }, updateBody, function (err, response) {
    //                             if (err) {
    //                                 betListCallback(err, null);
    //                             } else {
    //                                 betListCallback(null, response);
    //                             }
    //                         });
    //
    //                     } else {
    //
    //
    //                         let updateBody;
    //                         let isCalculateReceive = false;
    //                         let winLoseAmount = 0;
    //                         let shareReceive;
    //
    //                         if (betResult === LOSE) {
    //                             isCalculateReceive = true;
    //                             winLoseAmount = 0 - betObj.amount;
    //                             shareReceive = prepareShareReceive(winLoseAmount, LOSE, betObj);
    //                             updateBody = {
    //                                 $set: {
    //                                     'parlay.matches.$.fullScore': homeScoreFull + ':' + awayScoreFull,
    //                                     'parlay.matches.$.halfScore': homeScoreHalf + ':' + awayScoreHalf,
    //                                     'parlay.matches.$.betResult': betResult,
    //                                     'parlay.matches.$.settleDate': DateUtils.getCurrentDate(),
    //                                     'parlay.matches.$.remark': 'lose',
    //
    //                                     'commission.superAdmin.winLoseCom': shareReceive.superAdmin.winLoseCom,
    //                                     'commission.superAdmin.winLose': shareReceive.superAdmin.winLose,
    //                                     'commission.superAdmin.totalWinLoseCom': shareReceive.superAdmin.totalWinLoseCom,
    //
    //                                     'commission.company.winLoseCom': shareReceive.company.winLoseCom,
    //                                     'commission.company.winLose': shareReceive.company.winLose,
    //                                     'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,
    //
    //                                     'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
    //                                     'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
    //                                     'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,
    //
    //                                     'commission.api.winLoseCom': shareReceive.api.winLoseCom,
    //                                     'commission.api.winLose': shareReceive.api.winLose,
    //                                     'commission.api.totalWinLoseCom': shareReceive.api.totalWinLoseCom,
    //
    //                                     'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
    //                                     'commission.senior.winLose': shareReceive.senior.winLose,
    //                                     'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,
    //
    //                                     'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
    //                                     'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
    //                                     'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,
    //
    //                                     'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
    //                                     'commission.agent.winLose': shareReceive.agent.winLose,
    //                                     'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,
    //
    //                                     'commission.member.winLoseCom': shareReceive.member.winLoseCom,
    //                                     'commission.member.winLose': shareReceive.member.winLose,
    //                                     'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
    //                                     'validAmount': betObj.amount,
    //                                     'betResult': betResult,
    //                                     'isEndScore': true,
    //                                     'status': 'DONE',
    //                                     'settleDate': DateUtils.getCurrentDate()
    //                                 }
    //                             };
    //                         } else {
    //                             // NOT EQUALS LOSE
    //
    //                             if (unCheckMatch.length == 0) {
    //                                 console.log('no unCheck match');
    //                                 isCalculateReceive = true;
    //
    //                                 let totalUnCheckOdds = _.filter(allMatch, function (response) {
    //                                     return response.matchId !== matchId && response.betResult;
    //                                 }).map((item) => {
    //                                     return calculateParlayOdds(item.betResult, item.odd);
    //                                 });
    //
    //                                 let sumTotalUnCheckOdds = _.reduce(totalUnCheckOdds, (memo, item) => {
    //                                     return {sum: memo.sum * item};
    //                                 }, {sum: 1}).sum;
    //
    //
    //                                 let halfLoseCount = _.filter(allMatch, function (response) {
    //                                     return response.betResult === HALF_LOSE && response.matchId !== matchId;
    //                                 }).length;
    //
    //                                 //prepare with last match result
    //                                 sumTotalUnCheckOdds = roundTo.down(sumTotalUnCheckOdds * calculateParlayOdds(betResult, match.odd), 3);
    //
    //                                 if (betResult === HALF_LOSE) {
    //                                     halfLoseCount += 1;
    //                                 }
    //
    //                                 console.log('sumTotalUnCheckOdds : ', sumTotalUnCheckOdds);
    //                                 console.log('halfLoseCount : ', halfLoseCount);
    //
    //                                 winLoseAmount = (subPlyAmount(betObj.amount, halfLoseCount) * sumTotalUnCheckOdds);
    //                                 let validAmount = subPlyAmount(betObj.amount, halfLoseCount);
    //
    //                                 winLoseAmount = winLoseAmount - betObj.amount;
    //
    //                                 shareReceive = prepareShareReceive(winLoseAmount, WIN, betObj);
    //
    //
    //                                 //FOR REMARK
    //                                 let remark = _.filter(allMatch, function (response) {
    //                                     return response.matchId !== matchId && response.betResult;
    //                                 }).map((item) => {
    //                                     return 'matchId : ' + item.matchId + ' , result : ' + item.betResult + ' , odd : ' + calculateParlayOdds(item.betResult, item.odd);
    //                                 });
    //
    //                                 remark.push('matchId : ' + matchId + ' , result : ' + betResult + ' , odd : ' + calculateParlayOdds(betResult, match.odd))
    //
    //                                 updateBody = {
    //                                     $set: {
    //                                         'parlay.matches.$.fullScore': homeScoreFull + ':' + awayScoreFull,
    //                                         'parlay.matches.$.halfScore': homeScoreHalf + ':' + awayScoreHalf,
    //                                         'parlay.matches.$.betResult': betResult,
    //                                         'parlay.matches.$.settleDate': DateUtils.getCurrentDate(),
    //                                         'parlay.matches.$.remark': remark.join(' | '),
    //
    //                                         'commission.superAdmin.winLoseCom': shareReceive.superAdmin.winLoseCom,
    //                                         'commission.superAdmin.winLose': shareReceive.superAdmin.winLose,
    //                                         'commission.superAdmin.totalWinLoseCom': shareReceive.superAdmin.totalWinLoseCom,
    //
    //                                         'commission.company.winLoseCom': shareReceive.company.winLoseCom,
    //                                         'commission.company.winLose': shareReceive.company.winLose,
    //                                         'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,
    //
    //                                         'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
    //                                         'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
    //                                         'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,
    //
    //                                         'commission.api.winLoseCom': shareReceive.api.winLoseCom,
    //                                         'commission.api.winLose': shareReceive.api.winLose,
    //                                         'commission.api.totalWinLoseCom': shareReceive.api.totalWinLoseCom,
    //
    //                                         'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
    //                                         'commission.senior.winLose': shareReceive.senior.winLose,
    //                                         'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,
    //
    //                                         'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
    //                                         'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
    //                                         'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,
    //
    //                                         'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
    //                                         'commission.agent.winLose': shareReceive.agent.winLose,
    //                                         'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,
    //
    //                                         'commission.member.winLoseCom': shareReceive.member.winLoseCom,
    //                                         'commission.member.winLose': shareReceive.member.winLose,
    //                                         'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
    //                                         'validAmount': validAmount,
    //                                         'betResult': 'WIN',
    //                                         'isEndScore': true,
    //                                         'status': 'DONE',
    //                                         'settleDate': DateUtils.getCurrentDate()
    //                                     }
    //                                 };
    //                                 updateBody.$set.status = 'DONE';
    //
    //                             } else {
    //                                 updateBody = {
    //                                     $set: {
    //                                         'parlay.matches.$.fullScore': homeScoreFull + ':' + awayScoreFull,
    //                                         'parlay.matches.$.halfScore': homeScoreHalf + ':' + awayScoreHalf,
    //                                         'parlay.matches.$.betResult': betResult,
    //                                         'parlay.matches.$.settleDate': DateUtils.getCurrentDate(),
    //                                         'parlay.matches.$.remark': 'unCheckMatch : ' + unCheckMatch.length,
    //                                     }
    //                                 };
    //                                 console.log('have unCheck match : ', unCheckMatch);
    //                                 console.log(unCheckMatch);
    //
    //                             }
    //                         }
    //
    //
    //                         if (isCalculateReceive) {
    //                             updateAgentMemberBalance(shareReceive, 'PARLAY', 'END'/*requestBody.key*/, (err, response) => {
    //                                 if (err) {
    //                                     if (err == 888) {
    //                                         betListCallback(null, {});
    //                                     } else {
    //                                         betListCallback(err, null);
    //                                     }
    //                                 } else {
    //                                     BetTransactionModel.findOneAndUpdate({
    //                                         _id: betObj._id,
    //                                         'parlay.matches._id': match._id
    //                                     }, updateBody, {new: true}, function (err, response) {
    //                                         if (err) {
    //                                             betListCallback(err, null);
    //                                         } else {
    //
    //                                                 let object = Object.assign({}, response)._doc;
    //                                                 object.memberParentGroup = betObj.memberParentGroup;
    //                                                 object.memberId = betObj.memberId;
    //
    //                                                 if (err) {
    //                                                     betListCallback(err, null);
    //                                                 } else {
    //                                                     keepTransaction.push(object);
    //                                                     betListCallback(null, response);
    //                                                 }
    //
    //                                         }
    //                                     });
    //                                 }
    //                             });
    //                         } else {
    //                             if (err) {
    //                                 betListCallback(err, null);
    //                             } else {
    //                                 BetTransactionModel.findOneAndUpdate({
    //                                     _id: betObj._id,
    //                                     'parlay.matches._id': match._id
    //                                 }, updateBody, {new: true}, function (err, response) {
    //                                     if (err) {
    //                                         betListCallback(err, null);
    //                                     } else {
    //
    //                                         if (err) {
    //                                             betListCallback(err, null);
    //                                         } else {
    //                                             betListCallback(null, response);
    //                                         }
    //                                     }
    //                                 });
    //                             }
    //                         }
    //                     }
    //
    //
    //                 }, (err) => {
    //                     if (err) {
    //                         callback(err, null);
    //                     } else {
    //                         callback(null, keepTransaction);
    //                     }
    //                 });//end forEach Match
    //             });
    //     }
    //
    // })], (err, asyncResponse) => {
    //
    //     if (err) {
    //         endScoreCallback(err, null);
    //         return;
    //     }
    //     endScoreCallback(null, 'success');
    // });
    endScoreCallback(null, 'success');
}

function endScoreFunctionByMD5(requestBody, sportType, isSpecial, endScoreCallback) {
    let matchId = requestBody.matchId;
    let md5     = requestBody.md5;
    let homeScoreHalf = Number.parseInt(requestBody.homeScoreHT);
    let awayScoreHalf = Number.parseInt(requestBody.awayScoreHT);
    let homeScoreFull = Number.parseInt(requestBody.homeScoreFT);
    let awayScoreFull = Number.parseInt(requestBody.awayScoreFT);

    console.log('-- endScoreFunction --');
    console.log('isSpecial : ', isSpecial);

    async.parallel([(callback => {

        let condition = {
            'game': 'FOOTBALL',
            'hdp.md5': md5,
            'status': {$in: ['RUNNING', 'REJECTED']},
            'gameType': 'TODAY',
            active: true,
            createdDate: {$gte: new Date(moment().add(-10, 'd').format('YYYY-MM-DDT11:00:00.000'))},
        };

        if (sportType === 'FOOTBALL') {
            condition['source'] = 'FOOTBALL';

        } else if (sportType === 'BASKETBALL') {
            condition['source'] = 'BASKETBALL';

        } else if (sportType === 'TENNIS') {
            condition['source'] = 'TENNIS';

        } else if (sportType === 'TABLE_TENNIS') {
            condition['source'] = 'TABLE_TENNIS';

        } else if (sportType === 'VOLLEYBALL') {
            condition['source'] = 'VOLLEYBALL';

        } else if (sportType === 'ESPORT') {
            condition['source'] = 'ESPORT';

        } else if (sportType === 'RUGBY') {
            condition['source'] = 'RUGBY';
        } else {
            condition['source'] = 'BASKETBALL';
        }

        let orCondition = [];


        if (isSpecial) {
            if (requestBody.type === 'HT') {
                orCondition.push({$or: [{'hdp.oddType': 'TTC1ST'}]});
            } else {
                orCondition.push({$or: [{'hdp.oddType': 'TTC1ST'}, {'hdp.oddType': 'TTC'}]});
            }
        } else {
            if (requestBody.type === 'HT') {
                orCondition.push({$or: [{'hdp.oddType': 'AH1ST'}, {'hdp.oddType': 'OU1ST'}, {'hdp.oddType': 'OE1ST'}, {'hdp.oddType': 'X121ST'}, {'hdp.oddType': 'FHCS'}]});
            } else {
                orCondition.push({$and: [{'hdp.oddType': {$ne: 'TTC'}}, {'hdp.oddType': {$ne: 'TTC1ST'}}]});
            }
        }

        if (orCondition.length > 0) {
            condition['$and'] = orCondition;
        }


        BetTransactionModel.find(condition)
            .select('memberId memberUsername hdp commission amount memberCredit payout memberParentGroup betId status priceType betResult game gameType source')
            .populate({path: 'memberParentGroup', select: 'type endpoint secretKey'})
            .populate({path: 'memberId', select: 'username_lower'})
            .exec(function (err, response) {

                if (!response) {
                    callback(null, []);
                    return;
                }

                if (response.length === 0) {
                    callback(null, []);
                    return;
                }

                console.log("match size : ", response.length);

                let keepTransaction = [];
                async.each(response, (betObj, asyncCallback) => {

                    if (betObj.status === 'REJECTED') {
                        let updateBody = {
                            $set: {
                                'isEndScore': true
                            }
                        };

                        BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                            if (err) {
                                asyncCallback(err, null);
                            } else {
                                asyncCallback(null, response);
                            }
                        });

                    } else {
                        //NOT EQUAL REJECTED

                        console.log("========================= : ", betObj.hdp.handicap);
                        let betResult;

                        //checkScore >

                        // if (betObj.hdp.matchType === 'LIVE') {
                        //
                        //     if (homeScoreHalf < Number.parseInt(betObj.hdp.score.split(':')[0]) || awayScoreHalf < Number.parseInt(betObj.hdp.score.split(':')[1])) {
                        //         asyncCallback(null, response);
                        //         return;
                        //     }
                        //
                        //     if (requestBody.type === 'FT') {
                        //
                        //         if (homeScoreFull < Number.parseInt(betObj.hdp.score.split(':')[0]) || awayScoreFull < Number.parseInt(betObj.hdp.score.split(':')[1])) {
                        //             asyncCallback(null, response);
                        //             return;
                        //         }
                        //     }
                        // }


                        let _homeScoreHT, _awayScoreHT, _homeScoreFT, _awayScoreFT;

                        if (betObj.hdp.matchType === 'LIVE' && ['AH1ST', 'AH'].includes(betObj.hdp.oddType)) {
                            _homeScoreHT = homeScoreHalf - Number.parseInt(betObj.hdp.score.split(':')[0]);
                            _awayScoreHT = awayScoreHalf - Number.parseInt(betObj.hdp.score.split(':')[1]);
                            _homeScoreFT = homeScoreFull - Number.parseInt(betObj.hdp.score.split(':')[0]);
                            _awayScoreFT = awayScoreFull - Number.parseInt(betObj.hdp.score.split(':')[1]);

                        } else {
                            _homeScoreHT = homeScoreHalf;
                            _awayScoreHT = awayScoreHalf;
                            _homeScoreFT = homeScoreFull;
                            _awayScoreFT = awayScoreFull;
                        }


                        if (['AH1ST', 'OU1ST', 'OE1ST', 'X121ST', 'FHCS', 'TTC1ST'].includes(betObj.hdp.oddType)) {
                            if (betObj.hdp.oddType === 'AH1ST') {
                                betResult = calculateHandicap(_homeScoreHT, _awayScoreHT, betObj.hdp.handicap, betObj.hdp.bet);
                            } else if (betObj.hdp.oddType === 'OU1ST' || betObj.hdp.oddType === 'TTC1ST') {
                                betResult = calculateOuScore(_homeScoreHT, _awayScoreHT, betObj.hdp.handicap, betObj.hdp.bet);
                            } else if (betObj.hdp.oddType === 'OE1ST') {
                                betResult = calculateOeScore(_homeScoreHT, _awayScoreHT, betObj.hdp.bet);
                            } else if (betObj.hdp.oddType === 'X121ST') {
                                betResult = calculateOneTwo(_homeScoreHT, _awayScoreHT, betObj.hdp.bet);
                            } else if (betObj.hdp.oddType === 'FHCS') {
                                betResult = calculateCorrectScore(_homeScoreHT, _awayScoreHT, betObj.hdp.bet);
                            }
                        } else {
                            if (betObj.hdp.oddType === 'AH') {
                                betResult = calculateHandicap(_homeScoreFT, _awayScoreFT, betObj.hdp.handicap, betObj.hdp.bet);
                            } else if (betObj.hdp.oddType === 'OU' || betObj.hdp.oddType === 'TTC') {
                                betResult = calculateOuScore(_homeScoreFT, _awayScoreFT, betObj.hdp.handicap, betObj.hdp.bet);
                            } else if (betObj.hdp.oddType === 'OE') {
                                betResult = calculateOeScore(_homeScoreFT, _awayScoreFT, betObj.hdp.bet);
                            } else if (betObj.hdp.oddType === 'X12' || betObj.hdp.oddType === 'ML') {
                                betResult = calculateOneTwo(_homeScoreFT, _awayScoreFT, betObj.hdp.bet);
                            } else if (betObj.hdp.oddType === 'T1OU') {
                                betResult = calculateOuScore(_homeScoreFT, 0, betObj.hdp.handicap, betObj.hdp.bet);
                            } else if (betObj.hdp.oddType === 'T2OU') {
                                betResult = calculateOuScore(0, _awayScoreFT, betObj.hdp.handicap, betObj.hdp.bet);
                            } else if (betObj.hdp.oddType === 'CS') {
                                betResult = calculateCorrectScore(_homeScoreFT, _awayScoreFT, betObj.hdp.bet);
                            } else if (betObj.hdp.oddType === 'TG') {
                                betResult = calculateTotalGoal(_homeScoreFT, _awayScoreFT, betObj.hdp.bet);
                            } else if (betObj.hdp.oddType === 'DC') {
                                betResult = calculateDoubleChance(_homeScoreFT, _awayScoreFT, betObj.hdp.bet);
                            } else if (betObj.hdp.oddType === 'FTHT') {
                                betResult = calculateFTHT(_homeScoreHT, _awayScoreHT, _homeScoreFT, _awayScoreFT, betObj.hdp.bet);
                            }
                        }


                        console.log("betResult : ", betResult);

                        let amount = Math.abs(betObj.memberCredit);
                        let validAmount;
                        let winLose = 0;
                        if (betResult === DRAW) {
                            winLose = 0;
                            validAmount = 0;
                        } else if (betResult === WIN) {
                            FormulaUtils.calculatePayout(betObj.amount, betObj.hdp.odd, betObj.hdp.oddType, betObj.priceType, (payout) => {
                                winLose = Number.parseFloat(payout) - betObj.amount;
                            });
                            validAmount = betObj.amount;
                        } else if (betResult === HALF_WIN) {
                            FormulaUtils.calculatePayout(betObj.amount, betObj.hdp.odd, betObj.hdp.oddType, betObj.priceType, (payout) => {
                                winLose = (Number.parseFloat(payout) - betObj.amount) / 2;
                            });
                            validAmount = betObj.amount / 2;
                        } else if (betResult === LOSE) {
                            winLose = 0 - amount;
                            validAmount = betObj.amount;
                        } else if (betResult === HALF_LOSE) {
                            winLose = 0 - (amount / 2);
                            validAmount = betObj.amount / 2;
                        }

                        const shareReceive = prepareShareReceive(winLose, betResult, betObj);

                        let transaction = {
                            _id: betObj._id,
                            memberId: betObj.memberId,
                            matchId: matchId,
                            type: requestBody.type,
                            handicap: betObj.hdp.handicap,
                            oddType: betObj.hdp.oddType,
                            bet: betObj.hdp.bet,
                            betAmount: betObj.amount,
                            homeScoreHT: homeScoreHalf,
                            awayScoreHT: awayScoreHalf,
                            homeScoreFT: homeScoreFull,
                            awayScoreFT: awayScoreFull,
                            result: betResult,
                            transStatus: 'DONE',
                            validAmount: validAmount,
                            winLose: shareReceive
                        };

                        updateAgentMemberBalance(shareReceive, 'TODAY', 'END'/*requestBody.key*/, (err, response) => {
                            if (err) {
                                if (err == 888) {
                                    asyncCallback(null, {});
                                } else {
                                    asyncCallback(err, null);
                                }
                            } else {
                                async.series([subCallback => {

                                    let updateBody = {
                                        $set: {
                                            'hdp.fullScore': transaction.homeScoreFT + ':' + transaction.awayScoreFT,
                                            'hdp.halfScore': transaction.homeScoreHT + ':' + transaction.awayScoreHT,

                                            'commission.superAdmin.winLoseCom': transaction.winLose.superAdmin.winLoseCom,
                                            'commission.superAdmin.winLose': transaction.winLose.superAdmin.winLose,
                                            'commission.superAdmin.totalWinLoseCom': transaction.winLose.superAdmin.totalWinLoseCom,

                                            'commission.company.winLoseCom': transaction.winLose.company.winLoseCom,
                                            'commission.company.winLose': transaction.winLose.company.winLose,
                                            'commission.company.totalWinLoseCom': transaction.winLose.company.totalWinLoseCom,

                                            'commission.shareHolder.winLoseCom': transaction.winLose.shareHolder.winLoseCom,
                                            'commission.shareHolder.winLose': transaction.winLose.shareHolder.winLose,
                                            'commission.shareHolder.totalWinLoseCom': transaction.winLose.shareHolder.totalWinLoseCom,

                                            'commission.api.winLoseCom': transaction.winLose.api.winLoseCom,
                                            'commission.api.winLose': transaction.winLose.api.winLose,
                                            'commission.api.totalWinLoseCom': transaction.winLose.api.totalWinLoseCom,

                                            'commission.senior.winLoseCom': transaction.winLose.senior.winLoseCom,
                                            'commission.senior.winLose': transaction.winLose.senior.winLose,
                                            'commission.senior.totalWinLoseCom': transaction.winLose.senior.totalWinLoseCom,

                                            'commission.masterAgent.winLoseCom': transaction.winLose.masterAgent.winLoseCom,
                                            'commission.masterAgent.winLose': transaction.winLose.masterAgent.winLose,
                                            'commission.masterAgent.totalWinLoseCom': transaction.winLose.masterAgent.totalWinLoseCom,

                                            'commission.agent.winLoseCom': transaction.winLose.agent.winLoseCom,
                                            'commission.agent.winLose': transaction.winLose.agent.winLose,
                                            'commission.agent.totalWinLoseCom': transaction.winLose.agent.totalWinLoseCom,

                                            'commission.member.winLoseCom': transaction.winLose.member.winLoseCom,
                                            'commission.member.winLose': transaction.winLose.member.winLose,
                                            'commission.member.totalWinLoseCom': transaction.winLose.member.totalWinLoseCom,
                                            'validAmount': transaction.validAmount,
                                            'betResult': transaction.result,
                                            'isEndScore': true,
                                            'status': 'DONE',
                                            'settleDate': DateUtils.getCurrentDate()
                                        }
                                    };

                                    BetTransactionModel.findOneAndUpdate({_id: transaction._id}, updateBody, {new: true}, function (err, response) {
                                        if (err) {
                                            subCallback(err, null);
                                        } else {
                                            let object = Object.assign({}, response)._doc;
                                            object.memberParentGroup = betObj.memberParentGroup;
                                            object.memberId = betObj.memberId;
                                            subCallback(null, response);
                                        }
                                    });

                                }], function (err, asyncResponse) {
                                    if (err) {
                                        asyncCallback(err, null);
                                    } else {
                                        keepTransaction.push(asyncResponse[0]);
                                        asyncCallback(null, 'success');
                                    }
                                });
                            }

                        });
                    }

                }, (err) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, keepTransaction);
                    }
                });//end forEach Match
            });


    }), (callback => {


        if (isSpecial) {
            callback(null, []);
        } else {
            let condition = {
                'game': 'FOOTBALL',
                'gameType': {$ne: 'TODAY'},
                'parlay.matches.matchId':matchId,
                active: true,
                createdDate: {$gte: new Date(moment().add(-10, 'd').format('YYYY-MM-DDT11:00:00.000'))},
            };


            BetTransactionModel.find(condition)
                .select('memberId memberUsername parlay commission amount memberCredit payout memberParentGroup status betId betResult game gameType source')
                .populate({path: 'memberParentGroup', select: 'type endpoint'})
                .populate({path: 'memberId', select: 'username_lower'})
                .exec(function (err, response) {

                    if (!response) {
                        callback(null, []);
                        return;
                    }

                    let keepTransaction = [];
                    async.each(response, (betObj, betListCallback) => {

                        const allMatch = betObj.parlay.matches;

                        let filterList = _.filter(allMatch, function (response) {
                            if (requestBody.type === 'HT') {
                                return response.matchId === matchId && ['AH1ST', 'OU1ST', 'OE1ST', 'X121ST', 'FHCS', 'TTC1ST'].includes(response.oddType);
                            } else {
                                return response.matchId === matchId;
                            }
                        });


                        if (filterList.length === 0) {
                            betListCallback(null, 'not data found');
                            return;
                        }

                        let unCheckMatch = _.filter(allMatch, function (response) {
                            return response.matchId !== matchId && !response.betResult;
                        });


                        const match = filterList[0];

                        let betResult;

                        let _homeScoreHT, _awayScoreHT, _homeScoreFT, _awayScoreFT;

                        if (match.matchType === 'LIVE' && ['AH1ST', 'AH'].includes(match.oddType)) {
                            _homeScoreHT = homeScoreHalf - Number.parseInt(match.score.split(':')[0]);
                            _awayScoreHT = awayScoreHalf - Number.parseInt(match.score.split(':')[1]);
                            _homeScoreFT = homeScoreFull - Number.parseInt(match.score.split(':')[0]);
                            _awayScoreFT = awayScoreFull - Number.parseInt(match.score.split(':')[1]);

                        } else {
                            _homeScoreHT = homeScoreHalf;
                            _awayScoreHT = awayScoreHalf;
                            _homeScoreFT = homeScoreFull;
                            _awayScoreFT = awayScoreFull;
                        }


                        if (['AH1ST', 'OU1ST', 'OE1ST', 'X121ST', 'FHCS', 'TTC1ST'].includes(match.oddType)) {
                            if (match.oddType === 'AH1ST') {
                                betResult = calculateHandicap(_homeScoreHT, _awayScoreHT, match.handicap, match.bet);
                            } else if (match.oddType === 'OU1ST' || match.oddType === 'TTC1ST') {
                                betResult = calculateOuScore(_homeScoreHT, _awayScoreHT, match.handicap, match.bet);
                            } else if (match.oddType === 'OE1ST') {
                                betResult = calculateOeScore(_homeScoreHT, _awayScoreHT, match.bet);
                            } else if (match.oddType === 'X121ST') {
                                betResult = calculateOneTwo(_homeScoreHT, _awayScoreHT, match.bet);
                            }
                        } else {
                            if (match.oddType === 'AH') {
                                betResult = calculateHandicap(_homeScoreFT, _awayScoreFT, match.handicap, match.bet);
                            } else if (match.oddType === 'OU' || match.oddType === 'TTC') {
                                betResult = calculateOuScore(_homeScoreFT, _awayScoreFT, match.handicap, match.bet);
                            } else if (match.oddType === 'OE') {
                                betResult = calculateOeScore(_homeScoreFT, _awayScoreFT, match.bet);
                            } else if (match.oddType === 'X12' || match.oddType === 'ML') {
                                betResult = calculateOneTwo(_homeScoreFT, _awayScoreFT, match.bet);
                            } else if (betObj.hdp.oddType === 'T1OU') {
                                betResult = calculateOuScore(_homeScoreFT, 0, match.handicap, match.bet);
                            } else if (betObj.hdp.oddType === 'T2OU') {
                                betResult = calculateOuScore(0, _awayScoreFT, match.handicap, match.bet);
                            }
                        }


                        if (betObj.status === 'DONE' || betObj.status === 'REJECTED' || betObj.status === 'CANCELLED') {
                            let updateBody = {
                                $set: {
                                    'parlay.matches.$.fullScore': homeScoreFull + ':' + awayScoreFull,
                                    'parlay.matches.$.halfScore': homeScoreHalf + ':' + awayScoreHalf,
                                    'parlay.matches.$.betResult': betResult,
                                    'parlay.matches.$.settleDate': DateUtils.getCurrentDate(),
                                    'parlay.matches.$.remark': 'update score only',
                                }
                            };

                            BetTransactionModel.update({
                                _id: betObj._id,
                                'parlay.matches._id': match._id
                            }, updateBody, function (err, response) {
                                if (err) {
                                    betListCallback(err, null);
                                } else {
                                    betListCallback(null, response);
                                }
                            });

                        } else {


                            let updateBody;
                            let isCalculateReceive = false;
                            let winLoseAmount = 0;
                            let shareReceive;

                            if (betResult === LOSE) {
                                isCalculateReceive = true;
                                winLoseAmount = 0 - betObj.amount;
                                shareReceive = prepareShareReceive(winLoseAmount, LOSE, betObj);
                                updateBody = {
                                    $set: {
                                        'parlay.matches.$.fullScore': homeScoreFull + ':' + awayScoreFull,
                                        'parlay.matches.$.halfScore': homeScoreHalf + ':' + awayScoreHalf,
                                        'parlay.matches.$.betResult': betResult,
                                        'parlay.matches.$.settleDate': DateUtils.getCurrentDate(),
                                        'parlay.matches.$.remark': 'lose',

                                        'commission.superAdmin.winLoseCom': shareReceive.superAdmin.winLoseCom,
                                        'commission.superAdmin.winLose': shareReceive.superAdmin.winLose,
                                        'commission.superAdmin.totalWinLoseCom': shareReceive.superAdmin.totalWinLoseCom,

                                        'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                                        'commission.company.winLose': shareReceive.company.winLose,
                                        'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                                        'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                                        'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                                        'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                                        'commission.api.winLoseCom': shareReceive.api.winLoseCom,
                                        'commission.api.winLose': shareReceive.api.winLose,
                                        'commission.api.totalWinLoseCom': shareReceive.api.totalWinLoseCom,

                                        'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                                        'commission.senior.winLose': shareReceive.senior.winLose,
                                        'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                                        'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                                        'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                                        'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                                        'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                                        'commission.agent.winLose': shareReceive.agent.winLose,
                                        'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                                        'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                                        'commission.member.winLose': shareReceive.member.winLose,
                                        'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                                        'validAmount': betObj.amount,
                                        'betResult': betResult,
                                        'isEndScore': true,
                                        'status': 'DONE',
                                        'settleDate': DateUtils.getCurrentDate()
                                    }
                                };
                            } else {
                                // NOT EQUALS LOSE

                                if (unCheckMatch.length == 0) {
                                    console.log('no unCheck match');
                                    isCalculateReceive = true;

                                    let totalUnCheckOdds = _.filter(allMatch, function (response) {
                                        return response.matchId !== matchId && response.betResult;
                                    }).map((item) => {
                                        return calculateParlayOdds(item.betResult, item.odd);
                                    });

                                    let sumTotalUnCheckOdds = _.reduce(totalUnCheckOdds, (memo, item) => {
                                        return {sum: memo.sum * item};
                                    }, {sum: 1}).sum;


                                    let halfLoseCount = _.filter(allMatch, function (response) {
                                        return response.betResult === HALF_LOSE && response.matchId !== matchId;
                                    }).length;

                                    //prepare with last match result
                                    sumTotalUnCheckOdds = roundTo.down(sumTotalUnCheckOdds * calculateParlayOdds(betResult, match.odd), 3);

                                    if (betResult === HALF_LOSE) {
                                        halfLoseCount += 1;
                                    }

                                    console.log('sumTotalUnCheckOdds : ', sumTotalUnCheckOdds);
                                    console.log('halfLoseCount : ', halfLoseCount);

                                    winLoseAmount = (subPlyAmount(betObj.amount, halfLoseCount) * sumTotalUnCheckOdds);
                                    let validAmount = subPlyAmount(betObj.amount, halfLoseCount);

                                    winLoseAmount = winLoseAmount - betObj.amount;

                                    shareReceive = prepareShareReceive(winLoseAmount, WIN, betObj);


                                    //FOR REMARK
                                    let remark = _.filter(allMatch, function (response) {
                                        return response.matchId !== matchId && response.betResult;
                                    }).map((item) => {
                                        return 'matchId : ' + item.matchId + ' , result : ' + item.betResult + ' , odd : ' + calculateParlayOdds(item.betResult, item.odd);
                                    });

                                    remark.push('matchId : ' + matchId + ' , result : ' + betResult + ' , odd : ' + calculateParlayOdds(betResult, match.odd))

                                    updateBody = {
                                        $set: {
                                            'parlay.matches.$.fullScore': homeScoreFull + ':' + awayScoreFull,
                                            'parlay.matches.$.halfScore': homeScoreHalf + ':' + awayScoreHalf,
                                            'parlay.matches.$.betResult': betResult,
                                            'parlay.matches.$.settleDate': DateUtils.getCurrentDate(),
                                            'parlay.matches.$.remark': remark.join(' | '),

                                            'commission.superAdmin.winLoseCom': shareReceive.superAdmin.winLoseCom,
                                            'commission.superAdmin.winLose': shareReceive.superAdmin.winLose,
                                            'commission.superAdmin.totalWinLoseCom': shareReceive.superAdmin.totalWinLoseCom,

                                            'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                                            'commission.company.winLose': shareReceive.company.winLose,
                                            'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                                            'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                                            'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                                            'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                                            'commission.api.winLoseCom': shareReceive.api.winLoseCom,
                                            'commission.api.winLose': shareReceive.api.winLose,
                                            'commission.api.totalWinLoseCom': shareReceive.api.totalWinLoseCom,

                                            'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                                            'commission.senior.winLose': shareReceive.senior.winLose,
                                            'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                                            'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                                            'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                                            'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                                            'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                                            'commission.agent.winLose': shareReceive.agent.winLose,
                                            'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                                            'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                                            'commission.member.winLose': shareReceive.member.winLose,
                                            'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                                            'validAmount': validAmount,
                                            'betResult': 'WIN',
                                            'isEndScore': true,
                                            'status': 'DONE',
                                            'settleDate': DateUtils.getCurrentDate()
                                        }
                                    };
                                    updateBody.$set.status = 'DONE';

                                } else {
                                    updateBody = {
                                        $set: {
                                            'parlay.matches.$.fullScore': homeScoreFull + ':' + awayScoreFull,
                                            'parlay.matches.$.halfScore': homeScoreHalf + ':' + awayScoreHalf,
                                            'parlay.matches.$.betResult': betResult,
                                            'parlay.matches.$.settleDate': DateUtils.getCurrentDate(),
                                            'parlay.matches.$.remark': 'unCheckMatch : ' + unCheckMatch.length,
                                        }
                                    };
                                    console.log('have unCheck match : ', unCheckMatch);
                                    console.log(unCheckMatch);

                                }
                            }


                            if (isCalculateReceive) {
                                updateAgentMemberBalance(shareReceive, 'PARLAY', 'END'/*requestBody.key*/, (err, response) => {
                                    if (err) {
                                        if (err == 888) {
                                            betListCallback(null, {});
                                        } else {
                                            betListCallback(err, null);
                                        }
                                    } else {
                                        BetTransactionModel.findOneAndUpdate({
                                            _id: betObj._id,
                                            'parlay.matches._id': match._id
                                        }, updateBody, {new: true}, function (err, response) {
                                            if (err) {
                                                betListCallback(err, null);
                                            } else {
                                                let object = Object.assign({}, response)._doc;
                                                object.memberParentGroup = betObj.memberParentGroup;
                                                object.memberId = betObj.memberId;

                                                if (err) {
                                                    betListCallback(err, null);
                                                } else {
                                                    keepTransaction.push(object);
                                                    betListCallback(null, response);
                                                }
                                            }
                                        });
                                    }
                                });
                            } else {
                                if (err) {
                                    betListCallback(err, null);
                                } else {
                                    BetTransactionModel.findOneAndUpdate({
                                        _id: betObj._id,
                                        'parlay.matches._id': match._id
                                    }, updateBody, {new: true}, function (err, response) {
                                        if (err) {
                                            betListCallback(err, null);
                                        } else {

                                            if (err) {
                                                betListCallback(err, null);
                                            } else {
                                                betListCallback(null, response);
                                            }
                                        }
                                    });
                                }
                            }
                        }


                    }, (err) => {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, keepTransaction);
                        }
                    });//end forEach Match
                });
        }

    })], (err, asyncResponse) => {

        if (err) {
            endScoreCallback(err, null);
            return;
        }
        endScoreCallback(null, 'success');
    });

}

function reEndScoreFunction(requestBody, sportType, endScoreCallback) {
    let matchId = requestBody.matchId;
    let homeScoreHalf = Number.parseInt(requestBody.homeScoreHT);
    let awayScoreHalf = Number.parseInt(requestBody.awayScoreHT);
    let homeScoreFull = Number.parseInt(requestBody.homeScoreFT);
    let awayScoreFull = Number.parseInt(requestBody.awayScoreFT);

    console.log('-- reEndScoreFunction --')

    async.series([(callback => {

        let condition = {
            'game': 'FOOTBALL',
            'hdp.matchId': matchId,
            'status':{$in:['DONE','REJECTED']},
            'gameType': 'TODAY',
            active: true
        };

        if (sportType === 'FOOTBALL') {
            condition['source'] = 'FOOTBALL';

        } else if (sportType === 'BASKETBALL') {
            condition['source'] = 'BASKETBALL';

        } else if (sportType === 'TENNIS') {
            condition['source'] = 'TENNIS';

        } else if (sportType === 'TABLE_TENNIS') {
            condition['source'] = 'TABLE_TENNIS';

        } else if (sportType === 'VOLLEYBALL') {
            condition['source'] = 'VOLLEYBALL';

        } else if (sportType === 'ESPORT') {
            condition['source'] = 'ESPORT';

        } else if (sportType === 'RUGBY') {
            condition['source'] = 'RUGBY';

        } else if (sportType === 'MUAY_THAI') {
            condition['source'] = 'MUAY_THAI';

        } else {
            condition['source'] = 'BASKETBALL';
        }

        let orCondition = [];



        if (requestBody.type === 'HT') {
            orCondition.push({$or: [{'hdp.oddType': 'AH1ST'}, {'hdp.oddType': 'OU1ST'}, {'hdp.oddType': 'OE1ST'}, {'hdp.oddType': 'X121ST'}]});
        }

        if (orCondition.length > 0) {
            condition['$and'] = orCondition;
        }


        BetTransactionModel.find(condition)
            .select('memberId memberUsername hdp commission amount memberCredit payout memberParentGroup betId status priceType betResult game gameType source')
            .populate({path: 'memberParentGroup', select: 'type endpoint secretKey'})
            .populate({path: 'memberId', select: 'username_lower'})
            .exec(function (err, response) {

                if (!response) {
                    callback(null, 'success');
                    return;
                }

                if (response.length === 0) {
                    callback(null, '');
                    return;
                }

                let keepTransaction = [];
                async.each(response, (betObj, asyncCallback) => {


                    if (betObj.status === 'REJECTED') {
                        let updateBody = {
                            $set: {
                                'isEndScore': true
                            }
                        };

                        BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                            if (err) {
                                asyncCallback(err, null);
                            } else {
                                asyncCallback(null, response);
                            }
                        });

                    } else {
                        //NOT EQUAL REJECTED

                        console.log("========================= : ", betObj.hdp.handicap);
                        let betResult;

                        let _homeScoreHT, _awayScoreHT, _homeScoreFT, _awayScoreFT;

                        if (betObj.hdp.matchType === 'LIVE' && ['AH1ST', 'AH'].includes(betObj.hdp.oddType)) {
                            _homeScoreHT = homeScoreHalf - Number.parseInt(betObj.hdp.score.split(':')[0]);
                            _awayScoreHT = awayScoreHalf - Number.parseInt(betObj.hdp.score.split(':')[1]);
                            _homeScoreFT = homeScoreFull - Number.parseInt(betObj.hdp.score.split(':')[0]);
                            _awayScoreFT = awayScoreFull - Number.parseInt(betObj.hdp.score.split(':')[1]);

                        } else {
                            _homeScoreHT = homeScoreHalf;
                            _awayScoreHT = awayScoreHalf;
                            _homeScoreFT = homeScoreFull;
                            _awayScoreFT = awayScoreFull;
                        }


                        if (['AH1ST', 'OU1ST', 'OE1ST', 'X121ST'].includes(betObj.hdp.oddType)) {
                            if (betObj.hdp.oddType === 'AH1ST') {
                                betResult = calculateHandicap(_homeScoreHT, _awayScoreHT, betObj.hdp.handicap, betObj.hdp.bet);
                            } else if (betObj.hdp.oddType === 'OU1ST') {
                                betResult = calculateOuScore(_homeScoreHT, _awayScoreHT, betObj.hdp.handicap, betObj.hdp.bet);
                            } else if (betObj.hdp.oddType === 'OE1ST') {
                                betResult = calculateOeScore(_homeScoreHT, _awayScoreHT, betObj.hdp.bet);
                            } else if (betObj.hdp.oddType === 'X121ST') {
                                betResult = calculateOneTwo(_homeScoreHT, _awayScoreHT, betObj.hdp.bet);
                            }
                        } else {
                            if (betObj.hdp.oddType === 'AH') {
                                betResult = calculateHandicap(_homeScoreFT, _awayScoreFT, betObj.hdp.handicap, betObj.hdp.bet);
                            } else if (betObj.hdp.oddType === 'OU') {
                                betResult = calculateOuScore(_homeScoreFT, _awayScoreFT, betObj.hdp.handicap, betObj.hdp.bet);
                            } else if (betObj.hdp.oddType === 'OE') {
                                betResult = calculateOeScore(_homeScoreFT, _awayScoreFT, betObj.hdp.bet);
                            } else if (betObj.hdp.oddType === 'X12' || betObj.hdp.oddType === 'ML') {
                                betResult = calculateOneTwo(_homeScoreFT, _awayScoreFT, betObj.hdp.bet);
                            }
                            else if (betObj.hdp.oddType === 'T1OU') {
                                betResult = calculateOuScore(_homeScoreFT, 0, betObj.hdp.handicap, betObj.hdp.bet);
                            } else if (betObj.hdp.oddType === 'T2OU') {
                                betResult = calculateOuScore(0, _awayScoreFT, betObj.hdp.handicap, betObj.hdp.bet);
                            }
                        }

                        console.log('old betResult : ', betObj.betResult);
                        console.log("new betResult : ", betResult);

                        if (false && betResult === betObj.betResult) {
                            console.log('same result : update score only')
                            let updateBody = {
                                $set: {
                                    'hdp.fullScore': homeScoreFull + ':' + awayScoreFull,
                                    'hdp.halfScore': homeScoreHalf + ':' + awayScoreHalf,
                                }
                            };

                            BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                                if (err) {
                                    asyncCallback(err, null);
                                } else {
                                    asyncCallback(null, response);
                                }
                            });

                        } else {

                            let amount = Math.abs(betObj.memberCredit);
                            let validAmount;
                            let winLose = 0;
                            if (betResult === DRAW) {
                                winLose = 0;
                                validAmount = 0;
                            } else if (betResult === WIN) {
                                FormulaUtils.calculatePayout(betObj.amount, betObj.hdp.odd, betObj.hdp.oddType, betObj.priceType, (payout) => {
                                    winLose = Number.parseFloat(payout) - betObj.amount;
                                });
                                validAmount = betObj.amount;
                            } else if (betResult === HALF_WIN) {
                                FormulaUtils.calculatePayout(betObj.amount, betObj.hdp.odd, betObj.hdp.oddType, betObj.priceType, (payout) => {
                                    winLose = (Number.parseFloat(payout) - betObj.amount) / 2;
                                });
                                validAmount = betObj.amount / 2;
                            } else if (betResult === LOSE) {
                                winLose = 0 - amount;
                                validAmount = betObj.amount;
                            } else if (betResult === HALF_LOSE) {
                                winLose = 0 - (amount / 2);
                                validAmount = betObj.amount / 2;
                            }

                            const shareReceive = prepareShareReceive(winLose, betResult, betObj);

                            async.series([subCallback => {
                                //refundBalance
                                AgentService.rollbackCalculateAgentMemberBalance(betObj, '', (err, response) => {
                                    if (err) {
                                        subCallback(err, null);
                                    } else {
                                        subCallback(null, response);
                                    }
                                });

                            }, subCallback => {

                                let updateBody = {
                                    $set: {
                                        'hdp.fullScore': homeScoreFull + ':' + awayScoreFull,
                                        'hdp.halfScore': homeScoreHalf + ':' + awayScoreHalf,

                                        'commission.superAdmin.winLoseCom': shareReceive.superAdmin.winLoseCom,
                                        'commission.superAdmin.winLose': shareReceive.superAdmin.winLose,
                                        'commission.superAdmin.totalWinLoseCom': shareReceive.superAdmin.totalWinLoseCom,

                                        'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                                        'commission.company.winLose': shareReceive.company.winLose,
                                        'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                                        'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                                        'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                                        'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                                        'commission.api.winLoseCom': shareReceive.api.winLoseCom,
                                        'commission.api.winLose': shareReceive.api.winLose,
                                        'commission.api.totalWinLoseCom': shareReceive.api.totalWinLoseCom,

                                        'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                                        'commission.senior.winLose': shareReceive.senior.winLose,
                                        'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                                        'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                                        'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                                        'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                                        'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                                        'commission.agent.winLose': shareReceive.agent.winLose,
                                        'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                                        'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                                        'commission.member.winLose': shareReceive.member.winLose,
                                        'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                                        'validAmount': validAmount,
                                        'betResult': betResult,
                                        'isEndScore': true,
                                        'status': 'DONE',
                                        'settleDate': DateUtils.getCurrentDate()
                                    }
                                };

                                BetTransactionModel.findOneAndUpdate({_id: betObj._id}, updateBody, {new: true}, function (err, response) {
                                    if (err) {
                                        subCallback(err, null);
                                    } else {
                                            let object = Object.assign({}, response)._doc;
                                            object.memberParentGroup = betObj.memberParentGroup;
                                            object.memberId = betObj.memberId;
                                            subCallback(null, response);
                                    }
                                });

                            }, subCallback => {
                                updateAgentMemberBalance(shareReceive, 'TODAY', requestBody.key, (err, response) => {
                                    if (err) {
                                        subCallback(err, null);
                                    } else {
                                        subCallback(null, response);
                                    }
                                });
                            }], function (err, asyncResponse) {
                                if (err) {
                                    asyncCallback(err, null);
                                } else {
                                    keepTransaction.push(asyncResponse[1]);
                                    asyncCallback(null, 'success');
                                }
                            });
                        }
                    }

                }, (err) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, keepTransaction);
                    }
                });//end forEach Match
            });

    }), (callback => {

        // let condition = {'game': 'FOOTBALL', 'gameType': 'PARLAY', active: true};
        let condition = {
                'game': 'FOOTBALL',
                'gameType': {$ne: 'TODAY'},
                'parlay.matches.matchId' : matchId,
                status: 'DONE',
                active: true,
            };
        // if (sportType === 'FOOTBALL') {
        //     condition['source'] = 'FOOTBALL';
        // } else {
        //     condition['source'] = 'BASKETBALL';
        // }

        // homeScore
        // if (req.body.type === 'HT') {
        //     condition['$or'] = [{'parlay.matches.oddType': 'AH1ST'}, {'parlay.matches.oddType': 'OU1ST'}, {'parlay.matches.oddType': 'OE1ST'}, {'parlay.matches.oddType': 'X121ST'}]
        // }

        BetTransactionModel.find(condition)
            .select('memberId memberUsername parlay commission amount memberCredit payout memberParentGroup status betId betResult game gameType source')
            .populate({path: 'memberParentGroup', select: 'type endpoint secretKey'})
            .populate({path: 'memberId', select: 'username_lower'})
            .exec(function (err, response) {

                if (!response) {
                    callback(null, 'success');
                    return;
                }
                let keepTransaction = [];
                async.each(response, (betObj, betListCallback) => {

                    const allMatch = betObj.parlay.matches;

                    let filterList = _.filter(allMatch, function (response) {
                        if (requestBody.type === 'HT') {
                            return response.matchId === matchId && ['AH1ST', 'OU1ST', 'OE1ST', 'X121ST'].includes(response.oddType);
                        } else {
                            return response.matchId === matchId;
                        }
                    });

                    if (filterList.length === 0) {
                        betListCallback(null, 'not data found');
                        return;
                    }

                    let unCheckMatch = _.filter(allMatch, function (response) {
                        return response.matchId !== matchId && !response.betResult;
                    });


                    _.each(filterList, (match) => {

                        console.log("========================= : ", match.matchId);

                        let betResult;

                        let _homeScoreHT, _awayScoreHT, _homeScoreFT, _awayScoreFT;

                        if (match.matchType === 'LIVE' && ['AH1ST', 'AH'].includes(match.oddType)) {
                            _homeScoreHT = homeScoreHalf - Number.parseInt(match.score.split(':')[0]);
                            _awayScoreHT = awayScoreHalf - Number.parseInt(match.score.split(':')[1]);
                            _homeScoreFT = homeScoreFull - Number.parseInt(match.score.split(':')[0]);
                            _awayScoreFT = awayScoreFull - Number.parseInt(match.score.split(':')[1]);

                        } else {
                            _homeScoreHT = homeScoreHalf;
                            _awayScoreHT = awayScoreHalf;
                            _homeScoreFT = homeScoreFull;
                            _awayScoreFT = awayScoreFull;
                        }

                        if (['AH1ST', 'OU1ST', 'OE1ST', 'X121ST'].includes(match.oddType)) {
                            if (match.oddType === 'AH1ST') {
                                betResult = calculateHandicap(_homeScoreHT, _awayScoreHT, match.handicap, match.bet);
                            } else if (match.oddType === 'OU1ST') {
                                betResult = calculateOuScore(_homeScoreHT, _awayScoreHT, match.handicap, match.bet);
                            } else if (match.oddType === 'OE1ST') {
                                betResult = calculateOeScore(_homeScoreHT, _awayScoreHT, match.bet);
                            } else if (match.oddType === 'X121ST') {
                                betResult = calculateOneTwo(_homeScoreHT, _awayScoreHT, match.bet);
                            }
                        } else {
                            if (match.oddType === 'AH') {
                                betResult = calculateHandicap(_homeScoreFT, _awayScoreFT, match.handicap, match.bet);
                            } else if (match.oddType === 'OU') {
                                betResult = calculateOuScore(_homeScoreFT, _awayScoreFT, match.handicap, match.bet);
                            } else if (match.oddType === 'OE') {
                                betResult = calculateOeScore(_homeScoreFT, _awayScoreFT, match.bet);
                            } else if (match.oddType === 'X12' || match.oddType === 'ML') {
                                betResult = calculateOneTwo(_homeScoreFT, _awayScoreFT, match.bet);
                            } else if (betObj.hdp.oddType === 'T1OU') {
                                betResult = calculateOuScore(_homeScoreFT, 0, match.handicap, match.bet);
                            } else if (betObj.hdp.oddType === 'T2OU') {
                                betResult = calculateOuScore(0, _awayScoreFT, match.handicap, match.bet);
                            }
                        }

                        let updateBody;
                        let isCalculateReceive = false;
                        let isRefundBalance = false;
                        let winLoseAmount = 0;
                        let shareReceive;

                        if (match.betResult === betResult) {
                            console.log('same result : update score only');
                            updateBody = {
                                $set: {
                                    'parlay.matches.$.fullScore': homeScoreFull + ':' + awayScoreFull,
                                    'parlay.matches.$.halfScore': homeScoreHalf + ':' + awayScoreHalf,
                                    'parlay.matches.$.betResult': betResult,
                                    // 'betResult': betResult,
                                }
                            };

                        } else {

                            let isUnCheckMatchLose = _.filter(allMatch, function (response) {
                                    return response.matchId !== matchId && response.betResult === LOSE;
                                }).length > 0;

                            if (isUnCheckMatchLose || betResult === LOSE) {
                                isRefundBalance = true;
                                isCalculateReceive = true;
                                winLoseAmount = 0 - betObj.amount;
                                shareReceive = prepareShareReceive(winLoseAmount, LOSE, betObj);
                                updateBody = {
                                    $set: {
                                        'parlay.matches.$.fullScore': homeScoreFull + ':' + awayScoreFull,
                                        'parlay.matches.$.halfScore': homeScoreHalf + ':' + awayScoreHalf,
                                        'parlay.matches.$.betResult': betResult,

                                        'commission.superAdmin.winLoseCom': shareReceive.superAdmin.winLoseCom,
                                        'commission.superAdmin.winLose': shareReceive.superAdmin.winLose,
                                        'commission.superAdmin.totalWinLoseCom': shareReceive.superAdmin.totalWinLoseCom,

                                        'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                                        'commission.company.winLose': shareReceive.company.winLose,
                                        'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                                        'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                                        'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                                        'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                                        'commission.api.winLoseCom': shareReceive.api.winLoseCom,
                                        'commission.api.winLose': shareReceive.api.winLose,
                                        'commission.api.totalWinLoseCom': shareReceive.api.totalWinLoseCom,

                                        'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                                        'commission.senior.winLose': shareReceive.senior.winLose,
                                        'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                                        'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                                        'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                                        'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                                        'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                                        'commission.agent.winLose': shareReceive.agent.winLose,
                                        'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                                        'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                                        'commission.member.winLose': shareReceive.member.winLose,
                                        'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                                        'validAmount': betObj.amount,
                                        'betResult': betResult,
                                        'isEndScore': true,
                                        'status': 'DONE',
                                        'settleDate': DateUtils.getCurrentDate()
                                    }
                                };
                            } else {
                                // NOT EQUALS LOSE

                                if (unCheckMatch.length == 0) {
                                    console.log('no unCheck match');
                                    isRefundBalance = true;
                                    isCalculateReceive = true;

                                    let totalUnCheckOdds = _.filter(allMatch, function (response) {
                                        return response.matchId !== matchId && response.betResult;
                                    }).map((item) => {
                                        return calculateParlayOdds(item.betResult, item.odd);
                                    });

                                    console.log(' success match');
                                    console.log(totalUnCheckOdds);

                                    let sumTotalUnCheckOdds = _.reduce(totalUnCheckOdds, (memo, item) => {
                                        return {sum: memo.sum * item};
                                    }, {sum: 1}).sum;


                                    let halfLoseCount = _.filter(allMatch, function (response) {
                                        return response.betResult === HALF_LOSE && response.matchId !== matchId;
                                    }).length;

                                    //prepare with last match result
                                    sumTotalUnCheckOdds = roundTo.down(sumTotalUnCheckOdds * calculateParlayOdds(betResult, match.odd), 3);

                                    if (betResult === HALF_LOSE) {
                                        halfLoseCount += 1;
                                    }

                                    console.log('sumTotalUnCheckOdds : ', sumTotalUnCheckOdds);
                                    console.log('halfLoseCount : ', halfLoseCount);

                                    winLoseAmount = (subPlyAmount(betObj.amount, halfLoseCount) * sumTotalUnCheckOdds);
                                    let validAmount = subPlyAmount(betObj.amount, halfLoseCount);

                                    winLoseAmount = winLoseAmount - betObj.amount;

                                    shareReceive = prepareShareReceive(winLoseAmount, WIN, betObj);
                                    updateBody = {
                                        $set: {
                                            'parlay.matches.$.fullScore': homeScoreFull + ':' + awayScoreFull,
                                            'parlay.matches.$.halfScore': homeScoreHalf + ':' + awayScoreHalf,
                                            'parlay.matches.$.betResult': betResult,
                                            'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                                            'commission.company.winLose': shareReceive.company.winLose,
                                            'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                                            'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                                            'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                                            'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                                            'commission.api.winLoseCom': shareReceive.api.winLoseCom,
                                            'commission.api.winLose': shareReceive.api.winLose,
                                            'commission.api.totalWinLoseCom': shareReceive.api.totalWinLoseCom,

                                            'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                                            'commission.senior.winLose': shareReceive.senior.winLose,
                                            'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                                            'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                                            'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                                            'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                                            'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                                            'commission.agent.winLose': shareReceive.agent.winLose,
                                            'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                                            'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                                            'commission.member.winLose': shareReceive.member.winLose,
                                            'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                                            'validAmount': validAmount,
                                            'betResult': 'WIN',
                                            'isEndScore': true,
                                            'status': 'DONE',
                                            'settleDate': DateUtils.getCurrentDate()
                                        }
                                    };
                                    updateBody.$set.status = 'DONE';

                                } else {
                                    updateBody = {
                                        $set: {
                                            'parlay.matches.$.fullScore': homeScoreFull + ':' + awayScoreFull,
                                            'parlay.matches.$.halfScore': homeScoreHalf + ':' + awayScoreHalf,
                                            'parlay.matches.$.betResult': betResult,
                                            // 'betResult': betResult,
                                        }
                                    };
                                    console.log('have unCheck match : ', unCheckMatch);
                                    console.log(unCheckMatch);

                                }
                            }
                        }


                        async.series([callback => {
                            //refundBalance
                            if (isRefundBalance) {
                                AgentService.rollbackCalculateAgentMemberBalance(betObj, '', (err, response) => {
                                    if (err) {
                                        callback(err, null);
                                    } else {
                                        callback(null, response);
                                    }
                                });
                            } else {
                                callback(null, '');
                            }

                        }, callback => {

                            BetTransactionModel.findOneAndUpdate({
                                _id: betObj._id,
                                'parlay.matches._id': match._id
                            }, updateBody, {new: true}, function (err, response) {
                                if (err) {
                                    callback(err, null);
                                } else {


                                        let object = Object.assign({}, response)._doc;
                                        object.memberParentGroup = betObj.memberParentGroup;
                                        object.memberId = betObj.memberId;
                                        callback(null, object);


                                }
                            });

                        }, callback => {
                            if (isCalculateReceive) {
                                updateAgentMemberBalance(shareReceive, 'PARLAY', requestBody.key, (err, response) => {
                                    if (err) {
                                        callback(err, null);
                                    } else {
                                        callback(null, response);
                                    }
                                });
                            } else {
                                callback(null, '');
                            }
                        }], function (err, asyncResponse) {
                            if (err) {
                                betListCallback(err, null);
                            } else {
                                keepTransaction.push(asyncResponse[1]);
                                betListCallback(null, response);
                            }
                        });

                    });//end loop match in parlay

                }, (err) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, keepTransaction);
                    }
                });//end forEach Match
            });

    })], (err, asyncResponse) => {

        if (err) {
            endScoreCallback(err, null);
            return;
        }

        let transaction = asyncResponse[0].concat(asyncResponse[1]);

        // let apiResponse = _.filter(transaction, (item) => {
        //     return item && item.memberParentGroup.type === 'API' && item.status === 'DONE';
        // });
        //
        //
        // if (apiResponse.length > 0) {
        //     let settleInfo = {
        //         action: "RE_SETTLE",
        //         matchId: matchId,
        //         scoreFT: homeScoreFull + ':' + awayScoreFull,
        //         scoreHT: (homeScoreHalf || awayScoreHalf) ? homeScoreHalf + ':' + awayScoreHalf : ''
        //
        //     };
        //
        //     let endpoint = 'http://localhost:8002/spa/api';
        //     ApiService.settle(endpoint, settleInfo, apiResponse, (err, response) => {
        //         if (err) {
        //             endScoreCallback(err, null);
        //         } else {
        //             endScoreCallback(null, response);
        //         }
        //
        //     });
        // } else {

        if (err) {
            endScoreCallback(err, null);
        } else {
            endScoreCallback(null, 'success');
        }
        // }

    });

}

function endScoreMuayThaiFunction(requestBody, endScoreCallback) {
    let matchId = requestBody.matchId;
    let result = requestBody.result;
    let round = Number.parseInt(requestBody.round);

    console.log('-- endScoreFunction --');

    async.parallel([(callback => {

        let condition = {
            game: {$in:['FOOTBALL','M2']},
            'hdp.matchId': matchId,
            status:{$in:['RUNNING','REJECTED']},
            'gameType': 'TODAY',
            'active': true
        };


        let orCondition = [];

        orCondition.push({$or: [{'source': 'MUAY_THAI_LOCAL'}, {'source': 'MUAY_THAI'}, {'source': 'M2_MUAY'}]});

        if (orCondition.length > 0) {
            condition['$and'] = orCondition;
        }

        console.log(condition)

        let cursor = BetTransactionModel.find(condition)
            .batchSize(200)
            .select('memberId memberUsername hdp commission amount memberCredit payout memberParentGroup betId status priceType betResult game gameType source ref1')
            .populate({path: 'memberParentGroup', select: 'type endpoint secretKey'})
            .populate({path: 'memberId', select: 'username_lower'})
            .lean()
            .cursor();


        cursor.on('data', function (betObj) {

            if (betObj.status === 'REJECTED') {
                let updateBody = {
                    $set: {
                        'isEndScore': true
                    }
                };

                BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                    if (err) {
                        // asyncCallback(err, null);
                    } else {
                        // asyncCallback(null, response);
                        console.log('xxx')
                    }
                });

            } else {
                //NOT EQUAL REJECTED

                console.log("========================= : ", betObj.betId);
                let betResult;

                let _homeScore, _awayScore, _round;

                if (result === 'HOME') {
                    _homeScore = 1;
                    _awayScore = 0;
                } else if (result === 'AWAY') {
                    _homeScore = 0;
                    _awayScore = 1;
                } else if (result === 'DRAW') {
                    _homeScore = 0;
                    _awayScore = 0;
                }
                _round = round;


                if (betObj.hdp.oddType === 'AH') {
                    betResult = calculateHandicap(_homeScore, _awayScore, betObj.hdp.handicap, betObj.hdp.bet);
                } else if (betObj.hdp.oddType === 'OU') {
                    betResult = calculateOuScore(_round, 0, betObj.hdp.handicap, betObj.hdp.bet);
                }


                console.log("betResult : ", betResult);

                let amount = Math.abs(betObj.memberCredit);
                let validAmount;
                let winLose = 0;
                if (betResult === DRAW) {
                    winLose = 0;
                    validAmount = 0;
                } else if (betResult === WIN) {
                    FormulaUtils.calculatePayout(betObj.amount, betObj.hdp.odd, betObj.hdp.oddType, betObj.priceType, (payout) => {
                        winLose = Number.parseFloat(payout) - betObj.amount;
                    });
                    validAmount = betObj.amount;
                } else if (betResult === HALF_WIN) {
                    FormulaUtils.calculatePayout(betObj.amount, betObj.hdp.odd, betObj.hdp.oddType, betObj.priceType, (payout) => {
                        winLose = (Number.parseFloat(payout) - betObj.amount) / 2;
                    });
                    validAmount = betObj.amount / 2;
                } else if (betResult === LOSE) {
                    winLose = 0 - amount;
                    validAmount = betObj.amount;
                } else if (betResult === HALF_LOSE) {
                    winLose = 0 - (amount / 2);
                    validAmount = betObj.amount / 2;
                }

                const shareReceive = prepareShareReceive(winLose, betResult, betObj);

                let transaction = {
                    _id: betObj._id,
                    memberId: betObj.memberId,
                    matchId: matchId,
                    type: requestBody.type,
                    handicap: betObj.hdp.handicap,
                    oddType: betObj.hdp.oddType,
                    bet: betObj.hdp.bet,
                    betAmount: betObj.amount,
                    homeScoreFT: _homeScore,
                    awayScoreFT: _awayScore,
                    result: betResult,
                    transStatus: 'DONE',
                    validAmount: validAmount,
                    winLose: shareReceive
                };

                // updateAgentMemberBalance(shareReceive, 'TODAY', requestBody.key, (err, response) => {
                updateAgentMemberBalance(shareReceive, 'TODAY', `SETTLE_${requestBody.key}_${betObj.ref1}`, (err, response) => {
                    if (err) {
                        if (err == 888) {
                            // asyncCallback(null, {});
                        } else {
                            // asyncCallback(err, null);
                        }
                    } else {
                        async.series([subCallback => {

                            let updateBody = {
                                $set: {
                                    'hdp.fullScore': transaction.homeScoreFT + ':' + transaction.awayScoreFT + ':' + _round,

                                    'commission.superAdmin.winLoseCom': transaction.winLose.superAdmin.winLoseCom,
                                    'commission.superAdmin.winLose': transaction.winLose.superAdmin.winLose,
                                    'commission.superAdmin.totalWinLoseCom': transaction.winLose.superAdmin.totalWinLoseCom,

                                    'commission.company.winLoseCom': transaction.winLose.company.winLoseCom,
                                    'commission.company.winLose': transaction.winLose.company.winLose,
                                    'commission.company.totalWinLoseCom': transaction.winLose.company.totalWinLoseCom,

                                    'commission.shareHolder.winLoseCom': transaction.winLose.shareHolder.winLoseCom,
                                    'commission.shareHolder.winLose': transaction.winLose.shareHolder.winLose,
                                    'commission.shareHolder.totalWinLoseCom': transaction.winLose.shareHolder.totalWinLoseCom,

                                    'commission.api.winLoseCom': transaction.winLose.api.winLoseCom,
                                    'commission.api.winLose': transaction.winLose.api.winLose,
                                    'commission.api.totalWinLoseCom': transaction.winLose.api.totalWinLoseCom,

                                    'commission.senior.winLoseCom': transaction.winLose.senior.winLoseCom,
                                    'commission.senior.winLose': transaction.winLose.senior.winLose,
                                    'commission.senior.totalWinLoseCom': transaction.winLose.senior.totalWinLoseCom,

                                    'commission.masterAgent.winLoseCom': transaction.winLose.masterAgent.winLoseCom,
                                    'commission.masterAgent.winLose': transaction.winLose.masterAgent.winLose,
                                    'commission.masterAgent.totalWinLoseCom': transaction.winLose.masterAgent.totalWinLoseCom,

                                    'commission.agent.winLoseCom': transaction.winLose.agent.winLoseCom,
                                    'commission.agent.winLose': transaction.winLose.agent.winLose,
                                    'commission.agent.totalWinLoseCom': transaction.winLose.agent.totalWinLoseCom,

                                    'commission.member.winLoseCom': transaction.winLose.member.winLoseCom,
                                    'commission.member.winLose': transaction.winLose.member.winLose,
                                    'commission.member.totalWinLoseCom': transaction.winLose.member.totalWinLoseCom,
                                    'validAmount': transaction.validAmount,
                                    'betResult': transaction.result,
                                    'isEndScore': true,
                                    'status': 'DONE',
                                    'settleDate': DateUtils.getCurrentDate()
                                }
                            };

                            BetTransactionModel.findOneAndUpdate({_id: transaction._id}, updateBody, {new: true}, function (err, response) {
                                if (err) {
                                    subCallback(err, null);
                                } else {
                                        let object = Object.assign({}, response)._doc;
                                        object.memberParentGroup = betObj.memberParentGroup;
                                        object.memberId = betObj.memberId;
                                        subCallback(null, response);

                                }
                            });

                        }], function (err, asyncResponse) {
                            if (err) {
                                // asyncCallback(err, null);
                            } else {
                                // keepTransaction.push(asyncResponse[0]);
                                // asyncCallback(null, 'success');
                                console.log('success 1')
                            }
                        });
                    }

                });

            }
        });

        cursor.on('end', function () {
            console.log('Done!');

            callback(null, {});
        });
        // callback(null, {});

    }), (callback => {

        let condition = {
            game: {$in:['FOOTBALL','M2']},
            'gameType': {$ne: 'TODAY'},
            'parlay.matches.matchId':matchId,
            active: true
        };

        let cursor = BetTransactionModel.find(condition)
            .batchSize(200)
            .select('memberId memberUsername parlay commission amount memberCredit payout memberParentGroup status betId betResult game gameType source')
            .populate({path: 'memberParentGroup', select: 'type endpoint'})
            .populate({path: 'memberId', select: 'username_lower'})
            .lean()
            .cursor();


        cursor.on('data', function (betObj) {

            const allMatch = betObj.parlay.matches;

            let filterList = _.filter(allMatch, function (response) {
                return response.matchId === matchId;
            });

            if (filterList.length === 0) {
                // betListCallback(null, 'not data found');
                return;
            }


            let unCheckMatch = _.filter(allMatch, function (response) {
                return response.matchId !== matchId && !response.betResult;
            });

            console.log('unCheckMatch : ', unCheckMatch.length);

            const match = filterList[0];

            let _homeScore, _awayScore, _round;

            if (result === 'HOME') {
                _homeScore = 1;
                _awayScore = 0;
            } else if (result === 'AWAY') {
                _homeScore = 0;
                _awayScore = 1;
            } else if (result === 'DRAW') {
                _homeScore = 0;
                _awayScore = 0;
            }
            _round = round;

            const fullScoreResult = _homeScore + ':' + _awayScore + ':' + _round;

            if (betObj.status === 'DONE' || betObj.status === 'REJECTED' || betObj.status === 'CANCELLED') {
                let updateBody = {
                    $set: {
                        'parlay.matches.$.fullScore': fullScoreResult,
                        'parlay.matches.$.settleDate': DateUtils.getCurrentDate(),
                        'parlay.matches.$.remark': 'score only',
                    }
                };

                BetTransactionModel.update({
                    _id: betObj._id,
                    'parlay.matches._id': match._id
                }, updateBody, function (err, response) {
                    if (err) {
                        // betListCallback(err, null);
                    } else {
                        // betListCallback(null, response);
                    }
                });

            } else {

                let betResult;


                if (match.oddType === 'AH') {
                    betResult = calculateHandicap(_homeScore, _awayScore, match.handicap, match.bet);
                } else if (match.oddType === 'OU') {
                    betResult = calculateOuScore(_round, 0, match.handicap, match.bet);
                }

                let updateBody;
                let isCalculateReceive = false;
                let winLoseAmount = 0;
                let shareReceive;

                console.log('betResult : ', betResult)

                if (betResult === LOSE) {
                    isCalculateReceive = true;
                    winLoseAmount = 0 - betObj.amount;
                    shareReceive = prepareShareReceive(winLoseAmount, LOSE, betObj);
                    updateBody = {
                        $set: {
                            'parlay.matches.$.fullScore': fullScoreResult,
                            'parlay.matches.$.betResult': betResult,
                            'parlay.matches.$.settleDate': DateUtils.getCurrentDate(),
                            'parlay.matches.$.remark': 'lose',

                            'commission.superAdmin.winLoseCom': shareReceive.superAdmin.winLoseCom,
                            'commission.superAdmin.winLose': shareReceive.superAdmin.winLose,
                            'commission.superAdmin.totalWinLoseCom': shareReceive.superAdmin.totalWinLoseCom,

                            'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                            'commission.company.winLose': shareReceive.company.winLose,
                            'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                            'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                            'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                            'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                            'commission.api.winLoseCom': shareReceive.api.winLoseCom,
                            'commission.api.winLose': shareReceive.api.winLose,
                            'commission.api.totalWinLoseCom': shareReceive.api.totalWinLoseCom,

                            'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                            'commission.senior.winLose': shareReceive.senior.winLose,
                            'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                            'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                            'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                            'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                            'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                            'commission.agent.winLose': shareReceive.agent.winLose,
                            'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                            'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                            'commission.member.winLose': shareReceive.member.winLose,
                            'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                            'validAmount': betObj.amount,
                            'betResult': betResult,
                            'isEndScore': true,
                            'status': 'DONE',
                            'settleDate': DateUtils.getCurrentDate()
                        }
                    };
                } else {
                    // NOT EQUALS LOSE

                    if (unCheckMatch.length == 0) {
                        console.log('no unCheck match');
                        isCalculateReceive = true;

                        let totalUnCheckOdds = _.filter(allMatch, function (response) {
                            return response.matchId !== matchId && response.betResult;
                        }).map((item) => {
                            return calculateParlayOdds(item.betResult, item.odd);
                        });

                        console.log(' success match');
                        console.log(totalUnCheckOdds);


                        let sumTotalUnCheckOdds = _.reduce(totalUnCheckOdds, (memo, item) => {
                            return {sum: memo.sum * item};
                        }, {sum: 1}).sum;


                        let halfLoseCount = _.filter(allMatch, function (response) {
                            return response.betResult === HALF_LOSE && response.matchId !== matchId;
                        }).length;

                        //prepare with last match result
                        sumTotalUnCheckOdds = roundTo.down(sumTotalUnCheckOdds * calculateParlayOdds(betResult, match.odd), 3);

                        if (betResult === HALF_LOSE) {
                            halfLoseCount += 1;
                        }

                        console.log('sumTotalUnCheckOdds : ', sumTotalUnCheckOdds);
                        console.log('halfLoseCount : ', halfLoseCount);

                        winLoseAmount = (subPlyAmount(betObj.amount, halfLoseCount) * sumTotalUnCheckOdds);
                        let validAmount = subPlyAmount(betObj.amount, halfLoseCount);

                        winLoseAmount = winLoseAmount - betObj.amount;


                        //FOR REMARK
                        let remark = _.filter(allMatch, function (response) {
                            return response.matchId !== matchId && response.betResult;
                        }).map((item) => {
                            return 'matchId : ' + item.matchId + ' , result : ' + item.betResult + ' , odd : ' + calculateParlayOdds(item.betResult, item.odd);
                        });

                        remark.push('matchId : ' + matchId + ' , result : ' + betResult + ' , odd : ' + calculateParlayOdds(betResult, match.odd))


                        shareReceive = prepareShareReceive(winLoseAmount, WIN, betObj);
                        updateBody = {
                            $set: {
                                'parlay.matches.$.fullScore': fullScoreResult,
                                'parlay.matches.$.betResult': betResult,
                                'parlay.matches.$.settleDate': DateUtils.getCurrentDate(),
                                'parlay.matches.$.remark': remark.join('|'),

                                'commission.superAdmin.winLoseCom': shareReceive.superAdmin.winLoseCom,
                                'commission.superAdmin.winLose': shareReceive.superAdmin.winLose,
                                'commission.superAdmin.totalWinLoseCom': shareReceive.superAdmin.totalWinLoseCom,

                                'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                                'commission.company.winLose': shareReceive.company.winLose,
                                'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                                'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                                'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                                'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                                'commission.api.winLoseCom': shareReceive.api.winLoseCom,
                                'commission.api.winLose': shareReceive.api.winLose,
                                'commission.api.totalWinLoseCom': shareReceive.api.totalWinLoseCom,

                                'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                                'commission.senior.winLose': shareReceive.senior.winLose,
                                'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                                'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                                'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                                'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                                'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                                'commission.agent.winLose': shareReceive.agent.winLose,
                                'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                                'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                                'commission.member.winLose': shareReceive.member.winLose,
                                'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                                'validAmount': validAmount,
                                'betResult': 'WIN',
                                'isEndScore': true,
                                'status': 'DONE',
                                'settleDate': DateUtils.getCurrentDate()
                            }
                        };
                        updateBody.$set.status = 'DONE';

                    } else {
                        updateBody = {
                            $set: {
                                'parlay.matches.$.fullScore': fullScoreResult,
                                'parlay.matches.$.betResult': betResult,
                                'parlay.matches.$.settleDate': DateUtils.getCurrentDate(),
                                'parlay.matches.$.remark': 'unCheckMatch : ' + unCheckMatch.length,
                            }
                        };
                        console.log('have unCheck match : ', unCheckMatch);
                        console.log(unCheckMatch);

                    }
                }


                console.log('isCalculateReceive : ', isCalculateReceive)
                if (isCalculateReceive) {
                    updateAgentMemberBalance(shareReceive, 'PARLAY', `SETTLE_${requestBody.key}_${betObj.ref1}`, (err, response) => {
                        if (err) {
                            if (err == 888) {
                                // betListCallback(null, {});
                            } else {
                                // betListCallback(err, null);
                            }
                        } else {
                            BetTransactionModel.findOneAndUpdate({
                                _id: betObj._id,
                                'parlay.matches._id': match._id
                            }, updateBody, {new: true}, function (err, response) {
                                if (err) {
                                    // betListCallback(err, null);
                                    console.log(err)
                                } else {
                                        let object = Object.assign({}, response)._doc;
                                        object.memberParentGroup = betObj.memberParentGroup;
                                        object.memberId = betObj.memberId;

                                    // betListCallback(null, response);
                                    console.log('DONE')

                                }
                            });
                        }
                    });
                } else {

                    BetTransactionModel.findOneAndUpdate({
                        _id: betObj._id,
                        'parlay.matches._id': match._id
                    }, updateBody, {new: true}, function (err, response) {
                        if (err) {
                            // betListCallback(err, null);
                        } else {

                            console.log('DONE')

                        }
                    });
                }
            }
        });

        cursor.on('end', function () {
            console.log('Done!');
            callback(null, {});
        });


    })], (err, asyncResponse) => {

        if (err) {
            endScoreCallback(err, null);
            return;
        }
        endScoreCallback(null, 'success');
    });

}


function reEndScoreMuayThaiFunction(requestBody, endScoreCallback) {
    let matchId = requestBody.matchId;
    let result = requestBody.result;
    let round = Number.parseInt(requestBody.round);

    console.log('-- endScoreFunction --');

    async.parallel([(callback => {

        let condition = {
            'game': {$in: ['FOOTBALL', 'M2']},
            'hdp.matchId': matchId,
            'status':{$in:['DONE','REJECTED']},
            'gameType': 'TODAY',
            'active': true
        };


        let orCondition = [];

        orCondition.push({$or: [{'source': 'MUAY_THAI'}, {'source': 'M2_MUAY'}]});

        if (orCondition.length > 0) {
            condition['$and'] = orCondition;
        }

        console.log(condition)

        let cursor = BetTransactionModel.find(condition)
            .batchSize(200)
            .select('memberId memberUsername hdp commission amount memberCredit payout memberParentGroup betId status priceType betResult game gameType source')
            .populate({path: 'memberParentGroup', select: 'type endpoint secretKey'})
            .populate({path: 'memberId', select: 'username_lower'})
            .lean()
            .cursor();


        cursor.on('data', function (betObj) {

            if (betObj.status === 'REJECTED') {
                let updateBody = {
                    $set: {
                        'isEndScore': true
                    }
                };

                BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                    if (err) {
                        // asyncCallback(err, null);
                    } else {
                        // asyncCallback(null, response);
                        console.log('xxx')
                    }
                });

            } else {
                //NOT EQUAL REJECTED

                console.log("========================= : ", betObj.hdp.handicap);
                let betResult;

                let _homeScore, _awayScore, _round;

                if (result === 'HOME') {
                    _homeScore = 1;
                    _awayScore = 0;
                } else if (result === 'AWAY') {
                    _homeScore = 0;
                    _awayScore = 1;
                } else if (result === 'DRAW') {
                    _homeScore = 0;
                    _awayScore = 0;
                }
                _round = round;


                if (betObj.hdp.oddType === 'AH') {
                    betResult = calculateHandicap(_homeScore, _awayScore, betObj.hdp.handicap, betObj.hdp.bet);
                } else if (betObj.hdp.oddType === 'OU') {
                    betResult = calculateOuScore(_round, 0, betObj.hdp.handicap, betObj.hdp.bet);
                }


                console.log("betResult : ", betResult);

                let amount = Math.abs(betObj.memberCredit);
                let validAmount;
                let winLose = 0;
                if (betResult === DRAW) {
                    winLose = 0;
                    validAmount = 0;
                } else if (betResult === WIN) {
                    FormulaUtils.calculatePayout(betObj.amount, betObj.hdp.odd, betObj.hdp.oddType, betObj.priceType, (payout) => {
                        winLose = Number.parseFloat(payout) - betObj.amount;
                    });
                    validAmount = betObj.amount;
                } else if (betResult === HALF_WIN) {
                    FormulaUtils.calculatePayout(betObj.amount, betObj.hdp.odd, betObj.hdp.oddType, betObj.priceType, (payout) => {
                        winLose = (Number.parseFloat(payout) - betObj.amount) / 2;
                    });
                    validAmount = betObj.amount / 2;
                } else if (betResult === LOSE) {
                    winLose = 0 - amount;
                    validAmount = betObj.amount;
                } else if (betResult === HALF_LOSE) {
                    winLose = 0 - (amount / 2);
                    validAmount = betObj.amount / 2;
                }

                const shareReceive = prepareShareReceive(winLose, betResult, betObj);

                let transaction = {
                    _id: betObj._id,
                    memberId: betObj.memberId,
                    matchId: matchId,
                    type: requestBody.type,
                    handicap: betObj.hdp.handicap,
                    oddType: betObj.hdp.oddType,
                    bet: betObj.hdp.bet,
                    betAmount: betObj.amount,
                    homeScoreFT: _homeScore,
                    awayScoreFT: _awayScore,
                    result: betResult,
                    transStatus: 'DONE',
                    validAmount: validAmount,
                    winLose: shareReceive
                };

                // updateAgentMemberBalance(shareReceive, 'TODAY', requestBody.key, (err, response) => {

                async.series([subCallback => {
                    //refundBalance
                    AgentService.rollbackCalculateAgentMemberBalance(betObj, requestBody.key, (err, response) => {
                        if (err) {
                            subCallback(err, null);
                        } else {
                            subCallback(null, response);
                        }
                    });

                }, subCallback => {

                    updateAgentMemberBalance(shareReceive, 'TODAY', requestBody.key, (err, response) => {
                        if (err) {
                            if (err == 888) {
                                subCallback(null, {});
                            } else {
                                subCallback(err, null);
                            }
                        } else {


                            let updateBody = {
                                $set: {
                                    'hdp.fullScore': transaction.homeScoreFT + ':' + transaction.awayScoreFT + ':' + _round,

                                    'commission.superAdmin.winLoseCom': transaction.winLose.superAdmin.winLoseCom,
                                    'commission.superAdmin.winLose': transaction.winLose.superAdmin.winLose,
                                    'commission.superAdmin.totalWinLoseCom': transaction.winLose.superAdmin.totalWinLoseCom,

                                    'commission.company.winLoseCom': transaction.winLose.company.winLoseCom,
                                    'commission.company.winLose': transaction.winLose.company.winLose,
                                    'commission.company.totalWinLoseCom': transaction.winLose.company.totalWinLoseCom,

                                    'commission.shareHolder.winLoseCom': transaction.winLose.shareHolder.winLoseCom,
                                    'commission.shareHolder.winLose': transaction.winLose.shareHolder.winLose,
                                    'commission.shareHolder.totalWinLoseCom': transaction.winLose.shareHolder.totalWinLoseCom,

                                    'commission.api.winLoseCom': transaction.winLose.api.winLoseCom,
                                    'commission.api.winLose': transaction.winLose.api.winLose,
                                    'commission.api.totalWinLoseCom': transaction.winLose.api.totalWinLoseCom,

                                    'commission.senior.winLoseCom': transaction.winLose.senior.winLoseCom,
                                    'commission.senior.winLose': transaction.winLose.senior.winLose,
                                    'commission.senior.totalWinLoseCom': transaction.winLose.senior.totalWinLoseCom,

                                    'commission.masterAgent.winLoseCom': transaction.winLose.masterAgent.winLoseCom,
                                    'commission.masterAgent.winLose': transaction.winLose.masterAgent.winLose,
                                    'commission.masterAgent.totalWinLoseCom': transaction.winLose.masterAgent.totalWinLoseCom,

                                    'commission.agent.winLoseCom': transaction.winLose.agent.winLoseCom,
                                    'commission.agent.winLose': transaction.winLose.agent.winLose,
                                    'commission.agent.totalWinLoseCom': transaction.winLose.agent.totalWinLoseCom,

                                    'commission.member.winLoseCom': transaction.winLose.member.winLoseCom,
                                    'commission.member.winLose': transaction.winLose.member.winLose,
                                    'commission.member.totalWinLoseCom': transaction.winLose.member.totalWinLoseCom,
                                    'validAmount': transaction.validAmount,
                                    'betResult': transaction.result,
                                    'isEndScore': true,
                                    'status': 'DONE',
                                    'settleDate': DateUtils.getCurrentDate()
                                }
                            };

                            BetTransactionModel.findOneAndUpdate({_id: transaction._id}, updateBody, {new: true}, function (err, response) {
                                if (err) {
                                    subCallback(err, null);
                                } else {
                                        let object = Object.assign({}, response)._doc;
                                        object.memberParentGroup = betObj.memberParentGroup;
                                        object.memberId = betObj.memberId;
                                        subCallback(null, response);
                                }
                            });


                        }

                    });

                }], function (err, response) {
                    if (err) {
                        // asyncCallback(err, null);
                    } else {
                        // asyncCallback(null, response);
                        console.log('success 1')
                    }
                });
            }

        });

        cursor.on('end', function () {
            console.log('Done!');

            callback(null, {});
        });


    }), (callback => {

        let condition = {
            'game': {$in: ['FOOTBALL', 'M2']},
            'gameType': {$ne: 'TODAY'},
            'parlay.matches.matchId':matchId,
            active: true
        };


        let cursor = BetTransactionModel.find(condition)
            .batchSize(200)
            .select('memberId memberUsername parlay commission amount memberCredit payout memberParentGroup status betId betResult game gameType source')
            .populate({path: 'memberParentGroup', select: 'type endpoint'})
            .populate({path: 'memberId', select: 'username_lower'})
            .lean()
            .cursor();


        cursor.on('data', function (betObj) {

            const allMatch = betObj.parlay.matches;

            let filterList = _.filter(allMatch, function (response) {
                return response.matchId === matchId;
            });

            if (filterList.length === 0) {
                // betListCallback(null, 'not data found');
                return;
            }


            let unCheckMatch = _.filter(allMatch, function (response) {
                return response.matchId !== matchId && !response.betResult;
            });

            console.log('unCheckMatch : ', unCheckMatch.length);

            const match = filterList[0];

            let _homeScore, _awayScore, _round;

            if (result === 'HOME') {
                _homeScore = 1;
                _awayScore = 0;
            } else if (result === 'AWAY') {
                _homeScore = 0;
                _awayScore = 1;
            } else if (result === 'DRAW') {
                _homeScore = 0;
                _awayScore = 0;
            }
            _round = round;

            const fullScoreResult = _homeScore + ':' + _awayScore + ':' + _round;

            let betResult;


            if (match.oddType === 'AH') {
                betResult = calculateHandicap(_homeScore, _awayScore, match.handicap, match.bet);
            } else if (match.oddType === 'OU') {
                betResult = calculateOuScore(_round, 0, match.handicap, match.bet);
            }


            let updateBody;
            let isCalculateReceive = false;
            let isRefundBalance = false;
            let winLoseAmount = 0;
            let shareReceive;

            if (match.betResult === betResult) {
                console.log('same result : update score only');
                updateBody = {
                    $set: {
                        'parlay.matches.$.fullScore': fullScoreResult,
                        'parlay.matches.$.betResult': betResult,
                    }
                };

            } else {

                let isUnCheckMatchLose = _.filter(allMatch, function (response) {
                        return response.matchId !== matchId && response.betResult === LOSE;
                    }).length > 0;

                if (isUnCheckMatchLose || betResult === LOSE) {
                    isRefundBalance = true;
                    isCalculateReceive = true;
                    winLoseAmount = 0 - betObj.amount;
                    shareReceive = prepareShareReceive(winLoseAmount, LOSE, betObj);
                    updateBody = {
                        $set: {
                            'parlay.matches.$.fullScore': fullScoreResult,
                            'parlay.matches.$.betResult': betResult,

                            'commission.superAdmin.winLoseCom': shareReceive.superAdmin.winLoseCom,
                            'commission.superAdmin.winLose': shareReceive.superAdmin.winLose,
                            'commission.superAdmin.totalWinLoseCom': shareReceive.superAdmin.totalWinLoseCom,

                            'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                            'commission.company.winLose': shareReceive.company.winLose,
                            'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                            'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                            'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                            'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                            'commission.api.winLoseCom': shareReceive.api.winLoseCom,
                            'commission.api.winLose': shareReceive.api.winLose,
                            'commission.api.totalWinLoseCom': shareReceive.api.totalWinLoseCom,

                            'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                            'commission.senior.winLose': shareReceive.senior.winLose,
                            'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                            'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                            'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                            'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                            'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                            'commission.agent.winLose': shareReceive.agent.winLose,
                            'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                            'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                            'commission.member.winLose': shareReceive.member.winLose,
                            'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                            'validAmount': betObj.amount,
                            'betResult': betResult,
                            'isEndScore': true,
                            'status': 'DONE',
                            'settleDate': DateUtils.getCurrentDate()
                        }
                    };
                } else {
                    // NOT EQUALS LOSE

                    if (unCheckMatch.length == 0) {
                        console.log('no unCheck match');
                        isRefundBalance = true;
                        isCalculateReceive = true;

                        let totalUnCheckOdds = _.filter(allMatch, function (response) {
                            return response.matchId !== matchId && response.betResult;
                        }).map((item) => {
                            return calculateParlayOdds(item.betResult, item.odd);
                        });

                        console.log(' success match');
                        console.log(totalUnCheckOdds);

                        let sumTotalUnCheckOdds = _.reduce(totalUnCheckOdds, (memo, item) => {
                            return {sum: memo.sum * item};
                        }, {sum: 1}).sum;


                        let halfLoseCount = _.filter(allMatch, function (response) {
                            return response.betResult === HALF_LOSE && response.matchId !== matchId;
                        }).length;

                        //prepare with last match result
                        sumTotalUnCheckOdds = roundTo.down(sumTotalUnCheckOdds * calculateParlayOdds(betResult, match.odd), 3);

                        if (betResult === HALF_LOSE) {
                            halfLoseCount += 1;
                        }

                        console.log('sumTotalUnCheckOdds : ', sumTotalUnCheckOdds);
                        console.log('halfLoseCount : ', halfLoseCount);

                        winLoseAmount = (subPlyAmount(betObj.amount, halfLoseCount) * sumTotalUnCheckOdds);
                        let validAmount = subPlyAmount(betObj.amount, halfLoseCount);

                        winLoseAmount = winLoseAmount - betObj.amount;

                        shareReceive = prepareShareReceive(winLoseAmount, WIN, betObj);
                        updateBody = {
                            $set: {
                                'parlay.matches.$.fullScore': fullScoreResult,
                                'parlay.matches.$.betResult': betResult,
                                'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                                'commission.company.winLose': shareReceive.company.winLose,
                                'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                                'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                                'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                                'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                                'commission.api.winLoseCom': shareReceive.api.winLoseCom,
                                'commission.api.winLose': shareReceive.api.winLose,
                                'commission.api.totalWinLoseCom': shareReceive.api.totalWinLoseCom,

                                'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                                'commission.senior.winLose': shareReceive.senior.winLose,
                                'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                                'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                                'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                                'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                                'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                                'commission.agent.winLose': shareReceive.agent.winLose,
                                'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                                'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                                'commission.member.winLose': shareReceive.member.winLose,
                                'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                                'validAmount': validAmount,
                                'betResult': 'WIN',
                                'isEndScore': true,
                                'status': 'DONE',
                                'settleDate': DateUtils.getCurrentDate()
                            }
                        };
                        updateBody.$set.status = 'DONE';

                    } else {
                        updateBody = {
                            $set: {
                                'parlay.matches.$.fullScore': fullScoreResult,
                                'parlay.matches.$.betResult': betResult,
                                // 'betResult': betResult,
                            }
                        };
                        console.log('have unCheck match : ', unCheckMatch);
                        console.log(unCheckMatch);

                    }
                }
            }


            async.series([callback => {
                //refundBalance
                if (isRefundBalance) {
                    AgentService.rollbackCalculateAgentMemberBalance(betObj, '', (err, response) => {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, response);
                        }
                    });
                } else {
                    callback(null, '');
                }

            }, callback => {

                BetTransactionModel.findOneAndUpdate({
                    _id: betObj._id,
                    'parlay.matches._id': match._id
                }, updateBody, {new: true}, function (err, response) {
                    if (err) {
                        callback(err, null);
                    } else {

                            callback(null, '');


                    }
                });

            }, callback => {
                if (isCalculateReceive) {
                    updateAgentMemberBalance(shareReceive, 'PARLAY', requestBody.key, (err, response) => {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, response);
                        }
                    });
                } else {
                    callback(null, '');
                }
            }], function (err, asyncResponse) {
                if (err) {

                } else {
                    console.log('success 1')
                }
            });


        });

        cursor.on('end', function () {
            console.log('Done!');
            callback(null, {});
        });


    })], (err, asyncResponse) => {

        if (err) {
            endScoreCallback(err, null);
            return;
        }
        endScoreCallback(null, 'success');
    });

}


function endScoreSportFirstGoalFunction(requestBody, endScoreCallback) {
    let matchId = requestBody.matchId;
    let homeScore = Number.parseInt(requestBody.homeScore);
    let awayScore = Number.parseInt(requestBody.awayScore);
    let round = Number.parseInt(requestBody.round);

    console.log('-- endScoreFunction --');

    async.parallel([(callback => {


        let oddType = round == 1 ? '1STG' :
            round == 2 ? '2NDG' :
                round == 3 ? '3RDG' :
                    round == 4 ? '4THG' :
                        round == 5 ? '5THG' :
                            round == 6 ? '6THG' :
                                round == 7 ? '7THG' :
                                    round == 8 ? '8THG' :
                                        round == 9 ? '9THG' : '';


        let condition = {
            'game': 'FOOTBALL',
            'hdp.matchId': matchId,
            'hdp.oddType': oddType,
            'gameType': 'TODAY',
            'active': true
        };


        let orCondition = [];

        orCondition.push({$or: [{'status': 'RUNNING'}, {'status': 'REJECTED'}]});


        if (orCondition.length > 0) {
            condition['$and'] = orCondition;
        }

        console.log(condition)

        let cursor = BetTransactionModel.find(condition)
            .batchSize(200)
            .select('memberId memberUsername hdp commission amount memberCredit payout memberParentGroup betId status priceType betResult game gameType source')
            .populate({path: 'memberParentGroup', select: 'type endpoint secretKey'})
            .populate({path: 'memberId', select: 'username_lower'})
            .lean()
            .cursor();


        cursor.on('data', function (betObj) {

            if (betObj.status === 'REJECTED') {
                let updateBody = {
                    $set: {
                        'isEndScore': true
                    }
                };

                BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                    if (err) {
                        // asyncCallback(err, null);
                    } else {
                        // asyncCallback(null, response);
                        console.log('xxx')
                    }
                });

            } else {
                //NOT EQUAL REJECTED

                let betResult;

                let _homeScore, _awayScore, _round;

                _homeScore = homeScore;
                _awayScore = awayScore;


                betResult = calculateHandicap(_homeScore, _awayScore, betObj.hdp.handicap, betObj.hdp.bet);


                console.log("betResult : ", betResult);

                let amount = Math.abs(betObj.memberCredit);
                let validAmount;
                let winLose = 0;
                if (betResult === DRAW) {
                    winLose = 0;
                    validAmount = 0;
                } else if (betResult === WIN) {
                    FormulaUtils.calculatePayout(betObj.amount, betObj.hdp.odd, betObj.hdp.oddType, betObj.priceType, (payout) => {
                        winLose = Number.parseFloat(payout) - betObj.amount;
                    });
                    validAmount = betObj.amount;
                } else if (betResult === HALF_WIN) {
                    FormulaUtils.calculatePayout(betObj.amount, betObj.hdp.odd, betObj.hdp.oddType, betObj.priceType, (payout) => {
                        winLose = (Number.parseFloat(payout) - betObj.amount) / 2;
                    });
                    validAmount = betObj.amount / 2;
                } else if (betResult === LOSE) {
                    winLose = 0 - amount;
                    validAmount = betObj.amount;
                } else if (betResult === HALF_LOSE) {
                    winLose = 0 - (amount / 2);
                    validAmount = betObj.amount / 2;
                }

                const shareReceive = prepareShareReceive(winLose, betResult, betObj);

                let transaction = {
                    _id: betObj._id,
                    memberId: betObj.memberId,
                    matchId: matchId,
                    type: requestBody.type,
                    handicap: betObj.hdp.handicap,
                    oddType: betObj.hdp.oddType,
                    bet: betObj.hdp.bet,
                    betAmount: betObj.amount,
                    homeScoreFT: _homeScore,
                    awayScoreFT: _awayScore,
                    result: betResult,
                    transStatus: 'DONE',
                    validAmount: validAmount,
                    winLose: shareReceive
                };

                // updateAgentMemberBalance(shareReceive, 'TODAY', requestBody.key, (err, response) => {
                updateAgentMemberBalance(shareReceive, 'TODAY', '', (err, response) => {
                    if (err) {
                        if (err == 888) {
                            // asyncCallback(null, {});
                        } else {
                            // asyncCallback(err, null);
                        }
                    } else {
                        async.series([subCallback => {

                            let updateBody = {
                                $set: {
                                    'hdp.fullScore': transaction.homeScoreFT + ':' + transaction.awayScoreFT,

                                    'commission.superAdmin.winLoseCom': transaction.winLose.superAdmin.winLoseCom,
                                    'commission.superAdmin.winLose': transaction.winLose.superAdmin.winLose,
                                    'commission.superAdmin.totalWinLoseCom': transaction.winLose.superAdmin.totalWinLoseCom,

                                    'commission.company.winLoseCom': transaction.winLose.company.winLoseCom,
                                    'commission.company.winLose': transaction.winLose.company.winLose,
                                    'commission.company.totalWinLoseCom': transaction.winLose.company.totalWinLoseCom,

                                    'commission.shareHolder.winLoseCom': transaction.winLose.shareHolder.winLoseCom,
                                    'commission.shareHolder.winLose': transaction.winLose.shareHolder.winLose,
                                    'commission.shareHolder.totalWinLoseCom': transaction.winLose.shareHolder.totalWinLoseCom,

                                    'commission.api.winLoseCom': transaction.winLose.api.winLoseCom,
                                    'commission.api.winLose': transaction.winLose.api.winLose,
                                    'commission.api.totalWinLoseCom': transaction.winLose.api.totalWinLoseCom,

                                    'commission.senior.winLoseCom': transaction.winLose.senior.winLoseCom,
                                    'commission.senior.winLose': transaction.winLose.senior.winLose,
                                    'commission.senior.totalWinLoseCom': transaction.winLose.senior.totalWinLoseCom,

                                    'commission.masterAgent.winLoseCom': transaction.winLose.masterAgent.winLoseCom,
                                    'commission.masterAgent.winLose': transaction.winLose.masterAgent.winLose,
                                    'commission.masterAgent.totalWinLoseCom': transaction.winLose.masterAgent.totalWinLoseCom,

                                    'commission.agent.winLoseCom': transaction.winLose.agent.winLoseCom,
                                    'commission.agent.winLose': transaction.winLose.agent.winLose,
                                    'commission.agent.totalWinLoseCom': transaction.winLose.agent.totalWinLoseCom,

                                    'commission.member.winLoseCom': transaction.winLose.member.winLoseCom,
                                    'commission.member.winLose': transaction.winLose.member.winLose,
                                    'commission.member.totalWinLoseCom': transaction.winLose.member.totalWinLoseCom,
                                    'validAmount': transaction.validAmount,
                                    'betResult': transaction.result,
                                    'isEndScore': true,
                                    'status': 'DONE',
                                    'settleDate': DateUtils.getCurrentDate()
                                }
                            };

                            BetTransactionModel.findOneAndUpdate({_id: transaction._id}, updateBody, {new: true}, function (err, response) {
                                if (err) {
                                    subCallback(err, null);
                                } else {
                                        let object = Object.assign({}, response)._doc;
                                        object.memberParentGroup = betObj.memberParentGroup;
                                        object.memberId = betObj.memberId;
                                        subCallback(null, response);
                                }
                            });

                        }], function (err, asyncResponse) {
                            if (err) {
                                // asyncCallback(err, null);
                            } else {
                                // keepTransaction.push(asyncResponse[0]);
                                // asyncCallback(null, 'success');
                                console.log('success 1')
                            }
                        });
                    }

                });

            }
        });

        cursor.on('end', function () {
            console.log('Done!');

            callback(null, {});
        });


    }), (callback => {

        callback(null, {})
    })], (err, asyncResponse) => {

        if (err) {
            endScoreCallback(err, null);
            return;
        }

        // let transaction = asyncResponse[0].concat(asyncResponse[1]);
        //
        // console.log(transaction)

        // let apiResponse = _.filter(transaction, (item) => {
        //     return item && item.memberParentGroup.type === 'API' && item.status === 'DONE';
        // });


        // if (apiResponse.length > 0) {
        //     let settleInfo = {
        //         action: "SETTLE",
        //         matchId: matchId,
        //         scoreFT: homeScoreFull + ':' + awayScoreFull,
        //         scoreHT: homeScoreHalf + ':' + awayScoreHalf
        //
        //     };
        //
        //     let endpoint = 'http://localhost:8002/spa/api';
        //     ApiService.settle(endpoint, settleInfo, apiResponse, (err, response) => {
        //         if (err) {
        //             endScoreCallback(err, null);
        //         } else {
        //             endScoreCallback(null, response);
        //         }
        //
        //     });
        // } else {
        //
        //     if (err) {
        //         endScoreCallback(err, null);
        //     } else {
        //         endScoreCallback(null, 'success');
        //     }
        // }
        endScoreCallback(null, 'success');
    });

}

function calculateParlayOdds(betResult, odd) {
    if (betResult === WIN) {
        return odd;
    } else if (betResult === HALF_WIN) {
        return 1 + ((odd - 1) / 2);
    } else if (betResult === DRAW) {
        return 1;
    } else if (betResult === HALF_LOSE) {
        return 1;
    }
    return odd;
}

function updateAgentMemberBalance(shareReceive, gameType, key, updateCallback) {

    let memberReceiveAmount;

    console.log(key,'memberCredit : ' + shareReceive.memberCredit + '  , betAmount : ' + shareReceive.betAmount + ' , betResult : ' + shareReceive.betResult);

    if (gameType === 'PARLAY' || gameType === 'MIX_STEP') {
        if (shareReceive.betResult === WIN) {
            memberReceiveAmount = Math.abs(shareReceive.memberCredit) + shareReceive.member.totalWinLoseCom;
        } else {
            console.log('memberReceiveAmount ::: ' + shareReceive.betAmount + ' : ' + shareReceive.member.totalWinLoseCom)
            memberReceiveAmount = shareReceive.betAmount + shareReceive.member.totalWinLoseCom;
        }
    } else {

        console.log('memberReceiveAmount ::: ' + Math.abs(shareReceive.memberCredit) + ' : ' + shareReceive.member.totalWinLoseCom)
        memberReceiveAmount = roundTo(Math.abs(shareReceive.memberCredit) + shareReceive.member.totalWinLoseCom, 2);
    }

    MemberService.updateBalance2(shareReceive.memberUsername, memberReceiveAmount, shareReceive.betId, 'SPORTS_BOOK_END_SCORE', key, function (err, updateMemberResponse) {
        if (err) {
            updateCallback(err, null);
        } else {
            updateCallback(null, updateMemberResponse);
        }
    });
}
//TODO dup in cancel match in stock
function prepareShareReceive(winLose, betResult, betTransaction) {

    let winLoseInfo;
    const amount = betTransaction.amount;

    if (betResult !== DRAW) {

        let memberCommission = validateCommission(betResult, betTransaction.commission.member.commission);

        let superAdminShare = betTransaction.commission.superAdmin.shareReceive;

        let companyShare = betTransaction.commission.company.shareReceive;
        let companyCommission = validateCommission(betResult, betTransaction.commission.company.commission || 0);

        let shareHolderShare = betTransaction.commission.shareHolder.shareReceive;
        let shareHolderCommission = validateCommission(betResult, betTransaction.commission.shareHolder.commission || 0);

        let apiShare = betTransaction.commission.api.shareReceive;
        let apiCommission = validateCommission(betResult, betTransaction.commission.api.commission || 0);

        let seniorShare = betTransaction.commission.senior.shareReceive || 0;
        let seniorCommission = validateCommission(betResult, betTransaction.commission.senior.commission || 0);

        let masterAgentShare = betTransaction.commission.masterAgent.shareReceive || 0;
        let masterAgentCommission = validateCommission(betResult, betTransaction.commission.masterAgent.commission || 0);

        let agentShare = betTransaction.commission.agent.shareReceive || 0;
        let agentCommission = validateCommission(betResult, betTransaction.commission.agent.commission || 0);

        function validateCommission(betResult, commission) {
            return betResult === HALF_LOSE || betResult === HALF_WIN ? (commission / 2) : commission;
        }


        //SUPER ADMIN
        let superAdminTotalUpperShare = 0;
        let superAdminOwnAndChildShare = companyShare + shareHolderShare + seniorShare + masterAgentShare + agentShare;

        let superAdminReceiveCommission = 0;
        let superAdminGiveCommission = companyCommission;

        console.log('TOTAL W/L : ', winLose);
        let superAdminWinLose = roundTo(0 - (winLose * convertPercent(betTransaction.commission.superAdmin.shareReceive)), 3);
        let superAdminFinalCom = calculateCommission(amount,
            superAdminShare,
            superAdminTotalUpperShare,
            superAdminReceiveCommission,
            superAdminOwnAndChildShare,
            superAdminGiveCommission);

        console.log('superAdmin share : ', betTransaction.commission.superAdmin.shareReceive, ' %');
        console.log('superAdmin com % : ', betTransaction.commission.superAdmin.commission, ' %');
        console.log('superAdmin W/L : ', superAdminWinLose);
        console.log('superAdmin COM : ', superAdminFinalCom);
        console.log('superAdmin W/L + COM : ', superAdminWinLose + (superAdminFinalCom));
        console.log('');

        console.log("==================================");

        //COMPANY
        let companyTotalUpperShare = superAdminShare;
        let companyOwnAndChildShare = shareHolderShare + seniorShare + masterAgentShare + agentShare;

        let companyReceiveCommission = 0;
        let companyGiveCommission = shareHolderCommission;

        console.log('TOTAL W/L : ', winLose);
        let companyWinLose = roundTo(0 - (winLose * convertPercent(betTransaction.commission.company.shareReceive)), 3);
        let companyFinalCom = calculateCommission(amount,
            companyShare,
            companyTotalUpperShare,
            companyReceiveCommission,
            companyOwnAndChildShare,
            companyGiveCommission);

        console.log('company share : ', betTransaction.commission.company.shareReceive, ' %');
        console.log('company com % : ', betTransaction.commission.company.commission, ' %');
        console.log('company W/L : ', companyWinLose);
        console.log('company COM : ', companyFinalCom);
        console.log('company W/L + COM : ', companyWinLose + (companyFinalCom));
        console.log('');

        console.log("==================================");
        //SHARE HOLDER
        let shareHolderTotalUpperShare = superAdminShare + companyShare;
        let shareHolderOwnAndChildShare = seniorShare + masterAgentShare + agentShare;
        let shareHolderReceiveCommission = shareHolderCommission;
        let shareHolderGiveCommission = seniorCommission;

        let shareHolderWinLose = roundTo(0 - (winLose * convertPercent(betTransaction.commission.shareHolder.shareReceive)), 3);
        let shareHolderFinalCom = calculateCommission(amount,
            shareHolderShare,
            shareHolderTotalUpperShare,
            shareHolderReceiveCommission,
            shareHolderOwnAndChildShare,
            shareHolderGiveCommission);

        console.log('shareHolder share : ', betTransaction.commission.shareHolder.shareReceive, ' %');
        console.log('shareHolder com % : ', betTransaction.commission.shareHolder.commission, ' %');
        console.log('shareHolder W/L : ', shareHolderWinLose);
        console.log('shareHolder COM : ', shareHolderFinalCom);
        // console.log('shareHolder COM LOSE : ', calculateStep3(1000, shareHolderShare, shareHolderGiveCommission));
        console.log('shareHolder W/L + COM : ', shareHolderWinLose + (shareHolderFinalCom));
        console.log('');

        console.log("==================================");
        //API
        let apiTotalUpperShare = superAdminShare + companyShare;
        let apiOwnAndChildShare = 100 - (superAdminShare + companyShare);
        let apiReceiveCommission = apiCommission;
        let apiGiveCommission = memberCommission;

        let apiWinLose = roundTo(0 - (winLose * convertPercent(betTransaction.commission.api.shareReceive)), 3);
        let apiFinalCom = calculateCommission(amount,
            apiShare,
            apiTotalUpperShare,
            apiReceiveCommission,
            apiOwnAndChildShare,
            apiGiveCommission);

        console.log('api share : ', betTransaction.commission.api.shareReceive, ' %');
        console.log('api com % : ', betTransaction.commission.api.commission, ' %');
        console.log('api W/L : ', apiWinLose);
        console.log('api COM : ', apiFinalCom);
        // console.log('api COM LOSE : ', calculateStep3(1000, apiShare, apiGiveCommission));
        console.log('api W/L + COM : ', apiWinLose + (apiFinalCom));
        console.log('');

        console.log("==================================")
        //SENIOR
        let seniorTotalUpperShare = superAdminShare + companyShare + shareHolderShare;
        let seniorOwnAndChildShare = masterAgentShare + agentShare;
        let seniorReceiveCommission = seniorCommission;
        let seniorGiveCommission = betTransaction.memberParentGroup.type === 'SENIOR' ? memberCommission :
            betTransaction.commission.masterAgent.group ? masterAgentCommission : agentCommission;

        let seniorWinLose = roundTo(0 - (winLose * convertPercent(betTransaction.commission.senior.shareReceive)), 3);
        let seniorFinalCom = calculateCommission(amount,
            seniorShare,
            seniorTotalUpperShare,
            seniorReceiveCommission,
            seniorOwnAndChildShare,
            seniorGiveCommission);

        console.log('xxxx :: ', seniorFinalCom)
        console.log('senior share : ', betTransaction.commission.senior.shareReceive, ' %');
        console.log('senior com % : ', betTransaction.commission.senior.commission, ' %');
        console.log('senior W/L : ', seniorWinLose);
        console.log('senior COM : ', seniorFinalCom);
        // console.log('senior COM LOSE : ', calculateStep3(1000, seniorShare, seniorGiveCommission));
        console.log('senior W/L + COM : ', seniorWinLose + seniorFinalCom);
        console.log('');

        console.log("==================================");

        //MASTER AGENT
        let masterAgentTotalUpperShare = superAdminShare + companyShare + shareHolderShare + seniorShare;
        let masterAgentOwnAndChildShare = agentShare;
        let masterAgentReceiveCommission = masterAgentCommission;
        let masterAgentGiveCommission = betTransaction.memberParentGroup.type === 'MASTER_AGENT' ? memberCommission :
            betTransaction.commission.masterAgent.group ? agentCommission : 0;

        let masterAgentWinLose = roundTo(0 - (winLose * convertPercent(betTransaction.commission.masterAgent.shareReceive)), 3);
        let masterAgentFinalCom = calculateCommission(amount,
            masterAgentShare,
            masterAgentTotalUpperShare,
            masterAgentReceiveCommission,
            masterAgentOwnAndChildShare,
            masterAgentGiveCommission);

        console.log('masterAgent share : ', betTransaction.commission.masterAgent.shareReceive, ' %');
        console.log('masterAgent com % : ', betTransaction.commission.masterAgent.commission, ' %');
        console.log('masterAgent W/L : ', masterAgentWinLose);
        console.log('masterAgent COM : ', masterAgentFinalCom);
        // console.log('masterAgent COM LOSE : ', calculateStep3(1000, masterAgentShare, masterAgentGiveCommission));
        console.log('masterAgent W/L + COM : ', masterAgentWinLose + masterAgentFinalCom);
        console.log('');

        console.log("==================================");
        //AGENT
        let agentTotalUpperShare = superAdminShare + companyShare + shareHolderShare + seniorShare + masterAgentShare;

        let agentOwnAndChildShare = 100 - (superAdminShare + companyShare + shareHolderShare + seniorShare + masterAgentShare + agentShare);
        let agentReceiveCommission = agentCommission;
        let agentGiveCommission = betTransaction.memberParentGroup.type === 'AGENT' ? memberCommission : 0;
        let agentWinLose = roundTo(0 - (winLose * convertPercent(betTransaction.commission.agent.shareReceive)), 3);
        let agentFinalCom = calculateCommission(amount,
            agentShare,
            agentTotalUpperShare,
            agentReceiveCommission,
            agentOwnAndChildShare,
            agentGiveCommission);

        console.log('agent share : ', betTransaction.commission.agent.shareReceive, ' %');
        console.log('agent com % : ', betTransaction.commission.agent.commission, ' %');
        console.log('agent W/L : ', agentWinLose);
        console.log('agent COM : ', agentFinalCom);
        // console.log('agent COM LOSE : ', calculateStep3(1000, agentShare, agentGiveCommission));
        console.log('agent W/L + COM : ', agentWinLose + agentFinalCom);
        console.log('');

        console.log("==================================");
        console.log('member W/L : ', winLose);
        console.log('member COM : ', Math.abs(winLose * convertPercent(memberCommission)));
        console.log('member W/L + COM : ', winLose + Math.abs(winLose * convertPercent(betTransaction.commission.member.commission)));


        function calculateCommission(betPrice, ownShare, totalUpperShare, receiveCommission, ownAndChildShare, giveCommission) {

            let step1 = roundTo(Number.parseFloat(betPrice * (convertPercent(totalUpperShare)) * (receiveCommission / 100)), 3);
            console.log('step1 : ' + betPrice + ' x ' + totalUpperShare + ' x ' + (receiveCommission / 100) + ' = ' + step1);


            let step2 = roundTo(Number.parseFloat(betPrice * (convertPercent(totalUpperShare)) * (giveCommission / 100)), 3);
            console.log('step2 : ' + betPrice + ' x ' + totalUpperShare + ' x ' + (giveCommission / 100) + ' = ' + step2);

            // let step3 = betPrice * convertPercent(ownShare) * convertPercent(giveCommission);
            let step3 = roundTo(calculateStep3(betPrice, ownShare, giveCommission), 3);
            console.log('step3 : ' + betPrice + ' x ' + convertPercent(ownShare) + ' x ' + convertPercent(giveCommission) + ' = ' + step3);

            console.log('step3 : ' + step3);
            console.log('step1 : ' + step1 + ' , step2 : ' + step2 + ' , step3 : ' + step3)

            let result12 = roundTo((step1 - step2), 3);
            let finalResult = roundTo((result12 - step3), 3);
            console.log('result :: ', finalResult)

            return finalResult;
        }

        function calculateStep3(betPrice, ownShare, giveCommission) {
            return betPrice * convertPercent(ownShare) * convertPercent(giveCommission);
        }


        let memberWL = roundTo(winLose, 3);
        let memberWLCom = roundTo(Math.abs(amount * convertPercent(memberCommission)), 3);

        winLoseInfo = {
            betId: betTransaction.betId,
            betAmount: amount,
            memberCredit: betTransaction.memberCredit,
            memberUsername: betTransaction.memberUsername,
            betResult: betResult,
            superAdmin: {
                group: betTransaction.commission.superAdmin.group,
                winLoseCom: roundTo(superAdminFinalCom, 3),
                winLose: roundTo(superAdminWinLose, 3),
                totalWinLoseCom: roundTo((superAdminWinLose + superAdminFinalCom), 3)
            },
            company: {
                group: betTransaction.commission.company.group,
                winLoseCom: roundTo(companyFinalCom, 3),
                winLose: roundTo(companyWinLose, 3),
                totalWinLoseCom: roundTo((companyWinLose + companyFinalCom), 3)
            },
            shareHolder: {
                group: betTransaction.commission.shareHolder.group,
                winLoseCom: roundTo(shareHolderFinalCom, 3),
                winLose: roundTo(shareHolderWinLose, 3),
                totalWinLoseCom: roundTo((shareHolderWinLose + shareHolderFinalCom), 3)
            },
            api: {
                group: betTransaction.commission.api.group,
                winLoseCom: roundTo(apiFinalCom, 3),
                winLose: roundTo(apiWinLose, 3),
                totalWinLoseCom: roundTo((apiWinLose + apiFinalCom), 3)
            },
            senior: {
                group: betTransaction.commission.senior.group,
                winLoseCom: roundTo(seniorFinalCom, 3),
                winLose: roundTo(seniorWinLose, 3),
                totalWinLoseCom: roundTo((seniorWinLose + seniorFinalCom), 3)
            },
            masterAgent: {
                group: betTransaction.commission.masterAgent.group,
                winLoseCom: roundTo(masterAgentFinalCom, 3),
                winLose: roundTo(masterAgentWinLose, 3),
                totalWinLoseCom: roundTo((masterAgentWinLose + masterAgentFinalCom), 3)
            },
            agent: {
                group: betTransaction.commission.agent.group,
                winLoseCom: roundTo(agentFinalCom, 3),
                winLose: roundTo(agentWinLose, 3),
                totalWinLoseCom: roundTo((agentWinLose + agentFinalCom), 3)
            },
            member: {
                id: betTransaction.memberId,
                winLoseCom: memberWLCom,
                winLose: memberWL,
                totalWinLoseCom: roundTo((memberWL + memberWLCom), 3)
            }
        };

    } else {
        winLoseInfo = {
            betId: betTransaction.betId,
            betAmount: amount,
            memberCredit: betTransaction.memberCredit,
            memberUsername: betTransaction.memberUsername,
            betResult: betResult,
            superAdmin: {
                winLoseCom: 0,
                winLose: 0,
                totalWinLoseCom: 0
            },
            company: {
                winLoseCom: 0,
                winLose: 0,
                totalWinLoseCom: 0
            },
            shareHolder: {
                winLoseCom: 0,
                winLose: 0,
                totalWinLoseCom: 0
            },
            api: {
                winLoseCom: 0,
                winLose: 0,
                totalWinLoseCom: 0
            },
            senior: {
                winLoseCom: 0,
                winLose: 0,
                totalWinLoseCom: 0
            },
            masterAgent: {
                winLoseCom: 0,
                winLose: 0,
                totalWinLoseCom: 0
            },
            agent: {
                winLoseCom: 0,
                winLose: 0,
                totalWinLoseCom: 0
            },
            member: {
                id: betTransaction.memberId,
                winLoseCom: 0,
                winLose: 0,
                totalWinLoseCom: 0
            }
        };
    }
    return winLoseInfo;

}


router.post('/test', function (req, res) {

    let handicap = req.body.handicap;
    let homeScore = Number.parseFloat(req.body.homeScore);
    let awayScore = Number.parseFloat(req.body.awayScore);
    let bet = req.body.bet;


    // let start = moment();
    // let end = moment().add(1,'m');

    return res.send(
        {
            code: 0,
            message: "success",
            result: {
                handicap: handicap,
                winLose: calculateHandicap(homeScore, awayScore, handicap, bet)
            }
        }
    );

});

router.post('/testOe', function (req, res) {


    let homeScore = Number.parseFloat(req.body.homeScore);
    let awayScore = Number.parseFloat(req.body.awayScore);
    let bet = req.body.bet;


    return res.send(
        {
            code: 0,
            message: "success",
            result: {
                home: homeScore,
                score: awayScore,
                winLose: calculateOeScore(homeScore, awayScore, bet)
            }
        }
    );

});

router.post('/testOu', function (req, res) {

    let handicap = req.body.handicap;
    let homeScore = Number.parseFloat(req.body.homeScore);
    let awayScore = Number.parseFloat(req.body.awayScore);
    let bet = req.body.bet;


    return res.send(
        {
            code: 0,
            message: "success",
            result: {
                home: homeScore,
                away: awayScore,
                totalScore: homeScore + awayScore,
                handicap: handicap,
                winLose: calculateOuScore(homeScore, awayScore, handicap, bet)
            }
        }
    );

});

router.post('/test12', function (req, res) {

    let homeScore = Number.parseFloat(req.body.homeScore);
    let awayScore = Number.parseFloat(req.body.awayScore);
    let bet = req.body.bet;


    return res.send(
        {
            code: 0,
            message: "success",
            result: {
                home: homeScore,
                score: awayScore,
                winLose: calculateOneTwo(homeScore, awayScore, bet)
            }
        }
    );

});

function calculateOneTwo(home, away, bet) {

    if (bet === 'DRAW' && home == away) {
        return "WIN"
    } else if (bet === 'HOME' && home > away) {
        return "WIN"
    } else if (bet === 'AWAY' && away > home) {
        return "WIN"
    }
    return "LOSE";
}

function calculateOeScore(home, away, bet) {
    let oe = (home + away) % 2;
    if ((bet === 'EVEN' && oe == 0) || (bet === 'ODD' && oe != 0)) {
        return "WIN"
    }
    return "LOSE";
}

function calculateOuScore(home, away, handicap, bet) {

    let totalScore = (home + away);
    let matchHandicap = prePareHandicap(handicap);

    console.log('calculateOuScore');
    console.log('totalScore : ', totalScore);
    console.log('matchHandicap : ', matchHandicap);
    let finalRate;
    if (bet === 'OVER') {
        finalRate = totalScore - matchHandicap;
    } else {
        finalRate = matchHandicap - totalScore;
    }

    let result;
    console.log('finalRate : ', finalRate);
    if (finalRate == 0) {
        result = 'DRAW';
    } else if (finalRate == 0.25) {
        result = 'HALF_WIN';
    } else if (finalRate == -0.25) {
        result = 'HALF_LOSE';
    } else if (finalRate >= 0.5) {
        result = 'WIN';
    } else if (finalRate <= -0.5) {
        result = 'LOSE';
    }
    return result;
}

function calculateFTHT(homeHT, awayHT, homeFT, awayFT, bet) {


    if ((homeHT > awayHT || homeFT > awayFT) && bet === 'FTHT_HH') {
        return 'WIN';
    } else if ((homeHT > awayHT && homeFT == awayFT) && bet === 'FTHT_HD') {
        return 'WIN';
    } else if ((homeHT > awayHT && homeFT < awayFT) && bet === 'FTHT_HA') {
        return 'WIN';
    } else if ((homeHT == awayHT && homeFT > awayFT) && bet === 'FTHT_DH') {
        return 'WIN';
    } else if ((homeHT == awayHT && homeFT == awayFT) && bet === 'FTHT_DD') {
        return 'WIN';
    } else if ((homeHT == awayHT && homeFT < awayFT) && bet === 'FTHT_DA') {
        return 'WIN';
    } else if ((homeHT < awayHT && homeFT > awayFT) && bet === 'FTHT_AH') {
        return 'WIN';
    } else if ((homeHT < awayHT && homeFT == awayFT) && bet === 'FTHT_AD') {
        return 'WIN';
    } else if ((homeHT < awayHT && homeFT < awayFT) && bet === 'FTHT_AA') {
        return 'WIN';
    }

    return "LOSE";
}

function calculateDoubleChance(home, away, bet) {

    // 1x ถ้า ทีมเยือน ชนะ คือ เสีย ตัง
    // x2 ทีมเจ้าบ้านชนะ คือ เสียตัง
    // 12 คือ เสมอเสียตัง

    if ((home > away || home == away) && bet === 'DC1X') {
        return 'WIN';
    } else if (home != away && bet === 'DC12') {
        return 'WIN';
    } else if ((away > home || home == away) && bet === 'DC2X') {
        return 'WIN';
    }

    return "LOSE";
}

function calculateTotalGoal(home, away, bet) {

    let totalScore = (home + away);
    let betResult = '';
    if (totalScore <= 1) {
        betResult = 'TG01';
    } else if (totalScore <= 3) {
        betResult = 'TG23';
    } else if (totalScore <= 6) {
        betResult = 'TG46';
    } else {
        betResult = 'TG7OVER';
    }

    if (bet == betResult) {
        return "WIN"
    }
    return "LOSE";
}

function calculateCorrectScore(home, away, bet) {

    let betResult = '';
    if (home == 0 && away == 0) {
        betResult = 'CS00';
    } else if (home == 0 && away == 1) {
        betResult = 'CS01';
    } else if (home == 0 && away == 2) {
        betResult = 'CS02';
    } else if (home == 0 && away == 3) {
        betResult = 'CS03';
    } else if (home == 0 && away == 4) {
        betResult = 'CS04';
    } else if (home == 1 && away == 0) {
        betResult = 'CS10';
    } else if (home == 1 && away == 1) {
        betResult = 'CS11';
    } else if (home == 1 && away == 2) {
        betResult = 'CS12';
    } else if (home == 1 && away == 3) {
        betResult = 'CS13';
    } else if (home == 1 && away == 4) {
        betResult = 'CS14';
    } else if (home == 2 && away == 0) {
        betResult = 'CS20';
    } else if (home == 2 && away == 1) {
        betResult = 'CS21';
    } else if (home == 2 && away == 2) {
        betResult = 'CS22';
    } else if (home == 2 && away == 3) {
        betResult = 'CS23';
    } else if (home == 2 && away == 4) {
        betResult = 'CS24';
    } else if (home == 3 && away == 0) {
        betResult = 'CS30';
    } else if (home == 3 && away == 1) {
        betResult = 'CS31';
    } else if (home == 3 && away == 2) {
        betResult = 'CS32';
    } else if (home == 3 && away == 3) {
        betResult = 'CS33';
    } else if (home == 3 && away == 4) {
        betResult = 'CS34';
    } else if (home == 4 && away == 0) {
        betResult = 'CS40';
    } else if (home == 4 && away == 1) {
        betResult = 'CS41';
    } else if (home == 4 && away == 2) {
        betResult = 'CS42';
    } else if (home == 4 && away == 3) {
        betResult = 'CS43';
    } else if (home == 4 && away == 4) {
        betResult = 'CS44';
    } else {
        betResult = 'CSAOS';
    }
    if (bet == betResult) {
        return "WIN"
    }
    return "LOSE";
}

function prePareHandicap(handicap) {
    let xhan = handicap.split('/');

    let matchHandicap;

    if (xhan.length == 2) {
        let fh = Math.abs(Number.parseFloat(xhan[0]));
        let sh = Number.parseFloat(xhan[1]);
        matchHandicap = fh + ((sh - fh) / 2);
    } else {
        matchHandicap = Math.abs(Number.parseFloat(handicap));
    }
    return matchHandicap;
}

function calculateHandicap(homeScore, awayScore, handicap, bet) {

    let home = Number.parseFloat(homeScore);
    let away = Number.parseFloat(awayScore);

    let matchHandicap = prePareHandicap(handicap);

    let isTor = handicap.startsWith('-');

    let torScore;
    let longScore;

    if (bet === 'HOME') {
        if (isTor) {
            torScore = home;
            longScore = away;
        } else {
            torScore = away;
            longScore = home;
        }
    } else if (bet === 'AWAY') {
        if (isTor) {
            torScore = away;
            longScore = home;
        } else {
            torScore = home;
            longScore = away;
        }
    }


    let finalRate;
    console.log('isTor : ' + isTor);
    console.log('torScore : ' + torScore);
    console.log('longScore : ' + longScore);

    if (isTor) {
        console.log(torScore + ' - (' + longScore + ' + ' + matchHandicap + ')')
        finalRate = torScore - (longScore + matchHandicap);
    } else {
        console.log('(' + longScore + ' + ' + matchHandicap + ') - ' + torScore)
        finalRate = (longScore + matchHandicap) - torScore;
    }


    let result;
    console.log('finalRate : ', finalRate);
    if (finalRate == 0) {
        result = 'DRAW';
    } else if (finalRate == 0.25) {
        result = 'HALF_WIN';
    } else if (finalRate == -0.25) {
        result = 'HALF_LOSE';
    } else if (finalRate >= 0.5) {
        result = 'WIN';
    } else if (finalRate <= -0.5) {
        result = 'LOSE';
    }
    return result;
}


const moment = require('moment');


router.post('/cancel-ab', function (req, res) {

    let condition = {betId: req.body.betId, status: 'DONE'};

    BetTransactionModel.findOne(condition)
        .select('memberId memberUsername hdp commission amount memberCredit payout memberParentGroup betId status priceType source betResult')
        .populate({path: 'memberParentGroup', select: 'type'})
        .exec(function (err, betObj) {

            if (!betObj) {
                return res.send({message: "data not found", results: err, code: 999});
            }

            async.series([subCallback => {

                //refundBalance
                AgentService.refundAgentMemberBalance(betObj, '', (err, response) => {
                    if (err) {
                        subCallback(err, null);
                    } else {
                        subCallback(null, response);
                    }
                });
            }, callback => {

                let updateBody = {
                    $set: {
                        status: 'CANCELLED',
                        'commission.superAdmin.winLoseCom': 0,
                        'commission.superAdmin.winLose': 0,
                        'commission.superAdmin.totalWinLoseCom': 0,

                        'commission.company.winLoseCom': 0,
                        'commission.company.winLose': 0,
                        'commission.company.totalWinLoseCom': 0,

                        'commission.shareHolder.winLoseCom': 0,
                        'commission.shareHolder.winLose': 0,
                        'commission.shareHolder.totalWinLoseCom': 0,

                        'commission.senior.winLoseCom': 0,
                        'commission.senior.winLose': 0,
                        'commission.senior.totalWinLoseCom': 0,

                        'commission.masterAgent.winLoseCom': 0,
                        'commission.masterAgent.winLose': 0,
                        'commission.masterAgent.totalWinLoseCom': 0,

                        'commission.agent.winLoseCom': 0,
                        'commission.agent.winLose': 0,
                        'commission.agent.totalWinLoseCom': 0,

                        'commission.member.winLoseCom': 0,
                        'commission.member.winLose': 0,
                        'commission.member.totalWinLoseCom': 0,
                    }
                };
                BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, response);
                    }
                });
            }], function (err, response) {

                if (err)
                    return res.send({message: "error", results: err, code: 999});

                return res.send(
                    {
                        code: 0,
                        message: "success",
                        result: 'success'
                    }
                );
            });

        });

})


const rollScoreSchemaBetId = Joi.object().keys({
    betId: Joi.string().required(),
});


router.post('/roll-tran', function (req, res) {

    let validate = Joi.validate(req.body, rollScoreSchemaBetId);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    let condition = {
        gameDate: {$gte: new Date(moment().add(-10, 'd').format('YYYY-MM-DDT11:00:00.000'))},
        betId: req.body.betId,
        status: 'DONE'
    };

    BetTransactionModel.findOne(condition)
        .select('memberId memberUsername hdp commission amount memberCredit payout memberParentGroup betId status priceType source betResult gameType')
        .populate({path: 'memberParentGroup', select: 'type'})
        .exec(function (err, betObj) {

            if (!betObj) {
                return res.send({message: "data not found", results: err, code: 999});
            }


            //refundBalance
            AgentService.rollbackCalculateAgentMemberBalance(betObj, new Date(), (err, response) => {
                if (err) {
                    return res.send({message: "data not found", results: err, code: 999});
                } else {


                    let updateBody = {
                        $set: {
                            status: 'RUNNING',
                            'commission.superAdmin.winLoseCom': 0,
                            'commission.superAdmin.winLose': 0,
                            'commission.superAdmin.totalWinLoseCom': 0,

                            'commission.company.winLoseCom': 0,
                            'commission.company.winLose': 0,
                            'commission.company.totalWinLoseCom': 0,

                            'commission.shareHolder.winLoseCom': 0,
                            'commission.shareHolder.winLose': 0,
                            'commission.shareHolder.totalWinLoseCom': 0,

                            'commission.senior.winLoseCom': 0,
                            'commission.senior.winLose': 0,
                            'commission.senior.totalWinLoseCom': 0,

                            'commission.masterAgent.winLoseCom': 0,
                            'commission.masterAgent.winLose': 0,
                            'commission.masterAgent.totalWinLoseCom': 0,

                            'commission.agent.winLoseCom': 0,
                            'commission.agent.winLose': 0,
                            'commission.agent.totalWinLoseCom': 0,

                            'commission.member.winLoseCom': 0,
                            'commission.member.winLose': 0,
                            'commission.member.totalWinLoseCom': 0,
                        }
                    };
                    BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                        if (err) {
                            return res.send({message: "data not found", results: err, code: 999});
                        } else {
                            return res.send(
                                {
                                    code: 0,
                                    message: "success",
                                    result: 'success'
                                }
                            );
                        }
                    });

                }
            });
        });
});

const rollScoreSchemaMatchId = Joi.object().keys({
    matchId: Joi.string().required(),
});
router.post('/roll-tran-match', function (req, res) {

    let validate = Joi.validate(req.body, rollScoreSchemaMatchId);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    let condition = {
        gameDate: {$gte: new Date(moment().add(-10, 'd').format('YYYY-MM-DDT11:00:00.000'))},
        game: 'FOOTBALL',
        'hdp.matchId': req.body.matchId,
        status: 'DONE'
    };


    BetTransactionModel.findOne(condition)
        .select('memberId memberUsername hdp commission amount memberCredit payout memberParentGroup betId status priceType source betResult gameType')
        .populate({path: 'memberParentGroup', select: 'type'})
        .exec(function (err, betObj) {

            if (!betObj) {
                return res.send({message: "data not found", results: err, code: 999});
            }


            //refundBalance
            AgentService.rollbackCalculateAgentMemberBalance(betObj, new Date(), (err, response) => {
                if (err) {
                    return res.send({message: "data not found", results: err, code: 999});
                } else {


                    let updateBody = {
                        $set: {
                            status: 'RUNNING',
                            'commission.superAdmin.winLoseCom': 0,
                            'commission.superAdmin.winLose': 0,
                            'commission.superAdmin.totalWinLoseCom': 0,

                            'commission.company.winLoseCom': 0,
                            'commission.company.winLose': 0,
                            'commission.company.totalWinLoseCom': 0,

                            'commission.shareHolder.winLoseCom': 0,
                            'commission.shareHolder.winLose': 0,
                            'commission.shareHolder.totalWinLoseCom': 0,

                            'commission.senior.winLoseCom': 0,
                            'commission.senior.winLose': 0,
                            'commission.senior.totalWinLoseCom': 0,

                            'commission.masterAgent.winLoseCom': 0,
                            'commission.masterAgent.winLose': 0,
                            'commission.masterAgent.totalWinLoseCom': 0,

                            'commission.agent.winLoseCom': 0,
                            'commission.agent.winLose': 0,
                            'commission.agent.totalWinLoseCom': 0,

                            'commission.member.winLoseCom': 0,
                            'commission.member.winLose': 0,
                            'commission.member.totalWinLoseCom': 0,
                        }
                    };
                    BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                        if (err) {
                            return res.send({message: "data not found", results: err, code: 999});
                        } else {
                            return res.send(
                                {
                                    code: 0,
                                    message: "success",
                                    result: 'success'
                                }
                            );
                        }
                    });

                }
            });
        });
});

router.post('/rollback-tran', function (req, res) {

    let condition = {betId: req.body.betId, status: 'DONE'};

    BetTransactionModel.findOne(condition)
        .select('memberId memberUsername hdp commission amount memberCredit payout memberParentGroup betId status priceType source betResult gameType')
        .populate({path: 'memberParentGroup', select: 'type'})
        .exec(function (err, betObj) {

            if (!betObj) {
                return res.send({message: "data not found", results: err, code: 999});
            }

            AgentService.rollbackCalculateAgentMemberBalance(betObj, '', (err, response) => {
                if (err) {
                    return res.send({message: "data not found", results: err, code: 999});
                } else {
                    return res.send(
                        {
                            code: 0,
                            message: "success",
                            result: 'success'
                        }
                    );
                }
            });

        });
});


router.post('/repair-sexy', function (req, res) {

    // let condition = {memberId: mongoose.Types.ObjectId(req.body.memberId), status: 'DONE',betResult:'WIN','gameType': 'PARLAY'};
    //
    let condition = {betId: req.body.betId};

    // let condition = { "createdDate": { "$gte": new Date(moment('30/01/2018', "DD/MM/YYYY").format('YYYY-MM-DDT11:00:00.000'))}
    // ,status: 'DONE',betResult:'WIN','gameType': 'PARLAY'};

    BetTransactionModel.findOne(condition)
    // .select('memberId parlay commission amount baccarat')
        .populate({path: 'memberParentGroup', select: 'type'})
        .exec(function (err, betObj) {

            console.log(betObj)
            let betTxns = _.filter(betObj.baccarat.sexy.txns, item => {
                return item.userId === betObj.userId;
            });

            let validAmount = _.reduce(betTxns, function (total, x) {
                if (x.status !== 'CANCEL' && x.status !== 'VOID' && x.winLose != 0) {
                    return total + x.betAmount;
                }
                return total + 0;
            }, 0);

            let reductAmount = _.reduce(betTxns, function (total, x) {
                if (x.status !== 'CANCEL' && x.status !== 'VOID') {
                    return total + x.betAmount;
                }
                return total + 0;
            }, 0);

            let winLose = _.reduce(betTxns, function (total, x) {
                if (x.winLose && x.status !== 'CANCEL' && x.status !== 'VOID') {
                    return total + x.winLose;
                }
                return total + 0;
            }, 0);


            let betResult = winLose > 0 ? WIN : winLose < 0 ? LOSE : DRAW;

            let shareReceive = prepareShareReceive(winLose, betResult, betObj);

            let updateBody = {
                $set: {
                    'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                    'commission.company.winLose': shareReceive.company.winLose,
                    'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                    'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                    'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                    'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                    'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                    'commission.senior.winLose': shareReceive.senior.winLose,
                    'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                    'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                    'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                    'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                    'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                    'commission.agent.winLose': shareReceive.agent.winLose,
                    'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                    'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                    'commission.member.winLose': shareReceive.member.winLose,
                    'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                    'validAmount': validAmount
                }
            };


            async.series([callback => {

                BetTransactionModel.update({
                    _id: betObj._id
                }, updateBody, function (err, response) {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, response);
                    }
                });
            }], function (err, response) {

                if (err) {
                    return res.send({message: "data not found", results: err, code: 999});
                } else {
                    return res.send(
                        {
                            code: 0,
                            message: "success",
                            result: 'success'
                        }
                    );
                }
            });

        });
});

router.post('/rollback-ticket', function (req, res) {

    rollbackTicketFootball(req.body, (err, response) => {
        if (err){
            return res.send({message: "error", results: err, code: 999});
        }else {
            return res.send({
                code: 0,
                message: "success",
                result: 'success'
            });
        }
    });

});

function genKey(matchId, ref) {
    return `${matchId}:${ref}`
}

function rollbackTicketFootball(requestBody, rollbackCallback) {
    let matchId = `${requestBody.matchId}`;
    // let result = requestBody.result;
    let reff = genKey(requestBody.matchId);

    async.parallel([(callback => {

        let condition = {
            'hdp.matchId': matchId,
            'gameType': 'TODAY',
            active: true,
            'status' : 'DONE',
            'game' : 'FOOTBALL',
            // createdDate:{$gte: new Date(moment().add(-1, 'd').format('YYYY-MM-DDT11:00:00.000'))}
        };

        BetTransactionModel.find(condition)
            .select('memberId memberUsername hdp commission amount memberCredit payout memberParentGroup betId status priceType betResult game gameType source')
            .populate({path: 'memberParentGroup', select: 'type'})
            .exec(function (err, response) {


                if (!response) {
                    callback(null, 'success');
                    return;
                }

                if (response.length === 0) {
                    callback(null, '');
                    return;
                }

                // console.log('res=== ',response);

                async.each(response, (betObj, asyncCallback) => {

                    let betResult = 'DRAW';

                    // if (req.body.result === betObj.hdp.bet) {
                    //     betResult = 'WIN'
                    // } else if (req.body.result === 'DRAW') {
                    //     betResult = 'DRAW';
                    // } else {
                    //     betResult = 'LOSE'
                    // }

                    let amount = Math.abs(betObj.memberCredit);
                    let validAmount;
                    let winLose = 0;
                    if (betResult === 'DRAW') {
                        winLose = 0;
                        validAmount = 0;
                    } else if (betResult === 'WIN') {
                        FormulaUtils.calculatePayout(betObj.amount, betObj.hdp.odd, betObj.hdp.oddType, betObj.priceType, (payout) => {
                            winLose = Number.parseFloat(payout) - betObj.amount;
                        });
                        validAmount = betObj.amount;
                    } else {
                        winLose = 0 - amount;
                        validAmount = betObj.amount;
                    }

                    const shareReceive = prepareShareReceive(winLose, betResult, betObj);

                    async.series([subCallback => {
                        //refundBalance
                        AgentService.rollbackCalculateAgentMemberBalance(betObj, reff, (err, response) => {
                            if (err) {
                                subCallback(err, null);
                            } else {
                                subCallback(null, response);
                            }
                        });

                    }, subCallback => {

                        let updateBody = {
                            $set: {
                                'hdp.matchId': `${matchId}00000`,
                                'commission.superAdmin.winLoseCom': shareReceive.superAdmin.winLoseCom,
                                'commission.superAdmin.winLose': shareReceive.superAdmin.winLose,
                                'commission.superAdmin.totalWinLoseCom': shareReceive.superAdmin.totalWinLoseCom,

                                'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                                'commission.company.winLose': shareReceive.company.winLose,
                                'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                                'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                                'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                                'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                                'commission.api.winLoseCom': shareReceive.api.winLoseCom,
                                'commission.api.winLose': shareReceive.api.winLose,
                                'commission.api.totalWinLoseCom': shareReceive.api.totalWinLoseCom,

                                'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                                'commission.senior.winLose': shareReceive.senior.winLose,
                                'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                                'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                                'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                                'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                                'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                                'commission.agent.winLose': shareReceive.agent.winLose,
                                'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                                'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                                'commission.member.winLose': shareReceive.member.winLose,
                                'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                                'validAmount': validAmount,
                                'betResult': '',
                                'isEndScore': false,
                                'status': 'RUNNING'
                            }
                        };

                        BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                            if (err) {
                                subCallback(err, null);
                            } else {
                                subCallback(null, response);
                            }
                        });

                    }], function (err, response) {
                        if (err) {
                            asyncCallback(err, null);
                        } else {
                            asyncCallback(null, response);
                        }
                    });

                }, (err) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, 'success');
                    }
                });//end forEach Match
            });


    }), (callback => {



        let condition = {
            'game': {$in: ['FOOTBALL', 'M2']},
            'gameType': {$ne: 'TODAY'},
            active: true,
            status : 'DONE'
        };
        condition['parlay.matches.matchId'] = matchId;

        let cursor = BetTransactionModel.find(condition)
            .batchSize(200)
            .select('memberId memberUsername parlay commission amount memberCredit payout memberParentGroup status betId betResult game gameType source')
            .populate({path: 'memberParentGroup', select: 'type endpoint'})
            .populate({path: 'memberId', select: 'username_lower'})
            .lean()
            .cursor();


        cursor.on('data', function (betObj) {

            const allMatch = betObj.parlay.matches;

            let filterList = _.filter(allMatch, function (response) {
                return response.matchId === matchId;
            });

            if (filterList.length === 0) {
                // betListCallback(null, 'not data found');
                return;
            }


            let unCheckMatch = _.filter(allMatch, function (response) {
                return response.matchId !== matchId && !response.betResult;
            });

            console.log('unCheckMatch : ', unCheckMatch.length);

            const match = filterList[0];

            let betResult = 'DRAW';

            let updateBody;
            let isCalculateReceive = false;
            let isRefundBalance = false;
            let winLoseAmount = 0;
            let shareReceive;

            let isUnCheckMatchLose = _.filter(allMatch, function (response) {
                return response.matchId !== matchId && response.betResult === 'LOSE';
            }).length > 0;

            if (isUnCheckMatchLose) {
                //มีคู่อื่นที่แพ้
                console.log('-----------have match lose--------------')
                isRefundBalance = true;
                updateBody = {
                    $set: {
                        'parlay.matches.$.fullScore': '',
                        'parlay.matches.$.betResult': '',
                        'validAmount': betObj.amount,
                        'betResult': '',
                        'isEndScore': false,
                        'status': 'DONE',
                        'settleDate': DateUtils.getCurrentDate()
                    }
                };
            } else {
                // NOT EQUALS LOSE

                if (unCheckMatch.length == 0 || betObj.betResult === 'LOSE') {
                    console.log('-----------all match is end--------------')
                    //คู่อื่นจบหมดแล้ว
                    console.log('no unCheck match');
                    isRefundBalance = true;
                    isCalculateReceive = true;

                    let totalUnCheckOdds = _.filter(allMatch, function (response) {
                        return response.matchId !== matchId && response.betResult;
                    }).map((item) => {
                        return calculateParlayOdds(item.betResult, item.odd);
                    });

                    let sumTotalUnCheckOdds = _.reduce(totalUnCheckOdds, (memo, item) => {
                        return {sum: memo.sum * item};
                    }, {sum: 1}).sum;


                    let halfLoseCount = _.filter(allMatch, function (response) {
                        return response.betResult === 'HALF_LOSE' && response.matchId !== matchId;
                    }).length;

                    //prepare with last match result
                    sumTotalUnCheckOdds = roundTo.down(sumTotalUnCheckOdds * calculateParlayOdds(betResult, match.odd), 3);

                    if (betResult === 'HALF_LOSE') {
                        halfLoseCount += 1;
                    }

                    console.log('sumTotalUnCheckOdds : ', sumTotalUnCheckOdds);
                    console.log('halfLoseCount : ', halfLoseCount);

                    winLoseAmount = (subPlyAmount(betObj.amount, halfLoseCount) * sumTotalUnCheckOdds);
                    let validAmount = subPlyAmount(betObj.amount, halfLoseCount);

                    winLoseAmount = winLoseAmount - betObj.amount;

                    shareReceive = prepareShareReceive(winLoseAmount, 'DRAW', betObj);
                    updateBody = {
                        $set: {
                            'parlay.matches.$.fullScore': '',
                            'parlay.matches.$.betResult': '',
                            'parlay.matches.$.matchId': `${matchId}00000`,
                            'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                            'commission.company.winLose': shareReceive.company.winLose,
                            'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                            'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                            'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                            'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                            'commission.api.winLoseCom': shareReceive.api.winLoseCom,
                            'commission.api.winLose': shareReceive.api.winLose,
                            'commission.api.totalWinLoseCom': shareReceive.api.totalWinLoseCom,

                            'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                            'commission.senior.winLose': shareReceive.senior.winLose,
                            'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                            'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                            'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                            'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                            'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                            'commission.agent.winLose': shareReceive.agent.winLose,
                            'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                            'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                            'commission.member.winLose': shareReceive.member.winLose,
                            'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                            'validAmount': validAmount,
                            'betResult': '',
                            'isEndScore': false,
                            'status': 'RUNNING',
                            'settleDate': ''
                        }
                    };
                    updateBody.$set.status = 'RUNNING';

                    // console.log('update body == ',updateBody);

                } else {
                    console.log('-----------มีคู่ที่ยังไม่จบ--------------')
                    updateBody = {
                        $set: {
                            'parlay.matches.$.fullScore': '',
                            'parlay.matches.$.betResult': '',
                            'parlay.matches.$.matchId': `${matchId}00000`,
                            'validAmount': betObj.amount,
                            'betResult': '',
                            'isEndScore': false,
                            'status': 'RUNNING',
                            'settleDate': ''

                            // 'betResult': betResult,
                        }
                    };
                    // console.log('have unCheck match : ', unCheckMatch);
                    // console.log(unCheckMatch);

                }
            }

            console.log('----isRefundBalance-----',isRefundBalance);
            async.series([callback => {
                //refundBalance
                if (isRefundBalance) {
                    AgentService.rollbackCalculateAgentMemberBalance(betObj, reff, (err, response) => {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, response);
                        }
                    });
                } else {
                    callback(null, '');
                }

            }, callback => {

                BetTransactionModel.findOneAndUpdate({
                    _id: betObj._id,
                    'parlay.matches._id': match._id
                }, updateBody, {new: true}, function (err, response) {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, '');
                    }
                });

            }], function (err, asyncResponse) {
                if (err) {

                } else {
                    console.log('success 1')
                }
            });



        });

        cursor.on('end', function () {
            console.log('Done!');
            callback(null, {});
        });

    })], (err, asyncResponse) => {

        if (err){
            rollbackCallback(err, null);
        }else {
            rollbackCallback(null, 'success');
        }




    });
}


module.exports = router;