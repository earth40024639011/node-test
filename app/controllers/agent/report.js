const MemberModel = require('../../models/member.model');
const UpdateBalanceModel = require('../../models/updateBalanceHistory.model');
const BetTransactionModel = require('../../models/betTransaction.model.js');
const AgentGroupModel = require('../../models/agentGroup.model.js');
const CreditModifyHistory = require('../../models/creditModifyHistory.model.js');

const UserModel = require('../../models/users.model.js');
const PaymentHistoryModel = require('../../models/paymentHistory.model.js');
const LottoService = require('../../common/lottoService');
const LottoLaosService = require('../../common/lottoLaosService');
const AgentService = require('../../common/agentService');
const MemberService = require('../../common/memberService');
const ReportService = require('../../common/reportService');
const AmbGameService = require('../../common/ambGameService');
const PrettyGameService = require('../../common/prettygameService');


const express = require('express');
const router = express.Router();
const async = require("async");
const Joi = require('joi');
const mongoose = require('mongoose');
const Hashing = require('../../common/hashing');
const config = require('config');
const _ = require('underscore');
const DateUtils = require('../../common/dateUtils');
const moment = require('moment');
const naturalSort = require("javascript-natural-sort");
const roundTo = require('round-to');
const NumberUtils = require('../../common/numberUtils1');

function getGroupAndChild(groupType, callback) {
    let group, child;
    if (groupType === 'SUPER_ADMIN') {
        group = 'superAdmin';
        child = 'company';
    } else if (groupType === 'COMPANY') {
        group = 'company';
        child = 'shareHolder';
    } else if (groupType === 'SHARE_HOLDER') {
        group = 'shareHolder';
        child = 'senior';
    } else if (groupType === 'SENIOR') {
        group = 'senior';
        child = 'masterAgent';
    } else if (groupType === 'MASTER_AGENT') {
        group = 'masterAgent';
        child = 'agent';
    } else if (groupType === 'AGENT') {
        group = 'agent';
    }
    callback(group, child);
}

function prepareGroup(groupType) {
    if (groupType === 'SUPER_ADMIN') {
        return 'superAdmin';
    } else if (groupType === 'COMPANY') {
        return 'company';
    } else if (groupType === 'SHARE_HOLDER') {
        return 'shareHolder';
    } else if (groupType === 'SENIOR') {
        return 'senior';
    } else if (groupType === 'MASTER_AGENT') {
        return 'masterAgent';
    } else if (groupType === 'AGENT') {
        return 'agent';
    } else if (groupType === 'API') {
        return 'api';
    }
    return '';
}

router.get('/pending-status', function (req, res) {


    let userInfo = req.userInfo;

    let group = prepareGroup(userInfo.group.type);

    let condition = {};

    let dateList = {};

    if (moment().hours() >= 11) {
        condition['gameDate'] = {
            "$gte": moment().millisecond(0).second(0).minute(0).hour(11).add(-2, 'd').utc(true).toDate(),
            "$lt": moment().millisecond(0).second(0).minute(0).hour(11).add(1, 'd').utc(true).toDate()
        };
        dateList = DateUtils.enumerateBackDaysBetweenDates(moment().add(-2, 'days'), moment().add(0, 'd'), 'DD/MM/YYYY');
    } else {
        condition['gameDate'] = {
            "$gte": moment().millisecond(0).second(0).minute(0).hour(11).add(-3, 'd').utc(true).toDate(),
            "$lt": moment().millisecond(0).second(0).minute(0).hour(11).utc(true).toDate()
        };
        dateList = DateUtils.enumerateBackDaysBetweenDates(moment().add(-3, 'days'), moment().add(-1, 'd'), 'DD/MM/YYYY');
    }


    condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(userInfo.group._id);
    condition['game'] = {$in: ['FOOTBALL', 'M2']};
    condition['status'] = 'RUNNING';

    console.log(condition);

    BetTransactionModel.aggregate([
        {
            $match: condition
        },
        {
            $group: {
                _id: {
                    date: {
                        $cond: [
                            // {$and:[
                            {$gte: [{$hour: "$gameDate"}, 11]},
                            {$dateToString: {format: "%d/%m/%Y", date: "$gameDate"}},
                            {
                                $dateToString: {
                                    format: "%d/%m/%Y",
                                    date: {$subtract: ["$gameDate", 24 * 60 * 60 * 1000]}
                                }
                            }
                        ]
                    }
                },
                count: {$sum: 1},
                running: {$sum: {$cond: [{$eq: ['$status', 'RUNNING']}, 1, 0]}},
            }
        },
        {
            $project: {
                _id: 0,
                date: '$_id.date',
                count: '$count',
                running: '$running'
            }
        }
    ], (err, statementResponse) => {

        if (err) {
            return res.send({message: "error", result: err, code: 999});
        }

        dateList = _.map(dateList, (date) => {
            let mObj = _.filter(statementResponse, (pendingItem) => {
                return date === pendingItem.date;
            })[0];

            if (mObj) {
                return mObj;
            } else {
                return {
                    date: date,
                    count: 0,
                    running: 0
                }
            }
        });

        return res.send(
            {
                code: 0,
                message: "success",
                result: {
                    date1: dateList[2],
                    date2: dateList[1],
                    date3: dateList[0]
                }
            }
        );

    });

});

router.get('/findBetId', function (req, res) {

    let condition = {};
    condition['betId'] = req.query.betId;
    condition['status'] = 'DONE';
    condition['game'] = 'FOOTBALL';

    console.log(condition);

    BetTransactionModel.find(condition)
        .populate({path: 'memberId', select: '_id username'})
        .select('priceType hdp parlay commission amount memberCredit payout betId createdDate memberId gameType status game source baccarat slot lotto gameDate ipAddress')
        .exec(function (err, results) {

            if (err) {
                return res.send({message: "error", result: err, code: 999});
            }

            return res.send({
                code: 0,
                message: "success",
                result: {
                    list: results,
                }
            });
        });

});

const cancelSuccessTransactionSchema = Joi.object().keys({
    betId: Joi.string().required(),
    password: Joi.string().allow(''),
    remark: Joi.string().allow('')
});

router.post('/cancel-success-ticket', function (req, res) {

    let validate = Joi.validate(req.body, cancelSuccessTransactionSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    let userInfo = req.userInfo;
    let requestBody = req.body;

    if (userInfo.group.type != 'COMPANY') {
        return res.send({message: "access denied", code: 999});
    }

    async.series([callback => {

        UserModel.findById(userInfo._id, (err, agentResponse) => {

            if (agentResponse && (Hashing.encrypt256_1(requestBody.password) === agentResponse.password)) {
                callback(null, agentResponse)
            } else {
                callback(1001, null);
            }
        });

    }], function (err, asyncResponse) {

        if (err) {
            if (err === 1001) {
                return res.send({
                    code: 1001,
                    message: "Invalid Password"
                });
            }
            return res.send({message: err, result: err, code: 999});
        }

        let condition = {betId: req.body.betId, status: 'DONE', 'game': 'FOOTBALL'};

        BetTransactionModel.findOne(condition)
            .select('memberId memberUsername hdp commission amount memberCredit payout memberParentGroup betId status priceType source betResult game')
            .populate({path: 'memberParentGroup', select: 'type'})
            .exec(function (err, betObj) {

                if (!betObj) {
                    return res.send({message: "data not found", results: err, code: 999});
                }

                if (betObj.status === 'CANCELLED' || betObj.status === 'REJECTED') {
                    return res.send({message: "cancel already", results: err, code: 999});
                }


                async.series([subCallback => {

                    //refundBalance
                    AgentService.refundAgentMemberBalance(betObj, 'REFUND|'+betObj.betId, (err, response) => {
                        if (err) {
                            subCallback(err, null);
                        } else {
                            subCallback(null, response);
                        }
                    });

                }, callback => {

                    let updateBody = {
                        $set: {
                            'cancelDate': DateUtils.getCurrentDate(),
                            'cancelByType': 'AGENT',
                            'cancelByAgent': userInfo._id,
                            status: 'CANCELLED',
                            'commission.superAdmin.winLoseCom': 0,
                            'commission.superAdmin.winLose': 0,
                            'commission.superAdmin.totalWinLoseCom': 0,

                            'commission.company.winLoseCom': 0,
                            'commission.company.winLose': 0,
                            'commission.company.totalWinLoseCom': 0,

                            'commission.shareHolder.winLoseCom': 0,
                            'commission.shareHolder.winLose': 0,
                            'commission.shareHolder.totalWinLoseCom': 0,

                            'commission.senior.winLoseCom': 0,
                            'commission.senior.winLose': 0,
                            'commission.senior.totalWinLoseCom': 0,

                            'commission.masterAgent.winLoseCom': 0,
                            'commission.masterAgent.winLose': 0,
                            'commission.masterAgent.totalWinLoseCom': 0,

                            'commission.agent.winLoseCom': 0,
                            'commission.agent.winLose': 0,
                            'commission.agent.totalWinLoseCom': 0,

                            'commission.member.winLoseCom': 0,
                            'commission.member.winLose': 0,
                            'commission.member.totalWinLoseCom': 0,
                        }
                    };
                    BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, response);
                        }
                    });
                }], function (err, response) {

                    if (err) {
                        return res.send({message: "error", results: err, code: 999});
                    }

                    return res.send(
                        {
                            code: 0,
                            message: "success",
                            result: 'success'
                        }
                    );
                });

            });
    });

});

router.post('/cancel-success-ticket2', function (req, res) {


    let condition = {'hdp.matchId': '54094:2687408', status: 'DONE', 'game': 'FOOTBALL'};

    BetTransactionModel.find(condition)
        .select('memberId hdp commission amount memberCredit payout memberParentGroup betId status priceType source betResult game')
        .populate({path: 'memberParentGroup', select: 'type'})
        .exec(function (err, betList) {

            async.each(betList, (betObj, betListCallback) => {


                async.series([subCallback => {

                    //refundBalance
                    AgentService.refundAgentMemberBalance(betObj, '', (err, response) => {
                        if (err) {
                            subCallback(err, null);
                        } else {
                            subCallback(null, response);
                        }
                    });
                }, callback => {

                    let updateBody = {
                        $set: {
                            'cancelDate': DateUtils.getCurrentDate(),
                            'cancelByType': 'AGENT',
                            status: 'CANCELLED',
                            'commission.superAdmin.winLoseCom': 0,
                            'commission.superAdmin.winLose': 0,
                            'commission.superAdmin.totalWinLoseCom': 0,

                            'commission.company.winLoseCom': 0,
                            'commission.company.winLose': 0,
                            'commission.company.totalWinLoseCom': 0,

                            'commission.shareHolder.winLoseCom': 0,
                            'commission.shareHolder.winLose': 0,
                            'commission.shareHolder.totalWinLoseCom': 0,

                            'commission.senior.winLoseCom': 0,
                            'commission.senior.winLose': 0,
                            'commission.senior.totalWinLoseCom': 0,

                            'commission.masterAgent.winLoseCom': 0,
                            'commission.masterAgent.winLose': 0,
                            'commission.masterAgent.totalWinLoseCom': 0,

                            'commission.agent.winLoseCom': 0,
                            'commission.agent.winLose': 0,
                            'commission.agent.totalWinLoseCom': 0,

                            'commission.member.winLoseCom': 0,
                            'commission.member.winLose': 0,
                            'commission.member.totalWinLoseCom': 0,
                        }
                    };
                    BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, response);
                        }
                    });
                }], function (err, response) {

                    betListCallback(null, response);

                });

            }, (err, result) => {
                if (err) {
                    return res.send({code: 0, message: "fail"});
                }
                return res.send({code: 1, message: "success"});
            });

        });
});

router.get('/betResult/:betId', (req, res) => {
    const {
        betId
    } = req.params;

    BetTransactionModel.findOne({
        betId: betId
    })
        .lean()
        .populate({path: 'memberId', select: 'username_lower'})
        .exec((error, data) => {
            if (error) {
                return res.send({message: "error", result: error, code: 999});
            } else if (!data) {
                return res.send({message: "DATA NOT FOUND", result: data, code: 0});
            } else {
                return res.send({message: "success", result: data, code: 0});
            }
        });
});

router.get('/lotto-winning', function (req, res) {


    let groupId = req.query.groupId;
    let gameId = req.query.gameId;
    let userInfo = req.userInfo;


    async.waterfall([(callback => {
        AgentGroupModel.findById(groupId, (err, response) => {
            if (err) {
                callback(err, null);
            } else {
                if (!response) {
                    callback(888, null);
                } else {
                    callback(null, response);
                }
            }
        }).select('type parentId');

    }), (groupInfo, callback) => {

        LottoService.getGameByGameId(gameId, (err, response) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, groupInfo, response);
            }
        });

    }, ((groupInfo, gameInfo, callback) => {


        let gameDate = gameInfo.day + '/' + gameInfo.month + '/' + (gameInfo.year - 543);
        let condition = {};


        condition['gameDate'] = {
            "$gte": moment(gameDate, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).utc(true).toDate()
        }


        condition['memberParentGroup'] = mongoose.Types.ObjectId(groupId);
        condition['game'] = 'LOTTO';

        if (gameId) {
            condition['lotto.amb.gameId'] = gameId;
        }

        condition['betResult'] = 'WIN';
        condition['status'] = 'DONE';

        let group = prepareGroup(groupInfo.type);


        BetTransactionModel.aggregate([
            {
                $match: condition
            },
            {
                "$group": {
                    "_id": {
                        commission: '$memberParentGroup',
                        member: '$memberId',
                    },
                    "currency": {$first: "$currency"},
                    "stackCount": {$sum: 1},
                    "amount": {$sum: '$amount'},
                    "validAmount": {$sum: '$validAmount'},
                    "grossComm": {$sum: {$multiply: ['$validAmount', {$divide: ['$commission.' + group + '.commission', 100]}]}},
                    "memberWinLose": {$sum: '$commission.member.winLose'},
                    "memberWinLoseCom": {$sum: '$commission.member.winLoseCom'},
                    "memberTotalWinLoseCom": {$sum: '$commission.member.totalWinLoseCom'},

                    "agentWinLose": {$sum: '$commission.agent.winLose'},
                    "agentWinLoseCom": {$sum: '$commission.agent.winLoseCom'},
                    "agentTotalWinLoseCom": {$sum: '$commission.agent.totalWinLoseCom'},

                    "masterAgentWinLose": {$sum: '$commission.masterAgent.winLose'},
                    "masterAgentWinLoseCom": {$sum: '$commission.masterAgent.winLoseCom'},
                    "masterAgentTotalWinLoseCom": {$sum: '$commission.masterAgent.totalWinLoseCom'},

                    "seniorWinLose": {$sum: '$commission.senior.winLose'},
                    "seniorWinLoseCom": {$sum: '$commission.senior.winLoseCom'},
                    "seniorTotalWinLoseCom": {$sum: '$commission.senior.totalWinLoseCom'},

                    "shareHolderWinLose": {$sum: '$commission.shareHolder.winLose'},
                    "shareHolderWinLoseCom": {$sum: '$commission.shareHolder.winLoseCom'},
                    "shareHolderTotalWinLoseCom": {$sum: '$commission.shareHolder.totalWinLoseCom'},

                    "companyWinLose": {$sum: '$commission.company.winLose'},
                    "companyWinLoseCom": {$sum: '$commission.company.winLoseCom'},
                    "companyTotalWinLoseCom": {$sum: '$commission.company.totalWinLoseCom'},

                    "superAdminWinLose": {$sum: '$commission.superAdmin.winLose'},
                    "superAdminWinLoseCom": {$sum: '$commission.superAdmin.winLoseCom'},
                    "superAdminTotalWinLoseCom": {$sum: '$commission.superAdmin.totalWinLoseCom'}
                }
            },
            {
                $project: {
                    _id: 0,
                    type: 'M_GROUP',
                    currency: "$currency",
                    member: '$_id.member',
                    group: '$_id.commission',
                    stackCount: '$stackCount',
                    amount: '$amount',
                    validAmount: '$validAmount',
                    grossComm: '$grossComm',
                    memberWinLose: '$memberWinLose',
                    memberWinLoseCom: '$memberWinLoseCom',
                    memberTotalWinLoseCom: '$memberTotalWinLoseCom',
                    agentWinLose: '$agentWinLose',
                    agentWinLoseCom: '$agentWinLoseCom',
                    agentTotalWinLoseCom: '$agentTotalWinLoseCom',
                    masterAgentWinLose: '$masterAgentWinLose',
                    masterAgentWinLoseCom: '$masterAgentWinLoseCom',
                    masterAgentTotalWinLoseCom: '$masterAgentTotalWinLoseCom',
                    seniorWinLose: '$seniorWinLose',
                    seniorWinLoseCom: '$seniorWinLoseCom',
                    seniorTotalWinLoseCom: '$seniorTotalWinLoseCom',
                    shareHolderWinLose: '$shareHolderWinLose',
                    shareHolderWinLoseCom: '$shareHolderWinLoseCom',
                    shareHolderTotalWinLoseCom: '$shareHolderTotalWinLoseCom',
                    companyWinLose: '$companyWinLose',
                    companyWinLoseCom: '$companyWinLoseCom',
                    companyTotalWinLoseCom: '$companyTotalWinLoseCom',
                    superAdminWinLose: '$superAdminWinLose',
                    superAdminWinLoseCom: '$superAdminWinLoseCom',
                    superAdminTotalWinLoseCom: '$superAdminTotalWinLoseCom',

                }
            }
        ], (err, results) => {
            if (err) {
                callback(err, null);
            } else {
                MemberModel.populate(results, {
                    path: 'member',
                    select: '_id username username_lower contact'
                }, function (err, memberResult) {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, groupInfo, gameInfo, memberResult);
                    }
                });
            }
        });

    })], function (err, groupInfo, gameInfo, winLoseResponse) {

        if (err) {
            if (err == 888) {
                return res.send({message: "agentId not found", code: 888});
            }
            return res.send({message: "error", result: err, code: 999});
        }
        let agentGroup = groupInfo;

        let group = '';
        let childGroup;

        if (agentGroup.type === 'SUPER_ADMIN') {
            group = 'superAdmin';
            childGroup = 'company';
        } else if (agentGroup.type === 'COMPANY') {
            group = 'company';
            childGroup = 'shareHolder';
        } else if (agentGroup.type === 'SHARE_HOLDER') {
            group = 'shareHolder';
            childGroup = 'senior';
        } else if (agentGroup.type === 'SENIOR') {
            group = "senior";
            childGroup = 'masterAgent,agent';
        } else if (agentGroup.type === 'MASTER_AGENT') {
            group = "masterAgent";
            childGroup = 'agent';
        } else if (agentGroup.type === 'AGENT') {
            group = "agent";
            childGroup = '';
        } else if (agentGroup.type === 'API') {
            group = "api";
            childGroup = '';
        }


        let transaction = [];

        async.map(childGroup.split(','), (child, callback) => {


            let gameDate = gameInfo.day + '/' + gameInfo.month + '/' + (gameInfo.year - 543);
            let condition = {};


            condition['gameDate'] = {
                "$gte": moment(gameDate, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).utc(true).toDate()
            }


            condition['memberParentGroup'] = {$ne: mongoose.Types.ObjectId(groupId)};

            let orCondition = [
                {['commission.' + child + '.parentGroup']: mongoose.Types.ObjectId(groupId)},
                {['commission.' + group + '.group']: mongoose.Types.ObjectId(groupId)},
            ];


            condition['game'] = 'LOTTO';

            if (gameId) {
                condition['lotto.amb.gameId'] = gameId;
            }

            condition['betResult'] = 'WIN';
            condition['status'] = 'DONE';
            condition['$and'] = orCondition;

            console.log(condition)

            BetTransactionModel.aggregate([
                {
                    $match: condition
                },
                {
                    "$group": {
                        "_id": {
                            commission: '$commission.' + child + '.group'
                        },
                        "currency": {$first: "$currency"},
                        "stackCount": {$sum: 1},
                        "amount": {$sum: '$amount'},
                        "validAmount": {$sum: '$validAmount'},
                        "grossComm": {$sum: {$multiply: ['$validAmount', {$divide: ['$commission.' + group + '.commission', 100]}]}},
                        "memberWinLose": {$sum: '$commission.member.winLose'},
                        "memberWinLoseCom": {$sum: '$commission.member.winLoseCom'},
                        "memberTotalWinLoseCom": {$sum: '$commission.member.totalWinLoseCom'},

                        "agentWinLose": {$sum: '$commission.agent.winLose'},
                        "agentWinLoseCom": {$sum: '$commission.agent.winLoseCom'},
                        "agentTotalWinLoseCom": {$sum: '$commission.agent.totalWinLoseCom'},

                        "masterAgentWinLose": {$sum: '$commission.masterAgent.winLose'},
                        "masterAgentWinLoseCom": {$sum: '$commission.masterAgent.winLoseCom'},
                        "masterAgentTotalWinLoseCom": {$sum: '$commission.masterAgent.totalWinLoseCom'},

                        "seniorWinLose": {$sum: '$commission.senior.winLose'},
                        "seniorWinLoseCom": {$sum: '$commission.senior.winLoseCom'},
                        "seniorTotalWinLoseCom": {$sum: '$commission.senior.totalWinLoseCom'},

                        "shareHolderWinLose": {$sum: '$commission.shareHolder.winLose'},
                        "shareHolderWinLoseCom": {$sum: '$commission.shareHolder.winLoseCom'},
                        "shareHolderTotalWinLoseCom": {$sum: '$commission.shareHolder.totalWinLoseCom'},

                        "companyWinLose": {$sum: '$commission.company.winLose'},
                        "companyWinLoseCom": {$sum: '$commission.company.winLoseCom'},
                        "companyTotalWinLoseCom": {$sum: '$commission.company.totalWinLoseCom'},

                        "superAdminWinLose": {$sum: '$commission.superAdmin.winLose'},
                        "superAdminWinLoseCom": {$sum: '$commission.superAdmin.winLoseCom'},
                        "superAdminTotalWinLoseCom": {$sum: '$commission.superAdmin.totalWinLoseCom'}
                    }
                },
                {
                    $project: {
                        _id: 0,
                        type: 'A_GROUP',
                        group: '$_id.commission',
                        currency: "$currency",
                        stackCount: '$stackCount',
                        grossComm: '$grossComm',
                        amount: '$amount',
                        validAmount: '$validAmount',
                        memberWinLose: '$memberWinLose',
                        memberWinLoseCom: '$memberWinLoseCom',
                        memberTotalWinLoseCom: '$memberTotalWinLoseCom',
                        agentWinLose: '$agentWinLose',
                        agentWinLoseCom: '$agentWinLoseCom',
                        agentTotalWinLoseCom: '$agentTotalWinLoseCom',
                        masterAgentWinLose: '$masterAgentWinLose',
                        masterAgentWinLoseCom: '$masterAgentWinLoseCom',
                        masterAgentTotalWinLoseCom: '$masterAgentTotalWinLoseCom',
                        seniorWinLose: '$seniorWinLose',
                        seniorWinLoseCom: '$seniorWinLoseCom',
                        seniorTotalWinLoseCom: '$seniorTotalWinLoseCom',
                        shareHolderWinLose: '$shareHolderWinLose',
                        shareHolderWinLoseCom: '$shareHolderWinLoseCom',
                        shareHolderTotalWinLoseCom: '$shareHolderTotalWinLoseCom',
                        companyWinLose: '$companyWinLose',
                        companyWinLoseCom: '$companyWinLoseCom',
                        companyTotalWinLoseCom: '$companyTotalWinLoseCom',
                        superAdminWinLose: '$superAdminWinLose',
                        superAdminWinLoseCom: '$superAdminWinLoseCom',
                        superAdminTotalWinLoseCom: '$superAdminTotalWinLoseCom',

                    }
                }
            ], (err, results) => {
                _.each(results, item => {
                    transaction.push(item);
                });
                callback();
            });
        }, (err) => {

            if (err)
                return res.send({message: "error", results: err, code: 999});

            let mergeDataList = transaction.concat(winLoseResponse);

            async.series([(callback => {

                AgentGroupModel.populate(mergeDataList, {
                    path: 'group',
                    select: '_id name name_lower contact'
                }, function (err, memberResult) {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, memberResult);
                    }
                });


            })], (err, populateResults) => {

                if (err)
                    return res.send({message: "error", results: err, code: 999});


                let finalResult = transformDataByAgentType(agentGroup.type, mergeDataList);


                finalResult = finalResult.sort(function (a, b) {
                    let aKey = a.type === 'M_GROUP' ? a.member.username : a.group.name;
                    let bKey = b.type === 'M_GROUP' ? b.member.username : b.group.name;
                    return naturalSort(aKey, bKey);
                });

                if (req.query.username) {
                    finalResult = _.filter(finalResult, (item) => {
                        return (item.type === 'M_GROUP' ? item.member.username : item.group.name) === req.query.username;
                    })
                }

                let summary = _.reduce(finalResult, function (memo, obj) {
                    return {
                        amount: memo.amount + obj.amount,
                        validAmount: memo.validAmount + obj.validAmount,
                        stackCount: memo.stackCount + obj.stackCount,
                        grossComm: memo.grossComm + obj.grossComm,
                        memberWinLose: memo.memberWinLose + obj.memberWinLose,
                        memberWinLoseCom: memo.memberWinLoseCom + obj.memberWinLoseCom,
                        memberTotalWinLoseCom: memo.memberTotalWinLoseCom + obj.memberTotalWinLoseCom,

                        agentWinLose: memo.agentWinLose + obj.agentWinLose,
                        agentWinLoseCom: memo.agentWinLoseCom + obj.agentWinLoseCom,
                        agentTotalWinLoseCom: memo.agentTotalWinLoseCom + obj.agentTotalWinLoseCom,

                        companyWinLose: memo.companyWinLose + obj.companyWinLose,
                        companyWinLoseCom: memo.companyWinLoseCom + obj.companyWinLoseCom,
                        companyTotalWinLoseCom: memo.companyTotalWinLoseCom + obj.companyTotalWinLoseCom,

                    }
                }, {
                    amount: 0,
                    validAmount: 0,
                    stackCount: 0,
                    grossComm: 0,
                    memberWinLose: 0,
                    memberWinLoseCom: 0,
                    memberTotalWinLoseCom: 0,
                    agentWinLose: 0,
                    agentWinLoseCom: 0,
                    agentTotalWinLoseCom: 0,
                    companyWinLose: 0,
                    companyWinLoseCom: 0,
                    companyTotalWinLoseCom: 0,
                });


                return res.send(
                    {
                        code: 0,
                        message: "success",
                        result: {
                            group: agentGroup,
                            dataList: finalResult,
                            summary: summary,
                        }
                    }
                );

            });
        });
    });


});

router.get('/lotto-laos-winning', function (req, res) {


    let groupId = req.query.groupId;
    let gameId = req.query.gameId;
    let userInfo = req.userInfo;


    async.waterfall([(callback => {
        AgentGroupModel.findById(groupId, (err, response) => {
            if (err) {
                callback(err, null);
            } else {
                if (!response) {
                    callback(888, null);
                } else {
                    callback(null, response);
                }
            }
        }).select('type parentId');

    }), (groupInfo, callback) => {

        LottoLaosService.getGameByGameId(gameId, (err, response) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, groupInfo, response);
            }
        });

    }, ((groupInfo, gameInfo, callback) => {


        let gameDate = gameInfo.day + '/' + gameInfo.month + '/' + (gameInfo.year - 543);
        let condition = {};


        condition['gameDate'] = {
            "$gte": moment(gameDate, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).utc(true).toDate()
        };


        condition['memberParentGroup'] = mongoose.Types.ObjectId(groupId);
        condition['game'] = 'LOTTO';

        if (gameId) {
            condition['lotto.laos.gameId'] = gameId;
        }

        condition['betResult'] = 'WIN';
        condition['status'] = 'DONE';

        let group = prepareGroup(groupInfo.type);


        BetTransactionModel.aggregate([
            {
                $match: condition
            },
            {
                "$group": {
                    "_id": {
                        commission: '$memberParentGroup',
                        member: '$memberId',
                    },
                    "currency": {$first: "$currency"},
                    "stackCount": {$sum: 1},
                    "amount": {$sum: '$amount'},
                    "validAmount": {$sum: '$validAmount'},
                    "grossComm": {$sum: {$multiply: ['$validAmount', {$divide: ['$commission.' + group + '.commission', 100]}]}},
                    "memberWinLose": {$sum: '$commission.member.winLose'},
                    "memberWinLoseCom": {$sum: '$commission.member.winLoseCom'},
                    "memberTotalWinLoseCom": {$sum: '$commission.member.totalWinLoseCom'},

                    "agentWinLose": {$sum: '$commission.agent.winLose'},
                    "agentWinLoseCom": {$sum: '$commission.agent.winLoseCom'},
                    "agentTotalWinLoseCom": {$sum: '$commission.agent.totalWinLoseCom'},

                    "masterAgentWinLose": {$sum: '$commission.masterAgent.winLose'},
                    "masterAgentWinLoseCom": {$sum: '$commission.masterAgent.winLoseCom'},
                    "masterAgentTotalWinLoseCom": {$sum: '$commission.masterAgent.totalWinLoseCom'},

                    "seniorWinLose": {$sum: '$commission.senior.winLose'},
                    "seniorWinLoseCom": {$sum: '$commission.senior.winLoseCom'},
                    "seniorTotalWinLoseCom": {$sum: '$commission.senior.totalWinLoseCom'},

                    "shareHolderWinLose": {$sum: '$commission.shareHolder.winLose'},
                    "shareHolderWinLoseCom": {$sum: '$commission.shareHolder.winLoseCom'},
                    "shareHolderTotalWinLoseCom": {$sum: '$commission.shareHolder.totalWinLoseCom'},

                    "companyWinLose": {$sum: '$commission.company.winLose'},
                    "companyWinLoseCom": {$sum: '$commission.company.winLoseCom'},
                    "companyTotalWinLoseCom": {$sum: '$commission.company.totalWinLoseCom'},

                    "superAdminWinLose": {$sum: '$commission.superAdmin.winLose'},
                    "superAdminWinLoseCom": {$sum: '$commission.superAdmin.winLoseCom'},
                    "superAdminTotalWinLoseCom": {$sum: '$commission.superAdmin.totalWinLoseCom'}
                }
            },
            {
                $project: {
                    _id: 0,
                    type: 'M_GROUP',
                    currency: "$currency",
                    member: '$_id.member',
                    group: '$_id.commission',
                    stackCount: '$stackCount',
                    amount: '$amount',
                    validAmount: '$validAmount',
                    grossComm: '$grossComm',
                    memberWinLose: '$memberWinLose',
                    memberWinLoseCom: '$memberWinLoseCom',
                    memberTotalWinLoseCom: '$memberTotalWinLoseCom',
                    agentWinLose: '$agentWinLose',
                    agentWinLoseCom: '$agentWinLoseCom',
                    agentTotalWinLoseCom: '$agentTotalWinLoseCom',
                    masterAgentWinLose: '$masterAgentWinLose',
                    masterAgentWinLoseCom: '$masterAgentWinLoseCom',
                    masterAgentTotalWinLoseCom: '$masterAgentTotalWinLoseCom',
                    seniorWinLose: '$seniorWinLose',
                    seniorWinLoseCom: '$seniorWinLoseCom',
                    seniorTotalWinLoseCom: '$seniorTotalWinLoseCom',
                    shareHolderWinLose: '$shareHolderWinLose',
                    shareHolderWinLoseCom: '$shareHolderWinLoseCom',
                    shareHolderTotalWinLoseCom: '$shareHolderTotalWinLoseCom',
                    companyWinLose: '$companyWinLose',
                    companyWinLoseCom: '$companyWinLoseCom',
                    companyTotalWinLoseCom: '$companyTotalWinLoseCom',
                    superAdminWinLose: '$superAdminWinLose',
                    superAdminWinLoseCom: '$superAdminWinLoseCom',
                    superAdminTotalWinLoseCom: '$superAdminTotalWinLoseCom',

                }
            }
        ], (err, results) => {
            if (err) {
                callback(err, null);
            } else {
                MemberModel.populate(results, {
                    path: 'member',
                    select: '_id username username_lower contact'
                }, function (err, memberResult) {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, groupInfo, gameInfo, memberResult);
                    }
                });
            }
        });

    })], function (err, groupInfo, gameInfo, winLoseResponse) {

        if (err) {
            if (err == 888) {
                return res.send({message: "agentId not found", code: 888});
            }
            return res.send({message: "error", result: err, code: 999});
        }
        let agentGroup = groupInfo;

        let group = '';
        let childGroup;

        if (agentGroup.type === 'SUPER_ADMIN') {
            group = 'superAdmin';
            childGroup = 'company';
        } else if (agentGroup.type === 'COMPANY') {
            group = 'company';
            childGroup = 'shareHolder';
        } else if (agentGroup.type === 'SHARE_HOLDER') {
            group = 'shareHolder';
            childGroup = 'senior';
        } else if (agentGroup.type === 'SENIOR') {
            group = "senior";
            childGroup = 'masterAgent,agent';
        } else if (agentGroup.type === 'MASTER_AGENT') {
            group = "masterAgent";
            childGroup = 'agent';
        } else if (agentGroup.type === 'AGENT') {
            group = "agent";
            childGroup = '';
        } else if (agentGroup.type === 'API') {
            group = "api";
            childGroup = '';
        }


        let transaction = [];

        async.map(childGroup.split(','), (child, callback) => {


            let gameDate = gameInfo.day + '/' + gameInfo.month + '/' + (gameInfo.year - 543);
            let condition = {};


            condition['gameDate'] = {
                "$gte": moment(gameDate, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).utc(true).toDate()
            }


            condition['memberParentGroup'] = {$ne: mongoose.Types.ObjectId(groupId)};

            let orCondition = [
                {['commission.' + child + '.parentGroup']: mongoose.Types.ObjectId(groupId)},
                {['commission.' + group + '.group']: mongoose.Types.ObjectId(groupId)},
            ];


            condition['game'] = 'LOTTO';

            if (gameId) {
                condition['lotto.laos.gameId'] = gameId;
            }

            condition['betResult'] = 'WIN';
            condition['status'] = 'DONE';
            condition['$and'] = orCondition;

            console.log(condition)

            BetTransactionModel.aggregate([
                {
                    $match: condition
                },
                {
                    "$group": {
                        "_id": {
                            commission: '$commission.' + child + '.group'
                        },
                        "currency": {$first: "$currency"},
                        "stackCount": {$sum: 1},
                        "amount": {$sum: '$amount'},
                        "validAmount": {$sum: '$validAmount'},
                        "grossComm": {$sum: {$multiply: ['$validAmount', {$divide: ['$commission.' + group + '.commission', 100]}]}},
                        "memberWinLose": {$sum: '$commission.member.winLose'},
                        "memberWinLoseCom": {$sum: '$commission.member.winLoseCom'},
                        "memberTotalWinLoseCom": {$sum: '$commission.member.totalWinLoseCom'},

                        "agentWinLose": {$sum: '$commission.agent.winLose'},
                        "agentWinLoseCom": {$sum: '$commission.agent.winLoseCom'},
                        "agentTotalWinLoseCom": {$sum: '$commission.agent.totalWinLoseCom'},

                        "masterAgentWinLose": {$sum: '$commission.masterAgent.winLose'},
                        "masterAgentWinLoseCom": {$sum: '$commission.masterAgent.winLoseCom'},
                        "masterAgentTotalWinLoseCom": {$sum: '$commission.masterAgent.totalWinLoseCom'},

                        "seniorWinLose": {$sum: '$commission.senior.winLose'},
                        "seniorWinLoseCom": {$sum: '$commission.senior.winLoseCom'},
                        "seniorTotalWinLoseCom": {$sum: '$commission.senior.totalWinLoseCom'},

                        "shareHolderWinLose": {$sum: '$commission.shareHolder.winLose'},
                        "shareHolderWinLoseCom": {$sum: '$commission.shareHolder.winLoseCom'},
                        "shareHolderTotalWinLoseCom": {$sum: '$commission.shareHolder.totalWinLoseCom'},

                        "companyWinLose": {$sum: '$commission.company.winLose'},
                        "companyWinLoseCom": {$sum: '$commission.company.winLoseCom'},
                        "companyTotalWinLoseCom": {$sum: '$commission.company.totalWinLoseCom'},

                        "superAdminWinLose": {$sum: '$commission.superAdmin.winLose'},
                        "superAdminWinLoseCom": {$sum: '$commission.superAdmin.winLoseCom'},
                        "superAdminTotalWinLoseCom": {$sum: '$commission.superAdmin.totalWinLoseCom'}
                    }
                },
                {
                    $project: {
                        _id: 0,
                        type: 'A_GROUP',
                        group: '$_id.commission',
                        currency: "$currency",
                        stackCount: '$stackCount',
                        grossComm: '$grossComm',
                        amount: '$amount',
                        validAmount: '$validAmount',
                        memberWinLose: '$memberWinLose',
                        memberWinLoseCom: '$memberWinLoseCom',
                        memberTotalWinLoseCom: '$memberTotalWinLoseCom',
                        agentWinLose: '$agentWinLose',
                        agentWinLoseCom: '$agentWinLoseCom',
                        agentTotalWinLoseCom: '$agentTotalWinLoseCom',
                        masterAgentWinLose: '$masterAgentWinLose',
                        masterAgentWinLoseCom: '$masterAgentWinLoseCom',
                        masterAgentTotalWinLoseCom: '$masterAgentTotalWinLoseCom',
                        seniorWinLose: '$seniorWinLose',
                        seniorWinLoseCom: '$seniorWinLoseCom',
                        seniorTotalWinLoseCom: '$seniorTotalWinLoseCom',
                        shareHolderWinLose: '$shareHolderWinLose',
                        shareHolderWinLoseCom: '$shareHolderWinLoseCom',
                        shareHolderTotalWinLoseCom: '$shareHolderTotalWinLoseCom',
                        companyWinLose: '$companyWinLose',
                        companyWinLoseCom: '$companyWinLoseCom',
                        companyTotalWinLoseCom: '$companyTotalWinLoseCom',
                        superAdminWinLose: '$superAdminWinLose',
                        superAdminWinLoseCom: '$superAdminWinLoseCom',
                        superAdminTotalWinLoseCom: '$superAdminTotalWinLoseCom',

                    }
                }
            ], (err, results) => {
                _.each(results, item => {
                    transaction.push(item);
                });
                callback();
            });
        }, (err) => {

            if (err)
                return res.send({message: "error", results: err, code: 999});

            let mergeDataList = transaction.concat(winLoseResponse);

            async.series([(callback => {

                AgentGroupModel.populate(mergeDataList, {
                    path: 'group',
                    select: '_id name name_lower contact'
                }, function (err, memberResult) {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, memberResult);
                    }
                });


            })], (err, populateResults) => {

                if (err)
                    return res.send({message: "error", results: err, code: 999});


                let finalResult = transformDataByAgentType(agentGroup.type, mergeDataList);


                finalResult = finalResult.sort(function (a, b) {
                    let aKey = a.type === 'M_GROUP' ? a.member.username : a.group.name;
                    let bKey = b.type === 'M_GROUP' ? b.member.username : b.group.name;
                    return naturalSort(aKey, bKey);
                });

                if (req.query.username) {
                    finalResult = _.filter(finalResult, (item) => {
                        return (item.type === 'M_GROUP' ? item.member.username : item.group.name) === req.query.username;
                    })
                }

                let summary = _.reduce(finalResult, function (memo, obj) {
                    return {
                        amount: memo.amount + obj.amount,
                        validAmount: memo.validAmount + obj.validAmount,
                        stackCount: memo.stackCount + obj.stackCount,
                        grossComm: memo.grossComm + obj.grossComm,
                        memberWinLose: memo.memberWinLose + obj.memberWinLose,
                        memberWinLoseCom: memo.memberWinLoseCom + obj.memberWinLoseCom,
                        memberTotalWinLoseCom: memo.memberTotalWinLoseCom + obj.memberTotalWinLoseCom,

                        agentWinLose: memo.agentWinLose + obj.agentWinLose,
                        agentWinLoseCom: memo.agentWinLoseCom + obj.agentWinLoseCom,
                        agentTotalWinLoseCom: memo.agentTotalWinLoseCom + obj.agentTotalWinLoseCom,

                        companyWinLose: memo.companyWinLose + obj.companyWinLose,
                        companyWinLoseCom: memo.companyWinLoseCom + obj.companyWinLoseCom,
                        companyTotalWinLoseCom: memo.companyTotalWinLoseCom + obj.companyTotalWinLoseCom,

                    }
                }, {
                    amount: 0,
                    validAmount: 0,
                    stackCount: 0,
                    grossComm: 0,
                    memberWinLose: 0,
                    memberWinLoseCom: 0,
                    memberTotalWinLoseCom: 0,
                    agentWinLose: 0,
                    agentWinLoseCom: 0,
                    agentTotalWinLoseCom: 0,
                    companyWinLose: 0,
                    companyWinLoseCom: 0,
                    companyTotalWinLoseCom: 0,
                });


                return res.send(
                    {
                        code: 0,
                        message: "success",
                        result: {
                            group: agentGroup,
                            dataList: finalResult,
                            summary: summary,
                        }
                    }
                );

            });
        });
    });


});

router.get('/wl_account_detail_member1', function (req, res) {


    let page = req.query.page ? Number.parseInt(req.query.page) : 10;
    let limit = req.query.limit ? Number.parseInt(req.query.limit) : 1;

    let condition = {};

    let userInfo = req.userInfo;
    let memberId = req.query.memberId;
    let gameType = req.query.gameType;
    let games = req.query.game;
    let source = req.query.source;


    if (req.query.dateFrom && req.query.dateTo) {

        condition['gameDate'] = {
            "$gte": DateUtils.convertDateTime(req.query.dateFrom, req.query.timeFrom),
            "$lt": DateUtils.convertDateTime(req.query.dateTo, req.query.timeTo)
        }
    }


    condition['memberId'] = mongoose.Types.ObjectId(memberId);
    condition['status'] = {$in: ['DONE', 'REJECTED', 'CANCELLED']};

    if (gameType) {
        condition['gameType'] = gameType;
    }

    let orCondition = [];

    if (games) {
        let gamesCondition = {
            '$or': games.split(',').map(function (game) {
                if (game === 'STEP') {
                    return {'$and': [{'game': 'FOOTBALL'}, {gameType: 'MIX_STEP'}]}
                } else if (game === 'PARLAY') {
                    return {'$and': [{'game': 'FOOTBALL'}, {gameType: 'PARLAY'}]}
                } else if (game === 'FOOTBALL') {
                    return {'$and': [{'game': 'FOOTBALL'}, {gameType: 'TODAY'}]}
                } else if (game === 'LOTTO') {
                    return {'$and': [{'game': 'LOTTO'}, {'source': 'AMB_LOTTO'}]}
                } else if (game === 'LOTTO_LAOS') {
                    return {'$and': [{'game': 'LOTTO'}, {'source': 'AMB_LOTTO_LAOS'}]}
                } else if (game === 'LOTTO_PP') {
                    return {'$and': [{'game': 'LOTTO'}, {'source': 'AMB_LOTTO_PP'}]}
                } else {
                    return {'game': game};
                }
            })
        }

        orCondition.push(gamesCondition);
    }

    if (source) {
        if (source == 'AMB_LOTTO') {
            if (req.query.lottoGameId) {
                condition['lotto.amb.gameId'] = req.query.lottoGameId;
            }

            if (req.query.betResult) {
                condition['betResult'] = req.query.betResult;
            }
        } else if (source == 'AMB_LOTTO_LAOS') {
            if (req.query.lottoGameId) {
                condition['lotto.laos.gameId'] = req.query.lottoGameId;
            }

            if (req.query.betResult) {
                condition['betResult'] = req.query.betResult;
            }
        } else {
            condition['source'] = source;
        }
    }


    if (games.split(',').length != 10 && orCondition.length > 0) {
        condition['$and'] = orCondition;
    }
    console.log(condition)

    async.parallel([callback => {
        MemberModel.findById(memberId, (err, response) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, response);
            }
        }).select('group');

    }], (err, results) => {

        let memberInfo = results[0];

        BetTransactionModel.paginate(condition, {
            select: "priceType hdp parlay commission amount memberCredit payout betId createdDate memberId gameType status game source baccarat slot lotto multi gameDate ipAddress isOfflineCash codeOfflineTicket",
            sort: {createdDate: -1},
            lean: true,
            page: page,
            limit: limit,
            // read: {
            //     pref: 'secondaryPreferred'
            // },
            populate: {
                path: 'memberId',
                select: '_id username commissionSetting.sportsBook.typeHdpOuOe'
            }
        }, function (err, data) {


            if (err) {
                return res.send({
                    code: 999,
                    message: "fail",
                    result: ''
                });

            } else {


                let response = prepareSexyBaccaratDetail(data.docs);


                let finalResult = transformData(userInfo.group.type, response);


                function transformData(type, dataList) {
                    if (type === 'SHARE_HOLDER') {
                        return _.map(dataList, function (item) {
                            let newItem = item;

                            newItem.agentWinLose = item.commission.agent.amount;

                            newItem.commission.company.amount = item.commission.superAdmin.amount + item.commission.company.amount;
                            newItem.commission.company.shareReceive = item.commission.superAdmin.shareReceive + item.commission.company.shareReceive;

                            newItem.commission.company.commission = roundTo((item.commission.superAdmin.commission + item.commission.company.commission) / 2, 2);
                            newItem.commission.company.winLose = item.commission.superAdmin.winLose + item.commission.company.winLose;
                            newItem.commission.company.winLoseCom = item.commission.superAdmin.winLoseCom + item.commission.company.winLoseCom;
                            newItem.commission.company.totalWinLoseCom = item.commission.superAdmin.totalWinLoseCom + item.commission.company.totalWinLoseCom;
                            return newItem;
                        });
                    } else if (type === 'SENIOR') {
                        return _.map(dataList, function (item) {
                            let newItem = item;
                            newItem.commission.company.amount = item.commission.superAdmin.amount + item.commission.company.amount + item.commission.shareHolder.amount;
                            newItem.commission.company.shareReceive = item.commission.superAdmin.shareReceive + item.commission.company.shareReceive + item.commission.shareHolder.shareReceive;
                            newItem.commission.company.commission = roundTo((item.commission.superAdmin.commission + item.commission.company.commission + item.commission.shareHolder.commission) / 3, 2);
                            newItem.commission.company.winLose = item.commission.superAdmin.winLose + item.commission.company.winLose + item.commission.shareHolder.winLose;
                            newItem.commission.company.winLoseCom = item.commission.superAdmin.winLoseCom + item.commission.company.winLoseCom + item.commission.shareHolder.winLoseCom;
                            newItem.commission.company.totalWinLoseCom = item.commission.superAdmin.totalWinLoseCom + item.commission.company.totalWinLoseCom + item.commission.shareHolder.totalWinLoseCom;
                            return newItem;
                        });
                    } else if (type === 'MASTER_AGENT') {
                        return _.map(dataList, function (item) {
                            let newItem = item;

                            newItem.commission.company.amount = item.commission.superAdmin.amount + item.commission.company.amount + item.commission.shareHolder.amount
                                + item.commission.senior.amount;
                            newItem.commission.company.shareReceive = item.commission.superAdmin.shareReceive + item.commission.company.shareReceive + item.commission.shareHolder.shareReceive
                                + item.commission.senior.shareReceive;
                            newItem.commission.company.commission = roundTo((item.commission.superAdmin.commission + item.commission.company.commission + item.commission.shareHolder.commission
                                + item.commission.senior.commission) / 4, 2);
                            newItem.commission.company.winLose = item.commission.superAdmin.winLose + item.commission.company.winLose + item.commission.shareHolder.winLose + item.commission.senior.winLose;
                            newItem.commission.company.winLoseCom = item.commission.superAdmin.winLoseCom + item.commission.company.winLoseCom + item.commission.shareHolder.winLoseCom + item.commission.senior.winLoseCom;
                            newItem.commission.company.totalWinLoseCom = item.commission.superAdmin.totalWinLoseCom + item.commission.company.totalWinLoseCom + item.commission.shareHolder.totalWinLoseCom + item.commission.senior.totalWinLoseCom;
                            return newItem;
                        });
                    } else if (type === 'AGENT') {
                        return _.map(dataList, function (item) {
                            let newItem = item;
                            newItem.commission.company.amount = item.commission.superAdmin.amount + item.commission.company.amount + item.commission.shareHolder.amount
                                + item.commission.senior.amount + item.commission.masterAgent.amount;
                            newItem.commission.company.shareReceive = item.commission.superAdmin.shareReceive + item.commission.company.shareReceive + item.commission.shareHolder.shareReceive
                                + item.commission.senior.shareReceive + item.commission.masterAgent.shareReceive;
                            newItem.commission.company.commission = roundTo((item.commission.superAdmin.commission + item.commission.company.commission + item.commission.shareHolder.commission
                                + item.commission.senior.commission + item.commission.masterAgent.commission) / 5, 2);
                            newItem.commission.company.winLose = item.commission.superAdmin.winLose + item.commission.company.winLose + item.commission.shareHolder.winLose + item.commission.senior.winLose + item.commission.masterAgent.winLose;
                            newItem.commission.company.winLoseCom = item.commission.superAdmin.winLoseCom + item.commission.company.winLoseCom + item.commission.shareHolder.winLoseCom + item.commission.senior.winLoseCom + item.commission.masterAgent.winLoseCom;
                            newItem.commission.company.totalWinLoseCom = item.commission.superAdmin.totalWinLoseCom + item.commission.company.totalWinLoseCom + item.commission.shareHolder.totalWinLoseCom + item.commission.senior.totalWinLoseCom + item.commission.masterAgent.totalWinLoseCom;
                            return newItem;
                        });
                    } else {
                        return dataList;
                    }
                }

                let summary = _.reduce(response, function (memo, obj) {

                    return {
                        amount: memo.amount + obj.amount,
                        memberCredit: memo.memberCredit + _.defaults(obj.memberCredit, {memberCredit: 0}),
                        memberWinLose: obj.commission.member ? memo.memberWinLose + _.defaults(obj.commission.member, {winLose: 0}).winLose : 0,
                        memberWinLoseCom: obj.commission.member ? memo.memberWinLoseCom + _.defaults(obj.commission.member, {winLoseCom: 0}).winLoseCom : 0,
                        memberTotalWinLoseCom: obj.commission.member ? memo.memberTotalWinLoseCom + _.defaults(obj.commission.member, {totalWinLoseCom: 0}).totalWinLoseCom : 0,

                        agentWinLose: memo.agentWinLose + _.defaults(obj.commission.agent, {winLose: 0}).winLose,
                        agentWinLoseCom: memo.agentWinLoseCom + _.defaults(obj.commission.agent, {winLoseCom: 0}).winLoseCom,
                        agentTotalWinLoseCom: memo.agentTotalWinLoseCom + _.defaults(obj.commission.agent, {totalWinLoseCom: 0}).totalWinLoseCom,

                        masterAgentWinLose: memo.masterAgentWinLose + _.defaults(obj.commission.masterAgent, {winLose: 0}).winLose,
                        masterAgentWinLoseCom: memo.masterAgentWinLoseCom + _.defaults(obj.commission.masterAgent, {winLoseCom: 0}).winLoseCom,
                        masterAgentTotalWinLoseCom: memo.masterAgentTotalWinLoseCom + _.defaults(obj.commission.masterAgent, {totalWinLoseCom: 0}).totalWinLoseCom,

                        seniorWinLose: memo.seniorWinLose + _.defaults(obj.commission.senior, {winLose: 0}).winLose,
                        seniorWinLoseCom: memo.seniorWinLoseCom + _.defaults(obj.commission.senior, {winLoseCom: 0}).winLoseCom,
                        seniorTotalWinLoseCom: memo.seniorTotalWinLoseCom + _.defaults(obj.commission.senior, {totalWinLoseCom: 0}).totalWinLoseCom,

                        shareHolderWinLose: memo.shareHolderWinLose + _.defaults(obj.commission.shareHolder, {winLose: 0}).winLose,
                        shareHolderWinLoseCom: memo.shareHolderWinLoseCom + _.defaults(obj.commission.shareHolder, {winLoseCom: 0}).winLoseCom,
                        shareHolderTotalWinLoseCom: memo.shareHolderTotalWinLoseCom + _.defaults(obj.commission.shareHolder, {totalWinLoseCom: 0}).totalWinLoseCom,

                        companyWinLose: memo.companyWinLose + _.defaults(obj.commission.company, {winLose: 0}).winLose,
                        companyWinLoseCom: memo.companyWinLoseCom + _.defaults(obj.commission.company, {winLoseCom: 0}).winLoseCom,
                        companyTotalWinLoseCom: memo.companyTotalWinLoseCom + _.defaults(obj.commission.company, {totalWinLoseCom: 0}).totalWinLoseCom,

                        superAdminWinLose: memo.superAdminWinLose + _.defaults(obj.commission.superAdmin, {winLose: 0}).winLose,
                        superAdminWinLoseCom: memo.superAdminWinLoseCom + _.defaults(obj.commission.superAdmin, {winLoseCom: 0}).winLoseCom,
                        superAdminTotalWinLoseCom: memo.superAdminTotalWinLoseCom + _.defaults(obj.commission.superAdmin, {totalWinLoseCom: 0}).totalWinLoseCom,
                    }
                }, {
                    amount: 0,
                    memberCredit: 0,
                    memberWinLose: 0,
                    memberWinLoseCom: 0,
                    memberTotalWinLoseCom: 0,
                    agentWinLose: 0,
                    agentWinLoseCom: 0,
                    agentTotalWinLoseCom: 0,
                    masterAgentWinLose: 0,
                    masterAgentWinLoseCom: 0,
                    masterAgentTotalWinLoseCom: 0,
                    seniorWinLose: 0,
                    seniorWinLoseCom: 0,
                    seniorTotalWinLoseCom: 0,
                    shareHolderWinLose: 0,
                    shareHolderWinLoseCom: 0,
                    shareHolderTotalWinLoseCom: 0,
                    companyWinLose: 0,
                    companyWinLoseCom: 0,
                    companyTotalWinLoseCom: 0,
                    superAdminWinLose: 0,
                    superAdminWinLoseCom: 0,
                    superAdminTotalWinLoseCom: 0,
                });


                return res.send(
                    {
                        code: 0,
                        message: "success",
                        result: {
                            group: userInfo.group,
                            dataList: finalResult,
                            summary: summary,
                            total: data.totalDocs,
                            limit: data.limit,
                            page: data.page,
                            pages: data.totalPages,
                        }
                    }
                );
            }
        });


        // });

    });

});

function prepareSexyBaccaratDetail(dataList) {
    return _.map(dataList, (item) => {

        if (item.source === 'SEXY_BACCARAT') {
            let newItem = Object.assign({}, item)._doc ? Object.assign({}, item)._doc : Object.assign({}, item);

            let betList = _.filter(item.baccarat.sexy.txns, item => {
                return item.status !== 'CANCEL' && item.status !== 'VOID';
            });

            let gameType = item.baccarat.sexy.txns[0].gameType;

            // newItem.baccarat = item.baccarat;
            newItem.baccarat.gameType = gameType;
            if (gameType.toUpperCase() === "SICBO") {
                newItem.baccarat.betDetail = _.reduce(betList, function (total, obj) {
                        return {
                            AnyTriple: total.AnyTriple + (obj.category === 'AnyTriple' ? obj.betAmount : 0),
                            Odd: total.Odd + (obj.category === 'Odd' ? obj.betAmount : 0),
                            Even: total.Even + (obj.category === 'Even' ? obj.betAmount : 0),
                            Big: total.Big + (obj.category === 'Big' ? obj.betAmount : 0),
                            Small: total.Small + (obj.category === 'Small' ? obj.betAmount : 0),
                            Sum4: total.Sum4 + (obj.category === 'Sum 4' ? obj.betAmount : 0),
                            Sum5: total.Sum5 + (obj.category === 'Sum 4' ? obj.betAmount : 0),
                            Sum6: total.Sum6 + (obj.category === 'Sum 6' ? obj.betAmount : 0),
                            Sum7: total.Sum7 + (obj.category === 'Sum 7' ? obj.betAmount : 0),
                            Sum8: total.Sum8 + (obj.category === 'Sum 8' ? obj.betAmount : 0),
                            Sum9: total.Sum9 + (obj.category === 'Sum 9' ? obj.betAmount : 0),
                            Sum10: total.Sum10 + (obj.category === 'Sum 10' ? obj.betAmount : 0),
                            Sum11: total.Sum11 + (obj.category === 'Sum 11' ? obj.betAmount : 0),
                            Sum12: total.Sum12 + (obj.category === 'Sum 12' ? obj.betAmount : 0),
                            Sum13: total.Sum13 + (obj.category === 'Sum 13' ? obj.betAmount : 0),
                            Sum14: total.Sum14 + (obj.category === 'Sum 14' ? obj.betAmount : 0),
                            Sum15: total.Sum15 + (obj.category === 'Sum 15' ? obj.betAmount : 0),
                            Sum16: total.Sum16 + (obj.category === 'Sum 16' ? obj.betAmount : 0),
                            Sum17: total.Sum17 + (obj.category === 'Sum 17' ? obj.betAmount : 0),
                            Triple1: total.Triple1 + (obj.category === 'Triple 1' ? obj.betAmount : 0),
                            Triple2: total.Triple2 + (obj.category === 'Triple 2' ? obj.betAmount : 0),
                            Triple3: total.Triple3 + (obj.category === 'Triple 3' ? obj.betAmount : 0),
                            Triple4: total.Triple4 + (obj.category === 'Triple 4' ? obj.betAmount : 0),
                            Triple5: total.Triple5 + (obj.category === 'Triple 5' ? obj.betAmount : 0),
                            Triple6: total.Triple6 + (obj.category === 'Triple 6' ? obj.betAmount : 0),
                            Double1: total.Double1 + (obj.category === 'Double 1' ? obj.betAmount : 0),
                            Double2: total.Double2 + (obj.category === 'Double 2' ? obj.betAmount : 0),
                            Double3: total.Double3 + (obj.category === 'Double 3' ? obj.betAmount : 0),
                            Double4: total.Double4 + (obj.category === 'Double 4' ? obj.betAmount : 0),
                            Double5: total.Double5 + (obj.category === 'Double 5' ? obj.betAmount : 0),
                            Double6: total.Double6 + (obj.category === 'Double 6' ? obj.betAmount : 0),
                            Combine12: total.Combine12 + (obj.category === 'Combine 12 ' ? obj.betAmount : 0),
                            Combine13: total.Combine13 + (obj.category === 'Combine 13' ? obj.betAmount : 0),
                            Combine14: total.Combine14 + (obj.category === 'Combine 14' ? obj.betAmount : 0),
                            Combine15: total.Combine15 + (obj.category === 'Combine 15' ? obj.betAmount : 0),
                            Combine16: total.Combine16 + (obj.category === 'Combine 16' ? obj.betAmount : 0),
                            Combine23: total.Combine23 + (obj.category === 'Combine 23' ? obj.betAmount : 0),
                            Combine24: total.Combine24 + (obj.category === 'Combine 24' ? obj.betAmount : 0),
                            Combine25: total.Combine25 + (obj.category === 'Combine 25' ? obj.betAmount : 0),
                            Combine26: total.Combine26 + (obj.category === 'Combine 26' ? obj.betAmount : 0),
                            Combine34: total.Combine34 + (obj.category === 'Combine 34' ? obj.betAmount : 0),
                            Combine35: total.Combine35 + (obj.category === 'Combine 35' ? obj.betAmount : 0),
                            Combine36: total.Combine36 + (obj.category === 'Combine 36' ? obj.betAmount : 0),
                            Combine45: total.Combine45 + (obj.category === 'Combine 45' ? obj.betAmount : 0),
                            Combine46: total.Combine46 + (obj.category === 'Combine 46' ? obj.betAmount : 0),
                            Combine56: total.Combine56 + (obj.category === 'Combine 56' ? obj.betAmount : 0),
                            Single1: total.Single1 + (obj.category === 'Single 1' ? obj.betAmount : 0),
                            Single2: total.Single2 + (obj.category === 'Single 2' ? obj.betAmount : 0),
                            Single3: total.Single3 + (obj.category === 'Single 3' ? obj.betAmount : 0),
                            Single4: total.Single4 + (obj.category === 'Single 4' ? obj.betAmount : 0),
                            Single5: total.Single5 + (obj.category === 'Single 5' ? obj.betAmount : 0),
                            Single6: total.Single6 + (obj.category === 'Single 6' ? obj.betAmount : 0),
                        };
                    },
                    {
                        AnyTriple: 0,
                        Odd: 0,
                        Even: 0,
                        Big: 0,
                        Small: 0,
                        Sum4: 0,
                        Sum5: 0,
                        Sum6: 0,
                        Sum7: 0,
                        Sum8: 0,
                        Sum9: 0,
                        Sum10: 0,
                        Sum11: 0,
                        Sum12: 0,
                        Sum13: 0,
                        Sum14: 0,
                        Sum15: 0,
                        Sum16: 0,
                        Sum17: 0,
                        Triple1: 0,
                        Triple2: 0,
                        Triple3: 0,
                        Triple4: 0,
                        Triple5: 0,
                        Triple6: 0,
                        Double1: 0,
                        Double2: 0,
                        Double3: 0,
                        Double4: 0,
                        Double5: 0,
                        Double6: 0,
                        Combine12: 0,
                        Combine13: 0,
                        Combine14: 0,
                        Combine15: 0,
                        Combine16: 0,
                        Combine23: 0,
                        Combine24: 0,
                        Combine25: 0,
                        Combine26: 0,
                        Combine34: 0,
                        Combine35: 0,
                        Combine36: 0,
                        Combine45: 0,
                        Combine46: 0,
                        Combine56: 0,
                        Single1: 0,
                        Single2: 0,
                        Single3: 0,
                        Single4: 0,
                        Single5: 0,
                        Single6: 0
                    });

                return newItem;
            } else if (gameType.toUpperCase() === "DRAGONTIGER") {
                // console.log(betList,'123123123')
                newItem.baccarat.betDetail = _.reduce(betList, function (total, obj) {
                    return {
                        dragon: total.dragon + (obj.category === 'Dragon' ? obj.betAmount : 0),
                        tiger: total.tiger + (obj.category === 'Tiger' ? obj.betAmount : 0),
                        tie: total.tie + (obj.category === 'Tie' ? obj.betAmount : 0)
                    };
                }, {
                    dragon: 0,
                    tiger: 0,
                    tie: 0
                });
                return newItem;
            } else {
                newItem.baccarat.betDetail = _.reduce(betList, function (total, obj) {
                    return {
                        banker: total.banker + (obj.category === 'Banker' ? obj.betAmount : 0),
                        player: total.player + (obj.category === 'Player' ? obj.betAmount : 0),
                        tie: total.tie + (obj.category === 'Tie' ? obj.betAmount : 0),
                        bankerPair: total.bankerPair + (obj.category === 'BankerPair' ? obj.betAmount : 0),
                        playerPair: total.playerPair + (obj.category === 'PlayerPair' ? obj.betAmount : 0),
                        bankerBonus: total.bankerBonus + (obj.category === 'BankerBonus' ? obj.betAmount : 0),
                        playerBonus: total.playerBonus + (obj.category === 'PlayerBonus' ? obj.betAmount : 0),
                        big: total.big + (obj.category === 'Big' ? obj.betAmount : 0),
                        small: total.small + (obj.category === 'Small' ? obj.betAmount : 0),
                        bankerIn: total.bankerIn + (obj.category.startsWith('Banker Insurance') ? obj.betAmount : 0),
                        playerIn: total.playerIn + (obj.category.startsWith('Player Insurance') ? obj.betAmount : 0),
                    };
                }, {
                    banker: 0,
                    player: 0,
                    tie: 0,
                    bankerPair: 0,
                    playerPair: 0,
                    bankerBonus: 0,
                    playerBonus: 0,
                    big: 0,
                    small: 0,
                    bankerIn: 0,
                    playerIn: 0
                });
                return newItem;
            }
        }
        else if (item.source === 'AG_GAME') {
            let newItem = Object.assign({}, item)._doc ? Object.assign({}, item)._doc : Object.assign({}, item);

            newItem.baccarat.betDetail = _.chain(item.baccarat.ag.resultTransaction).groupBy(item => {
                return item.playType;
            }).map((value, key) => {
                return {
                    type: key,
                    amount: _.reduce(value, (total, x) => {
                        return total + x.validAmount;
                    }, 0)
                }
            }).map(item => {
                return item.type + ' ' + item.amount
            }).value();

            newItem.baccarat.betDetail = newItem.baccarat.betDetail.join(' , ')

            return newItem;
        }
        else if (item.source === 'PRETTY_GAME') {

            let newItem = Object.assign({}, item)._doc ? Object.assign({}, item)._doc : Object.assign({}, item);
            // newItem.baccarat.betDetail = _.chain(item.baccarat.pretty.txns).groupBy(item => {
            newItem.baccarat.betDetail = _.chain(item.baccarat.pretty.txns).groupBy(item => {
                // console.log(item.betPosition,'<><<<<<<<<<<<<M');
                return item.betPosition;
            }).map((value, key) => {
                return {
                    type: key,
                    amount: _.reduce(value, (total, x) => {
                        return total + x.betAmt;
                    }, 0)
                }
            }).map(item => {
                return item.type + ' ' + Math.abs(item.amount)
            }).value();

            newItem.baccarat.betDetail = newItem.baccarat.betDetail.join(' , ');
            // console.log(newItem.baccarat.pretty.result,item.baccarat.pretty.gameType,'<><<<<<<<<<<<<M');
            // console.log(PrettyGameService.getKeyBetResult(item.baccarat.pretty.gameType,newItem.baccarat.pretty.result),'<><<<<<<<<<<<<j');
            newItem.baccarat.betResult = newItem.baccarat.pretty.result;
            // console.log(newItem,'lkglgk')

            return newItem;

        }
        else if (item.source === 'AMB_GAME') {
            let newItem = Object.assign({}, item)._doc ? Object.assign({}, item)._doc : Object.assign({}, item);
            newItem.multi.amb.betDetail = _.chain(item.multi.amb.bet).map(item => {

                return {
                    type: AmbGameService.getBetKeyName(newItem.multi.amb.gameName, item.key),
                    amount: item.amount
                }

            }).map(item => {
                return item.type + ' ' + item.amount
            }).value();

            newItem.multi.amb.betDetail = newItem.multi.amb.betDetail.join(' , ')
            newItem.multi.amb.result = _.chain(item.multi.amb.results).map(item => {

                return {
                    type: AmbGameService.getResult(newItem.multi.amb.gameName, item),
                }

            }).map(item => {
                return item.type
            }).value();
            return newItem;

        }
        else {
            let newItem = Object.assign({}, item)._doc ? Object.assign({}, item)._doc : Object.assign({}, item);
            newItem.memberCommission = Math.abs(item.amount * NumberUtils.convertPercent(item.commission.member.commission));
            newItem.memberCreditUsage = Math.abs(item.memberCredit);
            newItem.memberCommPercent = item.commission.member.commission;
            return newItem;
        }

    });
}

router.get('/wl_member', function (req, res) {

    let groupId = req.query.groupId;
    let username = req.query.username;
    let userInfo = req.userInfo;

    if (!req.query.username) {

        return res.send(
            {
                code: 0,
                message: "success",
                result: {
                    code: 0,
                    message: "success",
                    result: {
                        group: {},
                        dataList: [],
                        summary: {},
                    }
                }
            }
        );
    }


    async.waterfall([(mainCallback => {

        AgentGroupModel.findById(userInfo.group._id, (err, response) => {
            if (err) {
                mainCallback(err, null);
            } else {
                if (!response) {
                    mainCallback(888, null);
                } else {
                    mainCallback(null, response);
                }
            }
        }).select('type parentId _id')

    }),(groupInfo, mainCallback) => {

        MemberModel.findOne({username_lower: username.toLowerCase()}).select('_id group').exec(function (err, memberResponse) {

            if (err) {
                mainCallback(err, null);
            } else {
                if (!memberResponse) {
                    mainCallback(null,groupInfo,false);
                } else {

                    AgentService.isAllowPermission(userInfo._id,memberResponse._id,(err,isAllow)=>{
                        if(isAllow){
                            console.log(memberResponse.group.toString() + " : "+groupInfo._id.toString())
                            if(memberResponse.group.toString() == groupInfo._id.toString()){
                                mainCallback(null,groupInfo,true);
                            }else {
                                mainCallback(null,groupInfo,false);
                            }
                        }else {
                            mainCallback(null,groupInfo,false);
                        }
                    });

                }
            }
        })

    }, (groupInfo,isAllow, mainCallback) => {

        const agentGroup = groupInfo;

        if(isAllow) {
            ReportService.getWinLoseReportMember2(groupId, agentGroup.type, req.query.dateFrom, req.query.dateTo, req.query.timeFrom, req.query.timeTo, req.query.game, username, (err, wlResponse) => {
                if (err) {
                    mainCallback(err, null);
                } else {

                    MemberModel.populate(wlResponse, {
                        path: 'member',
                        select: '_id username username_lower contact'
                    }, function (err, memberResult) {
                        if (err) {
                            mainCallback(err, null);
                        } else {
                            mainCallback(null, groupInfo, memberResult);
                        }
                    });
                }
            });
        }else {
            mainCallback(null, groupInfo, isAllow,[]);
        }


    }], (err, agentGroup, transaction) => {

        if (err) {
            if (err == 888) {
                return res.send({message: "agentId not found", code: 888});
            }
            return res.send({message: "error", result: err, code: 999});
        }

        let finalResult = transformDataByAgentType(agentGroup.type, transaction);


        finalResult = finalResult.sort(function (a, b) {
            let aKey = a.type === 'M_GROUP' ? a.member.username : a.group.name;
            let bKey = b.type === 'M_GROUP' ? b.member.username : b.group.name;
            return naturalSort(aKey, bKey);
        });

        let summary = _.reduce(finalResult, function (memo, obj) {

            return {
                amount: memo.amount + obj.amount,
                validAmount: memo.validAmount + obj.validAmount,
                stackCount: memo.stackCount + obj.stackCount,
                grossComm: memo.grossComm + obj.grossComm,
                memberWinLose: memo.memberWinLose + obj.memberWinLose,
                memberWinLoseCom: memo.memberWinLoseCom + obj.memberWinLoseCom,
                memberTotalWinLoseCom: memo.memberTotalWinLoseCom + obj.memberTotalWinLoseCom,

                agentWinLose: memo.agentWinLose + obj.agentWinLose,
                agentWinLoseCom: memo.agentWinLoseCom + obj.agentWinLoseCom,
                agentTotalWinLoseCom: memo.agentTotalWinLoseCom + obj.agentTotalWinLoseCom,

                companyWinLose: memo.companyWinLose + obj.companyWinLose,
                companyWinLoseCom: memo.companyWinLoseCom + obj.companyWinLoseCom,
                companyTotalWinLoseCom: roundTo(memo.companyTotalWinLoseCom + obj.companyTotalWinLoseCom, 3),

            }
        }, {
            amount: 0,
            validAmount: 0,
            stackCount: 0,
            grossComm: 0,
            memberWinLose: 0,
            memberWinLoseCom: 0,
            memberTotalWinLoseCom: 0,
            agentWinLose: 0,
            agentWinLoseCom: 0,
            agentTotalWinLoseCom: 0,
            companyWinLose: 0,
            companyWinLoseCom: 0,
            companyTotalWinLoseCom: 0,
        });


        return res.send(
            {
                code: 0,
                message: "success",
                result: {
                    group: agentGroup,
                    dataList: finalResult,
                    summary: summary,
                }
            }
        );

    });

});

router.get('/wl_account_2', function (req, res) {

    let groupId = req.query.groupId;


    async.waterfall([(mainCallback => {

        AgentGroupModel.findById(groupId, (err, response) => {
            if (err) {
                mainCallback(err, null);
            } else {
                if (!response) {
                    mainCallback(888, null);
                } else {
                    mainCallback(null, response);
                }
            }
        }).select('type parentId').populate({
            path: 'parentId',
            select: '_id type'
        });

    }), (groupInfo, mainCallback) => {

        const agentGroup = groupInfo;


        ReportService.isElasticEnabled(isElsEnabled => {

            console.log('isElsEnabled : ',isElsEnabled);
            if (isElsEnabled) {
                ReportService.queryMixUseWinLose(groupId, agentGroup.type, agentGroup.parentId ? agentGroup.parentId.type : null, req.query.dateFrom, req.query.dateTo, req.query.timeFrom, req.query.timeTo, req.query.game, (err, wlResponse) => {

                    AgentGroupModel.populate(wlResponse, {
                        path: 'group',
                        select: '_id name name_lower contact'
                    }, function (err, memberResult) {
                        if (err) {
                            // subQueryCallback(err, null);
                        } else {
                            MemberModel.populate(wlResponse, {
                                path: 'member',
                                select: '_id username username_lower contact'
                            }, function (err, memberResult) {
                                if (err) {
                                    // subQueryCallback(err, null);
                                } else {
                                    mainCallback(null, groupInfo, wlResponse);
                                }
                            });
                            //mainCallback(null, groupInfo,wlResponse);
                        }
                    });

                });

            } else {

                async.parallel([subQueryCallback => {

                    if (agentGroup.type === 'SENIOR' || agentGroup.type === 'MASTER_AGENT' || agentGroup.type === 'AGENT') {


                        ReportService.getWinLoseReportMember(groupId, agentGroup.type, req.query.dateFrom, req.query.dateTo, req.query.timeFrom, req.query.timeTo, req.query.game, (err, wlResponse) => {
                            if (err) {
                                subQueryCallback(err, null);
                            } else {

                                MemberModel.populate(wlResponse, {
                                    path: 'member',
                                    select: '_id username username_lower contact'
                                }, function (err, memberResult) {
                                    if (err) {
                                        subQueryCallback(err, null);
                                    } else {
                                        console.log('don2')
                                        subQueryCallback(null, memberResult);
                                    }
                                });
                            }
                        });
                    } else {
                        subQueryCallback(null, []);
                    }

                }, subQueryCallback => {

                    if (agentGroup.type !== 'AGENT') {
                        ReportService.getWinLoseReport(groupId, agentGroup.type, req.query.dateFrom, req.query.dateTo, req.query.timeFrom, req.query.timeTo, req.query.game, (err, wlResponse) => {

                            AgentGroupModel.populate(wlResponse, {
                                path: 'group',
                                select: '_id name name_lower contact'
                            }, function (err, memberResult) {
                                if (err) {
                                    subQueryCallback(err, null);
                                } else {
                                    console.log('xc')
                                    subQueryCallback(null, wlResponse);
                                }
                            });
                        });
                    } else {
                        subQueryCallback(null, []);
                    }
                }], (err, subQueryResult) => {
                    if (err) {
                        mainCallback(err, null);
                    } else {

                        if (subQueryResult[0] && subQueryResult[1]) {
                            mainCallback(null, agentGroup, subQueryResult[0].concat(subQueryResult[1]));
                        } else if (subQueryResult[0]) {
                            mainCallback(null, agentGroup, subQueryResult[0]);
                        } else if (subQueryResult[1]) {
                            mainCallback(null, agentGroup, subQueryResult[1]);
                        } else {
                            mainCallback(null, agentGroup, []);
                        }
                    }
                });
            }
        });

    }], (err, agentGroup, transaction) => {

        if (err) {
            if (err == 888) {
                return res.send({message: "agentId not found", code: 888});
            }
            return res.send({message: "error", result: err, code: 999});
        }

        let finalResult = transformDataByAgentType(agentGroup.type, transaction);


        finalResult = finalResult.sort(function (a, b) {
            let aKey = a.type === 'M_GROUP' ? a.member.username : a.group.name;
            let bKey = b.type === 'M_GROUP' ? b.member.username : b.group.name;
            return naturalSort(aKey, bKey);
        });

        //TODO refacfor
        if (req.query.username) {
            finalResult = _.filter(finalResult, (item) => {
                return (item.type === 'M_GROUP' ? item.member.username : item.group.name).toLowerCase() === req.query.username.toLowerCase();
            })
        }

        let summary = _.reduce(finalResult, function (memo, obj) {

            return {
                amount: memo.amount + obj.amount,
                validAmount: memo.validAmount + obj.validAmount,
                stackCount: memo.stackCount + obj.stackCount,
                grossComm: memo.grossComm + obj.grossComm,
                memberWinLose: memo.memberWinLose + obj.memberWinLose,
                memberWinLoseCom: memo.memberWinLoseCom + obj.memberWinLoseCom,
                memberTotalWinLoseCom: memo.memberTotalWinLoseCom + obj.memberTotalWinLoseCom,

                agentWinLose: memo.agentWinLose + obj.agentWinLose,
                agentWinLoseCom: memo.agentWinLoseCom + obj.agentWinLoseCom,
                agentTotalWinLoseCom: memo.agentTotalWinLoseCom + obj.agentTotalWinLoseCom,

                companyWinLose: memo.companyWinLose + obj.companyWinLose,
                companyWinLoseCom: memo.companyWinLoseCom + obj.companyWinLoseCom,
                companyTotalWinLoseCom: roundTo(memo.companyTotalWinLoseCom + obj.companyTotalWinLoseCom, 3),

            }
        }, {
            amount: 0,
            validAmount: 0,
            stackCount: 0,
            grossComm: 0,
            memberWinLose: 0,
            memberWinLoseCom: 0,
            memberTotalWinLoseCom: 0,
            agentWinLose: 0,
            agentWinLoseCom: 0,
            agentTotalWinLoseCom: 0,
            companyWinLose: 0,
            companyWinLoseCom: 0,
            companyTotalWinLoseCom: 0,
        });


        return res.send(
            {
                code: 0,
                message: "success",
                result: {
                    group: agentGroup,
                    dataList: finalResult,
                    summary: summary,
                }
            }
        );

    });

});

router.get('/wl_account_detail', function (req, res) {


    let groupId = req.query.groupId;
    let userInfo = req.userInfo;

    async.waterfall([(mainCallback => {
        AgentGroupModel.findById(groupId, (err, response) => {
            if (err) {
                mainCallback(err, null);
            } else {
                if (!response) {
                    mainCallback(888, null);
                } else {
                    mainCallback(null, response);
                }
            }
        }).select('type parentId').populate({
            path: 'parentId',
            select: '_id type'
        });

    }), ((groupInfo, mainCallback) => {


        const agentGroup = groupInfo;


        ReportService.isElasticEnabled(isElsEnabled => {

            console.log('isElsEnabled : ', isElsEnabled);
            if (isElsEnabled) {

                ReportService.queryMixUseWinLose(groupId, agentGroup.type, agentGroup.parentId ? agentGroup.parentId.type : null, req.query.dateFrom, req.query.dateTo, req.query.timeFrom, req.query.timeTo, req.query.game, (err, wlResponse) => {

                    AgentGroupModel.populate(wlResponse, {
                        path: 'group',
                        select: '_id name name_lower contact'
                    }, function (err, memberResult) {
                        if (err) {
                            // subQueryCallback(err, null);
                        } else {
                            MemberModel.populate(wlResponse, {
                                path: 'member',
                                select: '_id username username_lower contact'
                            }, function (err, memberResult) {
                                if (err) {
                                    // subQueryCallback(err, null);
                                } else {
                                    mainCallback(null, groupInfo, wlResponse);
                                }
                            });
                            //mainCallback(null, groupInfo,wlResponse);
                        }
                    });


                });

            } else {

                async.parallel([subQueryCallback => {

                    if (agentGroup.type === 'SENIOR' || agentGroup.type === 'MASTER_AGENT' || agentGroup.type === 'AGENT') {


                        ReportService.getWinLoseReportMember(groupId, agentGroup.type, req.query.dateFrom, req.query.dateTo, req.query.timeFrom, req.query.timeTo, req.query.game, (err, wlResponse) => {
                            if (err) {
                                subQueryCallback(err, null);
                            } else {

                                MemberModel.populate(wlResponse, {
                                    path: 'member',
                                    select: '_id username username_lower contact'
                                }, function (err, memberResult) {
                                    if (err) {
                                        subQueryCallback(err, null);
                                    } else {
                                        subQueryCallback(null, memberResult);
                                    }
                                });
                            }
                        });
                    } else {
                        subQueryCallback(null, []);
                    }

                }, subQueryCallback => {

                    if (agentGroup.type !== 'AGENT') {
                        ReportService.getWinLoseReport(groupId, agentGroup.type, req.query.dateFrom, req.query.dateTo, req.query.timeFrom, req.query.timeTo, req.query.game, (err, wlResponse) => {

                            AgentGroupModel.populate(wlResponse, {
                                path: 'group',
                                select: '_id name name_lower contact'
                            }, function (err, memberResult) {
                                if (err) {
                                    subQueryCallback(err, null);
                                } else {
                                    console.log('wl : ', wlResponse)
                                    subQueryCallback(null, wlResponse);
                                }
                            });
                        });
                    } else {
                        subQueryCallback(null, []);
                    }

                }], (err, subQueryResult) => {
                    if (err) {
                        mainCallback(err, null);
                    } else {

                        if (subQueryResult[0] && subQueryResult[1]) {
                            mainCallback(null, agentGroup, subQueryResult[0].concat(subQueryResult[1]));
                        } else if (subQueryResult[0]) {
                            mainCallback(null, agentGroup, subQueryResult[0]);
                        } else if (subQueryResult[1]) {
                            mainCallback(null, agentGroup, subQueryResult[1]);
                        } else {
                            mainCallback(null, agentGroup, []);
                        }
                    }
                });
            }
        });

    })], (err, agentGroup, transaction) => {

        if (err)
            return res.send({message: "error", results: err, code: 999});


        let finalResult = transformData(agentGroup.type, transaction);

        finalResult = finalResult.sort(function (a, b) {
            let aKey = a.type === 'M_GROUP' ? a.member.username : a.group.name;
            let bKey = b.type === 'M_GROUP' ? b.member.username : b.group.name;
            return naturalSort(aKey, bKey);
        });

        if (req.query.username) {
            finalResult = _.filter(finalResult, (item) => {
                return (item.type === 'M_GROUP' ? item.member.username : item.group.name).toLowerCase() === req.query.username.toLowerCase();
            })
        }

        function transformData(type, dataList) {
            if (type === 'SHARE_HOLDER') {
                return _.map(dataList, function (item) {
                    let newItem = item;
                    newItem.memberWinLose = item.memberWinLose;
                    newItem.memberWinLoseCom = item.memberWinLoseCom;
                    newItem.memberTotalWinLoseCom = item.memberTotalWinLoseCom;

                    newItem.agentWinLose = item.agentWinLose;
                    newItem.agentWinLoseCom = item.agentWinLoseCom;
                    newItem.agentTotalWinLoseCom = item.agentTotalWinLoseCom;

                    newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose;
                    newItem.companyWinLoseCom = item.superAdminWinLoseCom + item.companyWinLoseCom;
                    newItem.companyTotalWinLoseCom = item.superAdminTotalWinLoseCom + item.companyTotalWinLoseCom;
                    return newItem;
                });
            } else if (type === 'SENIOR') {
                return _.map(dataList, function (item) {
                    let newItem = item;
                    newItem.memberWinLose = item.memberWinLose;
                    newItem.memberWinLoseCom = item.memberWinLoseCom;
                    newItem.memberTotalWinLoseCom = item.memberTotalWinLoseCom;

                    newItem.agentWinLose = item.agentWinLose;
                    newItem.agentWinLoseCom = item.agentWinLoseCom;
                    newItem.agentTotalWinLoseCom = item.agentTotalWinLoseCom;

                    newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose + item.shareHolderWinLose;
                    newItem.companyWinLoseCom = item.superAdminWinLoseCom + item.companyWinLoseCom + item.shareHolderWinLoseCom;
                    newItem.companyTotalWinLoseCom = item.superAdminTotalWinLoseCom + item.companyTotalWinLoseCom + item.shareHolderTotalWinLoseCom;
                    return newItem;
                });
            } else if (type === 'MASTER_AGENT') {
                return _.map(dataList, function (item) {
                    let newItem = item;
                    newItem.memberWinLose = item.memberWinLose;
                    newItem.memberWinLoseCom = item.memberWinLoseCom;
                    newItem.memberTotalWinLoseCom = item.memberTotalWinLoseCom;

                    newItem.agentWinLose = item.agentWinLose;
                    newItem.agentWinLoseCom = item.agentWinLoseCom;
                    newItem.agentTotalWinLoseCom = item.agentTotalWinLoseCom;

                    newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose + item.shareHolderWinLose + item.seniorWinLose;
                    newItem.companyWinLoseCom = item.superAdminWinLoseCom + item.companyWinLoseCom + item.shareHolderWinLoseCom + item.seniorWinLoseCom;
                    newItem.companyTotalWinLoseCom = item.superAdminTotalWinLoseCom + item.companyTotalWinLoseCom + item.shareHolderTotalWinLoseCom + item.seniorTotalWinLoseCom;
                    return newItem;
                });
            } else if (type === 'AGENT') {
                return _.map(dataList, function (item) {
                    let newItem = item;
                    newItem.memberWinLose = item.memberWinLose;
                    newItem.memberWinLoseCom = item.memberWinLoseCom;
                    newItem.memberTotalWinLoseCom = item.memberTotalWinLoseCom;

                    newItem.agentWinLose = item.agentWinLose;
                    newItem.agentWinLoseCom = item.agentWinLoseCom;
                    newItem.agentTotalWinLoseCom = item.agentTotalWinLoseCom;

                    newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose + item.shareHolderWinLose + item.seniorWinLose + item.masterAgentWinLose;
                    newItem.companyWinLoseCom = item.superAdminWinLoseCom + item.companyWinLoseCom + item.shareHolderWinLoseCom + item.seniorWinLoseCom + item.masterAgentWinLoseCom;
                    newItem.companyTotalWinLoseCom = item.superAdminTotalWinLoseCom + item.companyTotalWinLoseCom + item.shareHolderTotalWinLoseCom + item.seniorTotalWinLoseCom + item.masterAgentTotalWinLoseCom;
                    return newItem;
                });
            } else {
                return dataList;
            }
        }


        let summary = _.reduce(finalResult, function (memo, obj) {
            return {
                amount: memo.amount + obj.amount,
                validAmount: memo.validAmount + obj.validAmount,
                stackCount: memo.stackCount + obj.stackCount,
                grossComm: memo.grossComm + obj.grossComm,
                memberWinLose: memo.memberWinLose + obj.memberWinLose,
                memberWinLoseCom: memo.memberWinLoseCom + obj.memberWinLoseCom,
                memberTotalWinLoseCom: memo.memberTotalWinLoseCom + obj.memberTotalWinLoseCom,

                agentWinLose: memo.agentWinLose + obj.agentWinLose,
                agentWinLoseCom: memo.agentWinLoseCom + obj.agentWinLoseCom,
                agentTotalWinLoseCom: memo.agentTotalWinLoseCom + obj.agentTotalWinLoseCom,

                masterAgentWinLose: memo.masterAgentWinLose + obj.masterAgentWinLose,
                masterAgentWinLoseCom: memo.masterAgentWinLoseCom + obj.masterAgentWinLoseCom,
                masterAgentTotalWinLoseCom: memo.masterAgentTotalWinLoseCom + obj.masterAgentTotalWinLoseCom,

                seniorWinLose: memo.seniorWinLose + obj.seniorWinLose,
                seniorWinLoseCom: memo.seniorWinLoseCom + obj.seniorWinLoseCom,
                seniorTotalWinLoseCom: memo.seniorTotalWinLoseCom + obj.seniorTotalWinLoseCom,

                shareHolderWinLose: memo.shareHolderWinLose + obj.shareHolderWinLose,
                shareHolderWinLoseCom: memo.shareHolderWinLoseCom + obj.shareHolderWinLoseCom,
                shareHolderTotalWinLoseCom: memo.shareHolderTotalWinLoseCom + obj.shareHolderTotalWinLoseCom,

                companyWinLose: memo.companyWinLose + obj.companyWinLose,
                companyWinLoseCom: memo.companyWinLoseCom + obj.companyWinLoseCom,
                companyTotalWinLoseCom: memo.companyTotalWinLoseCom + obj.companyTotalWinLoseCom,

                superAdminWinLose: memo.superAdminWinLose + obj.superAdminWinLose,
                superAdminWinLoseCom: memo.superAdminWinLoseCom + obj.superAdminWinLoseCom,
                superAdminTotalWinLoseCom: memo.superAdminTotalWinLoseCom + obj.superAdminTotalWinLoseCom
            }
        }, {
            amount: 0,
            validAmount: 0,
            stackCount: 0,
            grossComm: 0,
            memberWinLose: 0,
            memberWinLoseCom: 0,
            memberTotalWinLoseCom: 0,
            agentWinLose: 0,
            agentWinLoseCom: 0,
            agentTotalWinLoseCom: 0,
            masterAgentWinLose: 0,
            masterAgentWinLoseCom: 0,
            masterAgentTotalWinLoseCom: 0,
            seniorWinLose: 0,
            seniorWinLoseCom: 0,
            seniorTotalWinLoseCom: 0,
            shareHolderWinLose: 0,
            shareHolderWinLoseCom: 0,
            shareHolderTotalWinLoseCom: 0,
            companyWinLose: 0,
            companyWinLoseCom: 0,
            companyTotalWinLoseCom: 0,
            superAdminWinLose: 0,
            superAdminWinLoseCom: 0,
            superAdminTotalWinLoseCom: 0
        });

        return res.send(
            {
                code: 0,
                message: "success",
                result: {
                    group: agentGroup,
                    dataList: finalResult,
                    summary: summary,
                }
            }
        );

    });

});

router.get('/wl_account_new', function (req, res) {


    let groupId = req.query.groupId;
    let userInfo = req.userInfo;
    let mode = req.query.mode;

    console.log('query : ', req.query)

    async.waterfall([(mainCallback => {
        AgentGroupModel.findById(groupId, (err, response) => {
            if (err) {
                mainCallback(err, null);
            } else {
                if (!response) {
                    mainCallback(888, null);
                } else {
                    mainCallback(null, response);
                }
            }
        }).select('type parentId').populate({
            path: 'parentId',
            select: '_id type'
        });

    }), ((groupInfo, mainCallback) => {


        const agentGroup = groupInfo;

        ReportService.queryMixUseWinLose(groupId, agentGroup.type, agentGroup.parentId ? agentGroup.parentId.type : null, req.query.dateFrom, req.query.dateTo, req.query.timeFrom, req.query.timeTo, req.query.game, (err, wlResponse) => {

            AgentGroupModel.populate(wlResponse, {
                path: 'group',
                select: '_id name name_lower contact'
            }, function (err, memberResult) {
                if (err) {
                    // subQueryCallback(err, null);
                } else {
                    MemberModel.populate(wlResponse, {
                        path: 'member',
                        select: '_id username username_lower contact'
                    }, function (err, memberResult) {
                        if (err) {
                            // subQueryCallback(err, null);
                        } else {
                            mainCallback(null, groupInfo, wlResponse);
                        }
                    });
                    //mainCallback(null, groupInfo,wlResponse);
                }
            });


        });


    })], (err, agentGroup, transaction) => {

        if (err)
            return res.send({message: "error", results: err, code: 999});

        if (mode == 'simple') {
            let finalResult = transformDataByAgentType(agentGroup.type, transaction);


            finalResult = finalResult.sort(function (a, b) {
                let aKey = a.type === 'M_GROUP' ? a.member.username : a.group.name;
                let bKey = b.type === 'M_GROUP' ? b.member.username : b.group.name;
                return naturalSort(aKey, bKey);
            });

            //TODO refacfor
            if (req.query.username) {
                finalResult = _.filter(finalResult, (item) => {
                    return (item.type === 'M_GROUP' ? item.member.username : item.group.name).toLowerCase() === req.query.username.toLowerCase();
                })
            }

            let summary = _.reduce(finalResult, function (memo, obj) {

                return {
                    amount: memo.amount + obj.amount,
                    validAmount: memo.validAmount + obj.validAmount,
                    stackCount: memo.stackCount + obj.stackCount,
                    grossComm: memo.grossComm + obj.grossComm,
                    memberWinLose: memo.memberWinLose + obj.memberWinLose,
                    memberWinLoseCom: memo.memberWinLoseCom + obj.memberWinLoseCom,
                    memberTotalWinLoseCom: memo.memberTotalWinLoseCom + obj.memberTotalWinLoseCom,

                    agentWinLose: memo.agentWinLose + obj.agentWinLose,
                    agentWinLoseCom: memo.agentWinLoseCom + obj.agentWinLoseCom,
                    agentTotalWinLoseCom: memo.agentTotalWinLoseCom + obj.agentTotalWinLoseCom,

                    companyWinLose: memo.companyWinLose + obj.companyWinLose,
                    companyWinLoseCom: memo.companyWinLoseCom + obj.companyWinLoseCom,
                    companyTotalWinLoseCom: roundTo(memo.companyTotalWinLoseCom + obj.companyTotalWinLoseCom, 3),

                }
            }, {
                amount: 0,
                validAmount: 0,
                stackCount: 0,
                grossComm: 0,
                memberWinLose: 0,
                memberWinLoseCom: 0,
                memberTotalWinLoseCom: 0,
                agentWinLose: 0,
                agentWinLoseCom: 0,
                agentTotalWinLoseCom: 0,
                companyWinLose: 0,
                companyWinLoseCom: 0,
                companyTotalWinLoseCom: 0,
            });


            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: {
                        group: agentGroup,
                        dataList: finalResult,
                        summary: summary,
                    }
                }
            );

        } else {
            let finalResult = transformData(agentGroup.type, transaction);

            finalResult = finalResult.sort(function (a, b) {
                let aKey = a.type === 'M_GROUP' ? a.member.username : a.group.name;
                let bKey = b.type === 'M_GROUP' ? b.member.username : b.group.name;
                return naturalSort(aKey, bKey);
            });

            if (req.query.username) {
                finalResult = _.filter(finalResult, (item) => {
                    return (item.type === 'M_GROUP' ? item.member.username : item.group.name).toLowerCase() === req.query.username.toLowerCase();
                })
            }

            function transformData(type, dataList) {
                if (type === 'SHARE_HOLDER') {
                    return _.map(dataList, function (item) {
                        let newItem = item;
                        newItem.memberWinLose = item.memberWinLose;
                        newItem.memberWinLoseCom = item.memberWinLoseCom;
                        newItem.memberTotalWinLoseCom = item.memberTotalWinLoseCom;

                        newItem.agentWinLose = item.agentWinLose;
                        newItem.agentWinLoseCom = item.agentWinLoseCom;
                        newItem.agentTotalWinLoseCom = item.agentTotalWinLoseCom;

                        newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose;
                        newItem.companyWinLoseCom = item.superAdminWinLoseCom + item.companyWinLoseCom;
                        newItem.companyTotalWinLoseCom = item.superAdminTotalWinLoseCom + item.companyTotalWinLoseCom;
                        return newItem;
                    });
                } else if (type === 'SENIOR') {
                    return _.map(dataList, function (item) {
                        let newItem = item;
                        newItem.memberWinLose = item.memberWinLose;
                        newItem.memberWinLoseCom = item.memberWinLoseCom;
                        newItem.memberTotalWinLoseCom = item.memberTotalWinLoseCom;

                        newItem.agentWinLose = item.agentWinLose;
                        newItem.agentWinLoseCom = item.agentWinLoseCom;
                        newItem.agentTotalWinLoseCom = item.agentTotalWinLoseCom;

                        newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose + item.shareHolderWinLose;
                        newItem.companyWinLoseCom = item.superAdminWinLoseCom + item.companyWinLoseCom + item.shareHolderWinLoseCom;
                        newItem.companyTotalWinLoseCom = item.superAdminTotalWinLoseCom + item.companyTotalWinLoseCom + item.shareHolderTotalWinLoseCom;
                        return newItem;
                    });
                } else if (type === 'MASTER_AGENT') {
                    return _.map(dataList, function (item) {
                        let newItem = item;
                        newItem.memberWinLose = item.memberWinLose;
                        newItem.memberWinLoseCom = item.memberWinLoseCom;
                        newItem.memberTotalWinLoseCom = item.memberTotalWinLoseCom;

                        newItem.agentWinLose = item.agentWinLose;
                        newItem.agentWinLoseCom = item.agentWinLoseCom;
                        newItem.agentTotalWinLoseCom = item.agentTotalWinLoseCom;

                        newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose + item.shareHolderWinLose + item.seniorWinLose;
                        newItem.companyWinLoseCom = item.superAdminWinLoseCom + item.companyWinLoseCom + item.shareHolderWinLoseCom + item.seniorWinLoseCom;
                        newItem.companyTotalWinLoseCom = item.superAdminTotalWinLoseCom + item.companyTotalWinLoseCom + item.shareHolderTotalWinLoseCom + item.seniorTotalWinLoseCom;
                        return newItem;
                    });
                } else if (type === 'AGENT') {
                    return _.map(dataList, function (item) {
                        let newItem = item;
                        newItem.memberWinLose = item.memberWinLose;
                        newItem.memberWinLoseCom = item.memberWinLoseCom;
                        newItem.memberTotalWinLoseCom = item.memberTotalWinLoseCom;

                        newItem.agentWinLose = item.agentWinLose;
                        newItem.agentWinLoseCom = item.agentWinLoseCom;
                        newItem.agentTotalWinLoseCom = item.agentTotalWinLoseCom;

                        newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose + item.shareHolderWinLose + item.seniorWinLose + item.masterAgentWinLose;
                        newItem.companyWinLoseCom = item.superAdminWinLoseCom + item.companyWinLoseCom + item.shareHolderWinLoseCom + item.seniorWinLoseCom + item.masterAgentWinLoseCom;
                        newItem.companyTotalWinLoseCom = item.superAdminTotalWinLoseCom + item.companyTotalWinLoseCom + item.shareHolderTotalWinLoseCom + item.seniorTotalWinLoseCom + item.masterAgentTotalWinLoseCom;
                        return newItem;
                    });
                } else {
                    return dataList;
                }
            }


            let summary = _.reduce(finalResult, function (memo, obj) {
                return {
                    amount: memo.amount + obj.amount,
                    validAmount: memo.validAmount + obj.validAmount,
                    stackCount: memo.stackCount + obj.stackCount,
                    grossComm: memo.grossComm + obj.grossComm,
                    memberWinLose: memo.memberWinLose + obj.memberWinLose,
                    memberWinLoseCom: memo.memberWinLoseCom + obj.memberWinLoseCom,
                    memberTotalWinLoseCom: memo.memberTotalWinLoseCom + obj.memberTotalWinLoseCom,

                    agentWinLose: memo.agentWinLose + obj.agentWinLose,
                    agentWinLoseCom: memo.agentWinLoseCom + obj.agentWinLoseCom,
                    agentTotalWinLoseCom: memo.agentTotalWinLoseCom + obj.agentTotalWinLoseCom,

                    masterAgentWinLose: memo.masterAgentWinLose + obj.masterAgentWinLose,
                    masterAgentWinLoseCom: memo.masterAgentWinLoseCom + obj.masterAgentWinLoseCom,
                    masterAgentTotalWinLoseCom: memo.masterAgentTotalWinLoseCom + obj.masterAgentTotalWinLoseCom,

                    seniorWinLose: memo.seniorWinLose + obj.seniorWinLose,
                    seniorWinLoseCom: memo.seniorWinLoseCom + obj.seniorWinLoseCom,
                    seniorTotalWinLoseCom: memo.seniorTotalWinLoseCom + obj.seniorTotalWinLoseCom,

                    shareHolderWinLose: memo.shareHolderWinLose + obj.shareHolderWinLose,
                    shareHolderWinLoseCom: memo.shareHolderWinLoseCom + obj.shareHolderWinLoseCom,
                    shareHolderTotalWinLoseCom: memo.shareHolderTotalWinLoseCom + obj.shareHolderTotalWinLoseCom,

                    companyWinLose: memo.companyWinLose + obj.companyWinLose,
                    companyWinLoseCom: memo.companyWinLoseCom + obj.companyWinLoseCom,
                    companyTotalWinLoseCom: memo.companyTotalWinLoseCom + obj.companyTotalWinLoseCom,

                    superAdminWinLose: memo.superAdminWinLose + obj.superAdminWinLose,
                    superAdminWinLoseCom: memo.superAdminWinLoseCom + obj.superAdminWinLoseCom,
                    superAdminTotalWinLoseCom: memo.superAdminTotalWinLoseCom + obj.superAdminTotalWinLoseCom
                }
            }, {
                amount: 0,
                validAmount: 0,
                stackCount: 0,
                grossComm: 0,
                memberWinLose: 0,
                memberWinLoseCom: 0,
                memberTotalWinLoseCom: 0,
                agentWinLose: 0,
                agentWinLoseCom: 0,
                agentTotalWinLoseCom: 0,
                masterAgentWinLose: 0,
                masterAgentWinLoseCom: 0,
                masterAgentTotalWinLoseCom: 0,
                seniorWinLose: 0,
                seniorWinLoseCom: 0,
                seniorTotalWinLoseCom: 0,
                shareHolderWinLose: 0,
                shareHolderWinLoseCom: 0,
                shareHolderTotalWinLoseCom: 0,
                companyWinLose: 0,
                companyWinLoseCom: 0,
                companyTotalWinLoseCom: 0,
                superAdminWinLose: 0,
                superAdminWinLoseCom: 0,
                superAdminTotalWinLoseCom: 0
            });

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: {
                        group: agentGroup,
                        dataList: finalResult,
                        summary: summary,
                    }
                }
            );
        }


    });

});

router.get('/wl_match_new', function (req, res) {


    let userInfo = req.userInfo;

    let source = req.query.source;
    let groupBy = req.query.groupBy;
    let game = req.query.game;
    let league = req.query.league;
    let matchId = req.query.matchId;
    let currentLv = req.query.lv;


    async.waterfall([(callback => {

        AgentGroupModel.findById(userInfo.group._id, (err, response) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, response);
            }
        }).select('type parentId').populate({
            path: 'parentId',
            select: '_id type'
        });

    }), (groupInfo, callback) => {

        ReportService.queryMixUseWinLoseMatch(userInfo.group._id, userInfo.group.type, groupInfo.parentId ? groupInfo.parentId.type : null, req.query, (err, wlResponse) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, groupInfo, wlResponse);
            }
        });


    }], function (err, groupInfo, wlResponse) {

        if (err)
            return res.send({message: "error", results: err, code: 999});

        let results = wlResponse;

        console.log(results)

        async.series([(callback => {

            // if (((source === 'FOOTBALL' || source === 'BASKETBALL' || source === 'MUAY_THAI' || source === 'M2_MUAY'
            //     || source === 'TENNIS' || source === 'ESPORT' || source === 'TABLE_TENNIS' || source === 'VOLLEYBALL' || source === 'RUGBY'
            //     || source === 'AMERICAN_FOOTBALL' || source === 'CRICKET' || source === 'CYCLING' || source === 'DARTS') && currentLv == 3 ) ||
            //     ((source === 'SEXY_BACCARAT' || source === 'LOTUS' || source === 'AMB_LOTTO' || source === 'AMB_LOTTO_LAOS' || source === 'AMB_LOTTO_PP' || source === 'ALL_BET'
            //     || source === 'SA_GAME' || source === 'DREAM_GAME' || source === 'AG_GAME' || source === 'AMB_GAME' || source === 'PRETTY_GAME') && currentLv == 1 ) ||
            //     ((source === 'SLOT_XO' || source === 'LIVE22' || source === 'DS_GAME' || source === 'AMEBA' || source === 'SPADE_GAME' || source === 'GANAPATI' || source === 'PG_SLOT') && currentLv == 1 )) {
            //     MemberModel.populate(results, {
            //         path: 'member',
            //         select: '_id username contact'
            //     }, function (err, memberResult) {
            //         if (err) {
            //             callback(err, null);
            //         } else {
            //             callback(null, memberResult);
            //         }
            //     });
            // } else {
            callback();
            // }

        })], (err, populateResults) => {


            let finalResult = transformDataByAgentType(userInfo.group.type, results);


            let summary = _.reduce(finalResult, function (memo, obj) {

                return {
                    amount: memo.amount + obj.amount,
                    validAmount: memo.validAmount + obj.validAmount,
                    stackCount: memo.stackCount + obj.stackCount,
                    grossComm: memo.grossComm + obj.grossComm,
                    memberWinLose: memo.memberWinLose + obj.memberWinLose,
                    memberWinLoseCom: memo.memberWinLoseCom + obj.memberWinLoseCom,
                    memberTotalWinLoseCom: memo.memberTotalWinLoseCom + obj.memberTotalWinLoseCom,

                    agentWinLose: memo.agentWinLose + obj.agentWinLose,
                    agentWinLoseCom: memo.agentWinLoseCom + obj.agentWinLoseCom,
                    agentTotalWinLoseCom: memo.agentTotalWinLoseCom + obj.agentTotalWinLoseCom,


                    companyWinLose: memo.companyWinLose + obj.companyWinLose,
                    companyWinLoseCom: memo.companyWinLoseCom + obj.companyWinLoseCom,
                    companyTotalWinLoseCom: memo.companyTotalWinLoseCom + obj.companyTotalWinLoseCom
                }
            }, {
                amount: 0,
                validAmount: 0,
                stackCount: 0,
                grossComm: 0,
                memberWinLose: 0,
                memberWinLoseCom: 0,
                memberTotalWinLoseCom: 0,
                agentWinLose: 0,
                agentWinLoseCom: 0,
                agentTotalWinLoseCom: 0,
                companyWinLose: 0,
                companyWinLoseCom: 0,
                companyTotalWinLoseCom: 0
            });

            let lv = currentLv;

            let sortingData = finalResult;


            if (source === 'FOOTBALL' || source === 'BASKETBALL' || source === 'MUAY_THAI' || source === 'M2_MUAY' || source === 'TENNIS' || source === 'ESPORT'
                || source === 'TABLE_TENNIS' || source === 'VOLLEYBALL' || source === 'RUGBY'
                || source === 'AMERICAN_FOOTBALL' || source === 'CRICKET' || source === 'CYCLING' || source === 'DARTS') {
                if (currentLv == 1) {
                    game = groupBy;
                } else if (currentLv == 2) {
                    league = groupBy;
                } else if (currentLv == 3) {
                    matchId = groupBy;
                }
            }


            if (results && results.length > 0) {

                if (source === 'FOOTBALL' || source === 'BASKETBALL' || source === 'MUAY_THAI' || source === 'M2_MUAY' || source === 'TENNIS' || source === 'ESPORT'
                    || source === 'TABLE_TENNIS' || source === 'VOLLEYBALL' || source === 'RUGBY'
                    || source === 'AMERICAN_FOOTBALL' || source === 'CRICKET' || source === 'CYCLING' || source === 'DARTS') {
                    if (lv == 1) {
                        lv = 2;
                        sortingData = sortingData.sort(function (a, b) {
                            return naturalSort(a.leagueName, b.leagueName);
                        });
                    } else if (lv == 2) {
                        sortingData = sortingData.sort(function (a, b) {
                            return naturalSort(a.matchName.en.h + a.matchName.en.a, b.matchName.en.h + b.matchName.en.a);
                        });
                        lv = 3;
                    } else if (lv == 3) {
                        sortingData = sortingData.sort(function (a, b) {
                            return naturalSort(a.member.username, b.member.username);
                        });
                        lv = 4;
                    }
                } else if (source === 'SEXY_BACCARAT' || source === 'LOTUS' || source === 'SA_GAME' || source === 'DREAM_GAME' || source === 'SPADE_GAME' || source === 'GANAPATI' || source === 'AG_GAME'
                    || source === 'ALL_BET' || source === 'SLOT_XO' || source === 'DS_GAME' || source === 'LIVE22' || source === 'AMEBA' || source === 'AMB_LOTTO'
                    || source === 'AMB_LOTTO_LAOS' || source === 'AMB_LOTTO_PP' || source === 'AMB_GAME' || source === 'PRETTY_GAME' || source === 'PG_SLOT') {
                    if (lv == 1) {
                        sortingData = sortingData.sort(function (a, b) {
                            return naturalSort(a.member.username, b.member.username);
                        });
                        lv = 2;
                    }
                } else {
                    lv = 1;
                    sortingData = sortingData.sort(function (a, b) {
                        console.log(a.key, b.key)
                        return naturalSort(a.key, b.key);
                    });
                }
            }

            return res.send({
                code: 0,
                message: "success",
                result: {
                    group: groupInfo,
                    lv: lv,
                    game: game,
                    league: league,
                    matchId: matchId,
                    dataList: sortingData,
                    summary: summary,
                }
            });

        });
    });
});

router.get('/wl_match', function (req, res) {

    let userInfo = req.userInfo;

    let source = req.query.source;
    let groupBy = req.query.groupBy;
    let game = req.query.game;
    let league = req.query.league;
    let matchId = req.query.matchId;
    let currentLv = req.query.lv;

    async.waterfall([(callback => {

        AgentGroupModel.findById(userInfo.group._id, (err, response) => {
            if (err) {
                callback(err, null);
            } else {
                console.log('AgentGroupModel response ', req.userInfo, response)
                callback(null, response);
            }
        }).select('type parentId').populate({
            path: 'parentId',
            select: '_id type'
        });

    }), (groupInfo,callback) => {

        ReportService.isElasticEnabled(isElsEnabled => {

            console.log('isElsEnabled : ',isElsEnabled);
            if (isElsEnabled) {

                ReportService.queryMixUseWinLoseMatch(userInfo.group._id, userInfo.group.type, groupInfo.parentId ? groupInfo.parentId.type : null, req.query, (err, wlResponse) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, groupInfo, wlResponse);
                    }
                });

            }else {
                ReportService.getWinLoseMatch(userInfo.group._id, userInfo.group.type, req.query, (err, wlResponse) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, groupInfo,wlResponse);
                    }
                });
            }
        });

    }], function (err, groupInfo,transactions) {

        if (err)
            return res.send({message: "error", results: err, code: 999});

        let results = transactions;

        async.series([(callback => {

            if (((source === 'FOOTBALL' || source === 'BASKETBALL' || source === 'MUAY_THAI' || source === 'M2_MUAY'
                || source === 'TENNIS' || source === 'ESPORT' || source === 'TABLE_TENNIS' || source === 'VOLLEYBALL' || source === 'RUGBY'
                || source === 'AMERICAN_FOOTBALL' || source === 'CRICKET' || source === 'CYCLING' || source === 'DARTS') && currentLv == 3 ) ||
                ((source === 'SEXY_BACCARAT' || source === 'LOTUS' || source === 'AMB_LOTTO' || source === 'AMB_LOTTO_LAOS' || source === 'AMB_LOTTO_PP' || source === 'ALL_BET'
                || source === 'SA_GAME' || source === 'DREAM_GAME' || source === 'AG_GAME' || source === 'AMB_GAME'|| source === 'AMB_GAME2' || source === 'PRETTY_GAME') && currentLv == 1 ) ||
                ((source === 'SLOT_XO' || source === 'LIVE22' || source === 'DS_GAME' || source === 'AMEBA' || source === 'SPADE_GAME' || source === 'GANAPATI' || source === 'PG_SLOT') && currentLv == 1 )) {
                MemberModel.populate(results, {
                    path: 'member',
                    select: '_id username contact'
                }, function (err, memberResult) {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, memberResult);
                    }
                });
            } else {
                callback();
            }

        })], (err, populateResults) => {

            let finalResult = transformDataByAgentType(userInfo.group.type, results);

            let summary = _.reduce(finalResult, function (memo, obj) {

                return {
                    amount: memo.amount + obj.amount,
                    validAmount: memo.validAmount + obj.validAmount,
                    stackCount: memo.stackCount + obj.stackCount,
                    grossComm: memo.grossComm + obj.grossComm,
                    memberWinLose: memo.memberWinLose + obj.memberWinLose,
                    memberWinLoseCom: memo.memberWinLoseCom + obj.memberWinLoseCom,
                    memberTotalWinLoseCom: memo.memberTotalWinLoseCom + obj.memberTotalWinLoseCom,

                    agentWinLose: memo.agentWinLose + obj.agentWinLose,
                    agentWinLoseCom: memo.agentWinLoseCom + obj.agentWinLoseCom,
                    agentTotalWinLoseCom: memo.agentTotalWinLoseCom + obj.agentTotalWinLoseCom,


                    companyWinLose: memo.companyWinLose + obj.companyWinLose,
                    companyWinLoseCom: memo.companyWinLoseCom + obj.companyWinLoseCom,
                    companyTotalWinLoseCom: memo.companyTotalWinLoseCom + obj.companyTotalWinLoseCom
                }
            }, {
                amount: 0,
                validAmount: 0,
                stackCount: 0,
                grossComm: 0,
                memberWinLose: 0,
                memberWinLoseCom: 0,
                memberTotalWinLoseCom: 0,
                agentWinLose: 0,
                agentWinLoseCom: 0,
                agentTotalWinLoseCom: 0,
                companyWinLose: 0,
                companyWinLoseCom: 0,
                companyTotalWinLoseCom: 0
            });

            let lv = currentLv;

            let sortingData = finalResult;


            if (source === 'FOOTBALL' || source === 'BASKETBALL' || source === 'MUAY_THAI' || source === 'M2_MUAY' || source === 'TENNIS' || source === 'ESPORT'
                || source === 'TABLE_TENNIS' || source === 'VOLLEYBALL' || source === 'RUGBY'
                || source === 'AMERICAN_FOOTBALL' || source === 'CRICKET' || source === 'CYCLING' || source === 'DARTS') {
                if (currentLv == 1) {
                    game = groupBy;
                } else if (currentLv == 2) {
                    league = groupBy;
                } else if (currentLv == 3) {
                    matchId = groupBy;
                }
            }


            if (results && results.length > 0) {

                if (source === 'FOOTBALL' || source === 'BASKETBALL' || source === 'MUAY_THAI' || source === 'M2_MUAY' || source === 'TENNIS' || source === 'ESPORT'
                    || source === 'TABLE_TENNIS' || source === 'VOLLEYBALL' || source === 'RUGBY'
                    || source === 'AMERICAN_FOOTBALL' || source === 'CRICKET' || source === 'CYCLING' || source === 'DARTS') {
                    if (lv == 1) {
                        lv = 2;
                        sortingData = sortingData.sort(function (a, b) {
                            return naturalSort(a.leagueName, b.leagueName);
                        });
                    } else if (lv == 2) {
                        sortingData = sortingData.sort(function (a, b) {
                            return naturalSort(a.matchName.en.h + a.matchName.en.a, b.matchName.en.h + b.matchName.en.a);
                        });
                        lv = 3;
                    } else if (lv == 3) {
                        sortingData = sortingData.sort(function (a, b) {
                            return naturalSort(a.member.username, b.member.username);
                        });
                        lv = 4;
                    }
                } else if (source === 'SEXY_BACCARAT' || source === 'LOTUS' || source === 'SA_GAME' || source === 'DREAM_GAME' || source === 'SPADE_GAME' || source === 'GANAPATI' || source === 'AG_GAME'
                    || source === 'ALL_BET' || source === 'SLOT_XO' || source === 'DS_GAME' || source === 'LIVE22' || source === 'AMEBA' || source === 'AMB_LOTTO'
                    || source === 'AMB_LOTTO_LAOS' || source === 'AMB_LOTTO_PP' || source === 'AMB_GAME' || source === 'AMB_GAME2' || source === 'PRETTY_GAME' || source === 'PG_SLOT') {
                    if (lv == 1) {
                        sortingData = sortingData.sort(function (a, b){
                            return naturalSort(a.member.username, b.member.username);
                        });
                        lv = 2;
                    }
                } else {
                    lv = 1;
                    sortingData = sortingData.sort(function (a, b) {
                        return naturalSort(a.key, b.key);
                    });
                }
            }

            return res.send({
                code: 0,
                message: "success",
                result: {
                    group: groupInfo,
                    lv: lv,
                    game: game,
                    league: league,
                    matchId: matchId,
                    dataList: sortingData,
                    summary: summary,
                }
            });

        });
    });
});

router.get('/wl_match_detail1', function (req, res) {


    let userInfo = req.userInfo;

    let group = '';
    let child = '';

    getGroupAndChild(userInfo.group.type, (groupRes, childRes) => {
        group = groupRes;
        child = childRes;
    });

    let condition = {};

    if (req.query.dateFrom && req.query.dateTo) {
        condition['gameDate'] = {
            "$gte": DateUtils.convertDateTime(req.query.dateFrom, req.query.timeFrom),
            "$lt": DateUtils.convertDateTime(req.query.dateTo, req.query.timeTo)
        }
    }
    condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(userInfo.group._id);
    condition['status'] = {$in: ['DONE', 'REJECTED', 'CANCELLED']};


    BetTransactionModel.aggregate([
        {
            $match: condition
        },
        {
            "$group": {
                "_id": {
                    game: '$game',
                    source: '$source',
                    leagueId: '$hdp.leagueId',
                    leagueName: '$hdp.leagueName',
                    matchName: '$hdp.matchName',
                    matchId: '$hdp.matchId',
                    oddType: '$hdp.oddType',
                    parlay: '$parlay',
                    gameType: '$gameType',
                },
                "stackCount": {$sum: 1},
                "amount": {$sum: '$amount'},
                "grossComm": {$sum: {$multiply: ['$amount', {$divide: ['$commission.' + group + '.commission', 100]}]}},
                "memberWinLose": {$sum: '$commission.member.winLose'},
                "memberWinLoseCom": {$sum: '$commission.member.winLoseCom'},
                "memberTotalWinLoseCom": {$sum: '$commission.member.totalWinLoseCom'},

                "agentWinLose": {$sum: '$commission.agent.winLose'},
                "agentWinLoseCom": {$sum: '$commission.agent.winLoseCom'},
                "agentTotalWinLoseCom": {$sum: '$commission.agent.totalWinLoseCom'},

                "masterAgentWinLose": {$sum: '$commission.masterAgent.winLose'},
                "masterAgentWinLoseCom": {$sum: '$commission.masterAgent.winLoseCom'},
                "masterAgentTotalWinLoseCom": {$sum: '$commission.masterAgent.totalWinLoseCom'},

                "seniorWinLose": {$sum: '$commission.senior.winLose'},
                "seniorWinLoseCom": {$sum: '$commission.senior.winLoseCom'},
                "seniorTotalWinLoseCom": {$sum: '$commission.senior.totalWinLoseCom'},

                "shareHolderWinLose": {$sum: '$commission.shareHolder.winLose'},
                "shareHolderWinLoseCom": {$sum: '$commission.shareHolder.winLoseCom'},
                "shareHolderTotalWinLoseCom": {$sum: '$commission.shareHolder.totalWinLoseCom'},

                "companyWinLose": {$sum: '$commission.company.winLose'},
                "companyWinLoseCom": {$sum: '$commission.company.winLoseCom'},
                "companyTotalWinLoseCom": {$sum: '$commission.company.totalWinLoseCom'},

                "superAdminWinLose": {$sum: '$commission.superAdmin.winLose'},
                "superAdminWinLoseCom": {$sum: '$commission.superAdmin.winLoseCom'},
                "superAdminTotalWinLoseCom": {$sum: '$commission.superAdmin.totalWinLoseCom'}
            }
        },
        {
            $project: {
                _id: 0,
                key: '$_id',
                source: '$source',
                parlay: '$_id.parlay',
                member: '$_id.member',
                group: '$_id.commission',
                stackCount: '$stackCount',
                grossComm: '$grossComm',
                amount: '$amount',
                memberWinLose: '$memberWinLose',
                memberWinLoseCom: '$memberWinLoseCom',
                memberTotalWinLoseCom: '$memberTotalWinLoseCom',
                agentWinLose: '$agentWinLose',
                agentWinLoseCom: '$agentWinLoseCom',
                agentTotalWinLoseCom: '$agentTotalWinLoseCom',
                masterAgentWinLose: '$masterAgentWinLose',
                masterAgentWinLoseCom: '$masterAgentWinLoseCom',
                masterAgentTotalWinLoseCom: '$masterAgentTotalWinLoseCom',
                seniorWinLose: '$seniorWinLose',
                seniorWinLoseCom: '$seniorWinLoseCom',
                seniorTotalWinLoseCom: '$seniorTotalWinLoseCom',
                shareHolderWinLose: '$shareHolderWinLose',
                shareHolderWinLoseCom: '$shareHolderWinLoseCom',
                shareHolderTotalWinLoseCom: '$shareHolderTotalWinLoseCom',
                companyWinLose: '$companyWinLose',
                companyWinLoseCom: '$companyWinLoseCom',
                companyTotalWinLoseCom: '$companyTotalWinLoseCom',
                superAdminWinLose: '$superAdminWinLose',
                superAdminWinLoseCom: '$superAdminWinLoseCom',
                superAdminTotalWinLoseCom: '$superAdminTotalWinLoseCom'
            }
        }
    ]).read('secondaryPreferred').exec((err, results) => {
        if (err) {
            return res.send({message: "error", results: err, code: 999});
        } else {


            results = _.chain(results).groupBy(function (value) {
                if (value.key.game === 'FOOTBALL' || value.key.game === 'M2') {

                    if (value.key.gameType === 'TODAY') {
                        let oddType = '';
                        if (value.key.oddType === 'AH' || value.key.oddType === 'AH1ST') {
                            oddType = 'AH';
                        } else if (value.key.oddType === 'TTC' || value.key.oddType === 'TTC1ST') {
                            oddType = 'TTC';
                        } else if (value.key.oddType === 'OU' || value.key.oddType === 'OU1ST' || value.key.oddType === 'T1OU' || value.key.oddType === 'T2OU') {
                            oddType = 'OU';
                        } else if (value.key.oddType === 'OE' || value.key.oddType === 'OE1ST') {
                            oddType = 'OE';
                        } else if (value.key.oddType === 'X12' || value.key.oddType === 'X121ST') {
                            oddType = 'X12';
                        } else if (value.key.oddType === 'CS' || value.key.oddType === 'FHCS') {
                            oddType = 'CS';
                        } else if (value.key.oddType === 'TG') {
                            oddType = 'TG';
                        } else if (value.key.oddType === 'FTHT') {
                            oddType = 'FTHT';
                        } else if (value.key.oddType === 'DC') {
                            oddType = 'DC';
                        } else if (value.key.oddType === 'ML') {
                            oddType = 'ML';
                        } else {
                            oddType = value.key.oddType;
                        }
                        return value.key.source + '|' + value.key.leagueId + '|' + value.key.matchId + '|'
                            + value.key.matchName['en'].h + ' vs ' + value.key.matchName['en'].a + '|' + value.key.leagueName + '|' + oddType
                    } else {


                        let source = value.key.source;
                        let oddType = value.key.gameType === 'PARLAY' ? 'MIX_PARLAY' : 'MIX_STEP';
                        if (value.key.gameType == 'MIX_STEP' || value.key.gameType == 'MIX_PARLAY') {
                            if (value.key.parlay.matches[0].sportType === 'FOOTBALL') {
                                source = 'FOOTBALL';
                            } else if (value.key.parlay.matches[0].sportType === 'MUAY_THAI') {
                                source = 'MUAY_THAI';
                            } else if (value.key.parlay.matches[0].sportType === 'M2_MUAY') {
                                source = 'M2_MUAY';
                            } else if (value.key.parlay.matches[0].sportType === 'BASKETBALL') {
                                source = 'BASKETBALL';
                            }

                        }

                        // let oddType = value.key.gameType == 'PARLAY' ? 'PARLAY' :
                        return source + '|' + value.key.parlay.matches[0].leagueId + '|' + value.key.parlay.matches[0].matchId + '|'
                            + value.key.parlay.matches[0].matchName['en'].h + ' vs ' + value.key.parlay.matches[0].matchName['en'].a
                            + '|' + value.key.parlay.matches[0].leagueName + '|' + oddType;
                    }
                } else if (value.key.source === 'SEXY_BACCARAT') {
                    return 'SEXY_BACCARAT';
                } else if (value.key.source === 'LOTUS') {
                    return 'LOTUS';
                } else if (value.key.source === 'ALL_BET') {
                    return 'ALL_BET';
                } else if (value.key.source === 'SA_GAME') {
                    return 'SA_GAME';
                } else if (value.key.source === 'DREAM_GAME') {
                    return 'DREAM_GAME';
                } else if (value.key.source === 'PRETTY_GAME') {
                    return 'PRETTY_GAME';
                } else if (value.key.source === 'AG_GAME') {
                    return 'AG_GAME';
                } else if (value.key.source === 'SLOT_XO') {
                    return 'SLOT_XO';
                } else if (value.key.source === 'DS_GAME') {
                    return 'DS_GAME';
                } else if (value.key.source === 'LIVE22') {
                    return 'LIVE22';
                } else if (value.key.source === 'SPADE_GAME') {
                    return 'SPADE_GAME';
                } else if (value.key.source === 'AMEBA') {
                    return 'AMEBA';
                } else if (value.key.source === 'AMB_LOTTO') {
                    return 'AMB_LOTTO';
                } else if (value.key.source === 'AMB_GAME') {
                    return 'AMB_GAME';
                } else if (value.key.source === 'GANAPATI') {
                    return 'GANAPATI';
                } else if (value.key.source === 'PG_SLOT') {
                    return 'PG_SLOT';
                } else {
                    return value.key.source;
                }
            }).map(function (value, key) {

                return {
                    key: key,
                    game: key.split('|')[0],
                    leagueId: key.split('|')[1],
                    matchId: key.split('|')[2],
                    matchName: key.split('|')[3],
                    leagueName: key.split('|')[4],
                    oddType: key.split('|')[5],
                    sum: _.reduce(value, function (memo, obj) {
                        return {
                            amount: memo.amount + obj.amount,
                            grossComm: memo.grossComm + obj.grossComm,
                            memberWinLose: memo.memberWinLose + obj.memberWinLose,
                            memberWinLoseCom: memo.memberWinLoseCom + obj.memberWinLoseCom,
                            memberTotalWinLoseCom: memo.memberTotalWinLoseCom + obj.memberTotalWinLoseCom,
                            agentWinLose: memo.agentWinLose + obj.agentWinLose,
                            agentWinLoseCom: memo.agentWinLoseCom + obj.agentWinLoseCom,
                            agentTotalWinLoseCom: memo.agentTotalWinLoseCom + obj.agentTotalWinLoseCom,
                            masterAgentWinLose: memo.masterAgentWinLose + obj.masterAgentWinLose,
                            masterAgentWinLoseCom: memo.masterAgentWinLoseCom + obj.masterAgentWinLoseCom,
                            masterAgentTotalWinLoseCom: memo.masterAgentTotalWinLoseCom + obj.masterAgentTotalWinLoseCom,
                            seniorWinLose: memo.seniorWinLose + obj.seniorWinLose,
                            seniorWinLoseCom: memo.seniorWinLoseCom + obj.seniorWinLoseCom,
                            seniorTotalWinLoseCom: memo.seniorTotalWinLoseCom + obj.seniorTotalWinLoseCom,
                            shareHolderWinLose: memo.shareHolderWinLose + obj.shareHolderWinLose,
                            shareHolderWinLoseCom: memo.shareHolderWinLoseCom + obj.shareHolderWinLoseCom,
                            shareHolderTotalWinLoseCom: memo.shareHolderTotalWinLoseCom + obj.shareHolderTotalWinLoseCom,
                            companyWinLose: memo.companyWinLose + obj.companyWinLose,
                            companyWinLoseCom: memo.companyWinLoseCom + obj.companyWinLoseCom,
                            companyTotalWinLoseCom: memo.companyTotalWinLoseCom + obj.companyTotalWinLoseCom,
                            superAdminWinLose: memo.superAdminWinLose + obj.superAdminWinLose,
                            superAdminWinLoseCom: memo.superAdminWinLoseCom + obj.superAdminWinLoseCom,
                            superAdminTotalWinLoseCom: memo.superAdminTotalWinLoseCom + obj.superAdminTotalWinLoseCom
                        }
                    }, {
                        amount: 0,
                        grossComm: 0,
                        memberWinLose: 0,
                        memberWinLoseCom: 0,
                        memberTotalWinLoseCom: 0,
                        agentWinLose: 0,
                        agentWinLoseCom: 0,
                        agentTotalWinLoseCom: 0,
                        masterAgentWinLose: 0,
                        masterAgentWinLoseCom: 0,
                        masterAgentTotalWinLoseCom: 0,
                        seniorWinLose: 0,
                        seniorWinLoseCom: 0,
                        seniorTotalWinLoseCom: 0,
                        shareHolderWinLose: 0,
                        shareHolderWinLoseCom: 0,
                        shareHolderTotalWinLoseCom: 0,
                        companyWinLose: 0,
                        companyWinLoseCom: 0,
                        companyTotalWinLoseCom: 0,
                        superAdminWinLose: 0,
                        superAdminWinLoseCom: 0,
                        superAdminTotalWinLoseCom: 0
                    })
                }
            }).groupBy(function (value) {
                if (value.game === 'FOOTBALL' || value.game === 'BASKETBALL' || value.game === 'MUAY_THAI' || value.game === 'M2_MUAY' || value.game === 'TENNIS'
                    || value.game === 'ESPORT' || value.game === 'TABLE_TENNIS' || value.game === 'VOLLEYBALL' || value.game === 'RUGBY'
                    || value.game === 'AMERICAN_FOOTBALL' || value.game === 'CRICKET' || value.game === 'CYCLING' || value.game === 'DARTS') {
                    return value.game + '|' + value.leagueId + '|' + value.matchId + '|' + value.matchName + '|' + value.leagueName
                } else if (value.key === 'SEXY_BACCARAT') {
                    return 'SEXY_BACCARAT';
                } else if (value.key === 'LOTUS') {
                    return 'LOTUS';
                } else if (value.key === 'ALL_BET') {
                    return 'ALL_BET';
                } else if (value.key === 'SA_GAME') {
                    return 'SA_GAME';
                } else if (value.key === 'DREAM_GAME') {
                    return 'DREAM_GAME';
                } else if (value.key === 'AG_GAME') {
                    return 'AG_GAME';
                } else if (value.key === 'PRETTY_GAME') {
                    return 'PRETTY_GAME';
                } else if (value.key === 'SLOT_XO') {
                    return 'SLOT_XO';
                } else if (value.key === 'DS_GAME') {
                    return 'DS_GAME';
                } else if (value.key === 'LIVE22') {
                    return 'LIVE22';
                } else if (value.key === 'SPADE_GAME') {
                    return 'SPADE_GAME';
                } else if (value.key === 'AMEBA') {
                    return 'AMEBA';
                } else if (value.key === 'AMB_LOTTO') {
                    return 'AMB_LOTTO';
                } else if (value.key === 'AMB_LOTTO_LAOS') {
                    return 'AMB_LOTTO_LAOS';
                } else if (value.key === 'AMB_LOTTO_PP') {
                    return 'AMB_LOTTO_PP';
                } else if (value.key === 'AMB_GAME') {
                    return 'AMB_GAME';
                } else if (value.key === 'GANAPATI') {
                    return 'GANAPATI';
                } else if (value.key === 'PG_SLOT') {
                    return 'PG_SLOT';
                }
            }).map(function (value, key) {

                let betType = {};
                if (key == 'SEXY_BACCARAT') {
                    betType = _.map(value, function (value, key) {
                        return {
                            type: 'SB',
                            sum: value.sum
                        };
                    })
                } else if (key == 'LOTUS') {
                    betType = _.map(value, function (value, key) {
                        return {
                            type: 'LT',
                            sum: value.sum
                        };
                    })
                } else if (key == 'SA_GAME') {
                    betType = _.map(value, function (value, key) {
                        return {
                            type: 'SA',
                            sum: value.sum
                        };
                    })
                } else if (key == 'DREAM_GAME') {
                    betType = _.map(value, function (value, key) {
                        return {
                            type: 'DG',
                            sum: value.sum
                        };
                    })
                } else if (key == 'AG_GAME') {
                    betType = _.map(value, function (value, key) {
                        return {
                            type: 'AG',
                            sum: value.sum
                        };
                    })
                } else if (key == 'PRETTY_GAME') {
                    betType = _.map(value, function (value, key) {
                        return {
                            type: 'PT',
                            sum: value.sum
                        };
                    })
                } else if (key == 'ALL_BET') {
                    betType = _.map(value, function (value, key) {
                        return {
                            type: 'AB',
                            sum: value.sum
                        };
                    })
                } else if (key == 'SLOT_XO') {
                    betType = _.map(value, function (value, key) {
                        return {
                            type: 'Slot XO',
                            sum: value.sum
                        };
                    })

                } else if (key == 'SPADE_GAME') {
                    betType = _.map(value, function (value, key) {
                        return {
                            type: 'Spade Game',
                            sum: value.sum
                        };
                    })
                } else if (key == 'GANAPATI') {
                    betType = _.map(value, function (value, key) {
                        return {
                            type: 'Ganapati',
                            sum: value.sum
                        };
                    })
                } else if (key == 'LIVE22') {
                    betType = _.map(value, function (value, key) {
                        return {
                            type: 'Live 22',
                            sum: value.sum
                        };
                    })
                } else if (key == 'DS_GAME') {
                    betType = _.map(value, function (value, key) {
                        return {
                            type: 'DS_GAME',
                            sum: value.sum
                        };
                    })
                } else if (key == 'AMEBA') {
                    betType = _.map(value, function (value, key) {
                        return {
                            type: 'Ameba Game',
                            sum: value.sum
                        };
                    })
                } else if (key == 'PG_SLOT') {
                    betType = _.map(value, function (value, key) {
                        return {
                            type: 'PG Slot',
                            sum: value.sum
                        };
                    })
                } else if (key == 'AMB_LOTTO') {
                    betType = _.map(value, function (value, key) {
                        return {
                            type: 'Amb Lotto',
                            sum: value.sum
                        };
                    })
                } else if (key == 'AMB_LOTTO_LAOS') {
                    betType = _.map(value, function (value, key) {
                        return {
                            type: 'Amb Lotto Laos',
                            sum: value.sum
                        };
                    })
                } else if (key == 'AMB_LOTTO_PP') {
                    betType = _.map(value, function (value, key) {
                        return {
                            type: 'Amb Lotto Online',
                            sum: value.sum
                        };
                    })
                } else if (key == 'AMB_GAME') {
                    betType = _.map(value, function (value, key) {
                        return {
                            type: 'Amb Game',
                            sum: value.sum
                        };
                    })
                } else {
                    betType = _.map(value, function (value, key) {
                        return {
                            type: value.oddType,
                            sum: value.sum
                        };
                    })
                }

                _.forEach(betType, (item) => {
                    item.sum = transformData(userInfo.group.type, item.sum);
                });

                return {
                    game: key.split('|')[0],
                    leagueId: key.split('|')[1],
                    matchId: key.split('|')[2],
                    matchName: key.split('|')[3],
                    leagueName: key.split('|')[4],
                    oddType: key.split('|')[5],
                    betTypes: betType,
                    summary: _.reduce(value, function (memo, item) {
                        let obj = item.sum;
                        return {
                            amount: memo.amount + obj.amount,
                            grossComm: memo.grossComm + obj.grossComm,
                            memberWinLose: memo.memberWinLose + obj.memberWinLose,
                            memberWinLoseCom: memo.memberWinLoseCom + obj.memberWinLoseCom,
                            memberTotalWinLoseCom: memo.memberTotalWinLoseCom + obj.memberTotalWinLoseCom,
                            agentWinLose: memo.agentWinLose + obj.agentWinLose,
                            agentWinLoseCom: memo.agentWinLoseCom + obj.agentWinLoseCom,
                            agentTotalWinLoseCom: memo.agentTotalWinLoseCom + obj.agentTotalWinLoseCom,
                            masterAgentWinLose: memo.masterAgentWinLose + obj.masterAgentWinLose,
                            masterAgentWinLoseCom: memo.masterAgentWinLoseCom + obj.masterAgentWinLoseCom,
                            masterAgentTotalWinLoseCom: memo.masterAgentTotalWinLoseCom + obj.masterAgentTotalWinLoseCom,
                            seniorWinLose: memo.seniorWinLose + obj.seniorWinLose,
                            seniorWinLoseCom: memo.seniorWinLoseCom + obj.seniorWinLoseCom,
                            seniorTotalWinLoseCom: memo.seniorTotalWinLoseCom + obj.seniorTotalWinLoseCom,
                            shareHolderWinLose: memo.shareHolderWinLose + obj.shareHolderWinLose,
                            shareHolderWinLoseCom: memo.shareHolderWinLoseCom + obj.shareHolderWinLoseCom,
                            shareHolderTotalWinLoseCom: memo.shareHolderTotalWinLoseCom + obj.shareHolderTotalWinLoseCom,
                            companyWinLose: memo.companyWinLose + obj.companyWinLose,
                            companyWinLoseCom: memo.companyWinLoseCom + obj.companyWinLoseCom,
                            companyTotalWinLoseCom: memo.companyTotalWinLoseCom + obj.companyTotalWinLoseCom,
                            superAdminWinLose: memo.superAdminWinLose + obj.superAdminWinLose,
                            superAdminWinLoseCom: memo.superAdminWinLoseCom + obj.superAdminWinLoseCom,
                            superAdminTotalWinLoseCom: memo.superAdminTotalWinLoseCom + obj.superAdminTotalWinLoseCom
                        }
                    }, {
                        amount: 0,
                        grossComm: 0,
                        memberWinLose: 0,
                        memberWinLoseCom: 0,
                        memberTotalWinLoseCom: 0,
                        agentWinLose: 0,
                        agentWinLoseCom: 0,
                        agentTotalWinLoseCom: 0,
                        masterAgentWinLose: 0,
                        masterAgentWinLoseCom: 0,
                        masterAgentTotalWinLoseCom: 0,
                        seniorWinLose: 0,
                        seniorWinLoseCom: 0,
                        seniorTotalWinLoseCom: 0,
                        shareHolderWinLose: 0,
                        shareHolderWinLoseCom: 0,
                        shareHolderTotalWinLoseCom: 0,
                        companyWinLose: 0,
                        companyWinLoseCom: 0,
                        companyTotalWinLoseCom: 0,
                        superAdminWinLose: 0,
                        superAdminWinLoseCom: 0,
                        superAdminTotalWinLoseCom: 0
                    })
                }
            }).sortBy(function (o) {
                return o.matchName;
            }).sortBy(function (o) {
                return o.leagueName;
            }).sortBy(function (o) {
                return o.game;
            }).value();


            let totalSummary = _.reduce(results, function (memo, item) {
                let obj = item.summary;

                return {
                    amount: memo.amount + obj.amount,
                    grossComm: memo.grossComm + obj.grossComm,
                    memberWinLose: memo.memberWinLose + obj.memberWinLose,
                    memberWinLoseCom: memo.memberWinLoseCom + obj.memberWinLoseCom,
                    memberTotalWinLoseCom: memo.memberTotalWinLoseCom + obj.memberTotalWinLoseCom,
                    agentWinLose: memo.agentWinLose + obj.agentWinLose,
                    agentWinLoseCom: memo.agentWinLoseCom + obj.agentWinLoseCom,
                    agentTotalWinLoseCom: memo.agentTotalWinLoseCom + obj.agentTotalWinLoseCom,
                    masterAgentWinLose: memo.masterAgentWinLose + obj.masterAgentWinLose,
                    masterAgentWinLoseCom: memo.masterAgentWinLoseCom + obj.masterAgentWinLoseCom,
                    masterAgentTotalWinLoseCom: memo.masterAgentTotalWinLoseCom + obj.masterAgentTotalWinLoseCom,
                    seniorWinLose: memo.seniorWinLose + obj.seniorWinLose,
                    seniorWinLoseCom: memo.seniorWinLoseCom + obj.seniorWinLoseCom,
                    seniorTotalWinLoseCom: memo.seniorTotalWinLoseCom + obj.seniorTotalWinLoseCom,
                    shareHolderWinLose: memo.shareHolderWinLose + obj.shareHolderWinLose,
                    shareHolderWinLoseCom: memo.shareHolderWinLoseCom + obj.shareHolderWinLoseCom,
                    shareHolderTotalWinLoseCom: memo.shareHolderTotalWinLoseCom + obj.shareHolderTotalWinLoseCom,
                    companyWinLose: memo.companyWinLose + obj.companyWinLose,
                    companyWinLoseCom: memo.companyWinLoseCom + obj.companyWinLoseCom,
                    companyTotalWinLoseCom: memo.companyTotalWinLoseCom + obj.companyTotalWinLoseCom,
                    superAdminWinLose: memo.superAdminWinLose + obj.superAdminWinLose,
                    superAdminWinLoseCom: memo.superAdminWinLoseCom + obj.superAdminWinLoseCom,
                    superAdminTotalWinLoseCom: memo.superAdminTotalWinLoseCom + obj.superAdminTotalWinLoseCom
                }
            }, {
                amount: 0,
                grossComm: 0,
                memberWinLose: 0,
                memberWinLoseCom: 0,
                memberTotalWinLoseCom: 0,
                agentWinLose: 0,
                agentWinLoseCom: 0,
                agentTotalWinLoseCom: 0,
                masterAgentWinLose: 0,
                masterAgentWinLoseCom: 0,
                masterAgentTotalWinLoseCom: 0,
                seniorWinLose: 0,
                seniorWinLoseCom: 0,
                seniorTotalWinLoseCom: 0,
                shareHolderWinLose: 0,
                shareHolderWinLoseCom: 0,
                shareHolderTotalWinLoseCom: 0,
                companyWinLose: 0,
                companyWinLoseCom: 0,
                companyTotalWinLoseCom: 0,
                superAdminWinLose: 0,
                superAdminWinLoseCom: 0,
                superAdminTotalWinLoseCom: 0
            });


            function transformData(type, item) {

                if (type === 'SHARE_HOLDER') {
                    let newItem = item;
                    newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose;
                    newItem.companyWinLoseCom = item.superAdminWinLoseCom + item.companyWinLoseCom;
                    newItem.companyTotalWinLoseCom = item.superAdminTotalWinLoseCom + item.companyTotalWinLoseCom;
                    return newItem;
                } else if (type === 'SENIOR') {

                    let newItem = item;
                    newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose + item.shareHolderWinLose;
                    newItem.companyWinLoseCom = item.superAdminWinLoseCom + item.companyWinLoseCom + item.shareHolderWinLoseCom;
                    newItem.companyTotalWinLoseCom = item.superAdminTotalWinLoseCom + item.companyTotalWinLoseCom + item.shareHolderTotalWinLoseCom;
                    return newItem;

                } else if (type === 'MASTER_AGENT') {

                    let newItem = item;

                    newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose + item.shareHolderWinLose + item.seniorWinLose;
                    newItem.companyWinLoseCom = item.superAdminWinLoseCom + item.companyWinLoseCom + item.shareHolderWinLoseCom + item.seniorWinLoseCom;
                    newItem.companyTotalWinLoseCom = item.superAdminTotalWinLoseCom + item.companyTotalWinLoseCom + item.shareHolderTotalWinLoseCom + item.seniorTotalWinLoseCom;
                    return newItem;

                } else if (type === 'AGENT') {

                    let newItem = item;

                    newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose + item.shareHolderWinLose + item.seniorWinLose + item.masterAgentWinLose;
                    newItem.companyWinLoseCom = item.superAdminWinLoseCom + item.companyWinLoseCom + item.shareHolderWinLoseCom + item.seniorWinLoseCom + item.masterAgentWinLoseCom;
                    newItem.companyTotalWinLoseCom = item.superAdminTotalWinLoseCom + item.companyTotalWinLoseCom + item.shareHolderTotalWinLoseCom + item.seniorTotalWinLoseCom + item.masterAgentTotalWinLoseCom;
                    return newItem;

                } else {
                    return item;
                }
            }


            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: {
                        dataList: results,
                        summary: totalSummary
                    }
                }
            );
        }

    });

});

router.get('/memberBetDetail', function (req, res) {

    let condition = {};

    let page = req.query.page ? Number.parseInt(req.query.page) : 10;
    let limit = req.query.limit ? Number.parseInt(req.query.limit) : 1;

    let game = req.query.game;
    let source = req.query.source;
    let memberId = req.query.memberId;
    let matchId = req.query.matchId;
    let oddType = req.query.oddType;
    let gameType = req.query.gameType;
    let currency = req.query.currency;


    let userInfo = req.userInfo;


    let group = prepareGroup(userInfo.group.type);

    if (memberId) {
        condition['memberId'] = mongoose.Types.ObjectId(memberId)
    }

    let orCondition = [{'$or': [{'status': 'DONE'}, {'status': 'REJECTED'}, {'status': 'CANCELLED'}]}];

    if (game) {
        let gamesCondition = {
            '$or': game.split(',').map(function (game) {
                if (game === 'STEP') {
                    return {'$and': [{'game': 'FOOTBALL'}, {gameType: 'MIX_STEP'}]}
                } else if (game === 'PARLAY') {
                    return {'$and': [{'game': 'FOOTBALL'}, {gameType: 'PARLAY'}]}
                } else if (game === 'FOOTBALL') {
                    return {'$and': [{'game': 'FOOTBALL'}, {gameType: 'TODAY'}]}
                } else if (game === 'LOTTO') {
                    return {'$and': [{'game': 'LOTTO'}, {'source': 'AMB_LOTTO'}]}
                } else if (game === 'LOTTO_LAOS') {
                    return {'$and': [{'game': 'LOTTO'}, {'source': 'AMB_LOTTO_LAOS'}]}
                } else if (game === 'LOTTO_PP') {
                    return {'$and': [{'game': 'LOTTO'}, {'source': 'AMB_LOTTO_PP'}]}
                } else {
                    return {'game': game};
                }
            })
        };
        orCondition.push(gamesCondition);
    }

    if (source) {
        let gamesCondition = {
            '$or': source.split(',').map(function (source) {
                return {'source': source}
            })
        };
        orCondition.push(gamesCondition);
    }

    if (group) {
        condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(userInfo.group._id);
    }

    if (currency) {
        condition['currency'] = currency;
    }


    if (matchId) {
        orCondition.push({'$or': [{'hdp.matchId': matchId}, {'parlay.matches.0.matchId': matchId}]});
    }


    if (req.query.dateFrom && req.query.dateTo) {
        condition['gameDate'] = {
            "$gte": moment(req.query.dateFrom, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(11).utc(true).toDate(),
            "$lt": moment(req.query.dateTo, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(11).add(1, 'd').utc(true).toDate()
        }
    }

    if (req.query.timeFrom && req.query.timeTo) {
        condition['gameDate'] = {
            "$gte": DateUtils.convertDateTime(req.query.dateFrom, req.query.timeFrom),
            "$lt": DateUtils.convertDateTime(req.query.dateTo, req.query.timeTo)
        }


    }

    if (gameType) {
        condition['gameType'] = gameType;
    }

    if (oddType && gameType === 'TODAY') {
        if (oddType === 'AH') {
            orCondition.push({$or: [{'hdp.oddType': 'AH'}, {'hdp.oddType': 'AH1ST'}]})
        } else if (oddType === 'TTC') {
            orCondition.push({$or: [{'hdp.oddType': 'TTC'}, {'hdp.oddType': 'TTC1ST'}]})
        } else if (oddType === 'OU') {
            orCondition.push({$or: [{'hdp.oddType': 'OU'}, {'hdp.oddType': 'OU1ST'}, {'hdp.oddType': 'T1OU'}, {'hdp.oddType': 'T2OU'}]})
        } else if (oddType === 'OE') {
            orCondition.push({$or: [{'hdp.oddType': 'OE'}, {'hdp.oddType': 'OE1ST'}]})
        } else if (oddType === 'X12') {
            orCondition.push({$or: [{'hdp.oddType': 'X12'}, {'hdp.oddType': 'X121ST'}, {'hdp.oddType': 'ML'}]})
        } else if (oddType === '1STG' || oddType === '2NDG' || oddType === '3RDG' || oddType === '4THG' || oddType === '5THG' || oddType === '6THG'
            || oddType === '7THG' || oddType === '8THG' || oddType === '9THG') {
            orCondition.push({$or: [{'hdp.oddType': oddType}]});
        }
    }

    if (orCondition.length > 0) {
        condition['$and'] = orCondition;
    }


    console.log(condition)
    async.parallel([(callback => {
        AgentGroupModel.findById(userInfo.group._id, (err, response) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, response);
            }
        });

    }), (callback) => {

        BetTransactionModel.count(condition, function (err, data) {
            if (err) {
                callback(err, null);
                return;
            }
            callback(null, data);

        }).lean();
    }], (err, results) => {

        let agentInfo = results[0];
        let count = results[1];
        BetTransactionModel.aggregate([
            {
                $match: condition
            },
            {
                $skip: (page - 1) * limit
            }, {
                $limit: limit
            },
            {
                $sort: {createdDate: -1},
            },
            {
                $project: {
                    "priceType": '$priceType',
                    "hdp": "$hdp",
                    "multi": "$multi",
                    "parlay": "$parlay",
                    "baccarat": "$baccarat",
                    "lotto": "$lotto",
                    "slot": "$slot",
                    "commission": "$commission",
                    "amount": "$amount",
                    "memberCredit": "$memberCredit",
                    "payout": "$payout",
                    "betId": "$betId",
                    "status": "$status",
                    "createdDate": "$createdDate",
                    "member": "$memberId",
                    "game": "$game",
                    "gameDate": "$gameDate",
                    "source": "$source",
                    "gameType": "$gameType",
                    "ipAddress": "$ipAddress",
                    "betResult": "$betResult",
                    "memberCommPercent": '$commission.member.commission',
                    "memberCommission": {$multiply: ['$amount', {$divide: ['$commission.member.commission', 100]}]},
                    "memberCreditUsage": {$abs: '$memberCredit'},
                    "memberWinLose": '$commission.member.winLose',
                    "memberWinLoseCom": '$commission.member.winLoseCom',
                    "memberTotalWinLoseCom": '$commission.member.totalWinLoseCom',

                    "agentWinLose": '$commission.agent.winLose',
                    "agentWinLoseCom": '$commission.agent.winLoseCom',
                    "agentTotalWinLoseCom": '$commission.agent.totalWinLoseCom',

                    "masterAgentWinLose": '$commission.masterAgent.winLose',
                    "masterAgentWinLoseCom": '$commission.masterAgent.winLoseCom',
                    "masterAgentTotalWinLoseCom": '$commission.masterAgent.totalWinLoseCom',

                    "seniorWinLose": '$commission.senior.winLose',
                    "seniorWinLoseCom": '$commission.senior.winLoseCom',
                    "seniorTotalWinLoseCom": '$commission.senior.totalWinLoseCom',

                    "shareHolderWinLose": '$commission.shareHolder.winLose',
                    "shareHolderWinLoseCom": '$commission.shareHolder.winLoseCom',
                    "shareHolderTotalWinLoseCom": '$commission.shareHolder.totalWinLoseCom',

                    "companyWinLose": '$commission.company.winLose',
                    "companyWinLoseCom": '$commission.company.winLoseCom',
                    "companyTotalWinLoseCom": '$commission.company.totalWinLoseCom',

                    "superAdminWinLose": '$commission.superAdmin.winLose',
                    "superAdminWinLoseCom": '$commission.superAdmin.winLoseCom',
                    "superAdminTotalWinLoseCom": '$commission.superAdmin.totalWinLoseCom'
                }
            }

        ]).read('secondaryPreferred').option({maxTimeMS: 100000}).exec((err, results) => {

            if (err)
                return res.send({message: "error", results: err, code: 999});

            async.series([(callback => {

                MemberModel.populate(results, {
                    path: 'member',
                    select: '_id username contact commissionSetting.sportsBook.typeHdpOuOe'
                }, function (err, memberResult) {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, memberResult);
                    }
                });

            })], (err, populateResults) => {

                if (err)
                    return res.send({message: "error", results: err, code: 999});

                results = prepareSexyBaccaratDetail(results);

                const finalResult = transformDataByAgentType(agentInfo.type, results);


                let summary = _.reduce(finalResult, function (memo, obj) {

                    return {
                        amount: memo.amount + obj.amount,

                        memberCredit: memo.memberCredit + obj.memberCredit,

                        memberWinLose: memo.memberWinLose + (obj.status === 'CANCELLED' ? 0 : obj.memberWinLose),
                        memberWinLoseCom: memo.memberWinLoseCom + (obj.status === 'CANCELLED' ? 0 : obj.memberWinLoseCom),
                        memberTotalWinLoseCom: memo.memberTotalWinLoseCom + (obj.status === 'CANCELLED' ? 0 : obj.memberTotalWinLoseCom),

                        agentWinLose: memo.agentWinLose + (obj.status === 'CANCELLED' ? 0 : obj.agentWinLose),
                        agentWinLoseCom: memo.agentWinLoseCom + (obj.status === 'CANCELLED' ? 0 : obj.agentWinLoseCom),
                        agentTotalWinLoseCom: memo.agentTotalWinLoseCom + (obj.status === 'CANCELLED' ? 0 : obj.agentTotalWinLoseCom),

                        companyWinLose: memo.companyWinLose + (obj.status === 'CANCELLED' ? 0 : obj.companyWinLose),
                        companyWinLoseCom: memo.companyWinLoseCom + (obj.status === 'CANCELLED' ? 0 : obj.companyWinLoseCom),
                        companyTotalWinLoseCom: memo.companyTotalWinLoseCom + (obj.status === 'CANCELLED' ? 0 : obj.companyTotalWinLoseCom)
                    }
                }, {
                    amount: 0,
                    memberCredit: 0,
                    memberWinLose: 0,
                    memberWinLoseCom: 0,
                    memberTotalWinLoseCom: 0,
                    agentWinLose: 0,
                    agentWinLoseCom: 0,
                    agentTotalWinLoseCom: 0,
                    companyWinLose: 0,
                    companyWinLoseCom: 0,
                    companyTotalWinLoseCom: 0
                });

                return res.send(
                    {
                        code: 0,
                        message: "success",
                        result: {
                            group: userInfo.group,
                            "total": count,
                            "limit": limit,
                            "page": page,
                            "pages": Math.ceil(count / limit) || 1,
                            dataList: finalResult,
                            summary: summary
                        }
                    }
                );
            });
        });
    });

});

router.get('/statement', function (req, res) {


    let groupId = req.userInfo.group._id;

    let condition = {};
    if (req.query.id) {
        condition._id = req.query.id;
    }

    if (!req.query.type) {
        return res.send({message: "error", result: 'required type', code: 999});
    }


    condition['gameDate'] = {
        "$gte": moment().millisecond(0).second(0).minute(0).hour(11).add(-13, 'days').utc(true).toDate(),
        "$lt": moment().millisecond(0).second(0).minute(0).hour(11).utc(true).toDate()
    };


    async.waterfall([(callback => {


        if (req.query.type === 'AGENT') {
            AgentGroupModel.findById(mongoose.Types.ObjectId(req.query.id), (err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            }).select('balance type');
        } else {
            MemberModel.findById(mongoose.Types.ObjectId(req.query.id), (err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            }).select('balance');
        }

    }), ((agentGroup, callback) => {

        if (req.query.type === 'AGENT') {

            let condition = {};
            let group = '';

            getGroupAndChild(agentGroup.type, (groupRes, childRes) => {
                group = groupRes;
            });

            console.log(agentGroup)


            condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(req.query.id);
            condition['status'] = 'DONE';

            console.log(condition);

            BetTransactionModel.aggregate([
                {
                    $match: condition
                },
                {
                    $sort: {'createdDate': -1}
                },
                {
                    $group: {
                        _id: {
                            date: {
                                $cond: [
                                    // {$and:[
                                    {$gte: [{$hour: "$gameDate"}, 11]},
                                    {$dateToString: {format: "%d/%m/%Y", date: "$gameDate"}},
                                    {
                                        $dateToString: {
                                            format: "%d/%m/%Y",
                                            date: {$subtract: ["$gameDate", 24 * 60 * 60 * 1000]}
                                        }
                                    }
                                ]
                            }
                        },
                        balance: {$sum: '$commission.' + group + '.totalWinLoseCom'},
                        commission: {$sum: '$commission.' + group + '.winLoseCom'},
                        wl: {$sum: '$commission.' + group + '.winLose'},
                        stake: {
                            $sum: {
                                $cond: [{$eq: ['$gameType', 'PARLAY']}, {$multiply: ['$amount', {$abs: '$parlay.odds'}]}, {$multiply: ['$amount', {$abs: '$hdp.odd'}]}]
                            }
                        },
                    }
                },
                {
                    $project: {
                        _id: 0,
                        date: '$_id.date',
                        balance: '$balance',
                        wl: '$wl',
                        commission: '$commission',
                        stake: '$stake',
                    }
                }
            ], (err, statementResponse) => {
                if (err) {
                    callback(err, null);
                } else {

                    let dateFromString = moment().add(-13, 'days').format('DD/MM/YYYY');
                    let dateToString = moment().add(0, 'days').format('DD/MM/YYYY');

                    MemberService.findMemberOrAgentPaymentHistory(mongoose.Types.ObjectId(req.query.id), dateFromString, dateToString, function (err, paymentHistoryResponse) {

                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, agentGroup, statementResponse, paymentHistoryResponse);
                        }
                    });
                }
            });
        } else {
            let condition = {};
            condition['memberId'] = mongoose.Types.ObjectId(req.query.id);
            condition['status'] = 'DONE';


            console.log(condition)

            BetTransactionModel.aggregate([
                {
                    $match: condition
                },
                {
                    $sort: {'createdDate': -1}
                },
                {
                    $group: {
                        _id: {
                            date: {
                                $cond: [
                                    // {$and:[
                                    {$gte: [{$hour: "$gameDate"}, 11]},
                                    {$dateToString: {format: "%d/%m/%Y", date: "$gameDate"}},
                                    {
                                        $dateToString: {
                                            format: "%d/%m/%Y",
                                            date: {$subtract: ["$gameDate", 24 * 60 * 60 * 1000]}
                                        }
                                    }
                                ]
                            }
                        },
                        balance: {$sum: '$commission.member.totalWinLoseCom'},
                        commission: {$sum: '$commission.member.winLoseCom'},
                        wl: {$sum: '$commission.member.winLose'},
                        stake: {
                            $sum: {
                                $cond: [{$eq: ['$gameType', 'PARLAY']}, {$multiply: ['$amount', {$abs: '$parlay.odds'}]}, {$multiply: ['$amount', {$abs: '$hdp.odd'}]}]
                            }
                        },
                    }
                },
                {
                    $project: {
                        _id: 0,
                        date: '$_id.date',
                        balance: '$balance',
                        wl: '$wl',
                        commission: '$commission',
                        stake: '$stake',
                    }
                }
            ], (err, statementResponse) => {
                if (err) {
                    callback(err, null);
                } else {
                    let dateFromString = moment().add(-13, 'days').format('DD/MM/YYYY');
                    let dateToString = moment().add(0, 'days').format('DD/MM/YYYY');
                    MemberService.findMemberOrAgentPaymentHistory(mongoose.Types.ObjectId(req.query.id), dateFromString, dateToString, function (err, paymentHistoryResponse) {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, agentGroup, statementResponse, paymentHistoryResponse);
                        }
                    });
                }
            });
        }

    })], function (err, agentGroup, statementList, paymentHistory) {


        if (err) {
            return res.send({message: "error", result: err, code: 999});
        }

        // console.log(statementList);
        // console.log('zz ',paymentHistory);


        let dateList = DateUtils.enumerateNextDaysBetweenDates(moment().add(-13, 'days'), moment().add(0, 'days'), 'DD/MM/YYYY');


        let prepareStatement = [];


        async.each(dateList, function (dateStr, callback) {

            let statements = _.filter(statementList, function (payItem) {
                return payItem.date === dateStr;
            });

            if (statements.length > 0) {
                prepareStatement.push(statements[0]);
                callback()
            } else {
                prepareStatement.push({
                    settled: 0,
                    balance: 0,
                    wl: 0,
                    commission: 0,
                    date: dateStr,
                    stake: 0
                });
                callback()
            }

        }, (err) => {


            async.forEachOf(prepareStatement, (item, index, callback) => {

                if (index > 0 && (prepareStatement.length - 1) !== index) {
                    //is middle
                    item.balance = item.balance + prepareStatement[index - 1].balance;
                } else {
                    //is last
                    if (index === prepareStatement.length - 1) {
                        //is last
                        item.balance = (item.balance) + (prepareStatement[index - 1].balance);
                    } else {
                        //is first
                        item.balance = (item.balance);

                    }
                }

                let settled = _.filter(paymentHistory, function (payItem) {
                    return payItem.date === moment(item.date, "DD/MM/YYYY").format('DD/MM/YYYY');
                });

                if (settled.length > 0) {
                    item.settled = settled[0].wl;
                    item.balance = item.balance + item.settled;
                } else {
                    item.settled = 0;
                }

                callback();
            }, (err) => {
                if (err) {
                    return res.send({message: "error", result: err, code: 999});
                } else {

                    // _.sortBy(prepareStatement, function(o) { return o.date; })
                    return res.send({
                        code: 0,
                        message: "success",
                        result: {
                            list: _.chain(prepareStatement).reverse().value(),
                        }
                    });
                }
            });//end forEach Statement
        });


    });
});

router.get('/statement/payment', function (req, res) {

    let condition = {};

    if (!req.query.type) {
        return res.send({message: "error", result: 'required type', code: 999});
    }


    if (req.query.type === 'AGENT') {
        condition['agent'] = mongoose.Types.ObjectId(req.query.id);
    } else {
        condition['member'] = mongoose.Types.ObjectId(req.query.id);
    }

    if (req.query.date) {

        condition['createdDate'] = {
            "$gte": moment(req.query.date, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(11).utc(true).toDate(),
            "$lt": moment(req.query.date, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(11).add(1, 'days').utc(true).toDate()
        }
    }


    PaymentHistoryModel.find(condition,
        function (err, response) {
            if (err)
                return res.send({message: "error", results: err, code: 999});

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: {
                        list: response
                    }
                }
            );
        });

});

router.get('/memberLogin', function (req, res) {

    const userInfo = req.userInfo;

    let condition = {};

    // condition.

    async.series([callback => {
        AgentGroupModel.findById(userInfo.group._id, (err, response) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, response);
            }
        }).select('childGroups childMembers');

    }], function (err, asyncResponse) {
        if (err)
            return res.send({message: "error", results: err, code: 999});

        let childGroups = asyncResponse[0].childGroups;
        let childMembers = asyncResponse[0].childMembers;


        async.parallel([callback => {

            MemberModel.find({_id: {$in: childMembers}})
                .select('username ipAddress lastLogin')
                .lean()
                .exec(function (err, response) {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, response);
                    }
                });


        }, callback => {
            let condition = {'group': {$in: childGroups}};
            UserModel.find(condition)
                .select('username ipAddress lastLogin')
                // .sort({'username':-1})
                .lean()
                .exec(function (err, response) {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, response);
                    }
                });


        }], function (err, memberLoginResponse) {
            if (err)
                return res.send({message: "error", results: err, code: 999});

            let lastLoginList = memberLoginResponse[0].concat(memberLoginResponse[1]);


            lastLoginList = lastLoginList.sort(function (a, b) {
                return naturalSort(a.username, b.username);
            });

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: {
                        lastLoginList: lastLoginList
                    }
                }
            );
        });

    });

});

function findAllSubMember(groupId, listOfAgent, callback) {
    console.log('find : ', groupId);

    AgentGroupModel.findOne({_id: mongoose.Types.ObjectId(groupId)})
        .populate({
            path: 'childGroups',
            select: 'childGroups childMembers',
            populate: [{
                path: 'childGroups',
                model: 'AgentGroup',
                select: 'childGroups childMembers'
            }],
        })
        .select('childGroups childMembers')
        .exec((err, response) => {

            listOfAgent.push({id: response._id, members: response.childMembers});

            async.each(response.childGroups, (child, callback1) => {

                listOfAgent.push({id: child._id, members: child.childMembers});

                async.each(child.childGroups, (child2, callback2) => {

                    listOfAgent.push({id: child2._id, members: child2.childMembers});

                    findAllSubMember(child2._id, listOfAgent, (err, response) => {
                        callback2(null, true);
                    });

                }, (err) => {
                    callback1(null, true);
                });
            }, (err) => {
                callback(null, listOfAgent);
            });
        });
}

router.get('/robot', function (req, res) {


    let groupId = req.query.groupId;
    // let userInfo = req.userInfo.type;

    async.waterfall([(callback => {
        AgentGroupModel.findById(groupId, (err, response) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, response);
            }
        }).select('type parentId')

    })], function (err, groupInfo, winLoseResponse) {

        if (err) {
            return res.send({message: "error", result: err, code: 999});
        }
        let agentGroup = groupInfo;

        let group = '';

        if (agentGroup.type === 'SUPER_ADMIN') {
            group = 'superAdmin';
        } else if (agentGroup.type === 'COMPANY') {
            group = 'company';
        } else if (agentGroup.type === 'SHARE_HOLDER') {
            group = 'shareHolder';
        } else if (agentGroup.type === 'SENIOR') {
            group = "senior";
        } else if (agentGroup.type === 'MASTER_AGENT') {
            group = "masterAgent";
        } else if (agentGroup.type === 'AGENT') {
            group = "agent";
        }

        let condition = {};


        if (req.query.dateFrom && req.query.dateTo) {
            condition['gameDate'] = {
                "$gte": moment(req.query.dateFrom, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(11).utc(true).toDate(),
                "$lt": moment(req.query.dateTo, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(11).add(1, 'd').utc(true).toDate()
            }
        }

        condition['memberParentGroup'] = {$ne: mongoose.Types.ObjectId(groupId)};
        condition['$and'] = [
            {['commission.' + group + '.group']: mongoose.Types.ObjectId(groupId)},
        ];
        condition['game'] = 'FOOTBALL';
        condition['hdp.isRobot'] = true;


        BetTransactionModel.aggregate([
            {
                $match: condition
            },
            {
                "$group": {
                    "_id": {
                        member: '$memberId',
                        date: {
                            $cond: [
                                // {$and:[
                                {$gte: [{$hour: "$gameDate"}, 11]},
                                {$dateToString: {format: "%d/%m/%Y", date: "$gameDate"}},
                                {
                                    $dateToString: {
                                        format: "%d/%m/%Y",
                                        date: {$subtract: ["$gameDate", 24 * 60 * 60 * 1000]}
                                    }
                                }
                            ]
                        }
                    },
                    "stackCount": {$sum: 1},
                    "amount": {$sum: '$amount'},
                }
            },
            {
                $project: {
                    _id: 0,
                    type: 'M_GROUP',
                    member: '$_id.member',
                    stackCount: '$stackCount',
                    amount: '$amount',
                    date: '$_id.date'
                }
            }], (err, response) => {

            if (err)
                return res.send({message: "error", results: err, code: 999});

            let mergeDataList = response;

            async.series([(callback => {

                MemberModel.populate(mergeDataList, {
                    path: 'member',
                    select: '_id username username_lower contact isAutoChangeOdd'
                }, function (err, memberResult) {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, memberResult);
                    }
                });


            })], (err, populateResults) => {

                if (err)
                    return res.send({message: "error", results: err, code: 999});


                let totalTransaction = _.chain(mergeDataList)

                    .groupBy((value) => {
                        return value.member
                    })
                    .map((value, key) => {

                        return _.reduce(value, function (memo, obj) {
                            return {
                                member: obj.member,
                                amount: memo.amount + obj.amount,
                                stackCount: memo.stackCount + obj.stackCount

                            }
                        }, {
                            amount: 0,
                            stackCount: 0
                        });

                    }).value();


                let today;
                if (moment().hours() >= 11) {
                    today = moment().format('DD/MM/YYYY');
                } else {
                    today = moment().add(-1, 'd').format('DD/MM/YYYY');
                }

                let todayTransaction = _.filter(mergeDataList, (item) => {
                    return item.date === today;
                });

                let finalTransaction = _.map(totalTransaction, (item) => {

                    let newItem = Object.assign({}, item);

                    let itemToday = _.filter(todayTransaction, (tItem) => {
                        return item.member._id.toString() === tItem.member._id.toString();
                    })[0];

                    if (itemToday) {
                        newItem.today_stackCount = itemToday.stackCount;
                        newItem.today_amount = itemToday.amount;
                    } else {
                        newItem.today_stackCount = 0;
                        newItem.today_amount = 0;
                    }
                    return newItem;

                });


                // finalTransaction = finalTransaction.sort(function (a, b) {
                //     return naturalSort(a.member.username, b.member.username);
                // });


                finalTransaction = finalTransaction.sort(function (a, b) {
                    return b.stackCount - a.stackCount;
                });

                let summary = _.reduce(finalTransaction, function (memo, obj) {

                    return {
                        today_amount: memo.today_amount + obj.today_amount,
                        today_stackCount: memo.today_stackCount + obj.today_stackCount,
                        amount: memo.amount + obj.amount,
                        stackCount: memo.stackCount + obj.stackCount

                    }
                }, {
                    today_amount: 0,
                    today_stackCount: 0,
                    amount: 0,
                    stackCount: 0
                });


                return res.send(
                    {
                        code: 0,
                        message: "success",
                        result: {
                            group: agentGroup,
                            dataList: finalTransaction,
                            summary: summary,
                        }
                    }
                );

            });
        });
    });

});

router.get('/robot_details', function (req, res) {


    let page = req.query.page ? Number.parseInt(req.query.page) : 10;
    let limit = req.query.limit ? Number.parseInt(req.query.limit) : 1;

    let condition = {};

    let userInfo = req.userInfo;
    let memberId = req.query.memberId;
    let gameType = req.query.gameType;
    let game = req.query.game;
    let type = req.query.type;

    console.log(type)

    if (req.query.dateFrom && req.query.dateTo) {
        condition['gameDate'] = {
            "$gte": moment(req.query.dateFrom, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(11).utc(true).toDate(),
            "$lt": moment(req.query.dateTo, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(11).add(1, 'd').utc(true).toDate()
        }
    }


    condition['memberId'] = memberId;
    condition['hdp.isRobot'] = true;
    if (gameType) {
        condition['gameType'] = gameType;
    }

    if (game) {
        condition['game'] = game;
    }

    if (type === 'TODAY') {
        let today;
        if (moment().hours() >= 11) {
            today = moment().format('DD/MM/YYYY');
        } else {
            today = moment().add(-1, 'd').format('DD/MM/YYYY');
        }
        condition['gameDate'] = {
            "$gte": moment(today, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(11).utc(true).toDate(),
            "$lt": moment(today, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(11).add(1, 'd').utc(true).toDate()
        }
    }


    async.waterfall([(callback => {
        MemberModel.findById(memberId, (err, response) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, response);
            }
        }).select('group');

    }), (memberInfo, callback) => {

        AgentGroupModel.findById(memberInfo.group).select('_id type parentId name').populate({
            path: 'parentId',
            select: '_id type parentId name',
            populate: {
                path: 'parentId',
                select: '_id type parentId name',
                populate: {
                    path: 'parentId',
                    select: '_id type parentId name',
                    populate: {
                        path: 'parentId',
                        select: '_id type parentId name',
                        populate: {
                            path: 'parentId',
                            select: '_id type parentId name',
                            populate: {
                                path: 'parentId',
                                select: '_id type parentId name',
                            }
                        }
                    }
                }
            }

        }).exec(function (err, response) {
            if (err)
                callback(err, null);
            else
                callback(null, memberInfo, response);
        });
    }], (err, memberInfo, agents) => {


        BetTransactionModel.paginate(condition, {
            select: "priceType hdp parlay commission amount payout betId createdDate memberId gameType status game source baccarat gameDate ipAddress",
            lean: true,
            page: page,
            limit: limit,
            read: {
                pref: 'secondaryPreferred'
            },
            populate: {path: 'memberId', select: '_id username'}
        }, function (err, data) {


            if (err)
                return res.send({message: "error", results: err, code: 999});

            let agentList = [];
            prepareAgentList(agents, agentList);
            agentList.reverse();

            let response = prepareSexyBaccaratDetail(data.docs);

            let summary = _.reduce(response, function (memo, obj) {

                return {
                    amount: memo.amount + obj.amount,

                    memberWinLose: memo.memberWinLose + _.defaults(obj.commission.member, {winLose: 0}).winLose,
                    memberWinLoseCom: memo.memberWinLoseCom + _.defaults(obj.commission.member, {winLoseCom: 0}).winLoseCom,
                    memberTotalWinLoseCom: memo.memberTotalWinLoseCom + _.defaults(obj.commission.member, {totalWinLoseCom: 0}).totalWinLoseCom,

                    agentWinLose: memo.agentWinLose + _.defaults(obj.commission.agent, {winLose: 0}).winLose,
                    agentWinLoseCom: memo.agentWinLoseCom + _.defaults(obj.commission.agent, {winLoseCom: 0}).winLoseCom,
                    agentTotalWinLoseCom: memo.agentTotalWinLoseCom + _.defaults(obj.commission.agent, {totalWinLoseCom: 0}).totalWinLoseCom,

                    masterAgentWinLose: memo.masterAgentWinLose + _.defaults(obj.commission.masterAgent, {winLose: 0}).winLose,
                    masterAgentWinLoseCom: memo.masterAgentWinLoseCom + _.defaults(obj.commission.masterAgent, {winLoseCom: 0}).winLoseCom,
                    masterAgentTotalWinLoseCom: memo.masterAgentTotalWinLoseCom + _.defaults(obj.commission.masterAgent, {totalWinLoseCom: 0}).totalWinLoseCom,

                    seniorWinLose: memo.seniorWinLose + _.defaults(obj.commission.senior, {winLose: 0}).winLose,
                    seniorWinLoseCom: memo.seniorWinLoseCom + _.defaults(obj.commission.senior, {winLoseCom: 0}).winLoseCom,
                    seniorTotalWinLoseCom: memo.seniorTotalWinLoseCom + _.defaults(obj.commission.senior, {totalWinLoseCom: 0}).totalWinLoseCom,

                    shareHolderWinLose: memo.shareHolderWinLose + _.defaults(obj.commission.shareHolder, {winLose: 0}).winLose,
                    shareHolderWinLoseCom: memo.shareHolderWinLoseCom + _.defaults(obj.commission.shareHolder, {winLoseCom: 0}).winLoseCom,
                    shareHolderTotalWinLoseCom: memo.shareHolderTotalWinLoseCom + _.defaults(obj.commission.shareHolder, {totalWinLoseCom: 0}).totalWinLoseCom,

                    companyWinLose: memo.companyWinLose + _.defaults(obj.commission.company, {winLose: 0}).winLose,
                    companyWinLoseCom: memo.companyWinLoseCom + _.defaults(obj.commission.company, {winLoseCom: 0}).winLoseCom,
                    companyTotalWinLoseCom: memo.companyTotalWinLoseCom + _.defaults(obj.commission.company, {totalWinLoseCom: 0}).totalWinLoseCom,

                    superAdminWinLose: memo.superAdminWinLose + _.defaults(obj.commission.superAdmin, {winLose: 0}).winLose,
                    superAdminWinLoseCom: memo.superAdminWinLoseCom + _.defaults(obj.commission.superAdmin, {winLoseCom: 0}).winLoseCom,
                    superAdminTotalWinLoseCom: memo.superAdminTotalWinLoseCom + _.defaults(obj.commission.superAdmin, {totalWinLoseCom: 0}).totalWinLoseCom,
                }
            }, {
                amount: 0,
                memberWinLose: 0,
                memberWinLoseCom: 0,
                memberTotalWinLoseCom: 0,
                agentWinLose: 0,
                agentWinLoseCom: 0,
                agentTotalWinLoseCom: 0,
                masterAgentWinLose: 0,
                masterAgentWinLoseCom: 0,
                masterAgentTotalWinLoseCom: 0,
                seniorWinLose: 0,
                seniorWinLoseCom: 0,
                seniorTotalWinLoseCom: 0,
                shareHolderWinLose: 0,
                shareHolderWinLoseCom: 0,
                shareHolderTotalWinLoseCom: 0,
                companyWinLose: 0,
                companyWinLoseCom: 0,
                companyTotalWinLoseCom: 0,
                superAdminWinLose: 0,
                superAdminWinLoseCom: 0,
                superAdminTotalWinLoseCom: 0,
            });

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: {
                        group: memberInfo.group,
                        dataList: response,
                        agents: agentList,
                        summary: summary,
                        total: data.totalDocs,
                        limit: data.limit,
                        page: data.page,
                        pages: data.totalPages,
                    }
                }
            );
        });

    });

});

router.get('/cancel-list', function (req, res) {

    let condition = {};

    let userInfo = req.userInfo;

    if (req.query.dateFrom && req.query.dateTo) {
        condition['gameDate'] = {
            "$gte": moment(req.query.dateFrom, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(11).utc(true).toDate(),
            "$lt": moment(req.query.dateTo, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(11).add(1, 'd').utc(true).toDate()
        }
    }


    let group = prepareGroup(userInfo.group.type)


    condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(userInfo.group._id);
    if (req.query.status) {
        condition.status = req.query.status;
    }

    if (req.query.betId) {
        condition.betId = req.query.betId;
    }

    condition['game'] = 'FOOTBALL';


    console.log('condition : ', condition)


    let page = req.query.page ? Number.parseInt(req.query.page) : 10;
    let limit = req.query.limit ? Number.parseInt(req.query.limit) : 1;


    BetTransactionModel.paginate(condition, {
        select: "memberId hdp parlay amount payout status betId createdDate priceType gameType ipAddress cancelByType cancelByAgent cancelDate commission",
        lean: true,
        page: page,
        limit: limit,
        read: {
            pref: 'secondaryPreferred'
        },
        populate: [
            {path: 'memberId', select: '_id username'},
            {
                path: 'cancelByAgent',
                select: 'username group',
                populate: {path: 'group', select: 'type'}
            }]
    }, (err, data) => {

        if (err)
            return res.send({message: "error", results: err, code: 999});


        let results = data.docs;
        _.forEach(results, (item) => {
            item.stake_in = item.amount;
            item.stake_out = Math.abs(item.memberCredit);
            item.totalReceive = item.commission[group].amount;
        });

        let summary = _.reduce(results, function (memo, num) {
            return {
                totalAmount: memo.totalAmount + num.amount,
                totalReceive: memo.totalReceive + num.totalReceive
            }
        }, {totalAmount: 0, totalReceive: 0});

        return res.send(
            {
                code: 0,
                message: "success",
                result: {
                    betList: results,
                    summary: summary,
                    total: data.totalDocs,
                    limit: data.limit,
                    page: data.page,
                    pages: data.totalPages,
                }
            }
        );
    });

});

function prepareAgentList(agents, output) {
    if (agents && agents.parentId) {
        output.push(agents)
        prepareAgentList(agents.parentId, output);
    }
}

function transformDataByAgentType(type, dataList) {
    if (type === 'COMPANY') {
        return _.map(dataList, function (item) {

            let newItem = Object.assign({}, item);

            newItem.companyWinLose = item.superAdminWinLose;
            newItem.companyWinLoseCom = item.superAdminWinLoseCom;
            newItem.companyTotalWinLoseCom = item.superAdminTotalWinLoseCom;

            newItem.agentWinLose = item.companyWinLose;
            newItem.agentWinLoseCom = item.companyWinLoseCom;
            newItem.agentTotalWinLoseCom = item.companyTotalWinLoseCom;


            newItem.memberWinLose = (item.memberWinLose + item.masterAgentWinLose + item.agentWinLose + item.seniorWinLose + item.shareHolderWinLose);
            newItem.memberWinLoseCom = (item.memberWinLoseCom + item.masterAgentWinLoseCom + item.agentWinLoseCom + item.seniorWinLoseCom + item.shareHolderWinLoseCom);
            newItem.memberTotalWinLoseCom = (item.memberTotalWinLoseCom + item.masterAgentTotalWinLoseCom + item.agentTotalWinLoseCom + item.seniorTotalWinLoseCom + item.shareHolderTotalWinLoseCom);

            return newItem;
        });
    } else if (type === 'SHARE_HOLDER') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose;
            newItem.companyWinLoseCom = item.superAdminWinLoseCom + item.companyWinLoseCom;
            newItem.companyTotalWinLoseCom = item.superAdminTotalWinLoseCom + item.companyTotalWinLoseCom;

            newItem.agentWinLose = item.shareHolderWinLose;
            newItem.agentWinLoseCom = item.shareHolderWinLoseCom;
            newItem.agentTotalWinLoseCom = item.shareHolderTotalWinLoseCom;

            newItem.memberWinLose = (item.memberWinLose + item.masterAgentWinLose + item.agentWinLose + item.seniorWinLose);
            newItem.memberWinLoseCom = (item.memberWinLoseCom + item.masterAgentWinLoseCom + item.agentWinLoseCom + item.seniorWinLoseCom);
            newItem.memberTotalWinLoseCom = (item.memberTotalWinLoseCom + item.masterAgentTotalWinLoseCom + item.agentTotalWinLoseCom + item.seniorTotalWinLoseCom);
            return newItem;
        });
    } else if (type === 'SENIOR') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose + item.shareHolderWinLose;
            newItem.companyWinLoseCom = item.superAdminWinLoseCom + item.companyWinLoseCom + item.shareHolderWinLoseCom;
            newItem.companyTotalWinLoseCom = item.superAdminTotalWinLoseCom + item.companyTotalWinLoseCom + item.shareHolderTotalWinLoseCom;

            newItem.agentWinLose = item.seniorWinLose;
            newItem.agentWinLoseCom = item.seniorWinLoseCom;
            newItem.agentTotalWinLoseCom = item.seniorTotalWinLoseCom;


            newItem.memberWinLose = (item.memberWinLose + item.masterAgentWinLose + item.agentWinLose);
            newItem.memberWinLoseCom = (item.memberWinLoseCom + item.masterAgentWinLoseCom + item.agentWinLoseCom);
            newItem.memberTotalWinLoseCom = (item.memberTotalWinLoseCom + item.masterAgentTotalWinLoseCom + item.agentTotalWinLoseCom);

            return newItem;
        });
    } else if (type === 'MASTER_AGENT') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose + item.shareHolderWinLose + item.seniorWinLose;
            newItem.companyWinLoseCom = item.superAdminWinLoseCom + item.companyWinLoseCom + item.shareHolderWinLoseCom + item.seniorWinLoseCom;
            newItem.companyTotalWinLoseCom = item.superAdminTotalWinLoseCom + item.companyTotalWinLoseCom + item.shareHolderTotalWinLoseCom + item.seniorTotalWinLoseCom;

            newItem.agentWinLose = item.masterAgentWinLose;
            newItem.agentWinLoseCom = item.masterAgentWinLoseCom;
            newItem.agentTotalWinLoseCom = item.masterAgentTotalWinLoseCom;

            newItem.memberWinLose = roundTo(item.memberWinLose + item.agentWinLose, 3);
            newItem.memberWinLoseCom = roundTo(item.memberWinLoseCom + item.agentWinLoseCom, 3);
            newItem.memberTotalWinLoseCom = roundTo(item.memberTotalWinLoseCom + item.agentTotalWinLoseCom, 3);

            return newItem;
        });
    } else if (type === 'AGENT') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);
            newItem.companyWinLose = roundTo(item.superAdminWinLose + item.companyWinLose + item.shareHolderWinLose + item.seniorWinLose + item.masterAgentWinLose, 3);
            newItem.companyWinLoseCom = roundTo(item.superAdminWinLoseCom + item.companyWinLoseCom + item.shareHolderWinLoseCom + item.seniorWinLoseCom + item.masterAgentWinLoseCom, 3);
            newItem.companyTotalWinLoseCom = roundTo(item.superAdminTotalWinLoseCom + item.companyTotalWinLoseCom + item.shareHolderTotalWinLoseCom + item.seniorTotalWinLoseCom + item.masterAgentTotalWinLoseCom, 3);

            newItem.agentWinLose = item.agentWinLose;
            newItem.agentWinLoseCom = item.agentWinLoseCom;
            newItem.agentTotalWinLoseCom = item.agentTotalWinLoseCom;

            newItem.memberWinLose = item.memberWinLose;
            newItem.memberWinLoseCom = item.memberWinLoseCom;
            newItem.memberTotalWinLoseCom = item.memberTotalWinLoseCom;
            return newItem;
        });
    } else {
        return dataList;
    }
}


router.get('/reportX', function (req, res) {


    let agentName = req.query.agentName;
    let dateFrom = req.query.dateFrom; // DD/MM/YYYY
    let dateTo = req.query.dateTo; // DD/MM/YYYY
    let timeFrom = req.query.timeFrom; //HH:mm:ss
    let timeTo = req.query.timeTo; //HH:mm:ss
    let game = req.query.game; //FOOTBALL,STEP,PARLAY,CASINO,GAME,LOTTO,M2 (Optional)

    if (!agentName) {
        return res.send({message: "agentId not found", code: 888});
    }
    async.waterfall([(callback => {

        AgentGroupModel.findOne({name_lower: agentName.toLowerCase()}, (err, response) => {
            if (err) {
                callback(err, null);
            } else {
                if (!response) {
                    callback(888, null);
                } else {
                    callback(null, response);
                }
            }
        }).select('_id type parentId')

    }), (groupInfo, callback) => {

        const agentGroup = groupInfo;

        if (agentGroup.type === 'SENIOR' || agentGroup.type === 'MASTER_AGENT' || agentGroup.type === 'AGENT') {


            ReportService.getWinLoseReportMember(agentGroup._id, agentGroup.type, dateFrom, dateTo, timeFrom, timeTo, game, (err, wlResponse) => {
                if (err) {
                    callback(err, null);
                } else {

                    MemberModel.populate(wlResponse, {
                        path: 'member',
                        select: '_id username username_lower contact'
                    }, function (err, memberResult) {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, agentGroup, memberResult);
                        }
                    });
                }
            });
        } else {
            callback(null, agentGroup, []);
        }

    }], (err, agentGroup, transaction) => {

        if (err) {
            if (err == 888) {
                return res.send({message: "agentId not found", code: 888});
            }
            return res.send({message: "error", result: err, code: 999});
        }

        let finalResult = transformDataByAgentType(agentGroup.type, transaction);


        finalResult = finalResult.sort(function (a, b) {
            let aKey = a.type === 'M_GROUP' ? a.member.username : a.group.name;
            let bKey = b.type === 'M_GROUP' ? b.member.username : b.group.name;
            return naturalSort(aKey, bKey);
        });

        // //TODO refacfor
        // if (req.query.username) {
        //     finalResult = _.filter(finalResult, (item) => {
        //         return (item.type === 'M_GROUP' ? item.member.username : item.group.name).toLowerCase() === req.query.username.toLowerCase();
        //     })
        // }

        let summary = _.reduce(finalResult, function (memo, obj) {

            return {
                amount: memo.amount + obj.amount,
                validAmount: memo.validAmount + obj.validAmount,
                stackCount: memo.stackCount + obj.stackCount,
                grossComm: memo.grossComm + obj.grossComm,
                memberWinLose: memo.memberWinLose + obj.memberWinLose,
                memberWinLoseCom: memo.memberWinLoseCom + obj.memberWinLoseCom,
                memberTotalWinLoseCom: memo.memberTotalWinLoseCom + obj.memberTotalWinLoseCom,

                agentWinLose: memo.agentWinLose + obj.agentWinLose,
                agentWinLoseCom: memo.agentWinLoseCom + obj.agentWinLoseCom,
                agentTotalWinLoseCom: memo.agentTotalWinLoseCom + obj.agentTotalWinLoseCom,

                companyWinLose: memo.companyWinLose + obj.companyWinLose,
                companyWinLoseCom: memo.companyWinLoseCom + obj.companyWinLoseCom,
                companyTotalWinLoseCom: roundTo(memo.companyTotalWinLoseCom + obj.companyTotalWinLoseCom, 3),

            }
        }, {
            amount: 0,
            validAmount: 0,
            stackCount: 0,
            grossComm: 0,
            memberWinLose: 0,
            memberWinLoseCom: 0,
            memberTotalWinLoseCom: 0,
            agentWinLose: 0,
            agentWinLoseCom: 0,
            agentTotalWinLoseCom: 0,
            companyWinLose: 0,
            companyWinLoseCom: 0,
            companyTotalWinLoseCom: 0,
        });


        return res.send(
            {
                code: 0,
                message: "success",
                result: {
                    dataList: finalResult,
                    summary: summary,
                }
            }
        );

    });

});

router.get('/reportXY', function (req, res) {


    let username = req.query.username;
    let dateFrom = req.query.dateFrom; // DD/MM/YYYY
    let dateTo = req.query.dateTo; // DD/MM/YYYY
    let timeFrom = req.query.timeFrom; //HH:mm:ss
    let timeTo = req.query.timeTo; //HH:mm:ss

    if (!username) {
        return res.send({message: "member not found", code: 888});
    }
    async.waterfall([(callback => {

        MemberModel.findOne({username_lower: username.toLowerCase()}, (err, response) => {
            if (err) {
                callback(err, null);
            } else {
                if (!response) {
                    callback(888, null);
                } else {
                    callback(null, response);
                }
            }
        }).select('_id type parentId')

    }), (memberInfo, callback) => {

        let from = moment(dateFrom, 'DD/MM/YYYY').second(timeFrom.split(':')[2]).minute(timeFrom.split(':')[1]).hour(timeFrom.split(':')[0]).format('YYYY-MM-DDTHH:mm:ss');
        let to = moment(dateTo, 'DD/MM/YYYY').second(timeTo.split(':')[2]).minute(timeTo.split(':')[1]).hour(timeTo.split(':')[0]).format('YYYY-MM-DDTHH:mm:ss');

        let condition = {
            'gameDate': {
                "$gte": from,
                "$lt": to,
            },
            'memberId': mongoose.Types.ObjectId(memberInfo._id),
            'status': 'DONE'
        };

        BetTransactionModel.aggregate([
            {
                $match: condition
            },
            {
                "$group": {
                    "_id": {
                        member: '$memberId',
                    },
                    "memberTotalWinLoseCom": {$sum: '$commission.member.totalWinLoseCom'},
                }
            },
            {
                $project: {
                    _id: 0,
                    memberTotalWinLoseCom: '$memberTotalWinLoseCom',
                }
            }
        ], (err, results) => {
            callback(null, memberInfo, results[0] ? results[0].memberTotalWinLoseCom : 0);
        });

    }], (err, agentGroup, memberWinLoss) => {

        if (err) {
            if (err == 888) {
                return res.send({message: "member not found", code: 888});
            }
            return res.send({message: "error", result: err, code: 999});
        }

        console.log(memberWinLoss)

        return res.send(
            {
                code: 0,
                message: "success",
                result: {
                    username: username,
                    winLoss: memberWinLoss
                }
            }
        );

    });

});


// <-------------------// Update Balance--------------->

router.get('/update', function (req, res) {
    console.log('111');

    let page = req.query.page ? Number.parseInt(req.query.page) : 1;
    let limit = req.query.limit ? Number.parseInt(req.query.limit) : 100;
    let condition = {};
    let userInfo = req.query.username;
    let betId = req.query.betId;
    console.log('user', userInfo);
    // let userInfo = 'show3'
    // let userInfo ='comnpay'

    if (req.query.betId) {
        condition['betId'] = req.query.betId;

    }
    if (req.query.dateFrom) {
        condition['createdDate'] = {
            "$gte": new Date(moment(req.query.dateFrom, "DD/MM/YYYY").add(0, 'days').format('YYYY-MM-DDT11:00:00.000')),
            "$lt": moment(req.query.dateTo, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(11).add(1, 'd').utc(true).toDate()
        }
    }
    if (userInfo) {
        async.series(
            [(callback => {
                MemberModel.findOne({username_lower: userInfo})
                    .populate({path: 'group', select: '_id type name endpoint secretKey'})
                    .exec(function (err, response) {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, response);
                        }
                        console.log(response, '111')
                    });
            }),
                (callback => {
                    let data = '11'
                    callback(null, data);
                    console.log(data, '2222')
                })
            ], (err, results) => {
                console.log(results, '3333');

                let memberInfo = results[0]._id;
                let memberUsername = results[0].username_lower;
                condition['memberId'] = memberInfo;
                console.log(condition, 'condition');


                UpdateBalanceModel.paginate(condition, {
                        page: page,
                        limit: limit
                    }
                    // .populate({path: 'group'})
                    , (err, data) => {
                        console.log(data, '123');
                        if (err) {
                            return {message: "error", result: err, code: 999}
                        }
                        else {
                            let dataList = _.map(data.docs, (item) => {
                                // console.log(item, 'item')
                                return {
                                    memberId: item.memberId,
                                    betId: item.betId,
                                    description: item.description,
                                    status: item.status,
                                    createdDate: item.createdDate,
                                    beforeCredit: item.beforeCredit,
                                    amount: item.amount,
                                    username_lower: memberUsername

                                };
                            });


                            // return data = dataList
                            //
                            return res.send(
                                {
                                    code: 0,
                                    message: "success",
                                    result: {
                                        dataList: dataList,
                                        page: data.page,
                                        pages: data.pages,
                                        total: data.total,
                                        limit: data.limit
                                    },
                                }
                            );
                        }
                    });

            });
    }
    else {
        async.series(
            [
                (callback => {
                    BetTransactionModel.findOne({betId: betId})
                        .populate({path: 'group', select: '_id type name endpoint secretKey'})
                        .exec(function (err, response) {
                            if (err) {
                                callback(err, null);
                            } else {
                                callback(null, response);
                            }
                            console.log(response, '111')
                        });
                })

            ],
            (err, results) => {
                // console.log(results,'222');

                // let memberInfo = results[0]._id;
                let memberUsername = results[0].memberUsername;
                // condition['memberId'] = memberInfo;
                console.log(condition, 'condition101');


                UpdateBalanceModel.paginate(condition, {
                        page: page,
                        limit: limit
                    }
                    // .populate({path: 'group'})
                    , (err, data) => {
                        console.log(data, '123');
                        if (err) {

                            return res.send({message: "error", results: err, code: 999});
                            // callback(err, null);
                        } else {
                            // callback(null, data);

                            let dataList = _.map(data.docs, (item) => {
                                // console.log(item, 'item');
                                return {
                                    memberId: item.memberId,
                                    betId: item.betId,
                                    description: item.description,
                                    status: item.status,
                                    createdDate: item.createdDate,
                                    amount: item.amount,
                                    beforeCredit: item.beforeCredit,
                                    username_lower: memberUsername,
                                    // password: item.password

                                };
                            });

                            return res.send(
                                {
                                    code: 0,
                                    message: "success",
                                    result: {
                                        // dataList: {
                                        //     memberId: data[0].memberId,
                                        //     betId:data[0].betId,
                                        //     description:data[0].description,
                                        //     status:data[0].status,
                                        //     createdDate:data[0].createdDate,
                                        //     username_lower:memberUsername
                                        // }
                                        dataList: dataList,
                                        page: data.page,
                                        pages: data.pages,
                                        total: data.total,
                                        limit: data.limit
                                    }
                                    // memberUsername:memberUsername
                                }
                            );
                        }
                        // console.log(data,'111')
                    });
                // console.log(data)

            })

    }


});

router.get('/updateBeforeAfter', function (req, res) {
    console.log('111');

    let page = req.query.page ? Number.parseInt(req.query.page) : 1;
    let limit = req.query.limit ? Number.parseInt(req.query.limit) : 100;
    let condition = {};
    let userInfo = req.query.username;
    let betId = req.query.betId;
    console.log('user', userInfo);
    // let userInfo = 'show3'
    // let userInfo ='comnpay'

    if (req.query.betId) {
        condition['betId'] = req.query.betId;

    }
    if (req.query.dateFrom) {
        condition['createdDate'] = {
            "$gte": moment(req.query.dateFrom, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(11).utc(true).toDate(),
            "$lt": moment(req.query.dateTo, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(11).add(1, 'd').utc(true).toDate()
        }
    }
    if (userInfo) {
        async.series([
                (callback => {
                    MemberModel.findOne({username_lower: userInfo})
                        .populate({path: 'group', select: '_id type name endpoint secretKey'})
                        .exec(function (err, response) {
                            if (err) {
                                callback(err, null);
                            } else {
                                callback(null, response);
                            }
                            console.log(response, '111')
                        });
                }),
            ],
            (err, results) => {
                console.log(results, '333');

                let memberInfo = results[0]._id;
                let memberUsername = results[0].username_lower;
                condition['memberId'] = memberInfo;
                console.log(condition, 'condition');

                CreditModifyHistory.paginate(condition, {
                        page: page,
                        limit: limit
                    }
                    // .populate({path: 'group'})
                    , (err, data) => {
                        console.log(data, '123');
                        if (err) {
                            return {message: "error", result: err, code: 999}
                        }
                        else {
                            let dataList = _.map(data.docs, (item) => {
                                console.log(item, 'item')
                                return {
                                    memberId: item.memberId,
                                    betId: item.betId,
                                    createdDate: item.createdDate,
                                    beforeCredit: item.beforeCredit,
                                    beforeBalance: item.beforeBalance,
                                    afterCredit: item.afterCredit,
                                    afterBalance: item.afterBalance,
                                    username_lower: memberUsername,
                                    amount: item.amount

                                };
                            });

                            return res.send({
                                code: 0,
                                message: "success",
                                result: {
                                    dataList: dataList,
                                    page: data.page,
                                    pages: data.pages,
                                    total: data.total,
                                    limit: data.limit
                                }
                            });
                        }

                    });

            });
    }
    else {
        async.series(
            [
                (callback => {
                    BetTransactionModel.findOne({betId: betId})
                        .populate({path: 'group', select: '_id type name endpoint secretKey'})
                        .exec(function (err, response) {
                            if (err) {
                                callback(err, null);
                            } else {
                                callback(null, response);
                            }
                            console.log(response, '111')
                        });
                })

            ],
            (err, results) => {
                // console.log(results,'222');

                // let memberInfo = results[0]._id;
                let memberUsername = results[0].memberUsername;
                // condition['memberId'] = memberInfo;
                console.log(condition, 'condition101');


                CreditModifyHistory.paginate(condition, {
                        page: page,
                        limit: limit
                    }
                    // .populate({path: 'group'})
                    , (err, data) => {
                        console.log(data, '123');
                        if (err) {
                            // callback(err, null);
                        } else {
                            // callback(null, data);

                            let dataList = _.map(data.docs, (item) => {
                                console.log(item, 'item');
                                return {
                                    memberId: item.memberId,
                                    betId: item.betId,
                                    createdDate: item.createdDate,
                                    beforeCredit: item.beforeCredit,
                                    beforeBalance: item.beforeBalance,
                                    afterCredit: item.afterCredit,
                                    afterBalance: item.afterBalance,
                                    username_lower: memberUsername,
                                    amount: item.amount,
                                    // password: item.password

                                };
                            });

                            return res.send(
                                {
                                    code: 0,
                                    message: "success",
                                    result: {
                                        dataList: dataList,
                                        page: data.page,
                                        pages: data.pages,
                                        total: data.total,
                                        limit: data.limit
                                    }
                                }
                            );
                        }
                        // console.log(data,'111')
                    });
                // console.log(data)

            })

    }
});

router.get('/allUpdateBeforeAfter', function (req, res) {
    // console.log('111');
    let page = req.query.page ? Number.parseInt(req.query.page) : 1;
    let limit = req.query.limit ? Number.parseInt(req.query.limit) : 100;
    let condition = {};
    let userInfo = req.query.username;
    let betId = req.query.betId;
    // console.log('user', userInfo);
    // let userInfo = 'show3'
    // let userInfo ='comnpay'

    if (req.query.betId) {
        condition['betId'] = req.query.betId;

    }
    if (req.query.dateFrom) {
        condition['createdDate'] = {
            "$gte": new Date(moment(req.query.dateFrom, "DD/MM/YYYY").add(0, 'days').format('YYYY-MM-DDT11:00:00.000')),
            "$lt": moment(req.query.dateTo, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(11).add(1, 'd').utc(true).toDate()
        }
    }
    async.waterfall(
        [(callback => {
            MemberModel.findOne({username_lower: userInfo})
                .populate({path: 'group', select: '_id type name endpoint secretKey'})
                .exec(function (err, response) {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, response);
                    }
                });
        }),
            (groupInfo, callback) => {
                condition['memberId'] = groupInfo._id;
                CreditModifyHistory.paginate(condition,
                    {
                        page: page,
                        limit: limit
                    },
                    (err, result) => {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, groupInfo, result);
                        }

                    })

            },
            (groupInfo, creditResponse, callback) => {
                condition['memberId'] = groupInfo._id;
                UpdateBalanceModel.paginate(condition,
                    {
                        page: page,
                        limit: limit
                    },
                    (err, result) => {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, groupInfo, creditResponse, result);

                        }

                    })

            },
        ],
        (err, results, cc, vv) => {
            console.log(vv, 'qq')
            let userName = results.username_lower;
            let cd = _.map(cc.docs, (item) => {
                return {
                    memberId: item.memberId,
                    betId: item.betId,
                    createdDate: item.createdDate,
                    beforeCredit: item.beforeCredit,
                    beforeBalance: item.beforeBalance,
                    afterCredit: item.afterCredit,
                    afterBalance: item.afterBalance,
                    username_lower: userName,
                    amount: item.amount,
                    type: 'CREM'
                }
            });

            let bl = _.map(vv.docs, (item) => {
                console.log(item, 'ควาย')
                return {
                    memberId: item.memberId,
                    betId: item.betId,
                    description: item.description,
                    status: item.status,
                    createdDate: item.createdDate,
                    amount: item.amount,
                    beforeCredit: item.beforeCredit,
                    username_lower: userName,
                    type: 'BALANCE'
                }
            });

            let dataList = [];

            _.forEach(cd, (item) => {
                dataList.push(item)
            });

            _.forEach(bl, (item) => {
                dataList.push(item)
            });

            dataList = _.sortBy(dataList, function (o) {
                return o.createdDate;
            });
            console.log(dataList, '22222');


            if (err) {
                return res.send({message: "error", results: err, code: 999});
            }
            else {
                return res.send({
                    code: 0,
                    message: "success",
                    result: {
                        dataList: dataList,
                    }
                });
            }

        });

});


module.exports = router;