const express = require('express')
const uuid = require('uuid/v4');
const router = express.Router()
const async = require("async");
const Joi = require('joi');
const mongoose = require('mongoose');
const Hashing = require('../../common/hashing');
const DateUtils = require('../../common/dateUtils');
const config = require('config');
const moment = require('moment');
const _ = require('underscore');
const roundTo = require('round-to');
const MemberService = require('../../common/memberService');
const CashService = require('../../common/cashService');
const AgentService = require("../../common/agentService");
const jwt = require('../../common/jwt');
const BankModel = require('../../models/bank.model');
const AgentGroupModel = require('../../models/agentGroup.model');
const WebsiteModel = require('../../models/website.model');
const MemberModel = require('../../models/member.model');
const CashTransactionModel = require('../../models/cashTransaction.model');
const CreditModifyHistoryModel = require('../../models/creditModifyHistory.model');
const UserModel = require('../../models/users.model');

const CLIENT_NAME = process.env.CLIENT_NAME || 'SPORTBOOK88';
const Redis = require('../sbo/1_common/1_redis/1_1_redis');



router.get('/remainingCredit', function (req, res) {

    let userInfo = req.userInfo;

    MemberModel.aggregate([
        {
            $match: {group: mongoose.Types.ObjectId(userInfo.group._id)}
        },
        {
            $group: {
                _id: null,
                remainingBalance: {
                    $sum: {$toDouble: {$add: [{$toDecimal: '$creditLimit'}, {$toDecimal: '$balance'}]}}
                }
            }
        },
        {
            $project: {
                _id: 0,
                remainingBalance: '$remainingBalance'
            }
        }
    ], (err, results) => {
        if (err) {
            return res.send({message: "error", results: err, code: 999});
        } else {
            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: results[0]
                }
            );
        }
    });
});


router.get('/list', function (req, res) {

    let userInfo = req.userInfo;

    let page = req.query.page ? Number.parseInt(req.query.page) : 1;
    let limit = req.query.limit ? Number.parseInt(req.query.limit) : 50;

    let condition = {active: true};

    if (req.query.dateFrom && req.query.dateTo) {
        condition['createdDate'] = {
            "$gte": moment(req.query.dateFrom, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).utc(true).toDate(),
            "$lt": moment(req.query.dateTo, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).add(1,'d').utc(true).toDate()
        }
    }

    if (req.query.memberId) {
        condition.memberId = mongoose.Types.ObjectId(req.query.memberId);
    }
    if (req.query.status) {
        condition['status'] = req.query.status;
    }

    condition.agent = mongoose.Types.ObjectId(userInfo.group._id);

    if (req.query.type) {
        condition['type'] = req.query.type;
    }

    console.log(condition);

    CashTransactionModel.paginate(condition, {
            page: page,
            limit: limit,
            sort: {createdDate: -1},
            populate: [
                {
                    path: 'memberId',
                    select: 'username_lower _id'
                },
                {
                    path: 'agent',
                    select: 'name_lower',
                }]
        },
        function (err, data) {
            if (err)
                return res.send({message: "error", results: err, code: 999});


            let summary = _.reduce(data.docs, function (memo, obj) {
                return {
                    depositAmount: memo.depositAmount + (obj.tranType === 'DEPOSIT' ? obj.amount : 0 ),
                    withdrawAmount: memo.withdrawAmount + (obj.tranType === 'WITHDRAW' ? obj.amount : 0 ),
                }
            }, {
                depositAmount: 0,
                withdrawAmount: 0
            });
            data.summary = summary;

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: data
                }
            );
        });
});

router.get('/list/manual', function (req, res) {

    let userInfo = req.userInfo;

    let page = req.query.page ? Number.parseInt(req.query.page) : 1;
    let limit = req.query.limit ? Number.parseInt(req.query.limit) : 50;

    let condition = {};

    if (req.query.dateFrom && req.query.dateTo && req.query.timeFrom && req.query.timeTo) {
        condition['createdDate'] = {
            "$gte": DateUtils.convertDateTime(req.query.dateFrom,req.query.timeFrom),
            "$lt": DateUtils.convertDateTime(req.query.dateTo,req.query.timeTo)
        }
    }


    condition.agent = mongoose.Types.ObjectId(userInfo.group._id);
    condition.active = true;
    condition['type'] = 'MANUAL';
    if (req.query.tranType) {
        condition.tranType = req.query.tranType;
    }





    async.series([(callback => {
        if (req.query.member) {
            let mCondition = {username_lower: req.query.member.toLowerCase()};

            MemberModel.findOne(mCondition, (err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            }).select('_id username contact');
        } else {
            callback(null, null);
        }

    }), callback => {

        CashTransactionModel.aggregate([
            {
                $match: condition
            },
            {
                $group: {
                    _id: null,
                    depositAmount: {
                        $sum: {
                            $cond: [{$eq: ['$tranType', 'DEPOSIT']}, '$amount', 0]
                        }
                    },
                    depositCount: {
                        $sum: {
                            $cond: [{$eq: ['$tranType', 'DEPOSIT']}, 1, 0]
                        }
                    },
                    withdrawAmount: {
                        $sum: {
                            $cond: [{$eq: ['$tranType', 'WITHDRAW']}, '$amount', 0]
                        }
                    },
                    withdrawCount: {
                        $sum: {
                            $cond: [{$eq: ['$tranType', 'WITHDRAW']}, 1, 0]
                        }
                    }
                }
            },
            {
                $project: {
                    _id: 0,
                    depositAmount: '$depositAmount',
                    withdrawAmount: '$withdrawAmount',
                    depositCount: '$depositCount',
                    withdrawCount: '$withdrawCount',
                    sum: {$subtract: ['$depositAmount', '$withdrawAmount']}
                }
            }
        ], (err, results) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, results[0] ? results[0] : {
                        depositAmount: 0,
                        withdrawAmount: 0,
                        depositCount: 0,
                        withdrawCount: 0,
                        sum: 0
                    });
            }
        });
    }], function (err, results) {

        if (err) {
            return res.send({message: "error", results: err, code: 999});
        }

        if (req.query.member) {
            condition.memberId = mongoose.Types.ObjectId(results[0] ? results[0]._id : null);
        }


        CashTransactionModel.paginate(condition, {
                page: page,
                limit: limit,
                sort: {createdDate: -1},
                populate: [
                    {
                        path: 'memberId',
                        select: 'username_lower _id'
                    },
                    {
                        path: 'agent',
                        select: 'name_lower',
                    },
                    {
                        path: 'addCreditBy',
                        select: 'username_lower',
                    }]
            },
            function (err, data) {
                if (err)
                    return res.send({message: "error", results: err, code: 999});

                if (data.total > 0) {
                    let condition = {'$or': []};

                    _.forEach(data.docs, (item) => {
                        condition.$or.push({memberId: item.memberId, ref: item.ref})
                    });

                    CreditModifyHistoryModel.find(condition, (err, creditHistory) => {

                        if (err) {
                            return res.send({message: "error", results: err, code: 999});
                        }

                        let prepareList = _.map(data.docs, item => {
                            let newItem = Object.assign({}, item)._doc;

                            let matchItem = _.filter(creditHistory, (ch) => {
                                return (ch.memberId.toString() == item.memberId._id.toString()) && (ch.ref == item.ref);
                            })[0];

                            if (matchItem) {
                                newItem.remainingBalance = roundTo(parseFloat(matchItem.afterCredit), 3) + roundTo(parseFloat(matchItem.afterBalance), 3);
                            }
                            return newItem;
                        });

                        data.docs = prepareList;

                        return res.send(
                            {
                                code: 0,
                                message: "success",
                                result: data,
                                summary: results[1]
                            }
                        );
                    });
                } else {
                    return res.send(
                        {
                            code: 0,
                            message: "success",
                            result: data,
                            summary: results[1]
                        }
                    );
                }


            });

    });

});

router.get('/list/auto', function (req, res) {

    let userInfo = req.userInfo;

    let page = req.query.page ? Number.parseInt(req.query.page) : 1;
    let limit = req.query.limit ? Number.parseInt(req.query.limit) : 50;

    let condition = {};


    if (req.query.dateFrom && req.query.dateTo && req.query.timeFrom && req.query.timeTo) {
        condition['createdDate'] = {
            "$gte": DateUtils.convertDateTime(req.query.dateFrom,req.query.timeFrom),
            "$lt": DateUtils.convertDateTime(req.query.dateTo,req.query.timeTo)
        }
    }else {
        return res.send(
            {
                code: 0,
                message: "success",
                result: {}
            }
        );
    }

    condition.agent = mongoose.Types.ObjectId(userInfo.group._id);
    condition.active = true;
    condition['type'] = 'AUTO';
    if (req.query.tranType) {
        condition.tranType = req.query.tranType;
    }


    async.series([(callback => {
        if (req.query.member) {
            let condition = {username_lower: req.query.member.toLowerCase()};

            MemberModel.findOne(condition, (err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            }).select('_id username contact');
        } else {
            callback(null, null);
        }

    })], function (err, results) {

        if (err) {
            return res.send({message: "error", results: err, code: 999});
        }

        if (req.query.member) {
            condition.memberId = mongoose.Types.ObjectId(results[0] ? results[0]._id : null);
        }

        console.log(condition)

        CashTransactionModel.paginate(condition, {
                page: page,
                limit: limit,
                sort: {createdDate: -1},
                populate: [
                    {
                        path: 'memberId',
                        select: 'username_lower _id'
                    },
                    {
                        path: 'agent',
                        select: 'name_lower',
                    }]
            },
            function (err, data) {
                if (err)
                    return res.send({message: "error", results: err, code: 999});


                if (data.total > 0) {
                    let condition = {'$or': []};

                    _.forEach(data.docs, (item) => {
                        condition.$or.push({memberId: item.memberId, ref: item.ref})
                    });

                    CreditModifyHistoryModel.find(condition, (err, creditHistory) => {

                        if (err) {
                            return res.send({message: "error", results: err, code: 999});
                        }

                        let prepareList = _.map(data.docs, item => {
                            let newItem = Object.assign({}, item)._doc;

                            let matchItem = _.filter(creditHistory, (ch) => {
                                return (ch.memberId.toString() == item.memberId._id.toString()) && (ch.ref == item.ref);
                            })[0];

                            if (matchItem) {
                                newItem.remainingBalance = roundTo(parseFloat(matchItem.afterCredit), 3) + roundTo(parseFloat(matchItem.afterBalance), 3);
                            }
                            return newItem;
                        })

                        data.docs = prepareList;

                        return res.send(
                            {
                                code: 0,
                                message: "success",
                                result: data
                            }
                        );
                    });
                } else {
                    return res.send(
                        {
                            code: 0,
                            message: "success",
                            result: data
                        }
                    );
                }
            });

    });

});

router.get('/status', function (req, res) {

    let userInfo = req.userInfo;


    AgentGroupModel.findOne({_id: mongoose.Types.ObjectId(userInfo.group._id)}, 'cash')
        .lean()
        .exec(function (err, response) {
            if (err)
                return res.send({message: "error", result: err, code: 999});
            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: {
                        isActive: response.cash.isActive
                    }
                }
            );
        });
});

router.get('/setting', function (req, res) {

    let userInfo = req.userInfo;

    AgentGroupModel.findOne({_id: mongoose.Types.ObjectId(userInfo.group._id)}, 'cash')
        .lean()
        .exec(function (err, response) {
            if (err)
                return res.send({message: "error", result: err, code: 999});
            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: response.cash
                }
            );
        });
});

const settingSchema = Joi.object().keys({
    isMemberDeposit: Joi.boolean().required(),
    depositMin: Joi.number().required(),
    depositMax: Joi.number().required(),
    isMemberWithdraw: Joi.boolean().required(),
    withdrawCountDay: Joi.number().required(),
    withdrawMin: Joi.number().required(),
    withdrawMax: Joi.number().required(),
    isActive: Joi.boolean().required(),
});

router.post('/setting', function (req, res) {


    let validate = Joi.validate(req.body, settingSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            result: validate.error.details,
            code: 999
        });
    }

    let userInfo = req.userInfo;
    console.log(userInfo.group._id);

    let requestBody = req.body;

    AgentGroupModel.update({_id: mongoose.Types.ObjectId(userInfo.group._id)}, {
        $set: {
            'cash.isMemberDeposit': requestBody.isMemberDeposit,
            'cash.depositMin': requestBody.depositMin,
            'cash.depositMax': requestBody.depositMax,
            'cash.isMemberWithdraw': requestBody.isMemberWithdraw,
            'cash.withdrawCountDay': requestBody.withdrawCountDay,
            'cash.withdrawMin': requestBody.withdrawMin,
            'cash.withdrawMax': requestBody.withdrawMax,
            'cash.isActive': requestBody.isActive
        }
    }).exec(function (err, response) {
        console.log(response)
        if (err) {
            return res.send({message: "error", results: err, code: 999});
        } else {
            return res.send({
                code: 0,
                message: "success",
            });
        }
    });

});

router.get('/log', function (req, res) {

    let userInfo = req.userInfo;

    let page = req.query.page ? Number.parseInt(req.query.page) : 1;
    let limit = req.query.limit ? Number.parseInt(req.query.limit) : 50;

    let condition = {active: true};

    if (req.query.memberId) {
        condition['memberId'] = mongoose.Types.ObjectId(req.query.memberId);
    }


    condition.agent = mongoose.Types.ObjectId(userInfo.group._id);

    condition.status = 'COMPLETE';


    CashTransactionModel.paginate(condition, {
            page: page,
            limit: limit,
            sort: {createdDate: -1},
            populate: [
                {
                    path: 'memberId',
                    select: 'username_lower _id'
                },
                {
                    path: 'agent',
                    select: 'cash',
                    populate: {
                        path: 'cash.banks.bank'
                    }
                }
            ]
        },
        function (err, data) {
            if (err)
                return res.send({message: "error", results: err, code: 999});


            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: data
                }
            );
        });
});

const addBankSchema = Joi.object().keys({
    accountNumber: Joi.string().required(),
    accountName: Joi.string().required(),
    accountBranch: Joi.string().required(),
    bank: Joi.string().required(),
    isDeposit: Joi.boolean().required(),
    isWithdraw: Joi.boolean().required(),
});

router.post('/agent/bank', function (req, res) {




    let validate = Joi.validate(req.body, addBankSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            result: validate.error.details,
            code: 999
        });
    }

    let userInfo = req.userInfo;


    let requestBody = req.body;

    let updateBody = {
        accountNumber: requestBody.accountNumber,
        accountName: requestBody.accountName,
        accountBranch: requestBody.accountBranch,
        bank: requestBody.bank,
        isDeposit: requestBody.isDeposit,
        isWithdraw: requestBody.isWithdraw,
    };


    AgentGroupModel.update({_id: mongoose.Types.ObjectId(userInfo.group._id)},
        {
            $push: {
                'cash.banks': {
                    accountNumber: requestBody.accountNumber,
                    accountName: requestBody.accountName,
                    accountBranch: requestBody.accountBranch,
                    bank: requestBody.bank,
                    isDeposit: requestBody.isDeposit,
                    isWithdraw: requestBody.isWithdraw,
                    active: true
                }
            }
        },
        {safe: true, upsert: true, new: true})
        .exec(function (err, response) {

            if (err) {
                return res.send({message: "error", results: err, code: 999});
            } else {
                return res.send({
                    code: 0,
                    message: "success",
                });
            }
        });

});


router.get('/report/transaction', function (req, res) {

    let userInfo = req.userInfo;

    let page = req.query.page ? Number.parseInt(req.query.page) : 1;
    let limit = req.query.limit ? Number.parseInt(req.query.limit) : 50;

    let condition = {};

    if (req.query.dateFrom && req.query.dateTo && req.query.timeFrom && req.query.timeTo) {
        condition['createdDate'] = {
            "$gte": DateUtils.convertDateTime(req.query.dateFrom,req.query.timeFrom),
            "$lt": DateUtils.convertDateTime(req.query.dateTo,req.query.timeTo)
        }
    }else {
        return res.send(
            {
                code: 0,
                message: "success",
                result: {}
            }
        );
    }


    async.series([callback => {

        if (req.query.member) {
            let condition = {username_lower: req.query.member.toLowerCase()};

            MemberModel.findOne(condition, (err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            }).select('_id username contact');
        } else {
            callback(null, null);
        }

    }, callback => {

        AgentGroupModel.findById(userInfo.group._id, (err, response) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, response);
            }
        }).select('_id username childMembers');

    }], function (err, results) {

        if (err) {
            return res.send({message: "error", results: err, code: 999});
        }

        if (req.query.member) {
            condition['$and'] = [
                {memberId: mongoose.Types.ObjectId(results[0] ? results[0]._id : null)},
                {memberId: {$in: results[1].childMembers}}
            ]
        } else {
            condition.memberId = {$in: results[1].childMembers};
        }

        CreditModifyHistoryModel.paginate(condition, {
                page: page,
                limit: limit,
                sort: {createdDate: -1},
                populate: [
                    {
                        path: 'memberId',
                        select: 'username_lower _id'
                    }]
            },
            function (err, data) {
                if (err)
                    return res.send({message: "error", results: err, code: 999});

                let list = _.map(data.docs, item => {
                    let newItem = Object.assign({}, item)._doc;
                    try {
                        newItem.remainingBalance = roundTo(parseFloat(item.afterCredit), 3) + roundTo(parseFloat(item.afterBalance), 3);
                    } catch (err) {

                    }
                    return newItem;
                })


                let summary = _.reduce(data.docs, function (memo, obj) {
                    return {
                        amount: memo.amount + obj.amount,
                    }
                }, {
                    amount: 0
                });
                data.docs = list;
                data.summary = summary;

                return res.send(
                    {
                        code: 0,
                        message: "success",
                        result: data
                    }
                );
            });

    });

});

const updateBankSchema = Joi.object().keys({
    accountNumber: Joi.number().required(),
    accountName: Joi.string().required(),
    accountBranch: Joi.string().required(),
    bank: Joi.string().required(),
    isDeposit: Joi.boolean().required(),
    isWithdraw: Joi.boolean().required(),
});

router.put('/agent/bank/:id', function (req, res) {


    let validate = Joi.validate(req.body, updateBankSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            result: validate.error.details,
            code: 999
        });
    }

    let userInfo = req.userInfo;

    let requestBody = req.body;

    let updateBody = {
        accountNumber: requestBody.accountNumber,
        accountName: requestBody.accountName,
        accountBranch: requestBody.accountBranch,
        bank: requestBody.bank,
        isDeposit: requestBody.isDeposit,
        isWithdraw: requestBody.isWithdraw,
    };


    console.log('updateBody : ', updateBody)

    AgentGroupModel.update({
            _id: mongoose.Types.ObjectId(userInfo.group._id),
            'cash.banks._id': mongoose.Types.ObjectId(req.params.id)
        },
        {
            $set: {
                'cash.banks.$': {
                    _id: req.params.id,
                    accountNumber: requestBody.accountNumber,
                    accountName: requestBody.accountName,
                    accountBranch: requestBody.accountBranch,
                    bank: requestBody.bank,
                    isDeposit: requestBody.isDeposit,
                    isWithdraw: requestBody.isWithdraw,
                    active: true
                }
            }
        })
        .exec(function (err, response) {
            console.log(err)
            console.log(response)
            if (err) {
                return res.send({message: "error", results: err, code: 999});
            } else {
                return res.send({
                    code: 0,
                    message: "success",
                });
            }
        });

});

router.put('/agent/delete/bank/:id', function (req, res) {

    let userInfo = req.userInfo;

    AgentGroupModel.update({
            _id: mongoose.Types.ObjectId(userInfo.group._id),
            'cash.banks._id': mongoose.Types.ObjectId(req.params.id)
        },
        {$set: {'cash.banks.$.active': false}})
        .exec(function (err, response) {
            console.log(err)
            console.log(response)
            if (err) {
                return res.send({message: "error", results: err, code: 999});
            } else {
                return res.send({
                    code: 0,
                    message: "success",
                });
            }
        });

});

router.get('/agent/bank', function (req, res) {

    let userInfo = req.userInfo;

    AgentGroupModel.findById(mongoose.Types.ObjectId(userInfo.group._id), 'cash')
        .lean()
        .populate('cash.banks.bank')
        .exec(function (err, response) {
            if (err)
                return res.send({message: "error", results: err, code: 999});

            let bankList;

            if(CLIENT_NAME == 'OSGAME99' || CLIENT_NAME == 'SPORTBOOK88' ){

                bankList = _.filter(response.cash.banks, (item) => {
                    return item.active;
                });

            }else {

                bankList = _.filter(response.cash.banks, (item) => {
                    return item.active;
                }).map(bank => {

                    if (bank.accountNumber.length == 10) {
                        let p1 = bank.accountNumber.substring(3, 4);
                        let p2 = bank.accountNumber.substring(4, 9);
                        bank.accountNumber = "xxx-" + p1 + "-" + p2 + "-x";
                    } else if (bank.accountNumber.length > 10) {
                        let p1 = bank.accountNumber.substring(3, 4);
                        let p2 = bank.accountNumber.substring(4, bank.accountNumber.length);
                        bank.accountNumber = "xxx-" + p1 + "-" + p2;
                    }

                    return bank;
                });

            }
            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: {
                        list: bankList
                    }
                }
            );
        });
});

router.get('/agent/member/bank', function (req, res) {

    let userInfo = req.userInfo;

    AgentGroupModel.findById(mongoose.Types.ObjectId(userInfo.group._id), 'cash')
        .lean()
        .populate('cash.banks.bank')
        .exec(function (err, response) {
            if (err)
                return res.send({message: "error", results: err, code: 999});
            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: {
                        list: _.filter(response.cash.banks, (item) => {
                            return item.active;
                        })
                    }
                }
            );
        });
});

router.get('/agent/bank/:id', function (req, res) {

    let userInfo = req.userInfo;

    AgentGroupModel.findById(mongoose.Types.ObjectId(userInfo.group._id), 'cash')
        .lean()
        .exec(function (err, response) {
            if (err)
                return res.send({message: "error", results: err, code: 999});

            let result = _.filter(response.cash.banks, (item => {
                return item._id == req.params.id;
            }))[0];
            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: result
                }
            );
        });

});

router.get('/banks', function (req, res) {
    Redis.getByKey(`BankModel${CLIENT_NAME}`, (error, result) => {
        if (error || _.isNull(result) || _.isUndefined(result)) {
            BankModel.find({"active": true})
                .lean()
                .exec(function (err, response) {
                    if (err) {
                        return res.send({message: "error", results: err, code: 999});
                    } else {
                        Redis.setByKey(`BankModel${CLIENT_NAME}`, response, (error, result) => {

                        });
                        return res.send(
                            {
                                code: 0,
                                message: "success",
                                result: {
                                    list: response
                                }
                            }
                        );
                    }
                });
        } else {
            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: {
                        list: result
                    }
                }
            );
        }
    });
});

router.get('/deposit/history', function (req, res) {


    let page = req.query.page ? Number.parseInt(req.query.page) : 1;
    let limit = req.query.limit ? Number.parseInt(req.query.limit) : 50;

    let userInfo = req.userInfo;


    let condition = {active: true, tranType: 'DEPOSIT'};


    condition.memberId = mongoose.Types.ObjectId(userInfo._id);


    CashTransactionModel.paginate(condition, {
            page: page,
            limit: limit,
            sort: {createdDate: -1},
            populate: [
                {
                    path: 'memberId',
                    select: 'username_lower _id'
                }
            ]
        },
        function (err, data) {
            if (err)
                return res.send({message: "error", results: err, code: 999});


            let transformList = _.map(data.docs, item => {

                let newItem = {};

                // 'WAITING_CHECKING','WAITING_ADD_CREDIT','WAITING_TRANSFER','COMPLETE','REJECTED'

                let timeOperation;
                if (item.type === 'CASH') {
                    if (item.status === 'WAITING_CHECKING') {
                        timeOperation = item.createdDate;
                    } else if (item.status === 'WAITING_ADD_CREDIT' || item.status === 'WAITING_TRANSFER') {
                        timeOperation = item.checkDate;
                    } else if (item.status === 'COMPLETE') {
                        timeOperation = item.addCreditDate;
                    } else if (item.status === 'REJECTED') {
                        timeOperation = item.cancelDate;
                    }
                } else {
                    timeOperation = item.processDateTime;
                }


                newItem.ref = item.ref;
                newItem.createdDate = item.processDateTime;
                newItem.amount = item.amount;
                newItem.timeOperation = timeOperation;
                newItem.status = item.status === 'COMPLETE' ? 'SUCCESS' : item.status === 'REJECTED' ? 'CANCELLED' : 'WAITING'
                return newItem;
            });

            data.docs = transformList;

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: data
                }
            );
        });

});

router.get('/withdraw/history', function (req, res) {

    let page = req.query.page ? Number.parseInt(req.query.page) : 1;
    let limit = req.query.limit ? Number.parseInt(req.query.limit) : 50;

    let userInfo = req.userInfo;

    let condition = {active: true, tranType: 'WITHDRAW'};


    condition.memberId = mongoose.Types.ObjectId(userInfo._id);

    CashTransactionModel.paginate(condition, {
            page: page,
            limit: limit,
            sort: {createdDate: -1},
            populate: [
                {
                    path: 'memberId',
                    select: 'username_lower _id'
                }
            ]
        },
        function (err, data) {
            if (err)
                return res.send({message: "error", results: err, code: 999});


            let transformList = _.map(data.docs, item => {

                let newItem = {};

                // 'WAITING_CHECKING','WAITING_ADD_CREDIT','WAITING_TRANSFER','COMPLETE','REJECTED'

                let timeOperation;
                if (item.type === 'CASH') {
                    if (item.status === 'WAITING_CHECKING') {
                        timeOperation = item.createdDate;
                    } else if (item.status === 'WAITING_ADD_CREDIT' || item.status === 'WAITING_TRANSFER') {
                        timeOperation = item.checkDate;
                    } else if (item.status === 'COMPLETE') {
                        timeOperation = item.addCreditDate || item.transferDate;
                    } else if (item.status === 'REJECTED') {
                        timeOperation = item.cancelDate;
                    }
                } else {
                    timeOperation = item.processDateTime;
                }


                newItem.ref = item.ref;
                newItem.createdDate = item.processDateTime;
                newItem.amount = item.amount;
                newItem.timeOperation = timeOperation;
                newItem.status = item.status === 'COMPLETE' ? 'SUCCESS' : item.status === 'REJECTED' ? 'CANCELLED' : 'WAITING'
                return newItem;
            });

            data.docs = transformList;

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: data
                }
            );
        });

});

const depositSchema = Joi.object().keys({
    transferType: Joi.string().required(),
    transferFromBankId: Joi.string().required(),
    amount: Joi.number().required(),
    accountName: Joi.string().allow(''),
    accountNumber: Joi.string().allow(''),
    transferDate: Joi.string().required(),
    transferTime: Joi.string().required(),
    transferToAgentBankId: Joi.string().required(),
    depositSlip: Joi.string().allow(null, '')
});

router.post('/deposit', function (req, res) {


    let validate = Joi.validate(req.body, depositSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            result: validate.error.details,
            code: 999
        });
    }

    let userInfo = req.userInfo;
    let requestBody = req.body;


    async.series([callback => {

        AgentGroupModel.findById(userInfo.group._id, 'cash', (err, response) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, response);
            }
        });

    }, callback => {

        let condition = {};//{$or: [{'status': 'WAITING_CHECKING'}, {'status': 'WAITING_ADD_CREDIT'},{'status': 'WAITING_TRANSFER'}]};
        condition.memberId = mongoose.Types.ObjectId(userInfo._id);
        condition.tranType = 'DEPOSIT';

        CashTransactionModel.findOne(condition, (err, response) => {
            callback(null, response)
        }).sort({_id: -1}).limit(1);

    }], (err, results) => {

        const cashSetting = results[0].cash;
        const lastTransaction = results[1];


        if (lastTransaction) {
            if (lastTransaction.status === 'WAITING_CHECKING') {
                return res.send({
                    message: "Wait Checking",
                    results: err,
                    code: 4002
                });
            }
            if (lastTransaction.status === 'WAITING_ADD_CREDIT') {
                return res.send({
                    message: "Wait Change Credit",
                    results: err,
                    code: 4002
                });
            }
        }

        if (!cashSetting.isMemberDeposit) {
            return res.send({message: "deposit close", results: err, code: 4001});
        }
        if (requestBody.amount > cashSetting.depositMax) {
            return res.send({
                message: "Deposit Maximum " + cashSetting.depositMax,
                results: err,
                code: 4001
            });
        }
        if (requestBody.amount < cashSetting.depositMin) {
            return res.send({
                message: "Deposit Minimum " + cashSetting.depositMin,
                results: err,
                code: 4001
            });
        }

        let processDateTime = moment([Number.parseInt(requestBody.transferDate.split("-")[2]), Number.parseInt(requestBody.transferDate.split("-")[1]) - 1, Number.parseInt(requestBody.transferDate.split("-")[0])])
            .second(0).minute(Number.parseInt(requestBody.transferTime.split(":")[1])).hour(Number.parseInt(requestBody.transferTime.split(":")[0])).utc(true).toDate();

        let body = {
            ref: moment().toDate().getTime(),
            agent: userInfo.group._id,
            memberId: userInfo._id,
            member_name: userInfo.username_lower,
            transferType: requestBody.transferType,
            fromBank: requestBody.transferFromBankId,
            fromBankAccountName: requestBody.fromBankAccountName,
            fromBankAccountNumber: requestBody.accountNumber,
            processDateTime: processDateTime,
            amount: requestBody.amount,
            toAgentBankId: requestBody.transferToAgentBankId,
            status: 'WAITING_CHECKING',
            active: true,
            tranType: 'DEPOSIT',
            type: 'CASH',
            createdDate: DateUtils.getCurrentDate(),
            depositSlip: requestBody.depositSlip
        };

        let model = new CashTransactionModel(body);
        model.save((err, response) => {

            if (err) {
                return res.send({code: 999, message: err});
            }

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: req.body
                }
            );

        });

    });

});

const depositUpdateSchema = Joi.object().keys({
    _id: Joi.string().required(),
    status: Joi.string().required(),
    accountNumber: Joi.string().allow('')
});

router.post('/update/process', function (req, res) {

    let validate = Joi.validate(req.body, depositUpdateSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            result: validate.error.details,
            code: 999
        });
    }

    let userInfo = req.userInfo;
    let requestBody = req.body;

    UserModel.findOne({_id:userInfo._id},(err,checkAgentResponse)=>{


        if(!checkAgentResponse) {

            return res.send({
                message: "UnAuthorization",
                result: err,
                code: 401
            });


        }else {

            CashTransactionModel.findById(mongoose.Types.ObjectId(requestBody._id)).exec(function (err, cashTransaction) {

                if (cashTransaction) {

                    if (requestBody.status === 'WAITING_TRANSFER') {
                        if (cashTransaction.fromBankAccountNumber != requestBody.accountNumber) {
                            return res.send({
                                message: "account number not match",
                                result: err,
                                code: 5002
                            });
                        }
                    }

                    if (cashTransaction.status === 'COMPLETE' || cashTransaction.status === 'REJECTED') {
                        return res.send({
                            message: "transaction was already process",
                            result: {status: cashTransaction.status},
                            code: 5003
                        });
                    }

                    if (requestBody.status === 'WAITING_CHECKING' && cashTransaction.status !== 'WAITING_CHECKING') {
                        return res.send({
                            message: "transaction was already process",
                            result: {status: cashTransaction.status},
                            code: 5003
                        });
                    }

                    if (requestBody.status === 'WAITING_ADD_CREDIT' && cashTransaction.status !== 'WAITING_ADD_CREDIT') {
                        return res.send({
                            message: "transaction was already process",
                            result: {status: cashTransaction.status},
                            code: 5003
                        });
                    }

                    if (requestBody.status === 'WAITING_TRANSFER' && cashTransaction.status !== 'WAITING_TRANSFER') {
                        return res.send({
                            message: "transaction was already process",
                            result: {status: cashTransaction.status},
                            code: 5003
                        });
                    }

                    if (requestBody.status === 'WAITING_TRANSFER' && cashTransaction.status !== 'WAITING_TRANSFER') {
                        return res.send({
                            message: "transaction was already process",
                            result: {status: cashTransaction.status},
                            code: 5003
                        });
                    }


                    async.series([callback => {

                        if (requestBody.status === 'WAITING_ADD_CREDIT') {
                            addMemberCreditFunction(cashTransaction.agent, cashTransaction.memberId, cashTransaction.amount, cashTransaction.ref, (err, response) => {
                                console.log('modifyMemberCreditFunction : ', err)
                                console.log('modifyMemberCreditFunction : ', response)
                                if (err) {
                                    callback(err, null);
                                } else {
                                    callback(null, 'success');
                                }
                            })
                        } else if (requestBody.status === 'WAITING_TRANSFER') {


                            reduceMemberCreditFunction(cashTransaction.agent, cashTransaction.memberId, cashTransaction.amount, cashTransaction.ref, (err, response) => {
                                console.log('modifyMemberCreditFunction : ', err)
                                console.log('modifyMemberCreditFunction : ', response)
                                if (err) {
                                    callback(err, null);
                                } else {
                                    callback(null, 'success');
                                }
                            });

                        } else {
                            callback(null, 'success');
                        }

                    }], (err, results) => {

                        if (err) {
                            if (err == 5001) {
                                return res.send({
                                    message: "balance is not enough ",
                                    result: null,
                                    code: 5001
                                });
                            }
                            if (err == 4002) {
                                return res.send({
                                    message: "เครดิิตไม่เพียงพอ",
                                    result: null,
                                    code: 4002
                                });
                            }
                            return res.send({message: "error", result: err, code: 999});
                        }

                        let updateBody;

                        if (cashTransaction.tranType === 'DEPOSIT') {
                            if (requestBody.status === 'WAITING_CHECKING') {
                                updateBody = {
                                    status: 'WAITING_ADD_CREDIT',
                                    checkBy: userInfo._id,
                                    checkDate: DateUtils.getCurrentDate()
                                };
                            } else if (requestBody.status === 'WAITING_ADD_CREDIT') {
                                updateBody = {
                                    status: 'COMPLETE',
                                    addCreditBy: userInfo._id,
                                    addCreditDate: DateUtils.getCurrentDate()
                                };
                            } else if (requestBody.status === 'REJECT') {
                                updateBody = {
                                    status: 'REJECTED',
                                    cancelBy: userInfo._id,
                                    cancelDate: DateUtils.getCurrentDate()
                                };
                            }
                        } else {
                            if (requestBody.status === 'WAITING_CHECKING') {
                                updateBody = {
                                    status: 'WAITING_TRANSFER',
                                    checkBy: userInfo._id,
                                    checkDate: DateUtils.getCurrentDate()
                                };
                            } else if (requestBody.status === 'WAITING_TRANSFER') {

                                updateBody = {
                                    status: 'COMPLETE',
                                    transferBy: userInfo._id,
                                    transferDate: DateUtils.getCurrentDate()
                                };
                            } else if (requestBody.status === 'REJECT') {
                                updateBody = {
                                    status: 'REJECTED',
                                    cancelBy: userInfo._id,
                                    cancelDate: DateUtils.getCurrentDate()
                                };
                            }
                        }

                        CashTransactionModel.update({_id: mongoose.Types.ObjectId(requestBody._id)}, {$set: updateBody})
                            .exec(function (err, response) {

                                if (err) {
                                    return res.send({code: 999, message: err});
                                }

                                return res.send(
                                    {
                                        code: 0,
                                        message: "success",
                                        result: req.body
                                    }
                                );

                            });

                    });


                } else {
                    return res.send({message: "transaction not found", results: err, code: 999});
                }

            });
        }

    });

});

router.get('/deposit/list', function (req, res) {

    let userInfo = req.userInfo;

    let page = req.query.page ? Number.parseInt(req.query.page) : 1;
    let limit = req.query.limit ? Number.parseInt(req.query.limit) : 50;

    let condition = {active: true, tranType: 'DEPOSIT'};

    if (req.query.username) {
        condition['member_name'] = req.query.username;
    }

    condition.agent = mongoose.Types.ObjectId(userInfo.group._id);


    let orCondition;
    if (req.query.status) {
        if (req.query.status === 'WAITING_CHECKING_CREDIT') {
            orCondition = [{'$or': [{'status': 'WAITING_CHECKING'}, {'status': 'WAITING_ADD_CREDIT'}]}];
            condition['$and'] = orCondition;
        } else if (req.query.status !== 'ALL') {
            condition.status = req.query.status;
        } else if (req.query.status === 'ALL') {
            // condition.status = {$ne: 'REJECTED'}
        }
    }
    condition.type = 'CASH';

    CashTransactionModel.paginate(condition, {
            page: page,
            limit: limit,
            sort: {createdDate: -1},
            populate: [
                {
                    path: 'memberId',
                    select: 'username_lower _id'
                },
                {
                    path: 'fromBank',
                    select: 'bankShortName'
                },
                {
                    path: 'agent',
                    select: 'cash name_lower',
                    populate: {
                        path: 'cash.banks.bank'
                    }
                },
                {
                    path: 'checkBy',
                    select: 'username_lower'
                },
                {
                    path: 'addCreditBy',
                    select: 'username_lower'
                },
                {
                    path: 'cancelBy',
                    select: 'username_lower'
                },
            ]
        },
        function (err, data) {
            if (err)
                return res.send({message: "error", results: err, code: 999});


            let transformList = _.map(data.docs, item => {

                let newItem = Object.assign({}, item)._doc;
                let bank = _.filter(item.agent.cash.banks, (bankItem) => {
                    return bankItem._id.toString() === item.toAgentBankId.toString();
                })[0];

                newItem.toAgentBank = bank;
                delete newItem.agent;
                return newItem;
            });

            data.docs = transformList;

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: data
                }
            );
        });
});

router.get('/withdraw/list', function (req, res) {

    let userInfo = req.userInfo;

    let page = req.query.page ? Number.parseInt(req.query.page) : 1;
    let limit = req.query.limit ? Number.parseInt(req.query.limit) : 50;

    let condition = {active: true, tranType: 'WITHDRAW'};

    if (req.query.username) {
        condition['member_name'] = req.query.username;
    }

    condition.agent = mongoose.Types.ObjectId(userInfo.group._id);

    condition.type = 'CASH';

    let orCondition;
    if (req.query.status) {
        if (req.query.status === 'WAITING_CHECKING_CREDIT') {
            orCondition = [{'$or': [{'status': 'WAITING_CHECKING'}, {'status': 'WAITING_TRANSFER'}]}];
            condition['$and'] = orCondition;
        } else if (req.query.status !== 'ALL') {
            condition.status = req.query.status;
        } else if (req.query.status === 'ALL') {
            // condition.status = {$ne: 'REJECTED'}
        }
    }

    CashTransactionModel.paginate(condition, {
            page: page,
            limit: limit,
            sort: {createdDate: -1},
            populate: [
                {
                    path: 'memberId',
                    select: 'username_lower _id'
                },
                {
                    path: 'fromBank',
                    select: 'bankShortName'
                },
                {
                    path: 'agent',
                    select: 'cash',
                    populate: {
                        path: 'cash.banks.bank'
                    }
                },
                {
                    path: 'checkBy',
                    select: 'username_lower'
                },
                {
                    path: 'transferBy',
                    select: 'username_lower'
                },
                {
                    path: 'cancelBy',
                    select: 'username_lower'
                },
            ]
        },
        function (err, data) {
            if (err)
                return res.send({message: "error", results: err, code: 999});


            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: data
                }
            );
        });
});

const withdrawSchema = Joi.object().keys({
    transferBankId: Joi.string().required(),
    amount: Joi.number().required(),
    accountName: Joi.string().allow(''),
    accountNumber: Joi.string().allow('')
});

router.post('/withdraw', function (req, res) {


    let validate = Joi.validate(req.body, withdrawSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            result: validate.error.details,
            code: 999
        });
    }

    let userInfo = req.userInfo;
    let requestBody = req.body;
    // let results =  WebsiteModel.find().exec();


    // WebsiteModel.find((err, data) => {
    //
    //     let t = data[0].isWithdraw ;
    //
    //     if (t == false) {
    //         return res.send({message: "error", result: err, code: 99900000});
    //     }
    //     console.log(data,'0000')
    // });
    // console.log(data,'111111');


    async.waterfall([callback => {
        MemberService.getCredit(userInfo._id, (err, credit) => {
            if (err) {
                callback(err, null);
            } else {
                if (requestBody.amount > credit) {
                    callback(4003, null);
                } else {
                    callback(null, credit);
                }
            }
        });

    }, (memberCredit, callback) => {

        AgentGroupModel.findById(userInfo.group._id, 'cash', (err, response) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, memberCredit, response);
            }
        });

    }, (memberCredit, agentInfo, callback) => {

        let withdrawCountCondition = {
            memberId: mongoose.Types.ObjectId(userInfo._id),
            status: {$ne: 'REJECTED'},
            tranType: 'WITHDRAW'
        };
        withdrawCountCondition['createdDate'] = {
            "$gte": moment().millisecond(0).second(0).minute(0).hour(0).utc(true).toDate(),
            "$lt": moment().millisecond(0).second(0).minute(0).hour(0).add(1,'d').utc(true).toDate()
        };

        CashTransactionModel.countDocuments(withdrawCountCondition, function (err, completeWithdrawCount) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, memberCredit, agentInfo, completeWithdrawCount);
            }

        }).lean();

    }, (memberCredit, agentInfo, completeWithdrawCount, callback) => {

        let condition = {};//{$or: [{'status': 'WAITING_CHECKING'}, {'status': 'WAITING_ADD_CREDIT'},{'status': 'WAITING_TRANSFER'}]};
        condition.memberId = mongoose.Types.ObjectId(userInfo._id);
        condition.tranType = 'WITHDRAW';


        CashTransactionModel.findOne(condition, (err, response) => {
            callback(null, memberCredit, agentInfo, completeWithdrawCount, response);
        }).sort({_id: -1}).limit(1);

    }], (err, memberCredit, agentInfo, completeWithdrawCount, lastTran) => {

        if (err) {
            if (err === 4003) {
                return res.send({message: "balance is not enough", result: null, code: 4003});
            }

            return res.send({message: err, result: err, code: 999});
        }

        const cashSetting = agentInfo.cash;
        const withdrawCount = completeWithdrawCount;
        const lastTransaction = lastTran;

        if (lastTransaction) {
            if (lastTransaction.status === 'WAITING_CHECKING') {
                return res.send({
                    message: "Wait Checking",
                    results: err,
                    code: 4002
                });
            }
            if (lastTransaction.status === 'WAITING_TRANSFER') {
                return res.send({
                    message: "Wait Withdraw",
                    results: err,
                    code: 4002
                });
            }
        }


        console.log('withdrawCount : ', withdrawCount)


        if (!cashSetting.isMemberWithdraw) {
            return res.send({message: "withdraw close", results: err, code: 4001});
        }

        if (withdrawCount > cashSetting.withdrawCountDay) {
            return res.send({
                message: "Maximum Withdrawal Count/Day " + cashSetting.withdrawCountDay,
                results: err,
                code: 4001
            });
        }

        if (requestBody.amount > cashSetting.withdrawMax) {
            return res.send({
                message: "Withdrawal Maximum " + cashSetting.withdrawMax,
                results: err,
                code: 4001
            });
        }
        if (requestBody.amount < cashSetting.withdrawMin) {
            return res.send({
                message: "Withdrawal Minimum " + cashSetting.withdrawMin,
                results: err,
                code: 4001
            });
        }

        let body = {
            ref: moment().toDate().getTime(),
            agent: userInfo.group._id,
            memberId: userInfo._id,
            member_name: userInfo.username_lower,
            fromBank: requestBody.transferBankId,
            fromBankAccountName: requestBody.accountName,
            fromBankAccountNumber: requestBody.accountNumber,
            processDateTime: DateUtils.getCurrentDate(),
            amount: requestBody.amount,
            status: 'WAITING_CHECKING',
            active: true,
            tranType: 'WITHDRAW',
            type: 'CASH',
            createdDate: DateUtils.getCurrentDate()
        };

        let model = new CashTransactionModel(body);
        model.save((err, response) => {

            if (err) {
                return res.send({code: 999, message: err});
            }

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: req.body
                }
            );

        });
    });

});

router.get('/member/info', function (req, res) {

    async.parallel([callback => {

        MemberService.getInfo(req.query.memberId, function (err, credit) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, credit);
            }
        });
    }], function (err, asyncResponse) {
        if (err)
            return res.send({message: "error", result: err, code: 999});

        return res.send(
            {
                code: 0,
                message: "success",
                result: asyncResponse[0]
            }
        );
    });
});

router.get('/report', function (req, res) {

    let userInfo = req.userInfo;

    let page = req.query.page ? Number.parseInt(req.query.page) : 1;
    let limit = req.query.limit ? Number.parseInt(req.query.limit) : 50;

    let condition = {active: true, status: {$ne: 'REJECTED'}};

    condition.agent = mongoose.Types.ObjectId(userInfo.group._id);

    if (req.query.type) {
        condition['tranType'] = req.query.type;
    }

    if (req.query.date) {
        condition['createdDate'] = {
            "$gte": moment(req.query.date, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).utc(true).toDate(),
            "$lt":moment(req.query.date, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).add(1,'d').utc(true).toDate()
        }
    }

    condition.type = 'CASH';

    if (req.query.type === 'DEPOSIT') {

        async.series([(callback) => {

            AgentGroupModel.findById(mongoose.Types.ObjectId(userInfo.group._id), 'cash')
                .lean()
                .populate('cash.banks.bank')
                .exec(function (err, response) {
                    if (err) {
                        callback(err, null);
                        return;
                    }
                    callback(null, response);

                });
        }, (callback) => {

            CashTransactionModel.countDocuments(condition, function (err, data) {
                if (err) {
                    callback(err, null);
                    return;
                }
                callback(null, data);

            }).lean();
        }], (err, results) => {

            let castSetting = results[0];
            let count = results[1];

            CashTransactionModel.aggregate([
                {
                    $match: condition
                },
                // {
                //     $skip: (page - 1) * limit
                // }, {
                //     $limit: limit
                // },
                {
                    $group: {
                        _id: {
                            agentBankId: '$toAgentBankId'
                        },
                        accountName: {$first: "$accountName"},
                        waitChecking: {$sum: {$cond: [{$eq: ['$status', 'WAITING_CHECKING']}, '$amount', 0]}},
                        waitAddCredit: {$sum: {$cond: [{$in: ['$status', ['WAITING_ADD_CREDIT', 'WAITING_TRANSFER']]}, '$amount', 0]}},
                        addCreditSuccess: {$sum: {$cond: [{$eq: ['$status', 'COMPLETE']}, '$amount', 0]}}
                    }
                },
                {
                    $project: {
                        "agentBankId": '$_id.agentBankId',
                        "waitChecking": "$waitChecking",
                        "waitAddCredit": "$waitAddCredit",
                        "addCreditSuccess": "$addCreditSuccess"
                    }
                }

            ], (err, results) => {

                if (err)
                    return res.send({message: "error", results: err, code: 999});

                let transformList = _.map(results, item => {

                    let newItem = Object.assign({}, item);
                    let bank = _.filter(castSetting.cash.banks, (bankItem) => {
                        return bankItem._id.toString() === item.agentBankId.toString();
                    })[0];

                    if (bank) {
                        newItem.agentBank = bank;
                    }
                    return newItem;
                });

                console.log(transformList)

                let summary = _.reduce(results, function (memo, obj) {
                    return {
                        waitChecking: memo.waitChecking + obj.waitChecking,
                        waitAddCredit: memo.waitAddCredit + obj.waitAddCredit,
                        addCreditSuccess: memo.addCreditSuccess + obj.addCreditSuccess,
                    }
                }, {
                    waitChecking: 0,
                    waitAddCredit: 0,
                    addCreditSuccess: 0
                });
                return res.send(
                    {
                        code: 0,
                        message: "success",
                        result: {
                            total: count,
                            limit: limit,
                            page: page,
                            pages: Math.ceil(count / limit) || 1,
                            dataList: transformList,
                            summary: summary
                        }
                    }
                );
            });

            // });
        });
    } else {

        condition.status = 'COMPLETE';

        console.log(condition)

        async.series([(callback) => {

            CashTransactionModel.countDocuments(condition, function (err, data) {
                if (err) {
                    callback(err, null);
                    return;
                }
                callback(null, data);

            }).lean();
        }], (err, results) => {

            let count = results[0];
            console.log('count : ', count)

            CashTransactionModel.aggregate([
                {
                    $match: condition
                },
                // {
                //     $skip: (page - 1) * limit
                // }, {
                //     $limit: limit
                // },
                {
                    $group: {
                        _id: {
                            fromBankAccountNumber: '$fromBankAccountNumber'
                        },
                        accountName: {$first: "$fromBankAccountName"},
                        bank: {$first: "$fromBank"},
                        addCreditSuccess: {$sum: '$amount'}
                    }
                },
                {
                    $project: {
                        "accountNumber": '$_id.fromBankAccountNumber',
                        "accountName": "$accountName",
                        "bank": "$bank",
                        "addCreditSuccess": "$addCreditSuccess"
                    }
                }

            ], (err, results) => {

                if (err)
                    return res.send({message: "error", results: err, code: 999});

                // let transformList = _.map(results, item => {
                //
                //     let newItem = Object.assign({}, item);
                //     let bank = _.filter(castSetting.cash.banks, (bankItem) => {
                //         return bankItem._id.toString() === item.agentBank.toString();
                //     })[0];
                //
                //     if (bank) {
                //         newItem.agentBank = bank;
                //     }
                //     return newItem;
                // });

                BankModel.populate(results, {
                    path: 'bank',
                    select: '_id bankShortName bankName'
                }, function (err, memberResult) {
                    if (err) {
                        if (err)
                            return res.send({message: "error", results: err, code: 999});
                    } else {
                        let summary = _.reduce(results, function (memo, obj) {
                            return {
                                waitChecking: memo.waitChecking + obj.waitChecking,
                                waitAddCredit: memo.waitAddCredit + obj.waitAddCredit,
                                addCreditSuccess: memo.addCreditSuccess + obj.addCreditSuccess,
                            }
                        }, {
                            waitChecking: 0,
                            waitAddCredit: 0,
                            addCreditSuccess: 0
                        });
                        return res.send(
                            {
                                code: 0,
                                message: "success",
                                result: {
                                    total: count,
                                    limit: limit,
                                    page: page,
                                    pages: Math.ceil(count / limit) || 1,
                                    dataList: results,
                                    summary: summary
                                }
                            }
                        );
                    }
                });
            });

        });
    }
});

router.get('/report/deposit/detail', function (req, res) {

    let userInfo = req.userInfo;

    let page = req.query.page ? Number.parseInt(req.query.page) : 1;
    let limit = req.query.limit ? Number.parseInt(req.query.limit) : 50;

    let condition = {active: true};

    if (req.query.date) {
        condition['createdDate'] = {
            "$gte": moment(req.query.date, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).utc(true).toDate(),
            "$lt":moment(req.query.date, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).add(1,'d').utc(true).toDate()
        }
    }

    if (req.query.agentBankId) {
        condition['toAgentBankId'] = req.query.agentBankId;
    }

    if (req.query.memberId) {
        condition['memberId'] = mongoose.Types.ObjectId(req.query.memberId);
    }

    if (req.query.status) {
        condition['status'] = req.query.status;
    }

    condition.agent = mongoose.Types.ObjectId(userInfo.group._id);


    CashTransactionModel.paginate(condition, {
            page: page,
            limit: limit,
            sort: {createdDate: -1},
            populate: [
                {
                    path: 'memberId',
                    select: 'username_lower _id'
                },
                {
                    path: 'agent',
                    select: 'cash',
                    populate: {
                        path: 'cash.banks.bank'
                    }
                }
            ]
        },
        function (err, data) {
            if (err)
                return res.send({message: "error", results: err, code: 999});


            let summary = _.reduce(data.docs, function (memo, obj) {
                return {
                    amount: memo.amount + obj.amount
                }
            }, {
                amount: 0
            });
            data.summary = summary;

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: data
                }
            );
        });
});

router.get('/report/withdraw/detail', function (req, res) {

    let userInfo = req.userInfo;

    let page = req.query.page ? Number.parseInt(req.query.page) : 1;
    let limit = req.query.limit ? Number.parseInt(req.query.limit) : 50;

    let condition = {active: true};

    if (req.query.date) {
        condition['createdDate'] = {
            "$gte": moment(req.query.date, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).utc(true).toDate(),
            "$lt":moment(req.query.date, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).add(1,'d').utc(true).toDate()
        }
    }

    if (req.query.accountNumber) {
        condition['fromBankAccountNumber'] = req.query.accountNumber;
    }
    condition['tranType'] = 'WITHDRAW';
    condition['status'] = 'COMPLETE';
    condition.agent = mongoose.Types.ObjectId(userInfo.group._id);


    CashTransactionModel.paginate(condition, {
            page: page,
            limit: limit,
            sort: {createdDate: -1},
            populate: [
                {
                    path: 'memberId',
                    select: 'username_lower _id'
                }]
        },
        function (err, data) {
            if (err)
                return res.send({message: "error", results: err, code: 999});


            let summary = _.reduce(data.docs, function (memo, obj) {
                return {
                    amount: memo.amount + obj.amount
                }
            }, {
                amount: 0
            });
            data.summary = summary;

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: data
                }
            );
        });
});

router.get('/report/user', function (req, res) {

    let userInfo = req.userInfo;
    let page = req.query.page ? Number.parseInt(req.query.page) : 1;
    let limit = req.query.limit ? Number.parseInt(req.query.limit) : 50;

    let condition = {active: true, status: 'COMPLETE'};


    condition.agent = mongoose.Types.ObjectId(userInfo.group._id);

    if (req.query.username) {
        condition['member_name'] = req.query.username;
    }

    if (req.query.dateFrom && req.query.dateTo && req.query.timeFrom && req.query.timeTo) {
        condition['createdDate'] = {
            "$gte": DateUtils.convertDateTime(req.query.dateFrom,req.query.timeFrom),
            "$lt": DateUtils.convertDateTime(req.query.dateTo,req.query.timeTo)
        };
    }
    else {
        condition['createdDate'] = {
            "$gte": DateUtils.convertDateTime(req.query.dateFrom,'00:00:00'),
            "$lt": DateUtils.convertDateTime(req.query.dateTo,'00:00:00')
        };
    }

    console.log(condition,'33333');

    // condition.type = 'CASH';

    async.series([(callback) => {

        CashTransactionModel.countDocuments(condition, function (err, data) {
            if (err) {
                callback(err, null);
                return;
            }
            callback(null, data);

        }).lean();
    }], (err, results) => {

        let count = results[0];
        console.log('count : ', count)
        CashTransactionModel.aggregate([
            {
                $match: condition
            },
            {
                $skip: (page - 1) * limit
            }, {
                $limit: limit
            },
            {
                $group: {
                    _id: {
                        member: '$memberId'
                    },
                    depositCount: {$sum: {$cond: [{$eq: ['$tranType', 'DEPOSIT']}, 1, 0]}},
                    withdrawCount: {$sum: {$cond: [{$eq: ['$tranType', 'WITHDRAW']}, 1, 0]}},
                    depositAmount: {$sum: {$cond: [{$eq: ['$tranType', 'DEPOSIT']}, '$amount', 0]}},
                    withdrawAmount: {$sum: {$cond: [{$eq: ['$tranType', 'WITHDRAW']}, '$amount', 0]}}
                }
            },
            {
                $project: {
                    "member": '$_id.member',
                    "depositCount": "$depositCount",
                    "withdrawCount": "$withdrawCount",
                    "depositAmount": "$depositAmount",
                    "withdrawAmount": "$withdrawAmount"
                }
            }

        ], (err, results) => {

            if (err)
                return res.send({message: "error", results: err, code: 999});

            MemberModel.populate(results, {
                path: 'member',
                select: '_id username username_lower contact'
            }, function (err, memberResult) {

                return res.send(
                    {
                        code: 0,
                        message: "success",
                        result: {
                            total: count,
                            limit: limit,
                            page: page,
                            pages: Math.ceil(count / limit) || 1,
                            dataList: results
                        }
                    }
                );
            });

        });
    });
});

const modifyCreditSchema = Joi.object().keys({
    memberType: Joi.string().required(),
    modifyType: Joi.string().required(),
    id: Joi.string().required(),
    amount: Joi.number().required(),
    ref: Joi.string().allow('', null),
});

router.post('/credit/modify', function (req, res) {

    let validate = Joi.validate(req.body, modifyCreditSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    let requestBody = req.body;
    let userInfo = req.userInfo;

    console.log(userInfo)


    UserModel.findOne({_id:userInfo._id},(err,checkAgentResponse)=>{


        if(!checkAgentResponse) {

            return res.send({
                message: "UnAuthorization",
                result: err,
                code: 401
            });


        }else {

            async.parallel([callback => {

                AgentService.findById(userInfo.group._id, function (err, response) {
                    if (err) {
                        callback(err, '');
                    } else {
                        callback(null, response);
                    }
                });
            }, callback => {

                AgentService.getCreditLimitUsage(userInfo.group._id, function (err, response) {
                    if (err) {
                        callback(err, '');
                    } else {
                        callback(null, response);
                    }
                });

            }, callback => {

                if (requestBody.memberType === 'AGENT') {
                    AgentService.findById(requestBody.id, function (err, response) {
                        if (err) {
                            callback(err, '');
                        } else {
                            callback(null, response);
                        }
                    });

                } else {

                    AgentService.isAllowPermission(userInfo._id,requestBody.id,(err,isAllow)=>{

                        if (!isAllow) {
                            callback(401, null);
                        } else {
                            MemberService.findById(requestBody.id, 'creditLimit', function (err, response) {
                                if (err) {
                                    callback(err, '');
                                } else {
                                    callback(null, response);
                                }
                            });
                        }

                    });

                }
            }, callback => {
                if (requestBody.memberType === 'AGENT') {
                    AgentService.getCreditLimitUsage(requestBody.id, function (err, response) {
                        if (err) {
                            callback(err, '');
                        } else {
                            callback(null, response);
                        }
                    });
                } else {
                    callback(null, {});
                }

            }], (err, asyncResponse) => {

                if (err) {
                    return res.send({message: "err", result: err, code: 999});
                }

                const agentInfo = asyncResponse[0];
                const creditLimitUsage = asyncResponse[1];
                const editAgentInfo = asyncResponse[2];
                const editCreditLimitUsage = asyncResponse[3];

                let amount = requestBody.amount;


                if (requestBody.modifyType === 'ADD') {
                    amount = Math.abs(amount);
                } else {
                    amount = amount * -1;
                }

                console.log('creditLimit : ', agentInfo.creditLimit);
                console.log('creditLimitUsage : ', creditLimitUsage);


                if (amount > (agentInfo.creditLimit - creditLimitUsage)) {
                    return res.send({message: "creditLimit exceeded", result: err, code: 4002});
                }


                if (requestBody.memberType === 'AGENT') {

                    if ((amount + editAgentInfo.creditLimit) > agentInfo.maxCreditLimit) {
                        return res.send({message: "creditLimit exceeded", result: err, code: 4030});
                    }

                    if (requestBody.modifyType === 'REDUCE') {

                        if ((amount + editAgentInfo.creditLimit) < editCreditLimitUsage) {
                            return res.send({
                                message: "creditLimit exceeded",
                                result: err,
                                code: 4028
                            });
                        }
                    }


                    AgentGroupModel.update({_id: mongoose.Types.ObjectId(requestBody.id)}, {$inc: {creditLimit: amount}}, (err, reponse) => {
                        return res.send(
                            {
                                code: 0,
                                message: "success",
                                result: true
                            }
                        );
                    });


                } else if (requestBody.memberType === 'MEMBER') {


                    if ((amount + editAgentInfo.creditLimit) > agentInfo.maxCreditLimit) {
                        return res.send({message: "creditLimit exceeded", result: err, code: 4030});
                    }

                    MemberService.findById(requestBody.id, '', (err, memberResponse) => {

                        if (err) {
                            return res.send({message: "err", result: err, code: 999});
                        }

                        if (requestBody.modifyType === 'REDUCE') {

                            let afterUpdateCreditLimit = memberResponse.creditLimit - requestBody.amount;

                            if (afterUpdateCreditLimit < 0) {
                                return res.send({
                                    message: "credit < balance",
                                    result: err,
                                    code: 4002
                                });
                            }

                            if (memberResponse.balance < 0 && (afterUpdateCreditLimit < Math.abs(memberResponse.balance))) {
                                return res.send({
                                    message: "credit < balance",
                                    result: err,
                                    code: 4002
                                });
                            }
                        }


                        let historyBody = {
                            memberId: memberResponse._id,
                            type: 'ADD',
                            ref: requestBody.ref,
                            beforeCredit: memberResponse.creditLimit,
                            // afterCredit: memberUpdateResponse.creditLimit,
                            beforeBalance: memberResponse.balance,
                            // afterBalance: memberUpdateResponse.balance,
                            amount: amount,
                            createdDate: DateUtils.getCurrentDate()
                        };

                        let creditModifyModel = new CreditModifyHistoryModel(historyBody);
                        creditModifyModel.save((err, creditModifyResponse) => {

                            if (err) {
                                return res.send({code: 0, message: "success", result: true});
                            }

                            let cashBody = {
                                ref: requestBody.ref,
                                agent: userInfo.group._id,
                                addCreditBy: userInfo._id,
                                memberId: memberResponse._id,
                                member_name: memberResponse.username_lower,
                                transferType: requestBody.transferType,
                                processDateTime: DateUtils.getCurrentDate(),
                                amount: requestBody.amount,
                                status: 'COMPLETE',
                                active: true,
                                tranType: requestBody.modifyType === 'ADD' ? 'DEPOSIT' : 'WITHDRAW',
                                type: 'MANUAL',
                                createdDate: DateUtils.getCurrentDate(),
                            };

                            let cashModel = new CashTransactionModel(cashBody);
                            cashModel.save((err, HistoryResponse) => {

                                if (err) {
                                    return res.send({code: 999, message: "fail", result: true});
                                }

                                MemberModel.findOneAndUpdate({_id: mongoose.Types.ObjectId(requestBody.id)}, {$inc: {creditLimit: amount}}, {new: true}, (err, memberUpdateResponse) => {

                                    if (err) {
                                        return res.send({message: "err", result: err, code: 999});
                                    }
                                    let body = {
                                        afterCredit: roundTo(memberUpdateResponse.creditLimit, 3),
                                        afterBalance: roundTo(memberUpdateResponse.balance, 3)
                                    };

                                    CreditModifyHistoryModel.update({_id: creditModifyResponse._id}, body, (err, updateCreditRes) => {
                                        // callback(null, memberUpdateResponse)
                                        return res.send(
                                            {
                                                code: 0,
                                                message: "success",
                                                result: true
                                            }
                                        );
                                    });


                                });
                            });

                        });

                    });


                } else {
                    return res.send({message: "invalid type", result: err, code: 999});
                }

            });

        }
    });




});

const modifyManualSchema = Joi.object().keys({
    memberId: Joi.string().required(),
    amount: Joi.number().required(),
    ref: Joi.string().required(),
    modifyType: Joi.string().required()
});

router.post('/credit/modify_manual', function (req, res) {

    let validate = Joi.validate(req.body, modifyManualSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            result: validate.error.details,
            code: 999
        });
    }

    let userInfo = req.userInfo;
    let requestBody = req.body;



    async.series([callback => {

        AgentService.isAllowPermission(userInfo._id,requestBody.memberId,(err,isAllow)=> {
            if (!isAllow) {
                callback(401, null);
            } else {
                callback(null, 'success');
            }
        });

    },callback => {

        if (requestBody.modifyType === 'ADD') {

            addMemberCreditFunction(userInfo.group._id, requestBody.memberId, requestBody.amount, requestBody.ref, (err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            })
        } else if (requestBody.modifyType === 'REDUCE') {

            reduceMemberCreditFunction(userInfo.group._id, requestBody.memberId, requestBody.amount, requestBody.ref, (err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            });

        } else {
            callback(null, 'success');
        }

    }], (err, results) => {

        if (err) {
            if (err == 5001) {
                return res.send({
                    message: "balance is not enough ",
                    result: null,
                    code: 4002
                });
            }
            if (err == 4002) {
                return res.send({
                    message: "เครดิิตไม่เพียงพอ",
                    result: null,
                    code: 4002
                });
            }

            if (err == 401) {
                return res.send({
                    message: "Unauthorization",
                    result: null,
                    code: 401
                });
            }
            return res.send({message: "error", result: err, code: 999});
        }

        let memberInfo = results[1];

        let cashBody = {
            ref: requestBody.ref,
            agent: userInfo.group._id,
            addCreditBy: userInfo._id,
            memberId: requestBody.memberId,
            member_name: memberInfo.username_lower,
            transferType: requestBody.transferType,
            processDateTime: DateUtils.getCurrentDate(),
            amount: requestBody.amount,
            status: 'COMPLETE',
            active: true,
            tranType: requestBody.modifyType === 'ADD' ? 'DEPOSIT' : 'WITHDRAW',
            type: 'MANUAL',
            createdDate: DateUtils.getCurrentDate(),
        };

        let cashModel = new CashTransactionModel(cashBody);
        cashModel.save((err, HistoryResponse) => {

            if (err) {
                return res.send({code: 999, message: "fail", result: true});
            }

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: true
                }
            );
        });
    });

});

router.get('/memberWinLose', function (req, res) {

    let userInfo = req.userInfo;


    let requestBody = req.query;

    CashTransactionModel.findOne({
        member_name: requestBody.username.toLowerCase(),
        ref: requestBody.ref
    }).exec((err, cashTransactionResponse) => {

        if (err) {
            return res.send({code: 999, message: "Internal server error"});
        } else {

            if (cashTransactionResponse) {

                CashService.getWinLoseReportMember(cashTransactionResponse.memberId, cashTransactionResponse.processDateTime, (err, wlResponse) => {
                    if (err) {
                        return res.send({code: 999, message: "Internal server error"});
                    } else {


                        return res.send(
                            {
                                code: 0,
                                message: "success",
                                result: {
                                    username: requestBody.username,
                                    ref: requestBody.ref,
                                    data: wlResponse
                                }
                            }
                        );

                    }
                });
            } else {
                return res.send({code: 999, message: "Data Not Found"});
            }
        }

    });

});


function addMemberCreditFunction(agentId, memberId, amount, ref, callback) {
    async.parallel([callback => {
        AgentService.findById(agentId, function (err, response) {
            if (err) {
                callback(err, '');
            } else {
                callback(null, response);
            }
        });
    }, callback => {
        AgentService.getCreditLimitUsage(agentId, function (err, response) {
            if (err) {
                callback(err, '');
            } else {
                callback(null, response);
            }
        });
    }, callback => {
        MemberService.getOutstanding(memberId, function (err, response) {
            if (err) {
                callback(err, '');
            } else {
                callback(null, response);
            }
        });
    }], (err, asyncResponse) => {
        if (err) {
            callback(err, null);
        }

        const agentInfo = asyncResponse[0];
        const creditLimitUsage = asyncResponse[1];
        const memberOutstanding = asyncResponse[2];

        console.log('creditLimit : ', agentInfo.creditLimit);
        console.log('creditLimitUsage : ', creditLimitUsage);
        console.log('amount : ', amount)

        if (amount > (agentInfo.creditLimit - creditLimitUsage)) {
            callback(4002, null);
            return;
        }

        MemberService.findById(memberId, '', function (err, memberResponse) {

            let memberBalanceWithoutOutstanding = memberResponse.balance + memberOutstanding;


            let creditIncreaseAmount = memberBalanceWithoutOutstanding + amount;
            console.log('increaseAmount : ', amount)
            console.log('memberOutstanding : ', memberOutstanding)
            console.log('memberBalanceWithoutOutstanding : ', memberBalanceWithoutOutstanding)
            console.log('creditIncreaseAmount : ', creditIncreaseAmount);


            let historyBody = {
                memberId: memberId,
                type: 'ADD',
                ref: ref,
                beforeCredit: memberResponse.creditLimit,
                // afterCredit: memberUpdateResponse.creditLimit,
                beforeBalance: memberResponse.balance,
                // afterBalance: memberUpdateResponse.balance,
                amount: amount,
                createdDate: DateUtils.getCurrentDate()
            };

            let creditModifyModel = new CreditModifyHistoryModel(historyBody);

            creditModifyModel.save((err, creditModifyResponse) => {

                if (err) {
                    callback(err, null);
                    return;
                }

                let modifyBody = {
                    $inc: {
                        balance: (memberBalanceWithoutOutstanding * -1),
                        creditLimit: creditIncreaseAmount
                    }
                };

                MemberModel.findOneAndUpdate({_id: mongoose.Types.ObjectId(memberId)}, modifyBody, {new: true}, (err, memberUpdateResponse) => {

                    if (err) {
                        callback(err, null);
                    } else {

                        let body = {
                            afterCredit: roundTo(memberUpdateResponse.creditLimit, 3),
                            afterBalance: roundTo(memberUpdateResponse.balance, 3)
                        };

                        CreditModifyHistoryModel.update({_id: creditModifyResponse._id}, body, (err, updateCreditRes) => {
                            callback(null, memberUpdateResponse)
                        });

                    }
                });
            });

        });


    });
}

function reduceMemberCreditFunction(agentId, memberId, amount, ref, callback) {

    async.parallel([callback => {
        AgentService.findById(agentId, function (err, response) {
            if (err) {
                callback(err, '');
            } else {
                callback(null, response);
            }
        });
    }, callback => {
        AgentService.getCreditLimitUsage(agentId, function (err, response) {
            if (err) {
                callback(err, '');
            } else {
                callback(null, response);
            }
        });
    }], (err, asyncResponse) => {
        if (err) {
            callback(err, null);
        }

        const agentInfo = asyncResponse[0];
        const creditLimitUsage = asyncResponse[1];

        console.log('creditLimit : ', agentInfo.creditLimit);
        console.log('creditLimitUsage : ', creditLimitUsage);
        console.log('amount : ', amount)

        if (amount > (agentInfo.creditLimit - creditLimitUsage)) {
            callback(4002, null);
            return;
        }

        MemberService.findById(memberId, '', function (err, memberResponse) {


            let memberBalanceWithoutOutstanding = memberResponse.balance;


            let creditIncreaseAmount = memberBalanceWithoutOutstanding - amount;
            console.log('increaseAmount : ', amount);
            console.log('creditIncreaseAmount : ', creditIncreaseAmount);


            let finalCredit = (creditIncreaseAmount + memberResponse.creditLimit );

            if ((creditIncreaseAmount + memberResponse.creditLimit ) < 0) {
                callback(5001, null);
                return;
            }

            if ((memberResponse.balance + memberResponse.creditLimit ) < amount) {
                callback(5001, null);
                return;
            }


            let historyBody = {
                memberId: memberId,
                type: 'REDUCE',
                ref: ref,
                beforeCredit: memberResponse.creditLimit,
                // afterCredit: memberUpdateResponse.creditLimit,
                beforeBalance: memberResponse.balance,
                // afterBalance: memberUpdateResponse.balance,
                amount: amount,
                createdDate: DateUtils.getCurrentDate()
            };

            let creditModifyModel = new CreditModifyHistoryModel(historyBody);

            creditModifyModel.save((err, creditModifyResponse) => {

                if (err) {
                    callback(err, null);
                    return;
                }

                let modifyBody = {
                    $inc: {
                        balance: (memberBalanceWithoutOutstanding * -1),
                        creditLimit: creditIncreaseAmount
                    }
                };


                MemberModel.findOneAndUpdate({_id: mongoose.Types.ObjectId(memberId)}, modifyBody, {new: true}, (err, memberUpdateResponse) => {

                    let body = {
                        afterCredit: roundTo(parseFloat(memberUpdateResponse.creditLimit), 3),
                        afterBalance: roundTo(parseFloat(memberUpdateResponse.balance), 3)
                    };

                    CreditModifyHistoryModel.update({_id: creditModifyResponse._id}, body, (err, updateCreditRes) => {
                        callback(null, memberUpdateResponse)
                    });


                });

            });
        });


    });
}


module.exports = router;