const CommissionTypeModel = require('../../models/commissionType.model');
const express = require('express');
const router = express.Router();
const async = require("async");
const Joi = require('joi');
const mongoose = require('mongoose');
const config = require('config');


router.get('/:userId', function (req, res) {


    CommissionTypeModel.findById(req.params.userId,function (err, data) {
            if (err)
                return res.send({message: "error", result: err, code: 999});

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: data
                }
            );
        });

});



const addComTypeSchema = Joi.object().keys({
    type: Joi.string().required(),
    torValue: Joi.string().required(),
    longValue: Joi.string().required(),
});

router.post('/commissionType', function (req, res) {

    let validate = Joi.validate(req.body, addComTypeSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }


    let body = {
        type: req.body.type,
        torValue: req.body.torValue,
        longValue: req.body.longValue,
        active: true
    };


    let model = new CommissionTypeModel(body);

    model.save(function (err, data) {

        if (err) {
            return res.send({message: "error", results: err, code: 999});
        }
        return res.send(
            {
                code: 0,
                message: "success",
                result: data
            }
        );
    });

});


const updateUserSchema = Joi.object().keys({
    transactionCode: Joi.string().allow(''),
    contact: Joi.string().required(),
    permission: Joi.array().items(
        Joi.string().label('ALL').allow(),
        Joi.string().label('STOCK_MANAGEMENT').allow(),
        Joi.string().label('MEMBER_MANAGEMENT').allow(),
        Joi.string().label('MEMBER_MANAGEMENT_VIEW').allow(),
        Joi.string().label('REPORT').allow(),
        Joi.string().label('OLD_REPORT').allow(),
        Joi.string().label('PAYMENT').allow(),
        Joi.string().label('CORRECT_SCORE').allow())
});

router.put('/:userId', function (req, res) {

    let validate = Joi.validate(req.body, updateUserSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    let body = {
        contact: req.body.contact,
        permission: req.body.permission,
        active: true
    };

    UserModel.update({_id: mongoose.Types.ObjectId(req.params.userId)}, body, function (err, data) {

        if (err) {
            return res.send({message: "error", results: err, code: 999});
        }
        return res.send(
            {
                code: 0,
                message: "success",
                result: data
            }
        );
    });

});

module.exports = router;