const MemberModel = require('../../models/member.model');
const BetTransactionModel = require('../../models/betTransaction.model.js');
const UserModel = require("../../models/users.model");
const AgentGroupModel = require('../../models/agentGroup.model.js');
const MemberService = require('../../common/memberService');
const express = require('express');
const router = express.Router();
const async = require("async");
const Joi = require('joi');
const mongoose = require('mongoose');
const config = require('config');
const _ = require('underscore');
const moment = require('moment');
const naturalSort = require("javascript-natural-sort");
const roundTo = require('round-to');
const ApiService = require('../../common/apiService');
const Hashing = require('../../common/hashing');
const NumberUtils = require('../../common/numberUtils1');
const DateUtils = require('../../common/dateUtils');
const StockHdpService = require('../../common/stockHdpService');
const ReportService = require('../../common/reportService');
// const AdjustmentByMatch = require('../../models/oddAdjustmentByMatch.model');

router.get('/list', function (req, res) {

    // let condition = {game: 'FOOTBALL'};
    let condition = {};

    let userInfo = req.userInfo;

    if (req.query.dateFrom && req.query.dateTo) {
        condition['createdDate'] = {
            "$gte": moment(req.query.dateFrom, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).utc(true).toDate(),
            "$lt": moment(req.query.dateTo, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).add(1,'d').utc(true).toDate()
        }
    }


    let orCondition = [];

    if (req.query.status) {
        orCondition.push({
            $or: req.query.status.split(',').map(function (status) {
                return {'status': status}
            })
        });
    }

    if (req.query.oddType && req.query.oddType !== 'ALL') {
        if (req.query.gameType === 'PARLAY') {
            condition['gameType'] = 'PARLAY';
        } else if (req.query.gameType === 'MIX_STEP') {
            condition['gameType'] = 'MIX_STEP';
        } else if (req.query.gameType === 'TODAY') {
            condition['gameType'] = 'TODAY';
            if (req.query.oddType === 'HDP') {
                orCondition.push({$or: [{'hdp.oddType': 'AH'}, {'hdp.oddType': 'AH1ST'}]})
            } else if (req.query.oddType === 'OU') {
                orCondition.push({$or: [{'hdp.oddType': 'OU'}, {'hdp.oddType': 'OU1ST'}]})
            } else if (req.query.oddType === 'OE') {
                orCondition.push({$or: [{'hdp.oddType': 'OE'}, {'hdp.oddType': 'OE1ST'}]})
            } else if (req.query.oddType === 'X12') {
                orCondition.push({$or: [{'hdp.oddType': 'X12'}, {'hdp.oddType': 'X121ST'}]})
            } else {
                condition['hdp.oddType'] = req.query.oddType;
            }
        }
    }

    if (orCondition.length > 0) {
        condition['$and'] = orCondition;
    }

    let group = '';
    let child = '';
    if (userInfo.group.type === 'SUPER_ADMIN') {
        group = 'superAdmin';
        child = 'company';
    } else if (userInfo.group.type === 'COMPANY') {
        group = 'company';
        child = 'shareHolder';
    } else if (userInfo.group.type === 'SHARE_HOLDER') {
        group = 'shareHolder';
        child = 'superSenior';
    } else if (userInfo.group.type === 'SUPER_SENIOR') {
        group = 'superSenior';
        child = 'senior';
    } else if (userInfo.group.type === 'SENIOR') {
        group = 'senior';
        child = 'masterAgent';
    } else if (userInfo.group.type === 'MASTER_AGENT') {
        group = 'masterAgent';
        child = 'agent';
    } else if (userInfo.group.type === 'AGENT') {
        group = 'agent';
        child = 'member';
    }

    condition['$or'] = [{game: 'FOOTBALL'}, {game: 'M2'}]

    // condition['game'] = {$in : ['FOOTBALL', 'M2']};
    condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(userInfo.group._id);
    condition['active'] = true;

    console.log(condition,'121212');
    BetTransactionModel.aggregate([
        {
            $match: condition
        },
        {
            $project: {
                _id: 0,
                memberId: '$memberId',
                hdp: '$hdp',
                parlay: '$parlay',
                commission: '$commission.' + group,
                memberCommPercent: '$commission.member.commission',
                memberCommission: {$multiply: ['$amount', {$divide: ['$commission.member.commission', 100]}]},
                memberCreditUsage: {$abs: '$memberCredit'},
                child: '$commission.' + child,
                amount: '$amount',
                payout: '$payout',
                status: '$status',
                totalReceive: {$multiply: ['$amount', {$divide: ['$commission.' + group + '.shareReceive', 100]}]},
                stake_in: '$amount',
                stake_out: {
                    $cond: [{$eq: ['$gameType', 'TODAY']}, {$multiply: ['$amount', '$hdp.odd']}, {$multiply: ['$amount', '$parlay.odds']}]
                },
                betId: '$betId',
                createdDate: '$createdDate',
                priceType: '$priceType',
                gameType: '$gameType',
                ipAddress: '$ipAddress',
                isPrinted: '$isPrinted',
                codeOfflineTicket: '$codeOfflineTicket',
                isOfflineCash:'$isOfflineCash',
            }
        }
    ], (err, results) => {
        // console.log(results, '131313')
        if (err)
            return res.send({message: "error", results: err, code: 999});


        async.series([(callback => {

            MemberModel.populate(results, {
                path: 'memberId',
                select: '_id username'
            }, function (err, memberResult) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, memberResult);
                }
            });


        }), (callback => {

            AgentGroupModel.populate(results, {
                path: 'commission.group',
                select: '_id name'
            }, function (err, memberResult) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, memberResult);
                }
            });

        }), (callback => {

            AgentGroupModel.populate(results, {
                path: 'child.group',
                select: '_id name'
            }, function (err, memberResult) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, memberResult);
                }
            });

        })], (err, populateResults) => {

            if (err)
                return res.send({message: "error", results: err, code: 999});

            let summary = _.reduce(results, function (memo, num) {
                return {
                    totalAmount: memo.totalAmount + num.amount,
                    totalReceive: memo.totalReceive + num.totalReceive
                }
            }, {totalAmount: 0, totalReceive: 0});

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: {
                        betList: results,
                        summary: summary,
                    }
                }
            );
        });
    });

});


router.get('/list2', function (req, res) {

    let condition = {};

    let userInfo = req.userInfo;

    if (req.query.dateFrom && req.query.dateTo) {
        condition['createdDate'] = {
            "$gte": moment(req.query.dateFrom, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).utc(true).toDate(),
            "$lt": moment(req.query.dateTo, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).add(1,'d').utc(true).toDate()
        }
    }

    // condition.game = 'FOOTBALL';
    if (req.query.betId) {
        condition.betId = req.query.betId;
    }

    let orCondition = [];

    if (req.query.status) {
        orCondition.push({
            $or: req.query.status.split(',').map(function (status) {
                return {'status': status}
            })
        });
    }

    if (req.query.oddType && req.query.oddType !== 'ALL') {
        if (req.query.gameType === 'PARLAY') {
            condition['gameType'] = 'PARLAY';
        } else if (req.query.gameType === 'MIX_STEP') {
            condition['gameType'] = 'MIX_STEP';
        } else if (req.query.gameType === 'TODAY') {
            condition['gameType'] = 'TODAY';
            if (req.query.oddType === 'HDP') {
                orCondition.push({$or: [{'hdp.oddType': 'AH'}, {'hdp.oddType': 'AH1ST'}]})
            } else if (req.query.oddType === 'OU') {
                orCondition.push({$or: [{'hdp.oddType': 'OU'}, {'hdp.oddType': 'OU1ST'}]})
            } else if (req.query.oddType === 'OE') {
                orCondition.push({$or: [{'hdp.oddType': 'OE'}, {'hdp.oddType': 'OE1ST'}]})
            } else if (req.query.oddType === 'X12') {
                orCondition.push({$or: [{'hdp.oddType': 'X12'}, {'hdp.oddType': 'X121ST'}]})
            } else {
                condition['hdp.oddType'] = req.query.oddType;
            }
        }
    }

    if (orCondition.length > 0) {
        condition['$and'] = orCondition;
    }

    let group = '';
    let child = '';
    if (userInfo.group.type === 'SUPER_ADMIN') {
        group = 'superAdmin';
        child = 'company';
    } else if (userInfo.group.type === 'COMPANY') {
        group = 'company';
        child = 'shareHolder';
    } else if (userInfo.group.type === 'SHARE_HOLDER') {
        group = 'shareHolder';
        child = 'superSenior';
    } else if (userInfo.group.type === 'SUPER_SENIOR') {
        group = 'superSenior';
        child = 'senior';
    } else if (userInfo.group.type === 'SENIOR') {
        group = 'senior';
        child = 'masterAgent';
    } else if (userInfo.group.type === 'MASTER_AGENT') {
        group = 'masterAgent';
        child = 'agent';
    } else if (userInfo.group.type === 'AGENT') {
        group = 'agent';
        child = 'member';
    }

    // condition['game'] = 'FOOTBALL';
    condition['$or'] = [{game: 'FOOTBALL'}, {game: 'M2'}];
    condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(userInfo.group._id);
    condition['active'] = true;


    async.series([callback => {

        BetTransactionModel.count(condition, function (err, data) {
            if (err) {
                callback(err, null);
                return;
            }
            callback(null, data);

        }).lean();

    }], (err, results) => {
        console.log(condition);


        let page = req.query.page ? Number.parseInt(req.query.page) : 10;
        let limit = req.query.limit ? Number.parseInt(req.query.limit) : 1;

        let count = results[0];
        BetTransactionModel.aggregate([
            {
                $match: condition
            },
            {
                $skip: (page - 1) * limit
            }, {
                $limit: limit
            },
            {
                $sort: {createdDate: -1},
            },
            {
                $project: {
                    _id: 0,
                    memberId: '$memberId',
                    hdp: '$hdp',
                    parlay: '$parlay',
                    commission: '$commission.' + group,
                    memberCommPercent: '$commission.member.commission',
                    memberCommission: {$multiply: ['$amount', {$divide: ['$commission.member.commission', 100]}]},
                    child: '$commission.' + child,
                    amount: '$amount',
                    memberCreditUsage: {$abs: '$memberCredit'},
                    payout: '$payout',
                    status: '$status',
                    totalReceive: {$multiply: ['$amount', {$divide: ['$commission.' + group + '.shareReceive', 100]}]},
                    stake_in: '$amount',
                    stake_out: {
                        $cond: [{$eq: ['$gameType', 'TODAY']}, {$multiply: ['$amount', '$hdp.odd']}, {$multiply: ['$amount', '$parlay.odds']}]
                    },
                    betId: '$betId',
                    createdDate: '$createdDate',
                    priceType: '$priceType',
                    gameType: '$gameType',
                    ipAddress: '$ipAddress',
                    isPrinted: '$isPrinted',
                    codeOfflineTicket: '$codeOfflineTicket',
                    isOfflineCash:'$isOfflineCash',
                }
            }
        ], (err, results) => {
            if (err)
                return res.send({message: "error", results: err, code: 999});


            async.series([(callback => {

                MemberModel.populate(results, {
                    path: 'memberId',
                    select: '_id username'
                }, function (err, memberResult) {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, memberResult);
                    }
                });


            }), (callback => {

                AgentGroupModel.populate(results, {
                    path: 'commission.group',
                    select: '_id name'
                }, function (err, memberResult) {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, memberResult);
                    }
                });

            }), (callback => {

                AgentGroupModel.populate(results, {
                    path: 'child.group',
                    select: '_id name'
                }, function (err, memberResult) {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, memberResult);
                    }
                });

            })], (err, populateResults) => {

                if (err)
                    return res.send({message: "error", results: err, code: 999});

                let summary = _.reduce(results, function (memo, num) {
                    return {
                        totalAmount: memo.totalAmount + num.amount,
                        totalReceive: memo.totalReceive + num.totalReceive
                    }
                }, {totalAmount: 0, totalReceive: 0});

                return res.send(
                    {
                        code: 0,
                        message: "success",
                        result: {
                            betList: results,
                            summary: summary,
                            "total": count,
                            "limit": limit,
                            "page": page,
                            "pages": Math.ceil(count / limit) || 1,
                        }
                    }
                );
            });
        });
    });

});


router.get('/list/offline', function (req, res) {

    let condition = {game: 'FOOTBALL'};

    condition.createdDate = {$gte: new Date(moment().add(-2, 'd').format('YYYY-MM-DDT11:00:00.000'))};

    let userInfo = req.userInfo;

    let orCondition = [];

    if (req.query.status) {
        orCondition.push({
            $or: req.query.status.split(',').map(function (status) {
                return {'status': status}
            })
        });
    }


    if (orCondition.length > 0) {
        condition['$and'] = orCondition;
    }

    let group = '';
    let child = '';
    if (userInfo.group.type === 'SUPER_ADMIN') {
        group = 'superAdmin';
        child = 'company';
    } else if (userInfo.group.type === 'COMPANY') {
        group = 'company';
        child = 'shareHolder';
    } else if (userInfo.group.type === 'SHARE_HOLDER') {
        group = 'shareHolder';
        child = 'superSenior';
    } else if (userInfo.group.type === 'SUPER_SENIOR') {
        group = 'superSenior';
        child = 'senior';
    } else if (userInfo.group.type === 'SENIOR') {
        group = 'senior';
        child = 'masterAgent';
    } else if (userInfo.group.type === 'MASTER_AGENT') {
        group = 'masterAgent';
        child = 'agent';
    } else if (userInfo.group.type === 'AGENT') {
        group = 'agent';
        child = 'member';
    }

    condition['game'] = 'FOOTBALL';
    condition['memberParentGroup'] = mongoose.Types.ObjectId(userInfo.group._id);
    condition['isOfflineCash'] = true;
    condition['active'] = false;

    console.log(condition);
    BetTransactionModel.aggregate([
        {
            $match: condition
        },
        {
            $project: {
                _id: 0,
                memberId: '$memberId',
                hdp: '$hdp',
                parlay: '$parlay',
                commission: '$commission.' + group,
                child: '$commission.' + child,
                memberCommission: {$multiply: ['$amount', {$divide: ['$commission.member.commission', 100]}]},
                memberCommPercent: '$commission.member.commission',
                memberCreditUsage: {$abs: '$memberCredit'},
                amount: '$amount',
                payout: '$payout',
                status: '$status',
                totalReceive: {$multiply: ['$amount', {$divide: ['$commission.' + group + '.shareReceive', 100]}]},
                stake_in: '$amount',
                stake_out: {
                    $cond: [{$eq: ['$gameType', 'TODAY']}, {$multiply: ['$amount', '$hdp.odd']}, {$multiply: ['$amount', '$parlay.odds']}]
                },
                betId: '$betId',
                createdDate: '$createdDate',
                priceType: '$priceType',
                gameType: '$gameType',
                ipAddress: '$ipAddress',
                isPrinted: '$isPrinted',
                codeOfflineTicket: '$codeOfflineTicket',
                isOfflineCash:'$isOfflineCash',
            }
        }
    ], (err, results) => {
        if (err)
            return res.send({message: "error", results: err, code: 999});


        async.series([(callback => {

            MemberModel.populate(results, {
                path: 'memberId',
                select: '_id username'
            }, function (err, memberResult) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, memberResult);
                }
            });


        }), (callback => {

            AgentGroupModel.populate(results, {
                path: 'commission.group',
                select: '_id name'
            }, function (err, memberResult) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, memberResult);
                }
            });

        }), (callback => {

            AgentGroupModel.populate(results, {
                path: 'child.group',
                select: '_id name'
            }, function (err, memberResult) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, memberResult);
                }
            });

        })], (err, populateResults) => {

            if (err)
                return res.send({message: "error", results: err, code: 999});

            let summary = _.reduce(results, function (memo, num) {
                return {
                    totalAmount: memo.totalAmount + num.amount,
                    totalReceive: memo.totalReceive + num.totalReceive
                }
            }, {totalAmount: 0, totalReceive: 0});

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: {
                        betList: results,
                        summary: summary,
                    }
                }
            );
        });
    });

});

router.get('/list/offline2', function (req, res) {

    let condition = {};

    // condition.createdDate = {$gte: new Date(moment().add(-2, 'd').format('YYYY-MM-DDT11:00:00.000'))};

    let userInfo = req.userInfo;

    let orCondition = [];

    if (req.query.dateFrom && req.query.dateTo) {
        condition['createdDate'] = {
            "$gte": moment(req.query.dateFrom, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).utc(true).toDate(),
            "$lt": moment(req.query.dateTo, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).add(1,'d').utc(true).toDate()
        }
    }
    //
    // if (req.query.status) {
    //     orCondition.push({
    //         $or: req.query.status.split(',').map(function (status) {
    //             return {'status': status}
    //         })
    //     });
    // }
    if (req.query.oddType && req.query.oddType !== 'ALL') {
        if (req.query.gameType === 'PARLAY') {
            condition['gameType'] = 'PARLAY';
        } else if (req.query.gameType === 'MIX_STEP') {
            condition['gameType'] = 'MIX_STEP';
        } else if (req.query.gameType === 'TODAY') {
            condition['gameType'] = 'TODAY';
            if (req.query.oddType === 'HDP') {
                orCondition.push({$or: [{'hdp.oddType': 'AH'}, {'hdp.oddType': 'AH1ST'}]})
            } else if (req.query.oddType === 'OU') {
                orCondition.push({$or: [{'hdp.oddType': 'OU'}, {'hdp.oddType': 'OU1ST'}]})
            } else if (req.query.oddType === 'OE') {
                orCondition.push({$or: [{'hdp.oddType': 'OE'}, {'hdp.oddType': 'OE1ST'}]})
            } else if (req.query.oddType === 'X12') {
                orCondition.push({$or: [{'hdp.oddType': 'X12'}, {'hdp.oddType': 'X121ST'}]})
            } else {
                condition['hdp.oddType'] = req.query.oddType;
            }
        }
    }




    if (orCondition.length > 0) {
        condition['$and'] = orCondition;
    }

    let group = '';
    let child = '';
    if (userInfo.group.type === 'SUPER_ADMIN') {
        group = 'superAdmin';
        child = 'company';
    } else if (userInfo.group.type === 'COMPANY') {
        group = 'company';
        child = 'shareHolder';
    } else if (userInfo.group.type === 'SHARE_HOLDER') {
        group = 'shareHolder';
        child = 'superSenior';
    } else if (userInfo.group.type === 'SUPER_SENIOR') {
        group = 'superSenior';
        child = 'senior';
    } else if (userInfo.group.type === 'SENIOR') {
        group = 'senior';
        child = 'masterAgent';
    } else if (userInfo.group.type === 'MASTER_AGENT') {
        group = 'masterAgent';
        child = 'agent';
    } else if (userInfo.group.type === 'AGENT') {
        group = 'agent';
        child = 'member';
    }


    condition['memberParentGroup'] = mongoose.Types.ObjectId(userInfo.group._id);
    condition['game'] = 'FOOTBALL';
    condition['status'] = {$in: ['DONE','CANCELLED']};
    condition['isOfflineCash'] = true;
    condition['active'] = true;

    if (req.query.typePending !== 'ALL') {
        if (req.query.typePending === 'PENDING') {
            condition['isOfflineCashSuccess'] = false;
        } else if (req.query.typePending === 'COMPLETED') {
            condition['isOfflineCashSuccess'] = true;
        }
    }





    async.series([callback => {

        BetTransactionModel.count(condition, function (err, data) {
            if (err) {
                callback(err, null);
                return;
            }
            callback(null, data);

        }).lean();

    }], (err, results) => {
        console.log(condition);


        let page = req.query.page ? Number.parseInt(req.query.page) : 10;
        let limit = req.query.limit ? Number.parseInt(req.query.limit) : 1;

        let count = results[0];
        BetTransactionModel.aggregate([
                {
                    $match: condition
                },
                {
                    $skip: (page - 1) * limit
                }, {
                    $limit: limit
                },
                {
                    $sort: {createdDate: 1},
                },
                {
                    $project: {
                        _id: 0,
                        memberId: '$memberId',
                        hdp: '$hdp',
                        parlay: '$parlay',
                        commission: '$commission.' + group,
                        child: '$commission.' + child,
                        memberCommission: {$multiply: ['$amount', {$divide: ['$commission.member.commission', 100]}]},
                        memberCommPercent: '$commission.member.commission',
                        memberCommTotalWL: '$commission.member.totalWinLoseCom',
                        memberCreditUsage: {$abs: '$memberCredit'},
                        amount: '$amount',
                        payout: '$payout',
                        status: '$status',
                        totalReceive: {$multiply: ['$amount', {$divide: ['$commission.' + group + '.shareReceive', 100]}]},
                        stake_in: '$amount',
                        stake_out: {
                            $cond: [{$eq: ['$gameType', 'TODAY']}, {$multiply: ['$amount', '$hdp.odd']}, {$multiply: ['$amount', '$parlay.odds']}]
                        },
                        betId: '$betId',
                        createdDate: '$createdDate',
                        priceType: '$priceType',
                        gameType: '$gameType',
                        ipAddress: '$ipAddress',
                        isPrinted: '$isPrinted',
                        isOfflineCashSuccess: '$isOfflineCashSuccess'
                    }
                }
            ],
            (err, results) => {

                if (err)
                    return res.send({message: "error", results: err, code: 999});


                async.series([(callback => {

                    MemberModel.populate(results, {
                        path: 'memberId',
                        select: '_id username'
                    }, function (err, memberResult) {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, memberResult);
                        }
                    });


                }), (callback => {

                    AgentGroupModel.populate(results, {
                        path: 'commission.group',
                        select: '_id name'
                    }, function (err, memberResult) {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, memberResult);
                        }
                    });

                }), (callback => {

                    AgentGroupModel.populate(results, {
                        path: 'child.group',
                        select: '_id name'
                    }, function (err, memberResult) {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, memberResult);
                        }
                    });

                })], (err, populateResults) => {

                    if (err)
                        return res.send({message: "error", results: err, code: 999});

                    let summary = _.reduce(results, function (memo, num) {
                        return {
                            totalAmount: memo.totalAmount + num.amount,
                            totalReceive: memo.totalReceive + num.totalReceive
                        }
                    }, {totalAmount: 0, totalReceive: 0});
                    console.log()

                    return res.send(
                        {
                            code: 0,
                            message: "success",
                            result: {
                                betList: results,
                                summary: summary,
                                "total": count,
                                "limit": limit,
                                "page": page,
                                "pages": Math.ceil(count / limit) || 1,
                            }
                        }
                    );
                });
            });
    });

    console.log(condition);
    // let page = req.query.page ? Number.parseInt(req.query.page) : 1;
    // let limit = req.query.limit ? Number.parseInt(req.query.limit) : 50;
    //
    // BetTransactionModel.aggregate([
    //     {
    //         $match: condition
    //     },
    //     {
    //         $skip: (page - 1) * limit
    //     }, {
    //         $limit: limit
    //     },
    //     {
    //         $sort: {createdDate: 1},
    //     },
    //     {
    //         $project: {
    //             _id: 0,
    //             memberId: '$memberId',
    //             hdp: '$hdp',
    //             parlay: '$parlay',
    //             commission: '$commission.' + group,
    //             child: '$commission.' + child,
    //             memberCommission: {$multiply: ['$amount', {$divide: ['$commission.member.commission', 100]}]},
    //             memberCommPercent: '$commission.member.commission',
    //             memberCommTotalWL: '$commission.member.totalWinLoseCom',
    //             memberCreditUsage: {$abs: '$memberCredit'},
    //             amount: '$amount',
    //             payout: '$payout',
    //             status: '$status',
    //             totalReceive: {$multiply: ['$amount', {$divide: ['$commission.' + group + '.shareReceive', 100]}]},
    //             stake_in: '$amount',
    //             stake_out: {
    //                 $cond: [{$eq: ['$gameType', 'TODAY']}, {$multiply: ['$amount', '$hdp.odd']}, {$multiply: ['$amount', '$parlay.odds']}]
    //             },
    //             betId: '$betId',
    //             createdDate: '$createdDate',
    //             priceType: '$priceType',
    //             gameType: '$gameType',
    //             ipAddress: '$ipAddress',
    //             isPrinted: '$isPrinted',
    //             isOfflineCashSuccess: '$isOfflineCashSuccess'
    //         }
    //     }
    // ],
    //     (err, results) => {
    //     console.log(results,'qwqw111')
    //     if (err)
    //         return res.send({message: "error", results: err, code: 999});
    //
    //
    //     async.series([(callback => {
    //
    //         MemberModel.populate(results, {
    //             path: 'memberId',
    //             select: '_id username'
    //         }, function (err, memberResult) {
    //             if (err) {
    //                 callback(err, null);
    //             } else {
    //                 callback(null, memberResult);
    //             }
    //         });
    //
    //
    //     }), (callback => {
    //
    //         AgentGroupModel.populate(results, {
    //             path: 'commission.group',
    //             select: '_id name'
    //         }, function (err, memberResult) {
    //             if (err) {
    //                 callback(err, null);
    //             } else {
    //                 callback(null, memberResult);
    //             }
    //         });
    //
    //     }), (callback => {
    //
    //         AgentGroupModel.populate(results, {
    //             path: 'child.group',
    //             select: '_id name'
    //         }, function (err, memberResult) {
    //             if (err) {
    //                 callback(err, null);
    //             } else {
    //                 callback(null, memberResult);
    //             }
    //         });
    //
    //     })], (err, populateResults) => {
    //
    //         if (err)
    //             return res.send({message: "error", results: err, code: 999});
    //
    //         let summary = _.reduce(results, function (memo, num) {
    //             return {
    //                 totalAmount: memo.totalAmount + num.amount,
    //                 totalReceive: memo.totalReceive + num.totalReceive
    //             }
    //         }, {totalAmount: 0, totalReceive: 0});
    //         console.log()
    //
    //         return res.send(
    //             {
    //                 code: 0,
    //                 message: "success",
    //                 result: {
    //                     betList: results,
    //                     summary: summary,
    //                     "total": results.count,
    //                     "limit": limit,
    //                     "page": page,
    //                     "pages": Math.ceil(results.count / limit) || 1,
    //                 }
    //             }
    //         );
    //     });
    // });

});


router.get('/pending-status/offline', function (req, res) {


    let userInfo = req.userInfo;

    // let group = prepareGroup(userInfo.group.type);

    let condition = {};

    let dateList = {};

    if (moment().hours() >= 11) {
        condition['gameDate'] = {
            "$gte": moment().millisecond(0).second(0).minute(0).hour(11).add(-2,'d').utc(true).toDate(),
            "$lt": moment().millisecond(0).second(0).minute(0).hour(11).add(1,'d').utc(true).toDate()
        };
        dateList = DateUtils.enumerateBackDaysBetweenDates(moment().add(-2, 'days'), moment().add(0, 'd'), 'DD/MM/YYYY');
    } else {
        condition['gameDate'] = {
            "$gte": moment().millisecond(0).second(0).minute(0).hour(11).add(-3,'d').utc(true).toDate(),
            "$lt": moment().millisecond(0).second(0).minute(0).hour(11).add(0,'d').utc(true).toDate()
        };
        dateList = DateUtils.enumerateBackDaysBetweenDates(moment().add(-3, 'days'), moment().add(-1, 'd'), 'DD/MM/YYYY');
    }

    condition['status'] = 'DONE';
    condition['game'] = 'FOOTBALL';
    condition['memberParentGroup'] = mongoose.Types.ObjectId(userInfo.group._id);
    condition['isOfflineCash'] = true;
    condition['isOfflineCashSuccess'] = false;
    condition['active'] = true;

    BetTransactionModel.aggregate([
        {
            $match: condition
        },
        {
            $group: {
                _id: {
                    date: {
                        $cond: [
                            // {$and:[
                            {$gte: [{$hour: "$gameDate"}, 11]},
                            {$dateToString: {format: "%d/%m/%Y", date: "$gameDate"}},
                            {
                                $dateToString: {
                                    format: "%d/%m/%Y",
                                    date: {$subtract: ["$gameDate", 24 * 60 * 60 * 1000]}
                                }
                            }
                        ]
                    }
                },
                count: {$sum: 1},
                running: {$sum: {$cond: [{$eq: ['$isOfflineCashSuccess', false]}, 1, 0]}},
            }
        },
        {
            $project: {
                _id: 0,
                date: '$_id.date',
                count: '$count',
                running: '$running'
            }
        }
    ], (err, statementResponse) => {

        if (err) {
            return res.send({message: "error", result: err, code: 999});
        }

        dateList = _.map(dateList, (date) => {
            let mObj = _.filter(statementResponse, (pendingItem) => {
                return date === pendingItem.date;
            })[0];

            if (mObj) {
                return mObj;
            } else {
                return {
                    date: date,
                    count: 0,
                    running: 0
                }
            }
        });

        return res.send(
            {
                code: 0,
                message: "success",
                result: {
                    date1: dateList[2],
                    date2: dateList[1],
                    date3: dateList[0]
                }
            }
        );

    });

});

const updateStatusOfflineCashSchema = Joi.object().keys({
    betId: Joi.string().required(),
    codeTicket: Joi.string().required()
});
router.post('/list/updateStatusOfflineCash', function (req, res) {
    console.log('================= updateStatusOfflineCashSchema =================');

    let validate = Joi.validate(req.body, updateStatusOfflineCashSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    let userInfo = req.userInfo;
    // let ticketCode = req.query.ticketCode;
    // console.log(ticketCode,'qwwwqqqz')

    const {
        betId,
        codeTicket

    } = req.body;
    // console.log(req.body);


    console.log(`   JSON BODY => ${JSON.stringify(req.body)}`);

    async.series([callback => {

        BetTransactionModel.findOne({betId: betId}, function (err, response) {
            if (err) {
                callback(err, null);
            }
            if (response) {
                if (response.status === 'CANCELLED' || response.status === 'REJECTED') {
                    callback('cancel or reject already', null);
                } else {
                    callback(null, response);
                }
            } else {
                callback('bet Id not found', null);
            }
        }).populate({path: 'memberId', select: 'username_lower'})
            .populate({path: 'memberParentGroup', select: 'type endpoint'});

    }], function (err, asyncResponse) {
        if (err) {
            return res.send({message: err, result: err, code: 999});
        }

        let transaction = asyncResponse[0];


        let body = {
            isOfflineCashSuccess: true

        }
        // let random = _.random(1000, 9999);

        // console.log(transaction.codeOfflineTicket, random, '111111');

        if (transaction.codeOfflineTicket === codeTicket && transaction.isOfflineCashSuccess === false) {
            // console.log('ex1111')
            BetTransactionModel.update({_id: transaction._id}, body, (err, data) => {
                if (err) {
                    return res.send({message: "error", result: err, code: 999});
                } else if (data.nModified === 0) {
                    return res.send({
                        message: "error",
                        result: 'update ticket fail',
                        code: 998
                    });
                } else {
                    return res.send({message: "success", code: 0});
                }
            });
        }
        else {
            return res.send({
                message: "error",
                result: 'update ticket fail',
                code: 1001
            });
        }
    });
});


const approveOfflineBetSchema = Joi.object().keys({
    betId: Joi.string().required()
});

router.post('/approve-offline-ticket', function (req, res) {
    console.log('================= approve-offline-ticket =================');

    let validate = Joi.validate(req.body, approveOfflineBetSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    let userInfo = req.userInfo;

    const {
        betId,
    } = req.body;

    console.log(`   JSON BODY => ${JSON.stringify(req.body)}`);

    async.series([callback => {

        BetTransactionModel.findOne({betId: betId}, function (err, response) {
            if (err) {
                callback(err, null);
            }
            if (response) {
                if (response.status === 'CANCELLED' || response.status === 'REJECTED') {
                    callback('cancel or reject already', null);
                } else {
                    callback(null, response);
                }
            } else {
                callback('bet Id not found', null);
            }
        }).populate({path: 'memberId', select: 'username_lower'})
            .populate({path: 'memberParentGroup', select: 'type endpoint'});

    }], function (err, asyncResponse) {

        if (err) {
            return res.send({message: err, result: err, code: 999});
        }

        let transaction = asyncResponse[0];


        let body = {
            active: true
        };

        BetTransactionModel.update({_id: transaction._id}, body, (err, data) => {
            if (err) {
                return res.send({message: "error", result: err, code: 999});
            } else if (data.nModified === 0) {
                return res.send({
                    message: "error",
                    result: 'update ticket fail',
                    code: 998
                });
            } else {
                return res.send({message: "success", code: 0});
            }
        });
    });
});


const cancelBetSchema = Joi.object().keys({
    betId: Joi.string().required(),
    password: Joi.string().allow(''),
    status: Joi.string().valid('REJECTED', 'CANCELLED'),
    remark: Joi.string().allow('')
});

router.post('/cancel-ticket', function (req, res) {
    console.log('================= cancel-ticket =================');

    let validate = Joi.validate(req.body, cancelBetSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    let userInfo = req.userInfo;

    const {
        betId,
        status,
        remark
    } = req.body;
    console.log(`   JSON BODY => ${JSON.stringify(req.body)}`);
    // console.log('   ', betId, ' : ', status, ' : ', remark)


    async.series([callback => {

        UserModel.findById(userInfo._id, (err, agentResponse) => {

            if (agentResponse && (Hashing.encrypt256_1(req.body.password) === agentResponse.password)) {
                callback(null, agentResponse)
            } else {
                callback(1001, null);
            }
        });

    }, callback => {
        BetTransactionModel.findOne({betId: betId,status:'RUNNING'}, function (err, response) {
            if (err) {
                callback(err, null);
            }
            if (response) {
                if (response.status === 'CANCELLED' || response.status === 'REJECTED') {
                    callback('cancel or reject already', null);
                } else {
                    callback(null, response);
                }
            } else {
                callback('bet Id not found', null);
            }
        }).populate({path: 'memberId', select: 'username_lower'})
            .populate({path: 'memberParentGroup', select: 'type endpoint'});
    }], function (err, asyncResponse) {

        if (err) {
            if (err === 1001) {
                return res.send({
                    code: 1001,
                    message: "Invalid Password"
                });
            }
            return res.send({message: err, result: err, code: 999});
        }

        let transaction = asyncResponse[1];

        async.series([callback => {

            MemberService.updateBalance2(transaction.memberUsername, Math.abs(transaction.memberCredit), betId, 'CANCEL_TICKET', betId, function (err, creditResponse) {

                if (err) {
                    callback('fail', null);
                } else {
                    callback(null, 'success');
                }
            });


        }], function (err, response) {

            if (err) {
                return res.send({
                    message: "update credit failed",
                    result: err,
                    code: 999
                });
            }

            let body = {
                status: status,
                remark: remark,
                cancelByType: 'AGENT',
                cancelByAgent: userInfo._id,
                cancelDate: DateUtils.getCurrentDate(),
                settleDate: DateUtils.getCurrentDate()
            };

            BetTransactionModel.update({_id: transaction._id}, body, (err, data) => {
                if (err) {
                    return res.send({message: "error", result: err, code: 999});
                } else if (data.nModified === 0) {
                    return res.send({
                        message: "error",
                        result: 'update ticket fail',
                        code: 998
                    });
                } else {

                    if (transaction.memberParentGroup.type === 'API') {

                        ApiService.cancelBet(transaction.memberParentGroup.endpoint, remark, transaction, (err, response) => {
                            if (err) {
                                return res.send({message: "error", result: err, code: 999});
                            } else {
                                return res.send({message: "success", code: 0});
                            }

                        });
                    } else {

                            return res.send({message: "success", code: 0});
                    }
                }
            });
        });
    });
});


const cancelBetMatchSchema = Joi.object().keys({
    matchId: Joi.string().required(),
    status: Joi.string().valid('REJECTED', 'CANCELLED'),
    remark: Joi.string().allow(''),
    type: Joi.string().valid('HT', 'FT'),
});

router.post('/cancel-ticket-match', function (req, res) {
    console.log('================= cancel-ticket =================');

    let validate = Joi.validate(req.body, cancelBetMatchSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    const {
        matchId,
        status,
        remark,
        type
    } = req.body;


    async.series([callback => {

        let condition = {
            game: {$in:['FOOTBALL','M2']},
            'hdp.matchId': matchId,
            status: 'RUNNING',
            createdDate :{$gte: new Date(moment().add(-8, 'd').format('YYYY-MM-DDT11:00:00.000'))}
        };

        if (type === 'HT') {
            condition['hdp.oddType'] = {'$in': ['AH1ST', 'OU1ST', 'OE1ST', 'X121ST']}
        }

        BetTransactionModel.find(condition, function (err, betResponse) {
            if (err) {
                callback(err, null);
                return;
            }

            async.each(betResponse, (betObj, asyncCallback) => {

                async.series([callback => {

                    let body = {
                        status: status,
                        remark: remark,
                        cancelDate: DateUtils.getCurrentDate(),
                        cancelByType: 'API',
                        settleDate: DateUtils.getCurrentDate()
                    };

                    BetTransactionModel.update({_id: betObj._id}, body, (err, data) => {
                        if (err) {
                            callback('fail', null);
                        } else {
                                callback(null, 'success');
                        }
                    });

                }], function (err, asyncResponse) {

                    MemberService.updateBalance2(betObj.memberUsername, Math.abs(betObj.memberCredit), betObj.betId, 'CANCEL_MATCH', betObj.betId, function (err, creditResponse) {

                        if (err) {
                            asyncCallback('fail', null);
                        } else {
                            asyncCallback(null, 'success');
                        }
                    });

                });

            }, (err) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, betResponse);
                }
            });//end forEach Match

        }).populate({path: 'memberId', select: 'username_lower'})
            .populate({path: 'memberParentGroup', select: 'type endpoint'})

    }, callback => {

        let condition = {
            game: {$in:['FOOTBALL','M2']},
            'gameType': {$ne: 'TODAY'},
            'parlay.matches.matchId':matchId,
            'status': 'RUNNING'
        };


        BetTransactionModel.find(condition)
            .select('memberId parlay commission amount memberCredit payout memberParentGroup status betId')
            .populate({path: 'memberParentGroup', select: 'type'})
            .exec(function (err, response) {

                async.each(response, (betObj, betListCallback) => {


                    const allMatch = betObj.parlay.matches;

                    let filterList = _.filter(allMatch, function (response) {
                        if (type === 'HT') {
                            return response.matchId === matchId && ['AH1ST', 'OU1ST', 'OE1ST', 'X121ST'].includes(response.oddType);
                        } else {
                            return response.matchId === matchId;
                        }
                    });


                    if (filterList.length === 0) {
                        betListCallback(null, 'not data found');
                        return;
                    }

                    let endScoreMatch = _.filter(allMatch, function (response) {
                        return response.betResult;
                    });

                    let unCheckMatchCount = _.filter(allMatch, function (response) {
                        return response.matchId !== matchId && !response.betResult;
                    }).length;

                    console.log('endScoreMatch : ', endScoreMatch.length)
                    console.log('filterList : ', filterList.length)
                    console.log('allMatch : ', allMatch.length)

                    if (((endScoreMatch.length + filterList.length) == allMatch.length) && unCheckMatchCount == 0) {


                        let totalUnCheckOdds = _.filter(allMatch, function (response) {
                            return response.matchId !== matchId && response.betResult;
                        }).map((item) => {
                            return calculateParlayOdds(item.betResult, item.odd);
                        });


                        let sumTotalUnCheckOdds = _.reduce(totalUnCheckOdds, (memo, item) => {
                            return {sum: memo.sum * item};
                        }, {sum: 1}).sum;


                        let halfLoseCount = _.filter(allMatch, function (response) {
                            return response.betResult === HALF_LOSE && response.matchId !== matchId;
                        }).length;

                        console.log(sumTotalUnCheckOdds);
                        //prepare with last match result
                        sumTotalUnCheckOdds = roundTo.down(sumTotalUnCheckOdds, 3);


                        console.log('sumTotalUnCheckOdds : ', sumTotalUnCheckOdds);
                        console.log('halfLoseCount : ', halfLoseCount);

                        let winLoseAmount = 0;
                        let shareReceive;

                        winLoseAmount = (subPlyAmount(betObj.amount, halfLoseCount) * sumTotalUnCheckOdds);
                        let validAmount = subPlyAmount(betObj.amount, halfLoseCount);

                        winLoseAmount = winLoseAmount - betObj.amount;

                        shareReceive = prepareShareReceive(winLoseAmount, WIN, betObj);


                        updateAgentMemberBalance(shareReceive, 'PARLAY', (err, response) => {
                            if (err) {
                                betListCallback(err, null);
                            } else {
                                let updateBody = {
                                    $set: {
                                        'parlay.matches.$.isCancel': true,
                                        'parlay.matches.$.betResult': 'DRAW',
                                        'parlay.matches.$.settleDate': DateUtils.getCurrentDate(),
                                        'parlay.matches.$.remark': 'settle',
                                        'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                                        'commission.company.winLose': shareReceive.company.winLose,
                                        'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                                        'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                                        'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                                        'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                                        'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                                        'commission.senior.winLose': shareReceive.senior.winLose,
                                        'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                                        'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                                        'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                                        'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                                        'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                                        'commission.agent.winLose': shareReceive.agent.winLose,
                                        'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                                        'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                                        'commission.member.winLose': shareReceive.member.winLose,
                                        'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                                        'validAmount': validAmount,
                                        'betResult': 'WIN',
                                        'isEndScore': true,
                                        'status': 'DONE',
                                        'settleDate': DateUtils.getCurrentDate()
                                    }
                                };

                                BetTransactionModel.update({
                                    _id: betObj._id,
                                    'parlay.matches.matchId': matchId
                                }, updateBody, function (err, response) {
                                    if (err) {
                                        betListCallback(err, null);
                                    } else {
                                            betListCallback(null, response);
                                    }

                                });

                            }
                        });

                    } else {
                        _.each(filterList, (match) => {

                            let updateBody = {
                                $set: {
                                    'parlay.matches.$.isCancel': true,
                                    'parlay.matches.$.betResult': 'DRAW',
                                    'parlay.matches.$.settleDate': DateUtils.getCurrentDate(),
                                    'parlay.matches.$.remark': 'update only',
                                }
                            };

                            BetTransactionModel.update({
                                _id: betObj._id,
                                'parlay.matches._id': match._id
                            }, updateBody, function (err, response) {
                                if (err) {
                                    betListCallback(err, null);
                                } else {
                                    betListCallback(null, response);
                                }
                            });

                        });
                    }


                }, (err) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, 'success');
                    }
                });//end forEach Match

            });
    }], function (err, asyncResponse) {

        if (err) {
            return res.send({
                message: "find transaction failed",
                result: err,
                code: 999
            });
        }

        let transaction = asyncResponse[0];

        console.log(transaction)


        let apiResponse = _.filter(transaction, (item) => {
            return item.memberParentGroup.type === 'API';
        });

        if (apiResponse.length > 0) {
            let endpoint = apiResponse[0].memberParentGroup.endpoint;

            ApiService.cancelMatch(endpoint, matchId, remark, apiResponse, (err, response) => {
                if (err) {
                    return res.send({
                        message: "99",
                        result: err,
                        code: 999
                    });
                } else {
                    return res.send({message: "success", code: 0});
                }

            });
        } else {
            return res.send({message: "success", code: 0});
        }
    });
});

const DRAW = "DRAW";
const WIN = "WIN";
const HALF_WIN = "HALF_WIN";
const LOSE = "LOSE";
const HALF_LOSE = "HALF_LOSE";


function subPlyAmount(amount, count) {
    if (count > 0) {
        return subPlyAmount(amount / 2, --count);
    } else {
        return amount;
    }

}


function prepareShareReceive(winLose, betResult, betTransaction) {

    let winLoseInfo;
    const amount = betTransaction.amount;

    if (betResult !== DRAW) {

        let memberCommission = validateCommission(betResult, betTransaction.commission.member.commission);

        let superAdminShare = betTransaction.commission.superAdmin.shareReceive;

        let companyShare = betTransaction.commission.company.shareReceive;
        let companyCommission = validateCommission(betResult, betTransaction.commission.company.commission || 0);

        let shareHolderShare = betTransaction.commission.shareHolder.shareReceive;
        let shareHolderCommission = validateCommission(betResult, betTransaction.commission.shareHolder.commission || 0);

        let seniorShare = betTransaction.commission.senior.shareReceive || 0;
        let seniorCommission = validateCommission(betResult, betTransaction.commission.senior.commission || 0);

        let masterAgentShare = betTransaction.commission.masterAgent.shareReceive || 0;
        let masterAgentCommission = validateCommission(betResult, betTransaction.commission.masterAgent.commission || 0);

        let agentShare = betTransaction.commission.agent.shareReceive || 0;
        let agentCommission = validateCommission(betResult, betTransaction.commission.agent.commission || 0);

        function validateCommission(betResult, commission) {
            return betResult === HALF_LOSE || betResult === HALF_WIN ? (commission / 2) : commission;
        }


        //SUPER ADMIN
        let superAdminTotalUpperShare = 0;
        let superAdminOwnAndChildShare = companyShare + shareHolderShare + seniorShare + masterAgentShare + agentShare;

        let superAdminReceiveCommission = 0;
        let superAdminGiveCommission = companyCommission;

        console.log('TOTAL W/L : ', winLose);
        let superAdminWinLose = 0 - (winLose * convertPercent(betTransaction.commission.superAdmin.shareReceive));
        let superAdminFinalCom = calculateCommission(amount,
            superAdminShare,
            superAdminTotalUpperShare,
            superAdminReceiveCommission,
            superAdminOwnAndChildShare,
            superAdminGiveCommission);

        console.log('superAdmin share : ', betTransaction.commission.superAdmin.shareReceive, ' %');
        console.log('superAdmin com % : ', betTransaction.commission.superAdmin.commission, ' %');
        console.log('superAdmin W/L : ', superAdminWinLose);
        console.log('superAdmin COM : ', superAdminFinalCom);
        console.log('superAdmin W/L + COM : ', superAdminWinLose + (superAdminFinalCom));
        console.log('');

        console.log("==================================");

        //COMPANY
        let companyTotalUpperShare = superAdminShare;
        let companyOwnAndChildShare = shareHolderShare + seniorShare + masterAgentShare + agentShare;

        let companyReceiveCommission = 0;
        let companyGiveCommission = shareHolderCommission;

        console.log('TOTAL W/L : ', winLose);
        let companyWinLose = 0 - (winLose * convertPercent(betTransaction.commission.company.shareReceive));
        let companyFinalCom = calculateCommission(amount,
            companyShare,
            companyTotalUpperShare,
            companyReceiveCommission,
            companyOwnAndChildShare,
            companyGiveCommission);

        console.log('company share : ', betTransaction.commission.company.shareReceive, ' %');
        console.log('company com % : ', betTransaction.commission.company.commission, ' %');
        console.log('company W/L : ', companyWinLose);
        console.log('company COM : ', companyFinalCom);
        console.log('company W/L + COM : ', companyWinLose + (companyFinalCom));
        console.log('');

        console.log("==================================");
        //SHARE HOLDER
        let shareHolderTotalUpperShare = superAdminShare + companyShare;
        let shareHolderOwnAndChildShare = seniorShare + masterAgentShare + agentShare;
        let shareHolderReceiveCommission = shareHolderCommission;
        let shareHolderGiveCommission = seniorCommission;

        let shareHolderWinLose = 0 - (winLose * convertPercent(betTransaction.commission.shareHolder.shareReceive));
        let shareHolderFinalCom = calculateCommission(amount,
            shareHolderShare,
            shareHolderTotalUpperShare,
            shareHolderReceiveCommission,
            shareHolderOwnAndChildShare,
            shareHolderGiveCommission);

        console.log('shareHolder share : ', betTransaction.commission.shareHolder.shareReceive, ' %');
        console.log('shareHolder com % : ', betTransaction.commission.shareHolder.commission, ' %');
        console.log('shareHolder W/L : ', shareHolderWinLose);
        console.log('shareHolder COM : ', shareHolderFinalCom);
        // console.log('shareHolder COM LOSE : ', calculateStep3(1000, shareHolderShare, shareHolderGiveCommission));
        console.log('shareHolder W/L + COM : ', shareHolderWinLose + (shareHolderFinalCom));
        console.log('');

        console.log("==================================")
        //SENIOR
        let seniorTotalUpperShare = superAdminShare + companyShare + shareHolderShare;
        let seniorOwnAndChildShare = masterAgentShare + agentShare;
        let seniorReceiveCommission = seniorCommission;
        let seniorGiveCommission = betTransaction.memberParentGroup.type === 'SENIOR' ? memberCommission :
            betTransaction.commission.masterAgent.group ? masterAgentCommission : agentCommission;

        let seniorWinLose = 0 - (winLose * convertPercent(betTransaction.commission.senior.shareReceive));
        let seniorFinalCom = calculateCommission(amount,
            seniorShare,
            seniorTotalUpperShare,
            seniorReceiveCommission,
            seniorOwnAndChildShare,
            seniorGiveCommission);

        console.log('xxxx :: ', seniorFinalCom)
        console.log('senior share : ', betTransaction.commission.senior.shareReceive, ' %');
        console.log('senior com % : ', betTransaction.commission.senior.commission, ' %');
        console.log('senior W/L : ', seniorWinLose);
        console.log('senior COM : ', seniorFinalCom);
        // console.log('senior COM LOSE : ', calculateStep3(1000, seniorShare, seniorGiveCommission));
        console.log('senior W/L + COM : ', seniorWinLose + seniorFinalCom);
        console.log('');

        console.log("==================================");

        //MASTER AGENT
        let masterAgentTotalUpperShare = superAdminShare + companyShare + shareHolderShare + seniorShare;
        let masterAgentOwnAndChildShare = agentShare;
        let masterAgentReceiveCommission = masterAgentCommission;
        let masterAgentGiveCommission = betTransaction.memberParentGroup.type === 'MASTER_AGENT' ? memberCommission :
            betTransaction.commission.masterAgent.group ? agentCommission : 0;

        let masterAgentWinLose = 0 - (winLose * convertPercent(betTransaction.commission.masterAgent.shareReceive));
        let masterAgentFinalCom = calculateCommission(amount,
            masterAgentShare,
            masterAgentTotalUpperShare,
            masterAgentReceiveCommission,
            masterAgentOwnAndChildShare,
            masterAgentGiveCommission);

        console.log('masterAgent share : ', betTransaction.commission.masterAgent.shareReceive, ' %');
        console.log('masterAgent com % : ', betTransaction.commission.masterAgent.commission, ' %');
        console.log('masterAgent W/L : ', masterAgentWinLose);
        console.log('masterAgent COM : ', masterAgentFinalCom);
        // console.log('masterAgent COM LOSE : ', calculateStep3(1000, masterAgentShare, masterAgentGiveCommission));
        console.log('masterAgent W/L + COM : ', masterAgentWinLose + masterAgentFinalCom);
        console.log('');

        console.log("==================================");
        //AGENT
        let agentTotalUpperShare = superAdminShare + companyShare + shareHolderShare + seniorShare + masterAgentShare;

        let agentOwnAndChildShare = 100 - (superAdminShare + companyShare + shareHolderShare + seniorShare + masterAgentShare + agentShare);
        let agentReceiveCommission = agentCommission;
        let agentGiveCommission = betTransaction.memberParentGroup.type === 'AGENT' ? memberCommission : 0;
        let agentWinLose = 0 - (winLose * convertPercent(betTransaction.commission.agent.shareReceive));
        let agentFinalCom = calculateCommission(amount,
            agentShare,
            agentTotalUpperShare,
            agentReceiveCommission,
            agentOwnAndChildShare,
            agentGiveCommission);

        console.log('agent share : ', betTransaction.commission.agent.shareReceive, ' %');
        console.log('agent com % : ', betTransaction.commission.agent.commission, ' %');
        console.log('agent W/L : ', agentWinLose);
        console.log('agent COM : ', agentFinalCom);
        // console.log('agent COM LOSE : ', calculateStep3(1000, agentShare, agentGiveCommission));
        console.log('agent W/L + COM : ', agentWinLose + agentFinalCom);
        console.log('');

        console.log("==================================");
        console.log('member W/L : ', winLose);
        console.log('member COM : ', Math.abs(winLose * convertPercent(memberCommission)));
        console.log('member W/L + COM : ', winLose + Math.abs(winLose * convertPercent(betTransaction.commission.member.commission)));


        function calculateCommission(betPrice, ownShare, totalUpperShare, receiveCommission, ownAndChildShare, giveCommission) {

            let step1 = roundTo(Number.parseFloat(betPrice * (convertPercent(totalUpperShare)) * (receiveCommission / 100)), 3);
            console.log('step1 : ' + betPrice + ' x ' + totalUpperShare + ' x ' + (receiveCommission / 100) + ' = ' + step1);


            let step2 = roundTo(Number.parseFloat(betPrice * (convertPercent(totalUpperShare)) * (giveCommission / 100)), 3);
            console.log('step2 : ' + betPrice + ' x ' + totalUpperShare + ' x ' + (giveCommission / 100) + ' = ' + step2);

            // let step3 = betPrice * convertPercent(ownShare) * convertPercent(giveCommission);
            let step3 = roundTo(calculateStep3(betPrice, ownShare, giveCommission), 3);
            console.log('step3 : ' + betPrice + ' x ' + convertPercent(ownShare) + ' x ' + convertPercent(giveCommission) + ' = ' + step3);

            console.log('step3 : ' + step3);
            console.log('step1 : ' + step1 + ' , step2 : ' + step2 + ' , step3 : ' + step3)

            let result12 = roundTo((step1 - step2), 3);
            let finalResult = roundTo((result12 - step3), 3);
            console.log('result :: ', finalResult)

            return finalResult;
        }

        function calculateStep3(betPrice, ownShare, giveCommission) {
            return betPrice * convertPercent(ownShare) * convertPercent(giveCommission);
        }


        winLoseInfo = {
            betId: betTransaction.betId,
            betAmount: amount,
            memberCredit: betTransaction.memberCredit,
            memberUsername: betTransaction.memberUsername,
            betResult: betResult,
            superAdmin: {
                group: betTransaction.commission.superAdmin.group,
                winLoseCom: roundTo(superAdminFinalCom, 2),
                winLose: roundTo(superAdminWinLose, 2),
                totalWinLoseCom: roundTo((superAdminWinLose + superAdminFinalCom), 2)
            },
            company: {
                group: betTransaction.commission.company.group,
                winLoseCom: roundTo(companyFinalCom, 2),
                winLose: roundTo(companyWinLose, 2),
                totalWinLoseCom: roundTo((companyWinLose + companyFinalCom), 2)
            },
            shareHolder: {
                group: betTransaction.commission.shareHolder.group,
                winLoseCom: roundTo(shareHolderFinalCom, 2),
                winLose: roundTo(shareHolderWinLose, 2),
                totalWinLoseCom: roundTo((shareHolderWinLose + shareHolderFinalCom), 2)
            },
            senior: {
                group: betTransaction.commission.senior.group,
                winLoseCom: roundTo(seniorFinalCom, 2),
                winLose: roundTo(seniorWinLose, 2),
                totalWinLoseCom: roundTo((seniorWinLose + seniorFinalCom), 2)
            },
            masterAgent: {
                group: betTransaction.commission.masterAgent.group,
                winLoseCom: roundTo(masterAgentFinalCom, 2),
                winLose: roundTo(masterAgentWinLose, 2),
                totalWinLoseCom: roundTo((masterAgentWinLose + masterAgentFinalCom), 2)
            },
            agent: {
                group: betTransaction.commission.agent.group,
                winLoseCom: roundTo(agentFinalCom, 2),
                winLose: roundTo(agentWinLose, 2),
                totalWinLoseCom: roundTo((agentWinLose + agentFinalCom), 2)
            },
            member: {
                id: betTransaction.memberId,
                winLoseCom: roundTo(Math.abs(amount * convertPercent(memberCommission)), 2),
                winLose: roundTo(winLose, 2),
                totalWinLoseCom: roundTo((winLose + Math.abs(amount * convertPercent(memberCommission))), 2)
            }
        };
    } else {
        winLoseInfo = {
            betId: betTransaction.betId,
            betAmount: amount,
            memberCredit: betTransaction.memberCredit,
            memberUsername: betTransaction.memberUsername,
            betResult: betResult,
            superAdmin: {
                winLoseCom: 0,
                winLose: 0,
                totalWinLoseCom: 0
            },
            company: {
                winLoseCom: 0,
                winLose: 0,
                totalWinLoseCom: 0
            },
            shareHolder: {
                winLoseCom: 0,
                winLose: 0,
                totalWinLoseCom: 0
            },
            senior: {
                winLoseCom: 0,
                winLose: 0,
                totalWinLoseCom: 0
            },
            masterAgent: {
                winLoseCom: 0,
                winLose: 0,
                totalWinLoseCom: 0
            },
            agent: {
                winLoseCom: 0,
                winLose: 0,
                totalWinLoseCom: 0
            },
            member: {
                id: betTransaction.memberId,
                winLoseCom: 0,
                winLose: 0,
                totalWinLoseCom: 0
            }
        };
    }
    return winLoseInfo;

}

function calculateParlayOdds(betResult, odd) {
    if (betResult === WIN) {
        return odd;
    } else if (betResult === HALF_WIN) {
        return 1 + ((odd - 1) / 2);
    } else if (betResult === DRAW) {
        return 1;
    } else if (betResult === HALF_LOSE) {
        return 1;
    }
    return odd;
}

function updateAgentMemberBalance(shareReceive, gameType, updateCallback) {

    console.log('updateAgentMemberBalance');
    console.log('shareReceive : ', shareReceive);
    async.series([callback => {
        let updateWinLoseAgentList = [];

        updateWinLoseAgentList.push({
            group: shareReceive.superAdmin.group,
            amount: shareReceive.superAdmin.totalWinLoseCom
        });
        updateWinLoseAgentList.push({
            group: shareReceive.company.group,
            amount: (shareReceive.superAdmin.totalWinLoseCom * -1)
        });
        updateWinLoseAgentList.push({
            group: shareReceive.shareHolder.group,
            amount: (shareReceive.superAdmin.totalWinLoseCom * -1) + (shareReceive.company.totalWinLoseCom * -1)
        });
        updateWinLoseAgentList.push({
            group: shareReceive.senior.group,
            amount: (shareReceive.superAdmin.totalWinLoseCom * -1) + (shareReceive.company.totalWinLoseCom * -1) + (shareReceive.shareHolder.totalWinLoseCom * -1)
        });
        updateWinLoseAgentList.push({
            group: shareReceive.masterAgent.group,
            amount: (shareReceive.superAdmin.totalWinLoseCom * -1) + (shareReceive.company.totalWinLoseCom * -1) + (shareReceive.shareHolder.totalWinLoseCom * -1) + (shareReceive.senior.totalWinLoseCom * -1)
        });
        updateWinLoseAgentList.push({
            group: shareReceive.agent.group,
            amount: (shareReceive.superAdmin.totalWinLoseCom * -1) + (shareReceive.company.totalWinLoseCom * -1) + (shareReceive.shareHolder.totalWinLoseCom * -1) + (shareReceive.senior.totalWinLoseCom * -1) + (shareReceive.masterAgent.totalWinLoseCom * -1)
        });

        console.log("updateWinLoseAgent");
        async.each(updateWinLoseAgentList, (agentBalance, callbackWL) => {
            if (agentBalance.group && agentBalance.amount !== 0) {
                AgentGroupModel.findOneAndUpdate({_id: agentBalance.group}, {$inc: {balance: agentBalance.amount}})
                    .exec(function (err, creditResponse) {
                        if (err) {
                            callbackWL(err);
                        }
                        else {
                            callbackWL();
                        }
                    });
            } else {
                callbackWL();
            }
        }, (err) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, '');
            }
        });
    }, callback => {

        let memberReceiveAmount;

        console.log('memberCredit : ' + shareReceive.memberCredit + '  , betAmount : ' + shareReceive.betAmount + ' , betResult : ' + shareReceive.betResult);

        console.log('betResult : ', shareReceive.betResult)
        if (gameType === 'PARLAY') {
            if (shareReceive.betResult === WIN) {
                memberReceiveAmount = Math.abs(shareReceive.memberCredit) + shareReceive.member.totalWinLoseCom;
            } else {
                console.log('memberReceiveAmount ::: ' + shareReceive.betAmount + ' : ' + shareReceive.member.totalWinLoseCom)
                memberReceiveAmount = shareReceive.betAmount + shareReceive.member.totalWinLoseCom;
            }
        } else {
            // if ((Math.abs(shareReceive.memberCredit) < shareReceive.betAmount)) {
            //     if(shareReceive.betResult === HALF_LOSE){
            //         memberReceiveAmount = Math.abs(shareReceive.memberCredit) / 2;
            //     }
            // } else {
            console.log('memberReceiveAmount ::: ' + Math.abs(shareReceive.memberCredit) + ' : ' + shareReceive.member.totalWinLoseCom)
            memberReceiveAmount = Math.abs(shareReceive.memberCredit) + shareReceive.member.totalWinLoseCom;
            // }
        }

        MemberService.updateBalance2(shareReceive.memberUsername, memberReceiveAmount, shareReceive.betId, "SPORTS_BOOK_END_SCORE", shareReceive.betId, (err, updateMemberResponse) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, updateMemberResponse);
            }
        });

    }], function (err, response) {
        if (err) {
            updateCallback(err, null);
        } else {
            updateCallback(null, response);
        }
    });
}

function convertPercent(percent) {
    return (percent || 0) / 100
}

router.get('/stock_oe_x12', function (req, res) {

    let name = req.query.name;

    let condition = {game: 'FOOTBALL', active: true};

    let userInfo = req.userInfo;


    let lang = req.query.lang;

    if (!req.query.lang) {
        lang = 'en';
    }

    let group = '';
    if (userInfo.group.type === 'SUPER_ADMIN') {
        group = 'superAdmin';
    } else if (userInfo.group.type === 'COMPANY') {
        group = 'company';
    } else if (userInfo.group.type === 'SHARE_HOLDER') {
        group = 'shareHolder';
    } else if (userInfo.group.type === 'SUPER_SENIOR') {
        group = 'superSenior';
    } else if (userInfo.group.type === 'SENIOR') {
        group = 'senior';
    } else if (userInfo.group.type === 'MASTER_AGENT') {
        group = 'masterAgent';
    } else if (userInfo.group.type === 'AGENT') {
        group = 'agent';
    }
    condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(userInfo.group._id);

    condition['$and'] = [
        {$or: [{'hdp.oddType': 'OE'}, {'hdp.oddType': 'OE1ST'}, {'hdp.oddType': 'X12'}, {'hdp.oddType': 'X121ST'}]},
        {$or: [{"status": 'RUNNING'}]}
    ];

    BetTransactionModel.aggregate([
        {
            $match: condition
        },
        {
            $project: {
                leagueId: '$hdp.leagueId',
                leagueName: '$hdp.leagueName',
                match: {
                    matchId: '$hdp.matchId',
                    matchName: '$hdp.matchName',
                    matchDate: '$hdp.matchDate',
                    bet: '$hdp.bet'
                }
            }
        }, {
            "$group": {
                "_id": {
                    leagueId: '$leagueId',
                },
                "leagueName": {$first: "$leagueName"},
                "matches": {$push: '$match'},
            }
        },
        {
            "$project": {
                _id: 0,
                leagueId: '$_id.leagueId',
                leagueName: '$leagueName',
                matches: '$matches'
            }
        }
    ], (err, results) => {

        if (err)
            return res.send({message: "error", results: err, code: 999});

        let resultList = _.chain(results)
            .each(league => {

                let groupsMatch = _.groupBy(league.matches, function (value) {
                    return value.matchId + '|' + value.matchName[lang].h + ' -- vs -- ' + value.matchName[lang].a + '|' + value.matchDate
                });

                league.matches = _.map(groupsMatch, (value, key) => {
                    return {
                        key: key,
                        matchId: key.split('|')[0],
                        name: key.split('|')[1],
                        matchName: value[0].matchName,
                        date: value[0] ? value[0].matchDate : null,
                        matches: value
                    }
                });

                league.matches = _.sortBy(league.matches, function (o) {
                    return o.name;
                });

                _.each(league.matches, (subMatch) => {
                    let betCount = _.countBy(subMatch.matches, function (match) {
                        return match.bet;
                    });

                    delete subMatch.matches;
                    subMatch.bet = betCount;

                });

            }).value();

        resultList = _.sortBy(resultList, function (o) {
            return o.leagueName;
        });

        return res.send(
            {
                code: 0,
                message: "success",
                results: resultList

            }
        );
    });


});

router.get('/stock_oe_x12/:matchId/:bet', function (req, res) {

    let name = req.query.name;

    let condition = {game: 'FOOTBALL', active: true};

    let userInfo = req.userInfo;


    let group = '';
    if (userInfo.group.type === 'SUPER_ADMIN') {
        group = 'superAdmin';
    } else if (userInfo.group.type === 'COMPANY') {
        group = 'company';
    } else if (userInfo.group.type === 'SHARE_HOLDER') {
        group = 'shareHolder';
    } else if (userInfo.group.type === 'SUPER_SENIOR') {
        group = 'superSenior';
    } else if (userInfo.group.type === 'SENIOR') {
        group = 'senior';
    } else if (userInfo.group.type === 'MASTER_AGENT') {
        group = 'masterAgent';
    } else if (userInfo.group.type === 'AGENT') {
        group = 'agent';
    }
    condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(userInfo.group._id);

    condition['hdp.matchId'] = req.params.matchId;
    condition['$and'] = [
        {$or: [{'hdp.oddType': 'OE'}, {'hdp.oddType': 'OE1ST'}, {'hdp.oddType': 'X12'}, {'hdp.oddType': 'X121ST'}]},
        {$or: [{"status": 'RUNNING'}]}
    ];
    condition['hdp.bet'] = req.params.bet;
    console.log(condition)

    BetTransactionModel.aggregate([
            {
                $match: condition
            },
            {
                $project: {
                    _id: 0,
                    memberId: '$memberId',
                    hdp: '$hdp',
                    commission: '$commission.' + group,
                    amount: '$amount',
                    stake_in: '$amount',
                    stake_out: {$multiply: ['$amount', '$hdp.odd']},
                    totalReceive: {$multiply: ['$amount', {$divide: ['$commission.' + group + '.shareReceive', 100]}]},
                    betId: '$betId',
                    createdDate: '$createdDate',
                    priceType: '$priceType',
                    ipAddress: '$ipAddress'
                }
            }
        ],
        (err, results) => {
            if (err)
                return res.send({message: "error", results: err, code: 999});

            let x = {};

            MemberModel.populate(results, {
                path: 'memberId',
                select: '_id username'
            }, function (e, memberResult) {
                if (e) return;
                results.memberId = memberResult;

                if (err)
                    return res.send({message: "error", results: err, code: 999});


                let summary = _.reduce(results, function (memo, num) {
                    return {
                        totalAmount: memo.totalAmount + num.amount,
                        totalReceive: memo.totalReceive + num.totalReceive
                    }
                }, {totalAmount: 0, totalReceive: 0});

                return res.send(
                    {
                        code: 0,
                        message: "success",
                        result: {
                            dataList: results,
                            summary: summary,
                        }
                    }
                );
            });
        });

});

router.get('/stock_hdp_ou', function (req, res) {

    let condition = {game: {$in: ['FOOTBALL', 'M2']}};

    condition.gameDate = {$gte: new Date(moment().add(-7, 'd').format('YYYY-MM-DDT11:00:00.000'))};

    let userInfo = req.userInfo;
    let lang = req.query.lang;

    if (!req.query.lang) {
        lang = 'en';
    }


    condition.active = true;

    if (req.query.matchType) {
        condition['hdp.matchType'] = req.query.matchType;
    }

    let group = '';
    if (userInfo.group.type === 'SUPER_ADMIN') {
        group = 'superAdmin';
    } else if (userInfo.group.type === 'COMPANY') {
        group = 'company';
    } else if (userInfo.group.type === 'SHARE_HOLDER') {
        group = 'shareHolder';
    } else if (userInfo.group.type === 'SENIOR') {
        group = 'senior';
    } else if (userInfo.group.type === 'MASTER_AGENT') {
        group = 'masterAgent';
    } else if (userInfo.group.type === 'AGENT') {
        group = 'agent';
    }
    condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(userInfo.group._id);

    condition['$and'] = [
        {$or: [{'hdp.oddType': 'AH'}, {'hdp.oddType': 'AH1ST'}, {'hdp.oddType': 'OU'}, {'hdp.oddType': 'OU1ST'}]},
        {$or: [{"status": 'RUNNING'}]}
    ];


    async.parallel([(callback) => {

            // condition['hdp.matchType'] = 'NONE_LIVE';

            if (req.query.matchType === 'LIVE' && !_.isEmpty(req.query.matchType)) {
                condition['hdp.matchType'] = 'LIVE';
            }



            BetTransactionModel.aggregate([
                    {
                        $match: condition
                    },
                    {
                        "$group": {
                            "_id": {
                                leagueId: '$hdp.leagueId',
                                matchId: '$hdp.matchId',
                                score: '$hdp.score',
                                matchType: '$hdp.matchType',
                                oddType: '$hdp.oddType',
                            },
                            matchDate: {$last: "$hdp.matchDate"},
                            leagueName: {$last: "$hdp.leagueName"},
                            leagueNameN: {$last: "$hdp.leagueNameN"},
                            matchName: {$last: "$hdp.matchName"},
                            source: {$last: "$source"},
                            homeReceive: {$sum: {$cond: [{$in: ['$hdp.bet', ['HOME', 'OVER']]}, '$commission.' + group + '.amount', 0]}},
                            awayReceive: {$sum: {$cond: [{$in: ['$hdp.bet', ['AWAY', 'UNDER']]}, '$commission.' + group + '.amount', 0]}},
                            totalAmount: {$sum: '$amount'},
                            totalHomeReceive: {$sum: {$cond: [{$in: ['$hdp.bet', ['HOME', 'OVER']]}, '$amount', 0]}},
                            totalAwayReceive: {$sum: {$cond: [{$in: ['$hdp.bet', ['AWAY', 'UNDER']]}, '$amount', 0]}},
                            totalReceive: {$sum: '$commission.' + group + '.amount'},

                            homeBetCount: {$sum: {$cond: [{$eq: ['$hdp.bet', 'HOME']}, 1, 0]}},
                            awayBetCount: {$sum: {$cond: [{$eq: ['$hdp.bet', 'AWAY']}, 1, 0]}},
                            drawHandicap: {$sum: {$cond: [{$eq: ['$hdp.handicap', '0']}, 1, 0]}},

                            homeTor: {
                                $sum: {
                                    $cond: [{
                                        $and: [
                                            {$eq: ['$hdp.bet', 'HOME']},
                                            {$eq: [{$substr: ["$hdp.handicap", 0, 1]}, '-']}
                                        ]
                                    }, 1, 0]
                                }
                            },
                            awayTor: {
                                $sum: {
                                    $cond: [{
                                        $and: [
                                            {$eq: ['$hdp.bet', 'AWAY']},
                                            {$eq: [{$substr: ["$hdp.handicap", 0, 1]}, '-']}
                                        ]
                                    }, 1, 0]
                                }
                            }
                        }
                    },
                    {
                        $project: {
                            _id: 0,
                            leagueId: '$_id.leagueId',
                            matchId: '$_id.matchId',
                            leagueName: '$leagueName',
                            leagueNameN: '$leagueNameN',
                            score: '$_id.score',
                            matchName: '$matchName',
                            matchDate: '$matchDate',
                            source: '$source',
                            matchType: '$_id.matchType',
                            oddType: '$_id.oddType',
                            totalAmount: '$totalAmount',
                            totalReceive: '$totalReceive',
                            homeReceive: '$homeReceive',
                            awayReceive: '$awayReceive',
                            totalHomeReceive: '$totalHomeReceive',
                            totalAwayReceive: '$totalAwayReceive',
                            drawHandicap: '$drawHandicap',
                            homeBetCount: '$homeBetCount',
                            awayBetCount: '$awayBetCount',
                            homeTor: '$homeTor',
                            awayTor: '$awayTor'
                        }
                    },
                    {$sort: {'matchId': 1}},
                ],
                (err, results) => {
                    if (err) {
                        callback(err, null);
                    }
                    else {
                        callback(null, results);
                        // console.log('1',results)
                    }
                });
        },
            (callback) => {
                StockHdpService.getHdpList(
                    (err, data) => {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, data);
                        }
                        // console.log(data,'0000')
                    });

            }
        ],

        function (err, results) {
            // console.log('1',results);
            if (err)
                return res.send({message: "error", results: err, code: 999});

            // console.log(results[1],'11111');
            let noneLiveResult = results[0];
            let hdpall = results[1];

            noneLiveResult = _.chain(noneLiveResult)
                .groupBy((value) => {
                    // console.log(value,'ooo')
                    return value.leagueId + '|' + value.leagueName + '|' + value.source
                })
                .map((value, key) => {
                    // console.log(value,'22222');
                    return {
                        league: key.split('|')[0],
                        name: value[0].leagueNameN['en'].trim(),
                        source: key.split('|')[2],
                        matches: _.map(value, (value, key) => {

                            let tor = '';

                            if (value.homeBetCount == 0 && value.awayBetCount == 0) {
                                tor = 'DRAW';
                            } else {
                                // console.log(value.matchName.en.h + ' : '+value.score)
                                // console.log(value.drawHandicap + ' x ' +(value.homeBetCount + value.awayBetCount) / 2)
                                if (value.drawHandicap > ((value.homeBetCount + value.awayBetCount) / 2)) {
                                    tor = 'DRAW';
                                } else {
                                    if (value.homeBetCount > value.awayBetCount) {
                                        tor = value.homeTor > value.awayTor ? 'HOME' : 'AWAY';
                                    } else {
                                        tor = value.awayTor > value.homeTor ? 'AWAY' : 'HOME';
                                    }
                                }
                                // console.log('tor : ',tor)

                            }

                            return {
                                matchId: value.matchId,
                                matchName: value.matchName,
                                matchDate: value.matchDate,
                                matchType: value.matchType,
                                // source: value.source,
                                score: value.score,
                                oddType: value.oddType,
                                totalAmount: value.totalAmount,
                                totalReceive: value.totalReceive,
                                homeReceive: value.homeReceive,
                                awayReceive: value.awayReceive,
                                totalHomeReceive: value.totalHomeReceive,
                                totalAwayReceive: value.totalAwayReceive,
                                teamTor: tor,


                            };
                        })
                    }
                })
                .each(subMatch => {
                    let groupsMatch = _.groupBy(subMatch.matches, function (value) {
                        return value.matchId + '|' + value.matchName[lang].h + ' vs ' + value.matchName[lang].a + '|' + value.matchDate + '|' + value.matchType + '|' + value.score + '|' + value.source
                    });
                    subMatch.matches = _.map(groupsMatch, (value, key) => {
                        let shareMapping = _.map(value,
                            function (item) {
                                return {
                                    oddType: item.oddType,
                                    receive: item.homeReceive - item.awayReceive,
                                };
                            }
                        );


                        let fullMapping = _.map(value,
                            function (item) {
                                return {
                                    oddType: item.oddType,
                                    receive: item.totalHomeReceive - item.totalAwayReceive,
                                };
                            }
                        );

                        let tor = _.filter(value, (item) => {
                            return item.oddType === 'AH';
                        })[0];


                        return {
                            matchId: key.split('|')[0],
                            name: key.split('|')[1],
                            matchType: key.split('|')[3],
                            score: key.split('|')[4],
                            // source: key.split('|')[5],
                            matchName: value[0].matchName,
                            teamTor: tor ? tor.teamTor : 'DRAW',
                            date: value[0] ? value[0].matchDate : null,
                            totalAmount: _.object(_.map(fullMapping, _.values)),
                            totalReceive: _.object(_.map(shareMapping, _.values)),

                        }
                    });

                    subMatch.matches = subMatch.matches.sort(function (a, b) {
                        return naturalSort(a.score, b.score);
                    });

                    subMatch.matches = _.sortBy(subMatch.matches, function (o) {
                        return o.matchType;
                    });

                    subMatch.matches = _.sortBy(subMatch.matches, function (o) {
                        return o.name;
                    });

                    subMatch.matches = _.sortBy(subMatch.matches, function (o) {
                        return o.date;
                    });
                })
                .sortBy(function (o) {
                    return o.name;
                })
                .value();


            noneLiveResult.forEach((i) => {
                console.log(hdpall, '<===============')
                // i.matches.forEach((j)=>{
                // console.log("====================================")
                let currentMatchId = [];
                i.matches.forEach((sub, index) => {
                    //
                    // console.log('index : '+index+ ' , matchId : '+sub.matchId);

                    let i = _.filter(hdpall, (item) => {
                        return sub.matchId === item.id
                    })[0];
                    // console.log('index : '+index+ ' , matchId : '+sub.matchId);


                    if (i) {
                        if (!currentMatchId.includes(i.id)) {
                            currentMatchId.push(i.id);
                            // console.log(i.id,'currentMatchId');
                            sub.oddSetting = i;
                        }
                    }
                    else {
                        if (!currentMatchId.includes(sub.matchId)) {
                            currentMatchId.push(sub.matchId);
                            sub.oddSetting = {
                                "id": sub.matchId,
                                "ah": {
                                    "h": 0,
                                    "a": 0
                                },
                                "ou": {
                                    "o": 0,
                                    "u": 0
                                },
                                "ah1st": {
                                    "h": 0,
                                    "a": 0
                                },
                                "ou1st": {
                                    "o": 0,
                                    "u": 0
                                }
                            };
                        }
                    }


                });
            });


            return res.send(
                {
                    code: 0,
                    message: "success",
                    results: noneLiveResult
                }
            );

        })


});

router.get('/stock_hdp_ou/:matchId', function (req, res) {

    let condition = {game: {$in: ['FOOTBALL', 'M2']}};
    condition['hdp.matchId'] = req.params.matchId;
    condition['status'] = 'RUNNING';

    let userInfo = req.userInfo;
    let oddType = req.query.oddType;


    let group = '';
    if (userInfo.group.type === 'SUPER_ADMIN') {
        group = 'superAdmin';
    } else if (userInfo.group.type === 'COMPANY') {
        group = 'company';
    } else if (userInfo.group.type === 'SHARE_HOLDER') {
        group = 'shareHolder';
    } else if (userInfo.group.type === 'SUPER_SENIOR') {
        group = 'superSenior';
    } else if (userInfo.group.type === 'SENIOR') {
        group = 'senior';
    } else if (userInfo.group.type === 'MASTER_AGENT') {
        group = 'masterAgent';
    } else if (userInfo.group.type === 'AGENT') {
        group = 'agent';
    }

    condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(userInfo.group._id);

    condition['hdp.matchType'] = req.query.matchType;
    if (req.query.matchType === 'LIVE') {
        condition['hdp.score'] = req.query.score;
    }
    if (oddType) {
        condition['hdp.oddType'] = oddType.toUpperCase();
    }
    condition.active = true;
    console.log(condition);
    BetTransactionModel.aggregate([
        {
            $match: condition
        },
        {
            $project: {
                _id: 0,
                memberId: '$memberId',
                hdp: '$hdp',
                commission: '$commission.' + group,
                amount: '$amount',
                stake_in: '$amount',
                stake_out: {$multiply: ['$amount', '$hdp.odd']},
                totalReceive: {$multiply: ['$amount', {$divide: ['$commission.' + group + '.shareReceive', 100]}]},
                betId: '$betId',
                createdDate: '$createdDate',
                gameDate: '$gameDate',
                priceType: '$priceType',
                ipAddress: '$ipAddress',
            }
        }
    ], (err, results) => {
        if (err)
            return res.send({message: "error", results: err, code: 999});


        MemberModel.populate(results, {
            path: 'memberId',
            select: '_id username'
        }, function (e, memberResult) {
            if (e) return;
            results.memberId = memberResult;

            let homeList, awayList, overList, underList;
            if (oddType === 'ou' || oddType === 'ou1st') {
                overList = _.filter(results, (data) => {
                    return data.hdp.bet === 'OVER';
                });
                underList = _.filter(results, (data) => {
                    return data.hdp.bet === 'UNDER';
                });
            } else {
                homeList = _.filter(results, (data) => {
                    return data.hdp.bet === 'HOME';
                });
                awayList = _.filter(results, (data) => {
                    return data.hdp.bet === 'AWAY';
                });
            }


            let homeSummary = _.reduce(homeList, function (memo, num) {
                return {
                    totalAmount: memo.totalAmount + num.amount,
                    totalReceive: memo.totalReceive + num.totalReceive
                }
            }, {totalAmount: 0, totalReceive: 0});

            let awaySummary = _.reduce(awayList, function (memo, num) {
                return {
                    totalAmount: memo.totalAmount + num.amount,
                    totalReceive: memo.totalReceive + num.totalReceive
                }
            }, {totalAmount: 0, totalReceive: 0});

            let overSummary = _.reduce(overList, function (memo, num) {
                return {
                    totalAmount: memo.totalAmount + num.amount,
                    totalReceive: memo.totalReceive + num.totalReceive
                }
            }, {totalAmount: 0, totalReceive: 0});

            let underSummary = _.reduce(underList, function (memo, num) {
                return {
                    totalAmount: memo.totalAmount + num.amount,
                    totalReceive: memo.totalReceive + num.totalReceive
                }
            }, {totalAmount: 0, totalReceive: 0});


            //forecast
            // let prepareForecast = [];
            // let totalReceive = 0;
            // let gameDate = results[0].gameDate;
            //
            // if (oddType.toUpperCase() === 'AH' || oddType.toUpperCase() === 'AH1ST') {
            //
            //     totalReceive = homeSummary.totalReceive + awaySummary.totalReceive;
            //
            //     let home = req.query.matchType === 'LIVE' ? Number.parseInt(req.query.score.split(':')[0]) : 0;
            //     let away = req.query.matchType === 'LIVE' ? Number.parseInt(req.query.score.split(':')[1]) : 0;
            //     let scoreList = prepareHdpScore(home, away);
            //
            //     _.forEach(scoreList, (scoreItem) => {
            //
            //         let totalHomeWinLose = 0;
            //         _.forEach(homeList, (betObj) => {
            //
            //
            //             let betResult = calculateHandicap(scoreItem[0], scoreItem[1], betObj.hdp.handicap, betObj.hdp.bet);
            //
            //             let amount = betObj.totalReceive;
            //             let winLose = 0;
            //             if (betResult === DRAW) {
            //                 winLose = 0;
            //             } else if (betResult === WIN) {
            //                 FormulaUtils.calculatePayout(betObj.totalReceive, betObj.hdp.odd, betObj.hdp.oddType, betObj.priceType, (payout) => {
            //                     winLose = Number.parseFloat(payout) - betObj.totalReceive;
            //                 });
            //             } else if (betResult === HALF_WIN) {
            //                 FormulaUtils.calculatePayout(betObj.totalReceive, betObj.hdp.odd, betObj.hdp.oddType, betObj.priceType, (payout) => {
            //                     winLose = (Number.parseFloat(payout) - betObj.totalReceive) / 2;
            //                 });
            //             } else if (betResult === LOSE) {
            //                 winLose = 0 - amount;
            //             } else if (betResult === HALF_LOSE) {
            //                 winLose = 0 - (amount / 2);
            //             }
            //             totalHomeWinLose += winLose * -1;
            //         });
            //
            //         let totalAwayWinLose = 0;
            //         _.forEach(awayList, (betObj) => {
            //
            //             let betResult = calculateHandicap(scoreItem[0], scoreItem[1], betObj.hdp.handicap, betObj.hdp.bet);
            //             let amount = betObj.totalReceive;
            //             let winLose = 0;
            //             if (betResult === DRAW) {
            //                 winLose = 0;
            //             } else if (betResult === WIN) {
            //                 FormulaUtils.calculatePayout(betObj.totalReceive, betObj.hdp.odd, betObj.hdp.oddType, betObj.priceType, (payout) => {
            //                     winLose = Number.parseFloat(payout) - betObj.totalReceive;
            //                 });
            //             } else if (betResult === HALF_WIN) {
            //                 FormulaUtils.calculatePayout(betObj.totalReceive, betObj.hdp.odd, betObj.hdp.oddType, betObj.priceType, (payout) => {
            //                     winLose = (Number.parseFloat(payout) - betObj.totalReceive) / 2;
            //                 });
            //             } else if (betResult === LOSE) {
            //                 winLose = 0 - amount;
            //             } else if (betResult === HALF_LOSE) {
            //                 winLose = 0 - (amount / 2);
            //             }
            //             totalAwayWinLose += winLose * -1;
            //         });
            //
            //         // console.log('score : ', scoreItem);
            //         // console.log('totalHomeWinLose : ', Math.round(totalHomeWinLose));
            //         // console.log('totalAwayWinLose : ', Math.round(totalAwayWinLose));
            //
            //         let finalAmount = Math.round(totalHomeWinLose) + Math.round(totalAwayWinLose);
            //         prepareForecast.push({
            //             'score': scoreItem[0] + ' : ' + scoreItem[1],
            //             amount: finalAmount
            //         })
            //
            //
            //     });
            //     console.log(prepareForecast)
            //
            // } else if (oddType.toUpperCase() === 'OU' || oddType.toUpperCase() === 'OU1ST') {
            //
            //     totalReceive = overSummary.totalReceive + underSummary.totalReceive;
            //
            //     let scoreList = prepareOuScore();
            //
            //     _.forEach(scoreList, (scoreItem) => {
            //
            //         let totalHomeWinLose = 0;
            //         _.forEach(overList, (betObj) => {
            //
            //
            //             let betResult = calculateOuScore(scoreItem[0], scoreItem[1], betObj.hdp.handicap, betObj.hdp.bet);
            //
            //             let amount = betObj.totalReceive;
            //             let winLose = 0;
            //             if (betResult === DRAW) {
            //                 winLose = 0;
            //             } else if (betResult === WIN) {
            //                 FormulaUtils.calculatePayout(betObj.totalReceive, betObj.hdp.odd, betObj.hdp.oddType, betObj.priceType, (payout) => {
            //                     winLose = Number.parseFloat(payout) - betObj.totalReceive;
            //                 });
            //             } else if (betResult === HALF_WIN) {
            //                 FormulaUtils.calculatePayout(betObj.totalReceive, betObj.hdp.odd, betObj.hdp.oddType, betObj.priceType, (payout) => {
            //                     winLose = (Number.parseFloat(payout) - betObj.totalReceive) / 2;
            //                 });
            //             } else if (betResult === LOSE) {
            //                 winLose = 0 - amount;
            //             } else if (betResult === HALF_LOSE) {
            //                 winLose = 0 - (amount / 2);
            //             }
            //             totalHomeWinLose += winLose * -1;
            //         });
            //
            //         let totalAwayWinLose = 0;
            //         _.forEach(underList, (betObj) => {
            //
            //             let betResult = calculateOuScore(scoreItem[0], scoreItem[1], betObj.hdp.handicap, betObj.hdp.bet);
            //             let amount = betObj.totalReceive;
            //             let winLose = 0;
            //             if (betResult === DRAW) {
            //                 winLose = 0;
            //             } else if (betResult === WIN) {
            //                 FormulaUtils.calculatePayout(betObj.totalReceive, betObj.hdp.odd, betObj.hdp.oddType, betObj.priceType, (payout) => {
            //                     winLose = Number.parseFloat(payout) - betObj.totalReceive;
            //                 });
            //             } else if (betResult === HALF_WIN) {
            //                 FormulaUtils.calculatePayout(betObj.totalReceive, betObj.hdp.odd, betObj.hdp.oddType, betObj.priceType, (payout) => {
            //                     winLose = (Number.parseFloat(payout) - betObj.totalReceive) / 2;
            //                 });
            //             } else if (betResult === LOSE) {
            //                 winLose = 0 - amount;
            //             } else if (betResult === HALF_LOSE) {
            //                 winLose = 0 - (amount / 2);
            //             }
            //             totalAwayWinLose += winLose * -1;
            //         });
            //
            //         // console.log('score : ', scoreItem);
            //         // console.log('totalHomeWinLose : ', Math.round(totalHomeWinLose));
            //         // console.log('totalAwayWinLose : ', Math.round(totalAwayWinLose));
            //
            //         let finalAmount = Math.round(totalHomeWinLose) + Math.round(totalAwayWinLose);
            //         prepareForecast.push({
            //             'score': scoreItem[0],
            //             amount: finalAmount
            //         })
            //
            //
            //     });
            //     console.log(prepareForecast)
            //
            // }
            //
            //
            // // console.log(gameDate)
            // // console.log(gameDate.getUTCDate())
            // // console.log(gameDate.getUTCMonth())
            // // console.log(gameDate.getUTCFullYear())
            // async.series([callback => {
            //
            //     let currentTime = moment().add(2, 'm');
            //     let matchDate =  moment([Number.parseInt(gameDate.getUTCFullYear()), Number.parseInt(gameDate.getUTCMonth()), Number.parseInt(gameDate.getUTCDate())]);
            //     matchDate.second(0).minute(gameDate.getUTCMinutes()).hour(gameDate.getUTCHours());
            //
            //     console.log('currentTime : ', currentTime)
            //     console.log('gameDate : ', gameDate)
            //     console.log('gameDate2 : ', matchDate)
            //
            //     if (currentTime.isBefore(matchDate)) {
            //         let o = {
            //             isLive: false
            //         }
            //         callback(null, o);
            //     } else {
            //         Commons.getMatchDetails(req.params.matchId, (err, response) => {
            //             let o = {
            //                 isLive: true,
            //                 result: response
            //             }
            //             if (err) {
            //                 console.log('err : ', err)
            //                 callback(null, o);
            //             } else {
            //                 console.log(response)
            //
            //                 callback(null, o);
            //             }
            //         });
            //     }
            //
            // }], (err, asyncResponse) => {
            //
            //     const matchDetail = asyncResponse[0];
            //
            //
            //     if(matchDetail.result) {
            //         let homeScore = matchDetail.isLive ? Number.parseInt(matchDetail.result.i.h) : 0;
            //         let awayScore = matchDetail.isLive ? Number.parseInt(matchDetail.result.i.a) : 0;
            //         //
            //         // if(response.isWaitingForScore || false){
            //         //
            //         // }else {
            //
            //
            //         prepareForecast = _.map(prepareForecast, (item) => {
            //             let netItem = Object.assign({}, item);
            //
            //             if (oddType.toUpperCase() === 'AH' || oddType.toUpperCase() === 'AH1ST') {
            //                 if (item.score == (homeScore + ' : ' + awayScore)) {
            //                     netItem.activeScore = true;
            //                 } else {
            //                     netItem.activeScore = false;
            //                 }
            //             } else if (oddType.toUpperCase() === 'OU' || oddType.toUpperCase() === 'OU1ST') {
            //                 if (item.score == (homeScore + awayScore)) {
            //                     netItem.activeScore = true;
            //                 } else {
            //                     netItem.activeScore = false;
            //                 }
            //             }
            //
            //             return netItem;
            //
            //         });
            //
            //         return res.send({
            //             code: 0,
            //             message: "success",
            //             result: {
            //                 type: oddType,
            //                 homeList: homeList,
            //                 awayList: awayList,
            //                 overList: overList,
            //                 underList: underList,
            //                 homeSummary: homeSummary,
            //                 awaySummary: awaySummary,
            //                 overSummary: overSummary,
            //                 underSummary: underSummary,
            //                 forecastList: prepareForecast,
            //                 currentMatchScore: matchDetail.isLive ? (homeScore + ' : ' + awayScore) : '0 : 0',
            //                 currentMatchTime: matchDetail.isLive ? matchDetail.result.i.lt : '-',
            //                 totalReceive: totalReceive
            //             }
            //         });
            //     }else {
            //         return res.send({
            //             code: 0,
            //             message: "success",
            //             result: {
            //                 type: oddType,
            //                 homeList: homeList,
            //                 awayList: awayList,
            //                 overList: overList,
            //                 underList: underList,
            //                 homeSummary: homeSummary,
            //                 awaySummary: awaySummary,
            //                 overSummary: overSummary,
            //                 underSummary: underSummary,
            //                 forecastList: prepareForecast,
            //                 currentMatchScore: '0 : 0',
            //                 currentMatchTime: '-',
            //                 totalReceive: totalReceive
            //             }
            //         });
            //     }
            //
            //
            //
            // });
            return res.send({
                code: 0,
                message: "success",
                result: {
                    type: oddType,
                    homeList: homeList,
                    awayList: awayList,
                    overList: overList,
                    underList: underList,
                    homeSummary: homeSummary,
                    awaySummary: awaySummary,
                    overSummary: overSummary,
                    underSummary: underSummary,
                }
            });

        });

    });

});

function prepareHdpScore(home, away) {
    let x = [[home, away]];
    for (let i = 1; i <= 5; i++) {
        x.unshift([home + i, away]);
        x.push([home, away + i]);
    }
    return x;
}

function prepareOuScore() {
    let x = [];
    for (let i = 1; i <= 10; i++) {
        x.push([i, 0]);
    }
    return x;
}

router.get('/member_outstanding', function (req, res) {


    let groupId = req.query.groupId;
    // let userInfo = req.userInfo.type;

    async.series([(callback => {
        AgentGroupModel.findById(groupId, (err, response) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, response);
            }
        }).select('type parentId')

    }), (callback => {


        let condition = {};
        condition.gameDate = {$gte: new Date(moment().add(-11, 'd').format('YYYY-MM-DDT11:00:00.000'))};
        condition['memberParentGroup'] = mongoose.Types.ObjectId(groupId);
        condition['game'] = {$in: ['FOOTBALL', 'M2']};
        condition['status'] = 'RUNNING';
        condition['active'] = true;

        BetTransactionModel.aggregate([
            {
                $match: condition
            },
            {
                "$group": {
                    "_id": {
                        commission: '$memberParentGroup',
                        member: '$memberId'
                        // m:'$memberParentGroup'
                    },
                    "amount": {$sum: '$amount'},

                    "memberWinLose": {$sum: '$commission.member.amount'},

                    "agentWinLose": {$sum: '$commission.agent.amount'},

                    "masterAgentWinLose": {$sum: '$commission.masterAgent.amount'},

                    "seniorWinLose": {$sum: '$commission.senior.amount'},

                    "shareHolderWinLose": {$sum: '$commission.shareHolder.amount'},

                    "companyWinLose": {$sum: '$commission.company.amount'},

                    "superAdminWinLose": {$sum: '$commission.superAdmin.amount'},
                }
            },
            {
                $project: {
                    _id: 0,
                    type: 'M_GROUP',
                    member: '$_id.member',
                    group: '$_id.commission',
                    amount: '$amount',
                    memberWinLose: '$memberWinLose',
                    agentWinLose: '$agentWinLose',
                    masterAgentWinLose: '$masterAgentWinLose',
                    seniorWinLose: '$seniorWinLose',
                    shareHolderWinLose: '$shareHolderWinLose',
                    companyWinLose: '$companyWinLose',
                    superAdminWinLose: '$superAdminWinLose',
                    // company_stake: '$company_stake'
                }
            }
        ], (err, results) => {
            if (err) {
                callback(err, null);
            } else {
                MemberModel.populate(results, {
                    path: 'member',
                    select: '_id username username_lower contact'
                }, function (err, memberResult) {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, memberResult);
                    }
                });
            }
        });

    })], function (err, asyncResponse) {

        if (err) {
            return res.send({message: "error", result: err, code: 999});
        }
        let agentGroup = asyncResponse[0];

        let group = '';
        let childGroup;
        if (agentGroup.type === 'SUPER_ADMIN') {
            group = 'superAdmin';
            childGroup = 'company';
        } else if (agentGroup.type === 'COMPANY') {
            group = 'company';
            childGroup = 'shareHolder';
        } else if (agentGroup.type === 'SHARE_HOLDER') {
            group = 'shareHolder';
            childGroup = 'senior';
        } else if (agentGroup.type === 'SENIOR') {
            group = "senior";
            childGroup = 'masterAgent,agent';
        } else if (agentGroup.type === 'MASTER_AGENT') {
            group = "masterAgent";
            childGroup = 'agent';
        } else if (agentGroup.type === 'AGENT') {
            group = "agent";
            childGroup = '';
        }


        let transaction = [];

        async.map(childGroup.split(','), (child, callback) => {

            let condition = {};

            // if (req.query.dateFrom && req.query.dateTo) {
            //     condition['createdDate'] = {
            //         "$gte": new Date(moment(req.query.dateFrom, "DD/MM/YYYY").format('YYYY-MM-DDT00:00:00.000')),
            //         "$lt": new Date(moment(req.query.dateTo, "DD/MM/YYYY").add(1, 'd').format('YYYY-MM-DDT00:00:00.000'))
            //     }
            // }
            condition.gameDate = {$gte: new Date(moment().add(-11, 'd').format('YYYY-MM-DDT11:00:00.000'))};
            condition['memberParentGroup'] = {$ne: mongoose.Types.ObjectId(groupId)};
            condition['status'] = 'RUNNING';
            condition['game'] = {$in: ['FOOTBALL', 'M2']}
            condition['$and'] = [
                {['commission.' + child + '.parentGroup']: mongoose.Types.ObjectId(groupId)},
                {['commission.' + group + '.group']: mongoose.Types.ObjectId(groupId)}
            ];
            condition['active'] = true;

            BetTransactionModel.aggregate([
                {
                    $match: condition
                },
                {
                    "$group": {
                        "_id": {
                            commission: '$commission.' + child + '.group',
                        },
                        "amount": {$sum: '$amount'},

                        "memberWinLose": {$sum: '$commission.member.amount'},

                        "agentWinLose": {$sum: '$commission.agent.amount'},

                        "masterAgentWinLose": {$sum: '$commission.masterAgent.amount'},

                        "seniorWinLose": {$sum: '$commission.senior.amount'},

                        "shareHolderWinLose": {$sum: '$commission.shareHolder.amount'},

                        "companyWinLose": {$sum: '$commission.company.amount'},

                        "superAdminWinLose": {$sum: '$commission.superAdmin.amount'},
                    }
                },
                {
                    $project: {
                        _id: 0,
                        type: 'A_GROUP',
                        group: '$_id.commission',
                        amount: '$amount',
                        memberWinLose: '$memberWinLose',
                        agentWinLose: '$agentWinLose',
                        masterAgentWinLose: '$masterAgentWinLose',
                        seniorWinLose: '$seniorWinLose',
                        shareHolderWinLose: '$shareHolderWinLose',
                        companyWinLose: '$companyWinLose',
                        superAdminWinLose: '$superAdminWinLose',

                    }
                }
            ], (err, results) => {
                _.each(results, item => {
                    transaction.push(item);
                });
                callback();
            });
        }, (err) => {

            if (err)
                return res.send({message: "error", results: err, code: 999});

            let memberGroupList = asyncResponse[1];
            let mergeDataList = transaction.concat(memberGroupList);

            async.series([(callback => {

                AgentGroupModel.populate(mergeDataList, {
                    path: 'group',
                    select: '_id name name_lower contact'
                }, function (err, memberResult) {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, memberResult);
                    }
                });


            })], (err, populateResults) => {

                if (err)
                    return res.send({message: "error", results: err, code: 999});


                let finalResult = transformDataByAgentType(agentGroup.type, mergeDataList);


                finalResult = finalResult.sort(function (a, b) {
                    let aKey = a.type === 'M_GROUP' ? a.member.username : a.group.name;
                    let bKey = b.type === 'M_GROUP' ? b.member.username : b.group.name;
                    return naturalSort(aKey, bKey);
                });

                let summary = _.reduce(finalResult, function (memo, obj) {
                    return {
                        amount: memo.amount + obj.amount,
                        memberWinLose: memo.memberWinLose + obj.memberWinLose,
                        agentWinLose: memo.agentWinLose + obj.agentWinLose,
                        companyWinLose: memo.companyWinLose + obj.companyWinLose
                    }
                }, {
                    amount: 0,
                    memberWinLose: 0,
                    agentWinLose: 0,
                    companyWinLose: 0,
                });


                return res.send(
                    {
                        code: 0,
                        message: "success",
                        result: {
                            group: agentGroup,
                            dataList: finalResult,
                            summary: summary,
                        }
                    }
                );

            });
        });//end forEach Match
    });


});

router.get('/member_outstanding_member_detail', function (req, res) {

    let condition = {};
    let memberId = req.query.memberId;


    condition['memberId'] = memberId;
    condition['status'] = 'RUNNING';
    condition['game'] = {$in: ['FOOTBALL', 'M2']}
    condition['active'] = true;

    async.series([(callback => {
        MemberModel.findById(memberId, (err, response) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, response);
            }
        }).select('group').populate({path: 'group', select: '_id type parentId'});

    })], (err, results) => {

        let memberInfo = results[0];


        BetTransactionModel.find(condition)
            .populate({path: 'memberId', select: '_id username'})
            .select('priceType hdp parlay commission amount memberCredit payout betId createdDate memberId gameType ipAddress')
            .exec(function (err, response) {


                let prepareAmount = transformAmountReceive(memberInfo.group.type, response);

                _.forEach(prepareAmount, item => {
                    item.memberCommission = Math.abs(item.amount * NumberUtils.convertPercent(item.commission.member.commission));
                    item.memberCreditUsage = Math.abs(item.memberCredit);
                    item.memberCommPercent = Math.abs(item.commission.member.commission)
                });


                let summary = _.reduce(prepareAmount, function (memo, obj) {

                    return {
                        amount: memo.amount + obj.amount,

                        memberAmount: memo.memberAmount + obj.memberAmount,

                        agentAmount: memo.agentAmount + obj.agentAmount,

                        companyAmount: memo.companyAmount + obj.companyAmount,

                    }
                }, {
                    amount: 0,
                    memberAmount: 0,
                    agentAmount: 0,
                    companyAmount: 0
                });

                return res.send(
                    {
                        code: 0,
                        message: "success",
                        result: {
                            group: memberInfo.group,
                            dataList: prepareAmount,
                            summary: summary
                        }
                    }
                );
            });

    });

});

router.get('/memberAb', function (req, res) {

    let page = req.query.page ? Number.parseInt(req.query.page) : 10;
    let limit = req.query.limit ? Number.parseInt(req.query.limit) : 1;

    let condition = {active: true};

    MemberModel.paginate(condition, {
        select: "isAutoChangeOdd username_lower",
        lean: true,
        page: page,
        limit: limit,
        populate: {path: 'memberId', select: '_id username'}
    }, (err, data) => {

        if (err) {
            return res.send({message: "error", results: err, code: 999});
        } else {

            let sortingResult = data.docs.sort(function (a, b) {
                return naturalSort(a.username_lower, b.username_lower);
            });

            sortingResult = _.sortBy(sortingResult, (o) => {
                return o.isAutoChangeOdd ? -1 : 1;
            });


            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: {
                        list: sortingResult,
                        total: data.total,
                        limit: data.limit,
                        page: data.page,
                        pages: data.pages,
                    }
                }
            );
        }
    });

});


router.post('/re-running-bet', function (req, res) {


    let condition = {betId: req.body.betId, game: 'FOOTBALL', status: 'CANCELLED'};

    BetTransactionModel.findOne(condition, (err, betResponse) => {

        if (err) {
            return res.send({message: "error", result: err, code: 999});
        }

        if (!betResponse) {
            return res.send({message: "betId not found", result: err, code: 999});
        }


        MemberService.updateBalance2(betResponse.memberUsername, betResponse.memberCredit, betResponse.betId, 'SPORTS_BOOK_BET_RE_BET', betResponse.betId, (err, response) => {
            if (err) {
                return res.send({message: "error", result: err, code: 999});
            }

            let updateBody = {
                status: 'RUNNING'
            };

            BetTransactionModel.update({_id: betResponse._id}, updateBody, (err, data) => {
                if (err) {
                    return res.send({message: "error", result: err, code: 999});
                } else {
                    return res.send({
                        code: 0,
                        message: "success"
                    });
                }
            });
        });
    });
});


function transformAmountReceive(memberGroupType, dataList) {

    console.log('transformAmountReceive : ', memberGroupType)

    if (memberGroupType === 'SENIOR') {
        return _.map(dataList, function (item) {

            let newItem = Object.assign({}, item)._doc;

            newItem.companyAmount = _.defaults(item.commission.company, {amount: 0}).amount + _.defaults(item.commission.shareHolder, {amount: 0}).amount;

            newItem.agentAmount = _.defaults(item.commission.senior, {amount: 0}).amount;

            newItem.memberAmount = newItem.companyAmount + newItem.agentAmount;

            newItem.memberCommission = Math.abs(newItem.amount * NumberUtils.convertPercent(newItem.commission.member.commission));
            newItem.memberCreditUsage = Math.abs(newItem.memberCredit);

            return newItem;
        });
    } else if (memberGroupType === 'MASTER_AGENT') {
        return _.map(dataList, function (item) {

            let newItem = Object.assign({}, item)._doc;

            newItem.companyAmount = _.defaults(item.commission.company, {amount: 0}).amount + _.defaults(item.commission.shareHolder, {amount: 0}).amount + _.defaults(item.commission.senior, {amount: 0}).amount;

            newItem.agentAmount = _.defaults(item.commission.masterAgent, {amount: 0}).amount;

            newItem.memberAmount = newItem.companyAmount + newItem.agentAmount;

            return newItem;
        });
    } else if (memberGroupType === 'AGENT') {
        return _.map(dataList, function (item) {

            let newItem = Object.assign({}, item)._doc;

            newItem.companyAmount = _.defaults(item.commission.company, {amount: 0}).amount + _.defaults(item.commission.shareHolder, {amount: 0}).amount + _.defaults(item.commission.senior, {amount: 0}).amount
                + _.defaults(item.commission.masterAgent, {amount: 0}).amount;

            newItem.agentAmount = _.defaults(item.commission.agent, {amount: 0}).amount;

            newItem.memberAmount = newItem.companyAmount + newItem.agentAmount;

            return newItem;
        });
    } else {
        return dataList;
    }
}

function transformDataByAgentType(type, dataList) {
    if (type === 'SUPER_ADMIN') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = 0;

            newItem.agentWinLose = item.superAdminWinLose;

            newItem.memberWinLose = (item.masterAgentWinLose + item.agentWinLose + item.seniorWinLose + item.shareHolderWinLose + item.companyWinLose);

            return newItem;
        });
    } else if (type === 'COMPANY') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = item.superAdminWinLose;

            newItem.agentWinLose = item.companyWinLose;

            newItem.memberWinLose = (item.masterAgentWinLose + item.agentWinLose + item.seniorWinLose + item.shareHolderWinLose);

            return newItem;
        });
    } else if (type === 'SHARE_HOLDER') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose;

            newItem.agentWinLose = item.shareHolderWinLose;

            newItem.memberWinLose = (item.masterAgentWinLose + item.agentWinLose + item.seniorWinLose);
            return newItem;
        });
    } else if (type === 'SENIOR') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose + item.shareHolderWinLose;

            newItem.agentWinLose = item.seniorWinLose;

            newItem.memberWinLose = (item.masterAgentWinLose + item.agentWinLose);
            return newItem;
        });
    } else if (type === 'MASTER_AGENT') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose + item.shareHolderWinLose + item.seniorWinLose;

            newItem.agentWinLose = item.masterAgentWinLose;

            newItem.memberWinLose = (item.agentWinLose);

            return newItem;
        });
    } else if (type === 'AGENT') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);
            newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose + item.shareHolderWinLose + item.seniorWinLose + item.masterAgentWinLose;

            newItem.agentWinLose = item.agentWinLose;

            newItem.memberWinLose = item.memberWinLose;
            return newItem;
        });
    } else {
        return dataList;
    }
}

//DUPLICATE ENDSCORE


function calculateOneTwo(home, away, bet) {

    if (bet === 'DRAW' && home == away) {
        return "WIN"
    } else if (bet === 'HOME' && home > away) {
        return "WIN"
    } else if (bet === 'AWAY' && away > home) {
        return "WIN"
    }
    return "LOSE";
}

function calculateOeScore(home, away, bet) {
    let oe = (home + away) % 2;
    if ((bet === 'EVEN' && oe == 0) || (bet === 'ODD' && oe != 0)) {
        return "WIN"
    }
    return "LOSE";
}

function calculateOuScore(home, away, handicap, bet) {

    let totalScore = (home + away);
    let matchHandicap = prePareHandicap(handicap);

    console.log('calculateOuScore');
    console.log('totalScore : ', totalScore);
    console.log('matchHandicap : ', matchHandicap);
    let finalRate;
    if (bet === 'OVER') {
        finalRate = totalScore - matchHandicap;
    } else {
        finalRate = matchHandicap - totalScore;
    }

    let result;
    console.log('finalRate : ', finalRate);
    if (finalRate == 0) {
        result = 'DRAW';
    } else if (finalRate == 0.25) {
        result = 'HALF_WIN';
    } else if (finalRate == -0.25) {
        result = 'HALF_LOSE';
    } else if (finalRate >= 0.5) {
        result = 'WIN';
    } else if (finalRate <= -0.5) {
        result = 'LOSE';
    }
    return result;
}

function prePareHandicap(handicap) {
    let xhan = handicap.split('/');

    let matchHandicap;

    if (xhan.length == 2) {
        let fh = Math.abs(Number.parseFloat(xhan[0]));
        let sh = Number.parseFloat(xhan[1]);
        matchHandicap = fh + ((sh - fh) / 2);
    } else {
        matchHandicap = Math.abs(Number.parseFloat(handicap));
    }
    return matchHandicap;
}

function calculateHandicap(homeScore, awayScore, handicap, bet) {

    let home = Number.parseFloat(homeScore);
    let away = Number.parseFloat(awayScore);

    let matchHandicap = prePareHandicap(handicap);

    let isTor = handicap.startsWith('-');

    let torScore;
    let longScore;

    if (bet === 'HOME') {
        if (isTor) {
            torScore = home;
            longScore = away;
        } else {
            torScore = away;
            longScore = home;
        }
    } else if (bet === 'AWAY') {
        if (isTor) {
            torScore = away;
            longScore = home;
        } else {
            torScore = home;
            longScore = away;
        }
    }


    let finalRate;


    if (isTor) {
        finalRate = torScore - (longScore + matchHandicap);
    } else {
        finalRate = (longScore + matchHandicap) - torScore;
    }


    let result;
    if (finalRate == 0) {
        result = 'DRAW';
    } else if (finalRate == 0.25) {
        result = 'HALF_WIN';
    } else if (finalRate == -0.25) {
        result = 'HALF_LOSE';
    } else if (finalRate >= 0.5) {
        result = 'WIN';
    } else if (finalRate <= -0.5) {
        result = 'LOSE';
    }
    return result;
}

module.exports = router;