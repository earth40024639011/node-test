const UserModel = require('../../models/users.model');
const express = require('express')
const router = express.Router()
const async = require("async");
const Joi = require('joi');
const mongoose = require('mongoose');
const Hashing = require('../../common/hashing');
const config = require('config');
const logger = require('../../../utils/logger');
const naturalSort = require("javascript-natural-sort");
const _ = require('underscore');

router.get('/list', function (req, res) {

    let condition = {};

    let userInfo = req.userInfo;


    if (req.query.active) {
        condition.active = req.query.active;
    }


    if (userInfo.group._id) {
        condition.group = mongoose.Types.ObjectId(userInfo.group._id);
    }


    UserModel.find(condition, function (err, response) {
        if (err)
            return res.send({message: "error", results: err, code: 999});


        let sortingResult = response.sort(function (a, b) {
            return naturalSort(a.username_lower, b.username_lower);
        });

        sortingResult = _.sortBy(sortingResult, function (o) {
            return o.active ? -1 : 1;
        });

        sortingResult = _.filter(sortingResult, function (item) {
            return item.username_lower !== userInfo.group.name;
        });


        return res.send(
            {
                code: 0,
                message: "success",
                result: {
                    list: sortingResult
                }
            }
        );
    }).populate('group');

});


router.get('/:userId', function (req, res) {


    UserModel.findById(req.params.userId, function (err, data) {
        if (err)
            return res.send({message: "error", result: err, code: 999});

        return res.send(
            {
                code: 0,
                message: "success",
                result: data
            }
        );
    });

});

const addUserSchema = Joi.object().keys({
    username: Joi.string().required(),
    password: Joi.string().required(),
    transactionCode: Joi.string().allow(''),
    contact: Joi.string().allow('', null),
    permission: Joi.array().items(
        Joi.string().label('ALL').allow(),
        Joi.string().label('STOCK_MANAGEMENT').allow(),
        Joi.string().label('MEMBER_MANAGEMENT').allow(),
        Joi.string().label('MEMBER_MANAGEMENT_VIEW').allow(),
        Joi.string().label('REPORT').allow(),
        Joi.string().label('OLD_REPORT').allow(),
        Joi.string().label('PAYMENT').allow(),
        Joi.string().label('CASH').allow(),
        Joi.string().label('CORRECT_SCORE').allow())
});


router.post('/', function (req, res) {

    let validate = Joi.validate(req.body, addUserSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    const userInfo = req.userInfo;

    UserModel.findOne({_id:userInfo._id},(err,checkAgentResponse)=> {


        if (!checkAgentResponse) {

            return res.send({
                message: "UnAuthorization",
                result: err,
                code: 401
            });


        } else {


            let body = {
                username: req.body.username,
                username_lower: req.body.username.toLowerCase(),
                contact: req.body.contact,
                permission: req.body.permission,
                group: userInfo.group._id,
                active: true
            };

            Hashing.encrypt256(req.body.password, function (err, hashingPass) {
                body.password = hashingPass;
            });

            let model = new UserModel(body);

            model.save(function (err, data) {

                if (err) {
                    return res.send({message: "error", results: err, code: 999});
                }
                return res.send(
                    {
                        code: 0,
                        message: "success",
                        result: data
                    }
                );
            });
        }
    });

});


const updateUserSchema = Joi.object().keys({
    password: Joi.string().allow('', null),
    transactionCode: Joi.string().allow(''),
    contact: Joi.string().allow('', null),
    permission: Joi.array().items(
        Joi.string().label('ALL').allow(),
        Joi.string().label('STOCK_MANAGEMENT').allow(),
        Joi.string().label('MEMBER_MANAGEMENT').allow(),
        Joi.string().label('MEMBER_MANAGEMENT_VIEW').allow(),
        Joi.string().label('REPORT').allow(),
        Joi.string().label('OLD_REPORT').allow(),
        Joi.string().label('PAYMENT').allow(),
        Joi.string().label('CASH').allow(),
        Joi.string().label('CORRECT_SCORE').allow())
});

router.put('/update/:userId', function (req, res) {

    let validate = Joi.validate(req.body, updateUserSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    let userInfo = req.userInfo;


    let body = {
        contact: req.body.contact,
        permission: req.body.permission,
        active: true
    };

    UserModel.findOne({_id: mongoose.Types.ObjectId(userInfo._id)},(err,userActionResponse) => {

        if(userActionResponse) {

            UserModel.findOne({_id: mongoose.Types.ObjectId(req.params.userId)},(err,userEditResponse) => {

                if(userActionResponse.group.toString() == userEditResponse.group.toString()){


                    if (req.body.password !== '' && req.body.password != null) {
                        Hashing.encrypt256(req.body.password, function (err, hashingPass) {
                            body.password = hashingPass;
                        });
                    }


                    UserModel.update({_id: mongoose.Types.ObjectId(req.params.userId)}, body, function (err, data) {

                        if (err) {
                            return res.send({message: "error", results: err, code: 999});
                        }

                        return res.send(
                            {
                                code: 0,
                                message: "success",
                                result: data
                            }
                        );
                    });

                }else {
                    return res.send({message: "error", results: err, code: 999});
                }

            });


        }else {
            return res.send({message: "error", results: err, code: 999});
        }

    });

});


const updateStatusSchema = Joi.object().keys({
    passwordLock: Joi.boolean().required(),
    active: Joi.boolean().required(),
});


router.put('/status/:userId', function (req, res) {

    let validate = Joi.validate(req.body, updateStatusSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }


    let body = {
        passwordLock: req.body.passwordLock,
        active: req.body.active,
    };

    UserModel.update({_id: mongoose.Types.ObjectId(req.params.userId)}, body, function (err, data) {
        if (err) {
            return res.send({message: "error", result: err, code: 999});
        }

        return res.send(
            {
                code: 0,
                message: "success",
                result: data
            }
        );
    });

});


const resetPasswordSchema = Joi.object().keys({
    oldPassword: Joi.string().required(),
    newPassword: Joi.string().min(6).max(15).required()
});

router.put('/reset-password', function (req, res) {
    let validate = Joi.validate(req.body, resetPasswordSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            result: validate.error.details,
            code: 999
        });
    }

    const username = req.userInfo.username;
    let oldPassword = '';
    let newPassword = '';

    Hashing.encrypt256(req.body.oldPassword, function (err, hashingPass) {
        oldPassword = hashingPass;
    });

    Hashing.encrypt256(req.body.newPassword, function (err, hashingPass) {
        newPassword = hashingPass;
    });

    // const oldPass = req.body.oldHint;

    if(username === 'SB88') {
        UserModel.findOne({username: username}, function (err, data) {

            if (err) {
                return res.send({message: "error", result: err, code: 999});
            }
            if (data) {
                if (data.password !== oldPassword) {
                    return res.send({
                        message: "invalid password",
                        result: "invalid password",
                        code: 999
                    });
                }
                if (data.password === newPassword) {
                    return res.send({
                        message: "Is not able to use the same old password ",
                        result: "Is not able to use the same old password ",
                        code: 999
                    });
                }
                return res.send({message: "success", result: "success", code: 0});
            } else {
                return res.send({message: "invalid user", results: err, code: 999});
            }

        });
    }
    else {
        UserModel.findOne({username: username}, function (err, data) {

            if (err) {
                return res.send({message: "error", result: err, code: 999});
            }
            if (data) {
                if (data.password !== oldPassword) {
                    return res.send({
                        message: "invalid password",
                        result: "invalid password",
                        code: 999
                    });
                }
                if (data.password === newPassword) {
                    return res.send({
                        message: "Is not able to use the same old password ",
                        result: "Is not able to use the same old password ",
                        code: 999
                    });
                }


                UserModel.update({username: username}, {
                    password: newPassword,
                    // forceChangePassword: false,
                    // forceChangePasswordDate: DateUtils.convertThaiDate(moment().add(3, 'month').toDate())
                }, function (err, response) {
                    if (err) {
                        return res.send({message: "error", result: err, code: 999});
                    }
                    if (response.n === 1) {
                        return res.send({message: "success", result: "success", code: 0});

                    } else {
                        return res.send({message: "success", result: "success", code: 0});
                    }
                });
            } else {
                return res.send({message: "invalid user", results: err, code: 999});
            }

        });
    }
});


module.exports = router;