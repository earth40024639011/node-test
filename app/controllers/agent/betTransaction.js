const MemberModel = require('../../models/member.model');
const BetTransactionModel = require('../../models/betTransaction.model.js');
const RobotTransactionModel = require('../../models/robotTransaction.model.js');
const AgentGroupModel = require('../../models/agentGroup.model.js');
const SeqBettingTransactionModel = require('../../models/seqBettingTransaction.model.js');


const Commons = require('../../common/commons');
const express = require('express');
const router = express.Router();
const async = require("async");
const Joi = require('joi');
const mongoose = require('mongoose');
const config = require('config');
const _ = require('underscore');
const _Utils = require('../../common/underscoreUtils');
const FormulaUtils = require('../../common/formula');
const MemberService = require('../../common/memberService');
const AgentService = require("../../common/agentService");
const moment = require('moment');
const DateUtils = require('../../common/dateUtils');
const roundTo = require('round-to');
const naturalSort = require("javascript-natural-sort");
const ApiService = require('../../common/apiService');
const RedisService = require('../../common/redisService');

const clientWhiteList = ['AMB_SPORTBOOK'];
const CLIENT_NAME = process.env.CLIENT_NAME || 'SPORTBOOK88';

//TODO: duplicate in stock
router.get('/list', function (req, res) {

    let condition = {};

    let userInfo = req.userInfo;

    if (req.query.dateFrom && req.query.dateTo) {
        condition['createdDate'] = {
            "$gte": moment(req.query.dateFrom, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).utc(true).toDate(),
            "$lt": moment(req.query.dateTo, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).add(1,'d').utc(true).toDate()
        }
    }


    let orCondition = [];

    if (req.query.status) {
        orCondition.push({
            $or: req.query.status.split(',').map(function (status) {
                return {'status': status}
            })
        });
    }

    if (req.query.oddType && req.query.oddType !== 'ALL') {
        if (req.query.gameType === 'PARLAY') {
            condition['gameType'] = 'PARLAY';
        } else if (req.query.gameType === 'TODAY') {
            condition['gameType'] = 'TODAY';
            if (req.query.oddType === 'HDP') {
                orCondition.push({$or: [{'hdp.oddType': 'AH'}, {'hdp.oddType': 'AH1ST'}]})
            } else if (req.query.oddType === 'OU') {
                orCondition.push({$or: [{'hdp.oddType': 'OU'}, {'hdp.oddType': 'OU1ST'}]})
            } else if (req.query.oddType === 'OE') {
                orCondition.push({$or: [{'hdp.oddType': 'OE'}, {'hdp.oddType': 'OE1ST'}]})
            } else if (req.query.oddType === 'X12') {
                orCondition.push({$or: [{'hdp.oddType': 'X12'}, {'hdp.oddType': 'X121ST'}]})
            } else {
                condition['hdp.oddType'] = req.query.oddType;
            }
        }
    }

    if (orCondition.length > 0) {
        condition['$and'] = orCondition;
    }

    let group = '';
    let child = '';
    if (userInfo.group.type === 'COMPANY') {
        group = 'company';
        child = 'shareHolder';
    } else if (userInfo.group.type === 'SHARE_HOLDER') {
        group = 'shareHolder';
        child = 'superSenior';
    } else if (userInfo.group.type === 'SUPER_SENIOR') {
        group = 'superSenior';
        child = 'senior';
    } else if (userInfo.group.type === 'SENIOR') {
        group = 'senior';
        child = 'masterAgent';
    } else if (userInfo.group.type === 'MASTER_AGENT') {
        group = 'masterAgent';
        child = 'agent';
    } else if (userInfo.group.type === 'AGENT') {
        group = 'agent';
        child = 'member';
    }
    condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(userInfo.group._id);

    console.log(condition);
    BetTransactionModel.aggregate([
        {
            $match: condition
        },
        {
            $project: {
                _id: 0,
                memberId: '$memberId',
                hdp: '$hdp',
                parlay: '$parlay',
                commission: '$commission.' + group,
                child: '$commission.' + child,
                amount: '$amount',
                payout: '$payout',
                status: '$status',
                totalReceive: {$multiply: ['$amount', {$divide: ['$commission.' + group + '.shareReceive', 100]}]},
                stake_in: '$amount',
                stake_out: {
                    $cond: [{$eq: ['$gameType', 'PARLAY']}, {$multiply: ['$amount', '$parlay.odds']}, {$multiply: ['$amount', '$hdp.odd']}]
                },
                betId: '$betId',
                createdDate: '$createdDate',
                priceType: '$priceType',
                gameType: '$gameType'
            }
        }
    ], (err, results) => {
        if (err)
            return res.send({message: "error", results: err, code: 999});


        async.series([(callback => {

            MemberModel.populate(results, {
                path: 'memberId',
                select: '_id username'
            }, function (err, memberResult) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, memberResult);
                }
            });


        }), (callback => {

            AgentGroupModel.populate(results, {
                path: 'commission.group',
                select: '_id name'
            }, function (err, memberResult) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, memberResult);
                }
            });

        }), (callback => {

            AgentGroupModel.populate(results, {
                path: 'child.group',
                select: '_id name'
            }, function (err, memberResult) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, memberResult);
                }
            });

        })], (err, populateResults) => {

            if (err)
                return res.send({message: "error", results: err, code: 999});

            let summary = _.reduce(results, function (memo, num) {
                return {
                    totalAmount: memo.totalAmount + num.amount,
                    totalReceive: memo.totalReceive + num.totalReceive
                }
            }, {totalAmount: 0, totalReceive: 0});

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: {
                        betList: results,
                        summary: summary,
                    }
                }
            );
        });
    });

});


function generateBetId() {
    return 'BET' + new Date().getTime();
}


function calculateParlayPayout(amount, odd, callback) {
    let maxPayout = 1000000.1;
    let max = roundTo(maxPayout / odd, 2);
    if (amount > max) {
        callback(5012, null);
    } else {
        callback(null, '');
        // callback((amount * odd).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]);
    }
}


const addBetSchema = Joi.object().keys({
    memberId: Joi.string().required(),
    matchId: Joi.string().required(),
    odd: Joi.number().required(),
    oddKey: Joi.string().required(),
    oddType: Joi.string().required(),
    handicap: Joi.string().allow(''),
    bet: Joi.string().allow(''),
    gameType: Joi.string().allow(''),
    matchType: Joi.string().required(),
    acceptAnyOdd: Joi.boolean().required(),
    homeScore: Joi.number().required(),
    awayScore: Joi.number().required(),
    priceType: Joi.string().required(),
    amount: Joi.number().required(),
    payout: Joi.number().required(),
    allKey: Joi.array(),
    ip: Joi.string().required(),
    source:Joi.string().required()
});


router.post('/hdp', function (req, res) {

    let validate = Joi.validate(req.body, addBetSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    console.log(req.body)
    const source = req.body.source;
    const userInfo = req.userInfo;
    const membercurrency = userInfo.currency;
    async.waterfall([
            (callback) => {
                if (_.contains(clientWhiteList, CLIENT_NAME)) {
                    const time_out = [0, 350, 650, 950, 1250, 1550, 1850, 2150, 2450, 2750];
                    const seq = SeqBettingTransactionModel();
                    seq.username = req.userInfo.username;

                    seq.save((error, data) => {
                        if (error) {

                            const random = _.random(0, 7);
                            const sec = req.body.matchType === 'LIVE' ? 0 : time_out[random];
                            setTimeout(() => {
                                callback(null, sec);
                            }, sec);
                        } else {
                            let sec = 0;
                            console.log(' ####============> ', req.body.matchType);
                            if (req.body.matchType !== 'LIVE') {
                                const transactionLast = _.chain(`${data.transactionId}`)
                                    .flatten()
                                    .last()
                                    .value();
                                console.log(' ####============> ', (transactionLast % 5));
                                sec = req.body.matchType === 'LIVE' ? 0 : (parseInt(transactionLast) % 5) * 550;
                            }
                            console.log(' ####============> ', sec);
                            setTimeout(() => {
                                callback(null, sec);
                            }, sec);
                        }
                    });
                } else {
                    callback(null, 0);
                }

            }
        ],
        (error, response) => {
            async.series([
                    (callback) => {
                        MemberService.getOddAdjustment(req.userInfo.username, (err, result) => {
                            if (err) {
                                callback(error, null);
                            } else {
                                callback(null, result);
                            }
                        });
                    },

                    (callback) => {
                        MemberService.getLimitSetting(req.userInfo._id, (err, result) => {
                            if (err) {
                                callback(error, null);
                            } else {
                                callback(null, result.limitSetting);
                            }
                        });
                    },

                    (callback) => {
                        RedisService.getCurrency((error, result) => {
                            if (error) {
                                callback(null, false);
                            } else {
                                callback(null, result);
                            }
                        })
                    }
                ],
                (error, resultOddAndLimit) => {
                    if (error) {
                        return res.send({
                            code: 9999,
                            message: error
                        });
                    } else {

                        const [
                            oddAdjust,
                            limitSetting,
                            currency
                        ] = resultOddAndLimit;

                        async.series([callback => {

                            //TODO: duplicate find member
                            MemberModel.findById(req.body.memberId, (err, response) => {
                                if (err)
                                    callback(err, null);

                                if (response.suspend) {
                                    callback(5011, null);
                                } else {
                                    AgentService.getUpLineStatus(userInfo.group._id, (err, status) => {
                                        if (err) {
                                            callback(err, null);
                                        } else {
                                            if (status.suspend) {
                                                callback(5011, null);
                                            } else {
                                                callback(null, status);
                                            }
                                        }
                                    });
                                }
                            });


                        }, callback => {

                            if (userInfo.group.type === 'API') {
                                callback(null, 0);
                            } else {
                                MemberService.getCredit(userInfo._id, (err, credit) => {
                                    if (err) {
                                        callback(err, null);
                                    } else {
                                        if (credit < req.body.amount) {
                                            callback(5005, null);
                                        } else {
                                            callback(null, credit);
                                        }
                                    }
                                });
                            }

                        }, callback => {

                            FormulaUtils.calculatePayout(req.body.amount, req.body.odd, req.body.oddType, req.body.priceType.toUpperCase(), (payout) => {
                                // if (req.body.payout != payout) {
                                //     callback(5004, null);
                                // } else {
                                callback(null, payout);
                                // }
                            });

                        }], (err, results) => {

                            if (err) {
                                if (err == 5004) {
                                    return res.send({
                                        message: "payout not matched.",
                                        result: null,
                                        code: 5004
                                    });
                                }
                                if (err == 5005) {
                                    return res.send({
                                        message: "credit exceed.",
                                        result: null,
                                        code: 5005
                                    });
                                }
                                if (err == 5011) {
                                    return res.send({
                                        message: "Account or Upline was suspend.",
                                        result: null,
                                        code: 5011
                                    });
                                }
                                return res.send({message: "error", result: err, code: 999});
                            }

                            async.waterfall([callback => {

                                MemberModel.findById(req.body.memberId, (err, response) => {
                                    if (err)
                                        callback(err, null);

                                    if (response) {

                                        let maxBet = findMaxBet(response, req.body.oddType);

                                        if (req.body.amount > maxBet) {
                                            callback(5001, null);
                                        } else {
                                            callback(null, response);

                                        }
                                    }
                                });

                            }, (memberInfo, callback) => {


                                const oddType = req.body.oddType.toUpperCase();

                                if (oddType === 'AH' || oddType === 'AH1ST' || oddType === 'OU' || oddType === 'OU1ST' || oddType === 'OE' || oddType === 'OE1ST') {


                                    let condition = {
                                        memberId: mongoose.Types.ObjectId(req.body.memberId),
                                        'hdp.matchId': req.body.matchId
                                    };
                                    condition['$or'] = [{"status": 'RUNNING'}, {"status": 'WAITING'}, {"status": 'DONE'}];
                                    BetTransactionModel.aggregate([
                                        {
                                            $match: condition
                                        },
                                        {
                                            $project: {_id: 0}
                                        },
                                        {
                                            "$group": {
                                                "_id": {
                                                    id: '$_id'
                                                },
                                                "amount": {$sum: '$amount'}
                                            }
                                        }
                                    ], function (err, results) {

                                        if (err) {
                                            callback(err, '');
                                            return;
                                        }

                                        let totalBet = results[0] ? results[0].amount : 0;

                                        if ((totalBet + Number.parseFloat(req.body.amount)) > convertMinMaxByCurrency(memberInfo.limitSetting.sportsBook.hdpOuOe.maxPerMatch, currency, membercurrency)) {
                                            callback(5002, null);
                                        } else {
                                            callback(null, memberInfo, totalBet);
                                        }
                                    });
                                } else {
                                    callback(null, memberInfo, 0);
                                }

                            }, (memberInfo, totalBet, callback) => {

                                AgentGroupModel.findById(userInfo.group._id).select('_id type parentId shareSetting commissionSetting').populate({
                                    path: 'parentId',
                                    select: '_id type parentId shareSetting commissionSetting name',
                                    populate: {
                                        path: 'parentId',
                                        select: '_id type parentId shareSetting commissionSetting name',
                                        populate: {
                                            path: 'parentId',
                                            select: '_id type parentId shareSetting commissionSetting name',
                                            populate: {
                                                path: 'parentId',
                                                select: '_id type parentId shareSetting commissionSetting name',
                                                populate: {
                                                    path: 'parentId',
                                                    select: '_id type parentId shareSetting commissionSetting name',
                                                    populate: {
                                                        path: 'parentId',
                                                        select: '_id type parentId shareSetting commissionSetting name',
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }).exec(function (err, response) {
                                    if (err)
                                        callback(err, null);
                                    else
                                        callback(null, memberInfo, totalBet, response);
                                });

                            }, (memberInfo, totalBet, commission, callback) => {


                                if(source.toUpperCase() === 'FOOTBALL') {
                                    if (req.body.matchType === 'LIVE') {
                                        Commons.cal_bp_live_single_match(req.body.matchId, req.body.oddType.toLowerCase(), req.body.oddKey.toLowerCase(), memberInfo.commissionSetting.sportsBook.typeHdpOuOe, oddAdjust, limitSetting, function (err, response) {
                                            if (err)
                                                callback(err, null);


                                            if (_.isUndefined(response) ||
                                                _.isUndefined(response.min) ||
                                                _.isNull(response.min) ||  req.body.amount < convertMinMaxByCurrency(response.min, currency, membercurrency)) {
                                                callback(5009);
                                                return;
                                            }

                                            if (_.isUndefined(response) ||
                                                _.isUndefined(response.max) ||
                                                _.isNull(response.max) ||  req.body.amount > convertMinMaxByCurrency(response.max, currency, membercurrency)) {
                                                callback(5010);
                                                return;
                                            }

                                            if (response.score.h !== req.body.homeScore || response.score.a !== req.body.awayScore) {
                                                callback(5006);
                                                return;
                                            }

                                            checkOddProcess(response, req.body.priceType, req.body.oddType, req.body.oddKey, req.body.odd, req.body.handicap, function (isOddNotMatch, key, oddValue, handicapValue, bet) {

                                                let odd = {
                                                    leagueName: response.league_name,
                                                    leagueNameN: response.league_name_n,
                                                    matchId: response.matchId,
                                                    matchName: response.name,
                                                    matchDate: response.date,
                                                    oddType: req.body.oddType,
                                                    odd: oddValue,
                                                    key: key,
                                                    oddKey: req.body.oddKey,
                                                    handicap: handicapValue,
                                                    satang: response.s,
                                                    max: response.max,
                                                    isHalfTime: response.isHalfTime,
                                                    bet: bet,
                                                    maxPerMatch: response.maxPerMatch,
                                                    detail: response.detail
                                                };

                                                if (isOddNotMatch) {
                                                    if (req.body.acceptAnyOdd) {
                                                        callback(null, memberInfo, totalBet, commission, odd);
                                                    } else {
                                                        callback(5003, memberInfo, totalBet, commission, odd);
                                                    }
                                                } else {
                                                    callback(null, memberInfo, totalBet, commission, odd);
                                                }
                                            });

                                        });
                                    } else {
                                        Commons.cal_bp_hdp_single_match(req.body.matchId, req.body.oddType, req.body.oddKey.toLowerCase(), memberInfo.commissionSetting.sportsBook.typeHdpOuOe, oddAdjust, req.userInfo._id, function (err, response) {
                                            if (err)
                                                callback(err, null);
                                            else {

                                                console.log('----- :: ',response)
                                                checkOddProcess(response, req.body.priceType, req.body.oddType, req.body.oddKey, req.body.odd, req.body.handicap, function (isOddNotMatch, key, oddValue, handicapValue, bet) {

                                                    let odd = {
                                                        leagueName: response.league_name,
                                                        leagueNameN: response.league_name_n,
                                                        matchId: response.matchId,
                                                        matchName: response.name,
                                                        matchDate: response.date,
                                                        oddType: req.body.oddType,
                                                        odd: oddValue,
                                                        key: key,
                                                        oddKey: req.body.oddKey,
                                                        handicap: handicapValue,
                                                        satang: response.s,
                                                        max: response.max,
                                                        bet: bet,
                                                        maxPerMatch: response.maxPerMatch
                                                    };


                                                    if (isOddNotMatch) {
                                                        if (req.body.acceptAnyOdd) {
                                                            callback(null, memberInfo, totalBet, commission, odd);
                                                        } else {
                                                            callback(5003, memberInfo, totalBet, commission, odd);
                                                        }
                                                    } else {
                                                        callback(null, memberInfo, totalBet, commission, odd);
                                                    }
                                                });
                                            }
                                        });
                                    }
                                }

                                if(source.toUpperCase() === 'BASKETBALL'){
                                    Commons.cal_bp_hdp_single_match_for_basket_ball(req.body.matchId, req.body.oddType, req.body.oddKey.toLowerCase(), memberInfo.commissionSetting.sportsBook.typeHdpOuOe, oddAdjust, req.userInfo._id, function (err, response) {
                                        if (err)
                                            callback(err, null);
                                        else {

                                            console.log('----- :: ',response)

                                            checkOddProcess(response, req.body.priceType, req.body.oddType, req.body.oddKey, req.body.odd, req.body.handicap, function (isOddNotMatch, key, oddValue, handicapValue, bet) {

                                                let odd = {
                                                    leagueName: response.league_name,
                                                    leagueNameN: response.league_name_n,
                                                    matchId: response.matchId,
                                                    matchName: response.name,
                                                    matchDate: response.date,
                                                    oddType: req.body.oddType,
                                                    odd: oddValue,
                                                    key: key,
                                                    oddKey: req.body.oddKey,
                                                    handicap: handicapValue,
                                                    satang: response.s,
                                                    max: response.max,
                                                    bet: bet,
                                                    maxPerMatch: response.maxPerMatch
                                                };

                                                if (isOddNotMatch) {
                                                    if (req.body.acceptAnyOdd) {
                                                        callback(null, memberInfo, totalBet, commission, odd);
                                                    } else {
                                                        callback(5003, memberInfo, totalBet, commission, odd);
                                                    }
                                                } else {
                                                    callback(null, memberInfo, totalBet, commission, odd);
                                                }
                                            });
                                        }
                                    });
                                }




                            }], (err, memberInfo, totalBet, agentGroups, hdpInfo) => {


                                if (err) {
                                    if (err == 5001) {
                                        return res.send({
                                            message: "amount exceeded (per bet)",
                                            result: null,
                                            code: 5001
                                        });
                                    }
                                    else if (err == 5002) {
                                        return res.send({
                                            message: "Your total bet has exceeded the maximum bet per match.",
                                            result: null,
                                            code: 5002
                                        });
                                    }
                                    else if (err == 5003) {
                                        return res.send({
                                            message: "odd was already changed.",
                                            result: hdpInfo,
                                            code: 5003
                                        });
                                    }
                                    else if (err == 5006) {
                                        return res.send({
                                            message: "score was already changed.",
                                            result: hdpInfo,
                                            code: 5006
                                        });
                                    }
                                    return res.send({message: "error", result: err, code: 999});
                                }


                                if ((totalBet + Number.parseFloat(req.body.amount)) > convertMinMaxByCurrency(hdpInfo.maxPerMatch, currency, membercurrency)) {
                                    return res.send({
                                        message: "Your total bet has exceeded the maximum bet per match.",
                                        result: null,
                                        code: 5002
                                    });
                                }

                                let memberCredit = roundTo(hdpInfo.odd < 0 ? req.body.amount * hdpInfo.odd : (req.body.amount * -1), 2);
                                let processDate = DateUtils.getCurrentDate();

                                let body = {
                                    betId: generateBetId(),
                                    memberId: req.body.memberId,
                                    memberParentGroup: memberInfo.group,
                                    hdp: {
                                        leagueId: req.body.matchId.split(':')[0],
                                        matchId: req.body.matchId,
                                        leagueName: hdpInfo.leagueName,
                                        leagueNameN: hdpInfo.leagueNameN,
                                        matchName: hdpInfo.matchName,
                                        matchDate: hdpInfo.matchDate,
                                        oddKey: hdpInfo.oddKey,
                                        odd: hdpInfo.odd,
                                        oddType: req.body.oddType,
                                        handicap: hdpInfo.handicap,
                                        bet: hdpInfo.bet,
                                        score: req.body.homeScore + ':' + req.body.awayScore,
                                        matchType: req.body.matchType
                                    },
                                    amount: req.body.amount,
                                    memberCredit: memberCredit,
                                    payout: req.body.payout,
                                    gameType: 'TODAY',
                                    acceptHigherPrice: req.body.acceptHigherPrice,
                                    priceType: req.body.priceType.toUpperCase(),
                                    game: 'FOOTBALL',
                                    source: source.toUpperCase(),
                                    status: (req.body.matchType === 'LIVE' && !hdpInfo.isHalfTime && !_.isEqual(hdpInfo.detail.lt, 'Live') &&
                                    // !_.isEqual(hdpInfo.bet, 'UNDER') &&
                                    !_.isEqual(req.body.oddType, 'X12') && !_.isEqual(req.body.oddType, 'X121ST')) ? 'WAITING' : 'RUNNING',
                                    commission: {},
                                    gameDate: hdpInfo.matchDate,
                                    createdDate: processDate,
                                    ipAddress: req.body.ip,
                                    currency: memberInfo.currency
                                };

                                async.series([callback => {
                                    // if (req.body.matchType === 'LIVE' && !hdpInfo.isHalfTime && !_.isEqual(hdpInfo.detail.lt, 'Live') &&
                                    //     // !_.isEqual(body.hdp.bet, 'UNDER') &&
                                    //     !_.isEqual(body.hdp.oddType, 'X12') && !_.isEqual(body.hdp.oddType, 'X121ST')) {
                                    //     //TODO : callback hell - will be refactoring later
                                    //     //Sportbook must be preparing on MASTER
                                    //
                                    //     Commons.call_master_on_ticket(body.betId, body.hdp, (err, data) => {
                                    //         if (err) {
                                    //             callback(err, null);
                                    //         } else {
                                    //             callback(null, data);
                                    //         }
                                    //     });
                                    //
                                    // } else {
                                        callback(null, 'success')
                                    // }

                                }], (err, asyncResponse) => {

                                    if (err) {
                                        return res.send({
                                            message: "error",
                                            result: err,
                                            code: 999
                                        });
                                    }


                                    prepareCommission(memberInfo, body, agentGroups, null, 0, 0);


                                    if (req.body.matchType === 'LIVE' && !hdpInfo.isHalfTime && !_.isEqual(hdpInfo.detail.lt, 'Live') &&
                                        !_.isEqual(body.hdp.oddType, 'X12') && !_.isEqual(body.hdp.oddType, 'X121ST')) {
                                        Commons.call_master_on_ticket(body.betId, body.hdp, (err, data) => {
                                            if (err) {
                                                // callback(err, null);
                                            } else {
                                                // callback(null, data);
                                            }
                                        });
                                    }

                                    async.waterfall([callback => {
                                        console.log('============== 1 : SAVE TRANSACTION ============');
                                        if (userInfo.group.type === 'API') {

                                            body.username = memberInfo.username;
                                            body.gameType = 'TODAY';
                                            body.sportType = source.toUpperCase() === 'FOOTBALL' ? 'SOCCER' : source.toUpperCase() === 'BASKETBALL' ? 'BASKETBALL' : '';

                                            AgentGroupModel.findById(memberInfo.group, 'endpoint', function (err, agentResponse) {

                                                ApiService.placeBet(agentResponse.endpoint, body, (err, apiResponse) => {

                                                    if (err) {
                                                        if (err.code) {
                                                            callback(err.code, null);
                                                        } else {
                                                            callback(999, null);
                                                        }
                                                    } else {
                                                        let model = new BetTransactionModel(body);
                                                        model.save(function (err, betResponse) {

                                                            if (err) {
                                                                callback(999, null);
                                                            } else {
                                                                let betResponse1 = Object.assign({},betResponse)._doc;

                                                                betResponse1.playerBalance = apiResponse.balance;
                                                                callback(null, betResponse1);
                                                            }
                                                        });
                                                    }

                                                });
                                            });
                                        } else {


                                            let model = new BetTransactionModel(body);
                                            model.save(function (err, betResponse) {
                                                if (err) {
                                                    callback(999, null);
                                                } else {

                                                    callback(null, betResponse);
                                                }
                                            });
                                        }


                                    }, (betResponse, callback) => {

                                        console.log('============== 2 : ODD ADJUST ============');

                                        let maxBetPrice = _.min([findMaxBet(memberInfo, req.body.oddType), hdpInfo.max]);

                                        let oddAdjBody = {
                                            username: memberInfo.username,
                                            matchId: req.body.matchId,
                                            prefix: req.body.oddType.toLowerCase(),
                                            key: hdpInfo.key,
                                            value: hdpInfo.oddKey,
                                            maxBetPrice: maxBetPrice,
                                            amount: req.body.amount
                                        };

                                        MemberService.createOddAdjustment(oddAdjBody, (err, response) => {
                                            if (err)
                                                callback(5007, null);
                                            else
                                                callback(null, betResponse, response);
                                        });

                                    }, (betResponse, addJust, callback) => {

                                        if (userInfo.group.type === 'API') {
                                            callback(null, betResponse, addJust, {});
                                        } else {
                                            MemberService.updateBalance(userInfo._id, memberCredit, betResponse.betId, 'SPORTS_BOOK_BET', '',function (err, response) {
                                                if (err) {
                                                    callback(5008, null);
                                                } else {
                                                    callback(null, betResponse, addJust, response);
                                                }
                                            });
                                        }

                                    }, (betResponse, addJust, balance, callback) => {

                                        console.log('============== 3 : ROBOT ============');
                                        let body = {
                                            betId: betResponse.betId,
                                            memberId: betResponse.memberId,
                                            username: memberInfo.username,
                                            leagueId: betResponse.hdp.leagueId,
                                            matchId: betResponse.hdp.matchId,
                                            odd: betResponse.hdp.odd,
                                            oddKey: betResponse.hdp.oddKey,
                                            oddType: betResponse.hdp.oddType,
                                            handicap: betResponse.hdp.handicap,
                                            matchType: betResponse.hdp.matchType,
                                            priceType: betResponse.priceType,
                                            typeHdpOuOe: memberInfo.commissionSetting.sportsBook.typeHdpOuOe,
                                            isAutoUpdateOdd: memberInfo.isAutoChangeOdd,
                                            remark: '',
                                            createdDate: processDate
                                        };

                                        let model = new RobotTransactionModel(body);
                                        model.save(function (err, robotResponse) {
                                            callback(null, betResponse, addJust, balance, robotResponse);
                                        });
                                    }], function (err, betResponse, addJust, balance, robotResponse) {

                                        if (err) {

                                            if (err == 5007) {
                                                return res.send({
                                                    message: "createOddAdjustment failed.",
                                                    result: hdpInfo,
                                                    code: 5007
                                                });
                                            } else if (err == 5008) {
                                                return res.send({
                                                    message: "update credit failed",
                                                    result: hdpInfo,
                                                    code: 5008
                                                });
                                            } else if (err == 1003) {
                                                return res.send({
                                                    message: "Insufficient Balance",
                                                    code: 1003
                                                });
                                            } else {
                                                return res.send({
                                                    message: "Internal Server Error",
                                                    result: err,
                                                    code: 999
                                                });
                                            }
                                        } else {


                                            MemberService.getCredit(userInfo._id, function (err, credit) {
                                                if (err) {
                                                    return res.send({
                                                        message: "get credit fail",
                                                        result: err,
                                                        code: 999
                                                    });
                                                } else {

                                                    return res.send({
                                                        code: 0,
                                                        message: "success",
                                                        result: {
                                                            betId: betResponse.betId,
                                                            hdpInfo: hdpInfo,
                                                            creditLimit: userInfo.group.type === 'API' ? betResponse.playerBalance : credit
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                });
                            });


                        });
                    }
                }
            );
        });
});

function findMaxBet(memberInfo, oddType) {

    switch (oddType.toUpperCase()) {
        case 'AH':
        case 'AH1ST':
        case 'OU':
        case 'OU1ST':
        case 'OE':
        case 'OE1ST':
        case 'T1OU':
        case 'T2OU':
            return memberInfo.limitSetting.sportsBook.hdpOuOe.maxPerBet;
        case 'X12':
        case 'X121ST':
            return memberInfo.limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet;
    }
}


const addParlaySchema = Joi.object().keys({
    memberId: Joi.string().required(),
    matches: Joi.array().items({
        matchId: Joi.string().required(),
        odd: Joi.number().required(),
        oddKey: Joi.string().required(),
        oddType: Joi.string().required(),
        bet: Joi.string().allow(''),
        handicap: Joi.string().allow(''),
        homeScore: Joi.number().required(),
        awayScore: Joi.number().required(),
        matchType: Joi.string().required(),
    }),
    odds: Joi.number().required(),
    amount: Joi.number().required(),
    payout: Joi.number().required(),
    source: Joi.string().required(),
    ip: Joi.string().required()
});


router.post('/parlay', function (req, res) {

    let validate = Joi.validate(req.body, addParlaySchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }


    const userInfo = req.userInfo;
    const source = req.body.source;

    let oddAdjust;


    MemberService.getOddAdjustment(req.userInfo.username, (err, data) => {
        if (err) {
            console.log('Get Odd Adjest error');
        } else {
            oddAdjust = data;
        }
    });


    async.series([callback => {

        MemberModel.findById(req.body.memberId, function (err, response) {
            if (err)
                callback(err, null);

            if (response.suspend) {
                callback(5011, null);
            } else {
                AgentService.getUpLineStatus(userInfo.group._id, function (err, status) {

                    if (err) {
                        callback(err, null);
                    } else {
                        if (status.suspend) {
                            callback(5011, null);
                        } else {
                            callback(null, status);
                        }
                    }
                });
            }
        });

    }, callback => {

        if (userInfo.group.type === 'API') {
            callback(null, 0);
        } else {
            MemberService.getCredit(userInfo._id, function (err, credit) {
                if (err) {
                    callback(err, null);
                } else {
                    if (credit < req.body.amount) {
                        callback(5005, null);
                    } else {
                        callback(null, credit);
                    }
                }
            });
        }

    }, callback => {

        calculateParlayPayout(req.body.amount, req.body.odds, function (err, payout) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, payout);

            }
        });

    }], function (err, results) {

        if (err) {
            if (err == 5004) {
                return res.send({message: "Payout not matched.", result: null, code: 5004});
            }
            if (err == 5005) {
                return res.send({message: "Credit exceed.", result: null, code: 5005});
            }
            if (err == 5011) {
                return res.send({
                    message: "Account or Upline was suspend.",
                    result: null,
                    code: 5011
                });
            }
            if (err == 5012) {
                return res.send({message: "Over maximum bet amount.", result: null, code: 5012});
            }
            return res.send({message: "error", result: err, code: 999});
        }

        async.waterfall([callback => {

            MemberModel.findById(req.body.memberId, function (err, response) {
                if (err) {
                    callback(err, null);
                } else {

                    let maxBet = response.limitSetting.sportsBook.mixParlay.maxPerBet;
                    if (req.body.amount > maxBet) {
                        callback(5001, null);
                    } else {
                        callback(null, response);
                    }
                }
            });

        }, (memberInfo, callback) => {

            BetTransactionModel.aggregate([
                {
                    $match: {
                        memberId: mongoose.Types.ObjectId(req.body.memberId),
                        'hdp.matchId': req.body.matchId
                    }
                },
                {
                    $project: {_id: 0}
                },
                {
                    "$group": {
                        "_id": {
                            id: '$_id'
                        },
                        "amount": {$sum: '$amount'}
                    }
                }
            ], function (err, results) {

                if (err) {
                    callback(err, '');
                } else {
                    callback(null, memberInfo, results[0]);
                }
            });

        }, (memberInfo, totalBet, callback) => {

            let oddMatch = [];

            async.each(req.body.matches, (match, callback) => {

                let odd = _.filter(oddAdjust, (o) => {
                    return o.matchId === match.id;
                });

                if(source.toUpperCase() === 'BASKETBALL'){
                    Commons.cal_bp_parlay_for_basket_ball(match.matchId, match.oddType, match.oddKey.toLowerCase(), memberInfo.commissionSetting.sportsBook.typeHdpOuOe, odd, function (err, response) {
                        if (err)
                            callback(err, null);
                        else {

                            console.log('----- :: ',response)

                            checkOddProcess(response, 'MY', match.oddType.toUpperCase(), match.oddKey.toLowerCase(), match.odd, match.handicap, function (isOddNotMatch, key, oddValue, handicapValue, bet) {

                                let odd = {
                                    leagueName: response.league_name,
                                    leagueNameN: response.league_name_n,
                                    matchId: response.matchId,
                                    matchName: response.name,
                                    matchDate: response.date,
                                    oddType: match.oddType,
                                    odd: oddValue,
                                    key: key,
                                    oddKey: match.oddKey,
                                    handicap: handicapValue,
                                    score: '0:0',
                                    matchType: match.matchType,
                                    isOddNotMatch: isOddNotMatch,
                                    bet: bet
                                };
                                oddMatch.push(odd);
                                callback();
                            });
                        }
                    });
                }else {

                    Commons.cal_bp_parlay(match.matchId, match.oddType, match.oddKey.toLowerCase(), memberInfo.commissionSetting.sportsBook.typeHdpOuOe, odd, function (err, response) {

                        if (err)
                            callback(err, null);
                        else {

                            checkOddProcess(response, 'MY', match.oddType.toUpperCase(), match.oddKey, match.odd, match.handicap, function (isOddNotMatch, key, oddValue, handicapValue, bet) {


                                let odd = {
                                    leagueName: response.league_name,
                                    leagueNameN: response.league_name_n,
                                    matchId: response.matchId,
                                    matchName: response.name,
                                    matchDate: response.date,
                                    oddType: match.oddType,
                                    odd: oddValue,
                                    key: key,
                                    oddKey: match.oddKey,
                                    handicap: handicapValue,
                                    score: '0:0',
                                    matchType: match.matchType,
                                    isOddNotMatch: isOddNotMatch,
                                    bet: bet
                                };
                                oddMatch.push(odd);
                                callback();
                            });
                        }
                    });
                    // }
                }


            }, (err) => {

                if (oddMatch.length != req.body.matches.length) {
                    callback(9999, memberInfo, totalBet, oddMatch);
                    return;
                }
                let oddNotMatchLength = _.filter(oddMatch, function (item) {
                    return item.isOddNotMatch;
                });

                if (oddNotMatchLength.length > 0) {
                    callback(5003, memberInfo, totalBet, oddMatch);
                } else {
                    callback(null, memberInfo, totalBet, oddMatch);
                }
            });//end forEach Match


        }, (memberInfo, totalBet, matches, callback) => {

            AgentGroupModel.findById(userInfo.group._id).select('_id type parentId shareSetting commissionSetting').populate({
                path: 'parentId',
                select: '_id type parentId shareSetting commissionSetting name',
                populate: {
                    path: 'parentId',
                    select: '_id type parentId shareSetting commissionSetting name',
                    populate: {
                        path: 'parentId',
                        select: '_id type parentId shareSetting commissionSetting name',
                        populate: {
                            path: 'parentId',
                            select: '_id type parentId shareSetting commissionSetting name',
                            populate: {
                                path: 'parentId',
                                select: '_id type parentId shareSetting commissionSetting name',
                                populate: {
                                    path: 'parentId',
                                    select: '_id type parentId shareSetting commissionSetting name',
                                }
                            }
                        }
                    }
                }
            }).exec(function (err, response) {
                if (err)
                    callback(err, null);
                else
                    callback(null, memberInfo, totalBet, matches, response);
            });


        }], (err, memberInfo, totalBet, matches, agentGroups) => {

            if (err) {
                if (err == 5001) {
                    return res.send({
                        message: "amount exceeded (per bet)",
                        result: null,
                        code: 5001
                    });
                }
                else if (err == 5002) {
                    return res.send({
                        message: "amount exceeded (per match)",
                        result: null,
                        code: 5001
                    });
                }
                else if (err == 5003) {
                    return res.send({
                        message: "odd was already changed.",
                        result: matches,
                        code: 5003
                    });
                }
                else if (err == 9999) {
                    return res.send({
                        message: "err oddMatch.length != req.body.matches.length",
                        result: matches,
                        code: 9999
                    });
                }
                return res.send({message: "error", result: err, code: 999});
            }


            let matchTransform = [];
            _.each(matches, matchInfo => {
                matchTransform.push({
                    leagueId: matchInfo.matchId.split(':')[0],
                    matchId: matchInfo.matchId,
                    leagueName: matchInfo.leagueName,
                    leagueNameN: matchInfo.leagueNameN,
                    matchName: matchInfo.matchName,
                    matchDate: matchInfo.matchDate,
                    oddKey: matchInfo.oddKey,
                    odd: matchInfo.odd,
                    oddType: matchInfo.oddType.toUpperCase(),
                    handicap: matchInfo.handicap,
                    bet: matchInfo.bet,
                    score: matchInfo.score,
                    matchType: 'NONE_LIVE',
                });

            });

            let body = {
                betId: generateBetId(),
                memberId: req.body.memberId,
                memberParentGroup: memberInfo.group,
                parlay: {
                    matches: matchTransform,
                    odds: req.body.odds
                },
                amount: req.body.amount,
                memberCredit: (req.body.amount * -1),
                payout: req.body.payout,
                gameType: 'PARLAY',
                acceptHigherPrice: false,
                priceType: 'EU',
                game: 'FOOTBALL',
                source: source.toUpperCase(),
                status: 'RUNNING',
                commission: {},
                gameDate: matchTransform[0].matchDate,
                createdDate: DateUtils.getCurrentDate(),
                ipAddress: req.body.ip,
                currency: memberInfo.currency
            };

            prepareCommission(memberInfo, body, agentGroups, null, 0, 0);

            let model = new BetTransactionModel(body);
            model.save(function (err, betResponse) {

                if (err) {
                    return res.send({message: "error", result: err, code: 999});
                }

                if (userInfo.group.type === 'API') {

                    let body = Object.assign({}, betResponse)._doc;
                    body.username = memberInfo.username;
                    body.gameType = 'PARLAY';
                    body.sportType = 'SOCCER';

                    AgentGroupModel.findById(memberInfo.group, 'endpoint', function (err, agentResponse) {

                        ApiService.placeBet(agentResponse.endpoint, body, (err, response) => {
                            if (err) {
                                return res.send({
                                    code: 999,
                                    message: "error",
                                    result: err
                                });
                            } else {

                                return res.send({
                                    code: 0,
                                    message: "success",
                                    result: {
                                        betId: betResponse.betId,
                                        parlayInfo: betResponse.parlay
                                    }
                                });
                            }

                        });
                    });
                } else {
                    async.waterfall([callback => {

                        MemberService.updateBalance(userInfo._id, (req.body.amount * -1), betResponse.betId, 'SPORTS_BOOK_BET','', function (err, response) {
                            if (err) {
                                callback(5008, null);
                            } else {
                                callback(null, response);
                            }
                        });

                    }, (updateBalanceResponse, callback) => {

                        MemberService.getCredit(userInfo._id, function (err, credit) {
                            if (err) {
                                callback(999, null);
                            } else {
                                callback(null, updateBalanceResponse, credit);
                            }
                        });

                    }], function (err, balanceResponse, credit) {

                        if (err) {
                            return res.send({message: "error", result: err, code: 999});
                        }
                        return res.send(
                            {
                                code: 0,
                                message: "success",
                                result: {
                                    betId: betResponse.betId,
                                    parlayInfo: betResponse.parlay,
                                    creditLimit: credit
                                }
                            }
                        );

                    });
                }
            });

        });

    });


});


function checkOddProcess(bpResponse, priceType, oddType, oddKey, odd, handicap, callback) {


    console.log('----- :::::: ',oddType)
    console.log('----- :::::: ',oddKey)

    let handicapPrefix, oddPrefix;
    let type = oddType.toLowerCase();
    let isOddNotMatch = false;
    let oddValue, handicapValue;
    let key = '';
    let bet = '';
    if (oddType === 'AH' || oddType === 'AH1ST') {

        _Utils.findKeyByValue(bpResponse[type], oddKey, function (keyResponse) {
            key = keyResponse;
        });

        if (key === 'hpk') {
            handicapPrefix = 'h';
            oddPrefix = 'hp';
            bet = 'HOME';
        } else {
            handicapPrefix = 'a';
            oddPrefix = 'ap';
            bet = 'AWAY';
        }

        if (!_.isUndefined(bpResponse[type])) {
            console.log(bpResponse[type][oddPrefix] + '  1:  ' + odd)

            oddValue = bpResponse[type][oddPrefix];
            oddValue = convertOdd(priceType, oddValue, oddType);
            handicapValue = bpResponse[type][handicapPrefix];
            if (oddValue !== odd) {
                isOddNotMatch = true;
            }
            if (handicapValue !== handicap) {
                isOddNotMatch = true;
            }
        } else {
            isOddNotMatch = true;
        }

    } else if (oddType === 'OU' || oddType === 'OU1ST' || oddType === 'T1OU' || oddType === 'T2OU') {

        _Utils.findKeyByValue(bpResponse[type], oddKey, function (keyResponse) {
            key = keyResponse;
        });


        if (key === 'opk') {
            handicapPrefix = 'o';
            oddPrefix = 'op';
            bet = 'OVER';
        } else {
            handicapPrefix = 'u';
            oddPrefix = 'up';
            bet = 'UNDER';
        }

        if (!_.isUndefined(bpResponse[type])) {
            oddValue = bpResponse[type][oddPrefix];
            oddValue = convertOdd(priceType, oddValue, oddType);
            handicapValue = bpResponse[type][handicapPrefix];
            if (oddValue !== odd) {
                isOddNotMatch = true;
            }
            if (handicapValue !== handicap) {
                isOddNotMatch = true;
            }
        } else {
            isOddNotMatch = true;
        }

    } else if (oddType === 'X12' || oddType === 'X121ST' || oddType === 'ML') {

        _Utils.findKeyByValue(bpResponse[type], oddKey, function (keyResponse) {
            key = keyResponse;
        });

        if (key === 'hk') {
            oddPrefix = 'h';
            bet = 'HOME';
        } else if (key === 'ak') {
            oddPrefix = 'a';
            bet = 'AWAY';
        } else if (key === 'dk') {
            oddPrefix = 'd';
            bet = 'DRAW';
        }

        if (!_.isUndefined(bpResponse[type])) {
            oddValue = bpResponse[type][oddPrefix];
            oddValue = convertOdd(priceType, oddValue, oddType);
            if (oddValue !== odd) {
                isOddNotMatch = true;
            }
        } else {
            isOddNotMatch = true;
        }

    } else if (oddType === 'OE' || oddType === 'OE1ST') {

        _Utils.findKeyByValue(bpResponse[type], oddKey, function (keyResponse) {
            key = keyResponse;
        });

        if (key === 'ok') {
            oddPrefix = 'o';
            bet = 'ODD';
        } else if (key === 'ek') {
            oddPrefix = 'e';
            bet = 'EVEN';
        }

        if (!_.isUndefined(bpResponse[type])) {
            oddValue = bpResponse[type][oddPrefix];
            oddValue = convertOdd(priceType, oddValue, oddType);
            if (oddValue !== odd) {
                isOddNotMatch = true;
            }
        } else {
            isOddNotMatch = true;
        }
    } else {
        console.log('oddType not match')
    }
    console.log('input_odd : ', odd + '  , oddValue : ' + oddValue);
    console.log('input_handicap : ', handicap + '  , handicapValue : ' + handicapValue);
    console.log('isOddNotMatch : ', isOddNotMatch)
    callback(isOddNotMatch, key, oddValue, handicapValue, bet);

}


function prepareCommission(memberInfo, body, currentAgent, overAgent, remaining, overAllShare) {


    if (currentAgent && (currentAgent.type === 'SUPER_ADMIN' || currentAgent.parentId)) {


        console.log('==============================================');
        console.log('process : ' + currentAgent.type + ' : ' + currentAgent.name);


        let currentAgentShareSettingParent;
        let currentAgentShareSettingOwn;
        let currentAgentShareSettingRemaining;
        let currentAgentShareSettingMin;

        let overAgentShareSettingParent;
        let overAgentShareSettingOwn;
        let overAgentShareSettingRemaining;
        let overAgentShareSettingMin;

        let agentCommission;


        if (body.gameType === 'PARLAY') {
            currentAgentShareSettingParent = currentAgent.shareSetting.sportsBook.others.parent;
            currentAgentShareSettingOwn = currentAgent.shareSetting.sportsBook.others.own;
            currentAgentShareSettingRemaining = currentAgent.shareSetting.sportsBook.others.remaining;
            currentAgentShareSettingMin = currentAgent.shareSetting.sportsBook.others.min;

            if (overAgent) {
                overAgentShareSettingParent = overAgent.shareSetting.sportsBook.others.parent;
                overAgentShareSettingOwn = overAgent.shareSetting.sportsBook.others.own;
                overAgentShareSettingRemaining = overAgent.shareSetting.sportsBook.others.remaining;
                overAgentShareSettingMin = overAgent.shareSetting.sportsBook.others.min;
            } else {
                overAgent = {};
                overAgent.type = 'member';
                overAgentShareSettingParent = memberInfo.shareSetting.sportsBook.others.parent;
                overAgentShareSettingOwn = 0;
                overAgentShareSettingRemaining = 0;

                body.commission.member = {};
                body.commission.member.parent = memberInfo.shareSetting.sportsBook.others.parent;
                body.commission.member.commission = memberInfo.commissionSetting.sportsBook.others;
            }

            agentCommission = currentAgent.commissionSetting.sportsBook.others;

        } else {
            if (['AH', 'OU', 'OE', 'AH1ST', 'OU1ST', 'OE1ST','T1OU','T2OU'].includes(body.hdp.oddType.toUpperCase())) {
                currentAgentShareSettingParent = currentAgent.shareSetting.sportsBook.hdpOuOe.parent;
                currentAgentShareSettingOwn = currentAgent.shareSetting.sportsBook.hdpOuOe.own;
                currentAgentShareSettingRemaining = currentAgent.shareSetting.sportsBook.hdpOuOe.remaining;
                currentAgentShareSettingMin = currentAgent.shareSetting.sportsBook.hdpOuOe.min;

                if (overAgent) {
                    overAgentShareSettingParent = overAgent.shareSetting.sportsBook.hdpOuOe.parent;
                    overAgentShareSettingOwn = overAgent.shareSetting.sportsBook.hdpOuOe.own;
                    overAgentShareSettingRemaining = overAgent.shareSetting.sportsBook.hdpOuOe.remaining;
                    overAgentShareSettingMin = overAgent.shareSetting.sportsBook.hdpOuOe.min;
                } else {
                    overAgent = {};
                    overAgent.type = 'member';
                    overAgentShareSettingParent = memberInfo.shareSetting.sportsBook.hdpOuOe.parent;
                    overAgentShareSettingOwn = 0;
                    overAgentShareSettingRemaining = 0;

                    body.commission.member = {};
                    body.commission.member.parent = memberInfo.shareSetting.sportsBook.hdpOuOe.parent;
                    body.commission.member.commission = memberInfo.commissionSetting.sportsBook.hdpOuOe;
                }

                if (memberInfo.commissionSetting.sportsBook.typeHdpOuOe === 'A') {
                    agentCommission = currentAgent.commissionSetting.sportsBook.hdpOuOeA;
                }
                if (memberInfo.commissionSetting.sportsBook.typeHdpOuOe === 'B') {
                    agentCommission = currentAgent.commissionSetting.sportsBook.hdpOuOeB;
                }
                if (memberInfo.commissionSetting.sportsBook.typeHdpOuOe === 'C') {
                    agentCommission = currentAgent.commissionSetting.sportsBook.hdpOuOeC;
                }
                if (memberInfo.commissionSetting.sportsBook.typeHdpOuOe === 'D') {
                    agentCommission = currentAgent.commissionSetting.sportsBook.hdpOuOeD;
                }
                if (memberInfo.commissionSetting.sportsBook.typeHdpOuOe === 'E') {
                    agentCommission = currentAgent.commissionSetting.sportsBook.hdpOuOeE;
                }
                if (memberInfo.commissionSetting.sportsBook.typeHdpOuOe === 'F') {
                    agentCommission = currentAgent.commissionSetting.sportsBook.hdpOuOeF;
                }

            } else if (['X12', 'X121ST','ML'].includes(body.hdp.oddType.toUpperCase())) {
                currentAgentShareSettingParent = currentAgent.shareSetting.sportsBook.oneTwoDoubleChance.parent;
                currentAgentShareSettingOwn = currentAgent.shareSetting.sportsBook.oneTwoDoubleChance.own;
                currentAgentShareSettingRemaining = currentAgent.shareSetting.sportsBook.oneTwoDoubleChance.remaining;
                currentAgentShareSettingMin = currentAgent.shareSetting.sportsBook.oneTwoDoubleChance.min;

                if (overAgent) {
                    overAgentShareSettingParent = overAgent.shareSetting.sportsBook.oneTwoDoubleChance.parent;
                    overAgentShareSettingOwn = overAgent.shareSetting.sportsBook.oneTwoDoubleChance.own;
                    overAgentShareSettingRemaining = overAgent.shareSetting.sportsBook.oneTwoDoubleChance.remaining;
                    overAgentShareSettingMin = overAgent.shareSetting.sportsBook.oneTwoDoubleChance.min;
                } else {
                    overAgent = {};
                    overAgent.type = 'member';
                    overAgentShareSettingParent = memberInfo.shareSetting.sportsBook.oneTwoDoubleChance.parent;
                    overAgentShareSettingOwn = 0;
                    overAgentShareSettingRemaining = 0;

                    body.commission.member = {};
                    body.commission.member.parent = memberInfo.shareSetting.sportsBook.oneTwoDoubleChance.parent;
                    body.commission.member.commission = memberInfo.commissionSetting.sportsBook.oneTwoDoubleChance;
                }

                agentCommission = currentAgent.commissionSetting.sportsBook.oneTwoDoubleChance;

            }
        }

        let money = body.amount;

        console.log('');
        console.log('---- setting ----');
        console.log('ส่วนแบ่งที่ได้มามีทั้งหมด : ' + currentAgentShareSettingOwn + ' %');
        console.log('ขอส่วนแบ่งจาก : ' + overAgent.type + ' ' + overAgentShareSettingParent + ' %');
        console.log('ให้ส่วนแบ่ง : ' + overAgent.type + ' ที่เหลือไป ' + overAgentShareSettingOwn + ' %');
        console.log('remaining จาก : ' + overAgent.type + ' : ' + overAgentShareSettingRemaining + ' %');
        console.log('โดนบังคับเสียขั้นต่ำ : ' + currentAgentShareSettingMin + ' %');
        console.log('---- end setting ----');
        console.log('');


        // console.log('และได้รับ remaining ไว้ : '+overAgent.shareSetting.sportsBook.hdpOuOe.remaining+' %');

        let currentPercentReceive = overAgentShareSettingParent;
        console.log('% ที่ได้ : ' + currentPercentReceive);
        console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');
        let totalRemaining = remaining;

        if (overAgentShareSettingRemaining > 0) {
            // console.log()
            // console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');

            if (overAgentShareSettingRemaining > totalRemaining) {
                console.log('รับ remaining ทั้งหมดไว้: ' + totalRemaining + ' %');
                currentPercentReceive += totalRemaining;
                totalRemaining = 0;
            } else {
                totalRemaining = remaining - overAgentShareSettingRemaining;
                console.log('หักค่า remaining ที่ได้รับมากับที่เซตรับไว้ = ' + remaining + ' - ' + overAgentShareSettingRemaining + ' = ' + totalRemaining);
                currentPercentReceive += overAgentShareSettingRemaining;
            }

        }

        console.log('รวม % ที่ได้รับ : ' + currentPercentReceive);


        let unUseShare = currentAgentShareSettingOwn - (overAgentShareSettingParent + overAgentShareSettingOwn);

        console.log('เหลือส่วนแบ่งที่ไม่ได้ใช้ : ' + unUseShare + ' %');
        totalRemaining = (unUseShare + totalRemaining);
        console.log('ส่วนแบ่งที่ไม่ได้ใช้ บวกกับค่า remaining ท่ี่เหลือจะเป็น : ' + totalRemaining + ' %');

        console.log('จากที่โดนบังคับเสียขั้นต่ำ : ' + currentAgentShareSettingMin + ' %');
        console.log('overAllShare : ', overAllShare)
        if (currentAgentShareSettingMin > 0 && ((overAllShare + currentPercentReceive) < currentAgentShareSettingMin)) {

            if (currentPercentReceive < currentAgentShareSettingMin) {
                console.log('% ที่ได้รับ < ขั้นต่ำที่ถูกตั้ง min ไว้');
                let percent = currentAgentShareSettingMin - (currentPercentReceive + overAllShare);
                console.log('หักออกไป ' + percent + ' % : แล้วไปเพิ่มในส่วนที่ต้องรับผิดชอบ ');
                totalRemaining = totalRemaining - percent;
                if (totalRemaining < 0) {
                    totalRemaining = 0;
                }
                currentPercentReceive += percent;
            }
        } else {
            // currentPercentReceive += totalRemaining;
        }


        if (currentAgent.type === 'SUPER_ADMIN') {
            currentPercentReceive += totalRemaining;
        }
        let getMoney = (money * convertPercent(currentPercentReceive)).toFixed(2);
        overAllShare = overAllShare + currentPercentReceive;

        console.log('ยอดแทง ' + money + ' ได้ทั้งหมด : ' + currentPercentReceive + ' %');

        console.log('รวม % + % remaining  จะเป็นสัดส่วนที่ได้ทั้งหมด = ' + getMoney);
        console.log('ค่าที่จะถูกคืนกลับไป (remaining) : ' + totalRemaining + ' %');

        console.log('จำนวน % ที่ถูกแบ่งไปแล้วทั้งหมด : ', overAllShare);


        console.log('agentCommission : ', agentCommission)

        switch (currentAgent.type) {
            case 'SUPER_ADMIN':
                body.commission.superAdmin = {};
                body.commission.superAdmin.group = currentAgent._id;
                body.commission.superAdmin.parent = currentAgent.shareSetting.casino.sexy.parent;
                body.commission.superAdmin.own = currentAgent.shareSetting.casino.sexy.own;
                body.commission.superAdmin.remaining = currentAgent.shareSetting.casino.sexy.remaining;
                body.commission.superAdmin.min = currentAgent.shareSetting.casino.sexy.min;
                body.commission.superAdmin.shareReceive = Math.abs(currentPercentReceive);
                body.commission.superAdmin.commission = agentCommission;
                body.commission.superAdmin.amount = getMoney;
                break;
            case 'COMPANY':
                body.commission.company = {};
                body.commission.company.parentGroup = currentAgent.parentId;
                body.commission.company.group = currentAgent._id;
                body.commission.company.parent = currentAgentShareSettingParent;
                body.commission.company.own = currentAgentShareSettingOwn;
                body.commission.company.remaining = currentAgentShareSettingRemaining;
                body.commission.company.min = currentAgentShareSettingMin;
                body.commission.company.shareReceive = Math.abs(currentPercentReceive);
                body.commission.company.commission = agentCommission;
                body.commission.company.amount = getMoney;
                break;
            case 'SHARE_HOLDER':
                body.commission.shareHolder = {};
                body.commission.shareHolder.parentGroup = currentAgent.parentId;
                body.commission.shareHolder.group = currentAgent._id;
                body.commission.shareHolder.parent = currentAgentShareSettingParent;
                body.commission.shareHolder.own = currentAgentShareSettingOwn;
                body.commission.shareHolder.remaining = currentAgentShareSettingRemaining;
                body.commission.shareHolder.min = currentAgentShareSettingMin;
                body.commission.shareHolder.shareReceive = currentPercentReceive;
                body.commission.shareHolder.commission = agentCommission;
                body.commission.shareHolder.amount = getMoney;
                break;
            case 'API':
                body.commission.api = {};
                body.commission.api.parentGroup = currentAgent.parentId;
                body.commission.api.group = currentAgent._id;
                body.commission.api.parent = currentAgentShareSettingParent;
                body.commission.api.own = currentAgentShareSettingOwn;
                body.commission.api.remaining = currentAgentShareSettingRemaining;
                body.commission.api.min = currentAgentShareSettingMin;
                body.commission.api.shareReceive = currentPercentReceive;
                body.commission.api.commission = agentCommission;
                body.commission.api.amount = getMoney;
                break;
            case 'SENIOR':
                body.commission.senior = {};
                body.commission.senior.parentGroup = currentAgent.parentId;
                body.commission.senior.group = currentAgent._id;
                body.commission.senior.parent = currentAgentShareSettingParent;
                body.commission.senior.own = currentAgentShareSettingOwn;
                body.commission.senior.remaining = currentAgentShareSettingRemaining;
                body.commission.senior.min = currentAgentShareSettingMin;
                body.commission.senior.shareReceive = currentPercentReceive;
                body.commission.senior.commission = agentCommission;
                body.commission.senior.amount = getMoney;
                break;
            case 'MASTER_AGENT':
                body.commission.masterAgent = {};
                body.commission.masterAgent.parentGroup = currentAgent.parentId;
                body.commission.masterAgent.group = currentAgent._id;
                body.commission.masterAgent.parent = currentAgentShareSettingParent;
                body.commission.masterAgent.own = currentAgentShareSettingOwn;
                body.commission.masterAgent.remaining = currentAgentShareSettingRemaining;
                body.commission.masterAgent.min = currentAgentShareSettingMin;
                body.commission.masterAgent.shareReceive = currentPercentReceive;
                body.commission.masterAgent.commission = agentCommission;
                body.commission.masterAgent.amount = getMoney;
                break;
            case 'AGENT':
                body.commission.agent = {};
                body.commission.agent.parentGroup = currentAgent.parentId;
                body.commission.agent.group = currentAgent._id;
                body.commission.agent.parent = currentAgentShareSettingParent;
                body.commission.agent.own = currentAgentShareSettingOwn;
                body.commission.agent.remaining = currentAgentShareSettingRemaining;
                body.commission.agent.min = currentAgentShareSettingMin;
                body.commission.agent.shareReceive = currentPercentReceive;
                body.commission.agent.commission = agentCommission;
                body.commission.agent.amount = getMoney;
                break;
        }

        prepareCommission(memberInfo, body, currentAgent.parentId, currentAgent, totalRemaining, overAllShare);
    }
}

function convertPercent(percent) {
    return percent / 100
}


router.get('/ticket_live', function (req, res) {

    BetTransactionModel.find({
        'hdp.matchType': 'LIVE'
    }, (err, datas) => {
        if (err) {
            return res.send({message: "error", result: err, code: 999});
        } else {
            return res.send({message: "success", result: datas, code: 0});
        }
    }).select('betId hdp status')

});

// const updateBetSchema = Joi.object().keys({
//     betId: Joi.string().required(),
//     status: Joi.string().required(),
//     remark: Joi.string().required()
// });
router.put('/update/ticket_live', function (req, res) {
    console.log('================= /update/ticket_live =================');

    // let validate = Joi.validate(req.body, updateBetSchema);
    // if (validate.error) {
    //     return res.send({
    //         message: "validation fail",
    //         results: validate.error.details,
    //         code: 999
    //     });
    // }

    const {
        betId,
        status,
        remark
    } = req.body;
    console.log(`   JSON BODY => ${JSON.stringify(req.body)}`);
    // console.log('   ', betId, ' : ', status, ' : ', remark);

    let body = {};

    if (status.toLowerCase() === 'rejected') {
        body.status = 'REJECTED';
        body.remark = body.status + ' : ' + remark;
    }

    if (status.toLowerCase() === 'running') {
        body.status = 'RUNNING';
        body.remark = body.status + ' : ' + remark;
    }

    console.log('   ', body);

    async.series([callback => {
        BetTransactionModel.findOne({betId: betId, status: 'WAITING'}, function (err, response) {
            if (err) {
                callback(err, null);
            } else {
                if (response) {
                    callback(null, response);
                } else {
                    callback('transaction not found', null);
                }
            }
        });
    }], function (err, asyncResponse) {

        if (err) {
            return res.send({message: "find transaction failed", result: err, code: 999});
        }

        let transaction = asyncResponse[0];

        async.series([callback => {

            if (status.toLowerCase() === 'rejected') {

                MemberService.updateBalance2(transaction.memberUsername, Math.abs(transaction.memberCredit), betId, 'SPORTS_BOOK_REJECT',betId, function (err, creditResponse) {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, 'success');
                    }
                });
            } else {
                callback(null, 'success');
            }

        }], function (err, response) {

            if (err) {
                return res.send({
                    message: "update credit failed",
                    result: err,
                    code: 999
                });
            }

            BetTransactionModel.update({
                betId: betId/*,
                 'hdp.matchType': 'LIVE'*/
            }, body, (err, data) => {
                if (err) {
                    console.log(err);
                    console.log('================= /update/ticket_live =================');
                    return res.send({message: "error", result: err, code: 999});
                } else if (data.nModified === 0) {
                    console.log('update ticket fail');
                    console.log('================= /update/ticket_live =================');
                    return res.send({
                        message: "error",
                        result: 'update ticket fail',
                        code: 998
                    });
                } else {
                    console.log('================= /update/ticket_live =================');
                    return res.send({message: "success", code: 0});
                }
            });
        });
    });
});


function convertOdd(priceType, odd, oddType) {

    if (priceType === 'MY') {
        return odd;
    } else if (priceType === 'HK') {
        if (!odd.includes('-')) {
            return odd;
        }
        odd = odd.replace('-', '');
        odd = parseFloat(odd) * 100;

        let tmp = 100 / odd;
        return tmp.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
    } else if (priceType === 'EU') {
        if (!odd.includes('-')) {
            if (oddType === 'X12' || oddType === 'X121ST') {
                return odd;
            }
            let t = parseFloat(odd) + 1.00;
            if (!t.toString().includes('.')) {
                t = t.toString() + '.00';
                return t;
            } else {

                if (t.toString().length == 3) {
                    t = t.toString() + '0';
                    return t;
                } else {
                    return t.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
                }

            }
        }
        odd = odd.replace('-', '');
        odd = parseFloat(odd) * 100;
        let tmp = (100 / odd) + 1.00;
        return tmp.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];

    } else if (priceType === 'ID') {
        if (oddType === 'X12' || oddType === 'X121ST') {
            return odd;
        }


        let tmp = parseFloat(odd) * -1;
        tmp = 1 / tmp;

        if (!tmp.toString().includes('.')) {
            tmp = tmp.toString() + '.00';
            return tmp;
        } else {

            if (tmp.toString().length == 3) {
                tmp = tmp.toString() + '0';
                return tmp;
            } else {
                return tmp.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
            }

        }
    }
}


const convertMinMaxByCurrency = (value, currency, membercurrency) => {
    // console.log('###########################');
    // console.log('[BF]value      : ', value);
    // console.log('membercurrency : ', membercurrency);

    let minMax = value;
    if (_.isUndefined(membercurrency) || _.isNull(membercurrency)) {
        return minMax;
    } else {
        const predicate = JSON.parse(`{"currencyName":"${membercurrency}"}`);
        const obj = _.chain(currency)
            .findWhere(predicate)
            .pick('currencyTHB')
            .value();
        if (!_.isUndefined(obj.currencyTHB)) {
            // console.log('               : ',obj.currencyTHB);

            if (_.isEqual(membercurrency, "IDR") || _.isEqual(membercurrency, "VND")) {
                minMax = Math.ceil(value * obj.currencyTHB);
            } else {
                minMax = Math.ceil(value / obj.currencyTHB);
            }
            // console.log('[AF]value      : ', minMax);
            // console.log('###########################');
            return minMax;
        } else {
            return minMax;
        }
    }

};


module.exports = router;