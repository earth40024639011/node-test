const express     = require('express');
const router      = express.Router();
const _           = require('underscore');
const Joi         = require('joi');
const httpRequest = require('request');

const URL_FB_END_SCORE_RE_CAL= process.env.URL_FB_END_SCORE_RE_CAL || 'http://104.248.97.132:8001/football/endScore/re-cal';
const URL_BB_END_SCORE_RE_CAL= process.env.URL_BB_END_SCORE_RE_CAL || 'http://104.248.97.132:8001/basketball/endScore/re-cal';
const URL_MT_END_SCORE_RE_CAL= process.env.URL_MT_END_SCORE_RE_CAL || 'http://104.248.97.132:8001/muaythai/endScore/re-cal';
const URL_TN_END_SCORE_RE_CAL= process.env.URL_TN_END_SCORE_RE_CAL || 'http://104.248.97.132:8001/tennis/endScore/re-cal';
const URL_TT_END_SCORE_RE_CAL= process.env.URL_TT_END_SCORE_RE_CAL || 'http://104.248.97.132:8001/tableTennis/endScore/re-cal';
const URL_VB_END_SCORE_RE_CAL= process.env.URL_VB_END_SCORE_RE_CAL || 'http://104.248.97.132:8001/volleyball/endScore/re-cal';

const footballSchema = Joi.object().keys({
    id: Joi.string().required(),
    home: Joi.string().required(),
    away: Joi.string().required()
});
router.put('/football', (req, res) => {
    // let validate = Joi.validate(req.body, footballSchema);
    // if (validate.error) {
    //     return res.send({
    //         message: "validation fail",
    //         result: validate.error.details,
    //         code: 999
    //     });
    // }
    return res.send({
        message: "success",
        code: 0
    });

    // const {
    //     id,
    //     home,
    //     away
    // } = req.body;
    // const type = 'FT';
    //
    // const JSON = {
    //     id,
    //     home,
    //     away,
    //     type
    // };
    // console.log(URL_FB_END_SCORE_RE_CAL);
    // console.log(JSON);
    // httpRequest.post(
    //     URL_FB_END_SCORE_RE_CAL,
    //     {
    //         json: JSON
    //     },
    //     (error, response) => {
    //         if(error){
    //             console.error(error);
    //         } else {
    //             return res.send(
    //                 {
    //                     code: 0,
    //                     message: "success"
    //                 }
    //             );
    //         }
    //     });
});

router.put('/basketball', (req, res) => {
    // let validate = Joi.validate(req.body, footballSchema);
    // if (validate.error) {
    //     return res.send({
    //         message:"validation fail",
    //         result: validate.error.details,
    //         code: 999
    //     });
    // }

    // const {
    //     id,
    //     home,
    //     away
    // } = req.body;
    // const type = 'FT';

    // const JSON = {
    //     id,
    //     home,
    //     away,
    //     type
    // };
    //
    // console.log(URL_BB_END_SCORE_RE_CAL);
    // console.log(JSON);
    // httpRequest.post(
    //     URL_BB_END_SCORE_RE_CAL,
    //     {
    //         json: JSON
    //     },
    //     (error, response) => {
    //         if(error){
    //             console.error(error);
    //         } else {
    //             return res.send(
    //                 {
    //                     code: 0,
    //                     message: "success"
    //                 }
    //             );
    //         }
    //     });
    return res.send({
        message: "success",
        code: 0
    });
});

router.put('/muaythai', (req, res) => {
    let validate = Joi.validate(req.body, footballSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            result: validate.error.details,
            code: 999
        });
    }

    const {
        id,
        home,
        away
    } = req.body;

    const JSON = {
        id,
        home,
        away
    };

    console.log(URL_MT_END_SCORE_RE_CAL);
    console.log(JSON);
    httpRequest.post(
        URL_MT_END_SCORE_RE_CAL,
        {
            json: JSON
        },
        (error, response) => {
            if(error){
                console.error(error);
            } else {
                return res.send(
                    {
                        code: 0,
                        message: "success"
                    }
                );
            }
        });
});

router.put('/tennis', (req, res) => {
    let validate = Joi.validate(req.body, footballSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            result: validate.error.details,
            code: 999
        });
    }

    const {
        id,
        home,
        away
    } = req.body;

    const JSON = {
        id,
        home,
        away
    };

    console.log(URL_TN_END_SCORE_RE_CAL);
    console.log(JSON);
    httpRequest.post(
        URL_TN_END_SCORE_RE_CAL,
        {
            json: JSON
        },
        (error, response) => {
            if(error){
                console.error(error);
            } else {
                return res.send(
                    {
                        code: 0,
                        message: "success"
                    }
                );
            }
        });
});

router.put('/tableTennis', (req, res) => {
    let validate = Joi.validate(req.body, footballSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            result: validate.error.details,
            code: 999
        });
    }

    const {
        id,
        home,
        away
    } = req.body;

    const JSON = {
        id,
        home,
        away
    };

    console.log(URL_TT_END_SCORE_RE_CAL);
    console.log(JSON);
    httpRequest.post(
        URL_TT_END_SCORE_RE_CAL,
        {
            json: JSON
        },
        (error, response) => {
            if(error){
                console.error(error);
            } else {
                return res.send(
                    {
                        code: 0,
                        message: "success"
                    }
                );
            }
        });
});

router.put('/volleyball', (req, res) => {
    let validate = Joi.validate(req.body, footballSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            result: validate.error.details,
            code: 999
        });
    }

    const {
        id,
        home,
        away
    } = req.body;

    const JSON = {
        id,
        home,
        away
    };

    console.log(URL_VB_END_SCORE_RE_CAL);
    console.log(JSON);
    httpRequest.post(
        URL_VB_END_SCORE_RE_CAL,
        {
            json: JSON
        },
        (error, response) => {
            if(error){
                console.error(error);
            } else {
                return res.send(
                    {
                        code: 0,
                        message: "success"
                    }
                );
            }
        });
});
module.exports = router;