const MemberModel = require('../../models/member.model');
const BetTransactionModel = require('../../models/betTransaction.model.js');
const UserModel = require("../../models/users.model");
const AgentGroupModel = require('../../models/agentGroup.model.js');
const MemberService = require('../../common/memberService');
const express = require('express');
const router = express.Router();
const async = require("async");
const Joi = require('joi');
const mongoose = require('mongoose');
const config = require('config');
const _ = require('underscore');
const moment = require('moment');
const DateUtils = require('../../common/dateUtils');
const ReportService = require('../../common/reportService');


const cancelBetSchema = Joi.object().keys({
    betId: Joi.string().required(),
    status: Joi.string().valid('REJECTED', 'CANCELLED'),
    remark: Joi.string().allow('')
});

router.post('/cancel-ticket', function (req, res) {
    console.log('================= cancel-ticket =================');

    let validate = Joi.validate(req.body, cancelBetSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }
    const {
        betId,
        status,
        remark
    } = req.body;

    // console.log('   ', betId, ' : ', status, ' : ', remark)


    async.series([callback => {

        BetTransactionModel.findOne({betId: betId,status:'RUNNING'}, function (err, response) {
            if (err) {
                callback(err, null);
            }
            if (response) {
                if (response.status === 'CANCELLED' || response.status === 'REJECTED') {
                    callback('cancel or reject already', null);
                } else {
                    callback(null, response);
                }
            } else {
                callback('bet Id not found', null);
            }
        }).populate({path: 'memberId', select: 'username_lower'})
            .populate({path: 'memberParentGroup', select: 'type endpoint'});
    }], function (err, asyncResponse) {

        if (err) {
            if (err === 1001) {
                return res.send({
                    code: 1001,
                    message: "Invalid Password"
                });
            }
            return res.send({message: err, result: err, code: 999});
        }

        let transaction = asyncResponse[0];

        async.series([callback => {

            MemberService.updateBalance2(transaction.memberUsername, Math.abs(transaction.memberCredit), betId, 'CANCEL_TICKET', betId, function (err, creditResponse) {

                if (err) {
                    callback('fail', null);
                } else {
                    callback(null, 'success');
                }
            });


        }], function (err, response) {

            if (err) {
                return res.send({
                    message: "update credit failed",
                    result: err,
                    code: 999
                });
            }

            let body = {
                status: status,
                remark: remark,
                cancelDate:DateUtils.getCurrentDate(),
                cancelByType:'API',
                settleDate: DateUtils.getCurrentDate()
            };

            BetTransactionModel.update({_id: transaction._id}, body, (err, data) => {
                if (err) {
                    return res.send({message: "error", result: err, code: 999});
                } else if (data.nModified === 0) {
                    return res.send({
                        message: "error",
                        result: 'update ticket fail',
                        code: 998
                    });
                } else {
                    return res.send({message: "success", code: 0});
                }
            });
        });
    });
});




const cancelBetMatchScoreSchema = Joi.object().keys({
    matchId: Joi.string().required(),
    status: Joi.string().valid('REJECTED', 'CANCELLED'),
    remark: Joi.string().allow(''),
    homeScore : Joi.number().required(),
    awayScore : Joi.number().required()
});

router.post('/cancel-ticket-match-score', function (req, res) {
    console.log('================= cancel-ticket =================');

    let validate = Joi.validate(req.body, cancelBetMatchScoreSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    const {
        matchId,
        status,
        remark,
        type
    } = req.body;


    async.series([callback => {

        let condition = {game:'FOOTBALL',
            'hdp.matchId': matchId,
            status: 'RUNNING'};

        if (type === 'HT') {
            condition['hdp.oddType'] = {'$in': ['AH1ST', 'OU1ST', 'OE1ST', 'X121ST']}
        }

        BetTransactionModel.find(condition, function (err, betResponse) {
            if (err) {
                callback(err, null);
                return;
            }

            async.each(betResponse, (betObj, asyncCallback) => {

                async.series([callback => {

                    let body = {
                        status: status,
                        remark: remark,
                        cancelDate:DateUtils.getCurrentDate(),
                        cancelByType:'API',
                        settleDate: DateUtils.getCurrentDate()
                    };

                    BetTransactionModel.update({_id: betObj._id}, body, (err, data) => {
                        if (err) {

                            callback('fail', null);
                        } else {
                                callback(null, 'success');
                        }
                    });

                }], function (err, asyncResponse) {

                    MemberService.updateBalance2(betObj.memberUsername, Math.abs(betObj.memberCredit), betObj.betId, 'CANCEL_MATCH', betObj.betId, function (err, creditResponse) {

                        if (err) {
                            asyncCallback('fail', null);
                        } else {
                            asyncCallback(null, 'success');
                        }
                    });

                });

            }, (err) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, betResponse);
                }
            });//end forEach Match

        }).populate({path: 'memberId', select: 'username_lower'})
            .populate({path: 'memberParentGroup', select: 'type endpoint'})

    }, callback => {

        let condition = {
            game:'FOOTBALL',
            'gameType': {$ne: 'TODAY'},
            'parlay.matches.matchId':matchId,
            'status': 'RUNNING',
            createdDate: {$gte: moment().add(-10, 'd').utc(true).toDate()}
        };


        BetTransactionModel.find(condition)
            .select('memberId memberUsername parlay commission amount memberCredit payout memberParentGroup status betId')
            .populate({path: 'memberParentGroup', select: 'type'})
            .exec(function (err, response) {

                async.each(response, (betObj, betListCallback) => {


                    const allMatch = betObj.parlay.matches;

                    let filterList = _.filter(allMatch, function (response) {
                        if (type === 'HT') {
                            return response.matchId === matchId && ['AH1ST', 'OU1ST', 'OE1ST', 'X121ST'].includes(response.oddType);
                        } else {
                            return response.matchId === matchId;
                        }
                    });


                    if (filterList.length === 0) {
                        betListCallback(null, 'not data found');
                        return;
                    }

                    let endScoreMatch = _.filter(allMatch, function (response) {
                        return response.betResult && response.betResult !== '';
                    });


                    if ((endScoreMatch.length + filterList.length) == allMatch.length) {


                        let totalUnCheckOdds = _.filter(allMatch, function (response) {
                            return response.matchId !== matchId && response.betResult;
                        }).map((item) => {
                            return calculateParlayOdds(item.betResult, item.odd);
                        });


                        let sumTotalUnCheckOdds = _.reduce(totalUnCheckOdds, (memo, item) => {
                            return {sum: memo.sum * item};
                        }, {sum: 1}).sum;


                        let halfLoseCount = _.filter(allMatch, function (response) {
                            return response.betResult === HALF_LOSE && response.matchId !== matchId;
                        }).length;

                        console.log(sumTotalUnCheckOdds);
                        //prepare with last match result
                        sumTotalUnCheckOdds = roundTo.down(sumTotalUnCheckOdds, 3);


                        console.log('sumTotalUnCheckOdds : ', sumTotalUnCheckOdds);
                        console.log('halfLoseCount : ', halfLoseCount);

                        let winLoseAmount = 0;
                        let shareReceive;

                        winLoseAmount = (subPlyAmount(betObj.amount, halfLoseCount) * sumTotalUnCheckOdds);
                        let validAmount = subPlyAmount(betObj.amount, halfLoseCount);

                        winLoseAmount = winLoseAmount - betObj.amount;

                        shareReceive = prepareShareReceive(winLoseAmount, WIN, betObj);


                        updateAgentMemberBalance(shareReceive, 'PARLAY', (err, response) => {
                            if (err) {
                                betListCallback(err, null);
                            } else {
                                let updateBody = {
                                    $set: {
                                        'parlay.matches.$.isCancel': true,
                                        'parlay.matches.$.betResult': 'DRAW',
                                        'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                                        'commission.company.winLose': shareReceive.company.winLose,
                                        'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                                        'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                                        'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                                        'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                                        'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                                        'commission.senior.winLose': shareReceive.senior.winLose,
                                        'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                                        'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                                        'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                                        'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                                        'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                                        'commission.agent.winLose': shareReceive.agent.winLose,
                                        'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                                        'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                                        'commission.member.winLose': shareReceive.member.winLose,
                                        'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                                        'validAmount': validAmount,
                                        'betResult': 'WIN',
                                        'isEndScore': true,
                                        'status': 'DONE'
                                    }
                                };

                                BetTransactionModel.update({
                                    _id: betObj._id,
                                    'parlay.matches.matchId': matchId
                                }, updateBody, function (err, response) {
                                    if (err) {
                                        betListCallback(err, null);
                                    } else {
                                            betListCallback(null, response);
                                    }

                                });

                            }
                        });

                    } else {
                        _.each(filterList, (match) => {

                            let updateBody = {
                                $set: {
                                    'parlay.matches.$.isCancel': true,
                                    'parlay.matches.$.betResult': 'DRAW'
                                }
                            };

                            BetTransactionModel.update({
                                _id: betObj._id,
                                'parlay.matches._id': match._id
                            }, updateBody, function (err, response) {
                                if (err) {
                                    betListCallback(err, null);
                                } else {
                                    betListCallback(null, response);
                                }
                            });

                        });
                    }


                }, (err) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, 'success');
                    }
                });//end forEach Match

            });
    }], function (err, asyncResponse) {

        if (err) {
            return res.send({
                message: "find transaction failed",
                result: err,
                code: 999
            });
        }

        let transaction = asyncResponse[0];

        console.log(transaction)


        let apiResponse = _.filter(transaction, (item) => {
            return item.memberParentGroup.type === 'API';
        });

        if (apiResponse.length > 0) {
            let endpoint = apiResponse[0].memberParentGroup.endpoint;

            ApiService.cancelMatch(endpoint, matchId, remark, apiResponse, (err, response) => {
                if (err) {
                    return res.send({
                        message: "99",
                        result: err,
                        code: 999
                    });
                } else {
                    return res.send({message: "success", code: 0});
                }

            });
        } else {
            return res.send({message: "success", code: 0});
        }
    });
});



module.exports = router;