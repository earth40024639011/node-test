const MemberModel = require('../../models/member.model');
const MemberTransferHistoryModel = require('../../models/memberTransferHistory.model');
const AgentGroupModel = require('../../models/agentGroup.model');
const BetTransactionModel = require('../../models/betTransaction.model.js');
const PaymentHistoryModel = require('../../models/paymentHistory.model.js');
const TokenModel = require('../../models/token.model.js');
const express = require('express')
const uuid = require('uuid/v4');
const router = express.Router()
const async = require("async");
const Joi = require('joi');
const mongoose = require('mongoose');
const Hashing = require('../../common/hashing');
const DateUtils = require('../../common/dateUtils');
const config = require('config');
const moment = require('moment');
const _ = require('underscore');
const roundTo = require('round-to');
const MemberService = require('../../common/memberService');
const jwt = require('../../common/jwt');


const getUserInfo = Joi.object().keys({
    playerId: Joi.string().required(),
    signature: Joi.string().required(),
});

router.post('/playerInfo', function (req, res) {

    let validate = Joi.validate(req.body, getUserInfo);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            result: validate.error.details,
            code: 999
        });
    }

    let requestBody = req.body;
    let hashMessage = requestBody.playerId;


    if (!isValidSignature(requestBody.signature, hashMessage)) {
        return res.send({code: 1002, message: "Signature not match"});
    }

    MemberService.findByUserName(req.body.playerId, function (err, response) {
        if (err)
            return res.send({message: "error", result: err, code: 999});


        let credit = roundTo((response.creditLimit + response.balance), 2);

        let info = {
            playerId : response.username,
            credit : credit,
            contact: response.contact,
            phoneNo: response.phoneNo,
            currency: response.currency,
            responseDateTime: moment().format('YYYY-MM-DD HH:mm:ss')

        };

        return res.send(
            {
                code: 0,
                message: "success",
                result: info
            }
        );
    });

});




const topupSchema = Joi.object().keys({
    key: Joi.string().required(),
    playerId: Joi.string().required(),
    amount: Joi.number().required(),
    signature: Joi.string().required(),
});

router.post('/deposit', function (req, res) {

    let validate = Joi.validate(req.body, topupSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            result: validate.error.details,
            code: 999
        });
    }

    let requestBody = req.body;
    let hashMessage = requestBody.key + requestBody.playerId + requestBody.amount;


    if (!isValidSignature(requestBody.signature, hashMessage)) {
        return res.send({code: 1002, message: "Signature not match"});
    }

    MemberService.findByUserName(req.body.playerId, function (err, response) {
        if (err)
            return res.send({message: "error", result: err, code: 999});


        let credit = roundTo((response.creditLimit + response.balance), 2);

        MemberService.updateBalance2(response.username_lower,requestBody.amount,requestBody.key,"TOPUP_API_DEPOSIT",'',(err,updateBalanceResponse)=>{

            if (err)
                return res.send({message: "error", result: err, code: 999});

            let info = {
                key:requestBody.key,
                playerId : response.username,
                oldCredit : credit,
                newCredit : updateBalanceResponse.newBalance,
                responseDateTime: moment().format('YYYY-MM-DD HH:mm:ss')

            };

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: info
                }
            );
        });

    });

});


const topupWithDrawSchema = Joi.object().keys({
    key: Joi.string().required(),
    playerId: Joi.string().required(),
    amount: Joi.number().required(),
    signature: Joi.string().required(),
});

router.post('/withdraw', function (req, res) {

    let validate = Joi.validate(req.body, topupWithDrawSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            result: validate.error.details,
            code: 999
        });
    }

    let requestBody = req.body;
    let hashMessage = requestBody.key + requestBody.playerId + requestBody.amount;


    if (!isValidSignature(requestBody.signature, hashMessage)) {
        return res.send({code: 1002, message: "Signature not match"});
    }

    MemberService.findByUserName(req.body.playerId, function (err, response) {
        if (err)
            return res.send({message: "error", result: err, code: 999});


        let credit = roundTo((response.creditLimit + response.balance), 2);

        if(credit < requestBody.amount){

            let info = {
                key:requestBody.key,
                playerId : response.username,
                oldCredit : credit,
                responseDateTime: moment().format('YYYY-MM-DD HH:mm:ss')

            };

            return res.send(
                {
                    code: 50055,
                    message: "credit exceed",
                    result: info
                }
            );
        }

        MemberService.updateBalance2(response.username_lower,requestBody.amount,requestBody.key,"TOPUP_API_WITHDRAW",'',(err,updateBalanceResponse)=>{

            if (err)
                return res.send({message: "error", result: err, code: 999});

            let info = {
                key:requestBody.key,
                playerId : response.username,
                oldCredit : credit,
                newCredit : updateBalanceResponse.newBalance,
                responseDateTime: moment().format('YYYY-MM-DD HH:mm:ss')
            };

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: info
                }
            );
        });

    });

});



function isValidSignature(signature, bodyMessage) {
    console.log('request signature : ', signature)
    console.log('bodyMessage : ',bodyMessage)
    console.log('correct signature : ', Hashing.makeSignature('AMB', bodyMessage))
    return signature.toUpperCase() === Hashing.makeSignature('AMB', bodyMessage).toUpperCase();
}




module.exports = router;