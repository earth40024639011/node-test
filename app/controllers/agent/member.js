const MemberModel = require('../../models/member.model');
const CloneMemberHistory  = require('../../models/cloneMemberHistory.model');
const MemberTransferHistoryModel = require('../../models/memberTransferHistory.model');
const AgentGroupModel = require('../../models/agentGroup.model');
const BetTransactionModel = require('../../models/betTransaction.model.js');
const PaymentHistoryModel = require('../../models/paymentHistory.model.js');
const MemberHistoryModel = require('../../models/memberHistory.model.js');
const MemberClearCashModel = require('../../models/memberClearCash.model.js');
const UserModel = require("../../models/users.model");
const express = require('express');
const router = express.Router()
const async = require("async");
const Joi = require('joi');
const mongoose = require('mongoose');
const Hashing = require('../../common/hashing');
const DateUtils = require('../../common/dateUtils');
const config = require('config');
const moment = require('moment');
const _ = require('underscore');
const roundTo = require('round-to');
const MemberService = require('../../common/memberService');
const AgentService = require('../../common/agentService');
const AmbGameService = require('../../common/ambGameService');
const PrettyGameService = require('../../common/prettygameService');

const SexyBaccaratService = require('../../common/sexyService');
const SaGameService = require('../../common/sagameService');

const ApiService = require('../../common/apiService');
const NumberUtils = require('../../common/numberUtils1');
const Redis = require('../sbo/1_common/1_redis/1_1_redis');

const UpdateStatusHistoryModel = require('../../models/updateStatusHistory.model.js');

const httpRequest = require('request');
// const URL_PAYMENT_GATEWAY_UPDATE_PASSWORD = process.env.URL_PAYMENT_GATEWAY_UPDATE_PASSWORD || 'http://178.128.107.175:8002/external/customer/update-password';
const CLIENT_NAME = process.env.CLIENT_NAME || 'SPORTBOOK88';

const getClientKey = require('../getClientKey');
let {
    URL_PAYMENT_GATEWAY_UPDATE_PASSWORD = 'http://178.128.107.175:8002/external/customer/update-password'
} = getClientKey.getKey();


router.get('/ticket/list', function (req, res) {
    const now = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const startDate = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
    const endDate = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);

    if ((now.getHours() - 7) >= 11 && now.getMinutes() >= 0 && now.getSeconds() >= 0) {
        startDate.setDate(now.getDate());
        endDate.setDate(now.getDate() + 1);
    } else {
        startDate.setDate(now.getDate() - 1);
        endDate.setDate(now.getDate());
    }
    startDate.setHours(11 + 7);
    startDate.setMinutes(0);
    startDate.setSeconds(0);
    endDate.setHours(10 + 7);
    endDate.setMinutes(59);
    endDate.setSeconds(59);


    let condition = {};
    condition.memberId = req.userInfo._id;
    condition['status'] = {$in: ['RUNNING', 'WAITING', 'REJECTED', 'CANCELLED']};
    condition['isEndScore'] = {$ne: true};
    condition['game'] = {$in: ['FOOTBALL', 'M2']}

    BetTransactionModel.find(condition)
        .select('hdp parlay createdDate betId status gameType priceType amount memberCredit payout remark gameDate source commission.member')
        .sort({'createdDate': -1})
        .lean()
        .exec(function (err, data) {
            if (err)
                return res.send({
                    message: "error",
                    result: err,
                    code: 999
                });

            _.each(data, ticket => {
                if (!_.isEmpty(ticket.parlay.matches)) {
                    delete ticket.hdp;
                }
                ticket.memberCommission = Math.abs(ticket.amount * NumberUtils.convertPercent(ticket.commission.member.commission));
                ticket.memberCreditUsage = Math.abs(ticket.memberCredit);
                ticket.memberCommPercent = ticket.commission.member.commission;
            });

            data = _.filter(data, ticket => {
                if (_.isEqual(ticket.status, 'REJECTED')) {
                    return ticket.gameDate >= startDate &&
                        ticket.gameDate <= endDate;
                } else if (_.isEqual(ticket.status, 'CANCELLED')) {
                    return ticket.gameDate >= startDate &&
                        ticket.gameDate <= endDate;
                } else {
                    return true;
                }
            });

            return res.send(
                {
                    code: 0,
                    message: "success",
                    results: data
                }
            );
        });

});

router.get('/history/list', function (req, res) {


    let condition = {};

    let userInfo = req.userInfo;

    let game = req.query.game;

    if (game === 'FOOTBALL' || game === 'ALL') {
        if (req.query.oddType && req.query.oddType !== 'ALL') {
            if (req.query.gameType === 'PARLAY') {
                condition['gameType'] = 'PARLAY';
            } else if (req.query.gameType === 'TODAY') {
                condition['gameType'] = 'TODAY';
                if (req.query.oddType === 'HDP') {
                    condition['hdp.oddType'] = 'AH';
                } else if (req.query.oddType === 'OU') {
                    condition['$or'] = [{'hdp.oddType': 'OU'}, {'hdp.oddType': 'OU1ST'}];
                } else if (req.query.oddType === 'OE') {
                    condition['$or'] = [{'hdp.oddType': 'OE'}, {'hdp.oddType': 'OE1ST'}];
                } else if (req.query.oddType === 'X12') {
                    condition['$or'] = [{'hdp.oddType': 'X12'}, {'hdp.oddType': 'X121ST'}];
                } else {
                    condition['hdp.oddType'] = req.query.oddType;
                }
            }
        }
        condition['game'] = {$in: ['FOOTBALL', 'M2']};
    }
    if (game === 'CASINO' || game === 'ALL') {
        condition['game'] = 'CASINO';
    }

    if (game === 'LOTTO') {
        condition['game'] = 'LOTTO';
    }

    if (req.query.status) {
        condition['$or'] = req.query.status.split(',').map(function (status) {
            return {'status': status}
        });
    }

    condition['memberId'] = mongoose.Types.ObjectId(userInfo._id);

    if (req.query.day) {

        let dateFrom, dateTo;

        if (req.query.day === '0') {
            if (moment().hours() < 11) {
                dateFrom = moment().millisecond(0).second(0).minute(0).hour(11).add(-1, 'd').utc(true).toDate();
                dateTo = moment().millisecond(0).second(0).minute(0).hour(11).add(0, 'd').utc(true).toDate();
            } else {
                dateFrom = moment().millisecond(0).second(0).minute(0).hour(11).add(0, 'd').utc(true).toDate();
                dateTo = moment().millisecond(0).second(0).minute(0).hour(11).add(1, 'd').utc(true).toDate();
            }
            condition['gameDate'] = {
                "$gte": dateFrom,
                "$lt": dateTo
            };
        } else if (req.query.day === '1') {
            if (moment().hours() < 11) {
                dateFrom = moment().millisecond(0).second(0).minute(0).hour(11).add(0, 'd').utc(true).toDate();
                dateTo = moment().millisecond(0).second(0).minute(0).hour(11).add(1, 'd').utc(true).toDate();
            } else {
                dateFrom = moment().millisecond(0).second(0).minute(0).hour(11).add(1, 'd').utc(true).toDate();
                dateTo = moment().millisecond(0).second(0).minute(0).hour(11).add(2, 'd').utc(true).toDate();
            }
            condition['gameDate'] = {
                "$gte": dateFrom,
                "$lt": dateTo
            };
        } else if (req.query.day === '2') {
            if (moment().hours() < 11) {
                dateFrom = moment().millisecond(0).second(0).minute(0).hour(11).add(-1, 'd').utc(true).toDate();
            } else {
                dateFrom = moment().millisecond(0).second(0).minute(0).hour(11).add(-1, 'd').utc(true).toDate();
            }
            condition['gameDate'] = {
                "$gte": dateFrom
            };
        }

    }

    // console.log(yesterday);
    console.log(condition);


    BetTransactionModel.aggregate([
        {
            $match: condition
        },
        {
            $project: {
                _id: 0,
                memberId: '$memberId',
                hdp: '$hdp',
                lotto: '$lotto',
                parlay: '$parlay',
                memberCommission: {$multiply: ['$amount', {$divide: ['$commission.member.commission', 100]}]},
                memberCommPercent: '$commission.member.commission',
                memberCreditUsage: {$abs: '$memberCredit'},
                amount: '$amount',
                payout: '$payout',
                // totalReceive: {$multiply: ['$amount', {$divide: ['$commission.' + group + '.shareReceive', 100]}]},
                stakeIn: '$amount',
                stakeOut: {
                    $cond: [
                        {$eq: ['$gameType', 'PARLAY']},
                        {$cond: [{$gte: ['$parlay.odds', 0]}, '$amount', {$multiply: ['$amount', {$abs: '$parlay.odds'}]}]}
                        , {$cond: [{$gte: ['$hdp.odd', 0]}, '$amount', {$multiply: ['$amount', {$abs: '$hdp.odd'}]}]}
                    ]
                },
                betId: '$betId',
                createdDate: '$createdDate',
                priceType: '$priceType',
                gameType: '$gameType',
                game: '$game',
                source: '$source',
                status: '$status',
                winLose: '$commission.member.winLose',
                winLoseCom: '$commission.member.winLoseCom',
                betResult: '$betResult',
                remark: '$remark'
            }
        },
        {$sort: {'createdDate': -1}},
    ], (err, results) => {
        if (err)
            return res.send({message: "error", results: err, code: 999});

        let summary = _.reduce(results, function (memo, num) {
            return {
                totalAmount: memo.totalAmount + num.amount,
                totalStakeIn: memo.totalStakeIn + num.stakeIn,
                totalStakeOut: memo.totalStakeOut + num.stakeOut,
                totalWinLose: memo.totalWinLose + num.winLose,
                totalWinLoseCom: memo.totalWinLoseCom + num.winLoseCom
            }
        }, {
            totalAmount: 0,
            totalStakeIn: 0,
            totalStakeOut: 0,
            totalWinLose: 0,
            totalWinLoseCom: 0
        });

        return res.send(
            {
                code: 0,
                message: "success",
                result: {
                    betList: results,
                    summary: summary,
                }
            }
        );
    }).read('secondaryPreferred').option({maxTimeMS: 150000});
});

router.get('/statement', function (req, res) {


    let memberId = req.userInfo._id;

    async.waterfall([(callback => {

            MemberModel.findById(memberId, (err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            }).select('type parentId');

        }), ((agentGroup, callback) => {

            MemberService.findMemberOrAgentPaymentHistory(memberId, req.query.dateFrom, req.query.dateTo, function (err, response) {

                if (err) {
                    callback(err, null);
                } else {
                    response = _.map(response, item => {
                        let newItem = item;
                        newItem.order = 0;
                        return newItem
                    });
                    callback(null, agentGroup, response);
                }
            });

        }), ((agentGroup, paymentHistory, callback) => {

            let condition = {};

            if (req.query.dateFrom && req.query.dateTo) {
                condition['gameDate'] = {
                    "$gte": moment(req.query.dateFrom, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(11).utc(true).toDate(),
                    "$lt": moment(req.query.dateTo, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(11).add(1, 'd').utc(true).toDate()
                }
            }
            // condition['isEndScore'] = true;
            condition['memberId'] = mongoose.Types.ObjectId(memberId);
            condition['status'] = {$in: ['DONE', 'REJECTED', 'CANCELLED']};

            BetTransactionModel.aggregate([
                {
                    $match: condition
                },
                {
                    $group: {
                        _id: {
                            date: {
                                $cond: [
                                    // {$and:[
                                    {$gte: [{$hour: "$gameDate"}, 11]},
                                    {$dateToString: {format: "%d/%m/%Y", date: "$gameDate"}},
                                    {
                                        $dateToString: {
                                            format: "%d/%m/%Y",
                                            date: {$subtract: ["$gameDate", 24 * 60 * 60 * 1000]}
                                        }
                                    }
                                ]
                            },
                            game: '$game'
                        },
                        betAmount: {$sum: '$amount'},
                        balance: {$sum: '$commission.member.totalWinLoseCom'},
                        commission: {$sum: '$commission.member.winLoseCom'},
                        wl: {$sum: '$commission.member.winLose'}
                    }
                },
                {
                    $project: {
                        _id: 0,
                        game: '$_id.game',
                        date: '$_id.date',
                        betAmount: '$betAmount',
                        balance: '$balance',
                        wl: '$wl',
                        commission: '$commission',
                        order: {$cond: [{$eq: ['$_id.game', 'FOOTBALL']}, 1, {$cond: [{$eq: ['$_id.game', 'CASINO']}, 2, 3]}]},
                        remark: {$cond: [{$eq: ['$_id.game', 'FOOTBALL']}, 'Betting (Sports)', 'Betting (Live Casino & Casino Games)']}
                    }
                }
            ], (err, results) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, agentGroup, paymentHistory, results);
                }
            }).read('secondaryPreferred').option({maxTimeMS: 150000});

        }), ((agentGroup, paymentHistory, statementList, callback) => {


            let lastPaymentCondition = {
                member: mongoose.Types.ObjectId(memberId),
                createdDate: {
                    "$lt": moment(req.query.dateFrom, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(11).utc(true).toDate()
                }
            };


            PaymentHistoryModel.aggregate([
                {
                    $match: lastPaymentCondition
                },
                {
                    $sort: {_id: -1}
                },
                {
                    $limit: 1
                },
                {
                    $project: {
                        date: {
                            $cond: [
                                // {$and:[
                                {$gte: [{$hour: "$createdDate"}, 11]},
                                {$dateToString: {format: "%d/%m/%Y", date: "$createdDate"}},
                                {
                                    $dateToString: {
                                        format: "%d/%m/%Y",
                                        date: {$subtract: ["$createdDate", 24 * 60 * 60 * 1000]}
                                    }
                                }
                            ]
                        },
                        remainingBalance: '$remainingBalance',
                        amount: '$amount'
                    }
                }
            ], (err, results) => {

                let response = results[0];

                let condition = {};

                let paymentBalance = 0;


                if (response) {
                    paymentBalance = response.remainingBalance;

                    condition['gameDate'] = {
                        "$gte": moment(response.date, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(11).utc(true).toDate(),
                        "$lt": moment(req.query.dateFrom, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(11).utc(true).toDate()
                    };

                } else {

                    condition['gameDate'] = {
                        "$lt": moment(req.query.dateFrom, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(11).utc(true).toDate()
                    };

                }


                condition['isEndScore'] = true;
                condition['memberId'] = mongoose.Types.ObjectId(memberId);
                condition['status'] = 'DONE';

                console.log(condition)


                BetTransactionModel.aggregate([
                    {
                        $match: condition
                    },
                    {
                        $group: {
                            _id: {
                                memberId: '$memberId'
                            },
                            betAmount: {$sum: '$amount'},
                            balance: {$sum: '$commission.member.totalWinLoseCom'},
                            commission: {$sum: '$commission.member.winLoseCom'},
                            wl: {$sum: '$commission.member.winLose'},
                            count: {$sum: 1}
                        }
                    },
                    {
                        $project: {
                            _id: 0,
                            game: '$_id.game',
                            date: '$_id.date',
                            betAmount: '$betAmount',
                            balance: '$balance',
                            wl: '$wl',
                            commission: '$commission',
                            count: '$count'
                        }
                    }
                ], (err, results) => {
                    if (err) {
                        callback(err, null);
                    } else {

                        if (results.length > 0) {
                            console.log(results)
                            // console.log('remainingBalance : ',paymentBalance)
                            // console.log('xxxxxx : ',results[0])
                            paymentBalance += results[0].balance;
                        }

                        let openingBalance = {
                            game: '',
                            date: req.query.dateFrom,
                            betAmount: 0,
                            balance: paymentBalance,
                            wl: paymentBalance,
                            commission: 0,
                            remark: 'Opening Balance',
                        };
                        console.log(openingBalance)
                        callback(null, agentGroup, paymentHistory, statementList, openingBalance);
                    }

                }).read('secondaryPreferred').option({maxTimeMS: 150000});

            });
        })], function (err, agentGroup, paymentHistory, statementList, openingBalance) {

            if (err) {
                return res.send({message: "error", result: err, code: 999});
            }

            statementList = statementList.concat(paymentHistory);


            statementList.sort((a, b) => {
                return b.order - a.order;
            });

            statementList.push(openingBalance);


            // sorting date
            statementList = _.sortBy(statementList, (item) => {
                return -moment(item.date, 'DD/MM/YYYY');
            });


            let dateFromMoment = moment(req.query.dateFrom, "DD/MM/YYYY").add(-1, 'days').format('YYYY-MM-DDT00:00:00.000');
            let dateToMoment = moment(req.query.dateTo, "DD/MM/YYYY").add(1, 'days').format('YYYY-MM-DDT00:00:00.000');

            let filterDate = _.filter(statementList, function (response) {
                let da = moment(response.date, 'DD/MM/YYYY');
                return da.isBetween(dateFromMoment, dateToMoment);
            });


            let prepareStatement = _.map(filterDate, function (response) {
                let obj = response;
                const dateParts = obj.date.split("/");
                obj.date = DateUtils.convertThaiDate(new Date(dateParts[2], dateParts[1] - 1, dateParts[0]));
                obj.betAmount = obj.betAmount ? roundTo(obj.betAmount, 2) : 0;
                obj.balance = roundTo(obj.balance, 2);
                obj.wl = roundTo(obj.wl, 2);
                obj.commission = roundTo(obj.commission, 2);
                return response
            });

            prepareStatement = _.chain(prepareStatement).reverse().value();

            async.forEachOf(prepareStatement, (item, index, callback) => {
                if (index > 0 && (prepareStatement.length - 1) !== index) {
                    //is middle

                    if (item.remark === 'Transactions') {
                        item.game = '';
                        item.balance = item.wl + prepareStatement[index - 1].balance;
                    } else {
                        item.balance = item.balance + prepareStatement[index - 1].balance;
                    }
                } else {
                    //is last
                    if ((index === prepareStatement.length - 1) && prepareStatement.length > 1) {
                        //is last
                        if (item.remark === 'Transactions') {
                            item.game = '';
                            item.balance = item.wl + prepareStatement[index - 1].balance;
                        } else {
                            item.balance = (item.balance) + (prepareStatement[index - 1].balance);
                        }
                    } else {
                        //is first
                        item.balance = (item.balance);
                    }
                }

                callback();
            }, (err) => {
                if (err) {
                    return res.send({message: "error", result: err, code: 999});
                } else {

                    // _.sortBy(prepareStatement, function(o) { return o.date; })
                    return res.send({
                        code: 0,
                        message: "success",
                        result: {
                            dataList: prepareStatement
                        }
                    });
                }
            });//end forEach Statement

        }
    )
    ;
});

router.get('/statement/detail', function (req, res) {


    let condition = {};

    let userInfo = req.userInfo;

    if (req.query.date) {
        condition['gameDate'] = {
            "$gte": moment(req.query.date, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(11).utc(true).toDate(),
            "$lt": moment(req.query.date, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(11).add(1, 'd').utc(true).toDate()
        }

    }

    if (req.query.game) {
        condition['game'] = req.query.game;
    }

    condition['status'] = {$in: ['DONE', 'REJECTED', 'CANCELLED']};

    condition['memberId'] = mongoose.Types.ObjectId(userInfo._id);

    console.log(condition);

    BetTransactionModel.aggregate([
        {
            $match: condition
        },
        {
            $project: {
                _id: 0,
                memberId: '$memberId',
                hdp: '$hdp',
                parlay: '$parlay',
                baccarat: '$baccarat',
                lotto: '$lotto',
                multi: '$multi',
                slot: '$slot',
                amount: '$amount',
                payout: '$payout',
                // totalReceive: {$multiply: ['$amount', {$divide: ['$commission.' + group + '.shareReceive', 100]}]},
                stakeIn: '$amount',
                stakeOut: {
                    $cond: [
                        {$eq: ['$gameType', 'PARLAY']},
                        {$cond: [{$gte: ['$parlay.odds', 0]}, '$amount', {$multiply: ['$amount', {$abs: '$parlay.odds'}]}]}
                        , {$cond: [{$gte: ['$hdp.odd', 0]}, '$amount', {$multiply: ['$amount', {$abs: '$hdp.odd'}]}]}
                    ]
                },
                betId: '$betId',
                createdDate: '$createdDate',
                priceType: '$priceType',
                gameType: '$gameType',
                game: '$game',
                source: '$source',
                status: '$status',
                winLose: {$cond: [{$eq: ['$status', 'DONE']}, '$commission.member.winLose', 0]},
                winLoseCom: {$cond: [{$eq: ['$status', 'DONE']}, '$commission.member.winLoseCom', 0]},
                betResult: '$betResult',
            }
        },
        {$sort: {'createdDate': -1}},
    ], (err, results) => {
        if (err)
            return res.send({message: "error", results: err, code: 999});

        results = prepareSexyBaccaratDetail(results);

        let summary = _.reduce(results, function (memo, num) {
            return {
                totalAmount: memo.totalAmount + num.amount,
                totalStakeIn: memo.totalStakeIn + num.stakeIn,
                totalStakeOut: memo.totalStakeOut + num.stakeOut,
                totalWinLose: memo.totalWinLose + num.winLose,
                totalWinLoseCom: memo.totalWinLoseCom + num.winLoseCom
            }
        }, {
            totalAmount: 0,
            totalStakeIn: 0,
            totalStakeOut: 0,
            totalWinLose: 0,
            totalWinLoseCom: 0
        });

        return res.send(
            {
                code: 0,
                message: "success",
                result: {
                    betList: results,
                    summary: summary,
                }
            }
        );
    }).read('secondaryPreferred').option({maxTimeMS: 150000});
});

function prepareSexyBaccaratDetail(dataList) {
    return _.map(dataList, (item) => {

        if (item.source === 'SEXY_BACCARAT') {
            let newItem = Object.assign({}, item)._doc ? Object.assign({}, item)._doc : Object.assign({}, item);

            let betList = _.filter(item.baccarat.sexy.txns, item => {
                return item.status !== 'CANCEL' && item.status !== 'VOID';
            });

            let gameType = item.baccarat.sexy.txns[0].gameType;

            // newItem.baccarat = item.baccarat;
            newItem.baccarat.gameType = gameType;
            if (gameType.toUpperCase() === "DRAGONTIGER") {
                newItem.baccarat.betDetail = _.reduce(betList, function (total, obj) {
                    return {
                        dragon: total.dragon + (obj.category === 'Dragon' ? obj.betAmount : 0),
                        tiger: total.tiger + (obj.category === 'Tiger' ? obj.betAmount : 0),
                        tie: total.tie + (obj.category === 'Tie' ? obj.betAmount : 0)
                    };
                }, {
                    dragon: 0,
                    tiger: 0,
                    tie: 0
                });
                return newItem;
            } else {
                newItem.baccarat.betDetail = _.reduce(betList, function (total, obj) {
                    return {
                        banker: total.banker + (obj.category === 'Banker' ? obj.betAmount : 0),
                        player: total.player + (obj.category === 'Player' ? obj.betAmount : 0),
                        tie: total.tie + (obj.category === 'Tie' ? obj.betAmount : 0),
                        bankerPair: total.bankerPair + (obj.category === 'BankerPair' ? obj.betAmount : 0),
                        playerPair: total.playerPair + (obj.category === 'PlayerPair' ? obj.betAmount : 0),
                        bankerBonus: total.bankerBonus + (obj.category === 'BankerBonus' ? obj.betAmount : 0),
                        playerBonus: total.playerBonus + (obj.category === 'PlayerBonus' ? obj.betAmount : 0),
                        big: total.big + (obj.category === 'Big' ? obj.betAmount : 0),
                        small: total.small + (obj.category === 'Small' ? obj.betAmount : 0),
                        bankerIn: total.bankerIn + (obj.category.startsWith('Banker Insurance') ? obj.betAmount : 0),
                        playerIn: total.playerIn + (obj.category.startsWith('Player Insurance') ? obj.betAmount : 0),
                    };
                }, {
                    banker: 0,
                    player: 0,
                    tie: 0,
                    bankerPair: 0,
                    playerPair: 0,
                    bankerBonus: 0,
                    playerBonus: 0,
                    big: 0,
                    small: 0,
                    bankerIn: 0,
                    playerIn: 0
                });
                return newItem;
            }
        } else if (item.source === 'AG_GAME') {
            let newItem = Object.assign({}, item)._doc ? Object.assign({}, item)._doc : Object.assign({}, item);

            newItem.baccarat.betDetail = _.chain(item.baccarat.ag.resultTransaction).groupBy(item => {
                return item.playType;
            }).map((value, key) => {
                return {
                    type: key,
                    amount: _.reduce(value, (total, x) => {
                        return total + x.validAmount;
                    }, 0)
                }
            }).map(item => {
                return item.type + ' ' + item.amount
            }).value();

            newItem.baccarat.betDetail = newItem.baccarat.betDetail.join(' , ')

            return newItem;
        } else if (item.source === 'PRETTY_GAME') {
            let newItem = Object.assign({}, item)._doc ? Object.assign({}, item)._doc : Object.assign({}, item);

            newItem.baccarat.betDetail = _.chain(item.baccarat.pretty.txns).groupBy(item => {
                return item.betType;
            }).map((value, key) => {
                return {
                    type: PrettyGameService.getBetDetail(item.baccarat.pretty.gameType, key),
                    amount: _.reduce(value, (total, x) => {
                        return total + x.betAmount;
                    }, 0)
                }
            }).map(item => {
                return item.type + ' ' + item.amount
            }).value();

            newItem.baccarat.betDetail = newItem.baccarat.betDetail.join(' , ')

            return newItem;
        } else if (item.source === 'AMB_GAME') {
            let newItem = Object.assign({}, item)._doc ? Object.assign({}, item)._doc : Object.assign({}, item);
            newItem.multi.amb.betDetail = _.chain(item.multi.amb.bet).map(item => {

                return {
                    type: AmbGameService.getBetKeyName(newItem.multi.amb.gameName, item.key),
                    amount: item.amount
                }

            }).map(item => {
                return item.type + ' ' + item.amount
            }).value();

            newItem.multi.amb.betDetail = newItem.multi.amb.betDetail.join(' , ')
            newItem.multi.amb.result = _.chain(item.multi.amb.results).map(item => {

                return {
                    type: AmbGameService.getResult(newItem.multi.amb.gameName, item),
                }

            }).map(item => {
                return item.type
            }).value();
            return newItem;

        } else {
            let newItem = Object.assign({}, item)._doc ? Object.assign({}, item)._doc : Object.assign({}, item);
            // newItem.memberCommission = Math.abs(item.amount * NumberUtils.convertPercent(item.commission.member.commission));
            // newItem.memberCreditUsage = Math.abs(item.memberCredit);
            // newItem.memberCommPercent = item.commission.member.commission;
            return newItem;
        }

    });
}

router.get('/list', function (req, res) {

    let page = req.query.page ? Number.parseInt(req.query.page) : 10;
    let limit = req.query.limit ? Number.parseInt(req.query.limit) : 1;


    let condition = {};

    let userInfo = req.userInfo;


    if (req.query.active) {
        condition.active = req.query.active;
    }


    if (userInfo.group._id) {
        condition.group = userInfo.group._id;
    }

    if (req.query.username) {
        condition.username_lower = req.query.username.toLowerCase();
    }

    if (req.query.loginName) {
        condition.loginName_lower = req.query.loginName.toLowerCase();
    }

    async.parallel([callback => {
        MemberService.getOutstandingMemberByAgent(userInfo.group._id, function (err, response) {
            if (err) {
                callback(err, '');
            } else {
                callback(null, response);
            }
        });
    }, callback => {

        MemberModel.paginate(condition, {
            // select: "priceType hdp parlay commission amount payout betId createdDate memberId gameType status game source baccarat gameDate ipAddress",
            lean: true,
            page: page,
            limit: limit,
            populate: {path: 'group'},
            sort: {'active': -1, 'username': 1}
        }, function (err, data) {
            if (err) {
                callback(err, '');
            } else {
                callback(null, data);
            }
        });

    }], (err, asyncResponse) => {

        if (err) {
            return res.send({message: "error", result: err, code: 999});
        }

        let memberOutstandingList = asyncResponse[0];
        console.log('memberOutstandingList : ', memberOutstandingList)

        let memberData = asyncResponse[1];

        let list = _.map(memberData.docs, (item) => {
            let newItem = Object.assign({}, item);

            let outStand = _.filter(memberOutstandingList, (outStandItem) => {
                return outStandItem._id.toString() == item._id.toString();
            })[0];

            if (outStand) {
                newItem.totalOutstanding = outStand.amount;
            } else {
                newItem.totalOutstanding = 0;
            }
            newItem.trueBalance = newItem.balance;
            newItem.betCredit = newItem.balance + newItem.creditLimit;
            newItem.balance = newItem.balance + newItem.totalOutstanding;

            return newItem;
        });


        list = _.sortBy(list, function (o) {
            return o.suspend ? 1 : -1;
        });


        let summary = _.reduce(list, function (memo, num) {
            return {
                balance: memo.balance + num.balance,
                creditLimit: memo.creditLimit + num.creditLimit
            }
        }, {balance: 0, creditLimit: 0});


        return res.send(
            {
                code: 0,
                message: "success",
                result: {
                    list: list,
                    summary: summary,
                    total: memberData.total,
                    limit: memberData.limit,
                    page: memberData.page,
                    pages: memberData.pages,
                }
            }
        );

    });

});

router.get('/short_list', function (req, res) {

    let condition = {};

    if (req.query.groupId) {
        condition.group = mongoose.Types.ObjectId(req.query.groupId)
    }
    condition.active = true;


    AgentService.findById(req.query.groupId, (err, agentResponse) => {

        if (agentResponse) {

            MemberModel.findOne({
                username_lower: `${agentResponse.name_lower}mock`,
                group: mongoose.Types.ObjectId(req.query.groupId)
            }).select('username username_lower _id').exec((err, memberMock) => {


                MemberModel.find(condition).select('username username_lower _id').limit(200).exec(function (err, results) {
                    if (err) {
                        return res.send({message: "error", result: err, code: 999});
                    } else {

                        let list = results;
                        if (memberMock) {
                            list.unshift(memberMock)
                        }
                        return res.send(
                            {
                                code: 0,
                                message: "success",
                                result: {
                                    list: list
                                }
                            }
                        );
                    }
                });

            });


        } else {
            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: {
                        list: []
                    }
                }
            );
        }
    });


});

router.get('/user/:userId', function (req, res) {


    MemberModel.findById(req.params.userId, function (err, data) {
        if (err)
            return res.send({message: "error", result: err, code: 999});

        let obj = Object.assign({}, data);
        obj.remainingCredit = roundTo((data.creditLimit + data.balance), 2);

        return res.send(
            {
                code: 0,
                message: "success",
                result: obj
            }
        );
    }).populate({
        'path': 'group',
        'select': 'type'
    }).lean();

});

const addMemberSchema = Joi.object().keys({
    username: Joi.string().required(),
    password: Joi.string().required(),
    contact: Joi.string().allow(null, ''),
    currency: Joi.string().allow(null, ''),
    phoneNo: Joi.string().allow(null, ''),
    creditLimit: Joi.number().required(),
    shareSetting: Joi.object().allow(),
    limitSetting: Joi.object().allow(),
    commissionSetting: Joi.object().allow(),
    settingEnable: Joi.object().allow(),
});

router.post('/', function (req, res) {

    let validate = Joi.validate(req.body, addMemberSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    const userInfo = req.userInfo;

    UserModel.findOne({_id: userInfo._id}, (err, checkAgentResponse) => {


        if (!checkAgentResponse) {

            return res.send({
                message: "UnAuthorization",
                result: err,
                code: 401
            });


        } else {


            let body = {
                username: req.body.username,
                username_lower: req.body.username.toLowerCase(),
                contact: req.body.contact,
                phoneNo: req.body.phoneNo,
                group: userInfo.group._id,
                currency: req.body.currency,
                creditLimit: req.body.creditLimit,
                balance: 0,
                active: true,
                lastPaymentDate: DateUtils.getCurrentDate(),
                shareSetting: {
                    sportsBook: {
                        today: {
                            parent: req.body.shareSetting.sportsBook.today.parent
                        },
                        live: {
                            parent: req.body.shareSetting.sportsBook.live.parent
                        }
                    },
                    step: {
                        parlay: {
                            parent: req.body.shareSetting.step.parlay.parent
                        },
                        step: {
                            parent: req.body.shareSetting.step.step.parent
                        }
                    },
                    casino: {
                        sexy: {
                            parent: req.body.shareSetting.casino.sexy.parent
                        },
                        ag: {
                            parent: req.body.shareSetting.casino.ag.parent
                        },
                        // allbet: {
                        //     parent: req.body.shareSetting.casino.allbet.parent
                        // }
                        sa: {
                            parent: req.body.shareSetting.casino.sa.parent
                        },
                        dg: {
                            parent: req.body.shareSetting.casino.dg.parent
                        },
                        pt: {
                            parent: req.body.shareSetting.casino.pt.parent
                        },
                    },
                    game: {
                        slotXO: {
                            parent: req.body.shareSetting.game.slotXO.parent
                        }
                    },
                    multi: {
                        amb: {
                            parent: req.body.shareSetting.multi.amb.parent
                        }
                    },
                    lotto: {
                        amb: {
                            parent: req.body.shareSetting.lotto.amb.parent
                        },
                        pp: {
                            parent: req.body.shareSetting.lotto.pp.parent
                        },
                        laos: {
                            parent: req.body.shareSetting.lotto.laos.parent
                        }
                    },
                    other: {
                        m2: {
                            parent: req.body.shareSetting.other.m2.parent
                        }
                    }
                },
                limitSetting: {
                    sportsBook: {
                        hdpOuOe: {
                            maxPerBet: req.body.limitSetting.sportsBook.hdpOuOe.maxPerBet,
                            maxPerMatch: req.body.limitSetting.sportsBook.hdpOuOe.maxPerMatch
                        },
                        oneTwoDoubleChance: {
                            maxPerBet: req.body.limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet,
                            // maxPerMatch: req.body.limitSetting.sportsBook.oneTwoDoubleChance.maxPerMatch
                        },
                        others: {
                            maxPerBet: req.body.limitSetting.sportsBook.others.maxPerBet,
                            // maxPerMatch: req.body.limitSetting.sportsBook.others.maxPerMatch
                        },
                        // mixParlay: {
                        //     maxPerBet: req.body.limitSetting.sportsBook.mixParlay.maxPerBet,
                        //     maxPerMatch: req.body.limitSetting.sportsBook.mixParlay.maxPerMatch
                        // },
                        outright: {
                            // maxPerBet: req.body.limitSetting.sportsBook.outright.maxPerBet,
                            maxPerMatch: req.body.limitSetting.sportsBook.outright.maxPerMatch
                        },
                        isEnable: req.body.limitSetting.sportsBook.isEnable
                    },
                    step: {
                        parlay: {
                            maxPerBet: req.body.limitSetting.step.parlay.maxPerBet,
                            minPerBet: req.body.limitSetting.step.parlay.minPerBet,
                            maxBetPerDay: req.body.limitSetting.step.parlay.maxBetPerDay,
                            maxPayPerBill: req.body.limitSetting.step.parlay.maxPayPerBill,
                            maxMatchPerBet: req.body.limitSetting.step.parlay.maxMatchPerBet,
                            minMatchPerBet: req.body.limitSetting.step.parlay.minMatchPerBet,
                            isEnable: req.body.limitSetting.step.parlay.isEnable
                        },
                        step: {
                            maxPerBet: req.body.limitSetting.step.step.maxPerBet,
                            minPerBet: req.body.limitSetting.step.step.minPerBet,
                            maxBetPerDay: req.body.limitSetting.step.step.maxBetPerDay,
                            maxPayPerBill: req.body.limitSetting.step.step.maxPayPerBill,
                            maxMatchPerBet: req.body.limitSetting.step.step.maxMatchPerBet,
                            minMatchPerBet: req.body.limitSetting.step.step.minMatchPerBet,
                            isEnable: req.body.limitSetting.step.step.isEnable
                        }
                    },
                    casino: {
                        isWlEnable: req.body.limitSetting.casino.isWlEnable,
                        winPerDay: req.body.limitSetting.casino.winPerDay,
                        sexy: {
                            isEnable: req.body.limitSetting.casino.sexy.isEnable,
                            limit: req.body.limitSetting.casino.sexy.limit,
                        },
                        ag: {
                            isEnable: req.body.limitSetting.casino.ag.isEnable,
                            limit: req.body.limitSetting.casino.ag.limit,
                        },
                        // allbet: {
                        //     isEnable: req.body.limitSetting.casino.allbet.isEnable,
                        //     limitA: req.body.limitSetting.casino.allbet.limitA,
                        //     limitB: req.body.limitSetting.casino.allbet.limitB,
                        //     limitC: req.body.limitSetting.casino.allbet.limitC,
                        //     limitVip: req.body.limitSetting.casino.allbet.limitVip,
                        // },
                        sa: {
                            isEnable: req.body.limitSetting.casino.sa.isEnable,
                            limit: req.body.limitSetting.casino.sa.limit
                        },
                        dg: {
                            isEnable: req.body.limitSetting.casino.dg.isEnable,
                            limit: req.body.limitSetting.casino.dg.limit
                        },
                        pt: {
                            isEnable: req.body.limitSetting.casino.pt.isEnable,
                            limit: req.body.limitSetting.casino.pt.limit
                        },
                    },
                    game: {
                        slotXO: {
                            isEnable: req.body.limitSetting.game.slotXO.isEnable
                        }
                    },
                    multi: {
                        amb: {
                            isEnable: req.body.limitSetting.multi.amb.isEnable
                        }
                    },
                    other: {
                        m2: {
                            maxPerBetHDP: req.body.limitSetting.other.m2.maxPerBetHDP,
                            minPerBetHDP: req.body.limitSetting.other.m2.minPerBetHDP,
                            maxBetPerMatchHDP: req.body.limitSetting.other.m2.maxBetPerMatchHDP,

                            maxPerBet: req.body.limitSetting.other.m2.maxPerBet,
                            minPerBet: req.body.limitSetting.other.m2.minPerBet,
                            maxBetPerDay: req.body.limitSetting.other.m2.maxBetPerDay,
                            maxPayPerBill: req.body.limitSetting.other.m2.maxPayPerBill,
                            maxMatchPerBet: req.body.limitSetting.other.m2.maxMatchPerBet,
                            minMatchPerBet: req.body.limitSetting.other.m2.minMatchPerBet,
                            isEnable: req.body.limitSetting.other.m2.isEnable
                        }
                    },
                    lotto: {
                        amb: {
                            isEnable: req.body.limitSetting.lotto.amb.isEnable,
                            hour: req.body.limitSetting.lotto.amb.hour,
                            minute: req.body.limitSetting.lotto.amb.minute,
                            _6TOP: {
                                payout: req.body.limitSetting.lotto.amb._6TOP.payout,
                                discount: req.body.limitSetting.lotto.amb._6TOP.discount,
                                max: req.body.limitSetting.lotto.amb._6TOP.max
                            },
                            _5TOP: {
                                payout: req.body.limitSetting.lotto.amb._5TOP.payout,
                                discount: req.body.limitSetting.lotto.amb._5TOP.discount,
                                max: req.body.limitSetting.lotto.amb._5TOP.max
                            },
                            _4TOP: {
                                payout: req.body.limitSetting.lotto.amb._4TOP.payout,
                                discount: req.body.limitSetting.lotto.amb._4TOP.discount,
                                max: req.body.limitSetting.lotto.amb._4TOP.max
                            },
                            _4TOD: {
                                payout: req.body.limitSetting.lotto.amb._4TOD.payout,
                                discount: req.body.limitSetting.lotto.amb._4TOD.discount,
                                max: req.body.limitSetting.lotto.amb._4TOD.max
                            },
                            _3TOP: {
                                payout: req.body.limitSetting.lotto.amb._3TOP.payout,
                                discount: req.body.limitSetting.lotto.amb._3TOP.discount,
                                max: req.body.limitSetting.lotto.amb._3TOP.max
                            },
                            _3TOD: {
                                payout: req.body.limitSetting.lotto.amb._3TOD.payout,
                                discount: req.body.limitSetting.lotto.amb._3TOD.discount,
                                max: req.body.limitSetting.lotto.amb._3TOD.max
                            },
                            _3BOT: {
                                payout: req.body.limitSetting.lotto.amb._3BOT.payout,
                                discount: req.body.limitSetting.lotto.amb._3BOT.discount,
                                max: req.body.limitSetting.lotto.amb._3BOT.max
                            },
                            _3TOP_OE: {
                                payout: req.body.limitSetting.lotto.amb._3TOP_OE.payout,
                                discount: req.body.limitSetting.lotto.amb._3TOP_OE.discount,
                                max: req.body.limitSetting.lotto.amb._3TOP_OE.max
                            },
                            _3TOP_OU: {
                                payout: req.body.limitSetting.lotto.amb._3TOP_OU.payout,
                                discount: req.body.limitSetting.lotto.amb._3TOP_OU.discount,
                                max: req.body.limitSetting.lotto.amb._3TOP_OU.max
                            },
                            _2TOP: {
                                payout: req.body.limitSetting.lotto.amb._2TOP.payout,
                                discount: req.body.limitSetting.lotto.amb._2TOP.discount,
                                max: req.body.limitSetting.lotto.amb._2TOP.max
                            },
                            _2TOD: {
                                payout: req.body.limitSetting.lotto.amb._2TOD.payout,
                                discount: req.body.limitSetting.lotto.amb._2TOD.discount,
                                max: req.body.limitSetting.lotto.amb._2TOD.max
                            },
                            _2BOT: {
                                payout: req.body.limitSetting.lotto.amb._2BOT.payout,
                                discount: req.body.limitSetting.lotto.amb._2BOT.discount,
                                max: req.body.limitSetting.lotto.amb._2BOT.max
                            },
                            _2TOP_OE: {
                                payout: req.body.limitSetting.lotto.amb._2TOP_OE.payout,
                                discount: req.body.limitSetting.lotto.amb._2TOP_OE.discount,
                                max: req.body.limitSetting.lotto.amb._2TOP_OE.max
                            },
                            _2TOP_OU: {
                                payout: req.body.limitSetting.lotto.amb._2TOP_OU.payout,
                                discount: req.body.limitSetting.lotto.amb._2TOP_OU.discount,
                                max: req.body.limitSetting.lotto.amb._2TOP_OU.max
                            },
                            _2BOT_OE: {
                                payout: req.body.limitSetting.lotto.amb._2BOT_OE.payout,
                                discount: req.body.limitSetting.lotto.amb._2BOT_OE.discount,
                                max: req.body.limitSetting.lotto.amb._2BOT_OE.max
                            },
                            _2BOT_OU: {
                                payout: req.body.limitSetting.lotto.amb._2BOT_OU.payout,
                                discount: req.body.limitSetting.lotto.amb._2BOT_OU.discount,
                                max: req.body.limitSetting.lotto.amb._2BOT_OU.max
                            },
                            _1TOP: {
                                payout: req.body.limitSetting.lotto.amb._1TOP.payout,
                                discount: req.body.limitSetting.lotto.amb._1TOP.discount,
                                max: req.body.limitSetting.lotto.amb._1TOP.max
                            },
                            _1BOT: {
                                payout: req.body.limitSetting.lotto.amb._1BOT.payout,
                                discount: req.body.limitSetting.lotto.amb._1BOT.discount,
                                max: req.body.limitSetting.lotto.amb._1BOT.max
                            }
                        },
                        pp: {
                            isEnable: req.body.limitSetting.lotto.pp.isEnable,
                            hour: req.body.limitSetting.lotto.pp.hour,
                            minute: req.body.limitSetting.lotto.pp.minute,
                            _6TOP: {
                                payout: req.body.limitSetting.lotto.pp._6TOP.payout,
                                discount: req.body.limitSetting.lotto.pp._6TOP.discount,
                                max: req.body.limitSetting.lotto.pp._6TOP.max
                            },
                            _5TOP: {
                                payout: req.body.limitSetting.lotto.pp._5TOP.payout,
                                discount: req.body.limitSetting.lotto.pp._5TOP.discount,
                                max: req.body.limitSetting.lotto.pp._5TOP.max
                            },
                            _4TOP: {
                                payout: req.body.limitSetting.lotto.pp._4TOP.payout,
                                discount: req.body.limitSetting.lotto.pp._4TOP.discount,
                                max: req.body.limitSetting.lotto.pp._4TOP.max
                            },
                            _4TOD: {
                                payout: req.body.limitSetting.lotto.pp._4TOD.payout,
                                discount: req.body.limitSetting.lotto.pp._4TOD.discount,
                                max: req.body.limitSetting.lotto.pp._4TOD.max
                            },
                            _3TOP: {
                                payout: req.body.limitSetting.lotto.pp._3TOP.payout,
                                discount: req.body.limitSetting.lotto.pp._3TOP.discount,
                                max: req.body.limitSetting.lotto.pp._3TOP.max
                            },
                            _3TOD: {
                                payout: req.body.limitSetting.lotto.pp._3TOD.payout,
                                discount: req.body.limitSetting.lotto.pp._3TOD.discount,
                                max: req.body.limitSetting.lotto.pp._3TOD.max
                            },
                            _3BOT: {
                                payout: 0,
                                discount: 0,
                                max: 0
                            },
                            _3TOP_OE: {
                                payout: 0,
                                discount: 0,
                                max: 0
                            },
                            _3TOP_OU: {
                                payout: 0,
                                discount: 0,
                                max: 0
                            },
                            _2TOP: {
                                payout: req.body.limitSetting.lotto.pp._2TOP.payout,
                                discount: req.body.limitSetting.lotto.pp._2TOP.discount,
                                max: req.body.limitSetting.lotto.pp._2TOP.max
                            },
                            _2TOD: {
                                payout: req.body.limitSetting.lotto.pp._2TOD.payout,
                                discount: req.body.limitSetting.lotto.pp._2TOD.discount,
                                max: req.body.limitSetting.lotto.pp._2TOD.max
                            },
                            _2BOT: {
                                payout: req.body.limitSetting.lotto.pp._2BOT.payout,
                                discount: req.body.limitSetting.lotto.pp._2BOT.discount,
                                max: req.body.limitSetting.lotto.pp._2BOT.max
                            },
                            _2TOP_OE: {
                                payout: 0,
                                discount: 0,
                                max: 0
                            },
                            _2TOP_OU: {
                                payout: 0,
                                discount: 0,
                                max: 0
                            },
                            _2BOT_OE: {
                                payout: 0,
                                discount: 0,
                                max: 0
                            },
                            _2BOT_OU: {
                                payout: 0,
                                discount: 0,
                                max: 0
                            },
                            _1TOP: {
                                payout: req.body.limitSetting.lotto.pp._1TOP.payout,
                                discount: req.body.limitSetting.lotto.pp._1TOP.discount,
                                max: req.body.limitSetting.lotto.pp._1TOP.max
                            },
                            _1BOT: {
                                payout: req.body.limitSetting.lotto.pp._1BOT.payout,
                                discount: req.body.limitSetting.lotto.pp._1BOT.discount,
                                max: req.body.limitSetting.lotto.pp._1BOT.max
                            }
                        },
                        laos: {
                            isEnable: req.body.limitSetting.lotto.laos.isEnable,
                            hour: req.body.limitSetting.lotto.laos.hour,
                            minute: req.body.limitSetting.lotto.laos.minute,
                            L_4TOP: {
                                payout: req.body.limitSetting.lotto.laos.L_4TOP.payout,
                                discount: req.body.limitSetting.lotto.laos.L_4TOP.discount,
                                max: req.body.limitSetting.lotto.laos.L_4TOP.max
                            },
                            L_4TOD: {
                                payout: req.body.limitSetting.lotto.laos.L_4TOD.payout,
                                discount: req.body.limitSetting.lotto.laos.L_4TOP.discount,
                                max: req.body.limitSetting.lotto.laos.L_4TOP.max
                            },
                            L_3TOP: {
                                payout: req.body.limitSetting.lotto.laos.L_3TOP.payout,
                                discount: req.body.limitSetting.lotto.laos.L_4TOP.discount,
                                max: req.body.limitSetting.lotto.laos.L_4TOP.max
                            },
                            L_3TOD: {
                                payout: req.body.limitSetting.lotto.laos.L_3TOD.payout,
                                discount: req.body.limitSetting.lotto.laos.L_4TOP.discount,
                                max: req.body.limitSetting.lotto.laos.L_4TOP.max
                            },
                            L_2FB: {
                                payout: req.body.limitSetting.lotto.laos.L_2FB.payout,
                                discount: req.body.limitSetting.lotto.laos.L_4TOP.discount,
                                max: req.body.limitSetting.lotto.laos.L_4TOP.max
                            },
                            T_4TOP: {
                                payout: req.body.limitSetting.lotto.laos.T_4TOP.payout,
                                discount: req.body.limitSetting.lotto.laos.T_4TOP.discount,
                                max: req.body.limitSetting.lotto.laos.T_4TOP.max
                            },
                            T_4TOD: {
                                payout: req.body.limitSetting.lotto.laos.T_4TOD.payout,
                                discount: req.body.limitSetting.lotto.laos.T_4TOD.discount,
                                max: req.body.limitSetting.lotto.laos.T_4TOD.max
                            },
                            T_3TOP: {
                                payout: req.body.limitSetting.lotto.laos.T_3TOP.payout,
                                discount: req.body.limitSetting.lotto.laos.T_3TOP.discount,
                                max: req.body.limitSetting.lotto.laos.T_3TOP.max
                            },
                            T_3TOD: {
                                payout: req.body.limitSetting.lotto.laos.T_3TOD.payout,
                                discount: req.body.limitSetting.lotto.laos.T_3TOD.discount,
                                max: req.body.limitSetting.lotto.laos.T_3TOD.max
                            },
                            T_2TOP: {
                                payout: req.body.limitSetting.lotto.laos.T_2TOP.payout,
                                discount: req.body.limitSetting.lotto.laos.T_2TOP.discount,
                                max: req.body.limitSetting.lotto.laos.T_2TOP.max
                            },
                            T_2TOD: {
                                payout: req.body.limitSetting.lotto.laos.T_2TOD.payout,
                                discount: req.body.limitSetting.lotto.laos.T_2TOD.discount,
                                max: req.body.limitSetting.lotto.laos.T_2TOD.max
                            },
                            T_2BOT: {
                                payout: req.body.limitSetting.lotto.laos.T_2BOT.payout,
                                discount: req.body.limitSetting.lotto.laos.T_2BOT.discount,
                                max: req.body.limitSetting.lotto.laos.T_2BOT.max
                            },
                            T_1TOP: {
                                payout: req.body.limitSetting.lotto.laos.T_1TOP.payout,
                                discount: req.body.limitSetting.lotto.laos.T_1TOP.discount,
                                max: req.body.limitSetting.lotto.laos.T_1TOP.max
                            },
                            T_1BOT: {
                                payout: req.body.limitSetting.lotto.laos.T_1BOT.payout,
                                discount: req.body.limitSetting.lotto.laos.T_1BOT.discount,
                                max: req.body.limitSetting.lotto.laos.T_1BOT.max
                            }
                        }
                    }
                },
                commissionSetting: {
                    sportsBook: {
                        typeHdpOuOe: req.body.commissionSetting.sportsBook.typeHdpOuOe,
                        hdpOuOe: req.body.commissionSetting.sportsBook.hdpOuOe,
                        oneTwoDoubleChance: req.body.commissionSetting.sportsBook.oneTwoDoubleChance,
                        others: req.body.commissionSetting.sportsBook.others
                    },
                    parlay: {
                        com: req.body.commissionSetting.parlay.com,
                    },
                    step: {
                        com2: req.body.commissionSetting.step.com2,
                        com3: req.body.commissionSetting.step.com3,
                        com4: req.body.commissionSetting.step.com4,
                        com5: req.body.commissionSetting.step.com5,
                        com6: req.body.commissionSetting.step.com6,
                        com7: req.body.commissionSetting.step.com7,
                        com8: req.body.commissionSetting.step.com8,
                        com9: req.body.commissionSetting.step.com9,
                        com10: req.body.commissionSetting.step.com10,
                        com11: req.body.commissionSetting.step.com11,
                        com12: req.body.commissionSetting.step.com12,
                    },
                    casino: {
                        sexy: req.body.commissionSetting.casino.sexy,
                        ag: req.body.commissionSetting.casino.ag,
                        sa: req.body.commissionSetting.casino.sa,
                        dg: req.body.commissionSetting.casino.dg,
                        pt: req.body.commissionSetting.casino.pt,
                        // allbet: req.body.commissionSetting.casino.allbet
                    },
                    multi: {
                        amb: req.body.commissionSetting.multi.amb,
                    },
                    game: {
                        slotXO: req.body.commissionSetting.game.slotXO,
                    },
                    other: {
                        m2: req.body.commissionSetting.other.m2,
                    }
                },
                configuration: {
                    sportsBook: {
                        locale: "th-TH",
                        odd: "MY",
                        views: "DOUBLE",
                        acceptAnyOdd: false,
                        defaultPrice: {
                            check: false,
                            price: 0
                        },
                        last: false
                    }
                },
            };

            async.series([(callback) => {
                AgentGroupModel.findOne({name_lower: req.body.username.toLowerCase()}, function (err, response) {
                    if (err)
                        callback(err, null);
                    else
                        callback(null, response);

                });
            },
                (callback) => {
                    MemberModel.findOne({username_lower: req.body.username.toLowerCase()}, function (err, response) {
                        if (err)
                            callback(err, null);
                        else
                            callback(null, response);

                    });
                },
                (callback => {
                    AgentGroupModel.findById(userInfo.group._id, function (err, response) {
                        if (err)
                            callback(err, null);
                        else
                            callback(null, response);
                    });
                }),

                (callback) => {
                    AgentService.getCreditLimitUsage(userInfo.group._id, function (err, response) {
                        if (err) {
                            callback(err, '');
                        } else {
                            callback(null, response ? response : 0);
                        }
                    });
                },
                (callback) => {
                    if (_.isEqual('AMB_SPORTBOOK', CLIENT_NAME) ||
                        _.isEqual('SPORTBOOK88', CLIENT_NAME) ||
                        _.isEqual('MARDUO', CLIENT_NAME)) {
                        MemberService.isAutoChangeOddAgent(userInfo.group._id, (error, isAutoChangeOddAgent) => {
                            if (error) {
                                callback(null, false);
                            } else {
                                callback(null, isAutoChangeOddAgent);
                            }
                        });
                    } else {
                        callback(null, false);
                    }
                }

            ], (err, results) => {
                if (err) {
                    return res.send({message: "error", result: err, code: 999});
                }

                if (results[0] || results[1]) {
                    return res.send({message: "name was already used", result: err, code: 4001});
                }

                //check creditLimit
                let currentAgentInfo = results[2];
                let creditLimit = results[2].creditLimit;
                const creditLimitUsage = results[3];

                if (_.isEqual('AMB_SPORTBOOK', CLIENT_NAME) ||
                    _.isEqual('SPORTBOOK88', CLIENT_NAME) ||
                    _.isEqual('MARDUO', CLIENT_NAME)) {
                    body.isAutoChangeOdd = results[4];
                    console.log('isAutoChangeOdd => ', body.isAutoChangeOdd);
                }

                console.log('own credit : ', creditLimit);
                console.log('creditLimit : ' + req.body.creditLimit + ' , creditLimitUsage : ' + creditLimitUsage);


                if (req.body.creditLimit > (creditLimit - creditLimitUsage)) {
                    return res.send({message: "creditLimit exceeded", result: err, code: 4002});
                }

                if (req.body.creditLimit > currentAgentInfo.maxCreditLimit) {
                    return res.send({message: "max creditLimit limit", result: err, code: 4030});
                }

                Hashing.encrypt256(req.body.password, function (err, hashingPass) {
                    body.password = hashingPass;
                });

                if (body.limitSetting.sportsBook.hdpOuOe.maxPerBet > currentAgentInfo.limitSetting.sportsBook.hdpOuOe.maxPerBet) {
                    body.limitSetting.sportsBook.hdpOuOe.maxPerBet = currentAgentInfo.limitSetting.sportsBook.hdpOuOe.maxPerBet;
                }

                if (body.limitSetting.sportsBook.hdpOuOe.maxPerMatch > currentAgentInfo.limitSetting.sportsBook.hdpOuOe.maxPerMatch) {
                    body.limitSetting.sportsBook.hdpOuOe.maxPerMatch = currentAgentInfo.limitSetting.sportsBook.hdpOuOe.maxPerMatch
                }
                if (body.limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet > currentAgentInfo.limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet) {
                    body.limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet = currentAgentInfo.limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet
                }
                if (body.limitSetting.sportsBook.others.maxPerBet > currentAgentInfo.limitSetting.sportsBook.others.maxPerBet) {
                    body.limitSetting.sportsBook.others.maxPerBet = currentAgentInfo.limitSetting.sportsBook.others.maxPerBet
                }
                // if (body.limitSetting.sportsBook.mixParlay.maxPerBet > currentAgentInfo.limitSetting.sportsBook.mixParlay.maxPerBet) {
                //     body.limitSetting.sportsBook.mixParlay.maxPerBet = currentAgentInfo.limitSetting.sportsBook.mixParlay.maxPerBet
                // }
                // if (body.limitSetting.sportsBook.mixParlay.maxPerMatch > currentAgentInfo.limitSetting.sportsBook.mixParlay.maxPerMatch) {
                //     body.limitSetting.sportsBook.mixParlay.maxPerMatch = currentAgentInfo.limitSetting.sportsBook.mixParlay.maxPerMatch
                // }

                if (body.limitSetting.sportsBook.outright.maxPerMatch > currentAgentInfo.limitSetting.sportsBook.outright.maxPerMatch) {
                    body.limitSetting.sportsBook.outright.maxPerMatch = currentAgentInfo.limitSetting.sportsBook.outright.maxPerMatch
                }


                //PARLAY
                if (body.limitSetting.step.parlay.maxPerBet > currentAgentInfo.limitSetting.step.parlay.maxPerBet) {
                    body.limitSetting.step.parlay.maxPerBet = currentAgentInfo.limitSetting.step.parlay.maxPerBet
                }
                if (body.limitSetting.step.parlay.minPerBet < currentAgentInfo.limitSetting.step.parlay.minPerBet) {
                    body.limitSetting.step.parlay.minPerBet = currentAgentInfo.limitSetting.step.parlay.minPerBet
                }
                if (body.limitSetting.step.parlay.maxBetPerDay > currentAgentInfo.limitSetting.step.parlay.maxBetPerDay) {
                    body.limitSetting.step.parlay.maxBetPerDay = currentAgentInfo.limitSetting.step.parlay.maxBetPerDay
                }
                if (body.limitSetting.step.parlay.maxPayPerBill > currentAgentInfo.limitSetting.step.parlay.maxPayPerBill) {
                    body.limitSetting.step.parlay.maxPayPerBill = currentAgentInfo.limitSetting.step.parlay.maxPayPerBill
                }

                //STEP
                if (body.limitSetting.step.step.maxPerBet > currentAgentInfo.limitSetting.step.step.maxPerBet) {
                    body.limitSetting.step.step.maxPerBet = currentAgentInfo.limitSetting.step.step.maxPerBet
                }
                if (body.limitSetting.step.step.minPerBet < currentAgentInfo.limitSetting.step.step.minPerBet) {
                    body.limitSetting.step.step.minPerBet = currentAgentInfo.limitSetting.step.step.minPerBet
                }
                if (body.limitSetting.step.step.maxBetPerDay > currentAgentInfo.limitSetting.step.step.maxBetPerDay) {
                    body.limitSetting.step.step.maxBetPerDay = currentAgentInfo.limitSetting.step.step.maxBetPerDay
                }
                if (body.limitSetting.step.step.maxPayPerBill > currentAgentInfo.limitSetting.step.step.maxPayPerBill) {
                    body.limitSetting.step.step.maxPayPerBill = currentAgentInfo.limitSetting.step.step.maxPayPerBill
                }

                //TODO
                if (body.limitSetting.lotto.amb._6TOP.max > currentAgentInfo.limitSetting.lotto.amb._6TOP.max) {
                    body.limitSetting.lotto.amb._6TOP.max = currentAgentInfo.limitSetting.lotto.amb._6TOP.max
                }

                if (body.limitSetting.lotto.amb._5TOP.max > currentAgentInfo.limitSetting.lotto.amb._5TOP.max) {
                    body.limitSetting.lotto.amb._5TOP.max = currentAgentInfo.limitSetting.lotto.amb._5TOP.max
                }

                if (body.limitSetting.lotto.amb._4TOP.max > currentAgentInfo.limitSetting.lotto.amb._4TOP.max) {
                    body.limitSetting.lotto.amb._4TOP.max = currentAgentInfo.limitSetting.lotto.amb._4TOP.max
                }

                if (body.limitSetting.lotto.amb._4TOD.max > currentAgentInfo.limitSetting.lotto.amb._4TOD.max) {
                    body.limitSetting.lotto.amb._4TOD.max = currentAgentInfo.limitSetting.lotto.amb._4TOD.max
                }

                if (body.limitSetting.lotto.amb._3TOP.max > currentAgentInfo.limitSetting.lotto.amb._3TOP.max) {
                    body.limitSetting.lotto.amb._3TOP.max = currentAgentInfo.limitSetting.lotto.amb._3TOP.max
                }

                if (body.limitSetting.lotto.amb._3TOD.max > currentAgentInfo.limitSetting.lotto.amb._3TOD.max) {
                    body.limitSetting.lotto.amb._3TOD.max = currentAgentInfo.limitSetting.lotto.amb._3TOD.max
                }

                if (body.limitSetting.lotto.amb._3BOT.max > currentAgentInfo.limitSetting.lotto.amb._3BOT.max) {
                    body.limitSetting.lotto.amb._3BOT.max = currentAgentInfo.limitSetting.lotto.amb._3BOT.max
                }

                if (body.limitSetting.lotto.amb._3TOP_OE.max > currentAgentInfo.limitSetting.lotto.amb._3TOP_OE.max) {
                    body.limitSetting.lotto.amb._3TOP_OE.max = currentAgentInfo.limitSetting.lotto.amb._3TOP_OE.max
                }

                if (body.limitSetting.lotto.amb._3TOP_OU.max > currentAgentInfo.limitSetting.lotto.amb._3TOP_OU.max) {
                    body.limitSetting.lotto.amb._3TOP_OU.max = currentAgentInfo.limitSetting.lotto.amb._3TOP_OU.max
                }

                if (body.limitSetting.lotto.amb._2TOP.max > currentAgentInfo.limitSetting.lotto.amb._2TOP.max) {
                    body.limitSetting.lotto.amb._2TOP.max = currentAgentInfo.limitSetting.lotto.amb._2TOP.max
                }

                if (body.limitSetting.lotto.amb._2TOD.max > currentAgentInfo.limitSetting.lotto.amb._2TOD.max) {
                    body.limitSetting.lotto.amb._2TOD.max = currentAgentInfo.limitSetting.lotto.amb._2TOD.max
                }

                if (body.limitSetting.lotto.amb._2BOT.max > currentAgentInfo.limitSetting.lotto.amb._2BOT.max) {
                    body.limitSetting.lotto.amb._2BOT.max = currentAgentInfo.limitSetting.lotto.amb._2BOT.max
                }

                if (body.limitSetting.lotto.amb._2TOP_OE.max > currentAgentInfo.limitSetting.lotto.amb._2TOP_OE.max) {
                    body.limitSetting.lotto.amb._2TOP_OE.max = currentAgentInfo.limitSetting.lotto.amb._2TOP_OE.max
                }

                if (body.limitSetting.lotto.amb._2TOP_OU.max > currentAgentInfo.limitSetting.lotto.amb._2TOP_OU.max) {
                    body.limitSetting.lotto.amb._2TOP_OU.max = currentAgentInfo.limitSetting.lotto.amb._2TOP_OU.max
                }


                if (body.limitSetting.lotto.amb._2BOT_OE.max > currentAgentInfo.limitSetting.lotto.amb._2BOT_OE.max) {
                    body.limitSetting.lotto.amb._2BOT_OE.max = currentAgentInfo.limitSetting.lotto.amb._2BOT_OE.max
                }

                if (body.limitSetting.lotto.amb._2BOT_OU.max > currentAgentInfo.limitSetting.lotto.amb._2BOT_OU.max) {
                    body.limitSetting.lotto.amb._2BOT_OU.max = currentAgentInfo.limitSetting.lotto.amb._2BOT_OU.max
                }


                if (body.limitSetting.lotto.amb._1TOP.max > currentAgentInfo.limitSetting.lotto.amb._1TOP.max) {
                    body.limitSetting.lotto.amb._1TOP.max = currentAgentInfo.limitSetting.lotto.amb._1TOP.max
                }

                if (body.limitSetting.lotto.amb._1BOT.max > currentAgentInfo.limitSetting.lotto.amb._1BOT.max) {
                    body.limitSetting.lotto.amb._1BOT.max = currentAgentInfo.limitSetting.lotto.amb._1BOT.max
                }


                //PP
                if (body.limitSetting.lotto.pp._6TOP.max > currentAgentInfo.limitSetting.lotto.pp._6TOP.max) {
                    body.limitSetting.lotto.pp._6TOP.max = currentAgentInfo.limitSetting.lotto.pp._6TOP.max
                }

                if (body.limitSetting.lotto.pp._5TOP.max > currentAgentInfo.limitSetting.lotto.pp._5TOP.max) {
                    body.limitSetting.lotto.pp._5TOP.max = currentAgentInfo.limitSetting.lotto.pp._5TOP.max
                }

                if (body.limitSetting.lotto.pp._4TOP.max > currentAgentInfo.limitSetting.lotto.pp._4TOP.max) {
                    body.limitSetting.lotto.pp._4TOP.max = currentAgentInfo.limitSetting.lotto.pp._4TOP.max
                }

                if (body.limitSetting.lotto.pp._4TOD.max > currentAgentInfo.limitSetting.lotto.pp._4TOD.max) {
                    body.limitSetting.lotto.pp._4TOD.max = currentAgentInfo.limitSetting.lotto.pp._4TOD.max
                }

                if (body.limitSetting.lotto.pp._3TOP.max > currentAgentInfo.limitSetting.lotto.pp._3TOP.max) {
                    body.limitSetting.lotto.pp._3TOP.max = currentAgentInfo.limitSetting.lotto.pp._3TOP.max
                }

                if (body.limitSetting.lotto.pp._3TOD.max > currentAgentInfo.limitSetting.lotto.pp._3TOD.max) {
                    body.limitSetting.lotto.pp._3TOD.max = currentAgentInfo.limitSetting.lotto.pp._3TOD.max
                }

                if (body.limitSetting.lotto.pp._2TOP.max > currentAgentInfo.limitSetting.lotto.pp._2TOP.max) {
                    body.limitSetting.lotto.pp._2TOP.max = currentAgentInfo.limitSetting.lotto.pp._2TOP.max
                }

                if (body.limitSetting.lotto.pp._2TOD.max > currentAgentInfo.limitSetting.lotto.pp._2TOD.max) {
                    body.limitSetting.lotto.pp._2TOD.max = currentAgentInfo.limitSetting.lotto.pp._2TOD.max
                }

                if (body.limitSetting.lotto.pp._2BOT.max > currentAgentInfo.limitSetting.lotto.pp._2BOT.max) {
                    body.limitSetting.lotto.pp._2BOT.max = currentAgentInfo.limitSetting.lotto.pp._2BOT.max
                }

                if (body.limitSetting.lotto.pp._1TOP.max > currentAgentInfo.limitSetting.lotto.pp._1TOP.max) {
                    body.limitSetting.lotto.pp._1TOP.max = currentAgentInfo.limitSetting.lotto.pp._1TOP.max
                }

                if (body.limitSetting.lotto.pp._1BOT.max > currentAgentInfo.limitSetting.lotto.pp._1BOT.max) {
                    body.limitSetting.lotto.pp._1BOT.max = currentAgentInfo.limitSetting.lotto.pp._1BOT.max
                }

                //OTHER

                if (body.limitSetting.other.m2.maxPerBetHDP > currentAgentInfo.limitSetting.other.m2.maxPerBetHDP) {
                    body.limitSetting.other.m2.maxPerBetHDP = currentAgentInfo.limitSetting.other.m2.maxPerBetHDP
                }
                if (body.limitSetting.other.m2.minPerBetHDP < currentAgentInfo.limitSetting.other.m2.minPerBetHDP) {
                    body.limitSetting.other.m2.minPerBetHDP = currentAgentInfo.limitSetting.other.m2.minPerBetHDP
                }
                if (body.limitSetting.other.m2.maxBetPerMatchHDP > currentAgentInfo.limitSetting.other.m2.maxBetPerMatchHDP) {
                    body.limitSetting.other.m2.maxBetPerMatchHDP = currentAgentInfo.limitSetting.other.m2.maxBetPerMatchHDP
                }


                if (body.limitSetting.other.m2.maxPerBet > currentAgentInfo.limitSetting.other.m2.maxPerBet) {
                    body.limitSetting.other.m2.maxPerBet = currentAgentInfo.limitSetting.other.m2.maxPerBet
                }
                if (body.limitSetting.other.m2.minPerBet < currentAgentInfo.limitSetting.other.m2.minPerBet) {
                    body.limitSetting.other.m2.minPerBet = currentAgentInfo.limitSetting.other.m2.minPerBet
                }
                if (body.limitSetting.other.m2.maxBetPerDay > currentAgentInfo.limitSetting.other.m2.maxBetPerDay) {
                    body.limitSetting.other.m2.maxBetPerDay = currentAgentInfo.limitSetting.other.m2.maxBetPerDay
                }
                if (body.limitSetting.other.m2.maxPayPerBill > currentAgentInfo.limitSetting.other.m2.maxPayPerBill) {
                    body.limitSetting.other.m2.maxPayPerBill = currentAgentInfo.limitSetting.other.m2.maxPayPerBill
                }

                // if (req.body.shareSetting.lotto.amb.parent != currentAgentInfo.shareSetting.lotto.amb.own) {
                //     return res.send({
                //         message: "Agent Share (Amb Lotto) + Agent Max Share (Amb Lotto) should be greater than Agent Min Shares (Amb Lotto)",
                //         result: err,
                //         code: 4010
                //     });
                // }

                if (req.body.settingEnable.sportBookEnable) {
                    if ((req.body.shareSetting.sportsBook.today.parent ) < currentAgentInfo.shareSetting.sportsBook.today.min) {
                        return res.send({
                            message: "Agent Share (Sport Today) + Agent Max Share (Sport Today) should be greater than Agent Min Shares (Sport Today)",
                            result: err,
                            code: 4003
                        });
                    }

                    if ((req.body.shareSetting.sportsBook.live.parent ) < currentAgentInfo.shareSetting.sportsBook.live.min) {
                        return res.send({
                            message: "Agent Share (Sport Live) + Agent Max Share (Sport Live) should be greater than Agent Min Shares (Sport Live)",
                            result: err,
                            code: 4004
                        });
                    }

                }


                if (req.body.settingEnable.parlayEnable) {
                    if ((req.body.shareSetting.step.parlay.parent ) < currentAgentInfo.shareSetting.step.parlay.min) {
                        return res.send({
                            message: "Agent Share (Step Parlay) + Agent Max Share (Step Parlay) should be greater than Agent Min Shares (Step Parlay)",
                            result: err,
                            code: 4005
                        });
                    }
                }

                if (req.body.settingEnable.casinoSexyEnable) {
                    if ((req.body.shareSetting.casino.sexy.parent ) < currentAgentInfo.shareSetting.casino.sexy.min) {
                        return res.send({
                            message: "Agent Share (Sexy Baccarat) + Agent Max Share (Sexy Baccarat) should be greater than Agent Min Shares (Sexy Baccarat)",
                            result: err,
                            code: 4006
                        });
                    }
                }

                if (req.body.settingEnable.otherM2Enable) {
                    if ((req.body.shareSetting.other.m2.parent ) < currentAgentInfo.shareSetting.other.m2.min) {
                        return res.send({
                            message: "Agent Share (M2 Sport) + Agent Max Share (M2 Sport) should be greater than Agent Min Shares (M2 Sport)",
                            result: err,
                            code: 4007
                        });
                    }
                }

                if (req.body.settingEnable.stepEnable) {
                    if ((req.body.shareSetting.step.step.parent ) < currentAgentInfo.shareSetting.step.step.min) {
                        return res.send({
                            message: "Agent Share (Mix Step) + Agent Max Share (Mix Step) should be greater than Agent Min Shares (Mix Step)",
                            result: err,
                            code: 4008
                        });
                    }
                }

                // if ((req.body.shareSetting.casino.allbet.own + req.body.shareSetting.casino.allbet.parent ) < currentAgentInfo.shareSetting.casino.allbet.min) {
                //     return res.send({
                //         message: "Agent Share (All Bet) + Agent Max Share (All Bet) should be greater than Agent Min Shares (All Bet)",
                //         result: err,
                //         code: 4008
                //     });
                // }

                if (req.body.settingEnable.gameSlotEnable) {
                    if ((req.body.shareSetting.game.slotXO.parent ) < currentAgentInfo.shareSetting.game.slotXO.min) {
                        return res.send({
                            message: "Agent Share (Slot XO) + Agent Max Share (Slot XO) should be greater than Agent Min Shares (Slot XO)",
                            result: err,
                            code: 4009
                        });
                    }
                }

                if (req.body.settingEnable.casinoSaEnable) {
                    if ((req.body.shareSetting.casino.sa.parent ) < currentAgentInfo.shareSetting.casino.sa.min) {
                        return res.send({
                            message: "Agent Share (SA Gaming) + Agent Max Share (SA Gaming) should be greater than Agent Min Shares (SA Gaming)",
                            result: err,
                            code: 4011
                        });
                    }
                }


                if (req.body.settingEnable.casinoDgEnable) {
                    if ((req.body.shareSetting.casino.dg.parent ) < currentAgentInfo.shareSetting.casino.dg.min) {
                        return res.send({
                            message: "Agent Share (DG Gaming) + Agent Max Share (DG Gaming) should be greater than Agent Min Shares (DG Gaming)",
                            result: err,
                            code: 4012
                        });
                    }
                }


                if (req.body.settingEnable.casinoAgEnable) {
                    if ((req.body.shareSetting.casino.ag.parent ) < currentAgentInfo.shareSetting.casino.ag.min) {
                        return res.send({
                            message: "Agent Share (AG Gaming) + Agent Max Share (AG Gaming) should be greater than Agent Min Shares (AG Gaming)",
                            result: err,
                            code: 4013
                        });
                    }
                }

                if (req.body.settingEnable.gameCardEnable) {
                    if ((req.body.shareSetting.multi.amb.parent ) < currentAgentInfo.shareSetting.multi.amb.min) {
                        return res.send({
                            message: "Agent Share (AMB Game) + Agent Max Share (AMB Game) should be greater than Agent Min Shares (AMB Game)",
                            result: err,
                            code: 4014
                        });
                    }
                }

                if (req.body.settingEnable.casinoPtEnable) {
                    if ((req.body.shareSetting.casino.pt.parent ) < currentAgentInfo.shareSetting.casino.pt.min) {
                        return res.send({
                            message: "Agent Share (Pretty Gaming) + Agent Max Share (Pretty Gaming) should be greater than Agent Min Shares (Pretty Gaming)",
                            result: err,
                            code: 4015
                        });
                    }
                }

                // if ((req.body.shareSetting.lotto.amb.parent ) < currentAgentInfo.shareSetting.lotto.amb.own) {
                //     return res.send({
                //         message: "Agent Share (Amb Lotto) + Agent Max Share (Amb Lotto) should be greater than Agent Min Shares (Amb Lotto)",
                //         result: err,
                //         code: 4010
                //     });
                // }

                let model = new MemberModel(body);

                model.save(function (err, memberResponse) {

                    if (err) {
                        console.log(err)
                        return res.send({message: "error", results: err, code: 999});
                    }

                    AgentGroupModel.update(
                        {_id: userInfo.group._id},
                        {$push: {childMembers: memberResponse._id}}, function (err, data) {
                            if (err) {
                                return res.send({message: "error", results: err, code: 999});
                            } else {
                                console.log('update parent child success');
                                return res.send(
                                    {
                                        code: 0,
                                        message: "success",
                                        result: memberResponse
                                    }
                                );
                            }
                        });
                });
            });
        }
    });

});


const updateMemberSchema = Joi.object().keys({
    password: Joi.string().allow(null, ''),
    contact: Joi.string().allow(null, ''),
    phoneNo: Joi.string().allow(null, ''),
    creditLimit: Joi.number().required(),
    shareSetting: Joi.object().allow(),
    limitSetting: Joi.object().allow(),
    commissionSetting: Joi.object().allow(),
    settingEnable: Joi.object().allow(),
    creditModify: Joi.boolean().allow()
});

router.put('/update/:memberId', function (req, res) {

    let validate = Joi.validate(req.body, updateMemberSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    const userInfo = req.userInfo;

    UserModel.findOne({_id: userInfo._id}, (err, checkAgentResponse) => {


        if (!checkAgentResponse) {

            return res.send({
                message: "UnAuthorization",
                result: err,
                code: 401
            });


        } else {

            AgentService.isAllowPermission(userInfo._id, req.params.memberId, (err, isAllow) => {
                if (!isAllow) {
                    return res.send({
                        message: "UnAuthorization",
                        result: err,
                        code: 401
                    });
                } else {

                    let body = {
                        contact: req.body.contact,
                        phoneNo: req.body.phoneNo,
                        creditLimit: req.body.creditLimit,
                        shareSetting: {
                            sportsBook: {
                                today: {
                                    parent: req.body.shareSetting.sportsBook.today.parent
                                },
                                live: {
                                    parent: req.body.shareSetting.sportsBook.live.parent
                                }
                            },
                            step: {
                                parlay: {
                                    parent: req.body.shareSetting.step.parlay.parent
                                },
                                step: {
                                    parent: req.body.shareSetting.step.step.parent
                                }
                            },
                            casino: {
                                sexy: {
                                    parent: req.body.shareSetting.casino.sexy.parent
                                },
                                ag: {
                                    parent: req.body.shareSetting.casino.ag.parent
                                },
                                // allbet: {
                                //     parent: req.body.shareSetting.casino.allbet.parent
                                // }
                                sa: {
                                    parent: req.body.shareSetting.casino.sa.parent
                                },
                                dg: {
                                    parent: req.body.shareSetting.casino.dg.parent
                                },
                                pt: {
                                    parent: req.body.shareSetting.casino.pt.parent
                                },
                            },
                            multi: {
                                amb: {
                                    parent: req.body.shareSetting.multi.amb.parent
                                }
                            },
                            game: {
                                slotXO: {
                                    parent: req.body.shareSetting.game.slotXO.parent
                                }
                            },
                            lotto: {
                                amb: {
                                    parent: req.body.shareSetting.lotto.amb.parent
                                },
                                pp: {
                                    parent: req.body.shareSetting.lotto.pp.parent
                                },
                                laos: {
                                    parent: req.body.shareSetting.lotto.laos.parent
                                }
                            },
                            other: {
                                m2: {
                                    parent: req.body.shareSetting.other.m2.parent
                                }
                            }
                        },
                        limitSetting: {
                            sportsBook: {
                                hdpOuOe: {
                                    maxPerBet: req.body.limitSetting.sportsBook.hdpOuOe.maxPerBet,
                                    maxPerMatch: req.body.limitSetting.sportsBook.hdpOuOe.maxPerMatch
                                },
                                oneTwoDoubleChance: {
                                    maxPerBet: req.body.limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet,
                                    // maxPerMatch: req.body.limitSetting.sportsBook.oneTwoDoubleChance.maxPerMatch
                                },
                                others: {
                                    maxPerBet: req.body.limitSetting.sportsBook.others.maxPerBet,
                                    // maxPerMatch: req.body.limitSetting.sportsBook.others.maxPerMatch
                                },
                                outright: {
                                    // maxPerBet: req.body.limitSetting.sportsBook.outright.maxPerBet,
                                    maxPerMatch: req.body.limitSetting.sportsBook.outright.maxPerMatch
                                },
                                isEnable: req.body.limitSetting.sportsBook.isEnable
                            },
                            step: {
                                parlay: {
                                    maxPerBet: req.body.limitSetting.step.parlay.maxPerBet,
                                    minPerBet: req.body.limitSetting.step.parlay.minPerBet,
                                    maxBetPerDay: req.body.limitSetting.step.parlay.maxBetPerDay,
                                    maxPayPerBill: req.body.limitSetting.step.parlay.maxPayPerBill,
                                    maxMatchPerBet: req.body.limitSetting.step.parlay.maxMatchPerBet,
                                    minMatchPerBet: req.body.limitSetting.step.parlay.minMatchPerBet,
                                    isEnable: req.body.limitSetting.step.parlay.isEnable
                                },
                                step: {
                                    maxPerBet: req.body.limitSetting.step.step.maxPerBet,
                                    minPerBet: req.body.limitSetting.step.step.minPerBet,
                                    maxBetPerDay: req.body.limitSetting.step.step.maxBetPerDay,
                                    maxPayPerBill: req.body.limitSetting.step.step.maxPayPerBill,
                                    maxMatchPerBet: req.body.limitSetting.step.step.maxMatchPerBet,
                                    minMatchPerBet: req.body.limitSetting.step.step.minMatchPerBet,
                                    isEnable: req.body.limitSetting.step.step.isEnable
                                }
                            },
                            casino: {
                                isWlEnable: req.body.limitSetting.casino.isWlEnable,
                                winPerDay: req.body.limitSetting.casino.winPerDay,
                                sexy: {
                                    isEnable: req.body.limitSetting.casino.sexy.isEnable,
                                    limit: req.body.limitSetting.casino.sexy.limit,
                                },
                                ag: {
                                    isEnable: req.body.limitSetting.casino.ag.isEnable,
                                    limit: req.body.limitSetting.casino.ag.limit,
                                },
                                // allbet: {
                                //     isEnable: req.body.limitSetting.casino.allbet.isEnable,
                                //     limitA: req.body.limitSetting.casino.allbet.limitA,
                                //     limitB: req.body.limitSetting.casino.allbet.limitB,
                                //     limitC: req.body.limitSetting.casino.allbet.limitC,
                                //     limitVip: req.body.limitSetting.casino.allbet.limitVip,
                                // },
                                sa: {
                                    isEnable: req.body.limitSetting.casino.sa.isEnable,
                                    limit: req.body.limitSetting.casino.sa.limit
                                },
                                dg: {
                                    isEnable: req.body.limitSetting.casino.dg.isEnable,
                                    limit: req.body.limitSetting.casino.dg.limit
                                },
                                pt: {
                                    isEnable: req.body.limitSetting.casino.pt.isEnable,
                                    limit: req.body.limitSetting.casino.pt.limit
                                },
                            },
                            multi: {
                                amb: {
                                    isEnable: req.body.limitSetting.multi.amb.isEnable
                                }
                            },
                            game: {
                                slotXO: {
                                    isEnable: req.body.limitSetting.game.slotXO.isEnable
                                }
                            },
                            other: {
                                m2: {
                                    maxPerBetHDP: req.body.limitSetting.other.m2.maxPerBetHDP,
                                    minPerBetHDP: req.body.limitSetting.other.m2.minPerBetHDP,
                                    maxBetPerMatchHDP: req.body.limitSetting.other.m2.maxBetPerMatchHDP,

                                    maxPerBet: req.body.limitSetting.other.m2.maxPerBet,
                                    minPerBet: req.body.limitSetting.other.m2.minPerBet,
                                    maxBetPerDay: req.body.limitSetting.other.m2.maxBetPerDay,
                                    maxPayPerBill: req.body.limitSetting.other.m2.maxPayPerBill,
                                    maxMatchPerBet: req.body.limitSetting.other.m2.maxMatchPerBet,
                                    minMatchPerBet: req.body.limitSetting.other.m2.minMatchPerBet,
                                    isEnable: req.body.limitSetting.other.m2.isEnable
                                }
                            },
                            lotto: {
                                amb: {
                                    isEnable: req.body.limitSetting.lotto.amb.isEnable,
                                    hour: req.body.limitSetting.lotto.amb.hour,
                                    minute: req.body.limitSetting.lotto.amb.minute,
                                    _6TOP: {
                                        payout: req.body.limitSetting.lotto.amb._6TOP.payout,
                                        discount: req.body.limitSetting.lotto.amb._6TOP.discount,
                                        max: req.body.limitSetting.lotto.amb._6TOP.max
                                    },
                                    _5TOP: {
                                        payout: req.body.limitSetting.lotto.amb._5TOP.payout,
                                        discount: req.body.limitSetting.lotto.amb._5TOP.discount,
                                        max: req.body.limitSetting.lotto.amb._5TOP.max
                                    },
                                    _4TOP: {
                                        payout: req.body.limitSetting.lotto.amb._4TOP.payout,
                                        discount: req.body.limitSetting.lotto.amb._4TOP.discount,
                                        max: req.body.limitSetting.lotto.amb._4TOP.max
                                    },
                                    _4TOD: {
                                        payout: req.body.limitSetting.lotto.amb._4TOD.payout,
                                        discount: req.body.limitSetting.lotto.amb._4TOD.discount,
                                        max: req.body.limitSetting.lotto.amb._4TOD.max
                                    },
                                    _3TOP: {
                                        payout: req.body.limitSetting.lotto.amb._3TOP.payout,
                                        discount: req.body.limitSetting.lotto.amb._3TOP.discount,
                                        max: req.body.limitSetting.lotto.amb._3TOP.max
                                    },
                                    _3TOD: {
                                        payout: req.body.limitSetting.lotto.amb._3TOD.payout,
                                        discount: req.body.limitSetting.lotto.amb._3TOD.discount,
                                        max: req.body.limitSetting.lotto.amb._3TOD.max
                                    },
                                    _3BOT: {
                                        payout: req.body.limitSetting.lotto.amb._3BOT.payout,
                                        discount: req.body.limitSetting.lotto.amb._3BOT.discount,
                                        max: req.body.limitSetting.lotto.amb._3BOT.max
                                    },
                                    _3TOP_OE: {
                                        payout: req.body.limitSetting.lotto.amb._3TOP_OE.payout,
                                        discount: req.body.limitSetting.lotto.amb._3TOP_OE.discount,
                                        max: req.body.limitSetting.lotto.amb._3TOP_OE.max
                                    },
                                    _3TOP_OU: {
                                        payout: req.body.limitSetting.lotto.amb._3TOP_OU.payout,
                                        discount: req.body.limitSetting.lotto.amb._3TOP_OU.discount,
                                        max: req.body.limitSetting.lotto.amb._3TOP_OU.max
                                    },
                                    _2TOP: {
                                        payout: req.body.limitSetting.lotto.amb._2TOP.payout,
                                        discount: req.body.limitSetting.lotto.amb._2TOP.discount,
                                        max: req.body.limitSetting.lotto.amb._2TOP.max
                                    },
                                    _2TOD: {
                                        payout: req.body.limitSetting.lotto.amb._2TOD.payout,
                                        discount: req.body.limitSetting.lotto.amb._2TOD.discount,
                                        max: req.body.limitSetting.lotto.amb._2TOD.max
                                    },
                                    _2BOT: {
                                        payout: req.body.limitSetting.lotto.amb._2BOT.payout,
                                        discount: req.body.limitSetting.lotto.amb._2BOT.discount,
                                        max: req.body.limitSetting.lotto.amb._2BOT.max
                                    },
                                    _2TOP_OE: {
                                        payout: req.body.limitSetting.lotto.amb._2TOP_OE.payout,
                                        discount: req.body.limitSetting.lotto.amb._2TOP_OE.discount,
                                        max: req.body.limitSetting.lotto.amb._2TOP_OE.max
                                    },
                                    _2TOP_OU: {
                                        payout: req.body.limitSetting.lotto.amb._2TOP_OU.payout,
                                        discount: req.body.limitSetting.lotto.amb._2TOP_OU.discount,
                                        max: req.body.limitSetting.lotto.amb._2TOP_OU.max
                                    },
                                    _2BOT_OE: {
                                        payout: req.body.limitSetting.lotto.amb._2BOT_OE.payout,
                                        discount: req.body.limitSetting.lotto.amb._2BOT_OE.discount,
                                        max: req.body.limitSetting.lotto.amb._2BOT_OE.max
                                    },
                                    _2BOT_OU: {
                                        payout: req.body.limitSetting.lotto.amb._2BOT_OU.payout,
                                        discount: req.body.limitSetting.lotto.amb._2BOT_OU.discount,
                                        max: req.body.limitSetting.lotto.amb._2BOT_OU.max
                                    },
                                    _1TOP: {
                                        payout: req.body.limitSetting.lotto.amb._1TOP.payout,
                                        discount: req.body.limitSetting.lotto.amb._1TOP.discount,
                                        max: req.body.limitSetting.lotto.amb._1TOP.max
                                    },
                                    _1BOT: {
                                        payout: req.body.limitSetting.lotto.amb._1BOT.payout,
                                        discount: req.body.limitSetting.lotto.amb._1BOT.discount,
                                        max: req.body.limitSetting.lotto.amb._1BOT.max
                                    }
                                },
                                pp: {
                                    isEnable: req.body.limitSetting.lotto.pp.isEnable,
                                    hour: req.body.limitSetting.lotto.pp.hour,
                                    minute: req.body.limitSetting.lotto.pp.minute,
                                    _6TOP: {
                                        payout: req.body.limitSetting.lotto.pp._6TOP.payout,
                                        discount: req.body.limitSetting.lotto.pp._6TOP.discount,
                                        max: req.body.limitSetting.lotto.pp._6TOP.max
                                    },
                                    _5TOP: {
                                        payout: req.body.limitSetting.lotto.pp._5TOP.payout,
                                        discount: req.body.limitSetting.lotto.pp._5TOP.discount,
                                        max: req.body.limitSetting.lotto.pp._5TOP.max
                                    },
                                    _4TOP: {
                                        payout: req.body.limitSetting.lotto.pp._4TOP.payout,
                                        discount: req.body.limitSetting.lotto.pp._4TOP.discount,
                                        max: req.body.limitSetting.lotto.pp._4TOP.max
                                    },
                                    _4TOD: {
                                        payout: req.body.limitSetting.lotto.pp._4TOD.payout,
                                        discount: req.body.limitSetting.lotto.pp._4TOD.discount,
                                        max: req.body.limitSetting.lotto.pp._4TOD.max
                                    },
                                    _3TOP: {
                                        payout: req.body.limitSetting.lotto.pp._3TOP.payout,
                                        discount: req.body.limitSetting.lotto.pp._3TOP.discount,
                                        max: req.body.limitSetting.lotto.pp._3TOP.max
                                    },
                                    _3TOD: {
                                        payout: req.body.limitSetting.lotto.pp._3TOD.payout,
                                        discount: req.body.limitSetting.lotto.pp._3TOD.discount,
                                        max: req.body.limitSetting.lotto.pp._3TOD.max
                                    },
                                    _3BOT: {
                                        payout: 0,
                                        discount: 0,
                                        max: 0
                                    },
                                    _3TOP_OE: {
                                        payout: 0,
                                        discount: 0,
                                        max: 0
                                    },
                                    _3TOP_OU: {
                                        payout: 0,
                                        discount: 0,
                                        max: 0
                                    },
                                    _2TOP: {
                                        payout: req.body.limitSetting.lotto.pp._2TOP.payout,
                                        discount: req.body.limitSetting.lotto.pp._2TOP.discount,
                                        max: req.body.limitSetting.lotto.pp._2TOP.max
                                    },
                                    _2TOD: {
                                        payout: req.body.limitSetting.lotto.pp._2TOD.payout,
                                        discount: req.body.limitSetting.lotto.pp._2TOD.discount,
                                        max: req.body.limitSetting.lotto.pp._2TOD.max
                                    },
                                    _2BOT: {
                                        payout: req.body.limitSetting.lotto.pp._2BOT.payout,
                                        discount: req.body.limitSetting.lotto.pp._2BOT.discount,
                                        max: req.body.limitSetting.lotto.pp._2BOT.max
                                    },
                                    _2TOP_OE: {
                                        payout: 0,
                                        discount: 0,
                                        max: 0
                                    },
                                    _2TOP_OU: {
                                        payout: 0,
                                        discount: 0,
                                        max: 0
                                    },
                                    _2BOT_OE: {
                                        payout: 0,
                                        discount: 0,
                                        max: 0
                                    },
                                    _2BOT_OU: {
                                        payout: 0,
                                        discount: 0,
                                        max: 0
                                    },
                                    _1TOP: {
                                        payout: req.body.limitSetting.lotto.pp._1TOP.payout,
                                        discount: req.body.limitSetting.lotto.pp._1TOP.discount,
                                        max: req.body.limitSetting.lotto.pp._1TOP.max
                                    },
                                    _1BOT: {
                                        payout: req.body.limitSetting.lotto.pp._1BOT.payout,
                                        discount: req.body.limitSetting.lotto.pp._1BOT.discount,
                                        max: req.body.limitSetting.lotto.pp._1BOT.max
                                    }
                                },
                                laos: {
                                    isEnable: req.body.limitSetting.lotto.laos.isEnable,
                                    hour: req.body.limitSetting.lotto.laos.hour,
                                    minute: req.body.limitSetting.lotto.laos.minute,
                                    L_4TOP: {
                                        payout: req.body.limitSetting.lotto.laos.L_4TOP.payout,
                                        discount: req.body.limitSetting.lotto.laos.L_4TOP.discount,
                                        max: req.body.limitSetting.lotto.laos.L_4TOP.max
                                    },
                                    L_4TOD: {
                                        payout: req.body.limitSetting.lotto.laos.L_4TOD.payout,
                                        discount: req.body.limitSetting.lotto.laos.L_4TOP.discount,
                                        max: req.body.limitSetting.lotto.laos.L_4TOP.max
                                    },
                                    L_3TOP: {
                                        payout: req.body.limitSetting.lotto.laos.L_3TOP.payout,
                                        discount: req.body.limitSetting.lotto.laos.L_4TOP.discount,
                                        max: req.body.limitSetting.lotto.laos.L_4TOP.max
                                    },
                                    L_3TOD: {
                                        payout: req.body.limitSetting.lotto.laos.L_3TOD.payout,
                                        discount: req.body.limitSetting.lotto.laos.L_4TOP.discount,
                                        max: req.body.limitSetting.lotto.laos.L_4TOP.max
                                    },
                                    L_2FB: {
                                        payout: req.body.limitSetting.lotto.laos.L_2FB.payout,
                                        discount: req.body.limitSetting.lotto.laos.L_4TOP.discount,
                                        max: req.body.limitSetting.lotto.laos.L_4TOP.max
                                    },
                                    T_4TOP: {
                                        payout: req.body.limitSetting.lotto.laos.T_4TOP.payout,
                                        discount: req.body.limitSetting.lotto.laos.T_4TOP.discount,
                                        max: req.body.limitSetting.lotto.laos.T_4TOP.max
                                    },
                                    T_4TOD: {
                                        payout: req.body.limitSetting.lotto.laos.T_4TOD.payout,
                                        discount: req.body.limitSetting.lotto.laos.T_4TOD.discount,
                                        max: req.body.limitSetting.lotto.laos.T_4TOD.max
                                    },
                                    T_3TOP: {
                                        payout: req.body.limitSetting.lotto.laos.T_3TOP.payout,
                                        discount: req.body.limitSetting.lotto.laos.T_3TOP.discount,
                                        max: req.body.limitSetting.lotto.laos.T_3TOP.max
                                    },
                                    T_3TOD: {
                                        payout: req.body.limitSetting.lotto.laos.T_3TOD.payout,
                                        discount: req.body.limitSetting.lotto.laos.T_3TOD.discount,
                                        max: req.body.limitSetting.lotto.laos.T_3TOD.max
                                    },
                                    T_2TOP: {
                                        payout: req.body.limitSetting.lotto.laos.T_2TOP.payout,
                                        discount: req.body.limitSetting.lotto.laos.T_2TOP.discount,
                                        max: req.body.limitSetting.lotto.laos.T_2TOP.max
                                    },
                                    T_2TOD: {
                                        payout: req.body.limitSetting.lotto.laos.T_2TOD.payout,
                                        discount: req.body.limitSetting.lotto.laos.T_2TOD.discount,
                                        max: req.body.limitSetting.lotto.laos.T_2TOD.max
                                    },
                                    T_2BOT: {
                                        payout: req.body.limitSetting.lotto.laos.T_2BOT.payout,
                                        discount: req.body.limitSetting.lotto.laos.T_2BOT.discount,
                                        max: req.body.limitSetting.lotto.laos.T_2BOT.max
                                    },
                                    T_1TOP: {
                                        payout: req.body.limitSetting.lotto.laos.T_1TOP.payout,
                                        discount: req.body.limitSetting.lotto.laos.T_1TOP.discount,
                                        max: req.body.limitSetting.lotto.laos.T_1TOP.max
                                    },
                                    T_1BOT: {
                                        payout: req.body.limitSetting.lotto.laos.T_1BOT.payout,
                                        discount: req.body.limitSetting.lotto.laos.T_1BOT.discount,
                                        max: req.body.limitSetting.lotto.laos.T_1BOT.max
                                    }
                                }
                            }
                        },
                        commissionSetting: {
                            sportsBook: {
                                typeHdpOuOe: req.body.commissionSetting.sportsBook.typeHdpOuOe,
                                hdpOuOe: req.body.commissionSetting.sportsBook.hdpOuOe,
                                oneTwoDoubleChance: req.body.commissionSetting.sportsBook.oneTwoDoubleChance,
                                others: req.body.commissionSetting.sportsBook.others
                            },
                            parlay: {
                                com: req.body.commissionSetting.parlay.com
                            },
                            step: {
                                com2: req.body.commissionSetting.step.com2,
                                com3: req.body.commissionSetting.step.com3,
                                com4: req.body.commissionSetting.step.com4,
                                com5: req.body.commissionSetting.step.com5,
                                com6: req.body.commissionSetting.step.com6,
                                com7: req.body.commissionSetting.step.com7,
                                com8: req.body.commissionSetting.step.com8,
                                com9: req.body.commissionSetting.step.com9,
                                com10: req.body.commissionSetting.step.com10,
                                com11: req.body.commissionSetting.step.com11,
                                com12: req.body.commissionSetting.step.com12,
                            },
                            casino: {
                                sexy: req.body.commissionSetting.casino.sexy,
                                ag: req.body.commissionSetting.casino.ag,
                                // allbet: req.body.commissionSetting.casino.allbet
                                sa: req.body.commissionSetting.casino.sa,
                                dg: req.body.commissionSetting.casino.dg,
                                pt: req.body.commissionSetting.casino.pt,
                            },
                            multi: {
                                amb: req.body.commissionSetting.multi.amb,
                            },
                            game: {
                                slotXO: req.body.commissionSetting.game.slotXO,
                            },
                            other: {
                                m2: req.body.commissionSetting.other.m2,
                            }
                        },
                    };

                    if (req.body.password !== '' && req.body.password != null) {
                        Hashing.encrypt256(req.body.password, function (err, hashingPass) {
                            body.password = hashingPass;
                            try {
                                const username = req.userInfo.username;
                                const prefixAutoTopUp = req.userInfo.group.prefixAutoTopUp;
                                httpRequest.post(
                                    URL_PAYMENT_GATEWAY_UPDATE_PASSWORD,
                                    {
                                        json: {
                                            username: username.toUpperCase(),
                                            password: req.body.password,
                                            prefix: prefixAutoTopUp
                                        }
                                    },
                                    (error, response) => {
                                        if (error) {
                                            console.error(error);
                                        } else {
                                            console.log(`${URL_PAYMENT_GATEWAY_UPDATE_PASSWORD} [${response.body.code}]`);
                                        }
                                    });
                            } catch (e) {

                            }
                        });
                    }

                    async.series([(callback) => {
                        AgentGroupModel.findOne({name: req.body.username}, function (err, response) {
                            if (err)
                                callback(err, null);
                            else
                                callback(null, response);

                        });
                    }, (callback) => {
                        MemberModel.findOne({username: req.body.username}, function (err, response) {
                            if (err)
                                callback(err, null);
                            else
                                callback(null, response);

                        });
                    }, (callback => {
                        AgentGroupModel.findById(userInfo.group._id, function (err, response) {
                            if (err)
                                callback(err, null);
                            else
                                callback(null, response);
                        });
                    }), (callback => {
                        MemberModel.findById(req.params.memberId, function (err, response) {
                            if (err)
                                callback(err, null);
                            else
                                callback(null, response);
                        });
                    }), (callback) => {
                        AgentService.getCreditLimitUsage(userInfo.group._id, function (err, response) {
                            if (err) {
                                callback(err, '');
                            } else {
                                callback(null, response ? response : 0);
                            }
                        });
                    }], (err, results) => {
                        if (err) {
                            return res.send({message: "error", result: err, code: 999});
                        }

                        if (results[0] || results[1]) {
                            return res.send({
                                message: "name was already used",
                                result: err,
                                code: 4001
                            });
                        }

                        //check creditLimit
                        const currentAgentInfo = results[2];
                        const creditLimit = results[2].creditLimit;
                        const memberUpdateInfo = results[3];
                        const creditLimitUsage = results[4];

                        console.log('own creditLimit : ', creditLimit)
                        console.log('creditLimit : ' + creditLimit + ' , creditLimitUsage : ' + creditLimitUsage);

                        // if(req.body.creditLimit > currentAgentInfo.maxMemberCreditLimit){
                        //     return res.send({message: "max creditLimit limit", result: err, code: 4030});
                        // }


                        if (req.body.creditModify) {
                            if (req.body.creditLimit > (creditLimit - (creditLimitUsage - memberUpdateInfo.creditLimit))) {
                                return res.send({
                                    message: "creditLimit exceeded",
                                    result: err,
                                    code: 4002
                                });
                            }

                            if (req.body.creditLimit > currentAgentInfo.maxCreditLimit) {
                                return res.send({
                                    message: "max creditLimit limit",
                                    result: err,
                                    code: 4030
                                });
                            }

                            if ((req.body.creditLimit + memberUpdateInfo.balance) < 0) {
                                return res.send({
                                    message: "Match limit(Credit Limit) has less than its balance.",
                                    result: err,
                                    code: 4029
                                });
                            }
                        } else {
                            delete body.creditLimit;
                        }

                        // if (req.body.shareSetting.lotto.amb.parent != currentAgentInfo.shareSetting.lotto.amb.own) {
                        //     return res.send({
                        //         message: "Agent Share (Amb Lotto) + Agent Max Share (Amb Lotto) should be greater than Agent Min Shares (Amb Lotto)",
                        //         result: err,
                        //         code: 4030
                        //     });
                        // }

                        // if(!req.body.limitSetting.casino.allbet.limitA && !req.body.limitSetting.casino.allbet.limitB && !req.body.limitSetting.casino.allbet.limitC){
                        //     return res.send({
                        //         message: "Bet Limit(All Bet) choose one to three handicaps",
                        //         result: err,
                        //         code: 4031
                        //     });
                        // }


                        if (body.limitSetting.sportsBook.hdpOuOe.maxPerBet > currentAgentInfo.limitSetting.sportsBook.hdpOuOe.maxPerBet) {
                            body.limitSetting.sportsBook.hdpOuOe.maxPerBet = currentAgentInfo.limitSetting.sportsBook.hdpOuOe.maxPerBet;
                        }

                        if (body.limitSetting.sportsBook.hdpOuOe.maxPerMatch > currentAgentInfo.limitSetting.sportsBook.hdpOuOe.maxPerMatch) {
                            body.limitSetting.sportsBook.hdpOuOe.maxPerMatch = currentAgentInfo.limitSetting.sportsBook.hdpOuOe.maxPerMatch
                        }
                        if (body.limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet > currentAgentInfo.limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet) {
                            body.limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet = currentAgentInfo.limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet
                        }
                        if (body.limitSetting.sportsBook.others.maxPerBet > currentAgentInfo.limitSetting.sportsBook.others.maxPerBet) {
                            body.limitSetting.sportsBook.others.maxPerBet = currentAgentInfo.limitSetting.sportsBook.others.maxPerBet
                        }

                        if (body.limitSetting.sportsBook.outright.maxPerMatch > currentAgentInfo.limitSetting.sportsBook.outright.maxPerMatch) {
                            body.limitSetting.sportsBook.outright.maxPerMatch = currentAgentInfo.limitSetting.sportsBook.outright.maxPerMatch
                        }

                        //PARLAY
                        if (body.limitSetting.step.parlay.maxPerBet > currentAgentInfo.limitSetting.step.parlay.maxPerBet) {
                            body.limitSetting.step.parlay.maxPerBet = currentAgentInfo.limitSetting.step.parlay.maxPerBet
                        }
                        if (body.limitSetting.step.parlay.minPerBet < currentAgentInfo.limitSetting.step.parlay.minPerBet) {
                            body.limitSetting.step.parlay.minPerBet = currentAgentInfo.limitSetting.step.parlay.minPerBet
                        }
                        if (body.limitSetting.step.parlay.maxBetPerDay > currentAgentInfo.limitSetting.step.parlay.maxBetPerDay) {
                            body.limitSetting.step.parlay.maxBetPerDay = currentAgentInfo.limitSetting.step.parlay.maxBetPerDay
                        }
                        if (body.limitSetting.step.parlay.maxPayPerBill > currentAgentInfo.limitSetting.step.parlay.maxPayPerBill) {
                            body.limitSetting.step.parlay.maxPayPerBill = currentAgentInfo.limitSetting.step.parlay.maxPayPerBill
                        }

                        //STEP
                        if (body.limitSetting.step.step.maxPerBet > currentAgentInfo.limitSetting.step.step.maxPerBet) {
                            body.limitSetting.step.step.maxPerBet = currentAgentInfo.limitSetting.step.step.maxPerBet
                        }
                        if (body.limitSetting.step.step.minPerBet < currentAgentInfo.limitSetting.step.step.minPerBet) {
                            body.limitSetting.step.step.minPerBet = currentAgentInfo.limitSetting.step.step.minPerBet
                        }
                        if (body.limitSetting.step.step.maxBetPerDay > currentAgentInfo.limitSetting.step.step.maxBetPerDay) {
                            body.limitSetting.step.step.maxBetPerDay = currentAgentInfo.limitSetting.step.step.maxBetPerDay
                        }
                        if (body.limitSetting.step.step.maxPayPerBill > currentAgentInfo.limitSetting.step.step.maxPayPerBill) {
                            body.limitSetting.step.step.maxPayPerBill = currentAgentInfo.limitSetting.step.step.maxPayPerBill
                        }

                        //OTHER

                        if (body.limitSetting.other.m2.maxPerBetHDP > currentAgentInfo.limitSetting.other.m2.maxPerBetHDP) {
                            body.limitSetting.other.m2.maxPerBetHDP = currentAgentInfo.limitSetting.other.m2.maxPerBetHDP
                        }
                        if (body.limitSetting.other.m2.minPerBetHDP < currentAgentInfo.limitSetting.other.m2.minPerBetHDP) {
                            body.limitSetting.other.m2.minPerBetHDP = currentAgentInfo.limitSetting.other.m2.minPerBetHDP
                        }
                        if (body.limitSetting.other.m2.maxBetPerMatchHDP > currentAgentInfo.limitSetting.other.m2.maxBetPerMatchHDP) {
                            body.limitSetting.other.m2.maxBetPerMatchHDP = currentAgentInfo.limitSetting.other.m2.maxBetPerMatchHDP
                        }


                        if (body.limitSetting.other.m2.maxPerBet > currentAgentInfo.limitSetting.other.m2.maxPerBet) {
                            body.limitSetting.other.m2.maxPerBet = currentAgentInfo.limitSetting.other.m2.maxPerBet
                        }
                        if (body.limitSetting.other.m2.minPerBet < currentAgentInfo.limitSetting.other.m2.minPerBet) {
                            body.limitSetting.other.m2.minPerBet = currentAgentInfo.limitSetting.other.m2.minPerBet
                        }
                        if (body.limitSetting.other.m2.maxBetPerDay > currentAgentInfo.limitSetting.other.m2.maxBetPerDay) {
                            body.limitSetting.other.m2.maxBetPerDay = currentAgentInfo.limitSetting.other.m2.maxBetPerDay
                        }
                        if (body.limitSetting.other.m2.maxPayPerBill > currentAgentInfo.limitSetting.other.m2.maxPayPerBill) {
                            body.limitSetting.other.m2.maxPayPerBill = currentAgentInfo.limitSetting.other.m2.maxPayPerBill
                        }


                        //TODO
                        if (body.limitSetting.lotto.amb._6TOP.max > currentAgentInfo.limitSetting.lotto.amb._6TOP.max) {
                            body.limitSetting.lotto.amb._6TOP.max = currentAgentInfo.limitSetting.lotto.amb._6TOP.max
                        }

                        if (body.limitSetting.lotto.amb._5TOP.max > currentAgentInfo.limitSetting.lotto.amb._5TOP.max) {
                            body.limitSetting.lotto.amb._5TOP.max = currentAgentInfo.limitSetting.lotto.amb._5TOP.max
                        }

                        if (body.limitSetting.lotto.amb._4TOP.max > currentAgentInfo.limitSetting.lotto.amb._4TOP.max) {
                            body.limitSetting.lotto.amb._4TOP.max = currentAgentInfo.limitSetting.lotto.amb._4TOP.max
                        }

                        if (body.limitSetting.lotto.amb._4TOD.max > currentAgentInfo.limitSetting.lotto.amb._4TOD.max) {
                            body.limitSetting.lotto.amb._4TOD.max = currentAgentInfo.limitSetting.lotto.amb._4TOD.max
                        }

                        if (body.limitSetting.lotto.amb._3TOP.max > currentAgentInfo.limitSetting.lotto.amb._3TOP.max) {
                            body.limitSetting.lotto.amb._3TOP.max = currentAgentInfo.limitSetting.lotto.amb._3TOP.max
                        }

                        if (body.limitSetting.lotto.amb._3TOD.max > currentAgentInfo.limitSetting.lotto.amb._3TOD.max) {
                            body.limitSetting.lotto.amb._3TOD.max = currentAgentInfo.limitSetting.lotto.amb._3TOD.max
                        }

                        if (body.limitSetting.lotto.amb._3BOT.max > currentAgentInfo.limitSetting.lotto.amb._3BOT.max) {
                            body.limitSetting.lotto.amb._3BOT.max = currentAgentInfo.limitSetting.lotto.amb._3BOT.max
                        }

                        if (body.limitSetting.lotto.amb._3TOP_OE.max > currentAgentInfo.limitSetting.lotto.amb._3TOP_OE.max) {
                            body.limitSetting.lotto.amb._3TOP_OE.max = currentAgentInfo.limitSetting.lotto.amb._3TOP_OE.max
                        }

                        if (body.limitSetting.lotto.amb._3TOP_OU.max > currentAgentInfo.limitSetting.lotto.amb._3TOP_OU.max) {
                            body.limitSetting.lotto.amb._3TOP_OU.max = currentAgentInfo.limitSetting.lotto.amb._3TOP_OU.max
                        }

                        if (body.limitSetting.lotto.amb._2TOP.max > currentAgentInfo.limitSetting.lotto.amb._2TOP.max) {
                            body.limitSetting.lotto.amb._2TOP.max = currentAgentInfo.limitSetting.lotto.amb._2TOP.max
                        }

                        if (body.limitSetting.lotto.amb._2TOD.max > currentAgentInfo.limitSetting.lotto.amb._2TOD.max) {
                            body.limitSetting.lotto.amb._2TOD.max = currentAgentInfo.limitSetting.lotto.amb._2TOD.max
                        }

                        if (body.limitSetting.lotto.amb._2BOT.max > currentAgentInfo.limitSetting.lotto.amb._2BOT.max) {
                            body.limitSetting.lotto.amb._2BOT.max = currentAgentInfo.limitSetting.lotto.amb._2BOT.max
                        }

                        if (body.limitSetting.lotto.amb._2TOP_OE.max > currentAgentInfo.limitSetting.lotto.amb._2TOP_OE.max) {
                            body.limitSetting.lotto.amb._2TOP_OE.max = currentAgentInfo.limitSetting.lotto.amb._2TOP_OE.max
                        }

                        if (body.limitSetting.lotto.amb._2TOP_OU.max > currentAgentInfo.limitSetting.lotto.amb._2TOP_OU.max) {
                            body.limitSetting.lotto.amb._2TOP_OU.max = currentAgentInfo.limitSetting.lotto.amb._2TOP_OU.max
                        }


                        if (body.limitSetting.lotto.amb._2BOT_OE.max > currentAgentInfo.limitSetting.lotto.amb._2BOT_OE.max) {
                            body.limitSetting.lotto.amb._2BOT_OE.max = currentAgentInfo.limitSetting.lotto.amb._2BOT_OE.max
                        }

                        if (body.limitSetting.lotto.amb._2BOT_OU.max > currentAgentInfo.limitSetting.lotto.amb._2BOT_OU.max) {
                            body.limitSetting.lotto.amb._2BOT_OU.max = currentAgentInfo.limitSetting.lotto.amb._2BOT_OU.max
                        }


                        if (body.limitSetting.lotto.amb._1TOP.max > currentAgentInfo.limitSetting.lotto.amb._1TOP.max) {
                            body.limitSetting.lotto.amb._1TOP.max = currentAgentInfo.limitSetting.lotto.amb._1TOP.max
                        }

                        if (body.limitSetting.lotto.amb._1BOT.max > currentAgentInfo.limitSetting.lotto.amb._1BOT.max) {
                            body.limitSetting.lotto.amb._1BOT.max = currentAgentInfo.limitSetting.lotto.amb._1BOT.max
                        }


                        //PP
                        if (body.limitSetting.lotto.pp._6TOP.max > currentAgentInfo.limitSetting.lotto.pp._6TOP.max) {
                            body.limitSetting.lotto.pp._6TOP.max = currentAgentInfo.limitSetting.lotto.pp._6TOP.max
                        }

                        if (body.limitSetting.lotto.pp._5TOP.max > currentAgentInfo.limitSetting.lotto.pp._5TOP.max) {
                            body.limitSetting.lotto.pp._5TOP.max = currentAgentInfo.limitSetting.lotto.pp._5TOP.max
                        }

                        if (body.limitSetting.lotto.pp._4TOP.max > currentAgentInfo.limitSetting.lotto.pp._4TOP.max) {
                            body.limitSetting.lotto.pp._4TOP.max = currentAgentInfo.limitSetting.lotto.pp._4TOP.max
                        }

                        if (body.limitSetting.lotto.pp._4TOD.max > currentAgentInfo.limitSetting.lotto.pp._4TOD.max) {
                            body.limitSetting.lotto.pp._4TOD.max = currentAgentInfo.limitSetting.lotto.pp._4TOD.max
                        }

                        if (body.limitSetting.lotto.pp._3TOP.max > currentAgentInfo.limitSetting.lotto.pp._3TOP.max) {
                            body.limitSetting.lotto.pp._3TOP.max = currentAgentInfo.limitSetting.lotto.pp._3TOP.max
                        }

                        if (body.limitSetting.lotto.pp._3TOD.max > currentAgentInfo.limitSetting.lotto.pp._3TOD.max) {
                            body.limitSetting.lotto.pp._3TOD.max = currentAgentInfo.limitSetting.lotto.pp._3TOD.max
                        }

                        if (body.limitSetting.lotto.pp._2TOP.max > currentAgentInfo.limitSetting.lotto.pp._2TOP.max) {
                            body.limitSetting.lotto.pp._2TOP.max = currentAgentInfo.limitSetting.lotto.pp._2TOP.max
                        }

                        if (body.limitSetting.lotto.pp._2TOD.max > currentAgentInfo.limitSetting.lotto.pp._2TOD.max) {
                            body.limitSetting.lotto.pp._2TOD.max = currentAgentInfo.limitSetting.lotto.pp._2TOD.max
                        }

                        if (body.limitSetting.lotto.pp._2BOT.max > currentAgentInfo.limitSetting.lotto.pp._2BOT.max) {
                            body.limitSetting.lotto.pp._2BOT.max = currentAgentInfo.limitSetting.lotto.pp._2BOT.max
                        }

                        if (body.limitSetting.lotto.pp._1TOP.max > currentAgentInfo.limitSetting.lotto.pp._1TOP.max) {
                            body.limitSetting.lotto.pp._1TOP.max = currentAgentInfo.limitSetting.lotto.pp._1TOP.max
                        }

                        if (body.limitSetting.lotto.pp._1BOT.max > currentAgentInfo.limitSetting.lotto.pp._1BOT.max) {
                            body.limitSetting.lotto.pp._1BOT.max = currentAgentInfo.limitSetting.lotto.pp._1BOT.max
                        }


                        if (req.body.settingEnable.sportBookEnable) {
                            if ((req.body.shareSetting.sportsBook.today.parent ) < currentAgentInfo.shareSetting.sportsBook.today.min) {
                                return res.send({
                                    message: "Agent Share (Sport Today) + Agent Max Share (Sport Today) should be greater than Agent Min Shares (Sport Today)",
                                    result: err,
                                    code: 4003
                                });
                            }

                            if ((req.body.shareSetting.sportsBook.live.parent ) < currentAgentInfo.shareSetting.sportsBook.live.min) {
                                return res.send({
                                    message: "Agent Share (Sport Live) + Agent Max Share (Sport Live) should be greater than Agent Min Shares (Sport Live)",
                                    result: err,
                                    code: 4004
                                });
                            }
                        }

                        if (req.body.settingEnable.parlayEnable) {
                            if ((req.body.shareSetting.step.parlay.parent ) < currentAgentInfo.shareSetting.step.parlay.min) {
                                return res.send({
                                    message: "Agent Share (Step Parlay) + Agent Max Share (Step Parlay) should be greater than Agent Min Shares (Step Parlay)",
                                    result: err,
                                    code: 4005
                                });
                            }
                        }

                        if (req.body.settingEnable.casinoSexyEnable) {
                            if ((req.body.shareSetting.casino.sexy.parent ) < currentAgentInfo.shareSetting.casino.sexy.min) {
                                return res.send({
                                    message: "Agent Share (Sexy Baccarat) + Agent Max Share (Sexy Baccarat) should be greater than Agent Min Shares (Sexy Baccarat)",
                                    result: err,
                                    code: 4006
                                });
                            }
                        }

                        if (req.body.settingEnable.otherM2Enable) {
                            if ((req.body.shareSetting.other.m2.parent ) < currentAgentInfo.shareSetting.other.m2.min) {
                                return res.send({
                                    message: "Agent Share (M2 Sport) + Agent Max Share (M2 Sport) should be greater than Agent Min Shares (M2 Sport)",
                                    result: err,
                                    code: 4007
                                });
                            }
                        }

                        if (req.body.settingEnable.stepEnable) {
                            if ((req.body.shareSetting.step.step.parent ) < currentAgentInfo.shareSetting.step.step.min) {
                                return res.send({
                                    message: "Agent Share (Mix Step) + Agent Max Share (Mix Step) should be greater than Agent Min Shares (Mix Step)",
                                    result: err,
                                    code: 4008
                                });
                            }
                        }

                        // if ((req.body.shareSetting.casino.allbet.own + req.body.shareSetting.casino.allbet.parent ) < currentAgentInfo.shareSetting.casino.allbet.min) {
                        //     return res.send({
                        //         message: "Agent Share (All Bet) + Agent Max Share (All Bet) should be greater than Agent Min Shares (All Bet)",
                        //         result: err,
                        //         code: 4008
                        //     });
                        // }

                        if (req.body.settingEnable.gameSlotEnable) {
                            if ((req.body.shareSetting.game.slotXO.parent ) < currentAgentInfo.shareSetting.game.slotXO.min) {
                                return res.send({
                                    message: "Agent Share (Slot XO) + Agent Max Share (Slot XO) should be greater than Agent Min Shares (Slot XO)",
                                    result: err,
                                    code: 4009
                                });
                            }
                        }

                        if (req.body.settingEnable.casinoSaEnable) {
                            if ((req.body.shareSetting.casino.sa.parent ) < currentAgentInfo.shareSetting.casino.sa.min) {
                                return res.send({
                                    message: "Agent Share (SA Gaming) + Agent Max Share (SA Gaming) should be greater than Agent Min Shares (SA Gaming)",
                                    result: err,
                                    code: 4011
                                });
                            }
                        }

                        if (req.body.settingEnable.casinoDgEnable) {
                            if ((req.body.shareSetting.casino.dg.parent ) < currentAgentInfo.shareSetting.casino.dg.min) {
                                return res.send({
                                    message: "Agent Share (DG Gaming) + Agent Max Share (DG Gaming) should be greater than Agent Min Shares (DG Gaming)",
                                    result: err,
                                    code: 4012
                                });
                            }
                        }


                        if (req.body.settingEnable.casinoAgEnable) {
                            if ((req.body.shareSetting.casino.ag.parent ) < currentAgentInfo.shareSetting.casino.ag.min) {
                                return res.send({
                                    message: "Agent Share (AG Gaming) + Agent Max Share (AG Gaming) should be greater than Agent Min Shares (AG Gaming)",
                                    result: err,
                                    code: 4013
                                });
                            }
                        }


                        if (req.body.settingEnable.gameCardEnable) {
                            if ((req.body.shareSetting.multi.amb.parent ) < currentAgentInfo.shareSetting.multi.amb.min) {
                                return res.send({
                                    message: "Agent Share (AMB Game) + Agent Max Share (AMB Game) should be greater than Agent Min Shares (AMB Game)",
                                    result: err,
                                    code: 4014
                                });
                            }
                        }

                        if (req.body.settingEnable.casinoPtEnable) {
                            if ((req.body.shareSetting.casino.pt.parent ) < currentAgentInfo.shareSetting.casino.pt.min) {
                                return res.send({
                                    message: "Agent Share (Pretty Gaming) + Agent Max Share (Pretty Gaming) should be greater than Agent Min Shares (Pretty Gaming)",
                                    result: err,
                                    code: 4015
                                });
                            }
                        }


                        // if ((req.body.shareSetting.lotto.amb.parent ) < currentAgentInfo.shareSetting.lotto.amb.own) {
                        //     return res.send({
                        //         message: "Agent Share (Amb Lotto) + Agent Max Share (Amb Lotto) should be greater than Agent Min Shares (Amb Lotto)",
                        //         result: err,
                        //         code: 4010
                        //     });
                        // }

                        MemberModel.findOneAndUpdate({_id: mongoose.Types.ObjectId(req.params.memberId)}, body, {new: true}, function (err, data) {

                            if (err) {
                                return res.send({message: "error", results: err, code: 999});
                            }

                            let historyModel = MemberHistoryModel(body);
                            historyModel.id = req.params.memberId;
                            historyModel.createdBy = userInfo._id;
                            historyModel.createdDate = DateUtils.getCurrentDate();
                            historyModel.save((err, historyResponse) => {
                                console.log(err)
                            });


                            //kick live
                            if (!body.limitSetting.casino.sexy.isEnable) {
                                SexyBaccaratService.callKickUsers([data.username_lower], (err, response) => {
                                    console.log('sexy : ', response);
                                });
                            }

                            // if(!body.limitSetting.casino.dg.isEnable){
                            //     MemberService.modifyMemberStateDreamGame(req.params.memberId,false,(err,response)=>{
                            //         console.log('dg : ',response);
                            //     });
                            // }else {
                            //     MemberService.modifyMemberStateDreamGame(req.params.memberId,true,(err,response)=>{
                            //         console.log('dg : ',response);
                            //     });
                            // }

                            if (!body.limitSetting.casino.sa.isEnable) {
                                SaGameService.callKickUser(data.username_lower, (err, response) => {
                                    console.log('sa : ', response);
                                });
                            }

                            // AllBetService.modifyClient(data.username_lower,
                            //     req.body.limitSetting.casino.allbet.limitA,
                            //     req.body.limitSetting.casino.allbet.limitB,
                            //     req.body.limitSetting.casino.allbet.limitC,
                            //     (err,modifyClientResponse)=>{
                            //             console.log(modifyClientResponse)
                            //     });
                            try {
                                Redis.deleteByKey(`LimitSetting${CLIENT_NAME}`, (error, result) => {

                                });
                            } catch (e) {

                            }

                            return res.send(
                                {
                                    code: 0,
                                    message: "success",
                                    result: data
                                }
                            );
                        });

                    });
                }
            });

        }
    });

});


const resetPasswordSchema = Joi.object().keys({
    oldPassword: Joi.string().required(),
    newPassword: Joi.string().min(8).max(15).required()
});

router.put('/reset-password', function (req, res) {
    let validate = Joi.validate(req.body, resetPasswordSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            result: validate.error.details,
            code: 999
        });
    }

    let userInfo = req.userInfo;


    console.log('/reset-password');
    console.log(JSON.stringify(req.userInfo));
    const username = req.userInfo.username;
    const isAutoTopUp = req.userInfo.isAutoTopUp;
    const prefixAutoTopUp = req.userInfo.group.prefixAutoTopUp;

    let oldPassword = '';
    let newPassword = '';

    Hashing.encrypt256(req.body.oldPassword, function (err, hashingPass) {
        oldPassword = hashingPass;
    });

    Hashing.encrypt256(req.body.newPassword, function (err, hashingPass) {
        newPassword = hashingPass;
    });
    // console.log('1>>>>>>>',username,oldPassword,newPassword,'<<<<<<<>')

    if (username === 'SB8800001') {
        MemberModel.findOne({username_lower: username.toLowerCase()}, function (err, data) {

            if (err) {
                return res.send({message: "error", results: err, code: 999});
            }
            if (data) {
                console.log(1)
                if (data.password !== oldPassword) {
                    return res.send({
                        message: "invalid password",
                        result: "invalid password",
                        code: 999
                    });
                }
                if (data.password === newPassword) {
                    return res.send({
                        message: "Is not able to use the same old password ",
                        result: "Is not able to use the same old password ",
                        code: 999
                    });
                }

                return res.send({message: "success", result: "success", code: 0});
            } else {
                return res.send({message: "invalid user", results: err, code: 999});
            }

        });
    }
    else if (username === 'SB8800002') {
        MemberModel.findOne({username_lower: username.toLowerCase()}, function (err, data) {

            if (err) {
                return res.send({message: "error", results: err, code: 999});
            }
            if (data) {
                if (data.password !== oldPassword) {
                    return res.send({
                        message: "invalid password",
                        result: "invalid password",
                        code: 999
                    });
                }
                if (data.password === newPassword) {
                    return res.send({
                        message: "Is not able to use the same old password ",
                        result: "Is not able to use the same old password ",
                        code: 999
                    });
                }

                return res.send({message: "success", result: "success", code: 0});
            } else {
                return res.send({message: "invalid user", results: err, code: 999});
            }

        });
    }
    else if (username === 'SB8800003') {
        MemberModel.findOne({username_lower: username.toLowerCase()}, function (err, data) {

            if (err) {
                return res.send({message: "error", results: err, code: 999});
            }
            if (data) {
                if (data.password !== oldPassword) {
                    return res.send({
                        message: "invalid password",
                        result: "invalid password",
                        code: 999
                    });
                }
                if (data.password === newPassword) {
                    return res.send({
                        message: "Is not able to use the same old password ",
                        result: "Is not able to use the same old password ",
                        code: 999
                    });
                }

                return res.send({message: "success", result: "success", code: 0});
            } else {
                return res.send({message: "invalid user", results: err, code: 999});
            }

        });
    }
    else if (username === 'SB8800004') {
        MemberModel.findOne({username_lower: username.toLowerCase()}, function (err, data) {

            if (err) {
                return res.send({message: "error", results: err, code: 999});
            }
            if (data) {
                if (data.password !== oldPassword) {
                    return res.send({
                        message: "invalid password",
                        result: "invalid password",
                        code: 999
                    });
                }
                if (data.password === newPassword) {
                    return res.send({
                        message: "Is not able to use the same old password ",
                        result: "Is not able to use the same old password ",
                        code: 999
                    });
                }

                return res.send({message: "success", result: "success", code: 0});
            } else {
                return res.send({message: "invalid user", results: err, code: 999});
            }

        });
    }
    else if (username === 'SB8800005') {
        MemberModel.findOne({username_lower: username.toLowerCase()}, function (err, data) {

            if (err) {
                return res.send({message: "error", results: err, code: 999});
            }
            if (data) {
                if (data.password !== oldPassword) {
                    return res.send({
                        message: "invalid password",
                        result: "invalid password",
                        code: 999
                    });
                }
                if (data.password === newPassword) {
                    return res.send({
                        message: "Is not able to use the same old password ",
                        result: "Is not able to use the same old password ",
                        code: 999
                    });
                }

                return res.send({message: "success", result: "success", code: 0});
            } else {
                return res.send({message: "invalid user", results: err, code: 999});
            }

        });
    }
    else {
        MemberModel.findOne({username_lower: username.toLowerCase()}, function (err, data) {

            if (err) {
                return res.send({message: "error", results: err, code: 999});
            }
            if (data) {
                if (data.password !== oldPassword) {
                    return res.send({
                        message: "invalid password",
                        result: "invalid password",
                        code: 999
                    });
                }
                if (data.password === newPassword) {
                    return res.send({
                        message: "Is not able to use the same old password ",
                        result: "Is not able to use the same old password ",
                        code: 999
                    });
                }


                MemberModel.update({username_lower: username.toLowerCase()}, {
                    password: newPassword,
                    forceChangePassword: false,
                    forceChangePasswordDate: DateUtils.convertThaiDate(moment().add(3, 'month').toDate())
                }, function (err, response) {
                    if (err) {
                        return res.send({message: "error", result: err, code: 999});
                    }
                    if (response.n === 1) {
                        // if (isAutoTopUp) {
                        try {
                            httpRequest.post(
                                URL_PAYMENT_GATEWAY_UPDATE_PASSWORD,
                                {
                                    json: {
                                        username: username.toUpperCase(),
                                        password: req.body.newPassword,
                                        prefix: prefixAutoTopUp
                                    }
                                },
                                (error, response) => {
                                    if (error) {
                                        console.error(error);
                                    } else {
                                        console.log(`${URL_PAYMENT_GATEWAY_UPDATE_PASSWORD} [${response.body.code}]`);
                                    }
                                });
                        } catch (e) {

                        }
                        // }
                        return res.send({message: "success", result: "success", code: 0});

                    } else {
                        // if (isAutoTopUp) {
                        try {
                            httpRequest.post(
                                URL_PAYMENT_GATEWAY_UPDATE_PASSWORD,
                                {
                                    json: {
                                        username: username.toUpperCase(),
                                        password: req.body.newPassword,
                                        prefix: prefixAutoTopUp
                                    }
                                },
                                (error, response) => {
                                    if (error) {
                                        console.error(error);
                                    } else {
                                        console.log(`${URL_PAYMENT_GATEWAY_UPDATE_PASSWORD} [${response.body.code}]`);
                                    }
                                });
                        } catch (e) {

                        }
                        // }
                        return res.send({message: "success", result: "success", code: 0});
                    }
                });
            } else {
                return res.send({message: "invalid user", results: err, code: 999});
            }

        });
    }


});


const updateSportBookSchema = Joi.object().keys({
    locale: Joi.string().required(),
    odd: Joi.string().required(),
    view: Joi.string().required(),
    market: Joi.string().allow(''),
    acceptAnyOdd: Joi.boolean().required(),
    defaultPrice: {
        check: Joi.boolean().required(),
        price: Joi.number().required(),
    },
    last: Joi.boolean().required(),
    sportsMarket: Joi.string().required()
});

router.put('/config/sportbook', function (req, res) {
    let validate = Joi.validate(req.body, updateSportBookSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            result: validate.error.details,
            code: 999
        });
    }
    let body = {
        $set: {
            configuration: {
                sportsBook: {
                    locale: req.body.locale,
                    odd: req.body.odd,
                    views: req.body.view,
                    market: req.body.sportsMarket,
                    acceptAnyOdd: req.body.acceptAnyOdd,
                    defaultPrice: {
                        check: req.body.defaultPrice.check,
                        price: req.body.defaultPrice.price,
                    },
                    last: req.body.last
                }
            }
        }
    };


    MemberModel.findOneAndUpdate({_id: req.userInfo._id}, body, {
        upsert: true,
        new: true,
        setDefaultsOnInsert: true
    }, function (err, response) {
        if (err) {
            return res.send({message: "error", result: err, code: 999});
        }

        return res.send({message: "success", result: "success", code: 0});
    });

});


router.get('/config/sportbook', function (req, res) {

    MemberModel.findById(req.userInfo._id)
        .select({'configuration': 1, 'limitSetting': 1, '_id': 0})
        .exec(function (err, response) {
            if (err)
                return res.send({message: "error", result: err, code: 999});

            let result = Object.assign({}, response)._doc;

            result.configuration.sportsBook.today = {
                isEnable: response.limitSetting.sportsBook.isEnable,
            };

            result.configuration.sportsBook.parlay = {
                maxMatchPerBet: response.limitSetting.step.parlay.maxMatchPerBet,
                minMatchPerBet: response.limitSetting.step.parlay.minMatchPerBet,
                maxPerBet: response.limitSetting.step.parlay.maxPerBet,
                minPerBet: response.limitSetting.step.parlay.minPerBet,
                maxPayPerBill: response.limitSetting.step.parlay.maxPayPerBill,
                isEnable: response.limitSetting.step.parlay.isEnable,
            };

            result.configuration.sportsBook.step = {
                maxMatchPerBet: response.limitSetting.step.step.maxMatchPerBet,
                minMatchPerBet: response.limitSetting.step.step.minMatchPerBet,
                maxPerBet: response.limitSetting.step.step.maxPerBet,
                minPerBet: response.limitSetting.step.step.minPerBet,
                maxPayPerBill: response.limitSetting.step.step.maxPayPerBill,
                maxMixPayPerBill: roundTo(response.limitSetting.step.step.maxPayPerBill / 5, 2),
                isEnable: response.limitSetting.step.step.isEnable,
            };

            result.configuration.m2 = {
                maxPayPerBill: response.limitSetting.other.m2.maxPayPerBill,
                minPerBet: response.limitSetting.other.m2.minPerBet
            };


            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: {
                        configuration: result.configuration
                    }
                }
            );
        });
});


const createLoginNameSchema = Joi.object().keys({
    loginName: Joi.string().required(),
});

router.post('/create-login-name', function (req, res) {
    let validate = Joi.validate(req.body, createLoginNameSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            result: validate.error.details,
            code: 999
        });
    }

    const username = req.userInfo.username;
    const loginName = req.body.loginName;


    MemberModel.findOne({$or: [{'username_lower': loginName.toLowerCase()}, {'loginName_lower': loginName.toLowerCase()}]}, function (err, data) {

        if (err) {
            return res.send({message: "error", results: err, code: 999});
        }
        if (!data) {

            MemberModel.update({username_lower: username.toLowerCase()}, {
                loginName: loginName,
                loginName_lower: loginName.toLowerCase(),
                updatedDate: DateUtils.getCurrentDate()
            }, function (err, response) {
                if (err) {
                    return res.send({message: "error", result: err, code: 999});
                }

                if (response.n === 1) {
                    return res.send({message: "success", result: "success", code: 0});
                } else {
                    return res.send({message: "success", result: "success", code: 0});
                }
            });
        } else {
            return res.send({message: "loginName was already used", results: err, code: 999});
        }

    });
});


const termConditionSchema = Joi.object().keys({
    isAcceptCondition: Joi.boolean().required(),
});

router.post('/termAndCondition', function (req, res) {
    let validate = Joi.validate(req.body, termConditionSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            result: validate.error.details,
            code: 999
        });
    }

    const userInfo = req.userInfo;

    if (req.body.isAcceptCondition) {
        MemberModel.update({_id: userInfo._id}, {
            termAndCondition: true
        }, function (err, response) {
            if (err) {
                return res.send({message: "error", result: err, code: 999});
            }
            if (response.n === 1) {
                return res.send({message: "success", result: "success", code: 0});

            } else {
                return res.send({message: "success", result: "success", code: 0});
            }
        });
    } else {
        return res.send({message: "success", result: "success", code: 0});
    }
});


router.get('/credit', function (req, res) {


    if (req.userInfo.group.type === 'API') {


        AgentGroupModel.findById(req.userInfo.group._id, 'endpoint', function (err, agentResponse) {

            ApiService.getBalance(agentResponse.endpoint, req.userInfo.username, function (err, credit) {
                if (err)
                    return res.send({message: "error", result: err, code: 999});

                return res.send(
                    {
                        code: 0,
                        message: "success",
                        result: {
                            credit: credit
                        }
                    }
                );
            });
        });

    } else {

        MemberService.getCredit(req.userInfo._id, function (err, credit) {
            if (err) {
                return res.send({message: "error", result: err, code: 999});
            }

            return res.send({
                code: 0,
                message: "success",
                result: {
                    credit: credit
                }
            });
        });
    }

});


router.get('/profile', function (req, res) {

    let userInfo = req.userInfo;

    async.parallel([callback => {

        MemberService.getInfo(userInfo._id, function (err, credit) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, credit);
            }
        });
    }], function (err, asyncResponse) {
        if (err)
            return res.send({message: "error", result: err, code: 999});

        return res.send(
            {
                code: 0,
                message: "success",
                result: {
                    info: asyncResponse
                }
            }
        );
    });
});


const updateStatusGroupSchema = Joi.object().keys({
    suspend: Joi.boolean().required(),
    lock: Joi.boolean().required(),
    active: Joi.boolean().required(),
});


router.put('/status/:memberId', function (req, res) {

    let validate = Joi.validate(req.body, updateStatusGroupSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    let requestBody = req.body;
    const userInfo = req.userInfo;

    let body = {
        suspend: requestBody.suspend,
        lock: requestBody.lock,
        active: requestBody.active
    };

    if (!requestBody.active) {
        body.creditLimit = 0;
        body.balance = 0;
    }


    MemberModel.findOneAndUpdate({_id: mongoose.Types.ObjectId(req.params.memberId)}, body, {new: true}, function (err, data) {
        if (err) {
            return res.send({message: "error", result: err, code: 999});
        }

        if (requestBody.lock || !requestBody.active) {
            MemberService.clearMemberSession(data.username, (err, cResponse) => {

            });
        }

        if (requestBody.suspend || requestBody.lock || !requestBody.active) {
            MemberService.logoutAllPartnerGame(req.params.memberId, (err, response) => {
                console.log(response)
            });
        } else {
            MemberService.modifyMemberStateDreamGame(req.params.memberId, true, (err, response) => {
                console.log(response)
            });
        }

        let historyModel = new UpdateStatusHistoryModel(body);
        historyModel.id = req.params.memberId;
        historyModel.name = '';
        historyModel.type = 'MEMBER';
        historyModel.suspend = req.body.suspend;
        historyModel.lock = req.body.lock;
        historyModel.active = req.body.active;
        historyModel.createdBy = userInfo._id;
        historyModel.createdDate = DateUtils.getCurrentDate();
        historyModel.save((err, historyResponse) => {
            console.log(err)
            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: data
                }
            );
        });
    });

});


const updateAutoOddStatusSchema = Joi.object().keys({
    active: Joi.boolean().required(),
});


router.put('/autoOdd/:memberId', function (req, res) {

    let validate = Joi.validate(req.body, updateAutoOddStatusSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    let body = {
        isAutoChangeOdd: req.body.active,
    };

    MemberModel.update({_id: mongoose.Types.ObjectId(req.params.memberId)}, body, function (err, data) {
        if (err) {
            return res.send({message: "error", result: err, code: 999});
        }

        return res.send(
            {
                code: 0,
                message: "success",
                result: data
            }
        );
    });

});
// const updateBankSchema = Joi.object().keys({
//     name1: Joi.number().required(),
//     fromBank1: Joi.string().required(),
//     number1: Joi.string().required(),
//     name2: Joi.number().required(),
//     fromBank2: Joi.string().required(),
//     number2: Joi.string().required(),
//     name3: Joi.number().required(),
//     fromBank3: Joi.string().required(),
//     number3: Joi.string().required(),
//
// });
router.put('/config/cash/bank', (req, res) => {
    let userInfo = req.userInfo;

    let body = {
        name1: req.body.name1,
        bankId_1: req.body.bankId_1,
        number1: req.body.number1,

        name2: req.body.name2,
        bankId_2: req.body.bankId_2,
        number2: req.body.number2,

        name3: req.body.name3,
        bankId_3: req.body.bankId_3,
        number3: req.body.number3,

    };

    console.log('req : ', body);
    MemberService.updateBank(userInfo._id, body, (err, data) => {
        if (err) {
            return res.send({message: err, code: 999})
        } else if (!data) {
            return res.send({
                code: 999,
                message: 'fail'

            })
        }
        else {
            return res.send({
                code: 0,
                message: 'success',
                result: data
            })
        }
    })
});
router.get('/config/cash/bank', (req, res) => {
    let userInfo = req.userInfo;

    // let condition = {};
    // condition['memberId'] = mongoose.Types.ObjectId(userInfo._id);
    MemberModel.findOne({_id: userInfo._id}, {'configuration.bank': 1, _id: 0})
        .populate([{
            path: 'configuration.bank.bankId_1',
            model: 'Bank',
            select: 'bankName'
        },
            {
                path: 'configuration.bank.bankId_2',
                model: 'Bank',
                select: 'bankName'
            },
            {
                path: 'configuration.bank.bankId_3',
                model: 'Bank',
                select: 'bankName'
            }])
        .exec((err, results) => {
            if (err) {
                return res.send({message: "error", result: err, code: 999});
            } else {
                console.log(results, '0000000')
                return res.send(
                    {
                        code: 0,
                        message: "success",
                        result: {
                            bankList: [
                                {
                                    name: results.configuration.bank.name1 ? results.configuration.bank.name1 : '',
                                    number: results.configuration.bank.number1 ? results.configuration.bank.number1 : '',
                                    bank: results.configuration.bank.bankId_1 ? results.configuration.bank.bankId_1 : ''

                                },
                                {
                                    name: results.configuration.bank.name2 ? results.configuration.bank.name2 : '',
                                    number: results.configuration.bank.number2 ? results.configuration.bank.number2 : '',
                                    bank: results.configuration.bank.bankId_2 ? results.configuration.bank.bankId_2 : ''

                                },
                                {
                                    name: results.configuration.bank.name3 ? results.configuration.bank.name3 : '',
                                    number: results.configuration.bank.number3 ? results.configuration.bank.number3 : '',
                                    bank: results.configuration.bank.bankId_3 ? results.configuration.bank.bankId_3 : ''

                                },
                            ]
                        }

                    }
                );
            }
        })

});


const cloneMemberSchema = Joi.object().keys({
    gameSelected: Joi.string().allow(),
    shareSetting: Joi.object().allow(),
    limitSetting: Joi.object().allow(),
    commissionSetting: Joi.object().allow(),
    password: Joi.string().allow(''),
});

router.post('/clone-member', function (req, res) {

    let validate = Joi.validate(req.body, cloneMemberSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    let userInfo = req.userInfo;

    let requestBody = req.body;

    console.log(requestBody);

    async.series([callback => {

        UserModel.findById(userInfo._id, (err, agentResponse) => {

            console.log()
            if (agentResponse && (Hashing.encrypt256_1(req.body.password) === agentResponse.password)) {
                callback(null, agentResponse)
            } else {
                callback(1001, null);
            }
        });

    }, callback => {
        AgentGroupModel.findById(userInfo.group._id, function (err, response) {
            if (err)
                callback(err, null);
            else
                callback(null, response);
        });
    }], function (err, asyncResponse) {

        if (err) {
            if (err === 1001) {
                return res.send({
                    code: 1001,
                    message: "Invalid Password"
                });
            }
            return res.send({message: err, result: err, code: 999});
        }

        req.body.createByAgentName = userInfo.username
        req.body.createByAgentId = userInfo._id
        let model = CloneMemberHistory(req.body);
        model.actionBy = userInfo._id;
        model.createdDate = moment().utc(true).toDate();
        model.save(function (err, response) {
            console.log('save clone member history')
        });

        let currentAgentInfo = asyncResponse[1];

        let body = {
            shareSetting: requestBody.shareSetting,
            limitSetting: requestBody.limitSetting,
            commissionSetting: requestBody.commissionSetting
        };

        if (body.limitSetting.sportsBook.hdpOuOe.maxPerBet > currentAgentInfo.limitSetting.sportsBook.hdpOuOe.maxPerBet) {
            body.limitSetting.sportsBook.hdpOuOe.maxPerBet = currentAgentInfo.limitSetting.sportsBook.hdpOuOe.maxPerBet;
        }

        if (body.limitSetting.sportsBook.hdpOuOe.maxPerMatch > currentAgentInfo.limitSetting.sportsBook.hdpOuOe.maxPerMatch) {
            body.limitSetting.sportsBook.hdpOuOe.maxPerMatch = currentAgentInfo.limitSetting.sportsBook.hdpOuOe.maxPerMatch
        }
        if (body.limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet > currentAgentInfo.limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet) {
            body.limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet = currentAgentInfo.limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet
        }
        if (body.limitSetting.sportsBook.others.maxPerBet > currentAgentInfo.limitSetting.sportsBook.others.maxPerBet) {
            body.limitSetting.sportsBook.others.maxPerBet = currentAgentInfo.limitSetting.sportsBook.others.maxPerBet
        }

        if (body.limitSetting.sportsBook.outright.maxPerMatch > currentAgentInfo.limitSetting.sportsBook.outright.maxPerMatch) {
            body.limitSetting.sportsBook.outright.maxPerMatch = currentAgentInfo.limitSetting.sportsBook.outright.maxPerMatch
        }

        //PARLAY
        if (body.limitSetting.step.parlay.maxPerBet > currentAgentInfo.limitSetting.step.parlay.maxPerBet) {
            body.limitSetting.step.parlay.maxPerBet = currentAgentInfo.limitSetting.step.parlay.maxPerBet
        }
        if (body.limitSetting.step.parlay.minPerBet < currentAgentInfo.limitSetting.step.parlay.minPerBet) {
            body.limitSetting.step.parlay.minPerBet = currentAgentInfo.limitSetting.step.parlay.minPerBet
        }
        if (body.limitSetting.step.parlay.maxBetPerDay > currentAgentInfo.limitSetting.step.parlay.maxBetPerDay) {
            body.limitSetting.step.parlay.maxBetPerDay = currentAgentInfo.limitSetting.step.parlay.maxBetPerDay
        }
        if (body.limitSetting.step.parlay.maxPayPerBill > currentAgentInfo.limitSetting.step.parlay.maxPayPerBill) {
            body.limitSetting.step.parlay.maxPayPerBill = currentAgentInfo.limitSetting.step.parlay.maxPayPerBill
        }

        //STEP
        if (body.limitSetting.step.step.maxPerBet > currentAgentInfo.limitSetting.step.step.maxPerBet) {
            body.limitSetting.step.step.maxPerBet = currentAgentInfo.limitSetting.step.step.maxPerBet
        }
        if (body.limitSetting.step.step.minPerBet < currentAgentInfo.limitSetting.step.step.minPerBet) {
            body.limitSetting.step.step.minPerBet = currentAgentInfo.limitSetting.step.step.minPerBet
        }
        if (body.limitSetting.step.step.maxBetPerDay > currentAgentInfo.limitSetting.step.step.maxBetPerDay) {
            body.limitSetting.step.step.maxBetPerDay = currentAgentInfo.limitSetting.step.step.maxBetPerDay
        }
        if (body.limitSetting.step.step.maxPayPerBill > currentAgentInfo.limitSetting.step.step.maxPayPerBill) {
            body.limitSetting.step.step.maxPayPerBill = currentAgentInfo.limitSetting.step.step.maxPayPerBill
        }

        //OTHER

        if (body.limitSetting.other.m2.maxPerBetHDP > currentAgentInfo.limitSetting.other.m2.maxPerBetHDP) {
            body.limitSetting.other.m2.maxPerBetHDP = currentAgentInfo.limitSetting.other.m2.maxPerBetHDP
        }
        if (body.limitSetting.other.m2.minPerBetHDP < currentAgentInfo.limitSetting.other.m2.minPerBetHDP) {
            body.limitSetting.other.m2.minPerBetHDP = currentAgentInfo.limitSetting.other.m2.minPerBetHDP
        }
        if (body.limitSetting.other.m2.maxBetPerMatchHDP > currentAgentInfo.limitSetting.other.m2.maxBetPerMatchHDP) {
            body.limitSetting.other.m2.maxBetPerMatchHDP = currentAgentInfo.limitSetting.other.m2.maxBetPerMatchHDP
        }


        if (body.limitSetting.other.m2.maxPerBet > currentAgentInfo.limitSetting.other.m2.maxPerBet) {
            body.limitSetting.other.m2.maxPerBet = currentAgentInfo.limitSetting.other.m2.maxPerBet
        }
        if (body.limitSetting.other.m2.minPerBet < currentAgentInfo.limitSetting.other.m2.minPerBet) {
            body.limitSetting.other.m2.minPerBet = currentAgentInfo.limitSetting.other.m2.minPerBet
        }
        if (body.limitSetting.other.m2.maxBetPerDay > currentAgentInfo.limitSetting.other.m2.maxBetPerDay) {
            body.limitSetting.other.m2.maxBetPerDay = currentAgentInfo.limitSetting.other.m2.maxBetPerDay
        }
        if (body.limitSetting.other.m2.maxPayPerBill > currentAgentInfo.limitSetting.other.m2.maxPayPerBill) {
            body.limitSetting.other.m2.maxPayPerBill = currentAgentInfo.limitSetting.other.m2.maxPayPerBill
        }


        //TODO
        if (body.limitSetting.lotto.amb._6TOP.max > currentAgentInfo.limitSetting.lotto.amb._6TOP.max) {
            body.limitSetting.lotto.amb._6TOP.max = currentAgentInfo.limitSetting.lotto.amb._6TOP.max
        }

        if (body.limitSetting.lotto.amb._5TOP.max > currentAgentInfo.limitSetting.lotto.amb._5TOP.max) {
            body.limitSetting.lotto.amb._5TOP.max = currentAgentInfo.limitSetting.lotto.amb._5TOP.max
        }

        if (body.limitSetting.lotto.amb._4TOP.max > currentAgentInfo.limitSetting.lotto.amb._4TOP.max) {
            body.limitSetting.lotto.amb._4TOP.max = currentAgentInfo.limitSetting.lotto.amb._4TOP.max
        }

        if (body.limitSetting.lotto.amb._4TOD.max > currentAgentInfo.limitSetting.lotto.amb._4TOD.max) {
            body.limitSetting.lotto.amb._4TOD.max = currentAgentInfo.limitSetting.lotto.amb._4TOD.max
        }

        if (body.limitSetting.lotto.amb._3TOP.max > currentAgentInfo.limitSetting.lotto.amb._3TOP.max) {
            body.limitSetting.lotto.amb._3TOP.max = currentAgentInfo.limitSetting.lotto.amb._3TOP.max
        }

        if (body.limitSetting.lotto.amb._3TOD.max > currentAgentInfo.limitSetting.lotto.amb._3TOD.max) {
            body.limitSetting.lotto.amb._3TOD.max = currentAgentInfo.limitSetting.lotto.amb._3TOD.max
        }

        if (body.limitSetting.lotto.amb._3BOT.max > currentAgentInfo.limitSetting.lotto.amb._3BOT.max) {
            body.limitSetting.lotto.amb._3BOT.max = currentAgentInfo.limitSetting.lotto.amb._3BOT.max
        }

        if (body.limitSetting.lotto.amb._3TOP_OE.max > currentAgentInfo.limitSetting.lotto.amb._3TOP_OE.max) {
            body.limitSetting.lotto.amb._3TOP_OE.max = currentAgentInfo.limitSetting.lotto.amb._3TOP_OE.max
        }

        if (body.limitSetting.lotto.amb._3TOP_OU.max > currentAgentInfo.limitSetting.lotto.amb._3TOP_OU.max) {
            body.limitSetting.lotto.amb._3TOP_OU.max = currentAgentInfo.limitSetting.lotto.amb._3TOP_OU.max
        }

        if (body.limitSetting.lotto.amb._2TOP.max > currentAgentInfo.limitSetting.lotto.amb._2TOP.max) {
            body.limitSetting.lotto.amb._2TOP.max = currentAgentInfo.limitSetting.lotto.amb._2TOP.max
        }

        if (body.limitSetting.lotto.amb._2TOD.max > currentAgentInfo.limitSetting.lotto.amb._2TOD.max) {
            body.limitSetting.lotto.amb._2TOD.max = currentAgentInfo.limitSetting.lotto.amb._2TOD.max
        }

        if (body.limitSetting.lotto.amb._2BOT.max > currentAgentInfo.limitSetting.lotto.amb._2BOT.max) {
            body.limitSetting.lotto.amb._2BOT.max = currentAgentInfo.limitSetting.lotto.amb._2BOT.max
        }

        if (body.limitSetting.lotto.amb._2TOP_OE.max > currentAgentInfo.limitSetting.lotto.amb._2TOP_OE.max) {
            body.limitSetting.lotto.amb._2TOP_OE.max = currentAgentInfo.limitSetting.lotto.amb._2TOP_OE.max
        }

        if (body.limitSetting.lotto.amb._2TOP_OU.max > currentAgentInfo.limitSetting.lotto.amb._2TOP_OU.max) {
            body.limitSetting.lotto.amb._2TOP_OU.max = currentAgentInfo.limitSetting.lotto.amb._2TOP_OU.max
        }


        if (body.limitSetting.lotto.amb._2BOT_OE.max > currentAgentInfo.limitSetting.lotto.amb._2BOT_OE.max) {
            body.limitSetting.lotto.amb._2BOT_OE.max = currentAgentInfo.limitSetting.lotto.amb._2BOT_OE.max
        }

        if (body.limitSetting.lotto.amb._2BOT_OU.max > currentAgentInfo.limitSetting.lotto.amb._2BOT_OU.max) {
            body.limitSetting.lotto.amb._2BOT_OU.max = currentAgentInfo.limitSetting.lotto.amb._2BOT_OU.max
        }


        if (body.limitSetting.lotto.amb._1TOP.max > currentAgentInfo.limitSetting.lotto.amb._1TOP.max) {
            body.limitSetting.lotto.amb._1TOP.max = currentAgentInfo.limitSetting.lotto.amb._1TOP.max
        }

        if (body.limitSetting.lotto.amb._1BOT.max > currentAgentInfo.limitSetting.lotto.amb._1BOT.max) {
            body.limitSetting.lotto.amb._1BOT.max = currentAgentInfo.limitSetting.lotto.amb._1BOT.max
        }


        //PP
        if (body.limitSetting.lotto.pp._6TOP.max > currentAgentInfo.limitSetting.lotto.pp._6TOP.max) {
            body.limitSetting.lotto.pp._6TOP.max = currentAgentInfo.limitSetting.lotto.pp._6TOP.max
        }

        if (body.limitSetting.lotto.pp._5TOP.max > currentAgentInfo.limitSetting.lotto.pp._5TOP.max) {
            body.limitSetting.lotto.pp._5TOP.max = currentAgentInfo.limitSetting.lotto.pp._5TOP.max
        }

        if (body.limitSetting.lotto.pp._4TOP.max > currentAgentInfo.limitSetting.lotto.pp._4TOP.max) {
            body.limitSetting.lotto.pp._4TOP.max = currentAgentInfo.limitSetting.lotto.pp._4TOP.max
        }

        if (body.limitSetting.lotto.pp._4TOD.max > currentAgentInfo.limitSetting.lotto.pp._4TOD.max) {
            body.limitSetting.lotto.pp._4TOD.max = currentAgentInfo.limitSetting.lotto.pp._4TOD.max
        }

        if (body.limitSetting.lotto.pp._3TOP.max > currentAgentInfo.limitSetting.lotto.pp._3TOP.max) {
            body.limitSetting.lotto.pp._3TOP.max = currentAgentInfo.limitSetting.lotto.pp._3TOP.max
        }

        if (body.limitSetting.lotto.pp._3TOD.max > currentAgentInfo.limitSetting.lotto.pp._3TOD.max) {
            body.limitSetting.lotto.pp._3TOD.max = currentAgentInfo.limitSetting.lotto.pp._3TOD.max
        }

        if (body.limitSetting.lotto.pp._2TOP.max > currentAgentInfo.limitSetting.lotto.pp._2TOP.max) {
            body.limitSetting.lotto.pp._2TOP.max = currentAgentInfo.limitSetting.lotto.pp._2TOP.max
        }

        if (body.limitSetting.lotto.pp._2TOD.max > currentAgentInfo.limitSetting.lotto.pp._2TOD.max) {
            body.limitSetting.lotto.pp._2TOD.max = currentAgentInfo.limitSetting.lotto.pp._2TOD.max
        }

        if (body.limitSetting.lotto.pp._2BOT.max > currentAgentInfo.limitSetting.lotto.pp._2BOT.max) {
            body.limitSetting.lotto.pp._2BOT.max = currentAgentInfo.limitSetting.lotto.pp._2BOT.max
        }

        if (body.limitSetting.lotto.pp._1TOP.max > currentAgentInfo.limitSetting.lotto.pp._1TOP.max) {
            body.limitSetting.lotto.pp._1TOP.max = currentAgentInfo.limitSetting.lotto.pp._1TOP.max
        }

        if (body.limitSetting.lotto.pp._1BOT.max > currentAgentInfo.limitSetting.lotto.pp._1BOT.max) {
            body.limitSetting.lotto.pp._1BOT.max = currentAgentInfo.limitSetting.lotto.pp._1BOT.max
        }


        if ((requestBody.shareSetting.sportsBook.today.parent ) < currentAgentInfo.shareSetting.sportsBook.today.min) {
            return res.send({
                message: "Agent Share (Sport Today) + Agent Max Share (Sport Today) should be greater than Agent Min Shares (Sport Today)",
                result: err,
                code: 4003
            });
        }

        if ((requestBody.shareSetting.sportsBook.live.parent ) < currentAgentInfo.shareSetting.sportsBook.live.min) {
            return res.send({
                message: "Agent Share (Sport Live) + Agent Max Share (Sport Live) should be greater than Agent Min Shares (Sport Live)",
                result: err,
                code: 4004
            });
        }

        if ((requestBody.shareSetting.step.parlay.parent ) < currentAgentInfo.shareSetting.step.parlay.min) {
            return res.send({
                message: "Agent Share (Step Parlay) + Agent Max Share (Step Parlay) should be greater than Agent Min Shares (Step Parlay)",
                result: err,
                code: 4005
            });
        }

        if ((requestBody.shareSetting.casino.sexy.parent ) < currentAgentInfo.shareSetting.casino.sexy.min) {
            return res.send({
                message: "Agent Share (Sexy Baccarat) + Agent Max Share (Sexy Baccarat) should be greater than Agent Min Shares (Sexy Baccarat)",
                result: err,
                code: 4006
            });
        }

        if ((requestBody.shareSetting.other.m2.parent ) < currentAgentInfo.shareSetting.other.m2.min) {
            return res.send({
                message: "Agent Share (M2 Sport) + Agent Max Share (M2 Sport) should be greater than Agent Min Shares (M2 Sport)",
                result: err,
                code: 4007
            });
        }

        if ((requestBody.shareSetting.step.step.parent ) < currentAgentInfo.shareSetting.step.step.min) {
            return res.send({
                message: "Agent Share (Mix Step) + Agent Max Share (Mix Step) should be greater than Agent Min Shares (Mix Step)",
                result: err,
                code: 4008
            });
        }

        // if ((requestBody.shareSetting.casino.allbet.own + requestBody.shareSetting.casino.allbet.parent ) < currentAgentInfo.shareSetting.casino.allbet.min) {
        //     return res.send({
        //         message: "Agent Share (All Bet) + Agent Max Share (All Bet) should be greater than Agent Min Shares (All Bet)",
        //         result: err,
        //         code: 4008
        //     });
        // }

        if ((requestBody.shareSetting.game.slotXO.parent ) < currentAgentInfo.shareSetting.game.slotXO.min) {
            return res.send({
                message: "Agent Share (Slot XO) + Agent Max Share (Slot XO) should be greater than Agent Min Shares (Slot XO)",
                result: err,
                code: 4009
            });
        }

        if ((requestBody.shareSetting.casino.sa.parent ) < currentAgentInfo.shareSetting.casino.sa.min) {
            return res.send({
                message: "Agent Share (SA Gaming) + Agent Max Share (SA Gaming) should be greater than Agent Min Shares (SA Gaming)",
                result: err,
                code: 4011
            });
        }

        if ((requestBody.shareSetting.casino.dg.parent ) < currentAgentInfo.shareSetting.casino.dg.min) {
            return res.send({
                message: "Agent Share (DG Gaming) + Agent Max Share (DG Gaming) should be greater than Agent Min Shares (DG Gaming)",
                result: err,
                code: 4012
            });
        }

        if ((requestBody.shareSetting.casino.ag.parent ) < currentAgentInfo.shareSetting.casino.ag.min) {
            return res.send({
                message: "Agent Share (AG Gaming) + Agent Max Share (AG Gaming) should be greater than Agent Min Shares (AG Gaming)",
                result: err,
                code: 4013
            });
        }

        if ((requestBody.shareSetting.multi.amb.parent ) < currentAgentInfo.shareSetting.multi.amb.min) {
            return res.send({
                message: "Agent Share (AMB Game) + Agent Max Share (AMB Game) should be greater than Agent Min Shares (AMB Game)",
                result: err,
                code: 4014
            });
        }

        if ((requestBody.shareSetting.casino.pt.parent ) < currentAgentInfo.shareSetting.casino.pt.min) {
            return res.send({
                message: "Agent Share (Pretty Gaming) + Agent Max Share (Pretty Gaming) should be greater than Agent Min Shares (Pretty Gaming)",
                result: err,
                code: 4015
            });
        }

        let updateBody = {
            $set: {}
        }

        let gameList = requestBody.gameSelected.split(',');

        console.log('gameList : ', gameList)

        if (gameList.includes('FOOTBALL')) {
            updateBody.$set['shareSetting.sportsBook.today.parent'] = requestBody.shareSetting.sportsBook.today.parent;
            updateBody.$set['shareSetting.sportsBook.live.parent'] = requestBody.shareSetting.sportsBook.live.parent;

            updateBody.$set['limitSetting.sportsBook.hdpOuOe.maxPerBet'] = requestBody.limitSetting.sportsBook.hdpOuOe.maxPerBet;
            updateBody.$set['limitSetting.sportsBook.hdpOuOe.maxPerMatch'] = requestBody.limitSetting.sportsBook.hdpOuOe.maxPerMatch;
            updateBody.$set['limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet'] = requestBody.limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet;
            updateBody.$set['limitSetting.sportsBook.oneTwoDoubleChance.maxPerMatch'] = requestBody.limitSetting.sportsBook.oneTwoDoubleChance.maxPerMatch;
            updateBody.$set['limitSetting.sportsBook.others.maxPerBet'] = requestBody.limitSetting.sportsBook.others.maxPerBet;
            updateBody.$set['limitSetting.sportsBook.others.maxPerMatch'] = requestBody.limitSetting.sportsBook.others.maxPerMatch;
            updateBody.$set['limitSetting.sportsBook.outright.maxPerBet'] = requestBody.limitSetting.sportsBook.outright.maxPerBet;
            updateBody.$set['limitSetting.sportsBook.outright.maxPerMatch'] = requestBody.limitSetting.sportsBook.outright.maxPerMatch;
            updateBody.$set['limitSetting.sportsBook.isEnable'] = requestBody.limitSetting.sportsBook.isEnable;

            updateBody.$set['commissionSetting.sportsBook.typeHdpOuOe'] = requestBody.commissionSetting.sportsBook.typeHdpOuOe;
            updateBody.$set['commissionSetting.sportsBook.hdpOuOe'] = requestBody.commissionSetting.sportsBook.hdpOuOe;
            updateBody.$set['commissionSetting.sportsBook.oneTwoDoubleChance'] = requestBody.commissionSetting.sportsBook.oneTwoDoubleChance;
            updateBody.$set['commissionSetting.sportsBook.others'] = requestBody.commissionSetting.sportsBook.others;
        }

        if (gameList.includes('STEP')) {
            updateBody.$set['shareSetting.step.step.parent'] = requestBody.shareSetting.step.step.parent;

            updateBody.$set['limitSetting.step.step.maxPerBet'] = requestBody.limitSetting.step.step.maxPerBet;
            updateBody.$set['limitSetting.step.step.minPerBet'] = requestBody.limitSetting.step.step.minPerBet;
            updateBody.$set['limitSetting.step.step.maxBetPerDay'] = requestBody.limitSetting.step.step.maxBetPerDay;
            updateBody.$set['limitSetting.step.step.maxPayPerBill'] = requestBody.limitSetting.step.step.maxPayPerBill;
            updateBody.$set['limitSetting.step.step.maxMatchPerBet'] = requestBody.limitSetting.step.step.maxMatchPerBet;
            updateBody.$set['limitSetting.step.step.minMatchPerBet'] = requestBody.limitSetting.step.step.minMatchPerBet;
            updateBody.$set['limitSetting.step.step.isEnable'] = requestBody.limitSetting.step.step.isEnable;

            updateBody.$set['commissionSetting.step.com2'] = requestBody.commissionSetting.step.com2;
            updateBody.$set['commissionSetting.step.com3'] = requestBody.commissionSetting.step.com3;
            updateBody.$set['commissionSetting.step.com4'] = requestBody.commissionSetting.step.com4;
            updateBody.$set['commissionSetting.step.com5'] = requestBody.commissionSetting.step.com5;
            updateBody.$set['commissionSetting.step.com6'] = requestBody.commissionSetting.step.com6;
            updateBody.$set['commissionSetting.step.com7'] = requestBody.commissionSetting.step.com7;
            updateBody.$set['commissionSetting.step.com8'] = requestBody.commissionSetting.step.com8;
            updateBody.$set['commissionSetting.step.com9'] = requestBody.commissionSetting.step.com9;
            updateBody.$set['commissionSetting.step.com10'] = requestBody.commissionSetting.step.com10;
            updateBody.$set['commissionSetting.step.com11'] = requestBody.commissionSetting.step.com11;
            updateBody.$set['commissionSetting.step.com12'] = requestBody.commissionSetting.step.com12;
        }

        if (gameList.includes('PARLAY')) {
            updateBody.$set['shareSetting.step.parlay.parent'] = requestBody.shareSetting.step.parlay.parent;

            updateBody.$set['limitSetting.step.parlay.maxPerBet'] = requestBody.limitSetting.step.parlay.maxPerBet;
            updateBody.$set['limitSetting.step.parlay.minPerBet'] = requestBody.limitSetting.step.parlay.minPerBet;
            updateBody.$set['limitSetting.step.parlay.maxBetPerDay'] = requestBody.limitSetting.step.parlay.maxBetPerDay;
            updateBody.$set['limitSetting.step.parlay.maxPayPerBill'] = requestBody.limitSetting.step.parlay.maxPayPerBill;
            updateBody.$set['limitSetting.step.parlay.maxMatchPerBet'] = requestBody.limitSetting.step.parlay.maxMatchPerBet;
            updateBody.$set['limitSetting.step.parlay.minMatchPerBet'] = requestBody.limitSetting.step.parlay.minMatchPerBet;
            updateBody.$set['limitSetting.step.parlay.isEnable'] = requestBody.limitSetting.step.parlay.isEnable;

            updateBody.$set['commissionSetting.parlay.com'] = requestBody.commissionSetting.parlay.com;
        }

        if (gameList.includes('CASINO')) {
            updateBody.$set['shareSetting.casino.sexy.parent'] = requestBody.shareSetting.casino.sexy.parent;
            updateBody.$set['shareSetting.casino.ag.parent'] = requestBody.shareSetting.casino.ag.parent;
            updateBody.$set['shareSetting.casino.sa.parent'] = requestBody.shareSetting.casino.sa.parent;
            updateBody.$set['shareSetting.casino.dg.parent'] = requestBody.shareSetting.casino.dg.parent;
            updateBody.$set['shareSetting.casino.pt.parent'] = requestBody.shareSetting.casino.pt.parent;

            updateBody.$set['limitSetting.casino.isWlEnable'] = requestBody.limitSetting.casino.isWlEnable;
            updateBody.$set['limitSetting.casino.winPerDay'] = requestBody.limitSetting.casino.winPerDay;
            updateBody.$set['limitSetting.casino.sexy.isEnable'] = requestBody.limitSetting.casino.sexy.isEnable;
            updateBody.$set['limitSetting.casino.sexy.limit'] = requestBody.limitSetting.casino.sexy.limit;
            updateBody.$set['limitSetting.casino.ag.isEnable'] = requestBody.limitSetting.casino.ag.isEnable;
            updateBody.$set['limitSetting.casino.ag.limit'] = requestBody.limitSetting.casino.ag.limit;
            updateBody.$set['limitSetting.casino.sa.isEnable'] = requestBody.limitSetting.casino.sa.isEnable;
            updateBody.$set['limitSetting.casino.sa.limit'] = requestBody.limitSetting.casino.sa.limit;
            updateBody.$set['limitSetting.casino.dg.isEnable'] = requestBody.limitSetting.casino.dg.isEnable;
            updateBody.$set['limitSetting.casino.dg.limit'] = requestBody.limitSetting.casino.dg.limit;
            updateBody.$set['limitSetting.casino.pt.isEnable'] = requestBody.limitSetting.casino.pt.isEnable;
            updateBody.$set['limitSetting.casino.pt.limit'] = requestBody.limitSetting.casino.pt.limit;

            updateBody.$set['commissionSetting.casino.sexy'] = requestBody.commissionSetting.casino.sexy;
            updateBody.$set['commissionSetting.casino.ag'] = requestBody.commissionSetting.casino.ag;
            updateBody.$set['commissionSetting.casino.sa'] = requestBody.commissionSetting.casino.sa;
            updateBody.$set['commissionSetting.casino.dg'] = requestBody.commissionSetting.casino.dg;
            updateBody.$set['commissionSetting.casino.pt'] = requestBody.commissionSetting.casino.pt;
        }

        if (gameList.includes('GAME')) {
            updateBody.$set['shareSetting.game.slotXO.parent'] = requestBody.shareSetting.game.slotXO.parent;

            updateBody.$set['limitSetting.game.slotXO.isEnable'] = requestBody.limitSetting.game.slotXO.isEnable;

            updateBody.$set['commissionSetting.game.slotXO'] = requestBody.commissionSetting.game.slotXO;

        }

        if (gameList.includes('LOTTO')) {
            updateBody.$set['shareSetting.lotto.amb.parent'] = requestBody.shareSetting.lotto.amb.parent;

            updateBody.$set['limitSetting.lotto.amb.isEnable'] = requestBody.limitSetting.lotto.amb.isEnable;
            updateBody.$set['limitSetting.lotto.amb.hour'] = requestBody.limitSetting.lotto.amb.hour;
            updateBody.$set['limitSetting.lotto.amb.minute'] = requestBody.limitSetting.lotto.amb.minute;
            //
            updateBody.$set['limitSetting.lotto.amb._6TOP.payout'] = requestBody.limitSetting.lotto.amb._6TOP.payout;
            updateBody.$set['limitSetting.lotto.amb._6TOP.discount'] = requestBody.limitSetting.lotto.amb._6TOP.discount;
            updateBody.$set['limitSetting.lotto.amb._6TOP.max'] = requestBody.limitSetting.lotto.amb._6TOP.max;
            //
            updateBody.$set['limitSetting.lotto.amb._5TOP.payout'] = requestBody.limitSetting.lotto.amb._5TOP.payout;
            updateBody.$set['limitSetting.lotto.amb._5TOP.discount'] = requestBody.limitSetting.lotto.amb._5TOP.discount;
            updateBody.$set['limitSetting.lotto.amb._5TOP.max'] = requestBody.limitSetting.lotto.amb._5TOP.max;
            //
            updateBody.$set['limitSetting.lotto.amb._4TOP.payout'] = requestBody.limitSetting.lotto.amb._4TOP.payout;
            updateBody.$set['limitSetting.lotto.amb._4TOP.discount'] = requestBody.limitSetting.lotto.amb._4TOP.discount;
            updateBody.$set['limitSetting.lotto.amb._4TOP.max'] = requestBody.limitSetting.lotto.amb._4TOP.max;
            //
            updateBody.$set['limitSetting.lotto.amb._4TOD.payout'] = requestBody.limitSetting.lotto.amb._4TOD.payout;
            updateBody.$set['limitSetting.lotto.amb._4TOD.discount'] = requestBody.limitSetting.lotto.amb._4TOD.discount;
            updateBody.$set['limitSetting.lotto.amb._4TOD.max'] = requestBody.limitSetting.lotto.amb._4TOD.max;
            //
            updateBody.$set['limitSetting.lotto.amb._3TOP.payout'] = requestBody.limitSetting.lotto.amb._3TOP.payout;
            updateBody.$set['limitSetting.lotto.amb._3TOP.discount'] = requestBody.limitSetting.lotto.amb._3TOP.discount;
            updateBody.$set['limitSetting.lotto.amb._3TOP.max'] = requestBody.limitSetting.lotto.amb._3TOP.max;
            //
            updateBody.$set['limitSetting.lotto.amb._3TOD.payout'] = requestBody.limitSetting.lotto.amb._3TOD.payout;
            updateBody.$set['limitSetting.lotto.amb._3TOD.discount'] = requestBody.limitSetting.lotto.amb._3TOD.discount;
            updateBody.$set['limitSetting.lotto.amb._3TOD.max'] = requestBody.limitSetting.lotto.amb._3TOD.max;
            //
            updateBody.$set['limitSetting.lotto.amb._3BOT.payout'] = requestBody.limitSetting.lotto.amb._3BOT.payout;
            updateBody.$set['limitSetting.lotto.amb._3BOT.discount'] = requestBody.limitSetting.lotto.amb._3BOT.discount;
            updateBody.$set['limitSetting.lotto.amb._3BOT.max'] = requestBody.limitSetting.lotto.amb._3BOT.max;
            //
            updateBody.$set['limitSetting.lotto.amb._3TOP_OE.payout'] = requestBody.limitSetting.lotto.amb._3TOP_OE.payout;
            updateBody.$set['limitSetting.lotto.amb._3TOP_OE.discount'] = requestBody.limitSetting.lotto.amb._3TOP_OE.discount;
            updateBody.$set['limitSetting.lotto.amb._3TOP_OE.max'] = requestBody.limitSetting.lotto.amb._3TOP_OE.max;
            //
            updateBody.$set['limitSetting.lotto.amb._3TOP_OU.payout'] = requestBody.limitSetting.lotto.amb._3TOP_OU.payout;
            updateBody.$set['limitSetting.lotto.amb._3TOP_OU.discount'] = requestBody.limitSetting.lotto.amb._3TOP_OU.discount;
            updateBody.$set['limitSetting.lotto.amb._3TOP_OU.max'] = requestBody.limitSetting.lotto.amb._3TOP_OU.max;
            //
            updateBody.$set['limitSetting.lotto.amb._2TOP.payout'] = requestBody.limitSetting.lotto.amb._2TOP.payout;
            updateBody.$set['limitSetting.lotto.amb._2TOP.discount'] = requestBody.limitSetting.lotto.amb._2TOP.discount;
            updateBody.$set['limitSetting.lotto.amb._2TOP.max'] = requestBody.limitSetting.lotto.amb._2TOP.max;
            //
            updateBody.$set['limitSetting.lotto.amb._2TOD.payout'] = requestBody.limitSetting.lotto.amb._2TOD.payout;
            updateBody.$set['limitSetting.lotto.amb._2TOD.discount'] = requestBody.limitSetting.lotto.amb._2TOD.discount;
            updateBody.$set['limitSetting.lotto.amb._2TOD.max'] = requestBody.limitSetting.lotto.amb._2TOD.max;
            //
            updateBody.$set['limitSetting.lotto.amb._2BOT.payout'] = requestBody.limitSetting.lotto.amb._2BOT.payout;
            updateBody.$set['limitSetting.lotto.amb._2BOT.discount'] = requestBody.limitSetting.lotto.amb._2BOT.discount;
            updateBody.$set['limitSetting.lotto.amb._2BOT.max'] = requestBody.limitSetting.lotto.amb._2BOT.max;
            //
            updateBody.$set['limitSetting.lotto.amb._2TOP_OE.payout'] = requestBody.limitSetting.lotto.amb._2TOP_OE.payout;
            updateBody.$set['limitSetting.lotto.amb._2TOP_OE.discount'] = requestBody.limitSetting.lotto.amb._2TOP_OE.discount;
            updateBody.$set['limitSetting.lotto.amb._2TOP_OE.max'] = requestBody.limitSetting.lotto.amb._2TOP_OE.max;
            //
            updateBody.$set['limitSetting.lotto.amb._2TOP_OU.payout'] = requestBody.limitSetting.lotto.amb._2TOP_OU.payout;
            updateBody.$set['limitSetting.lotto.amb._2TOP_OU.discount'] = requestBody.limitSetting.lotto.amb._2TOP_OU.discount;
            updateBody.$set['limitSetting.lotto.amb._2TOP_OU.max'] = requestBody.limitSetting.lotto.amb._2TOP_OU.max;
            //
            updateBody.$set['limitSetting.lotto.amb._2BOT_OE.payout'] = requestBody.limitSetting.lotto.amb._2BOT_OE.payout;
            updateBody.$set['limitSetting.lotto.amb._2BOT_OE.discount'] = requestBody.limitSetting.lotto.amb._2BOT_OE.discount;
            updateBody.$set['limitSetting.lotto.amb._2BOT_OE.max'] = requestBody.limitSetting.lotto.amb._2BOT_OE.max;
            //
            updateBody.$set['limitSetting.lotto.amb._2BOT_OU.payout'] = requestBody.limitSetting.lotto.amb._2BOT_OU.payout;
            updateBody.$set['limitSetting.lotto.amb._2BOT_OU.discount'] = requestBody.limitSetting.lotto.amb._2BOT_OU.discount;
            updateBody.$set['limitSetting.lotto.amb._2BOT_OU.max'] = requestBody.limitSetting.lotto.amb._2BOT_OU.max;
            //
            updateBody.$set['limitSetting.lotto.amb._1TOP.payout'] = requestBody.limitSetting.lotto.amb._1TOP.payout;
            updateBody.$set['limitSetting.lotto.amb._1TOP.discount'] = requestBody.limitSetting.lotto.amb._1TOP.discount;
            updateBody.$set['limitSetting.lotto.amb._1TOP.max'] = requestBody.limitSetting.lotto.amb._1TOP.max;
            //
            updateBody.$set['limitSetting.lotto.amb._1BOT.payout'] = requestBody.limitSetting.lotto.amb._1BOT.payout;
            updateBody.$set['limitSetting.lotto.amb._1BOT.discount'] = requestBody.limitSetting.lotto.amb._1BOT.discount;
            updateBody.$set['limitSetting.lotto.amb._1BOT.max'] = requestBody.limitSetting.lotto.amb._1BOT.max;
        }

        if (gameList.includes('LOTTO_LAOS')) {
            updateBody.$set['shareSetting.lotto.laos.parent'] = requestBody.shareSetting.lotto.laos.parent;

            updateBody.$set['limitSetting.lotto.laos.hour'] = requestBody.limitSetting.lotto.laos.hour;
            updateBody.$set['limitSetting.lotto.laos.minute'] = requestBody.limitSetting.lotto.laos.minute;
            updateBody.$set['limitSetting.lotto.laos.L_4TOP.payout'] = requestBody.limitSetting.lotto.laos.L_4TOP.payout;
            updateBody.$set['limitSetting.lotto.laos.L_4TOP.discount'] = requestBody.limitSetting.lotto.laos.L_4TOP.discount;
            updateBody.$set['limitSetting.lotto.laos.L_4TOP.max'] = requestBody.limitSetting.lotto.laos.L_4TOP.max;
            //         //
            updateBody.$set['limitSetting.lotto.laos.L_4TOD.payout'] = requestBody.limitSetting.lotto.laos.L_4TOD.payout;
            updateBody.$set['limitSetting.lotto.laos.L_4TOD.discount'] = requestBody.limitSetting.lotto.laos.L_4TOD.discount;
            updateBody.$set['limitSetting.lotto.laos.L_4TOD.max'] = requestBody.limitSetting.lotto.laos.L_4TOD.max;
            //         //
            updateBody.$set['limitSetting.lotto.laos.L_3TOP.payout'] = requestBody.limitSetting.lotto.laos.L_3TOP.payout;
            updateBody.$set['limitSetting.lotto.laos.L_3TOP.discount'] = requestBody.limitSetting.lotto.laos.L_3TOP.discount;
            updateBody.$set['limitSetting.lotto.laos.L_3TOP.max'] = requestBody.limitSetting.lotto.laos.L_3TOP.max;
            //         //
            updateBody.$set['limitSetting.lotto.laos.L_3TOD.payout'] = requestBody.limitSetting.lotto.laos.L_3TOD.payout;
            updateBody.$set['limitSetting.lotto.laos.L_3TOD.discount'] = requestBody.limitSetting.lotto.laos.L_3TOD.discount;
            updateBody.$set['limitSetting.lotto.laos.L_3TOD.max'] = requestBody.limitSetting.lotto.laos.L_3TOD.max;
            //         //
            updateBody.$set['limitSetting.lotto.laos.L_2FB.payout'] = requestBody.limitSetting.lotto.laos.L_2FB.payout;
            updateBody.$set['limitSetting.lotto.laos.L_2FB.discount'] = requestBody.limitSetting.lotto.laos.L_2FB.discount;
            updateBody.$set['limitSetting.lotto.laos.L_2FB.max'] = requestBody.limitSetting.lotto.laos.L_2FB.max;
            //
            //
            updateBody.$set['limitSetting.lotto.laos.T_4TOP.payout'] = requestBody.limitSetting.lotto.laos.T_4TOP.payout;
            updateBody.$set['limitSetting.lotto.laos.T_4TOP.discount'] = requestBody.limitSetting.lotto.laos.T_4TOP.discount;
            updateBody.$set['limitSetting.lotto.laos.T_4TOP.max'] = requestBody.limitSetting.lotto.laos.T_4TOP.max;
            //         //
            updateBody.$set['limitSetting.lotto.laos.T_4TOD.payout'] = requestBody.limitSetting.lotto.laos.T_4TOD.payout;
            updateBody.$set['limitSetting.lotto.laos.T_4TOD.discount'] = requestBody.limitSetting.lotto.laos.T_4TOD.discount;
            updateBody.$set['limitSetting.lotto.laos.T_4TOD.max'] = requestBody.limitSetting.lotto.laos.T_4TOD.max;
            //         //
            updateBody.$set['limitSetting.lotto.laos.T_3TOP.payout'] = requestBody.limitSetting.lotto.laos.T_3TOP.payout;
            updateBody.$set['limitSetting.lotto.laos.T_3TOP.discount'] = requestBody.limitSetting.lotto.laos.T_3TOP.discount;
            updateBody.$set['limitSetting.lotto.laos.T_3TOP.max'] = requestBody.limitSetting.lotto.laos.T_3TOP.max;
            //         //
            updateBody.$set['limitSetting.lotto.laos.T_3TOD.payout'] = requestBody.limitSetting.lotto.laos.T_3TOD.payout;
            updateBody.$set['limitSetting.lotto.laos.T_3TOD.discount'] = requestBody.limitSetting.lotto.laos.T_3TOD.discount;
            updateBody.$set['limitSetting.lotto.laos.T_3TOD.max'] = requestBody.limitSetting.lotto.laos.T_3TOD.max;
            //
            updateBody.$set['limitSetting.lotto.laos.T_2TOP.payout'] = requestBody.limitSetting.lotto.laos.T_2TOP.payout;
            updateBody.$set['limitSetting.lotto.laos.T_2TOP.discount'] = requestBody.limitSetting.lotto.laos.T_2TOP.discount;
            updateBody.$set['limitSetting.lotto.laos.T_2TOP.max'] = requestBody.limitSetting.lotto.laos.T_2TOP.max;
            //         //
            updateBody.$set['limitSetting.lotto.laos.T_2TOD.payout'] = requestBody.limitSetting.lotto.laos.T_2TOD.payout;
            updateBody.$set['limitSetting.lotto.laos.T_2TOD.discount'] = requestBody.limitSetting.lotto.laos.T_2TOD.discount;
            updateBody.$set['limitSetting.lotto.laos.T_2TOD.max'] = requestBody.limitSetting.lotto.laos.T_2TOD.max;
            //         //
            updateBody.$set['limitSetting.lotto.laos.T_2BOT.payout'] = requestBody.limitSetting.lotto.laos.T_2BOT.payout;
            updateBody.$set['limitSetting.lotto.laos.T_2BOT.discount'] = requestBody.limitSetting.lotto.laos.T_2BOT.discount;
            updateBody.$set['limitSetting.lotto.laos.T_2BOT.max'] = requestBody.limitSetting.lotto.laos.T_2BOT.max;
            //
            updateBody.$set['limitSetting.lotto.laos.T_1TOP.payout'] = requestBody.limitSetting.lotto.laos.T_1TOP.payout;
            updateBody.$set['limitSetting.lotto.laos.T_1TOP.discount'] = requestBody.limitSetting.lotto.laos.T_1TOP.discount;
            updateBody.$set['limitSetting.lotto.laos.T_1TOP.max'] = requestBody.limitSetting.lotto.laos.T_1TOP.max;
            //         //
            updateBody.$set['limitSetting.lotto.laos.T_1BOT.payout'] = requestBody.limitSetting.lotto.laos.T_1BOT.payout;
            updateBody.$set['limitSetting.lotto.laos.T_1BOT.discount'] = requestBody.limitSetting.lotto.laos.T_1BOT.discount;
            updateBody.$set['limitSetting.lotto.laos.T_1BOT.max'] = requestBody.limitSetting.lotto.laos.T_1BOT.max;
        }

        if (gameList.includes('LOTTO_PP')) {
            updateBody.$set['shareSetting.lotto.pp.parent'] = requestBody.shareSetting.lotto.pp.parent;

            updateBody.$set['limitSetting.lotto.pp.isEnable'] = requestBody.limitSetting.lotto.pp.isEnable;
            updateBody.$set['limitSetting.lotto.pp.hour'] = requestBody.limitSetting.lotto.pp.hour;
            updateBody.$set['limitSetting.lotto.pp.minute'] = requestBody.limitSetting.lotto.pp.minute;
            //         //
            updateBody.$set['limitSetting.lotto.pp._6TOP.payout'] = requestBody.limitSetting.lotto.pp._6TOP.payout;
            updateBody.$set['limitSetting.lotto.pp._6TOP.discount'] = requestBody.limitSetting.lotto.pp._6TOP.discount;
            updateBody.$set['limitSetting.lotto.pp._6TOP.max'] = requestBody.limitSetting.lotto.pp._6TOP.max;
            //         //
            updateBody.$set['limitSetting.lotto.pp._5TOP.payout'] = requestBody.limitSetting.lotto.pp._5TOP.payout;
            updateBody.$set['limitSetting.lotto.pp._5TOP.discount'] = requestBody.limitSetting.lotto.pp._5TOP.discount;
            updateBody.$set['limitSetting.lotto.pp._5TOP.max'] = requestBody.limitSetting.lotto.pp._5TOP.max;
            //         //
            updateBody.$set['limitSetting.lotto.pp._4TOP.payout'] = requestBody.limitSetting.lotto.pp._4TOP.payout;
            updateBody.$set['limitSetting.lotto.pp._4TOP.discount'] = requestBody.limitSetting.lotto.pp._4TOP.discount;
            updateBody.$set['limitSetting.lotto.pp._4TOP.max'] = requestBody.limitSetting.lotto.pp._4TOP.max;
            //         //
            updateBody.$set['limitSetting.lotto.pp._4TOD.payout'] = requestBody.limitSetting.lotto.pp._4TOD.payout;
            updateBody.$set['limitSetting.lotto.pp._4TOD.discount'] = requestBody.limitSetting.lotto.pp._4TOD.discount;
            updateBody.$set['limitSetting.lotto.pp._4TOD.max'] = requestBody.limitSetting.lotto.pp._4TOD.max;
            //         //
            updateBody.$set['limitSetting.lotto.pp._3TOP.payout'] = requestBody.limitSetting.lotto.pp._3TOP.payout;
            updateBody.$set['limitSetting.lotto.pp._3TOP.discount'] = requestBody.limitSetting.lotto.pp._3TOP.discount;
            updateBody.$set['limitSetting.lotto.pp._3TOP.max'] = requestBody.limitSetting.lotto.pp._3TOP.max;
            //         //
            updateBody.$set['limitSetting.lotto.pp._3TOD.payout'] = requestBody.limitSetting.lotto.pp._3TOD.payout;
            updateBody.$set['limitSetting.lotto.pp._3TOD.discount'] = requestBody.limitSetting.lotto.pp._3TOD.discount;
            updateBody.$set['limitSetting.lotto.pp._3TOD.max'] = requestBody.limitSetting.lotto.pp._3TOD.max;
            //         //
            updateBody.$set['limitSetting.lotto.pp._2TOP.payout'] = requestBody.limitSetting.lotto.pp._2TOP.payout;
            updateBody.$set['limitSetting.lotto.pp._2TOP.discount'] = requestBody.limitSetting.lotto.pp._2TOP.discount;
            updateBody.$set['limitSetting.lotto.pp._2TOP.max'] = requestBody.limitSetting.lotto.pp._2TOP.max;
            //         //
            updateBody.$set['limitSetting.lotto.pp._2TOD.payout'] = requestBody.limitSetting.lotto.pp._2TOD.payout;
            updateBody.$set['limitSetting.lotto.pp._2TOD.discount'] = requestBody.limitSetting.lotto.pp._2TOD.discount;
            updateBody.$set['limitSetting.lotto.pp._2TOD.max'] = requestBody.limitSetting.lotto.pp._2TOD.max;

            updateBody.$set['limitSetting.lotto.pp._2BOT.payout'] = requestBody.limitSetting.lotto.pp._2BOT.payout;
            updateBody.$set['limitSetting.lotto.pp._2BOT.discount'] = requestBody.limitSetting.lotto.pp._2BOT.discount;
            updateBody.$set['limitSetting.lotto.pp._2BOT.max'] = requestBody.limitSetting.lotto.pp._2BOT.max;
            //         //
            updateBody.$set['limitSetting.lotto.pp._1TOP.payout'] = requestBody.limitSetting.lotto.pp._1TOP.payout;
            updateBody.$set['limitSetting.lotto.pp._1TOP.discount'] = requestBody.limitSetting.lotto.pp._1TOP.discount;
            updateBody.$set['limitSetting.lotto.pp._1TOP.max'] = requestBody.limitSetting.lotto.pp._1TOP.max;
            //         //
            updateBody.$set['limitSetting.lotto.pp._1BOT.payout'] = requestBody.limitSetting.lotto.pp._1BOT.payout;
            updateBody.$set['limitSetting.lotto.pp._1BOT.discount'] = requestBody.limitSetting.lotto.pp._1BOT.discount;
            updateBody.$set['limitSetting.lotto.pp._1BOT.max'] = requestBody.limitSetting.lotto.pp._1BOT.max;
        }

        if (gameList.includes('MULTI_PLAYER')) {
            updateBody.$set['shareSetting.multi.amb.parent'] = requestBody.shareSetting.multi.amb.parent;

            updateBody.$set['limitSetting.multi.amb.isEnable'] = requestBody.limitSetting.multi.amb.isEnable;

            updateBody.$set['commissionSetting.multi.amb'] = requestBody.commissionSetting.multi.amb;
        }

        if (gameList.includes('M2')) {
            updateBody.$set['shareSetting.other.m2.parent'] = requestBody.shareSetting.other.m2.parent;

            updateBody.$set['limitSetting.other.m2.maxPerBetHDP'] = requestBody.limitSetting.other.m2.maxPerBetHDP;
            updateBody.$set['limitSetting.other.m2.minPerBetHDP'] = requestBody.limitSetting.other.m2.minPerBetHDP;
            updateBody.$set['limitSetting.other.m2.maxBetPerMatchHDP'] = requestBody.limitSetting.other.m2.maxBetPerMatchHDP;
            updateBody.$set['limitSetting.other.m2.maxPerBet'] = requestBody.limitSetting.other.m2.maxPerBet;
            updateBody.$set['limitSetting.other.m2.minPerBet'] = requestBody.limitSetting.other.m2.minPerBet;
            updateBody.$set['limitSetting.other.m2.maxBetPerDay'] = requestBody.limitSetting.other.m2.maxBetPerDay;
            updateBody.$set['limitSetting.other.m2.maxPayPerBill'] = requestBody.limitSetting.other.m2.maxPayPerBill;
            updateBody.$set['limitSetting.other.m2.maxMatchPerBet'] = requestBody.limitSetting.other.m2.maxMatchPerBet;
            updateBody.$set['limitSetting.other.m2.minMatchPerBet'] = requestBody.limitSetting.other.m2.minMatchPerBet;
            updateBody.$set['limitSetting.other.m2.isEnable'] = requestBody.limitSetting.other.m2.isEnable;

            updateBody.$set['commissionSetting.other.m2'] = requestBody.commissionSetting.other.m2;
        }


        console.log('body : ', updateBody)
        // return res.send({code: 0, message: "success"});

        console.log({group: currentAgentInfo.group})
        //
        MemberModel.update({group: currentAgentInfo._id}, updateBody, {multi: true}).exec(function (err, creditResponse) {

            console.log(creditResponse)
            if (err) {
                // callbackWL(err);
            }
            else {
                return res.send({code: 0, message: "success"});
            }
        });

    });


});

router.put('/config/copyMock', (req, res) => {
    let {
        agent
    } = req.body;
    MemberModel.findOne({
        username_lower: agent.toLowerCase() + "mock"
    }, (err, data) => {
        if (err) {
            return res.send({
                code: 0, message: "success", result: agent
            });
        } else {
            const {
                shareSetting,
                limitSetting,
                commissionSetting,
                configuration,
                group
            } = data;
            MemberModel.find({
                group: group
            }, (err, data) => {
                _.each(data, obj => {
                    MemberModel.update({_id: obj._id}, {
                        $set: {
                            shareSetting,
                            limitSetting,
                            commissionSetting,
                            configuration
                        }
                    }, function (err, response) {
                        if (err) {
                            console.log(err);
                        } else {
                            console.log(obj.username_lower + ' + ', response.nModified);
                        }
                    });
                });
            }).select('username_lower');
            return res.send({
                code: 0, message: "success", result: data
            });
        }
    }).select('shareSetting limitSetting commissionSetting configuration group');
});


router.post('/clear-member-cash', function (req, res) {

    const requestBody = req.body;

    const userInfo = req.userInfo;

    async.series([callback => {

        UserModel.findById('59e6208101bf839b794da71b'/*userInfo._id*/, (err, agentResponse) => {

            // if (agentResponse && (Hashing.encrypt256_1(requestBody.password) === agentResponse.password)) {
                callback(null, agentResponse)
            // } else {
            //     callback(1001, null);
            // }
        });

    }], (err, asyncResponse) => {

        if (err) {
            if (err === 1001) {
                return res.send({
                    code: 1001,
                    message: "Invalid Password"
                });
            }
            return res.send({
                code: 999,
                message: "error"
            });
        }

        const user = asyncResponse[0];

        const cursor = MemberModel.find({
           "group": mongoose.Types.ObjectId('5e7b0cdf6ae5a20e257372d0'),
           // username_lower: 'bauto0155038'
        })
            .batchSize(10000)
            .select('_id username_lower creditLimit balance lastPaymentDate')
            // .limit(1)
            .cursor();

        cursor.on('data', function (memberObject) {

            const creditLimit = memberObject.creditLimit;
            const balance = memberObject.balance;

            console.log(memberObject.username_lower + " : " + creditLimit + " : " + balance);
            console.log(memberObject.lastPaymentDate)

            let remainingCredit = creditLimit + balance;

            console.log('remainingCredit : ', remainingCredit);

            let body = {
                creditLimit: 0,
                balance: 0
            };

            if (remainingCredit < 1) {
                body.creditLimit = 0;
                body.balance = 0;
            } else {
                body.creditLimit = remainingCredit;
                body.balance = 0;
            }


            if(balance < 0){
                let query = {
                    _id: memberObject._id, creditLimit: creditLimit, balance: balance,
                    lastUpdateBalance: {"$lt": moment().add(-30, 'minutes').utc(true).toDate()}
                };

                console.log('update query : ', query);
                console.log(body);
                MemberModel.updateOne(query, body).lean(true).exec(function (err, updateMemberResponse) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log(updateMemberResponse);
                        if (updateMemberResponse.nModified == 1) {

                            MemberModel.findOne({_id: memberObject._id}).lean(true).exec(function (err, findMemberResponse) {

                                let updateBody = {
                                    username: memberObject.username_lower,
                                    beforeCredit: creditLimit,
                                    afterCredit: findMemberResponse.creditLimit,
                                    totalBeforeCredit: creditLimit + balance,
                                    beforeBalance: balance,
                                    afterBalance: findMemberResponse.balance,
                                    totalAfterCredit: findMemberResponse.creditLimit + findMemberResponse.balance,
                                    actionBy: user._id,
                                    createdDate: DateUtils.getCurrentDate()
                                };

                                let model = new MemberClearCashModel(updateBody);
                                model.save();
                            });
                        }

                    }
                });
            }



        });


        cursor.on('end', function () {
            console.log('Done!');
            return res.send({code: 0, message: "success"});
        });


    });

});

module.exports = router;