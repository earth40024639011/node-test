const AgentGroupModel = require('../../models/agentGroup.model');
const UserModel = require("../../models/users.model");
const MemberModel = require('../../models/member.model');
const PaymentHistoryModel = require('../../models/paymentHistory.model.js');
const BetTransactionModel = require('../../models/betTransaction.model.js');
const MemberService = require('../../common/memberService');
const AgentService = require("../../common/agentService");
const PaymentService = require("../../common/paymentService");
const ReportService = require('../../common/reportService');
const express = require('express');
const router = express.Router();
const async = require("async");
const Joi = require('joi');
const mongoose = require('mongoose');
const config = require('config');
const Hashing = require('../../common/hashing');
const SequenceModel = require('../../models/sequence.model');
const _ = require('underscore');
const zpad = require('zpad');
const moment = require('moment');
const DateUtils = require('../../common/dateUtils');
const naturalSort = require("javascript-natural-sort");


function getGroupAndChild(groupType, callback) {
    let group, child;
    if (groupType === 'SUPER_ADMIN') {
        group = 'superAdmin';
        child = 'company';
    } else if (groupType === 'COMPANY') {
        group = 'company';
        child = 'shareHolder';
    } else if (groupType === 'SHARE_HOLDER') {
        group = 'shareHolder';
        child = 'senior';
    } else if (groupType === 'SENIOR') {
        group = 'senior';
        child = 'masterAgent';
    } else if (groupType === 'MASTER_AGENT') {
        group = 'masterAgent';
        child = 'agent';
    } else if (groupType === 'AGENT') {
        group = 'agent';
    }
    callback(group, child);
}


router.get('/list2', function (req, res) {

    let userInfo = req.userInfo;

    let todayDate;
    let yesterdayDate;
    if (moment().hours() >= 11) {
        todayDate = moment().utc(true);
        yesterdayDate = moment().add(-1, 'days').utc(true);
    } else {
        todayDate = moment().add(-1, 'days').utc(true);
        yesterdayDate = moment().add(-2, 'days').utc(true);
    }


    // if(!req.query.username){
    //
    //     return res.send(
    //         {
    //             code: 0,
    //             message: "success",
    //             result: {
    //                 data: {
    //                     docs:[],
    //                     total: 0,
    //                     limit:1,
    //                     page: 1,
    //                     pages: 1,
    //                 },
    //                 summary: {},
    //                 todayDate: todayDate.toDate(),
    //                 yesterdayDate: yesterdayDate.toDate()
    //             }
    //         }
    //     );
    // }

    findAllPaymentListPagin(userInfo, req.query, function (err, response) {
        if (err) {
            return res.send({message: "error", results: err, code: 999});
        }

        let summary = _.reduce(response.docs, function (memo, obj) {

            return {
                yesterdayBalance: memo.yesterdayBalance + obj.yesterdayBalance,
                todayWinLose: memo.todayWinLose + obj.todayWinLose,
                totalBalance: memo.totalBalance + obj.totalBalance,
                balance: memo.balance + obj.balance,
                amount: memo.amount + obj.amount,
                totalOutstanding: memo.totalOutstanding + obj.totalOutstanding,
                creditLimit: memo.creditLimit + obj.creditLimit,
            }
        }, {
            yesterdayBalance: 0,
            todayWinLose: 0,
            totalBalance: 0,
            balance: 0,
            amount: 0,
            totalOutstanding: 0,
            creditLimit: 0
        });


        return res.send(
            {
                code: 0,
                message: "success",
                result: {
                    data: response,
                    summary: summary,
                    todayDate: todayDate.toDate(),
                    yesterdayDate: yesterdayDate.toDate()
                }
            }
        );
    });


});


router.get('/setting/list', function (req, res) {


    let userInfo = req.userInfo;

    let condition = {parentId: mongoose.Types.ObjectId(userInfo.group._id)};
    if (req.query.active) {
        condition.active = req.query.active;
    }

    AgentGroupModel.find(condition)
        .lean()
        .exec(function (err, responseList) {


            responseList = _.sortBy(responseList, function (o) {
                return o.suspend ? 1 : -1;
            });

            responseList = _.sortBy(responseList, function (o) {
                return o.lock ? 1 : -1;
            });

            responseList = _.sortBy(responseList, (o) => {
                return o.active ? -1 : 1;
            });

            return res.send({
                code: 0,
                message: "success",
                result: {
                    list: responseList,
                }
            });

        });
});


const settingSchema = Joi.object().keys({
    monday: Joi.boolean().required(),
    tuesday: Joi.boolean().required(),
    wednesday: Joi.boolean().required(),
    thursday: Joi.boolean().required(),
    friday: Joi.boolean().required(),
    saturday: Joi.boolean().required(),
    sunday: Joi.boolean().required(),
});

router.put('/setting/:id', function (req, res) {

    let validate = Joi.validate(req.body, settingSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    let userInfo = req.userInfo;


    let updateBody = {
        paymentSetting: {
            monday: req.body.monday,
            tuesday: req.body.tuesday,
            wednesday: req.body.wednesday,
            thursday: req.body.thursday,
            friday: req.body.friday,
            saturday: req.body.saturday,
            sunday: req.body.sunday
        }
    };

    AgentGroupModel.findOneAndUpdate({_id: mongoose.Types.ObjectId(req.params.id)}, {$set: updateBody})
        .exec(function (err, response) {
            if (err) {
                return res.send({message: "error", results: err, code: 999});
            } else {
                return res.send({
                    code: 0,
                    message: "success",
                });
            }
        });
});


const settingAllSchema = Joi.object().keys({
    ids: Joi.string().required(),
    monday: Joi.boolean().allow(null),
    tuesday: Joi.boolean().allow(null),
    wednesday: Joi.boolean().allow(null),
    thursday: Joi.boolean().allow(null),
    friday: Joi.boolean().allow(null),
    saturday: Joi.boolean().allow(null),
    sunday: Joi.boolean().allow(null)
});

router.put('/settingAll', function (req, res) {

    const userInfo = req.userInfo;

    console.log(req.body)

    let validate = Joi.validate(req.body, settingAllSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    let ids = req.body.ids.split('|');


    let updateBody = {};


    if (req.body.monday != null) {
        updateBody['paymentSetting.monday'] = req.body.monday;
    }
    if (req.body.tuesday != null) {
        updateBody['paymentSetting.tuesday'] = req.body.tuesday;
    }
    if (req.body.wednesday != null) {
        updateBody['paymentSetting.wednesday'] = req.body.wednesday;
    }
    if (req.body.thursday != null) {
        updateBody['paymentSetting.thursday'] = req.body.thursday;
    }
    if (req.body.friday != null) {
        updateBody['paymentSetting.friday'] = req.body.friday;
    }
    if (req.body.saturday != null) {
        updateBody['paymentSetting.saturday'] = req.body.saturday;
    }
    if (req.body.sunday != null) {
        updateBody['paymentSetting.sunday'] = req.body.sunday;
    }

    console.log('updateBody : ', updateBody)


    AgentGroupModel.update({_id: {$in: ids}}, {$set: updateBody}, {multi: true})
        .exec(function (err, response) {
            if (err) {
                return res.send({code: 999, message: "error"});
            } else {
                return res.send({code: 0, message: "success"});
            }
        });


});

function findAllPaymentList(userInfo, callback) {

    let groupId = userInfo.group._id;

    async.series([(callback => {
        let condition = {};
        condition.group = mongoose.Types.ObjectId(groupId);
        MemberModel.find(condition, (err, response) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, response);
            }
        }).select('username contact currency creditLimit balance createdDate active lastPaymentDate');


    }), callback => {

        // let userInfo = req.userInfo.type;

        async.series([(subCallback => {
            AgentGroupModel.findById(groupId, (err, response) => {
                if (err) {
                    subCallback(err, null);
                } else {
                    subCallback(null, response);
                }
            }).select('type parentId lastPaymentDate')

        }), (subCallback => {


            let condition = {};

            condition['memberParentGroup'] = mongoose.Types.ObjectId(groupId);
            condition['status'] = 'RUNNING';
            condition['game'] = {$in: ['FOOTBALL', 'LOTTO', 'M2']};

            BetTransactionModel.aggregate([
                {
                    $match: condition
                },
                {
                    "$group": {
                        "_id": {
                            commission: '$memberParentGroup',
                            member: '$memberId'
                            // m:'$memberParentGroup'
                        },
                        "amount": {$sum: '$amount'},

                        "memberWinLose": {$sum: '$commission.member.amount'},

                        "agentWinLose": {$sum: '$commission.agent.amount'},

                        "masterAgentWinLose": {$sum: '$commission.masterAgent.amount'},

                        "seniorWinLose": {$sum: '$commission.senior.amount'},

                        "shareHolderWinLose": {$sum: '$commission.shareHolder.amount'},

                        "companyWinLose": {$sum: '$commission.company.amount'},

                        "superAdminWinLose": {$sum: '$commission.superAdmin.amount'},

                        // company_stake: {
                        //     $sum: {
                        //         $cond: [{$eq: ['$gameType', 'PARLAY']}, {$multiply: ['$commission.company.amount', {$abs: '$parlay.odds'}]}, {$multiply: ['$commission.company.amount', {$abs: '$hdp.odd'}]}]
                        //     }
                        // },
                    }
                },
                {
                    $project: {
                        _id: '$_id.member',
                        type: 'M_GROUP',
                        member: '$_id.member',
                        group: '$_id.commission',
                        amount: '$amount',
                        memberWinLose: '$memberWinLose',
                        agentWinLose: '$agentWinLose',
                        masterAgentWinLose: '$masterAgentWinLose',
                        seniorWinLose: '$seniorWinLose',
                        shareHolderWinLose: '$shareHolderWinLose',
                        companyWinLose: '$companyWinLose',
                        superAdminWinLose: '$superAdminWinLose',
                        // company_stake: '$company_stake'
                    }
                }
            ], (err, results) => {
                if (err) {
                    subCallback(err, null);
                } else {
                    MemberModel.populate(results, {
                        path: 'member',
                        select: '_id username username_lower contact'
                    }, function (err, memberResult) {
                        if (err) {
                            subCallback(err, null);
                        } else {
                            subCallback(null, memberResult);
                        }
                    });
                }
            });

        }), (subCallback => {
            AgentGroupModel.findById(groupId, (err, response) => {
                if (err) {
                    subCallback(err, null);
                } else {
                    subCallback(null, response);
                }
            }).select('type parentId');

        })], function (err, asyncResponse) {

            if (err) {
                callback(err, null);
                return;
            }
            let agentGroup = asyncResponse[0];

            let group = '';
            let childGroup;
            if (agentGroup.type === 'SUPER_ADMIN') {
                group = 'superAdmin';
                childGroup = 'company';
            } else if (agentGroup.type === 'COMPANY') {
                group = 'company';
                childGroup = 'shareHolder';
            } else if (agentGroup.type === 'SHARE_HOLDER') {
                group = 'shareHolder';
                childGroup = 'senior';
            } else if (agentGroup.type === 'SENIOR') {
                group = "senior";
                childGroup = 'masterAgent,agent';
            } else if (agentGroup.type === 'MASTER_AGENT') {
                group = "masterAgent";
                childGroup = 'agent';
            } else if (agentGroup.type === 'AGENT') {
                group = "agent";
                childGroup = '';
            }


            let transaction = [];

            async.map(childGroup.split(','), (child, callback) => {

                let condition = {};

                condition['memberParentGroup'] = {$ne: mongoose.Types.ObjectId(groupId)};
                condition['status'] = 'RUNNING';
                condition['game'] = {$in: ['FOOTBALL', 'LOTTO', 'M2']};
                condition['$and'] = [
                    {['commission.' + child + '.parentGroup']: mongoose.Types.ObjectId(groupId)},
                    {['commission.' + group + '.group']: mongoose.Types.ObjectId(groupId)}
                ];

                BetTransactionModel.aggregate([
                    {
                        $match: condition
                    },
                    {
                        "$group": {
                            "_id": {
                                commission: '$commission.' + child + '.group',
                            },
                            "amount": {$sum: '$amount'},

                            "memberWinLose": {$sum: '$commission.member.amount'},

                            "agentWinLose": {$sum: '$commission.agent.amount'},

                            "masterAgentWinLose": {$sum: '$commission.masterAgent.amount'},

                            "seniorWinLose": {$sum: '$commission.senior.amount'},

                            "shareHolderWinLose": {$sum: '$commission.shareHolder.amount'},

                            "companyWinLose": {$sum: '$commission.company.amount'},

                            "superAdminWinLose": {$sum: '$commission.superAdmin.amount'},
                        }
                    },
                    {
                        $project: {
                            _id: '$_id.commission',
                            type: 'A_GROUP',
                            group: '$_id.commission',
                            amount: '$amount',
                            memberWinLose: '$memberWinLose',
                            agentWinLose: '$agentWinLose',
                            masterAgentWinLose: '$masterAgentWinLose',
                            seniorWinLose: '$seniorWinLose',
                            shareHolderWinLose: '$shareHolderWinLose',
                            companyWinLose: '$companyWinLose',
                            superAdminWinLose: '$superAdminWinLose',

                        }
                    }
                ], (err, results) => {
                    _.each(results, item => {
                        transaction.push(item);
                    });
                    callback();
                });
            }, (err) => {

                if (err) {
                    callback(err, null);
                    return;
                }
                let memberGroupList = asyncResponse[1];
                let mergeDataList = transaction.concat(memberGroupList);

                async.series([(subCallback => {

                    AgentGroupModel.populate(mergeDataList, {
                        path: 'group',
                        select: '_id name name_lower contact'
                    }, function (err, memberResult) {
                        if (err) {
                            subCallback(err, null);
                        } else {
                            subCallback(null, memberResult);
                        }
                    });


                })], (err, populateResults) => {

                    callback(null, mergeDataList)
                });
            });
        });

    }], (err, results) => {

        let memberList = results[0];
        let outStandingList = results[1];

        let condition = {};
        condition.parentId = mongoose.Types.ObjectId(groupId);
        AgentGroupModel.find(condition)
            .populate({
                path: 'parentId',
                select: 'name contact currency creditLimit balance createdDate active'
            })
            .select('name contact currency creditLimit balance createdDate active lastPaymentDate')
            .lean()
            .exec(function (err, agentList) {

                if (err) {
                    callback(err, null);
                    return;
                }

                let agent = _.map(agentList, (value) => {
                    let s = _.clone(value);
                    s.type = 'AGENT';
                    return s;
                });


                let member = _.map(memberList, (value) => {
                    let s = Object.assign({}, value)._doc;
                    s.type = 'MEMBER';
                    return s;

                });

                let mergeDataList = member.concat(agent);


                let prepareOutStandingList = transformDataByAgentType(userInfo.group.type, outStandingList);

                mergeDataList = _.map(mergeDataList, (item) => {

                    let outStand = _.filter(prepareOutStandingList, (oItem) => {
                        return oItem._id.toString() == item._id.toString();
                    })[0];

                    let newItem = Object.assign({}, item);

                    if (outStand) {
                        if (outStand.type === 'M_GROUP') {
                            newItem.totalOutstanding = outStand.amount;
                        } else {
                            newItem.totalOutstanding = outStand.agentWinLose;
                        }
                    } else {
                        newItem.totalOutstanding = 0;
                    }
                    return newItem;

                });

                let summary = _.reduce(mergeDataList, function (memo, num) {
                    return {
                        balance: memo.balance + num.balance,
                        creditLimit: memo.creditLimit + num.creditLimit
                    }
                }, {balance: 0, creditLimit: 0});

                callback(null, mergeDataList);
            });
    });
}

function findAllPaymentList2(userInfo, params, callback) {

    console.log('findAllPaymentList2 params : ', params)
    let groupId = userInfo.group._id;

    async.parallel([(callback => {
        if (params.type === 'MEMBER') {
            let condition = {};
            condition.group = mongoose.Types.ObjectId(groupId);
            if (params.username) {
                condition.username_lower = params.username.toLowerCase()
            }

            if (params.active) {
                condition.active = params.active;
            }

            MemberModel.find(condition, (err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, response);
                }
            }).select('username contact currency creditLimit balance createdDate suspend lock active lastPaymentDate');
        } else {
            callback(null, []);
        }

    }), callback => {
        // let userInfo = req.userInfo.type;
        async.series([(subCallback => {
            AgentGroupModel.findById(groupId, (err, response) => {
                if (err) {
                    subCallback(err, null);
                } else {
                    subCallback(null, response);
                }
            }).select('type parentId')

        }), (subCallback => {

            if (params.type === 'MEMBER') {
                let condition = {game: {$in: ['FOOTBALL', 'LOTTO', 'M2']}};

                condition['memberParentGroup'] = mongoose.Types.ObjectId(groupId);
                condition['status'] = 'RUNNING';

                console.log('outstanding : ', groupId)

                BetTransactionModel.aggregate([
                    {
                        $match: condition
                    },
                    {
                        "$group": {
                            "_id": {
                                commission: '$memberParentGroup',
                                member: '$memberId'
                            },
                            "amount": {$sum: {$abs: '$memberCredit'}}
                        }
                    },
                    {
                        $project: {
                            _id: '$_id.member',
                            type: 'M_GROUP',
                            member: '$_id.member',
                            group: '$_id.commission',
                            amount: '$amount'
                        }
                    }
                ], (err, results) => {
                    if (err) {
                        subCallback(err, null);
                    } else {
                        console.log('outstand member  : ', results.length);
                        MemberModel.populate(results, {
                            path: 'member',
                            select: '_id username username_lower contact'
                        }, function (err, memberResult) {
                            if (err) {
                                subCallback(err, null);
                            } else {
                                subCallback(null, memberResult);
                            }
                        });
                    }
                });
            } else {
                subCallback(null, []);
            }

        }), (subCallback => {
            AgentGroupModel.findById(groupId, (err, response) => {
                if (err) {
                    subCallback(err, null);
                } else {
                    subCallback(null, response);
                }
            }).select('type parentId');

        }), (subCallback => {

            let todayTransaction = [];


            if (params.type === 'AGENT') {


                let group = '';
                let childGroup;

                if (userInfo.group.type === 'SUPER_ADMIN') {
                    group = 'superAdmin';
                    childGroup = 'company';
                } else if (userInfo.group.type === 'COMPANY') {
                    group = 'company';
                    childGroup = 'shareHolder';
                } else if (userInfo.group.type === 'SHARE_HOLDER') {
                    group = 'shareHolder';
                    childGroup = 'senior';
                } else if (userInfo.group.type === 'SENIOR') {
                    group = "senior";
                    childGroup = 'masterAgent,agent';
                } else if (userInfo.group.type === 'MASTER_AGENT') {
                    group = "masterAgent";
                    childGroup = 'agent';
                } else if (userInfo.group.type === 'AGENT') {
                    group = "agent";
                    childGroup = '';
                }


                async.map(childGroup.split(','), (child, callback) => {

                    let todayWinLoseCondition = {};

                    if (moment().hours() >= 11) {
                        todayWinLoseCondition['gameDate'] = {
                            "$gte": moment().millisecond(0).second(0).minute(0).hour(11).utc(true).toDate(),
                            "$lt": moment().millisecond(0).second(0).minute(0).hour(11).add(1, 'd').utc(true).toDate()
                        };
                    } else {
                        todayWinLoseCondition['gameDate'] = {
                            "$gte": moment().millisecond(0).second(0).minute(0).hour(11).add(-1, 'd').utc(true).toDate(),
                            "$lt": moment().millisecond(0).second(0).minute(0).hour(11).add(0, 'd').utc(true).toDate()
                        };

                    }

                    todayWinLoseCondition['memberParentGroup'] = {$ne: mongoose.Types.ObjectId(groupId)};

                    let orCondition = [
                        {'$or': [{'status': 'DONE'}]},
                        {['commission.' + child + '.parentGroup']: mongoose.Types.ObjectId(groupId)},
                        {['commission.' + group + '.group']: mongoose.Types.ObjectId(groupId)},

                    ];

                    todayWinLoseCondition['$and'] = orCondition;

                    BetTransactionModel.aggregate([
                        {
                            $match: todayWinLoseCondition
                        },
                        {
                            "$group": {
                                "_id": {
                                    commission: '$commission.' + child + '.group'
                                },
                                "currency": {$first: "$currency"},
                                "stackCount": {$sum: 1},
                                "amount": {$sum: '$amount'},
                                "validAmount": {$sum: '$validAmount'},
                                "grossComm": {$sum: {$multiply: ['$validAmount', {$divide: ['$commission.' + group + '.commission', 100]}]}},
                                "memberWinLose": {$sum: '$commission.member.winLose'},
                                "memberWinLoseCom": {$sum: '$commission.member.winLoseCom'},
                                "memberTotalWinLoseCom": {$sum: '$commission.member.totalWinLoseCom'},

                                "agentWinLose": {$sum: '$commission.agent.winLose'},
                                "agentWinLoseCom": {$sum: '$commission.agent.winLoseCom'},
                                "agentTotalWinLoseCom": {$sum: '$commission.agent.totalWinLoseCom'},

                                "masterAgentWinLose": {$sum: '$commission.masterAgent.winLose'},
                                "masterAgentWinLoseCom": {$sum: '$commission.masterAgent.winLoseCom'},
                                "masterAgentTotalWinLoseCom": {$sum: '$commission.masterAgent.totalWinLoseCom'},

                                "seniorWinLose": {$sum: '$commission.senior.winLose'},
                                "seniorWinLoseCom": {$sum: '$commission.senior.winLoseCom'},
                                "seniorTotalWinLoseCom": {$sum: '$commission.senior.totalWinLoseCom'},

                                "shareHolderWinLose": {$sum: '$commission.shareHolder.winLose'},
                                "shareHolderWinLoseCom": {$sum: '$commission.shareHolder.winLoseCom'},
                                "shareHolderTotalWinLoseCom": {$sum: '$commission.shareHolder.totalWinLoseCom'},

                                "companyWinLose": {$sum: '$commission.company.winLose'},
                                "companyWinLoseCom": {$sum: '$commission.company.winLoseCom'},
                                "companyTotalWinLoseCom": {$sum: '$commission.company.totalWinLoseCom'},

                                "superAdminWinLose": {$sum: '$commission.superAdmin.winLose'},
                                "superAdminWinLoseCom": {$sum: '$commission.superAdmin.winLoseCom'},
                                "superAdminTotalWinLoseCom": {$sum: '$commission.superAdmin.totalWinLoseCom'}
                            }
                        },
                        {
                            $project: {
                                _id: 0,
                                type: 'A_GROUP',
                                group: '$_id.commission',
                                currency: "$currency",
                                stackCount: '$stackCount',
                                grossComm: '$grossComm',
                                amount: '$amount',
                                validAmount: '$validAmount',
                                memberWinLose: '$memberWinLose',
                                memberWinLoseCom: '$memberWinLoseCom',
                                memberTotalWinLoseCom: '$memberTotalWinLoseCom',
                                agentWinLose: '$agentWinLose',
                                agentWinLoseCom: '$agentWinLoseCom',
                                agentTotalWinLoseCom: '$agentTotalWinLoseCom',
                                masterAgentWinLose: '$masterAgentWinLose',
                                masterAgentWinLoseCom: '$masterAgentWinLoseCom',
                                masterAgentTotalWinLoseCom: '$masterAgentTotalWinLoseCom',
                                seniorWinLose: '$seniorWinLose',
                                seniorWinLoseCom: '$seniorWinLoseCom',
                                seniorTotalWinLoseCom: '$seniorTotalWinLoseCom',
                                shareHolderWinLose: '$shareHolderWinLose',
                                shareHolderWinLoseCom: '$shareHolderWinLoseCom',
                                shareHolderTotalWinLoseCom: '$shareHolderTotalWinLoseCom',
                                companyWinLose: '$companyWinLose',
                                companyWinLoseCom: '$companyWinLoseCom',
                                companyTotalWinLoseCom: '$companyTotalWinLoseCom',
                                superAdminWinLose: '$superAdminWinLose',
                                superAdminWinLoseCom: '$superAdminWinLoseCom',
                                superAdminTotalWinLoseCom: '$superAdminTotalWinLoseCom',

                            }
                        }
                    ], (err, results) => {

                        console.log('pppppppppppppp')
                        _.each(results, item => {
                            todayTransaction.push(item);
                        });
                        callback();
                    });
                }, (err) => {

                    if (err) {
                        subCallback(err, null);
                        return;
                    }
                    subCallback(null, todayTransaction)
                });
            } else {
                //MEMBER

                // let todayWinLoseCondition = {};
                //
                // if (moment().hours() >= 11) {
                //     todayWinLoseCondition['gameDate'] = {
                //         "$gte": new Date(moment().add(0, 'days').utc(true).format('YYYY-MM-DDT11:00:00.000')),
                //         "$lt": new Date(moment().add(1, 'd').utc(true).format('YYYY-MM-DDT11:00:00.000'))
                //     };
                // } else {
                //     todayWinLoseCondition['gameDate'] = {
                //         "$gte": new Date(moment().add(-1, 'days').utc(true).format('YYYY-MM-DDT11:00:00.000')),
                //         "$lt": new Date(moment().add(0, 'd').utc(true).format('YYYY-MM-DDT11:00:00.000'))
                //     };
                //
                // }
                //
                // todayWinLoseCondition['memberParentGroup'] = mongoose.Types.ObjectId(groupId);
                // todayWinLoseCondition['status'] = 'DONE';
                //
                // BetTransactionModel.aggregate([
                //     {
                //         $match: todayWinLoseCondition
                //     },
                //     {
                //         "$group": {
                //             "_id": {
                //                 id: '$memberId'
                //             },
                //             "currency": {$first: "$currency"},
                //             "stackCount": {$sum: 1},
                //             "amount": {$sum: '$amount'},
                //             "validAmount": {$sum: '$validAmount'},
                //             "memberWinLose": {$sum: '$commission.member.winLose'},
                //             "memberWinLoseCom": {$sum: '$commission.member.winLoseCom'},
                //             "memberTotalWinLoseCom": {$sum: '$commission.member.totalWinLoseCom'},
                //         }
                //     },
                //     {
                //         $project: {
                //             _id: 0,
                //             type: 'M_GROUP',
                //             group: '$_id.id',
                //             currency: "$currency",
                //             stackCount: '$stackCount',
                //             grossComm: '$grossComm',
                //             amount: '$amount',
                //             validAmount: '$validAmount',
                //             memberWinLose: '$memberWinLose',
                //             memberWinLoseCom: '$memberWinLoseCom',
                //             memberTotalWinLoseCom: '$memberTotalWinLoseCom',
                //         }
                //     }
                // ], (err, results) => {
                //     todayTransaction = results;
                //     subCallback(null, todayTransaction)
                // });
                subCallback(null, [])
            }

        })], function (err, asyncResponse) {

            console.log(err)
            if (err) {
                callback(err, null);
                return;
            }
            let agentGroup = asyncResponse[0];

            let group = '';
            let childGroup;
            if (agentGroup.type === 'SUPER_ADMIN') {
                group = 'superAdmin';
                childGroup = 'company';
            } else if (agentGroup.type === 'COMPANY') {
                group = 'company';
                childGroup = 'shareHolder';
            } else if (agentGroup.type === 'SHARE_HOLDER') {
                group = 'shareHolder';
                childGroup = 'senior';
            } else if (agentGroup.type === 'SENIOR') {
                group = "senior";
                childGroup = 'masterAgent,agent';
            } else if (agentGroup.type === 'MASTER_AGENT') {
                group = "masterAgent";
                childGroup = 'agent';
            } else if (agentGroup.type === 'AGENT') {
                group = "agent";
                childGroup = '';
            }


            let transaction = [];

            async.map(childGroup.split(','), (child, callback) => {

                console.log('xxxx111111111')
                let condition = {};

                condition['memberParentGroup'] = {$ne: mongoose.Types.ObjectId(groupId)};
                condition['status'] = 'RUNNING';
                condition['game'] = {$in: ['FOOTBALL', 'LOTTO', 'M2']}
                condition['$and'] = [
                    {['commission.' + child + '.parentGroup']: mongoose.Types.ObjectId(groupId)},
                    {['commission.' + group + '.group']: mongoose.Types.ObjectId(groupId)}
                ];

                BetTransactionModel.aggregate([
                    {
                        $match: condition
                    },
                    {
                        "$group": {
                            "_id": {
                                commission: '$commission.' + child + '.group',
                            },
                            "amount": {$sum: '$commission.' + group + '.amount'}
                        }
                    },
                    {
                        $project: {
                            _id: '$_id.commission',
                            type: 'A_GROUP',
                            group: '$_id.commission',
                            amount: '$amount'
                        }
                    }
                ], (err, results) => {

                    console.log('2222222222222222')
                    _.each(results, item => {
                        transaction.push(item);
                    });
                    callback();
                });
            }, (err) => {

                if (err) {
                    callback(err, null);
                    return;
                }
                let memberGroupList = asyncResponse[1];
                let mergeDataList = transaction.concat(memberGroupList);
                let mergeTodayBalance = asyncResponse[3];


                console.log('outstand agent  : ', transaction.length);
                async.series([(subCallback => {

                    AgentGroupModel.populate(mergeDataList, {
                        path: 'group',
                        select: '_id name name_lower contact'
                    }, function (err, memberResult) {
                        if (err) {
                            subCallback(err, null);
                        } else {
                            subCallback(null, memberResult);
                        }
                    });


                })], (err, populateResults) => {
                    let result = {
                        outStandingList: mergeDataList,
                        todayBalanceList: mergeTodayBalance
                    };
                    callback(null, result)
                });
            });
        });

    }, callback => {

        if (params.type === 'AGENT') {
            let condition = {};
            condition.parentId = mongoose.Types.ObjectId(groupId);
            if (params.username) {
                condition.name_lower = params.username.toLowerCase()
            }
            if (params.active) {
                condition.active = params.active;
            }
            AgentGroupModel.find(condition)
                .populate({
                    path: 'parentId',
                    select: 'name contact currency creditLimit balance createdDate active'
                })
                .select('name contact currency creditLimit balance createdDate active type lastPaymentDate suspend lock active')
                .lean()
                .exec(function (err, agentList) {

                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, agentList)
                    }

                });
        } else {
            callback(null, []);
        }

    }], (err, results) => {

        let memberList = results[0];
        let outStandingList = results[1].outStandingList;
        let todayBalanceList = results[1].todayBalanceList;


        let member = _.map(memberList, (value) => {
            let s = Object.assign({}, value)._doc;
            s.type = 'MEMBER';
            return s;
        });

        let mergeDataList = member;

        if (params.type === 'AGENT') {
            let agentList = results[2];
            let agent = _.map(agentList, (value) => {
                let s = _.clone(value);
                s.type = 'AGENT';
                s._type = value.type;
                return s;
            });
            mergeDataList = mergeDataList.concat(agent);
        }


        let prepareOutStandingList = transformDataByAgentType(userInfo.group.type, outStandingList);

        let prepareTodayBalanceList = todayBalanceList;
        if (params.type === 'AGENT') {
            prepareTodayBalanceList = transformDataByAgentTypeReport(userInfo.group.type, todayBalanceList);
        }
        console.log('prepareOutStandingList : ', prepareOutStandingList.length)
        console.log('prepareTodayBalanceList : ', prepareTodayBalanceList.length)
        // console.log(prepareTodayBalanceList);
        let tasks = [];


        console.log('111111111111111111111111111111111111111111111111111111111111')
        if (params.type === 'AGENT') {
            _.map(mergeDataList, (item) => {
                tasks.push(getYesterdayPaymentBalanceTask(item._id.toString(), item._type, item.lastPaymentDate, userInfo.group.type));
            });
        } else {
            _.map(mergeDataList, (item) => {
                tasks.push(getYesterdayMemberPaymentBalanceTask(item._id.toString(), item.lastPaymentDate));
            });
        }

        async.parallel(tasks, (err, results) => {
            if (err) {
                console.log(err)
                // return res.send({message: "error", results: err, code: 999});
            }

            let previousList = _.filter(results, (item) => {
                return item.length > 0;
            }).map((preItem) => {
                return preItem[0];
            });


            mergeDataList = _.map(mergeDataList, (item) => {
                // console.log(item)


                let outStand = _.filter(prepareOutStandingList, (oItem) => {
                    return oItem._id.toString() == item._id.toString();
                })[0];

                let todayBalance = _.filter(prepareTodayBalanceList, (oItem) => {
                    return oItem.group.toString() == item._id.toString();
                })[0];

                let yesterdayBalance = _.filter(previousList, (oItem) => {
                    return oItem.group.toString() == item._id.toString();
                })[0];

                let newItem = Object.assign({}, item);

                if (outStand) {
                    newItem.totalOutstanding = outStand.amount;
                } else {
                    newItem.totalOutstanding = 0;
                }

                //yesterday balance
                if (yesterdayBalance) {
                    if (yesterdayBalance.type === 'M_GROUP') {
                        newItem.yesterdayBalance = yesterdayBalance.balance;
                    } else {
                        newItem.yesterdayBalance = yesterdayBalance.balance;
                    }
                } else {
                    newItem.yesterdayBalance = 0;
                }

                //today balance
                if (todayBalance) {
                    if (todayBalance.type === 'M_GROUP') {
                        newItem.todayWinLose = todayBalance.memberTotalWinLoseCom;
                    } else {
                        newItem.todayWinLose = todayBalance.memberTotalWinLoseCom;
                    }
                } else {
                    newItem.todayWinLose = 0;
                }


                // console.log("==================");

                if (newItem.balance < 0) {
                    newItem.balance = newItem.balance + newItem.totalOutstanding;
                }
                newItem.totalBalance = newItem.yesterdayBalance + newItem.todayWinLose;
                return newItem;

            });

            mergeDataList = mergeDataList.sort(function (a, b) {
                return naturalSort(a.username + a.name, b.username + b.name);
            });

            mergeDataList = _.sortBy(mergeDataList, function (o) {
                return o.suspend ? 1 : -1;
            });

            mergeDataList = _.sortBy(mergeDataList, function (o) {
                return o.lock ? 1 : -1;
            });

            mergeDataList = _.sortBy(mergeDataList, (o) => {
                return o.active ? -1 : 1;
            });
            callback(null, mergeDataList);
        });

    });
}

function findAllPaymentListPagin(userInfo, params, callback) {

    let page = params.page ? Number.parseInt(params.page) : 10;
    let limit = params.limit ? Number.parseInt(params.limit) : 1;

    console.log('findAllPaymentList2 params : ', params);

    let groupId = userInfo.group._id;

    AgentGroupModel.findById(groupId).select('type parentId').populate({
        path: 'parentId',
        select: '_id type'
    }).exec((err, agentResponse) => {
        if (err) {
            callback(err, null);
        } else {

            console.log('agentResponse : ', agentResponse)

            async.parallel([(callback => {

                if (params.type === 'MEMBER') {
                    let condition = {};
                    condition.group = mongoose.Types.ObjectId(groupId);
                    if (params.username) {
                        condition.username_lower = params.username.toLowerCase()
                    }

                    if (params.active) {
                        condition.active = params.active;
                    }

                    MemberModel.paginate(condition, {
                        select: "username contact currency creditLimit balance createdDate suspend lock active lastPaymentDate",
                        lean: true,
                        page: page,
                        limit: limit
                    }, function (err, data) {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, data);
                        }

                    });
                } else {
                    callback(null, []);
                }

            }), callback => {
                // let userInfo = req.userInfo.type;
                async.series([(subCallback => {
                    AgentGroupModel.findById(groupId, (err, response) => {

                        if (err) {
                            subCallback(err, null);
                        } else {
                            subCallback(null, response);
                        }

                    }).select('type parentId').populate({
                        path: 'parentId',
                        select: '_id type'
                    });

                }), (subCallback => {

                    if (params.type === 'MEMBER') {

                        let condition = {gameDate: {$gte: new Date(moment().add(-30, 'd').format('YYYY-MM-DDT11:00:00.000'))}};
                        condition['memberParentGroup'] = mongoose.Types.ObjectId(groupId);
                        condition['game'] = {$in: ['FOOTBALL', 'LOTTO', 'M2']};
                        condition['status'] = 'RUNNING';


                        BetTransactionModel.aggregate([
                            {
                                $match: condition
                            },
                            {
                                "$group": {
                                    "_id": {
                                        commission: '$memberParentGroup',
                                        member: '$memberId'
                                        // m:'$memberParentGroup'
                                    },
                                    "amount": {$sum: {$abs: '$memberCredit'}}
                                }
                            },
                            {
                                $project: {
                                    _id: '$_id.member',
                                    type: 'M_GROUP',
                                    member: '$_id.member',
                                    group: '$_id.commission',
                                    amount: '$amount'
                                }
                            }
                        ]).read('secondaryPreferred').option({maxTimeMS: 60000}).exec((err, results) => {
                            if (err) {
                                subCallback(err, null);
                            } else {
                                console.log('outstand member  : ', results.length);
                                MemberModel.populate(results, {
                                    path: 'member',
                                    select: '_id username username_lower contact'
                                }, function (err, memberResult) {
                                    if (err) {
                                        subCallback(err, null);
                                    } else {
                                        subCallback(null, memberResult);
                                    }
                                });
                            }
                        });
                    } else {
                        subCallback(null, []);
                    }

                }), (subCallback => {
                    AgentGroupModel.findById(groupId, (err, response) => {
                        if (err) {
                            subCallback(err, null);
                        } else {
                            subCallback(null, response);
                        }
                    }).select('type parentId').populate({
                        path: 'parentId',
                        select: '_id type'
                    });

                }), (subCallback => {

                    let todayTransaction = [];


                    if (params.type === 'AGENT') {

                        ReportService.isElasticEnabled(isElsEnabled => {

                            if (false) {

                                subCallback(null, todayTransaction)

                            } else {
                                let group = '';
                                let childGroup;

                                if (userInfo.group.type === 'SUPER_ADMIN') {
                                    group = 'superAdmin';
                                    childGroup = 'company';
                                } else if (userInfo.group.type === 'COMPANY') {
                                    group = 'company';
                                    childGroup = 'shareHolder';
                                } else if (userInfo.group.type === 'SHARE_HOLDER') {
                                    group = 'shareHolder';
                                    childGroup = 'senior';
                                } else if (userInfo.group.type === 'SENIOR') {
                                    group = "senior";
                                    childGroup = 'masterAgent,agent';
                                } else if (userInfo.group.type === 'MASTER_AGENT') {
                                    group = "masterAgent";
                                    childGroup = 'agent';
                                } else if (userInfo.group.type === 'AGENT') {
                                    group = "agent";
                                    childGroup = '';
                                }


                                async.map(childGroup.split(','), (child, callback) => {

                                    let todayWinLoseCondition = [];

                                    if (moment().hours() >= 11) {

                                        todayWinLoseCondition.push({
                                            gameDate: {
                                                "$gte": moment().millisecond(0).second(0).minute(0).hour(11).add(0, 'd').utc(true).toDate(),
                                                "$lt": moment().millisecond(0).second(0).minute(0).hour(11).add(1, 'd').utc(true).toDate()
                                            }
                                        });

                                    } else {

                                        todayWinLoseCondition.push({
                                            gameDate: {
                                                "$gte": moment().millisecond(0).second(0).minute(0).hour(11).add(-1, 'd').utc(true).toDate(),
                                                "$lt": moment().millisecond(0).second(0).minute(0).hour(11).add(0, 'd').utc(true).toDate()
                                            }
                                        });

                                    }
                                    todayWinLoseCondition.push({['commission.' + group + '.group']: mongoose.Types.ObjectId(groupId)})
                                    todayWinLoseCondition.push({['commission.' + child + '.parentGroup']: mongoose.Types.ObjectId(groupId)})
                                    todayWinLoseCondition.push({'status': 'DONE'});
                                    todayWinLoseCondition.push({'memberParentGroup': {$ne: mongoose.Types.ObjectId(groupId)}})

                                    console.log('start : ', todayWinLoseCondition)
                                    BetTransactionModel.aggregate([
                                        {
                                            $match: {'$and': todayWinLoseCondition}
                                        },
                                        {
                                            "$group": {
                                                "_id": {
                                                    commission: '$commission.' + child + '.group'
                                                },
                                                "memberWinLose": {$sum: '$commission.member.winLose'},
                                                "memberWinLoseCom": {$sum: '$commission.member.winLoseCom'},
                                                "memberTotalWinLoseCom": {$sum: '$commission.member.totalWinLoseCom'},

                                                "agentWinLose": {$sum: '$commission.agent.winLose'},
                                                "agentWinLoseCom": {$sum: '$commission.agent.winLoseCom'},
                                                "agentTotalWinLoseCom": {$sum: '$commission.agent.totalWinLoseCom'},

                                                "masterAgentWinLose": {$sum: '$commission.masterAgent.winLose'},
                                                "masterAgentWinLoseCom": {$sum: '$commission.masterAgent.winLoseCom'},
                                                "masterAgentTotalWinLoseCom": {$sum: '$commission.masterAgent.totalWinLoseCom'},

                                                "seniorWinLose": {$sum: '$commission.senior.winLose'},
                                                "seniorWinLoseCom": {$sum: '$commission.senior.winLoseCom'},
                                                "seniorTotalWinLoseCom": {$sum: '$commission.senior.totalWinLoseCom'},

                                                "shareHolderWinLose": {$sum: '$commission.shareHolder.winLose'},
                                                "shareHolderWinLoseCom": {$sum: '$commission.shareHolder.winLoseCom'},
                                                "shareHolderTotalWinLoseCom": {$sum: '$commission.shareHolder.totalWinLoseCom'},

                                                "companyWinLose": {$sum: '$commission.company.winLose'},
                                                "companyWinLoseCom": {$sum: '$commission.company.winLoseCom'},
                                                "companyTotalWinLoseCom": {$sum: '$commission.company.totalWinLoseCom'},

                                                "superAdminWinLose": {$sum: '$commission.superAdmin.winLose'},
                                                "superAdminWinLoseCom": {$sum: '$commission.superAdmin.winLoseCom'},
                                                "superAdminTotalWinLoseCom": {$sum: '$commission.superAdmin.totalWinLoseCom'}
                                            }
                                        },
                                        {
                                            $project: {
                                                _id: 0,
                                                type: 'A_GROUP',
                                                group: '$_id.commission',
                                                memberWinLose: '$memberWinLose',
                                                memberWinLoseCom: '$memberWinLoseCom',
                                                memberTotalWinLoseCom: '$memberTotalWinLoseCom',
                                                agentWinLose: '$agentWinLose',
                                                agentWinLoseCom: '$agentWinLoseCom',
                                                agentTotalWinLoseCom: '$agentTotalWinLoseCom',
                                                masterAgentWinLose: '$masterAgentWinLose',
                                                masterAgentWinLoseCom: '$masterAgentWinLoseCom',
                                                masterAgentTotalWinLoseCom: '$masterAgentTotalWinLoseCom',
                                                seniorWinLose: '$seniorWinLose',
                                                seniorWinLoseCom: '$seniorWinLoseCom',
                                                seniorTotalWinLoseCom: '$seniorTotalWinLoseCom',
                                                shareHolderWinLose: '$shareHolderWinLose',
                                                shareHolderWinLoseCom: '$shareHolderWinLoseCom',
                                                shareHolderTotalWinLoseCom: '$shareHolderTotalWinLoseCom',
                                                companyWinLose: '$companyWinLose',
                                                companyWinLoseCom: '$companyWinLoseCom',
                                                companyTotalWinLoseCom: '$companyTotalWinLoseCom',
                                                superAdminWinLose: '$superAdminWinLose',
                                                superAdminWinLoseCom: '$superAdminWinLoseCom',
                                                superAdminTotalWinLoseCom: '$superAdminTotalWinLoseCom',

                                            }
                                        }
                                    ]).read('secondaryPreferred').option({maxTimeMS: 60000}).exec((err, results) => {

                                        _.each(results, item => {
                                            todayTransaction.push(item);
                                        });
                                        callback();
                                    });

                                }, (err) => {

                                    if (err) {
                                        subCallback(err, null);
                                        return;
                                    }
                                    subCallback(null, todayTransaction)
                                });
                            }


                        });


                    } else {
                        //MEMBER
                        subCallback(null, [])
                    }

                })], function (err, asyncResponse) {

                    if (err) {
                        callback(err, null);
                        return;
                    }

                    console.log('xxxx11xxxx')
                    let agentGroup = asyncResponse[0];

                    let group = '';
                    let childGroup;
                    if (agentGroup.type === 'SUPER_ADMIN') {
                        group = 'superAdmin';
                        childGroup = 'company';
                    } else if (agentGroup.type === 'COMPANY') {
                        group = 'company';
                        childGroup = 'shareHolder';
                    } else if (agentGroup.type === 'SHARE_HOLDER') {
                        group = 'shareHolder';
                        childGroup = 'senior';
                    } else if (agentGroup.type === 'SENIOR') {
                        group = "senior";
                        childGroup = 'masterAgent,agent';
                    } else if (agentGroup.type === 'MASTER_AGENT') {
                        group = "masterAgent";
                        childGroup = 'agent';
                    } else if (agentGroup.type === 'AGENT') {
                        group = "agent";
                        childGroup = '';
                    }


                    let transaction = [];

                    async.map(childGroup.split(','), (child, callback) => {

                        let condition = {};

                        condition['gameDate'] = {$gte: new Date(moment().add(-60, 'd').format('YYYY-MM-DDT11:00:00.000'))};
                        condition['memberParentGroup'] = {$ne: mongoose.Types.ObjectId(groupId)};
                        condition['status'] = 'RUNNING';
                        condition['game'] = {$in: ['FOOTBALL', 'LOTTO', 'M2']};
                        condition['$and'] = [
                            {['commission.' + child + '.parentGroup']: mongoose.Types.ObjectId(groupId)},
                            {['commission.' + group + '.group']: mongoose.Types.ObjectId(groupId)}
                        ];

                        console.log('1111111')

                        BetTransactionModel.aggregate([
                            {
                                $match: condition
                            },
                            {
                                "$group": {
                                    "_id": {
                                        commission: '$commission.' + child + '.group',
                                    },
                                    "amount": {$sum: '$commission.' + group + '.amount'}
                                }
                            },
                            {
                                $project: {
                                    _id: '$_id.commission',
                                    type: 'A_GROUP',
                                    group: '$_id.commission',
                                    amount: '$amount'
                                }
                            }
                        ]).read('secondaryPreferred').option({maxTimeMS: 60000}).exec((err, results) => {
                            _.each(results, item => {
                                transaction.push(item);
                            });
                            callback();
                        });
                    }, (err) => {

                        if (err) {
                            callback(err, null);
                            return;
                        }
                        let memberGroupList = asyncResponse[1];
                        let mergeDataList = transaction.concat(memberGroupList);
                        let mergeTodayBalance = asyncResponse[3];


                        console.log('outstand agent  : ', transaction.length);
                        async.series([(subCallback => {

                            AgentGroupModel.populate(mergeDataList, {
                                path: 'group',
                                select: '_id name name_lower contact'
                            }, function (err, memberResult) {
                                if (err) {
                                    subCallback(err, null);
                                } else {
                                    subCallback(null, memberResult);
                                }
                            });


                        })], (err, populateResults) => {
                            let result = {
                                outStandingList: mergeDataList,
                                todayBalanceList: mergeTodayBalance
                            };
                            callback(null, result)
                        });
                    });
                });

            }, callback => {

                if (params.type === 'AGENT') {
                    let condition = {};
                    condition.parentId = mongoose.Types.ObjectId(groupId);
                    if (params.username) {
                        condition.name_lower = params.username.toLowerCase()
                    }
                    if (params.active) {
                        condition.active = params.active;
                    }

                    AgentGroupModel.paginate(condition, {
                        select: "name contact currency creditLimit balance createdDate active type lastPaymentDate suspend lock active",
                        lean: true,
                        page: page,
                        limit: limit,
                        populate: {
                            path: 'parentId',
                            select: 'name contact currency creditLimit balance createdDate active'
                        }
                    }, function (err, data) {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, data)
                        }
                    });
                } else {
                    callback(null, []);
                }

            }], (err, results) => {

                let paginData = results[0];
                let memberList = results[0].docs;
                let outStandingList = results[1].outStandingList;
                let todayBalanceList = results[1].todayBalanceList;


                let member = _.map(memberList, (value) => {
                    let s = Object.assign({}, value);
                    s.type = 'MEMBER';
                    return s;
                });

                let mergeDataList = member;

                if (params.type === 'AGENT') {
                    paginData = results[2];
                    let agentList = results[2].docs;
                    let agent = _.map(agentList, (value) => {
                        let s = Object.assign({}, value);
                        s.type = 'AGENT';
                        s._type = value.type;
                        return s;
                    });
                    mergeDataList = mergeDataList.concat(agent);
                }

                console.log('mergeDataList : ', mergeDataList.length);

                let prepareOutStandingList = transformDataByAgentType(userInfo.group.type, outStandingList);

                let prepareTodayBalanceList = todayBalanceList;
                if (params.type === 'AGENT') {
                    prepareTodayBalanceList = transformDataByAgentTypeReport(userInfo.group.type, todayBalanceList);
                }
                console.log('prepareOutStandingList : ', prepareOutStandingList.length)
                console.log('prepareTodayBalanceList : ', prepareTodayBalanceList.length)
                // console.log(prepareTodayBalanceList);
                let tasks = [];


                if (params.type === 'AGENT') {
                    _.map(mergeDataList, (item) => {
                        console.log('------------------------- :: ', item)
                        if (item.active && !item.suspend) {
                            tasks.push(getYesterdayPaymentBalanceTaskNew(item._id.toString(), item._type, item.lastPaymentDate, agentResponse));
                        }
                    });
                } else {
                    // _.map(mergeDataList, (item) => {
                    //     tasks.push(getYesterdayMemberPaymentBalanceTask(item._id.toString(), item.lastPaymentDate));
                    // });
                }

                console.log('task length : ', tasks.length)
                async.parallel(tasks, (err, results) => {
                    if (err) {
                        console.log(err);
                        // return res.send({message: "error", results: err, code: 999});
                    }

                    console.log('--------------------- :: ', results)

                    let previousList = _.filter(results, (item) => {
                        return item.length > 0;
                    }).map((preItem) => {
                        return preItem[0];
                    });


                    mergeDataList = _.map(mergeDataList, (item) => {
                        // console.log(item)


                        let outStand = _.filter(prepareOutStandingList, (oItem) => {
                            return oItem._id.toString() == item._id.toString();
                        })[0];

                        let todayBalance = _.filter(prepareTodayBalanceList, (oItem) => {
                            return oItem.group.toString() == item._id.toString();
                        })[0];

                        let yesterdayBalance = _.filter(previousList, (oItem) => {

                            console.log(oItem.group.toString() + ' , ' + item._id.toString())
                            return oItem.group.toString() == item._id.toString();
                        })[0];

                        let newItem = Object.assign({}, item);

                        if (outStand) {
                            newItem.totalOutstanding = outStand.amount;
                        } else {
                            newItem.totalOutstanding = 0;
                        }

                        //yesterday balance
                        if (yesterdayBalance) {
                            if (yesterdayBalance.type === 'M_GROUP') {
                                newItem.yesterdayBalance = yesterdayBalance.balance;
                            } else {
                                newItem.yesterdayBalance = yesterdayBalance.balance;
                            }
                        } else {
                            newItem.yesterdayBalance = 0;
                        }

                        //today balance
                        if (todayBalance) {
                            if (todayBalance.type === 'M_GROUP') {
                                newItem.todayWinLose = todayBalance.memberTotalWinLoseCom;
                            } else {
                                newItem.todayWinLose = todayBalance.memberTotalWinLoseCom;
                            }
                        } else {
                            newItem.todayWinLose = 0;
                        }


                        // console.log("==================");

                        if (newItem.balance < 0) {
                            newItem.balance = newItem.balance + newItem.totalOutstanding;
                        }
                        newItem.totalBalance = newItem.yesterdayBalance + newItem.todayWinLose;
                        return newItem;

                    });

                    // mergeDataList = _.filter(mergeDataList,item=>{
                    //     return item.totalBalance != 0;
                    // });

                    mergeDataList = mergeDataList.sort(function (a, b) {
                        return naturalSort(a.username + a.name, b.username + b.name);
                    });

                    mergeDataList = _.sortBy(mergeDataList, function (o) {
                        return o.suspend ? 1 : -1;
                    });

                    mergeDataList = _.sortBy(mergeDataList, function (o) {
                        return o.lock ? 1 : -1;
                    });

                    mergeDataList = _.sortBy(mergeDataList, (o) => {
                        return o.active ? -1 : 1;
                    });

                    paginData.docs = mergeDataList;
                    callback(null, paginData);
                });

            });
        }
    });

}

function transformDataByAgentType(type, dataList) {
    if (type === 'SUPER_ADMIN') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = 0;

            newItem.agentWinLose = item.superAdminWinLose;

            newItem.memberWinLose = (item.masterAgentWinLose + item.agentWinLose + item.seniorWinLose + item.shareHolderWinLose + item.companyWinLose);

            return newItem;
        });
    } else if (type === 'COMPANY') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = item.superAdminWinLose;

            newItem.agentWinLose = item.companyWinLose;

            newItem.memberWinLose = (item.masterAgentWinLose + item.agentWinLose + item.seniorWinLose + item.shareHolderWinLose);

            return newItem;
        });
    } else if (type === 'SHARE_HOLDER') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose;

            newItem.agentWinLose = item.shareHolderWinLose;

            newItem.memberWinLose = (item.masterAgentWinLose + item.agentWinLose + item.seniorWinLose);
            return newItem;
        });
    } else if (type === 'SENIOR') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose + item.shareHolderWinLose;

            newItem.agentWinLose = item.seniorWinLose;

            newItem.memberWinLose = (item.masterAgentWinLose + item.agentWinLose);
            return newItem;
        });
    } else if (type === 'MASTER_AGENT') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose + item.shareHolderWinLose + item.seniorWinLose;

            newItem.agentWinLose = item.masterAgentWinLose;

            newItem.memberWinLose = (item.agentWinLose);

            return newItem;
        });
    } else if (type === 'AGENT') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);
            newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose + item.shareHolderWinLose + item.seniorWinLose + item.masterAgentWinLose;

            newItem.agentWinLose = item.agentWinLose;

            newItem.memberWinLose = item.memberWinLose;
            return newItem;
        });
    } else {
        return dataList;
    }
}

router.get('/history', function (req, res) {

    let condition = {};

    let userInfo = req.userInfo;


    if (userInfo.group._id) {
        condition.group = userInfo.group._id;
    }

    PaymentHistoryModel.paginate(condition, {
            page: Number.parseInt(req.query.page),
            limit: Number.parseInt(req.query.limit),
            populate: ['member', 'agent'],
            sort: {_id: -1}
        },
        function (err, data) {
            if (err)
                return res.send({message: "error", results: err, code: 999});

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: data,
                    total: data.total,
                    limit: data.limit,
                    page: data.page,
                    pages: data.pages,
                }
            );
        });

});


const pay2Schema = Joi.object().keys({
    id: Joi.string().required(),
    type: Joi.string().required(),
    amount: Joi.number().allow(''),
});

router.post('/2', function (req, res) {

    let validate = Joi.validate(req.body, pay2Schema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    const userInfo = req.userInfo;


    UserModel.findOne({_id: userInfo._id}, (err, checkAgentResponse) => {


        if (!checkAgentResponse) {

            return res.send({
                message: "UnAuthorization",
                result: err,
                code: 401
            });


        } else {

            let payAmount = req.body.amount;


            let endDate;
            if (moment().hours() >= 11) {
                endDate = moment().add(-1, 'days').second(0).minute(0).hour(0).utc(true).toDate();
            } else {
                endDate = moment().add(-2, 'days').second(0).minute(0).hour(0).utc(true).toDate();
            }


            async.waterfall([callback => {

                AgentService.findById(userInfo.group._id, (err, response) => {

                    let currentDateString = DateUtils.getCurrentAmbDateString();

                    // console.log('currentDateString : ',currentDateString);
                    // console.log('paymentSetting : ',response.paymentSetting);

                    if (currentDateString === 'MONDAY' && response.paymentSetting.monday
                        || currentDateString === 'TUESDAY' && response.paymentSetting.tuesday
                        || currentDateString === 'WEDNESDAY' && response.paymentSetting.wednesday
                        || currentDateString === 'THURSDAY' && response.paymentSetting.thursday
                        || currentDateString === 'FRIDAY' && response.paymentSetting.friday
                        || currentDateString === 'SATURDAY' && response.paymentSetting.saturday
                        || currentDateString === 'SUNDAY' && response.paymentSetting.sunday) {

                        callback(null, response);
                    } else {
                        callback(6004, null);
                    }

                });

            }, (agentInfo, callback) => {

                if (req.body.type === 'MEMBER') {

                    AgentService.isAllowPermission(userInfo._id, req.body.id, (err, isAllow) => {
                        if (!isAllow) {
                            callback(isAllow, null);
                        } else {
                            let condition = {};
                            condition._id = mongoose.Types.ObjectId(req.body.id);
                            MemberModel.findOne(condition, (err, response) => {
                                if (err) {
                                    callback(err, null);
                                } else {
                                    MemberService.getOutstanding(req.body.id, (err, outstanding) => {
                                        if (err) {
                                            callback(err, null);
                                        } else {
                                            let obj = Object.assign({}, response)._doc;
                                            obj.outstanding = outstanding;
                                            callback(null, agentInfo, obj);
                                        }
                                    });
                                    // callback(null, agentInfo, response);
                                }
                            }).select('username contact currency balance creditLimit createdDate active lastPaymentDate');
                        }
                    });


                } else {
                    let condition = {};
                    condition._id = mongoose.Types.ObjectId(req.body.id);
                    AgentGroupModel.findOne(condition, (err, response) => {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, agentInfo, response);
                        }
                    }).select('name contact currency balance creditLimit createdDate active type lastPaymentDate');
                }

            }, (agentInfo, userPaymentInfo, callback) => {


                if (req.body.type === 'AGENT') {

                    let x = getYesterdayPaymentBalanceTask(userPaymentInfo._id, userPaymentInfo.type, userPaymentInfo.lastPaymentDate, userInfo.group.type);


                    x((err, yesterdayPaymentResponse) => {

                        console.log('----=====----=-=-=-=-==-======-----======-----')
                        console.log(yesterdayPaymentResponse);

                        let result = yesterdayPaymentResponse[0];
                        if (yesterdayPaymentResponse && yesterdayPaymentResponse.length == 0) {
                            result = {
                                balance: 0
                            }
                        }

                        AgentGroupModel.findOneAndUpdate({_id: userPaymentInfo._id}, {$set: {lastPaymentDate: endDate}})
                            .exec(function (err, response) {
                                if (err) {
                                    callback(err, null);
                                } else {
                                    callback(null, agentInfo, userPaymentInfo, result);
                                }
                            });
                        // } else {
                        //     callback(6002, null);
                        // }


                    });

                } else {


                    if (userPaymentInfo.balance > 0) {

                        if (payAmount < (userPaymentInfo.balance * -1) || payAmount >= 0) {
                            console.log('1')
                            callback('6001', null);
                            return;
                        }
                    } else {

                        if (payAmount > ((userPaymentInfo.balance + userPaymentInfo.outstanding) * -1) || payAmount <= 0) {
                            callback('6001', null);
                            return;
                        }
                    }


                    MemberModel.findOneAndUpdate({_id: userPaymentInfo._id}, {
                        $inc: {balance: payAmount},
                        $set: {lastPaymentDate: endDate}
                    }, {"new": true})
                        .exec(function (err, response) {
                            if (err) {
                                callback(err, null);
                            } else {
                                callback(null, agentInfo, userPaymentInfo, response);
                            }
                        });
                }


            }], (err, agentInfo, userPaymentInfo, updateBalanceInfo) => {

                if (err) {
                    if (err == 6001) {
                        return res.send({
                            message: "Payment should not more than outstanding amount!",
                            result: null,
                            code: 5001
                        });
                    }
                    if (err == 6002) {
                        return res.send({
                            message: "Success",
                            result: null,
                            code: 0
                        });
                    }
                    if (err == 6003) {
                        return res.send({
                            message: "Cannot transfer balance!",
                            result: null,
                            code: 6003
                        });
                    }
                    if (err == 6004) {
                        return res.send({
                            message: "Cannot transfer on today! Please contact your upline.",
                            result: null,
                            code: 6004
                        });
                    }
                    return res.send({message: "error", results: err, code: 999});
                }


                let body = {};
                if (req.body.type === 'AGENT') {
                    body = {
                        type: req.body.type,
                        amount: updateBalanceInfo.balance,
                        remainingBalance: 0,
                        currency: userPaymentInfo.currency,
                        group: userInfo.group._id,
                        createdBy: userInfo._id,
                        createdDate: DateUtils.getCurrentDate(),
                        startDate: userPaymentInfo.lastPaymentDate,
                        endDate: endDate,
                        agent: userPaymentInfo._id
                    };
                } else {
                    body = {
                        type: req.body.type,
                        amount: payAmount,
                        remainingBalance: updateBalanceInfo.balance,
                        currency: userPaymentInfo.currency,
                        group: userInfo.group._id,
                        createdBy: userInfo._id,
                        createdDate: DateUtils.getCurrentDate(),
                        member: userPaymentInfo._id
                    };
                }

                let model = new PaymentHistoryModel(body);

                model.save(function (err, data) {

                    if (err) {
                        return res.send({message: "error", results: err, code: 999});
                    }
                    return res.send(
                        {
                            code: 0,
                            message: "success",
                            result: data
                        }
                    );
                });

            });
        }

    });
});

function paymentTask(userInfo, paymentObj) {

    return function (callbackTask) {

        let endDate;
        if (moment().hours() >= 11) {
            endDate = moment().add(-1, 'days').second(0).minute(0).hour(0).utc(true).toDate();
        } else {
            endDate = moment().add(-2, 'days').second(0).minute(0).hour(0).utc(true).toDate();
        }


        let type = paymentObj.type;
        let _id = paymentObj._id;
        async.waterfall([callback => {

            if (type === 'MEMBER') {
                let condition = {};
                condition._id = mongoose.Types.ObjectId(_id);
                MemberModel.findOne(condition, (err, response) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        MemberService.getOutstanding(_id, (err, outstanding) => {
                            if (err) {
                                callback(err, null);
                            } else {
                                let obj = Object.assign({}, response)._doc;
                                obj.outstanding = outstanding;
                                callback(null, obj);
                            }
                        });
                    }
                }).select('username contact currency balance creditLimit createdDate active lastPaymentDate');
            } else {
                let condition = {};
                condition._id = mongoose.Types.ObjectId(_id);
                AgentGroupModel.findOne(condition, (err, response) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null, response);
                    }
                }).select('name contact currency balance creditLimit createdDate active type lastPaymentDate');
            }

        }, (userPaymentInfo, callback) => {


            if (type === 'AGENT') {


                // if(paymentObj.balance)

                let x = getYesterdayPaymentBalanceTask(userPaymentInfo._id, userPaymentInfo.type, userPaymentInfo.lastPaymentDate, userInfo.group.type);


                x((err, yesterdayPaymentResponse) => {
                    console.log('00000000000000000000000000');
                    console.log(yesterdayPaymentResponse);

                    let result = yesterdayPaymentResponse[0];
                    if (yesterdayPaymentResponse && yesterdayPaymentResponse.length == 0) {
                        result = {
                            balance: 0
                        }
                    }

                    // if (yesterdayPaymentResponse && yesterdayPaymentResponse.length > 0 && yesterdayPaymentResponse.balance != 0) {

                    AgentGroupModel.findOneAndUpdate({_id: userPaymentInfo._id}, {$set: {lastPaymentDate: endDate}})
                        .exec(function (err, response) {
                            if (err) {
                                callback(err, null);
                            } else {
                                callback(null, userPaymentInfo, result);
                            }
                        });
                    // } else {
                    //     callback(6002, null);
                    // }

                });

            } else {

                let balance = userPaymentInfo.balance < 0 ? (userPaymentInfo.balance + userPaymentInfo.outstanding ) * -1 : userPaymentInfo.balance * -1;

                MemberModel.findOneAndUpdate({_id: userPaymentInfo._id}, {
                    $inc: {balance: balance},
                    $set: {lastPaymentDate: endDate}
                }, {"new": true})
                    .exec(function (err, response) {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, userPaymentInfo, response);
                        }
                    });

            }

        }], (err, userPaymentInfo, updateBalanceInfo) => {

            if (err) {
                if (err == 6002) {
                    callbackTask(null, {});
                } else {
                    callbackTask(err, null);
                }
                return;
            }


            let body = {};
            if (type === 'AGENT') {
                body = {
                    type: type,
                    amount: updateBalanceInfo.balance,
                    remainingBalance: 0,
                    currency: userPaymentInfo.currency,
                    group: userInfo.group._id,
                    createdBy: userInfo._id,
                    createdDate: DateUtils.getCurrentDate(),
                    startDate: userPaymentInfo.lastPaymentDate,
                    endDate: endDate,
                    agent: userPaymentInfo._id
                };

                let model = new PaymentHistoryModel(body);

                model.save(function (err, data) {

                    if (err) {
                        callbackTask(err, null);
                    } else {
                        callbackTask(null, data);
                    }
                });

            } else {

                let balance = userPaymentInfo.balance < 0 ? (userPaymentInfo.balance + userPaymentInfo.outstanding ) * -1 : userPaymentInfo.balance * -1;
                body = {
                    type: type,
                    amount: balance,
                    remainingBalance: 0,
                    currency: userPaymentInfo.currency,
                    group: userInfo.group._id,
                    createdBy: userInfo._id,
                    createdDate: DateUtils.getCurrentDate(),
                    startDate: userPaymentInfo.lastPaymentDate,
                    endDate: endDate,
                    member: userPaymentInfo._id
                };

                if (balance != 0) {
                    let model = new PaymentHistoryModel(body);

                    model.save(function (err, data) {

                        if (err) {
                            callbackTask(err, null);
                        } else {
                            callbackTask(null, data);
                        }
                    });

                } else {
                    callbackTask(null, {});
                }
            }


        });
    }
}


const payAllSchema = Joi.object().keys({
    ids: Joi.string().required()
});

router.post('/payAll', function (req, res) {

    const userInfo = req.userInfo;

    let validate = Joi.validate(req.body, payAllSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    let ids = req.body.ids;


    UserModel.findOne({_id: userInfo._id}, (err, checkAgentResponse) => {


        if (!checkAgentResponse) {

            return res.send({
                message: "UnAuthorization",
                result: err,
                code: 401
            });


        } else {

            async.series([callback => {

                AgentService.findById(userInfo.group._id, (err, response) => {

                    let currentDateString = DateUtils.getCurrentAmbDateString();

                    // console.log('currentDateString : ',currentDateString);
                    // console.log('paymentSetting : ',response.paymentSetting);

                    if (currentDateString === 'MONDAY' && response.paymentSetting.monday
                        || currentDateString === 'TUESDAY' && response.paymentSetting.tuesday
                        || currentDateString === 'WEDNESDAY' && response.paymentSetting.wednesday
                        || currentDateString === 'THURSDAY' && response.paymentSetting.thursday
                        || currentDateString === 'FRIDAY' && response.paymentSetting.friday
                        || currentDateString === 'SATURDAY' && response.paymentSetting.saturday
                        || currentDateString === 'SUNDAY' && response.paymentSetting.sunday) {

                        callback(null, response);
                    } else {
                        callback(6004, null);
                    }

                });

            }], (err, asyncResponse) => {

                if (err) {
                    if (err == 6004) {
                        return res.send({
                            message: "Cannot transfer on today! Please contact your upline.",
                            result: null,
                            code: 6004
                        });
                    }
                    return res.send({message: "error", results: err, code: 999});
                }

                findAllPaymentList(userInfo, function (err, paymentList) {

                    let tasks = [];
                    _.chain(paymentList).filter((item) => {
                        return ids.indexOf(item._id.toString()) != -1;
                    }).each((item) => {
                        tasks.push(paymentTask(userInfo, item));
                    });

                    async.parallel(tasks, (err, results) => {
                        if (err) {
                            return res.send({message: "error", results: err, code: 999});
                        }
                        return res.send(
                            {
                                code: 0,
                                message: "success",
                                result: results
                            }
                        );
                    });

                })
            });

        }
    });

});


function getYesterdayPaymentBalanceTask(agentId, agentType, lastPaymentDate, currentAgentType) {

    return function (callbackTask) {

        let group = '';
        let child = '';

        getGroupAndChild(agentType, (groupRes, childRes) => {
            group = groupRes;
            child = childRes;
        });

        ReportService.isElasticEnabled(isElsEnabled => {

            if(false) {

                ReportService.queryMixUsePaymentReport(agentId, group, lastPaymentDate, (err, response) => {

                    let result = transformDataByAgentTypeReport(currentAgentType, response);

                    let finalResult = [];

                    if (result.length > 0) {
                        finalResult.push({
                            group: result[0].group,
                            balance: result[0].memberTotalWinLoseCom
                        })
                    }
                    callbackTask(null, finalResult);

                })

            } else {

                PaymentService.getWinLoseReportMember(agentId, group, lastPaymentDate, (err, response) => {

                    let result = transformDataByAgentTypeReport(currentAgentType, response);

                    let finalResult = [];

                    if (result.length > 0) {
                        finalResult.push({
                            group: result[0].group,
                            balance: result[0].memberTotalWinLoseCom
                        })
                    }
                    callbackTask(null, finalResult);
                });
            }

        });

    }
}

function getYesterdayPaymentBalanceTaskNew(agentId, agentType, lastPaymentDate, currentAgent) {

    return function (callbackTask) {

        let group = '';
        let child = '';

        getGroupAndChild(agentType, (groupRes, childRes) => {
            group = groupRes;
            child = childRes;
        });

        ReportService.isElasticEnabled(isElsEnabled => {

            if (false) {

                ReportService.queryMixUsePaymentReport(agentId, agentType, currentAgent.type, lastPaymentDate, (err, response) => {

                    console.log('------------------------------------------------------------',response)
                    let result = transformDataByAgentTypeReport(currentAgent.type, response);

                    let finalResult = [];

                    if (result.length > 0) {
                        finalResult.push({
                            group: result[0].group,
                            balance: result[0].memberTotalWinLoseCom
                        })
                    }
                    callbackTask(null, finalResult);

                })

            } else {

                PaymentService.getWinLoseReportMember(agentId, group, lastPaymentDate, (err, response) => {

                    let result = transformDataByAgentTypeReport(currentAgent.type, response);

                    let finalResult = [];

                    if (result.length > 0) {
                        finalResult.push({
                            group: result[0].group,
                            balance: result[0].memberTotalWinLoseCom
                        })
                    }
                    callbackTask(null, finalResult);
                });
            }

        });

    }
}


function getYesterdayMemberPaymentBalanceTask(memberId, lastPaymentDate) {

    return function (callbackTask) {


        let condition = {};

        let startYear = Number.parseInt(lastPaymentDate.getUTCFullYear());
        let startMonth = Number.parseInt(lastPaymentDate.getUTCMonth());
        let startDay = Number.parseInt(lastPaymentDate.getUTCDate());


        let currentAmbDay = moment().hours() >= 11 ? moment().utc(true).date() : moment().utc(true).date() - 1;

        console.log('currentAmbDay : ', currentAmbDay)

        if ((startDay + 1) == currentAmbDay) {
            callbackTask(null, []);
            return;
        }

        if (moment().hours() >= 11) {
            condition['gameDate'] = {
                "$gte": moment().millisecond(0).second(0).minute(0).hour(11).date(startDay).month(startMonth).year(startYear).add(1, 'd').utc(true).toDate(),
                "$lt": moment().millisecond(0).second(0).minute(0).hour(11).utc(true).toDate()
            };
        } else {
            condition['gameDate'] = {
                "$gte": moment().millisecond(0).second(0).minute(0).hour(11).date(startDay).month(startMonth).year(startYear).utc(true).toDate(),
                "$lt": moment().millisecond(0).second(0).minute(0).hour(11).add(-1, 'd').utc(true).toDate()
            };
        }

        condition['memberId'] = mongoose.Types.ObjectId(memberId);


        condition.status = 'DONE';


        BetTransactionModel.aggregate([
            {
                $match: condition
            },
            {
                "$group": {
                    "_id": {
                        commission: '$memberId',
                    },
                    "memberTotalWinLoseCom": {$sum: '$commission.member.totalWinLoseCom'},

                }
            },
            {
                $project: {
                    _id: 0,
                    group: '$_id.commission',
                    type: 'A_GROUP',
                    memberTotalWinLoseCom: '$memberTotalWinLoseCom',
                }
            }
        ]).read('secondaryPreferred').option({maxTimeMS: 60000}).exec((err, results) => {

            // let result = transformDataByAgentTypeReport(currentAgentType, results);

            let finalResult = [];

            if (results && results.length > 0) {
                finalResult.push({
                    group: results[0].group,
                    balance: results[0].memberTotalWinLoseCom
                })
            }
            callbackTask(null, finalResult);
        });
    }
}

function transformDataByAgentTypeReport(type, dataList) {
    if (type === 'COMPANY') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = item.superAdminWinLose;
            newItem.companyWinLoseCom = item.superAdminWinLoseCom;
            newItem.companyTotalWinLoseCom = item.superAdminTotalWinLoseCom;

            newItem.agentWinLose = item.companyWinLose;
            newItem.agentWinLoseCom = item.companyWinLoseCom;
            newItem.agentTotalWinLoseCom = item.companyTotalWinLoseCom;


            newItem.memberWinLose = (item.memberWinLose + item.masterAgentWinLose + item.agentWinLose + item.seniorWinLose + item.shareHolderWinLose);
            newItem.memberWinLoseCom = (item.memberWinLoseCom + item.masterAgentWinLoseCom + item.agentWinLoseCom + item.seniorWinLoseCom + item.shareHolderWinLoseCom);
            newItem.memberTotalWinLoseCom = (item.memberTotalWinLoseCom + item.masterAgentTotalWinLoseCom + item.agentTotalWinLoseCom + item.seniorTotalWinLoseCom + item.shareHolderTotalWinLoseCom);

            return newItem;
        });
    } else if (type === 'SHARE_HOLDER') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose;
            newItem.companyWinLoseCom = item.superAdminWinLoseCom + item.companyWinLoseCom;
            newItem.companyTotalWinLoseCom = item.superAdminTotalWinLoseCom + item.companyTotalWinLoseCom;

            newItem.agentWinLose = item.shareHolderWinLose;
            newItem.agentWinLoseCom = item.shareHolderWinLoseCom;
            newItem.agentTotalWinLoseCom = item.shareHolderTotalWinLoseCom;

            newItem.memberWinLose = (item.memberWinLose + item.masterAgentWinLose + item.agentWinLose + item.seniorWinLose);
            newItem.memberWinLoseCom = (item.memberWinLoseCom + item.masterAgentWinLoseCom + item.agentWinLoseCom + item.seniorWinLoseCom);
            newItem.memberTotalWinLoseCom = (item.memberTotalWinLoseCom + item.masterAgentTotalWinLoseCom + item.agentTotalWinLoseCom + item.seniorTotalWinLoseCom);
            return newItem;
        });
    } else if (type === 'SENIOR') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose + item.shareHolderWinLose;
            newItem.companyWinLoseCom = item.superAdminWinLoseCom + item.companyWinLoseCom + item.shareHolderWinLoseCom;
            newItem.companyTotalWinLoseCom = item.superAdminTotalWinLoseCom + item.companyTotalWinLoseCom + item.shareHolderTotalWinLoseCom;

            newItem.agentWinLose = item.seniorWinLose;
            newItem.agentWinLoseCom = item.seniorWinLoseCom;
            newItem.agentTotalWinLoseCom = item.seniorTotalWinLoseCom;


            newItem.memberWinLose = (item.memberWinLose + item.masterAgentWinLose + item.agentWinLose);
            newItem.memberWinLoseCom = (item.memberWinLoseCom + item.masterAgentWinLoseCom + item.agentWinLoseCom);
            newItem.memberTotalWinLoseCom = (item.memberTotalWinLoseCom + item.masterAgentTotalWinLoseCom + item.agentTotalWinLoseCom);

            return newItem;
        });
    } else if (type === 'MASTER_AGENT') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose + item.shareHolderWinLose + item.seniorWinLose;
            newItem.companyWinLoseCom = item.superAdminWinLoseCom + item.companyWinLoseCom + item.shareHolderWinLoseCom + item.seniorWinLoseCom;
            newItem.companyTotalWinLoseCom = item.superAdminTotalWinLoseCom + item.companyTotalWinLoseCom + item.shareHolderTotalWinLoseCom + item.seniorTotalWinLoseCom;

            newItem.agentWinLose = item.masterAgentWinLose;
            newItem.agentWinLoseCom = item.masterAgentWinLoseCom;
            newItem.agentTotalWinLoseCom = item.masterAgentTotalWinLoseCom;

            newItem.memberWinLose = (item.memberWinLose + item.agentWinLose);
            newItem.memberWinLoseCom = (item.memberWinLoseCom + item.agentWinLoseCom);
            newItem.memberTotalWinLoseCom = (item.memberTotalWinLoseCom + item.agentTotalWinLoseCom);

            return newItem;
        });
    } else if (type === 'AGENT') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);
            newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose + item.shareHolderWinLose + item.seniorWinLose + item.masterAgentWinLose;
            newItem.companyWinLoseCom = item.superAdminWinLoseCom + item.companyWinLoseCom + item.shareHolderWinLoseCom + item.seniorWinLoseCom + item.masterAgentWinLoseCom;
            newItem.companyTotalWinLoseCom = item.superAdminTotalWinLoseCom + item.companyTotalWinLoseCom + item.shareHolderTotalWinLoseCom + item.seniorTotalWinLoseCom + item.masterAgentTotalWinLoseCom;

            newItem.agentWinLose = item.agentWinLose;
            newItem.agentWinLoseCom = item.agentWinLoseCom;
            newItem.agentTotalWinLoseCom = item.agentTotalWinLoseCom;

            newItem.memberWinLose = item.memberWinLose;
            newItem.memberWinLoseCom = item.memberWinLoseCom;
            newItem.memberTotalWinLoseCom = item.memberTotalWinLoseCom;
            return newItem;
        });
    } else {
        return dataList;
    }
}


module.exports = router;

