const MemberModel = require('../../models/member.model');
const BetTransactionModel = require('../../models/betTransaction.model.js');
const RobotTransactionModel = require('../../models/robotTransaction.model.js');
const AgentGroupModel = require('../../models/agentGroup.model.js');
const Commons = require('../../common/commons');
const express = require('express');
const router = express.Router();
const async = require("async");
const Joi = require('joi');
const mongoose = require('mongoose');
const config = require('config');
const _ = require('underscore');
const _Utils = require('../../common/underscoreUtils');
const FormulaUtils = require('../../common/formula');
const MemberService = require('../../common/memberService');
const roundTo = require('round-to');
const moment = require('moment');
const DateUtils = require('../../common/dateUtils');

function robotTask(robotItem) {

    return function (callbackTask) {

        async.waterfall([(callback) => {

            MemberService.getOddAdjustment(robotItem.username, (err, result) => {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, result);
                }
            });

        },(oddAdjust,callback) => {


            console.log(' :========: ',robotItem.odd)
            if (robotItem.matchType === 'LIVE') {

                let limitSetting = {};
                limitSetting.sportsBook = {};
                limitSetting.sportsBook.oneTwoDoubleChance = {};
                limitSetting.sportsBook.oneTwoDoubleChance.maxPerBet = 0;
                limitSetting.sportsBook.hdpOuOe = {};
                limitSetting.sportsBook.hdpOuOe.maxPerBet = 0;
                Commons.cal_bp_live_single_match_sbo(robotItem.matchId, robotItem.oddType.toLowerCase(), robotItem.oddKey.toLowerCase(), robotItem.typeHdpOuOe, oddAdjust, limitSetting,robotItem.key, function (err, response) {
                    if (err) {
                        callback(err, null);
                    }else {

                        console.log('cal cal_bp_live_single_match_robot : ', response)

                        checkOddProcess(response, robotItem.priceType, robotItem.oddType, robotItem.oddKey, robotItem.odd, robotItem.handicap, function (isOddNotMatch, key, oddValue, handicapValue, bet) {

                            let odd = {
                                betId: robotItem.betId,
                                oldOdd: robotItem.odd,
                                handicap: handicapValue,
                                nextOdd: roundTo(Number.parseFloat(oddValue), 2),
                                time: DateUtils.getCurrentDate()
                            };

                            callback(null, oddAdjust, odd)
                        });
                    }

                });

            } else {

                Commons.cal_bp_hdp_single_match_sbo(robotItem.matchId, robotItem.oddType, robotItem.oddKey.toLowerCase(), robotItem.typeHdpOuOe, oddAdjust, robotItem.memberId,robotItem.matchType,robotItem.key, function (err, response) {
                    if (err) {
                        callback(err, null);
                    } else {

                        console.log('cal cal_bp_hdp_single_match : ',response)
                        checkOddProcess(response, robotItem.priceType, robotItem.oddType, robotItem.oddKey, robotItem.odd, robotItem.handicap, function (isOddNotMatch, key, oddValue, handicapValue, bet) {

                            let odd = {
                                betId:robotItem.betId,
                                oddType: robotItem.oddType,
                                handicap: handicapValue,
                                oldOdd: robotItem.odd,
                                nextOdd: roundTo(Number.parseFloat(oddValue),2),
                                time : DateUtils.getCurrentDate()

                            };

                            callback(null,oddAdjust,odd)
                        });
                    }
                });

            }


        }], (err, oddAdjust,odd) => {
            try {
                if(err){
                    console.log('err ::: ',err)
                    callbackTask(null,{});
                    return;
                }


                if(robotItem.matchType === 'LIVE'){
                    if(!odd){
                        callbackTask(null,{});
                        return;
                    }
                }
                let isRobot = false;
                let oddDistance = roundTo(Math.abs(roundTo(Math.abs(odd.oldOdd) - Math.abs(odd.nextOdd),2) * 100),2);

                //น้ำดำ
                if(odd.oldOdd > 0){
                    //ดำ -> แดง บ.กำไร
                    if(odd.nextOdd < 0){
                        isRobot = false;
                    }else {
                        //0.89> 0.96 w
                        if (odd.nextOdd < odd.oldOdd) {
                            isRobot = true;
                        }
                    }
                }

                //น้ำแดง
                if(odd.oldOdd < 0){
                    //แดง -> ดำ บ. ขาดทุน
                    if(odd.nextOdd > 0){
                        isRobot = true;
                        oddDistance = getOddRedBlackDistance(odd.oldOdd,odd.nextOdd);
                    }else {
                        if(odd.nextOdd < odd.oldOdd){
                            isRobot = true;
                        }
                    }



                }

                function getOddRedBlackDistance(redOdd,blackOdd) {

                    let red = (roundTo(redOdd+1,2));
                    let black = roundTo(blackOdd-1,2) * -1;
                    return  roundTo((red + black )* 100,2)
                }


                let body = {
                    'hdp.nextOdd' : odd.nextOdd,
                    'hdp.oddDistance' : oddDistance,
                    'hdp.isRobot' : oddDistance > 5,
                    'hdp.robotDate' : odd.time
                };


                console.log('-------------------------');
                console.log('oddOdd : '+ odd.oldOdd + '  ,  nextOdd : '+odd.nextOdd);
                console.log('oddDistance : ',oddDistance);
                console.log('isRobot : ',isRobot)

                //ดำ -> แดง บ.กำไร
                //แดง -> ดำ ขาดทุุน

                console.log(odd.time)
                // let duration = moment.duration(odd.time.diff(robotItem.createdDate));
                // let minutes = duration.asMinutes();

                function diff_seconds(dt2, dt1)
                {

                    let diff =(dt2.getTime() - dt1.getTime()) / 1000;
                    return Math.abs(Math.round(diff));

                }

                // console.log('duration  : ',(minutes <= 1 ))
                console.log('play date : ',robotItem.createdDate);
                console.log('next odd date : ',odd.time);

                let diffSeconds = diff_seconds(odd.time,robotItem.createdDate);
                console.log('diffSeconds : ' ,diffSeconds);

                if(diffSeconds > 60){
                    RobotTransactionModel.remove({betId: odd.betId}, function (err) {
                        callbackTask(null, odd)
                    });
                }else {
                    if (isRobot && oddDistance > 5) {

                        console.log('hi')


                        if (robotItem.isAutoUpdateOdd) {
                            body['hdp.oldOdd'] = odd.oldOdd;
                            body['hdp.odd'] = odd.nextOdd;
                            body['hdp.autoUpdateOdd'] = true;
                        }

                        if(robotItem.isAutoUpdateOdd && (odd.oldOdd < 0 && odd.nextOdd >= 0)){
                            body['memberCredit'] = robotItem.amount * -1;
                        }

                        let condition = {};

                        condition.gameDate =  {$gte: moment().millisecond(0).second(0).minute(0).hour(11).add(-1,'d').utc(true).toDate()};
                        condition.betId = odd.betId;
                        condition.status = 'RUNNING';

                        console.log('condition : ',condition)
                        console.log('body : ',body)
                        console.log('robotItem.isAutoUpdateOdd : ',robotItem.isAutoUpdateOdd)

                        BetTransactionModel.update(condition, body, function (err, response) {
                            console.log(response)

                            // if(response.nModified == 1) {

                                if(robotItem.isAutoUpdateOdd && (odd.oldOdd < 0 && odd.nextOdd >= 0)){

                                    let amount = (robotItem.amount - Math.abs(robotItem.memberCredit) ) * -1;

                                    console.log('amount : ',amount);

                                    MemberService.updateBalance2(robotItem.username.toLowerCase(),amount,robotItem.betId,'SPORTS_BOOK_BET_ROBOT',robotItem.betId,function () {
                                        RobotTransactionModel.remove({betId: odd.betId}, function (err) {
                                            callbackTask(null, odd)
                                        });
                                    });

                                }else {

                                    RobotTransactionModel.remove({betId: odd.betId}, function (err) {
                                        callbackTask(null, odd)
                                    });
                                }

                            // }else {
                            //     callbackTask(null, odd)
                            // }
                        });

                    } else {
                        callbackTask(null, {})
                    }
                }
            } catch (e) {
                callbackTask(null,{});
            }
        });
    }
}



router.get('/', function (req, res) {

    async.series([(callback) => {

        let oldOdd = -0.97;
        let nextOdd = -0.93;

        let oddDistance = roundTo(Math.abs(roundTo(Math.abs(oldOdd) - Math.abs(nextOdd),2) * 100),2);
        console.log(oddDistance);

        let condition = {};//{matchType:'NONE_LIVE'};

        condition['createdDate'] = {
                "$gte": moment().add(-10,'m').utc(true).toDate()
        };


        RobotTransactionModel.find(condition,function (err,response) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null,response);
                }
        });

        }], (error, asyncResponse) => {

            if (error) {
                return res.send({
                    code: 9999,
                    message: error
                });
            } else {

                let robotTransactions = asyncResponse[0];


                let tasks = [];

                _.each(robotTransactions, (item) => {
                    tasks.push(robotTask(item));
                });

                async.parallel(tasks, (err, results) => {

                    return res.send(
                        {
                            code: 0,
                            message: "success",
                            result: results
                        }
                    );
                });

            }
        }
    );

});


function checkOddProcess(bpResponse, priceType, oddType, oddKey, odd, handicap, callback) {

    let handicapPrefix, oddPrefix;
    let type = oddType.toLowerCase();
    let isOddNotMatch = false;
    let oddValue, handicapValue;
    let key = '';
    let bet = '';
    if (oddType === 'AH' || oddType === 'AH1ST') {

        _Utils.findKeyByValue(bpResponse[type], oddKey, function (keyResponse) {
            key = keyResponse;
        });

        if (key === 'hpk') {
            handicapPrefix = 'h';
            oddPrefix = 'hp';
            bet = 'HOME';
        } else {
            handicapPrefix = 'a';
            oddPrefix = 'ap';
            bet = 'AWAY';
        }

        if (!_.isUndefined(bpResponse[type])) {

            oddValue = bpResponse[type][oddPrefix];
            oddValue = convertOdd(priceType, oddValue, oddType);
            handicapValue = bpResponse[type][handicapPrefix];
            if (oddValue !== odd) {
                isOddNotMatch = true;
            }
            if (handicapValue !== handicap) {
                isOddNotMatch = true;
            }
        } else {
            isOddNotMatch = true;
        }

    } else if (oddType === 'OU' || oddType === 'OU1ST') {

        _Utils.findKeyByValue(bpResponse[type], oddKey, function (keyResponse) {
            key = keyResponse;
        });


        if (key === 'opk') {
            handicapPrefix = 'o';
            oddPrefix = 'op';
            bet = 'OVER';
        } else {
            handicapPrefix = 'u';
            oddPrefix = 'up';
            bet = 'UNDER';
        }

        if (!_.isUndefined(bpResponse[type])) {
            oddValue = bpResponse[type][oddPrefix];
            oddValue = convertOdd(priceType, oddValue, oddType);
            handicapValue = bpResponse[type][handicapPrefix];
            if (oddValue !== odd) {
                isOddNotMatch = true;
            }
            if (handicapValue !== handicap) {
                isOddNotMatch = true;
            }
        } else {
            isOddNotMatch = true;
        }

    } else if (oddType === 'X12' || oddType === 'X121ST') {

        _Utils.findKeyByValue(bpResponse[type], oddKey, function (keyResponse) {
            key = keyResponse;
        });

        if (key === 'hk') {
            oddPrefix = 'h';
            bet = 'HOME';
        } else if (key === 'ak') {
            oddPrefix = 'a';
            bet = 'AWAY';
        } else if (key === 'dk') {
            oddPrefix = 'd';
            bet = 'DRAW';
        }

        if (!_.isUndefined(bpResponse[type])) {
            oddValue = bpResponse[type][oddPrefix];
            oddValue = convertOdd(priceType, oddValue, oddType);
            if (oddValue !== odd) {
                isOddNotMatch = true;
            }
        } else {
            isOddNotMatch = true;
        }

    } else if (oddType === 'OE' || oddType === 'OE1ST') {

        _Utils.findKeyByValue(bpResponse[type], oddKey, function (keyResponse) {
            key = keyResponse;
        });

        if (key === 'ok') {
            oddPrefix = 'o';
            bet = 'ODD';
        } else if (key === 'ek') {
            oddPrefix = 'e';
            bet = 'EVEN';
        }

        if (!_.isUndefined(bpResponse[type])) {
            oddValue = bpResponse[type][oddPrefix];
            oddValue = convertOdd(priceType, oddValue, oddType);
            if (oddValue !== odd) {
                isOddNotMatch = true;
            }
        } else {
            isOddNotMatch = true;
        }
    } else {
        console.log('oddType not match')
    }
    console.log('input_odd : ', odd + '  , oddValue : ' + oddValue);
    console.log('input_handicap : ', handicap + '  , handicapValue : ' + handicapValue);
    console.log('isOddNotMatch : ', isOddNotMatch)
    callback(isOddNotMatch, key, oddValue, handicapValue, bet);

}


function convertPercent(percent) {
    return percent / 100
}


function convertOdd(priceType, odd, oddType) {

    if (priceType === 'MY') {
        return odd;
    } else if (priceType === 'HK') {
        if (!odd.includes('-')) {
            return odd;
        }
        odd = odd.replace('-', '');
        odd = parseFloat(odd) * 100;

        let tmp = 100 / odd;
        return tmp.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
    } else if (priceType === 'EU') {
        if (!odd.includes('-')) {
            if (oddType === 'X12' || oddType === 'X121ST') {
                return odd;
            }
            let t = parseFloat(odd) + 1.00;
            if (!t.toString().includes('.')) {
                t = t.toString() + '.00';
                return t;
            } else {

                if (t.toString().length == 3) {
                    t = t.toString() + '0';
                    return t;
                } else {
                    return t.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
                }

            }
        }
        odd = odd.replace('-', '');
        odd = parseFloat(odd) * 100;
        let tmp = (100 / odd) + 1.00;
        return tmp.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];

    } else if (priceType === 'ID') {
        if (oddType === 'X12' || oddType === 'X121ST') {
            return odd;
        }


        let tmp = parseFloat(odd) * -1;
        tmp = 1 / tmp;

        if (!tmp.toString().includes('.')) {
            tmp = tmp.toString() + '.00';
            return tmp;
        } else {

            if (tmp.toString().length == 3) {
                tmp = tmp.toString() + '0';
                return tmp;
            } else {
                return tmp.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
            }

        }
    }
}

module.exports = router;