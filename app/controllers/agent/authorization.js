const UserModel = require('../../models/users.model');
const express = require('express');
const router = express.Router()
const async = require("async");
const Joi = require('joi');
const mongoose = require('mongoose');
const Hashing = require('../../common/hashing');
const config = require('config');
const jwt = require('../../common/jwt');
const TokenModel = require("../../models/token.model");

const refreshTokenSchema = Joi.object().keys({
    refreshToken: Joi.string().required()
});


router.post('/refresh', function (req, res) {

    let validate = Joi.validate(req.body, refreshTokenSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    try {
        const refresh_token = req.body.refreshToken;
        const data = jwt.verify(refresh_token).data;
        const accessToken = jwt.generateToken(data);
        const refreshToken = jwt.generateRefreshToken(data);
        console.log(refresh_token)

        // console.log(data)
        // TokenModel.update({username: data.username}, data.uid, {upsert: true}, function (err, response) {
        //     if (err) {
        //         return res.send({message: "error", result: err, code: 999});
        //     } else {
        //         console.log(response)
                return res.send(
                    {
                        code: 0,
                        message: "success",
                        result: {
                            // profile: profile,
                            access_token: accessToken,
                            refresh_token: refreshToken
                        }
                    }
                );
        //     }
        // });

    }catch (err){
        if(err.name === 'TokenExpiredError'){
            return res.send({message: err.message, result: err.name, code: 1004});
        }
        return res.send({message: "error", result: err, code: 999});
    }


});





module.exports = router;