const AnnouncementModel = require('../../models/announcement.model.js');
const express = require('express');
const router = express.Router();
const async = require("async");
const Joi = require('joi');
const mongoose = require('mongoose');
const config = require('config');
const _ = require('underscore');
const moment = require('moment');
const DateUtils = require('../../common/dateUtils');

const NodeCache = require( "node-cache" );
const cache = new NodeCache( { stdTTL: 3000, checkperiod: 3001 } );

router.get('/list/member', function (req, res) {

    try{
        const contents = cache.get( "memberAnnouncement", true );
        return res.send(
            {
                code: 0,
                message: "success",
                result: {
                    list:contents
                }
            }
        );
    } catch( err ){
        let condition = {};

        let userInfo = req.userInfo;



        if (req.query.dateFrom && req.query.dateTo) {
            condition['createdDate'] = {
                "$gte": moment(req.query.dateFrom, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).utc(true).toDate(),
                "$lt": moment(req.query.dateTo, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).add(1, 'd').utc(true).toDate()
            }
        }

        condition.type = {$ne:'AGENT'};
        condition.active = true;

        AnnouncementModel.find(condition,'announcementId message message_th createdDate')
            .sort({'createdDate': -1})
            .exec((err, response)=>{
                if (err) {
                    return res.send({message: "error", results: err, code: 999});
                } else {
                    cache.set( "memberAnnouncement", response);
                    return res.send(
                        {
                            code: 0,
                            message: "success",
                            result: {
                                list:response
                            }
                        }
                    );
                }
            });
    }


});



router.get('/list', function (req, res) {

    try{
        const contents = cache.get( "memberAnnouncementList", true );
        return res.send(
            {
                code: 0,
                message: "success",
                result: contents
            }
        );
    } catch( err ){
        let condition = {};
        let userInfo = req.userInfo;
        condition.active = true;
        AnnouncementModel.paginate(condition, {
                page: Number.parseInt(req.query.page),
                limit: Number.parseInt(req.query.limit),
                populate:'createdBy',
                sort : {'createdDate': -1}
            },
            (err, data) => {
                if (err) {
                    return res.send({message: "error", results: err, code: 999});
                } else {
                    cache.set( "memberAnnouncementList", data);
                    return res.send(
                        {
                            code: 0,
                            message: "success",
                            result: data
                        }
                    );
                }
            });
    }
});




router.get('/get/:id', function (req, res) {

    let userInfo = req.userInfo;

    AnnouncementModel.findById(req.params.id)
        .exec((err, response)=>{
            if (err)
                return res.send({message: "error", results: err, code: 999});

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: {
                        info:response
                    }
                }
            );
        });

});


const addMsgAgentSchema = Joi.object().keys({
    type:Joi.string().required(),
    message: Joi.string().required()
});

router.post('/', function (req, res) {

    let validate = Joi.validate(req.body, addMsgAgentSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    const userInfo = req.userInfo;


        let body = {
            type:req.body.type,
            message: req.body.message,
            createdBy: userInfo._id
        };

        let model = new AnnouncementModel(body);

        model.save(function (err, data) {

            if (err) {
                return res.send({message: "error", results: err, code: 999});
            }
            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: data
                }
            );
        });
});



const addMsgBackAgentSchema = Joi.object().keys({
    type:Joi.string().required(),
    key:Joi.string().allow(''),
    dateTime:Joi.string().required(),
    message: Joi.string().required(),
    lang:Joi.string().allow('')
});

router.post('/backend', function (req, res) {

    let validate = Joi.validate(req.body, addMsgBackAgentSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    let body = {
        type:req.body.type,
        key:req.body.key
    };

    if(req.body.lang == 'EN'){
        body.message = req.body.message;
    }else {
        body.message_th = req.body.message;
    }

    console.log(body)

    AnnouncementModel.findOne({key:body.key},(err,response) => {

        if(response){
            AnnouncementModel.update({key:body.key},body).exec(function (err,response) {
                if (err) {
                    return res.send({message: "error", results: err, code: 999});
                }
                return res.send(
                    {
                        code: 0,
                        message: "success"
                    }
                );
            });

        }else {
            let body = {
                type:req.body.type,
                key:req.body.key
            };

            if(req.body.lang == 'EN'){
                body.message = req.body.message;
            }else {
                body.message_th = req.body.message;
            }

            let model = new AnnouncementModel(body);

            model.save(function (err, data) {

                if (err) {
                    return res.send({message: "error", results: err, code: 999});
                }
                return res.send(
                    {
                        code: 0,
                        message: "success"
                    }
                );
            });
        }


    });

});


const updateMsgAgentSchema = Joi.object().keys({
    type:Joi.string().required(),
    message: Joi.string().required()
});

router.put('/update/:id', function (req, res) {

    let validate = Joi.validate(req.body, updateMsgAgentSchema);
    if (validate.error) {
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 999
        });
    }

    const userInfo = req.userInfo;


    let body = {
        type:req.body.type,
        message: req.body.message,
        updatedBy: userInfo._id,
        updatedDate:DateUtils.getCurrentDate()
    };


    AnnouncementModel.update({_id:req.params.id},body,function (err, data) {

        if (err) {
            return res.send({message: "error", results: err, code: 999});
        }
        return res.send(
            {
                code: 0,
                message: "success",
                result: data
            }
        );
    });
});


router.put('/delete/:id', function (req, res) {


    const userInfo = req.userInfo;


    let body = {
        active: false,
        updatedBy: userInfo._id,
        updatedDate:DateUtils.getCurrentDate()
    };


    AnnouncementModel.update({_id:req.params.id},body,function (err, data) {

        if (err) {
            return res.send({message: "error", results: err, code: 999});
        }
        return res.send(
            {
                code: 0,
                message: "success",
                result: data
            }
        );
    });
});

module.exports = router;

