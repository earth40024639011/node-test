const MemberModel = require('../../models/member.model');
const BetTransactionModel = require('../../models/betTransaction.model.js');
const UserModel = require("../../models/users.model");
const AgentGroupModel = require('../../models/agentGroup.model.js');
const MemberService = require('../../common/memberService');
const express = require('express');
const router = express.Router();
const async = require("async");
const Joi = require('joi');
const mongoose = require('mongoose');
const config = require('config');
const _ = require('underscore');
const moment = require('moment');
const naturalSort = require("javascript-natural-sort");
const roundTo = require('round-to');
const ApiService = require('../../common/apiService');
const Hashing = require('../../common/hashing');
const FormulaUtils = require('../../common/formula');
const DateUtils = require('../../common/dateUtils');
const Redis     = require('../sbo/1_common/1_redis/1_1_redis');
const CommonService = require('../../common/commons');


router.get('/info', function (req, res) {

    let condition = {game: 'FOOTBALL'};

    let userInfo = req.userInfo;

    if (req.query.dateFrom && req.query.dateTo) {
        condition['createdDate'] = {
            "$gte": moment(req.query.dateFrom, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).utc(true).toDate(),
            "$lt":moment(req.query.dateTo, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).add(1,'d').utc(true).toDate()
        }
    }


    let orCondition = [];

    if (req.query.status) {
        orCondition.push({
            $or: req.query.status.split(',').map(function (status) {
                return {'status': status}
            })
        });
    }


    if (orCondition.length > 0) {
        condition['$and'] = orCondition;
    }

    let group = '';
    let child = '';
    if (userInfo.group.type === 'SUPER_ADMIN') {
        group = 'superAdmin';
        child = 'company';
    } else if (userInfo.group.type === 'COMPANY') {
        group = 'company';
        child = 'shareHolder';
    } else if (userInfo.group.type === 'SHARE_HOLDER') {
        group = 'shareHolder';
        child = 'superSenior';
    } else if (userInfo.group.type === 'SUPER_SENIOR') {
        group = 'superSenior';
        child = 'senior';
    } else if (userInfo.group.type === 'SENIOR') {
        group = 'senior';
        child = 'masterAgent';
    } else if (userInfo.group.type === 'MASTER_AGENT') {
        group = 'masterAgent';
        child = 'agent';
    } else if (userInfo.group.type === 'AGENT') {
        group = 'agent';
        child = 'member';
    }

    condition['game'] = 'FOOTBALL';
    condition['gameType'] = 'TODAY';
    condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(userInfo.group._id);
    condition['status'] = 'RUNNING';

    BetTransactionModel.aggregate([
        {
            $match: condition
        },
        {
            "$group": {
                "_id": {
                    matchId: '$hdp.matchId',
                    source: '$source'
                    // member: '$memberId',
                },
                "count": {$sum: 1},
            }
        },
        {
            $project: {
                _id: 0,
                source: '$_id.source',
                count: '$count',
            }
        }
    ], (err, results) => {
        if (err)
            return res.send({message: "error", results: err, code: 999});


        return res.send(
            {
                code: 0,
                message: "success",
                result: {
                    info: results
                }
            }
        );
    });

});


router.get('/match', function (req, res) {

    let userInfo = req.userInfo;

    let group = '';
    let child = '';

    if (userInfo.group.type === 'SUPER_ADMIN') {
        group = 'superAdmin';
        child = 'company';
    } else if (userInfo.group.type === 'COMPANY') {
        group = 'company';
        child = 'shareHolder';
    } else if (userInfo.group.type === 'SHARE_HOLDER') {
        group = 'shareHolder';
        child = 'superSenior';
    } else if (userInfo.group.type === 'SUPER_SENIOR') {
        group = 'superSenior';
        child = 'senior';
    } else if (userInfo.group.type === 'SENIOR') {
        group = 'senior';
        child = 'masterAgent';
    } else if (userInfo.group.type === 'MASTER_AGENT') {
        group = 'masterAgent';
        child = 'agent';
    } else if (userInfo.group.type === 'AGENT') {
        group = 'agent';
        child = 'member';
    }

    let condition = {gameDate: {$gte: new Date(moment().add(-10, 'd').format('YYYY-MM-DDT11:00:00.000'))}};
    condition['game'] = 'FOOTBALL';
    condition['gameType'] = 'TODAY';
    condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(userInfo.group._id);
    condition['status'] = 'RUNNING';

    BetTransactionModel.aggregate([
        {
            $match: condition
        },
        {
            "$group": {
                "_id": {
                    matchId: '$hdp.matchId',
                    source: '$source'
                    // member: '$memberId',
                },
                "count": {$sum: 1},
                "hdp": {$first: "$hdp"}
            }
        },
        {
            $project: {
                _id: 0,
                source: '$_id.source',
                hdp: '$hdp',
                count: '$count',
            }
        }
    ], (err, results) => {
        if (err)
            return res.send({message: "error", results: err, code: 999});


        async.series([(callback => {

            AgentGroupModel.populate(results, {
                path: 'commission.group',
                select: '_id name'
            }, function (err, memberResult) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, memberResult);
                }
            });

        }), (callback => {

            AgentGroupModel.populate(results, {
                path: 'child.group',
                select: '_id name'
            }, function (err, memberResult) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, memberResult);
                }
            });

        })], (err, populateResults) => {

            if (err) {
                return res.send({message: "error", results: err, code: 999});
            }


            results = _.sortBy(results, function (o) {
                return o.hdp.leagueNameN.en;
            });

            results = _.sortBy(results, function (o) {
                return o.hdp.matchDate;
            })

            prepareDiffMatchDate(results);

            let summary = _.reduce(results, function (memo, num) {
                return {
                    total: memo.total + 1,
                    football: memo.football + (num.source === 'FOOTBALL' ? 1 : 0),
                    basketBall: memo.basketBall + (num.source === 'BASKETBALL' ? 1 : 0),
                    muayThai: memo.muayThai + (num.source === 'MUAY_THAI' ? 1 : 0),
                    isMatchStart: memo.isMatchStart + (num.isMatchStart ? 1 : 0),
                    isMatchNotStart: memo.isMatchNotStart + (!num.isMatchStart ? 1 : 0),
                }
            }, {
                total: 0,
                football: 0,
                basketBall: 0,
                muayThai: 0,
                isMatchStart: 0,
                isMatchNotStart: 0
            });


            function prepareDiffMatchDate(list) {

                function diff_seconds(dt2, dt1) {
                    let diff = (dt2.getTime() - dt1.getTime()) / 1000;
                    return Math.round(diff);
                }

                return _.map(list, (item) => {
                    let xx = item;
                    let diffSeconds = diff_seconds(item.hdp.matchDate, DateUtils.getCurrentDate());
                    xx.playTimeMinute = parseInt(Math.abs(diffSeconds) / 60);//< 1800 ? (1800 - diffSeconds) : 0;
                    xx.isMatchStart = diffSeconds < 0;
                    return xx;
                });
            }

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: {
                        list: results,
                        info: summary,
                    }
                }
            );
        });
    });

});


router.get('/match-step-parlay', function (req, res) {

    let userInfo = req.userInfo;


    let group = '';
    let child = '';
    if (userInfo.group.type === 'SUPER_ADMIN') {
        group = 'superAdmin';
        child = 'company';
    } else if (userInfo.group.type === 'COMPANY') {
        group = 'company';
        child = 'shareHolder';
    } else if (userInfo.group.type === 'SHARE_HOLDER') {
        group = 'shareHolder';
        child = 'superSenior';
    } else if (userInfo.group.type === 'SUPER_SENIOR') {
        group = 'superSenior';
        child = 'senior';
    } else if (userInfo.group.type === 'SENIOR') {
        group = 'senior';
        child = 'masterAgent';
    } else if (userInfo.group.type === 'MASTER_AGENT') {
        group = 'masterAgent';
        child = 'agent';
    } else if (userInfo.group.type === 'AGENT') {
        group = 'agent';
        child = 'member';
    }


    let condition = {gameDate: {$gte: moment().add(-10, 'd').millisecond(0).second(0).minute(0).hour(11).utc(true).toDate()}};
    condition['game'] = 'FOOTBALL';
    condition['gameType'] = {$in: ["PARLAY", "MIX_STEP"]};
    condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(userInfo.group._id);
    condition['status'] = 'RUNNING';

    BetTransactionModel.aggregate([
        {
            $match: condition
        },
        {
            $project: {
                _id: 0,
                source: '$source',
                parlay: '$parlay',
                betId: "$betId",
                createdDate:"$createdDate",
                memberId:"$memberId",
                gameType:"$gameType"
            }
        }
    ], (err, results) => {
        if (err)
            return res.send({message: "error", results: err, code: 999});


        async.series([(callback => {

            AgentGroupModel.populate(results, {
                path: 'commission.group',
                select: '_id name'
            }, function (err, memberResult) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, memberResult);
                }
            });

        }), (callback => {

            MemberModel.populate(results, {
                path: 'memberId',
                select: '_id username_lower'
            }, function (err, memberResult) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, memberResult);
                }
            });

        })], (err, populateResults) => {

            if (err) {
                return res.send({message: "error", results: err, code: 999});
            }

            results = _.map(results, (item) => {
                let obj = Object.assign({},item);
                obj.parlay.matches = prepareDiffMatchDate(item.parlay.matches);
                obj.parlay.matches = _.sortBy(obj.parlay.matches, function (o) {
                    return o.matchDate;
                });
                obj.size = obj.parlay.matches.length;
                obj.unFinish = _.filter(obj.parlay.matches,(item)=>{

                    return _.isEmpty(item.betResult);
                }).length;
                return obj;
            });

            let summary = _.reduce(results, function (memo, num) {
                return {
                    total: memo.total + 1,
                    step: memo.step + (num.gameType === 'MIX_STEP' ? 1 : 0),
                    parlay: memo.parlay + (num.gameType === 'PARLAY' ? 1 : 0)
                }
            }, {
                total: 0,
                step: 0,
                parlay: 0
            });


            function prepareDiffMatchDate(list) {

                function diff_seconds(dt2, dt1) {
                    let diff = (dt2.getTime() - dt1.getTime()) / 1000;
                    return Math.round(diff);
                }

                return _.map(list, (item) => {
                    let xx = item;
                    let diffSeconds = diff_seconds(item.matchDate, DateUtils.getCurrentDate());
                    xx.playTimeMinute = parseInt(Math.abs(diffSeconds) / 60);//< 1800 ? (1800 - diffSeconds) : 0;
                    xx.isMatchStart = diffSeconds < 0;
                    return xx;
                });
            }

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: {
                        list: results,
                        info:summary
                    }
                }
            );
        });
    });

});

router.get('/ticket', function (req, res) {

    let condition = {game: 'FOOTBALL'};

    let userInfo = req.userInfo;

    if (req.query.dateFrom && req.query.dateTo) {
        condition['createdDate'] = {
            "$gte": moment(req.query.dateFrom, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).utc(true).toDate(),
            "$lt":moment(req.query.dateTo, "DD/MM/YYYY").millisecond(0).second(0).minute(0).hour(0).add(1,'d').utc(true).toDate()
        }
    }


    let group = '';
    let child = '';
    if (userInfo.group.type === 'SUPER_ADMIN') {
        group = 'superAdmin';
        child = 'company';
    } else if (userInfo.group.type === 'COMPANY') {
        group = 'company';
        child = 'shareHolder';
    } else if (userInfo.group.type === 'SHARE_HOLDER') {
        group = 'shareHolder';
        child = 'superSenior';
    } else if (userInfo.group.type === 'SUPER_SENIOR') {
        group = 'superSenior';
        child = 'senior';
    } else if (userInfo.group.type === 'SENIOR') {
        group = 'senior';
        child = 'masterAgent';
    } else if (userInfo.group.type === 'MASTER_AGENT') {
        group = 'masterAgent';
        child = 'agent';
    } else if (userInfo.group.type === 'AGENT') {
        group = 'agent';
        child = 'member';
    }

    condition['game'] = 'FOOTBALL';
    condition['gameType'] = 'TODAY';
    condition['commission.' + group + '.group'] = mongoose.Types.ObjectId(userInfo.group._id);
    condition['status'] = 'WAITING';

    BetTransactionModel.find(condition)
        .populate({
            path: 'memberId',
            select: '_id username'
        })
        .lean()
        .exec((err, results) => {

            if (err) {
                return res.send({message: "error", results: err, code: 999});
            }

            results = _.sortBy(results, function (o) {
                return o.createdDate;
            });


            function prepareDiffMatchDate(list) {

                function diff_seconds(dt2, dt1) {
                    let diff = (dt2.getTime() - dt1.getTime()) / 1000;
                    return Math.round(diff);
                }

                return _.map(list, (item) => {
                    let xx = Object.assign({}, item);
                    let diffSeconds = diff_seconds(item.createdDate, DateUtils.getCurrentDate());
                    xx.playTimeSecond = Math.abs(diffSeconds);//< 1800 ? (1800 - diffSeconds) : 0;
                    return xx;
                });

            }

            let summary = _.reduce(results, function (memo, num) {
                return {
                    total: memo.total + 1,
                    football: memo.football + (num.source === 'FOOTBALL' ? 1 : 0),
                    basketBall: memo.basketBall + (num.source === 'BASKETBALL' ? 1 : 0),
                    muayThai: memo.muayThai + (num.source === 'MUAY_THAI' ? 1 : 0),
                    isMatchStart: memo.isMatchStart + (num.isMatchStart ? 1 : 0),
                    isMatchNotStart: memo.isMatchNotStart + (!num.isMatchStart ? 1 : 0),
                }
            }, {
                total: 0,
                football: 0,
                basketBall: 0,
                muayThai: 0,
                isMatchStart: 0,
                isMatchNotStart: 0
            });

            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: {
                        list: prepareDiffMatchDate(results),
                        info: summary,
                    }
                }
            );
        });

});

router.get('/today', (req, res) => {
    Redis.getByKey('resultTodayAMBFM', (error, allMatch) => {
        if (error) {
            return res.send(
                {
                    code: 0,
                    message: "Error",
                    result: error
                });
        } else {

            // allMatch = [allMatch[0]]

            CommonService.oddTimeSeries((err,oddTimeSeries)=>{

                console.log('oddTimeSeries : ',oddTimeSeries.length)

                let list = _.chain(allMatch)

                .map((leagueObj)=>{
                    let item = leagueObj;


                    let newMatch = [];
                    _.map(leagueObj.m,(matchObj,matchIndex)=>{

                        const match = Object.assign({},matchObj);
                        console.log('========= matchIndex =========: ',matchIndex)
                        console.log('========= matchId =========: ',matchObj.id)
                        let currentMatchOdds = _.chain(oddTimeSeries).filter(obj => {
                            return _.isEqual(matchObj.id, obj.id)
                        }).value();

                        let max = _.map(currentMatchOdds,o =>{
                            let x = _.map(o.bp,b =>{
                                return b.ah.h;
                            });
                           return x;
                        });

                        let uniqueOdd = _.union([].concat.apply([], max));

                        console.log('matchId : '+matchObj.id+' uniqueOdd : '+uniqueOdd)


                        _.forEach(uniqueOdd,(xx,index) =>{
                            const handicap = xx;

                            let firstOdd = _.find(currentMatchOdds,(num)=>{

                               let hasHandicap = _.filter(num.bp,a =>{
                                    return a.ah.h == handicap;
                                });

                               if(hasHandicap && hasHandicap.length > 0) {
                                   return hasHandicap;
                               }else {
                                   return num.bp[0].ah.h == handicap;
                               }
                            });

                            let f = _.find(firstOdd.bp,f => {
                                return f.ah.h == handicap;
                            });



                            let lastOdd = _.find(currentMatchOdds.reverse(),(num)=>{

                                let hasHandicap = _.filter(num.bp,a =>{
                                    return a.ah.h == handicap;
                                });

                                if(hasHandicap && hasHandicap.length > 0) {
                                    return hasHandicap;
                                }else {
                                    return num.bp[0].ah.h == handicap;
                                }
                            });

                            let l = _.find(lastOdd.bp,f => {
                                return f.ah.h == handicap;
                            });


                            let bn = Object.assign({},match);
                            bn.index = index;
                            bn.handicap = handicap;
                            bn.firstOdd = f;
                            bn.lastOdd = l;
                            bn.firstSnapDate = firstOdd.snapDate;
                            bn.lastSnapDate = lastOdd.snapDate;
                            newMatch.push(bn)
                        });
                    });

                    leagueObj.m = newMatch;
                    console.log('leagueObj.m : ',leagueObj.m)


                    // item.m = _.map(leagueObj.m,(matchObj,matchIndex)=> {
                    //
                    //     matchObj.firstOdd = odds[0];//odd[0]
                    //     matchObj.lastOdd = odds[odds.length - 1];//odd[0]
                    //     matchObj.oddLength = oddLength;
                    //     return matchObj;
                    // });
                    console.log('========= end match =========: ')
                    return leagueObj;

                }).value();

                return res.send(
                    {
                        code: 0,
                        message: "success",
                        result: list
                    });
            });

        }
    });
});

router.get('/oddHistory', (req, res) => {


    let matchId = req.query.matchId;
    console.log('matchId : ',matchId)
    CommonService.oddTimeSeriesByMatchId(matchId,(err,response)=>{
        if (err) {
            return res.send(
                {
                    code: 0,
                    message: "Error",
                    result: err
                });
        } else {

            response = _.map(response,item => {
                item.time = moment(item.snapDate).utc(true).format('HH:mm');
               return item;
            });
            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: response
                });
        }
    });

});

module.exports = router;