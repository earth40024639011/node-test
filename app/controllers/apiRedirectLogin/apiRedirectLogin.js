const express = require('express');
const router  = express.Router();
const _       = require('underscore');

const AgentGroupModel   = require('../../../app/models/agentGroup.model');
const CLIENT_NAME       = process.env.CLIENT_NAME || 'SPORTBOOK88';

router.get('/:agent', (req, res) =>{
    const {
        agent
    } = req.params;
    AgentGroupModel.findOne({
        $or: [
            {
                name: agent
            },
            {
                name_lower: agent
            }
        ] ,
        active: true
    })
        .select('_id')
        .exec((error, response) => {
            if (error) {
                return res.send({
                    message: error,
                    code: 9995
                });
            } else {
                const domain = getDomainByClientName();
                return res.send({
                    message: 'success',
                    code: 0,
                    desktop: `https://${domain}/#!/redirect?username=USERNAME&password=PASSWORD&url=URL&hash=${response._id}`,
                    mobile: `https://m.${domain}/#!/redirect?username=USERNAME&password=PASSWORD&url=URL&hash=${response._id}`,
                });
            }
        });
});
function getDomainByClientName() {
    switch (CLIENT_NAME) {
        case 'AMB_SPORTBOOK' :
            return "ambbet.com";
            break;

        case 'CSR' :
            return "lucabet.com";
            break;

        case 'DD88BET' :
            return "sbocopa.com";
            break;

        case 'SB234' :
            return "sb995.net";
            break;

        case 'FOXZ' :
            return "foxz168.com";
            break;

        case 'MGM' :
            return "mgm99win.com";
            break;

        case 'IRON' :
            return "ironmanbet.com";
            break;

        case 'GOAL168' :
            return "sportbook88";
            break;

        case 'BET123' :
            return "123bet.link";
            break;

        case 'IFM789' :
            return "ifm789plus.com";
            break;

        case 'ALLSTAR55' :
            return "viewbet.com";
            break;

        case 'BSBBET' :
            return "bsbbet.com";
            break;

        case 'SPORTBOOK88' :
            return "sportbook88.com";
            break;

        case 'LOVEBALL' :
            return "5gbet.net";
            break;

        case 'WINBET' :
            return "winbetth.com";
            break;

        case 'SBO111' :
            return "allstar55.com";
            break;

        case 'GOAL6969' :
            return "goal6969.com";
            break;

        case 'ISB888' :
            return "isb888.com";
            break;

        case 'PROBET' :
            return "iprobet.com";
            break;

        case 'FAST98' :
            return "fastbet98.com";
            break;

        case 'IWANTBET' :
            return "iwantbet.bet";
            break;

        case 'SAND' :
            return "sand1988.com";
            break;

        case 'FIN88' :
            return "fin88.com";
            break;

        case 'VRCBET' :
            return "vrcbet.com";
            break;

        case 'AFC1688' :
            return "afc168888.com";
            break;

        case 'MBK168' :
            return "mbk168.com";
            break;

        case 'HENG168BET' :
            return "dragon888.online";
            break;

        case 'MVPATM168' :
            return "mvpatm168.com";
            break;

        case 'HAFABET' :
            return "hafabet.com";
            break;

        case 'SAND333' :
            return "sand333.com";
            break;

        case 'TT69BET' :
            return "tt69bet.com";
            break;

        case 'BASA888' :
            return "basa888.com";
            break;

        case 'G2GBET' :
            return "g2gbet.com";
            break;

        case 'POPZA24' :
            return "popza24.com";
            break;
    }
}


module.exports = router;