const redis     = require("ioredis");
const _         = require('underscore');

const client_redis = new redis({
    port: 19071,
    host: 'redis-19071.c1.ap-southeast-1-1.ec2.cloud.redislabs.com',
    family: 4,
    db: 0,
    password: "hxgEbXq4aV3Tl9MwK8RNz0VDSngncpO7"
});

module.exports = {
    getByKey : (key, callback) => {
        client_redis.get(key, (error, result) => {
            callback(null, JSON.parse(result));
        });
    },
    setByKey : (key, obj, callback) => {
        client_redis.set(key, JSON.stringify(obj));
        callback(null, 'success');
    },
    setByKeyTTL : (key, obj, callback) => {
        client_redis.set(key, JSON.stringify(obj), "EX", (3));
        callback(null, 'success');
    }
};

