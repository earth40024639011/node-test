const express               = require('express');
const router                = express.Router();
const CLIENT_NAME           = process.env.CLIENT_NAME || 'SPORTBOOK88';
const _                     = require('underscore');
const MemberModel           = require('../../models/member.model');
const AgentGroupModel       = require('../../models/agentGroup.model');
const CashTransactionModel  = require('../../models/cashTransaction.model');
const CreditModifyHistoryModel = require('../../models/creditModifyHistory.model');
const RedisService          = require('./redisTopup');
const mongoose              = require('mongoose');
const roundTo               = require('round-to');
const waterfall             = require("async/waterfall");
const parallel              = require("async/parallel");
const CashService           = require('../../common/cashService');
const SeqTopupModel         = require('../../models/seqTopup.model');
const crypto                = require('crypto');
const UserModel             = require('../../models/users.model');
const ROLE_LEVEL            = ['AGENT', 'SENIOR', 'MASTER_AGENT'];

router.post('/ambbo/register', (req, res) => {
    let {
        agent,
        prefix
    } = req.body;

    console.log('agent  => ', agent);
    console.log('prefix => ', prefix);
    AgentGroupModel.findOne({
        $or: [
            {
                name: agent
            },
            {
                name_lower: agent
            }
        ] ,
        active: true
    })
        .lean()
        .select('_id type')
        .exec((error, data)=> {
            if (error) {
                return res.send({
                    message: "error",
                    results: error, code: 999
                });
            } else {
                if (_.isNull(data)) {
                    return res.send(
                        {
                            code: 800056,
                            message: `Agent[${agent}] not found`
                        }
                    );
                } else {
                    const groupId = data._id;
                    const type    = data.type;
                    if (_.contains(ROLE_LEVEL, type.toUpperCase())) {
                        const prefixAutoTopUpGateway = new Buffer(prefix).toString('base64');
                        AgentGroupModel.update(
                            {
                                _id: mongoose.Types.ObjectId(groupId),
                                active: true
                            },
                            {
                                isAutoTopUp: true,
                                prefixAutoTopUp: prefix,
                                name: prefix,
                                name_lower: prefix.toLowerCase(),
                                prefixAutoTopUpGateway: prefixAutoTopUpGateway
                            },
                            (error, data) => {
                                if (error) {
                                    return res.send({
                                        message: "error",
                                        results: error, code: 999
                                    });
                                } else {
                                    UserModel.update(
                                        {
                                            group: groupId,
                                            active: true,
                                        },
                                        {
                                            isAutoTopUp: true,
                                            username: prefix,
                                            username_lower: prefix.toLowerCase()
                                        },
                                        (error, data) => {
                                            if (error) {
                                                return res.send({
                                                    message: "error",
                                                    results: error, code: 999
                                                });
                                            } else {
                                                return res.send(
                                                    {
                                                        code: 0,
                                                        message: "success",
                                                        agent: prefix,
                                                        prefixAutoTopUpGateway,
                                                        hash: groupId
                                                    }
                                                );
                                            }
                                        });
                                }
                            });
                    } else {
                        return res.send(
                            {
                                code: 800057,
                                message: `Agent type [${type}] should be ${ROLE_LEVEL}`
                            }
                        );
                    }
                }
            }
        });
});
router.get('/winLose/:memberId/:ref', (req, res) => {
    const {
        memberId,
        ref
    } = req.params;
    CashTransactionModel.findOne({
        member_name: memberId.toLowerCase(),
        ref: ref
    })
        .exec((error, data) => {
            if (error) {
                return res.send({message: "error", result: error, code: 999});
            } else {
                if (data) {
                    CashService.getWinLoseReportMember(data.memberId, data.processDateTime, (error, wlResponse) => {
                        if (error) {
                            return res.send({code: 999, message: "Internal server error" + error});
                        } else {
                            return res.send(
                                {
                                    code: 0,
                                    message: "success",
                                    result: {
                                        username: memberId,
                                        ref: ref,
                                        data: wlResponse
                                    }
                                }
                            );

                        }
                    });
                } else {
                    return res.send({code: 999, message: "Data Not Found"});
                }

            }
        });
});
router.post('/winLose2', (req, res) => {
    const {
        username,
        dateFrom,
        dateTo,
        timeFrom,
        timeTo
    } = req.body;
    CashService.getWinLoseReportMember2(username, dateFrom, dateTo,timeFrom,timeTo,(error, wlResponse) => {
        if (error) {
            if(error == 1004){
                return res.send({code: 999, message: "User not found!"});
            }
            return res.send({code: 999, message: "Internal server error" + error});
        } else {
            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: {
                        username: username,
                        data: wlResponse
                    }
                }
            );

        }
    });
});
router.post('/create/:groupId', (req, res) => {
    const groupId = req.params.groupId;
    let {
        phoneNo,
        prefix,
        key
    } = req.body;

    console.log('groupId  => ', groupId);
    console.log('prefix   => ', prefix);
    console.log('phoneNo  => ', phoneNo);
    waterfall([
            (callback) => {
                AgentGroupModel.findOne({
                    _id: mongoose.Types.ObjectId(groupId),
                    isAutoTopUp: true,
                    prefixAutoTopUp: prefix,
                    $or: [
                        {
                            type: "AGENT"
                        },
                        {
                            type: "SENIOR"
                        } ,
                        {
                            type: "MASTER_AGENT"
                        }
                    ] ,
                    active: true
                })
                    .lean()
                    .exec((error, data) => {
                        if (error) {
                            callback(error, null);
                        } else {
                            callback(null, data);
                        }
                    });
            }
        ],
        (error, result) => {
            if (error) {
                return res.send({
                    code    : 9999,
                    message : error
                });
            } else {
                if (_.isUndefined(result) || _.isNull(result)) {
                    return res.send({
                        code    : 9999,
                        message : `Agent are not allow please contact admin. [${prefix}]`
                    });
                } else {
                    const {
                        childMembers,
                        name
                    } = result;


                    let start = 3;
                    let end = 10;
                    if (phoneNo.length !== 10) {
                        start = 0;
                        end = phoneNo.length;
                    }
                    const numberSub = phoneNo.substring(start, end);

                    const nameMock = `${name.toLowerCase()}mock`;
                    const loginName = `${prefix.toLowerCase()}${numberSub}`;
                    console.log(nameMock);
                    console.log(loginName);
                    parallel([
                            //1
                            (callback) => {
                                MemberModel.findOne({
                                    $or : [
                                        {
                                            username_lower: nameMock.toLowerCase()
                                        },
                                        {
                                            username: nameMock
                                        }
                                    ]
                                })
                                    .lean()
                                    .exec((error, data) => {
                                        if (error) {
                                            callback(error, null);
                                        } else {
                                            callback(null, data);
                                        }
                                    });
                            },

                            //2
                            (callback) => {
                                MemberModel.findOne({
                                    $or : [
                                        {
                                            loginName_lower: loginName.toLocaleLowerCase()
                                        },
                                        {
                                            loginName: loginName
                                        }
                                    ]
                                })
                                    .lean()
                                    .exec((error, data) => {
                                        if (error) {
                                            callback(error, null);
                                        } else {
                                            callback(null, data);
                                        }
                                    });
                            }],
                        (error, parallelResult) => {
                            if (error) {
                                return res.send({
                                    message: "Error", result: error, code: 999
                                });
                            } else {
                                const [
                                    memberModelMock,
                                    memberModelPrefixPhoneNo
                                ] = parallelResult;
                                if (_.isUndefined(memberModelMock) || _.isNull(memberModelMock)) {
                                    return res.send({
                                        code    : 9999,
                                        message : `กรุณาไป สร้างยูสเม็มเบอร์ ชื่อ ${nameMock} \nในระบบเอเย่นสปอร์ตบุคก่อน จึงจะสามารถ\nสมัครผ่าน ระบบ ambbo ได้`
                                    });
                                } else if (!_.isUndefined(memberModelPrefixPhoneNo) && !_.isNull(memberModelPrefixPhoneNo)) {
                                    return res.send({
                                        code    : 8888,
                                        message : `ยูสเม็มเบอร์ ชื่อ ${prefix}${numberSub} มีการสมัครเรียบร้อยแล้ว`
                                    });
                                } else {
                                    const model = memberModelMock;
                                    delete model._id;
                                    delete model.lastPaymentDate;

                                    model.balance = 0;
                                    model.creditLimit= 0;
                                    model.forceChangePassword = false;
                                    model.isAutoTopUp = true;

                                    // const username = `${name}${runningNumber(''+(_.size(childMembers)+1))}`;
                                    const username = `${name}${numberSub}`;

                                    model.username = username;
                                    model.username_lower= username.toLowerCase();

                                    model.loginName= loginName.toUpperCase();
                                    model.loginName_lower= loginName.toLowerCase();

                                    model.createdDate = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
                                    model.updatedDate = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);

                                    model.password = crypto.createHash('sha256').update(key+'').digest('base64');

                                    model.phoneNo = phoneNo;
                                    model.currency = 'THB';

                                    const memberModel = new MemberModel(model);

                                    memberModel.save((error, memberResponse) => {
                                        if (error) {
                                            return res.send({
                                                message: "Error", result: error, code: 999
                                            });
                                        } else {
                                            AgentGroupModel.update(
                                                {
                                                    _id: groupId
                                                },
                                                {
                                                    $push: {
                                                        childMembers: memberResponse._id
                                                    }
                                                }, (error, data) => {
                                                    if (error) {
                                                        return res.send({
                                                            message: "Error", result: error, code: 999
                                                        });
                                                    } else {
                                                        console.log('update parent child success');
                                                        return res.send(
                                                            {
                                                                code: 0,
                                                                message: "success",
                                                                result: {
                                                                    username: memberResponse.username,
                                                                    loginName: memberResponse.loginName
                                                                }
                                                            }
                                                        );
                                                    }
                                                });
                                        }
                                    });
                                }
                            }
                        });
                }
            }
        });
});
router.put('/reset-password/:groupId/:memberId', (req, res) => {
    const groupId    = req.params.groupId;
    const memberId = req.params.memberId;
    const {
        password
    } = req.body;

    const hash = crypto.createHash('sha256').update(password+'').digest('base64');
    console.log('groupId  => ', groupId);
    console.log('memberId => ', memberId);
    console.log('hash     => ', hash);
    MemberModel.update({
            $or : [
                {
                    group: mongoose.Types.ObjectId(groupId),
                    loginName_lower: memberId.toLowerCase()
                },
                {
                    group: mongoose.Types.ObjectId(groupId),
                    username_lower: memberId.toLowerCase()
                },
                {
                    group: mongoose.Types.ObjectId(groupId),
                    username: memberId.toLowerCase()
                }
            ]
        },
        {
            password: hash
        },(error, data) => {
            if (error) {
                return res.send({message: "error", result: error, code: 999});
            } else {
                if (_.isUndefined(data)) {
                    return res.send({message: "DATA NOT FOUND", code: 8888});
                } else {
                    return res.send({code: 0});
                }
            }
        });
});
router.get('/credit/:groupId/:memberId', (req, res) => {
    const {
        groupId,
        memberId
    } = req.params;
    const redisKey = `GET_CREDIT_${CLIENT_NAME}_${groupId}_${memberId}`;
    RedisService.getByKey(redisKey, (error, response) => {
        if (_.isNull(response)) {
            MemberModel.aggregate([
                {
                    $match: {
                        $or: [
                            {
                                group: mongoose.Types.ObjectId(groupId),
                                loginName_lower: memberId.toLowerCase()
                            },
                            {
                                group: mongoose.Types.ObjectId(groupId),
                                username_lower: memberId.toLowerCase()
                            },
                            {
                                group: mongoose.Types.ObjectId(groupId),
                                username: memberId.toLowerCase()
                            }
                        ]
                    }
                },
                {
                    $project: {
                        credit:  {$toDouble:{$add: [{$toDecimal:'$creditLimit'}, {$toDecimal:'$balance'}]}},
                    }
                },
            ], (err, results) => {
                if (err) {
                    return res.send({
                        result: {
                            credit : 0
                        },
                        code: 0
                    });
                } else {
                    if (_.isEmpty(results)) {
                        return res.send({message: "DATA NOT FOUND", code: 999});
                    } else {
                        let credit = roundTo(results[0].credit, 2);
                        if (typeof credit !== 'number' || _.isUndefined(credit) || _.isNaN(credit)) {
                            RedisService.setByKeyTTL(redisKey, {
                                result: {
                                    credit : 0
                                },
                                code: 0
                            }, (error, result) => {

                            });
                            return res.send({
                                result: {
                                    credit : 0
                                },
                                code: 0
                            });
                        } else {
                            RedisService.setByKeyTTL(redisKey, {
                                result: {
                                    credit
                                },
                                code: 0
                            }, (error, result) => {

                            });
                            return res.send({
                                result: {
                                    credit
                                },
                                code: 0
                            });
                        }
                    }
                }
            });

        } else {
            return res.send(response);
        }
    });
});
router.post('/user/update/:groupId', (req, res) => {
    const groupId = req.params.groupId;
    let {
        prefix,
        prefixAutoTopUpGateway
    } = req.body;

    console.log('groupId                => ', groupId);
    console.log('prefix                 => ', prefix);
    console.log('prefixAutoTopUpGateway => ', prefixAutoTopUpGateway);

    parallel([
            //1
            (callback) => {
                AgentGroupModel.findOne({
                    _id: mongoose.Types.ObjectId(groupId),
                    active: true
                })
                    .lean()
                    .select('type name name_lower prefixAutoTopUp isAutoTopUp')
                    .exec((error, data) => {
                        if (error) {
                            callback(error, null);
                        } else {
                            callback(null, data);
                        }
                    });
            },

            //2
            (callback) => {
                UserModel.findOne({
                    group: groupId,
                    active: true,
                })
                    .lean()
                    .select('username username_lower isAutoTopUp')
                    .exec((error, data) => {
                        if (error) {
                            callback(error, null);
                        } else {
                            callback(null, data);
                        }
                    });
            }],
        (error, parallelResult) => {
            if (error) {
                return res.send({
                    message: "Error", result: error, code: 999
                });
            } else {
                const [
                    agentGroupModel,
                    userModel
                ] = parallelResult;

                if (_.isNull(agentGroupModel) || _.isNull(userModel)) {
                    return res.send(
                        {
                            code: 999,
                            message: "Something was wrong in AgentGroup or UserModel"
                        }
                    );
                } else {
                    AgentGroupModel.update(
                        {
                            _id: mongoose.Types.ObjectId(groupId),
                            active: true
                        },
                        {
                            isAutoTopUp: true,
                            prefixAutoTopUp: prefix,
                            name: prefix,
                            name_lower: prefix.toLowerCase(),
                            prefixAutoTopUpGateway: prefixAutoTopUpGateway
                        },
                        (error, data) => {
                            if (error) {
                                return res.send({
                                    message: "error",
                                    results: error, code: 999
                                });
                            } else {
                                UserModel.update(
                                    {
                                        group: groupId,
                                        active: true,
                                    },
                                    {
                                        isAutoTopUp: true,
                                        username: prefix,
                                        username_lower: prefix.toLowerCase()
                                    },
                                    (error, data) => {
                                        if (error) {
                                            return res.send({
                                                message: "error",
                                                results: error, code: 999
                                            });
                                        } else {
                                            return res.send(
                                                {
                                                    code: 0,
                                                    message: "success"
                                                }
                                            );
                                        }
                                    });
                            }
                        });
                }
            }
        });
});

router.post('/deposit/:groupId/:memberId', (req, res) => {
    const groupId    = req.params.groupId;
    const memberId = req.params.memberId;
    let {
        amount,
        date
    } = req.body;

    let ref = 'D' + new Date().getTime()+_.random(10000, 99999);
    if (!_.isUndefined(date)) {
        ref = `D${memberId}${amount}${date}`.toUpperCase().replace(/\s/g, '').replace(/\//g, '').replace(/:/g, '').replace(/-/g, '');
    }
    console.log('groupId  => ', groupId);
    console.log('memberId => ', memberId);
    console.log('amount   => ', amount);
    console.log('ref      => ', ref);
    CashTransactionModel.findOne({
        ref: ref
    })
        .lean()
        .exec((error, data) => {
            if (error) {
                return res.send({
                    message: "Error", result: error, code: 999
                });
            } else {
                if (_.isNull(data)) {
                    parallel([
                            //1
                            (callback) => {
                                AgentGroupModel.findOne({_id: mongoose.Types.ObjectId(groupId)})
                                    .lean()
                                    .exec((error, data) => {
                                        if (error) {
                                            callback(error, null);
                                        } else {
                                            callback(null, data);
                                        }
                                    });
                            },

                            //2
                            (callback) => {
                                parallel([
                                        (callback) => {
                                            AgentGroupModel.aggregate([
                                                {
                                                    $match: {
                                                        parentId: mongoose.Types.ObjectId(groupId)
                                                    }
                                                },
                                                {
                                                    $project: {_id: 0}
                                                },
                                                {
                                                    "$group": {
                                                        "_id": {
                                                            id: '$_id'
                                                        },
                                                        "creditLimit": {
                                                            $sum: '$creditLimit'
                                                        }
                                                    }
                                                }
                                            ], (error, data) => {
                                                if (error) {
                                                    callback(error, null);
                                                } else {
                                                    callback(null, !_.isUndefined(_.first(data)) ? _.first(data).creditLimit : 0);
                                                }
                                            });
                                        },

                                        (callback) => {
                                            MemberModel.aggregate([
                                                {
                                                    $match: {
                                                        group: mongoose.Types.ObjectId(groupId)
                                                    }
                                                },
                                                {
                                                    $project: {_id: 0}
                                                },
                                                {
                                                    "$group": {
                                                        "_id": {
                                                            id: '$_id'
                                                        },
                                                        "creditLimit": {
                                                            $sum: '$creditLimit'
                                                        }
                                                    }
                                                }
                                            ], (error, data) => {
                                                if (error) {
                                                    callback(error, null);
                                                } else {
                                                    callback(null, !_.isUndefined(_.first(data)) ? _.first(data).creditLimit : 0);
                                                }
                                            });
                                        },
                                    ],
                                    (error, parallelResult) => {
                                        if (error) {
                                            callback(error, null);
                                        } else {
                                            const [
                                                agentGroupCreditLimit,
                                                memberCreditLimit
                                            ] = parallelResult;
                                            callback(null, agentGroupCreditLimit + memberCreditLimit);
                                        }
                                    });
                            },

                            //3
                            (callback) => {
                                MemberModel.findOne({
                                    $or : [
                                        {
                                            loginName_lower: memberId.toLowerCase()
                                        },
                                        {
                                            username_lower: memberId.toLowerCase()
                                        },
                                        {
                                            username: memberId.toLowerCase()
                                        }
                                    ]
                                })
                                    .populate({
                                        path: 'group',
                                        select: '_id type name endpoint secretKey'
                                    })
                                    .select('')
                                    .exec((error, data)=>{
                                        if (error) {
                                            callback(error, null);
                                        } else {
                                            callback(null, data);
                                        }
                                    });
                            }],
                        (error, parallelResult) => {
                            if (error) {
                                return res.send({
                                    message: "Error", result: error, code: 999
                                });
                            } else {
                                const [
                                    agentGroupModel,
                                    creditLimitUsage,
                                    memberModel
                                ] = parallelResult;
                                amount = Math.abs(amount);
                                if (_.isNull(memberModel)) {
                                    return res.send({message: `Member[${memberId}] NOT FOUND`, code: 999});
                                }
                                if (amount > (agentGroupModel.creditLimit - creditLimitUsage)) {
                                    return res.send({message: "เครดิตในระบบเอเย่น ของท่านไม่เพียงพอ \n กรุณาติดต่อบริษัทของท่าน เพื่อขอเครดิตเพิ่ม", code: 4002});
                                } else {
                                    if ((amount + memberModel.creditLimit) > agentGroupModel.maxCreditLimit) {
                                        return res.send({message: "เครดิตในระบบเอเย่น ของท่านไม่เพียงพอ \n กรุณาติดต่อบริษัทของท่าน เพื่อขอเครดิตเพิ่ม", code: 4030});
                                    } else {
                                        MemberModel.findOneAndUpdate(
                                            {
                                                _id: mongoose.Types.ObjectId(memberModel._id)
                                            },
                                            {
                                                $inc: {
                                                    creditLimit: amount
                                                }
                                            },
                                            {
                                                new: true
                                            })
                                            .exec((error, memberUpdateResponse) => {
                                                if (error) {
                                                    return res.send({
                                                        message: "Error", result: error, code: 999
                                                    });
                                                } else {
                                                    const historyBody = {
                                                        memberId: memberUpdateResponse._id,
                                                        type: 'ADD',
                                                        ref: ref,
                                                        beforeCredit: memberModel.creditLimit,
                                                        beforeBalance: memberModel.balance,
                                                        afterCredit: memberUpdateResponse.creditLimit,
                                                        afterBalance: memberUpdateResponse.balance,
                                                        amount: amount,
                                                        createdDate: new Date(new Date().getTime() + 1000 * 60 * 60 * 7)
                                                    };

                                                    const creditModifyModel = new CreditModifyHistoryModel(historyBody);
                                                    creditModifyModel.save((err, creditModifyResponse) => {
                                                        if (err) {
                                                            return res.send({code: 0,message: "success",result: true});
                                                        }

                                                        let cashBody = {
                                                            ref: ref,
                                                            agent: groupId,
                                                            memberId: memberUpdateResponse._id,
                                                            member_name: memberUpdateResponse.username_lower,
                                                            transferType: 'API_PARTNER',
                                                            processDateTime: new Date(new Date().getTime() + 1000 * 60 * 60 * 7),
                                                            amount: amount,
                                                            status: 'COMPLETE',
                                                            active: true,
                                                            tranType: 'DEPOSIT',
                                                            type: 'AUTO',
                                                            createdDate: new Date(new Date().getTime() + 1000 * 60 * 60 * 7)
                                                        };

                                                        let cashModel = new CashTransactionModel(cashBody);
                                                        cashModel.save((err, HistoryResponse) => {

                                                            if(err){
                                                                return res.send({code: 999,message: "fail",result: true});
                                                            }

                                                            let body = {
                                                                afterCredit: memberUpdateResponse.creditLimit,
                                                                afterBalance: memberUpdateResponse.balance
                                                            };

                                                            CreditModifyHistoryModel.update(
                                                                {
                                                                    _id: creditModifyResponse._id
                                                                },
                                                                body,
                                                                (err, updateCreditRes) => {
                                                                    // callback(null, memberUpdateResponse)
                                                                });

                                                            return res.send(
                                                                {
                                                                    code: 0,
                                                                    result: {
                                                                        before: roundTo(memberModel.creditLimit+memberModel.balance, 2),
                                                                        amount,
                                                                        after: roundTo(memberUpdateResponse.creditLimit+memberUpdateResponse.balance, 2),
                                                                        ref
                                                                    }
                                                                }
                                                            );
                                                        });
                                                    });
                                                }
                                            });
                                    }
                                }
                            }
                        });
                } else {
                    return res.send({
                        message: ref+ "\nรายการดังกล่าวได้มีการเติมเรียบร้อยแล้ว", code: 900000
                    });
                }
            }
        });
});
router.post('/withdraw/:groupId/:memberId', (req, res) => {
    const groupId    = req.params.groupId;
    const memberId = req.params.memberId;
    let {
        amount
    } = req.body;

    const ref = 'W' + new Date().getTime()+_.random(10000, 99999);

    console.log('groupId  => ', groupId);
    console.log('memberId => ', memberId);
    console.log('amount   => ', amount);
    console.log('ref      => ', ref);

    parallel([
            //1
            (callback) => {
                AgentGroupModel.findOne({_id: mongoose.Types.ObjectId(groupId)})
                    .lean()
                    .exec((error, data) => {
                        if (error) {
                            callback(error, null);
                        } else {
                            callback(null, data);
                        }
                    });
            },

            //2
            (callback) => {
                parallel([
                        (callback) => {
                            AgentGroupModel.aggregate([
                                {
                                    $match: {
                                        parentId: mongoose.Types.ObjectId(groupId)
                                    }
                                },
                                {
                                    $project: {_id: 0}
                                },
                                {
                                    "$group": {
                                        "_id": {
                                            id: '$_id'
                                        },
                                        "creditLimit": {
                                            $sum: '$creditLimit'
                                        }
                                    }
                                }
                            ], (error, data) => {
                                if (error) {
                                    callback(error, null);
                                } else {
                                    callback(null, !_.isUndefined(_.first(data)) ? _.first(data).creditLimit : 0);
                                }
                            });
                        },

                        (callback) => {
                            MemberModel.aggregate([
                                {
                                    $match: {
                                        group: mongoose.Types.ObjectId(groupId)
                                    }
                                },
                                {
                                    $project: {_id: 0}
                                },
                                {
                                    "$group": {
                                        "_id": {
                                            id: '$_id'
                                        },
                                        "creditLimit": {
                                            $sum: '$creditLimit'
                                        }
                                    }
                                }
                            ], (error, data) => {
                                if (error) {
                                    callback(error, null);
                                } else {
                                    callback(null, !_.isUndefined(_.first(data)) ? _.first(data).creditLimit : 0);
                                }
                            });
                        },
                    ],
                    (error, parallelResult) => {
                        if (error) {
                            callback(error, null);
                        } else {
                            const [
                                agentGroupCreditLimit,
                                memberCreditLimit
                            ] = parallelResult;
                            callback(null, agentGroupCreditLimit + memberCreditLimit);
                        }
                    });
            },

            //3
            (callback) => {
                MemberModel.findOne({
                    $or : [
                        {
                            loginName_lower: memberId.toLowerCase()
                        },
                        {
                            username_lower: memberId.toLowerCase()
                        },
                        {
                            username: memberId.toLowerCase()
                        }
                    ]
                })
                    .populate({
                        path: 'group',
                        select: '_id type name endpoint secretKey'
                    })
                    .select('')
                    .exec((error, data) => {
                        if (error) {
                            callback(error, null);
                        } else {
                            callback(null, data);
                        }
                    });
            }],
        (error, parallelResult) => {
            if (error) {
                return res.send({
                    message: "Error", result: error, code: 999
                });
            } else {
                const [
                    agentGroupModel,
                    creditLimitUsage,
                    memberModel
                ] = parallelResult;

                amount = Math.abs(amount) * -1;
                if (amount > (agentGroupModel.creditLimit - creditLimitUsage)) {
                    return res.send({message: "เครดิตในระบบเอเย่น ของท่านไม่เพียงพอ \n กรุณาติดต่อบริษัทของท่าน เพื่อขอเครดิตเพิ่ม", code: 4002});
                } else {
                    if ((amount + memberModel.creditLimit) > agentGroupModel.maxCreditLimit) {
                        return res.send({message: "เครดิตในระบบเอเย่น ของท่านไม่เพียงพอ \n กรุณาติดต่อบริษัทของท่าน เพื่อขอเครดิตเพิ่ม", code: 4030});
                    } else {

                        const {
                            creditLimit,
                            balance
                        } = memberModel;

                        const totalWithdraw = roundTo(creditLimit, 2) + roundTo(balance, 2);
                        const afterUpdateCreditLimitPreCal = roundTo(totalWithdraw, 2) + roundTo(amount, 2);
                        const afterUpdateCreditLimit = _.isNaN(afterUpdateCreditLimitPreCal) ? 0 : afterUpdateCreditLimitPreCal;
                        console.log('totalWithdraw          : '+totalWithdraw);
                        console.log('amount                 : '+amount);
                        console.log('afterUpdateCreditLimit : '+afterUpdateCreditLimit);
                        const isValid = 0 <= afterUpdateCreditLimit;
                        if (isValid) {
                            MemberModel.findOneAndUpdate(
                                {
                                    _id: mongoose.Types.ObjectId(memberModel._id)
                                },
                                {
                                    creditLimit: afterUpdateCreditLimit,
                                    balance: 0
                                },
                                {
                                    new: true
                                })
                                .exec((error, memberUpdateResponse) => {
                                    if (error) {
                                        return res.send({
                                            message: "Error", result: error, code: 999
                                        });
                                    } else {

                                        const historyBody = {
                                            memberId: memberUpdateResponse._id,
                                            type: 'REDUCE',
                                            ref: ref,
                                            beforeCredit: memberModel.creditLimit,
                                            beforeBalance: memberModel.balance,
                                            afterCredit: memberUpdateResponse.creditLimit,
                                            afterBalance: memberUpdateResponse.balance,
                                            amount: Math.abs(amount),
                                            createdDate: new Date(new Date().getTime() + 1000 * 60 * 60 * 7)
                                        };

                                        const creditModifyModel = new CreditModifyHistoryModel(historyBody);
                                        creditModifyModel.save((err, creditModifyResponse) => {
                                            if (err) {
                                                return res.send({code: 0,message: "success",result: true});
                                            }

                                            let cashBody = {
                                                ref: ref,
                                                agent: groupId,
                                                memberId: memberUpdateResponse._id,
                                                member_name: memberUpdateResponse.username_lower,
                                                transferType: 'KIOSK',
                                                processDateTime: new Date(new Date().getTime() + 1000 * 60 * 60 * 7),
                                                amount: Math.abs(amount),
                                                status: 'COMPLETE',
                                                active: true,
                                                tranType: 'WITHDRAW',
                                                type: 'AUTO',
                                                createdDate: new Date(new Date().getTime() + 1000 * 60 * 60 * 7)
                                            };

                                            let cashModel = new CashTransactionModel(cashBody);
                                            cashModel.save((err, HistoryResponse) => {

                                                if(err){
                                                    return res.send({code: 999,message: "fail",result: true});
                                                }

                                                let body = {
                                                    afterCredit: memberUpdateResponse.creditLimit,
                                                    afterBalance: memberUpdateResponse.balance
                                                };

                                                CreditModifyHistoryModel.update(
                                                    {
                                                        _id: creditModifyResponse._id
                                                    },
                                                    body, (err, updateCreditRes) => {
                                                        // callback(null, memberUpdateResponse)
                                                    });

                                                return res.send(
                                                    {
                                                        code: 0,
                                                        result: {
                                                            before: roundTo(memberModel.creditLimit+memberModel.balance, 2),
                                                            amount,
                                                            after: afterUpdateCreditLimit,
                                                            ref
                                                        }
                                                    }
                                                );
                                            });
                                        });
                                    }
                                });
                        } else {
                            return res.send({
                                message: `0 <= ${totalWithdraw} + ${amount} diff ${totalWithdraw+amount}`,
                                code: 4002,
                                diff: totalWithdraw+amount
                            });
                        }
                    }
                }
            }
        });
});

router.get('/deposit/:ref', (req, res) => {
    const ref    = req.params.ref;
    console.log('ref => ', ref);
    CreditModifyHistoryModel.findOne({
        ref: ref
    })
        .lean()
        .exec((error, data) => {
            if (error) {
                return res.send({
                    message: "Error", result: error, code: 999
                });
            } else if (!data) {
                return res.send({code: 999, message: "Data Not Found"});
            } else {
                return res.send({message: "success", result: data, code: 0});
            }
        });
});
module.exports = router;