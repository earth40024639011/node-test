function data() {
  return [
      {
          "client": "SPORTBOOK88",
          "key": "c54583174899b9336642622799fa2bbb",
          "groupId": "5efe97555eb74d4a79590d9d",
          "usernameLower": "tsb",
          "isActive": true,
          "price": 0,
          "remark": "TEST"
      },
      {
          "client": "SPORTBOOK88",
          "key": "1818918170d3bcc096139e96126992a2",
          "groupId": "5d0b4a538fa0d6cf7b265755",
          "usernameLower": "14sk",
          "isActive": true,
          "price": 0,
          "remark": "TEST"
      },
      {
          "client": "CSR",
          "key": "6d86cc137810841e899d21afd34cf9bd",
          "groupId": "5c67c66e21770d4eb77ba4c4",
          "usernameLower": "91og",
          "isActive": true,
          "price": 5000
      },
      {
        "client": "CSR",
        "key": "94f960561287e132af600c161087f3a9",
        "groupId": "5e61e7553615ed0374cd7e21",
        "usernameLower": "26xwv1",
        "isActive": true,
        "price": 5000
      },
      {
        "client": "CSR",
        "key": "232be3bda7447b605d728cf2e5bf7420",
        "groupId": "5e95deab10ef325a13130405",
        "usernameLower": "62ay888",
        "isActive": true,
        "price": 5000
      },
      {
        "client": "CSR",
        "key": "946543aad46fe35cee73a20fae5b0d34",
        "groupId": "5e9463788e89353a922d9e28",
        "usernameLower": "55rbv1",
        "isActive": true,
        "price": 5000
      },
      {
        "client": "CSR",
        "key": "144ec4696b33af891aa379fa966dfa9c",
        "groupId": "5e9465b77f02be5a73447d21",
        "usernameLower": "01hiv1",
        "isActive": true,
        "price": 5000
      },
      {
        "client": "CSR",
        "key": "ebed09e8bcb5bf79094d1e4ddf25eb00",
        "groupId": "5e61e5788c4dcd39d0ef9bbb",
        "usernameLower": "29pfv1",
        "isActive": true,
        "price": 5000
      },
      {
        "client": "CSR",
        "key": "ddfd75fe10b84a9adf71e00fe95712dd",
        "groupId": "5e99670086e2ce5a0de40e8c",
        "usernameLower": "90jyv1",
          "isActive": false,
          "price": 0,
          "remark": "Canceled"
      },
      {
        "client": "CSR",
        "key": "03b5ea2d59037d656cc58c70c63dc249",
        "groupId": "5e996537143d843b373b3cd4",
        "usernameLower": "04asv1",
        "isActive": true,
        "price": 5000
      },
      {
        "client": "CSR",
        "key": "5f7d45380cf03ffc392c3a73e2f3f282",
        "groupId": "5e9468624432ec3765c53d2e",
        "usernameLower": "82alv1",
        "isActive": true,
        "price": 5000
      },
      {
        "client": "CSR",
        "key": "118135538525046d777c337ea39e3c19",
        "groupId": "5e61e634f36e29471b602c80",
        "usernameLower": "67xdv1",
        "isActive": true,
        "price": 5000
      },
      {
        "client": "CSR",
        "key": "c5d69e9c839c3aab30762acaa6ef1aef",
        "groupId": "5e942902fb328d3a823139e2",
        "usernameLower": "24lhts",
        "isActive": true,
        "price": 5000
      },
      {
        "client": "CSR",
        "key": "8c72ef81c7dd8993503837d61f9ac662",
        "groupId": "5e7a1dc3eb2fe910225fb410",
        "usernameLower": "87rgv1",
        "isActive": true,
        "price": 5000
      },
      {
        "client": "CSR",
        "key": "95171be5d072c33c54574e5430191894",
        "groupId": "5e7a1bdbbf1a0e7f6e2b787a",
        "usernameLower": "25afv1",
        "isActive": true,
        "price": 5000
      },
      {
        "client": "CSR",
        "key": "df66d89468d3933ca15a22f8ff369633",
        "groupId": "5e68cf4d3e2e523912910380",
        "usernameLower": "16ym555",
        "isActive": true,
        "price": 5000
      },
      {
        "client": "CSR",
        "key": "6cbf3f99fa4c03af9f1863ea52f399a9",
        "groupId": "5e6f8b3f606f9135f73ce96e",
        "usernameLower": "32ut01",
        "isActive": true,
        "price": 5000
      },
      {
          "client": "CSR",
          "key": "51d0d441e639c8fae3c27b07a74abed0",
          "groupId": "5d3826bcfcb3bc19a602c485",
          "usernameLower": "91dl01",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "17fbb801934f7de784119ae2d70f61c2",
          "groupId": "5d3826cd11f26618fc6ffe92",
          "usernameLower": "91dl02",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "3908272d758f13993d4d01ff1e5aaa53",
          "groupId": "5d626dbec5c7125e4f848861",
          "usernameLower": "48roa1",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "55bba8f56c38997527abc5e79e8868b1",
          "groupId": "5d64ed458a2f3a01883ab075",
          "usernameLower": "13dosky",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "97b2824f555cea36577606727ca8670a",
          "groupId": "5d761ffcec1bf419ffb0f86f",
          "usernameLower": "48ro0188",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "7d64d846d19623bae44eb863b8517e7c",
          "groupId": "5d76203737d94e1a524f247e",
          "usernameLower": "48ro0168",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "e2347d24130e28906bef2d910ebbfa45",
          "groupId": "5d6642dc786e50772a8e047d",
          "usernameLower": "48roa2",
          "isActive": false,
          "price": 0,
          "remark": "Canceled"
      },
      {
          "client": "CSR",
          "key": "b7ddc30b1bbdf7551f6c15fd233a4ed0",
          "groupId": "5d664389400b375b8a2dcc7e",
          "usernameLower": "48roa3",
          "isActive": false,
          "price": 0,
          "remark": "Canceled"
      },
      {
          "client": "CSR",
          "key": "b65d9a81b6a81577183427a1bc5a4311",
          "groupId": "5d6cfe0fe6694c6529f1017c",
          "usernameLower": "48roa4",
          "isActive": false,
          "price": 0,
          "remark": "Canceled"
      },
      {
          "client": "CSR",
          "key": "c1d79970b80664ef6f454f8a3652617d",
          "groupId": "5d740bc24d6dc21a1f1d822e",
          "usernameLower": "29tv01",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "c56ee41df8b17879e50acc1267149394",
          "groupId": "5d73bb5fe6694c6529f14b4d",
          "usernameLower": "02pt88",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "65d9f94349d87261f11d3c5f1fae3396",
          "groupId": "5d75f86909abc705062d2665",
          "usernameLower": "77ay01",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "650b45e9cdde7b5a0c4c22737cedd95d",
          "groupId": "5d75f8dc79d5f5057ab2999d",
          "usernameLower": "77ay02",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "bfb4bdcf31992ac1db0cc78d72bc246d",
          "groupId": "5d75f8f409330205c713a093",
          "usernameLower": "77ay03",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "3196ab7a61020472cccdd0131392cee9",
          "groupId": "5d75fdc6b454d519654c5262",
          "usernameLower": "46wh11",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "90f5e1cf2c80d7b92bab59be4bc1d1f0",
          "groupId": "5d8f2172f568bb6f5b09324f",
          "usernameLower": "48ro0155",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "324785ec5753434d62da2e11cf60b9e4",
          "groupId": "5d8c8f9ab310187120dd6a57",
          "usernameLower": "09jm0369",
          "isActive": false,
          "price": 0,
          "remark": "Canceled"
      },
      {
          "client": "CSR",
          "key": "a4b64dbac6c53a10a6fe5ba314428295",
          "groupId": "5da4510dbe74f408de92e911",
          "usernameLower": "31vw0789",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "d2777a910736062c373388410ce57935",
          "groupId": "5da5c77fe2c252101268689d",
          "usernameLower": "28ga01",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "674c20cb57adac0a1ba3f2ff6c4770b7",
          "groupId": "5db998a201ebd8165105f15f",
          "usernameLower": "15lu01",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "d2ca9534a9c20c8dd88d115c4a89dedc",
          "groupId": "5db31de77339a503de9739d1",
          "usernameLower": "52ch789",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "03b9a695e9bd68461ff49aaa070deca0",
          "groupId": "5dc1039902e09325aa1fe043",
          "usernameLower": "17gj888",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "4af5fdb85febd543cdd31a6fe26db9a2",
          "groupId": "5dd22a885bbbbd5d38e6b3d2",
          "usernameLower": "88cx88",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "7d7a02a78a1d2825417604bd57cad067",
          "groupId": "5dcd3946ca40601f4e51cf7c",
          "usernameLower": "40sy191",
          "isActive": false,
          "price": 0
      },
      {
          "client": "CSR",
          "key": "6395467463df54d4dc59a94f8e690092",
          "groupId": "5dcd3b7e2ecf6b5d8ff66da8",
          "usernameLower": "18eu99",
          "isActive": false,
          "price": 0
      },
      {
          "client": "CSR",
          "key": "95b3f90e1f470ae5da48669d52666599",
          "groupId": "5dd6301824b52f720946ce8d",
          "usernameLower": "64kjdg1",
          "isActive": false,
          "price": 0
      },
      {
          "client": "CSR",
          "key": "0261c7224f51005a0d601d5377902b73",
          "groupId": "5df500fbbd7bd7214ee9ba28",
          "usernameLower": "98vss",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "ee7f0a97c58937b83f1bdeeef65a96e3",
          "groupId": "5df6741d5a54e43b454f8a3c",
          "usernameLower": "80js01",
          "isActive": false,
          "price": 0
      },
      {
          "client": "CSR",
          "key": "15570d099fc34f812b07002a54411955",
          "groupId": "5df4f264c9c9a81da79dca5b",
          "usernameLower": "98vst",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "1ffd9101fef6d96ab94f8933c9b02244",
          "groupId": "5e03a91c4018d356262cbae1",
          "usernameLower": "41yp01",
          "isActive": true,
          "price": 5000
      },
      {
        "client": "CSR",
        "key": "e8cb2915b16f87020a85a9af6cf2098d",
        "groupId": "5e56469f9c148578b6092ef7",
        "usernameLower": "24lhxx",
        "isActive": true,
        "price": 5000
      },
      {
        "client": "CSR",
        "key": "3442554d9cef114948baa125a0a17c2f",
        "groupId": "5ea695909b4eb8252cb484d0",
        "usernameLower": "97rpv1",
        "isActive": true,
        "price": 5000
      },
      {
          "client": "CSR",
          "key": "22e62daf3da30bf0614d0829a3b37564",
          "groupId": "5ebb877bcd53a059cdea7175",
          "usernameLower": "40vvv1",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "2073df686f0e55a02c08edf0e4de9754",
          "groupId": "5ec382db4cbc9305263020ec",
          "usernameLower": "77ay09",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "451f11b3c069c658417d62eee2397184",
          "groupId": "5ec5f8d61b06f77771ac6c5a",
          "usernameLower": "11pzv1",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "552025ec136e4ea05ebcf8ad49bdf416",
          "groupId": "5ecb8fb710340a77b71671bb",
          "usernameLower": "44yy44",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "e65b2e28b0829357c95e926a18d7a7fc",
          "groupId": "5ecc95832998a077b51376cf",
          "usernameLower": "22vbv1",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "4cc457c757da8bc2b4e92f74481fc287",
          "groupId": "5ebbc215a020bf658e94154d",
          "usernameLower": "73gj01",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "7ab6e0e980b504655dad64b86409c1ea",
          "groupId": "5ece226a18e24f056ec33671",
          "usernameLower": "29pfv2",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "987825453ab2f75ba3ce90c1f3370f6d",
          "groupId": "5ed9cb4fb2cb7649de4cac2f",
          "usernameLower": "11mu11",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "373a5c83efa500c1b541ec98de89ca6a",
          "groupId": "5ed9cc7e936dca55a65cbe3b",
          "usernameLower": "45ky45",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "dda73ef3776c8d1f0d665964cc02e0ce",
          "groupId": "5ed9c687de03fe49fd1692a2",
          "usernameLower": "88pm88",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "20b18ce62729e604078a814fdfda17c5",
          "groupId": "5ed77e3e7d312235980d9694",
          "usernameLower": "98wq89",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "064e7d48d8aab82a190f16bb38113e36",
          "groupId": "5ed65f39b2e01c286d8fe330",
          "usernameLower": "48ro01688",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "5e1b239ed4bbbbc8b28cc1c8d699f77a",
          "groupId": "5ee8ce900696c96d3f170072",
          "usernameLower": "33wy33",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "bf2bd0272fbc5be6b52cdfef65210b61",
          "groupId": "5eec496848046804d8e0f364",
          "usernameLower": "38ko38",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "d267af7d997ac3abc435b56c046fa404",
          "groupId": "5eec58229f5bde04c4d1ec4d",
          "usernameLower": "33ga33",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "812e18a0332d85d3ae281a8dfe1b7564",
          "groupId": "5eec460405e7ee77208bc537",
          "usernameLower": "22zm22",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "55ee91c676b66c3a54f5026caf7d38b7",
          "groupId": "5eef6eb548c89c058b72dd3e",
          "usernameLower": "91og.v1",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "CSR",
          "key": "8b653c15fb0d5e1fd69d6980dc4ae89c",
          "groupId": "5eef77db69aba01b41789038",
          "usernameLower": "98wl01",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "BET123",
          "key": "3f3395f64f4218844ee4614ee48cf33e",
          "groupId": "5d1cd92fedbf374a01ff5969",
          "usernameLower": "sexy",
          "isActive": true,
          "price": 10000
      },
      {
        "client": "BET123",
        "key": "16db7247808f75e8fc07af9aa4a39900",
        "groupId": "5ea2a59813cdb474373153b5",
        "usernameLower": "aff",
        "isActive": true,
        "price": 10000
      },
      {
        "client": "BET123",
        "key": "fe3e20789aeca23cc0216b66e92f0e84",
        "groupId": "5ea14c5ccdf5513d709ea0d5",
        "usernameLower": "abc",
        "isActive": true,
        "price": 10000
      },
      {
        "client": "BET123",
        "key": "f998585fcec1a7ff67e443421b076b43",
        "groupId": "5e5cef12d2d5f61218b4d161",
        "usernameLower": "ver",
        "isActive": true,
        "price": 10000
      },
      {
        "client": "BET123",
        "key": "660d6280bf1c31e839a34b470396097f",
        "groupId": "5e5be4782aed1712039235a5",
        "usernameLower": "ok",
        "isActive": true,
        "price": 10000
      },
      {
        "client": "BET123",
        "key": "4b744d0f5baaf038171f6a763c5669cf",
        "groupId": "5e5bdd132aed17120392330b",
        "usernameLower": "aba",
        "isActive": true,
        "price": 10000
      },
      {
        "client": "BET123",
        "key": "78bdb8d6b77db184ee7189ef4310c3ad",
        "groupId": "5e5bd37650261211ed7339fe",
        "usernameLower": "68gd11",
        "isActive": true,
        "price": 5000
      },
      {
        "client": "BET123",
        "key": "4b6f6828896583bbf84914b07040e5ac",
        "groupId": "5e89a153b8df50432c510dc8",
        "usernameLower": "over",
        "isActive": true,
        "price": 10000
      },
      {
        "client": "BET123",
        "key": "2d52ff7d5d3c0ff67f579b8ade713871",
        "groupId": "5e673ff28339c77ce2c6f01e",
        "usernameLower": "46me91",
        "isActive": true,
        "price": 5000
      },
      {
        "client": "BET123",
        "key": "cf9ff0a248a37429b19fcdcfde47abab",
        "groupId": "5e5bd8d99b95fc7f0455130b",
        "usernameLower": "hot",
        "isActive": true,
        "price": 10000
      },
      {
        "client": "BET123",
        "key": "12339382d263b872b2843dac8f010638",
        "groupId": "5e5c7a8fd2d5f61218b4b2f2",
        "usernameLower": "twd",
        "isActive": true,
        "price": 10000
      },
      {
          "client": "BET123",
          "key": "53153c859f3515afc73790deeba91a01",
          "groupId": "5d065eabd712e9144b32567e",
          "usernameLower": "goal",
          "isActive": false,
          "price": 0,
          "remark": "Canceled"
      },
      {
        "client": "BET123",
        "key": "80c02cb3ef8a8ec58dfe46133e57c7c5",
        "groupId": "5e942f59dfa9294800e3fc5a",
        "usernameLower": "up",
        "isActive": true,
        "price": 10000
      },
      {
          "client": "BET123",
          "key": "0aabd9f138c2f984afdb9516d50d1160",
          "groupId": "5d08f82d323cd91e4fc8271b",
          "usernameLower": "66czz",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "BET123",
          "key": "531e53b671135af5515a5ba136653a01",
          "groupId": "5d2752be55721058fb0030ce",
          "usernameLower": "faz",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "3ccb8df42fda7324147f785115b4b77b",
          "groupId": "5e6b57e38243ba44d182d553",
          "usernameLower": "bet",
          "isActive": true,
          "price": 10000,

      },
      {
          "client": "BET123",
          "key": "67429e6ab98e97ea1ae10a480117125c",
          "groupId": "5d10daf5f08f952676450360",
          "usernameLower": "twin",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "64d4016bc0c0e4f6747c37a07f892b1c",
          "groupId": "5d56b2a7dae1366736566203",
          "usernameLower": "56ddzlgce",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "BET123",
          "key": "6ccc3481e1040ae9e75366f0c6582f92",
          "groupId": "5d5be63a2a776367296cefb6",
          "usernameLower": "48io89aa",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "BET123",
          "key": "a74c852cce171de7cbf353328e792f5c",
          "groupId": "5d581e2052f0890f87a6fc2b",
          "usernameLower": "22xkk",
          "isActive": false,
          "price": 0,
          "remark": "Canceled"
      },
      {
          "client": "BET123",
          "key": "8f11e21c65e1eff8e63582117c90e246",
          "groupId": "5d5d0e2645f185671986d889",
          "usernameLower": "live",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "0baa98e07d41ac7dfc65571bb1435bb5",
          "groupId": "5d5dac3b52f0890f87a7a6c7",
          "usernameLower": "ltd",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "ad959a0b8b64f8df39cddaa8727dd89f",
          "groupId": "5d4d539652bd9b1afe030b93",
          "usernameLower": "max",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "4270063dd4af33c4c18dd86e60ddff80",
          "groupId": "5d80dd1928b45743da5d90c5",
          "usernameLower": "22gma",
          "isActive": false,
          "price": 0,
          "remark": "Canceled"
      },
      {
          "client": "BET123",
          "key": "986cb1d31486b3939799dabe4676763e",
          "groupId": "5d6e7198e1264d3e3fc3466c",
          "usernameLower": "66lla",
          "isActive": true,
          "price": 5000
      },
      {
        "client": "BET123",
        "key": "44ac0d313328995ed68d1ff926923216",
        "groupId": "5e5b6eec4e3ce011f6182384",
        "usernameLower": "cup",
        "isActive": true,
        "price": 10000
      },
      {
          "client": "BET123",
          "key": "a4bbd25bf901fe9b515f339ca1b76a32",
          "groupId": "5d74759bc0627227a98d7734",
          "usernameLower": "22dxd",
          "isActive": false,
          "price": 0,
          "remark": "Canceled"
      },
      {
          "client": "BET123",
          "key": "f7c99fb5f271c6e950ff22406af1cdf0",
          "groupId": "5d7e14c668cb2733df50f18f",
          "usernameLower": "44fsee",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "BET123",
          "key": "bc154cc07766fe0af63f989b7a0f8d4d",
          "groupId": "5d80ce197e5d79642e6e2349",
          "usernameLower": "pro",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "eba59748f5acaa071f2d903d69c93e93",
          "groupId": "5d85ae864421f769f89ced22",
          "usernameLower": "vip",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "7ce81f508cce342e15f4843dde923015",
          "groupId": "5d90ebb64d9f830402481308",
          "usernameLower": "48iomc",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "BET123",
          "key": "09538d4b549967295abd04ed64c554ba",
          "groupId": "5dd3a4b51388844d5fc3515f",
          "usernameLower": "cash",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "64c4c7cc30c20ffa01ce5156c3310e3a",
          "groupId": "5d91f90341960f6112533a65",
          "usernameLower": "35rff",
          "isActive": false,
          "price": 0,
          "remark": "Canceled"
      },
      {
        "client": "BET123",
        "key": "bdecf362e15491e6b621604c1c8a5ed5",
        "groupId": "5e6b538c72bf6f44e751a2bf",
        "usernameLower": "gt",
        "isActive": true,
        "price": 10000
      },
      {
          "client": "BET123",
          "key": "6d91e2dadeb432022e11597274a9ebd3",
          "groupId": "5d946bdf551ea10bbe452a5b",
          "usernameLower": "89ke1",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "BET123",
          "key": "6c8ec75dc96dc756f4583374300aaf9c",
          "groupId": "5d8f36fb39093677fb913c0a",
          "usernameLower": "22gda",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "BET123",
          "key": "95c358525ccf15a841145a35cc7f9ab1",
          "groupId": "5da00c38f6b8897dd27168fe",
          "usernameLower": "88wcth",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "BET123",
          "key": "4e3cd4cb455762bdb65f46cf80ec2738",
          "groupId": "5da16a91f8107e251626ca85",
          "usernameLower": "fox",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "b34e1ffcf8295ebb65239667c67a0d5f",
          "groupId": "5da2d2de11f6f87d871d0f05",
          "usernameLower": "66aum",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "BET123",
          "key": "4fd4eadf8bc4317752f88436a04e1ab4",
          "groupId": "5d8e180e1a2655557f4041f0",
          "usernameLower": "88ktc",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "BET123",
          "key": "994b77d23d92b5a02fcf3a4eb5272cc2",
          "groupId": "5da7d89de4e8690a5d583b05",
          "usernameLower": "vvip",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "51a8da652ea4c2b7939c13909b12da3d",
          "groupId": "5db2bd85562ceb4102766157",
          "usernameLower": "sbo",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "c5e9970d197062090e51a91fc8e27b70",
          "groupId": "5db595440cd8b31cba5ede86",
          "usernameLower": "54ay11",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "BET123",
          "key": "4fcab155b4994a0580e06f4558e09f2b",
          "groupId": "5db7bf705f6cb94fab7d7e4c",
          "usernameLower": "vega",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "8a7ec4efb6d2d9e5cb60e1005eaeb463",
          "groupId": "5d980522d4dbb36f59957d43",
          "usernameLower": "king",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "2b5c2603a332b6baeb18a843448862b2",
          "groupId": "5db96d3366a2b82b1645c729",
          "usernameLower": "royal",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "c05116a7484b5c90d470122c7b837df4",
          "groupId": "5d96ef29be960a0fbec9ab3c",
          "usernameLower": "pm",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "3c5a3f2d092a2c8dbbd69cd6b45c5f44",
          "groupId": "5dd95829a42b1f658446e23b",
          "usernameLower": "84bk188",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "BET123",
          "key": "52e8625e7c77c958de140624a80925fe",
          "groupId": "5db9705e985e9f4f8f691844",
          "usernameLower": "play",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "da0ce3fa09f509484debd8c563feaeab",
          "groupId": "5d552d58d870400f72a803ba",
          "usernameLower": "58gfs2",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "BET123",
          "key": "7288c72b544c9bcb6d8c1028c5b8620a",
          "groupId": "5dc7d9eae77b9c68e93d35a0",
          "usernameLower": "88ktb",
          "isActive": false,
          "price": 0,
          "remark": "Canceled"
      },
      {
          "client": "BET123",
          "key": "113a5f243fbba5273b1e939a1a279c0a",
          "groupId": "5dd22daeaa107d729e268197",
          "usernameLower": "lucky",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "0760e33bc8cb0932c1c60ec22dea624d",
          "groupId": "5dc42e4e1b6a3568ffac4418",
          "usernameLower": "mjm",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "6b6f365df499e660076a5010cad764ad",
          "groupId": "5de798adfa39a77bd8fb02b0",
          "usernameLower": "88ktab",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "BET123",
          "key": "a03d1582d3d9bb4768fbe186fce1feba",
          "groupId": "5de7451a19c1777bc6ae6a39",
          "usernameLower": "kirin",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "5ebad3ac60d5d225a56316611d28b112",
          "groupId": "5dc1355501c34c604cdb8a8d",
          "usernameLower": "best",
          "isActive": false,
          "price": 0,
          "remark": "Canceled"
      },
      {
          "client": "BET123",
          "key": "9f9fbb9c0ebbb10a8cfc6ee30498c81a",
          "groupId": "5deca526f8ab130be0bb2603",
          "usernameLower": "ap",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "179efba9f0b88f05b4a5996b2d5517c8",
          "groupId": "5decfd51da87ae32e720b671",
          "usernameLower": "rpm",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "55439c079dd7569e925822311bb21a65",
          "groupId": "5dec90b143c3e80ba631bfe5",
          "usernameLower": "22dxx",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "BET123",
          "key": "592d258a7fa915fab8d5e44aeada870b",
          "groupId": "5ded9fbbda87ae32e721864f",
          "usernameLower": "four",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "ee42ed28a088add5621efedc948c88f4",
          "groupId": "5ded15613bc9240b90248d71",
          "usernameLower": "kick",
          "isActive": false,
          "price": 0,
          "remark": "Canceled"
      },
      {
          "client": "BET123",
          "key": "9d27c44c0e64145983b6dd8c028ba3a4",
          "groupId": "5dcac526aa107d729e20e97d",
          "usernameLower": "gtr",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "a475f282734fd9f20df518095add86a3",
          "groupId": "5e154f85d30ca76d7d44429b",
          "usernameLower": "46me81",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "BET123",
          "key": "38fc864ea8acdb8b0c9eb778d08b9b27",
          "groupId": "5e183f699d65ec1e7a7f0b2c",
          "usernameLower": "money",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "58539dedcdf74763434be8a793e65594",
          "groupId": "5e4151f0db47664ec1ed3c68",
          "usernameLower": "star",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "cda257be1179bf96819a9bcc7f3bae94",
          "groupId": "5e1853cb9d65ec1e7a7f20cf",
          "usernameLower": "gan",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "36f7d9042a8170113fa67261671becb6",
          "groupId": "5e6c94f6ec83e67cd20e7f01",
          "usernameLower": "ppp",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "0f6f8176f75dde5e2d93b4b25d7060bf",
          "groupId": "5e8612210f850a3dc423765a",
          "usernameLower": "auto",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "e25b56b2a0cd4bfd27be600627483b69",
          "groupId": "5e7b652bdefb4827fcf641b0",
          "usernameLower": "wip",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "ac3704a554fe19313d8751ef9fb3f35c",
          "groupId": "5e7b69954d2cda284063e35f",
          "usernameLower": "leo",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "37cfce7a6a71f2a5b6131fba630613bc",
          "groupId": "5e8f36468a44c6481a22f6e2",
          "usernameLower": "us",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "6fe8cc17aa6e82a1c5fdade0771b2792",
          "groupId": "5eae7a7a9d123628a9fc9f60",
          "usernameLower": "dd",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "5390a3f58b50f17ad53c5db5daea4406",
          "groupId": "5eae7bb78c1c850b4ea7b8c7",
          "usernameLower": "pro1",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "adfa450474edf71eb31dcffd2bca2f70",
          "groupId": "5eaad8b20f470a5c6a2425b3",
          "usernameLower": "yes",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "f0f6444ea30046150337d2812255ba1b",
          "groupId": "5ea84b984cff1a083dc546d1",
          "usernameLower": "utm",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "d17f2317bb3b7c21c714d9378bc286dc",
          "groupId": "5eb251c27526ba32e0348f65",
          "usernameLower": "at",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET123",
          "key": "586b6b8d025702c9d23a790b7b75eba4",
          "groupId": "5efca2dbb49772381dd75454",
          "usernameLower": "kfc",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "GOAL168",
          "key": "e36575e1d98f93aca9152dde7d97cd64",
          "groupId": "5c07e90959a7c975972f3dc6",
          "usernameLower": "44web",
          "isActive": false,
          "price": 0,
          "remark": "Canceled"
      },
      {
          "client": "BSBBET",
          "key": "6e912c29495fbfb675c93c3e25b44116",
          "groupId": "5d668585eb29f7491e9cfd45",
          "usernameLower": "04jh1881",
          "isActive": true,
          "price": 0,
          "remark": "Promotion free"
      },
      {
          "client": "BSBBET",
          "key": "5a9f1048674c66869baf18c36317fdca",
          "groupId": "5d4549d5bc7b660977bfd3dd",
          "usernameLower": "93sv",
          "isActive": false,
          "price": 0,
          "remark": "Canceled"
      },
      {
          "client": "BSBBET",
          "key": "36586e6c4d84fc38e97a5538596d73e1",
          "groupId": "5da4b1540ae23542953f3072",
          "usernameLower": "98lf02",
          "isActive": false,
          "price": 0,
          "remark": "Canceled"
      },
      {
          "client": "BSBBET",
          "key": "0a66835294d5d67ed804bda9015bf53c",
          "groupId": "5d9053c7d5502513e1b1b76f",
          "usernameLower": "98lf11",
          "isActive": false,
          "price": 0,
          "remark": "Canceled"
      },
      {
          "client": "BSBBET",
          "key": "ab7701edf9c75c7a357fbc5ec2701edf",
          "groupId": "5eeb950fe6b9b0630c5ffe0c",
          "usernameLower": "98lfwin",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "WINBET",
          "key": "032a32018bba5cb049b0e2af62b4375e",
          "groupId": "5da554425506b062da5a63a7",
          "usernameLower": "82duapi0001",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "WINBET",
          "key": "e70525a2f40a905c73a9118122133230",
          "groupId": "5ddbaa1fda396a3c171fcd72",
          "usernameLower": "winbet",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "WINBET",
          "key": "bb4dd6cf5351f4d22764b5c36a8a8725",
          "groupId": "5ec61d7a4890217cf018836c",
          "usernameLower": "45cykl05",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "AMB_SPORTBOOK",
          "key": "aefe1dd30ced503fb0b435552a36ec5b",
          "groupId": "5aa27828ea560cd64059ffae",
          "usernameLower": "45lj",
          "isActive": false,
          "price": 0,
          "remark": "Canceled"
      },
      {
          "client": "AMB_SPORTBOOK",
          "key": "960f12239ddb1d8d5fa8f7f71b8a2aae",
          "groupId": "5e744afa1c344c3d623a1c36",
          "usernameLower": "68sos",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "AMB_SPORTBOOK",
          "key": "3461be0253a1fd7a7a4ab19f103a723b",
          "groupId": "5e9ee5cf42721840b88a3b1c",
          "usernameLower": "88sf",
          "isActive": false,
          "price":0,
          "remark": "Canceled"
      },
      {
          "client": "AMB_SPORTBOOK",
          "key": "8eacc92fa165b2d7179aa044c5305143",
          "groupId": "5ee617330f1dcd7ff61f8a19",
          "usernameLower": "55xtsa",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "MVPATM168",
          "key": "fd0304b3f654dea64bde2953f83fbcca",
          "groupId": "5e5e281759dc45103a715af0",
          "usernameLower": "mvp",
          "isActive": true,
          "price": 0,
          "remark": "Promotion free"
      },
      {
          "client": "MVPATM168",
          "key": "d65af5fcbf7f10d59febf9c25b85639d",
          "groupId": "5e7206b398c1284be7761c4b",
          "usernameLower": "69hhh",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "MVPATM168",
          "key": "3d34c9a6418bbc8b57d1fb942fb2437f",
          "groupId": "5e749347a70cf458f4c67929",
          "usernameLower": "86nnn",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "MGM",
          "key": "06ad86c42afadcf1a7e918f9ab120dd9_",
          "groupId": "5ca6f52ee2bc3b1acdef8a5c",
          "usernameLower": "mgm",
          "isActive": true,
          "price": 0,
          "remark": "Promotion free"
      },
      {
          "client": "MGM",
          "key": "e552f5ef708c54df232c61982378e4e3_",
          "groupId": "5d722ec9c624137887463e63",
          "usernameLower": "tt",
          "isActive": true,
          "price": 0,
          "remark": "Promotion free"
      },
      {
        "client": "TT69BET",
        "key": "1baa237d266460724eeb41ce8201468b",
        "groupId": "5e6f0ceeb6e670242d9987e9",
        "usernameLower": "tam",
        "isActive": true,
        "price": 0,
        "remark": "Promotion free AMB Auto"
      },
      {
        "client": "IRON",
        "key": "c3d051e1962a52712c4076a68fe639ee",
        "groupId": "5d861e9786abcb77c1a916a6",
        "usernameLower": "88ut",
        "isActive": true,
        "price": 5000
      },
      {
        "client": "IWANTBET",
          "key": "b1fe0305e08e37003cd7bad0d7709e35",
          "groupId": "5e70ec79fe9981708b5ac5f7",
          "usernameLower": "04teywb",
          "isActive": true,
          "price": 5000,
          "remark": "Promotion free AMB Auto"
      },
      {
        "client": "SAND333",
        "key": "c1a3d2205c61d28c73851da9efb27ee6",
        "groupId": "5e649234a4b3d80fa2d6cefa",
        "usernameLower": "74fw2a",
        "isActive": true,
        "price": 5000
      },
      {
          "client": "SAND333",
          "key": "66150e4da2d116a7a1516d0f35d716fa",
          "groupId": "5efec07541955454a7c34318",
          "usernameLower": "37pmx",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "SAND333",
          "key": "caa4e249c13503c9fe1ee377b3088e48",
          "groupId": "5edef1221a1d5f0d85d747ea",
          "usernameLower": "12qc03168",
          "isActive": true,
          "price": 5000
      },
      {
        "client": "MBK168",
        "key": "778388071739e809ea741e50a9684aac",
        "groupId": "5e7347df100d1b552a0a42e5",
        "usernameLower": "mbn",
        "isActive": true,
        "price": 0,
          "remark": "Promotion free AMB Auto"
      },
      {
        "client": "MBK168",
        "key": "8dfe7fb9d03aca32ccb1a876980550e3",
        "groupId": "5ea9a1f9bfd62b71c8a51e5c",
        "usernameLower": "mbr",
        "isActive": true,
        "price": 0,
          "remark": "Promotion free AMB Auto"
      },
      {
        "client": "MBK168",
        "key": "b037eb24cc637fde7133ed434f569336",
        "groupId": "5ea9a0add1529271d5a9f12e",
        "usernameLower": "mba",
        "isActive": true,
        "price": 0,
          "remark": "Promotion free AMB Auto"
      },
      {
          "client": "MBK168",
          "key": "f8848929d5837f06c8d6e3061f95322d",
          "groupId": "5e92ca230b7af82c1e915e9a",
          "usernameLower": "60kqmbk66",
          "isActive": true,
          "price": 5000
      },
      {
        "client": "G2GBET",
        "key": "5f018a1001187d750de7cc1c25411d85",
        "groupId": "5e77a84346dc8c3612b9f6e3",
        "usernameLower": "88dda",
        "isActive": false,
        "price": 0,
        "remark": "Canceled"
      },
      {
          "client": "G2GBET",
          "key": "3e4a30870f38e6f3d9c7711246c1126a_",
          "groupId": "5e6c9d6d0a00c920c6186203",
          "usernameLower": "77xrx",
          "isActive": false,
          "price": 0,
          "remark": "Canceled"
      },
      {
          "client": "G2GBET",
          "key": "eb9f302ecc40a4243c2e19f293121e6b_",
          "groupId": "5e7d8b668f93c44144cc7092",
          "usernameLower": "44gr1",
          "isActive": false,
          "price": 0,
          "remark": "Canceled"
      },
      {
          "client": "G2GBET",
          "key": "47f894f81c5ad68d6572a4e537e7f10c_",
          "groupId": "5e7dce89a58ddf413eddfdc9",
          "usernameLower": "88vv",
          "isActive": false,
          "price": 0,
          "remark": "Canceled"
      },
      {
          "client": "G2GBET",
          "key": "3eb3415f25f6cca63b341bde14007408",
          "groupId": "5e7df4e86eb012460af0eb98",
          "usernameLower": "41lvz",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "G2GBET",
          "key": "0950aa4ac2bc782ef846f5edef19ec58",
          "groupId": "5e7cb0e7f89e55415202caf2",
          "usernameLower": "01xgg",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "G2GBET",
          "key": "84b5e174a89a527e8f9b00fcd3e48066",
          "groupId": "5e85cc0ce3e93e536275433d",
          "usernameLower": "88ddd",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "G2GBET",
          "key": "490c9ba564c2694fbe3a7cbbf3843f6a",
          "groupId": "5e6b3ca2f23b5a18efb5ae23",
          "usernameLower": "88lx",
          "isActive": false,
          "price": 0,
          "remark": "Canceled"
      },
      {
          "client": "AFC1688",
          "key": "988be591f2d1583fbec6c55cec210f49",
          "groupId": "5e327716f821871a49dd5059",
          "usernameLower": "31lq",
          "isActive": true,
          "price": 5000
      },
      {
        "client": "HENG168BET",
        "key": "97df095151a941598865db2b1ea68c45",
        "groupId": "5ea16790bb948a35d2b3b750",
        "usernameLower": "vip",
        "isActive": true,
        "price": 10000
      },
      {
        "client": "UKINGBET",
        "key": "b4d228f3ca3ea32f6fbedade78597c53",
        "groupId": "5ead6c177f5bda6136602184",
        "usernameLower": "uking",
        "isActive": true,
        "price": 10000
      },
      {
          "client": "AMGOAL",
          "key": "f367bca3130aa471bc8de4820ae1e28e",
          "groupId": "5eb039ab69cfd6535d65a3f0",
          "usernameLower": "44zza",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "AMGOAL",
          "key": "c03ebb6ddb39d8179571e2f49459d41d",
          "groupId": "5ea81723d547f03e551cd2b3",
          "usernameLower": "goal",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "AMGOAL",
          "key": "5af846553b60365e2236bc0f9195481f",
          "groupId": "5eb6ba871567515a8ebca050",
          "usernameLower": "am",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "AMGOAL",
          "key": "cc660a699c6fe3058dc7be2452e5dafa",
          "groupId": "5ec76f4c93fa0a5b82bebc5d",
          "usernameLower": "bw",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "NAZABET",
          "key": "f48d57b13e148828dcc7d606857afcf1",
          "groupId": "5ec00c1933ffd454c1bb398f",
          "usernameLower": "88uk",
          "isActive": false,
          "price": 0,
          "remark": "Canceled"
      },
      {
          "client": "NAZABET",
          "key": "eb2baa7a50080ab419c9cb926371852e",
          "groupId": "5ecbaeb033ffd454c1bb9cae",
          "usernameLower": "88uk01z",
          "isActive": true,
          "price": 5000,
      },
      {
          "client": "FAST98",
          "key": "2236e99d65a3b4c776dfd41e4179c2ae",
          "groupId": "5ec340b7801d694635e5e75f",
          "usernameLower": "66smauto",
          "isActive": true,
          "price": 5000
      },
      {
          "client": "BET789",
          "key": "0260ea5099ec634924c5ee94ddb78885",
          "groupId": "5ed0c18d2c20ec40f3e2493c",
          "usernameLower": "88swat",
          "isActive": false,
          "price": 0,
          "remark": "Canceled"
      },
      {
          "client": "BET789",
          "key": "77cf5fa3864104835df47c9e23b4a89c",
          "groupId": "5ee1e7d0ec9c79655f68f315",
          "usernameLower": "bet",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "BET789",
          "key": "e3811204c6aa40e6a59d3f5d2bb9c9d6",
          "groupId": "5ef9d9d8bb6ee52018efe5d4",
          "usernameLower": "vip",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "VRCBET",
          "key": "0b93a1682f1732d2f3a5f37ffeaa2d04",
          "groupId": "5ef99dc08ae1db40e9ff7be4",
          "usernameLower": "vmax",
          "isActive": true,
          "price": 10000
      },
      {
          "client": "IPROBET",
          "key": "fed233875ae42979e868e2c0fa796283",
          "groupId": "5ddbff880ef00d2ba8f920bd",
          "usernameLower": "hk",
          "isActive": true,
          "price": 0,
          "remark": "Promotion free"
      },
      {
          "client": "IPROBET",
          "key": "5d3198542bef8b0fbfbfd4e67749288e",
          "groupId": "5e4116a693e0ef73d634c5b3",
          "usernameLower": "dd",
          "isActive": true,
          "price": 0,
          "remark": "Promotion free"
      },
      {
          "client": "IPROBET",
          "key": "f3fbf647ae8d9c6c5db93b1b631288a2",
          "groupId": "5e5f72980d9f6429cd9952e4",
          "usernameLower": "bt",
          "isActive": true,
          "price": 0,
          "remark": "Promotion free"
      },
      {
          "client": "IPROBET",
          "key": "92873f06068cc431d8b81f0bee368708",
          "groupId": "5ed0e5b9b9fe0a1bb61dc848",
          "usernameLower": "aap",
          "isActive": true,
          "price": 0,
          "remark": "Promotion free"
      }
  ]
}

module.exports = {
  data
};