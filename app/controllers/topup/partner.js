const express               = require('express');
const router                = express.Router();
const DATA_LIST             = require('./dataList').data();
const CLIENT_NAME           = process.env.CLIENT_NAME || 'SPORTBOOK88';
const _                     = require('underscore');
const MemberModel           = require('../../models/member.model');
const AgentGroupModel       = require('../../models/agentGroup.model');
const CashTransactionModel  = require('../../models/cashTransaction.model');
const CreditModifyHistoryModel = require('../../models/creditModifyHistory.model');
const RedisService          = require('./redisTopup');
const mongoose              = require('mongoose');
const roundTo               = require('round-to');
const waterfall             = require("async/waterfall");
const parallel              = require("async/parallel");
const CashService           = require('../../common/cashService');
const md5                   = require('md5');
const SeqTopupModel         = require('../../models/seqTopup.model');
const crypto                = require('crypto');
const ReportService         = require('../../common/reportService');
const naturalSort           = require("javascript-natural-sort");

router.get('/credit/:key/:memberId', (req, res) => {
    const {
        key,
        memberId
    } = req.params;
    const dataObj = _.findWhere(DATA_LIST, JSON.parse(`{"client":"${CLIENT_NAME}","key":"${key}"}`));
    if (_.isUndefined(dataObj)) {
        return res.send(
            {
                code: 8000001,
                message: `Key[${key}] is incorrect.`
            }
        );
    } else {
        const {
            groupId
        } = dataObj;
        const redisKey = `GET_CREDIT_${CLIENT_NAME}_${groupId}_${memberId}`;
        RedisService.getByKey(redisKey, (error, response) => {
            if (_.isNull(response)) {
                MemberModel.aggregate([
                    {
                        $match: {
                            $or: [
                                {
                                    group: mongoose.Types.ObjectId(groupId),
                                    loginName_lower: memberId.toLowerCase()
                                },
                                {
                                    group: mongoose.Types.ObjectId(groupId),
                                    username_lower: memberId.toLowerCase()
                                },
                                {
                                    group: mongoose.Types.ObjectId(groupId),
                                    username: memberId.toLowerCase()
                                }
                            ]
                        }
                    },
                    {
                        $project: {
                            credit:  {$toDouble:{$add: [{$toDecimal:'$creditLimit'}, {$toDecimal:'$balance'}]}},
                        }
                    },
                ], (err, results) => {
                    if (err) {
                        return res.send({
                            result: {
                                credit : 0
                            },
                            code: 0
                        });
                    } else {
                        if (_.isEmpty(results)) {
                            return res.send({message: "DATA NOT FOUND", code: 999});
                        } else {
                            let credit = roundTo(results[0].credit, 2);
                            if (typeof credit !== 'number' || _.isUndefined(credit) || _.isNaN(credit)) {
                                RedisService.setByKeyTTL(redisKey, {
                                    result: {
                                        credit : 0
                                    },
                                    code: 0
                                }, (error, result) => {

                                });
                                return res.send({
                                    result: {
                                        credit : 0
                                    },
                                    code: 0
                                });
                            } else {
                                RedisService.setByKeyTTL(redisKey, {
                                    result: {
                                        credit
                                    },
                                    code: 0
                                }, (error, result) => {

                                });
                                return res.send({
                                    result: {
                                        credit
                                    },
                                    code: 0
                                });
                            }
                        }
                    }
                });

            } else {
                return res.send(response);
            }
        });
    }
});

router.get('/winLose/:key/:memberId/:ref', (req, res) => {
    const {
        key,
        memberId,
        ref
    } = req.params;
    console.log(ref);
    const dataObj = _.findWhere(DATA_LIST, JSON.parse(`{"client":"${CLIENT_NAME}","key":"${key}"}`));
    if (_.isUndefined(dataObj)) {
        return res.send(
            {
                code: 8000001,
                message: `Key[${key}] is incorrect.`
            }
        );
    } else {
        const {
            groupId
        } = dataObj;
        waterfall([
                (callback) => {
                    AgentGroupModel.findOne({
                        _id: mongoose.Types.ObjectId(groupId),
                        isAutoTopUp: true,
                        active: true
                    })
                        .lean()
                        .exec((error, data) => {
                            if (error) {
                                callback(error, null);
                            } else {
                                callback(null, data);
                            }
                        });
                }
            ],
            (error, result) => {
                if (error) {
                    return res.send({
                        code: 9999,
                        message: error
                    });
                } else {
                    if (_.isUndefined(result) || _.isNull(result)) {
                        return res.send({
                            code: 80000012,
                            message: `Agent are not allow please contact admin.`
                        });
                    } else {
                        const {
                            name_lower
                        } = result;
                        console.log(name_lower);
                        if (memberId.toLowerCase().startsWith(name_lower.toLowerCase())) {
                            console.log(memberId.toLowerCase());
                            console.log(ref);
                            CashTransactionModel.findOne({
                                member_name: memberId.toLowerCase(),
                                ref: ref
                            })
                                .lean()
                                .exec((error, data) => {
                                    if (error) {
                                        return res.send({
                                            message: "error",
                                            result: error,
                                            code: 999
                                        });
                                    } else {
                                        if (data) {
                                            CashService.getWinLoseReportMember(data.memberId, data.processDateTime, (error, wlResponse) => {
                                                if (error) {
                                                    return res.send({
                                                        code: 999,
                                                        message: "Internal server error[CashService getWinLoseReportMember]"
                                                    });
                                                } else {
                                                    return res.send(
                                                        {
                                                            code: 0,
                                                            message: "success",
                                                            result: {
                                                                username: memberId,
                                                                ref: ref,
                                                                data: wlResponse
                                                            }
                                                        }
                                                    );

                                                }
                                            });
                                        } else {
                                            return res.send({
                                                code: 999,
                                                message: "Data Not Found"
                                            });
                                        }
                                    }
                                });
                        } else {
                            return res.send({
                                code: 80000012,
                                message: `Member are not allow please contact admin. [${memberId.toLowerCase()}]`
                            });
                        }
                    }
                }
            });
    }
});

router.post('/deposit/:key/:memberId', (req, res) => {
    let {
        key,
        memberId
    } = req.params;
    const dataObj = _.findWhere(DATA_LIST, JSON.parse(`{"client":"${CLIENT_NAME}","key":"${key}"}`));
    if (_.isUndefined(dataObj)) {
        return res.send(
            {
                code: 8000001,
                message: `Key[${key}] is incorrect.`
            }
        );
    } else {
        let {
            amount,
            signature,
            date
        } = req.body;
        const {
            groupId,
            usernameLower
        } = dataObj;
        console.log('groupId         => ', groupId);
        console.log('usernameLower   => ', usernameLower);
        console.log(amount);
        console.log(md5(amount + ":" + usernameLower));
        if ( !_.isEqual(signature, md5(`${amount}:${memberId}:${usernameLower}`))) {
            return res.send(
                {
                    code: 5000001,
                    message: `Signature[${signature}] is incorrect.`
                }
            );
        } else {
            let {
                amount
            } = req.body;

            let ref = 'PD' + new Date().getTime()+_.random(10000, 99999);
            console.log(date);
            if (!_.isUndefined(date)) {
                ref = `PD${memberId}${amount}${date}`.toUpperCase().replace(/\s/g, '').replace(/\//g, '').replace(/:/g, '').replace(/-/g, '');
            }
            console.log('memberId => ', memberId);
            console.log('amount   => ', amount);
            console.log('ref      => ', ref);
            CashTransactionModel.findOne({
                ref: ref
            })
                .lean()
                .exec((error, data) => {
                    if (error) {
                        return res.send({
                            message: "Error", result: error, code: 999
                        });
                    } else {
                        if (_.isNull(data)) {
                            parallel([
                                    //1
                                    (callback) => {
                                        AgentGroupModel.findOne({_id: mongoose.Types.ObjectId(groupId)})
                                            .lean()
                                            .exec((error, data) => {
                                                if (error) {
                                                    callback(error, null);
                                                } else {
                                                    callback(null, data);
                                                }
                                            });
                                    },

                                    //2
                                    (callback) => {
                                        parallel([
                                                (callback) => {
                                                    AgentGroupModel.aggregate([
                                                        {
                                                            $match: {
                                                                parentId: mongoose.Types.ObjectId(groupId)
                                                            }
                                                        },
                                                        {
                                                            $project: {_id: 0}
                                                        },
                                                        {
                                                            "$group": {
                                                                "_id": {
                                                                    id: '$_id'
                                                                },
                                                                "creditLimit": {
                                                                    $sum: '$creditLimit'
                                                                }
                                                            }
                                                        }
                                                    ], (error, data) => {
                                                        if (error) {
                                                            callback(error, null);
                                                        } else {
                                                            callback(null, !_.isUndefined(_.first(data)) ? _.first(data).creditLimit : 0);
                                                        }
                                                    });
                                                },

                                                (callback) => {
                                                    MemberModel.aggregate([
                                                        {
                                                            $match: {
                                                                group: mongoose.Types.ObjectId(groupId)
                                                            }
                                                        },
                                                        {
                                                            $project: {_id: 0}
                                                        },
                                                        {
                                                            "$group": {
                                                                "_id": {
                                                                    id: '$_id'
                                                                },
                                                                "creditLimit": {
                                                                    $sum: '$creditLimit'
                                                                }
                                                            }
                                                        }
                                                    ], (error, data) => {
                                                        if (error) {
                                                            callback(error, null);
                                                        } else {
                                                            callback(null, !_.isUndefined(_.first(data)) ? _.first(data).creditLimit : 0);
                                                        }
                                                    });
                                                },
                                            ],
                                            (error, parallelResult) => {
                                                if (error) {
                                                    callback(error, null);
                                                } else {
                                                    const [
                                                        agentGroupCreditLimit,
                                                        memberCreditLimit
                                                    ] = parallelResult;
                                                    callback(null, agentGroupCreditLimit + memberCreditLimit);
                                                }
                                            });
                                    },

                                    //3
                                    (callback) => {
                                        MemberModel.findOne({
                                            $or : [
                                                {
                                                    loginName_lower: memberId.toLowerCase()
                                                },
                                                {
                                                    username_lower: memberId.toLowerCase()
                                                },
                                                {
                                                    username: memberId.toLowerCase()
                                                }
                                            ]
                                        })
                                            .populate({
                                                path: 'group',
                                                select: '_id type name endpoint secretKey'
                                            })
                                            .select('')
                                            .exec((error, data) => {
                                                if (error) {
                                                    callback(error, null);
                                                } else {
                                                    callback(null, data);
                                                }
                                            });
                                    }],
                                (error, parallelResult) => {
                                    if (error) {
                                        return res.send({
                                            message: "Error", result: error, code: 999
                                        });
                                    } else {
                                        const [
                                            agentGroupModel,
                                            creditLimitUsage,
                                            memberModel
                                        ] = parallelResult;

                                        amount = Math.abs(amount);
                                        if (_.isNull(memberModel)) {
                                            return res.send({message: `Member[${memberId}] NOT FOUND`, code: 999});
                                        }
                                        // console.log('memberModel => ', JSON.stringify(memberModel));

                                        if (amount > (agentGroupModel.creditLimit - creditLimitUsage)) {
                                            return res.send({message: "creditLimit exceeded", code: 9000001});
                                        } else {
                                            if ((amount + memberModel.creditLimit) > agentGroupModel.maxCreditLimit) {
                                                return res.send({message: "creditLimit exceeded", code: 9000002});
                                            } else {
                                                MemberModel.findOneAndUpdate(
                                                    {
                                                        _id: mongoose.Types.ObjectId(memberModel._id)
                                                    },
                                                    {
                                                        $inc: {
                                                            creditLimit: amount
                                                        }
                                                    },
                                                    {
                                                        new: true
                                                    })
                                                    .exec((error, memberUpdateResponse) => {
                                                        if (error) {
                                                            return res.send({
                                                                message: "Error", result: error, code: 999
                                                            });
                                                        } else {
                                                            const historyBody = {
                                                                memberId: memberUpdateResponse._id,
                                                                type: 'ADD',
                                                                ref: ref,
                                                                beforeCredit: memberModel.creditLimit,
                                                                beforeBalance: memberModel.balance,
                                                                afterCredit: memberUpdateResponse.creditLimit,
                                                                afterBalance: memberUpdateResponse.balance,
                                                                amount: amount,
                                                                createdDate: new Date(new Date().getTime() + 1000 * 60 * 60 * 7)
                                                            };

                                                            const creditModifyModel = new CreditModifyHistoryModel(historyBody);
                                                            creditModifyModel.save((err, creditModifyResponse) => {
                                                                if (err) {
                                                                    return res.send({code: 0,message: "success",result: true});
                                                                }

                                                                let cashBody = {
                                                                    ref: ref,
                                                                    agent: groupId,
                                                                    memberId: memberUpdateResponse._id,
                                                                    member_name: memberUpdateResponse.username_lower,
                                                                    transferType: 'API_PARTNER',
                                                                    processDateTime: new Date(new Date().getTime() + 1000 * 60 * 60 * 7),
                                                                    amount: amount,
                                                                    status: 'COMPLETE',
                                                                    active: true,
                                                                    tranType: 'DEPOSIT',
                                                                    type: 'AUTO',
                                                                    createdDate: new Date(new Date().getTime() + 1000 * 60 * 60 * 7)
                                                                };

                                                                let cashModel = new CashTransactionModel(cashBody);
                                                                cashModel.save((err, HistoryResponse) => {

                                                                    if(err){
                                                                        return res.send({code: 999,message: "fail",result: true});
                                                                    }

                                                                    let body = {
                                                                        afterCredit: memberUpdateResponse.creditLimit,
                                                                        afterBalance: memberUpdateResponse.balance
                                                                    };

                                                                    CreditModifyHistoryModel.update(
                                                                        {
                                                                            _id: creditModifyResponse._id
                                                                        },
                                                                        body,
                                                                        (err, updateCreditRes) => {
                                                                            // callback(null, memberUpdateResponse)
                                                                        });

                                                                    return res.send(
                                                                        {
                                                                            code: 0,
                                                                            result: {
                                                                                before: roundTo(memberModel.creditLimit+memberModel.balance, 2),
                                                                                amount,
                                                                                after: roundTo(memberUpdateResponse.creditLimit+memberUpdateResponse.balance, 2),
                                                                                ref
                                                                            }
                                                                        }
                                                                    );
                                                                });
                                                            });
                                                        }
                                                    });
                                            }
                                        }
                                    }
                                });
                        } else {
                            return res.send({
                                message: "Ref. is duplicated  "+ ref, code: 900000
                            });
                        }
                    }
                });
        }
    }
});

router.post('/withdraw/:key/:memberId', (req, res) => {
    const {
        key,
        memberId
    } = req.params;

    const dataObj = _.findWhere(DATA_LIST, JSON.parse(`{"client":"${CLIENT_NAME}","key":"${key}"}`));
    if (_.isUndefined(dataObj)) {
        return res.send(
            {
                code: 8000001,
                message: `Key[${key}] is incorrect.`
            }
        );
    } else {
        let {
            amount,
            signature
        } = req.body;
        const {
            groupId,
            usernameLower
        } = dataObj;
        console.log('groupId         => ', groupId);
        console.log('usernameLower   => ', usernameLower);

        if (!_.isEqual(signature, md5(`${amount}:${memberId}:${usernameLower}`))) {
            return res.send(
                {
                    code: 5000001,
                    message: `Signature[${signature}] is incorrect.`
                }
            );
        } else {
            let {
                amount
            } = req.body;

            const ref = 'PW' + new Date().getTime()+_.random(10000, 99999);

            console.log('memberId => ', memberId);
            console.log('amount   => ', amount);
            console.log('ref      => ', ref);

            parallel([
                    //1
                    (callback) => {
                        AgentGroupModel.findOne({_id: mongoose.Types.ObjectId(groupId)})
                            .lean()
                            .exec((error, data) => {
                                if (error) {
                                    callback(error, null);
                                } else {
                                    callback(null, data);
                                }
                            });
                    },

                    //2
                    (callback) => {
                        parallel([
                                (callback) => {
                                    AgentGroupModel.aggregate([
                                        {
                                            $match: {
                                                parentId: mongoose.Types.ObjectId(groupId)
                                            }
                                        },
                                        {
                                            $project: {_id: 0}
                                        },
                                        {
                                            "$group": {
                                                "_id": {
                                                    id: '$_id'
                                                },
                                                "creditLimit": {
                                                    $sum: '$creditLimit'
                                                }
                                            }
                                        }
                                    ], (error, data) => {
                                        if (error) {
                                            callback(error, null);
                                        } else {
                                            callback(null, !_.isUndefined(_.first(data)) ? _.first(data).creditLimit : 0);
                                        }
                                    });
                                },

                                (callback) => {
                                    MemberModel.aggregate([
                                        {
                                            $match: {
                                                group: mongoose.Types.ObjectId(groupId)
                                            }
                                        },
                                        {
                                            $project: {_id: 0}
                                        },
                                        {
                                            "$group": {
                                                "_id": {
                                                    id: '$_id'
                                                },
                                                "creditLimit": {
                                                    $sum: '$creditLimit'
                                                }
                                            }
                                        }
                                    ], (error, data) => {
                                        if (error) {
                                            callback(error, null);
                                        } else {
                                            callback(null, !_.isUndefined(_.first(data)) ? _.first(data).creditLimit : 0);
                                        }
                                    });
                                },
                            ],
                            (error, parallelResult) => {
                                if (error) {
                                    callback(error, null);
                                } else {
                                    const [
                                        agentGroupCreditLimit,
                                        memberCreditLimit
                                    ] = parallelResult;
                                    callback(null, agentGroupCreditLimit + memberCreditLimit);
                                }
                            });
                    },

                    //3
                    (callback) => {
                        MemberModel.findOne({
                            $or : [
                                {
                                    loginName_lower: memberId.toLowerCase()
                                },
                                {
                                    username_lower: memberId.toLowerCase()
                                },
                                {
                                    username: memberId.toLowerCase()
                                }
                            ]
                        })
                            .populate({
                                path: 'group',
                                select: '_id type name endpoint secretKey'
                            })
                            .select('')
                            .exec((error, data) => {
                                if (error) {
                                    callback(error, null);
                                } else {
                                    callback(null, data);
                                }
                            });
                    }],
                (error, parallelResult) => {
                    if (error) {
                        return res.send({
                            message: "Error", result: error, code: 999
                        });
                    } else {
                        const [
                            agentGroupModel,
                            creditLimitUsage,
                            memberModel
                        ] = parallelResult;

                        amount = Math.abs(amount) * -1;
                        if (amount > (agentGroupModel.creditLimit - creditLimitUsage)) {
                            return res.send({message: "creditLimit exceeded", code: 9000001});
                        } else {
                            if ((amount + memberModel.creditLimit) > agentGroupModel.maxCreditLimit) {
                                return res.send({message: "creditLimit exceeded", code: 9000002});
                            } else {

                                const {
                                    creditLimit,
                                    balance
                                } = memberModel;

                                const totalWithdraw = (roundTo(creditLimit, 2) + roundTo(balance, 2));
                                const afterUpdateCreditLimit = roundTo(totalWithdraw, 2) + roundTo(amount, 2);
                                console.log('totalWithdraw          : '+totalWithdraw);
                                console.log('amount                 : '+amount);
                                console.log('afterUpdateCreditLimit : '+afterUpdateCreditLimit);
                                const isValid = 0 <= afterUpdateCreditLimit;
                                if (isValid) {

                                    MemberModel.findOneAndUpdate(
                                        {
                                            _id: mongoose.Types.ObjectId(memberModel._id)
                                        },
                                        {
                                            creditLimit: afterUpdateCreditLimit,
                                            balance: 0
                                        },
                                        {
                                            new: true
                                        })
                                        .exec((error, memberUpdateResponse) => {
                                            if (error) {
                                                return res.send({
                                                    message: "Error", result: error, code: 999
                                                });
                                            } else {

                                                const historyBody = {
                                                    memberId: memberUpdateResponse._id,
                                                    type: 'REDUCE',
                                                    ref: ref,
                                                    beforeCredit: memberModel.creditLimit,
                                                    beforeBalance: memberModel.balance,
                                                    afterCredit: memberUpdateResponse.creditLimit,
                                                    afterBalance: memberUpdateResponse.balance,
                                                    amount: Math.abs(amount),
                                                    createdDate: new Date(new Date().getTime() + 1000 * 60 * 60 * 7)
                                                };

                                                const creditModifyModel = new CreditModifyHistoryModel(historyBody);
                                                creditModifyModel.save((err, creditModifyResponse) => {
                                                    if (err) {
                                                        return res.send({code: 0,message: "success",result: true});
                                                    }

                                                    let cashBody = {
                                                        ref: ref,
                                                        agent: groupId,
                                                        memberId: memberUpdateResponse._id,
                                                        member_name: memberUpdateResponse.username_lower,
                                                        transferType: 'API_PARTNER',
                                                        processDateTime: new Date(new Date().getTime() + 1000 * 60 * 60 * 7),
                                                        amount: Math.abs(amount),
                                                        status: 'COMPLETE',
                                                        active: true,
                                                        tranType: 'WITHDRAW',
                                                        type: 'AUTO',
                                                        createdDate: new Date(new Date().getTime() + 1000 * 60 * 60 * 7)
                                                    };

                                                    let cashModel = new CashTransactionModel(cashBody);
                                                    cashModel.save((err, HistoryResponse) => {

                                                        if(err){
                                                            return res.send({code: 999,message: "fail",result: true});
                                                        }

                                                        let body = {
                                                            afterCredit: memberUpdateResponse.creditLimit,
                                                            afterBalance: memberUpdateResponse.balance
                                                        };

                                                        CreditModifyHistoryModel.update(
                                                            {
                                                                _id: creditModifyResponse._id
                                                            },
                                                            body, (err, updateCreditRes) => {
                                                                // callback(null, memberUpdateResponse)
                                                            });

                                                        return res.send(
                                                            {
                                                                code: 0,
                                                                result: {
                                                                    before: roundTo(memberModel.creditLimit+memberModel.balance, 2),
                                                                    amount,
                                                                    after: afterUpdateCreditLimit,
                                                                    ref
                                                                }
                                                            }
                                                        );
                                                    });
                                                });
                                            }
                                        });
                                } else {
                                    return res.send({
                                        message: `0 <= ${totalWithdraw} + ${amount} diff ${totalWithdraw+amount}`,
                                        code: 4002
                                    });
                                }
                            }
                        }
                    }
                });
        }
    }
});

router.put('/reset-password/:key/:memberId', (req, res) => {
    const {
        key,
        memberId
    } = req.params;

    const dataObj = _.findWhere(DATA_LIST, JSON.parse(`{"client":"${CLIENT_NAME}","key":"${key}"}`));
    if (_.isUndefined(dataObj)) {
        return res.send(
            {
                code: 8000001,
                message: `Key[${key}] is incorrect.`
            }
        );
    } else {
        let {
            password,
            signature
        } = req.body;

        const {
            groupId,
            usernameLower
        } = dataObj;
        console.log('groupId         => ', groupId);
        console.log('usernameLower   => ', usernameLower);

        if (!_.isEqual(signature, md5(`${password}:${usernameLower}`))) {
            return res.send(
                {
                    code: 5000001,
                    message: `Signature[${signature}] is incorrect.`
                }
            );
        } else {
            const hash = crypto.createHash('sha256').update(password+'').digest('base64');
            console.log('groupId  => ', groupId);
            console.log('memberId => ', memberId);
            console.log('hash     => ', hash);
            console.log(password);
            MemberModel.update({
                    $or : [
                        {
                            group: mongoose.Types.ObjectId(groupId),
                            loginName_lower: memberId.toLowerCase()
                        },
                        {
                            group: mongoose.Types.ObjectId(groupId),
                            username_lower: memberId.toLowerCase()
                        },
                        {
                            group: mongoose.Types.ObjectId(groupId),
                            username: memberId.toLowerCase()
                        }
                    ]
                },
                {
                    password: hash
                }, (error, data) => {
                    if (error) {
                        return res.send({message: "error", result: error, code: 999});
                    } else {
                        if (_.isUndefined(data)) {
                            return res.send({message: "DATA NOT FOUND", code: 8888});
                        } else {
                            return res.send({code: 0});
                        }
                    }
                });
        }
    }
});

router.post('/create/:key', (req, res) => {
    const key = req.params.key;
    const dataObj = _.findWhere(DATA_LIST, JSON.parse(`{"client":"${CLIENT_NAME}","key":"${key}"}`));
    if (_.isUndefined(dataObj)) {
        return res.send(
            {
                code: 8000001,
                message: `Key[${key}] is incorrect.`
            }
        );
    } else {
        let {
            memberLoginName,
            memberLoginPass,
            phoneNo,
            contact,
            signature
        } = req.body;
        const {
            groupId,
            usernameLower
        } = dataObj;
        console.log('groupId         => ', groupId);
        console.log('usernameLower   => ', usernameLower);
        console.log(req.body);
        if (!_.isEqual(signature, md5(`${memberLoginName}:${memberLoginPass}:${usernameLower}`))) {
            return res.send(
                {
                    code: 5000001,
                    message: `Signature[${signature}] is incorrect.`
                }
            );
        } else {
            console.log('memberLoginName => ', memberLoginName);
            console.log('memberLoginPass => ', memberLoginPass);
            console.log('phoneNo         => ', phoneNo);
            console.log('contact         => ', contact);

            waterfall([
                    (callback) => {
                        AgentGroupModel.findOne({
                            _id: mongoose.Types.ObjectId(groupId),
                            isAutoTopUp: true,
                            name_lower: usernameLower,
                            $or: [
                                {
                                    type: "AGENT"
                                },
                                {
                                    type: "SENIOR"
                                } ,
                                {
                                    type: "MASTER_AGENT"
                                }
                            ] ,
                            active: true
                        })
                            .lean()
                            .exec((error, data) => {
                                if (error) {
                                    callback(error, null);
                                } else {
                                    callback(null, data);
                                }
                            });
                    }
                ],
                (error, result) => {
                    if (error) {
                        return res.send({
                            code    : 9999,
                            message : error
                        });
                    } else {
                        if (_.isUndefined(result) || _.isNull(result)) {
                            return res.send({
                                code    : 80000012,
                                message : `Agent are not allow please contact admin. [${usernameLower}]`
                            });
                        } else {
                            const {
                                childMembers,
                                name
                            } = result;

                            const nameMock = `${name.toLowerCase()}template`;
                            const loginName = `${usernameLower.toLowerCase()}${memberLoginName}`;
                            console.log(nameMock);
                            console.log(loginName);
                            parallel([
                                    //1
                                    (callback) => {
                                        MemberModel.findOne({
                                            $or : [
                                                {
                                                    username_lower: nameMock.toLowerCase()
                                                },
                                                {
                                                    username: nameMock
                                                }
                                            ]
                                        })
                                            .lean()
                                            .exec((error, data) => {
                                                if (error) {
                                                    callback(error, null);
                                                } else {
                                                    callback(null, data);
                                                }
                                            });
                                    },

                                    //2
                                    (callback) => {
                                        MemberModel.findOne({
                                            $or : [
                                                {
                                                    loginName_lower: loginName.toLocaleLowerCase()
                                                },
                                                {
                                                    loginName: loginName
                                                }
                                            ]
                                        })
                                            .lean()
                                            .exec((error, data) => {
                                                if (error) {
                                                    callback(error, null);
                                                } else {
                                                    callback(null, data);
                                                }
                                            });
                                    }],
                                (error, parallelResult) => {
                                    if (error) {
                                        return res.send({
                                            message: "Error", result: error, code: 999
                                        });
                                    } else {
                                        const [
                                            memberModelMock,
                                            memberModelPrefixPhoneNo
                                        ] = parallelResult;
                                        if (_.isUndefined(memberModelMock) || _.isNull(memberModelMock)) {
                                            return res.send({
                                                code    : 80000013,
                                                message : `Please create user ${name}template first.`
                                            });
                                        } else if (!_.isUndefined(memberModelPrefixPhoneNo) && !_.isNull(memberModelPrefixPhoneNo)) {
                                            return res.send({
                                                code    : 80000014,
                                                message : `User ${usernameLower}${memberLoginName} was created`
                                            });
                                        } else {
                                            const model = memberModelMock;
                                            delete model._id;
                                            delete model.lastPaymentDate;

                                            model.balance = 0;
                                            model.creditLimit= 0;
                                            model.forceChangePassword = false;
                                            model.isAutoTopUp = true;

                                            const username = `${name}${memberLoginName}`;

                                            model.username = username;
                                            model.username_lower= username.toLowerCase();

                                            model.loginName= loginName.toUpperCase();
                                            model.loginName_lower= loginName.toLowerCase();

                                            model.createdDate = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);
                                            model.updatedDate = new Date(new Date().getTime() + 1000 * 60 * 60 * 7);

                                            model.password = crypto.createHash('sha256').update(memberLoginPass+'').digest('base64');

                                            model.contact = contact;
                                            model.phoneNo = phoneNo;
                                            model.currency = 'THB';

                                            const memberModel = new MemberModel(model);

                                            memberModel.save((error, memberResponse) => {
                                                if (error) {
                                                    return res.send({
                                                        message: "Error", result: error, code: 999
                                                    });
                                                } else {
                                                    AgentGroupModel.update(
                                                        {
                                                            _id: groupId
                                                        },
                                                        {
                                                            $push: {
                                                                childMembers: memberResponse._id
                                                            }
                                                        }, (error, data) => {
                                                            if (error) {
                                                                return res.send({
                                                                    message: "Error", result: error, code: 999
                                                                });
                                                            } else {
                                                                console.log('update parent child success');
                                                                return res.send(
                                                                    {
                                                                        code: 0,
                                                                        result: {
                                                                            username: memberResponse.username,
                                                                            loginName: memberResponse.loginName
                                                                        }
                                                                    }
                                                                );
                                                            }
                                                        });
                                                }
                                            });
                                        }
                                    }
                                });
                        }
                    }
                });
        }
    }
});

router.post('/report/agent/:agentName', async (req, res) => {
    const {
        agentName
    } = req.params;
    let {
        game,
        dateFrom,
        dateTo,
        timeFrom,
        timeTo,
        signature
    } = req.body;
    if(!agentName){
        return res.send({message: "agentId not found", code: 888});
    }
    if (!_.isEqual(signature, md5(`${agentName}:${CLIENT_NAME}`))) {
        return res.send(
            {
                code: 5000001,
                message: `Signature[${signature}] is incorrect.`
            }
        );
    } else {
        if (_.isUndefined(game)) {
            game = 'FOOTBALL,STEP,PARLAY,CASINO,GAME,LOTTO,M2';
        }
        waterfall([(callback => {

            AgentGroupModel.findOne({name_lower: agentName.toLowerCase()}, (err, response) => {
                if (err) {
                    callback(err, null);
                } else {
                    if (!response) {
                        callback(888, null);
                    } else {
                        callback(null, response);
                    }
                }
            }).select('_id type parentId')

        }), (groupInfo, callback) => {

            const agentGroup = groupInfo;

            if (agentGroup.type === 'SENIOR' || agentGroup.type === 'MASTER_AGENT' || agentGroup.type === 'AGENT') {


                ReportService.getWinLoseReportMember(agentGroup._id, agentGroup.type, dateFrom, dateTo, timeFrom, timeTo, game, (err, wlResponse) => {
                    if (err) {
                        callback(err, null);
                    } else {

                        MemberModel.populate(wlResponse, {
                            path: 'member',
                            select: '_id username username_lower contact'
                        }, function (err, memberResult) {
                            if (err) {
                                callback(err, null);
                            } else {
                                callback(null, agentGroup, memberResult);
                            }
                        });
                    }
                });
            } else {
                callback(null, agentGroup, []);
            }

        }], (err, agentGroup, transaction) => {

            if (err) {
                if (err == 888) {
                    return res.send({message: "agentId not found", code: 888});
                }
                return res.send({message: "error", result: err, code: 999});
            }

            let finalResult = transformDataByAgentType(agentGroup.type, transaction);


            finalResult = finalResult.sort(function (a, b) {
                let aKey = a.type === 'M_GROUP' ? a.member.username : a.group.name;
                let bKey = b.type === 'M_GROUP' ? b.member.username : b.group.name;
                return naturalSort(aKey, bKey);
            });
            let summary = _.reduce(finalResult, function (memo, obj) {

                return {
                    amount: memo.amount + obj.amount,
                    validAmount: memo.validAmount + obj.validAmount,
                    stackCount: memo.stackCount + obj.stackCount,
                    grossComm: memo.grossComm + obj.grossComm,
                    memberWinLose: memo.memberWinLose + obj.memberWinLose,
                    memberWinLoseCom: memo.memberWinLoseCom + obj.memberWinLoseCom,
                    memberTotalWinLoseCom: memo.memberTotalWinLoseCom + obj.memberTotalWinLoseCom,

                    agentWinLose: memo.agentWinLose + obj.agentWinLose,
                    agentWinLoseCom: memo.agentWinLoseCom + obj.agentWinLoseCom,
                    agentTotalWinLoseCom: memo.agentTotalWinLoseCom + obj.agentTotalWinLoseCom,

                    companyWinLose: memo.companyWinLose + obj.companyWinLose,
                    companyWinLoseCom: memo.companyWinLoseCom + obj.companyWinLoseCom,
                    companyTotalWinLoseCom: roundTo(memo.companyTotalWinLoseCom + obj.companyTotalWinLoseCom, 3),

                }
            }, {
                amount: 0,
                validAmount: 0,
                stackCount: 0,
                grossComm: 0,
                memberWinLose: 0,
                memberWinLoseCom: 0,
                memberTotalWinLoseCom: 0,
                agentWinLose: 0,
                agentWinLoseCom: 0,
                agentTotalWinLoseCom: 0,
                companyWinLose: 0,
                companyWinLoseCom: 0,
                companyTotalWinLoseCom: 0,
            });


            return res.send(
                {
                    code: 0,
                    message: "success",
                    result: {
                        dataList: finalResult,
                        summary: summary,
                    }
                }
            );

        });
    }

});
function transformDataByAgentType(type, dataList) {
    if (type === 'COMPANY') {
        return _.map(dataList, function (item) {

            let newItem = Object.assign({}, item);

            newItem.companyWinLose = item.superAdminWinLose;
            newItem.companyWinLoseCom = item.superAdminWinLoseCom;
            newItem.companyTotalWinLoseCom = item.superAdminTotalWinLoseCom;

            newItem.agentWinLose = item.companyWinLose;
            newItem.agentWinLoseCom = item.companyWinLoseCom;
            newItem.agentTotalWinLoseCom = item.companyTotalWinLoseCom;


            newItem.memberWinLose = (item.memberWinLose + item.masterAgentWinLose + item.agentWinLose + item.seniorWinLose + item.shareHolderWinLose);
            newItem.memberWinLoseCom = (item.memberWinLoseCom + item.masterAgentWinLoseCom + item.agentWinLoseCom + item.seniorWinLoseCom + item.shareHolderWinLoseCom);
            newItem.memberTotalWinLoseCom = (item.memberTotalWinLoseCom + item.masterAgentTotalWinLoseCom + item.agentTotalWinLoseCom + item.seniorTotalWinLoseCom + item.shareHolderTotalWinLoseCom);

            return newItem;
        });
    } else if (type === 'SHARE_HOLDER') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose;
            newItem.companyWinLoseCom = item.superAdminWinLoseCom + item.companyWinLoseCom;
            newItem.companyTotalWinLoseCom = item.superAdminTotalWinLoseCom + item.companyTotalWinLoseCom;

            newItem.agentWinLose = item.shareHolderWinLose;
            newItem.agentWinLoseCom = item.shareHolderWinLoseCom;
            newItem.agentTotalWinLoseCom = item.shareHolderTotalWinLoseCom;

            newItem.memberWinLose = (item.memberWinLose + item.masterAgentWinLose + item.agentWinLose + item.seniorWinLose);
            newItem.memberWinLoseCom = (item.memberWinLoseCom + item.masterAgentWinLoseCom + item.agentWinLoseCom + item.seniorWinLoseCom);
            newItem.memberTotalWinLoseCom = (item.memberTotalWinLoseCom + item.masterAgentTotalWinLoseCom + item.agentTotalWinLoseCom + item.seniorTotalWinLoseCom);
            return newItem;
        });
    } else if (type === 'SENIOR') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose + item.shareHolderWinLose;
            newItem.companyWinLoseCom = item.superAdminWinLoseCom + item.companyWinLoseCom + item.shareHolderWinLoseCom;
            newItem.companyTotalWinLoseCom = item.superAdminTotalWinLoseCom + item.companyTotalWinLoseCom + item.shareHolderTotalWinLoseCom;

            newItem.agentWinLose = item.seniorWinLose;
            newItem.agentWinLoseCom = item.seniorWinLoseCom;
            newItem.agentTotalWinLoseCom = item.seniorTotalWinLoseCom;


            newItem.memberWinLose = (item.memberWinLose + item.masterAgentWinLose + item.agentWinLose);
            newItem.memberWinLoseCom = (item.memberWinLoseCom + item.masterAgentWinLoseCom + item.agentWinLoseCom);
            newItem.memberTotalWinLoseCom = (item.memberTotalWinLoseCom + item.masterAgentTotalWinLoseCom + item.agentTotalWinLoseCom);

            return newItem;
        });
    } else if (type === 'MASTER_AGENT') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);

            newItem.companyWinLose = item.superAdminWinLose + item.companyWinLose + item.shareHolderWinLose + item.seniorWinLose;
            newItem.companyWinLoseCom = item.superAdminWinLoseCom + item.companyWinLoseCom + item.shareHolderWinLoseCom + item.seniorWinLoseCom;
            newItem.companyTotalWinLoseCom = item.superAdminTotalWinLoseCom + item.companyTotalWinLoseCom + item.shareHolderTotalWinLoseCom + item.seniorTotalWinLoseCom;

            newItem.agentWinLose = item.masterAgentWinLose;
            newItem.agentWinLoseCom = item.masterAgentWinLoseCom;
            newItem.agentTotalWinLoseCom = item.masterAgentTotalWinLoseCom;

            newItem.memberWinLose = roundTo(item.memberWinLose + item.agentWinLose, 3);
            newItem.memberWinLoseCom = roundTo(item.memberWinLoseCom + item.agentWinLoseCom, 3);
            newItem.memberTotalWinLoseCom = roundTo(item.memberTotalWinLoseCom + item.agentTotalWinLoseCom, 3);

            return newItem;
        });
    } else if (type === 'AGENT') {
        return _.map(dataList, function (item) {
            let newItem = Object.assign({}, item);
            newItem.companyWinLose = roundTo(item.superAdminWinLose + item.companyWinLose + item.shareHolderWinLose + item.seniorWinLose + item.masterAgentWinLose, 3);
            newItem.companyWinLoseCom = roundTo(item.superAdminWinLoseCom + item.companyWinLoseCom + item.shareHolderWinLoseCom + item.seniorWinLoseCom + item.masterAgentWinLoseCom, 3);
            newItem.companyTotalWinLoseCom = roundTo(item.superAdminTotalWinLoseCom + item.companyTotalWinLoseCom + item.shareHolderTotalWinLoseCom + item.seniorTotalWinLoseCom + item.masterAgentTotalWinLoseCom, 3);

            newItem.agentWinLose = item.agentWinLose;
            newItem.agentWinLoseCom = item.agentWinLoseCom;
            newItem.agentTotalWinLoseCom = item.agentTotalWinLoseCom;

            newItem.memberWinLose = item.memberWinLose;
            newItem.memberWinLoseCom = item.memberWinLoseCom;
            newItem.memberTotalWinLoseCom = item.memberTotalWinLoseCom;
            return newItem;
        });
    } else {
        return dataList;
    }
}
module.exports = router;