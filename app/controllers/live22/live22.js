const express = require('express');
const router = express.Router();
const request = require('request');
const moment = require('moment');
const crypto = require('crypto');
const utility = require('../../common/utlity');
const async = require("async");
const config = require('config');
const MemberService = require('../../common/memberService');
const ReportService = require('../../common/reportService');
const Joi = require('joi');
const DateUtils = require('../../common/dateUtils');
const roundTo = require('round-to');
const mongoose = require('mongoose');
const Hashing = require('../../common/hashing');
const _ = require('underscore');
const AgentGroupModel = require('../../models/agentGroup.model.js');
const NumberUtils = require('../../common/numberUtils1');
const AgentService = require('../../common/agentService');
const MemberModel = require("../../models/member.model");

const SlotBetHistoryModel = require('../../models/slotBetHistory.model.js');
const SlotSettleHistoryModel = require('../../models/slotSettleHistory.model.js');
const SlotBetTransactionModel = require('../../models/slotBetTransaction.model.js');
const SlotRollbackHistoryModel = require('../../models/slotRollbackHistory.model.js');
const BetTransactionModel = require('../../models/betTransaction.model.js');

const ActiveLive22TransactionModel = require('../../models/activeLive22Transaction.model.js');
const LogLive22Model = require('../../models/logLive22.model.js');

const DRAW = "DRAW";
const WIN = "WIN";
const HALF_WIN = "HALF_WIN";
const LOSE = "LOSE";
const HALF_LOSE = "HALF_LOSE";



const getClientKey = require('../getClientKey');
let {
    GAMING_SOFT_URL = 'https://cwuat.889tech.net/CommonWalletOPWS.svc/OPAPI/',
    GAMING_SOFT_OPERATOR_ID = 'ambbetTHB',
    GAMING_SOFT_SECRET = 'ambbetTHB'
} = getClientKey.getKey();
const CLIENT_NAME         = process.env.CLIENT_NAME          || 'SPORTBOOK88';


    /* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {title: 'Express'});
});


function buildSignature(FunctionName, requestDatetime, playerID) {
    let s;
    if (playerID) {
        s = FunctionName + requestDatetime + GAMING_SOFT_OPERATOR_ID + GAMING_SOFT_SECRET + playerID;
    } else {
        s = FunctionName + requestDatetime + GAMING_SOFT_OPERATOR_ID + GAMING_SOFT_SECRET;
    }
    return crypto.createHash('md5').update(s).digest("hex");
}


function random(low, high) {
    return Math.floor(Math.random() * (high - low + 1) + low)
}
function generateBetId() {
    return 'BET_GL' + new Date().getTime()+random(1000000,9999999);
}


router.get('/getGameList', (req, res) => {

    let time = moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss');

    let headers = {
        'Content-Type': 'application/json'
    };

    let apiName = 'GetGameList';

    let data = {
        OperatorId: GAMING_SOFT_OPERATOR_ID,
        RequestDateTime: time,
        Signature: buildSignature('GetGameList', time),
        Lang: 'en-us'
    };

    let options = {
        url: GAMING_SOFT_URL + apiName,
        method: 'POST',
        headers: headers,
        json: data,
    };
         console.log(options);
    request(options, (error, response, body) => {
         // console.log(response,body,error)
        if (error) {
            console.log(error)
            return res.send({message: error, code: 999});
        } else {
            // let json = JSON.parse(body);

            // console.log

                let gameList = _.map(body.Game, (item) => {
                    return {
                        Feature:item.Feature,
                        GameId:item.GameId,
                        GameName:item.GameName,
                        GameType:item.GameType,
                        ImageUrl:item.ImageUrl,
                        Index:item.Index,
                        IsH5Support:item.IsH5Support,
                        JackpotName:item.JackpotName,
                        Method:item.Method,
                        OtherGameName:item.OtherGameName,
                        OtherImageUrl:item.OtherImageUrl,
                        Provider:"Live22"
                    }
                });
            return res.send({data: gameList, code: 0});
        }
    });
});


router.get('/getGameArcade', (req, res) => {

    let time = moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss');

    let headers = {
        'Content-Type': 'application/json'
    };

    let apiName = 'Gamelist';

    let data = {
        OperatorId: GAMING_SOFT_OPERATOR_ID,
        RequestDateTime: time,
        Signature: buildSignature(apiName, time),
        Lang: 'en-us'
    }

    let options = {
        url: GAMING_SOFT_URL + apiName,
        method: 'POST',
        headers: headers,
        json: data,
    };

    request(options, (error, response, body) => {
        console.log(response, body, error)
        if (error) {
            console.log(error)
            return res.send({message: error, code: 999});
        } else {
            // let json = JSON.parse(body);

            // console.log(body)

            return res.send({data: body.Game, code: body.Status, Description: body.Description});
        }
    });
})


router.get('/getJackpot', (req, res) => {


    let time = moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss');

    let apiName = 'GetJackpot';

    let headers = {
        'Content-Type': 'application/json'
    };

    let data = {
        OperatorId: GAMING_SOFT_OPERATOR_ID,
        RequestDateTime: time,
        Signature: buildSignature(apiName, time),
        Lang: 'en-us'
    }


    let options = {
        url: GAMING_SOFT_URL + apiName,
        method: 'POST',
        headers: headers,
        json: data,
    };

    request(options, (error, response, body) => {
        // console.log(response,body,error)
        if (error) {
            console.log(error)
            return res.send({message: error, code: 999});
        } else {
            // let json = JSON.parse(body);

            // console.log(body)
            return res.send({data: body.Jackpots, code: 0});
        }
    });
});


router.get('/gameLogin', (req, res) => {

    let ClientType;
    let GameId;


    // 0=Web 1=IOS 2=Android
    if (req.query.ClientType) {
        ClientType = req.query.ClientType;
    } else {
        ClientType = 0;
    }

    if (req.query.GameId) {
        GameId = req.query.GameId;
    } else {
        GameId = 0;
    }

    let time = moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss');

    let apiName = 'GameLogin';

    let headers = {
        'Content-Type': 'application/json'
    };


    async.series([callback => {

        MemberService.findByUserName(req.userInfo.username, function (err, memberResponse) {
            if (err) {
                callback(err, null);
            } else {

                if (memberResponse.suspend) {
                    callback(5011, null);
                } else if (memberResponse.limitSetting.game.slotXO.isEnable) {

                    AgentService.getLive22Status(memberResponse.group._id, (err, response) => {
                        console.log('response : ', response)
                        if (!response.isEnable) {
                            callback(1088, null);
                        } else {
                            callback(null, memberResponse);
                        }
                    });
                } else {
                    callback(1088, null);
                }


            }
        });

    }, callback => {

        AgentService.getUpLineStatus(req.userInfo.group._id, function (err, status) {
            console.log('upline status : ' + status);
            if (err) {
                callback(err, null);
            } else {
                if (status.suspend) {
                    callback(5011, null);
                } else {
                    callback(null, status);
                }
            }
        });

    }], (err, asyncResponse) => {

        if (err) {
            if (err == 1088) {
                return res.send({
                    code: 1088,
                    message: 'Service Locked, Please contact your upline.'
                });
            }
            if (err == 5011) {
                return res.send({
                    message: "Account or Upline was suspend.",
                    result: null,
                    code: 5011
                });
            }
            return res.send({code: 999, message: err});
        }
        let memberInfo = asyncResponse[0];

        let credit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2);
        if (_.isNaN(credit)) {
            credit = 0;
        }

        let data = {
            PlayerId: req.userInfo.username.toLowerCase(),
            DisplayName: req.userInfo.username,
            OperatorId: GAMING_SOFT_OPERATOR_ID,
            RequestDateTime: time,
            Ip: utility.getIP(req)[0],
            ClientType: ClientType,
            GameId: GameId,
            PlayerBalance: credit,
            Currency: 'THB',
            Signature: buildSignature(apiName, time, req.userInfo.username.toLowerCase()),
            Lang: 'en-us'
        }

        let options = {
            url: GAMING_SOFT_URL + apiName,
            method: 'POST',
            headers: headers,
            json: data,
        };

        console.log(options)

        request(options, (error, response, body) => {
            console.log(body)
            if (error) {
                console.log(error)
                return res.send({message: error, code: 999});
            } else {
                // let json = JSON.parse(body);

                // console.log(body)
                return res.send({Url: body.Url, message: body.Description, code: 0});
            }
        });
    });

});


router.post('/callback', (req, res) => {
    console.log(req.body)

    return res.send('OK')
});

const getBalance = Joi.object().keys({
    RequestDateTime: Joi.string().required(),
    PlayerId: Joi.string().required(),
    OperatorId: Joi.string().required(),
    Signature: Joi.string().required(),
    Currency: Joi.string().required()
});

router.post('/callback/GetBalance', (req, res) => {
    console.log(req.body)

    let validate = Joi.validate(req.body, getBalance);
    if (validate.error) {
        console.log('Request Incomplete')
        let respond = {
            "Status": 900405,
            "Description": "Incoming Request Info Incomplete",
            "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
        }
        return res.send(respond)
    }

    let playerId = req.body.PlayerId;
    let Signature = buildSignature('GetBalance', req.body.RequestDateTime, playerId)

    if (Signature !== req.body.Signature) {
        console.log('Invalid Signature')

        let respond = {
            "Status": 900407,
            "Description": "Invalid Signature",
            "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
        }

        return res.send(respond)
    }

    MemberService.findByUserNameForPartnerService(playerId, function (err, memberResponse) {
        if (err) {
            return res.send({
                "Status": 900500,
                "Description": "Internal Server Error",
                "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                "OldBalance": 0,
                "NewBalance": 0
            });
        }
        if (memberResponse) {
            let credit = roundTo(memberResponse.creditLimit + memberResponse.balance, 2);
            return res.send({
                "Status": 200,
                "Description": "OK",
                "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                "Balance": credit
            });
        } else {
            return res.send({
                "Status": 900404,
                "Description": "Invalid player / Password",
                "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                "OldBalance": 0,
                "NewBalance": 0
            });
        }
    });

});


const Bet = Joi.object().keys({
    RequestDateTime: Joi.string().required(),
    TranDateTime: Joi.string().required(),
    BetId: Joi.number().required(),
    GameId: Joi.number().required(),
    GameType: Joi.string().required(),
    PlayerId: Joi.string().required(),
    BetAmount: Joi.number().required(),
    JackpotContribution: Joi.number().required(),
    Currency: Joi.string().required(),
    ExchangeRate: Joi.number().required(),
    Signature: Joi.string().required(),
});


router.post('/callback/Bet', (req, res) => {
    console.log(req.body)

    let validate = Joi.validate(req.body, Bet);
    if (validate.error) {
        console.log('Request Incomplete');
        let respond = {
            "Status": 900405,
            "Description": "Incoming Request Info Incomplete",
            "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
        }
        return res.send(respond)
    }

    let Signature = buildSignature('Bet', req.body.RequestDateTime, req.body.PlayerId);
    if (Signature !== req.body.Signature) {
        console.log('Invalid Signature');

        let respond = {
            "Status": 900407,
            "Description": "Invalid Signature",
            "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
        };

        return res.send(respond);
    }

    let requestBody = req.body;

    let body = {
        requestDateTime: requestBody.RequestDateTime,
        tranDateTime: requestBody.TranDateTime,
        betId: requestBody.BetId,
        gameId: requestBody.GameId,
        gameType: requestBody.GameType,
        playerId: requestBody.PlayerId,
        betAmount: requestBody.BetAmount,
        jackpotContribution: requestBody.JackpotContribution,
        currency: requestBody.Currency,
        createdDate: DateUtils.getCurrentDate(),
        exchangeRate: requestBody.ExchangeRate
    };

    let model = new SlotBetHistoryModel(body);

    model.save((err, agentResponse) => {

        if (err) {
            // callback(err, null);
        } else {
            // callback(null, agentResponse);
        }
    });

    async.series([callback => {

        MemberService.findByUserNameForPartnerService(requestBody.PlayerId, function (err, memberResponse) {
            if (err) {
                callback(err, null);
                return;
            }
            if (memberResponse) {

                let condition = {
                    'memberUsername': memberResponse.username_lower,
                    'slot.live22.betId': requestBody.BetId
                };

                ActiveLive22TransactionModel.findOne(condition).exec(function (err, response) {

                    if (err) {
                        callback(err, null);
                        return
                    }
                    if (response) {
                        callback(900409, memberResponse)
                    } else {
                        if (roundTo((memberResponse.creditLimit + memberResponse.balance), 2) < requestBody.BetAmount) {
                            callback(900605, memberResponse);
                        } else {
                            callback(null, memberResponse);
                        }
                    }
                });


            } else {
                callback(900404, null);
            }
        });

    }], (err, asyncResponse) => {

        if (err) {
            if (err == 900404) {
                return res.send({
                    "Status": 900404,
                    "Description": "Invalid player / Password",
                    "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                    "OldBalance": 0,
                    "NewBalance": 0
                });
            } else if (err == 900409) {
                let memberInfo = asyncResponse[0];
                let credit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2)

                let logLive22Model = new LogLive22Model({
                    betId:'',
                    message:JSON.stringify(requestBody),
                    createdDate:DateUtils.getCurrentDate()
                });
                logLive22Model.save((err, betObj) => {

                });

                return res.send({
                    "Status": 900409,
                    "Description": "Duplicate Transaction",
                    "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                    "OldBalance": credit,
                    "NewBalance": credit
                });

            } else if (err == 900605) {
                let memberInfo = asyncResponse[0];
                let credit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2);

                return res.send({
                    "Status": 900605,
                    "Description": "Insufficient Balance",
                    "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                    "OldBalance": credit,
                    "NewBalance": credit
                });

            } else {
                return res.send({
                    "Status": 900500,
                    "Description": "Internal Server Error",
                    "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                    "OldBalance": 0,
                    "NewBalance": 0
                });
            }
        }

        let memberInfo = asyncResponse[0];
        let credit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2);
        let betAmount = requestBody.BetAmount;


        AgentGroupModel.findById(memberInfo.group).select('_id type parentId shareSetting commissionSetting').populate({
            path: 'parentId',
            select: '_id type parentId shareSetting commissionSetting name',
            populate: {
                path: 'parentId',
                select: '_id type parentId shareSetting commissionSetting name',
                populate: {
                    path: 'parentId',
                    select: '_id type parentId shareSetting commissionSetting name',
                    populate: {
                        path: 'parentId',
                        select: '_id type parentId shareSetting commissionSetting name',
                        populate: {
                            path: 'parentId',
                            select: '_id type parentId shareSetting commissionSetting name',
                            populate: {
                                path: 'parentId',
                                select: '_id type parentId shareSetting commissionSetting name',
                            }
                        }
                    }
                }
            }

        }).exec(function (err, agentGroups) {

            let body = {
                memberId: memberInfo._id,
                memberParentGroup: memberInfo.group,
                memberUsername: memberInfo.username_lower,
                betId: generateBetId(),
                slot: {
                    live22: {
                        requestDateTime: requestBody.RequestDateTime,
                        tranDateTime: requestBody.TranDateTime,
                        betId: requestBody.BetId,
                        gameId: requestBody.GameId,
                        gameType: requestBody.GameType,
                        jackpotContribution: requestBody.JackpotContribution,
                        currency: requestBody.Currency,
                        exchangeRate: requestBody.ExchangeRate,
                        betAmount: requestBody.BetAmount
                    },
                },
                amount: betAmount,
                memberCredit: betAmount * -1,
                payout: betAmount,
                commission: {},
                status: 'RUNNING',
                gameDate: DateUtils.getCurrentDate(),
                createdDate: DateUtils.getCurrentDate(),
                currency: memberInfo.currency
            };

            prepareCommission(memberInfo, body, agentGroups, null, 0, 0);

            let betTransactionModel = new ActiveLive22TransactionModel(body);

            betTransactionModel.save(function (err, response) {

                if (err) {
                    if (err.code == 11000) {

                        let logLive22Model = new LogLive22Model({
                            betId:body.betId,
                            message:JSON.stringify(requestBody),
                            createdDate:DateUtils.getCurrentDate()
                        });
                        logLive22Model.save((err, betObj) => {

                        });

                        return res.send({
                            "Status": 900409,
                            "Description": "Duplicate Transaction",
                            "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                            "OldBalance": credit,
                            "NewBalance": credit
                        });
                    }
                    return res.send({
                        "Status": 900500,
                        "Description": "Internal Server Error",
                        "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                        "OldBalance": 0,
                        "NewBalance": 0
                    });
                }

                MemberService.updateBalance(memberInfo._id, (betAmount * -1), response.betId, 'SLOT_LIVE22_BET', requestBody.BetId, function (err, updateBalanceResponse) {
                    if (err) {
                        return res.send({code: 0, message: "update balance fail"});
                    } else {

                        let oldCredit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2)

                        let respond = {
                            "Status": 200,
                            "Description": "OK",
                            "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                            "OldBalance": oldCredit,
                            "NewBalance": updateBalanceResponse.newBalance
                        };

                        return res.send(respond);
                    }
                });
            });
        });


    });

});

const GameResult = Joi.object().keys({
    RequestDateTime: Joi.string().required(),
    TranDateTime: Joi.string().required(),
    ResultType: Joi.number().required(),
    ResultId: Joi.number().required(),
    BetId: Joi.number().required(),
    Result: Joi.string().required(),
    GameId: Joi.number().required(),
    GameType: Joi.string().required(),
    GameName: Joi.string().required(),
    PlayerId: Joi.string().required(),
    BetAmount: Joi.number().required(),
    JackpotContribution: Joi.number().required(),
    Payout: Joi.number().required(),
    WinLose: Joi.number().required(),
    Currency: Joi.string().required(),
    ExchangeRate: Joi.number().required(),
    ValidBetAmount: Joi.number(),
    Signature: Joi.string().required(),
});

router.post('/callback/GameResult', (req, res) => {
    console.log(req.body)

    let validate = Joi.validate(req.body, GameResult);
    if (validate.error) {
        console.log('Request Incomplete')
        console.log(validate.error)
        let respond = {
            "Status": 900405,
            "Description": "Incoming Request Info Incomplete",
            "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
        };
        return res.send(respond)
    }

    let Signature = buildSignature('GameResult', req.body.RequestDateTime, req.body.PlayerId);
    if (Signature !== req.body.Signature) {
        console.log('Invalid Signature')

        let respond = {
            "Status": 900407,
            "Description": "Invalid Signature",
            "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
        };

        return res.send(respond);
    }

    let requestBody = req.body;

    let body = {
        requestDateTime: requestBody.RequestDateTime,
        tranDateTime: requestBody.TranDateTime,
        betId: requestBody.BetId,
        gameId: requestBody.GameId,
        gameType: requestBody.GameType,
        playerId: requestBody.PlayerId,
        betAmount: requestBody.BetAmount,
        jackpotContribution: requestBody.JackpotContribution,
        currency: requestBody.Currency,
        resultId: requestBody.ResultId,
        result: requestBody.Result,
        resultType: requestBody.ResultType,
        payout: requestBody.Payout,
        winLose: requestBody.WinLose,
        createdDate: DateUtils.getCurrentDate(),
        exchangeRate: requestBody.ExchangeRate
    };

    let model = new SlotSettleHistoryModel(body);
    model.save((err, agentResponse) => {

        if (err) {
            // callback(err, null);
        } else {
            // callback(null, agentResponse);
        }
    });

    async.series([callback => {

        MemberService.findByUserNameForPartnerService(requestBody.PlayerId, function (err, memberResponse) {
            if (err) {
                callback(err, null);
                return;
            }
            if (memberResponse) {
                callback(null, memberResponse);
            } else {
                callback(900404, null);
            }
        });

    }], (err, asyncResponse) => {

        if (err) {
            if (err == 900404) {
                return res.send({
                    "Status": 900404,
                    "Description": "Invalid player / Password",
                    "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                    "OldBalance": 0,
                    "NewBalance": 0
                });
            } else if (err == 900409) {
                return res.send({
                    "Status": 900409,
                    "Description": "Duplicate Transaction",
                    "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                    "OldBalance": 0,
                    "NewBalance": 0
                });

            } else {
                return res.send({
                    "Status": 900500,
                    "Description": "Internal Server Error",
                    "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                    "OldBalance": 0,
                    "NewBalance": 0
                });
            }
        }

        let memberInfo = asyncResponse[0];


        let credit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2);


        const RESULT_BASE = 0;
        const RESULT_FREE_SPIN = 1;
        const RESULT_BONUS = 2;
        const RESULT_RE_SPIN = 3;
        const RESULT_JACKPOT = 4;

        async.series([callback => {

            let condition = {
                'memberUsername': requestBody.PlayerId,
                'slot.live22.betId': requestBody.BetId
            };

            ActiveLive22TransactionModel.findOne(condition).exec(function (err, mainObj) {

                if (err) {
                    callback(err, null);
                    return
                }

                if (!mainObj) {
                    callback(900415, null);
                    return
                }

                if (requestBody.ResultType == RESULT_BASE) {

                    if (mainObj.status === 'DONE') {
                        callback(900409, null)
                    } else {
                        callback(null, mainObj)
                    }
                } else {

                    let condition = {
                        'memberUsername': requestBody.PlayerId,
                        'slot.live22.betId': requestBody.BetId,
                        'slot.live22.resultId': requestBody.ResultId,
                        'status': 'DONE'
                    };

                    ActiveLive22TransactionModel.findOne(condition).exec(function (err, response) {

                        if (err) {
                            callback(err, null);
                            return
                        }

                        if (response) {
                            callback(900409, null);
                        } else {

                            let betAmount = requestBody.BetAmount;
                            let body = {
                                memberId: memberInfo._id,
                                memberParentGroup: memberInfo.group,
                                memberUsername: memberInfo.username_lower,
                                betId: generateBetId(),
                                slot: {
                                    live22: {
                                        requestDateTime: requestBody.RequestDateTime,
                                        tranDateTime: requestBody.TranDateTime,
                                        betId: requestBody.BetId,
                                        gameId: requestBody.GameId,
                                        gameType: requestBody.GameType,
                                        jackpotContribution: requestBody.JackpotContribution,
                                        currency: requestBody.Currency,
                                        exchangeRate: requestBody.ExchangeRate,
                                        betAmount: requestBody.BetAmount,

                                        gameName: requestBody.GameName,
                                        result: requestBody.Result,
                                        resultId: requestBody.ResultId,
                                        resultType: requestBody.ResultType,
                                        payout: requestBody.Payout,
                                        winLose: requestBody.WinLose
                                    },
                                },
                                amount: betAmount,
                                memberCredit: betAmount * -1,
                                payout: betAmount,
                                status: 'RUNNING',
                                commission: {},
                                gameDate: DateUtils.getCurrentDate(),
                                createdDate: DateUtils.getCurrentDate(),
                                isEndScore: false,
                                currency: memberInfo.currency
                            };

                            body.commission = mainObj.commission;

                            let betTransactionModel = new ActiveLive22TransactionModel(body);

                            betTransactionModel.save(function (err, betObj) {
                                if (err) {
                                    callback(err, null);
                                } else {
                                    callback(null, betObj);
                                }
                            });
                        }

                    });

                }

            });


        }], (err, asyncResponse) => {

            let activeBetObj = asyncResponse[0];

            if (err) {
                if (err == 900415) {
                    return res.send({
                        "Status": 900415,
                        "Description": "Bet Transaction Not Found",
                        "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                        "OldBalance": credit,
                        "NewBalance": credit
                    });
                }

                if (err == 900409) {
                    return res.send({
                        "Status": 900409,
                        "Description": "Duplicate Transaction",
                        "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                        "OldBalance": credit,
                        "NewBalance": credit
                    });
                }
            }

            if (!activeBetObj) {
                return res.send({
                    "Status": 900415,
                    "Description": "Bet Transaction Not Found",
                    "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                    "OldBalance": credit,
                    "NewBalance": credit
                });
            }

            let betResult = requestBody.WinLose > 0 ? WIN : requestBody.WinLose < 0 ? LOSE : DRAW;

            const shareReceive = AgentService.prepareShareReceive(requestBody.WinLose, betResult, activeBetObj);


            updateAgentMemberBalance(shareReceive, requestBody.BetAmount, requestBody.BetId, function (err, updateBalanceResponse) {
                if (err) {
                    if (err == 888) {
                        return res.send({
                            "Status": 900409,
                            "Description": "Duplicate Transaction",
                            "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                            "OldBalance": credit,
                            "NewBalance": credit
                        });
                    }
                    return res.send({
                        "Status": 900500,
                        "Description": "Internal Server Error",
                        "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                        "OldBalance": 0,
                        "NewBalance": 0
                    });
                } else {

                    let updateBody = {
                        $set: {
                            'slot.live22.gameName': requestBody.GameName,
                            'slot.live22.result': requestBody.Result,
                            'slot.live22.resultId': requestBody.ResultId,
                            'slot.live22.resultType': requestBody.ResultType,
                            'slot.live22.payout': requestBody.Payout,
                            'slot.live22.winLose': requestBody.WinLose,
                            'commission.superAdmin.winLoseCom': shareReceive.superAdmin.winLoseCom,
                            'commission.superAdmin.winLose': shareReceive.superAdmin.winLose,
                            'commission.superAdmin.totalWinLoseCom': shareReceive.superAdmin.totalWinLoseCom,

                            'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                            'commission.company.winLose': shareReceive.company.winLose,
                            'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                            'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                            'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                            'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                            'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                            'commission.senior.winLose': shareReceive.senior.winLose,
                            'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                            'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                            'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                            'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                            'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                            'commission.agent.winLose': shareReceive.agent.winLose,
                            'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                            'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                            'commission.member.winLose': shareReceive.member.winLose,
                            'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                            'validAmount': requestBody.BetAmount,
                            'betResult': betResult,
                            'isEndScore': true,
                            'status': 'DONE',
                            'settleDate': DateUtils.getCurrentDate()
                        }
                    };

                    ActiveLive22TransactionModel.update({_id: activeBetObj._id}, updateBody, function (err, response) {

                        if (response.nModified == 1) {

                            //TODO queue process
                            let body = {
                                memberId: activeBetObj.memberId,
                                memberParentGroup: activeBetObj.memberParentGroup,
                                memberUsername: activeBetObj.memberUsername,
                                betId: activeBetObj.betId,
                                slot: activeBetObj.slot,
                                amount: activeBetObj.amount,
                                memberCredit: activeBetObj.amount * -1,
                                payout: activeBetObj.amount,
                                gameType: 'TODAY',
                                acceptHigherPrice: false,
                                priceType: 'TH',
                                game: 'GAME',
                                source: 'LIVE22',
                                status: 'DONE',
                                commission: activeBetObj.commission,
                                gameDate: DateUtils.getCurrentDate(),
                                createdDate: DateUtils.getCurrentDate(),
                                validAmount: activeBetObj.amount,
                                betResult: betResult,
                                isEndScore: true,
                                currency: memberInfo.currency
                            };

                            body.commission.superAdmin.winLoseCom = shareReceive.superAdmin.winLoseCom;
                            body.commission.superAdmin.winLose = shareReceive.superAdmin.winLose;
                            body.commission.superAdmin.totalWinLoseCom = shareReceive.superAdmin.totalWinLoseCom;

                            body.commission.company.winLoseCom = shareReceive.company.winLoseCom;
                            body.commission.company.winLose = shareReceive.company.winLose;
                            body.commission.company.totalWinLoseCom = shareReceive.company.totalWinLoseCom;

                            body.commission.shareHolder.winLoseCom = shareReceive.shareHolder.winLoseCom;
                            body.commission.shareHolder.winLose = shareReceive.shareHolder.winLose;
                            body.commission.shareHolder.totalWinLoseCom = shareReceive.shareHolder.totalWinLoseCom;

                            body.commission.senior.winLoseCom = shareReceive.senior.winLoseCom;
                            body.commission.senior.winLose = shareReceive.senior.winLose;
                            body.commission.senior.totalWinLoseCom = shareReceive.senior.totalWinLoseCom;

                            body.commission.masterAgent.winLoseCom = shareReceive.masterAgent.winLoseCom;
                            body.commission.masterAgent.winLose = shareReceive.masterAgent.winLose;
                            body.commission.masterAgent.totalWinLoseCom = shareReceive.masterAgent.totalWinLoseCom;

                            body.commission.agent.winLoseCom = shareReceive.agent.winLoseCom;
                            body.commission.agent.winLose = shareReceive.agent.winLose;
                            body.commission.agent.totalWinLoseCom = shareReceive.agent.totalWinLoseCom;

                            body.commission.member.winLoseCom = shareReceive.member.winLoseCom;
                            body.commission.member.winLose = shareReceive.member.winLose;
                            body.commission.member.totalWinLoseCom = shareReceive.member.totalWinLoseCom;


                            body.slot.live22.gameName = requestBody.GameName;
                            body.slot.live22.result = requestBody.Result;
                            body.slot.live22.resultId = requestBody.ResultId;
                            body.slot.live22.resultType = requestBody.ResultType;
                            body.slot.live22.payout = requestBody.Payout;
                            body.slot.live22.winLose = requestBody.WinLose;

                            body.settleDate =  DateUtils.getCurrentDate();

                            let betTransactionModel = new BetTransactionModel(body);
                            betTransactionModel.save((err, betObj) => {
                                if (err) {
                                    if (err.code == 11000) {
                                        return res.send({
                                            "Status": 900409,
                                            "Description": "Duplicate Transaction",
                                            "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                                            "OldBalance": credit,
                                            "NewBalance": credit
                                        });
                                    }



                                    let logLive22Model = new LogLive22Model({
                                        betId:body.betId,
                                        message:err,
                                        createdDate:DateUtils.getCurrentDate()
                                    });
                                    logLive22Model.save((err, betObj) => {

                                    });

                                    return res.send({
                                        "Status": 900500,
                                        "Description": "Internal Server Error",
                                        "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                                        "OldBalance": 0,
                                        "NewBalance": 0
                                    });
                                } else {

                                    let newBalance = updateBalanceResponse.newBalance;
                                    let oldCredit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2)

                                    let respond = {
                                        "Status": 200,
                                        "Description": "OK",
                                        "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                                        "OldBalance": oldCredit,
                                        "NewBalance": newBalance
                                    };

                                        console.log('response : ',response)
                                    return res.send(respond);
                                }


                            });

                        } else {
                            return res.send({
                                "Status": 900500,
                                "Description": "Internal Server Error",
                                "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                                "OldBalance": 0,
                                "NewBalance": 0
                            });
                        }
                    });
                }
            });

        });
    });

});

const Rollback = Joi.object().keys({
    RequestDateTime: Joi.string().required(),
    TranDateTime: Joi.string().required(),
    BetId: Joi.number().required(),
    GameId: Joi.number().required(),
    GameType: Joi.string().required(),
    PlayerId: Joi.string().required(),
    BetAmount: Joi.number().required(),
    JackpotContribution: Joi.number().required(),
    Currency: Joi.string().required(),
    ExchangeRate: Joi.number().required(),
    Signature: Joi.string().required(),
});

router.post('/callback/Rollback', (req, res) => {
    console.log(req.body)

    let validate = Joi.validate(req.body, Rollback);
    if (validate.error) {
        console.log('Request Incomplete')
        let respond = {
            "Status": 900405,
            "Description": "Incoming Request Info Incomplete",
            "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
        }
        return res.send(respond)
    }

    let Signature = buildSignature('Rollback', req.body.RequestDateTime, req.body.PlayerId);
    if (Signature !== req.body.Signature) {
        console.log('Invalid Signature')


        let respond = {
            "Status": 900407,
            "Description": "Invalid Signature",
            "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
        };

        return res.send(respond)
    }


    let requestBody = req.body;

    let body = {
        requestDateTime: requestBody.RequestDateTime,
        tranDateTime: requestBody.TranDateTime,
        betId: requestBody.BetId,
        gameId: requestBody.GameId,
        gameType: requestBody.GameType,
        playerId: requestBody.PlayerId,
        betAmount: requestBody.BetAmount,
        jackpotContribution: requestBody.JackpotContribution,
        currency: requestBody.Currency,
        createdDate: DateUtils.getCurrentDate(),
        exchangeRate: requestBody.ExchangeRate
    };

    let model = new SlotRollbackHistoryModel(body);
    model.save((err, agentResponse) => {

        if (err) {
            // callback(err, null);
        } else {
            // callback(null, agentResponse);
        }
    });
    async.series([callback => {

        MemberService.findByUserNameForPartnerService(requestBody.PlayerId, function (err, memberResponse) {
            if (err) {
                callback(err, null);
                return;
            }
            if (memberResponse) {
                callback(null, memberResponse);
            } else {
                callback(900404, null);
            }
        });

    }], (err, asyncResponse) => {

        if (err) {
            if (err == 900404) {
                return res.send({
                    "Status": 900404,
                    "Description": "Invalid player / Password",
                    "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                    "OldBalance": 0,
                    "NewBalance": 0
                });
            } else if (err == 900409) {
                return res.send({
                    "Status": 900409,
                    "Description": "Duplicate Transaction",
                    "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                    "OldBalance": 0,
                    "NewBalance": 0
                });

            } else {
                return res.send({
                    "Status": 900500,
                    "Description": "Internal Server Error",
                    "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                    "OldBalance": 0,
                    "NewBalance": 0
                });
            }
        }


        let memberInfo = asyncResponse[0];
        let oldCredit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2)


        console.log('oldCredit : ', oldCredit)
        let condition = {
            'memberUsername': requestBody.PlayerId,
            'slot.live22.betId': requestBody.BetId
        };


        ActiveLive22TransactionModel.findOne(condition).exec(function (err, activeLive22Response) {
            if (err) {
                return res.send({
                    "Status": 900500,
                    "Description": "Internal Server Error",
                    "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                    "OldBalance": oldCredit,
                    "NewBalance": oldCredit
                });
            } else {


                if (!activeLive22Response) {
                    return res.send({
                        "Status": 900415,
                        "Description": "Bet Transaction Not Found",
                        "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                        "OldBalance": oldCredit,
                        "NewBalance": oldCredit
                    });
                }

                if (activeLive22Response.status === 'CANCELLED') {
                    return res.send({
                        "Status": 900409,
                        "Description": "Duplicate Transaction",
                        "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                        "OldBalance": oldCredit,
                        "NewBalance": oldCredit
                    });
                }

                async.series([callback => {


                    MemberService.updateBalance(activeLive22Response.memberId, Math.abs(activeLive22Response.memberCredit), activeLive22Response.betId, 'SLOT_LIVE22_ROLLBACK', requestBody.BetId, function (err, creditResponse) {
                        if (err) {
                            callback(err, null);
                        } else {
                            console.log('creditResponse : ', creditResponse)
                            callback(null, creditResponse);
                        }
                    });


                }], function (err, asyncResponse) {

                    if (err) {
                        return res.send({
                            "Status": 900500,
                            "Description": "Internal Server Error",
                            "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                            "OldBalance": 0,
                            "NewBalance": 0
                        });
                    }

                    let newBalance = asyncResponse[0];

                    let updateBody = {};
                    updateBody.status = 'CANCELLED';

                    ActiveLive22TransactionModel.update({_id: activeLive22Response._id}, updateBody, function (err, response) {

                        if (response.nModified == 1) {

                            //TODO queue

                            let updateBetTranBody = {
                                'commission.superAdmin.winLoseCom': 0,
                                'commission.superAdmin.winLose': 0,
                                'commission.superAdmin.totalWinLoseCom': 0,

                                'commission.company.winLoseCom': 0,
                                'commission.company.winLose': 0,
                                'commission.company.totalWinLoseCom': 0,

                                'commission.shareHolder.winLoseCom': 0,
                                'commission.shareHolder.winLose': 0,
                                'commission.shareHolder.totalWinLoseCom': 0,

                                'commission.senior.winLoseCom': 0,
                                'commission.senior.winLose': 0,
                                'commission.senior.totalWinLoseCom': 0,

                                'commission.masterAgent.winLoseCom': 0,
                                'commission.masterAgent.winLose': 0,
                                'commission.masterAgent.totalWinLoseCom': 0,

                                'commission.agent.winLoseCom': 0,
                                'commission.agent.winLose': 0,
                                'commission.agent.totalWinLoseCom': 0,

                                'commission.member.winLoseCom': 0,
                                'commission.member.winLose': 0,
                                'commission.member.totalWinLoseCom': 0,
                                'status': 'CANCELLED'
                            };

                            BetTransactionModel.update({
                                betId: activeLive22Response.betId
                            }, updateBetTranBody, (err, data) => {
                                if (err) {
                                    return res.send({
                                        "Status": 900500,
                                        "Description": "Internal Server Error",
                                        "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                                        "OldBalance": 0,
                                        "NewBalance": 0
                                    });
                                } else if (data) {
                                    let respond = {
                                        "Status": 200,
                                        "Description": "OK",
                                        "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                                        "OldBalance": oldCredit,
                                        "NewBalance": newBalance.newBalance
                                    }

                                    return res.send(respond)
                                }
                            });
                        } else {
                            return res.send({
                                "Status": 900500,
                                "Description": "Internal Server Error",
                                "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                                "OldBalance": 0,
                                "NewBalance": 0
                            });
                        }
                    });

                });
            }
        });


    });


});


router.post('/callback/re', (req, res) => {
    console.log(req.body.betId)


    ActiveLive22TransactionModel.findOne({betId: req.body.betId}, function (err, activeBetObj) {

        console.log(err)
        console.log(activeBetObj)

        if (activeBetObj) {
            //TODO queue process
            let body = {
                memberId: activeBetObj.memberId,
                memberParentGroup: activeBetObj.memberParentGroup,
                memberUsername: activeBetObj.memberUsername,
                betId: activeBetObj.betId,
                slot: activeBetObj.slot,
                amount: activeBetObj.amount,
                memberCredit: activeBetObj.amount * -1,
                payout: activeBetObj.amount,
                gameType: 'TODAY',
                acceptHigherPrice: false,
                priceType: 'TH',
                game: 'GAME',
                source: 'LIVE22',
                status: 'DONE',
                commission: activeBetObj.commission,
                gameDate: activeBetObj.gameDate,
                createdDate: activeBetObj.createdDate,
                validAmount: activeBetObj.amount,
                betResult: activeBetObj.betResult,
                isEndScore: true,
                currency: 'THB'
            };



            let betTransactionModel = new BetTransactionModel(body);
            betTransactionModel.save((err, betObj) => {
                if (err) {
                    console.log(err)
                    if (err.code == 11000) {
                        return res.send({
                            "Status": 900409,
                            "Description": "Duplicate Transaction",
                            "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                            "OldBalance": credit,
                            "NewBalance": credit
                        });
                    }
                    return res.send({
                        "Status": 900500,
                        "Description": "Internal Server Error",
                        "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                        "OldBalance": 0,
                        "NewBalance": 0
                    });
                } else {

                    let newBalance = updateBalanceResponse.newBalance;
                    let oldCredit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2)

                    let respond = {
                        "Status": 200,
                        "Description": "OK",
                        "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                        "OldBalance": oldCredit,
                        "NewBalance": newBalance
                    };
                    return res.send(respond);
                }


            });

        } else {
            return res.send({
                "Status": 900500,
                "Description": "Data not found",
                "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                "OldBalance": 0,
                "NewBalance": 0
            });
        }
    });


});

const MobileLogin = Joi.object().keys({
    LoginId: Joi.string().required(),
    Password: Joi.string().required(),
    IpAddress: Joi.string().allow(''),
    BrandCode: Joi.string().allow(''),
});

router.post('/callback/MobileLogin', (req, res) => {
    console.log(req.body)

    let validate = Joi.validate(req.body, MobileLogin);
    if (validate.error) {
        console.log('Request Incomplete');
        let respond = {
            "Status": 900405,
            "Description": "Incoming Request Info Incomplete",
            "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
        }
        return res.send(respond)
    }

    let requestBody = req.body;


    MemberModel.findOne({$or: [{'username_lower': requestBody.LoginId.toLowerCase()}, {'loginName_lower': requestBody.LoginId.toLowerCase()}]})
        .populate({path: 'group', select: '_id type name endpoint secretKey'})
        .exec(function (err, memberResponse) {

            console.log('---- :: ', memberResponse)

            if (err) {

                return res.send({
                    "Status": 900500,
                    "Description": "Internal Server Error",
                    "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                    "PlayerId": requestBody.LoginId,
                    "Balance": 0,
                    "Currency": "",
                    "DisplayName": requestBody.LoginId
                });

            }


            if (memberResponse && (Hashing.encrypt256_1(requestBody.Password) === memberResponse.password)) {
                let credit = roundTo((memberResponse.creditLimit + memberResponse.balance), 2);


                async.waterfall([callback => {

                    AgentService.getUpLineStatus(memberResponse.group._id, function (err, status) {
                        console.log('upline status : ' + status);
                        if (err) {
                            callback(err, null);
                        } else {
                            if (status.suspend) {
                                callback(5011, null);
                            } else {
                                callback(null, status);
                            }
                        }
                    });

                }, (uplineStatus, callback) => {

                    if (memberResponse.suspend) {
                        callback(5011, null);
                    } else if (memberResponse.limitSetting.game.slotXO.isEnable) {
                        AgentService.getLive22Status(memberResponse.group._id, (err, response) => {
                            if (!response.isEnable) {
                                callback(1088, null);
                            } else {
                                callback(null, uplineStatus, memberResponse);
                            }
                        });
                    } else {
                        callback(1088, null);
                    }

                }], function (err, uplineStatus, memberInfo) {

                    if (err) {

                        if (err == 1088 || err == 5011) {
                            let respond = {
                                "Status": 900404,
                                "Description": "Invalid player / password",
                                "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                                "PlayerId": requestBody.LoginId,
                                "Balance": 0,
                                "Currency": "THB",
                                "DisplayName": requestBody.LoginId
                            };
                            return res.send(respond)
                        }

                        return res.send({
                            "Status": 900500,
                            "Description": "Internal Server Error",
                            "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                            "PlayerId": requestBody.LoginId,
                            "Balance": 0,
                            "Currency": "",
                            "DisplayName": requestBody.LoginId
                        });
                    }

                    let respond = {
                        "Status": 200,
                        "Description": "OK",
                        "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                        "PlayerId": memberResponse.username_lower,
                        "Balance": credit,
                        "Currency": memberResponse.currency,
                        "DisplayName": memberResponse.username_lower
                    };
                    return res.send(respond);
                });

            } else {
                let respond = {
                    "Status": 900404,
                    "Description": "Invalid player / password",
                    "ResponseDateTime": moment().add(-7, 'hour').format('YYYY-MM-DD HH:mm:ss'),
                    "PlayerId": requestBody.LoginId,
                    "Balance": 0,
                    "Currency": "THB",
                    "DisplayName": requestBody.LoginId
                };
                return res.send(respond)
            }
        });


});

function prepareCommission(memberInfo, body, currentAgent, overAgent, remaining, overAllShare) {


    if (currentAgent && (currentAgent.type === 'SUPER_ADMIN' || currentAgent.parentId)) {


        console.log('==============================================');
        console.log('process : ' + currentAgent.type + ' : ' + currentAgent.name);


        let money = body.amount;


        if (!overAgent) {
            overAgent = {};
            overAgent.type = 'member';
            overAgent.shareSetting = {};
            overAgent.shareSetting.game = {};
            overAgent.shareSetting.game.slotXO = {};
            overAgent.shareSetting.game.slotXO.parent = memberInfo.shareSetting.game.slotXO.parent;
            overAgent.shareSetting.game.slotXO.own = 0;
            overAgent.shareSetting.game.slotXO.remaining = 0;

            body.commission.member = {};
            body.commission.member.parent = memberInfo.shareSetting.game.slotXO.parent;
            body.commission.member.commission = memberInfo.commissionSetting.game.slotXO;

        }

        console.log('');
        console.log('---- setting ----');
        console.log('ส่วนแบ่งที่ได้มามีทั้งหมด : ' + currentAgent.shareSetting.game.slotXO.own + ' %');
        console.log('ขอส่วนแบ่งจาก : ' + overAgent.type + ' ' + overAgent.shareSetting.game.slotXO.parent + ' %');
        console.log('ให้ส่วนแบ่ง : ' + overAgent.type + ' ที่เหลือไป ' + overAgent.shareSetting.game.slotXO.own + ' %');
        console.log('remaining จาก : ' + overAgent.type + ' : ' + overAgent.shareSetting.game.slotXO.remaining + ' %');
        console.log('โดนบังคับเสียขั้นต่ำ : ' + currentAgent.shareSetting.game.slotXO.min + ' %');
        console.log('---- end setting ----');
        console.log('');


        // console.log('และได้รับ remaining ไว้ : '+overAgent.shareSetting.game.slotXO.remaining+' %');

        let currentPercentReceive = overAgent.shareSetting.game.slotXO.parent;
        console.log('% ที่ได้ : ' + currentPercentReceive);
        console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');
        let totalRemaining = remaining;

        if (overAgent.shareSetting.game.slotXO.remaining > 0) {
            // console.log()
            // console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');

            if (overAgent.shareSetting.game.slotXO.remaining > totalRemaining) {
                console.log('รับ remaining ทั้งหมดไว้: ' + totalRemaining + ' %');
                currentPercentReceive += totalRemaining;
                totalRemaining = 0;
            } else {
                totalRemaining = remaining - overAgent.shareSetting.game.slotXO.remaining;
                console.log('หักค่า remaining ที่ได้รับมากับที่เซตรับไว้ = ' + remaining + ' - ' + overAgent.shareSetting.game.slotXO.remaining + ' = ' + totalRemaining);
                currentPercentReceive += overAgent.shareSetting.game.slotXO.remaining;
            }

        }

        console.log('รวม % ที่ได้รับ : ' + currentPercentReceive);


        let unUseShare = currentAgent.shareSetting.game.slotXO.own -
            (overAgent.shareSetting.game.slotXO.parent + overAgent.shareSetting.game.slotXO.own);

        console.log('เหลือส่วนแบ่งที่ไม่ได้ใช้ : ' + unUseShare + ' %');
        totalRemaining = (unUseShare + totalRemaining);
        console.log('ส่วนแบ่งที่ไม่ได้ใช้ บวกกับค่า remaining ท่ี่เหลือจะเป็น : ' + totalRemaining + ' %');

        console.log('จากที่โดนบังคับเสียขั้นต่ำ : ' + currentAgent.shareSetting.game.slotXO.min + ' %');
        console.log('overAllShare : ', overAllShare);
        if (currentAgent.shareSetting.game.slotXO.min > 0 && ((overAllShare + currentPercentReceive) < currentAgent.shareSetting.game.slotXO.min)) {

            if (currentPercentReceive < currentAgent.shareSetting.game.slotXO.min) {
                console.log('% ที่ได้รับ < ขั้นต่ำที่ถูกตั้ง min ไว้');
                let percent = currentAgent.shareSetting.game.slotXO.min - (currentPercentReceive + overAllShare);
                console.log('หักออกไป ' + percent + ' % : แล้วไปเพิ่มในส่วนที่ต้องรับผิดชอบ ');
                totalRemaining = totalRemaining - percent;
                if (totalRemaining < 0) {
                    totalRemaining = 0;
                }
                currentPercentReceive += percent;
            }
        } else {
            // currentPercentReceive += totalRemaining;
        }


        if (currentAgent.type === 'SUPER_ADMIN') {
            currentPercentReceive += totalRemaining;
        }

        let getMoney = (money * NumberUtils.convertPercent(currentPercentReceive)).toFixed(2);
        overAllShare = overAllShare + currentPercentReceive;

        console.log('ยอดแทง ' + money + ' ได้ทั้งหมด : ' + currentPercentReceive + ' %');

        console.log('รวม % + % remaining  จะเป็นสัดส่วนที่ได้ทั้งหมด = ' + getMoney);
        console.log('ค่าที่จะถูกคืนกลับไป (remaining) : ' + totalRemaining + ' %');

        console.log('จำนวน % ที่ถูกแบ่งไปแล้วทั้งหมด : ', overAllShare);

        //set commission
        let agentCommission = currentAgent.commissionSetting.game.slotXO;


        switch (currentAgent.type) {
            case 'SUPER_ADMIN':
                body.commission.superAdmin = {};
                body.commission.superAdmin.group = currentAgent._id;
                body.commission.superAdmin.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.superAdmin.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.superAdmin.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.superAdmin.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.superAdmin.shareReceive = Math.abs(currentPercentReceive);
                body.commission.superAdmin.commission = agentCommission;
                body.commission.superAdmin.amount = getMoney;
                break;
            case 'COMPANY':
                body.commission.company = {};
                body.commission.company.parentGroup = currentAgent.parentId;
                body.commission.company.group = currentAgent._id;
                body.commission.company.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.company.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.company.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.company.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.company.shareReceive = Math.abs(currentPercentReceive);
                body.commission.company.commission = agentCommission;
                body.commission.company.amount = getMoney;
                break;
            case 'SHARE_HOLDER':
                body.commission.shareHolder = {};
                body.commission.shareHolder.parentGroup = currentAgent.parentId;
                body.commission.shareHolder.group = currentAgent._id;
                body.commission.shareHolder.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.shareHolder.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.shareHolder.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.shareHolder.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.shareHolder.shareReceive = currentPercentReceive;
                body.commission.shareHolder.commission = agentCommission;
                body.commission.shareHolder.amount = getMoney;
                break;
            case 'SENIOR':
                body.commission.senior = {};
                body.commission.senior.parentGroup = currentAgent.parentId;
                body.commission.senior.group = currentAgent._id;
                body.commission.senior.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.senior.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.senior.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.senior.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.senior.shareReceive = currentPercentReceive;
                body.commission.senior.commission = agentCommission;
                body.commission.senior.amount = getMoney;
                break;
            case 'MASTER_AGENT':
                body.commission.masterAgent = {};
                body.commission.masterAgent.parentGroup = currentAgent.parentId;
                body.commission.masterAgent.group = currentAgent._id;
                body.commission.masterAgent.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.masterAgent.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.masterAgent.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.masterAgent.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.masterAgent.shareReceive = currentPercentReceive;
                body.commission.masterAgent.commission = agentCommission;
                body.commission.masterAgent.amount = getMoney;
                break;
            case 'AGENT':
                body.commission.agent = {};
                body.commission.agent.parentGroup = currentAgent.parentId;
                body.commission.agent.group = currentAgent._id;
                body.commission.agent.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.agent.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.agent.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.agent.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.agent.shareReceive = currentPercentReceive;
                body.commission.agent.commission = agentCommission;
                body.commission.agent.amount = getMoney;
                break;
        }

        prepareCommission(memberInfo, body, currentAgent.parentId, currentAgent, totalRemaining, overAllShare);
    }
}


function updateAgentMemberBalance(shareReceive, betAmount, ref1, updateCallback) {

    console.log('updateAgentMemberBalance');
    console.log('shareReceive : ', shareReceive);

    let balance = betAmount + shareReceive.member.totalWinLoseCom;

    try {
        console.log('update member balance : ', balance);

        MemberService.updateBalance(shareReceive.member.id, balance, shareReceive.betId, 'SLOT_LIVE22_SETTLE', ref1, function (err, updateBalanceResponse) {
            if (err) {
                updateCallback(err, null);
            } else {
                updateCallback(null, updateBalanceResponse);
            }
        });
    } catch (err) {
        MemberService.updateBalance(shareReceive.member.id, balance, shareReceive.betId, 'SLOT_LIVE22_SETTLE_ERROR ' + shareReceive.member.id + ' : ' + balance, '', function (err, updateBalanceResponse) {
            if (err) {
                updateCallback(err, null);
            } else {
                updateCallback(null, updateBalanceResponse);
            }
        });
    }

}

module.exports = router;
