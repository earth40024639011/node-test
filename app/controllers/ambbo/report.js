const express       = require('express');
const router        = express.Router();
const CashService   = require('../../common/cashService');
const _             = require('underscore');
const waterfall     = require("async/waterfall");
const RedisServiceReport = require('./redisReport');

router.post('/', (req, res) => {
    const {
        username,
        dateFrom,
        dateTo
    } = req.body;
    console.log('/ambbo/report', JSON.stringify(req.body));
    const usernameUpperCase = username.toUpperCase();
    waterfall([
            (callback) => {
                RedisServiceReport.getByKey(usernameUpperCase, (error, response) => {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, response);
                    }
                });
            }
        ],
        (error, dataRedis) => {
            if (error) {
                return res.send({
                    code: 7041,
                    message: "Report service error "+error
                });
            } else {
                if (_.isNull(dataRedis)) {
                    console.log('/ambbo/report query new '+ usernameUpperCase);
                    CashService.getWinLoseReportMember2(usernameUpperCase, dateFrom, dateTo,(error, wlResponse) => {
                        if (error) {
                            if(error == 1004){
                                return res.send({code: 999, message: "User not found!"});
                            }
                            return res.send({code: 999, message: "Internal server error"});
                        } else {
                            const body = {
                                dateFrom,
                                dateTo,
                                wlResponse
                            };
                            RedisServiceReport.setByKey(usernameUpperCase, body, (error, result) => {

                            });
                            return res.send(
                                {
                                    code: 0,
                                    message: "success",
                                    result: {
                                        username: usernameUpperCase,
                                        data: wlResponse
                                    }
                                }
                            );

                        }
                    });
                } else {
                    if (_.isEqual(dataRedis.dateFrom, dateFrom) &&_.isEqual(dataRedis.dateTo, dateTo)) {
                        console.log('/ambbo/report query redis '+ usernameUpperCase);
                        return res.send(
                            {
                                code: 0,
                                message: "success",
                                result: {
                                    username: usernameUpperCase,
                                    data: dataRedis
                                }
                            }
                        );
                    } else {
                        console.log('/ambbo/report query new '+ usernameUpperCase);
                        CashService.getWinLoseReportMember2(usernameUpperCase, dateFrom, dateTo,(error, wlResponse) => {
                            if (error) {
                                if(error == 1004){
                                    return res.send({code: 999, message: "User not found!"});
                                }
                                return res.send({code: 999, message: "Internal server error"});
                            } else {
                                const body = {
                                    dateFrom,
                                    dateTo,
                                    wlResponse
                                };
                                RedisServiceReport.setByKey(usernameUpperCase, body, (error, result) => {

                                });
                                return res.send(
                                    {
                                        code: 0,
                                        message: "success",
                                        result: {
                                            username: usernameUpperCase,
                                            data: wlResponse
                                        }
                                    }
                                );

                            }
                        });
                    }
                }
            }
        });
});


router.delete('/:key', (req, res) => {
    const {
        key
    } = req.params;
    RedisServiceReport.deleteByKey(key, (error, result) => {

    });
    return res.send({
        message: "success",
        code: 0,
        result : `username[${key}] was deleted`
    });
});


module.exports = router;