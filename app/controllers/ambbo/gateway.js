const express = require('express');
const router  = express.Router();
const request = require('request');
const AgentService = require('../../common/agentService');
const _       = require('underscore');


const getClientKey = require('../getClientKey');
let {
    URL_ENCRYPTED = 'https://ambbet-topup.serverless-hub.com/secure/encrypted',
    URL_AMBBO = 'https://ambbo.co/login/auto'
} = getClientKey.getKey();

const CLIENT_NAME         = process.env.CLIENT_NAME          || 'SPORTBOOK88';


router.get('/', (req, res) =>{
    const {
        username_lower,
        group
    } = req.userInfo;

    AgentService.findById(group._id,  (error, response) => {
        if (error) {
            return res.send({
                message: error,
                code: 9995
            });
        } else {
            const {
                prefixAutoTopUp
            } = response;

            if (!_.isUndefined(prefixAutoTopUp)) {
                request.get(
                    `${URL_ENCRYPTED}/${username_lower}`,
                    (error, result) => {
                        if (error) {
                            return res.send({
                                message: error,
                                code: 9994
                            });
                        } else {
                            const key = JSON.parse(result.body).encrypted;
                            return res.send({
                                result: `${URL_AMBBO}?prefix=${prefixAutoTopUp}&token=${key}`,
                                code: 0
                            });
                        }
                    });
            } else {
                return res.send({
                    message: 'This user are not allow to use auto-topup',
                    code: 8002
                });
            }
        }
    });
});

module.exports = router;