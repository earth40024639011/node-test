const express = require('express');
const router = express.Router();
const _ = require('underscore');
const async = require("async");
const roundTo = require('round-to');
const NumberUtils = require('../../common/numberUtils1');
const AgentService = require('../../common/agentService');
const PgService = require('../../common/pgService');
const Joi = require('joi');
const MemberService = require('../../common/memberService');
const DateUtils = require('../../common/dateUtils');
const AgentGroupModel            = require('../../models/agentGroup.model.js');
const BetTransactionModel = require('../../models/betTransaction.model.js');
const TokenModel = require('../../models/token.model');
const PgSlotHistoryModel = require('../../models/pgSlotHistory.model');

const SECRET_KEY = process.env.PG_SECRETKEY ||'72e3ded5e96b750560110c47ff9944c9';
const OPERATOR_TOKEN = process.env.PG_TOKEN ||'fd8e66ef36cbe310501481de441cd557';

const DRAW = "DRAW";
const WIN = "WIN";
const HALF_WIN = "HALF_WIN";
const LOSE = "LOSE";
const HALF_LOSE = "HALF_LOSE";
const CURRENCY = "THB";


function random(low, high) {
    return Math.floor(Math.random() * (high - low + 1) + low)
}
function generateBetId() {
    return 'BET' + new Date().getTime() + random(1000000, 9999999);
}

router.get('/list-games', function (req, res) {
    let gameList = PgService.callGetGameList();
    return res.send({
        code : 0,
        result : gameList,
        message : 'success'
    })
});


const forwardGame = Joi.object().keys({
    gameCode: Joi.string().required()
});

router.post('/forward-to-game', function (req, res) {
    let validate = Joi.validate(req.body, forwardGame);
    if (validate.error) {
        return res.send({
            code : 999,
            message : 'Invalid parameters',
            result : ''
        })
    }
    const userInfo       = req.userInfo;
    let requestBody = req.body;

    async.waterfall([callback => {
        try {
            AgentService.getUpLineStatus(userInfo.group._id, function (err, status) {
                if (err) {
                    callback(err, null);
                } else {
                    if (status.suspend) {
                        callback(5011, null);
                    } else {
                        callback(null, status);
                    }
                }
            });
        } catch (err) {
            callback(9929, null);
        }

    }, (uplineStatus, callback) => {

        try {
            MemberService.findById(userInfo._id, 'creditLimit balance limitSetting group suspend username_lower', function (err, memberResponse) {
                if (err) {
                    callback(err, null);
                } else {

                    if (memberResponse.suspend) {
                        callback(5011, null);
                    } else if (memberResponse.limitSetting.game.slotXO.isEnable) {
                        AgentService.getLive22Status(memberResponse.group._id, (err, response) => {
                            if (!response.isEnable) {
                                callback(1088, null);
                            } else {
                                callback(null, uplineStatus, memberResponse);
                            }
                        });
                    } else {
                        callback(1088, null);
                    }
                }
            });
        } catch (err) {
            callback(999, null);
        }

    }], function (err, uplineStatus, memberInfo) {

        if (err) {
            if (err == 1088) {
                return res.send({
                    code: 1088,
                    message: 'Service Locked, Please contact your upline.'
                });
            }
            if (err == 5011) {
                return res.send({
                    message: "Account or Upline was suspend.",
                    result: null,
                    code: 5011
                });
            }
            return res.send({code: 999, message: err});
        }

        TokenModel.findOne({username: userInfo.username}, function (err, response) {
            console.log('find by token ',response);
            if(response) {
                PgService.forwardToGame(response.uniqueId, requestBody.gameCode, (err, result) =>{
                    // console.log('---------forwardToGame----------',result);
                    return res.send({
                        code : 0,
                        message : 'success',
                        result : result
                    })
                })
            } else {
                return res.send({
                    code : 1,
                    message : 'Token not found.',
                    result : ''
                })
            }

        });
    });

});

router.post('/callback/VerifySession', function (req, res) {

    let requestBody = req.body;

    console.log('request VerifySession =========== ',requestBody);

    if(requestBody.secret_key != SECRET_KEY || requestBody.operator_token != OPERATOR_TOKEN){
        return res.send({
            "data": null,
            "error": {
                "code": "1034",
                "message": "Invalid request."
            }
        });
    }

    TokenModel.findOne({uniqueId: requestBody.operator_player_session}, function (err, response) {
        // console.log('token = ',response);
        if(response) {
            MemberService.findByUserNameForPartnerService(response.username, (errr, memberResponse) => {
                // console.log('memberResponse================= ',memberResponse);
                if (memberResponse) {

                    let credit = roundTo((memberResponse.creditLimit + memberResponse.balance), 2);

                    return res.send({
                        data : {
                            player_name : memberResponse.username,
                            nickname : memberResponse.username,
                            currency : CURRENCY,
                            reminder_time: new Date().getTime()
                        },
                        error : null
                    });

                } else {
                    return res.send({
                        "data": null,
                        "error": {
                            "code": "1034",
                            "message": "Invalid request."
                        }
                    });
                }

            });
        } else {
            return res.send({
                "data": null,
                "error": {
                    "code": "1034",
                    "message": "Invalid request."
                }
            });
        }

    });

});

const getBalanceSchema = Joi.object().keys({
    secret_key: Joi.string().required(),
    player_name: Joi.string().required(),
    operator_player_session: Joi.string().required(),
    operator_token: Joi.string().required(),
    game_id : Joi.string().allow('')
});

router.post('/callback/Cash/Get', function (req, res) {
    // let validate = Joi.validate(req.body, getBalanceSchema);
    // if (validate.error) {
    //     return res.send({
    //         "data": null,
    //         "error": {
    //             "code": "3001",
    //             "message": "Value cannot be null."
    //         }
    //     });
    // }
    let requestBody = req.body;
    console.log('=====Get Balance11 ================================== ', requestBody);
    if(!requestBody.secret_key || !requestBody.player_name || !requestBody.operator_player_session || !requestBody.operator_token){
        console.error('errorororororororo')
        return res.send({
            "data": null,
            "error": {
                "code": "3001",
                "message": "Value cannot be null."
            }
        });
    }
    console.log('=====Get Balance ================================== ', requestBody);

    if(requestBody.secret_key != SECRET_KEY || requestBody.operator_token != OPERATOR_TOKEN){
        return res.send({
            "data": null,
            "error": {
                "code": "1034",
                "message": "Invalid request."
            }
        });
    }

    MemberService.findByUserName(requestBody.player_name,  (err, memberResponse) => {

        if (err) {
            return res.send({
                "data": null,
                "error": {
                    "code": "1200",
                    "message": "Internal server error."
                }
            });
        }

        if (memberResponse) {

            let credit = roundTo((memberResponse.creditLimit + memberResponse.balance), 2);
            // console.log('credit ============ ',credit);
            return res.send({
                data : {
                    balance_amount : credit,
                    currency_code : CURRENCY,
                    updated_time: new Date().getTime()
                },
                error : null
            });

        } else {
            return res.send({
                "data": null,
                "error": {
                    "code": "3004",
                    "message": "Player doesn’t exist."
                }
            });
        }

    });
});

const getBetSchema = Joi.object().keys({
    secret_key: Joi.string().required(),
    player_name: Joi.string().required(),
    operator_player_session: Joi.string().allow(''),
    operator_token: Joi.string().required(),
    game_id: Joi.string().required(),
    parent_bet_id: Joi.string().required(),
    bet_id: Joi.string().required(),
    currency_code: Joi.string().required(),
    create_time: Joi.string().required(),
    updated_time: Joi.string().required(),
    transfer_amount: Joi.string().required(),
    transaction_id: Joi.string().required(),
    wallet_type: Joi.string().allow(''),
    bet_type: Joi.string().allow(''),
    is_feature: Joi.string().allow(''),
    is_minus_count: Joi.string().allow(''),
    is_wager: Joi.string().allow(''),
    platform: Joi.string().allow(''),
    is_parent_zero_stake: Joi.string().allow(''),
    free_game_transaction_id: Joi.string().allow(''),
    bonus_transaction_id: Joi.string().allow(''),
    jackpot_rtp_contribution_amount: Joi.string().allow(''),
    jackpot_win_amount: Joi.string().allow(''),
    is_validate_bet: Joi.string().allow('')
});

router.post('/callback/Cash/TransferOut', function (req, res) {
    // let validate = Joi.validate(req.body, getBetSchema);
    // if (validate.error) {
    //     return res.send({
    //         "data": null,
    //         "error": {
    //             "code": "3001",
    //             "message": "Value cannot be null."
    //         }
    //     });
    // }

    let requestBody = req.body;

    console.log('TransferOut1==============================',requestBody);

    if((requestBody.is_validate_bet && requestBody.is_validate_bet === 'True') ||(requestBody.is_adjustment && requestBody.is_adjustment === 'True') ){
        console.error('----------------True------------------')
    }else {
        console.error('----------------False------------------')
        if(!requestBody.operator_player_session){
            return res.send({
                "data": null,
                "error": {
                    "code": "3001",
                    "message": "Value cannot be null."
                }
            });
        }

        if(requestBody.operator_token != OPERATOR_TOKEN){
            return res.send({
                "data": null,
                "error": {
                    "code": "1034",
                    "message": "Invalid request."
                }
            });
        }
    }

    if(!requestBody.secret_key || !requestBody.player_name || !requestBody.operator_token || !requestBody.parent_bet_id || !requestBody.game_id
        || !requestBody.bet_id || !requestBody.currency_code || !requestBody.create_time || !requestBody.updated_time || !requestBody.transfer_amount || !requestBody.transaction_id){
        return res.send({
            "data": null,
            "error": {
                "code": "3001",
                "message": "Value cannot be null."
            }
        });
    }
    console.log('TransferOut ================================== ', requestBody);

    if(requestBody.secret_key != SECRET_KEY || requestBody.currency_code != CURRENCY){
        return res.send({
            "data": null,
            "error": {
                "code": "1034",
                "message": "Invalid request."
            }
        });
    }

    requestBody.transfer_amount = roundTo(Number.parseFloat(requestBody.transfer_amount), 2);
    requestBody.jackpot_rtp_contribution_amount = roundTo(Number.parseFloat(requestBody.jackpot_rtp_contribution_amount), 2);
    requestBody.jackpot_win_amount = roundTo(Number.parseFloat(requestBody.jackpot_win_amount), 2);

    let hisModel = new PgSlotHistoryModel(requestBody);
    hisModel.save((err, hisRes) => {
        if (err) {
            console.log('-----------PgSlotHistoryModel fail------------')
        } else {
            console.log('-----------PgSlotHistoryModel success------------')
        }
    });

    let betAmount = roundTo(Number.parseFloat(requestBody.transfer_amount), 2);

    if(betAmount < 0){
        return res.send({
            "data": null,
            "error": {
                "code": "3003",
                "message": "Bet failed."
            }
        });
    }

    async.series([callback => {

        MemberService.findByUserName(requestBody.player_name, function (err, memberResponse) {
            if (err) {
                callback(err, null);
                return;
            }

            if (memberResponse) {
                let credit = roundTo((memberResponse.creditLimit + memberResponse.balance), 2);
                // console.log('credit ==== ',credit);
                if (typeof credit !== 'number' || _.isUndefined(credit) || _.isNaN(credit)) {
                    callback(900606, memberResponse);
                }else {
                    if (credit < betAmount) {
                        callback(900605, memberResponse);
                    } else {
                        callback(null, memberResponse);
                    }
                }
            }else {
                callback(999999, null);
            }
        });

    }], (err, asyncResponse) => {

        if (err) {
            if (err == 900605) {

                return res.send({
                    "data": null,
                    "error": {
                        "code": "3202",
                        "message": "No enough cash balance to bet."
                    }
                });

            } else {
                return res.send({
                    "data": null,
                    "error": {
                        "code": "1034",
                        "message": "Invalid request."
                    }
                });
            }
        }

        let memberInfo = asyncResponse[0];

        let credit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2);

        if(credit < betAmount){
            return res.send({
                "data": null,
                "error": {
                    "code": "3202",
                    "message": "No enough cash balance to bet."
                }
            });
        }

        let condition = {
            'ref1': `${requestBody.player_name}_${requestBody.transaction_id}`
        };

        BetTransactionModel.findOne(condition, (err, response) => {
            if(response){
                //duplicate transaction

                console.log('-------------------duplicate---------------------')

                return res.send({
                    data : {
                        balance_amount : credit,
                        currency_code : CURRENCY,
                        updated_time: requestBody.updated_time
                    },
                    error : null
                });
            }else{
                AgentGroupModel.findById(memberInfo.group).select('_id type parentId shareSetting commissionSetting').populate({
                    path: 'parentId',
                    select: '_id type parentId shareSetting commissionSetting name',
                    populate: {
                        path: 'parentId',
                        select: '_id type parentId shareSetting commissionSetting name',
                        populate: {
                            path: 'parentId',
                            select: '_id type parentId shareSetting commissionSetting name',
                            populate: {
                                path: 'parentId',
                                select: '_id type parentId shareSetting commissionSetting name',
                                populate: {
                                    path: 'parentId',
                                    select: '_id type parentId shareSetting commissionSetting name',
                                    populate: {
                                        path: 'parentId',
                                        select: '_id type parentId shareSetting commissionSetting name',
                                    }
                                }
                            }
                        }
                    }

                }).exec(function (err, agentGroups) {

                    let body = {
                        memberId: memberInfo._id,
                        memberParentGroup: memberInfo.group,
                        memberUsername: memberInfo.username_lower,
                        betId: generateBetId(),
                        slot: {
                            pg : {
                                transactionId: requestBody.transaction_id,
                                amount : betAmount,
                                gameName: PgService.getGameName(requestBody.game_id),
                                pgBetId : requestBody.bet_id,
                                winLoss : betAmount * -1
                            }
                        },
                        amount: betAmount,
                        memberCredit: betAmount * -1,
                        payout: 0,
                        validAmount: betAmount,
                        gameType: 'TODAY',
                        acceptHigherPrice: false,
                        priceType: 'TH',
                        game: 'GAME',
                        source: 'PG_SLOT',
                        status: 'RUNNING',
                        commission: {},
                        gameDate: DateUtils.getCurrentDate(),
                        createdDate: DateUtils.getCurrentDate(),
                        isEndScore: false,
                        currency: memberInfo.currency,
                        ipAddress: memberInfo.ipAddress,
                        ref1 : `${requestBody.player_name}_${requestBody.transaction_id}`
                    };

                    prepareCommission(memberInfo, body, agentGroups, null, 0, 0);

                    let betTransactionModel = new BetTransactionModel(body);

                    betTransactionModel.save(function (err, response) {
                        if (err) {
                            return res.send({
                                "data": null,
                                "error": {
                                    "code": "1200",
                                    "message": "Internal server error."
                                }
                            });
                        }else {
                            MemberService.updateBalance(memberInfo._id, (betAmount * -1), response.betId, 'BET_PGSLOT', requestBody.transaction_id, function (err, updateBalanceResponse) {
                                if (err) {
                                    return res.send({
                                        "data": null,
                                        "error": {
                                            "code": "1200",
                                            "message": "Internal server error."
                                        }
                                    });
                                } else {

                                    return res.send({
                                        data : {
                                            balance_amount : updateBalanceResponse.newBalance,
                                            currency_code : CURRENCY,
                                            updated_time: requestBody.updated_time
                                        },
                                        error : null
                                    });
                                }
                            });
                        }

                    });
                });
            }
        })

    });
});

const settleSchema = Joi.object().keys({
    secret_key: Joi.string().required(),
    player_name: Joi.string().required(),
    operator_player_session: Joi.string().allow(''),
    operator_token: Joi.string().required(),
    game_id: Joi.string().required(),
    parent_bet_id: Joi.string().required(),
    bet_id: Joi.string().required(),
    currency_code: Joi.string().required(),
    create_time: Joi.string().required(),
    updated_time: Joi.string().required(),
    transfer_amount: Joi.string().required(),
    is_validate_bet: Joi.string().allow(''),
    transaction_id: Joi.string().required(),
    bet_transaction_id: Joi.string().allow(''),
    wallet_type: Joi.string().allow(''),
    bet_type: Joi.string().allow(''),
    is_end_round: Joi.string().allow(''),
    is_feature: Joi.string().allow(''),
    is_minus_count: Joi.string().allow(''),
    is_wager: Joi.string().allow(''),
    platform: Joi.string().allow(''),
    is_parent_zero_stake: Joi.string().allow(''),
    bonus_transaction_id: Joi.string().allow(''),
    free_game_transaction_id: Joi.string().allow(''),
    free_game_name: Joi.string().allow(''),
    free_game_id: Joi.string().allow(''),
    jackpot_rtp_contribution_amount: Joi.string().allow(''),
    jackpot_win_amount: Joi.string().allow(''),
    jackpot_pool_id: Joi.string().allow(''),
    jackpot_type: Joi.string().allow('')
});

router.post('/callback/Cash/TransferIn', function (req, res) {
    // let validate = Joi.validate(req.body, settleSchema);
    // if (validate.error) {
    //     // console.log('validate ====================== ',validate.error);
    //     return res.send({
    //         "data": null,
    //         "error": {
    //             "code": "3001",
    //             "message": "Value cannot be null."
    //         }
    //     });
    // }

    let requestBody = req.body;

    if((requestBody.is_validate_bet && requestBody.is_validate_bet === 'True') ||(requestBody.is_adjustment && requestBody.is_adjustment === 'True') ){
        console.error('----------------True------------------')
    }else {
        console.error('----------------False------------------')
        if(!requestBody.operator_player_session){
            return res.send({
                "data": null,
                "error": {
                    "code": "3001",
                    "message": "Value cannot be null."
                }
            });
        }

        if(requestBody.operator_token != OPERATOR_TOKEN){
            return res.send({
                "data": null,
                "error": {
                    "code": "1034",
                    "message": "Invalid request."
                }
            });
        }
    }
    if(!requestBody.secret_key || !requestBody.player_name || !requestBody.operator_token || !requestBody.parent_bet_id || !requestBody.game_id
        || !requestBody.bet_id || !requestBody.currency_code || !requestBody.create_time || !requestBody.updated_time || !requestBody.transfer_amount || !requestBody.transaction_id
        || !requestBody.is_end_round){
        console.error('---------TransferIn---------')
        return res.send({
            "data": null,
            "error": {
                "code": "3001",
                "message": "Value cannot be null."
            }
        });
    }

    console.log('TransferIn==============================',requestBody);

    if(requestBody.secret_key != SECRET_KEY || requestBody.currency_code != CURRENCY){
        return res.send({
            "data": null,
            "error": {
                "code": "1034",
                "message": "Invalid request."
            }
        });
    }

    requestBody.transfer_amount = roundTo(Number.parseFloat(requestBody.transfer_amount), 2);
    requestBody.jackpot_rtp_contribution_amount = roundTo(Number.parseFloat(requestBody.jackpot_rtp_contribution_amount), 2);
    requestBody.jackpot_win_amount = roundTo(Number.parseFloat(requestBody.jackpot_win_amount), 2);
    requestBody.jackpot_pool_id = roundTo(Number.parseFloat(requestBody.jackpot_pool_id), 2);
    requestBody.jackpot_type = roundTo(Number.parseFloat(requestBody.jackpot_type), 2);

    let hisModel = new PgSlotHistoryModel(requestBody);
    hisModel.save((err, hisRes) => {
        if (err) {
            console.log('-----------PgSlotHistoryModel fail------------')
        } else {
            console.log('-----------PgSlotHistoryModel success------------')
        }
    });

    let betAmount = roundTo(Number.parseFloat(requestBody.transfer_amount), 2);

    if(betAmount < 0){
        return res.send({
            "data": null,
            "error": {
                "code": "3034",
                "message": "Payout failed."
            }
        });
    }

    async.series([callback => {

        MemberService.findByUserName(requestBody.player_name, function (err, memberResponse) {
            if (err) {
                return res.send({
                    "data": null,
                    "error": {
                        "code": "1034",
                        "message": "Invalid request."
                    }
                });
            }else if(memberResponse){
                callback(null, memberResponse);
            }else {
                return res.send({
                    "data": null,
                    "error": {
                        "code": "1034",
                        "message": "Invalid request."
                    }
                });
            }

        });

    }], (err, asyncResponse) => {

        if (err) {
            return res.send({
                "data": null,
                "error": {
                    "code": "1200",
                    "message": "Internal server error."
                }
            });
        }

        let memberInfo = asyncResponse[0];

        let credit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2);

        let transType = requestBody.transaction_id.split('-');

        if(transType[2] == 400 || transType[2] == 403){
            //Bonus
            let condition = {
                'ref1': `${requestBody.player_name}_${requestBody.transaction_id}`
            };

            BetTransactionModel.findOne(condition, (err, response) => {
                console.log('-----res----',response);
                if(response){
                    //duplicate transaction

                    console.log(SECRET_KEY, '-------------------duplicate---------------------')

                    return res.send({
                        data : {
                            balance_amount : credit,
                            currency_code : CURRENCY,
                            updated_time: requestBody.updated_time
                        },
                        error : null
                    });
                }else{
                    AgentGroupModel.findById(memberInfo.group).select('_id type parentId shareSetting commissionSetting').populate({
                        path: 'parentId',
                        select: '_id type parentId shareSetting commissionSetting name',
                        populate: {
                            path: 'parentId',
                            select: '_id type parentId shareSetting commissionSetting name',
                            populate: {
                                path: 'parentId',
                                select: '_id type parentId shareSetting commissionSetting name',
                                populate: {
                                    path: 'parentId',
                                    select: '_id type parentId shareSetting commissionSetting name',
                                    populate: {
                                        path: 'parentId',
                                        select: '_id type parentId shareSetting commissionSetting name',
                                        populate: {
                                            path: 'parentId',
                                            select: '_id type parentId shareSetting commissionSetting name',
                                        }
                                    }
                                }
                            }
                        }

                    }).exec(function (err, agentGroups) {

                        let body = {
                            memberId: memberInfo._id,
                            memberParentGroup: memberInfo.group,
                            memberUsername: memberInfo.username_lower,
                            betId: generateBetId(),
                            slot: {
                                pg : {
                                    transactionId: requestBody.transaction_id,
                                    amount : betAmount,
                                    gameName: PgService.getGameName(requestBody.game_id),
                                    pgBetId : requestBody.bet_id,
                                    winLoss : betAmount
                                }
                            },
                            amount: 0,
                            memberCredit: 0,
                            payout: betAmount,
                            validAmount: 0,
                            gameType: 'TODAY',
                            acceptHigherPrice: false,
                            priceType: 'TH',
                            game: 'GAME',
                            source: 'PG_SLOT',
                            status: 'DONE',
                            commission: {},
                            gameDate: DateUtils.getCurrentDate(),
                            createdDate: DateUtils.getCurrentDate(),
                            isEndScore: false,
                            currency: memberInfo.currency,
                            ipAddress: memberInfo.ipAddress,
                            settleDate :DateUtils.getCurrentDate(),
                            ref1 : `${requestBody.player_name}_${requestBody.transaction_id}`
                        };

                        prepareCommission(memberInfo, body, agentGroups, null, 0, 0);

                        let winLose = betAmount;
                        let betResult = winLose > 0 ? WIN : winLose < 0 ? LOSE : DRAW;

                        //init
                        if (!body.commission.senior) {
                            body.commission.senior = {};
                        }

                        if (!body.commission.masterAgent) {
                            body.commission.masterAgent = {};
                        }

                        if (!body.commission.agent) {
                            body.commission.agent = {};
                        }

                        // console.log('body === ',body);
                        const shareReceive = AgentService.prepareShareReceive(winLose, betResult, body);


                        body.commission.superAdmin.winLoseCom = shareReceive.superAdmin.winLoseCom;
                        body.commission.superAdmin.winLose = shareReceive.superAdmin.winLose;
                        body.commission.superAdmin.totalWinLoseCom = shareReceive.superAdmin.totalWinLoseCom;

                        body.commission.company.winLoseCom = shareReceive.company.winLoseCom;
                        body.commission.company.winLose = shareReceive.company.winLose;
                        body.commission.company.totalWinLoseCom = shareReceive.company.totalWinLoseCom;

                        body.commission.shareHolder.winLoseCom = shareReceive.shareHolder.winLoseCom;
                        body.commission.shareHolder.winLose = shareReceive.shareHolder.winLose;
                        body.commission.shareHolder.totalWinLoseCom = shareReceive.shareHolder.totalWinLoseCom;

                        body.commission.senior.winLoseCom = shareReceive.senior.winLoseCom;
                        body.commission.senior.winLose = shareReceive.senior.winLose;
                        body.commission.senior.totalWinLoseCom = shareReceive.senior.totalWinLoseCom;

                        body.commission.masterAgent.winLoseCom = shareReceive.masterAgent.winLoseCom;
                        body.commission.masterAgent.winLose = shareReceive.masterAgent.winLose;
                        body.commission.masterAgent.totalWinLoseCom = shareReceive.masterAgent.totalWinLoseCom;

                        body.commission.agent.winLoseCom = shareReceive.agent.winLoseCom;
                        body.commission.agent.winLose = shareReceive.agent.winLose;
                        body.commission.agent.totalWinLoseCom = shareReceive.agent.totalWinLoseCom;

                        body.commission.member.winLoseCom = shareReceive.member.winLoseCom;
                        body.commission.member.winLose = shareReceive.member.winLose;
                        body.commission.member.totalWinLoseCom = shareReceive.member.totalWinLoseCom;
                        body.betResult = betResult;

                        console.log('body = ', body);
                        let betTransactionModel = new BetTransactionModel(body);

                        betTransactionModel.save(function (err, response) {
                            // console.log('err ===== ',err);
                            if (err) {
                                return res.send({
                                    "data": null,
                                    "error": {
                                        "code": "1200",
                                        "message": "Internal server error."
                                    }
                                });
                            }else {
                                MemberService.updateBalance(memberInfo._id, betAmount, response.betId, 'PG_BONUS', requestBody.transaction_id, function (err, updateBalanceResponse) {
                                    if (err) {
                                        return res.send({
                                            "data": null,
                                            "error": {
                                                "code": "1200",
                                                "message": "Internal server error."
                                            }
                                        });
                                    } else {
                                        return res.send({
                                            data : {
                                                balance_amount : updateBalanceResponse.newBalance,
                                                currency_code : CURRENCY,
                                                updated_time: requestBody.updated_time
                                            },
                                            error : null
                                        });
                                    }
                                });
                            }

                        });
                    });
                }
            })

        }else {
            //Settle bet
            async.series([callback => {

                let condition = {
                    'ref1': `${requestBody.player_name}_${requestBody.bet_transaction_id}`
                };

                BetTransactionModel.findOne(condition).lean().exec(function (err, mainObj) {
                    // console.log('active : ',mainObj);
                    if (err) {
                        callback(err, null);
                        return
                    }

                    if (!mainObj) {
                        callback(900415, null);
                    } else {

                        if (mainObj.status == 'DONE') {
                            callback(900409, null)
                        }else {
                            callback(null, mainObj);
                        }
                    }

                });

            }], (err, asyncResponse) => {

                let activeBetObj = asyncResponse[0];

                // console.log('activeBetObj======= =================',activeBetObj);
                if (err) {

                    if (err == 900415) {
                        return res.send({
                            "data": null,
                            "error": {
                                "code": "3021",
                                "message": "No bet exists."
                            }
                        });
                    }

                    if (err == 900409) {
                        return res.send({
                            data : {
                                balance_amount : credit,
                                currency_code : CURRENCY,
                                updated_time: requestBody.updated_time
                            },
                            error : null
                        });
                    }
                }

                let winLose = betAmount - activeBetObj.amount;
                let betResult = winLose > 0 ? 'WIN' : winLose < 0 ? 'LOSE' : 'DRAW';

                const shareReceive = AgentService.prepareShareReceive(winLose, betResult, activeBetObj);

                let updateBody = {
                    $set: {
                        'slot.pg.winLoss': winLose,
                        'commission.superAdmin.winLoseCom': shareReceive.superAdmin.winLoseCom,
                        'commission.superAdmin.winLose': shareReceive.superAdmin.winLose,
                        'commission.superAdmin.totalWinLoseCom': shareReceive.superAdmin.totalWinLoseCom,

                        'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                        'commission.company.winLose': shareReceive.company.winLose,
                        'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                        'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                        'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                        'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                        'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                        'commission.senior.winLose': shareReceive.senior.winLose,
                        'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                        'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                        'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                        'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                        'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                        'commission.agent.winLose': shareReceive.agent.winLose,
                        'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                        'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                        'commission.member.winLose': shareReceive.member.winLose,
                        'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                        'betResult': betResult,
                        'isEndScore': true,
                        'status': 'DONE'
                    }
                };

                BetTransactionModel.update({_id: activeBetObj._id}, updateBody, function (err, response) {
                    if (response.nModified == 1) {
                        MemberService.updateBalance(memberInfo._id, betAmount, activeBetObj.betId, 'SETTLE_PGSLOT', requestBody.transaction_id, function (err, creditResponse) {
                            if (err) {
                                return res.send({
                                    "data": null,
                                    "error": {
                                        "code": "1200",
                                        "message": "Internal server error."
                                    }
                                });
                            } else {

                                return res.send({
                                    data : {
                                        balance_amount : creditResponse.newBalance,
                                        currency_code : CURRENCY,
                                        updated_time: requestBody.updated_time
                                    },
                                    error : null
                                });
                            }
                        });
                    } else {
                        return res.send({
                            "data": null,
                            "error": {
                                "code": "1200",
                                "message": "Internal server error."
                            }
                        });
                    }
                });


            });
        }
    });

});


function prepareCommission(memberInfo, body, currentAgent, overAgent, remaining, overAllShare) {

    if (currentAgent && (currentAgent.type === 'SUPER_ADMIN' || currentAgent.parentId)) {

        let money = body.amount;

        if (!overAgent) {
            overAgent = {};
            overAgent.type = 'member';
            overAgent.shareSetting = {};
            overAgent.shareSetting.game = {};
            overAgent.shareSetting.game.slotXO = {};
            overAgent.shareSetting.game.slotXO.parent = memberInfo.shareSetting.game.slotXO.parent;
            overAgent.shareSetting.game.slotXO.own = 0;
            overAgent.shareSetting.game.slotXO.remaining = 0;

            body.commission.member = {};
            body.commission.member.parent = memberInfo.shareSetting.game.slotXO.parent;
            body.commission.member.commission = memberInfo.commissionSetting.game.slotXO;

        }

        let currentPercentReceive = overAgent.shareSetting.game.slotXO.parent;
        let totalRemaining = remaining;

        if (overAgent.shareSetting.game.slotXO.remaining > 0) {

            if (overAgent.shareSetting.game.slotXO.remaining > totalRemaining) {
                currentPercentReceive += totalRemaining;
                totalRemaining = 0;
            } else {
                totalRemaining = remaining - overAgent.shareSetting.game.slotXO.remaining;
                currentPercentReceive += overAgent.shareSetting.game.slotXO.remaining;
            }

        }

        let unUseShare = currentAgent.shareSetting.game.slotXO.own -
            (overAgent.shareSetting.game.slotXO.parent + overAgent.shareSetting.game.slotXO.own);

        totalRemaining = (unUseShare + totalRemaining);

        if (currentAgent.shareSetting.game.slotXO.min > 0 && ((overAllShare + currentPercentReceive) < currentAgent.shareSetting.game.slotXO.min)) {

            if (currentPercentReceive < currentAgent.shareSetting.game.slotXO.min) {
                let percent = currentAgent.shareSetting.game.slotXO.min - (currentPercentReceive + overAllShare);
                totalRemaining = totalRemaining - percent;
                if (totalRemaining < 0) {
                    totalRemaining = 0;
                }
                currentPercentReceive += percent;
            }
        } else {
            // currentPercentReceive += totalRemaining;
        }


        if (currentAgent.type === 'SUPER_ADMIN') {
            currentPercentReceive += totalRemaining;
        }

        let getMoney = (money * NumberUtils.convertPercent(currentPercentReceive)).toFixed(2);
        overAllShare = overAllShare + currentPercentReceive;

        //set commission
        let agentCommission = currentAgent.commissionSetting.game.slotXO;


        switch (currentAgent.type) {
            case 'SUPER_ADMIN':
                body.commission.superAdmin = {};
                body.commission.superAdmin.group = currentAgent._id;
                body.commission.superAdmin.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.superAdmin.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.superAdmin.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.superAdmin.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.superAdmin.shareReceive = Math.abs(currentPercentReceive);
                body.commission.superAdmin.commission = agentCommission;
                body.commission.superAdmin.amount = getMoney;
                break;
            case 'COMPANY':
                body.commission.company = {};
                body.commission.company.parentGroup = currentAgent.parentId;
                body.commission.company.group = currentAgent._id;
                body.commission.company.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.company.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.company.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.company.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.company.shareReceive = Math.abs(currentPercentReceive);
                body.commission.company.commission = agentCommission;
                body.commission.company.amount = getMoney;
                break;
            case 'SHARE_HOLDER':
                body.commission.shareHolder = {};
                body.commission.shareHolder.parentGroup = currentAgent.parentId;
                body.commission.shareHolder.group = currentAgent._id;
                body.commission.shareHolder.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.shareHolder.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.shareHolder.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.shareHolder.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.shareHolder.shareReceive = currentPercentReceive;
                body.commission.shareHolder.commission = agentCommission;
                body.commission.shareHolder.amount = getMoney;
                break;
            case 'SENIOR':
                body.commission.senior = {};
                body.commission.senior.parentGroup = currentAgent.parentId;
                body.commission.senior.group = currentAgent._id;
                body.commission.senior.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.senior.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.senior.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.senior.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.senior.shareReceive = currentPercentReceive;
                body.commission.senior.commission = agentCommission;
                body.commission.senior.amount = getMoney;
                break;
            case 'MASTER_AGENT':
                body.commission.masterAgent = {};
                body.commission.masterAgent.parentGroup = currentAgent.parentId;
                body.commission.masterAgent.group = currentAgent._id;
                body.commission.masterAgent.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.masterAgent.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.masterAgent.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.masterAgent.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.masterAgent.shareReceive = currentPercentReceive;
                body.commission.masterAgent.commission = agentCommission;
                body.commission.masterAgent.amount = getMoney;
                break;
            case 'AGENT':
                body.commission.agent = {};
                body.commission.agent.parentGroup = currentAgent.parentId;
                body.commission.agent.group = currentAgent._id;
                body.commission.agent.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.agent.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.agent.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.agent.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.agent.shareReceive = currentPercentReceive;
                body.commission.agent.commission = agentCommission;
                body.commission.agent.amount = getMoney;
                break;
        }

        prepareCommission(memberInfo, body, currentAgent.parentId, currentAgent, totalRemaining, overAllShare);
    }
}


module.exports = router;