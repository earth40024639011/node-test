const FootballModel = require('../../models/football.model');
const express = require('express');
const router  = express.Router();
const request = require('request');
const _ = require('underscore');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const async = require("async");

const hdpTotalPage = (callback) => {
    const headers = {
        'Content-Type' : 'application/x-www-form-urlencoded'
    };
    const options = {
        url: 'https://sb.1eighty8.com/th-th/Service/CentralService?GetData&ts='+ new Date().getTime(),
        method: 'POST',
        headers: headers,
        form: {
            IsFirstLoad   : true,
            VersionL      : -1,
            VersionU      : 0,
            VersionS      : -1,
            VersionF      : -1,
            VersionH      : 0,
            VersionT      : -1,
            IsEventMenu   : false,
            SportID       : 1,
            CompetitionID : -1,
            reqUrl        : '/th-th/sports/football/competition/full-time-asian-handicap-and-over-under',
            oIsInplayAll  : false,
            oIsFirstLoad  : true,
            oSortBy       : 1,
            oOddsType     : 0,
            oPageNo       : 1
        }
    };
    request(options, (err, response, body) => {
        if (err) {
            callback(err, null)
        } else {
            let result = JSON.parse(body);
            callback(null, result.mod.p.t);
        }
    });
};
const hdpAPI = (page, callback) => {
    const headers = {
        'Content-Type' : 'application/x-www-form-urlencoded',
        'Cookie' : 'settingProfile=OddsType%3D3'
    };
    const options = {
        url: 'https://sb.1eighty8.com/th-th/Service/CentralService?GetData&ts='+ new Date().getTime(),
        method: 'POST',
        headers: headers,
        form: {
            IsFirstLoad   : true,
            VersionL      : -1,
            VersionU      : 0,
            VersionS      : -1,
            VersionF      : -1,
            VersionH      : 0,
            VersionT      : -1,
            IsEventMenu   : false,
            SportID       : 1,
            CompetitionID : -1,
            reqUrl        : '/th-th/sports/football/competition/full-time-asian-handicap-and-over-under',
            oIsInplayAll  : false,
            oIsFirstLoad  : true,
            oSortBy       : 1,
            oOddsType     : 0,
            oPageNo       : page
        }
    };
    request(options, (err, response, body) => {
        if (err) {
            callback(err, null)
        } else {
            let result = JSON.parse(body);
            let masterData = JSON.stringify(_.first(result.mod.d));
            callback(null, mappingMasterData(masterData));
        }
    });
};

const liveTotalPage = (callback) => {
    const headers = {
        'Content-Type' : 'application/x-www-form-urlencoded'
    };

    let options = {
        url: 'https://sb.1eighty8.com/th-th/Service/CentralService?GetData&ts='+ new Date().getTime(),
        method: 'POST',
        headers: headers,
        form: {
            IsFirstLoad  : true,
            VersionL      : -1,
            VersionU      : 0,
            VersionS      : -1,
            VersionF      : -1,
            VersionH      : '1:0,2:0,3:0,4:0,9:0,13:0,14:0,21:0,23:0',
            VersionT      : -1,
            IsEventMenu   : false,
            SportID       : 1,
            CompetitionID : -1,
            reqUrl        : '/th-th/sports/football/in-play/full-time-asian-handicap-and-over-under',
            oIsInplayAll  : false,
            oIsFirstLoad  : true,
            oSortBy       : 1,
            oOddsType     : 0,
            oPageNo       : 0
        }
    };

    request(options, (err, response, body) => {
        if (err) {
            callback(err, null)
        } else {
            let result = JSON.parse(body);
            const totalPage = result.mod.t === 0 ? 1 : result.mod.t;
            callback(null, totalPage);
        }
    });
};
const liveAPI = (page, callback) => {
    const headers = {
        'Content-Type' : 'application/x-www-form-urlencoded',
        'Cookie' : 'settingProfile=OddsType%3D3'
    };

    let options = {
        url: 'https://sb.1eighty8.com/th-th/Service/CentralService?GetData&ts='+ new Date().getTime(),
        method: 'POST',
        headers: headers,
        form: {
            IsFirstLoad  : true,
            VersionL      : -1,
            VersionU      : 0,
            VersionS      : -1,
            VersionF      : -1,
            VersionH      : '1:0,2:0,3:0,4:0,9:0,13:0,14:0,21:0,23:0',
            VersionT      : -1,
            IsEventMenu   : false,
            SportID       : 1,
            CompetitionID : -1,
            reqUrl        : '/th-th/sports/football/in-play/full-time-asian-handicap-and-over-under',
            oIsInplayAll  : false,
            oIsFirstLoad  : true,
            oSortBy       : 1,
            oOddsType     : 0,
            oPageNo       : page
        }
    };
    request(options, (err, response, body) => {
        if (err) {
            callback(err, null)
        } else {
            let result = JSON.parse(body);
            let masterData = JSON.stringify(_.first(result.mod.d));
            callback(null, mappingMasterData(masterData));
        }
    });
};

function grouping(data) {
    data[0].l = _.chain(data)
        .pluck('l')
        .flatten()
        .value();
    return data[0];
}
function mappingMasterData(value){
    let modelList = [];
    if (_.isUndefined(value)) {
        return modelList;
    }

    let result = JSON.parse(value.replace(/1x2/g, 'x12').replace(/1x21st/g, 'x121st').replace(/<br>/g, ''));
    const key_1 = "เดิมพันพิเศษ";
    const key_2 = "Special";
    _.each(result.c, (leagueValue) => {
        let model = {};//FootballModel();//INDEX
        model.k = leagueValue.k;
        model.n = leagueValue.n;

        model.m = [];

        if (leagueValue.n.includes(key_1) ||
            leagueValue.n.includes(key_2)) {
            console.log(`${leagueValue.n} was removed`);
            return false;
        }

        _.each(leagueValue.e, (matchValue) => {
            if (matchValue.cei.ctid !== 0 ||
                _.isUndefined(matchValue.o)) {
                return false;
            }
            m = {};
            m.id = leagueValue.k + ':' +matchValue.k;
            m.k = matchValue.k;
            m.d = new Date(new Date(matchValue.edt).getTime()+1000*60*60*11);
            m.hasParlay = matchValue.hasParlay;
            m.i = {};
            m.i.lt = matchValue.i[5];
            m.i.mt = matchValue.i[12];
            m.i.h = matchValue.i[10];
            m.i.a = matchValue.i[11];
            m.i.s = matchValue.g;
            m.i.hrc = matchValue.i[8];
            m.i.arc = matchValue.i[9];

            m.n = {};
            const team = matchValue.i[36].replace(/-/g, ' ').split(' vs ');
            m.n.en = {};
            m.n.en.h = _.first(team);
            m.n.en.a = _.last(team);
            m.n.th = {};
            m.n.th.h = matchValue.i[0];
            m.n.th.a = matchValue.i[1];

            m.r = {};
            m.r.pm = 0;

            m.bp=[];

            const ahArray = ahObject(matchValue.o.ah);
            const ouArray = ouObject(matchValue.o.ou);
            const x12Array = x12Object(matchValue.o.x12);
            const oeArray = oeObject(matchValue.o.oe);
            const ah1stArray = ah1stObject(matchValue.o.ah1st);
            const ou1stArray = ou1stObject(matchValue.o.ou1st);
            const x121stArray = x121stObject(matchValue.o.x121st);
            const oe1stArray = oe1stObject(matchValue.o.oe1st);

            let isHasObject = true;
            let index = 0;
            do {
                bp = {};
                const isAhUndefined  = _.isUndefined(ahArray[index]);
                const isOuUndefined  = _.isUndefined(ouArray[index]);
                const isX12Undefined = _.isUndefined(x12Array[index]);
                const isOeUndefined  = _.isUndefined(oeArray[index]);

                const isAh1stUndefined  = _.isUndefined(ah1stArray[index]);
                const isOu1stUndefined  = _.isUndefined(ou1stArray[index]);
                const isX121stUndefined = _.isUndefined(x121stArray[index]);
                const isOe1stUndefined  = _.isUndefined(oe1stArray[index]);

                if (!isAhUndefined) {
                    bp.ah = ahArray[index];
                }

                if (!isOuUndefined) {
                    bp.ou = ouArray[index];
                }

                if (!isX12Undefined) {
                    bp.x12 = x12Array[index];
                }

                if (!isOeUndefined) {
                    bp.oe = oeArray[index];
                }

                if (!isAh1stUndefined) {
                    bp.ah1st = ah1stArray[index];
                }

                if (!isOu1stUndefined) {
                    bp.ou1st = ou1stArray[index];
                }

                if (!isX121stUndefined) {
                    bp.x121st = x121stArray[index];
                }

                if (!isOe1stUndefined) {
                    bp.oe1st = oe1stArray[index];
                }

                bp.r = {};
                m.bp.push(bp);
                index++;
                if (_.isUndefined(ahArray[index]) &&
                    _.isUndefined(ouArray[index]) &&
                    _.isUndefined(x12Array[index])&&
                    _.isUndefined(oeArray[index]) &&
                    _.isUndefined(ah1stArray[index]) &&
                    _.isUndefined(ou1stArray[index]) &&
                    _.isUndefined(x121stArray[index])&&
                    _.isUndefined(oe1stArray[index])) {
                    isHasObject = false;
                }
            } while (isHasObject);

            model.m.push(m);
        });
        modelList.push(model);
    });
    return modelList;
}
function ahObject(value){
    ah = {};
    const size = _.size(value);
    let round = 1;
    let objectArray = [];
    if (size !== 0) {
        for (i = 0; i < size; i+=4) {
            if (round === 1) {
                ah.hk = value[i];
                ah.h  = value[i+1];
                ah.ak = value[i+2];
                ah.a  = value[i+3];
                round++;
            } else {
                ah.hpk = value[i];
                ah.hp  = value[i+1];
                ah.apk = value[i+2];
                ah.ap  = value[i+3];
                round = 1;
                objectArray.push(ah);
                ah = {};
            }
        }
    }
    return objectArray;
}
function ouObject(value){
    ou = {};
    const size = _.size(value);
    let round = 1;
    let objectArray = [];
    if (size !== 0) {
        for (i = 0; i < size; i+=4) {
            if (round === 1) {
                ou.ok = value[i];
                ou.o  = value[i+1];
                ou.uk = value[i+2];
                ou.u  = value[i+3];
                round++;
            } else {
                ou.opk = value[i];
                ou.op  = value[i+1];
                ou.upk = value[i+2];
                ou.up  = value[i+3];
                round = 1;
                objectArray.push(ou);
                ou = {};
            }
        }
    }
    return objectArray;
}
function x12Object(value){
    x12 = {};
    const size = _.size(value);
    let round = 1;
    let objectArray = [];
    if (size !== 0) {
        for (i = 0; i < size; i+=2) {
            if (round === 1) {
                x12.hk = value[i];
                x12.h  = value[i+1];
                round++;
            } else if (round === 2) {
                x12.ak = value[i];
                x12.a  = value[i+1];
                round++;
            } else {
                x12.dk = value[i];
                x12.d  = value[i+1];
                round = 1;
                objectArray.push(x12);
                x12 = {};
            }
        }
    }
    return objectArray;
}
function oeObject(value){
    oe = {};
    const size = _.size(value);
    let round = 1;
    let objectArray = [];
    if (size !== 0) {
        for (i = 0; i < size; i+=4) {
            oe.ok = value[i];
            oe.o  = value[i+1];
            oe.ek = value[i+2];
            oe.e  = value[i+3];
            round = 1;
            objectArray.push(oe);
            oe = {};
        }
    }
    return objectArray;
}
function ah1stObject(value){
    ah1st = {};
    const size = _.size(value);
    let round = 1;
    let objectArray = [];
    if (size !== 0) {
        for (i = 0; i < size; i+=4) {
            if (round === 1) {
                ah1st.hk = value[i];
                ah1st.h  = value[i+1];
                ah1st.ak = value[i+2];
                ah1st.a  = value[i+3];
                round++;
            } else {
                ah1st.hpk = value[i];
                ah1st.hp  = value[i+1];
                ah1st.apk = value[i+2];
                ah1st.ap  = value[i+3];
                round = 1;
                objectArray.push(ah1st);
                ah1st = {};
            }
        }
    }
    return objectArray;
}
function ou1stObject(value){
    ou1st = {};
    const size = _.size(value);
    let round = 1;
    let objectArray = [];
    if (size !== 0) {
        for (i = 0; i < size; i+=4) {
            if (round === 1) {
                ou1st.ok = value[i];
                ou1st.o  = value[i+1];
                ou1st.uk = value[i+2];
                ou1st.u  = value[i+3];
                round++;
            } else {
                ou1st.opk = value[i];
                ou1st.op  = value[i+1];
                ou1st.upk = value[i+2];
                ou1st.up  = value[i+3];
                round = 1;
                objectArray.push(ou1st);
                ou1st = {};
            }
        }
    }
    return objectArray;
}
function x121stObject(value){
    x121st = {};
    const size = _.size(value);
    let round = 1;
    let objectArray = [];
    if (size !== 0) {
        for (i = 0; i < size; i+=2) {
            if (round === 1) {
                x121st.hk = value[i];
                x121st.h  = value[i+1];
                round++;
            } else if (round === 2) {
                x121st.ak = value[i];
                x121st.a  = value[i+1];
                round++;
            } else {
                x121st.dk = value[i];
                x121st.d  = value[i+1];
                round = 1;
                objectArray.push(x121st);
                x121st = {};
            }
        }
    }
    return objectArray;
}
function oe1stObject(value){
    oe1st = {};
    const size = _.size(value);
    let round = 1;
    let objectArray = [];
    if (size !== 0) {
        for (i = 0; i < size; i+=4) {
            oe1st.ok = value[i];
            oe1st.o  = value[i+1];
            oe1st.ek = value[i+2];
            oe1st.e  = value[i+3];
            round = 1;
            objectArray.push(oe1st);
            oe1st = {};
        }
    }
    return objectArray;
}

router.get('/load_league', (req, res) => {
    hdpTotalPage((err, totalPage) =>{
        if (err) {
            return res.send(
                {
                    code: 9999,
                    message: err
                });
        } else {
            let taskList = [];
            const range = _.range(totalPage);
            console.log('Total Page => ', totalPage);
            range.forEach(page => {
                taskList.push((callback) => {
                    hdpAPI(page, (err, result) => {
                        if (err) {
                            callback(err, null)
                        } else {
                            callback(null, result);
                        }
                    });
                });
            });

            async.parallel(taskList,
                (err, resultLeft) => {
                    if (err) {
                        return res.send({message: err, code: 10102});
                    } else {
                        FootballModel.findOne({},
                            (err, resultRight) => {
                                if (err) {
                                    return res.send({message: "error", result: err, code: 999});
                                } else if (!resultRight) {
                                    return res.send({message: "error", result: 'Data not found', code: 999});
                                } else {
                                    const table_left = grouping(resultLeft);
                                    let LEFT  = table_left.l;
                                    let RIGHT = resultRight.l;

                                    console.log('====API Key====');
                                    let resultLeftArray = _.chain(LEFT)
                                        .flatten(true)
                                        .pluck('k')
                                        .value();
                                    // console.log(resultLeftArray);
                                    console.log('====API Key===='+_.size(resultLeftArray));

                                    console.log('====DB Key====');
                                    let resultRightArray = _.chain(RIGHT)
                                        .flatten(true)
                                        .pluck('k')
                                        .value();
                                    // console.log(resultRightArray);
                                    console.log('====DB Key===='+_.size(resultRightArray));

                                    console.log('====Dif Key====');
                                    let resultDifArray = _.difference(resultLeftArray, resultRightArray);
                                    // console.log(resultDifArray);
                                    console.log('====Dif Key===='+_.size(resultDifArray));

                                    table_left.l = _.chain(LEFT)
                                        .flatten(true)
                                        .filter(league => {
                                            return _.contains(_.chain(resultDifArray)
                                                .flatten(true)
                                                .uniq()
                                                .value(), league.k);
                                        })
                                        .each(league => {
                                            league.m = [];
                                            FootballModel.update(
                                                {},
                                                {
                                                    $push: {
                                                        l: league
                                                    }
                                                },
                                                (err, data) => {
                                                    if (err) {
                                                        console.log(`Error while in insert into league ${league.k} : ${league.n.en}`, err);
                                                    }
                                                });
                                        })
                                        .value();

                                    return res.send({
                                        message: "success",
                                        result: table_left,
                                        code: 0
                                    });
                                }
                            });
                    }
                });
        }
    });
});

//[2]
router.get('/load_table_left', (req, res) => {
    hdpTotalPage((err, totalPage) =>{
        if (err) {
            console.log('[2.1]999 : ==> \n', err);
            return res.send(
                {
                    code: 9999,
                    message: err
                });
        } else {
            let taskList = [];
            const range = _.range(totalPage);
            console.log('Total Page => ', totalPage);
            range.forEach(page => {
                taskList.push((callback) => {
                    hdpAPI(page, (err, result) => {
                        if (err) {
                            callback(err, null)
                        } else {
                            callback(null, result);
                        }
                    });
                });
            });

            async.parallel(taskList,
                (err, resultLeft) => {
                    if (err) {
                        return res.send({message: err, code: 10102});
                    } else {
                        FootballModel.find({},
                            (err, resultRight) => {
                                if (err) {
                                    console.log('[2.2]999 : ==> \n', err);
                                    return res.send({message: "error", result: err, code: 999});
                                } else if (!resultRight) {
                                    console.log('[2.3]999 : ==> Data not found');
                                    return res.send({message: "error", result: 'Data not found', code: 999});
                                } else {
                                    console.log('=========== LEFT ===========');
                                    let resultLeftArray = _.chain(resultLeft)
                                        .flatten(true)
                                        .pluck('m')
                                        .flatten(true)
                                        .pluck('id')
                                        .value();
                                    console.log('=========== LEFT ==========='+_.size(resultLeftArray));

                                    console.log('=========== RIGHT ===========');
                                    let resultRightArray = _.chain(resultRight)
                                        .flatten(true)
                                        .pluck('m')
                                        .flatten(true)
                                        .pluck('id')
                                        .value();
                                    console.log('=========== RIGHT ==========='+_.size(resultRightArray));

                                    console.log('=========== Dif Key ===========');
                                    let resultDifArray = _.difference(resultLeftArray, resultRightArray);
                                    // console.log(resultDifArray);
                                    console.log('=========== Dif Key ==========='+_.size(resultDifArray));

                                    console.log('=========== Total ===========');
                                    let resultTotalArray = _.chain(resultLeft)
                                        .flatten(true)
                                        .each(league => {
                                            // console.log('Before => ', _.size(league.m));
                                            league.m = _.chain(league.m)
                                                .flatten(true)
                                                .filter(match => {
                                                    // console.log(match.id +' : '+ _.contains(resultDifArray, match.id));
                                                    return _.contains(resultDifArray, match.id);
                                                });
                                            // console.log('After => ', _.size(league.m));
                                        })
                                        .value();
                                    console.log('[2.4] success');
                                    return res.send({
                                        message: "success",
                                        result: resultTotalArray,
                                        code: 0
                                    });
                                }
                            });
                    }
                });
        }
    });
});
//[3]
router.get('/load_table_right', (req, res) => {
    FootballModel.find({
            m: {
                $elemMatch: {
                    isE: false
                }
            }
        })
        .sort({'sk': 1})
        .lean()
        .exec((err, data) => {
            if (err) {
                console.log('[3.1]999 : ==> \n', err);
                return res.send({message: "error", result: err, code: 999});
            } else if (!data) {
                console.log('[3.2]999 : ==> Data not found');
                return res.send({message: "error", result: 'Data not found', code: 999});
            } else {
                console.log('[3.3] success');
                return res.send({
                    message: "success",
                    result: data,
                    code: 0
                });
            }
    });
});
//[4]
router.put('/insert_match', (req, res) =>{
    const {
        id
    } = req.body;

    const league_key = parseInt(_.first(id.split(':')));
    const match_key = parseInt(_.last(id.split(':')));

    FootballModel.aggregate(
        {
            $match: {
                k: league_key
            }
        },
        {
            $unwind: '$m'
        },
        {
            $match: {
                'm.k': match_key
            }
        },
        (err, data) => {
            if (err) {
                console.log('999 : ==> \n', err);
                return res.send({
                    message: err,
                    code: 9999
                });

            }  else if (_.size(data) > 0) {
                return res.send({
                    message: `Duplicate match : ${league_key} ${match_key}`,
                    code: 9998
                });

            } else {
                FootballModel.update({
                        k: league_key
                    },
                    {
                        $push: {
                            m: req.body
                        }
                    },
                    {
                        multi: false
                    },
                    (err, data) => {
                        if (err) {
                            console.log('999 : ==> \n', err);
                            return res.send({
                                message: err,
                                code: 9999
                            });

                        } else if (data.nModified === 0) {
                            return res.send({
                                message: `Update match fail -> ${req.body}`,
                                code: 9998
                            });

                        } else {
                            return res.send({
                                message: "success",
                                code: 0
                            });
                        }
                    });
            }
        });
});
//[5]
router.put('/delete_match/:id', (req, res) =>{
    const id = req.params.id;
    if (!id) {
        return res.send({
            code    : 900001,
            message : 'Parameter is incorrect.'
        });
    }

    const league_key = parseInt(_.first(id.split(':')));
    const match_key = parseInt(_.last(id.split(':')));

    FootballModel.update({
            k: league_key
        },
        {
            $pull: {
                m: {
                    '$id' : match_key
                }
            }
        },
        {
            multi: false
        },
        (err, data) => {
            if (err) {
                console.log('999 : ==> \n', err);
                return res.send({
                    code    : 9999,
                    message : err
                });

            } else {
                return res.send({
                    code    : 0,
                    message : "success",
                    result  : id
                });
            }
        });
});
//[6]
router.get('/match/:id', (req, res) => {
    const id = req.params.id;
    if (!id) {
        return res.send({
            code: 900001,
            message: 'Parameter is incorrect.'
        });
    }

    const league_key = parseInt(_.first(id.split(':')));
    const match_key = parseInt(_.last(id.split(':')));

    FootballModel.aggregate(
        {
            $match: {
                k: league_key
            }
        },
        {
            $unwind: '$m'
        },
        {
            $match: {
                'm.k': match_key
            }
        },
        (err, data) => {
            if (err) {
                console.log('999 : ==> \n', err);
                return res.send({
                    message: err,
                    code: 9999
                });

            }  else if (_.size(data) === 0) {
                return res.send({
                    message: `Match id ${id} not found`,
                    code: 9998
                });

            } else {
                return res.send({
                    message: "success",
                    code: 0,
                    result : _.first(data)
                });
            }
        });
});
//[7]
router.get('/league/:id', (req, res) => {
    const id = req.params.id;
    if (!id) {
        return res.send({
            code: 900001,
            message: 'Parameter is incorrect.'
        });
    }

    FootballModel.findOne({
        k: id
    })
        .lean()
        .exec((err, data) => {
            if (err) {
                console.log('999 : ==> \n', err);
                return res.send({
                    message: err,
                    code: 9999
                });

            }  else if (_.size(data) === 0) {
                return res.send({
                    message: `League id ${id} not found`,
                    code: 9998
                });

            } else {
                return res.send({
                    code: 0,
                    message: "success",
                    result : data
                });
            }
        });
});
//[8]
router.put('/insert_all_match', (req, res) => {
    const {
        k,
        n,
        m
    } = req.body;

    FootballModel.aggregate(
        {
            $match: {
                k: k
            }
        },
        (err, data) => {
            if (err) {
                console.log('[8.1]999 : ==> \n', err);
                return res.send({
                    message: err,
                    code: 9999
                });

            }  else if (_.size(data) === 0) {
                let model = FootballModel();
                model.k = k;
                model.n = n;
                model.m = m;
                model.save((err, data) => {
                    if (err) {
                        console.log('[8.2]999 : ==> \n', err);
                        return res.send({message: "error", result: err, code: 999});
                    } else {
                        return res.send(
                            {
                                code: 0,
                                message: "success",
                                result: data
                            });
                    }
                });
            } else {
                _.each(m, match => {
                    const {
                        id
                    } = match;
                    const league_key = parseInt(_.first(id.split(':')));
                    const match_key = parseInt(_.last(id.split(':')));

                    FootballModel.aggregate(
                        {
                            $match: {
                                k: league_key
                            }
                        },
                        {
                            $unwind: '$m'
                        },
                        {
                            $match: {
                                'm.k': match_key
                            }
                        },
                        (err, data) => {
                            if (err) {
                                console.log('[8.3.1]999 : ==> \n', err);
                            }  else if (_.size(data) > 0) {
                                console.log(`[8.3.2] Duplicate match : ${league_key} ${match_key}`);
                            } else {
                                FootballModel.update({
                                        k: league_key
                                    },
                                    {
                                        $push: {
                                            m: match
                                        }
                                    },
                                    {
                                        multi: false
                                    },
                                    (err, data) => {
                                        if (err) {
                                            console.log(`[8.3.3] Error insert match : ${league_key} ${match_key}`);
                                            console.log(err);
                                        } else if (data.nModified === 0) {
                                            console.log(`[8.3.4] insert match fail : ${league_key} ${match_key}`);
                                        } else {
                                            console.log(`[8.3.5] insert match success : ${league_key} ${match_key}`);
                                        }
                                    });
                            }
                        });
                });
                return res.send(
                    {
                        code: 0,
                        message: "success"
                    });
            }
        });
});
//[9]
router.put('/update_match', (req, res) =>{
    const {
        id
    } = req.body;

    const league_key = parseInt(_.first(id.split(':')));
    const match_key = parseInt(_.last(id.split(':')));

    FootballModel.aggregate(
        {
            $match: {
                k: league_key
            }
        },
        {
            $unwind: '$m'
        },
        {
            $match: {
                'm.k': match_key
            }
        },
        (err, data) => {
            if (err) {
                console.log('[9.1] 999 : ==> \n', err);
                return res.send({
                    message: err,
                    code: 9999
                });

            }  else if (_.size(data) === 0) {
                console.log(`[9.2] Match : ${league_key} ${match_key} not found`);
                return res.send({
                    message: `Match id ${league_key} ${match_key} not found`,
                    code: 9998
                });

            } else {
                FootballModel.update({
                        k: league_key,
                        m: {
                            $elemMatch: {
                                k: match_key
                            }
                        }
                    },
                    {
                        $set: {
                            'm.$': req.body
                        }
                    },
                    {
                        multi: false
                    },
                    (err, data) => {
                        if (err) {
                            console.log('[9.3] 999 : ==> \n', err);
                            return res.send({
                                message: err,
                                code: 9999
                            });

                        } else if (data.nModified === 0) {
                            console.log('[9.4] Update match fail');
                            return res.send({
                                message: `Update match fail`,
                                code: 9998
                            });

                        } else {
                            console.log('[9.5] success');
                            return res.send({
                                message: "success",
                                code: 0
                            });
                        }
                    });
            }
        });
});
//[10]
router.get('/load_live_match', (req, res) =>{
    liveTotalPage((err, totalPage) =>{
        if (err) {
            console.log('[10.1]999 : ==> \n', err);
            return res.send(
                {
                    code: 9999,
                    message: err
                });
        } else {
            let taskList = [];
            const range = _.range(totalPage);
            console.log('Total Page => ', totalPage);
            range.forEach(page => {
                taskList.push((callback) => {
                    liveAPI(page, (err, result) => {
                        if (err) {
                            callback(err, null)
                        } else {
                            callback(null, result);
                        }
                    });
                });
            });

            async.parallel(taskList,
                (err, result) => {
                    if (err) {
                        return res.send({message: err, code: 10102});
                    } else {
                        console.log('[10.2] success');
                        return res.send({
                            message: "success",
                            result: _.first(result),
                            code: 0
                        });
                    }
                });
        }
    });
});

module.exports = router;