const express = require('express');
const router = express.Router();
const request = require('request');
const moment = require('moment');
const querystring = require('querystring');
const async = require("async");
const config = require('config');
const Joi = require('joi');
const _ = require('underscore');
const MemberService = require('../../common/memberService');
const LottoService = require('../../common/lottoService');
const ReportService = require('../../common/reportService');
const BetTransactionModel = require('../../models/betTransaction.model');
const LottoSettleHistoryModel = require('../../models/lottoSettleHistory.model');
const LottoLaosSettleHistoryModel = require('../../models/lottoLaosSettleHistory.model');
const LottoPPSettleHistoryModel = require('../../models/lottoSettlePPHistory.model');
const AgentGroupModel = require('../../models/agentGroup.model.js');
const AgentLottoLimitModel = require('../../models/agentLottoLimit.model.js');
const AgentLottoLaosLimitModel = require('../../models/agentLottoLaosLimit.model.js');
const AgentLottoPPLimitModel = require('../../models/agentLottoPPLimit.model.js');
const LottoGameListModel = require('../../models/lottoGameList.model.js');
const DateUtils = require('../../common/dateUtils');
const crypto = require('crypto');
const mongoose = require('mongoose');
const roundTo = require('round-to');
const LotusService = require('../../common/lotusService');
const AgentService = require('../../common/agentService');
const Hashing = require('../../common/hashing');

const NumberUtils = require('../../common/numberUtils1');


const DRAW = "DRAW";
const WIN = "WIN";
const HALF_WIN = "HALF_WIN";
const LOSE = "LOSE";
const HALF_LOSE = "HALF_LOSE";

router.get('/login', (req, res) => {

    const userInfo = req.userInfo;
    const username = req.userInfo.username;


    let client = req.query.client;

    LottoService.login(username, client, function (err, response) {

        if (err) {
            res.send({code: 999, message: 'fail'});
        } else {
            if (response.code == 0) {
                res.send({code: 0, url: response.result.url})
            } else {
                res.send({code: 999, message: 'fail'})
            }
        }
    });

});


const infoSchema = Joi.object().keys({
    playerId: Joi.string().required(),
    gameId: Joi.string().required(),
});

router.post('/callback/info', (req, res) => {


    let validate = Joi.validate(req.body, infoSchema);
    if (validate.error) {
        console.log('Request Incomplete')
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 1001
        });
    }


    MemberService.findByUserNameForPartnerService(req.body.playerId, function (err, memberResponse) {

        if (!memberResponse) {
            return res.send({
                message: "invalid playerId",
                results: null,
                code: 999
            });
        }
        let credit = roundTo((memberResponse.creditLimit + memberResponse.balance), 2);

        let condition = {
            memberId: mongoose.Types.ObjectId(memberResponse._id),
            'game': 'LOTTO',
            'source': 'AMB_LOTTO',
            'status': 'RUNNING'
        };
        BetTransactionModel.aggregate([
            {
                $match: condition
            },
            {
                "$group": {
                    "_id": {
                        memberId: '$memberId'
                    },
                    totalAmount: {$sum: '$amount'},
                    totalDiscount: {$sum: {$multiply: ['$amount', {$divide: ['$commission.member.commission', 100]}]}},
                }
            },
            {
                $project: {
                    _id: 0,
                    totalAmount: '$totalAmount',
                    totalDiscount: '$totalDiscount'
                }
            }
        ], (err, results) => {

            let totalBet = results[0] ? results[0] : {totalAmount: 0, totalDiscount: 0};
            return res.send({
                code: 0,
                message: "success",
                result: {
                    balance: credit,
                    limitSetting: memberResponse.limitSetting.lotto.amb,
                    totalBetAmount: totalBet.totalAmount,
                    totalDiscountAmount: totalBet.totalDiscount
                }
            });
        });

    });

});


const newGameSchema = Joi.object().keys({
    id: Joi.string().required(),
    day: Joi.string().required(),
    month: Joi.string().required(),
    year: Joi.string().required()
});

router.post('/callback/new-game', (req, res) => {


    let validate = Joi.validate(req.body, newGameSchema);
    if (validate.error) {
        console.log('Request Incomplete')
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 1001
        });
    }


    let body = req.body;
    body.createdDate = DateUtils.getCurrentDate()
    let model = new LottoGameListModel(body);
    model.save((err, response) => {
        if (err) {
            if (err.code == 11000) {
                return res.send({code: 999, message: "duplicate game id"});
            }
            return res.send({code: 999, message: "err", result: err});
        }


        return res.send({code: 0, message: "success"});
    });


});


function generateBetId() {
    return 'BET' + new Date().getTime();
}


const Bet = Joi.object().keys({
    betId: Joi.number().required(),
    gameId: Joi.string().required(),
    playerId: Joi.string().required(),
    totalBetAmt: Joi.number().required(),
    betTime: Joi.string().required(),
    type: Joi.string().required(),
    txns: Joi.array().items({
        txId: Joi.string().required(),
        betType: Joi.string().valid('_1TOP', '_1BOT', '_2TOP', '_2BOT', '_2TOD', '_2TOP_OE', '_2TOP_OU',
            '_2BOT_OE', '_2BOT_OU',
            '_3TOP', '_3BOT', '_3TOD', '_3TOP_OE', '_3TOP_OU',
            '_4TOP', '_4TOD', '_5TOP', '_6TOP').required(),
        betNumber: Joi.string().required(),
        betAmt: Joi.number().required(),
        max: Joi.number().required(),
        discount: Joi.number().required(),
        payout: Joi.number().required()
    }),
    ipAddress: Joi.string().required(),
    signature: Joi.string().required(),
});


function getCurrentTime() {
    return moment().utc(true).format('HH:mm:ss.SSS')
}

router.post('/callback/bet', (req, res) => {

    let validate = Joi.validate(req.body, Bet);
    if (validate.error) {
        console.log('Request Incomplete');
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 1001
        });
    }

    // let hashMessage = requestBody.key + requestBody.userId + requestBody.tableId + requestBody.roundId;
    //
    // if (!isValidSignature(requestBody.signature, hashMessage)) {
    //     return res.send({code: 1002, message: "Signature not match"});
    // }

    let requestBody = req.body;


    let result = req.body;
    delete result.signature;

    let createdDate = DateUtils.getCurrentDate();

    let timeFindUser, timeUpline, timeGameList, timeCheckTran, timeCheckBetId, timeGetAgents;


    async.waterfall([callback => {

        MemberService.findByUserNameForPartnerService(requestBody.playerId, function (err, memberResponse) {
            if (err) {
                callback(err, null);
                return;
            }

            if (memberResponse) {

                if (memberResponse.suspend || memberResponse.lock) {
                    callback(5011, null);
                } else if (memberResponse.limitSetting.lotto.amb.isEnable) {

                    timeFindUser = getCurrentTime();

                    AgentService.getUpLineStatus(memberResponse.group, (err, response) => {

                        timeUpline = getCurrentTime();
                        if (response.suspend || response.lock) {
                            callback(5011, null);
                        } else if (!response.ambLottoEnable) {
                            callback(5012, null);
                        } else {
                            let betAmount = _.reduce(requestBody.txns, function (total, tran) {
                                return total + (tran.betAmt - (tran.betAmt * (LottoService.getLottoCommission(memberResponse, tran.betType) / 100)));
                            }, 0);

                            if (roundTo((memberResponse.creditLimit + memberResponse.balance), 2) < betAmount) {
                                callback(1003, null);
                            } else {
                                callback(null, memberResponse);
                            }

                        }
                    });
                } else {
                    callback(5012, null);
                }


            } else {
                callback(1004, null);
            }
        });

    }, (memberInfo, callback) => {

        LottoGameListModel.findOne().sort({createdDate: -1}).exec(function (err, gameResponse) {
            timeGameList = getCurrentTime();
            if (gameResponse.id !== requestBody.gameId) {
                callback(1006, null);
            } else {
                callback(null, memberInfo, gameResponse);
            }

        });

    }, (memberInfo, gameInfo, callback) => {

        //TODO fix gameDate
        let condition = {
            memberId: mongoose.Types.ObjectId(memberInfo._id),
            game: 'LOTTO',
            source: 'AMB_LOTTO',
            'lotto.amb.gameId': gameInfo.id,
            status: 'RUNNING'
        };

        BetTransactionModel.aggregate([
            {
                $match: condition
            },
            {
                "$group": {
                    "_id": {
                        betType: '$lotto.amb.betType',
                        betNumber: '$lotto.amb.betNumber'
                    },
                    amount: {$sum: '$amount'},
                }
            },
            {
                $project: {
                    _id: 0,
                    betType: '$_id.betType',
                    betNumber: '$_id.betNumber',
                    amount: '$amount'
                }
            }], (err, results) => {

            timeCheckTran = getCurrentTime();

            results = _.chain(results).groupBy(key => {
                if (key.betType == '_2TOD' || key.betType == '_3TOD' || key.betType == '_4TOD') {
                    return key.betType + '|' + LottoService.sortTodNumber(key.betNumber);
                } else {
                    return key.betType + '|' + key.betNumber;
                }
            }).map((value, key) => {
                return {
                    betType: key.split('|')[0],
                    betNumber: key.split('|')[1],
                    amount: _.reduce(value, function (memo, obj) {
                        return memo + obj.amount;
                    }, 0)
                }
            }).value();


            let txns = requestBody.txns;


            let prepareTxns = _.map(txns, item => {

                let newItem = Object.assign({}, item);

                let totalMemberBet = _.filter(results, fItem => {

                    if (item.betType == '_2TOD' || item.betType == '_3TOD' || item.betType == '_4TOD') {
                        return (fItem.betType === item.betType) && (LottoService.sortTodNumber(fItem.betNumber) === LottoService.sortTodNumber(item.betNumber));
                    } else {
                        return (fItem.betType === item.betType) && (fItem.betNumber === item.betNumber);
                    }
                })[0];


                let limit = LottoService.getLottoLimit(memberInfo, item.betType);
                console.log('totalMemberBet : ', totalMemberBet);
                console.log('limit : ', limit);

                if (item.betAmt > limit.max) {
                    newItem.code = 2001;
                } else if (item.discount != limit.discount) {
                    newItem.code = 2002;
                } else if (item.max != limit.max) {
                    newItem.code = 2003;
                } else if (item.payout != limit.payout) {
                    newItem.code = 2004;
                } else {
                    newItem.code = 0;
                }

                if (totalMemberBet) {
                    if ((item.betAmt + totalMemberBet.amount) > limit.max) {
                        newItem.code = 2005;
                    }
                }

                // newItem.remainingAmt = item.betAmt * NumberUtils.convertPercent(10);
                return newItem;
            });

            callback(null, memberInfo, gameInfo, prepareTxns);
        });

    }], (err, memberInfo, gameInfo, prepareTxns) => {

        if (err) {
            if (err == 5012) {
                return res.send({
                    code: 5012,
                    message: 'Service Locked, Please contact your upline.'
                });
            }

            if (err == 5011) {
                return res.send({
                    message: "Account or Upline was suspend.",
                    result: null,
                    code: 5011
                });
            }

            if (err === 1003) {
                return res.send({code: 1003, message: "Insufficient balance"});
            }
            if (err === 1006) {
                return res.send({code: 1006, message: "GameId not found"});
            }
            return res.send({code: 999, message: "update balance fail"});
        }


        let isNotSuccess = _.filter(prepareTxns, (item) => {
            return item.code != 0;
        });

        if (isNotSuccess.length > 0) {
            result.txns = prepareTxns;
            return res.send({code: 2000, message: "bet fail", result: result});
        }


        let checkBetCountCondition = {
            'memberId': mongoose.Types.ObjectId(memberInfo._id),
            'game': 'LOTTO',
            'source': 'AMB_LOTTO',
            'lotto.amb.gameId': gameInfo.id,
            'lotto.amb.betId': requestBody.betId
        };

        BetTransactionModel.findOne(checkBetCountCondition, function (err, betResponse) {
            if (betResponse) {
                return res.send({code: 999, message: "duplicate betId"});
            } else {

                timeCheckBetId = getCurrentTime();

                AgentGroupModel.findById(memberInfo.group).select('_id type parentId shareSetting limitSetting ambLotto')
                    .populate({
                        path: 'parentId',
                        select: '_id type parentId shareSetting limitSetting name ambLotto',
                        options: {lean: true},
                        populate: {
                            path: 'parentId',
                            select: '_id type parentId shareSetting limitSetting name ambLotto',
                            options: {lean: true},
                            populate: {
                                path: 'parentId',
                                select: '_id type parentId shareSetting limitSetting name ambLotto',
                                options: {lean: true},
                                populate: {
                                    path: 'parentId',
                                    select: '_id type parentId shareSetting limitSetting name ambLotto',
                                    options: {lean: true},
                                    populate: {
                                        path: 'parentId',
                                        select: '_id type parentId shareSetting limitSetting name ambLotto',
                                        options: {lean: true},
                                        populate: {
                                            path: 'parentId',
                                            select: '_id type parentId shareSetting limitSetting name ambLotto',
                                            options: {lean: true},
                                        },
                                    },
                                },
                            },
                        },
                    }).lean().exec((err, agentGroups) => {
                    if (err) {
                        return res.send({code: 999, message: "err"});
                    } else {

                        timeGetAgents = getCurrentTime();

                        // let prepareTxns = _.map(requestBody.txns, item => {
                        //     let newItem = Object.assign({}, item);
                        //     let limitObj = updateLimitResponse.po[item.betType];
                        //     let betNumberObj = _.filter(limitObj.bets,filterItem => {
                        //         return filterItem.betNumber === item.betNumber;
                        //     })[0];
                        //
                        //     console.log('-------1 :: ',betNumberObj);
                        //     console.log('-------2 :: ',betNumberObj ||0);
                        //     if(betNumberObj && (limitObj.limit > betNumberObj.betAmount)){
                        //         newItem.code = 2005;
                        //     }
                        //
                        //     newItem.remainingAmt = item.betAmt * NumberUtils.convertPercent(10);
                        //     return newItem;
                        // });

                        let remark = 'createdDate : || ' + createdDate + ' || timeFindUser : ' + timeFindUser + ' ||  timeUpline : ' + timeUpline + ' || timeGameList : ' + timeGameList + ' || timeCheckTran : ' + timeCheckTran + ' || timeCheckBetId : ' + timeCheckBetId + ' || timeGetAgents :' + timeGetAgents;

                        let transactionCount = 0;
                        saveTransaction(gameInfo, agentGroups, requestBody, {}, transactionCount, remark);
                        // if(isValidMin){
                        //     saveTransaction(agentGroups, requestBody);
                        // }else {
                        //     return res.send({
                        //         code: 8838,
                        //         message: "fail"
                        //     });
                        // }

                    }
                });
            }
        });


        function saveTransaction(gameInfo, agentGroups, objBody, trans, transactionCount, remark) {

            if (objBody.txns.length == 0) {

                result.txns = trans;

                return res.send({
                    code: 0,
                    message: "success",
                    result: result
                });

            } else {

                let tran = objBody.txns.shift();

                let commission = LottoService.getLottoCommission(memberInfo, tran.betType);

                let memberCredit = roundTo(tran.betAmt - (commission / 100 * tran.betAmt), 2);

                let gameDate = moment([Number.parseInt(gameInfo.year) - 543, Number.parseInt(gameInfo.month) - 1, Number.parseInt(gameInfo.day)]);
                gameDate.second(0).minute(30).hour(15).utc(true).toDate();


                let body = {
                    memberId: memberInfo._id,
                    memberParentGroup: memberInfo.group,
                    betId: generateBetId(),
                    lotto: {
                        amb: {
                            betId: objBody.betId,
                            gameId: objBody.gameId,
                            type: objBody.type,
                            txId: tran.txId,
                            betTime: DateUtils.convertThaiDate(moment(requestBody.betTime).toDate()),
                            betType: tran.betType,
                            betNumber: tran.betNumber,
                            result: 0,
                            betAmount: tran.betAmt,
                            payout: tran.payout,
                            winLose: 0,
                        },
                    },
                    amount: tran.betAmt,
                    memberCredit: memberCredit * -1,
                    payout: tran.betAmt * tran.payout,
                    gameType: 'TODAY',
                    acceptHigherPrice: false,
                    priceType: 'TH',
                    game: 'LOTTO',
                    source: 'AMB_LOTTO',
                    status: 'RUNNING',
                    commission: {},
                    gameDate: gameDate,
                    createdDate: DateUtils.getCurrentDate(),
                    isEndScore: false,
                    ipAddress: requestBody.ipAddress,
                    currency: memberInfo.currency,
                    remark: remark
                };


                prepareCommission(gameInfo, agentGroups, objBody, memberInfo, body, agentGroups, null, 0, 0, transactionCount, remark)

            }
        }


        function prepareCommission(gameInfo, allAgents, objBody, memberInfo, body, currentAgent, overAgent, remainingBalance, overAllShare, transactionCount, remark) {


            if (currentAgent && (currentAgent.type === 'SUPER_ADMIN' || currentAgent.parentId)) {


                console.log('==============================================');
                console.log('process : ' + currentAgent.type + ' : ' + currentAgent.name);
                // console.log('currentAgent : ',currentAgent)
                // console.log('overAgent : ',overAgent)

                const betType = body.lotto.amb.betType;
                let betNumber = body.lotto.amb.betNumber;
                const betAmount = body.lotto.amb.betAmount;


                if (betType == '_2TOD' || betType == '_3TOD' || betType == '_4TOD') {
                    betNumber = LottoService.sortTodNumber(betNumber);
                }

                let conditions = {};
                conditions['agent'] = mongoose.Types.ObjectId(currentAgent._id);
                // conditions['po.'+body.lotto.amb.betType+'.bets.betNumber'] = body.lotto.amb.betNumber;

                AgentLottoLimitModel.findOne(conditions, 'hour min po.' + betType, function (err, agentLimit) {

                    let money = body.amount;

                    if (!overAgent) {
                        overAgent = {};
                        overAgent.type = 'member';
                        overAgent.shareSetting = {};
                        overAgent.shareSetting.lotto = {};
                        overAgent.shareSetting.lotto.amb = {};
                        overAgent.shareSetting.lotto.amb.parent = memberInfo.shareSetting.lotto.amb.parent;
                        overAgent.shareSetting.lotto.amb.own = 0;
                        overAgent.shareSetting.lotto.amb.remaining = 0;

                        body.commission.member = {};
                        body.commission.member.parent = memberInfo.shareSetting.lotto.amb.parent;
                        body.commission.member.commission = LottoService.getLottoCommission(memberInfo, body.lotto.amb.betType);
                        body.commission.member.payout = LottoService.getLottoPayout(memberInfo, body.lotto.amb.betType);

                    }


                    console.log('');
                    console.log('---- setting ----');
                    console.log('ส่วนแบ่งที่ได้มามีทั้งหมด : ' + currentAgent.shareSetting.lotto.amb.own + ' %');
                    console.log('ขอส่วนแบ่งจาก : ' + overAgent.type + ' ' + overAgent.shareSetting.lotto.amb.parent + ' %');
                    console.log('ให้ส่วนแบ่ง : ' + overAgent.type + ' ที่เหลือไป ' + overAgent.shareSetting.lotto.amb.own + ' %');
                    console.log('มีการตั้งค่าremaining จาก : ' + overAgent.type + ' : ' + overAgent.shareSetting.lotto.amb.remaining + ' %');
                    console.log('ได้remaining จาก : ' + overAgent.type + ' : ' + remainingBalance + ' บาท');
                    // console.log('ได้remaining จาก : ' + overAgent.type + ' : ' + remainingBalancePercent + ' %');
                    console.log('---- end setting ----');
                    console.log('');


                    // console.log('และได้รับ remaining ไว้ : '+overAgent.shareSetting.lotto.amb.remaining+' %');

                    let currentPercentReceive = overAgent.shareSetting.lotto.amb.parent;
                    let currentReceiveAmount = (betAmount * NumberUtils.convertPercent(currentPercentReceive));
                    let remainingBalancePercent = NumberUtils.findBetAmountPercentage(remainingBalance, betAmount);

                    // let totalRemaining = remaining;


                    console.log('มีค่า remainingBalance จาก : ' + overAgent.type + ' : ' + remainingBalance);
                    console.log('ค่า remainingBalance คิดเป็น % =  ' + NumberUtils.findBetAmountPercentage(remainingBalance, betAmount) + ' %');


                    if (overAgent.shareSetting.lotto.amb.remaining > 0) {
                        // console.log()
                        // console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');
                        console.log('overAgent.shareSetting.lotto.amb.remaining : ', overAgent.shareSetting.lotto.amb.remaining);
                        console.log('remainingBalancePercent : ', remainingBalancePercent);

                        if (overAgent.shareSetting.lotto.amb.remaining >= remainingBalancePercent) {
                            console.log('รับ remaining ทั้งหมดไว้: ' + remainingBalancePercent + ' %');
                            currentPercentReceive += remainingBalancePercent;
                            currentReceiveAmount += remainingBalance;
                            remainingBalance = 0;
                        } else {

                            console.log('หักค่า remaining ที่ได้รับมากับที่เซตรับไว้ = ' + remainingBalancePercent + ' - ' + overAgent.shareSetting.lotto.amb.remaining + ' = ' + (remainingBalancePercent - overAgent.shareSetting.lotto.amb.remaining));
                            remainingBalancePercent = remainingBalancePercent - overAgent.shareSetting.lotto.amb.remaining;
                            currentPercentReceive += overAgent.shareSetting.lotto.amb.remaining;
                            currentReceiveAmount = (betAmount * NumberUtils.convertPercent(currentPercentReceive));
                            remainingBalance = (betAmount * NumberUtils.convertPercent(remainingBalancePercent));

                        }

                    }
                    console.log('remainingBalancePercent : ', remainingBalancePercent)
                    console.log('remaining balance ที่ถูกยกมาคงเหลือ : ', remainingBalance);

                    // else {
                    //     currentPercentReceive += NumberUtils.findBetAmountPercentage(remainingBalance, betAmount);
                    //     currentReceiveAmount += remainingBalance;
                    // }

                    console.log('% ที่ได้ : ' + currentPercentReceive);
                    console.log('เป็นจำนวนเงิน : ' + currentReceiveAmount);

                    console.log('สรุปรับ % ทั้งหมด รวมเงิน remaining : ' + currentPercentReceive + ' %');
                    console.log('สรุปรับ % ทั้งหมด รวมเงิน remaining เป็นเงิน  : ' + currentReceiveAmount);


                    let totalRemainingAmount = currentReceiveAmount;


                    let limitObj = agentLimit.po[betType];

                    let betNumberObj = _.filter(limitObj.bets, filterItem => {
                        return filterItem.betNumber === betNumber;
                    })[0];

                    console.log('------- :: limit : ' + limitObj.limit + ' ' + betNumberObj);

                    let finalReceiveAmount = 0;


                    // .year(Number.parseInt(gameInfo.year) - 543).month(Number.parseInt(gameInfo.month) - 1).day(Number.parseInt(gameInfo.day))
                    //check receive time
                    console.log('gameInfo : ', gameInfo)
                    // let receiveTime = moment().second(0).minute(agentLimit.min).hour(agentLimit.hour);
                    // receiveTime.year(Number.parseInt(gameInfo.year) - 543)
                    // receiveTime.day(3);
                    // receiveTime.month(Number.parseInt(gameInfo.month) - 1);
                    let receiveTime = moment([Number.parseInt(gameInfo.year) - 543, Number.parseInt(gameInfo.month) - 1, Number.parseInt(gameInfo.day)]);
                    receiveTime.second(0).minute(agentLimit.min).hour(agentLimit.hour);
                    let currentTime = moment();

                    console.log('receiveTime ::: ', receiveTime);
                    console.log('currentTime ::: ', moment());
                    console.log(currentTime.isBefore(receiveTime));


                    let agentReceiveTime = moment([Number.parseInt(gameInfo.year) - 543, Number.parseInt(gameInfo.month) - 1, Number.parseInt(gameInfo.day)]);
                    agentReceiveTime.second(0).minute(currentAgent.limitSetting.lotto.amb.minute).hour(currentAgent.limitSetting.lotto.amb.hour);

                    // console.log(currentAgent.limitSetting.lotto.amb)
                    console.log('agent Time : ', agentReceiveTime);
                    if ((currentAgent.type !== 'SUPER_ADMIN')) {
                        //set limit
                        let inTime = (currentTime.isBefore(receiveTime) && currentTime.isBefore(agentReceiveTime));
                        // let inTime = true;
                        // if(currentAgent.type === 'SHARE_HOLDER'){
                        //     inTime = true;
                        // }
                        console.log('limit : ', limitObj.limit);
                        console.log('currentPercentReceive : ', currentPercentReceive);
                        console.log('inTime : ', inTime);
                        if (limitObj.limit > 0 && currentPercentReceive > 0 && inTime) {


                            let increaseAmountValue = 0;

                            if (betNumberObj && betNumberObj.betAmount >= limitObj.limit) {
                                //limit is full
                                console.log('case 1');
                                finalReceiveAmount = 0;
                                totalRemainingAmount = currentReceiveAmount;
                                currentPercentReceive = 0;
                                currentReceiveAmount = 0;
                            } else {

                                if (!betNumberObj) {
                                    betNumberObj = {
                                        betAmount: 0,
                                        betNumber: betNumber,
                                    }
                                }

                                console.log('limit : ' + limitObj.limit + ' , totalBetNumberAmount : ' + betNumberObj.betAmount + ' = remain ' + (limitObj.limit - (betNumberObj.betAmount) ))


                                let currentTotalBetNumberAmount = betNumberObj.betAmount + currentReceiveAmount;

                                console.log('currentReceiveAmount : ', currentReceiveAmount);
                                console.log('currentTotalBetNumberAmount : ', currentTotalBetNumberAmount)
                                if (currentTotalBetNumberAmount > limitObj.limit) {
                                    console.log('case 2');

                                    increaseAmountValue = limitObj.limit - betNumberObj.betAmount;
                                    finalReceiveAmount = increaseAmountValue;

                                    totalRemainingAmount = currentReceiveAmount - increaseAmountValue;
                                    console.log('totalReceiveAmount : ', finalReceiveAmount);

                                    currentPercentReceive = NumberUtils.findBetAmountPercentage(finalReceiveAmount, betAmount);

                                } else {
                                    console.log('case 3');
                                    finalReceiveAmount = currentReceiveAmount;
                                    increaseAmountValue = currentReceiveAmount;
                                    totalRemainingAmount = 0;
                                }

                                console.log('currentReceiveAmount : ', currentReceiveAmount);
                                console.log('increaseAmountValue : ', increaseAmountValue);
                                console.log('totalReceiveAmount : ', finalReceiveAmount);

                                updateBetLimit(currentAgent._id, betType, betNumber, increaseAmountValue, (err, response) => {

                                });
                            }

                            // totalReceiveAmount -


                        } else {
                            console.log('nooooooooooo')
                            //unset limit limit = 0 shareReceive = 0 outOfTime
                            totalRemainingAmount = currentReceiveAmount;
                            finalReceiveAmount = 0;
                            currentPercentReceive = 0;
                        }
                    } else {
                        // superadmin
                        finalReceiveAmount = currentReceiveAmount + remainingBalance;
                        currentPercentReceive = NumberUtils.findBetAmountPercentage(finalReceiveAmount, betAmount)
                        totalRemainingAmount = 0;
                    }

                    overAllShare = overAllShare + currentPercentReceive;

                    let unUseShare = currentAgent.shareSetting.lotto.amb.own -
                        (overAgent.shareSetting.lotto.amb.parent + overAgent.shareSetting.lotto.amb.own);

                    let unUseShareBalance = betAmount * NumberUtils.convertPercent(unUseShare);


                    totalRemainingAmount = (unUseShareBalance + totalRemainingAmount) + remainingBalance;


                    console.log('ยอดแทง ' + money + ' ได้ทั้งหมด : ' + currentPercentReceive + ' %');

                    console.log('รวม % + % remaining  จะเป็นสัดส่วนที่ได้ทั้งหมด = ' + finalReceiveAmount);
                    console.log('ค่าที่จะถูกคืนกลับไป (remainingBalance) : ' + totalRemainingAmount);
                    console.log('เหลือส่วนแบ่งที่ไม่ได้ใช้ : ' + unUseShare + ' %');

                    console.log('จำนวน % ที่ถูกแบ่งไปแล้วทั้งหมด : ', overAllShare);

                    //set commission
                    let agentCommission = LottoService.getLottoCommission(currentAgent, body.lotto.amb.betType);


                    switch (currentAgent.type) {
                        case 'SUPER_ADMIN':
                            body.commission.superAdmin = {};
                            body.commission.superAdmin.group = currentAgent._id;
                            body.commission.superAdmin.parent = currentAgent.shareSetting.lotto.amb.parent;
                            body.commission.superAdmin.own = currentAgent.shareSetting.lotto.amb.own;
                            body.commission.superAdmin.remaining = currentAgent.shareSetting.lotto.amb.remaining;
                            body.commission.superAdmin.min = currentAgent.shareSetting.lotto.amb.min;
                            body.commission.superAdmin.shareReceive = Math.abs(currentPercentReceive);
                            body.commission.superAdmin.commission = agentCommission;
                            body.commission.superAdmin.amount = finalReceiveAmount;
                            body.commission.superAdmin.payout = LottoService.getLottoPayout(currentAgent, body.lotto.amb.betType);
                            break;
                        case 'COMPANY':
                            body.commission.company = {};
                            body.commission.company.parentGroup = currentAgent.parentId;
                            body.commission.company.group = currentAgent._id;
                            body.commission.company.parent = currentAgent.shareSetting.lotto.amb.parent;
                            body.commission.company.own = currentAgent.shareSetting.lotto.amb.own;
                            body.commission.company.remaining = currentAgent.shareSetting.lotto.amb.remaining;
                            body.commission.company.min = currentAgent.shareSetting.lotto.amb.min;
                            body.commission.company.shareReceive = Math.abs(currentPercentReceive);
                            body.commission.company.commission = agentCommission;
                            body.commission.company.amount = finalReceiveAmount;
                            body.commission.company.payout = LottoService.getLottoPayout(currentAgent, body.lotto.amb.betType);
                            break;
                        case 'SHARE_HOLDER':
                            body.commission.shareHolder = {};
                            body.commission.shareHolder.parentGroup = currentAgent.parentId;
                            body.commission.shareHolder.group = currentAgent._id;
                            body.commission.shareHolder.parent = currentAgent.shareSetting.lotto.amb.parent;
                            body.commission.shareHolder.own = currentAgent.shareSetting.lotto.amb.own;
                            body.commission.shareHolder.remaining = currentAgent.shareSetting.lotto.amb.remaining;
                            body.commission.shareHolder.min = currentAgent.shareSetting.lotto.amb.min;
                            body.commission.shareHolder.shareReceive = currentPercentReceive;
                            body.commission.shareHolder.commission = agentCommission;
                            body.commission.shareHolder.amount = finalReceiveAmount;
                            body.commission.shareHolder.payout = LottoService.getLottoPayout(currentAgent, body.lotto.amb.betType);
                            break;
                        case 'SENIOR':
                            body.commission.senior = {};
                            body.commission.senior.parentGroup = currentAgent.parentId;
                            body.commission.senior.group = currentAgent._id;
                            body.commission.senior.parent = currentAgent.shareSetting.lotto.amb.parent;
                            body.commission.senior.own = currentAgent.shareSetting.lotto.amb.own;
                            body.commission.senior.remaining = currentAgent.shareSetting.lotto.amb.remaining;
                            body.commission.senior.min = currentAgent.shareSetting.lotto.amb.min;
                            body.commission.senior.shareReceive = currentPercentReceive;
                            body.commission.senior.commission = agentCommission;
                            body.commission.senior.amount = finalReceiveAmount;
                            body.commission.senior.payout = LottoService.getLottoPayout(currentAgent, body.lotto.amb.betType);
                            break;
                        case 'MASTER_AGENT':
                            body.commission.masterAgent = {};
                            body.commission.masterAgent.parentGroup = currentAgent.parentId;
                            body.commission.masterAgent.group = currentAgent._id;
                            body.commission.masterAgent.parent = currentAgent.shareSetting.lotto.amb.parent;
                            body.commission.masterAgent.own = currentAgent.shareSetting.lotto.amb.own;
                            body.commission.masterAgent.remaining = currentAgent.shareSetting.lotto.amb.remaining;
                            body.commission.masterAgent.min = currentAgent.shareSetting.lotto.amb.min;
                            body.commission.masterAgent.shareReceive = currentPercentReceive;
                            body.commission.masterAgent.commission = agentCommission;
                            body.commission.masterAgent.amount = finalReceiveAmount;
                            body.commission.masterAgent.payout = LottoService.getLottoPayout(currentAgent, body.lotto.amb.betType);
                            break;
                        case 'AGENT':
                            body.commission.agent = {};
                            body.commission.agent.parentGroup = currentAgent.parentId;
                            body.commission.agent.group = currentAgent._id;
                            body.commission.agent.parent = currentAgent.shareSetting.lotto.amb.parent;
                            body.commission.agent.own = currentAgent.shareSetting.lotto.amb.own;
                            body.commission.agent.remaining = currentAgent.shareSetting.lotto.amb.remaining;
                            body.commission.agent.min = currentAgent.shareSetting.lotto.amb.min;
                            body.commission.agent.shareReceive = currentPercentReceive;
                            body.commission.agent.commission = agentCommission;
                            body.commission.agent.amount = finalReceiveAmount;
                            body.commission.agent.payout = LottoService.getLottoPayout(currentAgent, body.lotto.amb.betType);
                            break;
                    }

                    prepareCommission(gameInfo, allAgents, objBody, memberInfo, body, currentAgent.parentId, currentAgent, totalRemainingAmount, overAllShare, transactionCount, remark);
                });


            } else {
                console.log("========== End prepareCommission ==========");
                // callback(null,'xzxc')

                console.log('bodyyyyyyyyy :: ', body);

                console.log('transactionCount : ', transactionCount)


                let prepareTxnsRemain = prepareTxns;

                let matchItem = _.filter(prepareTxns, filterItem => {
                    return filterItem.txId === body.lotto.amb.txId;
                })[0];

                if (matchItem) {
                    prepareTxnsRemain[transactionCount].remainingAmt = body.commission.superAdmin.amount;
                }


                // saveTransaction(gameInfo,allAgents, objBody, prepareTxnsRemain,++transactionCount);


                //TODO: OPEN FOR PROD
                let betTransactionModel = new BetTransactionModel(body);
                betTransactionModel.save(function (err, response) {

                    if (err) {
                        console.log(err)
                    }
                    MemberService.updateBalance(memberInfo._id, body.memberCredit, response.betId, 'LOTTO_BET', '', function (err, updateBalanceResponse) {
                        if (err == 888) {
                            console.log(err)
                        }
                        saveTransaction(gameInfo, allAgents, objBody, prepareTxnsRemain, ++transactionCount, remark);
                    });

                });
            }

        }


    });


    function updateBetLimit(agentId, betType, betNumber, betAmount, callback) {
        console.log('::::: updateBetLimit ::::: ' + betType + ' : betNumber = ' + betNumber + ' : amount = ' + betAmount)
        let increaseAmountConditions = {};
        increaseAmountConditions['agent'] = mongoose.Types.ObjectId(agentId);
        increaseAmountConditions['po.' + betType + '.bets.betNumber'] = betNumber;

        let updateIncreaseAmountStatement = {};
        updateIncreaseAmountStatement['po.' + betType + '.bets.$.betAmount'] = betAmount;


        AgentLottoLimitModel.update(increaseAmountConditions, {$inc: updateIncreaseAmountStatement}, function (err, updateLimitResponse) {

            if (updateLimitResponse.nModified == 0) {

                console.log('set new betnumber')
                let conditions = {};
                conditions['agent'] = mongoose.Types.ObjectId(agentId);
                conditions['po.' + betType + '.bets.betNumber'] = {$nin: [betNumber]};
                let updateStatement = {};
                updateStatement['po.' + betType + '.bets'] = {
                    betNumber: betNumber,
                    betAmount: betAmount
                };

                AgentLottoLimitModel.update(conditions, {$push: updateStatement}, function (err, updateLimitResponse) {
                    if (err) {
                        console.log('err : ', err.code)
                    }
                    if (updateLimitResponse.nModified == 0) {
                        AgentLottoLimitModel.update(increaseAmountConditions, {$inc: updateIncreaseAmountStatement}, function (err, updateLimitResponse) {
                            console.log('xc', err)
                            console.log('xc', updateLimitResponse)
                            callback(null, {})
                        });
                    } else {
                        callback(null, {})
                    }
                });

            } else if (updateLimitResponse.nModified == 1) {
                //update success
                callback(null, {})
            }
        });
    }
});


const cancelBet = Joi.object().keys({
    betId: Joi.number().required(),
    gameId: Joi.string().required(),
    playerId: Joi.string().required(),
    type: Joi.string().required(),
    cancelTime: Joi.string().required(),
    txId: Joi.string().required(),
    betType: Joi.string().required(),
    betNumber: Joi.string().required(),
    signature: Joi.string().required(),
});


router.post('/callback/cancel-bet', (req, res) => {
    console.log(req.body)

    let validate = Joi.validate(req.body, cancelBet);
    if (validate.error) {
        console.log('Request Incomplete')
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 1001
        });
    }

    // let hashMessage = requestBody.key + requestBody.userId + requestBody.tableId + requestBody.roundId;
    //
    // if (!isValidSignature(requestBody.signature, hashMessage)) {
    //     return res.send({code: 1002, message: "Signature not match"});
    // }

    let requestBody = req.body;


    //code=1 insufficaintbalance
    //code=2 maxbet
    //code=2 เลขอั้น

    async.series([callback => {

        MemberService.findByUserNameForPartnerService(requestBody.playerId, function (err, memberResponse) {
            if (err) {
                callback(err, null);
                return;
            }
            if (memberResponse) {
                callback(null, memberResponse);
            } else {
                callback(1004, null);
            }
        });

    }], (err, asyncResponse) => {

        if (err) {
            if (err === 1004) {
                return res.send({code: 1004, message: "Invalid user id"});
            }
        }

        let memberInfo = asyncResponse[0];

        let condition = {
            'memberId': mongoose.Types.ObjectId(memberInfo._id),
            'game': 'LOTTO',
            'source': 'AMB_LOTTO',
            'lotto.amb.gameId': requestBody.gameId,
            'lotto.amb.betId': requestBody.betId,
            'lotto.amb.txId': requestBody.txId,
        };


        BetTransactionModel.findOne(condition)
            .populate({path: 'memberParentGroup', select: 'type'})
            .exec((err, betObj) => {


                if (!betObj) {
                    return res.send({
                        code: 999,
                        message: "transaction not found",
                        result: req.body,
                    });
                }

                if (betObj.status === 'DONE' || betObj.status === 'CANCELLED') {

                    return res.send({
                        code: 1005,
                        message: "transaction was already cancelled"
                    });
                }

                let betAmount = betObj.memberCredit * -1;


                MemberService.updateBalance(memberInfo._id, betAmount, betObj.betId, "LOTTO_CANCEL", '', (err, updateBalanceResponse) => {

                    let updateBody = {
                        $set: {
                            'status': 'CANCELLED',
                            'cancelByType': 'MEMBER',
                            'cancelDate': DateUtils.getCurrentDate()
                        }
                    };

                    BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                        if (err) {
                            return res.send({
                                code: 0,
                                message: "success"
                                // balance: 2999
                            });

                        } else {

                            decreaseAgentLimit(betObj, (err, limitResponse) => {

                                return res.send({
                                    code: 0,
                                    message: "success"
                                });
                            });

                        }
                    });

                });

            });
    });

});


function decreaseAgentLimit(betTransaction, callback) {

    const winLose = betTransaction.commission;
    const betType = betTransaction.lotto.amb.betType;
    let betNumber = betTransaction.lotto.amb.betNumber;

    if (betType == '_2TOD' || betType == '_3TOD' || betType == '_4TOD') {
        betNumber = LottoService.sortTodNumber(betNumber);
    }

    let updateAgentLimitList = [];

    if (winLose.company.group) {
        updateAgentLimitList.push({
            group: winLose.company.group,
            amount: winLose.company.amount
        });
    }
    if (winLose.shareHolder.group) {
        updateAgentLimitList.push({
            group: winLose.shareHolder.group,
            amount: winLose.shareHolder.amount
        });
    }
    if (winLose.senior.group) {
        updateAgentLimitList.push({
            group: winLose.senior.group,
            amount: winLose.senior.amount
        });
    }
    if (winLose.masterAgent.group) {
        updateAgentLimitList.push({
            group: winLose.masterAgent.group,
            amount: winLose.masterAgent.amount
        });
    }
    if (winLose.agent.group) {
        updateAgentLimitList.push({
            group: winLose.agent.group,
            amount: winLose.agent.amount
        });
    }


    console.log('xxxxxx ::: ', updateAgentLimitList);
    updateAgentLimit(updateAgentLimitList);

    function updateAgentLimit(lists) {

        if (lists.length == 0) {
            callback(null, 'success')
        } else {

            let item = lists.pop();

            if (item.group && item.amount !== 0) {

                let increaseAmountConditions = {};
                increaseAmountConditions['agent'] = mongoose.Types.ObjectId(item.group);
                increaseAmountConditions['po.' + betType + '.bets.betNumber'] = betNumber;

                let updateIncreaseAmountStatement = {};
                updateIncreaseAmountStatement['po.' + betType + '.bets.$.betAmount'] = item.amount * -1;

                AgentLottoLimitModel.update(increaseAmountConditions, {$inc: updateIncreaseAmountStatement}, function (err, updateLimitResponse) {
                    console.log('group : ' + item.group + ' , balance : ' + item.amount);
                    updateAgentLimit(lists);

                });
            } else {
                updateAgentLimit(lists);
            }
        }
    }

}


const voidTransaction = Joi.object().keys({
    betId: Joi.number().required(),
    gameId: Joi.string().required(),
    playerId: Joi.string().required(),
    type: Joi.string().required(),
    cancelTime: Joi.string().required(),
    txns: Joi.array().items({
        txId: Joi.string().required(),
        betType: Joi.string().required(),
        betNumber: Joi.string().required(),
    }),
    signature: Joi.string().required(),
});


router.post('/callback/void-transaction', (req, res) => {
    console.log(req.body)

    let validate = Joi.validate(req.body, voidTransaction);
    if (validate.error) {
        console.log('Request Incomplete')
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 1001
        });
    }

    // let hashMessage = requestBody.key + requestBody.userId + requestBody.tableId + requestBody.roundId;
    //
    // if (!isValidSignature(requestBody.signature, hashMessage)) {
    //     return res.send({code: 1002, message: "Signature not match"});
    // }

    let requestBody = req.body;


    let condition = {
        'lotto.amb.betId': requestBody.betId,
        'lotto.amb.gameId': requestBody.gameId,
        'game': 'LOTTO',
        'source': 'AMB_LOTTO'
    };


    BetTransactionModel.find(condition)
        .populate({path: 'memberParentGroup', select: 'type'})
        .exec(function (err, transaction) {


            let runningTransaction = _.filter(transaction, item => {
                return item.status === 'RUNNING';
            });

            if (runningTransaction.length > 0) {
                cancelBetTask(runningTransaction, requestBody.remark);

            } else {
                return res.send({
                    code: 1005,
                    message: "transaction was already cancelled"
                });
            }
        });

    function cancelBetTask(transactions, remark) {
        if (transactions.length == 0) {
            return res.send({
                code: 0,
                message: "success"
            });
        } else {

            let item = transactions.pop();

            let betAmount = item.memberCredit * -1;


            MemberService.updateBalance(item.memberId, betAmount, item.betId, "LOTTO_CANCEL", '', (err, updateBalanceResponse) => {

                let updateBody = {
                    $set: {
                        'status': 'CANCELLED',
                        'remark': remark,
                        'cancelByType': 'MEMBER',
                        'cancelDate': DateUtils.getCurrentDate()
                    }
                };

                BetTransactionModel.update({_id: item._id}, updateBody, function (err, response) {
                    decreaseAgentLimit(item, (err, limitResponse) => {
                        cancelBetTask(transactions);
                    });
                });

            });

        }
    }

});

const settleSchema = Joi.object().keys({
    gameId: Joi.string().required(),
    _2BOT_OE: Joi.string().required(),
    _2BOT_OU: Joi.string().required(),
    _2BOT: Joi.string().required(),
    _2TOP_OE: Joi.string().required(),
    _2TOP_OU: Joi.string().required(),
    _2TOP: Joi.string().required(),
    _3TOP_OE: Joi.string().required(),
    _3TOP_OU: Joi.string().required(),
    _3TOP: Joi.string().required(),
    _4TOP: Joi.string().required(),
    _5TOP: Joi.string().required(),
    _6TOP: Joi.string().required(),
    _6TOD: Joi.array().items(Joi.string().required()),
    _5TOD: Joi.array().items(Joi.string().required()),
    _4TOD: Joi.array().items(Joi.string().required()),
    _3TOD: Joi.array().items(Joi.string().required()),
    _3BOT: Joi.array().items(Joi.string().required()),
    _2TOD: Joi.array().items(Joi.string().required()),
    _1TOP: Joi.array().items(Joi.string().required()),
    _1BOT: Joi.array().items(Joi.string().required()),
    signature: Joi.string().required(),
});

router.post('/callback/settle', (req, res) => {
    let validate = Joi.validate(req.body, settleSchema);
    if (validate.error) {
        console.log('Request Incomplete')
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 1001
        });
    }

    // let hashMessage = requestBody.key + requestBody.userId + requestBody.tableId + requestBody.roundId;
    //
    // if (!isValidSignature(requestBody.signature, hashMessage)) {
    //     return res.send({code: 1002, message: "Signature not match"});
    // }

    let requestBody = req.body;


    async.series([callback => {

        let model = new LottoSettleHistoryModel(requestBody);
        model.createdDate = DateUtils.getCurrentDate();
        model.save((err, response) => {
            callback(null, response);
        });

    }], (err, asyncResponse) => {


        async.parallel([callback => {
            let condition = {
                'gameDate': {$gte: new Date(moment().add(-20, 'd').format('YYYY-MM-DDT11:00:00.000'))},
                'game': 'LOTTO',
                'source': 'AMB_LOTTO',
                'lotto.amb.gameId': requestBody.gameId,
                'status': 'RUNNING',
                '$or': [{'lotto.amb.betType': '_1TOP'}, {'lotto.amb.betType': '_2TOP'}, {'lotto.amb.betType': '_2TOP_OE'},
                    {'lotto.amb.betType': '_2TOP_OU'}, {'lotto.amb.betType': '_2BOT_OE'}, {'lotto.amb.betType': '_2BOT_OU'}, {'lotto.amb.betType': '_3TOP'},
                    {'lotto.amb.betType': '_4TOP'}, {'lotto.amb.betType': '_4TOD'}]
            };


            const cursor = BetTransactionModel.find(condition)
                .batchSize(5000)
                .populate({path: 'memberParentGroup', select: 'type'})
                .lean()
                .cursor();

            cursor.on('data', function (betObj) {


                let betAmount = betObj.amount;

                // betObj.memberCredit; //(betAmount * betObj.commission.member.payout) -  betAmount;
                let betResult = calculateResult(requestBody, betObj.lotto.amb.betType, betObj.lotto.amb.betNumber);


                function calculateResult(result, betType, betNumber) {
                    let _TOP_NUMBER = result._6TOP.split("");
                    let _2BOT_NUMBER = result._2BOT;

                    let tmp5 = _TOP_NUMBER.splice(1);
                    let tmp4 = tmp5.splice(1);
                    let tmp3 = tmp4.splice(1);
                    //------
                    let cal3top = (parseInt(tmp3[0]) + parseInt(tmp3[1]) + parseInt(tmp3[2]));
                    let tmp2 = tmp3.splice(1);

                    let calodd2top = (parseInt(tmp2[0]) + parseInt(tmp2[1]));
                    let cal2bot = (parseInt(_2BOT_NUMBER[0]) + parseInt(_2BOT_NUMBER[1]));


                    //------

                    switch (betType) {
                        case '_1TOP' :
                            return result._1TOP.indexOf(betNumber) > -1 ? WIN : LOSE;
                        case '_1BOT' :
                            return result._1BOT.indexOf(betNumber) > -1 ? WIN : LOSE;
                        case '_2TOP' :
                            return result._2TOP == betNumber ? WIN : LOSE;
                        case '_2TOD' :
                            return result._2TOD.indexOf(betNumber) > -1 ? WIN : LOSE;
                        case '_2BOT' :
                            return result._2BOT == betNumber ? WIN : LOSE;
                        case '_2TOP_OE' :
                            console.log('_2TOP_OE : ', isEvenOdd(calodd2top));
                            return result._2TOP_OE === isEvenOdd(calodd2top) ? WIN : LOSE;
                        case '_2TOP_OU' :
                            console.log('_2TOP_OU : ', is2TOP_OU(calodd2top));
                            return result._2TOP_OU === is2TOP_OU(calodd2top) ? WIN : LOSE;
                        case '_2BOT_OE' :
                            console.log('_2BOT_OE : ', isEvenOdd(cal2bot));
                            return result._2BOT_OE === isEvenOdd(cal2bot) ? WIN : LOSE;
                        case '_2BOT_OU' :
                            console.log('_2BOT_OU : ', is2BOT_OU(cal2bot));
                            return result._2BOT_OU === is2BOT_OU(cal2bot) ? WIN : LOSE;
                        case '_3TOP' :
                            return result._3TOP === betNumber ? WIN : LOSE;
                        case '_3TOD' :
                            return result._3TOD.indexOf(betNumber) > -1 ? WIN : LOSE;
                        case '_3BOT' :
                            return result._3BOT.indexOf(betNumber) > -1 ? WIN : LOSE;
                        case '_3TOP_OE' :
                            return result._3TOP_OE === isEvenOdd(cal3top) ? WIN : LOSE;
                        case '_3TOP_OU' :
                            return result._3TOP_OU === is3TOP_OU(cal3top) ? WIN : LOSE;
                        case '_4TOP' :
                            return result._4TOP == betNumber ? WIN : LOSE;
                        case '_4TOD' :
                            return result._4TOD.indexOf(betNumber) > -1 ? WIN : LOSE;
                        case '_5TOP' :
                            return result._5TOP == betNumber ? WIN : LOSE;
                        case '_6TOP' :
                            return result._6TOP == betNumber ? WIN : LOSE;
                        default :
                            return null;
                    }
                }

                const shareReceive = AgentService.prepareShareReceiveLotto(betResult, betObj);

                updateAgentMemberBalance(shareReceive, betAmount, function (err, updateBalanceResponse) {
                    if (err) {
                        return res.send({code: 999, message: "999"});
                    } else {

                        let updateBody = {
                            $set: {

                                'lotto.amb.result': getResult(requestBody, betObj.lotto.amb.betType),
                                'commission.superAdmin.winLoseCom': shareReceive.superAdmin.winLoseCom,
                                'commission.superAdmin.winLose': shareReceive.superAdmin.winLose,
                                'commission.superAdmin.totalWinLoseCom': shareReceive.superAdmin.totalWinLoseCom,

                                'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                                'commission.company.winLose': shareReceive.company.winLose,
                                'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                                'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                                'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                                'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                                'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                                'commission.senior.winLose': shareReceive.senior.winLose,
                                'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                                'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                                'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                                'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                                'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                                'commission.agent.winLose': shareReceive.agent.winLose,
                                'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                                'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                                'commission.member.winLose': shareReceive.member.winLose,
                                'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                                'validAmount': betAmount,
                                'betResult': betResult,
                                'isEndScore': true,
                                'status': 'DONE',
                                'updatedDate':DateUtils.getCurrentDate(),
                                'settleDate': DateUtils.getCurrentDate()
                            }
                        }

                        // return res.send({code: 0, message: "999"});

                        BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                            if (err) {
                                console.log(err)
                                // return res.send({code: 999, message: "999"});
                            } else {
                                // console.log('success')
                                // return res.send({code: 0, message: "success"});

                            }
                        });
                    }
                });


            });


            cursor.on('end', function () {
                callback(null, {});
            });

        }, callback => {
            let condition = {
                'gameDate': {$gte: new Date(moment().add(-20, 'd').format('YYYY-MM-DDT11:00:00.000'))},
                'game': 'LOTTO',
                'source': 'AMB_LOTTO',
                'lotto.amb.gameId': requestBody.gameId,
                'status': 'RUNNING',
                '$or': [{'lotto.amb.betType': '_1BOT'}, {'lotto.amb.betType': '_2BOT'}, {'lotto.amb.betType': '_2TOD'},
                    {'lotto.amb.betType': '_3BOT'}, {'lotto.amb.betType': '_3TOD'}, {'lotto.amb.betType': '_3TOP_OE'},
                    {'lotto.amb.betType': '_3TOP_OU'}, {'lotto.amb.betType': '_5TOP'}, {'lotto.amb.betType': '_6TOP'}]
            };

            const cursor = BetTransactionModel.find(condition)
                .batchSize(5000)
                .populate({path: 'memberParentGroup', select: 'type'})
                .lean()
                .cursor();

            cursor.on('data', function (betObj) {


                let betAmount = betObj.amount;

                // betObj.memberCredit; //(betAmount * betObj.commission.member.payout) -  betAmount;
                let betResult = calculateResult(requestBody, betObj.lotto.amb.betType, betObj.lotto.amb.betNumber);


                function calculateResult(result, betType, betNumber) {
                    let _TOP_NUMBER = result._6TOP.split("");
                    let _2BOT_NUMBER = result._2BOT;

                    let tmp5 = _TOP_NUMBER.splice(1);
                    let tmp4 = tmp5.splice(1);
                    let tmp3 = tmp4.splice(1);
                    //------
                    let cal3top = (parseInt(tmp3[0]) + parseInt(tmp3[1]) + parseInt(tmp3[2]));
                    let tmp2 = tmp3.splice(1);

                    let calodd2top = (parseInt(tmp2[0]) + parseInt(tmp2[1]));
                    let cal2bot = (parseInt(_2BOT_NUMBER[0]) + parseInt(_2BOT_NUMBER[1]));


                    //------

                    switch (betType) {
                        case '_1TOP' :
                            return result._1TOP.indexOf(betNumber) > -1 ? WIN : LOSE;
                        case '_1BOT' :
                            return result._1BOT.indexOf(betNumber) > -1 ? WIN : LOSE;
                        case '_2TOP' :
                            return result._2TOP == betNumber ? WIN : LOSE;
                        case '_2TOD' :
                            return result._2TOD.indexOf(betNumber) > -1 ? WIN : LOSE;
                        case '_2BOT' :
                            return result._2BOT == betNumber ? WIN : LOSE;
                        case '_2TOP_OE' :
                            return result._2TOP_OE === isEvenOdd(calodd2top) ? WIN : LOSE;
                        case '_2TOP_OU' :
                            return result._2TOP_OU === is2TOP_OU(calodd2top) ? WIN : LOSE;
                        case '_2BOT_OE' :
                            return result._2BOT_OE === isEvenOdd(cal2bot) ? WIN : LOSE;
                        case '_2BOT_OU' :
                            return result._2BOT_OU === is2BOT_OU(cal2bot) ? WIN : LOSE;
                        case '_3TOP' :
                            return result._3TOP === betNumber ? WIN : LOSE;
                        case '_3TOD' :
                            return result._3TOD.indexOf(betNumber) > -1 ? WIN : LOSE;
                        case '_3BOT' :
                            return result._3BOT.indexOf(betNumber) > -1 ? WIN : LOSE;
                        case '_3TOP_OE' :
                            return result._3TOP_OE === isEvenOdd(cal3top) ? WIN : LOSE;
                        case '_3TOP_OU' :
                            return result._3TOP_OU === is3TOP_OU(cal3top) ? WIN : LOSE;
                        case '_4TOP' :
                            return result._4TOP == betNumber ? WIN : LOSE;
                        case '_4TOD' :
                            return result._4TOD.indexOf(betNumber) > -1 ? WIN : LOSE;
                        case '_5TOP' :
                            return result._5TOP == betNumber ? WIN : LOSE;
                        case '_6TOP' :
                            return result._6TOP == betNumber ? WIN : LOSE;
                        default :
                            return null;
                    }
                }

                const shareReceive = AgentService.prepareShareReceiveLotto(betResult, betObj);

                updateAgentMemberBalance(shareReceive, betAmount, function (err, updateBalanceResponse) {
                    if (err) {
                        return res.send({code: 999, message: "999"});
                    } else {

                        let updateBody = {
                            $set: {

                                'lotto.amb.result': getResult(requestBody, betObj.lotto.amb.betType),
                                'commission.superAdmin.winLoseCom': shareReceive.superAdmin.winLoseCom,
                                'commission.superAdmin.winLose': shareReceive.superAdmin.winLose,
                                'commission.superAdmin.totalWinLoseCom': shareReceive.superAdmin.totalWinLoseCom,

                                'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                                'commission.company.winLose': shareReceive.company.winLose,
                                'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                                'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                                'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                                'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                                'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                                'commission.senior.winLose': shareReceive.senior.winLose,
                                'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                                'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                                'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                                'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                                'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                                'commission.agent.winLose': shareReceive.agent.winLose,
                                'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                                'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                                'commission.member.winLose': shareReceive.member.winLose,
                                'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                                'validAmount': betAmount,
                                'betResult': betResult,
                                'isEndScore': true,
                                'status': 'DONE',
                                'updatedDate':DateUtils.getCurrentDate(),
                                'settleDate': DateUtils.getCurrentDate()
                            }
                        }

                        // return res.send({code: 0, message: "999"});

                        BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                            if (err) {
                                console.log(err)
                                // return res.send({code: 999, message: "999"});
                            } else {

                                // console.log('success')
                                // return res.send({code: 0, message: "success"});
                            }
                        });
                    }
                });


            });


            cursor.on('end', function () {
                callback(null, {});
            });

        }], (err, asyncResponse) => {

            clearLimit((err, response) => {
                return res.send({code: 0, message: "success"});
            });
        });
    });

    function is3TOP_OU(number) {
        if (number >= 0 && number <= 13) {
            return 'UNDER';
        } else if (number >= 14 && number <= 27) {
            return 'OVER';
        }
    }

    function is2TOP_OU(number) {
        if (number >= 0 && number < 10) {
            return 'UNDER';
        } else if (number > 9 && number <= 19) {
            return 'OVER';
        }
    }

    function is2BOT_OU(number) {
        if (number >= 0 && number <= 9) {
            return 'UNDER';
        } else {
            return 'OVER';
        }
    }


    function isEvenOdd(number) {
        if (number % 2 == 0) {
            return 'EVEN';
        } else {
            return 'ODD';
        }
    }


    function getResult(result, betType) {
        switch (betType) {
            case '_1BOT' :
            case '_2BOT' :
            case '_2BOT_OE' :
            case '_2BOT_OU' :
                return result._2BOT;
            case '_3BOT' :
                return result._3BOT.join(',');
            case '_1TOP' :
            case '_2TOP' :
            case '_2TOD' :
            case '_2TOP_OE' :
            case '_2TOP_OU' :
            case '_3TOP' :
            case '_3TOD' :
            case '_3TOP_OE' :
            case '_3TOP_OU' :
            case '_4TOP' :
            case '_4TOD' :
            case '_5TOP' :
            case '_6TOP' :
                return result._6TOP;
            default :
                return null;
        }
    }

    function clearLimit(callback) {
        AgentLottoLimitModel.find({}).exec(function (err, response) {


            console.log('length : ', response.length)

            async.each(response, (transaction, callbackWL) => {


                let body = {

                    $set: {
                        'po._1TOP.bets': [],
                        'po._1BOT.bets': [],
                        'po._2TOP.bets': [],
                        'po._2BOT.bets': [],
                        'po._2TOD.bets': [],
                        'po._2TOP_OE.bets': [],
                        'po._2TOP_OU.bets': [],
                        'po._2BOT_OE.bets': [],
                        'po._2BOT_OU.bets': [],
                        'po._3TOP.bets': [],
                        'po._3BOT.bets': [],
                        'po._3TOD.bets': [],
                        'po._3TOP_OE.bets': [],
                        'po._3TOP_OU.bets': [],
                        'po._4TOP.bets': [],
                        'po._4TOD.bets': [],
                        'po._5TOP.bets': [],
                        'po._6TOP.bets': []
                    }
                };


                AgentLottoLimitModel.update({_id: transaction._id}, body, function (err, lottoResponse) {
                    if (err) {
                        console.log(err)
                        callbackWL(err);
                    }
                    else {
                        console.log(lottoResponse)
                        callbackWL();
                    }
                });
            }, (err) => {
                callback(null, 'success');
            });

        });
    }

});


const settlePPSchema = Joi.object().keys({
    gameId: Joi.string().required(),
    // _2BOT_OE: Joi.string().required(),
    // _2BOT_OU: Joi.string().required(),
    _2BOT: Joi.string().required(),
    // _2TOP_OE: Joi.string().required(),
    // _2TOP_OU: Joi.string().required(),
    _2TOP: Joi.string().required(),
    // _3TOP_OE: Joi.string().required(),
    // _3TOP_OU: Joi.string().required(),
    _3TOP: Joi.string().required(),
    _4TOP: Joi.string().required(),
    _5TOP: Joi.string().required(),
    _6TOP: Joi.string().required(),
    _6TOD: Joi.array().items(Joi.string().required()),
    _5TOD: Joi.array().items(Joi.string().required()),
    _4TOD: Joi.array().items(Joi.string().required()),
    _3TOD: Joi.array().items(Joi.string().required()),
    // _3BOT: Joi.array().items(Joi.string().required()),
    _2TOD: Joi.array().items(Joi.string().required()),
    _1TOP: Joi.array().items(Joi.string().required()),
    _1BOT: Joi.array().items(Joi.string().required()),
    signature: Joi.string().required(),
});

router.post('/callback/settle-online', (req, res) => {
    let validate = Joi.validate(req.body, settlePPSchema);
    if (validate.error) {
        console.log('Request Incomplete')
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 1001
        });
    }

    // let hashMessage = requestBody.key + requestBody.userId + requestBody.tableId + requestBody.roundId;
    //
    // if (!isValidSignature(requestBody.signature, hashMessage)) {
    //     return res.send({code: 1002, message: "Signature not match"});
    // }

    let requestBody = req.body;


    async.series([callback => {

        let model = new LottoPPSettleHistoryModel(requestBody);
        model.createdDate = DateUtils.getCurrentDate();
        model.save((err, response) => {
            callback(null, response);
        });

    }], (err, asyncResponse) => {

        let condition = {
            'gameDate': {$gte: new Date(moment().add(-20, 'd').format('YYYY-MM-DDT11:00:00.000'))},
            'game': 'LOTTO',
            'source': 'AMB_LOTTO_PP',
            'lotto.pp.gameId': requestBody.gameId,
            'status': 'RUNNING'
        };


        const cursor = BetTransactionModel.find(condition)
            .batchSize(5000)
            .populate({path: 'memberParentGroup', select: 'type'})
            .lean()
            .cursor();

        cursor.on('data', function (betObj) {


            let betAmount = betObj.amount;

            // betObj.memberCredit; //(betAmount * betObj.commission.member.payout) -  betAmount;
            let betResult = calculateResult(requestBody, betObj.lotto.pp.betType, betObj.lotto.pp.betNumber);


            function calculateResult(result, betType, betNumber) {
                let _TOP_NUMBER = result._6TOP.split("");
                let _2BOT_NUMBER = result._2BOT;

                let tmp5 = _TOP_NUMBER.splice(1);
                let tmp4 = tmp5.splice(1);
                let tmp3 = tmp4.splice(1);
                //------
                let cal3top = (parseInt(tmp3[0]) + parseInt(tmp3[1]) + parseInt(tmp3[2]));
                let tmp2 = tmp3.splice(1);

                let calodd2top = (parseInt(tmp2[0]) + parseInt(tmp2[1]));
                let cal2bot = (parseInt(_2BOT_NUMBER[0]) + parseInt(_2BOT_NUMBER[1]));


                //------

                switch (betType) {
                    case '_1TOP' :
                        return result._1TOP.indexOf(betNumber) > -1 ? WIN : LOSE;
                    case '_1BOT' :
                        return result._1BOT.indexOf(betNumber) > -1 ? WIN : LOSE;
                    case '_2TOP' :
                        return result._2TOP == betNumber ? WIN : LOSE;
                    case '_2TOD' :
                        return result._2TOD.indexOf(betNumber) > -1 ? WIN : LOSE;
                    case '_2BOT' :
                        return result._2BOT == betNumber ? WIN : LOSE;
                    case '_2TOP_OE' :
                        console.log('_2TOP_OE : ', isEvenOdd(calodd2top));
                        return result._2TOP_OE === isEvenOdd(calodd2top) ? WIN : LOSE;
                    case '_2TOP_OU' :
                        console.log('_2TOP_OU : ', is2TOP_OU(calodd2top));
                        return result._2TOP_OU === is2TOP_OU(calodd2top) ? WIN : LOSE;
                    case '_2BOT_OE' :
                        console.log('_2BOT_OE : ', isEvenOdd(cal2bot));
                        return result._2BOT_OE === isEvenOdd(cal2bot) ? WIN : LOSE;
                    case '_2BOT_OU' :
                        console.log('_2BOT_OU : ', is2BOT_OU(cal2bot));
                        return result._2BOT_OU === is2BOT_OU(cal2bot) ? WIN : LOSE;
                    case '_3TOP' :
                        return result._3TOP === betNumber ? WIN : LOSE;
                    case '_3TOD' :
                        return result._3TOD.indexOf(betNumber) > -1 ? WIN : LOSE;
                    case '_3BOT' :
                        return result._3BOT.indexOf(betNumber) > -1 ? WIN : LOSE;
                    case '_3TOP_OE' :
                        return result._3TOP_OE === isEvenOdd(cal3top) ? WIN : LOSE;
                    case '_3TOP_OU' :
                        return result._3TOP_OU === is3TOP_OU(cal3top) ? WIN : LOSE;
                    case '_4TOP' :
                        return result._4TOP == betNumber ? WIN : LOSE;
                    case '_4TOD' :
                        return result._4TOD.indexOf(betNumber) > -1 ? WIN : LOSE;
                    case '_5TOP' :
                        return result._5TOP == betNumber ? WIN : LOSE;
                    case '_6TOP' :
                        return result._6TOP == betNumber ? WIN : LOSE;
                    default :
                        return null;
                }
            }

            const shareReceive = AgentService.prepareShareReceiveLotto(betResult, betObj);

            updateAgentMemberBalance(shareReceive, betAmount, function (err, updateBalanceResponse) {
                if (err) {
                    return res.send({code: 999, message: "999"});
                } else {

                    let updateBody = {
                        $set: {

                            'lotto.pp.result': getResult(requestBody, betObj.lotto.pp.betType),
                            'commission.superAdmin.winLoseCom': shareReceive.superAdmin.winLoseCom,
                            'commission.superAdmin.winLose': shareReceive.superAdmin.winLose,
                            'commission.superAdmin.totalWinLoseCom': shareReceive.superAdmin.totalWinLoseCom,

                            'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                            'commission.company.winLose': shareReceive.company.winLose,
                            'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                            'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                            'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                            'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                            'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                            'commission.senior.winLose': shareReceive.senior.winLose,
                            'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                            'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                            'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                            'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                            'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                            'commission.agent.winLose': shareReceive.agent.winLose,
                            'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                            'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                            'commission.member.winLose': shareReceive.member.winLose,
                            'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                            'validAmount': betAmount,
                            'betResult': betResult,
                            'isEndScore': true,
                            'status': 'DONE',
                            'updatedDate':DateUtils.getCurrentDate(),
                            'settleDate': DateUtils.getCurrentDate()
                        }
                    }

                    // return res.send({code: 0, message: "999"});

                    BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                        if (err) {
                            console.log(err)
                            // return res.send({code: 999, message: "999"});
                        } else {

                            // console.log('success')
                            // return res.send({code: 0, message: "success"});
                        }
                    });
                }
            });


        });


        cursor.on('end', function () {

            clearLimit((err, response) => {
                return res.send({code: 0, message: "success"});
            });
        });

    });

    function is3TOP_OU(number) {
        if (number >= 0 && number <= 13) {
            return 'UNDER';
        } else if (number >= 14 && number <= 27) {
            return 'OVER';
        }
    }

    function is2TOP_OU(number) {
        if (number >= 0 && number < 10) {
            return 'UNDER';
        } else if (number > 9 && number <= 19) {
            return 'OVER';
        }
    }

    function is2BOT_OU(number) {
        if (number >= 0 && number <= 9) {
            return 'UNDER';
        } else {
            return 'OVER';
        }
    }


    function isEvenOdd(number) {
        if (number % 2 == 0) {
            return 'EVEN';
        } else {
            return 'ODD';
        }
    }


    function getResult(result, betType) {
        switch (betType) {
            case '_1BOT' :
            case '_2BOT' :
            case '_2BOT_OE' :
            case '_2BOT_OU' :
                return result._2BOT;
            case '_3BOT' :
                return result._3BOT.join(',');
            case '_1TOP' :
            case '_2TOP' :
            case '_2TOD' :
            case '_2TOP_OE' :
            case '_2TOP_OU' :
            case '_3TOP' :
            case '_3TOD' :
            case '_3TOP_OE' :
            case '_3TOP_OU' :
            case '_4TOP' :
            case '_4TOD' :
            case '_5TOP' :
            case '_6TOP' :
                return result._6TOP;
            default :
                return null;
        }
    }

    function clearLimit(callback) {
        AgentLottoPPLimitModel.find({}).exec(function (err, response) {


            console.log('length : ', response.length)

            async.each(response, (transaction, callbackWL) => {


                let body = {

                    $set: {
                        'po._1TOP.bets': [],
                        'po._1BOT.bets': [],
                        'po._2TOP.bets': [],
                        'po._2BOT.bets': [],
                        'po._2TOD.bets': [],
                        'po._2TOP_OE.bets': [],
                        'po._2TOP_OU.bets': [],
                        'po._2BOT_OE.bets': [],
                        'po._2BOT_OU.bets': [],
                        'po._3TOP.bets': [],
                        'po._3BOT.bets': [],
                        'po._3TOD.bets': [],
                        'po._3TOP_OE.bets': [],
                        'po._3TOP_OU.bets': [],
                        'po._4TOP.bets': [],
                        'po._4TOD.bets': [],
                        'po._5TOP.bets': [],
                        'po._6TOP.bets': []
                    }
                };


                AgentLottoPPLimitModel.update({_id: transaction._id}, body, function (err, lottoResponse) {
                    if (err) {
                        console.log(err)
                        callbackWL(err);
                    }
                    else {
                        console.log(lottoResponse)
                        callbackWL();
                    }
                });
            }, (err) => {
                callback(null, 'success');
            });

        });
    }

});

const settleLaosSchema = Joi.object().keys({
    gameId: Joi.string().required(),
    L_4TOP: Joi.string().required(),
    L_4TOD: Joi.array().items(Joi.string().required()),
    L_3TOP: Joi.string().required(),
    L_3TOD: Joi.array().items(Joi.string().required()),
    L_2FB: Joi.array().items(Joi.string().required()),

    T_4TOP: Joi.string().required(),
    T_4TOD: Joi.array().items(Joi.string().required()),
    T_3TOP: Joi.string().required(),
    T_3TOD: Joi.array().items(Joi.string().required()),
    T_2TOP: Joi.string().required(),
    T_2TOD: Joi.array().items(Joi.string().required()),
    T_2BOT: Joi.string().required(),
    T_1TOP: Joi.array().items(Joi.string().required()),
    T_1BOT: Joi.array().items(Joi.string().required()),

    signature: Joi.string().required(),
});

router.post('/callback/settle-laos', (req, res) => {
    let validate = Joi.validate(req.body, settleLaosSchema);
    if (validate.error) {
        console.log('Request Incomplete')
        return res.send({
            message: "validation fail",
            results: validate.error.details,
            code: 1001
        });
    }

    // let hashMessage = requestBody.key + requestBody.userId + requestBody.tableId + requestBody.roundId;
    //
    // if (!isValidSignature(requestBody.signature, hashMessage)) {
    //     return res.send({code: 1002, message: "Signature not match"});
    // }

    let requestBody = req.body;

    let model = new LottoLaosSettleHistoryModel(requestBody);
    model.createdDate = DateUtils.getCurrentDate();
    model.save((err, response) => {
        // callback(null, response);
    });

    console.log(requestBody)
    let condition = {
        'gameDate': {$gte: new Date(moment().add(-7, 'd').format('YYYY-MM-DDT11:00:00.000'))},
        'game': 'LOTTO',
        'source': 'AMB_LOTTO_LAOS',
        'lotto.laos.gameId': requestBody.gameId,
        'status': 'RUNNING'
    };


    const cursor = BetTransactionModel.find(condition)
        .batchSize(5000)
        .populate({path: 'memberParentGroup', select: 'type'})
        .lean()
        .cursor();

    cursor.on('data', function (betObj) {


        let betAmount = betObj.amount;

        // betObj.memberCredit; //(betAmount * betObj.commission.member.payout) -  betAmount;
        let betResult = calculateResult(requestBody, betObj.lotto.laos.betType, betObj.lotto.laos.betNumber);

        function calculateResult(result, betType, betNumber) {

            switch (betType) {
                case 'T_1TOP' :
                    return result.T_1TOP.indexOf(betNumber) > -1 ? WIN : LOSE;
                case 'T_1BOT' :
                    return result.T_1BOT.indexOf(betNumber) > -1 ? WIN : LOSE;
                case 'T_2TOP' :
                    return result.T_2TOP == betNumber ? WIN : LOSE;
                case 'T_2TOD' :
                    return result.T_2TOD.indexOf(betNumber) > -1 ? WIN : LOSE;
                case 'T_2BOT' :
                    return result.T_2BOT == betNumber ? WIN : LOSE;
                case 'T_3TOP' :
                    return result.T_3TOP == betNumber ? WIN : LOSE;
                case 'T_3TOD' :
                    return result.T_3TOD.indexOf(betNumber) > -1 ? WIN : LOSE;
                case 'T_4TOP' :
                    return result.T_4TOP == betNumber ? WIN : LOSE;
                case 'T_4TOD' :
                    return result.T_4TOD.indexOf(betNumber) > -1 ? WIN : LOSE;
                default :
                    return null;
            }
        }

        let winBetType;
        if (betObj.lotto.laos.betType == 'L_4TOP') {

            let betNumber = betObj.lotto.laos.betNumber;

            if (requestBody.L_4TOP == betNumber) {
                winBetType = 'L_4TOP';
                betResult = WIN;
            }
            else if (requestBody.L_4TOD.indexOf(betNumber) > -1) {
                winBetType = 'L_4TOD';
                betResult = WIN;
            }
            else if (requestBody.L_3TOP == betNumber.substring(1, 4)) {
                winBetType = 'L_3TOP';
                betResult = WIN;
            }
            else if (requestBody.L_3TOD.indexOf(betNumber.substring(1, 4)) > -1) {
                winBetType = 'L_3TOD';
                betResult = WIN;
            }
            else {

                let isWin = false;

                // หลัง
                if (requestBody.L_2FB[0] == betNumber.substring(2, 4)) {

                    isWin = true;
                }

                // หน้า
                if (requestBody.L_2FB[1] == betNumber.substring(0, 2)) {
                    isWin = true;
                }


                if (isWin) {
                    betResult = WIN;
                    winBetType = 'L_2FB';
                } else {
                    betResult = LOSE;
                }

            }
        } else {
            winBetType = betObj.lotto.laos.betType;
        }

        const shareReceive = AgentService.prepareShareReceiveLottoLaos(betResult, betObj, winBetType);

        updateAgentMemberBalance(shareReceive, betAmount, function (err, updateBalanceResponse) {
            if (err) {
                return res.send({code: 999, message: "999"});
            } else {

                let updateBody = {
                    $set: {

                        'lotto.laos.result': getResult(requestBody, betObj.lotto.laos.betType),
                        'commission.superAdmin.winLoseCom': shareReceive.superAdmin.winLoseCom,
                        'commission.superAdmin.winLose': shareReceive.superAdmin.winLose,
                        'commission.superAdmin.totalWinLoseCom': shareReceive.superAdmin.totalWinLoseCom,

                        'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                        'commission.company.winLose': shareReceive.company.winLose,
                        'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                        'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                        'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                        'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                        'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                        'commission.senior.winLose': shareReceive.senior.winLose,
                        'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                        'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                        'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                        'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                        'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                        'commission.agent.winLose': shareReceive.agent.winLose,
                        'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                        'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                        'commission.member.winLose': shareReceive.member.winLose,
                        'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,

                        'validAmount': betAmount,
                        'betResult': betResult,
                        'isEndScore': true,
                        'status': 'DONE',
                        'remark': 'WIN : ' + winBetType,
                        'updatedDate':DateUtils.getCurrentDate(),
                        'settleDate': DateUtils.getCurrentDate()
                    }
                }

                // return res.send({code: 0, message: "999"});

                BetTransactionModel.update({_id: betObj._id}, updateBody, function (err, response) {
                    if (err) {
                        console.log(err)
                        // return res.send({code: 999, message: "999"});
                    } else {

                        // return res.send({code: 0, message: "success"});
                    }
                });
            }
        });


    });


    cursor.on('end', function () {
        clearLimit((err, response) => {
            return res.send({code: 0, message: "success"});
        });
    });


    function getResult(result, betType) {
        switch (betType) {
            case 'T_1TOP' :
            case 'T_1BOT' :
            case 'T_2TOP' :
            case 'T_2TOD' :
            case 'T_2BOT' :
            case 'T_3TOP' :
            case 'T_3TOD' :
            case 'T_4TOP' :
            case 'T_4TOD' :
            case 'L_4TOP' :
                return result.L_4TOP;
            default :
                return null;
        }
    }

    function clearLimit(callback) {

        let body = {

            $set: {
                'po.T_1TOP.bets': [],
                'po.T_1BOT.bets': [],
                'po.T_2TOP.bets': [],
                'po.T_2BOT.bets': [],
                'po.T_2TOD.bets': [],
                'po.T_3TOP.bets': [],
                'po.T_3TOD.bets': [],
                'po.T_4TOP.bets': [],
                'po.T_4TOD.bets': [],
                'po.L_4TOP.bets': []
            }
        };
        AgentLottoLaosLimitModel.update({}, body, {multi: true}, (err, response) => {
            if (err) {
                callback(err, null);
            }
            else {
                callback(null, 'success');
            }
        });

    }

});


function updateAgentMemberBalance(shareReceive, betAmount, updateCallback) {

    // console.log('updateAgentMemberBalance');
    // console.log('shareReceive : ', shareReceive);


    let balance = Math.abs(shareReceive.memberCredit) + shareReceive.member.totalWinLoseCom;

    try {
        // console.log('update member balance : ', balance);

        MemberService.updateBalance(shareReceive.member.id, balance, shareReceive.betId, 'LOTTO_SETTLE', '', function (err, updateBalanceResponse) {
            if (err) {
                updateCallback(err, null);
            } else {
                updateCallback(null, updateBalanceResponse);
            }
        });
    } catch (err) {
        MemberService.updateBalance(shareReceive.member.id, balance, shareReceive.betId, 'LOTTO_SETTLE_ERROR ' + shareReceive.member.id + ' : ' + balance, '', function (err, updateBalanceResponse) {
            if (err) {
                updateCallback(err, null);
            } else {
                updateCallback(null, updateBalanceResponse);
            }
        });
    }

}


module.exports = router;
