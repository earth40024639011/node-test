const MemberService = require('../../common/memberService');
const SexyService = require('../../common/sexyService2');
const AgentService = require('../../common/agentService');
const DateUtils = require('../../common/dateUtils');
const NumberUtils = require('../../common/numberUtils1');
const express = require('express');
const router = express.Router();
const request = require('request');
const _ = require('underscore');
const async = require("async");
const Joi = require('joi');
const Hashing = require('../../common/hashing');
const mongoose = require('mongoose');
const moment = require('moment');
const roundTo = require('round-to');

const HOST = process.env.SEXY_HOST || 'https://cqyjf.bewpb77.com';
const WEBSITE = process.env.SEXY_WEBSITE || 'SiteAmbbet';
const CERT = process.env.SEXY_CERT || 'xOd0SRmifkys2coa';
const EXTENSION1 = process.env.SEXY_EXTENSION1 || 'ammbet88';
const SEXY_SECRET_KEY = process.env.SEXY_SECRET_KEY || 'AMBDEV';


const DRAW = "DRAW";
const WIN = "WIN";
const HALF_WIN = "HALF_WIN";
const LOSE = "LOSE";
const HALF_LOSE = "HALF_LOSE";


function random(low, high) {
    return Math.floor(Math.random() * (high - low + 1) + low)
}
function generateBetId() {
    return 'BET' + new Date().getTime() + random(1000000, 9999999);
}


function isValidSignature(signature, bodyMessage) {
    // console.log('request signature : ', signature)
    // console.log('correct signature : ', Hashing.makeSignature(SEXY_SECRET_KEY, bodyMessage))
    return signature.toUpperCase() === Hashing.makeSignature(SEXY_SECRET_KEY, bodyMessage).toUpperCase();
}


router.get('/login', function (req, res) {


    const userInfo = req.userInfo;

    let account = userInfo.username_lower;
    let client = req.query.client;


    async.waterfall([callback => {

        AgentService.getUpLineStatus(userInfo.group._id, function (err, status) {
            // console.log('upline status : ' + status);
            if (err) {
                callback(err, null);
            } else {
                if (status.suspend) {
                    callback(5011, null);
                } else {
                    callback(null, status);
                }
            }
        });

    }, (uplineStatus, callback) => {


        MemberService.findById(userInfo._id, 'creditLimit balance limitSetting group suspend', function (err, memberResponse) {
            if (err) {
                callback(err, null);
            } else {

                if (memberResponse.suspend) {
                    callback(5011, null);
                } else if (memberResponse.limitSetting.casino.sexy.isEnable) {
                    AgentService.getSexyStatus(memberResponse.group._id, (err, response) => {

                        if (!response) {
                            callback(1088, null);
                        } else if (!response.isEnable) {
                            callback(1088, null);
                        } else {
                            callback(null, uplineStatus, memberResponse);
                        }
                    });
                } else {
                    callback(1088, null);
                }
            }
        });

    }, (uplineStatus, memberInfo, callback) => {


        SexyService.callCreateMember(account, memberInfo.limitSetting.casino.sexy.limit, (err, keyInfo) => {
            if (err) {
                callback(err, null);
            } else {
                callback(null, memberInfo, keyInfo)
            }
        });

    }], function (err, memberInfo, keyInfo) {

        if (err) {
            if (err == 1088) {
                return res.send({
                    code: 1088,
                    message: 'Service Locked, Please contact your upline.'
                });
            }
            if (err == 5011) {
                return res.send({
                    message: "Account or Upline was suspend.",
                    result: null,
                    code: 5011
                });
            }
            return res.send({code: 999, message: err});
        }


        let mode = 0;
        if (client === 'MB') {
            mode = 1;
        }


        SexyService.callDoLoginAndLaunchGame(account, memberInfo.limitSetting.casino.sexy.limit, (err, loginResponse) => {
            if (err) {
                return res.send({code: 999, message: err});
            }
            return res.send({
                code: 1,
                url: loginResponse.url
            });
        });


    });

});

router.put('/updateBetLimits', function (req, res) {

    let {
        account,
        betLimitIds
    } = req.body;

    const headers = {
        'Content-Type': 'application/json'
    };

    const option = {
        url: `${HOST}/api/${WEBSITE}/updateBetLimitIds?cert=${CERT}&user=${account}&betLimitIds=${JSON.stringify(betLimitIds)}`,
        method: 'GET',
        headers: headers
    };

    request(option, (err, response, body) => {
        if (err) {
            return res.send({code: 999, result: err});
        } else {
            let resultBody = JSON.parse(body);

            if (resultBody.status == 0) {
                return res.send({code: 0, message: 'Update bet limit failed'});
            } else if (resultBody.status == 1) {
                return res.send({code: 1, message: 'Update bet limit success'});
            } else if (resultBody.status == 1001) {
                return res.send({code: 1001, message: 'Not Authorized.'});
            } else {
                return res.send({
                    code: 1011,
                    message: 'User min bet or max bet violate agent bet setting.'
                });
            }

        }
    });
});


router.post('/logoutAll', function (req, res) {

    let {
        password
    } = req.body;

    if (password != 'Super123') {
        return res.send(
            {
                code: 999,
                message: "invalid pass"
            }
        );
    }
    const headers = {
        'Content-Type': 'application/json'
    };

    const option = {
        url: `${HOST}/api/${WEBSITE}/logoutAllUsers?cert=${CERT}`,
        method: 'GET',
        headers: headers
    };

    request(option, (err, response, body) => {
        if (err) {
            return res.send({code: 999, result: err});
        } else {
            let resultBody = JSON.parse(body);
            console.log(resultBody)

            if (resultBody.status == 1) {
                return res.send(
                    {
                        code: 0,
                        message: "success",
                        result: {
                            count: resultBody.count
                        }
                    }
                );
            } else {
                return res.send(
                    {
                        code: 999,
                        message: "status " + resultBody.status,
                        result: 'success'
                    }
                );
            }

        }
    });
});


router.put('/logout', function (req, res) {

    let {
        account,
    } = req.body;

    const headers = {
        'Content-Type': 'application/json'
    };

    const option = {
        url: `${HOST}/api/${WEBSITE}/logout?cert=${CERT}&user=${account}`,
        method: 'GET',
        headers: headers
    };

    request(option, (err, response, body) => {
        if (err) {
            return res.send({code: 999, result: err});
        } else {
            let resultBody = JSON.parse(body);
            if (resultBody.status == 0) {
                return res.send({code: 0, message: 'Logout failed'});
            } else if (resultBody.status == 1) {
                return res.send({code: 1, message: 'Logout success', result: resultBody});
            }

        }
    });
});


router.get('/getLog', function (req, res) {

    let {
        username,
        platform,
        platformTxId
    } = req.query;

    const userInfo = req.userInfo;

    SexyService.callGetTransactionHistoryResult(username, platform, platformTxId, (err, response) => {


        console.log(response)
        return res.send(
            {
                code: 0,
                message: 'success',
                result: response
            }
        );


    });
});


module.exports = router;