const express = require('express');
const router = express.Router();
const request = require('request');
const moment = require('moment');
const crypto = require('crypto');
const utility = require('../../common/utlity');
const async = require("async");
const config = require('config');
const MemberService = require('../../common/memberService');
const Joi = require('joi');
const DateUtils = require('../../common/dateUtils');
const roundTo = require('round-to');
const mongoose = require('mongoose');
const Hashing = require('../../common/hashing');
const _ = require('underscore');
const AgentGroupModel = require('../../models/agentGroup.model.js');
const NumberUtils = require('../../common/numberUtils1');
const AgentService = require('../../common/agentService');
const MemberModel = require("../../models/member.model");

const AmebaService = require('../../common/amebaService');

const AmebaBetHistoryModel = require('../../models/amebaBetHistory.model.js');
const SlotRollbackHistoryModel = require('../../models/slotRollbackHistory.model.js');
const BetTransactionModel = require('../../models/betTransaction.model.js');

const ActiveAmebaTransactionModel = require('../../models/activeAmebaTransaction.model.js');
const LogLive22Model = require('../../models/logLive22.model.js');

const DRAW = "DRAW";
const WIN = "WIN";
const HALF_WIN = "HALF_WIN";
const LOSE = "LOSE";
const HALF_LOSE = "HALF_LOSE";


function random(low, high) {
    return Math.floor(Math.random() * (high - low + 1) + low)
}
function generateBetId() {
    return 'BET' + new Date().getTime() + random(1000000, 9999999);
}


router.get('/getGameList', (req, res) => {
    try {
        AmebaService.callGetGameList((err, gameList) => {

            gameList = gameList.filter((data) => {
                return data.id !== 31
            });
            // console.log(response,body,error)
            if (err) {
                return res.send({message: err, code: 999});
            } else {
                // let json = JSON.parse(body);

                // console.log(body)
                gameList = _.map(gameList, (item) => {
                    return {
                        GameId: item.id,
                        // GameId:item.filter((item.id !== '31')),
                        GameName: item.locale.enUS.name,
                        ImageUrl: AmebaService.getGameImage(item.id),
                        GameType:"Slots",
                        Provider:"Ameba"
                    }
                });
                return res.send({data: gameList, code: 0});
            }
        });
    } catch (e) {
        return res.send({message: e, code: 999});
    }
});


const callbackGetBalanceSchema = Joi.object().keys({
    action: Joi.string().required(),
    site_id: Joi.string().required(),
    account_name: Joi.string().required(),
});

const callbackBetSchema = Joi.object().keys({
    action: Joi.string().required(),
    site_id: Joi.string().required(),
    account_name: Joi.string().required(),
    bet_amt: Joi.string().required(),
    game_id: Joi.string().required(),
    round_id: Joi.string().required(),
    tx_id: Joi.string().required(),
    session_id: Joi.string().required(),
    free: Joi.string().required(),
});

//2. get_balance
router.post('/callback', (req, res) => {

    // getBodyFromToken(req.headers)
    AmebaService.getRequestBody(req, (err, result) => {

        if (err) {
            console.log('ameba error ', err);
            return res.send({
                error_code: "SignatureNotMatch",
                balance: 0
            });
        }
        let requestBody = result;
        // let requestBody = req.body;

        console.log(requestBody)
        if (requestBody.action === 'get_balance') {

            // let validateAgent = Joi.validate(requestBody, callbackGetBalanceSchema);
            // if (validateAgent.error) {
            //     return res.send({
            //         codeId: CODE_PARAMETER_ERROR,
            //         token: requestBody.token
            //     });
            // }

            getBalanceFunction(requestBody, res);

        } else if (requestBody.action === 'bet') {

            betFunction(requestBody, res)

        } else if (requestBody.action === 'cancel_bet') {

            cancelFunction(requestBody, res)

        } else if (requestBody.action === 'payout') {

            settleFunction(requestBody, res);

        }

    });

});

function getBalanceFunction(requestBody, res) {

    MemberService.findByUserNameForPartnerService(requestBody.account_name, (err, memberResponse) => {

        if (err) {
            return res.send({
                error_code: "InternalError",
                balance: 0
            });

        }

        if (memberResponse) {

            let credit = roundTo((memberResponse.creditLimit + memberResponse.balance), 2);

            return res.send({
                error_code: "OK",
                balance: credit
            });

        } else {

            return res.send({
                error_code: "PlayerNotFound",
                balance: 0
            });
        }

    });
}

function betFunction(requestBody, res) {

    let body = {
        action: requestBody.action,
        siteId: requestBody.site_id,
        accountName: requestBody.account_name,
        betAmt: requestBody.bet_amt,
        gameId: requestBody.game_id,
        roundId: requestBody.round_id,
        txId: requestBody.tx_id,
        sessionId: requestBody.session_id,
        free: requestBody.free,
        createdDate: DateUtils.getCurrentDate()
    };

    let model = new AmebaBetHistoryModel(body);
    model.save((err, agentResponse) => {

        if (err) {
            // callback(err, null);
        } else {
            // callback(null, agentResponse);
        }
    });

    let betAmount = requestBody.free ? 0 : roundTo(Number.parseFloat(requestBody.bet_amt), 2);

    async.series([callback => {

        MemberService.findByUserNameForPartnerService(requestBody.account_name, function (err, memberResponse) {
            if (err) {
                callback(err, null);
                return;
            }

            if (memberResponse) {
                if (roundTo((memberResponse.creditLimit + memberResponse.balance), 2) < betAmount) {
                    callback(900605, memberResponse);
                } else {
                    callback(null, memberResponse);
                }
            }
        });

    }], (err, asyncResponse) => {

        if (err) {
            if (err == 900404) {
                return res.send({
                    error_code: "PlayerNotFound",
                    balance: 0
                });
            } else if (err == 900605) {
                let memberInfo = asyncResponse[0];
                let credit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2);

                return res.send({
                    error_code: "InsufficientBalance",
                    balance: credit
                });

            } else {
                return res.send({
                    error_code: "InternalError",
                    balance: 0
                });
            }
        }

        let memberInfo = asyncResponse[0];
        let credit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2);

        let condition = {
            'memberId': memberInfo._id,
            'slot.roundId': requestBody.round_id
        };

        ActiveAmebaTransactionModel.findOne(condition, (err, response) => {


            if (response) {

                let txIds = _.map(response.slot.txns, item => {
                    return item.txId;
                });

                const sameTxId = _.intersection(txIds, [requestBody.tx_id]);


                if (sameTxId.length > 0) {
                    return res.send({
                        error_code: "AlreadyProcessed",
                        balance: credit
                    });
                }

                let update = {
                    $inc: {
                        'amount': betAmount,
                        "memberCredit": (betAmount * -1),
                        "payout": betAmount
                    },
                    $push: {
                        'slot.txns': {
                            $each: [{
                                txId: requestBody.tx_id,
                                betAmt: requestBody.bet_amt,
                                sessionId: requestBody.session_id,
                                free: requestBody.free,
                            }]
                        }
                    }
                };


                ActiveAmebaTransactionModel.update({
                    _id: response._id,
                    'slot.txns.txId': {$nin: requestBody.tx_id}
                }, update, (err, updateTranResponse) => {

                    if (updateTranResponse.nModified == 1) {

                        MemberService.updateBalance(memberInfo._id, (betAmount * -1), response.betId, 'SLOT_AMEBA_BET', requestBody.tx_id, function (err, updateBalanceResponse) {
                            if (err) {
                                if (err == 888) {
                                    return res.send({
                                        error_code: "AlreadyProcessed",
                                        balance: credit
                                    });
                                }
                                return res.send({
                                    error_code: "InternalError",
                                    balance: credit
                                });
                            } else {
                                return res.send({
                                    error_code: "OK",
                                    balance: updateBalanceResponse.newBalance
                                });

                            }
                        });
                    } else if (updateTranResponse.ok == 1 && updateTranResponse.nModified == 0) {
                        return res.send({
                            error_code: "AlreadyProcessed",
                            balance: credit
                        });
                    } else {
                        return res.send({
                            error_code: "InternalError",
                            balance: credit
                        });
                    }
                });

            } else {

                AgentGroupModel.findById(memberInfo.group).select('_id type parentId shareSetting commissionSetting').populate({
                    path: 'parentId',
                    select: '_id type parentId shareSetting commissionSetting name',
                    populate: {
                        path: 'parentId',
                        select: '_id type parentId shareSetting commissionSetting name',
                        populate: {
                            path: 'parentId',
                            select: '_id type parentId shareSetting commissionSetting name',
                            populate: {
                                path: 'parentId',
                                select: '_id type parentId shareSetting commissionSetting name',
                                populate: {
                                    path: 'parentId',
                                    select: '_id type parentId shareSetting commissionSetting name',
                                    populate: {
                                        path: 'parentId',
                                        select: '_id type parentId shareSetting commissionSetting name',
                                    }
                                }
                            }
                        }
                    }

                }).exec(function (err, agentGroups) {

                    let body = {
                        memberId: memberInfo._id,
                        memberParentGroup: memberInfo.group,
                        memberUsername: memberInfo.username_lower,
                        betId: generateBetId(),
                        slot: {
                            siteId: requestBody.site_id,
                            accountName: requestBody.account_name,
                            gameId: requestBody.game_id,
                            gameName: AmebaService.getGameName(requestBody.game_id),
                            roundId: requestBody.round_id,
                            txns: [
                                {
                                    txId: requestBody.tx_id,
                                    betAmt: requestBody.bet_amt,
                                    sessionId: requestBody.session_id,
                                    free: requestBody.free,
                                }
                            ]
                        },
                        amount: betAmount,
                        memberCredit: betAmount * -1,
                        payout: betAmount,
                        commission: {},
                        status: 'RUNNING',
                        gameDate: DateUtils.getCurrentDate(),
                        createdDate: DateUtils.getCurrentDate(),
                        currency: memberInfo.currency,
                        ipAddress:memberInfo.ipAddress
                    };

                    prepareCommission(memberInfo, body, agentGroups, null, 0, 0);

                    let betTransactionModel = new ActiveAmebaTransactionModel(body);

                    betTransactionModel.save(function (err, response) {

                        if (err) {
                            if (err.code == 11000) {
                                return res.send({
                                    error_code: "AlreadyProcessed",
                                    balance: credit
                                });
                            }
                            return res.send({
                                error_code: "InternalError",
                                balance: credit
                            });
                        }

                        MemberService.updateBalance(memberInfo._id, (betAmount * -1), response.betId, 'SLOT_AMEBA_BET', requestBody.tx_id, function (err, updateBalanceResponse) {
                            if (err) {
                                return res.send({
                                    error_code: "InternalError",
                                    balance: credit
                                });
                            } else {

                                return res.send({
                                    error_code: "OK",
                                    balance: updateBalanceResponse.newBalance
                                });
                            }
                        });
                    });
                });


            }

        });

    });
}

function cancelFunction(requestBody, res) {

    let body = {
        action: requestBody.action,
        siteId: requestBody.site_id,
        accountName: requestBody.account_name,
        betAmt: requestBody.bet_amt,
        gameId: requestBody.game_id,
        roundId: requestBody.round_id,
        txId: requestBody.tx_id,
        sessionId: requestBody.session_id,
        free: requestBody.free,
        createdDate: DateUtils.getCurrentDate()
    };

    let model = new AmebaBetHistoryModel(body);
    model.save((err, agentResponse) => {

        if (err) {
            // callback(err, null);
        } else {
            // callback(null, agentResponse);
        }
    });


    async.series([callback => {

        MemberService.findByUserNameForPartnerService(requestBody.account_name, function (err, memberResponse) {
            if (err) {
                callback(err, null);
                return;
            }
            if (memberResponse) {
                callback(null, memberResponse);
            } else {
                callback(900404, null);
            }
        });

    }], (err, asyncResponse) => {

        if (err) {
            if (err == 900404) {
                return res.send({
                    error_code: "PlayerNotFound",
                    balance: 0
                });
            } else {
                return res.send({
                    error_code: "InternalError",
                    balance: 0
                });
            }
        }


        let memberInfo = asyncResponse[0];
        let credit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2);


        let condition = {
            'memberUsername': requestBody.account_name,
            'slot.roundId': requestBody.round_id,
            'slot.txns.txId': requestBody.tx_id
        };


        ActiveAmebaTransactionModel.findOne(condition).exec(function (err, activeAmebaResponse) {
            if (err) {
                return res.send({
                    error_code: "InternalError",
                    balance: 0
                });
            } else {


                if (!activeAmebaResponse) {
                    return res.send({
                        error_code: "BetNotFound",
                        balance: credit
                    });
                }

                let betAmount = Math.abs(requestBody.bet_amt);

                //HAVE SAME SERIAL
                let updateCondition = {
                    $pull: {'slot.txns': {"txId": requestBody.tx_id}},
                    $inc: {
                        amount: betAmount * -1,
                        memberCredit: betAmount
                    }
                };

                ActiveAmebaTransactionModel.update({
                    _id: activeAmebaResponse._id,
                    'slot.txns.txId': requestBody.tx_id
                }, updateCondition, {safe: true}, function (err, cancelResponse) {
                    if (err) {
                        return res.send({
                            error_code: "InternalError",
                            balance: 0
                        });
                    }

                    if (cancelResponse.nModified == 1) {

                        let updateAmount = requestBody.free ? 0 : requestBody.bet_amt;

                        MemberService.updateBalance(activeAmebaResponse.memberId, updateAmount , activeAmebaResponse.betId, 'SLOT_AMEBA_CANCEL', requestBody.tx_id, function (err, updateBalanceResponse) {
                            if (err) {
                                if (err == 888) {
                                    return res.send({
                                        error_code: "AlreadyProcessed",
                                        balance: credit
                                    });
                                }

                                return res.send({
                                    error_code: "InternalError",
                                    balance: 0
                                });
                            } else {
                                //
                                // ActiveAmebaTransactionModel.remove({
                                //     _id: activeAmebaResponse._id,
                                //     "slot.txns": {$size: 0}
                                // }, function (err, removeResponse) {
                                //     console.log("remove res : ", removeResponse)
                                //     console.log(err)
                                // });


                                //TODO queue
                                return res.send({
                                    error_code: "OK",
                                    balance: updateBalanceResponse.newBalance
                                });

                            }
                        });
                    }
                });

            }
        });


    });

}

function settleFunction(requestBody, res) {

    let body = {
        action: requestBody.action,
        siteId: requestBody.site_id,
        accountName: requestBody.account_name,
        betAmt: requestBody.bet_amt,
        payoutAmt: requestBody.payout_amt,
        gameId: requestBody.game_id,
        roundId: requestBody.round_id,
        txId: requestBody.tx_id,
        rebateAmt: requestBody.rebate_amt,
        sessionId: requestBody.session_id,
        free: requestBody.free,
        prizeType: requestBody.prize_type,
        prizeAmt: requestBody.prize_amt,
        sumPayoutAmt:requestBody.sum_payout_amt,
        time:requestBody.time,
        jp: {
            pcConAmt: requestBody.jp.pc_con_amt,
            jcConAmt: requestBody.jp.jc_con_amt,
            winId: requestBody.jp.win_id,
            pcWinAmt: requestBody.jp.pc_win_amt,
            jcWinAmt: requestBody.jp.jc_win_amt,
            winLv: requestBody.jp.win_lv,
            directPay: requestBody.jp.direct_pay,
        },
        createdDate: DateUtils.getCurrentDate()
    };

    let model = new AmebaBetHistoryModel(body);
    model.save((err, agentResponse) => {
        if (err) {
            // callback(err, null);
        } else {
            // callback(null, agentResponse);
        }
    });

    let betAmount = roundTo(Number.parseFloat(requestBody.bet_amt), 2);

    let payoutAmount  = Number.parseFloat(requestBody.sum_payout_amt);

    async.series([callback => {

        MemberService.findByUserNameForPartnerService(requestBody.account_name, function (err, memberResponse) {
            if (err) {
                callback(err, null);
                return;
            }
            if (memberResponse) {
                callback(null, memberResponse);
            } else {
                callback(900404, null);
            }
        });

    }], (err, asyncResponse) => {

        if (err) {
            if (err == 900404) {
                return res.send({
                    error_code: "PlayerNotFound",
                    balance: 0
                });
            } else {
                return res.send({
                    error_code: "InternalError",
                    balance: 0
                });
            }
        }

        let memberInfo = asyncResponse[0];

        let credit = roundTo((memberInfo.creditLimit + memberInfo.balance), 2);

        async.series([callback => {

            let condition = {
                'memberUsername': requestBody.account_name,
                'slot.roundId': requestBody.round_id
            };

            ActiveAmebaTransactionModel.findOne(condition).lean().exec(function (err, mainObj) {

                if (err) {
                    callback(err, null);
                    return
                }

                if (!mainObj) {
                    callback(900415, null);
                } else {

                    if (mainObj.status == 'DONE') {
                        callback(900409)
                    } else {
                        callback(null, mainObj);
                    }
                }

            });


        }], (err, asyncResponse) => {

            let activeBetObj = asyncResponse[0];

            if (err) {
                if (err == 900415) {
                    return res.send({
                        error_code: "TransactionNotMatch",
                        balance: credit
                    });
                }

                if (err == 900409) {
                    return res.send({
                        error_code: "AlreadyProcessed",
                        balance: credit
                    });
                }
            }

            let winLose = payoutAmount - betAmount;
            let betResult = winLose > 0 ? WIN : winLose < 0 ? LOSE : DRAW;

            const shareReceive = AgentService.prepareShareReceive(winLose, betResult, activeBetObj);

            updateAgentMemberBalance(shareReceive, betAmount, requestBody.tx_id, function (err, updateBalanceResponse) {

                if (err) {
                    if (err == 888) {
                        return res.send({
                            error_code: "AlreadyProcessed",
                            balance: credit
                        });
                    }
                    return res.send({
                        error_code: "InternalError",
                        balance: credit
                    });
                } else {

                    let updateBody = {
                        $set: {
                            'slot.prizeType': requestBody.prize_type,
                            'slot.prizeAmt': requestBody.prize_amt,
                            'slot.payoutAmt': requestBody.payout_amt,
                            'slot.rebateAmt': requestBody.rebate_amt,
                            'slot.sumPayoutAmt':requestBody.sum_payout_amt,
                            'slot.time':requestBody.time,
                            'slot.jp.pcConAmt': requestBody.jp.pc_con_amt,
                            'slot.jp.jcConAmt': requestBody.jp.jc_con_amt,
                            'slot.jp.winId': requestBody.jp.win_id,
                            'slot.jp.pcWinAmt': requestBody.jp.pc_win_amt,
                            'slot.jp.jcWinAmt': requestBody.jp.jc_win_amt,
                            'slot.jp.winLv': requestBody.jp.win_lv,
                            'slot.jp.directPay': requestBody.jp.direct_pay,
                            'commission.superAdmin.winLoseCom': shareReceive.superAdmin.winLoseCom,
                            'commission.superAdmin.winLose': shareReceive.superAdmin.winLose,
                            'commission.superAdmin.totalWinLoseCom': shareReceive.superAdmin.totalWinLoseCom,

                            'commission.company.winLoseCom': shareReceive.company.winLoseCom,
                            'commission.company.winLose': shareReceive.company.winLose,
                            'commission.company.totalWinLoseCom': shareReceive.company.totalWinLoseCom,

                            'commission.shareHolder.winLoseCom': shareReceive.shareHolder.winLoseCom,
                            'commission.shareHolder.winLose': shareReceive.shareHolder.winLose,
                            'commission.shareHolder.totalWinLoseCom': shareReceive.shareHolder.totalWinLoseCom,

                            'commission.senior.winLoseCom': shareReceive.senior.winLoseCom,
                            'commission.senior.winLose': shareReceive.senior.winLose,
                            'commission.senior.totalWinLoseCom': shareReceive.senior.totalWinLoseCom,

                            'commission.masterAgent.winLoseCom': shareReceive.masterAgent.winLoseCom,
                            'commission.masterAgent.winLose': shareReceive.masterAgent.winLose,
                            'commission.masterAgent.totalWinLoseCom': shareReceive.masterAgent.totalWinLoseCom,

                            'commission.agent.winLoseCom': shareReceive.agent.winLoseCom,
                            'commission.agent.winLose': shareReceive.agent.winLose,
                            'commission.agent.totalWinLoseCom': shareReceive.agent.totalWinLoseCom,

                            'commission.member.winLoseCom': shareReceive.member.winLoseCom,
                            'commission.member.winLose': shareReceive.member.winLose,
                            'commission.member.totalWinLoseCom': shareReceive.member.totalWinLoseCom,
                            'validAmount': betAmount,
                            'betResult': betResult,
                            'isEndScore': true,
                            'status': 'DONE'
                        }
                    };

                    ActiveAmebaTransactionModel.update({_id: activeBetObj._id}, updateBody, function (err, response) {

                        if (response.nModified == 1) {

                            //TODO queue process
                            let body = {
                                memberId: activeBetObj.memberId,
                                memberParentGroup: activeBetObj.memberParentGroup,
                                memberUsername: activeBetObj.memberUsername,
                                betId: activeBetObj.betId,
                                slot: {ameba: activeBetObj.slot},
                                amount: activeBetObj.amount,
                                memberCredit: activeBetObj.amount * -1,
                                payout: activeBetObj.amount,
                                gameType: 'TODAY',
                                acceptHigherPrice: false,
                                priceType: 'TH',
                                game: 'GAME',
                                source: 'AMEBA',
                                status: 'DONE',
                                commission: activeBetObj.commission,
                                gameDate: DateUtils.getCurrentDate(),
                                createdDate: DateUtils.getCurrentDate(),
                                validAmount: activeBetObj.amount,
                                betResult: betResult,
                                isEndScore: true,
                                currency: memberInfo.currency
                            };

                            body.commission.superAdmin.winLoseCom = shareReceive.superAdmin.winLoseCom;
                            body.commission.superAdmin.winLose = shareReceive.superAdmin.winLose;
                            body.commission.superAdmin.totalWinLoseCom = shareReceive.superAdmin.totalWinLoseCom;

                            body.commission.company.winLoseCom = shareReceive.company.winLoseCom;
                            body.commission.company.winLose = shareReceive.company.winLose;
                            body.commission.company.totalWinLoseCom = shareReceive.company.totalWinLoseCom;

                            body.commission.shareHolder.winLoseCom = shareReceive.shareHolder.winLoseCom;
                            body.commission.shareHolder.winLose = shareReceive.shareHolder.winLose;
                            body.commission.shareHolder.totalWinLoseCom = shareReceive.shareHolder.totalWinLoseCom;

                            body.commission.senior.winLoseCom = shareReceive.senior.winLoseCom;
                            body.commission.senior.winLose = shareReceive.senior.winLose;
                            body.commission.senior.totalWinLoseCom = shareReceive.senior.totalWinLoseCom;

                            body.commission.masterAgent.winLoseCom = shareReceive.masterAgent.winLoseCom;
                            body.commission.masterAgent.winLose = shareReceive.masterAgent.winLose;
                            body.commission.masterAgent.totalWinLoseCom = shareReceive.masterAgent.totalWinLoseCom;

                            body.commission.agent.winLoseCom = shareReceive.agent.winLoseCom;
                            body.commission.agent.winLose = shareReceive.agent.winLose;
                            body.commission.agent.totalWinLoseCom = shareReceive.agent.totalWinLoseCom;

                            body.commission.member.winLoseCom = shareReceive.member.winLoseCom;
                            body.commission.member.winLose = shareReceive.member.winLose;
                            body.commission.member.totalWinLoseCom = shareReceive.member.totalWinLoseCom;

                            body.slot.ameba.prizeType = requestBody.prize_type;
                            body.slot.ameba.priceAmt = requestBody.prize_amt;
                            body.slot.ameba.payoutAmt = requestBody.payout_amt;
                            body.slot.ameba.rebateAmt = requestBody.rebate_amt;
                            body.slot.ameba.payout = requestBody.Payout;
                            body.slot.ameba.winLose = requestBody.WinLose;
                            body.slot.ameba.sumPayoutAmt = requestBody.sum_payout_amt;
                            body.slot.ameba.time = requestBody.time;
                            body.slot.ameba.jp = {
                                pcConAmt: requestBody.jp.pc_con_amt,
                                jcConAmt: requestBody.jp.jc_con_amt,
                                winId: requestBody.jp.win_id,
                                pcWinAmt: requestBody.jp.pc_win_amt,
                                jcWinAmt: requestBody.jp.jc_win_amt,
                                winLv: requestBody.jp.win_lv,
                                directPay: requestBody.jp.direct_pay,
                            };
                            body.settleDate = DateUtils.getCurrentDate();

                            let betTransactionModel = new BetTransactionModel(body);
                            betTransactionModel.save((err, betObj) => {
                                if (err) {
                                    if (err.code == 11000) {
                                        return res.send({
                                            error_code: "AlreadyProcessed",
                                            balance: credit
                                        });
                                    }

                                    return res.send({
                                        error_code: "InternalError",
                                        balance: credit
                                    });
                                } else {

                                    return res.send({
                                        error_code: "OK",
                                        balance: updateBalanceResponse.newBalance
                                    });
                                }


                            });

                        } else {
                            return res.send({
                                error_code: "InternalError",
                                balance: credit
                            });
                        }
                    });
                }
            });

        });
    });

}


router.get('/login', (req, res) => {

    let ClientType;
    let GameId;

    // if (req.userInfo.username != '51rrwin') {
    //     return res.send({
    //         code: 1088,
    //         message: 'Service Locked, Please contact your upline.'
    //     });
    // }
    async.series([callback => {

        MemberService.findByUserName(req.userInfo.username, function (err, memberResponse) {
            if (err) {
                callback(err, null);
            } else {

                if (memberResponse.suspend) {
                    callback(5011, null);
                } else if (memberResponse.limitSetting.game.slotXO.isEnable) {

                    AgentService.getLive22Status(memberResponse.group._id, (err, response) => {
                        if (!response.isEnable) {
                            callback(1088, null);
                        } else {
                            callback(null, memberResponse);
                        }
                    });
                } else {
                    callback(1088, null);
                }


            }
        });

    }, callback => {

        AgentService.getUpLineStatus(req.userInfo.group._id, function (err, status) {
            console.log('upline status : ' + status);
            if (err) {
                callback(err, null);
            } else {
                if (status.suspend) {
                    callback(5011, null);
                } else {
                    callback(null, status);
                }
            }
        });

    }], (err, asyncResponse) => {

        if (err) {
            if (err == 1088) {
                return res.send({
                    code: 1088,
                    message: 'Service Locked, Please contact your upline.'
                });
            }
            if (err == 5011) {
                return res.send({
                    message: "Account or Upline was suspend.",
                    result: null,
                    code: 5011
                });
            }
            return res.send({code: 999, message: err});
        }
        let memberInfo = asyncResponse[0];

        // call api

        AmebaService.callCreateAccount(memberInfo.username_lower, (err, createAccountResponse) => {

            if (err) {
                return res.send({message: 'fail', code: 999});
            } else {
                //bar 601 , sicbo 631 , dragon 640

                let gameId;
                if (req.query.GameId) {
                    gameId = req.query.GameId
                    ;
                } else {
                    gameId = 0;
                }
                AmebaService.callRegisterToken(memberInfo.username_lower, gameId, (err, url) => {

                    if (err) {
                        return res.send({message: err, code: 999});
                    } else {
                        return res.send({message: 'success', code: 0, url: url});
                    }
                });


            }
        });
    });

});


function prepareCommission(memberInfo, body, currentAgent, overAgent, remaining, overAllShare) {


    if (currentAgent && (currentAgent.type === 'SUPER_ADMIN' || currentAgent.parentId)) {


        console.log('==============================================');
        console.log('process : ' + currentAgent.type + ' : ' + currentAgent.name);


        let money = body.amount;


        if (!overAgent) {
            overAgent = {};
            overAgent.type = 'member';
            overAgent.shareSetting = {};
            overAgent.shareSetting.game = {};
            overAgent.shareSetting.game.slotXO = {};
            overAgent.shareSetting.game.slotXO.parent = memberInfo.shareSetting.game.slotXO.parent;
            overAgent.shareSetting.game.slotXO.own = 0;
            overAgent.shareSetting.game.slotXO.remaining = 0;

            body.commission.member = {};
            body.commission.member.parent = memberInfo.shareSetting.game.slotXO.parent;
            body.commission.member.commission = memberInfo.commissionSetting.game.slotXO;

        }

        console.log('');
        console.log('---- setting ----');
        console.log('ส่วนแบ่งที่ได้มามีทั้งหมด : ' + currentAgent.shareSetting.game.slotXO.own + ' %');
        console.log('ขอส่วนแบ่งจาก : ' + overAgent.type + ' ' + overAgent.shareSetting.game.slotXO.parent + ' %');
        console.log('ให้ส่วนแบ่ง : ' + overAgent.type + ' ที่เหลือไป ' + overAgent.shareSetting.game.slotXO.own + ' %');
        console.log('remaining จาก : ' + overAgent.type + ' : ' + overAgent.shareSetting.game.slotXO.remaining + ' %');
        console.log('โดนบังคับเสียขั้นต่ำ : ' + currentAgent.shareSetting.game.slotXO.min + ' %');
        console.log('---- end setting ----');
        console.log('');


        // console.log('และได้รับ remaining ไว้ : '+overAgent.shareSetting.game.slotXO.remaining+' %');

        let currentPercentReceive = overAgent.shareSetting.game.slotXO.parent;
        console.log('% ที่ได้ : ' + currentPercentReceive);
        console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');
        let totalRemaining = remaining;

        if (overAgent.shareSetting.game.slotXO.remaining > 0) {
            // console.log()
            // console.log('มีค่า remaining จาก : ' + overAgent.type + ' : ' + remaining + ' %');

            if (overAgent.shareSetting.game.slotXO.remaining > totalRemaining) {
                console.log('รับ remaining ทั้งหมดไว้: ' + totalRemaining + ' %');
                currentPercentReceive += totalRemaining;
                totalRemaining = 0;
            } else {
                totalRemaining = remaining - overAgent.shareSetting.game.slotXO.remaining;
                console.log('หักค่า remaining ที่ได้รับมากับที่เซตรับไว้ = ' + remaining + ' - ' + overAgent.shareSetting.game.slotXO.remaining + ' = ' + totalRemaining);
                currentPercentReceive += overAgent.shareSetting.game.slotXO.remaining;
            }

        }

        console.log('รวม % ที่ได้รับ : ' + currentPercentReceive);


        let unUseShare = currentAgent.shareSetting.game.slotXO.own -
            (overAgent.shareSetting.game.slotXO.parent + overAgent.shareSetting.game.slotXO.own);

        console.log('เหลือส่วนแบ่งที่ไม่ได้ใช้ : ' + unUseShare + ' %');
        totalRemaining = (unUseShare + totalRemaining);
        console.log('ส่วนแบ่งที่ไม่ได้ใช้ บวกกับค่า remaining ท่ี่เหลือจะเป็น : ' + totalRemaining + ' %');

        console.log('จากที่โดนบังคับเสียขั้นต่ำ : ' + currentAgent.shareSetting.game.slotXO.min + ' %');
        console.log('overAllShare : ', overAllShare);
        if (currentAgent.shareSetting.game.slotXO.min > 0 && ((overAllShare + currentPercentReceive) < currentAgent.shareSetting.game.slotXO.min)) {

            if (currentPercentReceive < currentAgent.shareSetting.game.slotXO.min) {
                console.log('% ที่ได้รับ < ขั้นต่ำที่ถูกตั้ง min ไว้');
                let percent = currentAgent.shareSetting.game.slotXO.min - (currentPercentReceive + overAllShare);
                console.log('หักออกไป ' + percent + ' % : แล้วไปเพิ่มในส่วนที่ต้องรับผิดชอบ ');
                totalRemaining = totalRemaining - percent;
                if (totalRemaining < 0) {
                    totalRemaining = 0;
                }
                currentPercentReceive += percent;
            }
        } else {
            // currentPercentReceive += totalRemaining;
        }


        if (currentAgent.type === 'SUPER_ADMIN') {
            currentPercentReceive += totalRemaining;
        }

        let getMoney = (money * NumberUtils.convertPercent(currentPercentReceive)).toFixed(2);
        overAllShare = overAllShare + currentPercentReceive;

        console.log('ยอดแทง ' + money + ' ได้ทั้งหมด : ' + currentPercentReceive + ' %');

        console.log('รวม % + % remaining  จะเป็นสัดส่วนที่ได้ทั้งหมด = ' + getMoney);
        console.log('ค่าที่จะถูกคืนกลับไป (remaining) : ' + totalRemaining + ' %');

        console.log('จำนวน % ที่ถูกแบ่งไปแล้วทั้งหมด : ', overAllShare);

        //set commission
        let agentCommission = currentAgent.commissionSetting.game.slotXO;


        switch (currentAgent.type) {
            case 'SUPER_ADMIN':
                body.commission.superAdmin = {};
                body.commission.superAdmin.group = currentAgent._id;
                body.commission.superAdmin.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.superAdmin.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.superAdmin.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.superAdmin.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.superAdmin.shareReceive = Math.abs(currentPercentReceive);
                body.commission.superAdmin.commission = agentCommission;
                body.commission.superAdmin.amount = getMoney;
                break;
            case 'COMPANY':
                body.commission.company = {};
                body.commission.company.parentGroup = currentAgent.parentId;
                body.commission.company.group = currentAgent._id;
                body.commission.company.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.company.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.company.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.company.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.company.shareReceive = Math.abs(currentPercentReceive);
                body.commission.company.commission = agentCommission;
                body.commission.company.amount = getMoney;
                break;
            case 'SHARE_HOLDER':
                body.commission.shareHolder = {};
                body.commission.shareHolder.parentGroup = currentAgent.parentId;
                body.commission.shareHolder.group = currentAgent._id;
                body.commission.shareHolder.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.shareHolder.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.shareHolder.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.shareHolder.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.shareHolder.shareReceive = currentPercentReceive;
                body.commission.shareHolder.commission = agentCommission;
                body.commission.shareHolder.amount = getMoney;
                break;
            case 'SENIOR':
                body.commission.senior = {};
                body.commission.senior.parentGroup = currentAgent.parentId;
                body.commission.senior.group = currentAgent._id;
                body.commission.senior.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.senior.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.senior.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.senior.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.senior.shareReceive = currentPercentReceive;
                body.commission.senior.commission = agentCommission;
                body.commission.senior.amount = getMoney;
                break;
            case 'MASTER_AGENT':
                body.commission.masterAgent = {};
                body.commission.masterAgent.parentGroup = currentAgent.parentId;
                body.commission.masterAgent.group = currentAgent._id;
                body.commission.masterAgent.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.masterAgent.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.masterAgent.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.masterAgent.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.masterAgent.shareReceive = currentPercentReceive;
                body.commission.masterAgent.commission = agentCommission;
                body.commission.masterAgent.amount = getMoney;
                break;
            case 'AGENT':
                body.commission.agent = {};
                body.commission.agent.parentGroup = currentAgent.parentId;
                body.commission.agent.group = currentAgent._id;
                body.commission.agent.parent = currentAgent.shareSetting.game.slotXO.parent;
                body.commission.agent.own = currentAgent.shareSetting.game.slotXO.own;
                body.commission.agent.remaining = currentAgent.shareSetting.game.slotXO.remaining;
                body.commission.agent.min = currentAgent.shareSetting.game.slotXO.min;
                body.commission.agent.shareReceive = currentPercentReceive;
                body.commission.agent.commission = agentCommission;
                body.commission.agent.amount = getMoney;
                break;
        }

        prepareCommission(memberInfo, body, currentAgent.parentId, currentAgent, totalRemaining, overAllShare);
    }
}

function updateAgentMemberBalance(shareReceive, betAmount, ref1, updateCallback) {

    console.log('updateAgentMemberBalance');
    console.log('shareReceive : ', shareReceive);

    let balance = betAmount + shareReceive.member.totalWinLoseCom;

    try {
        console.log('update member balance : ', balance);

        MemberService.updateBalance(shareReceive.member.id, balance, shareReceive.betId, 'SLOT_AMEBA_SETTLE', ref1, function (err, updateBalanceResponse) {
            if (err) {
                updateCallback(err, null);
            } else {
                updateCallback(null, updateBalanceResponse);
            }
        });
    } catch (err) {
        MemberService.updateBalance(shareReceive.member.id, balance, shareReceive.betId, 'SLOT_AMEBA_SETTLE_ERROR ' + shareReceive.member.id + ' : ' + balance, '', function (err, updateBalanceResponse) {
            if (err) {
                updateCallback(err, null);
            } else {
                updateCallback(null, updateBalanceResponse);
            }
        });
    }

}

module.exports = router;
