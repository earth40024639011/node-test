const express = require('express');
const router = express.Router();
const fs = require('fs');
const mkdirp = require('mkdirp');
const async = require('async');
const NexFile = require('nex-file-simple1').FileS3;
const multer = require('multer');
const Joi = require('joi');
const config = require('config');

const upload = multer();

const nexS3 = new NexFile(config.get('S3'));


const uploadSchema = Joi.object().keys({
    category: Joi.string().required(),
    file: Joi.object().required()
});


router.post('/v1/upload', upload.single('file'), function (req, res, next) {

    const category = req.body.category;
    const validate = Joi.validate({
        category: category,
        file: req.file
    }, uploadSchema);

    console.log('Category : ', category, ', File : ', req.file)

    if (validate.error) {
        console.log(validate.error)
        return res.send({code: 998,message: "Upload file fail",results: validate.error.details.message})
    }

    const fileStream = req.file.buffer;
    if (!Buffer.isBuffer(fileStream)) {
        return res.send({code: 998,message: "file not found",results: validate.error.details});
    }

    const metaData = {
        category: category,
        original_name: req.file.originalname,
        mime_type: req.file.mimetype
    }

    nexS3.upLoadFile(category, fileStream, req.file.originalname, metaData, function (err, results) {
        if (err) {
            console.log(err)
            return res.send({code: 998, message: "Upload file fail", results: err})
        } else{

            const result = {
                category: category,
                original_name: req.file.originalname,
                fileName: results.fileName,
                link: results.location
            };
            return res.send({code: 0, message: "Upload file Success", result: result})
        }
    });


});

router.post('/v1/uploads', upload.array('files', 20), function (req, res, next) {

    const category = req.body.category;
    async.map(req.files, function (file, callback) {
        var fileStream = file.buffer;
        var metaData = {
            category: category,
            original_name: file.originalname,
            mime_type: file.mimetype
        };
        nexS3.upLoadFile(category, fileStream, file.originalname, metaData, function (err, results) {
            callback(null, results)
        });
    }, function (err, results) {
        if (err)
            return res.send({code: 998, message: "Upload file fail", results: err});

        var data = [];
        results.forEach(function (obj) {
            data.push({
                category: category,
                original_name: obj.originalName,
                fileName: obj.fileName,
                link: obj.location,
            })
        });
        return res.send({code: 0, message: "Upload files Success", results: data})
    });

});


router.get('/v1/find/:id', function (req, res, next) {

    var id = req.params.id;
    nexS3.findAll(id, function (err, result) {
        return res.send({code: 0, message: "Success", results: result})
    });

});

router.get('/v1/find/:id/:category', function (req, res, next) {
    var id = req.params.id;
    var category = req.params.category;
    nexS3.findByCategory(id, category, function (err, result) {
        if (err) {
            return res.send({code: 998, message: "Upload file fail", results: err});
        }
        return res.send({code: 0, message: "Success", results: result})
    });

});


router.put('/v1/deletes', function (req, res, next) {
    var files = req.body.files;
    nexS3.deletes(files, function (err, result) {
        if (err) {
            return res.send({code: 998, message: "Delete file fail", results: err});
        }
        return res.send({code: 0, message: "Success", results: files})
    });

});

module.exports = router;