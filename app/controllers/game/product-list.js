const express = require("express");
const router = express.Router();

const service = require("../../common/productService");

const CLIENT_NAME = process.env.CLIENT_NAME || "SPORTBOOK88";

//API Frontend 1
router.get("/getProductList", (req, res) => {
  service.getProductList().then((result) => {
    var dataList = [];
    if (typeof result === "undefined") {
      res.status(500).res.json({ status: 1, message: "Not Found" });
    } else {
      result.filter((value) => {
        if (value.isActive) {
          let data = {
            productName: value.productName,
            productCode: value.productCode,
            lists: value.lists,
          };
          dataList.push(data);
        }
      });
      dataList.forEach((value) => {
        let newActiveVal = [];
        if (CLIENT_NAME === "SPORTBOOK88") {
          value.lists.forEach((val) => {
            delete val["mode"];
          });
        } else {
          let newModeVal = [];
          value.lists.filter((val) => {
            if (val.mode == 1) {
              newModeVal.push(val);
              value.lists = [];
            }
            value.lists = newModeVal;
            delete val["mode"];
          });
        }
        value.lists.filter((val) => {
          if (val.isActive) {
            newActiveVal.push(val);
            value.lists = [];
          }
          value.lists = newActiveVal;
          delete val["isActive"];
        });
      });
      res.status(200).send({ status: 0, data: dataList });
    }
  });
});

router.get("/getGameList", (req, res) => {
  console.log("------------------getGameList------------------");
  let productCode = req.query.code;
  service.getProductList().then((result) => {
    var resultList = {};
    result.forEach((value) => {
      if (value.productCode == productCode) {
        resultList = value;
      }
    });
    if (typeof resultList == "undefined") {
      console.log("Data Not Found");
      res.status(500).send({ status: 1, message: "Data Not Found" });
    } else {
      if (resultList.isActive) {
        let newActiveVal = [];
        if (CLIENT_NAME === "SPORTBOOK88") {
          resultList.lists.filter((val) => {
            delete val["mode"];
          });
          resultList.lists.filter((val) => {
            if (val.isActive) {
              newActiveVal.push(val);
              resultList.lists = [];
            }
            resultList.lists = newActiveVal;
            delete val["isActive"];
          });
          res.status(200).send({ status: 0, data: resultList });
        } else {
          let newModeVal = [];
          resultList.lists.filter((val) => {
            if (val.mode == 1) {
              newModeVal.push(val);
              resultList.lists = [];
            }
            resultList.lists = newModeVal;
            delete val["mode"];
          });
          resultList.lists.filter((val) => {
            if (val.isActive) {
              newActiveVal.push(val);
              resultList.lists = [];
            }
            resultList.lists = newActiveVal;
            delete val["isActive"];
          });
          res.status(200).send({ status: 0, data: resultList });
        }
      } else {
        console.log("Data Not Active");
        res.status(500).send({ status: 2, message: "Data Not Active" });
      }
    }
  });
});

//productList
router.get("/productList", (req, res) => {
  service.getProductList().then((result) => {
    var dataList = [];
    if (typeof result == "undefined") {
      res.json(dataList);
    } else {
      result.forEach((value) => {
        dataList.push({
          productName: value.productName,
          productCode: value.productCode,
          isActive: value.isActive,
        });
      });
      res.json(dataList);
    }
  });
});

router.get("/findProduct", (req, res) => {
  let productCode = req.query.productCode;
  service.getProductList().then((result) => {
    var listResult = {};
    result.forEach((value) => {
      if (value.productCode == productCode) {
        listResult = value;
      }
    });
    res.json(listResult);
  });
});

router.post("/addProduct", (req, res) => {
  let obj = req.body;
  service.addProduct(obj).then((result) => {
    res.json({ name: "success", code: 0 });
  });
});

router.post("/editProduct", (req, res) => {
  let productCode = req.body.productCode;
  let obj = req.body.data;
  service.editProduct(productCode, obj).then((result) => {
    res.json({ data: result, name: "success", code: 0 });
  });
});

//gameList
router.get("/gameList", (req, res) => {
  let productCode = req.query.id;
  let mode = req.query.mode;
  service.getProductList().then((result) => {
    var resultList = {};
    result.forEach((value) => {
      if (value.productCode == productCode) {
        if (mode == "prod") {
          let newModeVal = [];
          value.lists.filter((val) => {
            if (val.mode == 1) {
              newModeVal.push(val);
              value.lists = [];
            }
            value.lists = newModeVal;
          });
        }
        resultList = value;
      }
    });
    res.json(resultList);
  });
});

router.get("/findGameById", (req, res) => {
  let productCode = req.query.productCode;
  let gameId = req.query.id;
  service.getProductList().then((result) => {
    var listResult = {};
    result.forEach((value) => {
      if (value.productCode == productCode) {
        value.lists.forEach((v) => {
          if (v.gameId == gameId) {
            listResult = v;
          }
        });
      }
    });
    res.json(listResult);
  });
});

router.post("/addGame", (req, res) => {
  let productCode = req.body.productCode;
  let obj = req.body.data;
  service.addGame(productCode, obj).then((result) => {
    res.json({ name: "success", code: 0 });
  });
});

router.post("/editGameById", (req, res) => {
  let productCode = req.body.productCode;
  let gameId = req.body.gameId;
  let obj = req.body.data;

  service.editGame(productCode, gameId, obj).then((result) => {
    res.json({ data: result, name: "success", code: 0 });
  });
});

module.exports = router;
