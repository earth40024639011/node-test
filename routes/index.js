const express           = require('express');
const router            = express.Router();
const multer            = require('multer');
const jwt               = require('../app/common/jwt');
const DateUtils         = require('../app/common/dateUtils');
const MemberModel       = require('../app/models/member.model');
const AgentGroupModel   = require('../app/models/agentGroup.model');
const UserModel         = require('../app/models/users.model');
const async             = require("async");
const TokenModel        = require("../app/models/token.model");
const requestIp         = require('request-ip');
const parallel          = require("async/parallel");
const AgentService      = require('../app/common/agentService');
const _                 = require('underscore');
const httpRequest       = require('request');
const CLIENT_NAME       = process.env.CLIENT_NAME || 'SPORTBOOK88';
router.use('/agent/re-cal',            require('../app/controllers/agent/re-cal'));

///SBO
router.use('/sbo/match/today',                  require('../app/controllers/sbo/2_match/2_1_today'));
router.use('/sbo/match/early_market',           require('../app/controllers/sbo/2_match/2_2_early_market'));
router.use('/sbo/match/live',                   require('../app/controllers/sbo/2_match/2_3_live'));
router.use('/sbo/match/parlay',                 require('../app/controllers/sbo/2_match/2_5_parlay'));
router.use('/sbo/match/oetg',                   require('../app/controllers/sbo/2_match/2_6_oe_tg'));
router.use('/sbo/match/1x2dc',                  require('../app/controllers/sbo/2_match/2_7_1x2_dc'));
router.use('/sbo/match/ftfhcs',                 require('../app/controllers/sbo/2_match/2_8_ft_fh_cs'));
router.use('/sbo/match/htft',                   require('../app/controllers/sbo/2_match/2_9_htft'));
router.use('/sbo/match/fglg',                   require('../app/controllers/sbo/2_match/2_10_fglg'));
router.use('/sbo/match/mixParlay',              require('../app/controllers/sbo/2_match/2_11_mix_parlay'));
router.use('/sbo/match/mixStep',                require('../app/controllers/sbo/2_match/2_12_mix_step'));
router.use('/sbo/match/parlayTable',            require('../app/controllers/sbo/2_match/2_13_parlay_table'));

router.use('/sbo/match/betting/today',          require('../app/controllers/sbo/2_match/2_4_betting/2_4_1_today'));
router.use('/sbo/match/betting/early_market',   require('../app/controllers/sbo/2_match/2_4_betting/2_4_2_early_market'));
router.use('/sbo/match/betting/live',           require('../app/controllers/sbo/2_match/2_4_betting/2_4_3_live'));
router.use('/sbo/match/betting/',               require('../app/controllers/sbo/2_match/2_4_betting/2_4_4_betTransaction'));

router.use('/sbo/sports/count',                      require('../app/controllers/sbo/4_sports/4_1_count'));
router.use('/sbo/sports/football/setting/min-max',   require('../app/controllers/sbo/3_setting/1_1_minmax'));
router.use('/sbo/sports/football/setting/rp',        require('../app/controllers/sbo/3_setting/1_2_odds'));
router.use('/sbo/sports/football/setting/sk',        require('../app/controllers/sbo/3_setting/1_3_sk'));
router.use('/sbo/sports/football/setting/sboLeague', require('../app/controllers/sbo/3_setting/1_4_sboLeague'));
router.use('/sbo/sports/football/setting/sboMatch',  require('../app/controllers/sbo/3_setting/1_5_match'));
router.use('/sbo/sports/football/setting/result',    require('../app/controllers/sbo/5_result/5_1_result'));


router.use('/sbo/match/basketball/today',              require('../app/controllers/sbo/5_basketball/2_match/2_1_today'));
router.use('/sbo/match/basketball/betting/today',      require('../app/controllers/sbo/5_basketball/2_match/2_4_betting/2_4_1_today'));
router.use('/sbo/match/basketball/betting/',           require('../app/controllers/sbo/5_basketball/2_match/2_4_betting/2_4_2_betTransaction'));
router.use('/sbo/sports/basketball/setting/min-max',   require('../app/controllers/sbo/5_basketball/3_setting/1_1_minmax'));
router.use('/sbo/sports/basketball/setting/rp',        require('../app/controllers/sbo/5_basketball/3_setting/1_2_odds'));
router.use('/sbo/sports/basketball/setting/sk',        require('../app/controllers/sbo/5_basketball/3_setting/1_3_sk'));
router.use('/sbo/sports/basketball/setting/sboLeague', require('../app/controllers/sbo/5_basketball/3_setting/1_4_sboLeague'));
router.use('/sbo/sports/basketball/setting/result',    require('../app/controllers/sbo/5_basketball/5_1_result'));

router.use('/sbo/match/muaythai/today',              require('../app/controllers/sbo/6_muaythai/2_match/2_1_today'));
router.use('/sbo/match/muaythai/betting/today',      require('../app/controllers/sbo/6_muaythai/2_match/2_4_betting/2_4_1_today'));
router.use('/sbo/match/muaythai/betting/',           require('../app/controllers/sbo/6_muaythai/2_match/2_4_betting/2_4_2_betTransaction'));
router.use('/sbo/sports/muaythai/setting/min-max',   require('../app/controllers/sbo/6_muaythai/3_setting/1_1_minmax'));
router.use('/sbo/sports/muaythai/setting/rp',        require('../app/controllers/sbo/6_muaythai/3_setting/1_2_odds'));
router.use('/sbo/sports/muaythai/setting/sk',        require('../app/controllers/sbo/6_muaythai/3_setting/1_3_sk'));
router.use('/sbo/sports/muaythai/setting/sboLeague', require('../app/controllers/sbo/6_muaythai/3_setting/1_4_sboLeague'));
router.use('/sbo/sports/muaythai/setting/result',    require('../app/controllers/sbo/6_muaythai/6_1_result'));

router.use('/sbo/match/tennis/today',              require('../app/controllers/sbo/7_tennis/2_match/2_1_today'));
router.use('/sbo/match/tennis/betting/today',      require('../app/controllers/sbo/7_tennis/2_match/2_4_betting/2_4_1_today'));
router.use('/sbo/match/tennis/betting/',           require('../app/controllers/sbo/7_tennis/2_match/2_4_betting/2_4_2_betTransaction'));
router.use('/sbo/sports/tennis/setting/min-max',   require('../app/controllers/sbo/7_tennis/3_setting/1_1_minmax'));
router.use('/sbo/sports/tennis/setting/rp',        require('../app/controllers/sbo/7_tennis/3_setting/1_2_odds'));
router.use('/sbo/sports/tennis/setting/sk',        require('../app/controllers/sbo/7_tennis/3_setting/1_3_sk'));
router.use('/sbo/sports/tennis/setting/sboLeague', require('../app/controllers/sbo/7_tennis/3_setting/1_4_sboLeague'));
router.use('/sbo/sports/tennis/setting/result',    require('../app/controllers/sbo/7_tennis/6_1_result'));

router.use('/sbo/match/eSport/today',              require('../app/controllers/sbo/8_eSport/2_match/2_1_today'));
router.use('/sbo/match/eSport/betting/today',      require('../app/controllers/sbo/8_eSport/2_match/2_4_betting/2_4_1_today'));
router.use('/sbo/match/eSport/betting/',           require('../app/controllers/sbo/8_eSport/2_match/2_4_betting/2_4_2_betTransaction'));
router.use('/sbo/sports/eSport/setting/min-max',   require('../app/controllers/sbo/8_eSport/3_setting/1_1_minmax'));
router.use('/sbo/sports/eSport/setting/rp',        require('../app/controllers/sbo/8_eSport/3_setting/1_2_odds'));
router.use('/sbo/sports/eSport/setting/sk',        require('../app/controllers/sbo/8_eSport/3_setting/1_3_sk'));
router.use('/sbo/sports/eSport/setting/sboLeague', require('../app/controllers/sbo/8_eSport/3_setting/1_4_sboLeague'));
router.use('/sbo/sports/eSport/setting/result',    require('../app/controllers/sbo/8_eSport/6_1_result'));

router.use('/sbo/match/tableTennis/today',              require('../app/controllers/sbo/14_tableTennis/2_match/2_1_today'));
router.use('/sbo/match/tableTennis/betting/today',      require('../app/controllers/sbo/14_tableTennis/2_match/2_4_betting/2_4_1_today'));
router.use('/sbo/match/tableTennis/betting/',           require('../app/controllers/sbo/14_tableTennis/2_match/2_4_betting/2_4_2_betTransaction'));
router.use('/sbo/sports/tableTennis/setting/min-max',   require('../app/controllers/sbo/14_tableTennis/3_setting/1_1_minmax'));
router.use('/sbo/sports/tableTennis/setting/rp',        require('../app/controllers/sbo/14_tableTennis/3_setting/1_2_odds'));
router.use('/sbo/sports/tableTennis/setting/sk',        require('../app/controllers/sbo/14_tableTennis/3_setting/1_3_sk'));
router.use('/sbo/sports/tableTennis/setting/sboLeague', require('../app/controllers/sbo/14_tableTennis/3_setting/1_4_sboLeague'));
router.use('/sbo/sports/tableTennis/setting/result',    require('../app/controllers/sbo/14_tableTennis/6_1_result'));

router.use('/sbo/match/volleyball/today',              require('../app/controllers/sbo/15_Volleyball/2_match/2_1_today'));
router.use('/sbo/match/volleyball/betting/today',      require('../app/controllers/sbo/15_Volleyball/2_match/2_4_betting/2_4_1_today'));
router.use('/sbo/match/volleyball/betting/',           require('../app/controllers/sbo/15_Volleyball/2_match/2_4_betting/2_4_2_betTransaction'));
router.use('/sbo/sports/volleyball/setting/min-max',   require('../app/controllers/sbo/15_Volleyball/3_setting/1_1_minmax'));
router.use('/sbo/sports/volleyball/setting/rp',        require('../app/controllers/sbo/15_Volleyball/3_setting/1_2_odds'));
router.use('/sbo/sports/volleyball/setting/sk',        require('../app/controllers/sbo/15_Volleyball/3_setting/1_3_sk'));
router.use('/sbo/sports/volleyball/setting/sboLeague', require('../app/controllers/sbo/15_Volleyball/3_setting/1_4_sboLeague'));
router.use('/sbo/sports/volleyball/setting/result',    require('../app/controllers/sbo/15_Volleyball/6_1_result'));

router.use('/sbo/match/spacialOdds/today',              require('../app/controllers/sbo/21_SpacialOdds/2_match/2_1_today'));
router.use('/sbo/match/spacialOdds/betting/today',      require('../app/controllers/sbo/21_SpacialOdds/2_match/2_4_betting/2_4_1_today'));
router.use('/sbo/match/spacialOdds/betting/',           require('../app/controllers/sbo/21_SpacialOdds/2_match/2_4_betting/2_4_2_betTransaction'));
router.use('/sbo/sports/spacialOdds/setting/min-max',   require('../app/controllers/sbo/21_SpacialOdds/3_setting/1_1_minmax'));
router.use('/sbo/sports/spacialOdds/setting/rp',        require('../app/controllers/sbo/21_SpacialOdds/3_setting/1_2_odds'));
router.use('/sbo/sports/spacialOdds/setting/sk',        require('../app/controllers/sbo/21_SpacialOdds/3_setting/1_3_sk'));
router.use('/sbo/sports/spacialOdds/setting/sboLeague', require('../app/controllers/sbo/21_SpacialOdds/3_setting/1_4_sboLeague'));
router.use('/sbo/sports/spacialOdds/setting/sboMatch',  require('../app/controllers/sbo/21_SpacialOdds/3_setting/1_5_match'));
router.use('/sbo/sports/spacialOdds/setting/result',    require('../app/controllers/sbo/21_SpacialOdds/6_1_result'));

router.use('/ambbo/gateway',                       require('../app/controllers/ambbo/gateway'));

router.use('/ambbo/report',                        require('../app/controllers/ambbo/report'));

router.use('/match',            require('../app/controllers/match/match'));
router.use('/basketball/match', require('../app/controllers/match/basketball'));
router.use('/tennis/match',     require('../app/controllers/match/tennis'));

router.use('/spa/patch',            require('../app/controllers/pathDateScript'));
router.use('/configuration',            require('../app/controllers/configuration/configuration'));
router.use('/basketball/configuration', require('../app/controllers/configuration/configurationBasketball'));

router.use('/maintenance', require('../app/controllers/match/maintenance'));
router.use('/version', require('../app/controllers/version'));
router.use('/upline',  require('../app/controllers/upline'));
// router.use('/auth', require('../app/controllers/authentication/auth'));
router.use('/spa/agentgroup', require('../app/controllers/agent/agentGroup'));
router.use('/spa/user', require('../app/controllers/agent/user'));
router.use('/spa/member', require('../app/controllers/agent/member'));

router.use('/spa/dashboard', require('../app/controllers/agent/dashboard'));


router.use('/bd', require('../app/controllers/agent/backdoor'));

router.use('/spa/bet',              require('../app/controllers/agent/betTransaction'));
router.use('/spa/stock',            require('../app/controllers/agent/stockManagement'));
router.use('/spa/forecast',         require('../app/controllers/agent/forecast'));
router.use('/spa/cash',             require('../app/controllers/agent/cash'));
router.use('/spa/lotto/stock',      require('../app/controllers/agent/stockLottoManagement'));
router.use('/spa/lotto/laos/stock', require('../app/controllers/agent/stockLottoLaosManagement'));
router.use('/spa/lotto/pp/stock',   require('../app/controllers/agent/stockLottoPPManagement'));
router.use('/spa/report',           require('../app/controllers/agent/report'));
router.use('/spa/payment',          require('../app/controllers/agent/payment'));
router.use('/spa/end',              require('../app/controllers/agent/endScore'));
router.use('/end/agent',            require('../app/controllers/agent/endScoreByAgent'));
router.use('/spa/md5/end',          require('../app/controllers/agent/endScoreByMd5'));
router.use('/spa/agent/md5/end',   require('../app/controllers/agent/endScoreByAgentMd5'));

router.use('/spa/website', require('../app/controllers/website/website'));
router.use('/spa/master', require('../app/controllers/agent/commissionType'));

router.use('/spa/announcement', require('../app/controllers/agent/announcement'));
router.use('/spa/announcementFE', require('../app/controllers/agent/announcementFrontEnd'));
router.use('/spa/auth', require('../app/controllers/agent/authorization'));
router.use('/sexy-baccarat', require('../app/controllers/sexy-baccarat/sexy-baccarat2'));
// router.use('/sexy-baccarat2', require('../app/controllers/sexy-baccarat/sexy-baccarat2'));
router.use('/slot-xo', require('../app/controllers/slotXO/slotXO'));
router.use('/spa/amb', require('../app/controllers/agent/ambReport'));
router.use('/spa/patch', require('../app/controllers/pathDateScript'));
router.use('/spa/topup', require('../app/controllers/agent/topup'));

router.use('/spa/robot', require('../app/controllers/agent/robot'));
router.use('/currency',  require('../app/controllers/currency/currency'));
router.use('/print',     require('../app/controllers/print/print'));


router.use('/jdb',     require('../app/controllers/jdbgame/jdbgame'));
router.use('/ds',     require('../app/controllers/ds/ds'));
router.use('/live22',     require('../app/controllers/live22/live22'));
router.use('/spg',     require('../app/controllers/spadegame/spadegame'));
router.use('/lotto',     require('../app/controllers/lotto/lotto'));
router.use('/allbet',     require('../app/controllers/allbet/allbet'));
router.use('/sagame',     require('../app/controllers/sagame/sagame'));
router.use('/pretty',     require('../app/controllers/prettygame/prettygame'));
router.use('/pretty2',     require('../app/controllers/prettygame/prettygameNew'));
router.use('/ambgame',     require('../app/controllers/ambgame/ambgame'));
router.use('/ambgame2',     require('../app/controllers/ambgame/ambgame2'));
router.use('/agg',     require('../app/controllers/aggame/aggame'));
router.use('/dreamgame',     require('../app/controllers/dreamgame/dreamgame'));
router.use('/ameba',     require('../app/controllers/ameba/ameba'));
router.use('/ibc',     require('../app/controllers/ibc/sport'));
router.use('/muaystep2',     require('../app/controllers/muay-step2/muay-step2'));
router.use('/muaystep2/betting/',     require('../app/controllers/muay-step2/betTransaction'));
router.use('/muaystep2/external/',     require('../app/controllers/muay-step2/muay-step2-external'));
router.use('/muay5g',     require('../app/controllers/muay5g/muay-5g'));
router.use('/muay5g/betting/',     require('../app/controllers/muay5g/betTransaction'));
router.use('/apiservice/amb/api/',     require('../app/controllers/muay5g/muay-5g-external'));
router.use('/v1/callback/slotxo/',     require('../app/controllers/slotXO/slotXO'));
router.use('/slotxo/',     require('../app/controllers/slotXO/slot-xo-internal'));
router.use('/ganapati',     require('../app/controllers/ganapati/ganapati'));
router.use('/vt',     require('../app/controllers/vt-slot/vt'));
router.use('/pg',     require('../app/controllers/pg-slot/pg'));
router.use('/ebet',     require('../app/controllers/ebet/ebet'));
router.use('/ecgaming',     require('../app/controllers/ecgame/ecgame'));
router.use('/betgame',     require('../app/controllers/betgame/betgame'));
// router.use('/mannaplay',     require('../app/controllers/mannaplay/mannaplay'));

router.use('/game', require('../app/controllers/game/product-list'))

router.use('/partner/member',   require('../app/controllers/topup/partner'));
router.use('/member',           require('../app/controllers/topup/member'));

module.exports = function (passport) {

    router.post('/spa/user/login', function (req, res, next) {

        passport.authenticate('local', function (err, user, message) {

            if (user) {
                let userToken = JSON.parse(JSON.stringify(user));
                const accessToken = jwt.generateToken(userToken);
                const refreshToken = jwt.generateRefreshToken(userToken);
                // const clientIp = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
                let clientIp = req.headers['cf-connecting-ip'] || req.headers['x-forwarded-for'] || req.connection.remoteAddress;
                try {
                    clientIp = clientIp.split(':').pop();
                } catch (e) {
                    clientIp = '9.8.7.6'
                }

                async.series([callback => {
                    UserModel.update({_id: user._id}, {
                        $set: {
                            ipAddress: clientIp,
                            lastLogin: DateUtils.getCurrentDate()
                        }
                    }, function (err, response) {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, response);
                        }
                    });

                }, callback => {

                    let tokenBody = {
                        username: user.username,
                        uniqueId: user.uid,
                        type:'AGENT',
                        createdAt: DateUtils.getCurrentDate()
                    };

                    TokenModel.update({username: user.username}, tokenBody, {upsert: true}, function (err, response) {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, response);
                        }
                    });

                }], function (err, response) {

                    res.send({
                        code: 0,
                        message: "success",
                        result: {
                            profile: user,
                            access_token: accessToken,
                            refresh_token: refreshToken
                        }
                    });
                });
            } else {
                return res.send({message: message.message, result: null, code: message.code});
            }
        })(req, res, next);
    });

    router.post('/spa/member/login', function (req, res, next) {

        passport.authenticate('member', function (err, user, message) {

            if (user) {
                let userToken = JSON.parse(JSON.stringify(user));
                delete userToken.limitSetting;
                delete userToken.configuration;
                const accessToken = jwt.generateToken(userToken);
                const refreshToken = jwt.generateRefreshToken(userToken);

                let clientIp = req.headers['cf-connecting-ip'] || req.headers['x-forwarded-for'] || req.connection.remoteAddress;
                try {
                    clientIp = clientIp.split(':').pop();
                } catch (e) {
                    clientIp = '9.8.7.6'
                }
                // const clientIp = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

                async.series([callback => {
                    
                    MemberModel.update({_id: user._id}, {
                        $set: {
                            ipAddress: clientIp,
                            lastLogin: DateUtils.getCurrentDate()
                        }
                    }, function (err, response) {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, response);
                        }
                    });

                }, callback => {

                    let tokenBody = {
                        username: user.username,
                        uniqueId: user.uid,
                        type:'MEMBER',
                        createdAt: DateUtils.getCurrentDate()
                    };

                    TokenModel.update({username: user.username}, tokenBody, {upsert: true}, function (err, response) {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, response);
                        }
                    });

                }], function (err, response) {

                    if (err)
                        return res.send({message: "error", result: err, code: 999});

                    res.send({
                        code: 0,
                        message: "success",
                        result: {
                            profile: user,
                            access_token: accessToken,
                            refresh_token: refreshToken
                        }
                    });
                });
            } else {
                return res.send({message: message.message, result: null, code: message.code});
            }
        })(req, res, next);

    });

    router.post('/baccarat/login', function (req, res, next) {

        passport.authenticate('member', (err, user, message) =>{
            if (user) {
                const groupId = user.group._id;
                let userToken = JSON.parse(JSON.stringify(user));
                const limitSetting = userToken.limitSetting;
                delete userToken.limitSetting;
                delete userToken.configuration;
                const accessToken = jwt.generateToken(userToken);
                const refreshToken = jwt.generateRefreshToken(userToken);
                // const clientIp = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
                let clientIp = req.headers['cf-connecting-ip'] || req.headers['x-forwarded-for'] || req.connection.remoteAddress;
                try {
                    clientIp = clientIp.split(':').pop();
                } catch (e) {
                    clientIp = '9.8.7.6'
                }
                console.log('++++++++++');
                console.log(req);
                async.series([callback => {

                    MemberModel.update({_id: user._id}, {
                        $set: {
                            ipAddress: clientIp,
                            lastLogin: DateUtils.getCurrentDate()
                        }
                    }, function (err, response) {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, response);
                        }
                    });

                }, callback => {

                    let tokenBody = {
                        username: user.username,
                        uniqueId: user.uid,
                        type:'MEMBER',
                        createdAt: DateUtils.getCurrentDate()
                    };

                    TokenModel.update({username: user.username}, tokenBody, {upsert: true}, function (err, response) {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, response);
                        }
                    });

                }], function (err, response) {

                    if (err) {
                        return res.send({
                            message: "error",
                            result: err,
                            code: 999
                        });
                    } else {
                        parallel([
                                (callback) => {
                                    AgentService.getSexyStatus(groupId, (err, response) => {
                                        callback(null, response);
                                    });
                                },

                                (callback) => {
                                    AgentService.getLotusStatus(groupId, (err, response) => {
                                        callback(null, response);
                                    });
                                },

                                (callback) => {
                                    AgentService.getLive22Status(groupId, (err, response) => {
                                        callback(null, response);
                                    });
                                },

                                (callback) => {
                                    AgentService.getSaGameStatus(groupId, (err, response) => {
                                        callback(null, response);
                                    });
                                },

                                (callback) => {
                                    AgentService.getAllBetStatus(groupId, (err, response) => {
                                        callback(null, response);
                                    });
                                },

                                (callback) => {
                                    AgentService.getSportBookStatus(groupId, (err, response) => {
                                        callback(null, response);
                                    });
                                }
                            ],
                            (error, parallelResult) => {
                                if (error) {
                                    return res.send({
                                        code    : 9999,
                                        message : error
                                    });
                                } else {
                                    let [
                                        sexy,
                                        lotus,
                                        live22,
                                        sa,
                                        allBet,
                                        sportBook
                                    ] = parallelResult;

                                    console.log('---------------------------');
                                    console.log(sexy);
                                    console.log(lotus);
                                    console.log(live22);
                                    console.log(sa);
                                    console.log(allBet);
                                    console.log(sportBook);
                                    console.log('---------------------------');

                                    res.send({
                                        code: 0,
                                        message: "success",
                                        result: {
                                            profile: user,
                                            access_token: accessToken,
                                            refresh_token: refreshToken,
                                            sexyIsEnable:   sexy.isEnable ? limitSetting.casino.sexy.isEnable    : false,
                                            lotusIsEnable:  false,// lotus.isEnable ? limitSetting.casino.lotus.isEnable : false,
                                            gameIsEnable:   live22.isEnable ? limitSetting.game.slotXO.isEnable  : false,
                                            saIsEnable:     sa.isEnable ? limitSetting.casino.sa.isEnable  : false,
                                            allbetIsEnable: allBet.isEnable ? limitSetting.casino.allbet.isEnable  : false,
                                            sportBookIsEnable: sportBook.isEnable ? limitSetting.sportsBook.isEnable  : false
                                        }
                                    });
                                }
                            });
                    }
                });
            } else {
                return res.send({message: message.message, result: null, code: message.code});
            }
        })(req, res, next);

    });

    router.post('/redirect/login/:license', function (req, res, next) {
        const license = req.params.license;
        console.log(license);/*
        passport.authenticate('member', (err, user, message) =>{
            if (user) {
                const groupId = user.group._id;
                let userToken = JSON.parse(JSON.stringify(user));

                // delete userToken.limitSetting;
                // delete userToken.configuration;
                userToken.isAutoExternal = true;
                try {
                    if (userToken.isAutoTopUp) {
                        console.log(CLIENT_NAME);
                        if (_.isEqual(CLIENT_NAME, 'BET123')) {
                            if (user.username_lower.includes('sexy') ||
                                user.username_lower.includes('twin') ||
                                user.username_lower.includes('faz') ) {
                                userToken.isAutoExternal = false;
                            }
                        }//
                        console.log(userToken.isAutoExternal);
                        // if (
                        //     (_.isUndefined(userToken.prefixAutoTopUp) || _.isEmpty(userToken.prefixAutoTopUp)) &&
                        //     (_.isUndefined(userToken.prefixAutoTopUpGateway) || _.isEmpty(userToken.prefixAutoTopUpGateway))
                        // ) {
                        //     // console.log('1')
                        //     userToken.isAutoExternal = true;
                        // } else if (
                        //     (!_.isUndefined(userToken.prefixAutoTopUp) && !_.isEmpty(userToken.prefixAutoTopUp)) &&
                        //     (_.isUndefined(userToken.prefixAutoTopUpGateway) ||_.isEmpty(userToken.prefixAutoTopUpGateway))
                        // ) {
                        //     // console.log('2');
                        //     userToken.isAutoExternal = true;
                        // }
                    }
                } catch (e) {
                    console.error(e);
                }
                // console.log(userToken);
                user.isAutoExternal = userToken.isAutoExternal;
                console.log(user.isAutoExternal);

                const limitSetting = userToken.limitSetting;
                delete userToken.limitSetting;
                delete userToken.configuration;
                const accessToken = jwt.generateToken(userToken);
                const refreshToken = jwt.generateRefreshToken(userToken);
                // const clientIp = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
                let clientIp = req.headers['cf-connecting-ip'] || req.headers['x-forwarded-for'] || req.connection.remoteAddress;
                try {
                    clientIp = clientIp.split(':').pop();
                } catch (e) {
                    clientIp = '9.8.7.6'
                }
                async.series([callback => {

                    MemberModel.update({_id: user._id}, {
                        $set: {
                            ipAddress: clientIp,
                            lastLogin: DateUtils.getCurrentDate()
                        }
                    }, function (err, response) {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, response);
                        }
                    });

                }, callback => {

                    let tokenBody = {
                        username: user.username,
                        uniqueId: user.uid,
                        type:'MEMBER',
                        createdAt: DateUtils.getCurrentDate()
                    };

                    TokenModel.update({username: user.username}, tokenBody, {upsert: true}, function (err, response) {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, response);
                        }
                    });

                }], function (err, response) {

                    if (err) {
                        return res.send({
                            message: "error",
                            result: err,
                            code: 999
                        });
                    } else {

                        try {
                            MemberModel
                                .findOne({
                                    username_lower: user.username.toLowerCase()
                                })
                                .populate({
                                    path: 'group', select: '_id'
                                })
                                .exec((err, response) => {
                                    if (err) {
                                        callback(err, null);
                                    } else {
                                        AgentGroupModel.findById(response.group._id)
                                            .select('_id name parentId')
                                            .populate({
                                                path: 'parentId',
                                                select: '_id name parentId',
                                                populate: {
                                                    path: 'parentId',
                                                    select: '_id name parentId',
                                                    populate: {
                                                        path: 'parentId',
                                                        select: '_id name parentId',
                                                        populate: {
                                                            path: 'parentId',
                                                            select: '_id name parentId',
                                                            populate: {
                                                                path: 'parentId',
                                                                select: '_id name parentId',
                                                                populate: {
                                                                    path: 'parentId',
                                                                    select: '_id name parentId',
                                                                    populate: {
                                                                        path: 'parentId',
                                                                        select: '_id name parentId',
                                                                        populate: {
                                                                            path: 'parentId',
                                                                            select: '_id name parentId',
                                                                            populate: {
                                                                                path: 'parentId',
                                                                                select: '_id name parentId',
                                                                                populate: {
                                                                                    path: 'parentId',
                                                                                    select: '_id name parentId',
                                                                                    populate: {
                                                                                        path: 'parentId',
                                                                                        select: '_id name parentId',
                                                                                        populate: {
                                                                                            path: 'parentId',
                                                                                            select: '_id name parentId',
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }).exec((err, response) => {
                                            if (err) {
                                                callback(err, null);
                                            } else {
                                                console.log('getShaUpLine response => ', JSON.stringify(response));

                                                const result = isAllowed(response);
                                                console.log('isAllowed : ', result);
                                                if (result) {
                                                    parallel([
                                                            (callback) => {
                                                                AgentService.getSexyStatus(groupId, (err, response) => {
                                                                    callback(null, response);
                                                                });
                                                            },

                                                            (callback) => {
                                                                AgentService.getLive22Status(groupId, (err, response) => {
                                                                    callback(null, response);
                                                                });
                                                            },

                                                            (callback) => {
                                                                AgentService.getSaGameStatus(groupId, (err, response) => {
                                                                    callback(null, response);
                                                                });
                                                            },

                                                            (callback) => {
                                                                let result = {
                                                                    isEnable: false,
                                                                };
                                                                callback(null, result);
                                                                // AgentService.getAllBetStatus(groupId, (err, response) => {
                                                                //     callback(null, response);
                                                                // });
                                                            },

                                                            (callback) => {
                                                                AgentService.getSportBookStatus(groupId, (err, response) => {
                                                                    callback(null, response);
                                                                });
                                                            }
                                                        ],
                                                        (error, parallelResult) => {
                                                            if (error) {
                                                                return res.send({
                                                                    code    : 9999,
                                                                    message : error
                                                                });
                                                            } else {
                                                                let [
                                                                    sexy,
                                                                    live22,
                                                                    sa,
                                                                    allBet,
                                                                    sportBook
                                                                ] = parallelResult;

                                                                console.log('---------------------------');
                                                                console.log({
                                                                    sexy,
                                                                    live22,
                                                                    sa,
                                                                    allBet,
                                                                    sportBook
                                                                });
                                                                // console.log(limitSetting);
                                                                console.log('---------------------------');

                                                                // limitSetting.casino.allbet.isEnable = _.isUndefined(limitSetting.casino.allbet.isEnable) ? false : limitSetting.casino.allbet.isEnable;
                                                                limitSetting.sportsBook.isEnable = _.isUndefined(limitSetting.sportsBook.isEnable) ? true : limitSetting.sportsBook.isEnable;
                                                                // console.log(limitSetting);
                                                                res.send({
                                                                    code: 0,
                                                                    message: "success",
                                                                    result: {
                                                                        profile: user,
                                                                        access_token: accessToken,
                                                                        refresh_token: refreshToken,
                                                                        sexyIsEnable:   sexy.isEnable ? limitSetting.casino.sexy.isEnable    : false,
                                                                        lotusIsEnable:  false,
                                                                        gameIsEnable:   live22.isEnable ? limitSetting.game.slotXO.isEnable  : false,
                                                                        saIsEnable:     sa.isEnable ? limitSetting.casino.sa.isEnable  : false,
                                                                        // allbetIsEnable: allBet.isEnable ? limitSetting.casino.allbet.isEnable  : false,
                                                                        sportBookIsEnable: sportBook.isEnable ? limitSetting.sportsBook.isEnable  : false
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    //
                                                    // res.send({
                                                    //     code: 0,
                                                    //     message: "success",
                                                    //     result: {
                                                    //         profile: user,
                                                    //         access_token: accessToken,
                                                    //         refresh_token: refreshToken
                                                    //     }
                                                    // });
                                                } else {
                                                    return res.send({message: "user not allow to login to this website.", code: 1002});
                                                }
                                            }
                                        });

                                        const isAllowed = (data) =>{
                                            let result = false;
                                            if (!_.isUndefined(data._id) && !_.isNull(data.name) && !result) {
                                                console.log('======================2');
                                                // console.log('data.parentId => ', JSON.stringify(data.parentId));
                                                console.log(license + ' data._id => ',data._id, ' ', data.name);
                                                console.log(license == data._id);
                                                if (license == data._id) {
                                                    result = true;
                                                    return result;
                                                }
                                                if (!result) {
                                                    return isAllowed(data.parentId);
                                                } else {
                                                    return result
                                                }
                                            } else if (!_.isUndefined(data.parentId) && !_.isNull(data.parentId) && !result) {
                                                console.log('======================');
                                                // console.log('data.parentId => ', JSON.stringify(data.parentId));
                                                console.log(license + ' data.parentId._id => ',data.parentId._id, ' ', data.parentId.name);
                                                console.log(license == data.parentId._id);
                                                if (license == data.parentId._id) {
                                                    result = true;
                                                    return result;
                                                }
                                                if (!result) {
                                                    return isAllowed(data.parentId);
                                                } else {
                                                    return result
                                                }
                                            } else {
                                                return result;
                                            }
                                        }
                                    }
                                });
                        } catch (e) {
                            return res.send({message: "error", result: e, code: 999});
                        }
                    }
                });
            } else {
                return res.send({message: message.message, result: null, code: message.code});
            }
        })(req, res, next);*/
        passport.authenticate('member', (err, user, message) =>{
            if (user) {
                const groupId = user.group._id;
                let userToken = JSON.parse(JSON.stringify(user));
                const limitSetting = userToken.limitSetting;
                delete userToken.limitSetting;
                delete userToken.configuration;
                userToken.isAutoExternal = true;
                user.isAutoExternal = true;
                try {
                    if (user.isAutoTopUp) {
                        if (_.isEqual(CLIENT_NAME, 'BET123')) {
                            userToken.isAutoExternal = true;
                        }
                    }
                } catch (e) {
                    console.error(e);
                }
                user.isAutoExternal = userToken.isAutoExternal;
                console.log(user.isAutoExternal);


                const accessToken = jwt.generateToken(userToken);
                const refreshToken = jwt.generateRefreshToken(userToken);
                // const clientIp = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
                let clientIp = req.headers['cf-connecting-ip'] || req.headers['x-forwarded-for'] || req.connection.remoteAddress;
                try {
                    clientIp = clientIp.split(':').pop();
                    if(clientIp.length === 4){
                        clientIp = req.headers['cf-connecting-ip'] || req.headers['x-forwarded-for'] || req.connection.remoteAddress;
                    }
                } catch (e) {
                    clientIp = '9.8.7.6'
                }
                async.series([callback => {

                    MemberModel.update({_id: user._id}, {
                        $set: {
                            ipAddress: clientIp,
                            lastLogin: DateUtils.getCurrentDate()
                        }
                    }, function (err, response) {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, response);
                        }
                    });

                }, callback => {

                    let tokenBody = {
                        username: user.username,
                        uniqueId: user.uid,
                        type:'MEMBER',
                        createdAt: DateUtils.getCurrentDate()
                    };

                    TokenModel.update({username: user.username}, tokenBody, {upsert: true}, function (err, response) {
                        if (err) {
                            callback(err, null);
                        } else {
                            callback(null, response);
                        }
                    });

                }], function (err, response) {

                    if (err) {
                        return res.send({
                            message: "error",
                            result: err,
                            code: 999
                        });
                    } else {

                        try {
                            MemberModel
                                .findOne({
                                    username_lower: user.username.toLowerCase()
                                })
                                .populate({
                                    path: 'group', select: '_id'
                                })
                                .exec((err, response) => {
                                    if (err) {
                                        callback(err, null);
                                    } else {
                                        AgentGroupModel.findById(response.group._id)
                                            .select('_id name parentId')
                                            .populate({
                                                path: 'parentId',
                                                select: '_id name parentId',
                                                populate: {
                                                    path: 'parentId',
                                                    select: '_id name parentId',
                                                    populate: {
                                                        path: 'parentId',
                                                        select: '_id name parentId',
                                                        populate: {
                                                            path: 'parentId',
                                                            select: '_id name parentId',
                                                            populate: {
                                                                path: 'parentId',
                                                                select: '_id name parentId',
                                                                populate: {
                                                                    path: 'parentId',
                                                                    select: '_id name parentId',
                                                                    populate: {
                                                                        path: 'parentId',
                                                                        select: '_id name parentId',
                                                                        populate: {
                                                                            path: 'parentId',
                                                                            select: '_id name parentId',
                                                                            populate: {
                                                                                path: 'parentId',
                                                                                select: '_id name parentId',
                                                                                populate: {
                                                                                    path: 'parentId',
                                                                                    select: '_id name parentId',
                                                                                    populate: {
                                                                                        path: 'parentId',
                                                                                        select: '_id name parentId',
                                                                                        populate: {
                                                                                            path: 'parentId',
                                                                                            select: '_id name parentId',
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }).exec((err, response) => {
                                            if (err) {
                                                callback(err, null);
                                            } else {
                                                // console.log('getShaUpLine response => ', JSON.stringify(response));

                                                const result = _.isUndefined(license) ? true : isAllowed(response);
                                                // console.log('isAllowed : ', result);
                                                if (result) {
                                                    parallel([
                                                            (callback) => {
                                                                AgentService.getSexyStatus(groupId, (err, response) => {
                                                                    callback(null, response);
                                                                });
                                                            },

                                                            (callback) => {
                                                                AgentService.getLive22Status(groupId, (err, response) => {
                                                                    callback(null, response);
                                                                });
                                                            },

                                                            (callback) => {
                                                                AgentService.getSaGameStatus(groupId, (err, response) => {
                                                                    callback(null, response);
                                                                });
                                                            },

                                                            // (callback) => {
                                                            //     AgentService.getAllBetStatus(groupId, (err, response) => {
                                                            //         callback(null, response);
                                                            //     });
                                                            // },

                                                            (callback) => {
                                                                AgentService.getSportBookStatus(groupId, (err, response) => {
                                                                    callback(null, response);
                                                                });
                                                            }
                                                        ],
                                                        (error, parallelResult) => {
                                                            if (error) {
                                                                return res.send({
                                                                    code    : 9999,
                                                                    message : error
                                                                });
                                                            } else {
                                                                let [
                                                                    sexy,
                                                                    live22,
                                                                    sa,
                                                                    // allBet,
                                                                    sportBook
                                                                ] = parallelResult;

                                                                // console.log('---------------------------');
                                                                // console.log({
                                                                //     sexy,
                                                                //     live22,
                                                                //     sa,
                                                                //     allBet,
                                                                //     sportBook
                                                                // });
                                                                // // console.log(limitSetting);
                                                                // console.log('---------------------------');

                                                                // limitSetting.casino.allbet.isEnable = _.isUndefined(limitSetting.casino.allbet.isEnable) ? false : limitSetting.casino.allbet.isEnable;
                                                                limitSetting.sportsBook.isEnable = _.isUndefined(limitSetting.sportsBook.isEnable) ? true : limitSetting.sportsBook.isEnable;
                                                                // console.log(limitSetting);
                                                                res.send({
                                                                    code: 0,
                                                                    message: "success",
                                                                    result: {
                                                                        profile: user,
                                                                        access_token: accessToken,
                                                                        refresh_token: refreshToken,
                                                                        sexyIsEnable:   sexy.isEnable ? limitSetting.casino.sexy.isEnable    : false,
                                                                        lotusIsEnable:  false,
                                                                        gameIsEnable:   live22.isEnable ? limitSetting.game.slotXO.isEnable  : false,
                                                                        saIsEnable:     sa.isEnable ? limitSetting.casino.sa.isEnable  : false,
                                                                        // allbetIsEnable: allBet.isEnable ? limitSetting.casino.allbet.isEnable  : false,
                                                                        sportBookIsEnable: sportBook.isEnable ? limitSetting.sportsBook.isEnable  : false
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    //
                                                    // res.send({
                                                    //     code: 0,
                                                    //     message: "success",
                                                    //     result: {
                                                    //         profile: user,
                                                    //         access_token: accessToken,
                                                    //         refresh_token: refreshToken
                                                    //     }
                                                    // });
                                                } else {
                                                    return res.send({message: 'User or password is incorrect', result: null, code: 1003});
                                                }
                                            }
                                        });

                                        const isAllowed = (data) =>{
                                            let result = false;
                                            if(!_.isNull(data)){
                                                if (!_.isUndefined(data._id) && !_.isNull(data.name) && !result) {
                                                    // console.log('======================2');
                                                    // console.log('data.parentId => ', JSON.stringify(data.parentId));
                                                    // console.log(license + ' data._id => ',data._id, ' ', data.name);
                                                    // console.log(license == data._id);
                                                    if (license == data._id) {
                                                        result = true;
                                                        return result;
                                                    }
                                                    if (!result) {
                                                        return isAllowed(data.parentId);
                                                    } else {
                                                        return result
                                                    }
                                                } else if (!_.isUndefined(data.parentId) && !_.isNull(data.parentId) && !result) {
                                                    // console.log('======================');
                                                    // console.log('data.parentId => ', JSON.stringify(data.parentId));
                                                    // console.log(license + ' data.parentId._id => ',data.parentId._id, ' ', data.parentId.name);
                                                    // console.log(license == data.parentId._id);
                                                    if (license == data.parentId._id) {
                                                        result = true;
                                                        return result;
                                                    }
                                                    if (!result) {
                                                        return isAllowed(data.parentId);
                                                    } else {
                                                        return result
                                                    }
                                                } else {
                                                    return result;
                                                }
                                            }else {
                                                return result;
                                            }
                                        }
                                    }
                                });
                        } catch (e) {
                            return res.send({message: 'User or password is incorrect', result: null, code: 1003});
                        }
                    }
                });
            } else {
                return res.send({message: message.message, result: null, code: message.code});
            }
        })(req, res, next);
    });

    router.get('/ip',function (req,res,next) {

        let ip = req.headers['cf-connecting-ip'] || req.headers['x-forwarded-for'] || req.connection.remoteAddress;
        ip = ip.split(':').pop();

        var headers = {
            'cache-control': 'no-cache',
            'authority': 'ipinfo.io',
            'referer': 'https://ipinfo.io/',
            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36',
        };

        var options = {
            url: 'https://ipinfo.io/widget/'+ip,
            headers: headers
        };

        function callback(error, response, body) {
            console.log(body);
            res.send({ip:ip,
                api:JSON.parse(body)
            });
        }

        httpRequest(options, callback);


    });
    return router;
};

// const pmx = require('pmx').init({
//     http: true,
//     ignore_routes: [/socket\.io/, /notFound/],
//     errors: true,
//     custom_probes: true,
//     network: true,
//     ports: true,
//     transactions: true
// });
