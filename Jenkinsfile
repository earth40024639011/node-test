pipeline {
    agent { label "master" }
    environment{
        sonarqubeURL="http://localhost:9000"
        newVersion="1.0.0-${env.BUILD_NUMBER}"
        appName="sportbook-client-api"
        targetNamespace="snocko-core"
        CONTAINER_IMAGE="birdyman/${appName}"
        hostname="IBD"
        ingressClass="nginx"
        ingressClassProd="nginx-prod"
    }
    stages {
        stage ('Initialize') {
            steps {
                sh '''
                    echo "Initialize"
                    echo "PATH = ${PATH}"
                '''
            }
        }

       stage ('Build Image') {
            steps {
                withCredentials([usernamePassword(credentialsId: 'dockerhub-birdyman', passwordVariable: 'gitlabPassword', usernameVariable: 'gitlabUsername')]) {
                    sh '''
                        echo "Build Image to GitLab"
                        docker login -u ${gitlabUsername} -p ${gitlabPassword}
                        docker build --pull -t ${CONTAINER_IMAGE}:${newVersion} .
                    '''
                }
            }
        }

        stage ('Unit Test') {
            steps {
                sh '''
                    echo "Unit test - SKIP"
                '''
            }
        }

        stage ('Build with Sonarqube') {
            steps {
                sh '''
                    echo "Build with Sonarqube - SKIP"
                '''
            }
        }

        stage ('Quality Gate') {
            steps {
                sh '''
                    echo "Quality Gate - SKIP"
                '''
            }
        }

       stage('Push Deployment image to Registry') {
            parallel {
                stage ('Build image for develop or master branch') {
                    when {
                        anyOf {
                            branch 'develop';
                            branch 'master'
                            }
                        }
                    steps {
                        withCredentials([usernamePassword(credentialsId: 'dockerhub-birdyman', passwordVariable: 'gitlabPassword', usernameVariable: 'gitlabUsername')]) {
                            sh '''
                                echo "Build Image to Registry"
                                docker login -u ${gitlabUsername} -p ${gitlabPassword}
                                docker tag ${CONTAINER_IMAGE}:${newVersion} ${CONTAINER_IMAGE}:${BRANCH_NAME}-${newVersion}
                                docker push ${CONTAINER_IMAGE}:${BRANCH_NAME}-${newVersion}
                                docker tag ${CONTAINER_IMAGE}:${BRANCH_NAME}-${newVersion} ${CONTAINER_IMAGE}:${BRANCH_NAME}
                                docker push ${CONTAINER_IMAGE}:${BRANCH_NAME}
                            '''
                        }
                    }
                }
            }
        }

        stage('Deploy Stage') {
            parallel {
                stage ('Deploy Develop to Dev environment') {
                    when {
                        branch 'develop'
                    }
                    steps {
                        sh '''
                            echo "Create deployment file"
                            cp deployment.yaml deployment-${appName}-${newVersion}.yaml
                            sed -i s/#APPNAME#/${appName}/g deployment-${appName}-${newVersion}.yaml
                            sed -i s/#NAMESPACE#/dev-${targetNamespace}/g deployment-${appName}-${newVersion}.yaml
                            sed -i s/#REGISTRY#/${appName}:${BRANCH_NAME}-${newVersion}/g deployment-${appName}-${newVersion}.yaml
                            sed -i s/#SUBGROUP#/${subgroup}/g deployment-${appName}-${newVersion}.yaml
                            sed -i s/#HOSTNAME#/dev-${hostname}/g deployment-${appName}-${newVersion}.yaml
                            sed -i s/#VERSION#/${newVersion}/g deployment-${appName}-${newVersion}.yaml
                            sed -i s/#INGRESS#/${ingressClass}/g deployment-${appName}-${newVersion}.yaml
                            echo "Deploy Container"
                            kubectl get nodes
                            cat deployment-${appName}-${newVersion}.yaml
                            kubectl apply -f deployment-${appName}-${newVersion}.yaml
                        '''
                    }
                }
                stage ('Deploy Master to Staging environment') {
                    when {
                        branch 'master'
                    }
                    steps {
                        sh '''
                            echo "Create deployment file"
                            cp deployment.yaml deployment-${appName}-${newVersion}.yaml
                            sed -i s/#APPNAME#/${appName}/g deployment-${appName}-${newVersion}.yaml
                            sed -i s/#NAMESPACE#/${targetNamespace}/g deployment-${appName}-${newVersion}.yaml
                            sed -i s/#REGISTRY#/${appName}:${BRANCH_NAME}-${newVersion}/g deployment-${appName}-${newVersion}.yaml
                            sed -i s/#SUBGROUP#/${subgroup}/g deployment-${appName}-${newVersion}.yaml
                            sed -i s/#HOSTNAME#/${hostname}/g deployment-${appName}-${newVersion}.yaml
                            sed -i s/#VERSION#/${newVersion}/g deployment-${appName}-${newVersion}.yaml
                            sed -i s/#INGRESS#/${ingressClassProd}/g deployment-${appName}-${newVersion}.yaml
                            echo "Deploy Container"
                            kubectl get nodes
                            cat deployment-${appName}-${newVersion}.yaml
                            kubectl apply -f deployment-${appName}-${newVersion}.yaml
                        '''
                    }
                }
            }
        }

        stage ('Running Robot Framework') {
            when {
                anyOf {
                    branch 'develop';
                    branch 'master'
                    }
                }
            steps {
                sh '''
                    echo "Running Robot Framework - SKIP"
                '''
            }
        }

        stage ('Archive deployment file to S3') {
            when {
                anyOf {
                    branch 'develop';
                    branch 'master'
                    }
                }
            steps {
                echo "Archive deployment file to S3 - SKIP"
            }
        }
    }
}
