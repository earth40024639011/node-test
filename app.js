const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const passport = require('passport');
const mongoose = require('mongoose');
const multer = require('multer');
const compression = require('compression');
const config = require('config');
const jwt = require('./app/common/jwt');
const requestIp = require('request-ip');
const authorizeService = require('./app/services/authorizeService');
const async = require("async");
const app = express();
const uuid = require('uuid/v1');
const logger = require("./utils/logger");
const _      = require('underscore');
const xmlparser = require('express-xml-bodyparser');
const { Client } = require('@elastic/elasticsearch');


app.use(require('morgan')('response : :url | :status | :res[content-length] - :response-time ms', { "stream": logger.stream }));

logger.debug("Overriding 'Express' logger");

function preBodyParser(fn) {
    return function(req, res, next) {
        delete req.headers['content-encoding'];
        fn(req, res, next);
    }
}

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(preBodyParser(bodyParser.json({limit: '50mb'})));
app.use(preBodyParser(bodyParser.urlencoded({limit: '50mb', extended: true, parameterLimit: 50000})));
app.use(preBodyParser(bodyParser.text()));
app.use(cookieParser());
app.use(compression());
app.use('/upload', express.static('upload'));
app.use(requestIp.mw());
app.use(xmlparser());

// inside middleware handler


// let shortHash = 'SH5a98bed21323f36f4ccb1053'//getShortAg(groupType);
//
// const {Pool, Client} = require('pg');
//
// const client = new Client({
//     user: 'postgres',
//     host: 'sportbooks-instance-1.cmvhmebsh7w7.ap-southeast-1.rds.amazonaws.com',
//     database: 'sportbooks',
//     password: 'iTIzIJXq51jihTnye3YY',
//     port: 5432,
// })
// client.connect();
// let qs = `SELECT sum(amount),sum(member_credit),count(*) FROM bet_transactions WHERE hash_agent LIKE '%${shortHash}%' group by hash_agent`;
// console.log(qs)
// client.query(qs, (err, res) => {
//     console.log(err, JSON.stringify(res));
//
//     client.end()
// });


const CLIENT_NAME = process.env.CLIENT_NAME || 'SPORTBOOK88';
console.log('CLIENT_NAME', CLIENT_NAME);


if(CLIENT_NAME == 'AMB_SPORTBOOK'){
    process.env['ELS_INDEX_NAME'] = 'sportbook-ambbet_latest';
}else if(CLIENT_NAME == 'CSR'){
    process.env['ELS_INDEX_NAME'] = 'sportbook-csr_latest';
}else if(CLIENT_NAME == 'FAST98'){
    process.env['ELS_INDEX_NAME'] = 'sportbook-fastbet98_latest';
}else if(CLIENT_NAME == 'BET123'){
    process.env['ELS_INDEX_NAME'] = 'sportbook-123bet*';
}else if(CLIENT_NAME == 'MGM'){
    process.env['ELS_INDEX_NAME'] = 'sportbook-mgm_latest';
}else if(CLIENT_NAME == 'SB234'){
    process.env['ELS_INDEX_NAME'] = 'sportbook-sb234_latest';
}else if(CLIENT_NAME == 'FIN88') {
    process.env['ELS_INDEX_NAME'] = 'sportbook-fin88_latest';
}else if(CLIENT_NAME == 'FOXZ') {
    process.env['ELS_INDEX_NAME'] = 'sportbook-foxz168_latest';
}else if(CLIENT_NAME == 'IFM789') {
    process.env['ELS_INDEX_NAME'] = 'sportbook-ifm789_latest';
}else if(CLIENT_NAME == 'UZIBET'){
    process.env['ELS_INDEX_NAME'] = 'sportbook-uzibets-*';
}else if(CLIENT_NAME == 'SBO111'){//allstar
    process.env['ELS_INDEX_NAME'] = 'sportbook-allstar55-*';
}else if(CLIENT_NAME == 'PROBET'){
    process.env['ELS_INDEX_NAME'] = 'sportbook-iprobet-*';
}else if(CLIENT_NAME == 'ALLSTAR55'){//viewbet
    process.env['ELS_INDEX_NAME'] = 'sportbook-viewbet-*'
}else if(CLIENT_NAME == 'AMGOAL'){//viewbet
    process.env['ELS_INDEX_NAME'] = 'sportbook-amgoal-*'
}

// process.env['ELS_INDEX_NAME'] = 'sportbook-allstar55-2020-*';


function configReport() {

    if(_.isEqual(CLIENT_NAME, 'AMB_SPORTBOOK')){
        process.env['REPORT_TASK_HOUR'] = 6;
    }else if(_.isEqual(CLIENT_NAME, 'CSR')){
        process.env['REPORT_TASK_HOUR'] = 4;
    }else if(_.isEqual(CLIENT_NAME, 'IRON')){
        process.env['REPORT_TASK_HOUR'] = 12;
    }else if(_.isEqual(CLIENT_NAME, 'MGM')){
        process.env['REPORT_TASK_HOUR'] = 24;
    }else if(_.isEqual(CLIENT_NAME, 'GOAL168')){
        process.env['REPORT_TASK_HOUR'] = 12;
    }else if(_.isEqual(CLIENT_NAME, 'MARDUO')){
        process.env['REPORT_TASK_HOUR'] = 12;
    }else if(_.isEqual(CLIENT_NAME, 'FOXZ')){
        process.env['REPORT_TASK_HOUR'] = 6;
    }else if(_.isEqual(CLIENT_NAME, 'DD88BET')){
        process.env['REPORT_TASK_HOUR'] = 12;
    }else if(_.isEqual(CLIENT_NAME, 'BET123')){
        process.env['REPORT_TASK_HOUR'] = 4;
    }else if(_.isEqual(CLIENT_NAME, 'SB234')){
        process.env['REPORT_TASK_HOUR'] = 6;
    }else if(_.isEqual(CLIENT_NAME, 'IFM789')){
        process.env['REPORT_TASK_HOUR'] = 12;
    } else if(_.isEqual(CLIENT_NAME, 'ALLSTAR55')){
        process.env['REPORT_TASK_HOUR'] = 24;
    } else if(_.isEqual(CLIENT_NAME, 'BSBBET')){
        process.env['REPORT_TASK_HOUR'] = 24;
    } else {
        process.env['REPORT_TASK_HOUR'] = 4;
    }
}
configReport();
const connectWithRetry = function () {
    const MGDB_URI    = `${process.env.MGDB_URI}`;
    console.log('MGDB_URI => ', MGDB_URI);


    mongoose.connect(MGDB_URI, {useNewUrlParser: true}, function (err, db) {
        if (!err) {
            console.info("Host : ", db.host);
            console.info("Database Name : ", db.name);
            console.info("Port Number : ", db.port);
            console.info("Database connected ");
        } else {
            console.error(err);

            //connectWithRetry();
        }
    });
};
connectWithRetry();

// Elasticsearch connect

const esClient = new Client({
    cloud: {
        id: 'sportbook:YXAtc291dGhlYXN0LTEuYXdzLmZvdW5kLmlvJDQwNDY3N2ZjZWU0NTQ2NTg5ZDc1ZGQwMGUxMTYwZDMzJGQ3NDMzN2VmOTQ4NjRiNGY5YjA1MDhlYjU4ZDBlZWI3'
    },
    auth: {
        username: 'elastic',
        password: 'awZMM4jlvfpmqgklVXaTE3Nc'
    }
});


process.on('SIGINT', function(){
    mongoose.connection.close(function(){
        console.log("Mongoose default connection is disconnected due to application termination");
        process.exit(0)
    });
});


app.locals.ESCLIEND = esClient;


app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header("Access-Control-Allow-Headers", "Content-Type,accept,access_token,X-Requested-With,Authorization");

    const token = req.header('authorization') || '';
    req.clientName = CLIENT_NAME;
    req.requestId = uuid();

    if (req.method === 'OPTIONS') {
        let headers = {};
        // IE8 does not allow domains to be specified, just the *
        // headers["Access-Control-Allow-Origin"] = req.headers.origin;
        headers["Access-Control-Allow-Origin"] = "*";
        headers["Access-Control-Allow-Methods"] = "POST, GET, PUT, DELETE, OPTIONS";
        headers["Access-Control-Allow-Credentials"] = false;
        headers["Access-Control-Max-Age"] = '86400'; // 24 hours
        headers["Access-Control-Allow-Headers"] = "Content-Type,accept,access_token,X-Requested-With,Authorization";
        res.writeHead(200, headers);
        res.end();

    } else {
        logger.info(req.requestId+ ' | request : '+req.method +' '+req.url);
        let clientIp = req.headers['cf-connecting-ip'] || req.headers['x-forwarded-for'] || req.connection.remoteAddress;

        // console.log('-----------client------------',clientIp);
        // console.log(req.url);
        if (!req.url.startsWith('/spa/cash/memberWinLose') &&
            !req.url.startsWith('/spg/callback') &&
            !req.url.startsWith('/ameba/callback') &&
            !req.url.startsWith('/sagame/callback/') &&

            !req.url.startsWith('/sagame/patch') &&

            // !req.url.startsWith('/ambgame/') &&
            !req.url.startsWith('/agg/rest/') &&
            !req.url.startsWith('/dreamgame/callback/') &&
            !req.url.startsWith('/bd') &&
            !req.url.startsWith('/allbet/verify_token') &&
            !req.url.startsWith('/lotto/callback') &&
            // !req.url.startsWith('/spa/lotto/stock/export-betout') &&
            !req.url.startsWith('/spa/patch') &&
            !req.url.startsWith('/spa/api') &&
            !req.url.startsWith('/spa/topup') &&
            !req.url.startsWith('/partner/member') &&
            !req.url.startsWith('/member') &&
            !req.url.startsWith('/member/winLose2') &&
            !req.url.startsWith('/spa/member/clear-member-cash') &&
            !req.url.startsWith('/spa/website') &&
            // !req.url.startsWith('/basketball/match') &&
            !req.url.startsWith('/currency') &&
            !req.url.startsWith('/spa/robot') &&
            !req.url.startsWith('/ambbo/report') &&
            !req.url.startsWith('/live22/getGameList') &&
            !req.url.startsWith('/live22/callback') &&
            !req.url.startsWith('/lotus/setUserCash') &&
            !req.url.startsWith('/lotus/getUserCash') &&
            !req.url.startsWith('/live22/callback') &&
            !req.url.startsWith('/spa/report/wl_joker_detail') &&
            !req.url.startsWith('/slot-xo/retrieve-transaction') &&
            !req.url.startsWith('/slot-xo/transaction-total') &&
            (req.url == '/spa/end/endScoreAgentFB' || !req.url.startsWith('/spa/end')) &&
            !req.url.startsWith('/spa/amb') &&
            !req.url.startsWith('/spa/md5/end') &&
            !req.url.startsWith('/configuration') &&
            !req.url.startsWith('/basketball/configuration') &&
            !req.url.startsWith('/version') &&
            !req.url.startsWith('/upline') &&
            !req.url.startsWith('/spa/stock/cancel-ticket-match') &&
            !req.url.startsWith('/spa/announcement/backend') &&
            !req.url.startsWith('/spa/announcementFE/home') &&
            !req.url.startsWith('/redirect/login') &&
            req.url !== '/spa/auth/refresh' &&
            req.url !== '/spa/user/login' &&
            req.url !== '/spa/member/login' &&
            req.url !== '/baccarat/login' &&
            req.url !== '/spa/bet/ticket_live' &&
            req.url !== '/spa/bet/update/ticket_live' &&
            !req.url.startsWith('/spa/report/reportX') &&
            !req.url.startsWith('/spa/report/reportXY') &&
            !req.url.startsWith('/sexy-baccarat') &&
            !req.url.startsWith('/ip') &&
            !req.url.startsWith('/slotlist/slotList') &&
            // req.url === '/ambbo/gateway' &&
            !req.url.startsWith('/muaystep2/external') &&
            !req.url.startsWith('/ameba/getGameLis') &&
            !req.url.startsWith('/live22/getGameLis') &&
            !req.url.startsWith('/spg/getGameLis') &&
            !req.url.startsWith('/slotxo/getGameLis') &&
            !req.url.startsWith('/ds/getGameLis') &&
            !req.url.startsWith('/ambgame/getGameLis') &&
            !req.url.startsWith('/ambgame2/getGameList') &&
            !req.url.startsWith('/apiservice/amb/api') &&
            !req.url.startsWith('/v1/callback/slotxo') &&
            !req.url.startsWith('/ganapati/callback') &&
            !req.url.startsWith('/pg/callback') &&
            !req.url.startsWith('/ebet/callback') &&
            !req.url.startsWith('/betgame/callback') &&
            !req.url.startsWith('/ecgaming') &&
            !req.url.startsWith('/game') &&
            !req.url.startsWith('/slot-xo/login-factor') ||
            req.url.startsWith('/sexy-baccarat/login') ||
            req.url.startsWith('/sexy-baccarat2/login')) {


            if (token) {

                try {
                    const decoded = jwt.verify(token);
                    req.userInfo = decoded.data;

                    authorizeService.isValidToken(req.userInfo.username,req.userInfo.uid, function (err, response) {
                        if (err) {
                            if(err == 1005){
                                res.send({
                                    code: 1005,
                                    message: 'You have been signed out due to multiple login. Please sign in again.',
                                    error: 'You have been signed out due to multiple login. Please sign in again.'
                                });
                            }else if(err === 1003) {
                                res.send({
                                    code: 1003,
                                    message: 'session expired',
                                    error: 'session expired'
                                });

                            }else{
                                res.send({
                                    code: 999,
                                    message: err.message,
                                    error: err
                                });
                            }
                        }else{
                            next();
                        }
                    });

                } catch (err) {
                    res.send({
                        code: 1003,
                        message: err.message,
                        error: err.name
                    });
                }
            } else {
                res.send({
                    code: 1003,
                    message: 'unauthorized',
                    error: 'token not found'
                });
            }
        }else{
            next();
        }
    }
});

// app.all('*', function(req, res, next) {
//     req.meta = {
//         ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
//         timestamp: uuid(),
//         user_agent: req.headers['user-agent'],
//         body: req.body,
//     }
//     return next();
// })



app.use('/upload', express.static('upload'));
app.use(passport.initialize());
app.use(passport.session());

const initPassport = require('./app/passport/init');
initPassport(passport);


const routes = require('./routes/index')(passport);
app.use('/', routes);

app.use(function (req, res, next) {
    let err = new Error('Not Found');
    // err.status = 404;
    // res.render('error', {
    //     message: err.message,
    //     error: {}
    // });

    res.status(415).json({})
});


// app.set('view engine', 'html');

app.use(function (err, req, res, next) {
    console.log(err);

    res.status(415).json({status:'415','message':'unsupported content encoding'})

});

module.exports = app;